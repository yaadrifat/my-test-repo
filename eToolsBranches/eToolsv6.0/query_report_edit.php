<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Published Reports - Modify SQL</title>
<script>
function validate(){
	if (document.editrep.repname.value == ""){
		alert("Report name can not be blank.");
		document.editrep.repname.focus();
		return false;
	}
}
</script>
<?php
include("./includes/oci_functions.php");
include_once "./adodb/adodb.inc.php";
include_once('./adodb/adodb-pager.inc.php'); 
include("db_config.php");
include("./includes/header.php");
require_once('audit_queries.php');


$db = NewADOConnection("oci8");
$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);

if (!$db) die("Connection failed");

?>
<style>
textarea
{
  font-size: 10px;
  color: #0000ff;
  font-family: courier
}
</style>
</head>


<body>
<!--<body onLoad="document.editrep.data.focus();">-->
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	


<?php
if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
	$v_pk_report = $_GET["pk_report"];
	$v_mode = $_GET["mode"];


	$query_sql="select rep_filterapplicable from er_report where pk_report = ".$v_pk_report ;
	$results = $db->Execute($query_sql);
	$v_applicableFilter = $results->fields['REP_FILTERAPPLICABLE'];

	if ($v_mode == 'sql'){
		echo '<div class="navigate">Published Reports - Modify Query</div>';
		$query_sql = "select rep_name,rep_sql_clob as data,rep_hide from er_report where pk_report = ".$v_pk_report ;
	}

	if ($v_mode == 'xsl'){
		echo '<div class="navigate">Published Reports - Modify XSL</div>';
		$query_sql = "select repxsl_name as rep_name,repxsl_xsl as data from er_repxsl where fk_report = ".$v_pk_report ;
	}

	$results = $db->Execute($query_sql);
	$data = $results->fields['DATA'];
	$data = str_replace('&','&amp;',$data);

	$rs_filter = $db->Execute("select repfilter_coldispname,repfilter_keyword from er_repfilter,er_repfiltermap where pk_repfilter = fk_repfilter and repfiltermap_repcat = 'custom reports' order by repfilter_coldispname");

	?>
	<form name="editrep" action="query_report_edit.php" method="post" onsubmit='if (validate() == false) return false;'>

	<?PHP
	if ($v_mode == 'sql'){
?>
	<BR>Report Name: <b><input type="text" size="50" maxlength="50" name="repname" value="<?php echo $results->fields['REP_NAME']; ?>" /></b>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	Hide Report: <input <?PHP echo ($results->fields['REP_HIDE'] == "Y" ? "checked" : ""); ?> type="checkbox" name="rephide"><br><br>
<?PHP
	echo "Select Filters applicable to this report:";
		$counter = 0;
		echo "<TABLE width='100%'>";
		$i = 0;
		while (!$rs_filter->EOF){
			if ($counter==0) echo "<TR>";
			echo "<TD width='20%'>";
			$v_pos = strpos("PAD".$v_applicableFilter,$rs_filter->fields["REPFILTER_KEYWORD"]);
			if (empty($v_pos)) $v_pos = -1;
			if ($v_pos >= 0) {
				echo '<INPUT checked TYPE="CHECKBOX" name="filter_name['.$i.']" value="'.$rs_filter->fields["REPFILTER_COLDISPNAME"].'">'.$rs_filter->fields["REPFILTER_COLDISPNAME"]." - ".$rs_filter->fields["REPFILTER_KEYWORD"];
			} else {
				echo '<INPUT TYPE="CHECKBOX" name="filter_name['.$i.']" value="'.$rs_filter->fields["REPFILTER_COLDISPNAME"].'">'.$rs_filter->fields["REPFILTER_COLDISPNAME"]." - ".$rs_filter->fields["REPFILTER_KEYWORD"];
			}
			echo "</TD>";
			echo '<INPUT type="hidden" name="filter_keyword['.$i.']" value="'.$rs_filter->fields["REPFILTER_KEYWORD"].'">';
			$counter++;
			$i++;
			if ($counter==5) $counter = 0;
			if ($counter==0) echo "</TR>";
			$rs_filter->MoveNext();
		}
		echo "</TABLE>";
		echo "<BR>Report SQL:<BR>";
	} else {
?>
	<br>Report Name: <b><?php echo $results->fields['REP_NAME']; ?></b><br><br>
	<input type="hidden" name="repname" value="in xsl" />
<?PHP
		echo "Report XSL:<BR>";
	}
	?>
		<input type="hidden" name="pk_report" value="<?PHP echo $v_pk_report; ?>">
		<input type="hidden" name="mode" value="<?PHP echo $v_mode; ?>">
		
	
		<textarea name="data" rows=25 cols=98 ><?php echo $data; ?></textarea>
		<!-- <br><input type="submit" name="submit" value="Submit"> -->
		<br><input type="image" name="submit" value="Submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />

	</form>

<?PHP if ($v_mode == 'sql'){ ?>

	<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
	<tr>
	<th align="left">Note</th>
	</tr>
	<tr><td align="left" valign="top">
	<p>Filter Usage: Use the keyword after the hyphen - in your SQL prefixed by a colon sign. Keyword is case sensitive.</p>
	<p>Example: If you want to use study filter in a report find the keyword from the applicable filter list, in this case the string is "Study - <b>studyId</b>". The keyword is studyId, use this keyword in your SQL as shown below:</p>
	<p>Sample SQL: SELECT STUDY_NUMBER, STUDY_TITLE FROM ER_STUDY WHERE PK_STUDY IN (<B>:studyId</B>).</p>
	<p>Date Filter can be applied by using keywords <b>:fromDate</b> and <b>:toDate</b></p>
	<p>Sample SQL with date filter: SELECT * FROM ER_STUDY WHERE CREATED_ON between <b>TO_DATE(':fromDate','mm/dd/yyyy')</b> and <b>TO_DATE(':toDate','mm/dd/yyyy')</b></p>
	</td></tr>
	</table></div>

		
<?PHP 
	}
} else {
	$v_pk_report = $_POST["pk_report"];
	$v_mode = $_POST["mode"];
	$v_data = $_POST["data"];

	if ($v_mode == 'sql'){
		$v_repname = $_POST["repname"];
		$v_rephide = (isset($_POST["rephide"]) ? "Y": "N");
		if (isset($_POST["filter_name"])) {
			$v_filter_name = $_POST["filter_name"]; 
			$v_filter_keyword = $_POST["filter_keyword"];
		} else {
			$v_filter_name = "";
			$v_filter_keyword = "";
		}

		$v_filter_string = "";
		for ($i=0;$i<count($v_filter_keyword);$i++){
			if (isset($v_filter_name[$i])) {
				$v_filter_string .= $v_filter_keyword[$i].":";
			}
		}

		if (strlen($v_filter_string) > 0) {
			$v_filter_string = "date:".substr($v_filter_string,0,strlen($v_filter_string)-1);
		} else {
			$v_filter_string = "date";
		}
		
		$oldValueRs = $db->Execute("select rep_filterapplicable, rep_name , rep_hide from eres.er_report where pk_report = ".$v_pk_report);		
		
		$oldValueRsArr = array();
		$oldValueRsArr[0] = $oldValueRs->fields['REP_FILTERAPPLICABLE'];
		$oldValueRsArr[1] = $oldValueRs->fields['REP_NAME'];
		$oldValueRsArr[2] = $oldValueRs->fields['REP_HIDE'];
		
		$oldValueXSL = $db->Execute("select rep_sql_clob as rsc from eres.er_report where pk_report = ".$v_pk_report);
		$oldValueXSL = $oldValueXSL->fields['RSC'];

	
		$db->Execute("update er_report set rep_filterapplicable = '".$v_filter_string."',rep_name = '$v_repname', rep_hide = '$v_rephide' where pk_report = ".$v_pk_report) ;
		$v_status = $db->UpdateClob('er_report','rep_sql_clob',stripslashes($v_data),'pk_report='.$v_pk_report); 
		if (!$v_status) print $db->ErrorMsg();
                
                        $grpName = mysql_query("SELECT group_name from et_groups WHERE pk_groups =".$_SESSION["FK_GROUPS"]);	
                        $grpNameVal = mysql_fetch_array($grpName);                            
		
			/*--- AUDIT ---*/
			$rowInfo = array(6,$_SESSION["user"],$grpNameVal['group_name'], "SYSDATE", "SYSDATE", "U", "Edit SQL");
			auditQuery(6,$rowInfo);

			$colArray = array('rep_filterapplicable','rep_name','rep_hide');
			$colvalarray = array($v_filter_string,$v_repname,$v_rephide);
			$tblname = 'er_report';	
			$clob = '';
			colQueries($colArray, $colvalarray, $tblname, $oldValueRsArr, $clob);

			$tblname = 'er_report';	
			//$v_status = $db->Execute("INSERT INTO VELINK.VLNK_AUDIT_COLUMN (FK_AUDITROW, OLD_VALUE_CLOB, NEW_VALUE_CLOB, COLUMN_NAME, TABLE_NAME,REMARKS ) VALUES ((select max(pk_audit) from velink.vlnk_audit_row),'','','rep_sql_clob','".$tblname."','clob')");
                        $v_status = mysql_query("INSERT INTO et_audit_column (FK_AUDITROW, OLD_VALUE_CLOB, NEW_VALUE_CLOB, COLUMN_NAME, TABLE_NAME,REMARKS ) VALUES ((select MAX(PK_AUDIT) from et_audit_row),'','','rep_sql_clob','".$tblname."','clob')");                        
			if (!$v_status) print $db->ErrorMsg();

			//$v_status = $db->UpdateClob('VELINK.VLNK_AUDIT_COLUMN','OLD_VALUE_CLOB',stripslashes($oldValueXSL),'PK_AUDITCOLUMN=(select max(PK_AUDITCOLUMN) from velink.VLNK_AUDIT_COLUMN)');
                        $v_status = mysql_query("UPDATE et_audit_column set OLD_VALUE_CLOB='".addslashes($oldValueXSL)."' where FK_AUDITROW=(select max(PK_AUDIT) from et_audit_row)");
			//if (!$v_status) print $db->ErrorMsg();			
			//$v_status = $db->UpdateClob('VELINK.VLNK_AUDIT_COLUMN','NEW_VALUE_CLOB',stripslashes($v_data),'PK_AUDITCOLUMN=(select max(PK_AUDITCOLUMN) from velink.VLNK_AUDIT_COLUMN)'); 
                        $v_status = mysql_query("UPDATE et_audit_column set NEW_VALUE_CLOB ='".addslashes($v_data)."' where FK_AUDITROW=(select max(PK_AUDIT) from et_audit_row)");                         
			if (!$v_status) print $db->ErrorMsg();
			/*--- END ---*/
	}
	if ($v_mode == 'xsl'){

		$oldvalrepxsl = $db->Execute('select repxsl_xsl from eres.er_repxsl where fk_report='.$v_pk_report); 
		$oldvalrepxsl = $oldvalrepxsl->fields[0];
		if (!$v_status) print $db->ErrorMsg();	
	
	
		$v_status = $db->UpdateClob('er_repxsl','repxsl_xsl',stripslashes($v_data),'fk_report='.$v_pk_report); 
		if (!$v_status) print $db->ErrorMsg();

		//--
		$rowInfo = array(6,$_SESSION["user"],$grpNameVal['group_name'], "SYSDATE", "SYSDATE", "U","Edit SQL");
		auditQuery(6,$rowInfo);

		$tblname = 'er_report';	
		//$v_status = $db->Execute("INSERT INTO VELINK.VLNK_AUDIT_COLUMN (FK_AUDITROW, OLD_VALUE_CLOB, NEW_VALUE_CLOB, COLUMN_NAME, TABLE_NAME,REMARKS ) VALUES ((select max(pk_audit) from velink.vlnk_audit_row),'','','er_repxsl','".$tblname."','clob')");
                $v_status = mysql_query("INSERT INTO et_audit_column (FK_AUDITROW, OLD_VALUE_CLOB, NEW_VALUE_CLOB, COLUMN_NAME, TABLE_NAME,REMARKS ) VALUES ((select max(pk_audit) from et_audit_row),'','','er_repxsl','".$tblname."','clob')");                
		if (!$v_status) print $db->ErrorMsg();

		//$v_status = $db->UpdateClob('VELINK.VLNK_AUDIT_COLUMN','NEW_VALUE_CLOB',stripslashes($v_data),'FK_AUDITROW=(select max(pk_audit) from velink.vlnk_audit_row)'); 
                $v_status = mysql_query("UPDATE et_audit_column set NEW_VALUE_CLOB ='".addslashes($v_data)."' where FK_AUDITROW=(select max(PK_AUDIT) from et_audit_row)"); 
		if (!$v_status) print $db->ErrorMsg();			
		
		//$v_status = $db->UpdateClob('VELINK.VLNK_AUDIT_COLUMN','OLD_VALUE_CLOB',stripslashes($oldvalrepxsl),'FK_AUDITROW=(select max(pk_audit) from velink.vlnk_audit_row)'); 
                $v_status = mysql_query("UPDATE et_audit_column set OLD_VALUE_CLOB ='".addslashes($oldvalrepxsl)."' where FK_AUDITROW=(select max(PK_AUDIT) from et_audit_row)"); 
		if (!$v_status) print $db->ErrorMsg();			
		//--		
	}
	echo "Report Saved";
	$url = "./query_report_manager.php";
	echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";
}

?>
</div>


</body>
</html>
<?php

$db->Close();

}
else header("location: ./index.php?fail=1");
?>
