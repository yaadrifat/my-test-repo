<?php
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Groups</title>

<?php
include("./includes/oci_functions.php");

include("db_config.php");
include("./includes/header.php");
//include ("./txt-db-api.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 



?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Manage Account - Groups</div>
<?PHP
echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=account_groups_edit.php?mode=n&pk_groups=".$_SESSION["FK_GROUPS"].">New Group</a>";

$rs = mysql_query("select * from et_groups");
//$childgroupsQry = mysql_query("SELECT * from et_groups where pk_groups in [".$_SESSION["FK_GROUPS"].",1] or fk_parentgroup =".$_SESSION["FK_GROUPS"]);

?>
<Table border="1" width="100%"><TR>
<TH>NAME</TH>
<TH>DESCRIPTION</TH>
<TH>&nbsp;</TH>
</TR>
<?php

while ($rs_row = mysql_fetch_array($rs)){
?>
    <TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
    <TD><?php echo $rs_row["group_name"] ?></TD>
    <TD><?php echo $rs_row["group_desc"] ?></TD>
    <TD  align="center"><?php 
            // ------ Allowing admin level -------//
            if($_SESSION["FK_GROUPS"] == 1){
                    if($rs_row["pk_groups"] != 1){
                        echo '<a href=account_groups_edit.php?mode=m&pk_groups='.$rs_row["pk_groups"].'> Edit</a> ';
                    }			
            }else{
                //------- Allowing logged-in user level -------//
                if($_SESSION["FK_GROUPS"] == $rs_row["pk_groups"] || $_SESSION["FK_GROUPS"] == $rs_row["fk_parentgroup"]){
                    echo '<a href=account_groups_edit.php?mode=m&pk_groups='.$rs_row["pk_groups"].'> Edit</a> ';
                }
            }
            ?>
    </TD>
<?php
}
?>
</TABLE>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
