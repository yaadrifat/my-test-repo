<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Data</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
</head>
<body>
<div id="fedora-content">	
<div class="navigate">Migrate Data - Standard - Migrate</div>

<?php

echo "Migrating Data...<BR>";

$pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];
$pk_vlnk_adaptor = $_GET["pk_vlnk_adaptor"];

$dbs_Q = "select * from velink.vlnk_pipe where FK_VLNK_ADAPMOD=".$pk_vlnk_adapmod;
$dbs_rs = executeOCIQuery($dbs_Q,$ds_conn);

$query_sql = "select proc_name,proc_user from velink.vlnk_modproc where fk_vlnk_module in (select fk_vlnk_module from velink.vlnk_adapmod where pk_vlnk_adapmod = ".$pk_vlnk_adapmod.")" ;
$results = executeOCIQuery($query_sql,$ds_conn);
$procedure = $results["PROC_USER"][0].'.'.$results["PROC_NAME"][0]."(".$pk_vlnk_adapmod.")";
//print $procedure; exit();

$query_sql = "begin ".$procedure."; end;";
//echo $query_sql; exit;
$results = executeOCIUpdateQuery($query_sql,$ds_conn);
		
echo "Data migrated...<BR>";


$url = "mig_modules.php?pk_vlnk_adaptor=".$pk_vlnk_adaptor;
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";

?>
</div>
</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
