<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> Quick Form</title>
<script>
function formPreview(formPk){
	var win = "form_preview.php?formPk="+formPk;
	window.open(win,'mywin',"toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,top=90,left=150");
}


</script>
</head>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");
include_once("fieldXSL.php");


$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<?php
$v_category = $_POST["category"];
$v_account = $_POST["account"];
$v_formname = $_POST["formname"];
$v_secName = $_POST["secName"];
$v_secSeq = $_POST["secSeq"];
$v_fieldName = $_POST["fieldName"];
$v_fieldDesc = $_POST["fieldDesc"];
$v_fieldType = $_POST["fieldType"];
$v_multiChoice = $_POST["multiChoice"];
$v_fieldId = $_POST["fieldId"];
$v_fieldSeq = $_POST["fieldSeq"];
$v_mandatory = $_POST["mandatory"];
$v_multiValues = $_POST["multiValues"];
	
//$v_account = 53;
//$v_account = 1170;

//echo $v_category;
//echo $v_formname;
//echo count($v_fieldName);

// Insert record in ER_FORMLIB
$v_formname = substr($v_formname,0,49);
$results = executeOCIQuery("SELECT pk_codelst from er_codelst where codelst_type='frmlibstat' and codelst_subtyp='W'",$ds_conn);
$v_form_status = $results["PK_CODELST"][0];
$results = executeOCIQuery("SELECT seq_er_formlib.NEXTVAL as pk_formlib FROM dual",$ds_conn);
$v_pk_formlib = $results["PK_FORMLIB"][0];

//$se_sql = "select form_name from er_formlib where fk_catlib = '".$v_category."' and fk_account = '".$v_account."' and form_name = '".$v_formname."' and record_type <> 'D'";
//$se_sql = "select form_name from er_formlib where  fk_account = '".$v_account."' and form_name = '".$v_formname."' and record_type <> 'D' and form_linkto = 'L'";//
//$se_sql = "select form_name from er_formlib where fk_catlib = '".$v_category."' and fk_account = '".$v_account."' and form_name = '".$v_formname."'";
$se_sql = "select form_name from er_formlib where  fk_account = '".$v_account."' and lower(form_name) = lower('".$v_formname."') and record_type <> 'D' and form_linkto = 'L'";////
//echo $se_sql;
//exit;
$results = executeOCIQuery($se_sql,$ds_conn);
if($results["FORM_NAME"][0] != '') {
	//echo '<div stryle="color:red;">'."Form <b>".$v_formname.'</b> already exists.<BR><BR>';
        echo '<div style="color:red;">';
        echo "Form <b>$v_formname</b> already exists.";
        echo '</div><br><br>';
	echo '<meta http-equiv="refresh" content="2; url=./form_quick.php">';
} else {
	$v_sql = "insert into er_formlib (pk_formlib,fk_catlib,fk_account,form_name,form_desc,form_sharedwith,form_status,form_linkto,form_xslrefresh,record_type) values ($v_pk_formlib,$v_category,$v_account,'$v_formname','$v_formname','A',$v_form_status,'L',1,'N')";
$results = executeOCIUpdateQuery($v_sql,$ds_conn);

// Insert records in er_objectshare
$results = executeOCIQuery("SELECT seq_er_objectshare.NEXTVAL as pk_objectshare FROM dual",$ds_conn);
$v_pk_objectshare = $results["PK_OBJECTSHARE"][0];
$v_sql = "insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type) values ($v_pk_objectshare,1,$v_pk_formlib,$v_account,'A','N')";
$results = executeOCIUpdateQuery($v_sql,$ds_conn);

$v_sql = "insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type,fk_objectshare) select seq_er_objectshare.NEXTVAL,1,$v_pk_formlib,pk_user,'U','N',$v_pk_objectshare from er_user where fk_account = $v_account and usr_type <> 'X'";
$results = executeOCIUpdateQuery($v_sql,$ds_conn);

// Insert record in ER_FORMSEC
$v_section = "";
$v_counter = 0;
for($i=0;$i<count($v_secName);$i++){
	if ($v_section != substr($v_secName[$i],0,49)) {
		$v_section = str_replace("'","''",substr($v_secName[$i],0,49));
		$results = executeOCIQuery("SELECT seq_er_formsec.NEXTVAL as pk_formsec FROM dual",$ds_conn);
		$v_pk_formsec = $results["PK_FORMSEC"][0];
		$v_sql = "insert into er_formsec (pk_formsec,fk_formlib,formsec_name,formsec_seq,formsec_fmt,formsec_repno,record_type) values ($v_pk_formsec,$v_pk_formlib,'$v_section','$v_secSeq[$i]','N',0,'N')";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);
		$v_sections[$v_counter] = $v_secName[$i];
		$v_sections_pk[$v_counter] = $v_pk_formsec;
		$v_counter++;
	}
}


// Insert record in ER_FLDLIB
$results = executeOCIQuery("SELECT seq_er_fldlib.NEXTVAL as pk_field FROM dual",$ds_conn);
$v_pk_field = $results["PK_FIELD"][0];
$v_sql = "insert into er_fldlib (pk_field,fk_account,fld_libflag,fld_name,fld_desc,fld_uniqueid,fld_systemid,fld_type,fld_datatype,record_type,fld_align) values ($v_pk_field,$v_account,'F','Data Entry Date','','er_def_date_01','er_def_date_01','E','ED','N','left')";
$results = executeOCIUpdateQuery($v_sql,$ds_conn);

$fieldXSL = new fieldXSL();
$fieldXSL->fieldName = 'Data Entry Date';
$fieldXSL->fieldId = "er_def_date_01";
$fieldXSL->fieldType = "E";
$fieldXSL->fieldDataType = "ED";
$fieldXSL->fieldMandatory = "1";
$fieldXSL->getFieldXSL();

$fieldXSL->xsl = str_replace(array("<b>","</b>"),"",$fieldXSL->xsl);

$v_sql = "insert into er_formfld (pk_formfld,fk_formsec,fk_field,formfld_seq,formfld_mandatory,formfld_browserflg,record_type,formfld_xsl,formfld_javascr) values (seq_er_formfld.nextval,$v_sections_pk[0],$v_pk_field,0,1,1,'N','".str_replace("'","''",$fieldXSL->xsl)."','".str_replace("'","''",$fieldXSL->js)."')";
$results = executeOCIUpdateQuery($v_sql,$ds_conn);

for($i=0;$i<count($v_fieldName);$i++){

	if (empty($v_mandatory[$i])){
		$v_ismandatory = 0;
	} else {
		$v_ismandatory = 1;
	}
		$v_fldType = "";
		$v_fldDatatype = "";
		$v_fldinstructions = "";
	switch (strtoupper($v_fieldType[$i])){
	case "TEXT":
		$v_fldType = "E";
		$v_fldDatatype = 'ET';
		break;
	case "DATE":
		$v_fldType = "E";
		$v_fldDatatype = 'ED';
		break;
	case "NUMBER":
		$v_fldType = "E";
		$v_fldDatatype = 'EN';
		break;
	case "COMMENT":
		$v_fldType = "C";
		$v_fldDatatype = '';
		$v_ismandatory = 0;
		$v_fldinstructions = stripslashes(str_replace("'","''",$v_fieldName[$i]));
		break;
	case "MULTIPLE":
		$v_fldType = "M";
		switch ($v_multiChoice[$i]){ 
		case "DROPDOWN":
			$v_fldDatatype = 'MD';
			break;
		case "RADIO":
			$v_fldDatatype = 'MR';
			break;
		case "CHECKBOX":
			$v_fldDatatype = 'MC';
			break;
		}
	}

	$results = executeOCIQuery("SELECT seq_er_fldlib.NEXTVAL as pk_field FROM dual",$ds_conn);
	$v_pk_field = $results["PK_FIELD"][0];

	$v_fldname = stripslashes(str_replace("'","''",$v_fieldName[$i]));
	$v_flddesc = stripslashes(str_replace("'","''",$v_fieldDesc[$i]));
	if (($v_fldDatatype == 'MR') || ($v_fldDatatype == 'MC') || ($v_fldDatatype == "MD")) {
		$v_sql = "insert into er_fldlib (pk_field,fk_account,fld_libflag,fld_name,fld_desc,fld_uniqueid,fld_type,fld_datatype,record_type,fld_colcount,fld_align) values ($v_pk_field,$v_account,'F','$v_fldname','$v_flddesc','".str_replace("'","",$v_fieldId[$i])."','$v_fldType','$v_fldDatatype','N',1,'left')";
	} else {
		$v_sql = "insert into er_fldlib (pk_field,fk_account,fld_libflag,fld_name,fld_desc,fld_uniqueid,fld_type,fld_datatype,record_type,fld_align,fld_instructions) values ($v_pk_field,$v_account,'F','$v_fldname','$v_flddesc','".str_replace("'","",$v_fieldId[$i])."','$v_fldType','$v_fldDatatype','N','left','$v_fldinstructions')";
	}
	$results = executeOCIUpdateQuery($v_sql,$ds_conn);

	$v_sql = "insert into er_fldvalidate (pk_fldvalidate,fk_fldlib,record_type) values (seq_er_fldvalidate.nextval,$v_pk_field,'N')";
	$results = executeOCIUpdateQuery($v_sql,$ds_conn);


	if (($v_fldDatatype == "MD") || ($v_fldDatatype == "MR") || ($v_fldDatatype == "MC")){
		$v_values = explode("|",$v_multiValues[$i]);
		for ($j=0;$j<count($v_values);$j++){
			$v_mvalues = explode(":",$v_values[$j]);
			$v_mvalues_seq = $v_mvalues[0];
			$v_mvalues_disp = str_replace("'","''",$v_mvalues[1]);
			if (isset($v_mvalues[2])){
				$v_mvalues_data = str_replace("'","''",$v_mvalues[2]);
			} else {
				$v_mvalues_data = $v_mvalues_disp;
			}
			$v_sql = "insert into er_fldresp (pk_fldresp,fk_field,fldresp_seq,fldresp_dispval,fldresp_dataval,fldresp_score,record_type) values (seq_er_fldresp.nextval,$v_pk_field,$v_mvalues_seq,'$v_mvalues_disp','$v_mvalues_data',0,'N')";
			$results = executeOCIUpdateQuery($v_sql,$ds_conn);
		}
	}
	
	for($j=0;$j<count($v_sections);$j++){
		if ($v_sections[$j] == $v_secName[$i]){
			$v_pk_formsec = $v_sections_pk[$j];
		}
	}

	$results = executeOCIQuery("SELECT fld_systemid from er_fldlib where pk_field = ".$v_pk_field,$ds_conn);
	$v_fldSysId = $results["FLD_SYSTEMID"][0];
	
	$fieldXSL = new fieldXSL();
	$fieldXSL->fieldName = $v_fieldName[$i];
	$fieldXSL->fieldId = $v_fldSysId;
	$fieldXSL->fieldType = $v_fldType;
	$fieldXSL->fieldDataType = $v_fldDatatype;
	$fieldXSL->fieldMandatory = $v_ismandatory;
	$fieldXSL->getFieldXSL();
	
	$fieldXSL->xsl = str_replace(array("<b>","</b>"),"",$fieldXSL->xsl);

	$v_sql = "insert into er_formfld (pk_formfld,fk_formsec,fk_field,formfld_seq,formfld_mandatory,formfld_browserflg,record_type,formfld_xsl,formfld_javascr) values (seq_er_formfld.nextval,$v_pk_formsec,$v_pk_field,$v_fieldSeq[$i],$v_ismandatory,0,'N','".str_replace("'","''",$fieldXSL->xsl)."','".str_replace("'","''",$fieldXSL->js)."')";
	$results = executeOCIUpdateQuery($v_sql,$ds_conn);

}
$v_html = "";
$sql = "begin PKG_FORM.SP_CLUBFORMXSL(:pk_formlib,:html); end;";
$stmt = oci_parse($ds_conn,$sql);

//  Bind the input parameter
oci_bind_by_name($stmt,':pk_formlib',$v_pk_formlib);

// Bind the output parameter
oci_bind_by_name($stmt,':html',$v_html);

oci_execute($stmt);


$sql = "begin PKG_FILLEDFORM.SP_GENERATE_PRINTXSL(".$v_pk_formlib."); end;";
$results = executeOCIUpdateQuery($sql,$ds_conn);

OCICommit($ds_conn);
OCILogoff($ds_conn);
	
echo "Form <b>$v_formname</b> created.<BR><BR>";
echo '<a href=# onclick="formPreview('.$v_pk_formlib.')">Preview</a>';
}



?>
<!--
<SCRIPT LANGUAGE="javascript">
window.open ('<?PHP echo $_SESSION["URL"]; ?>/eres/jsp/refreshFormFields.jsp?formIdList=<?PHP echo $v_pk_formlib; ?>', 'newwindow', config='height=100,width=400, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no')
</SCRIPT>
-->	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
