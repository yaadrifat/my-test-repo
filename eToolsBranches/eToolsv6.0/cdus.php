<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> CDUS</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

$v_exp_definition = $_GET["fk_exp_definition"];
?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">CDUS Submission</div>
<a href="cdus_mainparam.php?fk_exp_definition=<?php echo $v_exp_definition;?>&account=<?PHP echo $_GET['account']; ?>">New Study Submission</a>
<Table border="1"><TR>
<TH>Export #</TH>
<TH>Exported On</TH>
<TH>Export Description</TH>
<TH>Mapping</TH>
<TH>Validate</TH>
<TH>Full Log</TH>
<TH>Error Log</TH>
<TH>Export</TH>
<TH>Data</TH>
<TH>&nbsp;</TH>
</TR>
<?php
$query_sql = "SELECT pk_datamain,to_char(exported_on,'dd-Mon-yyyy hh:mi') as exported_on,fk_exp_definition, datamain_desc from exp_datamain where fk_exp_definition = ".$v_exp_definition." order by pk_datamain desc";

$results = executeOCIQuery($query_sql,$ds_conn);

for ($rec = 0; $rec < $results_nrows; $rec++){
?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
	<TD><?php echo $results["PK_DATAMAIN"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["EXPORTED_ON"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["DATAMAIN_DESC"][$rec] . "&nbsp;"; ?></TD>
<?php
	echo "<td><a href=cdus_mapping.php?fk_exp_definition=".$results["FK_EXP_DEFINITION"][$rec]."&pk_exp_datamain=".$results["PK_DATAMAIN"][$rec].">Edit</a></td>";
	echo "<td><a href=cdus_validate.php?pk_datamain=".$results["PK_DATAMAIN"][$rec]."&fk_exp_definition=".$results["FK_EXP_DEFINITION"][$rec].">Validate</a></td>";

	echo "<td><a href=cdus_logview.php?pk_datamain=".$results["PK_DATAMAIN"][$rec]."&fk_exp_definition=".$results["FK_EXP_DEFINITION"][$rec].">View</a></td>";
	echo "<td><a href=cdus_errlogview.php?pk_datamain=".$results["PK_DATAMAIN"][$rec]."&fk_exp_definition=".$results["FK_EXP_DEFINITION"][$rec].">View</a></td>";

	echo "<td><a href=cdus_export.php?fk_exp_definition=".$results["FK_EXP_DEFINITION"][$rec]."&pk_datamain=".$results["PK_DATAMAIN"][$rec].">Final Export</a></td>";
	echo "<td><a href=cdus_viewdata.php?pk_datamain=".$results["PK_DATAMAIN"][$rec].">View</a></td>";
	echo "<td><a href=cdus_delete_datamain.php?fk_exp_definition=".$results["FK_EXP_DEFINITION"][$rec]."&pk_datamain=".$results["PK_DATAMAIN"][$rec].">Delete</a></td>";
	echo "</TR>";
}
?>
</TABLE>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
