<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> CDUS</title>

<script language="javascript" >
//   var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?~_"; 
function splchar(objstr){
   var iChars = "\'\"<"; 
   for (var i = 0; i < objstr.length; i++) {
  	if (iChars.indexOf(objstr.charAt(i)) != -1) {
  	  alert ("Your prefix has special characters. "+objstr.charAt(i)+" This is not allowed.");
  	return false;
  	}
  }
 }
</script>
 
 
<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	

<div class="navigate">System Settings - Edit</div>
<?php

if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 

$pk_ctrltab = $_GET['pk'];
$key = $_GET['key'];
$key_desc = $_GET['key_desc'];
$ctrl_value = $_GET['ctrl_value'];
$ctrl_desc = $_GET['ctrl_desc'];
$pk_ac = $_GET['pkac'];
$m = $_GET['m'];
$ct = $_GET['ct'];
$sv = $_GET['sv'];
$usr = $_GET['u'];

if(isset($_GET['re'])){
	if($_GET['re']!="ia"){
		$sqlString = "SELECT * from er_account_settings where FK_ACCOUNT=".$pk_ac;
		$sqlrs = executeOCIQuery($sqlString,$ds_conn);	
	}
}


$us = "select usr_firstname || ' ' || usr_lastname AS username, FK_ACCOUNT from er_user where fk_account in (select pk_account from er_account where ac_stat='A') ORDER BY lower(usr_firstname || ' ' || usr_lastname)";
$results = executeOCIQuery($us,$ds_conn);
$users = "";

for ($rec = 0; $rec < $results_nrows; $rec++){
	if($pk_ac==$results["FK_ACCOUNT"][$rec]){
		$users .= "<option value=".$results["FK_ACCOUNT"][$rec]." SELECTED>".$results["USERNAME"][$rec]."</option>";
	}else{
		$users .= "<option value=".$results["FK_ACCOUNT"][$rec].">".$results["USERNAME"][$rec]."</option>";
	} 
}

//if($pk_ac!=""){
if($key==""){
	switch($_GET['re']){
		case "apid":
			echo '<FORM name=systemsettings method=post action=system_settings_edit.php onSubmit="if (splchar(document.systemsettings.prefix.value) == false) return false;">';
			echo '<BR><table  width="100%" border="0" cellpadding="5">';
			if($m=="p"){
				echo '<TR><TD width="10%">Name</TD><TD width="90%">Autogeneration Patient ID</TD></TR>';
			}else{
				echo '<TR><TD width="10%">Name</TD><TD width="90%">Autogeneration Study ID</TD></TR>';	
			}
			echo '<INPUT type="hidden" name="pid" value="psid"/>';		
			echo '<INPUT type="hidden" name="mod" value="'.$m.'"/>';
			echo '<INPUT type="hidden" name="pkacval" value="'.$pk_ac.'"/>';
			echo '<INPUT type="hidden" name="pusr" value="'.$usr.'"/>';			
			echo '<TR><TD width="10%">Value</TD>';			
			if($_GET['id']==0){
				echo '<td><input name="toggleval" type="radio" value="1" />On <input name="toggleval" type="radio" value="0" checked="checked"/>Off</td></tr>';		
			}else{
				echo '<td><input name="toggleval" type="radio" value="1" checked="checked"/>On <input name="toggleval" type="radio" value="0" />Off</td></tr>';		
			}
			if($m=="p"){
				$str = explode("''",$sqlrs["PATIENTID_AUTOGEN_SQL"][0]);
				$te = substr($str[0],8,strlen($str[0])-19);
				echo '<tr><td>SQL Prefix</td><td><INPUT type="text" name="prefix" value="'.$te.'" size="40"/></td></tr>';			
			}else{
				$str = explode("''",$sqlrs["STUDYNUM_AUTOGEN_SQL"][0]);
				$te = substr($str[0],8,strlen($str[0])-19);			
				echo '<tr><td>SQL Prefix</td><td><INPUT type="text" name="prefix" value="'.$te.'" size="40"/></td></tr>';				
			}
			echo "</table>"; ?>
			<BR><input type="image" src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
			<?php
		break;
		
		case "ia":
			echo "<FORM name=systemsettings method=post action=system_settings_edit.php>";
			echo '<BR><table  width="100%" border="0" cellpadding="5">';
			echo '<TR><TD width="10%">Name</TD><TD width="90">Interfield Action</TD></TR>';				
			echo '<INPUT type="hidden" name="pid" value="uia"/>';
			echo '<INPUT type="hidden" name="pusr" value="'.$usr.'"/>';								
			echo '<TR><TD width="10%">Value</TD>';
				if($ct==0){
					echo '<td><input name="toggleval" type="radio" value="1" />On <input name="toggleval" type="radio" value="0" checked="checked"/>Off</td></tr>';		
				}else{
					echo '<td><input name="toggleval" type="radio" value="1" checked="checked"/>On <input name="toggleval" type="radio" value="0" />Off</td></tr>';		
				}
			echo "</table>"; ?>
			<BR><input type="image" src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
			<?php		
		break;
		
		case "ape":
			$proQ = "select * from er_settings where SETTINGS_KEYWORD='com.velos.remoteservice.demographics.service' and SETTINGS_MODNUM=".$pk_ac;
			$proRs = executeOCIQuery($proQ,$ds_conn);			
			echo "<FORM name=systemsettings method=post action=system_settings_edit.php>";
			echo '<BR><table  width="100%" border="0" cellpadding="5">';
			echo '<TR><TD width="10%">Name</TD><TD width="90%">Accelarated Patient Enrollment</TD></TR>';							
			echo '<INPUT type="hidden" name="pid" value="ape"/>';
			echo '<INPUT type="hidden" name="pkcc" value="'.$pk_ac.'"/>';
			echo '<INPUT type="hidden" name="svv" value="'.$sv.'"/>';
			echo '<INPUT type="hidden" name="pusr" value="'.$usr.'"/>';						
			echo '<TR><TD width="15%">Qualified Class Name</TD><TD width="75%"><input size="60" type="text" value="'.$proRs["SETTINGS_VALUE"][0].'" name="apenrollment"/></TD></TR>';
			echo '<TR><TD width="10%">Value</TD>';
				if($sv=="0"){
					echo '<td><input name="toggleval" type="radio" value="1" />On <input name="toggleval" type="radio" value="0" checked="checked"/>Off</td></tr>';		
				}else{
					echo '<td><input name="toggleval" type="radio" value="1" checked="checked"/>On <input name="toggleval" type="radio" value="0" />Off</td></tr>';		
				}
			echo "</table>"; ?>
			<BR><input type="image" src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
			<?php		
		break;
	}
}else{
$v_help = "";
$v_dd = "";
switch ($key) {
	case "site_url":
		$v_help = "Enter web site URL used to access the Velos eResearch Application. This URL is sent in email notifications on account creation and password/e-sign reset.";
		break;
	case "mailsettings":
		$v_help = "SMTP Server address - Velos eResearch application will use this SMTP server to send emails to users.";
		break;
	case "vmailuser":
		$v_help = "If SMTP server requires authentication then specify the mail user.";
		break;
	case "vmailpass":
		$v_help = "If SMTP server requires authentication then specify the mail user password.";
		break;
	case "eresuser":
		$v_help = "Site Administrator email address.";
		break;
	case "irb_app_stat":
		$v_help = "Specify the study status that will be used to identify IRB Approval Status.";
		$v_sql = "select codelst_desc,codelst_subtyp from er_codelst where codelst_type='studystat'";
		$results = executeOCIQuery($v_sql,$ds_conn);
		for ($rec = 0; $rec < $results_nrows; $rec++){
			if ($ctrl_value == $results["CODELST_SUBTYP"][$rec]){
				$v_dd .= "<option value=".$results["CODELST_SUBTYP"][$rec]." SELECTED>".$results["CODELST_DESC"][$rec]."</option>";
			} else {
				$v_dd .= "<option value=".$results["CODELST_SUBTYP"][$rec].">".$results["CODELST_DESC"][$rec]."</option>";
			}
		}
		break;
	case "DBTimezone":
		$v_help = "Identifies the system timezone.";
		$v_sql = "SELECT pk_tz,tz_name as tz_name FROM sch_timezones";
		$results = executeOCIQuery($v_sql,$ds_conn);
		for ($rec = 0; $rec < $results_nrows; $rec++){
			if ($ctrl_desc == $results["PK_TZ"][$rec]){
				$v_dd .= "<option value=".$results["PK_TZ"][$rec]." SELECTED>".$results["TZ_NAME"][$rec]."</option>";
			} else {
				$v_dd .= "<option value=".$results["PK_TZ"][$rec].">".$results["TZ_NAME"][$rec]."</option>";
			}
		}
		break;
}

		echo "<FORM name=systemsettings method=post action=system_settings_edit.php>";
		echo '<INPUT type="hidden" name="pk" value="'.$pk_ctrltab.'"/>';	
		echo '<BR><table  width="100%" border="0" cellpadding="5">';
		echo '<TR><TD width="5%">Name</TD><TD width="95%">'.$key_desc.'</TD></TR>';
		echo '<INPUT type="hidden" name="ctrl_key" value="'.$key.'"/>';
		echo '<INPUT type="hidden" name="svv" value="'.$sv.'"/>';
		echo '<INPUT type="hidden" name="pusr" value="'.$usr.'"/>';			
		if (empty($v_dd)) {
			echo '<TR><TD>Value</TD><TD><INPUT size="75" type="TEXT" name="ctrl_value" value="'.$ctrl_value.'"></TD></TR>';
		} else {
			echo '<TR><TD>Value</TD><TD><SELECT name="ctrl_value">'.$v_dd.'</SELECT></TD></TR>';
		}
		echo "</table>";
?>
	<BR><input type="image" src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
<?PHP }
if (!empty($v_help)) { ?>
	<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
	<tr>
	<th align="left">Note</th>
	</tr>
	<tr><td align="left" valign="top">
	<p>
	      <?PHP echo $v_help; ?>
    </p>
	</td></tr>
	</table></div>
<?PHP }

} else {

$pk = $_POST["pk"];
$key = $_POST["ctrl_key"];
$ctrl_value = $_POST["ctrl_value"];

$usr = $_POST['users'];
$option = $_POST['toggleval'];
$preFix = $_POST['prefix'];
$pusr = $_POST['pusr'];

if ($key == 'mailsettings'){
	$query = "update er_ctrltab set ctrl_desc = '$ctrl_value',ctrl_value = '$ctrl_value' where pk_ctrltab=$pk";
} elseif ($key == 'DBTimezone'){ 
	$query = "update sch_codelst set codelst_subtyp = '$ctrl_value', codelst_desc = (select tz_value from sch_timezones where pk_tz = $ctrl_value) where codelst_type='DBTimezone'";
} else{
	$query = "update er_ctrltab set ctrl_value = '$ctrl_value' where pk_ctrltab='$pk'";
}
$results = executeOCIUpdateQuery($query,$ds_conn);

switch($_POST['pid']){
	case "psid":
		if($preFix!=""){
			$preString = "SELECT '".$preFix."' from dual";
			$preString = str_replace("'","''",$preString);
		}else{
			$preString = "SELECT '' from dual";
			$preString = str_replace("'","''",$preString);
		}		

		$v_pkc = $_POST['pkacval'];
		$er_ac_se = "SELECT * from ER_ACCOUNT_SETTINGS where FK_ACCOUNT=".$v_pkc;
		$ac_rs = executeOCIQuery($er_ac_se, $ds_conn);
		$ac_rs_rows = $results_nrows;

		if($option==1){ //current option is yes (ON)
			if($_POST['mod']=="p"){ // selected is patient
//					$maxPk = "select max(PK_ACCOUNT_SETTINGS) as maxpk, PATIENTID_LASTID from ER_ACCOUNT_SETTINGS group by PK_ACCOUNT_SETTINGS, PATIENTID_LASTID";
					$maxPk = "select PK_ACCOUNT_SETTINGS, PATIENTID_LASTID from ER_ACCOUNT_SETTINGS where PK_ACCOUNT_SETTINGS=(select max(PK_ACCOUNT_SETTINGS) from er_account_settings)";
					$maxrs = executeOCIQuery($maxPk, $ds_conn);
					if($results_nrows>0){
						$nxtval = $maxrs["PK_ACCOUNT_SETTINGS"][0]+1;
						//$nxtlastid = $maxrs["PATIENTID_LASTID"][0]+1;						
					}else{
						$nxtval = 1;																
					}			
				if($ac_rs_rows>0){ // to update the er_settings
					//$er_ac_se_up = "UPDATE ER_ACCOUNT_SETTINGS set PATIENTID_AUTOGEN_SQL='".$preString."', PATIENTID_LASTID=".$nxtlastid." where FK_ACCOUNT=".$v_pkc;
					if($maxrs["PATIENTID_LASTID"][0]>0){
						$er_ac_se_up = "UPDATE ER_ACCOUNT_SETTINGS set PATIENTID_AUTOGEN_SQL='".$preString."' where FK_ACCOUNT=".$v_pkc;					
						executeOCIUpdateQuery($er_ac_se_up,$ds_conn);
					}else{
						$er_ac_se_up = "UPDATE ER_ACCOUNT_SETTINGS set PATIENTID_AUTOGEN_SQL='".$preString."', PATIENTID_LASTID=0 where FK_ACCOUNT=".$v_pkc;				
						executeOCIUpdateQuery($er_ac_se_up,$ds_conn);					
					}
					
					$er_ac_se_up = "UPDATE ER_ACCOUNT_SETTINGS set PATIENTID_AUTOGEN_SQL='".$preString."' where FK_ACCOUNT=".$v_pkc;
					executeOCIUpdateQuery($er_ac_se_up,$ds_conn);
				}else{ // to insert into er_settings					
					$er_ac_se_in = "INSERT into ER_ACCOUNT_SETTINGS (PK_ACCOUNT_SETTINGS, FK_ACCOUNT, PATIENTID_AUTOGEN_SQL, PATIENTID_LASTID) VALUES (".$nxtval.",".$v_pkc.",'".$preString."',0)";
					executeOCIUpdateQuery($er_ac_se_in,$ds_conn);
				}
				$query ="update er_account set AC_AUTOGEN_PATIENT=".$option." where PK_ACCOUNT='".$v_pkc."'";	
				executeOCIUpdateQuery($query,$ds_conn);							
			}else{ // selected is study
//					$maxPk = "select max(PK_ACCOUNT_SETTINGS) as maxpk, STUDYNUM_LASTID from ER_ACCOUNT_SETTINGS group by PK_ACCOUNT_SETTINGS, STUDYNUM_LASTID";
					$maxPk = "select PK_ACCOUNT_SETTINGS, STUDYNUM_LASTID from ER_ACCOUNT_SETTINGS where PK_ACCOUNT_SETTINGS=(select max(PK_ACCOUNT_SETTINGS) from er_account_settings)";
					$maxrs = executeOCIQuery($maxPk, $ds_conn);
					$nxtval = $maxrs["PK_ACCOUNT_SETTINGS"][0]+1;
					//$nxtlastid = $maxrs["STUDYNUM_LASTID"][0]+1; 					
				if($ac_rs_rows>0){
					//$er_ac_se_up = "UPDATE ER_ACCOUNT_SETTINGS set STUDYNUM_AUTOGEN_SQL='".$preString."', STUDYNUM_LASTID=".$nxtlastid." where FK_ACCOUNT=".$v_pkc;
					if($maxrs["STUDYNUM_LASTID"][0]>0){
						$er_ac_se_up = "UPDATE ER_ACCOUNT_SETTINGS set STUDYNUM_AUTOGEN_SQL='".$preString."' where FK_ACCOUNT=".$v_pkc;					
						executeOCIUpdateQuery($er_ac_se_up,$ds_conn);
					}else{
						$er_ac_se_up = "UPDATE ER_ACCOUNT_SETTINGS set STUDYNUM_AUTOGEN_SQL='".$preString."', STUDYNUM_LASTID=0 where FK_ACCOUNT=".$v_pkc;				
						executeOCIUpdateQuery($er_ac_se_up,$ds_conn);					
					}
				}else{
					$er_ac_se_in = "INSERT into ER_ACCOUNT_SETTINGS (PK_ACCOUNT_SETTINGS, FK_ACCOUNT, STUDYNUM_AUTOGEN_SQL, STUDYNUM_LASTID) VALUES (".$nxtval.",".$v_pkc.",'".$preString."',0)";
					executeOCIUpdateQuery($er_ac_se_in,$ds_conn);
				}
				$query ="update er_account set AC_AUTOGEN_STUDY=".$option." where PK_ACCOUNT='".$v_pkc."'";	
				executeOCIUpdateQuery($query,$ds_conn);							
			}
		}else{ // current option is no (OFF)
			$isBothAcOffQ = "SELECT * from ER_ACCOUNT where PK_ACCOUNT=".$v_pkc;
			$isBothAcOffRs = executeOCIQuery($isBothAcOffQ, $ds_conn);
			if($isBothAcOffRs["AC_AUTOGEN_STUDY"][0]==0 && $isBothAcOffRs["AC_AUTOGEN_PATIENT"][0]==0){
				$query ="update er_account set AC_AUTOGEN_STUDY=0, AC_AUTOGEN_PATIENT=0 where PK_ACCOUNT=".$v_pkc;	
				executeOCIUpdateQuery($query,$ds_conn);
				$delQ = "DELETE from ER_ACCOUNT_SETTINGS where FK_ACCOUNT=".$v_pkc;
				executeOCIUpdateQuery($delQ,$ds_conn);
			}else{
				$preString = "SELECT '' from dual";
				$preString = str_replace("'","''",$preString);			
				if($_POST['mod']=="p"){
					if($isBothAcOffRs["AC_AUTOGEN_STUDY"][0]!=0){
						$query ="update er_account set AC_AUTOGEN_PATIENT=".$option." where PK_ACCOUNT=".$v_pkc;	
						executeOCIUpdateQuery($query,$ds_conn);
						$er_ac_se_up = "UPDATE ER_ACCOUNT_SETTINGS set PATIENTID_AUTOGEN_SQL='".$preString."' where FK_ACCOUNT=".$v_pkc;							
						executeOCIUpdateQuery($er_ac_se_up,$ds_conn);						
						
//						$maxPk = "select max(PK_ACCOUNT_SETTINGS) as maxpk, PATIENTID_LASTID from ER_ACCOUNT_SETTINGS group by PK_ACCOUNT_SETTINGS, PATIENTID_LASTID";
/*						$maxPk = "select PK_ACCOUNT_SETTINGS, PATIENTID_LASTID from ER_ACCOUNT_SETTINGS where PK_ACCOUNT_SETTINGS=(select max(PK_ACCOUNT_SETTINGS) from er_account_settings)";
						$maxrs = executeOCIQuery($maxPk, $ds_conn);
						$nxtval = $maxrs["PK_ACCOUNT_SETTINGS"][0]+1;
						//$nxtlastid = $maxrs["PATIENTID_LASTID"][0]+1;
						
						if($ac_rs_rows>0){
							//$er_ac_se_up = "UPDATE ER_ACCOUNT_SETTINGS set PATIENTID_AUTOGEN_SQL='".$preString."', PATIENTID_LASTID=".$nxtlastid." where FK_ACCOUNT=".$v_pkc;
							$er_ac_se_up = "UPDATE ER_ACCOUNT_SETTINGS set PATIENTID_AUTOGEN_SQL='".$preString."' where FK_ACCOUNT=".$v_pkc;							
							executeOCIUpdateQuery($er_ac_se_up,$ds_conn);						
						}else{
							$er_ac_se_in = "INSERT into ER_ACCOUNT_SETTINGS (PK_ACCOUNT_SETTINGS, FK_ACCOUNT, PATIENTID_AUTOGEN_SQL, PATIENTID_LASTID) VALUES (".$nxtval.",".$v_pkc.",'".$preString."',".$nxtlastid.")";
							executeOCIUpdateQuery($er_ac_se_in,$ds_conn);
						} */
					}else{
						$query ="update er_account set AC_AUTOGEN_PATIENT=0 where PK_ACCOUNT=".$v_pkc;	
						executeOCIUpdateQuery($query,$ds_conn);					
						$delQ = "DELETE from ER_ACCOUNT_SETTINGS where FK_ACCOUNT=".$v_pkc;
						executeOCIUpdateQuery($delQ,$ds_conn);
					}										
				}else{
					if($isBothAcOffRs["AC_AUTOGEN_PATIENT"][0]!=0){
						$query ="update er_account set AC_AUTOGEN_STUDY=".$option." where PK_ACCOUNT=".$v_pkc;	
						executeOCIUpdateQuery($query,$ds_conn);
						$er_ac_se_up = "UPDATE ER_ACCOUNT_SETTINGS set STUDYNUM_AUTOGEN_SQL='".$preString."' where FK_ACCOUNT=".$v_pkc;							
						executeOCIUpdateQuery($er_ac_se_up,$ds_conn);						

//						$maxPk = "select max(PK_ACCOUNT_SETTINGS) as maxpk, STUDYNUM_LASTID from ER_ACCOUNT_SETTINGS group by PK_ACCOUNT_SETTINGS, PATIENTID_LASTID";
/*						$maxPk = "select PK_ACCOUNT_SETTINGS, STUDYNUM_LASTID from ER_ACCOUNT_SETTINGS where PK_ACCOUNT_SETTINGS=(select max(PK_ACCOUNT_SETTINGS) from er_account_settings)";
						$maxrs = executeOCIQuery($maxPk, $ds_conn);
						$nxtval = $maxrs["PK_ACCOUNT_SETTINGS"][0]+1;
						$nxtlastid = $maxrs["STUDYNUM_LASTID"][0]+1; 

						if($ac_rs_rows>0){
							$er_ac_se_up = "UPDATE ER_ACCOUNT_SETTINGS set STUDYNUM_AUTOGEN_SQL='".$preString."', STUDYNUM_LASTID=".$nxtlastid."where FK_ACCOUNT=".$v_pkc;
							executeOCIUpdateQuery($er_ac_se_up,$ds_conn);						
						}else{
							$er_ac_se_in = "INSERT into ER_ACCOUNT_SETTINGS (PK_ACCOUNT_SETTINGS, FK_ACCOUNT, STUDYNUM_AUTOGEN_SQL, STUDYNUM_LASTID) VALUES (".$nxtval.",".$v_pkc.",'".$preString."',".$nxtlastid.")";
						}*/
					}else{
						$query ="update er_account set AC_AUTOGEN_STUDY=0 where PK_ACCOUNT=".$v_pkc;	
						executeOCIUpdateQuery($query,$ds_conn);					
						$delQ = "DELETE from ER_ACCOUNT_SETTINGS where FK_ACCOUNT=".$v_pkc;
						executeOCIUpdateQuery($delQ,$ds_conn); 						
					}					
				}
			}
		}		
	break;
	
	case "uia":
		$ctval = $_POST['toggleval'];		
		$ctup = "UPDATE ER_CTRLTAB set CTRL_VALUE='".$ctval."' where CTRL_KEY='use_disp'";
		$results = executeOCIQuery($ctup, $ds_conn);
	break;	

}
OCICommit($ds_conn);
OCILogoff($ds_conn);
echo "Data Saved Successfully !!!";
$url = "./system_settings.php?u=".$pusr;
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";
}
?>
</div>
</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>