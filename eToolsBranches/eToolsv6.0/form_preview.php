<?php
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.
//set_time_limit(0);
//ini_set('memory_limit', '5120M');

if(@$_SESSION["user"]){
?>

<html>
<head>
<link type="text/css" rel="stylesheet" href="./styles/common.css">
    <title>Velos eTools -> Form Preview</title>
<?php
    include("./includes/oci_functions.php");
    //include_once "./adodb/adodb.inc.php";
    $v_pk_formlib = $_GET["formPk"];
    $v_mode = (isset($_GET["mode"])?$_GET["mode"]:"");
    
    if($v_mode == 'print'){
        $v_xslcol = 'form_viewxsl';
    }else{
        $v_xslcol = 'form_xsl';
    }
    // fix for 8642
    //
    //
    //
    //
    //$db = NewADOConnection("oci8");
    //$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);
    
    //$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
    
    //-------- fix for the bug 17902
    //$v_sql = "SELECT x.html.getclobval() FROM (SELECT XMLTRANSFORM(A.FORM_XML, (SELECT sys.XMLTYPE.createXML( REPLACE( REPLACE(b.form_xsl,'[VELFLOATFORMATDEF]','<xsl:decimal-format name=''numFormatDef'' decimal-separator=''.'' grouping-separator='',''  NaN=''''/>'),'[VELNUMFORMATDEF]','<xsl:decimal-format name=''floatFormatDef'' decimal-separator=''.'' grouping-separator='','' NaN=''''/>')) FROM ER_FORMLIB B WHERE pk_formlib = ".$v_pk_formlib."  )) AS html FROM ER_FORMLIB a WHERE pk_formlib =  ".$v_pk_formlib." ) x" ;
    
    // fix for 8642
    $c = oci_connect("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]);
    //$v_sql_xml = "select form_xml from er_formlib where pk_formlib=".$v_pk_formlib;        
    $v_sql_xml = " SELECT XMLSERIALIZE (document FORM_XML AS CLOB) AS FORM_XML FROM ER_FORMLIB WHERE PK_FORMLIB=".$v_pk_formlib;   
    $s = oci_parse($c, $v_sql_xml);
    oci_execute($s);
    $r = oci_fetch_array($s, OCI_ASSOC);
    $v_xml = $r['FORM_XML']->load();   // Treat column data as a LOB descriptor

//    echo "<pre>";
//    print_r($r['']);
//    echo "</pre>";    
//    exit;
        
    //$v_sql_xml = "select form_xml from er_formlib where pk_formlib=".$v_pk_formlib;    
    //$v_sql_xml = " SELECT XMLSERIALIZE (document FORM_XML AS CLOB) AS FORM_XML FROM ER_FORMLIB WHERE PK_FORMLIB=".$v_pk_formlib;   
    //$recordSet1 = $db->Execute($v_sql_xml);
   // $v_xml =  $recordSet1->fields[0];
    
    $v_sql_xsl = "select REPLACE( REPLACE(b.".$v_xslcol.",'[VELFLOATFORMATDEF]','<xsl:decimal-format name=''floatFormatDef'' decimal-separator='''
        ||
        (SELECT CODELST_DESC
        FROM ER_CODELST
        WHERE codelst_type ='num_format'
        AND codelst_subtyp ='decimal_symbol'
        )
        ||''' grouping-separator='''
        ||
        (SELECT CODELST_DESC
        FROM ER_CODELST
        WHERE codelst_type ='num_format'
        AND codelst_subtyp ='group_symbol'
        )
        ||''' NaN=''''/>'),'[VELNUMFORMATDEF]','<xsl:decimal-format name=''numFormatDef'' decimal-separator='''
        ||
        (SELECT CODELST_DESC
        FROM ER_CODELST
        WHERE codelst_type ='num_format'
        AND codelst_subtyp ='decimal_symbol'
        )
        ||''' grouping-separator='''
        ||
        (SELECT CODELST_DESC
        FROM ER_CODELST
        WHERE codelst_type ='num_format'
        AND codelst_subtyp ='group_symbol'
        )
        ||'''  NaN=''''/>')  as form_xsl  from er_formlib b where pk_formlib= ".$v_pk_formlib;
    
    //$recordSet = $db->Execute($v_sql_xsl);
    //$v_xsl =  $recordSet->fields[0];
 
    
    // fix for 8642
    $xsl = oci_parse($c, $v_sql_xsl);
    oci_execute($xsl);
    $v_xsl = oci_fetch_array($xsl);
    $v_xsl = $v_xsl['FORM_XSL']->load();
    
    $xml = new DOMDocument;
    $xml->loadXML($v_xml);

    $xsl = new DOMDocument;
    $xsl->loadXML($v_xsl);

    $proc = new XSLTProcessor;
    $proc->importStyleSheet($xsl);

    $v_html = $proc->transformToXML($xml);
    
    $v_html = str_replace('&amp;','&',$v_html);
    $v_html = str_replace('&quot;','"',$v_html);
    $v_html = str_replace('&apos;',"'",$v_html);
    $v_html = str_replace('&lt;','<',$v_html);
    $v_html = str_replace('&gt;','>',$v_html);
    $v_html = str_replace('&#xa0;',' ',$v_html);
    $v_html = str_replace('VELDQUOTE','"',$v_html);
    $v_html = str_replace('er_textarea_tag<','<',$v_html);
    $v_html = str_replace('[VELSYSTEMDATE]','',$v_html);
    $v_html = str_replace('"../images/jpg/help.jpg"','"./img/help.jpg"',$v_html);
    $v_html = str_replace('"../jsp/images/calendar.jpg"','"./img/calendar.jpg"',$v_html);
    $v_html = str_replace('<input type="image" src="../images/jpg/Submit.gif"','<input type="hidden" src="./img/Submit.gif"',$v_html);
    $v_html = str_replace('<span style="display:none;" id="spec_span"/>','',$v_html);
    
    $pattern = '/(<script language="javascript">)(.*)(<\/script>)/ismU';
    $v_html = preg_replace($pattern, '', $v_html);


    $pattern_1 = '/(<script> function)(.*)(<\/script>)/ismU';
    $v_html = preg_replace($pattern_1, '', $v_html);

    $pattern_2 = '/(onMouseover=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_2, '', $v_html);

    $pattern_3 = '/(onMouseOut=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_3, '', $v_html);

    $pattern_4 = '/(onChange=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_4, '', $v_html);

    $pattern_5 = '/(onClick=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_5, '', $v_html);


    $pattern_6 = '/(onFocus=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_6, '', $v_html);

    $pattern_7 = '/(onBlur=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_6, '', $v_html);

    $pattern_8 = '/(onKeyDown=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_6, '', $v_html);

    $pattern_9 = '/(onKeyPress=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_6, '', $v_html);

    $pattern_10 = '/(onKeyUp=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_6, '', $v_html);

    $pattern_11 = '/(onMouseMove=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_11, '', $v_html);

    $pattern_12 = '/(onMouseUp=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_12, '', $v_html);

    $pattern_13 = '/(onDblClick=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_13, '', $v_html);

    $pattern_14 = '/(onMouseDown=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_14, '', $v_html);

    $pattern_15 = '/(onSelect=")(.*)(")/ismU';
    $v_html = preg_replace($pattern_15, '', $v_html);


    print $v_html;
?>
</head>
<body>
</body>
</html>
<?php
//$db->Close();
}
else header("location: ./index.php?fail=1");
?>
