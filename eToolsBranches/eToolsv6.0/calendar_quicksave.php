<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> Quick Calendar</title>
</head>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");
include("./includes/calendar.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<?PHP

$v_category = $_POST["category"];
$v_eventtype = $_POST["eventtype"];



$v_account = $_POST["account"];
$v_calname = $_POST["calname"];
$v_caldur = $_POST["calduration"];
$v_durunit = $_POST["durunit"];
$v_column = $_POST["column"];
$v_migrate = (isset($_POST["migrate"])?"YES":"");


//$rs_libtype = executeOCIQuery("SELECT PK_CODELST FROM SCH_CODELST where CODELST_TYPE='lib_type' and CODELST_SUBTYP='default'",$ds_conn);
//$v_libtype = $rs_libtype["PK_CODELST"][0];

$results = executeOCIQuery("SELECT event_definition_seq.NEXTVAL as pk_protocol FROM dual",$ds_conn);
$v_pk_protocol = $results["PK_PROTOCOL"][0];

$pk_calStat_q = executeOCIQuery("select pk_codelst from sch_codelst where codelst_type = 'calStatLib'  and codelst_subtyp = 'W'",$ds_conn);
$pk_calStat = $pk_calStat_q["PK_CODELST"][0];

// eT3.3-2 enhancement change occurs
/*$v_sql = "insert into event_def (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,COST,DURATION,USER_ID,FUZZY_PERIOD,STATUS,
			DESCRIPTION,DISPLACEMENT,ORG_ID,EVENT_FLAG,CALENDAR_SHAREDWITH,DURATION_UNIT,FK_VISIT,EVENT_FUZZYAFTER,
			EVENT_DURATIONAFTER,EVENT_DURATIONBEFORE,FK_CATLIB,EVENT_LIBRARY_TYPE) values ($v_pk_protocol, $v_pk_protocol, 'P','$v_calname',0,$v_caldur,
			$v_account,null,'W','$v_calname',0,0,0,'A','$v_durunit',null,null,'D','D',$v_category,$v_eventtype)";*/
$v_sql = "insert into event_def (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,COST,DURATION,USER_ID,FUZZY_PERIOD,FK_CODELST_CALSTAT,
			DESCRIPTION,DISPLACEMENT,ORG_ID,EVENT_FLAG,CALENDAR_SHAREDWITH,DURATION_UNIT,FK_VISIT,EVENT_FUZZYAFTER,
			EVENT_DURATIONAFTER,EVENT_DURATIONBEFORE,FK_CATLIB,EVENT_LIBRARY_TYPE) values ($v_pk_protocol, $v_pk_protocol, 'P','$v_calname',0,$v_caldur,
			$v_account,null,$pk_calStat,'$v_calname',0,0,0,'A','$v_durunit',null,null,'D','D',$v_category,$v_eventtype)";
executeOCIUpdateQuery($v_sql,$ds_conn);

$v_counter = 0;
$k=0;
$j=0;
foreach($v_column as $rows=>$columns){
	$i=0;
	$v_cols = 0;
	foreach($columns as $column){
		if ($v_counter == 0){
			if ($v_cols > 0){
				//echo "column= ".$column.' displacement= '.getDisplacement($column)."<BR>";
				list($v_visit,$v_visit_name) = explode('|',$column);
				//echo "v_visit= ".$v_visit."  v_visit_name= ".$v_visit_name."<BR>";
				if (strlen($v_visit_name) == 0) $v_visit_name = $v_visit;	
				//echo "v_visit2= ".$v_visit."  v_visit_name2= ".$v_visit_name."<BR>";
				list($v_months,$v_weeks,$v_days,$v_disp) = explode('|',getDisplacement($v_visit));

				//echo "v_months= ".$v_months."  v_weeks= ".$v_weeks."  v_days= ".$v_days."  v_disp= ".$v_disp."<BR>";				
				//die();
			
				//if($v_months=="0" && $v_weeks=="0" && $v_days=="0"){
				/*if($v_months=="0" && $v_weeks=="0" && $v_days!="0"){
					$no_interval_flag = 1; //When it is set as NO Interval Define (month, week and days are 0)
				}else{
					$no_interval_flag = 0; //When there is interval set (month, week and days are not equal to 0)
				}*/			
				
				
				//implemented for the bug 6460 and 6459
				if($v_days==0 && $v_months==0 && $v_weeks==0){
					$v_days=0;
				}

				$no_interval_flag = 0;
				$results = executeOCIQuery("SELECT sch_prot_visit_seq.nextval as pk_visit FROM dual",$ds_conn);
				$v_pk_visit = $results["PK_VISIT"][0];
				$v_sql = "insert into sch_protocol_visit (PK_PROTOCOL_VISIT,FK_PROTOCOL,VISIT_NO,VISIT_NAME,DISPLACEMENT,NUM_MONTHS,
				NUM_WEEKS,NUM_DAYS,INSERT_AFTER,INSERT_AFTER_INTERVAL,creator,last_modified_by,NO_INTERVAL_FLAG) values ($v_pk_visit, $v_pk_protocol,$v_cols,'$v_visit_name',$v_disp,
				$v_months,$v_weeks,$v_days,0,0,0,0,$no_interval_flag)";
				executeOCIUpdateQuery($v_sql,$ds_conn);


				$v_visitno[$i] = $v_cols;
				$v_visitdisp[$i] = $v_disp;
				$v_visitpk[$i] = $v_pk_visit;
				$i++;
			}
			$v_cols++;
		} else {
			if ($i == 0) {
				$v_event[$k] = $column;
				$v_eventname = $column;
				$v_event_seq[$k] = $v_counter;
				$k++;
				$i++;
			} else {
				if (strlen($column) > 0) {
					$v_visiteventname[$j] = $v_eventname;
					$v_visiteventno[$j] = $v_counter;
					for($x=0; $x<count($v_visitno); $x++) {
						//echo $v_visitdisp[$x]."|".$v_visitno[$x]."|".$v_cols."+++++++++++++++<BR>";
						if ($v_visitno[$x] == $v_cols) {
							$v_visiteventdisp[$j] = $v_visitdisp[$x];
							$v_visiteventpk[$j] = $v_visitpk[$x];
							//echo "**************************";
						}
					}					
					$j++;
				}
			}
			$v_cols++;
		}
	}
	$v_counter++;
}


for($i=0; $i<count($v_event); $i++) {
	$results = executeOCIQuery("SELECT event_definition_seq.NEXTVAL as pk_event FROM dual",$ds_conn);
	$v_pk_event = $results["PK_EVENT"][0];
	
	/* ---------- Calendar from file description character restriction -------- */
	
	$event_des = substr($v_event[$i],0,200);
	
	$v_sql = "insert into event_def (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,COST,DURATION,USER_ID,FUZZY_PERIOD,STATUS,
				DESCRIPTION,DISPLACEMENT,ORG_ID,EVENT_FLAG,CALENDAR_SHAREDWITH,DURATION_UNIT,FK_VISIT,EVENT_FUZZYAFTER,
				EVENT_DURATIONAFTER,EVENT_DURATIONBEFORE,FK_CATLIB,EVENT_LIBRARY_TYPE) values ($v_pk_event, $v_pk_protocol, 'A','$v_event[$i]',$v_event_seq[$i],0,
				$v_account,0,null,'$event_des',0,0,0,'A','$v_durunit',null,0,'D','D',$v_category,$v_eventtype)";
/*	$v_sql = "insert into event_def (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,COST,DURATION,USER_ID,FUZZY_PERIOD,STATUS,
				DISPLACEMENT,ORG_ID,EVENT_FLAG,CALENDAR_SHAREDWITH,DURATION_UNIT,FK_VISIT,EVENT_FUZZYAFTER,
				EVENT_DURATIONAFTER,EVENT_DURATIONBEFORE,FK_CATLIB,EVENT_LIBRARY_TYPE) values ($v_pk_event, $v_pk_protocol, 'A','$v_event[$i]',$v_event_seq[$i],0,
				$v_account,0,null,0,0,0,'A','$v_durunit',null,0,'D','D',$v_category,$v_eventtype)";*/
	
	executeOCIUpdateQuery($v_sql,$ds_conn);
}


for($i=0; $i<count($v_visiteventname); $i++) {
	$results = executeOCIQuery("SELECT event_definition_seq.NEXTVAL as pk_event FROM dual",$ds_conn);
	$v_pk_event = $results["PK_EVENT"][0];	
	
	/* ---------- Calendar from file description character restriction -------- */
	
/*	$v_sql = "insert into event_def (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,COST,DURATION,USER_ID,FUZZY_PERIOD,STATUS,
				DESCRIPTION,DISPLACEMENT,ORG_ID,EVENT_FLAG,CALENDAR_SHAREDWITH,DURATION_UNIT,FK_VISIT,EVENT_FUZZYAFTER,
				EVENT_DURATIONAFTER,EVENT_DURATIONBEFORE,FK_CATLIB,EVENT_LIBRARY_TYPE) values ($v_pk_event, $v_pk_protocol, 'A','$v_visiteventname[$i]',$v_visiteventno[$i],0,
				$v_account,0,null,'$v_visiteventname[$i]',$v_visiteventdisp[$i],0,0,'A','$v_durunit',$v_visiteventpk[$i],0,'D','D',$v_category,$v_eventtype)";*/
	$v_sql = "insert into event_def (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,COST,DURATION,USER_ID,FUZZY_PERIOD,STATUS,
				DISPLACEMENT,ORG_ID,EVENT_FLAG,CALENDAR_SHAREDWITH,DURATION_UNIT,FK_VISIT,EVENT_FUZZYAFTER,
				EVENT_DURATIONAFTER,EVENT_DURATIONBEFORE,FK_CATLIB,EVENT_LIBRARY_TYPE) values ($v_pk_event, $v_pk_protocol, 'A','$v_visiteventname[$i]',$v_visiteventno[$i],0,
				$v_account,0,null,$v_visiteventdisp[$i],0,0,'A','$v_durunit',$v_visiteventpk[$i],0,'D','D',$v_category,$v_eventtype)";
	
	executeOCIUpdateQuery($v_sql,$ds_conn);
	
	if ($v_migrate == "YES") {
		$v_query = "SELECT EVENT_ID,PAT_DAYSBEFORE, PAT_DAYSAFTER, USR_DAYSBEFORE, USR_DAYSAFTER, PAT_MSGBEFORE, PAT_MSGAFTER, USR_MSGBEFORE, USR_MSGAFTER,description, fuzzy_period, event_fuzzyafter, event_durationafter, event_cptcode, event_durationbefore,notes,duration,duration_unit FROM event_def WHERE EVENT_TYPE = 'E' and user_id = ".$v_account." and trim(upper(name)) = trim(upper('$v_visiteventname[$i]'))";
		$results = executeOCIQuery($v_query,$ds_conn);
		if ($results_nrows > 0) {
			$v_libpk = $results["EVENT_ID"][0];
			$v_sql = "update event_def set PAT_DAYSBEFORE = ".(strlen($results["PAT_DAYSBEFORE"][0]) ==0?"null":$results["PAT_DAYSBEFORE"][0]).",
			PAT_DAYSAFTER = ".(strlen($results["PAT_DAYSAFTER"][0]) ==0?"null":$results["PAT_DAYSAFTER"][0]).",
			USR_DAYSBEFORE = ".(strlen($results["USR_DAYSBEFORE"][0]) ==0?"null":$results["USR_DAYSBEFORE"][0]).", 
			USR_DAYSAFTER = ".(strlen($results["USR_DAYSAFTER"][0]) ==0?"null":$results["USR_DAYSAFTER"][0]).", 
			PAT_MSGBEFORE = '".$results["PAT_MSGBEFORE"][0]."', 
			PAT_MSGAFTER = '".$results["PAT_MSGAFTER"][0]."', 
			USR_MSGBEFORE = '".$results["USR_MSGBEFORE"][0]."', 
			USR_MSGAFTER = '".$results["USR_MSGAFTER"][0]."',
			DESCRIPTION = '".$results["DESCRIPTION"][0]."', 
			FUZZY_PERIOD = '".$results["FUZZY_PERIOD"][0]."', 
			EVENT_FUZZYAFTER = '".$results["EVENT_FUZZYAFTER"][0]."', 
			EVENT_DURATIONAFTER = '".$results["EVENT_DURATIONAFTER"][0]."', 
			EVENT_CPTCODE = '".$results["EVENT_CPTCODE"][0]."', 
			EVENT_DURATIONBEFORE = '".$results["EVENT_DURATIONBEFORE"][0]."', 
			DURATION = '".$results["DURATION"][0]."', 
			DURATION_UNIT = '".$results["DURATION_UNIT"][0]."', 
			NOTES = '".$results["NOTES"][0]."'
			where event_id = $v_pk_event";
			executeOCIUpdateQuery($v_sql,$ds_conn);

			$v_sql = "insert into sch_eventcost (PK_EVENTCOST, EVENTCOST_VALUE, FK_EVENT, FK_COST_DESC,FK_CURRENCY)  
			select esch.sch_eventcost_seq.nextval,EVENTCOST_VALUE, $v_pk_event, FK_COST_DESC,FK_CURRENCY from sch_eventcost where FK_EVENT = $v_libpk";
			executeOCIUpdateQuery($v_sql,$ds_conn);

			$v_sql = "insert into sch_eventusr (PK_EVENTUSR, EVENTUSR, EVENTUSR_TYPE, FK_EVENT, EVENTUSR_DURATION, EVENTUSR_NOTES)  
			select esch.sch_eventusr_seq.nextval,EVENTUSR, EVENTUSR_TYPE, $v_pk_event, EVENTUSR_DURATION, EVENTUSR_NOTES from sch_eventusr where FK_EVENT = $v_libpk";
			executeOCIUpdateQuery($v_sql,$ds_conn);

			$v_query = "SELECT PK_EVENTDOC, PK_DOCS, FK_EVENT from sch_eventdoc where fk_event = $v_libpk";
			$results = executeOCIQuery($v_query,$ds_conn);
			$rows = $results_nrows;
			for ($rec = 0; $rec < $rows; $rec++){
				$v_query = "SELECT esch.sch_docs_seq.nextval as pk from dual";
				$results1 = executeOCIQuery($v_query,$ds_conn);
				$v_pk_docs = $results1["PK"][0];
				$v_sql = "insert into sch_docs (PK_DOCS,DOC_NAME,DOC_DESC,DOC_TYPE,DOC,DOC_SIZE)  
				select $v_pk_docs,DOC_NAME,DOC_DESC,DOC_TYPE,DOC,DOC_SIZE from sch_docs where pk_docs = ".$results["PK_DOCS"][$rec];
				executeOCIUpdateQuery($v_sql,$ds_conn);
				$v_sql = "insert into sch_eventdoc (PK_EVENTDOC, PK_DOCS, FK_EVENT)  
				values (esch.sch_eventdoc_seq.nextval,$v_pk_docs,$v_pk_event)";
				executeOCIUpdateQuery($v_sql,$ds_conn);
			}

		}
	}
}

//$v_query = "SELECT EVENT_ID,FK_VISIT from event_def where event_type = 'A' and chain_id = $v_pk_protocol and displacement <> 0 order by displacement,name";
$v_query = "SELECT EVENT_ID,FK_VISIT, COST  from event_def where event_type = 'A' and chain_id =".$v_pk_protocol." and displacement <> 0 order by displacement,name";
$results = executeOCIQuery($v_query,$ds_conn);
$v_seq = 1;
$v_visit = "";

// suspect area 
for ($rec = 0; $rec < $results_nrows; $rec++){        
    
    // for the bug 22805
/*    if ($v_visit  != $results["FK_VISIT"][$rec]) {
            $v_visit = $results["FK_VISIT"][$rec];
            $v_seq = 1;
    }
    $v_query = "update event_def set event_sequence =".$v_seq." where event_id = ".$results["EVENT_ID"][$rec];

    executeOCIUpdateQuery($v_query,$ds_conn);
    $v_seq++;*/
    
    $v_query = "update event_def set event_sequence =".$results["COST"][$rec]." where event_id = ".$results["EVENT_ID"][$rec];
    executeOCIUpdateQuery($v_query,$ds_conn);
        
}

echo "Calendar <font color=green><b>$v_calname</b></font> created.";


$reC = executeOCIQuery("SELECT * from EVENT_DEF where NAME='".$v_calname."' and EVENT_TYPE='P' and USER_ID='".$v_account."'",$ds_conn);
$v_pk_calid = $reC["EVENT_ID"][0];

$results1 = executeOCIQuery("SELECT seq_er_objectshare.NEXTVAL as pk_objectshare FROM dual",$ds_conn);
$v_pk_objectshare = $results1["PK_OBJECTSHARE"][0];
$v_sql = "insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type) values ($v_pk_objectshare,3,$v_pk_calid,$v_account,'A','N')";
$results = executeOCIUpdateQuery($v_sql, $ds_conn);

// each calendar name entry in EVENT_DEF should write relevant records in ER_OBJECTSHARE table with no of users belongs to the account which users select while import calendar in etools

$query = "SELECT * from ER_USER where FK_ACCOUNT=$v_account";
$rs = executeOCIQuery($query, $ds_conn);
$totalrow = $results_nrows;

for($rr=0; $rr < $totalrow; $rr++){	
	$objshare = $rs["PK_USER"][$rr];
	$results = executeOCIQuery("SELECT seq_er_objectshare.NEXTVAL as pk_objshr FROM dual", $ds_conn);
	$v_pk_objshr = $results["PK_OBJSHR"][0];
	$v_sql = "insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type,fk_objectshare) values ($v_pk_objshr,3,$v_pk_calid,$objshare,'U','N',(select PK_OBJECTSHARE from ER_OBJECTSHARE where FK_OBJECT=$v_pk_calid and OBJECTSHARE_TYPE='A'))";	
	$results = executeOCIUpdateQuery($v_sql,$ds_conn);	
}

// Insert records in er_objectshare (old)
/*$results = executeOCIQuery("SELECT seq_er_objectshare.NEXTVAL as pk_objectshare FROM dual",$ds_conn);
$v_pk_objectshare = $results["PK_OBJECTSHARE"][0];
$v_sql = "insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type) values ($v_pk_objectshare,1,$v_pk_formlib,$v_account,'A','N')";
$results = executeOCIUpdateQuery($v_sql,$ds_conn);

$v_sql = "insert into er_objectshare (pk_objectshare,object_number,fk_object,fk_objectshare_id,objectshare_type,record_type,fk_objectshare) select seq_er_objectshare.NEXTVAL,1,$v_pk_formlib,pk_user,'U','N',$v_pk_objectshare from er_user where fk_account = $v_account and usr_type <> 'X'";
$results = executeOCIUpdateQuery($v_sql,$ds_conn); */


//EVENT_ID,CHAIN_ID,EVENT_TYPE,COST,DURATION,USER_ID,FUZZY_PERIOD,MSG_TO,STATUS,DESCRIPTION,DISPLACEMENT,ORG_ID,EVENT_FLAG,CALENDAR_SHAREDWITH,DURATION_UNIT,FK_VISIT,EVENT_FUZZYAFTER,EVENT_DURATIONAFTER,EVENT_DURATIONBEFORE,FK_CATLIB


//PK_PROTOCOL_VISIT,FK_PROTOCOL,VISIT_NO,VISIT_NAME,DISPLACEMENT,NUM_MONTHS,NUM_WEEKS,NUM_DAYS,INSERT_AFTER,INSERT_AFTER_INTERVAL

?>	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
