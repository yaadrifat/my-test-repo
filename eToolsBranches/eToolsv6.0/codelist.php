<?php
session_start();
header("Cache-control: private");
if(@$_SESSION["user"]){
?>
<html>
<head>
    <title>Velos eTools -> Code List</title>
      <script language="javascript" type="text/javascript">
      function getHTTPObject(){
      if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
      else if (window.XMLHttpRequest) return new XMLHttpRequest();
      else {
      alert("Your browser does not support AJAX.");
      return null;
      }
      }

      function refresh(pk_codelst_maint,order,auto){
      httpObject = getHTTPObject();
      if (httpObject != null) {	  
      httpObject.open("GET", "codelist_ajax.php?pk_codelst_maint="+pk_codelst_maint+"&sequence="+order+"&auto="+auto, false);
	  httpObject.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
	  httpObject.setRequestHeader("Cache-Control", "no-cache");
      httpObject.send(null);
	  if(httpObject.status != 200){document.write("404 not found");}
		var finalString = httpObject.responseText;
		if (finalString != "") {
			document.getElementById('codelistdata').innerHTML = finalString;
		}

      }
      }

      var httpObject = null;
      </script>

</head>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");
include ("./txt-db-api.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
<body onLoad="refresh(document.codelist.pk_codelst_maint.value,'CODELST_DESC')">
<div id="fedora-content">

<div class="navigate">Code List</div>
<FORM name="codelist" action="codelist.php" method="post">
<table width="100%" border="0">
<tr>
<td>Select Module / Field: 

<?php
//$db = new Database("etools");
//$rs = $db->executeQuery("select CATEGORY,DESCRIPTION,PK_CODELST_MAINT from ET_CODELST_MAINT where DISPLAY = 1 order by CATEGORY,DESCRIPTION");
//$rs = $db->executeQuery($codlst_1);
//$rs = mysql_query("select CATEGORY,DESCRIPTION,PK_CODELST_MAINT from ET_CODELST_MAINT where DISPLAY = 1 order by CATEGORY,DESCRIPTION");
$rs = mysql_query("select category,description,pk_codelst_maint from et_codelst_maint where display = 1 and hide_flag <> 1 order by category, description");


$dropdown = '';

if (isset($_POST['pk_codelst_maint']) ) {
    $v_pk_codelst_maint =  $_POST['pk_codelst_maint'];
    $_SESSION['pk_codelst_maint'] = $v_pk_codelst_maint;
} else {
    if (isset($_SESSION['pk_codelst_maint']) ) {
        $v_pk_codelst_maint =  $_SESSION['pk_codelst_maint'];
    } else {
        $v_pk_codelst_maint =  "";
    }
}

$v_counter = 0;
//while ($rs->next()) {
while ($data = mysql_fetch_array($rs)) {
	#$data = $rs->getCurrentValuesAsHash();
	if ($v_pk_codelst_maint == "" && $v_counter == 0) {
		$v_pk_codelst_maint = $data["pk_codelst_maint"];
		$_SESSION['PK_CODELST_MAINT'] = $v_pk_codelst_maint;
	} 
	if ($v_pk_codelst_maint == $data["pk_codelst_maint"]) {
      $dropdown .= "<option value=".$data["pk_codelst_maint"]." SELECTED>".$data["category"].' - '.$data["description"]."</option>";
	} else {
      $dropdown .= "<option value=".$data["pk_codelst_maint"].">".$data["category"].' - '.$data["description"]."</option>";
    }
    $v_counter++;
}
?>

<select name="pk_codelst_maint" onChange="refresh(document.codelist.pk_codelst_maint.value,'CODELST_DESC');"><?PHP echo $dropdown; ?></select>
</td></tr></table>


</form>
<div id="codelistdata">

</div>
</div>
</body>
</html>
<?php
OCICommit($ds_conn);
OCILogoff($ds_conn);

}
else header("location: index.php?fail=1");

?>