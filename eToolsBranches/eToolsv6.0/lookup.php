<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.
if(@$_SESSION["user"]){
?>
<html>
<head>
    <title>Velos eTools -> Lookup</title>
<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
</head>


<body>

<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	


<?php


$query_sql = "select distinct pk_lkplib,lkptype_name,lkptype_version,lkptype_desc,lkptype_type,lkpcol_table from er_lkplib,er_lkpcol where pk_lkplib = fk_lkplib and nvl(lkptype_type,'Y') not in ('dyn_p','dyn_s','dyn_a') order by lower(lkptype_name)";

$results = executeOCIQuery($query_sql,$ds_conn);
echo '<div class="navigate">Lookup</div>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
echo '<a href="import_lookup.php">Import Static Lookup from File</a>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="import_lookup_table.php">Create Dynamic Lookup from Table/View</a>';
echo '<table  width="100%" border="1">';
echo '<TR><TH width="20%">Name</TH>';
echo '<TH width="20%">Description</TH>';
echo '<TH width="10%">Table/View Name</TH>';
echo '<TH width="10%">Type</TH>';
echo '<TH width="10%">Version</TH>';
echo '<TH width="10%">&nbsp;</TH>';
echo '<TH width="10%">&nbsp;</TH></TR>';
for ($rec = 0; $rec < $results_nrows; $rec++){
	echo '<TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';
	echo '<TD>'.$results["LKPTYPE_NAME"][$rec].'</TD>';
	echo '<TD>'.$results["LKPTYPE_DESC"][$rec].'</TD>';
	echo '<TD>'.$results["LKPCOL_TABLE"][$rec].'</TD>';
	echo '<TD>'.$results["LKPTYPE_TYPE"][$rec].'</TD>';
	echo '<TD>'.$results["LKPTYPE_VERSION"][$rec].'</TD>';
	if (strtoupper($results["LKPCOL_TABLE"][$rec]) == 'ER_LKPDATA') {
		echo '<TD  align="center"><a href="update_lookup.php?pk_lkplib='.$results["PK_LKPLIB"][$rec].'">Update Lookup</a></TD>';
	} else {
		echo '<TD>&nbsp;</TD>';
	}
	echo '<TD align="center"><a href="lookup_view.php?pk_lkplib='.$results["PK_LKPLIB"][$rec].'&table='.$results["LKPCOL_TABLE"][$rec].'">View</a></TD>';
	echo '</TR>';
}
echo "</table>"
?>

</div>

</body>
</html>
<?php
OCILogoff($ds_conn);
}
else header("location: ./index.php?fail=1");
?>
