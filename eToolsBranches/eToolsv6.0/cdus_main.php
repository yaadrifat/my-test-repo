<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> CDUS Submission</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">CDUS Submission</div>
<Table border="1" ><TR>
<TH>Export Type</TH>
<TH>Description</TH>
<TH>Account</TH>
<TH></TH>
</TR>
<?php

// $query_sql = "SELECT pk_exp_definition,pk_exp_main,exp_desc,definition_desc FROM EXP_DEFINITION, EXP_MAIN WHERE pk_exp_main = fk_exp_main and fk_account = ".$_SESSION["ACCOUNT"];

$query_sql = "SELECT pk_exp_definition,pk_exp_main,exp_desc,definition_desc,fk_account,(select ac_name from er_account where pk_account = fk_account) as ac_name FROM EXP_DEFINITION, EXP_MAIN WHERE pk_exp_main = fk_exp_main";

$results = executeOCIQuery($query_sql,$ds_conn);

for ($rec = 0; $rec < $results_nrows; $rec++){
?>
<TR>
	<TD><?php echo $results["EXP_DESC"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["DEFINITION_DESC"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["AC_NAME"][$rec] . "&nbsp;"; ?></TD>
<?php
	echo "<td><a href=cdus.php?fk_exp_definition=".$results["PK_EXP_DEFINITION"][$rec]."&account=".$results["FK_ACCOUNT"][$rec].">Select</a></td>";
	echo "</TR>";
}
?>
</TABLE>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
