<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Form Transfer</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Form Transfer</div>
	
<?PHP

$v_account = 1170;
//$v_account = 52;
//$v_account = 51;

if (isset($_POST["formname"])){
	$searchvalue = $_POST["formname"];
} else {
	$searchvalue = "";
}
?>
<form name="formtransfer" method="post" action="form_transfer_multi.php">
<table border = "1">
<tr>
	<td>Search Form Name: </td><td><input name="formname" type="text" size="50" maxlength="100" value="<?PHP echo $searchvalue; ?>"/></td>
	<td><input name=submit type=submit value=Submit></input></td>
</tr>

</table>

<?PHP
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
//$v_account = $_POST["fk_account"];
?>
<input name=select type=submit value="Select Forms"></input>
<table width="100%">
	<tr><td width="50%">
		<Table border="1"><TR>
		<TH>&nbsp;</TH>
		<TH>FORM NAME</TH>
		<TH>FORM DESCRIPTION</TH>
		<TH>LINKED TO</TH>
		<TH>STUDY NUMBER</TH>
		</TR>
		<?php

		$query_sql = "SELECT pk_formlib,form_name, form_desc,lf_displaytype,DECODE(fk_study,NULL,'',(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study)) AS study_number FROM ER_FORMLIB a, ER_LINKEDFORMS b WHERE pk_formlib = fk_formlib AND  b.record_type <> 'D' AND form_status = (SELECT pk_codelst FROM ER_CODELST WHERE codelst_type = 'frmstat' AND codelst_subtyp = 'A') AND a.fk_account = ".$v_account." and  LOWER(trim(form_name)) LIKE lower('%".$searchvalue."%')";
		$results = executeOCIQuery($query_sql,$ds_conn);
		if (isset($_POST["form_pk"])) {
			$v_formpk = $_POST["form_pk"];
		} else { 
			$v_formpk[0] = 0;
		}
			

		for ($rec = 0; $rec < $results_nrows; $rec++){
			if ($rec % 2) {
				echo '<TR class="even">';
			} else {
				echo '<TR>';
			}
			$v_found=false;
			for ($i=0;$i<count($v_formpk);$i++){
				if (isset($v_formpk[$i])){
					if ($v_formpk[$i] == $results["PK_FORMLIB"][$rec]){
						echo '<TD><INPUT checked TYPE="CHECKBOX" name="form_pk['.$rec.']" value="'.$results["PK_FORMLIB"][$rec].'"></TD>';
						$v_found=true;
						break;
					}
				}
			}
			
			if (!$v_found){
				echo '<TD><INPUT TYPE="CHECKBOX" name="form_pk['.$rec.']" value="'.$results["PK_FORMLIB"][$rec].'"></TD>';
			}

			echo '<INPUT TYPE="HIDDEN" name="form_name['.$rec.']" value="'.$results["FORM_NAME"][$rec].'">';
		?>
			<TD><?php echo $results["FORM_NAME"][$rec] . "&nbsp;"; ?></TD>
			<TD><?php echo $results["FORM_DESC"][$rec] . "&nbsp;"; ?></TD>
			<TD><?php echo $results["LF_DISPLAYTYPE"][$rec] . "&nbsp;"; ?></TD>
			<TD><?php echo $results["STUDY_NUMBER"][$rec] . "&nbsp;"; ?></TD>
		<?php
		//	echo "<td><a href=form_transfer_step2.php?pk_formlib=".$results["PK_FORMLIB"][$rec]."&linked_to=".trim($results["LF_DISPLAYTYPE"][$rec])."&formname=".urlencode($results["FORM_NAME"][$rec])."&account=".$v_account.">Select</a></td>";
			echo "</TR>";
		}
		?>
		</TABLE>
	</td><td width="50%" valign="top">
<?php

		if (isset($_POST["select"]) || ($v_formpk[0] != 0)){
			$v_formname = $_POST["form_name"];
			$v_formpk = $_POST["form_pk"];
			if (isset($_POST["form_pk_selected"])) $v_formpk_selected = $_POST["form_pk_selected"];
			if (isset($_POST["form_pk_selected"])) $v_formname_selected = $_POST["form_name_selected"];
			for ($i=0;$i<count($v_formname);$i++){
				if (isset($v_formpk[$i])){
					if (!isset($v_formname_selected)){
						$v_counter = 0;
						$v_formpk_selected[$v_counter] = $v_formpk[$i];
						$v_formname_selected[$v_counter] = $v_formname[$i];
//						echo "1 - ".$v_formname[$i];
					} else {
						if (!isset($v_counter)) $v_counter = count($v_formname_selected);
						$v_formpk_selected[$v_counter] = $v_formpk[$i];
						$v_formname_selected[$v_counter] = $v_formname[$i];
//						echo "2 - ".$v_formname[$i];
//						echo $v_counter."<BR>";
					}
				}
			}

			for ($i=0;$i<count($v_formname_selected);$i++){
				echo $v_formpk_selected[$i]." - ".$v_formname_selected[$i];
				echo "<BR>";
				echo '<INPUT TYPE="HIDDEN" name="form_pk_selected['.$i.']" value="'.$v_formpk_selected[$i].'">';
				echo '<INPUT TYPE="HIDDEN" name="form_name_selected['.$i.']" value="'.$v_formname_selected[$i].'">';
			}
		}
?>
	</td></tr>
</table>
</form>
<?php } ?>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
