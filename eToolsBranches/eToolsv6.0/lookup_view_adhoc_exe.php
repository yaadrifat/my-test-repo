<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Publish View to AHQ</title>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");
include("./txt-db-api.php");
require_once('audit_queries.php');

$dbe = new Database("etools");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>

</head>
<body>
<div id="fedora-content">	

<?php


$v_viewname = $_POST["viewname"];
$v_lookupname = $_POST['lookupname'];
$v_colname = $_POST['colname'];
$v_coltype = $_POST['coltype'];
$v_colkeyword = $_POST['colkeyword'];
$v_dispname = $_POST['dispname'];
$v_viewtype = $_POST['viewtype'];

$results = executeOCIQuery("SELECT MAX(pk_lkplib)+1 as pk_lkplib FROM ER_LKPLIB",$ds_conn);
$pk_lkplib = $results["PK_LKPLIB"][0];
if ($pk_lkplib < 100000) {$pk_lkplib = 100000;}

$results = executeOCIQuery("SELECT MAX(pk_lkpview)+1 as pk_lkpview FROM ER_LKPVIEW",$ds_conn);
$pk_lkpview = $results["PK_LKPVIEW"][0];
if ($pk_lkpview < 100000) {$pk_lkpview = 100000;}

$results = executeOCIQuery("SELECT MAX(pk_lkpcol)+1 as pk_lkpcol FROM ER_LKPCOL",$ds_conn);
$pk_lkpcol = $results["PK_LKPCOL"][0];
if ($pk_lkpcol < 100000) {$pk_lkpcol = 100000;}

$results = executeOCIQuery("SELECT MAX(pk_lkpviewcol)+1 as pk_lkpviewcol FROM ER_LKPVIEWCOL",$ds_conn);
$pk_lkpviewcol = $results["PK_LKPVIEWCOL"][0];
if ($pk_lkpviewcol < 100000) {$pk_lkpviewcol = 100000;}

////////////////////////////////////////////////////////////////////////////////
$sql = "insert into er_lkplib (pk_lkplib,lkptype_name,lkptype_version,lkptype_desc,lkptype_type) values
 ($pk_lkplib,'dynReports','','$v_lookupname','$v_viewtype')";
$results = executeOCIUpdateQuery($sql,$ds_conn);

	//---Audit
	//$grpName = $dbe->executeQuery("SELECT GROUP_NAME from ET_GROUPS WHERE PK_GROUPS =".$_SESSION["FK_GROUPS"]);
	//$grpName->next();
	//$grpNameVal = $grpName->getCurrentValuesAsHash();
        
	$grpName = mysql_query("SELECT group_name from et_groups WHERE pk_groups =".$_SESSION["FK_GROUPS"]);
	$grpNameVal = mysql_fetch_array($grpName);        

	$rowInfo = array(5,$_SESSION["user"],$grpNameVal['group_name'], "SYSDATE", "", "", "I");
	auditQuery(5,$rowInfo);
	
	$colArray = array('pk_lkplib','lkptype_name','lkptype_version','lkptype_desc','lkptype_type');
	$colvalarray = array($pk_lkplib, 'dynReports', '', $v_lookupname, $v_viewtype);
	$tblname = 'er_lkplib';
	colQueries($colArray, $colvalarray, $tblname);
	//----

$sql = "insert into er_lkpview (pk_lkpview,lkpview_name,fk_lkplib,lkpview_filter) values
 ($pk_lkpview,'$v_lookupname',$pk_lkplib,'fk_account=[:ACCID]')";
$results = executeOCIUpdateQuery($sql,$ds_conn);

	//---Audit
	$colArray = array('pk_lkpview','lkpview_name','fk_lkplib','lkpview_filter');
	$colvalarray = array($pk_lkpview, $v_lookupname, $pk_lkplib, 'fk_account=[:ACCID]');
	$tblname = 'er_lkpview';
	colQueries($colArray, $colvalarray, $tblname);
	//---
	
$results = executeOCIQuery("SELECT MAX(pk_lkpcol)+1 as pk_lkpcol FROM ER_LKPCOL",$ds_conn);
$pk_lkpcol = $results["PK_LKPCOL"][0];
if ($pk_lkpcol < 100000) {$pk_lkpcol = 100000;}
//echo $pk_lkpcol."<BR>";

$results = executeOCIQuery("SELECT MAX(pk_lkpviewcol)+1 as pk_lkpviewcol FROM ER_LKPVIEWCOL",$ds_conn);
$pk_lkpviewcol = $results["PK_LKPVIEWCOL"][0];
if ($pk_lkpviewcol < 100000) {$pk_lkpviewcol = 100000;}
//echo $pk_lkpviewcol."<BR>";
$v_seq = 1;
		
		$rowInfo = array(5,$_SESSION["user"],$grpNameVal['group_name'], "SYSDATE", "", "", "I");
		auditQuery(5,$rowInfo);
		
for ($i = 0; $i < count($v_colname); $i++){
//    echo $i." ".$colName[$i]." ".$dispName[$i]." ".$colSeq[$i]." ".$colShow[$i]." ".$colSearch[$i]." ".$colWidth[$i]."<BR>";
      $tabColName = 'custom'.str_pad(($i + 1),3,'0',STR_PAD_LEFT);
      
      $sql = "insert into er_lkpcol (pk_lkpcol,fk_lkplib,lkpcol_name,lkpcol_dispval,lkpcol_datatype,lkpcol_len,lkpcol_table,lkpcol_keyword) values
      ($pk_lkpcol,$pk_lkplib,'$v_colname[$i]','$v_dispname[$i]','$v_coltype[$i]',null,'$v_viewname','$v_colkeyword[$i]')";
      $results = executeOCIUpdateQuery($sql,$ds_conn);
		
		//---Audit
		$colArray = array('pk_lkpcol','fk_lkplib','lkpcol_name','lkpcol_dispval','lkpcol_datatype','lkpcol_len','lkpcol_table','lkpcol_keyword');
		$colvalarray = array($pk_lkpcol,$pk_lkplib,$v_colname[$i],$v_dispname[$i],$v_coltype[$i],'null',$v_viewname,$v_colkeyword[$i]);
		$tblname = 'er_lkpcol';
		colQueries($colArray, $colvalarray, $tblname);	  
		//---
		
	  if (($v_colname[$i] == 'FK_PER') || ($v_colname[$i] == 'FK_STUDY') || ($v_colname[$i] == 'FK_ACCOUNT')) {
	      $sql = "insert into er_lkpviewcol (pk_lkpviewcol, fk_lkpcol,  lkpview_seq, fk_lkpview, lkpview_displen, lkpview_is_display) values
	      ($pk_lkpviewcol, $pk_lkpcol, $v_seq,$pk_lkpview,'10%','N') ";
		  $lastfldva = 'N';
	  } else {
	      $sql = "insert into er_lkpviewcol (pk_lkpviewcol, fk_lkpcol,  lkpview_seq, fk_lkpview, lkpview_displen, lkpview_is_display) values
	      ($pk_lkpviewcol, $pk_lkpcol, $v_seq,$pk_lkpview,'10%','Y') ";
		  $lastfldva = 'Y';
	  }
	  
		//---Audit
		$colArray = array('pk_lkpviewcol','fk_lkpcol','lkpview_seq','fk_lkpview','lkpview_displen','lkpview_is_display');
		$colvalarray = array($pk_lkpviewcol, $pk_lkpcol, $v_seq, $pk_lkpview,'10%', $lastfldva);
		$tblname = 'pk_lkpviewcol';
		colQueries($colArray, $colvalarray, $tblname);		  
		//---
	  
      $results = executeOCIUpdateQuery($sql,$ds_conn);

      $pk_lkpcol++;
      $pk_lkpviewcol++;
      $v_seq++;
}
echo "View published to Adhoc query.";
///////////////////////////////////////////////////////////////////////
OCICommit($ds_conn);
OCILogoff($ds_conn);
$url = "./lookup_view_adhoc.php";
echo "<meta http-equiv=\"refresh\" content=\"2; url=./".$url."\">";
?>
</div>


</body>
</html>


<?php
}
else header("location: index.php?fail=1");
?>
		
