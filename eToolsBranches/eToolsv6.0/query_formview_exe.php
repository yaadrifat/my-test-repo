<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Create Form View</title>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");
include("./txt-db-api.php");
require_once('audit_queries.php');

$dbe = new Database("etools");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>

<script>
function VerfiyValidation() {
var invalid = " ";
	if (document.getElementById("viewname").value == "") {
		alert('View Name cannot be balnk.');
		document.getElementById("viewname").focus();
		return false;
	}
	if (document.getElementById("viewname").value != "") {
		var test = document.getElementById("viewname").value;
		if(!isNaN(test.charAt(0))) {
			alert("View name should not start with a number");
			document.getElementById("viewname").focus();
			 return false;
		}
		
		if (document.getElementById("viewname").value.indexOf(invalid) > -1) {
			alert("View name should not contain space");
			document.getElementById("viewname").focus();
			return false;
		}
		
	}
	return true;
	
}
</script>

</head>
<body>
<div id="fedora-content">	
<div class="navigate">Create Form View - View Name</div>
<BR>
<?php

// phpinfo();


if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
$v_formname = $_GET["formname"];
$v_pk_formlib = $_GET["pk_formlib"];

?>
<FORM name="formview" action="query_formview_exe.php" method=post onSubmit="return VerfiyValidation();">

<input type="hidden" name="pk_formlib" value="<?php echo $v_pk_formlib; ?>"></input>

<TABLE width="100%" border="0">
<TR><TD width="10%">Form Name</TD><TD width="90%"><?PHP echo $v_formname ?></TD></TR>
<TR><TD>View Name</TD><TD><INPUT TYPE="TEXT" class="required" NAME="viewname" id="viewname" size=32 maxlength=32></INPUT></TD></TR>
</TABLE>

<BR><input type="image" name="submit" value="Submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /></form>

<div class="important" style="margin-left: 0.5in; margin-right: 0.5in; width:600px;">
		<table border="1" style="margin:0px;" summary="Warning: Disabling the firewall may make your system vulnerable">
			<tr>
				<th align="left">Note</th>
			</tr>
			<tr>
				<td align="left" valign="top">
					<p>The view name should not contain space <br> The view name should not start with a number <br> The maximum length of the view name should be less than 32 character </p>
				</td>
			</tr>
		</table>
	</div>

<?php 
} else {

$v_pk_formlib = $_POST["pk_formlib"];
$v_viewname = str_replace("-","_",$_POST["viewname"]);

$query = "select velink.f_generate_formview($v_pk_formlib,1,'$v_viewname') as view_sql from dual";
$results = executeOCIQuery($query,$ds_conn);
$query = $results["VIEW_SQL"][0];
$query_flag = executeOCIQueryDDL($query,$ds_conn);

	//-- Audit
	$grpName = mysql_query("SELECT group_name from et_groups WHERE pk_groups =".$_SESSION["FK_GROUPS"]);
	$grpNameVal = mysql_fetch_array($grpName);

	$rowInfo = array(5,$_SESSION["user"],$grpNameVal['group_name'], "SYSDATE", "", "", "E", "Form view");
	auditQuery(5,$rowInfo);
	//--

OCICommit($ds_conn);
OCILogoff($ds_conn);
echo "View Created !!!";
$url = "./query_formview.php";
echo "<meta http-equiv=\"refresh\" content=\"1; url=./".$url."\">";


?>
<?PHP
}

?>
</div>
<?php if($results == '') { ?>
<script type="text/javascript">
	document.getElementById("viewname").focus();
</script>
<?php } ?>
</body>
</html>


<?php
}
else header("location: index.php?fail=1");
?>
		
