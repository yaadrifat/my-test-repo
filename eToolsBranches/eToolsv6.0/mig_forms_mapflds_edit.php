<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Form Data</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 



?>
</head>


<body>

<div id="fedora-content">	

<div class="navigate">Migrate Data - <a href="./mig_forms.php?pk_vlnk_adaptor=<?PHP echo $_GET['pk_vlnk_adaptor'];?>">Forms</a> - Edit Mapping</div>

<?php

if ($_SERVER['REQUEST_METHOD'] != "POST"){

$linkedto = $_GET['linkedto'];

$query_sql = "SELECT distinct column_name FROM DBA_TAB_COLUMNS WHERE table_name=upper('".$_GET['tablename']."') ORDER BY 1";
$results_tab = executeOCIQuery($query_sql,$ds_conn);
$tab_rows = $results_nrows;


/*
$query_sql = "select mp_sequence,mp_mapcolname,mp_dispname,decode(mp_flddatatype,'ED','Date','EN','Number','ET','Text','MD','Dropdown','MC','Checkbox','MR','Radiobutton') as mp_flddatatype,
mp_secname,mp_pkfld,table_colname, pk_imp_formfld,map_displayval, formfld_mandatory 
from er_mapform a,velink.vlnk_imp_formfld b, er_formfld c
where b.fk_field(+) = mp_pkfld and fk_form = ".$_GET['pk_formlib']." and 
fk_vlnk_adapmod = ".$_GET['pk_vlnk_adapmod']." and mp_pkfld = c.fk_field order by mp_sequence";
*/

$query_sql = "select mp_sequence,mp_mapcolname,mp_dispname,decode(mp_flddatatype,'ED','Date','EN','Number','ET','Text','MD','Dropdown','MC','Checkbox','MR','Radiobutton') as mp_flddatatype,
mp_secname,mp_pkfld,table_colname, pk_imp_formfld,map_displayval, fld_mandatory as formfld_mandatory 
from er_mapform a,velink.vlnk_imp_formfld b
where b.fk_field(+) = mp_pkfld and fk_form = ".$_GET['pk_formlib']." and 
fk_vlnk_adapmod = ".$_GET['pk_vlnk_adapmod']." order by mp_sequence";

$results = executeOCIQuery($query_sql,$ds_conn);
$v_trows = $results_nrows;
$dd_map = '<option value="DISP">Display Value</option><option value="DATA">Data Value</option>';

?>
<form name="formtabmap" method="post" action="mig_forms_mapflds_edit.php">
<input type="hidden" name="pk_formlib" value="<?PHP echo $_GET['pk_formlib'] ?>"/>
<input type="hidden" name="pk_vlnk_adapmod" value="<?PHP echo $_GET['pk_vlnk_adapmod'] ?>"/>
<span style="width:233px;"><a href="./mig_forms.php?pk_vlnk_adaptor=<?PHP echo $_REQUEST["pk_vlnk_adaptor"] ?>">Back to Migration Page</a></span>
Form Name: <input style="border:none" size="40" readonly name="formname" value="<?PHP echo $_GET['formname'] ?>"/>
Table Name: <input style="border:none" size="40" readonly name="tabname" value="<?PHP echo $_GET['tablename'] ?>"/>
<!-- <input type="hidden" name="linked_to" value="<?PHP echo $_POST['linked_to'] ?>"/> 

<table>
	<tr><td>Creator: </td><td><input type="text" size="20" name="creator"/></td></tr>
	<tr><td>Account ID: </td><td><input type="text" size="20" name="account" value=<? echo $_SESSION["accountId"]; ?> /></td></tr>
	<tr><td>Patient ID: </td><td><input type="text" size="100" name="patientId"/></td></tr>
	<tr><td>Patient Study ID: </td><td><input type="text" size="100" name="patientStdId"/></td></tr>
	<tr><td>Study ID: </td><td><input type="text" size="100" name="studyId"/></td></tr>
</table>
-->



<Table border="1" width="100%"><TR>
<TH width="1%">&nbsp;</TH>
<TH width="20%">Section Name</TH>
<TH width="34%">Field Name</TH>
<TH width="20%">Column Name</TH>
<TH width="10%">Field Type</TH>
<TH width="10%">Map Response to</TH>
</TR>
	<?php 
	if ($linkedto == "SP" or $linkedto == "PA" or $linkedto == "PR" or $linkedto == "PS") { 
		$dd_table = '<option value=""></option>';
		$query_sql = "select pk_imp_formfld,table_colname from velink.vlnk_imp_formfld where lookup = 'PATIENT' and fk_vlnk_adapmod = ".$_GET['pk_vlnk_adapmod'];
		$presults = executeOCIQuery($query_sql,$ds_conn);
		
		for ($rec_tab = 0; $rec_tab < $tab_rows; $rec_tab++){
			if ($presults["TABLE_COLNAME"][0] == $results_tab["COLUMN_NAME"][$rec_tab]) {
		        $dd_table .= "<option selected value=".$results_tab["COLUMN_NAME"][$rec_tab].">".$results_tab["COLUMN_NAME"][$rec_tab]."</option>";
		    } else {
		    	$dd_table .= "<option value=".$results_tab["COLUMN_NAME"][$rec_tab].">".$results_tab["COLUMN_NAME"][$rec_tab]."</option>";
		    }
		}	
		echo "<input type='hidden' name='pk_imp_pat' value='".$presults["PK_IMP_FORMFLD"][0]."' />";
		?>
		
		<TR>
			<TD></TD>
			<TD></TD>
			<TD><?php echo "<INPUT style='border:none' readonly name=\"patient_id\" value=\"PATIENT_ID\" size=8 /><font color='red'><b><font size='3'>*</font>"; ?></TD>
			<td><?PHP echo "<select class='required' name=\"pat_colname\">".$dd_table."</select>"; ?></td>
			<TD></TD>
			<TD></TD>
		</TR>
		<?php 
	} 
	if ($linkedto == "SP" or $linkedto == "SA" or $linkedto == "S" or $linkedto == "PS" or $linkedto == "PR") {
		$dd_table = '<option value=""></option>';
		$query_sql = "select pk_imp_formfld,table_colname from velink.vlnk_imp_formfld where lookup = 'STUDY' and fk_vlnk_adapmod = ".$_GET['pk_vlnk_adapmod'];
		$presults = executeOCIQuery($query_sql,$ds_conn);
		
		for ($rec_tab = 0; $rec_tab < $tab_rows; $rec_tab++){
			if ($presults["TABLE_COLNAME"][0] == $results_tab["COLUMN_NAME"][$rec_tab]) {
		        $dd_table .= "<option selected value=".$results_tab["COLUMN_NAME"][$rec_tab].">".$results_tab["COLUMN_NAME"][$rec_tab]."</option>";
		    } else {
		    	$dd_table .= "<option value=".$results_tab["COLUMN_NAME"][$rec_tab].">".$results_tab["COLUMN_NAME"][$rec_tab]."</option>";
		    }
		}	
		echo "<input type='hidden' name='pk_imp_std' value='".$presults["PK_IMP_FORMFLD"][0]."' />";
		?>
		<TR>
			<TD></TD>
			<TD></TD>
			<TD><?php echo "<INPUT style='border:none' readonly name=\"study_number\" value=\"STUDY_NUMBER\" size=13 /><font color='red'><b><font size='3'>*</font>"; ?></TD>
			<td><?PHP echo "<select name=\"std_colname\">".$dd_table."</select>"; ?></td>
			<TD></TD>
			<TD></TD>
		</TR>

		<?php
	}

for ($rec = 0; $rec < $v_trows; $rec++){
$fld_datatype = $results["MP_FLDDATATYPE"][$rec];


if (strtoupper($results["TABLE_COLNAME"][$rec]) == "TRUNC(SYSDATE)") {
	$dd_table = '<option value=""></option><option selected value="trunc(SYSDATE)">{ Migration Date }</option>';
} else {
	$dd_table = '<option value=""></option><option value="trunc(SYSDATE)">{ Migration Date }</option>';
}


//$dd_table="";
for ($rec_tab = 0; $rec_tab < $tab_rows; $rec_tab++){
	if ($results["TABLE_COLNAME"][$rec] == $results_tab["COLUMN_NAME"][$rec_tab]) {
        $dd_table .= "<option selected value=".$results_tab["COLUMN_NAME"][$rec_tab].">".$results_tab["COLUMN_NAME"][$rec_tab]."</option>";
    } else {
    	$dd_table .= "<option value=".$results_tab["COLUMN_NAME"][$rec_tab].">".$results_tab["COLUMN_NAME"][$rec_tab]."</option>";
    }
}

$v_mandatory = ($results["FORMFLD_MANDATORY"][$rec] == 1)? "class='required'":"";
?>
<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
	<TD><?php echo $results["MP_SEQUENCE"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["MP_SECNAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["MP_DISPNAME"][$rec] ."&nbsp;". (($results["FORMFLD_MANDATORY"][$rec] == 1)?"<font color='red'><b><font size='3'>*</font></b></font>":""); ?></TD>
	<td><?PHP echo "<select $v_mandatory name=\"tablename[$rec]\">".$dd_table."</select>"; ?></td>
	<TD><?php echo $results["MP_FLDDATATYPE"][$rec] . "&nbsp;"; ?></TD>


	<td><?PHP 
//		if (($fld_datatype == 'Dropdown') || ($fld_datatype == 'Radiobutton') || ($fld_datatype == 'Checkbox')) {
		if (($fld_datatype == 'Dropdown') || ($fld_datatype == 'Radiobutton')) {
			if ($results["MAP_DISPLAYVAL"][$rec] == "DATA") {
				echo "<select name=\"map[$rec]\">".'<option value="DISP">Display Value</option><option selected value="DATA">Data Value</option>'."</select>"; 
			} else {
				echo "<select name=\"map[$rec]\">".'<option selected value="DISP">Display Value</option><option  value="DATA">Data Value</option>'."</select>"; 
			}
		
		
		//} else {
		//echo "<select disabled name=\"map[$rec]\">".$dd_map."</select>"; 
		}
		?>
	</td>
	<?PHP echo "<INPUT type=\"hidden\" name=\"pk_field[$rec]\" value=".$results["MP_PKFLD"][$rec]."></input>" ?>
	<?PHP echo "<INPUT type=\"hidden\" name=\"mp_mapcolname[$rec]\" value=".$results["MP_MAPCOLNAME"][$rec]."></input>" ?>
	<?PHP echo "<INPUT type=\"hidden\" name=\"mp_mapcoltype[$rec]\" value=".$results["MP_FLDDATATYPE"][$rec]."></input>" ?>
	<?PHP echo "<INPUT type=\"hidden\" name=\"pk_imp_formfld[$rec]\" value=".$results["PK_IMP_FORMFLD"][$rec]."></input>" ?>
</TR>
<?php
//	echo "<td><a href=formimport_select.php?pk_formlib=".$results["PK_FORMLIB"][$rec]."&formname=".urlencode($results["FORM_NAME"][$rec]).">Select</a></td>";
//	echo "</TR>";
}
?>
</TABLE><br>

<input type="hidden" name="pk_vlnk_adaptor" value=<?PHP echo $_GET["pk_vlnk_adaptor"]; ?> >
<input type="image" name="submit" value="SUBMIT" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
</form>

<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
<tr>
<th align="left">Note</th>
</tr>
<tr><td align="left" valign="top">
<p>Multiple values should be enclosed in brackets [ ]<br>
For example: In case of a check box fields having the values like aa, bb, cc, dd. Here we need to send the value like [aa], [bb], [cc], [dd] in the CSV file</p>
</td></tr>
</table></div>

<?php 
} else { 
//$v_sql = "delete from velink.vlnk_imp_formfld where fk_vlnk_adapmod = ".$_POST["pk_vlnk_adapmod"];
//$results = executeOCIUpdateQuery($v_sql,$ds_conn);

$tab_cols = $_POST["tablename"];
$pk_field = $_POST["pk_field"];
$mp_mapcolname = $_POST["mp_mapcolname"];
$map = $_POST["map"];
$fld_type = $_POST["mp_mapcoltype"];
$pk_imp_formfld = $_POST["pk_imp_formfld"];


if (isset($_POST["patient_id"])) {	
	$v_sql = "update velink.vlnk_imp_formfld set table_colname = '".$_POST["pat_colname"]."' where pk_imp_formfld = ".$_POST["pk_imp_pat"];
	$results = executeOCIUpdateQuery($v_sql,$ds_conn);
}
if (isset($_POST["study_number"])) {
	$v_sql = "update velink.vlnk_imp_formfld set table_colname = '".$_POST["std_colname"]."' where pk_imp_formfld = ".$_POST["pk_imp_std"];
	$results = executeOCIUpdateQuery($v_sql,$ds_conn);
}
for ($i=0;$i < count($tab_cols);$i++){
//	$v_sql = "insert into velink.vlnk_imp_formfld (pk_imp_formfld, fk_vlnk_adapmod,form_colname,table_colname,map_displayval,fk_field,fld_type) values (velink.seq_vlnk_imp_formfld.nextval,".$_POST["pk_vlnk_adapmod"].",'".$mp_mapcolname[$i]."','".$tab_cols[$i]."','".(!isset($map[$i]) ?  "" : $map[$i])."',".$pk_field[$i].",'".$fld_type[$i]."')";

	$v_sql = "update velink.vlnk_imp_formfld set table_colname = '".$tab_cols[$i]."', map_displayval = '".(!isset($map[$i]) ?  "" : $map[$i])."' where pk_imp_formfld = ".$pk_imp_formfld[$i];
	$results = executeOCIUpdateQuery($v_sql,$ds_conn);
}

echo "Data Saved...";
$url = "mig_forms.php?pk_vlnk_adaptor=".$_POST["pk_vlnk_adaptor"]."&flag=".$flag;
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";
OCICommit($ds_conn);
OCILogoff($ds_conn);

//echo "<script>window.history.go(-1);</script>";
}
?>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
