<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> New Code List</title>	
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");
//include ("./txt-db-api.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="js/validate.js"></SCRIPT>

<SCRIPT language="Javascript1.2">
function validate(form){
	if (!/^[a-zA-Z0-9_]*$/.test(fnTrimSpaces(form.SUBTYPE.value))) { //Added by Ruchira for Bugzilla #20545
		alert("Sub Type does not allow space or special characters (underscore allowed)");
		return false;
	}
	
	if (fnTrimSpaces(form.SUBTYPE.value) == "" || fnTrimSpaces(form.DESC.value) == "" || fnTrimSpaces(form.SEQ.value) == "") {
		alert(" Sub Type / Description / Sequence cannot be blank.");
		return false;
	}
	
	if (!isInteger(form.SEQ.value)){
		alert("Sequence allows only numbers");	
		return false;
	}	
}

 function fnTrimSpaces(strToTrim){  
        strTemp = strToTrim;
        len=strTemp.length;
        i=0;  
        while (i < len && strTemp.charAt(i)==" "){
            i=i+1; 
        }
        strTemp = strTemp.substring(i);
        len=strTemp.length;
        j = len-1;
        while (j>0 && strTemp.charAt(j)==" "){ 
            j = j-1; 
        }
        strTemp = strTemp.substring(0,j+1);
        return strTemp;
}

</SCRIPT>

</head>
<body>
<!-- <body onLoad="document.codelist.SUBTYPE.focus();"> -->
<!-- <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	

<?php

$v_exists = 0;
if (isset($_GET['exists'])) $v_exists = 1;

if (!($_SERVER['REQUEST_METHOD'] == 'POST')) {
$v_pk_codelst_maint = $_GET["pk_codelst_maint"];
$rs = mysql_query("select codelst_type, table_name from et_codelst_maint where pk_codelst_maint = ".$v_pk_codelst_maint);

if (mysql_num_rows($rs) > 0) {
	while ($data = mysql_fetch_array($rs)) {		
		$v_codelst_type = $data["codelst_type"];
		$v_tablename = $data["table_name"];
	}
}
if ($v_exists == 1){
	$v_subtyp = $_GET["subtype"];
	$v_desc = $_GET["desc"];
	$v_seq = $_GET["seq"];
} else {
	$v_desc = "";
	if($v_tablename!="EVENT_DEF"){
		$query = "select max(codelst_seq) as sequence from $v_tablename where trim(codelst_type) = trim('$v_codelst_type')";
		$results = executeOCIQuery($query,$ds_conn);	
		$v_seq = ($results["SEQUENCE"][0] + 1);
		$v_len = 15 - strlen("_".$v_seq);
		$v_subtyp = substr(trim($v_codelst_type),0,$v_len)."_".$v_seq;
	}	
}
echo '<div class="navigate">Code List - Add New Value</div>';
?>
	
<FORM name="codelist" action="codelist_new.php" method=post onSubmit="if (validate(document.codelist) == false) return false;">
<INPUT TYPE="HIDDEN" NAME="pk_codelst_maint" value = "<?PHP echo $v_pk_codelst_maint ?>"></INPUT>
<INPUT TYPE="HIDDEN" NAME="TABLE_NAME" value = "<?PHP echo $v_tablename ?>"></INPUT>
<?php
if($v_tablename == "EVENT_DEF"){ ?>
	<BR><TABLE width="100%" border="0" >
	<TR><TD>Event Type</TD><TD><INPUT TYPE="TEXT" READONLY NAME="TYPE" value = "<?PHP echo trim($v_codelst_type) ?>"></INPUT></TD></TR>
	<TR><TD>Name</TD><TD style="font-size:11px; font-style:italic; color:#999999;"><INPUT NAME="SUBTYPE" TYPE="TEXT" class="required" value="<?PHP echo $v_subtyp; ?>" size="35" maxlength="200">
	</INPUT> (Maximum Length: 200 Characters)</TD></TR>
	<TR><TD>Description</TD><TD><INPUT class="required" TYPE="TEXT" NAME="DESC" value="<?PHP echo $v_desc; ?>" size=100 maxlength=1000></INPUT>
		<span style="font-size:11px; font-style:italic; color:#999999;"> (Maximum Length: 1000 Characters)</span></TD>
	</TR>	
<?php }else{ ?>
	<BR><TABLE width="100%" border="0" >
	<TR><TD>Type</TD><TD><INPUT TYPE="TEXT" READONLY NAME="TYPE" value = "<?PHP echo trim($v_codelst_type) ?>"></INPUT></TD></TR>
	<TR><TD>Sub Type</TD><TD style="font-size:11px; font-style:italic; color:#999999;"><INPUT NAME="SUBTYPE" TYPE="TEXT" class="required" value="<?PHP echo $v_subtyp; ?>" size="15" maxlength="15">
	</INPUT> (Maximum Length: 15 Characters)</TD></TR>
	<TR><TD>Description</TD><TD><INPUT class="required" TYPE="TEXT" NAME="DESC" value="<?PHP echo $v_desc; ?>" size=100 maxlength=200></INPUT>
		<span style="font-size:11px; font-style:italic; color:#999999;"> (Maximum Length: 200 Characters)</span></TD>
	</TR>
	<TR><TD>Sequence</TD><TD><INPUT class="required" TYPE="TEXT" NAME="SEQ" value="<?PHP echo $v_seq; ?>" size=3 maxlength=3>
	  </INPUT>
	  <span style="font-size:11px; font-style:italic; color:#999999;">
	  </INPUT>
	(Maximum Length: 3 Numbers)</span></TD>
	</TR>
	<?php if($v_codelst_type == "evtaddlcode") {?>
	<TR><TD>Financial Details</TD>
	<TD><input type = "radio" name="codelst_financial" value="financial">Yes</input>
	<input type = "radio" name="codelst_financial" value="">No</input></TD>
	</TR>
	<?php }?>	
<?php }?>
<?PHP
	$codelst_typeArr = array("fillformstat","studystat_type","studystat","query_status","milestone_stat","budget_stat","calStatStd","cost_desc","coverage_type");	
	if(in_array(trim($v_codelst_type), $codelst_typeArr)){
		if($v_codelst_type == 'calStatStd'){
			echo "<tr><TD>&nbsp;</TD><td>&nbsp;</td></tr>";
			echo '<tr><td>&nbsp;</td><td><input type=checkbox name=roleconfig value=checkbox disabled value="" checked=checked>&nbsp;&nbsp;&nbsp;This Code is available when a "study role" is not in scope or when no other is configured for a study team role. Please leave it checked if you are not sure at this moment or you always want this code to be visible. Please uncheck if you wish to make this code available in study team "Roles".</td></tr>';
			echo "<tr><TD>&nbsp;</TD><td>&nbsp;</td></tr>";
			echo '<Input type="hidden" name="config_codtyp" id="config_codtyp" value="'.trim($v_codelst_type).'">';			
		}else{
			echo "<tr><TD>&nbsp;</TD><td>&nbsp;</td></tr>";
			echo '<tr><td>&nbsp;</td><td><input type=checkbox name=roleconfig value=checkbox >&nbsp;&nbsp;&nbsp;This Code is available when a "study role" is not in scope or when no other is configured for a study team role. Please leave it checked if you are not sure at this moment or you always want this code to be visible. Please uncheck if you wish to make this code available in study team "Roles".</td></tr>';
			echo "<tr><TD>&nbsp;</TD><td>&nbsp;</td></tr>";
			echo '<Input type="hidden" name="config_codtyp" id="config_codtyp" value="'.trim($v_codelst_type).'">';			
		}
	}	

	//implemented for the enhancement v8.9eT-3 on 20-12-2010
	if($v_codelst_type=="site_type"){
		echo "<tr><TD>&nbsp;</TD><td>&nbsp;</td></tr>";
		echo "<tr><td></td><td><input type=checkbox name=sos value=checkbox>&nbsp;This is a 'Site of Service'.</td></tr>";
		echo "<tr><TD>&nbsp;</TD><td>&nbsp;</td></tr>";
	}


$linking = array('studystat'=>'studystat_type','studystat_type'=>'studystat','tarea'=>'study_division','study_division'=>'tarea');
if (array_key_exists($v_codelst_type,$linking)) {
	$v_sql = "select codelst_desc,codelst_subtyp,pk_codelst from er_codelst where codelst_type='".$linking[$v_codelst_type]."' order by codelst_desc";
	$results1 = executeOCIQuery($v_sql,$ds_conn);
	echo "<tr><td colspan=2><hr color=red/></td></tr>";
	echo '<TR><TD>';
	echo ($v_codelst_type == "studystat"?'Study Types':'');
	echo ($v_codelst_type == "tarea"?'Study Division':'');
	echo ($v_codelst_type == "studystat_type"?'Study Statuses':'');
	echo ($v_codelst_type == "study_division"?'Therapeutic Area':'');
	echo ($v_codelst_type == "lib_type"?'Event Library Type':'');	
	echo '</TD><TD><table width="100%" style="border-width: 0px 0px 0px 1px;	border-spacing: 0px;	border-style: dotted dotted dotted dotted;	border-color: blue blue blue #33cc00;	border-collapse: collapse;	background-color: white;"><tr>';
	for ($rec1 = 0; $rec1 < $results_nrows; $rec1++){
		echo '<td><input type=checkbox name=CUSTOMCOL1['.$rec1.'] value='.$results1["PK_CODELST"][$rec1].'>'.$results1["CODELST_DESC"][$rec1].'&nbsp;&nbsp;&nbsp;</td>';
		if (fmod($rec1+1,3) == 0) echo '</tr><tr>';
	}
	echo '</tr></table></TD></TR>';
	echo "<tr><td colspan=2><hr color=red/></td></tr>";
}else if($v_codelst_type=="lib_type"){	

	$v_sql = "select max(EVENT_ID) as EVENT_ID,name,DESCRIPTION,EVENT_TYPE from EVENT_DEF where event_type = 'L' group by name,DESCRIPTION,EVENT_TYPE order by name";
	$results = executeOCIQuery($v_sql,$ds_conn);

}
if($v_codelst_type=="L" && $v_tablename=="EVENT_DEF"){
		$etypeQ = "SELECT * FROM SCH_CODELST where CODELST_TYPE='lib_type'";	
		$etype = executeOCIQuery($etypeQ,$ds_conn);		
		
		$types = "";
		
		for($ty=0; $ty < $results_nrows; $ty++){
			if($etype["PK_CODELST"][$ty]==$seltyp["EVENT_LIBRARY_TYPE"][0]){
				$types .= "<option value=".$etype["PK_CODELST"][$ty]." SELECTED>".$etype["CODELST_DESC"][$ty]."</option>";			
			}else{
				$types .= "<option value=".$etype["PK_CODELST"][$ty].">".$etype["CODELST_DESC"][$ty]."</option>";						
			}

		}		
		echo '<tr>';
		echo '<td >Event Library type</td><TD><select name="typeChk">'.$types.'</select>';
		echo '</TD></tr>';
}
?>
</TABLE>
<BR><input type="image" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';"> 

<!--<BR><img src="./img/submit.png" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';" onClick="if (validate(document.codelist) == false) return false;"/> -->

</form>
<?php 
	if ($v_exists == 1) {
		if(isset($_GET['dCat'])){ ?>
			<div class="important" style="margin-left: 0.5in; margin-right: 0.5in;">
			<table border="0" summary="Warning: Disabling the firewall may make your system vulnerable">
			<tr>
			<th align="left">Note</th>
			</tr>
			<tr><td align="left" valign="top">
				<p>Category is already exists. Please enter a unique Category.</p></td>
			</tr>
			</table>
			</div>
		<?php }else{
		?>
			<script type="text/javascript">
				alert("Sub Type already exists. Please enter a unique Sub Type.");
			</script>

			<div class="important" style="margin-left: 0.5in; margin-right: 0.5in;">
			<table border="0" summary="Warning: Disabling the firewall may make your system vulnerable">
			<tr>
			<th align="left">Note</th>
			</tr>
			<tr><td align="left" valign="top">
				<p>Sub Type already exists. Please enter a unique Sub Type.</p></td>
			</tr>
			</table>
			</div>
		<?PHP
		}
	} else {
		?>
		<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
		<tr>
		<th align="left">Note</th>
		</tr>
		<tr><td align="left" valign="top">
				<p>Sub Type must be unique within Type.</p>
		</td></tr>
		</table></div>
		<?PHP
	}
} else {

$v_tablename = $_POST['TABLE_NAME'];
$v_pk_codelst_maint = $_POST['pk_codelst_maint'];
$v_type = $_POST['TYPE'];
$v_subtyp = $_POST['SUBTYPE'];
$v_seq = $_POST['SEQ'];
$v_desc = $_POST['DESC'];
$v_codelst_custom_col1 = "";
$skip=0;

if (isset($_POST['CUSTOMCOL1'])) {
	if ($v_type == 'studystat_type' || $v_type == 'study_division')	 {
		$studystat_types = $_POST['CUSTOMCOL1'];
		foreach($studystat_types as $studystat_type_pk) {
			$query = "select codelst_custom_col1 from er_codelst where pk_codelst = $studystat_type_pk";
			$results = executeOCIQuery($query,$ds_conn);
			$customCol1 = $results["CODELST_CUSTOM_COL1"][0];
			if (strlen(trim($customCol1)) == 0) {
				$v_codelst_custom_col1 = $v_subtyp;				
				$query = "update er_codelst set codelst_custom_col1 = '".trim($v_codelst_custom_col1)."' where pk_codelst = $studystat_type_pk";
				$results = executeOCIUpdateQuery($query,$ds_conn);
			} else {
				if (!stripos(','.$customCol1.',',$v_subtyp)) {
					$v_codelst_custom_col1 .= $customCol1.','.$v_subtyp;
					$query = "update er_codelst set codelst_custom_col1 = '".trim($v_codelst_custom_col1)."' where pk_codelst = $studystat_type_pk";					
					$results = executeOCIUpdateQuery($query,$ds_conn);
				}
			}
			$v_codelst_custom_col1 = "";
		}		
	}
	if ($v_type == 'studystat' || $v_type == 'tarea')	 {
		$v_codelst_custom_col1 = isset($_POST['CUSTOMCOL1'])? $_POST['CUSTOMCOL1'] : "" ;
		if (is_array($v_codelst_custom_col1)) {
			$v_codelst_custom_col1 = implode(",",$v_codelst_custom_col1);
			$query = "select codelst_subtyp from er_codelst where pk_codelst in ($v_codelst_custom_col1)";
			$results = executeOCIQuery($query,$ds_conn);
//			$results = executeOCIQuery($codlst_new_7,$ds_conn);
			$v_codelst_custom_col1 = implode(",",$results['CODELST_SUBTYP']);
		}
	}

}

if($v_tablename=="EVENT_DEF"){
	$query = "select count(*) as count from ".$v_tablename." where trim(event_id) = '".trim($v_type)."' and trim(name) = '".trim($v_subtyp)."'";
	$results = executeOCIQuery($query,$ds_conn);
	$v_count = $results["COUNT"][0];
}else{
	$query = "select count(*) as count from ".$v_tablename." where trim(codelst_type) = '".trim($v_type)."' and trim(codelst_subtyp) = '".trim($v_subtyp)."'";
	$results = executeOCIQuery($query,$ds_conn);
	$v_count = $results["COUNT"][0];
}

if($v_count > 0){
	echo '<meta http-equiv="refresh" content="0; url=./codelist_new.php?exists=1&pk_codelst_maint='.$v_pk_codelst_maint.'&subtype='.$v_subtyp.'&desc='.$_POST['DESC'].'&seq='.$_POST['SEQ'].'">';
}else{
        if($v_tablename == "ER_CODELST"){
		$query="select count(*) as count from er_ctrltab where ctrl_key = 'study_rights'";
		$results1 = executeOCIQuery($query,$ds_conn);
		$v_role = str_pad("",$results1["COUNT"][0],"0");
                
                // for the bug 22213
		//$query = "insert into er_ctrltab (pk_ctrltab,ctrl_value,ctrl_key,ctrl_desc) values (seq_er_ctrltab.nextval,'$v_role','".trim($_POST['SUBTYPE'])."','".trim($_POST['DESC'])."')";
		//$results = executeOCIUpdateQuery($query,$ds_conn);
		
		if(isset($_POST['sos'])){                    
                    $query = "insert into er_codelst (pk_codelst,codelst_type,codelst_subtyp,codelst_desc,codelst_hide,codelst_kind,CODELST_SEQ,CODELST_CUSTOM_COL1,CODELST_CUSTOM_COL) values (seq_er_codelst.nextval,'".trim($_POST['TYPE'])."','".trim($_POST['SUBTYPE'])."','".trim($_POST['DESC'])."','N','".trim($_POST['codelst_financial'])."',$v_seq,'".trim($v_codelst_custom_col1)."','service')";
		}else{                    
                    $query = "insert into er_codelst (pk_codelst,codelst_type,codelst_subtyp,codelst_desc,codelst_hide,codelst_kind,CODELST_SEQ,CODELST_CUSTOM_COL1,CODELST_CUSTOM_COL) values (seq_er_codelst.nextval,'".trim($_POST['TYPE'])."','".trim($_POST['SUBTYPE'])."','".trim($_POST['DESC'])."','N','".trim($_POST['codelst_financial'])."',$v_seq,'".trim($v_codelst_custom_col1)."','')";
		}                
	}elseif(trim($v_tablename) == "SCH_CODELST"){
		$exiQ = "select * from sch_codelst where CODELST_TYPE='calStatStd' and upper(CODELST_SUBTYP)='".strtoupper($_POST['SUBTYPE'])."'";
		$results = executeOCIQuery($exiQ,$ds_conn);
		
		if($results_nrows!=1){
			$query = "insert into sch_codelst (pk_codelst,codelst_type,codelst_subtyp,codelst_desc,fk_account,codelst_hide,CODELST_SEQ) values (sch_codelst_seq1.nextval,'".trim($_POST['TYPE'])."','".trim($_POST['SUBTYPE'])."','".trim($_POST['DESC'])."',0,'N',$v_seq)";			
		}else{
		?>
		<script type="text/javascript">
			alert("Sub Type Already Exists!!!");
		</script>
		<?php	
			$skip = 1;
			echo "<meta http-equiv='refresh' content='0; url=./codelist_new.php?pk_codelst_maint=131'>";			
		}
		

	}elseif(trim($v_tablename) == "EVENT_DEF"){
		$v_type = $_POST['TYPE'];
		$v_name = $_POST['SUBTYPE'];
		$v_desc = $_POST['DESC'];		
		$pk_mt = $_POST['pk_codelst_maint'];
		
		$newType = $_POST['typeChk'];
		
		$selQ = "SELECT * FROM EVENT_DEF WHERE NAME='".$v_name."'";
		$cResult = executeOCIQuery($selQ,$ds_conn);
		if($results_nrows!=0){
			echo '<input type="hidden" value="1" name="exists" id="exists"/>';			
			header("Location: codelist_new.php?exists=1&dCat=1&pk_codelst_maint=".$pk_mt);
//			echo '<meta http-equiv="refresh" content="0; url=./codelist_new.php">';
		}else{
			$query = "INSERT INTO EVENT_DEF (EVENT_ID, CHAIN_ID, EVENT_TYPE, NAME,  DESCRIPTION, EVENT_LIBRARY_TYPE) VALUES (EVENT_DEFINITION_SEQ.nextval,EVENT_DEFINITION_SEQ.currval,'L','".trim($v_name)."','".trim($v_desc)."',".trim($newType).")";		
		}	
	}
	$results = executeOCIUpdateQuery($query,$ds_conn);
        $mapcolname = "SELECT distinct codelst_subtyp FROM er_codelst WHERE codelst_subtyp IN (SELECT codelst_subtyp FROM er_codelst WHERE codelst_type = 'patStatus')";
        $mapcolnamers = executeOCIQuery($mapcolname, $ds_conn);
        $mapcolnames = '';
        if(isset($results_nrows)){
            for($q=0; $q<$results_nrows; $q++){
                if($q == $results_nrows-1){
                    $mapcolnames .= "'".strtoupper($mapcolnamers['CODELST_SUBTYP'][$q])."'";
                }else{
                    $mapcolnames .= "'".strtoupper($mapcolnamers['CODELST_SUBTYP'][$q])."',";
                }

            }
        }
        $mapcolnames = str_replace("'", "''", $mapcolnames);
        $mapcolnames = "'".$mapcolnames."'";
        $mapcolQry = "update velink.vlnk_modcol set map_subtyp = ".$mapcolnames." where pk_vlnk_modcol = 112";
        
	$results1 = executeOCIUpdateQuery($mapcolQry,$ds_conn);
       // echo "nothing";
       // exit;
	
	// implemented this on 09-12-2010 for the requirement of eT3.1-1 (for eres compatible of 8.9.x)
	if($skip!=1){	
		$codelst_typeArr = array("fillformstat","studystat_type","studystat","query_status","milestone_stat","budget_stat","cost_desc","coverage_type");
		if(in_array($_POST['config_codtyp'], $codelst_typeArr)){
			if($v_tablename=="ER_CODELST"){
				$curQ = "SELECT SEQ_ER_CODELST.currval as cuva from ER_CODELST";
			}else{
				$curQ = "SELECT SCH_CODELST_SEQ1.currval as cuva from SCH_CODELST";			
			}
			$curQRS = executeOCIQuery($curQ,$ds_conn);	
			$cuVal = $curQRS['CUVA'][0];
			
			if($_POST['roleconfig']!=""){
				$conQ = "UPDATE ".$v_tablename." set CODELST_STUDY_ROLE ='default_data' where pk_codelst=".$cuVal;			
			}else{
				$conQ = "UPDATE ".$v_tablename." set CODELST_STUDY_ROLE ='' where pk_codelst=".$cuVal;	
			}
			$results = executeOCIUpdateQuery($conQ,$ds_conn);
		}elseif($_POST['config_codtyp']=="calStatStd"){
			if($v_tablename=="ER_CODELST"){
				$curQ = "SELECT SEQ_ER_CODELST.currval as cuva from ER_CODELST";
			}else{
				$curQ = "SELECT SCH_CODELST_SEQ1.currval as cuva from SCH_CODELST";			
			}
			$curQRS = executeOCIQuery($curQ,$ds_conn);	
			$cuVal = $curQRS['CUVA'][0];
			
			$conQ = "UPDATE ".$v_tablename." set CODELST_STUDY_ROLE ='default_data' where pk_codelst=".$cuVal;
			$results = executeOCIUpdateQuery($conQ,$ds_conn);
		}
	}
	
	
echo "Data Saved Successfully !!!";
echo '<meta http-equiv="refresh" content="0; url=./codelist.php">';

}
OCICommit($ds_conn);
OCILogoff($ds_conn);
	
}

?>
</div>
</body>
</html>
<?php
}
else header("location: index.php?fail=1");
?>
