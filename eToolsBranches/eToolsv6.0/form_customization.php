<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>
<html>
<head>
    <title>Velos eTools -> Form Customization</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>

<script>
function formPreview(formPk,mode){
	var win = "form_preview.php?formPk="+formPk+"&mode="+mode;
	window.open(win,'mywin',"toolbar=no,scrollbars=yes,resizable=no,menubar=no,status=yes,width=825,height=600,top=90,left=150");
}


</script>

</head>


<body>

<div id="fedora-content">	
<div class="navigate">Form Customization</div>


 
<?PHP


if (isset($_POST["formname"]) || isset($_GET["formname"])){
	if (isset($_POST["formname"])) { 
		$searchvalue = $_POST["formname"];
		$v_formstat = $_POST["frmstat"];
		$v_linkto = $_POST["linkedto"];
	} else {
		$searchvalue = $_GET["formname"];
		$v_formstat = $_GET["frmstat"];
		$v_linkto = $_GET["linkedto"];
	}
	$query_sql = "select pk_codelst,codelst_desc || decode(codelst_type,'frmlibstat',' (library)') as codelst_desc  from er_codelst where (codelst_type = 'frmstat' or codelst_type = 'frmlibstat')";
	$results = executeOCIQuery($query_sql,$ds_conn);
	$v_frmstat = '<option value="">All</option>';
	for ($rec = 0; $rec < $results_nrows; $rec++){
		$v_frmstat .= '<option '.($results["PK_CODELST"][$rec] == $v_formstat ? " selected ": "").'value="'.$results["PK_CODELST"][$rec].'">'.$results["CODELST_DESC"][$rec]."</option>";
	}

	$v_linkedto = '<option '.($v_linkto == "ALL"? "selected":"").' value="ALL">All</option>
	<option '.($v_linkto == "A"? "selected":"").' value="A">Account</option>
	<option '.($v_linkto == "PA"? "selected":"").' value="PA">All Patients</option>
	<option '.($v_linkto == "SA"? "selected":"").' value="SA">All Studies</option>
	<option '.($v_linkto == "C"? "selected":"").' value="C">CRF</option>
	<option '.($v_linkto == "L"? "selected":"").' value="L">Library</option>
	<option '.($v_linkto == "PS"? "selected":"").' value="PS">Patient (All Studies)</option>
	<option '.($v_linkto == "PR"? "selected":"").' value="PR">Patient (All Studies - Restricted)</option>
	<option '.($v_linkto == "SP"? "selected":"").' value="SP">Patient (Specific Study)</option>
	<option '.($v_linkto == "S"? "selected":"").' value="S">Study</option>
	';
	
} else {
	$searchvalue = "";
	$query_sql = "select pk_codelst,codelst_desc || decode(codelst_type,'frmlibstat',' (library)') as codelst_desc from er_codelst where codelst_type = 'frmstat' or codelst_type = 'frmlibstat'";
	$results = executeOCIQuery($query_sql,$ds_conn);
	$v_frmstat = '<option value="">All</option>';
	for ($rec = 0; $rec < $results_nrows; $rec++){
		$v_frmstat .= '<option value="'.$results["PK_CODELST"][$rec].'">'.$results["CODELST_DESC"][$rec]."</option>";
	}
	
	$v_linkedto = '<option selected value="ALL">All</option>
	<option value="A">Account</option>
	<option value="PA">All Patients</option>
	<option value="SA">All Studies</option>
	<option value="C">CRF</option>
	<option value="L">Library</option>
	<option value="PS">Patient (All Studies)</option>
	<option value="PR">Patient (All Studies - Restricted)</option>
	<option value="SP">Patient (Specific Study)</option>
	<option value="S">Study</option>
	';

}
?>
<form name="formtransfer" method="post" action="form_customization.php">
<table border = "0">
<tr>
	<td>Search Form Name: </td><td><input name="formname" type="text" size="30" maxlength="100" value="<?PHP echo $searchvalue; ?>"/></td>
	<td>Status: </td><td><select name="frmstat"><?PHP echo $v_frmstat;?></select></td>
	<td>Linked to: </td><td><select name="linkedto" sort><?PHP echo $v_linkedto;?></select></td>
	<td>
	<!-- <input type=submit value=Submit> -->
	<img src="./img/search.png" onMouseOver="this.src='./img/search_m.png';" onMouseOut="this.src='./img/search.png';" onClick="document.formtransfer.submit();"/>
<!--	<img src="./img/search.png" onclick="document.formtransfer.submit();"></input></td> -->
</tr>

</table>
</form>

<?PHP
if (($_SERVER['REQUEST_METHOD'] == 'POST') || isset($_GET["formname"])){
//$v_account = $_POST["fk_account"];
?>
<Table border="1" width="100%"><TR>
<TH width="20%">FORM NAME</TH>
<TH width="30%">FORM DESCRIPTION</TH>
<TH width="10%">STATUS</TH>
<!--
<TH width="20%">LINKED TO</TH>
<TH width="10%">STUDY NUMBER</TH>
-->
<TH width="10%">Form XSL</TH>
<TH width="10%">Print XSL</TH>
<TH width="10%">Form XML</TH>
</TR>
<?php

$v_query_sql_1 = "SELECT pk_formlib,form_name, form_desc,decode(trim(lf_displaytype),'C','CRF','S','Study','SP','Patient (Specific Study)','PS','Patient (All Studies)','PR','Patient (All Studies - Restricted)','A','Account','SA','All Studies','PA','All Patients') as lf_displaytype,DECODE(fk_study,NULL,'',(SELECT study_number FROM ER_STUDY WHERE pk_study = fk_study)) AS study_number,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = form_status) as status,
length(form_viewxsl) as printpreview
FROM ER_FORMLIB a, ER_LINKEDFORMS b 
WHERE pk_formlib = fk_formlib AND  b.record_type <> 'D' AND 
LOWER(trim(form_name)) LIKE lower('%".str_replace("'","''",$searchvalue)."%') and 
form_status like '%$v_formstat%'";

$v_query_sql_1 .= ($v_linkto == 'ALL'? "":" and lf_displaytype = '$v_linkto'");


$v_query_sql_2 = "SELECT pk_formlib,form_name, form_desc,'Library' as lf_displaytype,'' AS study_number,
(SELECT codelst_desc FROM ER_CODELST WHERE pk_codelst = form_status) as status ,
length(form_viewxsl) as printpreview 
from er_formlib
where form_linkto = 'L' and record_type <> 'D' and 
LOWER(trim(form_name)) LIKE lower('%".str_replace("'","''",$searchvalue)."%') and 
form_status like '%$v_formstat%'
order by form_name
";

switch ($v_linkto){
case "ALL":
	$query_sql = $v_query_sql_1." union ".$v_query_sql_2;
	break;
case "L":
	$query_sql = $v_query_sql_2;
	break;
default:
	$query_sql = $v_query_sql_1;
}

$results = executeOCIQuery($query_sql,$ds_conn);

for ($rec = 0; $rec < $results_nrows; $rec++){
?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
	<TD><a href="#" class="tip"><img src="./img/info_1.png">
	<span>
	<table class="ttip" width="100%">
	<tr><td width="25%" valign="top"><b>Primary Key</b></td><td width="75%"><?PHP echo $results["PK_FORMLIB"][$rec]; ?></td><tr>
	<tr><td valign="top"><b>Form Name</b></td><td><?PHP echo $results["FORM_NAME"][$rec]; ?></td><tr>
	<tr><td valign="top"><b>Description</b></td><td><?PHP echo $results["FORM_DESC"][$rec]; ?></td><tr>
	<tr><td valign="top"><b>Status</b></td><td><?PHP echo $results["STATUS"][$rec]; ?></td><tr>
	<tr><td valign="top"><b>Linked to</b></td><td><?PHP echo $results["LF_DISPLAYTYPE"][$rec]; ?></td><tr>
	<tr><td valign="top"><b>Study Number</b></td><td><?PHP echo $results["STUDY_NUMBER"][$rec]; ?></td><tr>
	</table>
	</span></a> <?php echo $results["FORM_NAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["FORM_DESC"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["STATUS"][$rec] . "&nbsp;"; ?></TD>
<!--
	<TD><?php echo $results["LF_DISPLAYTYPE"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["STUDY_NUMBER"][$rec] . "&nbsp;"; ?></TD>
-->
<?php

	echo "<td align=center><a href=form_modifyxsl.php?formPk=".$results["PK_FORMLIB"][$rec]."&formname=".urlencode($results["FORM_NAME"][$rec])."&formstat=".$v_formstat."&linkedto=".$v_linkto."&type=xsl>Edit</a>";
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=# onclick="formPreview('.$results["PK_FORMLIB"][$rec].',\'form\')">Preview</a></td>';
	

	if ($results["PRINTPREVIEW"][$rec] > 0 ) {
		echo "<td align=center><a href=form_modifyxsl.php?formPk=".$results["PK_FORMLIB"][$rec]."&formname=".urlencode($results["FORM_NAME"][$rec])."&formstat=".$v_formstat."&linkedto=".$v_linkto."&type=viewxsl>Edit</a>";
		echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=# onclick="formPreview('.$results["PK_FORMLIB"][$rec].',\'print\')">Preview</a></td>';
	} else {
		echo "<td align=center><font color='#d1cebf'>Edit</font>";
		echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color='#d1cebf'>Preview</font>";
	}
	


	echo "<td align=center><a href=form_modifyxsl.php?formPk=".$results["PK_FORMLIB"][$rec]."&formname=".urlencode($results["FORM_NAME"][$rec])."&formstat=".$v_formstat."&linkedto=".$v_linkto."&type=xml>View</a></td>";

	echo "</TR>";
}
?>
</TABLE>
<?php } ?>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
