<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> CDUS Log View</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 



?>
</head>


<body>

<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<div class="navigate">CDUS Submission</div>
<?php
$pk_datamain = $_GET["pk_datamain"];
$fk_exp_definition = $_GET["fk_exp_definition"];



//$query_sql = "SELECT pk_exp_datadetails,sec_name,COL1, COL2, COL3, COL4, COL5 FROM EXP_DATADETAILS, EXP_SECTIONS WHERE fk_exp_datamain = ".$pk_datamain." AND pk_exp_sections = fk_exp_sections ORDER BY col1,col2,fk_exp_sections";

$query_sql = "SELECT count(*) as cnt from EXP_LOG,exp_datadetails WHERE fk_exp_datamain = ".$pk_datamain." AND pk_exp_datadetails = fk_exp_datadetails ";
$results = executeOCIQuery($query_sql,$ds_conn);

echo '<table width="100%">';
echo "<tr>";
echo "<td>";
echo "<a href=cdus_validate.php?pk_datamain=".$pk_datamain."&fk_exp_definition=".$fk_exp_definition.">VALIDATE</a>";
echo "</td>";
echo "<td align=right>";
echo "Total Rows: ".$results["CNT"][0];
echo "</td>";
echo "</tr>";
echo "</table>";

?>
<Table border="1" width="100%"><TR>
<TH>PK</TH> 
<TH>&nbsp;</TH>
<TH>STUDY</TH>
<TH>PATIENT</TH>
<TH>SECTION</TH>
<TH>COL3</TH>
<TH>COL4</TH>
<TH>COL5</TH>
<!--
<TH>COL6</TH>
<TH>COL7</TH>
<TH>COL8</TH>
<TH>COL9</TH>
-->
<TH>Rule</TH>
<TH>&nbsp;</TH>
</TR>
<?php


$query_sql = "SELECT pk_exp_datadetails,fk_exp_sections,sec_name,COL1, COL2, COL3, COL4, COL5, (SELECT rule_name FROM EXP_RULES WHERE pk_exp_rules = fk_exp_rules) AS rule_name FROM EXP_DATADETAILS, EXP_SECTIONS, EXP_LOG WHERE fk_exp_datamain = ".$pk_datamain." AND pk_exp_sections = fk_exp_sections AND pk_exp_datadetails = fk_exp_datadetails ORDER BY col1,col2,fk_exp_sections";
$results = executeOCIQuery($query_sql,$ds_conn);
$rows = $results_nrows;

for ($rec = 0; $rec < $rows; $rec++){

if ($results["RULE_NAME"][$rec] == "" ) { 
	echo "<TR>";
} else {
	echo '<TR bgcolor="PINK">';
}

	echo "<td><a href=cdus_edit.php?pk_exp_datadetails=".$results["PK_EXP_DATADETAILS"][$rec]."&fk_exp_sections=".$results["FK_EXP_SECTIONS"][$rec]."&pk_exp_datamain=".$pk_datamain."&fk_exp_definition=".$fk_exp_definition.">EDIT</a></td>";

?>

 	<TD><?php echo $results["PK_EXP_DATADETAILS"][$rec] . "&nbsp;"; ?></TD> 
	
	<TD><?php echo $results["COL1"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL2"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["SEC_NAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL3"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL4"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL5"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["RULE_NAME"][$rec] . "&nbsp;"; ?></TD>

<?php
	echo "<td><a href=cdus_delete.php?pk_exp_datadetails=".$results["PK_EXP_DATADETAILS"][$rec]."&fk_exp_sections=".$results["FK_EXP_SECTIONS"][$rec]."&pk_exp_datamain=".$pk_datamain."&fk_exp_definition=".$fk_exp_definition.">DELETE</a></td>";

//	echo "<td><a href=cdus_delete.php?pk_exp_datadetails=".$results["PK_EXP_DATADETAILS"][$rec].">DELETE</a></td>";
?>
	
<!--
	<TD><?php echo $results["COL6"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL7"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL8"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL9"][$rec] . "&nbsp;"; ?></TD>
-->
<?php

echo "</TR>";
}
?>
</TABLE>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
