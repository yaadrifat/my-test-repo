<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Publish View to AHQ</title>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
<script>
function validate() {
	if (document.ahq.view.value == "") {
		alert("Select 'View Name'."); 
		document.ahq.view.focus(); 
		return false;
	} 
	if (document.ahq.LOOKUP.value == "") {
		alert("Enter data in 'Adhoc Query Name'."); 
		document.ahq.LOOKUP.focus(); 
		return false;
	} 
	return true;
	
}
</script>
</head>
<body onLoad="document.ahq.view.focus();">
<div id="fedora-content">	
<div class="navigate">Publish view to AHQ</div>
<br>
<?php


if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 

$query_sql = "SELECT view_name FROM ALL_VIEWS WHERE owner = 'ERES'";
$results = executeOCIQuery($query_sql,$ds_conn);

$dd_views = '<option value="">Select an option</option>';
for ($rec = 0; $rec < $results_nrows; $rec++){
           $dd_views .= "<option value=".$results["VIEW_NAME"][$rec].">".$results["VIEW_NAME"][$rec]."</option>";
}


?>

<FORM name="ahq" action="lookup_view_adhoc.php" method=post onSubmit="if (!validate(document.ahq)) return false;">

<TABLE width="100%" border="0">
<TR><TD width="25%">View Name</TD><TD><?PHP echo "<select class=required name=\"view\">".$dd_views."</select>"; ?></TD></TR>
<TR><TD>Adhoc Query 'Available Tables' Name</TD><TD><INPUT TYPE="TEXT" class="required" NAME="LOOKUP" maxlength=20></INPUT></TD></TR>
</TABLE>
<!-- <input type="submit" name="submit" value="Submit"></INPUT> -->
<BR><input type="image" name="submit" value="Submit" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';" />


</form>

		<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
		<tr>
		<th align="left">Note</th>
		</tr>
		<tr><td align="left" valign="top">
		<p>
		      In order to apply appropriate access rights in Adhoc query the view must have one or all columns below depending on the type of data:<BR><BR>
		      FK_ACCOUNT - Mandatory<BR>
			  FK_PER - Required for patient related adhoc query<BR>
			  FK_STUDY - Required for study related adhoc query<BR>
		    </p>
		</td></tr>
		</table></div>
		
<?php 
} else {


$v_viewname = $_POST["view"];
$v_lookupname = $_POST["LOOKUP"];


$query_sql = "SELECT column_name,lower(data_type) as data_type FROM ALL_TAB_COLS WHERE Table_name = '$v_viewname' and owner = 'ERES' ORDER BY column_id";
$results = executeOCIQuery($query_sql,$ds_conn);

$v_patlkp = 0;
$v_stdlkp = 0;
$v_actlkp = 0;
$v_lkptype = "";
for ($rec = 0; $rec < $results_nrows; $rec++){
	if ($results["COLUMN_NAME"][$rec] == 'FK_PER'){
		$v_patlkp = 1;
	}
	if ($results["COLUMN_NAME"][$rec] == 'FK_STUDY'){
		$v_stdlkp = 1;
	}
	if ($results["COLUMN_NAME"][$rec] == 'FK_ACCOUNT'){
		$v_actlkp = 1;
	}
}
if (($v_actlkp == 1) || ($v_patlkp == 1) || ($v_stdlkp == 1)) {
?>
<FORM action="lookup_view_adhoc_exe.php" method=post>
<?php

if ($v_patlkp == 0 &&  $v_stdlkp == 0 && $v_actlkp == 1) $v_lkptype = 'dyn_a';

if ($v_patlkp == 0 &&  $v_stdlkp == 1 && $v_actlkp == 1) $v_lkptype = 'dyn_s';

if ($v_patlkp == 1 && $v_actlkp == 1) $v_lkptype = 'dyn_p';

/*
if ($v_patlkp == 1) {
	$v_lkptype = 'dyn_p';
} else {
	if ($v_stdlkp == 1) {
		$v_lkptype = 'dyn_s';
	}
}
*/

echo "View Name: <INPUT readonly name=\"viewname\" value=\"$v_viewname\"></input>"; 
echo "&nbsp;&nbsp;&nbsp;&nbsp;";
echo "Adhoc Query Lookup Name: <INPUT readonly name=\"lookupname\" value=\"$v_lookupname\"></input>"; 
echo "&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<INPUT type=\"hidden\" size=5 readonly name=\"viewtype\" value=\"$v_lkptype\"></input>"; 
echo '<TABLE width="100%" BORDER=1>';
echo '<TR><TH width="30%">Column Name</TH><TH width="10%">Type</TH><TH width="30%">Display Name</TH></TR>';

$v_coltype = '';		
$v_colkeyword = '';
for ($rec = 0; $rec < $results_nrows; $rec++){

if (($results["DATA_TYPE"][$rec] == 'varchar2') || ($results["DATA_TYPE"][$rec] == 'varchar') || ($results["DATA_TYPE"][$rec] == 'char') || ($results["DATA_TYPE"][$rec] == 'clob')) {
	$v_coltype = 'varchar';		
} else {
	$v_coltype = $results["DATA_TYPE"][$rec];		
}

$v_colkeyword = substr($results["COLUMN_NAME"][$rec],0,19);

if ($results["COLUMN_NAME"][$rec] == 'FK_PER') {
	$v_colkeyword = 'LKP_PK';
} else {
	if (($results["COLUMN_NAME"][$rec] == 'FK_STUDY') && ($v_patlkp == 1)){
		$v_colkeyword = 'LKP_STUDY_CHKRIGHT';
	}
	if (($results["COLUMN_NAME"][$rec] == 'FK_STUDY') && ($v_patlkp == 0)){
		$v_colkeyword = 'LKP_PK';
	}
}

if ($results["COLUMN_NAME"][$rec] == 'FK_ACCOUNT' && $v_lkptype == 'dyn_a') {
	$v_colkeyword = 'LKP_PK';
}

echo "<INPUT type=hidden size=40 name=\"colkeyword[$rec]\" value=\"".$v_colkeyword.'"></input>';
?>
	<TR>
	<TD><?php echo "<INPUT readonly size=40 name=\"colname[$rec]\" value=\"".$results["COLUMN_NAME"][$rec].'"></input>'; ?></TD>
	<TD><?php echo "<INPUT readonly size=10 name=\"coltype[$rec]\" value=\"".$v_coltype.'"></input>'; ?></TD>
<?php
	if (($results["COLUMN_NAME"][$rec] == 'FK_PER') || ($results["COLUMN_NAME"][$rec] == 'FK_STUDY') || ($results["COLUMN_NAME"][$rec] == 'FK_ACCOUNT')){
?>
	<TD><?php echo "<INPUT readonly size=40 name=\"dispname[$rec]\" value=".$results["COLUMN_NAME"][$rec]. "></input>"; ?></TD>
<?php } else { ?>
	<TD><?php echo "<INPUT size=40 name=\"dispname[$rec]\" value=".$results["COLUMN_NAME"][$rec]. "></input>"; ?></TD>
<?php } ?>	
	</TR>
<?php
}
echo "</TABLE>";
?>
<BR><input type="image" name="submit" value="SUBMIT" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';" />

</FORM>
<?php
} else {
	echo "View cannot be used in Adhoc query, please check if the view has the following columns:";
	echo "<BR><BR>";
	echo "<table border = 1>";
	echo "<TR>";
	echo "<TD><b>FK_ACCOUNT</b></TD><TD>Mandatory</TD>";
	echo "</TR><TR>";
	echo "<TD><b>FK_PER</b></TD><TD>Required for patient related adhoc query</TD>";
	echo "</TR><TR>";
	echo "<TD><b>FK_STUDY</b></TD><TD>Required for study related adhoc query<TD>";
	echo "</TR>";
	echo "</table>";
}
OCICommit($ds_conn);
OCILogoff($ds_conn);

?>
<?PHP
}

?>
</div>


</body>
</html>


<?php
}
else header("location: index.php?fail=1");
?>
		
