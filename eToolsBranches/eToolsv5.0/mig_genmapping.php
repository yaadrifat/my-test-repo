<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Data</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Migrate Data - Standard - Generate Mapping</div>

<?php

$pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];
$query_sql = "begin velink.pkg_velink.sp_generate_map(".$pk_vlnk_adapmod."); end;";
$results = executeOCIUpdateQuery($query_sql,$ds_conn);

echo "Mapping Generated...";
echo '<meta http-equiv="refresh" content="0; url=./mig_datamapping.php?pk_vlnk_adapmod='.$pk_vlnk_adapmod.'"> ';

?>
      </div>


</body>
</html>
<?php
OCICommit($ds_conn);
OCILogoff($ds_conn);
}
else header("location: ./index.php?fail=1");
?>
