<?php

	
	
session_start();	// Maintain session state
//require_once "./includes/session.php";
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){

include("./includes/oci_functions.php");
include ("./txt-db-api.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]);

//print"<pre>"; print_r($_GET); print"</pre>";

	$codelst_custom_col = $_GET['codelst_custom_col'];
	$report_name = $_GET['report_name'];
	$pk_report = $_GET['pk_report'];
	$new_report_name = $_GET['new_report_name'];
	
	if($pk_report == '') {
		$codelst_custom_col_sql = "SELECT PK_CODELST, CODELST_SUBTYP, CODELST_DESC, CODELST_CUSTOM_COL FROM ER_CODELST WHERE CODELST_CUSTOM_COL = '".trim($codelst_custom_col)."'";
		$codelst_custom_col_results = executeOCIQuery($codelst_custom_col_sql,$ds_conn);

		$codelst_custom_col_results_cnt = $results_nrows;
		
		$v_html = '<table width="100%">';
		
		$v_html .= '<Table width="80%">';
		$v_html .= '<tr><td><b>';
		$v_html .= "Velos Standard Reports:";
		$v_html .= '</b></td></tr></table>';
		
		$v_html .= '<Table border="1" width="80%"><thead><TR>';
		$v_html .= '<TH width="35%">REPORT TYPE</TH>';
		$v_html .= '<TH width="35%">REPORT NAME</TH>';
		$v_html .= '<TH width="10%">SELECT</TH>';
		$v_html .= '</TR></thead>';
		
		for ($rec = 0; $rec < $codelst_custom_col_results_cnt; $rec++){
		 
			$get_report_name_sql = "SELECT PK_REPORT, REP_NAME FROM ER_REPORT WHERE REP_TYPE = '".$codelst_custom_col_results["CODELST_SUBTYP"][$rec]."' AND UPPER(REP_NAME) like UPPER('%".trim($report_name)."%') AND REP_HIDE = 'N'";
			$get_report_name_results = executeOCIQuery($get_report_name_sql,$ds_conn);
			
			$get_report_name_results_cnt = $results_nrows;
			
			if($get_report_name_results["PK_REPORT"][0] != '') {
				for ($recc = 0; $recc < $get_report_name_results_cnt; $recc++){
				//print"<pre>"; print_r($codelst_custom_col_results); print"</pre>";
					$v_html .= '<tbody><TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';
					
					if($recc == '0') {
						$v_html .= '<TD style="padding-left:10px;">'.ucfirst($codelst_custom_col_results["CODELST_DESC"][$rec]).'</TD>';
					} else {
						$v_html .= '<TD style="padding-left:10px;">&nbsp;</TD>';
					}
					
					$v_html .= '<TD style="padding-left:10px;">'.$get_report_name_results["REP_NAME"][$recc].'</TD>';
					$v_html .= "<TD style='text-align:center;'><input name='".$get_report_name_results["PK_REPORT"][$recc]."' type='checkbox' value='".$get_report_name_results["PK_REPORT"][$recc]."' onclick=\"javascrip:select_report_name('".$get_report_name_results["PK_REPORT"][$recc]."'); Popup.showModal('showpopup');return false;\"></TD>"; 
					$v_html .= '</TR>';
					
					$noreports = $get_report_name_results["REP_NAME"][$recc];
				}
			}
		}
		if($noreports == '') {
			$v_html .= '<TR>';
			$v_html .= '<TD style="padding-left:10px; border-right:0px;">&nbsp;</TD>';
			$v_html .= '<TD style="padding-left:10px; color:#ff0000; border-left:0px; border-right:0px;">No Reports Found</TD>';
			$v_html .= '<TD style="padding-left:10px; border-left:0px;">&nbsp;</TD>';
			$v_html .= '</TR>';
		}
		
		
		$v_html .= '</table>';
		$v_html .= '<br>';
		
		$v_html .= '</table>';
		
		echo $v_html;
		
	} else {
		$pk_report_sql = "SELECT * FROM eres.ER_REPORT WHERE PK_REPORT = '".trim($pk_report)."'";
		$pk_report_results = executeOCIQuery($pk_report_sql,$ds_conn);
		
		$reportname_exists_sql = "SELECT REP_NAME FROM eres.ER_REPORT WHERE REP_NAME = '".trim($new_report_name)."'";
		$reportname_exists_results = executeOCIQuery($reportname_exists_sql,$ds_conn);
				
		if(trim($reportname_exists_results["REP_NAME"][0]) == '') {
			$last_pk_report_sql = "SELECT MAX(PK_REPORT) AS PK_REPORT FROM ER_REPORT ORDER BY ER_REPORT.PK_REPORT DESC";
			$last_pk_report_results = executeOCIQuery($last_pk_report_sql,$ds_conn);

			$last_pk_report = $last_pk_report_results["PK_REPORT"][0] + 1;
			
			$rep_sql_data = "SELECT ER_REPORT.REP_SQL FROM ERES.ER_REPORT WHERE PK_REPORT = '".trim($pk_report)."'";
			
			$rep_sql_clob = "SELECT ER_REPORT.REP_SQL_CLOB FROM ERES.ER_REPORT WHERE PK_REPORT = '".trim($pk_report)."'";
			
			$rep_column_data = "SELECT ER_REPORT.REP_COLUMNS FROM ERES.ER_REPORT WHERE PK_REPORT = '".trim($pk_report)."'";
			
			$rep_filterby_data = "SELECT ER_REPORT.REP_FILTERBY FROM ERES.ER_REPORT WHERE PK_REPORT = '".trim($pk_report)."'";
			
			$rep_filterkeyword_data = "SELECT ER_REPORT.REP_FILTERKEYWORD FROM ERES.ER_REPORT WHERE PK_REPORT = '".trim($pk_report)."'";
			
			$rep_filterapp_data = "SELECT ER_REPORT.REP_FILTERAPPLICABLE FROM ERES.ER_REPORT WHERE PK_REPORT = '".trim($pk_report)."'";
			
			$rep_codelstype_data = "SELECT ER_REPORT.FK_CODELST_TYPE FROM ERES.ER_REPORT WHERE PK_REPORT = '".trim($pk_report)."'";
			
			$rep_fkacc_data = "SELECT ER_REPORT.FK_ACCOUNT FROM ERES.ER_REPORT WHERE PK_REPORT = '".trim($pk_report)."'";
			
			$rep_genxml_data = "SELECT ER_REPORT.GENERATE_XML FROM ERES.ER_REPORT WHERE PK_REPORT = '".trim($pk_report)."'";
			
			$rep_ipadd_data = "SELECT ER_REPORT.IP_ADD FROM ERES.ER_REPORT WHERE PK_REPORT = '".trim($pk_report)."'";
			
			$rep_repdesc_data = "SELECT ER_REPORT.REP_DESC FROM ERES.ER_REPORT WHERE PK_REPORT = '".trim($pk_report)."'";
			
			$insert_copy_report = "INSERT INTO eres.ER_REPORT (PK_REPORT, REP_NAME, REP_DESC, REP_SQL, FK_ACCOUNT, FK_CODELST_TYPE, IP_ADD, REP_HIDE, REP_COLUMNS, REP_FILTERBY, GENERATE_XML, REP_TYPE, REP_SQL_CLOB, REP_FILTERKEYWORD, REP_FILTERAPPLICABLE) values ('".$last_pk_report."', '".trim($new_report_name)."', ($rep_repdesc_data), ($rep_sql_data), ($rep_fkacc_data), ($rep_codelstype_data), ($rep_ipadd_data), 'N', ($rep_column_data), ($rep_filterby_data), ($rep_genxml_data), 'rep_cstm', ($rep_sql_clob), ($rep_filterkeyword_data), ($rep_filterapp_data))";
			//print $insert_copy_report; print"<br><br><br>";
			executeOCIUpdateQuery($insert_copy_report,$ds_conn);
			
			$rep_xsl_sql = "SELECT * FROM eres.ER_REPXSL WHERE FK_REPORT = '".trim($pk_report)."'";
			$rep_xsl_results = executeOCIQuery($rep_xsl_sql,$ds_conn);
			
			$last_pk_rep_xsl_sql = "SELECT MAX(PK_REPXSL) AS PK_REPXSL FROM ER_REPXSL ORDER BY ER_REPXSL.PK_REPXSL DESC";
			$last_pk_rep_xsl_results = executeOCIQuery($last_pk_rep_xsl_sql,$ds_conn);

			$last_pk_rep_xsl = $last_pk_rep_xsl_results["PK_REPXSL"][0] + 1;
			
			$rep_xsl_data = "SELECT ER_REPXSL.REPXSL FROM ERES.ER_REPXSL WHERE FK_REPORT = '".trim($pk_report)."'";
			
			$rep_xsl_xsl_data = "SELECT ER_REPXSL.REPXSL_XSL FROM ERES.ER_REPXSL WHERE FK_REPORT = '".trim($pk_report)."'";
			
			$insert_copy_rep_xsl = "INSERT INTO eres.ER_REPXSL (PK_REPXSL, FK_REPORT, FK_ACCOUNT, REPXSL, REPXSL_DESC, REPXSL_NAME, IP_ADD, REPXSL_XSL, REPXSL_FO) values ('".$last_pk_report."', '".$last_pk_report."', '".trim($rep_xsl_results["FK_ACCOUNT"][0])."', ($rep_xsl_data), '".trim($rep_xsl_results["REPXSL_DESC"][0])."', '".trim($new_report_name)."', '".trim($rep_xsl_results["IP_ADD"][0])."', ($rep_xsl_xsl_data), '".trim($rep_xsl_results["REPXSL_FO"][0])."')";
			//print $insert_copy_rep_xsl;
			executeOCIUpdateQuery($insert_copy_rep_xsl,$ds_conn);
			
			$v_html = "<font color='#00CC00'>New custom report is created</font>";
		} else {
			$v_html = "<font color='#FF0000'>Custom report already exists</font>";
		}
		echo $v_html;
	}

OCICommit($ds_conn);
OCILogoff($ds_conn);

}
else header("location: index.php?fail=1");
?>