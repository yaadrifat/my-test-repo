<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> Upload</title>
</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");
include("./includes/file_upload.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<div id="fedora-content">	
<div class="navigate">Import Excel File</div>
<br>
<?php
$ext[0] = 'txt';
$ext[1] = 'xls';
uploader(1,$ext,50000000,getcwd().'/excelread/');
?>
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
