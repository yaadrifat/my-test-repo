<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velink -> Upload</title>
</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<?PHP

$delimiter = $_POST["delimiter"];
$lookup = $_POST["lookup"];
$fileDir = $_POST["fileDir"];

//$pk_lkpcol = $_POST["pk_lkpcol"];
//$lkpcol_name = $_POST["lkpcol_name"];
$lkpcol_dispval = $_POST["lkpcol_dispval"];
$filecol = $_POST["filecol"];

$sql = "delete from er_lkpdata where fk_lkplib = ".$lookup;
//echo $sql;
//echo "<BR>";
$results = executeOCIUpdateQuery($sql,$ds_conn);

$handle = fopen($fileDir, "r");
$row = 0;
while (($data = fgetcsv($handle, 10000, $delimiter)) !== FALSE) {
      $num = count($data);
      $row++;

      if ($row == 1) {
         $colsql = "insert into er_lkpdata (pk_lkpdata, fk_lkplib,";
         for ($c=0; $c < $num; $c++) {
             $colsql .= 'custom'.str_pad(($c + 1),3,'0',STR_PAD_LEFT).",";
         }
         $colsql = substr($colsql,0,strlen($colsql)-1).") values (";
       } else {
         $valsql = "seq_er_lkpdata.nextval,$lookup,";
         for ($c=0; $c < $num; $c++) {
             $dataval = str_replace("'","''",$data[$c]);
             $valsql .= "'$dataval',";
         }
         $valsql = substr($valsql,0,strlen($valsql)-1).")";
         $sql = $colsql.$valsql;

//echo $sql;
//echo "<BR>";
$results = executeOCIUpdateQuery($sql,$ds_conn);

       }

}

echo "Total rows imports: ".$row;
ocilogoff($ds_conn);
?>

</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
