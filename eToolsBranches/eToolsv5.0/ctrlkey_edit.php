<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Edit Code List</title>
	<script src="js/validate.js" language="javascript"></script>
	<script language="javascript">
		function splchar(){
		   var objstr = document.ctrlkeypage.object_desc.value;
		   var iChars = "\'<"; 
		   for (var i = 0; i < objstr.length; i++) {
			if (iChars.indexOf(objstr.charAt(i)) != -1) {
			  alert ("Your display text has special characters. "+objstr.charAt(i)+" This is not allowed.");
			  return false;
			}
		  }
		 }
		 
		 function fldval(){
			if(ctrlkeypage.object_desc.value =="" || ctrlkeypage.object_seq.value ==""){
				alert("Required Fields cannot be empty");
				return false;
			}
			if (!isInteger(ctrlkeypage.object_seq.value)){
				alert("Sequence allows only numbers");
				return false;
			}			
		 }		        
	</script>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");
include ("./txt-db-api.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
</head>
<body>
<div id="fedora-content">	
<?php
	$obj_pk_obj_settings= $_GET['pk_object_settings'];
	$obj_object_name = $_GET['object_name'];
	$obj_object_subtype = $_GET['object_subtype'];
	$obj_object_type = $_GET['object_type'];
	$obj_user = $_GET['user'];
	$obj_sno = $_GET['sno'];

	if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
		
		if($obj_object_type=="M" || $obj_object_type=="T"){
			$edit_sql = "SELECT * from ER_OBJECT_SETTINGS where object_name='".$obj_object_name."' and PK_OBJECT_SETTINGS ='".$obj_pk_obj_settings."'";				
		}else{
			$edit_sql = "SELECT * from ER_OBJECT_SETTINGS where object_subtype='".$obj_object_subtype."' and PK_OBJECT_SETTINGS ='".$obj_pk_obj_settings."'";				
		}
		$results = executeOCIQuery($edit_sql,$ds_conn);			
?>
<div class="navigate">Control Key  - Edit	</div>
<FORM name="ctrlkeypage" action="ctrlkey_edit.php" method="post" onSubmit="if (splchar()==false) return false; if(fldval() == false) return false;">

<input type="hidden" name="strd_pk_obj" value="<?php echo $results["PK_OBJECT_SETTINGS"][0];?>">
<input type="hidden" name="strd_type" value="<?php echo $results["OBJECT_TYPE"][0];?>">
<input type="hidden" name="strd_subtype" value="<?php echo $results["OBJECT_SUBTYPE"][0];?>">
<input type="hidden" name="strd_name" value="<?php echo $results["OBJECT_NAME"][0];?>">
<input type="hidden" name="strd_fk_ac" value="<?php echo $results["FK_ACCOUNT"][0];?>">


<input type="hidden" name="usr" value=<?php echo $obj_user;?>>
<input type="hidden" name="mod" value=<?php echo $obj_sno;?>>
<TABLE border="0">
<TR>
<td width="89">Object Name </td>
<TD colspan="2"><input disabled type = "text" name="object_name" value="<?php echo trim($results["OBJECT_NAME"][0]); ?>"></TD>
</tr><tr>
<td>Object Type </td>
<TD colspan="2"><input  disabled type = "text" name="object_type" value="<?php echo trim($results["OBJECT_TYPE"][0]); ?>"></TD>
</tr><tr>
	<td >Display Text </td>
	<TD colspan="2"><input size="100" type = "text" class="required" name="object_desc" value='<?php echo trim($results["OBJECT_DISPLAYTEXT"][0]); ?>'></TD>

</tr>
<tr>
	<td >Sequence</td><TD colspan="2"><input size="10" type = "text" class="required" name="object_seq" value="<?php echo trim($results["OBJECT_SEQUENCE"][0]); ?>"></TD>
</tr>		
	<tr>
	<?php 
		if($results["OBJECT_VISIBLE"][0]=="1"){ ?>
		  <td>Visible</td>
		  <td width="51"><label>
			<input name="object_visible" type="radio" value="1" checked>
		  Yes</label></td>
		  <td width="547"><label>
			<input name="object_visible" type="radio" value="0">
		  No</label></td>
	<?php	}else{ ?>
		  <td>Visible</td>
		  <td width="51"><label>
			<input name="object_visible" type="radio" value="1" >
		  Yes</label></td>
		  <td width="547"><label>
			<input name="object_visible" type="radio" value="0" checked>
		  No</label></td>	
	<?php	}
	?>
	</tr>
	<tr>
	<td>
	<BR><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /> 
	<input type="hidden" name="rightstr" id="rightstr" value="0" /></td>
	<td colspan="2">&nbsp;</td>
</tr>
	<tr>
	  <td>&nbsp;</td>
	  <td colspan="2">
		<div class="important" style="margin-left: 0.5in; margin-right: 0.5in;">
		<table border="0" summary="Warning: Disabling the firewall may make your system vulnerable">
		<tr>
		<th align="left">Note</th>
		</tr>
		<tr><td align="left" valign="top">
			<p>Editable fields cannot be empty.</p></td>
		</tr>
		</table>
		</div>	  
	  </td>
	  </tr>
</table>

</form>
<?php
}else{
		$pk_object = $_POST['strd_pk_obj'];
		$updated_type = $_POST['strd_type'];
		$updated_subtype = $_POST['strd_subtype'];
		$updated_name = $_POST['strd_name'];
		$udpated_user = $_POST['strd_user'];
		$updated_desc = $_POST['object_desc'];
		$updated_seq = $_POST['object_seq'];
		$updated_vis = $_POST['object_visible'];				
		$updated_fk_ac = $_POST['strd_fk_ac'];
		
		$usr = $_POST['usr'];		
		$sno = $_POST['mod'];		

		$fk_ac = "SELECT * from ER_USER WHERE PK_USER ='".$usr."'";
		$fk_account = executeOCIQuery($fk_ac,$ds_conn);
		$fk_accountVal = $fk_account["FK_ACCOUNT"][0];
	
		$updated_desc = str_replace("'","''",$updated_desc);		
		
		if($_POST['object_desc']=="" or $_POST['object_seq'] == ""){
			echo "<script language=javascript>fldVal()</script>";
		}else{					
		
			$selQ = "select * from ER_OBJECT_SETTINGS where object_subtype = '".$updated_subtype."' and  object_type = '".$updated_type."' and object_name='".$updated_name."' and fk_account=".$fk_accountVal;			
			$results = executeOCIQuery($selQ,$ds_conn);

			if($results_nrows==0){
				$insert_sql= "INSERT into ER_OBJECT_SETTINGS (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT ) VALUES (SEQ_ER_OBJECT_SETTINGS.nextval,'".trim($updated_type)."','".trim($updated_subtype)."', '".trim($updated_name)."', '$updated_seq', '$updated_vis', '".trim($updated_desc)."',$fk_accountVal)";
				$results = executeOCIQuery($insert_sql,$ds_conn);					
			}else{
				$udate_sql = "UPDATE ER_OBJECT_SETTINGS set OBJECT_DISPLAYTEXT='".trim($updated_desc)."', OBJECT_SEQUENCE='$updated_seq', OBJECT_VISIBLE='$updated_vis' where PK_OBJECT_SETTINGS='".$pk_object."' and fk_account='".$fk_accountVal."'";
				$results = executeOCIQuery($udate_sql,$ds_conn);								
			}		
		}
		
	$_SESSION['usr1']=$usr;
	$_SESSION['mod1']=$sno;
OCICommit($ds_conn);
OCILogoff($ds_conn);

echo "Data Saved Successfully !!!";	
echo '<meta http-equiv="refresh" content="0; url=./ctrlkey.php">';
}

?>

</div>
</body>
</html>


<?php
}
else header("location: index.php?fail=1");
?>
		
