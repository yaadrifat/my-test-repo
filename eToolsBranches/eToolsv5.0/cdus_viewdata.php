<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> CDUS</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">CDUS Submission - View Data</div>

<?php

$v_datamain = $_GET["pk_datamain"];

$query_sql = "select exported_data from exp_datamain WHERE pk_datamain = ".$v_datamain;
$results = executeOCIQuery($query_sql,$ds_conn);



?>
<table>
<tr><td>
<textarea rows=30 cols=130><?php echo $results['EXPORTED_DATA'][0]; ?></textarea>
</td></tr>
</table>
</div>


</body>
</html>
<?php
OCICommit($ds_conn);
OCILogoff($ds_conn);
}
else header("location: ./index.php?fail=1");
?>
