<?PHP
// OCI query commands function
function executeOCIQuery($query, &$conn) {
//echo $query;
	global $results_nrows;

	if(!$conn) {
		if($error = OCIError()) {
			die("<p>ERROR!! Couldn't connect to server!</p>");
		}
	}
	$parsed = OCIParse($conn, $query);
	OCIExecute($parsed,OCI_DEFAULT);
	if($error = OCIError($parsed)) {
		echo $query;	
		die("<p>ERROR!! Could not execute statement!</p>");
	}
	
	$results_nrows = ocifetchstatement($parsed, $results);
	return $results; 
}  

function executeOCIQueryDDL($query, &$conn) {
//echo $query;
	global $results_nrows;

	if(!$conn) {
		if($error = OCIError()) {
			die("<p>ERROR!! Couldn't connect to server!</p>");
		}
	}
	$parsed = OCIParse($conn, $query);
	OCIExecute($parsed,OCI_DEFAULT);
	if($error = OCIError($parsed)) {
		echo $query;	
		die("<p>ERROR!! Could not execute statement!</p>");
		return -1;
	}
	
//	ocifetchstatement($parsed, $results);
	return 1; 
}  

function executeOCIUpdateQuery($query, &$conn) {
//echo $query;
	global $results_nrows;

	if(!$conn) {
		if($error = OCIError()) {
			die("<p>ERROR!! Couldn't connect to server!</p>");
		}
	}
	
	$parsed = OCIParse($conn, $query);
	OCIExecute($parsed,OCI_DEFAULT);
	if($error = OCIError($parsed)) {
		echo $query;
		echo $error["sqltext"];
		die("<p>ERROR!! Could not execute insert/update statement!</p>");
	} else {
		OCICommit($conn);
	}
	
}  
?>



