<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velink -> STS Adult Cardiac Surgery Harvest</title>
</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>

<script>
function f_validate(){

	if (sts_acs.partid.value ==""){
			alert("Please enter Participant ID");
			sts_acs.partid.focus();
			return false;
	}

	if (!sts_acs.optional[0].checked && !sts_acs.optional[1].checked){
			alert("Please select Option");
			sts_acs.optional[0].focus();
			return false;
	}

	if (sts_acs.fromdate.value ==""){
			alert("Please enter From Date");
			sts_acs.fromdate.focus();
			return false;
	}

	if (sts_acs.todate.value ==""){
			alert("Please enter To Date");
			sts_acs.todate.focus();
			return false;
	}

	if (!validate_date(sts_acs.fromdate)){
			sts_acs.fromdate.focus();
			return false;
	}

	if (!validate_date(sts_acs.todate)){
			sts_acs.todate.focus();
			return false;
	}
}

function validate_date(date_col){

	if (date_col.disabled)
	{	
		return true;
	}

    date=date_col.value
    if(date==null || date=='') return true
    
    Month = date.substring(0,date.indexOf("/"));
    Day = date.substring(date.indexOf("/") + 1,date.lastIndexOf("/"));
    Year= date.substring(date.lastIndexOf("/") + 1,date.length);
   
    if ((Month.length<2) || (Day.length<2) || (Year.length<4) ) {
	alert("Please enter a valid Date in 'mm/dd/yyyy' format");
     date_col.focus()
    return false
	}
	
	
   
    if(Month > 0 && Month <= 12 && Day > 0 && Day <= 31 && Year > 999 && Year <= 9999){
            if (Month==1 || Month==3 || Month==5 || Month==7 || Month==8 || Month==10 || Month==12){
                validdays=31 
            }else if (Month==4 || Month==6 || Month==9 || Month==11){
                validdays=30
            }else if (Month==2)  {
                if (((Year % 4)==0) && ((Year % 100)!=0) || ((Year % 400)==0)) {
                    validdays=29;
                }
                 else {
                  validdays=28;
                }
            }else{
                validdays=0
           }
         if(Day > validdays || Day <= 0){
             alert("Please enter a valid Date in 'mm/dd/yyyy' format");
             date_col.focus()
             return false
         }
    }else{
        alert("Please enter a valid Date in 'mm/dd/yyyy' format");
        date_col.focus()
        return false
    }
	
return true
}


</script>

<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<?PHP
if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
$dd_year = '<select name="year"><option value="">Select an option</option>';
for ($i=2000; $i<2100; $i++){
	$dd_year .= '<option value="'.$i.'">'.$i.'</option>';
}
$dd_year .= '</select>';


$v_query = "select col8 from er_formslinear where fk_form = 486";
$results = executeOCIQuery($v_query,$ds_conn);

$dd_partid = '<select name="partid"><option value="">Select an option</option>';
for ($rec = 0; $rec < $results_nrows; $rec++){
	$dd_partid .= '<option value="'.$results["COL8"][$rec].'">'.$results["COL8"][$rec].'</option>';
}
$dd_partid .= '</select>';

$dd_patpop="";
//$dd_patpop = '<select name="patpop"><option value="">Select an option</option>';
$dd_patpop .= '<option value="1">All ICD Patients</option>';
$dd_patpop .= '<option value="2">Only CMS primary prevention ICD patients</option>';
//$dd_patpop .= '</select>';


?>
<form name="sts_acs" action="sts_acs_harvest.php" method="POST" onSubmit="return f_validate();">
<p fontsize="12"><b><u>STS Adult Cardiac Surgery Harvest</u></b></p>
<table>
<tr><td>Participant ID:</td><td><input type="text" name="partid" size="6" maxlength="6"></td></tr>
<tr><td>Select an Option:</td><td> <input type="radio" name="optional" value="1">With Optional Fields</input> 
<input type="radio" name="optional" value="0">Without Optional Fields</input>	</td></tr>
<tr><td>From Date:</td><td><input type="text" name="fromdate" size="10" maxlength="10">(mm/dd/yyyy)</td></tr>
<tr><td>To Date:</td><td><input type="text" name="todate" size="10" maxlength="10">(mm/dd/yyyy)</td></tr>
<tr><td><input type="submit" name="submit" value="Submit"/></td></tr>
</table>
</form>

<?PHP } else { 

$v_partid = $_POST["partid"];
$v_optional  = $_POST["optional"];
$v_fromdate  = $_POST["fromdate"];
$v_todate  = $_POST["todate"];

$v_filename = $v_partid."adt.DAT";
$v_query = "select pkg_stsacs.F_Sts_Adult_Harvest(537, $v_partid, '$v_fromdate', '$v_todate', $v_optional) as hardata from dual";
$results = executeOCIQuery($v_query,$ds_conn);

If (substr($results["HARDATA"][0],0,5) != 'Error'){

	$handle = fopen("./sts_harvest/".$v_filename,'w');
	fwrite($handle,$results["HARDATA"][0]);
	fclose($handle);

	$lines = file("./sts_harvest/".$v_filename);
	$num_lines = count($lines) - 1;

	$handle = fopen("./sts_harvest/harvestreport.txt",'w');
	fwrite($handle,"Participant ID = ".$v_partid."\r\n" );
	fwrite($handle,"From_Date = ".$v_fromdate."\r\n" );
	fwrite($handle,"To_Date = ".$v_todate."\r\n" );
	fwrite($handle,"Number of surgeries harvested = ".$num_lines."\r\n" );
	fclose($handle);
	echo "<BR>Harvest Completed Successfully!";
	
	$url = "./sts_acs_harvest_view.php";
	echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";

} else {
	echo "<BR>".$results["HARDATA"][0]."<BR>";

}


} 
	
OCICommit($ds_conn);
OCILogoff($ds_conn);
?>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
