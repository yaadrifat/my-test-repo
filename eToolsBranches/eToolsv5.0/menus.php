<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.
// Check that we are logged in and an admin
if(@$_SESSION["user"]){
	$_SESSION["MODULE"] = $_GET["module"];
	$_SESSION["PAGE"] = $_GET["page"];
	if ((isset($_SESSION["DATASOURCE"])|| ($_SESSION["MODULE"] == 'logout'))){
		if (!empty($_SESSION["PAGE"])) {
			header("location: ".$_SESSION["PAGE"]);
		} else {
			header("location: etools.php?module=".$_SESSION["MODULE"]);
		}
	} else {
		header("location: datasource.php");
		$_SESSION["MODULE"] = "";
	}

}
else header("location: index.php?fail=1");
?>
