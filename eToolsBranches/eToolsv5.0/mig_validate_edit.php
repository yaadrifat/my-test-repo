<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Validate Edit</title>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>

</head>
<body>
<div id="fedora-content">	
<div class="navigate">Migrate - Validate - Edit</div>
<?php


if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 

$v_pk_adapmod = $_GET["pk_vlnk_adapmod"];
$v_colname = $_GET["colname"];
$v_pk_vlnk_pipe = $_GET["pk_vlnk_pipe"];
$v_pk_vlnk_adaptor = $_GET["pk_vlnk_adaptor"];

$query = "SELECT fk_vlnk_modcol,column_name,pipe_colname FROM velink.vlnk_adapmodcol,velink.vlnk_modcol WHERE pk_vlnk_modcol = fk_vlnk_modcol AND map_col_name IS NOT NULL AND fk_vlnk_adapmod = ".$v_pk_adapmod;
$results = executeOCIQuery($query,$ds_conn);
$v_colstr = "";
$trows = $results_nrows;
for ($rec = 0; $rec < $trows; $rec++){
	$v_colstr .= $results["PIPE_COLNAME"][$rec].",";
}
$v_colstr = substr($v_colstr,0,strlen($v_colstr)-1);

$query = "SELECT ".$v_colstr." from velink.vlnk_pipe WHERE pk_vlnk_pipe = ".$v_pk_vlnk_pipe;
$results1 = executeOCIQuery($query,$ds_conn);
?>
	
<FORM action="mig_validate_edit.php" method=post>
<BR>
<TABLE>
<TR>
<input type = "hidden" name="pk_adapmod" value=<?php echo $v_pk_adapmod; ?>>
<input type = "hidden" name="pk_vlnk_adaptor" value=<?php echo $v_pk_vlnk_adaptor; ?>>
<input type = "hidden" name="pk_vlnk_pipe" value=<?php echo $v_pk_vlnk_pipe; ?>>
</tr>
<?php

for ($rec = 0; $rec < $trows; $rec++){
	$v_pipe_colname = $results["PIPE_COLNAME"][$rec];
	if ($v_pipe_colname == $v_colname){
		echo '<TR bgcolor="CEE2F5">';
	} else {
		echo "<TR>";
	}
	echo "<TD>".$results["COLUMN_NAME"][$rec]."</TD>";
	echo '<TD><INPUT size="50" type="text" name="colval['.$rec.']" value="'.$results1["$v_pipe_colname"][0].'"></TD>';
	echo '<input type = "hidden" name="cols['.$rec.']" value="'.$results["PIPE_COLNAME"][$rec].'">';
	echo "</TR>";
}

?>

<TR>
<td>
<input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /> 
</td></tr>
</form>

<?php 
} else {
$v_vlnk_pipe = $_POST["pk_vlnk_pipe"];
//$pk_adapmod_rt = $_POST["pk_adapmod"];
$v_cols = $_POST["cols"];
$v_colval = $_POST["colval"];
$v_pk_vlnk_adaptor = $_POST["pk_vlnk_adaptor"];
$v_sqlstr = "";
for ($i = 0; $i < count($v_cols); $i++){
	$v_sqlstr .= $v_cols[$i]."='".$v_colval[$i]."',";
}
$v_sqlstr = "update velink.vlnk_pipe set ".substr($v_sqlstr,0,strlen($v_sqlstr)-1)." where pk_vlnk_pipe = ".$v_vlnk_pipe;
$results = executeOCIUpdateQuery($v_sqlstr,$ds_conn);
OCICommit($ds_conn);
OCILogoff($ds_conn);
echo "Data Saved Successfully !!!";
$url = "./mig_validate.php?pk_vlnk_adapmod=".$_POST["pk_adapmod"]."&pk_vlnk_adaptor=".$v_pk_vlnk_adaptor;
//$url = "./mig_validate.php?pk_vlnk_adapmod=".$pk_adapmod_rt."&pk_vlnk_adaptor=".$v_pk_vlnk_adaptor;
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";

?>
<?PHP
}

?>
</div>
</body>
</html>


<?php
}
else header("location: index.php?fail=1");
?>
		
