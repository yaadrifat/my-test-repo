<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Groups</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");
include ("./txt-db-api.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 



?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Manage Account - Groups</div>
<?PHP
echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=account_groups_edit.php?mode=n&pk_groups=0>New Group</a>";

$db = new Database("etools");
$rs = $db->executeQuery("select PK_GROUPS,GROUP_NAME, GROUP_DESC from ET_GROUPS");

//$rs = $db->executeQuery($ac_grp);


?>
<Table border="1" width="100%"><TR>
<TH>NAME</TH>
<TH>DESCRIPTION</TH>
<TH>&nbsp;</TH>
</TR>
<?php


$v_counter = 0;
while ($rs->next()) {
	$data = $rs->getCurrentValuesAsHash();
?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
	<TD><?php echo $data["GROUP_NAME"] ?></TD>
	<TD><?php echo $data["GROUP_DESC"] ?></TD>
	<TD><?php 
		if ($data["PK_GROUPS"] != 1) echo '<a href=account_groups_edit.php?mode=m&pk_groups='.$data["PK_GROUPS"].'> Edit</a> ';

		?>
	</TD>
	<?php
	$v_counter++;
}
?>
</TABLE>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
