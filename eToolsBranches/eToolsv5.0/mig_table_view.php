<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Data</title>
</head>
<?php
include("./includes/header.php");

include_once "./adodb/adodb.inc.php";
include_once('./adodb/adodb-pager.inc.php'); 

$db = NewADOConnection("oci8");
$connect_string = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST='.$_SESSION["DS_HOST"].')(PORT='.$_SESSION["DS_PORT"].'))(CONNECT_DATA=(SID='.$_SESSION["DS_SID"].')))';
$db->Connect($connect_string, "eres", $_SESSION["DS_PASS"]);


if (!$db) die("Connection failed");

	
?>
<body>
<div id="fedora-content">	

<?PHP 
if (isset($_GET["sql"])){
	$v_sql = urldecode($_GET["sql"]);
	} else if (isset($_GET["pk_vlnk_adapmod"])) {
	$v_pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];
	$v_table = $_GET["table"];
	$v_query = "select pipe_colname, column_name from velink.vlnk_adapmodcol, velink.vlnk_modcol where pk_vlnk_modcol = fk_vlnk_modcol and map_colname is not null and fk_vlnk_adapmod = ".$v_pk_vlnk_adapmod;
	$rs = $db->Execute($v_query);
	$v_sql = "";
	$v_cols = "";
	while (!$rs->EOF) {
		$v_sql .= $rs->fields["PIPE_COLNAME"].' as "'.substr($rs->fields["COLUMN_NAME"],0,28).(strlen($rs->fields["COLUMN_NAME"]) > 28?"..":"").'",';
		$rs->MoveNext();
	}
	if ($v_table == 'VLNK_PIPE') {
		$v_sql = substr($v_sql,0,strlen($v_sql)-1);
		if (isset($_GET["flag"])) {
			if ($_GET["flag"] == 0) {
				$v_sql = "select ".$v_sql." from velink.vlnk_pipe where fk_vlnk_adapmod = ".$v_pk_vlnk_adapmod;
				$v_sql .= " and (process_flag = 0 or  process_flag is null)";
			} elseif ($_GET["flag"] == -1) {
				$v_sql = "select (SELECT message from velink.vlnk_log where fk_vlnk_pipe = pk_vlnk_pipe and pk_vlnk_log = (select max(pk_vlnk_log) from velink.vlnk_log where fk_vlnk_pipe = pk_vlnk_pipe group by fk_vlnk_pipe)) as error_message,".$v_sql." from velink.vlnk_pipe where fk_vlnk_adapmod = ".$v_pk_vlnk_adapmod;
				$v_sql .= " and process_flag = -1";
			} else {
				$v_sql = "select ".$v_sql." from velink.vlnk_pipe where fk_vlnk_adapmod = ".$v_pk_vlnk_adapmod;
				$v_sql .= (isset($_GET["flag"])) ? " and process_flag = ".$_GET["flag"] : "";
			}
		} else {
			$v_sql = "select ".$v_sql." from velink.vlnk_pipe where fk_vlnk_adapmod = ".$v_pk_vlnk_adapmod;
		}
	} else {
		$v_sql = "select * from $v_table";
	}

	$_SESSION["sql"] = $v_sql;
} else {
	$v_sql = $_SESSION["sql"] ;
}
?>
<style>DIV.tablerows {
height:500px;
width:800px;
overflow: auto;
align:left;
}</style>

<div>
<?PHP

$rs = $db->Execute($v_sql);
if (!$rs) {
	echo "SQL ERROR: ";
	print $db->ErrorMsg();
} else {
$pager = new ADODB_Pager($db,$v_sql);
$pager->htmlSpecialChars = false;
echo "<table width='100%'><tr><td width='50%'><b>Total Rows: ".$rs->RecordCount()."</b></td>";
echo '<td width="50%"><a href="export2csv.php?data='.urlencode($v_sql).'">Export to CSV</a></td></tr></table>';
echo "<table border='1'><tr><td>";
echo "<div class='tablerows'>";
$pager->Render($rows_per_page=25); 
echo "</div>";
echo "</td></tr></table>";
}

	
?>



</div>


</body>
</html>
<?php 
$db->Close();

}
else header("location: index.php?fail=1");
?>
