<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Data</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Migrate Data - Standard - Edit Mapping</div>

<?php
$pk_vlnk_pipemap = $_POST["pk_vlnk_pipemap"];
$target_value = $_POST["target_value"];
$target_id = $_POST["target_id"];


for($i=0;$i<count($pk_vlnk_pipemap);$i++){
	if ($target_id[$i] == 0 && $target_value[$i] != "") {
		$v_sql = "update velink.vlnk_pipemap set target_value = '".urldecode($target_value[$i])."' where pk_vlnk_pipemap = ".$pk_vlnk_pipemap[$i];
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);
	}
}
echo "Mapping Saved...<BR>";

$pk_vlnk_adapmod = $_POST["pk_vlnk_adapmod"];

$query_sql = "begin velink.pkg_velink.sp_generate_map(".$pk_vlnk_adapmod."); end;";
$results = executeOCIUpdateQuery($query_sql,$ds_conn);

echo "Mapping Generated...<BR>";

$url = "mig_datamapping.php?pk_vlnk_adapmod=".$pk_vlnk_adapmod."&refresh=0";

echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";

?>


      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
