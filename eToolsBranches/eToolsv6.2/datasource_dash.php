<?php
session_start();
header("Cache-control: private");
if(@$_SESSION["user"]){
?>

<html>
    <head>
        <title>Velos eTools -&gt; Data Source</title>
        <?php
        include("db_config.php");
        include("./includes/oci_functions.php");        
        include("./includes/header.php");
        require_once('audit_queries.php');
        ?>
        <script src="./js/jquery-1.10.2.js"></script>
        <script>
            function conalert(pkds,grp){
                //alert("pkds= "+pkds+", grp= "+grp);
                if(grp==0){
                    $.ajax({
                        url:  "datasource_dash_ajax.php",
                        type: "post",
                        data: {'pkds':pkds},
                        async:true,
                        success: function() {
                            //alert('went back');
                        }
                    });
                }else{
                    if(confirm("This datasource is associated with a group..!")){
                        $.ajax({                                
                            url:  "datasource_dash_ajax.php",
                            type: "post",
                            data: {'pkds':pkds},
                            async:true,
                            success: function() {
                                //alert('deleted');
                            }
                        });                       
                    }                        
                }
            }
        </script>
    </head>
<body>
    
    <div id="fedora-content">	
    <div class="navigate">Data Sources</div>
    <?PHP
        echo "</br>";
        echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        echo "<a href=datasource_edit.php?mode=n>Add Data Source</a>";
        echo "</br>";
        echo "&nbsp;"; ?>
        <Table border="1" width="100%"><TR>
            <TH width="3%">SNO</TH>
            <TH width="24%">DS Name </TH>
            <TH width="12%">DS Host </TH>
            <TH width="8%">DS Port </TH>
            <TH width="15%">DS SID </TH>
            <TH width="10%">DS Status </TH>		
            <TH width="6%">&nbsp;</TH>
            <TH width="10%">&nbsp;</TH>		
            <TH width="6%">&nbsp;</TH>
        </TR>		
    <?php        
        $dsRights = '';
        if($_SESSION["FK_GROUPS"] == 1){            
            //$records = mysql_query("SELECT ds_rights FROM et_groups where pk_groups=1");
            $records = "SELECT ds_rights FROM et_groups where pk_groups = 1";
            $records = $db->query($records);
            //if(mysql_num_rows($records)>0){
            if($records->num_rows>0){
                //while($row = mysql_fetch_array($records)){
                while($row = $records->fetch_assoc()){
                    if(substr($row['ds_rights'],0,1) == "|"){
                        $dsString = substr($row['ds_rights'],1,strlen($row['ds_rights']));
                    }else{
                        $dsString = $row['ds_rights'];
                    }
                    $dsRights = explode("|",$dsString);
                }
            }
        }else{
            //$recordsot = mysql_query("SELECT ds_rights FROM et_groups where pk_groups = ".$_SESSION["FK_GROUPS"]);
            $recordsot = "SELECT ds_rights FROM et_groups where pk_groups = ".$_SESSION["FK_GROUPS"];
            $recordsot = $db->query($recordsot);
            //if(mysql_num_rows($recordsot) > 0){
            if($recordsot->num_rows > 0){
                //while($rowot = mysql_fetch_array($recordsot
                while($rowot = $recordsot->fetch_assoc()){                    
                    if(substr($rowot['ds_rights'],0,1) == "|"){
                        $dsString = substr($rowot['ds_rights'],1,strlen($rowot['ds_rights']));
                    }
                    //echo '<pre>'.print_r($dsRights).'</pre>';
                    $dsRights = explode("|",$rowot['ds_rights']);                    
                }
            }
        }
    $v_counter = 0;    
    //$dsda_rs = mysql_query("SELECT * FROM et_groups where pk_groups = ".$_SESSION["FK_GROUPS"]);
    $dsda_rs = "SELECT * FROM et_groups where pk_groups = ".$_SESSION["FK_GROUPS"];
    $dsda_rs = $db->query($dsda_rs);
    //$v_sno = mysql_num_rows($dsda_rs);
    $v_sno = $dsda_rs->num_rows;
    $v_adsno =1;
    $v_usno =1;
    $dsRights = array_filter($dsRights);
    $dsRights = array_values($dsRights);
    for($i=0; $i<sizeof($dsRights); $i++){
        list($pk_ds, $ds)= explode(":",$dsRights[$v_counter]);
        //$dsQry = mysql_query("SELECT * from et_ds where pk_ds = ".$pk_ds);
        $dsQry = "SELECT * from et_ds where pk_ds = ".$pk_ds;
        $dsQry = $db->query($dsQry);
        //$dsQry_rs = mysql_fetch_array($dsQry);
        $dsQry_rs = $dsQry->fetch_assoc();        
        if($_SESSION["PK_USERS"] == 1){ ?> 
            <TR bgcolor="#FFFFFF" onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
            <TD><?php echo $v_adsno ?></TD>
            <TD><?php echo $dsQry_rs["ds_name"] ?></TD>
            <TD><?php echo $dsQry_rs["ds_host"]?></TD>
            <TD><?php echo $dsQry_rs["ds_port"] ?></TD>
            <TD><?php echo $dsQry_rs["ds_sid"] ?></TD>
            <TD><?php 
                if($ds == 1){
                    echo "Enabled";                     
                }else{ 
                    echo "Disabled";                    
                }?></TD>			
            <TD><?PHP echo '<a href=datasource_edit.php?mode=m&pk_ds='.$dsQry_rs["pk_ds"].'>Edit</a>'; ?></TD>
            <TD><?PHP echo '<a href=datasource_edit.php?mode=c&pk_ds='.$dsQry_rs["pk_ds"].'>Change Password</a>'; ?></TD>
            <TD><?PHP echo '<a href=datasource_dash.php?mode=d&pk_ds='.$dsQry_rs["pk_ds"].'>Delete</a>'; ?></TD>			
            <input type='hidden' name='dsst' value=<?PHP echo $ds; ?>>			
            </TR>			
            <?php            
            $v_adsno++;
        }else{
            if($ds == 1){
            ?>
            <TR bgcolor="#FFFFFF" onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
            <TD><?php echo $v_usno ?></TD>
            <TD><?php echo $dsQry_rs["ds_name"] ?></TD>
            <TD><?php echo $dsQry_rs["ds_host"]?></TD>
            <TD><?php echo $dsQry_rs["ds_port"] ?></TD>
            <TD><?php echo $dsQry_rs["ds_sid"] ?></TD>
            <TD><?php 
                if($ds == 1){
                    echo "Enabled";                     
                }else{ 
                    echo "Disabled";                    
                }?></TD>            
            <?php
                if($dsQry_rs["fk_user"] == $_SESSION["PK_USERS"]){ ?>
                    <TD><?PHP echo '<a href=datasource_edit.php?mode=m&pk_ds='.$dsQry_rs["pk_ds"].'>Edit</a>'; ?></TD>
                    <TD><?PHP echo '<a href=datasource_edit.php?mode=c&pk_ds='.$dsQry_rs["pk_ds"].'>Change Password</a>'; ?></TD>
                    <TD><?PHP echo '<a href=datasource_dash.php?mode=d&pk_ds='.$dsQry_rs["pk_ds"].'>Delete</a>'; ?></TD>			                    
            <?php    }else{?>
                    <TD><?PHP echo '<a href=#></a>'; ?></TD>
                    <TD><?PHP echo '<a href=#> </a>'; ?></TD>
                    <TD><?PHP echo '<a href=#></a>'; ?></TD>			                    
            <?php    }  ?>            
            <input type='hidden' name='dsst' value=<?PHP echo $ds; ?>>			
            </TR>                        
            <?php  $v_usno++;
            }else{
                //$dsQry1 = mysql_query("SELECT * from et_ds where pk_ds = ".$pk_ds." and fk_user = ".$_SESSION["PK_USERS"]);
                $dsQry1 = "SELECT * from et_ds where pk_ds = ".$pk_ds." and fk_user = ".$_SESSION["PK_USERS"];
                $dsQry1 = $db->query($dsQry1);
                //$dsQry_rs1 = mysql_fetch_array($dsQry1);                 
                $dsQry_rs1 = $dsQry1->fetch_assoc();                 
                //if(mysql_num_rows($dsQry1) >= 1){
                if($dsQry1->num_rows >= 1){
                 
            ?>
            <TR bgcolor="#FFFFFF" onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
            <TD><?php echo $v_usno ?></TD>
            <TD><?php echo $dsQry_rs["ds_name"] ?></TD>
            <TD><?php echo $dsQry_rs["ds_host"]?></TD>
            <TD><?php echo $dsQry_rs["ds_port"] ?></TD>
            <TD><?php echo $dsQry_rs["ds_sid"] ?></TD>
            <TD><?php 
                if($ds == 1){
                    echo "Enabled";                     
                }else{ 
                    echo "Disabled";                    
                }?></TD>
            
            <?php
                if($dsQry_rs["fk_user"] == $_SESSION["PK_USERS"]){ ?>
                    <TD><?PHP echo '<a href=datasource_edit.php?mode=m&pk_ds='.$dsQry_rs["pk_ds"].'>Edit</a>'; ?></TD>
                    <TD><?PHP echo '<a href=datasource_edit.php?mode=c&pk_ds='.$dsQry_rs["pk_ds"].'>Change Password</a>'; ?></TD>
                    <TD><?PHP echo '<a href=datasource_dash.php?mode=d&pk_ds='.$dsQry_rs["pk_ds"].'>Delete</a>'; ?></TD>
            <?php    }else{?>
                    <TD><?PHP echo '<a href=#></a>'; ?></TD>
                    <TD><?PHP echo '<a href=#> </a>'; ?></TD>
                    <TD><?PHP echo '<a href=#></a>'; ?></TD>			                    
            <?php    }  ?>            
            <input type='hidden' name='dsst' value=<?PHP echo $ds; ?>>			
            </TR>                        
            <?php  $v_usno++;                    
                    
                }                
            }
        }        
        $v_counter++;
    }    
?>          
    <?php
        if (!($_SERVER['REQUEST_METHOD'] == 'POST')) {

            $v_mode = $_GET["mode"];
            $pk_num = $_GET["pk_ds"];
            /*--- AUDIT ---*/
            //$grpName = mysql_query("SELECT group_name from et_groups WHERE pk_groups =".$_SESSION["FK_GROUPS"]);
            $grpName = "SELECT group_name from et_groups WHERE pk_groups =".$_SESSION["FK_GROUPS"];
            $grpName = $db->query($grpName);
            //$grpNameVal = mysql_fetch_array($grpName);
            $grpNameVal = $grpName->fetch_assoc();
            /*--- END ---*/

            if($v_mode == "d"){                
                if($_SESSION["DS"] != $pk_num ){
                    //$grpName1 = mysql_query("SELECT ds_rights from et_groups where pk_groups <> 1");
                    $grpName1 = "SELECT ds_rights from et_groups where pk_groups <> 1";
                    $grpName1 = $db->query($grpName1);
                    
                    //while($grpName1rs =  mysql_fetch_array($grpName1)){
                    while($grpName1rs =  $grpName1->fetch_assoc()){
                        $ds_rights1 = explode("|",$grpName1rs['ds_rights']);                        
                        foreach($ds_rights1 as $v_ds){
                            $tmpStr = explode(":",$v_ds);
//                            echo $tmpStr[0].' = '.$pk_num.' && '.$tmpStr[1].' == 1 <br>';                            
                            if($tmpStr[0]==$pk_num && $tmpStr[1] == 1){                                
                                $grpn = 1;                                
                            }
                        }
                    }

                    /*--- AUDIT ---*/
                    //$grpName = mysql_query("SELECT group_name from et_groups WHERE pk_groups =".$_SESSION["FK_GROUPS"]);
                    $grpName = "SELECT group_name from et_groups WHERE pk_groups =".$_SESSION["FK_GROUPS"];
                    $grpName = $db->query($grpName);
                    //$grpNameVal = mysql_fetch_array($grpName);
                    $grpNameVal = $grpName->fetch_assoc();
                    /*--- END ---*/                    
                    if($grpn == 1){ //-- when it is associated
                        echo "<script>conalert(".$pk_num.','.$grpn.");</script>";
                        echo '<meta http-equiv="refresh" content="0; url=./datasource_dash.php">';
                        
                        /*--- AUDIT ---*/
                        $rowInfo = array(2,$_SESSION["user"],$grpNameVal["group_name"], "SYSDATE", "SYSDATE", "U");
                        auditQuery(2,$rowInfo);
                        /*--- END ---*/

                        /*--- AUDIT ---*/
                        $diffName = array('DS_RIGHTS');
                        $diffValue = array($rightsStr);
                        $tblname = 'et_groups';
                        colQueries(array_diff($diffName, array('')), array_diff($diffValue, array('')), $tblname, array_diff($data, array('')),'noclob');
                        /*--- END ---*/  



                        //$deleteRs = mysql_query("DELETE from et_ds where pk_ds=".$pk_num);

                        /*--- AUDIT ---*/
                        $rowInfo = array(2,$_SESSION["user"],$grpNameVal["group_name"], "SYSDATE", "SYSDATE", "D");
                        auditQuery(2,$rowInfo);
                        /*--- END ---*/            
                    }else{ //-- when it is not associated                        
                        $grpn = 0;
                        echo "<script>conalert(".$pk_num.','.$grpn.");</script>";
                        echo '<meta http-equiv="refresh" content="0; url=./datasource_dash.php">';
                        
                        /*--- AUDIT ---*/
                        $rowInfo = array(2,$_SESSION["user"],$grpNameVal["group_name"], "SYSDATE", "SYSDATE", "U");
                        auditQuery(2,$rowInfo);
                        /*--- END ---*/

                        /*--- AUDIT ---*/
                        $diffName = array('DS_RIGHTS');
                        $diffValue = array($rightsStr);
                        $tblname = 'et_groups';
                        colQueries(array_diff($diffName, array('')), array_diff($diffValue, array('')), $tblname, array_diff($data, array('')),'','noclob');
                        /*--- END ---*/  



                        //$deleteRs = mysql_query("DELETE from et_ds where pk_ds=".$pk_num);

                        /*--- AUDIT ---*/
                        $rowInfo = array(2,$_SESSION["user"],$grpNameVal["group_name"], "SYSDATE", "SYSDATE", "D");
                        auditQuery(2,$rowInfo);
                        /*--- END ---*/                           
                        
                    }                    
                }else{
                    echo '<script>alert("Connected datasource cannot be deleted");</script>';
                }
                echo '<meta http-equiv="refresh" content="0; url=./datasource_dash.php">';
            }
        }
    ?>
    </TABLE>
    </div>            
            
            
            
            
            
            
</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
