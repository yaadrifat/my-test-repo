<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>


<?php
include_once "./adodb/adodb.inc.php";

$v_pk_formlib = $_GET["calpk"];

$db = NewADOConnection("oci8");
$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);

$v_sql = "select x.txml.getclobval() from (
select xmltransform(sys.XMLTYPE.createXML(repxml),sys.XMLTYPE.createXML(repxsl)) as txml from (
select 
(select F_GETXML('
SELECT
PROT_NAME,
PROT_DESC,
PROT_DURATION,
VISIT_NAME,
VISIT_DESC,
VISIT_INTERVAL,
EVENT_NAME,
EVENT_DESC,
DECODE(trim(vwindow_bef),''(BEFORE)'','''',vwindow_bef) AS vwindow_bef,
DECODE(trim(vwindow_aft),''(AFTER)'','''',vwindow_aft) AS vwindow_aft,
EVENT_COST ,
RESOURCES,
APPENDIX,
MESSAGES,
FORMS,insert_after
FROM  erv_calendar_template
WHERE prot_id = $v_pk_formlib
ORDER BY displacement')
from dual) as repxml,
(select repxsl from er_repxsl where fk_report = 106) as repxsl
from dual)) x";

$recordSet = $db->Execute($v_sql);

if (!$recordSet) 
	{print $db->ErrorMsg();}
else {
	$v_html =  $recordSet->fields[0];
	// For IE rendering issue
	if ($recordSet->fields[0] != '') {
		$v_html = str_replace('<TITLE/>','<TITLE>Calendar Preview</TITLE>',$v_html);
	} else {
		$v_html = '<b style="color:red; font-family:Arial, Helvetica, sans-serif; font-size:13px;">Calendar Preview is not available</b>';
	}
	print $v_html;
}
$db->Close();
}
else header("location: ./index.php?fail=1");
?>
