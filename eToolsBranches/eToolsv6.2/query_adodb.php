<?php
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>
<html>
<head>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">

    <title>Velos eTools -> Modify Query</title>
      <script language="javascript" type="text/javascript">

	  
      var sel_table;
	  function getHTTPObject(){
      if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
      else if (window.XMLHttpRequest) return new XMLHttpRequest();
      else {
      alert("Your browser does not support AJAX.");
      return null;
      }
      }

function refreshcol(tablename,action){
    document.getElementById('tblName').innerHTML = 'Table Name: '+tablename;
    sel_table=tablename;
    httpObject = getHTTPObject();
    if (httpObject != null) {
        httpObject.open("GET", "tables_ajax.php?table="+tablename+"&action="+action, false);
        httpObject.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
        httpObject.setRequestHeader("Cache-Control", "no-cache");
        httpObject.send(null);
        if(httpObject.status != 200){document.write("404 not found");}
              var finalString = httpObject.responseText;
              if (finalString != "") {
                      if (action == 'CLIPBOARD') {
                              document.getElementById('clipboard').value = finalString.replace(/^\s+/,"");
                      } else {
                              document.getElementById('columns').innerHTML = finalString;
                      }
              }
    }
}
      var httpObject = null;
      </script>
	  
<script language="javascript" type="text/javascript">
function refreshlist(table){
table=table.toUpperCase()
var length=table.length;
var count = tables.length;
var tlist = "";

for (var i=0; i<count; i++){
    //if (table == tables[i].substring(5,length+5)){
    if (table == tables[i].substring(0,length)){ // for the fix 23804
        tlist = tlist + '<tr onMouseOver="bgColor=\'#a4bef1\'" onMouseOut="bgColor=\'#FFFFFF\'">'+"<td ondblclick=refreshcol(this.innerHTML,'')>" + tables[i] + "</td></tr>";                
    }
}
tlist = (tlist.length == 0) ? tlist="No tables found":"<table border='1'>"+tlist+"</table>";
document.getElementById('tables').innerHTML = tlist;

}

function placeholdervalue(qry){
    
    
    //alert(qry);
    
    var qrystring = qry;
    qrystring = qrystring.split(" ");
    var qrystringArray = new Array();
    for(var i =0; i < qrystring.length; i++){
        qrystringArray.push(qrystring[i]);
        if(i != qrystring.length-1){
            qrystringArray.push(" ");
        }
    }    
    //alert(qrystringArray.length);
    var indices = [];
    for(var j=0; j<qrystringArray.length; j++) {          
          if (qrystringArray[j].substring(0,1) == ":"){
              indices.push(qrystringArray[j]);
          }
    }
    
    if(indices.length != 0){
       // for(var k=0; k<indices.length; k++){
            //var person = prompt("Please enter the '"+indices[k]+"'  value ", "");
            var person = prompt("Please enter the datefilter value ", "");
            if(person != null){
                //document.getElementById('placeval').value = person;            
                //document.cookie = 'plh='+person;
                httpObject = getHTTPObject();
                    if (httpObject != null) {                    
                            httpObject.open("GET", "scheduler_ajax.php?plh="+person, false);
                            httpObject.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
                            httpObject.setRequestHeader("Cache-Control", "no-cache");
                            httpObject.send(null);
                            if(httpObject.status != 200){document.write("404 not found");}                              
                    }            
            }        
        //}
    }
    
    //var n = qry.search(":");
        
//    var n = qry.search(":");
//    if(n!=-1){
//        var person = prompt("Please enter the filter value", "");
//        if(person != null){
//            //document.getElementById('placeval').value = person;            
//            //document.cookie = 'plh='+person;
//            httpObject = getHTTPObject();
//                if (httpObject != null) {                    
//                        httpObject.open("GET", "scheduler_ajax.php?plh="+person, false);
//                        httpObject.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
//                        httpObject.setRequestHeader("Cache-Control", "no-cache");
//                        httpObject.send(null);
//                        if(httpObject.status != 200){document.write("404 not found");}                              
//                }            
//        }
//    }
}

</script>

<script> 
var tables = new Array();
</script>
<style>
textarea
{
  font-size: 10px;
  color: #0000ff;
  font-family: courier
}
</style>
</head>
<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");

include_once "./adodb/adodb.inc.php";
include_once('./adodb/adodb-pager.inc.php'); 
include_once('./adodb/pivottable.inc.php'); 
require_once('audit_queries.php');

$dbora = NewADOConnection("oci8");
$dbora->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
if (!$dbora) die("Connection failed");
    $query="select owner || '.' || table_name as tables from dba_tables where owner in ('ERES','ESCH','EPAT','VDA') 
    union select owner || '.' || view_name as tables from dba_views where owner in ('ERES','ESCH','EPAT','VDA') order by 1";
    $results = $dbora->execute($query);
    $rec = 0;
while (!$results->EOF){
?>
    <script>tables[<?PHP echo $rec; ?>] = <?PHP echo '"'.$results->fields["TABLES"].'"';?>;</script>
<?PHP
    $results->MoveNext();
    $rec++;
}
?>
<body onLoad="refreshlist('');document.qman.query.focus();">
<div id="fedora-content">	
<div class="navigate">Query Manager</div>



<FORM name="qman" action="query_adodb.php" method=post>
    <?php
    
    if (isset($_GET['mode'])) {
        $v_query = ""; 
        $_SESSION['query'] = "";
        $_SESSION['pk'] = "";
    } else {	
        if (isset($_GET['pk_queryman']) ) {
            $v_pk =  $_GET['pk_queryman'];
            $_SESSION['pk'] = $v_pk;
            $recordSet = $dbora->Execute("select query from velink.vlnk_queryman where pk_queryman = ".$v_pk);
            $v_query =  $recordSet->fields[0];
            $_SESSION['query'] = $recordSet->fields[0];
        } else {
            if (isset($_POST['query']) ) {
                $v_query =  stripslashes($_POST['query']);
                $_SESSION['query'] = stripslashes($_POST['query']);
            } else {
                if (isset($_SESSION['query']) ) {
                        $v_query =  stripslashes($_SESSION['query']);
                } else {
                        $v_query =  "";
                        $_SESSION['query'] = "";
                }
            }
        }
    }
    ?>
    <table width="100%" border="0">
        <tr>
            <td colspan="2" ><Textarea name="query" id="query" type="text" cols=65 maxlength=10000 rows=19><?PHP echo $v_query; ?></textarea>
            <style>
                DIV.tables { height:145px;  overflow: auto;  align:left;}
                DIV.tables_1 { height:153px;  overflow: auto;  align:left; }
                .mawidth{width:900px;	overflow-x:scroll;}
            </style>
            </td>
            <input type="hidden" name="subHint" value="subhit111" />
            <td valign="top" align="left" width="100%">
                <div  style="margin-left:20px;">Search: <input type="text" name="stable" size="33" onKeyUp="refreshlist(this.value)"></div>
                <div style="float:left; width:45%;">
                    <div style="margin-left:20px; width:100%;"><table><th style="width:278px; height:25px;">TABLE NAME</th></table></div>
                    <div style="float:left; height:170px; width:100%; margin-right:30px; margin-left:20px;" class="tables" id="tables"></div>
                </div>
                <div  style="float:left; margin-left:20px;  width:50%;">
                    <div style="width:100%;" id="tblName"></div>
                    <div style="width:100%;"><table width="100%"><th style="height:25px; width:50%;">COLUMN NAME</th><th style="height:25px; width:25%;">TYPE</th><th style="height:25px; width:15%;">SIZE</th></table></div>
                    <div style="width:100%;" class="tables_1" id="columns"></div>
                </div>
            </td>
            <td>Column Clipboard:<br><textarea rows=9 cols=30 id='clipboard'></textarea></td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="hidden" name="save" value="" />
                <input type="image" name="submit" value="Submit" src="./img/equery.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/equery_m.png';" onMouseOut="this.src='./img/equery.png';" onclick="return placeholdervalue(document.getElementById('query').value);" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <input type="image" src="./img/squery.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/squery_m.png';" onMouseOut="this.src='./img/squery.png';" onClick="document.qman.save.value='Yes'; placeholdervalue(document.getElementById('query').value);"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="image" name="copy" value="Copy to Clipboard" src="./img/copyclip.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/copyclip_m.png';" onMouseOut="this.src='./img/copyclip.png';" onClick="refreshcol(sel_table,'CLIPBOARD'); return false;" />
                &nbsp;
                
                <input type="image" name="copy" value="Clear Clipboard" src="./img/clearclip.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/clearclip_m.png';" onMouseOut="this.src='./img/clearclip.png';" onClick="document.getElementById('clipboard').value = ''; return false;" />
            </td>
            
        </tr>
    </table>    
</form>

<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;">
    <table summary="Note: Note">
    <tr><th align="left">Note</th></tr>
    <tr><td align="left" valign="top">
        <p>Table name should be preceding with schema name. i.e. eres.er_user for any custom query. Scheduler will fail if there is no schema name preceding with table name.</p>                
    </td></tr>
    </table>
</div>

<!-- -------------------------------------------------------------------------------- page submitting code -------------------------------------------------- -->
<?php   

if ($_SERVER['REQUEST_METHOD'] == 'POST'){ 
    
    if (isset($_POST['graph'])) {
        $rs = $dbora->Execute($v_query);
        $field_count = $rs->FieldCount();
        $dd_fldname = '<option value="" selected>Select an option</option>';
        for ($i=0;$i<$field_count;$i++){
                $field = $rs->FetchField($i);
                $field_name = $field->name;
                $dd_fldname .= "<option value=".$field_name.">".$field_name."</option>";
        }	
        echo '<FORM action="query_graph.php" method="post">';
        echo '<INPUT type="hidden" name="query" value="'.$_SESSION['query'].'"></INPUT>';
        echo "<TABLE>";
        echo "<TR>";
        echo "<TD>Graph Title: </TD>";
        echo "<TD><INPUT type=text name=gtitle size=50></INPUT></TD>";
        echo "</TR><TR>";
        echo "<TD>X Axis: </TD>";
        echo "<TD><select name=xaxis>".$dd_fldname."</select>";
        echo "</TR>";
        echo "<TD>Y Axis: </TD>";
        echo "<TD><select name=yaxis>".$dd_fldname."</select>";
        echo "</TR>";
        echo "<TR><TD>&nbsp;</TD></TR>";
        echo "</TABLE>";
        Echo '<input type="submit" name="submit" value="Submit"></input>';
        echo '</FORM>';
    }
    
    //------------------------------------------------------- above code is not used -----------------------------------------------------//

if (isset($_POST['save'])) {    
    if ($_POST['save'] == 'Yes') {
        if (empty($_SESSION["pk"])) {
            $v_qname =  "";
            $v_qdesc =  "";
        } else {
            $recordSet = $dbora->Execute("select query_name,query_desc from velink.vlnk_queryman where pk_queryman = ".$_SESSION["pk"]);
            $v_qname =  $recordSet->fields[0];
            $v_qdesc =  $recordSet->fields[1];
        }
        echo '<FORM action="query_save.php" method="post">';
        echo '<INPUT type="hidden" name="pk" value="'.$_SESSION["pk"].'"></INPUT>';
        echo '<INPUT type="hidden" name="query" value="'.htmlentities(stripslashes($_SESSION['query'])).'"></INPUT>';
        echo '<INPUT type="hidden" name="queryfil" value="'.$_SESSION["plh"].'"></INPUT>';
        echo "<TABLE>";
        echo "<TR>";
        echo "<TD>Query Name: </TD>";
        echo '<TD><INPUT type=text name=queryname size=50 value="'.$v_qname.'"></INPUT></TD>';
        echo "</TR><TR>";
        echo "<TD>Query Description: </TD>";
        echo '<TD><INPUT type=text name=querydesc size=80 value="'.$v_qdesc.'"></INPUT></TD>';
        echo "</TR>";
        echo "<TR><TD>&nbsp;</TD></TR>";
        echo "</TABLE>";        
?>
        
        <input type="image" name="submit" value="Submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
<?PHP
        echo '</FORM>';
    }
}

$tquery=$_SESSION['query'];
$subhint = $_POST['subHint'];
$pkQueryman = $_GET['pk_queryman'];
$queryname = $_POST['queryname'];
//$newvalue = $_COOKIE['plh'];
//unset($_COOKIE['plh']);
//brings here for execute query


if (!isset($_POST['submit']) && strtoupper(substr(trim($tquery),0,6)) == ''){
    echo '<table><tr><td>';
    echo "<div>";
    $pager->Render($rows_per_page=10); 
    echo "</div>";
    echo "</td</tr></table>";
}

if(isset($_POST['submit']) || strlen($_SESSION['query']) > 0){
    if(strtoupper(substr(trim($tquery),0,6)) == 'SELECT'){
        //---- for custom notification
        $qryexplod = explode(" ", $tquery);
        $qryexplodrs = [];
        for($qe=0; $qe<sizeof($qryexplod); $qe++){
            if(substr($qryexplod[$qe],0,1) == ':'){
                $qryexplodrs[$qe] = $qryexplod[$qe];
            }
        }
        $qryexplodrs = array_values($qryexplodrs);
        
        //$qryfilter = explode(",",$_SESSION["plh"]);
        $qryfilter = $_SESSION["plh"];
        $_SESSION["plh"] = '';
        if(!empty($qryexplodrs)){
            $trimmedqry = $tquery;
            for($qel=0; $qel<sizeof($qryexplodrs); $qel++){
                //$datestring = "to_date('".$qryfilter[$qel]."', 'dd-mm-yy')";
                $datestring = "to_date('".$qryfilter."', 'dd-mm-yy')";
                $trimmedqry = str_replace($qryexplodrs[$qel], $datestring, $trimmedqry);
            }
            $tquery = $trimmedqry;        
        }
        
        
//        if(!empty($_SESSION["plh"])){
//            $pos = strpos($tquery, ":");
//            if(isset($pos)){
//                $trimmedqry = substr($tquery,0,$pos);
//                if(substr($_SESSION["plh"],2,1)=='-'){
//                    $tquery =  $trimmedqry."to_date('".$_SESSION["plh"]."','dd-mm-yy')";
//                }else{
//                    $tquery =  $trimmedqry."'".$_SESSION["plh"]."'";
//                }
//            }else{
//                $tquery = $tquery;
//            }
//            $_SESSION["plh"] = '';
//        }
        
        $rs = $dbora->Execute($tquery);        
        //$fC = $rs->FieldCount();
        $fC = $rs->fields[0];
        
        
        if(!$rs){
            echo "SQL ERROR: ";
            print $dbora->ErrorMsg();
            $_SESSION["plh"] = '';
            executeQuery(5, $tquery);
        }else{
            executeQuery(5, $tquery);
            $pager = new ADODB_Pager($dbora,"select * from ($tquery) where rownum < 101");
            $pager->htmlSpecialChars = false;
            if($fC > 8){
                echo '<table><tr><th>';
                echo "<div class='mawidth'>";
                $pager->Render($rows_per_page=10); 
                echo "</div>";
                echo "</th></tr></table>";
            }else{
                echo '<table><tr><th>';
                echo "<div>";
                $pager->Render($rows_per_page=12); 
                echo "</div>";
                echo "</th></tr></table>";		
            }
            $_SESSION["plh"] = '';
        }
    }else{
        echo "Only SELECT statement allowed here.";
        executeQuery(5, $tquery);
    }
?>
</TABLE>
<?php

} 
    }
?>
</div>

</body>
</html>
<?PHP 
$dbora->Close();
}
else header("location: index.php?fail=1");
?>
