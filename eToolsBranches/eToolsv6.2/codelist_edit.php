<?php

	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Edit Code List</title>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");
//include ("./txt-db-api.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
<script src="./js/jquery-1.10.2.js"></script>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="js/validate.js"></SCRIPT>
<script>
    $(document).ready(function(){
        $('.codelst_custom_col').on('click',function(){
            ftype = $(this).val();            
            pkval = $(this).attr('id');
            //alert(pkval);
            $.ajax({
                url:  "codelist_lookupajax.php",
                type: "get",
                data: {'p':ftype,'pk':pkval,'ke':'ln'},
                async:true,
                success: function(htm) {                    
                    $('#tbox').html(htm);
                }
            });            
        });
    });
</script>
<SCRIPT language="Javascript1.2">
      function getHTTPObject(){
        if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
        else if (window.XMLHttpRequest) return new XMLHttpRequest();
        else {
            alert("Your browser does not support AJAX.");
            return null;
        }
      }
	function setRights(formobj){	  
            if(typeof(formobj.totalrows) != "undefined"){
                if (formobj.codelst_custom_col) {
                    for (var i=0; i < formobj.codelst_custom_col.length; i++)
                       {
                       if (formobj.codelst_custom_col[i].checked && formobj.codelst_custom_col[i].value == "dropdown" && formobj.codelst_custom_col1.value == "")
                            {
                            alert("'HTML for Dropdown' field can not be blank.");
                            formobj.codelst_custom_col1.focus();
                            return false;
                        }
                    }
                }
                totrows = formobj.totalrows.value;						
                var resultStr = "";
                for (i=0;i<totrows;i++){
                    resultStr += formobj.rights[i].value;
                }		
                formobj.rightstr.value = resultStr;	
            }
	}
	
	function changeRights(obj,row,formobj)
	{
	  selrow = row ;
	  totrows = formobj.totalrows.value;
	  if (totrows > 1) 		
	  	rights =formobj.rights[selrow].value;
	  else 		
	  	rights =formobj.rights.value;

	 objName = obj.name;

	  if (obj.checked)
   	  {         
       	if (objName == "newr") 
			{
				rights = parseInt(rights) + 1;
				if (!formobj.view[selrow].checked)
				{ 
					formobj.view[selrow].checked = true;
					rights = parseInt(rights) + 4;
				}
			}
     	if (objName == "edit") 
			{		
				rights = parseInt(rights) + 2;
				if (!formobj.view[selrow].checked)		
				{ 
					formobj.view[selrow].checked = true;		
					rights = parseInt(rights) + 4;		
				}		
			}
     	if (objName == "view") rights = parseInt(rights) + 4;
			if (totrows > 1 )
				formobj.rights[selrow].value = rights;
			else
				formobj.rights.value = rights;
  }else
	{
       	if (objName == "newr") rights = parseInt(rights) - 1;
       	if (objName == "edit") rights = parseInt(rights) - 2;
       	if (objName == "view") 
		{	
			if (formobj.newr[selrow].checked) 
			{
				alert("The user has right to New or Edit. You can not revoke right to View");
				formobj.view[selrow].checked = true;		
			}	
		    else if (formobj.edit[selrow].checked) 
			{
				alert("The user has right to New or Edit. You can not revoke right to View");
				formobj.view[selrow].checked = true;		
			}
			else
			{		
			  rights = parseInt(rights) - 4;
			}
		}
		

	if (totrows > 1 ) {
		formobj.rights[selrow].value = rights;		
    } 
	else {
		formobj.rights.value = rights; 
  	 }	
  }  
}

	function fnEmpty(formobj){	
			if (isEmpty(formobj.role.value))
			{	alert("Please enter mandatory fields");	
				formobj.role.focus();
				return false;	 
			} 
			if (isEmpty(formobj.rolesubtype.value)) {	 
				alert("Please enter mandatory fields");	
				formobj.rolesubtype.focus();
				return false;	 
			} 
			return true;
	} 

	function validate(){
		if (!/^[a-zA-Z0-9_]*$/.test(fnTrimSpaces(stdRights.codelst_subtyp.value))) { //Added by Ruchira for Bugzilla #20545
			alert("Sub Type does not allow space or special characters (underscore allowed)");
			return false;
		}
		if (fnTrimSpaces(stdRights.codelst_subtyp.value) == "" || fnTrimSpaces(stdRights.codelst_desc.value) == "" || fnTrimSpaces(stdRights.codelst_seq.value) == "") {
			alert("Sub Type / Description / Sequence cannot be blank.");
			return false;
		}		
		if (!isInteger(stdRights.codelst_seq.value)){
			alert("Sequence allows only numbers");	
			return false;
		}	
	}

 function fnTrimSpaces(strToTrim){  
        strTemp = strToTrim;
        len=strTemp.length;
        i=0;  
        while (i < len && strTemp.charAt(i)==" "){
            i=i+1; 
        }
        strTemp = strTemp.substring(i);
        len=strTemp.length;
        j = len-1;
        while (j>0 && strTemp.charAt(j)==" "){ 
            j = j-1; 
        }
        strTemp = strTemp.substring(0,j+1);
        return strTemp;
}

//  function setReadOnly(field_type, v_pk_codelst){      
//        if (window.XMLHttpRequest) {            
//            xmlhttp = new XMLHttpRequest();
//        } else {            
//            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
//        }
//        xmlhttp.onreadystatechange = function() {
//            if (this.readyState == 4 && this.status == 200) {
//                document.getElementById("tbox").innerHTML = this.responseText;
//            }
//        };
//        xmlhttp.open("GET","codelist_lookupajax.php?p="+field_type+"&pk="+v_pk_codelst+"&ke=ln",true);
//        xmlhttp.send();      
//      
////  	if(field_type == 'text' || field_type == 'chkbox' || field_type == 'date' || field_type == 'textarea' || field_type == 'readonly-input') {         
////            document.getElementById("codelst_custom_col1").disabled = true;
////	}else{
////            document.getElementById("codelst_custom_col1").disabled = false;            
////        }
//  }
  

  
  
  //fix for the vtiger ticket 33370
  function subtypeCheck(){	
	var session = eval('(<?php echo json_encode($_SESSION)?>)');
	console.log(session);
	var sbtar = session.sbt;
	var oldsbtype = document.getElementById("oldsbt").value;
	var newsbtype = document.getElementById("codelst_subtyp1").value;
	if(oldsbtype != newsbtype){
		if(sbtar.indexOf(newsbtype) != -1){
			alert("Sub Type Already Exists!!!");
			return false;
		}
	}	
  }


  
</SCRIPT>




</head>
<body>
<div id="fedora-content">
<?php

if ($_SERVER['REQUEST_METHOD'] != 'POST'){	

	$v_pk_codelst_maint = $_GET["pk_codelst_maint"];	
        //$rs = mysql_query("select browser_flag, dd_cb_flag from et_codelst_maint where pk_codelst_maint = ".$v_pk_codelst_maint);
        $rs = "select browser_flag, dd_cb_flag from et_codelst_maint where pk_codelst_maint = ".$v_pk_codelst_maint;
        $rs = $db->query($rs);
	//if (mysql_num_rows($rs) > 0) {
        if ($rs->num_rows > 0) {
		//while ($data = mysql_fetch_array($rs)) {
                while ($data = $rs->fetch_assoc()) {
			$v_browser = $data["browser_flag"];
			$v_dd_cb = $data["dd_cb_flag"];
		}
	}


	$v_pk_codelst = $_GET['pk_codelst'];
	$v_tablename = $_GET['tablename'];
	$v_event_id = $_GET['event_id'];


	if ($v_tablename == "ER_CODELST") {
		$query="select codelst_type,codelst_subtyp,codelst_desc,codelst_custom_col,codelst_seq,codelst_custom_col,codelst_custom_col1,codelst_hide,codelst_kind from er_codelst where pk_codelst = '".$v_pk_codelst."'";
	} else  if($v_tablename == "EVENT_DEF"){
		$query="select EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,DESCRIPTION,(SELECT CODELST_TYPE FROM SCH_CODELST WHERE PK_CODELST=EVENT_LIBRARY_TYPE) as CODELST_T,(SELECT CODELST_SUBTYP FROM SCH_CODELST WHERE PK_CODELST=EVENT_LIBRARY_TYPE) as CODELST_SUBTYP, EVENT_LIBRARY_TYPE from EVENT_DEF where EVENT_ID ='".$v_event_id."'";
	}else{ 
		$query="select codelst_type,codelst_subtyp,codelst_desc,'' as codelst_custom_col,codelst_seq,'' as codelst_custom_col1,codelst_hide,codelst_kind from sch_codelst where pk_codelst = '".$v_pk_codelst."'";
	}
        
	$results = executeOCIQuery($query,$ds_conn);
	$total_rows = $results_nrows;

	$query="select count(*) as count from velink.vlnk_codelst_action where table_name = lower('$v_tablename') and lower(codelst_type) = lower('".trim($results["CODELST_TYPE"][0])."') and lower(codelst_subtyp) = lower('".$results["CODELST_SUBTYP"][0]."')";
	$results1 = executeOCIQuery($query,$ds_conn);
	$v_exists = $results1["COUNT"][0];

	// this is to set the codelst subtype editable or readonly
	$stq = "select pk_codelst from er_codelst where codelst_type = 'studystat' and instr(','||CODELST_CUSTOM_COL1||',',',".trim($results["CODELST_SUBTYP"][0]).",')>0";
	$strs = executeOCIQuery($stq, $ds_conn);
	$stno = $results_nrows;

	$roq = "select pk_codelst from er_codelst where codelst_type='tarea'  and instr(','||CODELST_CUSTOM_COL1||',',',".trim($results["CODELST_SUBTYP"][0]).",') >0";
	$rors = executeOCIQuery($roq, $ds_conn);
	$rono = $results_nrows;

	switch($results["CODELST_TYPE"][0]){
		
		case "study_division":
			if($rono==0){
				$v_exists = 0;
			}else{
				$v_exists = 1;
			}	
		break;
		
		case "studystat_type":
			if($stno==0){
				$v_exists = 0;
			}else{
				$v_exists = 1;
			}			
			break;
	}

	for ($rec=0; $rec < $total_rows; $rec++){
	?>

	<div class="navigate">Code List - Edit	</div>
	<FORM name="stdRights" action="codelist_edit.php" method="post" onSubmit="if (setRights(document.stdRights) == false) return false; if (validate() == false) return false; if(subtypeCheck() == false) return false;" >
	<BR>
	<TABLE width="1000" border="0">
	<TR>

	<input type = "hidden" name="pk_codelst" value=<?php echo $v_pk_codelst; ?>>
	<input type = "hidden" name="event_id" value=<?php echo $v_event_id; ?>>
	<input type = "hidden" name="tablename" value=<?php echo $v_tablename; ?>>
	<input type = "hidden" name="pkmaint" value=<?php echo $v_pk_codelst_maint; ?>>
	<?php
	if(trim($v_tablename) == "EVENT_DEF"){ ?>
		<td width="127">Type</td>
		<TD width="863"><input disabled type = "text" name="codelst_type" value="<?php echo trim($results["EVENT_TYPE"][$rec]); ?>"></TD>
		</tr>
		<tr><td>Name</td><td><input name="codelst_name" type = "text" class="required" value="<?php echo trim($results["NAME"][$rec]); ?>" size="50" maxlength="200">
			  <span style="font-size:11px; font-style:italic; color:#999999;"> (Maximum Length: 200 Characters)</span></td>
		</tr>
		<tr>
			<td >Description</td><TD><input name="codelst_desc" type = "text" class="required" value="<?php echo htmlspecialchars(trim($results["DESCRIPTION"][$rec])); ?>" size="100" maxlength="200">
			  <span style="font-size:11px; font-style:italic; color:#999999;"> (Maximum Length: 200 Characters)</span></TD>	
		</tr>
		<?php
	}else{ ?>
		<td width="127">Type</td>
		<TD width="863"><input disabled type = "text" name="codelst_type" value="<?php echo $results["CODELST_TYPE"][$rec]; ?>"><input type = "hidden" name="codelst_type" value=<?php echo $results["CODELST_TYPE"][$rec]; ?>></TD>
		</tr><tr>
		
		<?php
			if(trim($results["CODELST_TYPE"][$rec]) == 'calStatStd') {
				if(trim($results["CODELST_SUBTYP"][$rec]) == 'A' || trim($results["CODELST_SUBTYP"][$rec]) == 'W' || trim($results["CODELST_SUBTYP"][$rec]) == 'O') { ?>
				<td>Sub Type</td><TD style="font-size:11px; font-style:italic; color:#999999;"><input name="codelst_subtyp" id="codelst_subtyp1" type = "text" class="required" value="<?php echo trim($results["CODELST_SUBTYP"][$rec]); ?>" maxlength="15" readonly=""> (Maximum Length: 15 Characters)</TD>
				<?php } else { ?>
					<td>Sub Type</td><TD style="font-size:11px; font-style:italic; color:#999999;"><input name="codelst_subtyp" id="codelst_subtyp1" type = "text" class="required" value="<?php echo trim($results["CODELST_SUBTYP"][$rec]); ?>" maxlength="15" > (Maximum Length: 15 Characters)</TD>
				<?php } ?>
			<?php } elseif (trim($results["CODELST_TYPE"][$rec]) == 'calStatLib') {
				if(trim($results["CODELST_SUBTYP"][$rec]) == 'F' || trim($results["CODELST_SUBTYP"][$rec]) == 'W') { ?>
				<td>Sub Type</td><TD style="font-size:11px; font-style:italic; color:#999999;"><input name="codelst_subtyp" id="codelst_subtyp1" type = "text" class="required" value="<?php echo trim($results["CODELST_SUBTYP"][$rec]); ?>" maxlength="15" readonly=""> (Maximum Length: 15 Characters)</TD>
				<?php } else { ?>
					<td>Sub Type</td><TD style="font-size:11px; font-style:italic; color:#999999;"><input name="codelst_subtyp" id="codelst_subtyp1" type = "text" class="required" value="<?php echo trim($results["CODELST_SUBTYP"][$rec]); ?>" maxlength="15" > (Maximum Length: 15 Characters)</TD>
			
			<?php } } elseif (trim($results["CODELST_TYPE"][$rec]) == 'role') {
				if($v_exists == '1') { ?>
				<td>Sub Type</td><TD style="font-size:11px; font-style:italic; color:#999999;"><input name="codelst_subtyp" id="codelst_subtyp1" type = "text" class="required" value="<?php echo trim($results["CODELST_SUBTYP"][$rec]); ?>" maxlength="15" readonly=""> (Maximum Length: 15 Characters)</TD>
				<?php } else { ?>
					<td>Sub Type</td><TD style="font-size:11px; font-style:italic; color:#999999;"><input name="codelst_subtyp" id="codelst_subtyp1" type = "text" class="required" value="<?php echo trim($results["CODELST_SUBTYP"][$rec]); ?>" maxlength="15" > (Maximum Length: 15 Characters)</TD>
				<?php } ?>
			<?php } else { ?>		
				<td>Sub Type</td><TD style="font-size:11px; font-style:italic; color:#999999;"><input name="codelst_subtyp" id="codelst_subtyp1" type = "text" class="required" value="<?php echo trim($results["CODELST_SUBTYP"][$rec]); ?>" maxlength="15" <?PHP echo (($v_exists == 0)? "":"readonly"); ?>> (Maximum Length: 15 Characters)</TD>
			<?php } ?>
		</tr><tr>
			<td >Description</td><TD><input name="codelst_desc" type = "text" class="required" value="<?php echo htmlspecialchars(trim($results["CODELST_DESC"][$rec])); ?>" size="100" maxlength="200">
			  <span style="font-size:11px; font-style:italic; color:#999999;"> (Maximum Length: 200 Characters)</span></TD>
		
		</tr>
		<tr>
			<td >Sequence</td><TD><input name="codelst_seq" type = "text" class="required" value="<?php echo trim($results["CODELST_SEQ"][$rec]); ?>" size="3" maxlength="3">
			  </INPUT>
			  <span style="font-size:11px; font-style:italic; color:#999999;">
			  </INPUT>		  
		(Maximum Length: 3 Numbers)</span></TD>
		</tr>			
		<input type="hidden" id="oldsbt" name="oldsbt" value=<?php echo trim($results["CODELST_SUBTYP"][$rec]); ?>>
		<?php		
	}?>

	<?PHP 
		if ($v_browser == 1) {
			echo "<tr><TD>Display in Browser?</TD>";
			echo '<TD><input type = "radio" name="codelst_custom_col" value="browser"'.(($results["CODELST_CUSTOM_COL"][$rec]=='browser')?' checked' : '') .'>Yes</input><input type = "radio" name="codelst_custom_col" value=""'.(empty($results["CODELST_CUSTOM_COL"][$rec])?' checked' : '') .'>No</input></TD></tr>';
		}
		if ($v_dd_cb == 1) {
			echo "<tr><TD>Field Type</TD>";
			//echo '<TD><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'text'".')" value=""'.(empty($results["CODELST_CUSTOM_COL"][$rec])?' checked' : '') .'>Text</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'dropdown'".')" value="dropdown"'.(($results["CODELST_CUSTOM_COL"][$rec]=='dropdown')?' checked' : '') .'>Dropdown</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'checkbox'".')" value="chkbox"'.(($results["CODELST_CUSTOM_COL"][$rec]=='chkbox')?' checked' : '') .'>Checkbox</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'date'".')" value="date"'.(($results["CODELST_CUSTOM_COL"][$rec]=='date')?' checked' : '') .'>Date</input></TD></tr>';	
                        //echo '<TD><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'text'".')" value=""'.(empty($results["CODELST_CUSTOM_COL"][$rec])?' checked' : '') .'>Text</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'dropdown'".')" value="dropdown"'.(($results["CODELST_CUSTOM_COL"][$rec]=='dropdown')?' checked' : '') .'>Dropdown</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'checkbox'".')" value="chkbox"'.(($results["CODELST_CUSTOM_COL"][$rec]=='chkbox')?' checked' : '') .'>Checkbox</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'date'".')" value="date"'.(($results["CODELST_CUSTOM_COL"][$rec]=='date')?' checked' : '') .'>Date</input> <input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'date'".')" value="date"'.(($results["CODELST_CUSTOM_COL"][$rec]=='date')?' checked' : '') .'>Read only</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'date'".')" value="date"'.(($results["CODELST_CUSTOM_COL"][$rec]=='date')?' checked' : '') .'>Checkbox group</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'date'".')" value="date"'.(($results["CODELST_CUSTOM_COL"][$rec]=='date')?' checked' : '') .'>Lookup (Single select and Multiple select) </input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'date'".')" value="date"'.(($results["CODELST_CUSTOM_COL"][$rec]=='date')?' checked' : '') .'>Textarea</input></TD></tr>';	
                        //echo '<TD><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'text',$v_pk_codelst".')" value=""'.(empty($results["CODELST_CUSTOM_COL"][$rec])?' checked' : '') .'>Text</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'textarea',$v_pk_codelst".')" value="textarea"'.(($results["CODELST_CUSTOM_COL"][$rec]=='textarea')?' checked' : '') .'>Text area</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'dropdown',$v_pk_codelst".')" value="dropdown"'.(($results["CODELST_CUSTOM_COL"][$rec]=='dropdown')?' checked' : '') .'>Dropdown</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'chkbox',$v_pk_codelst".')" value="chkbox"'.(($results["CODELST_CUSTOM_COL"][$rec]=='chkbox')?' checked' : '') .'>Checkbox</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'checkbox',$v_pk_codelst".')" value="checkbox"'.(($results["CODELST_CUSTOM_COL"][$rec]=='checkbox')?' checked' : '') .'>Checkbox group</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'date',$v_pk_codelst".')" value="date"'.(($results["CODELST_CUSTOM_COL"][$rec]=='date')?' checked' : '') .'>Date</input> <input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'readonly-input',$v_pk_codelst".')" value="readonly-input"'.(($results["CODELST_CUSTOM_COL"][$rec]=='readonly-input')?' checked' : '') .'>Read only</input><input type = "radio" name="codelst_custom_col" onclick="setReadOnly('."'lookup',$v_pk_codelst".')" value="lookup"'.(($results["CODELST_CUSTOM_COL"][$rec]=='lookup')?' checked' : '') .'>Lookup</input></TD></tr>';
                        echo '<TD><input type = "radio" name="codelst_custom_col" id='.$v_pk_codelst.' class="codelst_custom_col"  value="text"'.(($results["CODELST_CUSTOM_COL"][$rec])?' checked' : '') .'>Text</input><input type = "radio" id='.$v_pk_codelst.' name="codelst_custom_col" class="codelst_custom_col"  value="textarea"'.(($results["CODELST_CUSTOM_COL"][$rec]=='textarea')?' checked' : '') .'>Text area</input><input type = "radio" id='.$v_pk_codelst.' name="codelst_custom_col" class="codelst_custom_col"  value="dropdown"'.(($results["CODELST_CUSTOM_COL"][$rec]=='dropdown')?' checked' : '') .'>Dropdown</input><input type = "radio" id='.$v_pk_codelst.' name="codelst_custom_col" class="codelst_custom_col"  value="chkbox"'.(($results["CODELST_CUSTOM_COL"][$rec]=='chkbox')?' checked' : '') .'>Checkbox</input><input type = "radio" id='.$v_pk_codelst.' name="codelst_custom_col" class="codelst_custom_col"  value="checkbox"'.(($results["CODELST_CUSTOM_COL"][$rec]=='checkbox')?' checked' : '') .'>Checkbox group</input><input type = "radio" id='.$v_pk_codelst.' name="codelst_custom_col" class="codelst_custom_col"  value="date"'.(($results["CODELST_CUSTOM_COL"][$rec]=='date')?' checked' : '') .'>Date</input> <input type = "radio" id='.$v_pk_codelst.' name="codelst_custom_col" class="codelst_custom_col"  value="readonly-input"'.(($results["CODELST_CUSTOM_COL"][$rec]=='readonly-input')?' checked' : '') .'>Read only</input><input type = "radio" id='.$v_pk_codelst.' name="codelst_custom_col" class="codelst_custom_col"  value="lookup"'.(($results["CODELST_CUSTOM_COL"][$rec]=='lookup')?' checked' : '') .'>Lookup</input></TD></tr>';                        
                        echo '<tr><td>HTML Code</br></td><TD id="tbox">';
                            if($results["CODELST_CUSTOM_COL"][$rec]=='text' || $results["CODELST_CUSTOM_COL"][$rec]=='chkbox' || $results["CODELST_CUSTOM_COL"][$rec]=='' || $results["CODELST_CUSTOM_COL"][$rec]=='date'  || $results["CODELST_CUSTOM_COL"][$rec]=='textarea' || $results["CODELST_CUSTOM_COL"][$rec]=='readonly-input'){
                                echo '<textarea rows=10 cols=75 id="codelst_custom_col1" name="codelst_custom_col1" disabled>'.$results["CODELST_CUSTOM_COL1"][$rec].'</textarea>';		
                            }else if($results["CODELST_CUSTOM_COL"][$rec]=='dropdown' || $results["CODELST_CUSTOM_COL"][$rec]=='checkbox'  || $results["CODELST_CUSTOM_COL"][$rec]=='lookup'){
                                echo '<textarea id="codelst_custom_col1" rows=10 cols=75 name="codelst_custom_col1" >'.$results["CODELST_CUSTOM_COL1"][$rec].'</textarea>';				
                            }
                        echo '</TD></tr>';
//			}elseif($results["CODELST_CUSTOM_COL"][$rec]=='lookup'){
//                            echo '<tr><td>Lookup</br></td><TD id="tbox"><input type="text"></input></TD></tr>';
//                        }
		}
		if($v_tablename!="EVENT_DEF"){
			echo "<tr><TD>Hide Code List?</TD>";
			echo '<TD><input type = "radio" name="codelst_hide" value="Y"'.(($results["CODELST_HIDE"][$rec]=='Y')?' checked' : '') .'>Yes</input>
			<input type = "radio" name="codelst_hide" value="N"'.(($results["CODELST_HIDE"][$rec]=='N')?' checked' : '') .'>No</input></TD></tr>';
			if($v_tablename=="ER_CODELST" && $results["CODELST_TYPE"][$rec] == "patStatus"){
				$stq = "SELECT * from ER_CODELST where CODELST_TYPE='".$results["CODELST_SUBTYP"][$rec]."'";			
				$resultsSt = executeOCIQuery($stq,$ds_conn);
				$count = $results_nrows;
				$subType = $results["CODELST_SUBTYP"][$rec];
			}

			if($results["CODELST_TYPE"][$rec]=='evtaddlcode') {
				echo "<tr><TD>Financial Details</TD>";
				echo '<TD><input type = "radio" name="codelst_financial" value="financial"'.(($results["CODELST_KIND"][$rec]=='financial')?' checked' : '') .'>Yes</input>
				<input type = "radio" name="codelst_financial" value=""'.(($results["CODELST_KIND"][$rec]!='financial')?' checked' : '') .'>No</input></TD></tr>';	
			}
		}
                
                //bug fix 26687 
                if($results["CODELST_TYPE"][0]=="race" || $results["CODELST_TYPE"][0]=="ethnicity"){
                    echo '<tr><TD>&nbsp;</TD>';                        
                            /*----------- enhancement 24319 for eTools6.1 - eRes 10  -------------------*/
                            $customcol1_dataqry = 'SELECT CODELST_CUSTOM_COL1 from er_codelst where pk_codelst = '.$v_pk_codelst;
                            $customcol1_datars = executeOCIQuery($customcol1_dataqry, $ds_conn);                            
                            $customcol1_arr = explode(',',$customcol1_datars['CODELST_CUSTOM_COL1'][0]);

                            if($results["CODELST_TYPE"][0]=="race"){
                                $substrloc = 5;
                            }elseif($results["CODELST_TYPE"][0]=="ethnicity"){
                                $substrloc = 4;
                            }
                            $pschks = "";
                            $pschkp = "";
                            if($customcol1_datars['CODELST_CUSTOM_COL1'][0] != ""){
                                for($c1=0; $c1<sizeof($customcol1_arr); $c1++){
                                    $sbstr = substr($customcol1_arr[$c1],$substrloc,strlen($customcol1_arr[$c1]));

                                    if($substrloc == 5){
                                        if($sbstr == 'prim'){
                                            $pschkp = "checked";
                                        }
                                        if($sbstr == 'second'){
                                            $pschks = "checked";
                                        }
                                    }else{
                                        if($sbstr == 'prim'){
                                            $pschkp = "checked";
                                        }
                                        if($sbstr == 'second'){
                                            $pschks = "checked";
                                        }                                            
                                    }
                                }
                            }else{
                                $pschkp = "";
                                $pschks = "";
                            }
                        
                        echo '<TD><input type = "checkbox" name="eth_race_prime"'.$pschkp.' value="P">Primary</input>';
                        echo '<input type = "checkbox" name="eth_race_second" '.$pschks.' value="S">Secondary</input>';
                    echo '</TD></tr>';
                }
                
                
		//implemented for the enhancement v8.9eT-3 on 20-12-2010
		if($results["CODELST_TYPE"][$rec]=="site_type"){
			$siteServiceQ = "SELECT CODELST_CUSTOM_COL from ER_CODELST where PK_CODELST=".$_GET['pk_codelst']." and CODELST_TYPE='site_type'";
			$siteServiceRS = executeOCIQuery($siteServiceQ, $ds_conn);
			
			if($siteServiceRS["CODELST_CUSTOM_COL"][0]=="service"){
					$vachk = "checked";
				}else{
					$vachk = "";			
				}
			echo "<tr><TD>&nbsp;</TD><td>&nbsp;</td></tr>";
			echo '<tr><td>&nbsp;</td><td><input type=checkbox name=sos value=checkbox '.$vachk.">&nbsp;This is a 'Site of Service'.</td></tr>";
			echo "<tr><TD>&nbsp;</TD><td>&nbsp;</td></tr>";
		}


		$linking = array('studystat'=>'studystat_type','studystat_type'=>'studystat','tarea'=>'study_division','study_division'=>'tarea');
		$v_codelst_type = trim($results["CODELST_TYPE"][$rec]);	

		//implemented for eT3.1-req1 - 08-12-2010
		$codelst_typeArr = array("fillformstat","studystat_type","studystat","query_status","milestone_stat","budget_stat","calStatStd","cost_desc","coverage_type");	
		if(in_array($v_codelst_type, $codelst_typeArr)){
			$chkquery = "SELECT CODELST_STUDY_ROLE from ".trim($v_tablename)." where PK_CODELST=".$_GET['pk_codelst'];
			
			$chkrs = executeOCIQuery($chkquery, $ds_conn);
			
			if($chkrs["CODELST_STUDY_ROLE"][0]!=""){
				if($chkrs["CODELST_STUDY_ROLE"][0]=="default_data"){
					$vachk = "checked";
				}elseif(substr_count($chkrs["CODELST_STUDY_ROLE"][0],',')==0){
					$vachk = "";				
				}else{
					$tmpAr = explode(',', $chkrs["CODELST_STUDY_ROLE"][0]);
					if(in_array("default_data",$tmpAr)){
						$vachk = "checked";	
					}else{
						$vachk = "";					
					}
				}			
			}else{
				$vachk = "";
			}
			if($v_codelst_type=='calStatStd'){
				echo "<tr><TD>&nbsp;</TD><td>&nbsp;</td></tr>";
				// ---- fixed bug 17897 ---- //
				//echo '<tr><td>&nbsp;</td><td><input type=checkbox name=roleconfig value=checkbox disabled value="" checked=checked>&nbsp;&nbsp;&nbsp;This Code is available when a "study role" is not in scope or when no other is configured for a study team role. Please leave it checked if you are not sure at this moment or you always want this code to be visible. Please uncheck if you wish to make this code available in study team "Roles".</td></tr>';
				echo '<tr><td>&nbsp;</td><td><input type=checkbox name=roleconfig value="checkbox" '.$vachk.'>&nbsp;&nbsp;&nbsp;This Code is available when a "study role" is not in scope or when no other is configured for a study team role. Please leave it checked if you are not sure at this moment or you always want this code to be visible. Please uncheck if you wish to make this code available in study team "Roles".</td></tr>';
				//echo '<Input type="hidden" name="roleconfig" id="roleconfig" value="checkbox">'; -- commented this for the bug 23220
				echo "<tr><TD>&nbsp;</TD><td>&nbsp;</td></tr>";
				echo '<Input type="hidden" name="config_codtyp" id="config_codtyp" value="'.$v_codelst_type.'">';
			}else{
				echo "<tr><TD>&nbsp;</TD><td>&nbsp;</td></tr>";
				echo '<tr><td>&nbsp;</td><td><input type=checkbox name=roleconfig value=checkbox '.$vachk.'>&nbsp;&nbsp;&nbsp;This Code is available when a "study role" is not in scope or when no other is configured for a study team role. Please leave it checked if you are not sure at this moment or you always want this code to be visible. Please uncheck if you wish to make this code available in study team "Roles".</td></tr>';
				echo "<tr><TD>&nbsp;</TD><td>&nbsp;</td></tr>";
				echo '<Input type="hidden" name="config_codtyp" id="config_codtyp" value="'.$v_codelst_type.'">';
			}
		}

		// display the reasons under patient status edit screen
		if($count>0){
			$st_html = '<tr><td width="127">&nbsp;</td>';		
			$st_html .= '<td width="863">';	
			$st_html .= '<table width="100%">';		
			$st_html .= '<tr><td width="70%">&nbsp;</td><td width="30%">&nbsp;</td></tr>';		
			$st_html .= '<tr><td width="70%">';
			$st_html .= "Total Rows: ".$results_nrows."</td>";
			$st_html .= '<td><a href=codelist_edit_renew.php?m=n&pkc='.$resultsSt["PK_CODELST"][0]."&tablename=".$v_tablename."&ps=".$v_pk_codelst."&pkm=".$v_pk_codelst_maint."&link=ex>Add New Reason</a></td>";
			$st_html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$st_html .= "</tr></table>";	
			$st_html .= '<Table border="1" width="100%"><thead><TR>';
			$st_html .= '<TH width="50%" onclick="refresh(document.codelist.pk_codelst_maint.value,\'CODELST_DESC\',\'\')">Resaon</TH>';	
			$st_html .= '<TH width="20%" onclick="refresh(document.codelist.pk_codelst_maint.value,\'CODELST_DESC\',\'\')">Sub Type</TH>';
			$st_html .= '<TH width="10%"></TH>';
			$st_html .= '</TR></thead>';	
			for ($recst=0; $recst < $results_nrows; $recst++){
				$st_html .= '<tbody><TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';		
				$st_html .= '<TD>'.$v_hide.$resultsSt["CODELST_DESC"][$recst].'</TD>';		
				$st_html .= '<TD>'.$v_hide.$resultsSt["CODELST_SUBTYP"][$recst].'</TD>';
				$st_html .= '<td><a href=codelist_edit_re.php?m=e&pkc='.$resultsSt["PK_CODELST"][$recst]."&tablename=".$v_tablename."&ps=".$v_pk_codelst."&pkm=".$v_pk_codelst_maint.">Edit</a></td>";		
				$st_html .= '</TR>';		
			}
			$st_html .= '</TABLE>';		
			$st_html .= '</td>';
			echo $st_html;
		}else{
			if($results["CODELST_TYPE"][$rec] == 'patStatus'){
				echo '<table><br><tr><td>Reasons&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><a href=codelist_edit_renew.php?m=n&pkc='.$subType.'&tablename='.$v_tablename.'&ps='.$v_pk_codelst.'&pkm='.$v_pk_codelst_maint.'&link=ne>Add New Reason</a></td></tr></table>';			
			}
		} 	
	?>	

	
	<?php	
		/*----- this is to show the Event Category --------*/
		if ($v_tablename == "EVENT_DEF") {			
			// event library category implemented on 19-8-2010
			$seltypQ = "SELECT * from EVENT_DEF where EVENT_ID='".$_GET['event_id']."'";
			$seltyp = executeOCIQuery($seltypQ,$ds_conn);
					
			$etypeQ = "SELECT * FROM SCH_CODELST where CODELST_TYPE='lib_type'";	
			$etype = executeOCIQuery($etypeQ,$ds_conn);		
			
			$types = "";
			
			for($ty=0; $ty < $results_nrows; $ty++){
				if($etype["PK_CODELST"][$ty]==$seltyp["EVENT_LIBRARY_TYPE"][0]){
					$types .= "<option value=".$etype["PK_CODELST"][$ty]." SELECTED>".$etype["CODELST_DESC"][$ty]."</option>";			
				}else{
					$types .= "<option value=".$etype["PK_CODELST"][$ty].">".$etype["CODELST_DESC"][$ty]."</option>";						
				}

			}		
			echo '<tr>';
			echo '<td >Event Library type</td><TD><select name="typeChk">'.$types.'</select>';
			echo '</TD></tr>';
		}



		if ($v_codelst_type == "studystat" || $v_codelst_type == "tarea") {	
			$v_sql = "select codelst_desc,codelst_subtyp from er_codelst where codelst_type='".$linking[$v_codelst_type]."' order by codelst_desc";
			$results1 = executeOCIQuery($v_sql,$ds_conn);
			echo "<tr><td colspan=2><hr color=red/></td></tr>";
			echo '<TR><TD>';
			echo ($v_codelst_type == "studystat"?'Study Types':'');
			echo ($v_codelst_type == "tarea"?'Study Division':'');
			echo '</TD><TD><table width="100%" style="border-width: 0px 0px 0px 1px;	border-spacing: 0px;	border-style: dotted dotted dotted dotted;	border-color: blue blue blue #33cc00;	border-collapse: collapse;	background-color: white;"><tr>';
			$subtyp_list = ' ,'.$results["CODELST_CUSTOM_COL1"][$rec].','; //Added by Ruchira for Bugzilla #20546
			for ($rec1 = 0; $rec1 < $results_nrows; $rec1++){
				$subtyp_checked = ','.(empty($results1["CODELST_SUBTYP"][$rec1])?"":$results1["CODELST_SUBTYP"][$rec1]).','; //Added by Ruchira for Bugzilla #20546
				if (stripos($subtyp_list,$subtyp_checked)) { //Added by Ruchira for Bugzilla #20546
					echo '<td><b><font color="#009900;"><input type=checkbox name=codelst_custom_col1['.$rec1.'] checked value='.$results1["CODELST_SUBTYP"][$rec1].'>'.$results1["CODELST_DESC"][$rec1].'&nbsp;&nbsp;&nbsp;</font></b></td>';
				} else {
					echo '<td><input type=checkbox name=codelst_custom_col1['.$rec1.'] value='.$results1["CODELST_SUBTYP"][$rec1].'>'.$results1["CODELST_DESC"][$rec1].'&nbsp;&nbsp;&nbsp;</td>';
				}
				if (fmod($rec1+1,3) == 0) echo '</tr><tr>';
			}
			echo '</tr></table></TD></TR>';
			echo "<tr><td colspan=2><hr color=red/></td></tr>";
		}

		/*------ this is to show event_type--------*/
		if($v_codelst_type == "lib_type" && $v_tablename == "SCH_CODELST"){				
			$cat = "SELECT NAME, USER_ID, (select USR_LOGNAME from ER_USER where PK_USER=EVENT_DEF.CREATOR) as usr FROM EVENT_DEF WHERE EVENT_TYPE='L' and EVENT_LIBRARY_TYPE=".$v_pk_codelst;
			$chkdvals = executeOCIQuery($cat,$ds_conn);	
					
			echo "<tr><td colspan=2><hr color=red/></td></tr>";
			echo '<TR><TD>';
			echo ($v_codelst_type == "lib_type"?'Associated Event Categories':'');
			echo '</TD><TD><table width="100%" style="border-width: 0px 0px 0px 1px;	border-spacing: 0px;	border-style: dotted dotted dotted dotted;	border-color: blue blue blue #33cc00;	border-collapse: collapse;	background-color: white;"><tr>';
			$v_html = "";
			$counter = 0;		
			for ($rec=0; $rec < $results_nrows; $rec++){		
					echo  '<TD height="23"><a href="#" class="tip"><img src="./img/info_1.png">';
					echo '<span>';
					echo '<table class="ttip" width="100%">';
					echo '<tr height="23"><td width="40%" valign="top"><b>For Account</b></td><td width="60%"><font color=red><b>'.$chkdvals["USER_ID"][$rec].'</b></font></td><tr>';
					echo '<tr height="23"><td width="40%" valign="top"><b>For User</b></td><td width="60%"><font color=red><b>'.$chkdvals["USR"][$rec].'</b></font></td><tr>';				
					echo '</table>';
					echo '</span></a>';					
					echo  '&nbsp;&nbsp;'.ucfirst(strtolower($chkdvals["NAME"][$rec])).'&nbsp;&nbsp;&nbsp;</td>';			
				$counter++;
				if (fmod($rec+1,2) == 0) echo '</tr><tr>';									
			}		
			
		}
		
		if ($v_codelst_type == "studystat_type" || $v_codelst_type == "study_division") {	
			$v_sql = "select a.pk_codelst from 
			(select pk_codelst, codelst_desc,'99999,' || codelst_custom_col1 || ',' as codelst_custom_col1 from er_codelst where codelst_type = '".$linking[$v_codelst_type]."') a,
			(select ',' || codelst_subtyp || ',' as codelst_compare,codelst_subtyp  from er_codelst where codelst_type = '$v_codelst_type') b
			where instr(a.codelst_custom_col1,b.codelst_compare) > 0 
			and b.codelst_subtyp = '".$results["CODELST_SUBTYP"][$rec]."'";
			$results2 = executeOCIQuery($v_sql,$ds_conn);
			$v_sql = "select pk_codelst,codelst_desc from er_codelst where codelst_type='".$linking[$v_codelst_type]."' order by codelst_desc";
			$results1 = executeOCIQuery($v_sql,$ds_conn);
			echo "<tr><td colspan=2><hr color=red/></td></tr>";
			echo '<TR><TD>';
			echo ($v_codelst_type == "studystat_type"?'Study Statuses':'');
			echo ($v_codelst_type == "study_division"?'Therapeutic Area':'');
			echo '</TD><TD><table width="100%" style="border-width: 0px 0px 0px 1px;	border-spacing: 0px;	border-style: dotted dotted dotted dotted;	border-color: blue blue blue #33cc00;	border-collapse: collapse;	background-color: white;"><tr>';
			$counter = 0;
			for ($rec1 = 0; $rec1 < $results_nrows; $rec1++){
				if (in_array($results1["PK_CODELST"][$rec1],$results2['PK_CODELST'])) {
					echo '<td><b><font color="#009900;"><input type=checkbox name=studystat_type['.$rec1.'] checked value='.$results1["PK_CODELST"][$rec1].' />'.$results1["CODELST_DESC"][$rec1].'&nbsp;&nbsp;&nbsp;</font></b></td>';
					echo '<input type=hidden name=checkedvals['.$counter.'] value="'.$results1["PK_CODELST"][$rec1].'">';
					$counter++;
				} else {
					echo '<td><input type=checkbox name=studystat_type['.$rec1.'] value='.$results1["PK_CODELST"][$rec1].'>'.$results1["CODELST_DESC"][$rec1].'&nbsp;&nbsp;&nbsp;</td>';
				}
				if (fmod($rec1+1,3) == 0) echo '</tr><tr>';
			}
			echo '</tr></table></TD></TR>';
			echo "<tr><td colspan=2><hr color=red/></td></tr>";			
		}
		
                
		if ($results["CODELST_TYPE"][$rec] == "role"){
			$query="select ctrl_value from er_ctrltab where ctrl_key = '".$results["CODELST_SUBTYP"][$rec]."'";
			$results1 = executeOCIQuery($query,$ds_conn);
			if (isset($results1["CTRL_VALUE"][0])) {
				if ($results1["CTRL_VALUE"][0] == '0') {
					$query="select count(*) as count from er_ctrltab where ctrl_key = 'study_rights'";
					$results1 = executeOCIQuery($query,$ds_conn);
					$v_role = str_pad("",$results1["COUNT"][0],"0");
				} else {
					$v_role = $results1["CTRL_VALUE"][0] ;
				}
			} else {
				$query="select count(*) as count from er_ctrltab where ctrl_key = 'study_rights'";
				$results1 = executeOCIQuery($query,$ds_conn);
				$v_role = str_pad("",$results1["COUNT"][0],"0");
			}		

			$query="select ctrl_value,ctrl_desc,ctrl_seq from er_ctrltab where ctrl_key = 'study_rights' order by ctrl_seq";
			$results1 = executeOCIQuery($query,$ds_conn);		
			?>
			<table border="1">
			<tr><th>MODULE</th><th>&nbsp;NEW&nbsp;</th><th>&nbsp;EDIT&nbsp;</th><th>&nbsp;VIEW&nbsp;</th></tr>
			<Input type="hidden" name="totalrows" id="totalrows" value= "<?PHP echo $results_nrows; ?>" >

	<?PHP
			for ($rec1 = 0; $rec1 < $results_nrows; $rec1++){
				echo '<TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';"><TD>';
				echo '<input type="hidden" name=key['.$rec1.'] value="'.$results1["CTRL_VALUE"][$rec1].'" />';
				if (substr($results1["CTRL_VALUE"][$rec1],0,2) == "H_") {
					echo "<B>".strtoupper($results1["CTRL_DESC"][$rec1])."</B>";
				} elseif (($results1["CTRL_VALUE"][$rec1] == "STUDYSEC") || ($results1["CTRL_VALUE"][$rec1] == "STUDYAPNDX") || ($results1["CTRL_VALUE"][$rec1] == "STUDYEUSR")) {
					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<I>".$results1["CTRL_DESC"][$rec1]."</I>";
				} else {
					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$results1["CTRL_DESC"][$rec1];
				}
				
				echo "</TD>";
				if (substr($results1["CTRL_VALUE"][$rec1],0,2) == "H_" || ($results1["CTRL_VALUE"][$rec1] == "STUDYSUM") || ($results1["CTRL_VALUE"][$rec1] == "STUDYVPDET")) {
					echo "<TD align='center'><INPUT TYPE='hidden' value='HIDDEN' NAME='newr'/></TD>";
				} else {
					echo "<TD align='center'><INPUT ";
					if (substr($v_role,$rec1,1) == 5 || substr($v_role,$rec1,1) == 7) echo " checked ";
					echo "TYPE='CHECKBOX' value='X' NAME='newr' onclick='changeRights(this,$rec1,document.stdRights)'/></TD>";

				}
				if (substr($results1["CTRL_VALUE"][$rec1],0,2) == "H_" || ($results1["CTRL_VALUE"][$rec1] == "STUDYNOTIFY") || ($results1["CTRL_VALUE"][$rec1] == "STUDYVPDET")) {
					echo "<TD align='center'><INPUT TYPE='hidden' value='HIDDEN' NAME='edit'/></TD>";
				} else {
					echo "<TD align='center'><INPUT ".(substr($v_role,$rec1,1) >= 6 ? "CHECKED" : "")." TYPE='CHECKBOX' value='X' NAME='edit' onclick='changeRights(this,$rec1,document.stdRights)'/></TD>";
				}
				if (substr($results1["CTRL_VALUE"][$rec1],0,2) == "H_") {
					echo "<TD align='center'><INPUT TYPE='hidden' value='HIDDEN' NAME='view'/></TD>";
				} else {
					echo "<TD align='center'><INPUT ".(substr($v_role,$rec1,1) >= 4 ? "CHECKED" : "")." TYPE='CHECKBOX' value='X' NAME='view' onclick='changeRights(this,$rec1,document.stdRights)'/></TD>";
				}
				echo "</TR>";
				echo '<Input type="hidden" NAME="rights" value= "'.substr($v_role,$rec1,1).'" >';
			}
			
	?>

			<br>Apply study team role access rights changes to existing users in study team? <input name="applyrights" type="checkbox"><br>
			<?PHP
			
			echo '<BR><INPUT type="hidden" name="rcount" value='.$results_nrows.'>';
		}
		?>


                      
	</table>
		<tr>
		<td>
		<BR><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /> 
		<input type="hidden" name="rightstr" id="rightstr" value="0" />
		
	</td></tr>
	</table>

		<div class="important" style="margin-left: 0.5in; margin-right: 0.5in; margin-top:20px;">
			<table border="0" summary="Warning: Disabling the firewall may make your system vulnerable">
				<tr>
					<th align="left">Note</th>
				</tr>
				<tr>
					<td align="left" valign="top">
						<p>Sub Type must be unique within Type</p>
					</td>
				</tr>
			</table>
		</div>
	</form>

	<?PHP if ($v_dd_cb == 1) { ?>
		<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;">
		<table border="0" summary="Note: Note">
		<tr>
		<th align="left">Note</th>
		</tr>
		<tr><td align="left" valign="top">
		<p>
			  If 'Field Type' is dropdown use the HTML syntax below to create the dropdown field:
		</p><p>
				&lt;SELECT NAME="alternateId"> <br>
				&lt;OPTION value = "">Select an option&lt;/OPTION> <br>
				&lt;OPTION value = "Yes">Yes&lt;/OPTION> <br>
				&lt;OPTION value = "No">No&lt;/OPTION> <br>
				&lt;/SELECT>
		</p>
		<p>
			  If 'Field Type' is checkbox group use the syntax below to create the dropdown field:
		</p><p>
                          {chkArray:[ {data: "option1", display:"Drug"}, {data: "option2", display:"Device"}, {data: "option3", display:"Imaging"}]}
		</p>
                <br>
		<p>
			  If 'Field Type' is lookup use the syntax below to create the lookup field, If selection is single use this selection:"single", If selection is multi use this selection:"multi":
		</p><p>
                          {lookupPK:100040, selection:"single", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}
		</p>

                </td></tr>
		<tr><td>&nbsp;</td></tr>	
		</table></div>
	<?php 
}
}
} else {


	$v_codelst_custom_col = "";
	$v_pk_codelst = $_POST['pk_codelst'];
	$v_codelst_desc = $_POST['codelst_desc'];

	$v_codelst_ty = $_POST['codelst_type'];

	$v_codelst_subtyp = isset($_POST['codelst_subtyp'])? $_POST['codelst_subtyp'] : "";

        $v_codelst_type = $_POST['codelst_type'];

        
        $eth_race_prime = $_POST['eth_race_prime'];
        $eth_race_second = $_POST['eth_race_second'];
        
        
        $lkpid = $_POST['lkpname'];
        $lkpcol = $_POST['lkpcol'];
        $lkptype = $_POST['lkptype'];        
        //$lkp_codelist_custom_col1 = '{lookupPK: '.$lkpid.', selection:"'.$lkptype.'", mapping:[{source:"Sponsor_Name", target:"alternateId"}]}';
        
        
	//implemented for the bug 6540
	if($_POST['codelst_type']=='calStatStd'){
		$v_codelst_subtyp = strtoupper($v_codelst_subtyp);
	}else{
		$v_codelst_subtyp = $v_codelst_subtyp;
	}

	$v_codelst_hide = (isset($_POST['codelst_hide']) ? $_POST['codelst_hide'] : "");
	$v_codelst_financial = (isset($_POST['codelst_financial']) ? $_POST['codelst_financial'] : "");

	if (isset($_POST['codelst_custom_col'])) $v_codelst_custom_col = $_POST['codelst_custom_col'];
		$v_tablename= $_POST['tablename'];
		$v_codelst_seq = $_POST['codelst_seq'];

	if ($v_codelst_seq==''){
		$v_codelst_seq = 'NULL';
	}


	if (isset($_POST['codelst_custom_col1'])) {
		$v_codelst_custom_col1 = $_POST['codelst_custom_col1']; 
	} else {
		$v_codelst_custom_col1 = "";
	}
        
        //--- this will prevent the query which comes with single quote
        //$v_codelst_custom_col1 =  str_replace("'","''",$v_codelst_custom_col1);

	if (is_array($v_codelst_custom_col1)) {
		$str = "";
		foreach($v_codelst_custom_col1 as $checked) {
			if (!empty($checked)) $str .= ','.$checked;
		}
		$v_codelst_custom_col1 = substr($str,1);
	}

if (isset($_POST['studystat_type'])) {
		$studystat_types = $_POST['studystat_type']; //new_checked
		$checkedVals = isset($_POST['checkedvals'])?$_POST['checkedvals']:array(); //pre_checked
		$unCheckedVals = array_diff($checkedVals,$studystat_types); //un_checked	
		

		foreach($studystat_types as $studystat_type_pk) {		
			$query = "select codelst_custom_col1 from er_codelst where pk_codelst = $studystat_type_pk";
			$results = executeOCIQuery($query,$ds_conn);
			$customCol1 = $results["CODELST_CUSTOM_COL1"][0];
			if (strlen(trim($customCol1)) == 0) {
				$v_codelst_custom_col1 = $v_codelst_subtyp;
				$query = "update er_codelst set codelst_custom_col1 = '$v_codelst_custom_col1' where pk_codelst = $studystat_type_pk";
				$results = executeOCIUpdateQuery($query,$ds_conn);
			} else {
				if (!stripos(','.$customCol1.',',$v_codelst_subtyp)) {
					if (!empty($v_codelst_subtyp)) {
						$v_codelst_custom_col1.= $customCol1.','.$v_codelst_subtyp;
						$query = "update er_codelst set codelst_custom_col1 = '$v_codelst_custom_col1' where pk_codelst = $studystat_type_pk";					
						$results = executeOCIUpdateQuery($query,$ds_conn);
						
					}
				}										
			}	
			$v_codelst_custom_col1 = "";	
		}

		// this happens when unchecked
		foreach($unCheckedVals as $studystat_type_pk){
			$query = "select codelst_custom_col1 from er_codelst where pk_codelst = $studystat_type_pk and codelst_subtyp is not null";
			$results = executeOCIQuery($query,$ds_conn);
			$customCol1 = $results["CODELST_CUSTOM_COL1"][0];
			$customCol1 = explode(',',$customCol1);
			$customCol2 = array_diff($customCol1,array($v_codelst_subtyp));
			$customCol1 = implode(',',$customCol2);
			$query = "update er_codelst set codelst_custom_col1 = '$customCol1' where pk_codelst = $studystat_type_pk";
			$results = executeOCIUpdateQuery($query,$ds_conn);
		}
		$v_codelst_custom_col1 = ""; 
		
}else{ // completely unchecked
		$studystat_types = $_POST['checkedvals'];	
		foreach($studystat_types as $studystat_type_pk) {
			$query = "select codelst_custom_col1 from er_codelst where pk_codelst = $studystat_type_pk";
			$results = executeOCIQuery($query,$ds_conn);
			$customCol1 = $results["CODELST_CUSTOM_COL1"][0];
			if(strlen(trim($customCol1))!=0){
				$customCol1 = explode(',',$customCol1);
				$customCol2 = array_diff($customCol1,array($v_codelst_subtyp));
				$customCol1 = implode(',',$customCol2);
				$query = "update er_codelst set codelst_custom_col1 = '$customCol1' where pk_codelst = $studystat_type_pk";
				$results = executeOCIUpdateQuery($query,$ds_conn);
			}else{
				$query = "update er_codelst set codelst_custom_col1 is null where pk_codelst = $studystat_type_pk";
				$results = executeOCIUpdateQuery($query,$ds_conn);		
			}
		}	

}

if ($v_tablename == "ER_CODELST") {
	// introduced codelst_type to bug 6057 on 25-04-2011
	$select_codelst_subtyp = "SELECT codelst_subtyp from er_codelst WHERE codelst_type='$v_codelst_ty' AND codelst_subtyp = '$v_codelst_subtyp' AND pk_codelst != '$v_pk_codelst'";	
	$codelst_subtyp_results = executeOCIQuery($select_codelst_subtyp, $ds_conn);
	$count_codelst_subtyp_results = count($codelst_subtyp_results["CODELST_SUBTYP"]);

	$tmpQuery = "select CODELST_CUSTOM_COL, CODELST_CUSTOM_COL1 from er_codelst where codelst_type='$v_codelst_ty' AND codelst_subtyp = '$v_codelst_subtyp' AND pk_codelst = '$v_pk_codelst'";
	$tmpQuery_results = executeOCIQuery($tmpQuery, $ds_conn);
	$tmpQuery_results_col = $tmpQuery_results["CODELST_CUSTOM_COL"][0];
	$tmpQuery_results_col1 = $tmpQuery_results["CODELST_CUSTOM_COL1"][0];


	if($count_codelst_subtyp_results == '0') {		
		if($_POST['codelst_type']=="evtaddlcode" || $_POST['codelst_type']=="peridtype" || $_POST['codelst_type']=="user" || $_POST['codelst_type']=="studyidtype" || $_POST['codelst_type']=="advtype"){
			$query = "update er_codelst set ".(isset($_POST['codelst_subtyp'])?"codelst_subtyp = '".trim($v_codelst_subtyp)."',":"")." codelst_desc = '".trim($v_codelst_desc)."',";	
			 if($v_codelst_custom_col=="chkbox" || $v_codelst_custom_col=="" || $v_codelst_custom_col=="date" || $v_codelst_custom_col=="textarea" || $v_codelst_custom_col=="readonly-input" || $v_codelst_custom_col=="text"){
				$query .= "codelst_custom_col = '$v_codelst_custom_col',";
				$query .= "codelst_custom_col1 = '',";
				$query .= "codelst_seq = $v_codelst_seq,codelst_hide = '$v_codelst_hide',codelst_kind = '$v_codelst_financial' where pk_codelst=$v_pk_codelst";	
			 }else if($v_codelst_custom_col=="dropdown" || $v_codelst_custom_col=="checkbox"){
 				$query .= "codelst_custom_col = '$v_codelst_custom_col',";		 	
				$query .= "codelst_custom_col1 = '".stripslashes($v_codelst_custom_col1)."', ";				
				$query .= "codelst_seq = $v_codelst_seq,codelst_hide = '$v_codelst_hide',codelst_kind = '$v_codelst_financial' where pk_codelst=$v_pk_codelst";
			 }elseif($v_codelst_custom_col=="lookup"){
				$query .= "codelst_custom_col = '$v_codelst_custom_col',";
                                //$query .= "codelst_custom_col1 = '".$lkp_codelist_custom_col1."',"; --fix for the bug#26265
                                $query .= "codelst_custom_col1 = '".stripslashes($v_codelst_custom_col1)."', ";				
				$query .= "codelst_seq = $v_codelst_seq,codelst_hide = '$v_codelst_hide',codelst_kind = '$v_codelst_financial' where pk_codelst=$v_pk_codelst";

                         }
		}else{			
			$query = "update er_codelst set ".(isset($_POST['codelst_subtyp'])?"codelst_subtyp = '".trim($v_codelst_subtyp)."',":"")." codelst_desc = '".trim($v_codelst_desc)."', ";
			
			if (strlen($v_codelst_custom_col) > 0 )
				$query .= "codelst_custom_col = '$v_codelst_custom_col', ";
			if(strlen($v_codelst_custom_col1) > 0){
				$query .= "codelst_custom_col1 = '".stripslashes($v_codelst_custom_col1)."', ";
			}
			
			if(isset($_POST['sos'])){
				if($tmpQuery_results_col == ''){
					$query.= "codelst_custom_col ='service',";	
				}				
			}else if($_POST['codelst_type']=="site_type"){
				$query.= "codelst_custom_col ='',";
			}
			if($_POST['codelst_type']=="patStatus"){
				if($v_codelst_custom_col == '' && $tmpQuery_results_col != ''){
					$query.= "codelst_custom_col ='',";
				}				
			}
			if($_POST['codelst_type']=="tarea"){
				if($v_codelst_custom_col1 == ''){
					$query.= "codelst_custom_col1 ='',";
				}								
			}
			
			if($_POST['codelst_type']=="studystat"){
				if($v_codelst_custom_col == '' && $tmpQuery_results_col != ''){
					$query.= "codelst_custom_col ='',";
				}				

				if($v_codelst_custom_col1 == '' && $tmpQuery_results_col1 != ''){
					$query.= "codelst_custom_col1 ='',";
				}				
			}
                        
                        /*----------- enhancement 24319 for eTools6.1 - eRes 10  -------------------*/
                        if($_POST['codelst_type']=="race" || $_POST['codelst_type']=="ethnicity"){
                            if($_POST['codelst_type'] == 'race'){
                                $key = 'race';
                            }else{
                                $key = 'eth';
                            }
                            
                            if($eth_race_prime == 'P'){
                                $cusstringp = $key.'_prim';
                            }
                            if($eth_race_second == 'S'){
                                $cusstrings = $key.'_second';
                            }
                            
                            if($cusstringp != '' && $cusstrings != ''){
                                $cus1 = $cusstringp.','.$cusstrings;
                            }elseif($cusstringp != ''){
                                $cus1 = $cusstringp;
                            }elseif($cusstrings != ''){
                                $cus1 = $cusstrings;
                            }
                            $updatecustom1qry = "UPDATE ER_CODELST set CODELST_CUSTOM_COL1='".$cus1."' where pk_codelst=".$v_pk_codelst;
                            $updatecustom1rs = executeOCIUpdateQuery($updatecustom1qry,$ds_conn);
                        }                        
			$query .= "codelst_seq = $v_codelst_seq,codelst_hide = '$v_codelst_hide' where pk_codelst=$v_pk_codelst";
		}
		$subtypeFlag = 0;                
                
	} else {
	?>
		<script type="text/javascript">
			alert("Sub Type Already Exists!!!");
		</script>		
	<?php
		$subtypeFlag = 1;
		$edit_url = "pk_codelst=$v_pk_codelst&tablename=ER_CODELST&pk_codelst_maint=105&stf=d";
		echo "<meta http-equiv='refresh' content='0; url=./codelist_edit.php?$edit_url'>";		
	}

} else if($v_tablename == "SCH_CODELST") { /* -----------   Update and Insert query for event type and category ----------------*/
   
	$select_codelst_subtyp = "SELECT codelst_subtyp from sch_codelst WHERE codelst_subtyp = '$v_codelst_subtyp' AND pk_codelst != '$v_pk_codelst' AND codelst_type='$v_codelst_ty'";
	$codelst_subtyp_results = executeOCIQuery($select_codelst_subtyp, $ds_conn);
	
	$count_codelst_subtyp_results = count($codelst_subtyp_results["CODELST_SUBTYP"]);
	
	if($count_codelst_subtyp_results == '0') {
            $query = "update sch_codelst set ".(isset($_POST['codelst_subtyp'])?"codelst_subtyp = '".trim($v_codelst_subtyp)."',":"")." codelst_desc = '".trim($v_codelst_desc)."', codelst_seq = $v_codelst_seq, codelst_hide = '$v_codelst_hide' where pk_codelst=$v_pk_codelst";
            $subtypeFlag = 0;
	} else {
            ?>
            <script type="text/javascript">
                    alert("Sub Type Already Exists!!!");
            </script>
            <?php		
            $subtypeFlag = 1;
            $edit_url = "pk_codelst=$v_pk_codelst&tablename=SCH_CODELST&pk_codelst_maint=119&stf=d";
            echo "<meta http-equiv='refresh' content='0; url=./codelist_edit.php?$edit_url'>";
	}

} else  if($v_tablename == "EVENT_DEF"){
	$cName = $_POST['codelst_name'];
	$cDesc = $_POST['codelst_desc'];
	$cType = $_POST['typeChk'];

	$evntid = $_POST['event_id'];
	
	$relQ = "SELECT * from EVENT_DEF where CHAIN_ID=".$evntid." and NAME='".$cName."'";
	$results = executeOCIQuery($relQ, $ds_conn);
	if($results_nrows>0){
		
	}else{

		$query = "update event_def set  NAME = '".$cName."', DESCRIPTION = '".$cDesc."', EVENT_LIBRARY_TYPE = ".$cType." where event_id=".$evntid;	
	}
}

//
//        echo $query;
//        exit;
        

	$results = executeOCIUpdateQuery($query, $ds_conn);	

	// implemented this on 09-12-2010 for the requirement of eT3.1-1 (for eres compatible of 8.9.x)
	$codelst_typeArr = array("fillformstat","studystat_type","studystat","query_status","milestone_stat","budget_stat","calStatStd","cost_desc","coverage_type");
        
	if(in_array($_POST['config_codtyp'], $codelst_typeArr)){
            $chQ = "SELECT CODELST_STUDY_ROLE from ".$v_tablename." where pk_codelst=".$v_pk_codelst;
            $chQRS = executeOCIQuery($chQ, $ds_conn);
            $chQRS_array = explode(',',$chQRS["CODELST_STUDY_ROLE"][0]);            
            
            if($_POST['roleconfig']!= ""){
                    $inarray = in_array('default_data', $chQRS_array);
                    if($chQRS["CODELST_STUDY_ROLE"][0] == ""){
                        $conQ = "UPDATE ".$v_tablename." set CODELST_STUDY_ROLE ='default_data' where pk_codelst=".$v_pk_codelst;
                    }elseif($inarray == 1){
                        $conQ = "UPDATE ".$v_tablename." set CODELST_STUDY_ROLE ='".$chQRS["CODELST_STUDY_ROLE"][0]."' where pk_codelst=".$v_pk_codelst;
                    }elseif($inarray != 1){
                        $conQ = "UPDATE ".$v_tablename." set CODELST_STUDY_ROLE ='".$chQRS["CODELST_STUDY_ROLE"][0].",default_data' where pk_codelst=".$v_pk_codelst;	
                    }			
            }else{
                if($chQRS["CODELST_STUDY_ROLE"][0]!=""){			
                    if($chQRS["CODELST_STUDY_ROLE"][0]=="default_data"){
                        $conQ = "UPDATE ".$v_tablename." set CODELST_STUDY_ROLE ='' where pk_codelst=".$v_pk_codelst;	
                    }elseif(substr_count($chQRS["CODELST_STUDY_ROLE"][0],',')!=0){
                        $tmpAr = explode(',', $chQRS["CODELST_STUDY_ROLE"][0]);					
                        if(in_array("default_data", $tmpAr)){						
                            $tt = "";
                            for($i=0; $i<count($tmpAr); $i++){
                                if($tmpAr[$i]!="default_data"){							
                                    $tt.= $tmpAr[$i].",";
                                }
                            }
                            if(substr($tt,strlen($tt)-1,strlen($tt))==","){
                                $tt = substr($tt,0,strlen($tt)-1);
                            }
                            $conQ = "UPDATE ".$v_tablename." set CODELST_STUDY_ROLE ='".$tt."' where pk_codelst=".$v_pk_codelst;	
                        }
                    }
                }					
            }
            $results = executeOCIUpdateQuery($conQ,$ds_conn);
	}

	if(isset($_POST["rightstr"])){     
            $v_rights = $_POST["rightstr"];
            $codelsttypearr = array('user','peridtype','studyidtype','evtaddlcode','advtype');            
            if(in_array($v_codelst_type,$codelsttypearr)==0){
		$query = "select count(*) as count from er_ctrltab where trim(ctrl_key) = trim('$v_codelst_subtyp')";
		$results = executeOCIQuery($query,$ds_conn);
		if ($results["COUNT"][0] == 0) {
                    $query = "insert into er_ctrltab (pk_ctrltab,ctrl_value,ctrl_key,ctrl_desc) values (seq_er_ctrltab.nextval,'$v_rights','".trim($v_codelst_subtyp)."','".trim($v_codelst_desc)."')";
		}else{
                    $query = "update er_ctrltab set ctrl_value = '".$v_rights."' where trim(ctrl_key) = '".trim($v_codelst_subtyp)."'";
		}
		$results = executeOCIUpdateQuery($query,$ds_conn);  
            }
	}

	if (isset($_POST["applyrights"])) {
		$query = "update er_studyteam set study_team_rights = '$v_rights' where fk_codelst_tmrole = $v_pk_codelst";
		$results = executeOCIUpdateQuery($query,$ds_conn);
	}

OCICommit($ds_conn);
OCILogoff($ds_conn);
	if (($v_tablename == "SCH_CODELST" || $v_tablename == "ER_CODELST") && $count_codelst_subtyp_results == '0') {
		echo "Data Saved Successfully !!!";
	}
	
	if($subtypeFlag == 1){
		echo "<meta http-equiv='refresh' content='0; url=./codelist_edit.php?$edit_url'>";		
	}else{
		echo '<meta http-equiv="refresh" content="0; url=./codelist.php">';
	}

}

?>

</div>
</body>
</html>

<?php
}
else header("location: index.php?fail=1");
?>
