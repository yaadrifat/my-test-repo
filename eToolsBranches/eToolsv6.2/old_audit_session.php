<?php
	/*------ Author: Nicholas L -----------*/	
	include ("./txt-db-api.php");
	include("./includes/oci_functions.php");
	require_once('audit_queries.php');
	
	function sessionTracking($param, $pagelink){
		$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]);
		$db = new Database("etools");		
		switch($param){
			case "loggin":
				$loggedinTime = date("d-m-Y H:i:s");
				$_SESSION['logintime'] = $loggedinTime;
				
				$grpName = $db->executeQuery("SELECT GROUP_NAME from ET_GROUPS WHERE PK_GROUPS =".$_SESSION["FK_GROUPS"]);
				$grpName->next();
				$grpNameVal = $grpName->getCurrentValuesAsHash();				
				
				$rowInfo = array(3,$_SESSION["user"],$grpNameVal['GROUP_NAME'] ,"SYSDATE", "SYSDATE", "I", "SYSDATE");
				auditQuery(3,$rowInfo);
			break;
			
			case "loggedout":
				$login = $_SESSION['logintime'];
				$loggedinTime = strtotime($_SESSION['logintime']);
				$loggedoutTime = strtotime(date("d-m-Y H:i:s"));
				$loginDuration = $loggedoutTime- $loggedinTime;
				$days    = floor($loginDuration / 86400);
				$hours   = floor(($loginDuration - ($days * 86400)) / 3600);
				$minutes = floor(($loginDuration - ($days * 86400) - ($hours * 3600))/60);
				$seconds = floor(($loginDuration - ($days * 86400) - ($hours * 3600) - ($minutes*60)));
				$loginDuration = $days.":".$hours.":".$minutes.":".$seconds;
				//$rs = $db->executeQuery("UPDATE ET_SESSION_AUDIT SET USER_NAME ='".$_SESSION["PK_USERS"]."', GROUP_NAME = '".$_SESSION["FK_GROUPS"]."', LOGGEDOUT_TIME ='".date("d-m-Y H:i:s")."', LOGIN_DURATION = '".$loginDuration."',ACCESSED_ON = '".date("d-m-Y H:i:s")."' ,ACCESSED_SEARCH_DATE = '".date("d-m-Y")."',MOD_OPT = 'optLo' WHERE LOGIN_TIME = '".$login."' AND USER_NAME ='".$_SESSION["PK_USERS"]."'");
				$insQuery = "UPDATE ET_SESSION_AUDIT SET USER_NAME ='".$_SESSION["PK_USERS"]."', GROUP_NAME = '".$_SESSION["FK_GROUPS"]."', LOGGEDOUT_TIME ='".date("d-m-Y H:i:s")."', LOGIN_DURATION = '".$loginDuration."',ACCESSED_ON = '".date("d-m-Y H:i:s")."' ,ACCESSED_SEARCH_DATE = '".date("d-m-Y")."',MOD_OPT = 'optLo' WHERE LOGIN_TIME = '".$login."' AND USER_NAME ='".$_SESSION["PK_USERS"]."'";
				$rs = executeOCIUpdateQuery($insQuery,$ds_conn);
			break;
			
			case "pageaccessed":
				//$rs = $db->executeQuery("INSERT INTO ET_SESSION_AUDIT (MODULE, USER_NAME, GROUP_NAME, ACCESSED_PAGE, ACCESSED_ON,ACCESSED_SEARCH_DATE,MOD_OPT) VALUES ('".$_SESSION["MODULE"]."','".$_SESSION["PK_USERS"]."','".$_SESSION["FK_GROUPS"]."','".$pagelink."', '".date("d-m-Y H:i:s")."','".date("d-m-Y")."','optPa')");
				$insQuery = "INSERT INTO ET_SESSION_AUDIT (MODULE, USER_NAME, GROUP_NAME, ACCESSED_PAGE, ACCESSED_ON,ACCESSED_SEARCH_DATE,MOD_OPT) VALUES ('".$_SESSION["MODULE"]."','".$_SESSION["PK_USERS"]."','".$_SESSION["FK_GROUPS"]."','".$pagelink."', '".date("d-m-Y H:i:s")."','".date("d-m-Y")."','optPa')";
				$rs = executeOCIUpdateQuery($insQuery,$ds_conn);
			break;
			
			case "datasource":				
				$dsname = $_SESSION["DATASOURCE"];
				$dsversion = $_SESSION["VERSION"];				
				$rs = $db->executeQuery("INSERT INTO ET_SESSION_AUDIT (MODULE, USER_NAME, GROUP_NAME, DS_NAME, DS_HOST, DS_VERSION, DS_ACCESSED_ON, ACCESSED_ON,ACCESSED_SEARCH_DATE,MOD_OPT) VALUES ('".$_SESSION["MODULE"]."','".$_SESSION["PK_USERS"]."','".$_SESSION["FK_GROUPS"]."','".$dsname."','".$_SESSION["DS_HOST"]."', '".$dsversion."', '".date("d-m-Y H:i:s")."','".date("d-m-Y H:i:s")."','".date("d-m-Y")."','optDa')");
			break;
		}		
	}
?>