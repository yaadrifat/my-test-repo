<?PHP
class fieldXSL {
	public $fieldType;
	public $fieldDataType;
	public $fieldName;
	public $fieldMandatory;
	public $fieldId;
	public $xsl;
	public $js;
	
	public function getFieldXSL() {
		$this->xsl = "";
		$this->js = "";
		$fontClass = "";
		
		if ($this->fieldMandatory == 1) {
			$fontClass = '<font class = "mandatory">*</font>';
			if ($this->fieldDataType == "MR" || $this->fieldDataType == "MC" ) {
				$this->js = ' if (!(validate_chk_radio("'.$this->fieldName.'",formobj.'.$this->fieldId.'))) return false ; ';  
			} else {
				$this->js = ' if (!(validate_col("'.$this->fieldName.'",formobj.'.$this->fieldId.'))) return false ; ';
			}
		}
		if ($this->fieldType == "C") {
			$this->xsl .= '<td width="15%" align = "left" ><label   id = "'.$this->fieldId.'_id" >'.$this->fieldName.'</label>  &#xa0;&#xa0;&#xa0;</td>';
			$this->js = '';
		} else {
			switch ($this->fieldDataType) {
				case "ET":
					$this->xsl .= '<td width="35%" align = "left" >';
					$this->xsl .= '<label   id = "'.$this->fieldId.'_id" for = "'.$this->fieldId.'" ><b>'.$this->fieldName;
					$this->xsl .= $fontClass;
					$this->xsl .= '</b></label>  &#xa0;&#xa0;&#xa0;</td><td>';
					$this->xsl .= '<span id="'.$this->fieldId.'_span">';
					$this->xsl .= '<input  id = "'.$this->fieldId.'" name = "'.$this->fieldId.'" flddatatype = "T" type = "text" maxlength = "100" size = "20"';
					$this->xsl .= ' onChange="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  >';
					$this->xsl .= '<xsl:attribute name="value"><xsl:value-of select="'.$this->fieldId.'"/></xsl:attribute>';
					$this->xsl .= '</input>    </span></td>';
					break;
				case "EN":
					$this->xsl .= '<td width="35%" align = "left" >';
					$this->xsl .= '<label   id = "'.$this->fieldId.'_id" for = "'.$this->fieldId.'" ><b>'.$this->fieldName;
					$this->xsl .= $fontClass;
					$this->xsl .= '</b></label>  &#xa0;&#xa0;&#xa0;</td><td>';
					$this->xsl .= '<span id="'.$this->fieldId.'_span">';
					$this->xsl .= '<input  id = "'.$this->fieldId.'" name = "'.$this->fieldId.'" flddatatype = "N" type = "text" maxlength = "100" size = "20"';
					$this->xsl .= ' onChange="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  >';
					$this->xsl .= '<xsl:attribute name="value"><xsl:value-of select="'.$this->fieldId.'"/></xsl:attribute>';
					$this->xsl .= '</input>    </span></td>';
					$this->js .= ' if (! formobj.'.$this->fieldId.'.disabled) { if(isDecimal(formobj.'.$this->fieldId.'.value) == false) {  alert("Please enter a valid Number");  formobj.'.$this->fieldId.'.focus();  return false; } }';
					break;
				case "ED":
					$this->xsl .= '<td width="35%" align = "left" >';
					$this->xsl .= '<label   id = "'.$this->fieldId.'_id" for = "'.$this->fieldId.'" ><b>'.$this->fieldName;
					$this->xsl .= $fontClass;
					$this->xsl .= '</b></label>  &#xa0;&#xa0;&#xa0;</td><td>';
					$this->xsl .= '<span id="'.$this->fieldId.'_span">';
					$this->xsl .= '<input  id = "'.$this->fieldId.'" name = "'.$this->fieldId.'" flddatatype = "D" type = "text" size = "10"';
					$this->xsl .= ' onChange="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  ';
					//$this->xsl .= ' onBlur="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  > '; ///fixed for the bug #21558
                                        $this->xsl .= ' onBlur="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')" class="datefield"  > ';
					$this->xsl .= '<xsl:attribute name="value"><xsl:value-of select="'.$this->fieldId.'"/></xsl:attribute>';
					$this->xsl .= '</input> ';
					//$this->xsl .= '<A onClick="javascript:return fnShowCalendar(document.er_fillform1.'.$this->fieldId.')"><img src="../jsp/images/calendar.jpg" border="0" onMouseover="this.style.cursor=\'hand\'" onMouseOut="this.style.cursor=\'arrow\'"></img></A> '; ///fixed for the bug #21558
					$this->xsl .= '</span></td>';
					$this->js .= '  if (! formobj.'.$this->fieldId.'.disabled) { if (!(validate_date(formobj.'.$this->fieldId.'))) return false ; }';
					break;
				case "MD":
					$this->xsl .= '<td width="35%" align = "left" >';
					$this->xsl .= '<label   id = "'.$this->fieldId.'_id" ><b>'.$this->fieldName;
					$this->xsl .= $fontClass;
					$this->xsl .= '</b></label>  &#xa0;&#xa0;&#xa0;</td><td><span id="'.$this->fieldId.'_span">';
					$this->xsl .= '<select  id = "'.$this->fieldId.'" name = "'.$this->fieldId.'"';
					$this->xsl .= ' onChange="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  ><option>';
					$this->xsl .= '<xsl:attribute name="value"></xsl:attribute>Select an Option</option>';
					$this->xsl .= '<xsl:for-each select = "'.$this->fieldId.'/resp"><option><xsl:attribute name="value"><xsl:value-of select="@dataval"/></xsl:attribute>';
					$this->xsl .= '<xsl:if test="@selected=\'1\'"><xsl:attribute name="selected"/></xsl:if> ';
					$this->xsl .= '<xsl:value-of select="@dispval"/></option></xsl:for-each>';
					$this->xsl .= '</select> </span></td>';
					break;
				case "MR":
					$this->xsl .= '<xsl:variable name = "colcount" select = "'.$this->fieldId.'/@colcount" />';
					$this->xsl .= '<td width="35%" align = "left" >';
					$this->xsl .= '<label   id = "'.$this->fieldId.'_id" ><b>'.$this->fieldName;
					$this->xsl .= $fontClass;
					$this->xsl .= '</b></label>  &#xa0;&#xa0;&#xa0;</td><td><table width="100%" border = "0"><tr><td>';
					$this->xsl .= '<span id="'.$this->fieldId.'_span">';
					$this->xsl .= '<table width="100%">';
					$this->xsl .= '<xsl:for-each select = "'.$this->fieldId.'/resp"><td width="100%">';
					$this->xsl .= '<input  name = "'.$this->fieldId.'" type = "radio" ';
					$this->xsl .= ' ondblclick= "this.checked = false; performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.'); "';
					$this->xsl .= ' onClick="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  >';
					$this->xsl .= '<xsl:attribute name="value"><xsl:value-of select="@dataval"/></xsl:attribute>';
					$this->xsl .= '<xsl:attribute name="id"><xsl:value-of select="@dispval"/></xsl:attribute>';
					$this->xsl .= '<xsl:if test="@checked=\'1\'"><xsl:attribute name="checked"/></xsl:if>';
					$this->xsl .= '<xsl:value-of select="@dispval"/></input></td>';
					$this->xsl .= '<xsl:if test = "number(position() mod $colcount)=0"><tr></tr></xsl:if>';
					$this->xsl .= '</xsl:for-each></table></span></td></tr></table></td>';
					break;
				case "MC":
					$this->xsl .= '<xsl:variable name = "colcount" select = "'.$this->fieldId.'/@colcount" />';
					$this->xsl .= '<td width="35%" align = "left" >';
					$this->xsl .= '<label   id = "'.$this->fieldId.'_id" ><b>'.$this->fieldName;
					$this->xsl .= $fontClass;
					$this->xsl .= '</b></label>  &#xa0;&#xa0;&#xa0;</td><td><table width="100%" border = "0"><tr><td>';
					$this->xsl .= '<span id="'.$this->fieldId.'_span">';
					$this->xsl .= '<table width="100%">';
					$this->xsl .= '<xsl:for-each select = "'.$this->fieldId.'/resp"><td width="100%">';
					$this->xsl .= '<input  name = "'.$this->fieldId.'" type = "checkbox" ';
					$this->xsl .= ' onClick="performAction(document.er_fillform1,document.er_fillform1.'.$this->fieldId.')"  >';
					$this->xsl .= '<xsl:attribute name="value"><xsl:value-of select="@dataval"/></xsl:attribute>';
					$this->xsl .= '<xsl:attribute name="id"><xsl:value-of select="@dispval"/></xsl:attribute>';
					$this->xsl .= '<xsl:if test="@checked=\'1\'"><xsl:attribute name="checked"/></xsl:if>';
					$this->xsl .= '<xsl:value-of select="@dispval"/></input></td><xsl:if test = "number(position() mod $colcount)=0"><tr></tr></xsl:if>';
					$this->xsl .= '</xsl:for-each></table></span></td></tr></table></td>';
					break;
			}
		}


	}


}


 
 ?>