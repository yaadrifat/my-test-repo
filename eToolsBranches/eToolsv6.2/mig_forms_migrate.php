<?php
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Form Data Migration</title>

<?php
include("db_config.php");
include("./includes/header.php");
include_once "./adodb/adodb.inc.php";
?>	
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Form Import</div><br>
<a href="./mig_forms.php?pk_vlnk_adaptor=<?PHP echo $_REQUEST["pk_vlnk_adaptor"] ?>">Back to Migration Page</a>
<br><br>
<?php

include_once "./adodb/adodb.inc.php";
$db = NewADOConnection("oci8");
$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);

$v_adapmod = $_GET["pk_vlnk_adapmod"];
$v_form = $_GET["form"];
$v_form_linkto = $_GET["form_linkto"];

$recordSet = $db->Execute("select a.form_xml.getclobval() as xml from er_formlib a where pk_formlib = $v_form");
$v_formxml =  $recordSet->fields[0];

$recordSet = $db->Execute("SELECT MAX(pk_formlibver) AS pk_formlibver FROM ER_FORMLIBVER WHERE fk_formlib = $v_form");
$v_formlibver =  $recordSet->fields[0];

$v_collist = "";
$v_counter = 0;
$v_sql = "select mp_systemid, mp_mapcolname from er_mapform where fk_form = $v_form order by mp_sequence";
$rs = $db->Execute($v_sql);
if(!$rs){
    print $source_db->ErrorMsg();
}else{
    while (!$rs->EOF) {
        $v_collist .= $rs->fields["MP_MAPCOLNAME"] . ",";
        $v_systemids[$v_counter] = $rs->fields["MP_SYSTEMID"];
        $v_counter++;
        $rs->MoveNext();
    }
}

switch ($v_form_linkto) {
    case "A":
	$v_sql = "SELECT ".$v_collist."pk_impacctform,nvl(fk_filledform,-1) fk_filledform, fk_account,filldate,form_completed,is_valid,notification_sent,creator,ip_Add,custom_col FROM eres.er_impacctform WHERE fk_form = $v_form and fk_formlibver = $v_formlibver and (export_flag = 9 or export_flag = -1) and custom_col = '$v_adapmod'";
	break;
    case "S":
	$v_sql = "SELECT ".$v_collist."pk_impstudyform, nvl(fk_filledform,-1) fk_filledform, fk_account, fk_study,filldate,form_completed,is_valid,notification_sent,creator,ip_Add,custom_col FROM eres.er_impstudyform WHERE fk_form = $v_form and fk_formlibver = $v_formlibver and (export_flag = 9 or export_flag = -1) and custom_col = '$v_adapmod' and fk_study is not null";
	break;
    case "P":
	$v_sql = "SELECT " .$v_collist . "pk_imppatform, nvl(fk_filledform,-1) fk_filledform, fk_account,fk_per,fk_patprot,filldate,form_completed,is_valid,notification_sent,custom_col,creator,ip_Add FROM eres.er_imppatform WHERE fk_form = $v_form and fk_formlibver = $v_formlibver and (export_flag = 9 or export_flag = -1) and custom_col = '$v_adapmod' and fk_per is not null";
	break;
}

//echo $v_sql;
//exit;


$v_collist = substr($v_collist,0,strlen($v_collist)-1);
$v_colcount = count($v_systemids);

$rs = $db->Execute($v_sql);

//echo $v_colcount.'<br><br>';

//echo '<pre>';
//print_r($rs);
//echo '</pre><br><br>';

if (!$rs){
    print $db->ErrorMsg();
}else{
	while (!$rs->EOF) {
		$v_usql = "";
		$v_datastr = "";
		for ($i=0; $i < $v_colcount; $i++) {
                    $v_dataval[$i] = $rs->fields["COL".($i+1)];
                    $v_datastr .= ",'".str_replace("'","''",$rs->fields["COL".($i+1)])."'";
		}
		$v_xml = updateXML($v_formxml,$v_systemids,$v_dataval);
                
		$v_patprot = "";
		switch ($v_form_linkto) {
		case "A":
			$v_pkimp = $rs->fields["PK_IMPACCTFORM"];
			$db->BeginTrans();
			$stmt = $db->PrepareSP("BEGIN velink.pkg_velink_import.sp_imp_formxml(:p_form, :p_id, :p_patprot, :p_formxml, :p_completed, :p_creator, :p_valid, :p_ipAdd, :p_type, :o_ret); END;");
			$db->InParameter($stmt,$v_form,'p_form',-1,SQLT_INT);
			$db->InParameter($stmt,$rs->fields["FK_ACCOUNT"],'p_id',-1,SQLT_INT);
			$db->InParameter($stmt,$v_patprot,'p_patprot',-1,SQLT_CHR);
			$db->InParameter($stmt,$v_xml,'p_formxml',-1,SQLT_CLOB );
			$db->InParameter($stmt,$rs->fields["FORM_COMPLETED"],'p_completed',-1,SQLT_INT);
			$db->InParameter($stmt,$rs->fields["CREATOR"],'p_creator',-1,SQLT_INT);
			$db->InParameter($stmt,$rs->fields["IS_VALID"],'p_valid',-1,SQLT_CHR);
			$db->InParameter($stmt,$rs->fields["IP_ADD"],'p_ipAdd',-1,SQLT_CHR);
			$db->InParameter($stmt,$v_form_linkto,'p_type',-1,SQLT_CHR);
			$db->OutParameter($stmt,$v_filledform,'o_ret');
			$v_ok = $db->Execute($stmt);
			$vflg = 'on';
			if ($v_ok) {
                            $rs_1 = $db->execute("SELECT NVL(ACCTFORMS_FILLDATE,SYSDATE) FROM ER_ACCTFORMS WHERE PK_ACCTFORMS = $v_filledform");
                            $v_filleddate = $rs_1->fields[0];

                            $v_sql = "INSERT INTO er_formslinear(pk_formslinear,id,fk_filledform,fk_form,filldate,form_type,export_flag,creator,created_on,form_completed,".$v_collist.")
                            VALUES (seq_er_formslinear.nextval,".$rs->fields["FK_ACCOUNT"].", $v_filledform,$v_form,'".$v_filleddate."','A',0 ,".$rs->fields["CREATOR"].",sysdate,".$rs->fields["FORM_COMPLETED"].$v_datastr.")";

                            $v_ok = $db->Execute($v_sql);
			}
			if ($v_ok) {
				$v_usql = "update er_impacctform set export_flag = 0 where pk_impacctform = $v_pkimp";
			} else {
				$v_usql = "update er_impacctform set export_flag = -1 where pk_impacctform = $v_pkimp";
			}
			break;
		case "S":

			$existStdy = "select * from ER_Linkedforms where fk_formlib = $v_form and lf_displaytype in ('S','SA')";
			$eStdyrs = $db->Execute($existStdy);
			
			if($eStdyrs->fields["PK_LF"]>0){
			
				if(trim($eStdyrs->fields["LF_DISPLAYTYPE"])=='S'){
					$eStdy = $eStdyrs->fields["FK_STUDY"];	
				}else{	
					$eStdy = $rs->fields["FK_STUDY"];
				}
			}
			$v_pkimp = trim($rs->fields["PK_IMPSTUDYFORM"]);
			$v_fkstdy = trim($rs->fields["FK_STUDY"]);

			
			if($eStdy == $v_fkstdy){
				
				$db->BeginTrans();
				$stmt = $db->PrepareSP("BEGIN velink.pkg_velink_import.sp_imp_formxml(:p_form, :p_id, :p_patprot, :p_formxml, :p_completed, :p_creator, :p_valid, :p_ipAdd, :p_type, :o_ret); END;");
				$db->InParameter($stmt,$v_form,'p_form',-1,SQLT_INT);
				$db->InParameter($stmt,$rs->fields["FK_STUDY"],'p_id',-1,SQLT_INT);
				$db->InParameter($stmt,$v_patprot,'p_patprot',-1,SQLT_CHR);
				$db->InParameter($stmt,$v_xml,'p_formxml',-1,SQLT_CLOB );
				$db->InParameter($stmt,$rs->fields["FORM_COMPLETED"],'p_completed',-1,SQLT_INT);
				$db->InParameter($stmt,$rs->fields["CREATOR"],'p_creator',-1,SQLT_INT);
				$db->InParameter($stmt,$rs->fields["IS_VALID"],'p_valid',-1,SQLT_CHR);
				$db->InParameter($stmt,$rs->fields["IP_ADD"],'p_ipAdd',-1,SQLT_CHR);
				$db->InParameter($stmt,$v_form_linkto,'p_type',-1,SQLT_CHR);
				$db->OutParameter($stmt,$v_filledform,'o_ret');	
				$v_ok = $db->Execute($stmt);			
				$vflg = 'on';
			}
			if ($v_ok) {
				$rs_1 = $db->execute("SELECT NVL(STUDYFORMS_FILLDATE,SYSDATE) FROM ER_STUDYFORMS WHERE PK_STUDYFORMS = $v_filledform");
				$v_filleddate = $rs_1->fields[0];

				$v_sql = "INSERT INTO er_formslinear(pk_formslinear,id,fk_filledform,fk_form,filldate,form_type,export_flag,creator,created_on,form_completed,".$v_collist.")
				VALUES (seq_er_formslinear.nextval,".$rs->fields["FK_STUDY"].", $v_filledform,$v_form,'".$v_filleddate."','S',0 ,".$rs->fields["CREATOR"].",sysdate,".$rs->fields["FORM_COMPLETED"].$v_datastr.")";
				
				$v_ok = $db->Execute($v_sql);
			}
			if ($v_ok) {
				$v_usql = "update er_impstudyform set export_flag = 0 where PK_IMPSTUDYFORM = $v_pkimp";
			} else {
				$v_usql = "update er_impstudyform set export_flag = -1 where PK_IMPSTUDYFORM = $v_pkimp";
			}
			break;
		case "P":                 
			$v_pkimp = $rs->fields["PK_IMPPATFORM"];

			$db->BeginTrans();
			$v_patprot = ($rs->fields["FK_PATPROT"] == 0 ?'NULL':$rs->fields["FK_PATPROT"]);
			$stmt = $db->PrepareSP("BEGIN velink.pkg_velink_import.sp_imp_formxml(:p_form, :p_id, :p_patprot, :p_formxml, :p_completed, :p_creator, :p_valid, :p_ipAdd, :p_type, :o_ret); END;");
                        
                        //echo $v_form.'<br>'.$rs->fields["FK_PER"].'<br>'.$v_patprot.'<br>'.'$v_xml: '.$v_xml.'<br>'.$rs->fields["FORM_COMPLETED"].'<br>'.$rs->fields["CREATOR"].'<br>'.'IS_VALID: '.$rs->fields["IS_VALID"].'<br>'.'IP_ADD: '.$rs->fields["IP_ADD"].'<br>'.$v_form_linkto.'<br>'.'$v_filledform: '.$v_filledform.'<br><br><br><br>';
                        //exit;
                        

                        
			$db->InParameter($stmt,$v_form,'p_form',-1,SQLT_INT);
			$db->InParameter($stmt,$rs->fields["FK_PER"],'p_id',-1,SQLT_INT);
			$db->InParameter($stmt,$v_patprot,'p_patprot',-1,SQLT_CHR);
			$db->InParameter($stmt,$v_xml,'p_formxml',-1,SQLT_CLOB );
                        //$db->InParameter($stmt,$v_formxml,'p_formxml',-1,SQLT_CLOB );
			$db->InParameter($stmt,$rs->fields["FORM_COMPLETED"],'p_completed',-1,SQLT_INT);
			$db->InParameter($stmt,$rs->fields["CREATOR"],'p_creator',-1,SQLT_INT);
			$db->InParameter($stmt,$rs->fields["IS_VALID"],'p_valid',-1,SQLT_CHR);
			$db->InParameter($stmt,$rs->fields["IP_ADD"],'p_ipAdd',-1,SQLT_CHR);
			$db->InParameter($stmt,$v_form_linkto,'p_type',-1,SQLT_CHR);
			$db->OutParameter($stmt,$v_filledform,'o_ret');
			$vflg = 'on';
                        
//                        echo '<pre>';
//                            print_r($stmt);
//                            echo '</pre>';
//                        exit;                        
                        
			$v_ok = $db->Execute($stmt);
			if ($v_ok) {
				$rs_1 = $db->execute("SELECT NVL(PATFORMS_FILLDATE,SYSDATE) FROM ER_PATFORMS WHERE PK_PATFORMS = $v_filledform");
				$v_filleddate = $rs_1->fields[0];
				$v_sql = "INSERT INTO er_formslinear(pk_formslinear,id,fk_patprot,fk_filledform,fk_form,filldate,form_type,export_flag,creator,created_on,form_completed,".$v_collist.")
				VALUES (seq_er_formslinear.nextval,".$rs->fields["FK_PER"].",".$v_patprot.", $v_filledform,$v_form,'".$v_filleddate."','P',0 ,".$rs->fields["CREATOR"].",sysdate,".$rs->fields["FORM_COMPLETED"].$v_datastr.")";
				$v_ok = $db->Execute($v_sql);
			}
			if ($v_ok) {
				$v_usql = "update er_imppatform set export_flag = 0 where PK_IMPPATFORM = $v_pkimp";
			} else {
				$v_usql = "update er_imppatform set export_flag = -1 where PK_IMPPATFORM = $v_pkimp";
			}
			break;
		}
		if ($v_ok) {
			$db->CommitTrans();
			$v_ok = $db->Execute($v_usql);
		} else {
			if($vflg=='on'){
                            echo $db->ErrorMsg().$v_sql."<BR><BR>";
                            $v_error = $db->ErrorMsg();
                            $db->RollbackTrans();
			}
			$v_error = 'Selected form is not associated with selected study';
			$v_ok = $db->Execute($v_usql);
			$v_sql = "insert into velink.vlnk_log(PK_VLNK_LOG, TIMESTAMP, STATUS, MESSAGE, FK_VLNK_ADAPMOD,rid,message_long) values
            (velink.seq_vlnk_log.nextval,sysdate,-2,'".$v_error."' ,".$rs->fields["CUSTOM_COL"].",$v_pkimp,'".str_replace("'","''",$v_sql)."')";
			$v_ok = $db->Execute($v_sql);
			if ((!$v_ok) && ($vFlg=="on"))  echo $db->ErrorMsg()."<BR>".$v_sql."<BR><BR>";
			$v_sql = "";
		}

		$rs->MoveNext();		
		
	}
}
$db->Close();
if($vflg=='on'){
	echo "Form data migrated successfully, review migration log.";
}else{
	echo "Form data migration is not successfull, review migration log.";
}
$url = "mig_forms.php?pk_vlnk_adaptor=".$_GET["pk_vlnk_adaptor"]."&refresh=0";

echo "</div></body></html>";
	
}
else header("location: ./index.php?fail=1");

function updateXML($p_formxml,$p_systemids,$p_dataval) {
    $doc = new domDocument;
    $doc->preserveWhiteSpace = false;
    $doc->encoding = 'utf-8';
    $doc->loadXML($p_formxml);

    $v_colcount = count($p_systemids);
    for ($i = 0; $i < $v_colcount; $i++){
        $items = $doc->getElementsByTagName($p_systemids[$i]);
        foreach ($items as $item) {
            $v_type =  $item->getAttribute("type");
            $v_cbdata = "";
            if ($v_type == "MD" || $v_type == "MR" || $v_type == "MC") {
                $v_clCount = ($item->hasAttribute("colcount"))?$item->getAttribute("colcount"):"";
                if (($v_type == "MR") || ($v_type == "MC")) {
                        $v_multiType = "checked";
                } else if ($v_type == "MD") {
                        $v_multiType = "selected";
                }
                $v_responses = $item->getElementsByTagName("resp");
                if ($v_type == "MC"){
                    foreach ($v_responses as $v_response) {
                        if (strpos("strpos".$p_dataval[$i],'['.$v_response->getAttribute("dispval").']')){
                            $v_response->setAttribute($v_multiType,"1");
                            if (empty($v_cbdata) ) {
                                $v_cbdata = "[". $v_response->getAttribute("dispval")."]";
                            } else {
                                $v_cbdata .= ", [". $v_response->getAttribute("dispval")."]";
                            }
                        } else {
                            $v_response->setAttribute($v_multiType,"0");
                        }
                    }
                } else {
                    foreach ($v_responses as $v_response) {
                        if (strpos("strpos".$p_dataval[$i],$v_response->getAttribute("dispval").'[VELSEP1]'.$v_response->getAttribute("dataval"))){
                            $v_response->setAttribute($v_multiType,"1");
                            $v_cbdata = $v_response->getAttribute("dispval");
                        } else {
                            $v_response->setAttribute($v_multiType,"0");
                        }
                    }
                }
                $item->setAttribute("checkboxesdata",$v_cbdata);
            } else {
                    $item->nodeValue = $p_dataval[$i];
            }
        }
    }
    $doc->formatOutput = true;
    return $doc->saveXML();
}
?>
