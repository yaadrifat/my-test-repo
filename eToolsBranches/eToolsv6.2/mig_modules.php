<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Data</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

$pk_vlnk_adaptor = $_GET["pk_vlnk_adaptor"];
if (isset($_GET["name"])) {
	$v_name = $_GET["name"];
	$v_desc = $_GET["desc"];
	$v_uname = $_GET["uname"];
} else {
	$query_sql = "SELECT pk_vlnk_adaptor, adaptor_name, adaptor_desc, (select usr_firstname || ' ' || usr_lastname from er_user where pk_user = a.creator) as username,to_char(created_on,'mm/dd/yyyy hh24:mi:ss') as created_on from velink.vlnk_adaptor a where pk_vlnk_adaptor = $pk_vlnk_adaptor";
	$results = executeOCIQuery($query_sql,$ds_conn);
	$v_name = $results["ADAPTOR_NAME"][0];
	$v_desc = $results["ADAPTOR_DESC"][0];
	$v_uname = $results["USERNAME"][0];
}

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Migrate Data - Standard</div>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php
echo "<table width='100%'><tr>";
echo "<td width='25%'><b>Name: ".$v_name."</b></td>";
echo "<td width='50%'><b>Description: ".$v_desc."</b></td>";
echo "<td width='25%'><b>Audit User Name: ".$v_uname."</b></td>";
echo "</tr></table>";
echo "<BR><a href=mig_new.php?pk_vlnk_adaptor=".$pk_vlnk_adaptor.">New migration</a>";
?>
<Table border="1" width="100%"><TR>
<TH>NAME</TH>
<TH>MODULE</TH>
<TH>TABLE NAME</TH>
<TH>MAP FIELDS</TH>
<TH>STAGING AREA</TH>
<TH>MAP RESPONSES</TH>
<TH>IMPORT INTO APPLICATION</TH>
<TH>VALIDATION</TH>
</TR>
<?php

$query_sql = "SELECT pk_vlnk_adapmod,pk_vlnk_module,module_name,module_desc,table_name,adapmod_name,
(select count(*) from velink.vlnk_adapmodcol where fk_vlnk_adapmod = pk_vlnk_adapmod) as map_fields,
(select count(*) from velink.vlnk_modcol where fk_vlnk_module = pk_vlnk_module and is_required = 1) as req_fields,
(select count(*) from velink.vlnk_modcol,velink.vlnk_adapmodcol where pk_vlnk_adapmod = fk_vlnk_adapmod and map_col_name is not null and pk_vlnk_modcol = fk_vlnk_modcol and fk_vlnk_module = pk_vlnk_module and is_required = 1) as mapreq_fields,
(select count(*) from velink.vlnk_pipe where fk_vlnk_adapmod = pk_vlnk_adapmod) as pipe_count,
(select count(*) from velink.vlnk_pipemap where fk_vlnk_adapmod = pk_vlnk_adapmod) as pipemap_count,
(select count(*) from velink.vlnk_pipemap where fk_vlnk_adapmod = pk_vlnk_adapmod and target_id = 0) as pipemapt_count,
(select count(*) from velink.vlnk_pipe where fk_vlnk_adapmod = pk_vlnk_adapmod and process_flag is not null) as mig_count,
(select count(*) from velink.vlnk_pipe where fk_vlnk_adapmod = pk_vlnk_adapmod and process_flag = -1) as migerr_count,
(select count(*) from velink.vlnk_pipe where fk_vlnk_adapmod = pk_vlnk_adapmod and process_flag = 2) as migdup_count
FROM velink.VLNK_ADAPMOD, velink.VLNK_MODULE 
WHERE  fk_vlnk_module = pk_vlnk_module AND fk_vlnk_adaptor = ".$pk_vlnk_adaptor." and nvl(record_type,'N') <> 'D' order by 1 desc" ;
$results = executeOCIQuery($query_sql,$ds_conn);
$total_rows = $results_nrows;
for ($rec = 0; $rec < $total_rows; $rec++){
	$results1 = executeOCIQuery("select count(*) as count from dba_tables where owner = 'ERES' and table_name = '".strtoupper($results["TABLE_NAME"][$rec])."'",$ds_conn);
?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">	
	<TD><?php echo $results["ADAPMOD_NAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["MODULE_NAME"][$rec] . "&nbsp;"; ?></TD>
	<TD>
	<?php 
	if ($results1["COUNT"][0] == 0) {
		echo $results["TABLE_NAME"][$rec]; 
	} else {
		$results1 = executeOCIQuery('select count(*) as count from '.$results["TABLE_NAME"][$rec],$ds_conn);
		echo "<a href='mig_table_view.php?table=".$results["TABLE_NAME"][$rec]."&pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."'>".$results["TABLE_NAME"][$rec].' ('.(($results1["COUNT"][0])? $results1["COUNT"][0]:"").")</a>"; 
	}
	?>
	</TD>
<?php
	echo "<td>";
	$v_mappingDefined = true;
	if ($results["MAP_FIELDS"][$rec] == 0) {
		echo "<img src='./img/red.png' />";
		$v_mappingDefined = false;
	} elseif ($results["REQ_FIELDS"][$rec] != $results["MAPREQ_FIELDS"][$rec]) {
		echo "<img src='./img/yellow.png' />";
	} elseif ($results["REQ_FIELDS"][$rec] == $results["MAPREQ_FIELDS"][$rec]) {
		echo "<img src='./img/green.png' />";
	}
	echo "<a href=mig_mod_columns.php?pk_vlnk_module=".$results["PK_VLNK_MODULE"][$rec]."&pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&tablename=".$results["TABLE_NAME"][$rec].">Edit</a></td>";
	echo "<td>";
	$v_pipedata = true;
	if ($results["PIPE_COUNT"][$rec] == 0) {
		$v_pipedata = false;
		echo "<img src='./img/yellow.png' />";
	} else {
		echo "<img src='./img/green.png' />";
	}

	if ($v_mappingDefined) {
		echo "<a href=mig_imp2staging.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&pk_vlnk_adaptor=".$pk_vlnk_adaptor."&rows=0&mode=import>Import</a>";
		if ($results["PIPE_COUNT"][$rec] != 0) {
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='mig_table_view.php?table=VLNK_PIPE&pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."'>View"; 
			echo " (".$results["PIPE_COUNT"][$rec].")";
			echo "</a>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=mig_imp2staging.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&pk_vlnk_adaptor=".$pk_vlnk_adaptor."&rows=".$results["PIPE_COUNT"][$rec]."&mode=empty>Empty</a>"; 
		} else echo "<font color='#d1cebf'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;View&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Empty</font>";
	} else {
		echo "<font color='#d1cebf'>Import&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;View&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Empty</font>";
	}

	echo "</td><td>";
	if ($results["PIPEMAP_COUNT"][$rec] == 0) {
		echo "<img src='./img/yellow.png' />";
	} elseif ($results["PIPEMAPT_COUNT"][$rec] == 0) {
		echo "<img src='./img/green.png' />";
	} else {
		echo "<img src='./img/yellow.png' />";
	}
	if ($v_mappingDefined && $v_pipedata) {
		echo "<a href=mig_datamapping.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&refresh=1>Edit</a>";
	} else {
		echo "<font color='#d1cebf'>Edit</font>";
	}

	echo "<td>";
	if ($results["MIG_COUNT"][$rec] == 0) {
		echo "<img src='./img/yellow.png' />";
	} elseif ($results["MIGERR_COUNT"][$rec] > 0) {
		echo "<img src='./img/yellow.png' />";
	} elseif ($results["MIGDUP_COUNT"][$rec] > 0 || $results["MIG_COUNT"][$rec] == 0){
		echo "<img src='./img/yellow.png' />";
	}else{
		echo "<img src='./img/green.png' />";		
	}
	if ($v_mappingDefined && $v_pipedata) {
		echo "<a href=mig_data.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&pk_vlnk_adaptor=".$pk_vlnk_adaptor.">Import</a>";
	} else {
		echo "<font color='#d1cebf'>Migrate</font>";
	}
	if ($v_mappingDefined && $v_pipedata) {
		echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=mig_data_log.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec].">View Log</a>";
	} else {
		echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color='#d1cebf'>View Log</font>";
	}
	echo "</td>";
	echo "<td align=center>";
	if ($v_mappingDefined) {
		echo "<a href=mig_validate.php?pk_vlnk_adapmod=".$results["PK_VLNK_ADAPMOD"][$rec]."&pk_vlnk_adaptor=".$pk_vlnk_adaptor.">Validate</a>";
	} else {
		echo "<font color='#d1cebf'>Validate</font>";
	}
	echo "</td>";

	echo "</TR>";
}
?>
</TABLE>
<BR><BR>
<Table class="ttip" border="0" width="100%"><TR>
<TH>MAP FIELDS</TH>
<TH>STAGING AREA</TH>
<TH>MAP RESPONSES</TH>
<TH>IMPORT INTO APPLICATION</TH>
</TR>
<tr>
<td>
<img src='./img/yellow.png' /> - Mandatory Fields not mapped
<br><img src='./img/green.png' /> - All Mandatory Fields  mapped
</td>
<td>
<img src='./img/yellow.png' /> - No data in staging area
<br><img src='./img/green.png' /> - Data exists in staging area
</td>
<td>
<img src='./img/yellow.png' /> - Responses not mapped
<br><img src='./img/green.png' /> - Response mapping exists
</td>
<td>
<img src='./img/yellow.png' /> - Error migrating some records
<br><img src='./img/green.png' /> - Data migrated (check log)
</td>
</tr>
</table>

      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
