<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
    <title>Velos eTools -> Save Query</title>
</head>
<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");
include_once "./adodb/adodb.inc.php";
include_once('./adodb/adodb-pager.inc.php'); 
require_once('audit_queries.php');

$db = NewADOConnection("oci8");
$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]);	

if (!$db) die("Connection failed");

?>
<body>
<div id="fedora-content">	
<?php

$v_query = html_entity_decode(stripslashes($_POST['query']));
$v_queryname = $_POST["queryname"];
$v_querydesc = $_POST["querydesc"];
$queryfil = $_POST['queryfil'];
$oldquery = $v_query;

$qryexplod = explode(" ", $v_query);
$qryexplodrs = [];
for($qe=0; $qe<sizeof($qryexplod); $qe++){
    if(substr($qryexplod[$qe],0,1) == ':'){
        $qryexplodrs[$qe] = $qryexplod[$qe];
    }
}
$qryexplodrs = array_values($qryexplodrs);
//$qryfilter = explode(",",$queryfil);
$qryfilter = $queryfil;


if(sizeof($qryfilter)>0){
    $trimmedqry = $v_query;
    for($qel=0; $qel<sizeof($qryexplodrs); $qel++){
        //$datestring = "to_date('".$qryfilter[$qel]."', 'dd-mm-yy')";
        $datestring = "to_date('".$qryfilter."', 'dd-mm-yy')";
        $trimmedqry = str_replace($qryexplodrs[$qel], $datestring, $trimmedqry);
    }
    $v_query = $trimmedqry; 
}
 

//if($queryfil != ''){
//    $pos = strpos($v_query, ":");
//    $trimmedqry = substr($v_query,0,$pos);
//    $v_query =  $trimmedqry."'".$queryfil."'";            
//}


if (empty($v_query) || empty($v_queryname)){
    echo "Query name or SQL missing, please try again";
}else{
    $saveFlag = 'yes';
    $rs = $db->Execute("select count(*) from ( ".$v_query.")");
    if(!$rs){
        echo "SQL ERROR: ";
        print $db->ErrorMsg();
    }else{
        if (empty($_POST["pk"])) {
            $recordSet = $db->Execute("select velink.seq_vlnk_queryman.nextval from dual");
            $pk = $recordSet->fields[0];
            $v_insert_sql = "insert into velink.vlnk_queryman (pk_queryman,query_name,query_desc,record_type) values (".$recordSet->fields[0].",'$v_queryname','$v_querydesc','N')";
            executeQuery(5, $v_query, $saveFlag);
            
            /*--- AUDIT ---*/
            $colArray = array('pk_queryman','query_name','query_desc','record_type');
            $colvalarray = array($recordSet->fields[0],$v_queryname,$v_querydesc,'N');
            $tblname = 'vlnk_queryman';
            $queryman = 'queryman';
            colQueries($colArray, $colvalarray, $tblname, $oldValues ,$queryman);
            /*--- END ---*/
        } else {
            /*--- AUDIT ---*/
            $oldValue = $db->Execute("SELECT query_name, query_desc, query  FROM velink.vlnk_queryman WHERE pk_queryman=".$_POST["pk"]);            
            $oldName = $oldValue->fields[0];
            $oldDec = $oldValue->fields[1];
            $oldQry = $oldValue->fields[2];
            $oldQry = str_replace("'",'"', $oldQry);
            $oldValue = array($oldName, $oldDec, $oldQry);
            /*--- END ---*/		


                
            $v_insert_sql = "update velink.vlnk_queryman set query_name = '$v_queryname',query_desc='$v_querydesc' where pk_queryman=".$_POST["pk"];
            $pk = $_POST["pk"];

            executeQuery(5, $v_query, $saveFlag, $_SESSION['pk']);
            /*--- AUDIT ---*/
            $v_query1 = str_replace("'",'"', $v_query);
            $colArray = array('query_name','query_desc','query', 'pk_queryman',);
            $colvalarray = array($v_queryname,$v_querydesc,$v_query1, $_POST["pk"],);
            $tblname = 'vlnk_queryman';
            $queryman = 'queryman';
            colQueries($colArray, $colvalarray, $tblname, $oldValue , $queryman);
            /*--- END ---*/		
        }

        $v_status = $db->Execute($v_insert_sql);
        if (!$v_status) print $db->ErrorMsg();
        $v_query = $oldquery;
        $v_status = $db->UpdateClob('velink.vlnk_queryman','query',$v_query,'pk_queryman='.$pk); 
        if (!$v_status) print $db->ErrorMsg();

        echo "Query Saved";

        $url = "./query_manager.php";
        echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";
    }

?>
</TABLE>
<?php
} ?>
</div>


</body>
</html>
<?php 
$db->Close();

}
else header("location: index.php?fail=1");
?>
