<?php
    
    //----- ENCRYPTION & DECRYPTION -----//
    /*----- Author: Nicholas L -----*/
    /*----- Build: 170 ------*/    
    //$key = 'bRuD5WYw5wd0rdHR9yLlM6wt2vteuiniQBqE70nAuhU=';
    $key = $_SESSION['fnkey'];
    function my_encrypt($data, $key) {
        $encryption_key = base64_decode($key);
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
        return base64_encode($encrypted.'::'.$iv);
    }

    function my_decrypt($data, $key) {
        $encryption_key = base64_decode($key);
        list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }
    
    function detect_delimiter($name) {
      $delimiters = array(
        ';' => 0,
        ',' => 0,
      );

      $handle = fopen($name, 'r');
      $first_line = fgets($handle);
      fclose($handle);
      foreach ($delimiters as $delimiter => &$count) {
        $count = count(str_getcsv($first_line, $delimiter));
      }

      return array_search(max($delimiters), $delimiters);
    }    

    function ds_delete($pk, $d){
        echo 'dddd';
        exit;
    }
    
?>
