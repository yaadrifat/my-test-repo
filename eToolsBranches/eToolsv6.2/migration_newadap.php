<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> New Migration Category</title>
<?php
include("db_config.php");
include("./includes/header.php");
include("./includes/oci_functions.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
<script language="javascript" type="text/javascript">
function validate(formobj){
	if (formobj.aName.value == ""){
		alert("Name cannot be blank.");
		return false;
	}
}
</script>
</head>
<body>
<!-- <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	

<?php

echo '<div class="navigate">Migrate Data - New Migration Category</div>';

if ($_SERVER['REQUEST_METHOD'] != 'POST') {	
$v_sql = "SELECT pk_user,usr_firstname || ' ' || usr_lastname AS username FROM ER_USER WHERE USR_STAT = 'A' ORDER BY lower(usr_firstname || ' ' || usr_lastname)";
$results = executeOCIQuery($v_sql,$ds_conn);
$v_dd = "";
for ($rec = 0; $rec < $results_nrows; $rec++){
	$results["USERNAME"][$rec] = str_replace(array("<script>","</script>"),array("",""),$results["USERNAME"][$rec]);
	$v_dd .= "<option value=".$results["PK_USER"][$rec]." SELECTED>".$results["USERNAME"][$rec]."</option>";
}

?>

	
<FORM name="newadap" action="migration_newadap.php" method=post onSubmit="if (validate(document.newadap) == false) return false;">

	<br><TABLE border="0">
	<TR><TD>Name</TD><TD><INPUT TYPE="TEXT" class="required" name="aName" id="aName" SIZE="50"></INPUT></TD></TR>
	<TR><TD>Description</TD><TD><INPUT TYPE="TEXT" NAME="aDesc" SIZE="50"></INPUT></TD></TR>
	<TR><TD>Audit Trail User</TD><TD><SELECT class="required" NAME="aUser"><?PHP echo $v_dd; ?></SELECT></TD></TR>
	</TABLE>

<BR><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />

</form>

<script type="text/javascript">
	if(document.getElementById("aName").value == '') {
		document.getElementById("aName").focus();
	}
</script>

<?php 
} else {

	$v_name = $_POST["aName"];
	$v_desc = $_POST["aDesc"];
	$v_user = $_POST["aUser"];
	$v_sql = "SELECT fk_account FROM ER_USER WHERE pk_user = ".$v_user;
	$results = executeOCIQuery($v_sql,$ds_conn);
	$v_account = $results["FK_ACCOUNT"][0];
	
	$v_sql = "insert into velink.vlnk_adaptor (pk_vlnk_adaptor,adaptor_name,adaptor_desc,fk_account,creator,record_type,created_on) values (velink.seq_vlnk_adaptor.nextval,'$v_name','$v_desc',$v_account,$v_user,'N',sysdate)";
	$results = executeOCIUpdateQuery($v_sql,$ds_conn);

	echo "Data Saved Successfully !!!"
?>
<meta http-equiv="refresh" content="0; url=./migration.php"> 
<?PHP

}
OCICommit($ds_conn);
OCILogoff($ds_conn);

	


?>
</div>

</body>
</html>


<?php
}
else header("location: index.php?fail=1");
?>
		
