<?php
session_start();
header("Cache-control: private");
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Code List</title>

</head>
<?php
include("./includes/oci_functions.php");
include("db_config.php");
require_once("audit_queries.php");
include('gen_functions.php');
include("./includes/header.php");
if (!isset($_POST['DS'])) {
    $ds = $_SESSION["DS"];
} else {
    $ds = $_POST['DS'];
}

$rs = "SELECT pk_ds,ds_name,ds_host,ds_port,ds_sid,ds_pass from et_ds where pk_ds=".$ds;
$rs = $db->query($rs);
//$rs = mysql_query("SELECT pk_ds,ds_name,ds_host,ds_port,ds_sid,ds_pass from et_ds where pk_ds=".$ds);
//while($row = mysql_fetch_array($rs)) {
while($row = $rs->fetch_assoc()) {
    $_SESSION["DS_HOST"] = $row["ds_host"];
    $_SESSION["DS_PORT"] = $row["ds_port"];
    $_SESSION["DS_SID"] = $row["ds_sid"];
    $_SESSION["DS_PASS"] = $row["ds_pass"];
    
    $_SESSION["DS"] = $row["pk_ds"];
    
    $db  = "(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)";
    $db .= "(HOST = ".$_SESSION["DS_HOST"].")(PORT = ".$_SESSION["DS_PORT"].")) )";
    $db .= "(CONNECT_DATA = (SERVICE_NAME = ".$_SESSION["DS_SID"].")  ) )";
    $_SESSION["DB"] = $db;
    //$key = 'bRuD5WYw5wd0rdHR9yLlM6wt2vteuiniQBqE70nAuhU=';        
    $password_decrypted = my_decrypt(trim($_SESSION["DS_PASS"]), $_SESSION['fnkey']);
    $_SESSION["DS_PASS"] = $password_decrypted;
    $ds_conn = ocilogon("eres",$password_decrypted, $db); 
    //$ds_conn = ocilogon("eres",$_SESSION["DS_PASS"], $db); 
    if ($ds_conn != '') {	
            $_SESSION["DATASOURCE"] = $row["ds_name"];
            $query_sql = "select ctrl_value from er_ctrltab where ctrl_key = 'app_version'";
            $results = executeOCIQuery($query_sql,$ds_conn);	
            $_SESSION["VERSION"] = $results["CTRL_VALUE"][0];
            $_SESSION["MODULE"] = "dbm";
            sessionTracking("datasource","");            
    }
}
if(empty($_GET['refsta'])){
     header('Location:loggedinusers.php?refsta=1');
     exit;
}

?>
<body>
<FORM action="codelist.php" method=post>
<div id="fedora-content">	

<?PHP

if ($ds_conn == ''){
	unset($_SESSION["DATASOURCE"]);
	unset($_SESSION["VERSION"]);
	unset($_SESSION["MODULE"]);
	echo '<script> alert("Datasource failed to connect"); </script>';
	echo '<meta http-equiv="refresh" content="0; url=./datasource.php">';		
}

$query="SELECT usr_firstname || ' ' || usr_lastname AS username , usr_logname,grp_name,  add_email, add_phone, to_char(ER_USER.last_modified_date,'dd-Mon-yy hh:mi:ss') AS logged_on, (select site_name from er_site where pk_site = fk_siteid) as site_name FROM ER_USER,ER_ADD,ER_GRPS WHERE pk_add = fk_peradd AND pk_grp =   fk_grp_default AND usr_loggedinflag = 1 and trunc(er_user.last_modified_date) = trunc(sysdate)";
$results = executeOCIQuery($query,$ds_conn);
echo "<B>Logged in Users: ".$results_nrows."</B>";
?>
<Table border="1"><TR>
    <TH width="10%">User Name</TH>
    <TH width="15%">Organization</TH>
    <TH width="10%">Login Name</TH>
    <TH width="10%">Group</TH>
    <TH width="20%">E-mail</TH>
    <TH width="10%">Phone</TH>
    <TH width="15%">Login Time</TH>
</TR>
<?php
for ($rec = 0; $rec < $results_nrows; $rec++){
?>
<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
    <TD><?php echo str_replace(array("<script>","</script>"),array("",""),$results["USERNAME"][$rec]). "&nbsp;"; ?></TD>
    <TD><?php echo $results["SITE_NAME"][$rec] . "&nbsp;"; ?></TD>
    <TD><?php echo str_replace(array("<script>","</script>"),array("",""),$results["USR_LOGNAME"][$rec]). "&nbsp;"; ?></TD>
    <TD><?php echo $results["GRP_NAME"][$rec] . " "; ?></TD>
    <TD><?php echo $results["ADD_EMAIL"][$rec] . "&nbsp;"; ?></TD>
    <TD><?php echo $results["ADD_PHONE"][$rec] . "&nbsp;"; ?></TD>
    <TD><?php echo $results["LOGGED_ON"][$rec] . "&nbsp;"; ?></TD>	
</TR>
<?php
}
//}
?>
</TABLE>
</div>


</body>
</html>
<?php
OCICommit($ds_conn);
OCILogoff($ds_conn);

}
else header("location: index.php?fail=1");
?>
