<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Users</title>

<?php
include("./includes/oci_functions.php");
include("db_config.php");
include("./includes/header.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>

<body>

<div id="fedora-content">	
<div class="navigate">Manage Account - Users</div>
<?PHP
echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=account_users_edit.php?mode=n&pk_users=0>New User</a>";

$rs = "SELECT pk_users, first_name, last_name, email, phone, group_name, fk_groups FROM et_users, et_groups WHERE et_groups.pk_groups = et_users.fk_groups order by group_name, first_name, last_name";
$rs = $db->query($rs);
//$rs = mysql_query("SELECT pk_users, first_name, last_name, email, phone, group_name, fk_groups FROM et_users, et_groups WHERE et_groups.pk_groups = et_users.fk_groups order by group_name, first_name, last_name");
?>
<Table border="1" width="100%"><TR>
<TH>NAME</TH>
<TH>GROUP</TH>
<TH>EMAIL</TH>
<TH>PHONE</TH>
<TH>&nbsp;</TH>
</TR>
<?php
while ($row = $rs->fetch_assoc()) {
?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';" >	
	<TD><?php echo htmlentities($row["first_name"]).' '. $row["last_name"]; ?></TD>
        <TD><?php echo $row["group_name"] ?></TD>
	<TD><?php echo $row["email"] ?></TD>
	<TD><?php 
            if($row["phone"] == 0){
                echo "";
            }else{
                echo $row["phone"];
            }
            ?></TD>
	<TD  align="center">
		<?PHP
			//----- Allowing admin level -------//
			if($_SESSION["FK_GROUPS"] == 1){
				if ($row["pk_users"] != 1){
					echo '<a href=account_users_edit.php?mode=m&pk_users='.$row["pk_users"].'>Edit</a>';
				}                            
			}else{
                            //---- fixed this for the bug #19614 (P1)
                            if($_SESSION["FK_GROUPS"] == $row["fk_groups"]){
                                echo '<a href=account_users_edit.php?mode=m&pk_users='.$row["pk_users"].'>Edit</a>';
                            }
			}
		?>
	</TD>
<?php
}
?>
</TABLE>
      </div>
</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
