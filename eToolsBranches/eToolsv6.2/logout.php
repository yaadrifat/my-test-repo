<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.
require_once("audit_queries.php");
// Clear session variable and redirect
if($_SESSION["user"]!= ''){    
    sessionTracking("loggedout");
    $_SESSION["user"] = '';
    session_destroy();
}
//header("location: ./index.php?logout=1");
echo '<meta http-equiv="refresh" content="0; url=./index.php">';
?>