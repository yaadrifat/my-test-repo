<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>
	<html>
	<head>    <title>Velos eTools -> Calendar from File</title>
<script>
function validate(formObj) {
	if (formObj.calname.value == ""){
		alert("Calendar name cannot be blank");
		formObj.calname.focus();
		return false;	
	}
	if (formObj.calduration.value == ""){
		alert("Calendar duration cannot be blank");
		formObj.calduration.focus();
		return false;	
	}
	if (formObj.delimiter.value == ""){
		alert("Delimiter cannot be blank");
		formObj.delimiter.focus();
		return false;	
	}
	if (formObj.uploadedfile.value == ""){
		alert("File name cannot be blank");
		formObj.uploadedfile.focus();
		return false;	
	}
} 
</script>
	</head>
	<?php
	include("./includes/header.php");
	include("./includes/oci_functions.php");
	include("./includes/calendar.php");

	$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
	<body>
	<div id="fedora-content">	
<?PHP
	if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
		$v_query = "SELECT pk_catlib,catlib_name || decode(ac_name,null,'',' [' || ac_name || ']') as catlib_name FROM ER_CATLIB,er_account WHERE fk_account <> 46 and pk_account = fk_account and catlib_type = 'L' order by ac_name || ' - ' || lower(catlib_name)";
		$results = executeOCIQuery($v_query,$ds_conn);
		$dd_lookup = '<select class="required" name="category">';
		for ($rec = 0; $rec < $results_nrows; $rec++){
			$dd_lookup .= '<option value="'.$results["PK_CATLIB"][$rec].'">'.$results["CATLIB_NAME"][$rec].'</option>';
		}
		$dd_lookup .= '</select>';
		
		$et_query = "SELECT * from  SCH_CODELST WHERE CODELST_TYPE='lib_type'";
		$rs = executeOCIQuery($et_query,$ds_conn);
		$event_type = '<select class="required" name="eventtype">';
		for ($rec = 0; $rec < $results_nrows; $rec++){
			$event_type .= '<option value="'.$rs["PK_CODELST"][$rec].'">'.$rs["CODELST_DESC"][$rec].'</option>';
		}
		$event_type .= '</select>';
		
?>
		<div class="navigate">Calendar from File:</div>
		<form name="calendar" enctype="multipart/form-data" action="calendar_quick.php" method="POST" onSubmit="if (validate(document.calendar) == false) return false;">
		<table>
		<tr><td>Calendar Name:</td><td><input type="text" name="calname" size="50" class="required" /></td></tr>
		<tr><td>Calendar Type:</td><td><?PHP echo $dd_lookup; ?></td></tr>
		<tr><td>Calendar Duration:</td><td><input class="required" type="text" name="calduration" size="10"/>
		<select class="required" name="durunit">
		<option value="D">Day(s)</option>
		<option value="W">Week(s)</option>
		<option value="M">Month(s)</option>
		<option value="Y">Year(s)</option>
		</select>
		</td></tr>
		<tr>
		  <td>Event Type: </td><td><?php echo $event_type; ?></td></tr>		
		<tr><td>Delimited File name:</td><td><input class="required" type="file" name="uploadedfile" size="75"/></td></tr>
		<tr><td>Delimiter:</td><td><input class="required" type="text" name="delimiter" size="2" value="," /></td></tr>
		<tr><td><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /></td><td align="right">Sample template: <a href="./template/calendar_template.csv">calendar_template.csv</a></td></tr>
		</table>
		</form>
		<script>
			document.calendar.calname.focus();
		</script>

		<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;">
		<table summary="Note: Note">
		<tr>
		<th align="left">Note</th>
		</tr>
		<tr><td align="left" valign="top">
		<p>Supported File Types: Any delimited file (delimiters such as comma, semicolon, pipe etc. are commonly used)</p>
		</td></tr>
		</table></div>

	<?php } else {
	
		$v_delimiter = $_POST["delimiter"];
		$v_category = $_POST["category"];
		$v_eventtype = $_POST["eventtype"];
		$v_calname = $_POST["calname"];
		$v_calduration = $_POST["calduration"];
		$v_durunit = $_POST["durunit"];
		$v_query = "SELECT fk_account FROM ER_CATLIB WHERE pk_catlib = ".$v_category;
		$results = executeOCIQuery($v_query,$ds_conn);
		$v_account= $results["FK_ACCOUNT"][0];
		$v_query = "SELECT count(*) as count FROM event_def WHERE event_type = 'P' and user_id = $v_account and name = '$v_calname'";
		$results = executeOCIQuery($v_query,$ds_conn);
		$v_calexists= $results["COUNT"][0];
		if (($v_calexists > 0) || (intval($v_calduration) == 0)) {
			$v_query = "SELECT pk_catlib,ac_name || ' - ' || catlib_name as catlib_name FROM ER_CATLIB,er_account WHERE pk_account = fk_account and catlib_type = 'L'";
			$results = executeOCIQuery($v_query,$ds_conn);
			
			$dd_lookup = '<select name="category">';
			for ($rec = 0; $rec < $results_nrows; $rec++){
				if ($v_category == $results["PK_CATLIB"][$rec]){
					$dd_lookup .= '<option selected value="'.$results["PK_CATLIB"][$rec].'">'.$results["CATLIB_NAME"][$rec].'</option>';
				} else {
					$dd_lookup .= '<option value="'.$results["PK_CATLIB"][$rec].'">'.$results["CATLIB_NAME"][$rec].'</option>';
				}
			}
			$dd_lookup .= '</select>';
	?>
			<div class="navigate">Quick Calendar:</div>
			<form enctype="multipart/form-data" action="calendar_quick.php" method="POST">
			<table>
			<tr><td>Calendar Name:</td><td><input type="text" name="calname" size="50" value="<?PHP echo $v_calname; ?>"/>
			<?PHP if ($v_calexists > 0) echo '<font color="red"> <b>Calendar name already exists.</b></font>'; ?>
			</td></tr>
			<tr><td>Calendar Type:</td><td><?PHP echo $dd_lookup; ?></td></tr>
			<tr><td>Calendar Duration:</td><td><input type="text" name="calduration" size="10" value="<?PHP echo $v_calduration; ?>"/>
			<select name="durunit">
		<?PHP
			if ($v_durunit == 'D') { echo '<option selected value="D">Day(s)</option>'; } else { echo '<option value="D">Day(s)</option>';}
			if ($v_durunit == 'W') { echo '<option selected value="W">Week(s)</option>'; } else { echo '<option value="W">Week(s)</option>';}
			if ($v_durunit == 'M') { echo '<option selected value="M">Month(s)</option>';} else {echo '<option value="M">Month(s)</option>';}
			if ($v_durunit == 'Y') { echo '<option selected value="Y">Year(s)</option>';} else {echo '<option value="Y">Year(s)</option>';}
			echo "</select>";
			if (intval($v_calduration) == 0) echo '<font color="red"><b> Invalid calendar duration.</b></font>';
		?>
			</td></tr>
			<tr><td>Delimited File name:</td><td><input type="file" name="uploadedfile" size="100"/></td></tr>
			<tr><td>Delimiter:</td><td><input type="text" name="delimiter" size="2" value="<?PHP echo $v_delimiter; ?>"/></td></tr>
			<tr><td><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';" /></td></tr>
			</table>
			</form>
			<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
			<tr>
			<!--<td rowspan="2" align="center" valign="top" width="25"><img alt="[Note]" src="./stylesheet-images/note.png"></td>-->
			<th align="left">Note</th>
			</tr>
			<tr><td align="left" valign="top">
			<p>Supported File Types: Any delimited file (delimiters such as comma, semicolon, pipe etc. are commonly used)</p>
			<!--<p>First row of the file should contain Column Name, no special characters other than '_' (underscore), should be one continuous string of characters (no space) and delimited with same delimiter as the actual data rows.</p>-->
			</td></tr>
			</table></div>
		<?PHP	
		} else {

			$curDir = getcwd();
			$target_path = $curDir."/upload/";
			$target_path = $target_path.basename( $_FILES['uploadedfile']['name']);
			if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
		?>
		        <div class="navigate">Quick Calendar:</div>
		        <form action="calendar_quicksave.php" method="POST">
		<?PHP				
				echo '<input type="hidden" name="delimiter" value="'.$v_delimiter.'"/>';
				echo '<input type="hidden" name="category" value="'.$v_category.'"/>';
				echo '<input type="hidden" name="eventtype" value="'.$v_eventtype.'"/>';
				echo '<input type="hidden" name="account" value="'.$v_account.'"/>';
				echo '<input type="hidden" name="fileDir" value="'.$target_path.'"/>';
				echo 'Calendar Name: <b>'.$v_calname.'</b><input readonly size=30 type="hidden" name="calname" value="'.$v_calname.'"/>';
				echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Calendar Duration: <b>'.$v_calduration.'<input  readonly size=5 type="hidden" name="calduration1" value="'.$v_calduration.'"/>';
				switch ($v_durunit) {
				case 'D':
				    echo ' Day(s)';
				    $v_caldur = $v_calduration;
				    break;
				case 'W':
				    echo " Week(s)";
				    $v_caldur = $v_calduration * 7;
				    break;
				case 'M':
				    $v_caldur = $v_calduration * 30;
				    echo " Month(s)";
				    break;
				case 'Y':
				    $v_caldur = $v_calduration * 365;
				    echo " Year(s)";
				    break;
				}
				echo '</b><input  type="hidden" size=8 name="calduration" value="'.$v_caldur.'"/>';
				echo '<input  type="hidden" size=8 name="durunit" value="'.$v_durunit.'"/>';
				echo '<table width="100%" border="1">';
				$handle = fopen($target_path, "r");
				$row = 0;
				$v_fatal = "";
				$v_fatal1 = "";
				$v_fatal2 = "";
				$v_fatal3 = "";
				while (($data = fgetcsv($handle, 10000, $v_delimiter)) !== FALSE) {
				 $num = count($data);
				 if ($row == 0) {
					 echo "<TR>";
					 for ($c=0; $c < $num; $c++) {
						if ($c == 0){
				  	 		echo '<td bgcolor="#5C7099"><input style="background-color:#5C7099;border:none;color:white" readonly size="20" type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/></td>';
			  	 		} else {
				  	 		// Check if the visit is duplicate
  	 						list($v_visit,$v_visit_name) = explode('|',$data[$c]);
							if (strlen($v_visit_name) == 0) $v_visit_name = $v_visit;
							$v_visits[$c-1] = $v_visit_name;
//							echo "--= ".$v_visits[$c-1];
							$v_visitcount = 0;
							for($v=0;$v<count($v_visits);$v++){
								if ($v_visit_name == $v_visits[$v]) $v_visitcount++;
							}
							
							if ($v_visitcount > 1) $v_fatal3 = "X";
							
				  	 		$v_disp = getDisplacementValue($data[$c]);
				  	 		$v_dispvalidate = displacementValidate($data[$c]);
				  	 		if ($v_disp > $v_caldur) $v_fatal1 = "Visit interval exceeds calendar duration.";
				  	 		if ($v_disp == 0 || $v_dispvalidate > 0 ){
				  	 			echo '<td bgcolor="red"><input style="background-color:red;border:none;color:white" readonly size="10" type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/>';
								if ($v_dispvalidate > 0) {
					  	 			echo " * ";
					  	 			$v_fatal2 = "X";
				  	 			}
				  	 			echo '</td>';
				  	 			if ($v_disp == 0) $v_fatal .= $data[$c].", ";
							
			  	 			} else if ($v_disp > $v_caldur) {
				  	 			echo '<td bgcolor="orange"><input style="background-color:orange;border:none;color:white" readonly size="10" type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/></td>';
							} else {
				  	 			echo '<td bgcolor="#5C7099"><input style="background-color:#5C7099;border:none;color:white" readonly  size="10" type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/></td>';
			  	 			}
			  	 		}
					 }
					 echo "</TR>";
				 } else {
				     
					 for ($c=0; $c < $num; $c++) {
						 if ($c == 0){
							 $v_eventname = $data[$c];
				 			$v_query = "SELECT count(*) as count FROM event_def WHERE EVENT_TYPE = 'E' and user_id = ".$v_account." and trim(upper(name)) = trim(upper('$v_eventname'))";
							$results = executeOCIQuery($v_query,$ds_conn);
							$v_count = $results["COUNT"][0];
							if ($v_count == 0) {echo "<tr>";} else {echo "<tr bgcolor=#e7f0fe>";}
							 echo '<td bgcolor="#5C7099"><input style="background-color:#5C7099;border:none;color:white" readonly size="20" type="text" name="column['.$row.']['.$c.']" value="'.$data[$c].'"/></td>';
						 } else {
							 if ($v_count == 0) {
								echo '<td><input style="border:none;text-align: center;" readonly size="10" type="text" name="column['.$row.']['.$c.']" value="'.(empty($data[$c])?"":"X").'"/></td>';
							} else {
								echo '<td><input style="background-color:#e7f0fe;border:none;text-align: center;" readonly size="10" type="text" name="column['.$row.']['.$c.']" value="'.(empty($data[$c])?"":"X").'"/></td>';
							}
						 }
					 }
				     echo '</tr>';
				}
				 $row++;
				}
				
				echo "</table>";
				echo "<BR>";
				if ((strlen($v_fatal) == 0) && (strlen($v_fatal1) == 0) && (strlen($v_fatal2) == 0) && (strlen($v_fatal3) == 0)){
					echo 'Highlighted events exists in the event library. Do you want to transfer event attributes (cost/messages/resources/documents) from library? <input align=right type=checkbox name=migrate checked><br><br>';
					echo '<input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src=\'./img/submit_m.png\';" onmouseout="this.src=\'./img/submit.png\';" />';
				} else {
					if (strlen($v_fatal) > 0) {
						$v_fatal = substr($v_fatal,0,strlen($v_fatal)-2);
						echo "<font color=red><b>Invalid visit interval definition(s): $v_fatal <BR>Valid format is MnWnDn, were n is the number of month(s)/week(s)/day(s).<br></b></font>";
					}
					if (strlen($v_fatal1) > 0) {
						echo "<font color=orange><b><BR>$v_fatal1</b></font>";
					}
					if (strlen($v_fatal2) > 0) echo "<font color=orange><b><BR>* - Weeks cannot be more than 4 when month is entered / Days cannot be more than 7 when week is entered.<br></b></font>";
					if (strlen($v_fatal3) > 0) echo "<font color=magenta><b><BR>Visit name cannot be duplicate.<br></b></font>";
				}
				echo "</form>";
				fclose($handle);
			} else{
				echo "There was an error uploading the file, please try again!"."<BR>";
			}
		}
	}

?>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
