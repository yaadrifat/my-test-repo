<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> CDUS Delete</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">CDUS Submission - Delete</div>

<?php

$v_datamain = $_GET["pk_datamain"];

$query_sql = "delete from exp_mapping WHERE fk_exp_datamain = ".$v_datamain;
$results = executeOCIUpdateQuery($query_sql,$ds_conn);

$query_sql = "delete from exp_datadetails WHERE fk_exp_datamain = ".$v_datamain;
$results = executeOCIUpdateQuery($query_sql,$ds_conn);

$query_sql = "delete from exp_datamain WHERE pk_datamain = ".$v_datamain;
$results = executeOCIUpdateQuery($query_sql,$ds_conn);

echo "Data deleted.";
$url = "./cdus.php?fk_exp_definition=".$_GET["fk_exp_definition"];
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";

?>


</div>


</body>
</html>
<?php
OCICommit($ds_conn);
OCILogoff($ds_conn);
}
else header("location: ./index.php?fail=1");
?>
