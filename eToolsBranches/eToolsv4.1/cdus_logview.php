<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> CDUS Log View</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">CDUS Submission - View Full Log</div>


<Table border="1"><TR>
<!-- <TH>PK</TH> -->
<TH>STUDY</TH>
<TH>PATIENT</TH>
<TH>SECTION</TH>
<TH>COL3</TH>
<TH>COL4</TH>
<TH>COL5</TH>
<!--
<TH>COL6</TH>
<TH>COL7</TH>
<TH>COL8</TH>
<TH>COL9</TH>
-->
<TH>Rule</TH>
</TR>
<?php

$pk_datamain = $_GET["pk_datamain"];
//$query_sql = "SELECT pk_exp_datadetails,sec_name,COL1, COL2, COL3, COL4, COL5 FROM EXP_DATADETAILS, EXP_SECTIONS WHERE fk_exp_datamain = ".$pk_datamain." AND pk_exp_sections = fk_exp_sections ORDER BY col1,col2,fk_exp_sections";

$query_sql = "SELECT pk_exp_datadetails,sec_name,COL1, COL2, COL3, COL4, COL5, (SELECT rule_name FROM EXP_RULES WHERE pk_exp_rules = fk_exp_rules) AS rule_name FROM EXP_DATADETAILS, EXP_SECTIONS, EXP_LOG WHERE fk_exp_datamain = ".$pk_datamain." AND pk_exp_sections = fk_exp_sections AND pk_exp_datadetails = fk_exp_datadetails(+) ORDER BY col1,col2,fk_exp_sections";


$results = executeOCIQuery($query_sql,$ds_conn);
$rows = $results_nrows;

for ($rec = 0; $rec < $rows; $rec++){

if ($results["RULE_NAME"][$rec] == "" ) { 
	echo "<TR>";
} else {
	echo '<TR bgcolor="PINK">';
}
?>

<!-- 	<TD><?php echo $results["PK_EXP_DATADETAILS"][$rec] . "&nbsp;"; ?></TD> -->
	<TD><?php echo $results["COL1"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL2"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["SEC_NAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL3"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL4"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL5"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["RULE_NAME"][$rec] . "&nbsp;"; ?></TD>
<!--
	<TD><?php echo $results["COL6"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL7"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL8"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["COL9"][$rec] . "&nbsp;"; ?></TD>
-->
<?php

echo "</TR>";
}
?>
</TABLE>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
