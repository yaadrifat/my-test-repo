<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velink -> STS Adult Cardiac Surgery Registry</title>
</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<p fontsize="12"><b><u>STS Adult Cardiac Surgery Registry</u></b></p>

<table width="100%" border="0">
       <tr height="30px" bgcolor="d6e0e7"  bordertop="1"><td align="center"><B><a href="sts_acs_update_lookup.php">Upload files (Valve Devices/VAD Devices/Risk Model Coefficients)</a></B></td></tr>
<tr height="5px"></tr>
       <tr height="30px" bgcolor="d6e0e7"  bordertop="1"><td align="center"><B><a href="sts_acs_harvest.php">Harvest</a></B></td></tr>
<tr height="5px"></tr>
       <tr height="30px" bgcolor="d6e0e7"  bordertop="1"><td align="center"><B><a href="sts_acs_harvest_view.php">View Harvest Files</a></B></td></tr>
<tr height="5px"></tr>
       <tr height="30px" bgcolor="d6e0e7"  bordertop="1"><td align="center"><B><a href="sts_acs_datacomp.php">Change Data Completeness Criteria</a></B></td></tr>
<tr height="5px"></tr>
</table>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
