<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Users</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");
include ("./txt-db-api.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 



?>
<script>
function validate(form,mode){
	if (form.pass1.value != form.pass2.value){
		alert("Password does not match, please re-enter password.");
		return false;
	}
	if (form.loguser.value == ""){
		alert("Login name cannot be blank.");
		return false;
	}
	if (form.group.value == ""){
		alert("Group cannot be blank.");
		return false;
	}
	if (mode == 'n') {
		if (form.pass1.value == "" || form.pass2.value == ""){
			alert("Password cannot be blank.");
			return false;
		}
	}
}
</script>



</head>


<body >

<div id="fedora-content">	

<?PHP
if (isset($_GET["mode"])) $v_mode = $_GET["mode"];
if (isset($_POST["mode"])) $v_mode = $_POST["mode"];




if (!($_SERVER['REQUEST_METHOD'] == 'POST')) {


	if ($v_mode == 'n'){
//		$query_sql = "select pk_user,usr_firstname || ' ' || usr_lastname as uname from er_user where usr_type = 'S' order by usr_firstname || ' ' || usr_lastname";
		$v_grpName = "";
		$v_pk_users = "";
		$v_fname = "";
		$v_lname = "";
		$v_email = "";
		$v_phone = "";
		$v_loguser = "";
	} else {
		$v_pk_users = $_GET["pk_users"];
		$db = new Database("etools");
		$rs = $db->executeQuery("SELECT PK_USERS,FIRST_NAME,LAST_NAME,GROUP_NAME,PHONE,EMAIL,USER_NAME FROM ET_USERS,ET_GROUPS WHERE ET_GROUPS.PK_GROUPS = ET_USERS.FK_GROUPS and PK_USERS=".$v_pk_users);
		$rs->next();
		$data = $rs->getCurrentValuesAsHash();
		$v_grpName = $data["GROUP_NAME"];
		$v_fname = $data["FIRST_NAME"];
		$v_lname = $data["LAST_NAME"];
		$v_email = $data["EMAIL"];
		$v_phone = $data["PHONE"];
		$v_loguser = $data["USER_NAME"];
	}

	$db = new Database("etools");
	$rs = $db->executeQuery("select PK_GROUPS,GROUP_NAME from ET_GROUPS");
	$dd_grps = '';
	while ($rs->next()) {
		$data = $rs->getCurrentValuesAsHash();
		if ($data["GROUP_NAME"] == $v_grpName) {
			$dd_grps .= "<option selected value=".$data["PK_GROUPS"].">".$data["GROUP_NAME"]."</option>";
		} else {
			$dd_grps .= "<option value=".$data["PK_GROUPS"].">".$data["GROUP_NAME"]."</option>";
		}
	}



	if ($v_mode == 'n'){
		echo '<div class="navigate">Manage Account - Users - New User</div>';
	} else {
		echo '<div class="navigate">Manage Account - Users - Edit</div>';
	}
	?>
		<form name="edituser" action="account_users_edit.php" method="POST" onSubmit="if (validate(document.edituser,'<?PHP echo $v_mode;?>') == false) return false;">
			<input type='hidden' name='mode' value=<?PHP echo $v_mode; ?>>
			<input type='hidden' name='pk_users' value=<?PHP echo $v_pk_users; ?>>
			<BR><table>

			<tr><td>First Name: </td><td><input type = "text" size="40" name="fname" value="<?PHP echo $v_fname; ?>"></td></tr>
			<tr><td>Last Name: </td><td><input type = "text" size="40" name="lname" value="<?PHP echo $v_lname; ?>"></td></tr>
			<tr><td>Email Address: </td><td><input type = "text" size="40" name="email" value="<?PHP echo $v_email; ?>"></td></tr>
			<tr><td>Phone: </td><td><input type = "text" size="40" name="phone" value="<?PHP echo $v_phone; ?>"></td></tr>
			<tr><td>Login Name: </td><td><input  <?PHP  if ($v_mode == 'm') echo "disabled"; else echo 'class="required"';?> type = "text" size="40" name="loguser" value="<?PHP echo $v_loguser; ?>"></td></tr>

			<tr><td>Select Group: </td><td><select class="required" name="group"><?PHP echo $dd_grps; ?></select></td></tr>
			<tr><td>Password: </td><td><input <?PHP echo (($v_mode == 'n') ? " class = 'required'" : "")?>type="password" name="pass1" size="20"/></td></tr>
			<tr><td>Re-enter Password: </td><td><input <?PHP echo (($v_mode == 'n') ? " class = 'required'" : "")?>type="password" name="pass2" size="20"/></td></tr>
			</table>
				<BR>
			<input type="image" src="./img/submit.png" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';">

			</form>
		
	<?PHP
} else {
	$v_pk_users = $_POST["pk_users"];
	$v_group = $_POST["group"];
	$v_pass = $_POST["pass1"];
	$v_mode = $_POST["mode"];
	
	$v_fname = $_POST["fname"];
	$v_lname = $_POST["lname"];
	$v_email = $_POST["email"];
	$v_phone = $_POST["phone"];
	if (isset($_POST["loguser"])) $v_loguser = $_POST["loguser"];

	
	$db = new Database("etools");
	if ($v_mode == 'n') {
		$result = $db->executeQuery("SELECT USER_NAME from ET_USERS where USER_NAME='$v_loguser'");
		if ($result->getRowCount() > 0) {
			echo "User already exists";
			echo '<meta http-equiv="refresh" content="3; url=./account_users.php">';
		} else {
			$rs = $db->executeQuery("INSERT INTO ET_USERS (LAST_NAME,FIRST_NAME,EMAIL,PHONE,USER_NAME,PASSWORD,FK_GROUPS) VALUES ('".$_POST["lname"]."','".$_POST["fname"]."','".$_POST["email"]."','".$_POST["phone"]."','".$_POST["loguser"]."','".md5($v_pass)."',".$v_group.")");
			echo "Data Saved.";
			echo '<meta http-equiv="refresh" content="0; url=./account_users.php">';
		}
	} else {
		if (!empty($v_pass)) $rs = $db->executeQuery("UPDATE ET_USERS set PASSWORD = '".md5($v_pass)."' where PK_USERS = ".$v_pk_users);
		$rs = $db->executeQuery("UPDATE ET_USERS set FIRST_NAME = '".$v_fname."',LAST_NAME = '".$v_lname."',EMAIL = '".$v_email."',PHONE = '".$v_phone."', FK_GROUPS = ".$v_group." where PK_USERS = ".$v_pk_users);
		echo "Data Saved.";
		echo '<meta http-equiv="refresh" content="0; url=./account_users.php">';
	}

}


?>
</div>

</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
