<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> CDUS Edit</title>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>

</head>
<body>
<div id="fedora-content">	
<div class="navigate">CDUS Submission - Validate - Edit</div>
<?php

// phpinfo();


if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 

$pk_exp_datadetails = $_GET["pk_exp_datadetails"];
$fk_exp_sections = $_GET["fk_exp_sections"];
$pk_datamain = $_GET["pk_exp_datamain"];
$fk_exp_definition = $_GET["fk_exp_definition"];

$query="SELECT col_name FROM EXP_SECTION_COLS where fk_exp_sections= ".$fk_exp_sections;
$sec_results = executeOCIQuery($query,$ds_conn);
$sec_rows = $results_nrows;

$query="select PK_EXP_DATADETAILS, COL1, COL2, COL3, COL4, COL5,COL6,COL7,COL8,COL9,COL10,COL11,COL12,COL13,COL14,COL15,COL16,COL17,COL18,COL19,COL20,COL21,COL22,COL23 from exp_datadetails where pk_exp_datadetails = ".$pk_exp_datadetails;
$results = executeOCIQuery($query,$ds_conn);

?>
	
<FORM action="cdus_edit.php" method=post>

<TABLE>
<TR>
<input type = "hidden" name="pk_exp_datadetails" value=<?php echo $pk_exp_datadetails; ?>>
<input type = "hidden" name="pk_datamain" value=<?php echo $pk_datamain; ?>>
<input type = "hidden" name="fk_exp_definition" value=<?php echo $fk_exp_definition; ?>>
</tr>
<?php

for ($rec = 0; $rec < $sec_rows; $rec++){
	$col_num = $rec + 1;
	echo "<TR>";
	echo "<TD>".$sec_results["COL_NAME"][$rec]."</TD>";
	echo '<TD><INPUT size="50" type="text" name="'.'COL'.$col_num.'" value="'.$results['COL'.$col_num][0].'"</TD>';
	echo "</TR>";
}

?>

<TR>
<td>
	<input type="submit" name="submit" value="Submit">
</td></tr>
</form>

<?php 
} else {

$pk_exp_datadetails = $_POST['pk_exp_datadetails'];
$v_col1 = isset($_POST['COL1'])?$_POST['COL1']:'';
$v_col2 = isset($_POST['COL2'])?$_POST['COL2']:'';
$v_col3 = isset($_POST['COL3'])?$_POST['COL3']:'';
$v_col4 = isset($_POST['COL4'])?$_POST['COL4']:'';
$v_col5 = isset($_POST['COL5'])?$_POST['COL5']:'';
$v_col6 = isset($_POST['COL6'])?$_POST['COL6']:'';
$v_col7 = isset($_POST['COL7'])?$_POST['COL7']:'';
$v_col8 = isset($_POST['COL8'])?$_POST['COL8']:'';
$v_col9 = isset($_POST['COL9'])?$_POST['COL9']:'';
$v_col10 = isset($_POST['COL10'])?$_POST['COL10']:'';
$v_col11 = isset($_POST['COL11'])?$_POST['COL11']:'';
$v_col12 = isset($_POST['COL12'])?$_POST['COL12']:'';
$v_col13 = isset($_POST['COL13'])?$_POST['COL13']:'';
$v_col14 = isset($_POST['COL14'])?$_POST['COL14']:'';
$v_col15 = isset($_POST['COL15'])?$_POST['COL15']:'';
$v_col16 = isset($_POST['COL16'])?$_POST['COL16']:'';
$v_col17 = isset($_POST['COL17'])?$_POST['COL17']:'';
$v_col18 = isset($_POST['COL18'])?$_POST['COL18']:'';
$v_col19 = isset($_POST['COL19'])?$_POST['COL19']:'';
$v_col20 = isset($_POST['COL20'])?$_POST['COL20']:'';
$v_col21 = isset($_POST['COL21'])?$_POST['COL21']:'';
$v_col22 = isset($_POST['COL22'])?$_POST['COL22']:'';
$v_col23 = isset($_POST['COL23'])?$_POST['COL23']:'';


$query = "update exp_datadetails set col1='$v_col1',col2='$v_col2',col3='$v_col3',col4='$v_col4',col5='$v_col5',col6='$v_col6',col7='$v_col7',col8='$v_col8',col9='$v_col9',col10='$v_col10',col11='$v_col11',col12='$v_col12',col13='$v_col13',col14='$v_col14',col15='$v_col15',col16='$v_col16',col17='$v_col17',col18='$v_col18',col19='$v_col19',col20='$v_col20',col21='$v_col21',col22='$v_col22',col23='$v_col23' where pk_exp_datadetails=$pk_exp_datadetails";

$results = executeOCIUpdateQuery($query,$ds_conn);

OCICommit($ds_conn);
OCILogoff($ds_conn);

echo "Data Saved Successfully !!!";

$pk_datamain = $_POST["pk_datamain"];
$fk_exp_definition = $_POST["fk_exp_definition"];
	
$url = "./cdus_errlogview.php?pk_datamain=".$pk_datamain."&fk_exp_definition=".$fk_exp_definition;
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";
?>
<!-- <meta http-equiv="refresh" content="0; url=./codelist.php"> -->
<?PHP
}

?>
</div>
<!--


-->
</body>
</html>


<?php
}
else header("location: index.php?fail=1");
?>
		
