<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Form Data</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 



?>
</head>


<body>

<div id="fedora-content">	

<div class="navigate">Migrate Data - Form - Map Columns</div>

<?PHP


if (isset($_GET["pk_vlnk_adapmod"])) {
	$pk_vlnk_module = $_GET["pk_vlnk_adapmod"];
} else {
	$pk_vlnk_module = 0;
}
/*
$query_sql = "select count(*) as count from velink.vlnk_imp_formfld where fk_vlnk_adapmod = ".$pk_vlnk_module;
$results = executeOCIQuery($query_sql,$ds_conn);
if ($results["COUNT"][0] == 0) {
*/
	if ($_SERVER['REQUEST_METHOD'] != "POST"){

	$linkedto = $_GET['linkedto'];

	$query_sql = "SELECT distinct column_name FROM DBA_TAB_COLUMNS WHERE table_name=upper('".$_GET['tablename']."') ORDER BY 1";
	$results = executeOCIQuery($query_sql,$ds_conn);

	$dd_table = '<option value=""></option><option value="trunc(SYSDATE)">{ Migration Date }</option>';
	for ($rec = 0; $rec < $results_nrows; $rec++){
	           $dd_table .= "<option value=".$results["COLUMN_NAME"][$rec].">".$results["COLUMN_NAME"][$rec]."</option>";
	}
	$query_sql = "select mp_sequence,mp_mapcolname,mp_dispname,decode(mp_flddatatype,'ED','Date','EN','Number','ET','Text','MD','Dropdown','MC','Checkbox','MR','Radiobutton') as mp_flddatatype,
	mp_secname,mp_pkfld , formfld_mandatory 
	from er_mapform , er_formfld 
	where fk_form = ".$_GET['pk_formlib']." and mp_pkfld = fk_field
	union
	select mp_sequence,mp_mapcolname,mp_dispname,decode(mp_flddatatype,'ED','Date','EN','Number','ET','Text','MD','Dropdown','MC','Checkbox','MR','Radiobutton') as mp_flddatatype,
	mp_secname,mp_pkfld , b.formfld_mandatory 
	from er_mapform , er_repformfld a, er_formfld b 
	where fk_form = ".$_GET['pk_formlib']." and mp_pkfld = a.fk_field and pk_formfld = fk_formfld
	order by mp_sequence";
	//echo $query_sql;
	$results = executeOCIQuery($query_sql,$ds_conn);

	$dd_map = '<option value="DISP">Display Value</option><option value="DATA">Data Value</option>';

	?>
	<form name="formtabmap" method="post" action="mig_forms_mapflds.php">
	<input type="hidden" name="pk_vlnk_adaptor" value="<?PHP echo $_GET['pk_vlnk_adaptor'] ?>"/>
	<input type="hidden" name="pk_formlib" value="<?PHP echo $_GET['pk_formlib'] ?>"/>
	<input type="hidden" name="pk_vlnk_adapmod" value="<?PHP echo $_GET['pk_vlnk_adapmod'] ?>"/>
	Form Name: <input size="40" style="border:none" readonly name="formname" value="<?PHP echo $_GET['formname'] ?>"/>
	Table Name: <input size="40" style="border:none" readonly name="tabname" value="<?PHP echo $_GET['tablename'] ?>"/>
	<!-- <input type="hidden" name="linked_to" value="<?PHP echo $_POST['linked_to'] ?>"/> 

	<table>
		<tr><td>Creator: </td><td><input type="text" size="20" name="creator"/></td></tr>
		<tr><td>Account ID: </td><td><input type="text" size="20" name="account" value=<? echo $_SESSION["accountId"]; ?> /></td></tr>
		<tr><td>Patient ID: </td><td><input type="text" size="100" name="patientId"/></td></tr>
		<tr><td>Patient Study ID: </td><td><input type="text" size="100" name="patientStdId"/></td></tr>
		<tr><td>Study ID: </td><td><input type="text" size="100" name="studyId"/></td></tr>
	</table>
	-->

	<Table border="1" width="100%"><TR>
	<TH width="1%">&nbsp;</TH>
	<TH width="20%">Section Name</TH>
	<TH width="34%">Field Name</TH>
	<TH width="20%">Table Column Name</TH>
	<TH width="10%">Field Type</TH>
	<TH width="10%">Map Response to</TH>
	</TR>
	<?php if ($linkedto == "SP" or $linkedto == "PA" or $linkedto == "PR" or $linkedto == "PS") { ?>
	<TR>
		<TD></TD>
		<TD></TD>
		<TD><?php echo "<INPUT style='border:none' readonly name=\"patient_id\" value=\"PATIENT_ID\" size=8 /><font color='red'><b><font size='3'>*</font>"; ?></TD>
		<td><?PHP echo "<select class='required' name=\"pat_colname\">".$dd_table."</select>"; ?></td>
		<TD></TD>
		<TD></TD>
	</TR>
	<?php } 
	if ($linkedto == "SP" or $linkedto == "SA" or $linkedto == "S" or $linkedto == "PS" or $linkedto == "PR") {
	?>
	<TR>
		<TD></TD>
		<TD></TD>
		<TD><?php echo "<INPUT style='border:none' readonly name=\"study_number\" value=\"STUDY_NUMBER\" size=8 /><font color='red'><b><font size='3'>*</font>"; ?></TD>
		<td><?PHP echo "<select name=\"std_colname\">".$dd_table."</select>"; ?></td>
		<TD></TD>
		<TD></TD>
	</TR>

	<?php
	}

	for ($rec = 0; $rec < $results_nrows; $rec++){
	$fld_datatype = $results["MP_FLDDATATYPE"][$rec];
	
	$v_mandatory = ($results["FORMFLD_MANDATORY"][$rec] == 1)? "class='required'":"";

	?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
		<TD><?php echo $results["MP_SEQUENCE"][$rec] . "&nbsp;"; ?></TD>
		<TD><?php echo $results["MP_SECNAME"][$rec] . "&nbsp;"; ?></TD>
		<TD><?php echo $results["MP_DISPNAME"][$rec] . "&nbsp;". (($results["FORMFLD_MANDATORY"][$rec] == 1)?"<font color='red'><b><font size='3'>*</font></b></font>":""); ?></TD>
		<td><?PHP echo "<select  $v_mandatory name=\"tablename[$rec]\">".$dd_table."</select>"; ?></td>
		<TD><?php echo $results["MP_FLDDATATYPE"][$rec] . "&nbsp;"; ?></TD>

<!--		<TD><?php echo "<INPUT style='border:none' readonly name=\"dispname[$rec]\" value=\"".$results["MP_DISPNAME"][$rec].'"></input>'; ?></TD>
		<TD><?php echo "<INPUT style='border:none' readonly name=\"fldtype[$rec]\" value=".$fld_datatype . "></input>"; ?></TD>
-->
		<td><?PHP 
			if (($fld_datatype == 'Dropdown') || ($fld_datatype == 'Radiobutton')) {
			echo "<select name=\"map[$rec]\">".$dd_map."</select>"; 
			} else {
			//echo "<select disabled name=\"map[$rec]\">".$dd_map."</select>"; 
			}
			?>
		</td>
		<?PHP echo "<INPUT type=\"hidden\" name=\"pk_field[$rec]\" value=".$results["MP_PKFLD"][$rec]."></input>" ?>
		<?PHP echo "<INPUT type=\"hidden\" name=\"mp_mapcolname[$rec]\" value=".$results["MP_MAPCOLNAME"][$rec]."></input>" ?>
		<?PHP echo "<INPUT type=\"hidden\" name=\"mp_mapcoltype[$rec]\" value=".$results["MP_FLDDATATYPE"][$rec]."></input>" ?>
		<?PHP echo "<INPUT type=\"hidden\" name=\"mp_mandatory[$rec]\" value=".$results["FORMFLD_MANDATORY"][$rec]."></input>" ?>
	</TR>
	<?php
	//	echo "<td><a href=formimport_select.php?pk_formlib=".$results["PK_FORMLIB"][$rec]."&formname=".urlencode($results["FORM_NAME"][$rec]).">Select</a></td>";
	//	echo "</TR>";
	}
	?>
	</TABLE>
<input type="image" name="submit" value="SUBMIT" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
	</form>

	<?php 
	} else { 
	//$v_sql = "delete from velink.vlnk_imp_formfld where fk_vlnk_adapmod = ".$_POST["pk_vlnk_adapmod"];
	//$results = executeOCIUpdateQuery($v_sql,$ds_conn);

	if (isset($_POST["patient_id"])) {
		$v_sql = "insert into velink.vlnk_imp_formfld (pk_imp_formfld, fk_vlnk_adapmod,table_colname,lookup,fld_mandatory) values (velink.seq_vlnk_imp_formfld.nextval,".$_POST["pk_vlnk_adapmod"].",'".$_POST["pat_colname"]."','PATIENT',1)";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);
	}
	if (isset($_POST["study_number"])) {
		$v_sql = "insert into velink.vlnk_imp_formfld (pk_imp_formfld, fk_vlnk_adapmod,table_colname,lookup,fld_mandatory) values (velink.seq_vlnk_imp_formfld.nextval,".$_POST["pk_vlnk_adapmod"].",'".$_POST["std_colname"]."','STUDY',1)";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);
	}
	$tab_cols = $_POST["tablename"];
	$pk_field = $_POST["pk_field"];
	$mp_mapcolname = $_POST["mp_mapcolname"];
	$map = $_POST["map"];
	$fld_type = $_POST["mp_mapcoltype"];
	$fld_mandatory = $_POST["mp_mandatory"];
	
	
	for ($i=0;$i < count($tab_cols);$i++){
		$v_mandatory = (empty($fld_mandatory[$i])?'null':$fld_mandatory[$i]);
		if(count($map[$i])>0){
			$fk_map_resp = 0;
		}else{
			$fk_map_resp = "null";
		}
		
		$v_sql = "insert into velink.vlnk_imp_formfld (pk_imp_formfld, fk_vlnk_adapmod,form_colname,table_colname,map_displayval,fk_field,fld_type,fld_mandatory,fk_map_resp) values (velink.seq_vlnk_imp_formfld.nextval,".$_POST["pk_vlnk_adapmod"].",'".$mp_mapcolname[$i]."','".$tab_cols[$i]."','".(!isset($map[$i]) ?  "" : $map[$i])."',".$pk_field[$i].",'".$fld_type[$i]."',$v_mandatory,$fk_map_resp)";
		$results = executeOCIUpdateQuery($v_sql,$ds_conn);
	}
	

	
	echo "Data Saved...";
	$url = "mig_forms.php?pk_vlnk_adaptor=".$_POST["pk_vlnk_adaptor"];
	echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";

	OCICommit($ds_conn);
	OCILogoff($ds_conn);
?>
     </div>
</body>
</html>
<?PHP
	}
/*
} else {
?>
	<meta http-equiv="refresh" content="0; url=./mig_forms_mapflds_edit.php?pk_vlnk_adaptor=<?PHP echo $_GET["pk_vlnk_adaptor"]; ?>&pk_vlnk_adapmod=<?PHP echo $_GET["pk_vlnk_adapmod"]; ?>&tablename=<?PHP echo $_GET["tablename"]; ?>&pk_formlib=<?PHP echo $_GET["pk_formlib"]; ?>&formname=<?PHP echo $_GET["formname"]; ?>&linkedto=<?PHP echo $_GET["linkedto"]; ?>"> 
<?php
}
*/
}
else header("location: ./index.php?fail=1");
?>
