<?php

	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Edit Code List</title>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");
include ("./txt-db-api.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="js/validate.js"></SCRIPT>
<SCRIPT Language="javascript">
	function validate(){
		if (fnTrimSpaces(stdRights.codelst_subtyp.value) == "" || fnTrimSpaces(stdRights.codelst_desc.value) == "" || fnTrimSpaces(stdRights.codelst_seq.value) == "") {
			alert("Sub Type / Description / Sequence cannot be blank.");
			return false;
		}
		
		if (!isInteger(stdRights.codelst_seq.value)){
			alert("Sequence allows only numbers");	
			return false;
		}	
	}

	 function fnTrimSpaces(strToTrim){  
			strTemp = strToTrim;
			len=strTemp.length;
			i=0;  
			while (i < len && strTemp.charAt(i)==" "){
				i=i+1; 
			}
			strTemp = strTemp.substring(i);
			len=strTemp.length;
			j = len-1;
			while (j>0 && strTemp.charAt(j)==" "){ 
				j = j-1; 
			}
			strTemp = strTemp.substring(0,j+1);
			return strTemp;
	}
</SCRIPT>

</head>
<body>
<!-- <body onLoad="document.stdRights.codelst_subtyp.focus();"> -->
<!-- <div class="browserDefault" id="browserDefault"> -->

<div id="fedora-content">

<?php
// phpinfo();
if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
	$tble = $_GET['tablename'];
	$pk_code = $_GET['pkc'];		
	$mode = $_GET['m'];	
	$patStatus = $_GET['ps'];
	$pkmaint = $_GET['pkm'];

	$selQ = "SELECT * from ".$tble." where PK_CODELST='".$pk_code."'";
	$results = executeOCIQuery($selQ, $ds_conn);	
	$total_rows = $results_nrows;
	
	for ($rec=0; $rec < $total_rows; $rec++){
?>

<div class="navigate">Reason - Edit	</div><br/>
<form name="stdRights" action="codelist_edit_re.php" method="post" onSubmit="if (validate() == false) return false;">
<table width="1000" border="0">
<tbody><tr>
	<td width="127">Type</td>
	<td width="863"><input disabled="disabled" name="codelst_type" value="<?php echo $results["CODELST_TYPE"][$rec]; ?>" type="text">
	<input name="codelst_type" value="<?php echo $results["CODELST_TYPE"][$rec]; ?>" type="hidden" >
	<input name="pkcodelsit" value="<?php echo $pk_code; ?>" type="hidden" >
	<input name="tablename" value="<?php echo $tble; ?>" type="hidden" >
	<input name="pkmain" value="<?php echo $pkmaint; ?>" type="hidden" >
	<input name="m" value="<?php echo $mode; ?>" type="hidden" >
	<input name="ps" value="<?php echo $patStatus; ?>" type="hidden" >		
	</td>

	</tr><tr>
	<td>Sub Type</td><td style="font-size: 11px; font-style: italic; color: rgb(153, 153, 153);"><input name="codelst_subtyp" class="required" value="<?php echo trim($results["CODELST_SUBTYP"][$rec]); ?>" maxlength="15" type="text" <?PHP echo (($v_exists == 0)? "":"readonly"); ?>> (Maximum Length: 15 Characters)</td>
	</tr><tr>
		<td>Reason</td><td><input name="codelst_desc" class="required" value="<?php echo htmlspecialchars(trim($results["CODELST_DESC"][$rec])); ?>" size="100" maxlength="200" type="text">
		  <span style="font-size: 11px; font-style: italic; color: rgb(153, 153, 153);"> (Maximum Length: 200 Characters)</span></td></tr>
	<tr><td>Sequence</td><td><input name="codelst_seq" class="required" value="<?php echo trim($results["CODELST_SEQ"][$rec]); ?>" size="3" maxlength="3" type="text"><span style="font-size: 11px; font-style: italic; color: rgb(153, 153, 153);">(Maximum Length: 3 Numbers)</span></td></tr>
<?php

	if($v_tablename!="EVENT_DEF"){
		echo "<tr><TD>Hide Code List?</TD>";
		echo '<TD><input type = "radio" name="codelst_hide" value="Y"'.(($results["CODELST_HIDE"][$rec]=='Y')?' checked' : '') .'>Yes</input>
		<input type = "radio" name="codelst_hide" value="N"'.(($results["CODELST_HIDE"][$rec]=='N')?' checked' : '') .'>No</input></TD></tr>';
	}	
}	// for loop end
?>
	<tr>
	<td>
	<BR><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /> 

</td></tr>
</tbody>
</table>
</form>

<?php }else{
	$pk = $_POST['pkcodelsit'];
	$typ = $_POST['codelst_type'];
	$sType = $_POST['codelst_subtyp'];
	$reason = $_POST['codelst_desc'];
	$hide = $_POST['codelst_hide'];
	$seq = $_POST['codelst_seq'];
	$patstatus = $_POST['ps'];
	$tble = $_POST['tablename'];
	$pkmnt = $_POST['pkmain'];
	
	if($_POST['m']=="n"){
		$inQuery = "INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ) VALUES (SEQ_ER_CODELST.NEXTVAL,'".$typ."','".$sType."','".$reason."','".$hide."','".$seq."')";

		$results = executeOCIUpdateQuery($inQuery, $ds_conn);	
//		$redir = "?pk_codelst='".trim($v_pkcl)."'&tablename='".trim($v_tble)."'&pk_codelst_maint='".trim($v_pkma)."'";
	}else{
		$upQuery = "UPDATE ER_CODELST set CODELST_TYPE='".$typ."', CODELST_SUBTYP='".$sType."', CODELST_SEQ='".$seq."',CODELST_DESC='".$reason."', CODELST_HIDE='".$hide."' where PK_CODELST='".$pk."'";

		$results = executeOCIUpdateQuery($upQuery, $ds_conn);
//		$redir = "?pk_codelst='".trim($v_pkcl)."'&tablename='".trim($v_tble)."'&pk_codelst_maint='".trim($v_pkma)."'";
	}	
	
	
OCICommit($ds_conn);
OCILogoff($ds_conn);

echo "Data Saved Successfully !!!";	
//echo '<meta http-equiv="refresh" content="0; url=./codelist_edit.php">';
//echo '<meta http-equiv="refresh" content="0; url=./codelist_edit.php?pk_codelst='.$patstatus.'&tablename='.trim($tble).'&pk_codelst_maint='.$pkmnt.'>';
header("Location:codelist_edit.php?pk_codelst=".$patstatus."&tablename=".trim($tble)."&pk_codelst_maint=".$pkmnt);
}
?>
</div>
</body>
</html>
<?php
}
else header("location: index.php?fail=1");
?>
		
