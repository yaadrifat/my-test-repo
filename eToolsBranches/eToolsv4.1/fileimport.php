<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> Import Delimited File</title>
</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<div id="fedora-content">	
<div class="navigate">Import Delimited File</div>
<br>
<?php
if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 

?>
<form name="fimport" enctype="multipart/form-data" action="fileimport.php" method="POST">
<table>
<tr><td>Delimited File name:</td><td><input class="required" type="file" name="uploadedfile" size="100"/></td></tr> 
<tr><td>Delimiter:</td><td><input type="text" class="required" name="delimiter" size="2"/></td></tr>
<tr><td>Table Name:</td><td><input type="text" class="required" name="tablename" id="tablename" size="20"/></td></tr>



</table>
<BR><input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" /> 

</form>


<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
<tr>
<th align="left">Note</th>
</tr>
<tr><td align="left" valign="top">
<p>Supported File Types: Any delimited file (delimiters such as comma, semicolon, pipe etc. are commonly used)</p>
<p>First row of the file should contain Column Name, no special characters other than underscore, should be one continuous string of characters (no space) and delimited with same delimiter as the actual data rows</p>
<p>Multiple values should be enclosed in brackets [ ]<br>
For example: In case of a check box fields having the values like aa, bb, cc, dd. Here we need to send the value like [aa], [bb], [cc], [dd] in the CSV file</p>
<p>Delimited files cannot be updated to an existing tables.</p>
</td></tr>
</table></div>

<?php } else {

$delimiter = $_POST["delimiter"];
$tablename = $_POST["tablename"];

$curDir = getcwd();
$target_path = $curDir."/upload/";

$target_path = $target_path.basename( $_FILES['uploadedfile']['name']);
$fail = 0;
if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
  echo "The file <font size=2 color=green><b>".basename( $_FILES['uploadedfile']['name'])."</b></font> has been uploaded"."<BR>";

	$row = 0;
	$handle = fopen($target_path, "r");
	$v_sql="";
	$encodingList[] = "ASCII";
	$encodingList[] = "EUC-KR";
	while (($data = fgetcsv($handle, 10000, $delimiter)) !== FALSE) {
	   $num = count($data);
	   $row++;
	   for ($c=0; $c < $num; $c++){
			if ($row == 1) { // writing the column name
				$colName = 'C'.($c+1).'_'.cleanSpecialChars($data[$c]);
				$v_sql = $v_sql.substr(str_replace(" ","",str_replace("-","_",$colName)),0,29)." varchar2(4000),";
			} else { // writing data into the columns
				$edata = $data[$c];
//				echo "edata= ".$edata."<br/>";
/*				if (mb_detect_encoding($edata) != 'ASCII') {
					$encoding = mb_detect_encoding($edata ,$encodingList);
					$edata = htmlspecialchars(mb_convert_encoding($edata , "HTML-ENTITIES", $encoding),ENT_NOQUOTES,'iso-8859-1',false);
					$edata = htmlspecialchars(mb_convert_encoding($edata , "HTML-ENTITIES", $encoding),ENT_QUOTES,'iso-8859-1',false);
				} */
				$colval = "'".str_replace("'","''",$edata)."'";
				$v_sql = $v_sql.$colval.",";
			}
	   }
	
		if ($row == 1) {
			$tabQuery = "SELECT * FROM all_tables WHERE table_name='".strtoupper($tablename)."'";
			$results = executeOCIQuery($tabQuery,$ds_conn);
				if($results_nrows==0){
					$v_sql = "create table ".$tablename." (".substr($v_sql,0,strlen($v_sql)-1).")";
					$results = executeOCIUpdateQuery($v_sql,$ds_conn);
					$v_sql = "";			
				}else{
					$fail = 1;
					break;
				}
		} else {
			$v_sql = "insert into ".$tablename." values (".substr($v_sql,0,strlen($v_sql)-1).")";
			$results = executeOCIUpdateQuery($v_sql,$ds_conn);
			$v_sql = "";			
	    }

	}
	fclose($handle);
	if($fail==0){
		echo "Data Imported to table: <font size=2 color=green><b>".strtoupper($tablename)."</b></font><BR>";
		echo "Total rows imported: <font size=2 color=green><b>".($row - 1)."</b></font>";
	}else{
		echo "<BR>Data has not been imported to table: <font size=2 color=red><b>".strtoupper($tablename)."</b></font><BR>";
		echo "<BR>Either Table <font size=2 color=red><b>".strtoupper($tablename)."</b></font> already exists or Data cannot be updated to <font size=2 color=red><b> ".strtoupper($tablename)."</b></font>";
		$fail = 0;
	}
} else {
	echo "Error uploading file.";
}

} ?>	
</div>
</body>
</html>
<?php

}
else header("location: index.php?fail=1");


function cleanSpecialChars($string)
{
	$specialCharacters = array(
		'#' => '',
		'$' => '',
		'%' => '',
		'&' => '',
		'@' => '',
		'.' => '',
		'�' => '',
		'+' => '',
		'=' => '',
		'�' => '',
		'\\' => '',
		'/' => '',
	);
	
	while (list($character, $replacement) = each($specialCharacters)) {
		$string = str_replace($character, '-' . $replacement . '-', $string);
	}
	
	$string = strtr($string,"������? ����������������������������������������������","AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn");	
	$string = preg_replace('/[^a-zA-Z0-9\-]/', ' ', $string);
	$string = preg_replace('/^[\-]+/', '', $string);
	$string = preg_replace('/[\-]+$/', '', $string);
	$string = preg_replace('/[\-]{2,}/', ' ', $string);
		
	return $string;
}
?>
