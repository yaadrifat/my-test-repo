<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html>
<head><title>Velos eTools</title>

		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" type="text/css" media="print" href="css/print.css">	
		<style type="text/css" media="screen">
			@import url("css/layout.css");
			@import url("css/content.css");
			@import url("css/docbook.css");			
		</style>
		<meta name="MSSmartTagsPreventParsing" content="TRUE">
</head>


	<body>
		<!-- header BEGIN -->
		<div id="fedora-header">

     <table width="100%" border="0" height="72">
            <tr align="left"><td class="headertext" style="padding-left:265px; padding-top:10px;"><BR><BR>You are connected to:
			<font size=4 color=green>
                        
         <?PHP		 
		  if (isset($_SESSION["DATASOURCE"])) {
			echo $_SESSION["DATASOURCE"]." Database"; 

		} else {
			echo "&nbsp;"; 
		}
		?>
         </font> <font size=4 color=red><?PHP if (isset($_SESSION["VERSION"])) echo '{'.$_SESSION["VERSION"].'}'; ?>
         </font> 
		 </td>
		 </tr>

		 </table>

<?PHP
//include ("./querystring_inc.php");
include ("./txt-db-api.php");
	if (isset($_SESSION["MODULE"])) {
		$module = $_SESSION["MODULE"];
	} else {
		$module = "";
	}
	
$db = new Database("etools");
$result = $db->executeQuery("SELECT * from ET_VERSION");

while ($result->next()) {
	$data = $result->getCurrentValuesAsHash();
	$version_num = $data["ET_VERSION_NUM"];
	$build_num = $data["ET_BUILD_NUM"];
}
?>

	
			<div id="fedora-header-logo">
				<a id="link-internal"><img src="img/velos_etools_v3.png" alt="Velos eTools"></a><br>
				<span>Version <b style="color:red;"><?php echo $version_num ?></b> Build#<b style="color:red;"><?php echo $build_num ?></b></span>
			</div>
			<div id="fedora-header-items">
				<span class="fedora-header-icon">
					<a href="./docs/Velos eTools Technical User Manual.pdf" id="link-internal" target="_blank"><img src="img/help.png" alt=" ">Help</a>
				</span> 


			</div>

		<div id="fedora-nav"></div>
		<!-- header END -->
		
		<!-- leftside BEGIN -->
		<div id="fedora-side-left">
		<div id="fedora-side-nav-label">Site Navigation:</div>	<ul id="fedora-side-nav">
<?PHP

			$db = new Database("etools");
			$result = $db->executeQuery("SELECT APP_RIGHTS from ET_GROUPS where PK_GROUPS=".$_SESSION['FK_GROUPS']);
			while ($result->next()) {
				$data = $result->getCurrentValuesAsHash();
				$v_app_rights = $data["APP_RIGHTS"];
			}
			$result = $db->executeQuery("SELECT pk_menu,menu_name,level,disp_seq,visible,page,module,default_access from ET_MENUS order by disp_seq");
			if ($result->getRowCount() > 0) {
			$v_level1 = false;
			$v_level2 = false;
			$v_menu = "";
			$v_has_child = false;
			while ($result->next()) {
				$data = $result->getCurrentValuesAsHash();
				if ($data["level"] == 0){
					$pos = strpos("PAD".$v_app_rights,"|".$data["pk_menu"].":1");
					if (empty($pos)) $pos = -1;
					if (($data["visible"] == 1) && ($pos >= 0)){
						if ($v_level1) {
							$v_menu .= "</li>";
						}
						if ($v_level2) {
							$v_menu .= "</ul>";
							$v_level2 = false;
						}
						$v_level1 = true;

						if ($v_has_child ){
							echo $v_menu;
							$v_has_child = false;
						}
						$v_menu = "";
						if ($data["default_access"] == 1) $v_has_child = true;
							$v_menu .= '<li><a href="menus.php?page='.$data["page"].'&module='.$data["module"].'" id="link-internal">'.$data["menu_name"].'</a>';
						}
				} else {
					$pos = strpos($v_app_rights,'|'.$data["pk_menu"].":1");
					if (empty($pos)) $pos = -1;
					if (($data["visible"] == 1) && ($pos >= 0)){
						if ($data["module"] == $module) {
							$v_level2 = true;
							if ($v_level1) {
								$v_menu .= '<ul>';
							}
							$v_level1 = false;
							$v_menu .= '<li><strong><a href="menus.php?page='.$data["page"].'&module='.$data["module"].'" id="link-internal">'.$data["menu_name"].'</a></strong></li>';
						}

						$v_has_child = true;
					}
				}
			}
			}
echo $v_menu;
?>

		</div>
		<!-- leftside END -->

		<!-- content BEGIN -->
		<div id="fedora-middle-two">
		<!-- footer END -->
</body>
</html>
