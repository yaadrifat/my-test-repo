<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
include("./includes/header.php");

?>

<html>
<head>
    <title>Velos eTools</title>
</head>
<body>
<div id="fedora-content">	

<?PHP
	
$db = new Database("etools");
$result = $db->executeQuery("SELECT APP_RIGHTS from ET_GROUPS where PK_GROUPS=".$_SESSION['FK_GROUPS']);
while ($result->next()) {
	$data = $result->getCurrentValuesAsHash();
	$v_app_rights = $data["APP_RIGHTS"];
}
$result = $db->executeQuery("SELECT pk_menu,menu_name,level,disp_seq,visible,page,module,default_access from ET_MENUS where module = '".$_SESSION["MODULE"]."' order by disp_seq");
if ($result->getRowCount() > 0) {
$v_level1 = false;
$v_level2 = false;
$v_menu = "";
$v_has_child = false;
while ($result->next()) {
	$data = $result->getCurrentValuesAsHash();
	if ($data["level"] == 1){
		$pos = strpos($v_app_rights,'|'.$data["pk_menu"].":1");
		if (empty($pos)) $pos = -1;
		if (($data["visible"] == 1) && ($pos >= 0)){
				$v_menu .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><a href="menus.php?page='.$data["page"].'&module='.$data["module"].'" id="link-internal">'.$data["menu_name"].'</a></strong><BR><BR>';
			$v_has_child = true;
		}
	} else {
		echo "<div class=navigate>".$data["menu_name"]."</div><BR>";
	}
}
}
echo $v_menu;
?>

</div>
</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
