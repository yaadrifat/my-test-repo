<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> Import Lookup</title>

<SCRIPT language="Javascript1.2">
function validate(form,lkpnames){

	if (form.tablename.value == ""){
		alert("Table/View name cannot be blank.");
		form.tablename.focus();
		return false;
	}

	if (form.lookupname.value == ""){
		alert("Lookup Name cannot be blank.");
		form.lookupname.focus();
		return false;
	}
	
	var v_lkpnames = new Array();
	v_lkpnames = lkpnames.split('|');
	for (var i=0;i<v_lkpnames.length;i++){
		if (v_lkpnames[i] == form.lookupname.value){
			alert("Lookup Name already exists.");
			return false;
		}
	}
	return true;
}
</SCRIPT>
	
<script language="javascript" type="text/javascript">
function refreshlist(table){

table=table.toUpperCase()
var length=table.length;
count = tables.length;
var tlist = "";
for (var i=0;i<count;i++){
	if (table == tables[i].substring(5,length+5)){
		tlist = tlist + '<tr onMouseOver="bgColor=\'#a4bef1\'" onMouseOut="bgColor=\'#FFFFFF\'">'+"<td ondblclick='document.lookup.tablename.value = this.innerHTML'>" + tables[i] + "</td></tr>";
	}
}


 
tlist = (tlist.length == 0) ? tlist="No tables found":"<table border='1'>"+tlist+"</table>";
//document.getElementById('tables').innerHTML = "<textarea rows=13 cols=45>"+tlist+"</textarea>";
document.getElementById('tables').innerHTML = tlist;
}
      

</script>

<script> 
var tables = new Array();
</script>
</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

$query="select owner || '.' || table_name as tables from dba_tables where owner in ('ERES','ESCH','EPAT') 
union
select owner || '.' || view_name as tables from dba_views where owner in ('ERES','ESCH','EPAT') 
order by 1
";
$results = executeOCIQuery($query,$ds_conn);
for ($rec = 0; $rec < $results_nrows; $rec++){
?>
		<script>tables[<?PHP echo $rec; ?>] = <?PHP echo '"'.$results["TABLES"][$rec].'"';?>;</script>
<?PHP
	}
?>
<body onLoad="document.lookup.tablename.focus();">
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<?php 

if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 

$query_sql = "select distinct lkptype_name from er_lkplib,er_lkpcol where pk_lkplib = fk_lkplib and nvl(lkptype_type,'Y') not in ('dyn_p','dyn_s','dyn_a')";
$results = executeOCIQuery($query_sql,$ds_conn);
$v_lkpnames = "";
for ($rec = 0; $rec < $results_nrows; $rec++){
	$v_lkpnames .= $results["LKPTYPE_NAME"][$rec].'|';
}
?>

<div class="navigate">Create Dynamic Lookup from Table/View</div>
<form name="lookup" action="import_lookup_table.php" method="POST" onSubmit="if (validate(document.lookup,'<?PHP echo $v_lkpnames; ?>') == false) return false;">
<table border="0">
<tr><td valign="top">
<BR><table border="0">
<tr><td>Table/View name:</td><td><input type="text" class="required" name="tablename" size="30" onkeyup="refreshlist(document.lookup.tablename.value);"/></td></tr>
<tr><td>Lookup Name:</td><td><input type="text" class="required" name="lookupname" maxlength="20"/></td></tr>
<tr><td>Version:</td><td><input type="text" name="lookupVersion" maxlength="10"/></td></tr>
<tr><td>Description:</td><td><input type="text" name="lookupDesc" size="75" maxlength="250"/></td></tr>
<tr><td>Type:</td><td><input type="text" name="lookupType" maxlength="20"/></td></tr>
</table>
<BR><input type="image" name="submit" value="Submit" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';" />

</td><td height="200px">

<style>DIV.tables {
height:100%;
overflow: auto;
align:left;
}</style>

<div class="tables" id="tables"></div>
</td></tr>
</table>
</form>
<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
<tr>
<th align="left">Note</th>
</tr>
<tr><td align="left" valign="top">
<p>Supported File Types: Any delimited file (delimiters such as comma, semicolon, pipe etc. are commonly used)</p>
<p>First row of the file should contain Column Name, no special characters other than '_' (underscore), should be one continuous string of characters (no space) and delimited with same delimiter as the actual data rows.</p>
</td></tr>
</table></div>

<?php } else {

  $tablename = strtoupper($_POST["tablename"]);
  $lookupName = $_POST["lookupname"];
  $lookupVersion = $_POST["lookupVersion"];
  $lookupDesc = $_POST["lookupDesc"];
  $lookupType = $_POST["lookupType"];


	echo '<div class="navigate">Create Dynamic Lookup from Table/View</div>';

	echo '<form action="import_lookup_data.php" method="POST">';
	  echo '<input type="hidden" name="lookupName" value="'.$lookupName.'"/>';
	  echo '<input type="hidden" name="lookupVersion" value="'.$lookupVersion.'"/>';
	  echo '<input type="hidden" name="lookupDesc" value="'.$lookupDesc.'"/>';
	  echo '<input type="hidden" name="lookupType" value="'.$lookupType.'"/>';
	  echo '<input type="hidden" name="tablename" value="'.$tablename.'"/>';
?>
	  <table width="100%" border="1">
	  <tr>
	  <th width="20%">Column Name</th>
	  <th width="20%">Display Name</th>
	  <th width="20%">Keyword</th>
	  <th width="10%">Sequence</th>
	  <th width="10%">Show</th>
	  <th width="10%">Searchable</th>
	  <th width="10%">Width(%)</th>
	  </tr>
<?php          

if (strlen(strpos($tablename,".")) == 0){
	$query="select column_name,data_type from dba_tab_cols where table_name = '".$tablename."'";
} else {
	list($owner,$table) = explode(".",$tablename);
	$query="select column_name,data_type from dba_tab_cols where table_name = '".$table."' and owner = '".$owner."'";
}
$results = executeOCIQuery($query,$ds_conn);
$dd_yn = '<option value="Y" selected>Yes</option><option value="N">No</option>';
$width = round(100 / ($results_nrows + 1));
for ($rec = 0; $rec < $results_nrows; $rec++){
	echo '<TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';
	echo '<td><input style="background-color:#d1cebf" readonly size="30" type="text" name="colName['.$rec.']" value="'.$results["COLUMN_NAME"][$rec].'"/></td>';
	echo '<td><input size="30" type="text" name="dispName['.$rec.']" value="'.$results["COLUMN_NAME"][$rec].'"/></td>';
	echo '<td><input size="30" type="text" name="keyword['.$rec.']" value="'.substr($results["COLUMN_NAME"][$rec],0,20).'"/></td>';
	echo '<td align="center"><input size="5" type="text" name="colSeq['.$rec.']" value="'.($rec + 1).'"/></td>';
	echo '<td align="center"><select name="colShow['.$rec.']">'.$dd_yn.'</select></td>';
	echo '<td align="center"><select name="colSearch['.$rec.']">'.$dd_yn.'</select></td>';
	echo '<td><input size="5" type="text" name="colWidth['.$rec.']" value="'.$width.'"/></td>';
	echo '</tr>';
	echo '<input type="hidden" name="colType['.$rec.']" value="'.$results["DATA_TYPE"][$rec].'"/>';
}
  echo "</table>";
  echo "<BR>";

  if ($results_nrows > 0) {
	?>
	<input type="image" name="submit" value="Submit" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';" />
	<?PHP
	}
  echo "</form>";



}

?>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
