<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velink -> ICD Harvest View</title>
</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<p fontsize="12"><b><u>ACC-NCDR ICD Registry Harvest Files</u></b></p>
<?PHP

$v_command = 'dir /b .\\icd\\*.zip';
$output = "";
$return_var = "";
exec($v_command,$output,$return_var);

foreach($output as $temp_output){
	echo '<a href="./icd/'.$temp_output.'">'.$temp_output.'</a>';
	echo "<BR>";
}

	
OCICommit($ds_conn);
OCILogoff($ds_conn);
?>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
