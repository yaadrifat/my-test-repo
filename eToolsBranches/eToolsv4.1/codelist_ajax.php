<?php

	
	
session_start();	// Maintain session state
//require_once "./includes/session.php";
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){

include("./includes/oci_functions.php");
include ("./txt-db-api.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]);


if (isset($_GET['pk_codelst_maint']) ) {
$v_pk_codelst_maint =  $_GET['pk_codelst_maint'];
$v_sequence = $_GET['sequence'];
$_SESSION['pk_codelst_maint'] = $v_pk_codelst_maint;
} else {
if (isset($_SESSION['pk_codelst_maint']) ) {
$v_pk_codelst_maint =  $_SESSION['pk_codelst_maint'];
} else {
$v_pk_codelst_maint =  "";
}
}

$db = new Database("etools");
$rs = $db->executeQuery("select CODELST_TYPE,TABLE_NAME,BROWSER_FLAG,DD_CB_FLAG from ET_CODELST_MAINT where PK_CODELST_MAINT = ".$v_pk_codelst_maint);
if ($rs->getRowCount() > 0) {
	while ($rs->next()) {
		$data = $rs->getCurrentValuesAsHash();
		$v_codelst_type = $data["CODELST_TYPE"];
		$v_tablename = $data["TABLE_NAME"];
		$v_browser = $data["BROWSER_FLAG"];
		$v_dd_cb = $data["DD_CB_FLAG"];
	}
}

$_SESSION['codelst_type'] = $v_codelst_type;
if ($_GET['auto'] == 'AUTO'){
	$query="select pk_codelst from $v_tablename where codelst_type = '".$v_codelst_type."' order by codelst_desc";
	$results = executeOCIQuery($query,$ds_conn);
	for ($rec = 0; $rec < $results_nrows; $rec++){
		$query="update $v_tablename set codelst_seq = ".(($rec + 1) * 5)." where pk_codelst = ".$results["PK_CODELST"][$rec];
		$results1 = executeOCIUpdateQuery($query,$ds_conn);
	}
}

if ($v_tablename == "ER_CODELST") {
	$query="select pk_codelst,codelst_type,codelst_subtyp,codelst_desc,codelst_seq,
	decode(codelst_custom_col,'chkbox','Checkbox',
	'dropdown','Dropdown',
	'browser','Browser',
	'splfld','Special Field'
	) as codelst_custom_col,
	codelst_hide from $v_tablename where codelst_type = '".$v_codelst_type."' order by $v_sequence";
} else if($v_tablename == "EVENT_DEF"){
//		$query="select event_id,chain_id,event_type,name,DESCRIPTION,(SELECT CODELST_DESC from SCH_CODELST WHERE PK_CODELST= EVENT_LIBRARY_TYPE) EVENT_LIBRARY_TYPE from $v_tablename where event_type = 'L' order by name";
		$query = "select max(EVENT_ID) as EVENT_ID,name,DESCRIPTION from EVENt_DEF where event_type = 'L'  group by name,DESCRIPTION order by name";
	}else{
		$query = "select pk_codelst,codelst_type,codelst_subtyp,codelst_desc,codelst_seq,'' as codelst_custom_col,codelst_hide from $v_tablename where codelst_type = '".$v_codelst_type."' order by $v_sequence";
}	
$results = executeOCIQuery($query,$ds_conn);
$rows = $results_nrows;


if($v_tablename == "EVENT_DEF"){
	$v_html = '<table width="100%"><tr><td width="50%">';
	$v_html .= "Total Rows: ".$results_nrows."</td>";
	$v_html .= '<td width="50%"><a href="codelist_new.php?pk_codelst_maint='.$v_pk_codelst_maint.'">Add New Code List</a>';
	$v_html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
//	$v_html .= '<img src="./img/seq_clist.png" onmouseover="this.src=\'./img/seq_clist_m.png\';" onmouseout="this.src=\'./img/seq_clist.png\';" onclick="if (confirm(\'Click OK to confirm code list sequencing\')) refresh(document.codelist.pk_codelst_maint.value,\'CODELST_DESC\',\'AUTO\')"/></td>';
	$v_html .= "</tr></table>";

	$v_html .= '<Table border="1" width="100%"><thead><TR>';
	$v_html .= '<TH width="2%"></TH>';
	$v_html .= '<TH width="30%" onclick="refresh(document.codelist.pk_codelst_maint.value,\'CODELST_DESC\',\'\')">Name</TH>';	
	$v_html .= '<TH width="40%" onclick="refresh(document.codelist.pk_codelst_maint.value,\'CODELST_DESC\',\'\')">Description</TH>';
	$v_html .= '<TH width="15%" onclick="refresh(document.codelist.pk_codelst_maint.value,\'CODELST_DESC\',\'\')">Library Type</TH>';	
//	$v_html .= '<TH width="10%" onclick="refresh(document.codelist.pk_codelst_maint.value,\'CODELST_SEQ\',\'\')">Event Library Type</TH>';
//	$v_html .= '<TH width="10%">Event Type</TH>';
//	if ($v_browser == 1 || $v_dd_cb == 1) $v_html .= '<TH width="10%">Flag</TH>'; 
	$v_html .= '<TH width="10%"></TH>';
	$v_html .= '</TR></thead>';	

	for ($rec = 0; $rec < $rows; $rec++){

	$libtq = "select * from sch_codelst where codelst_type='lib_type' and pk_codelst=(select EVENT_LIBRARY_TYPE from event_def where event_id='".$results["EVENT_ID"][$rec]."')";
	$librs = executeOCIQuery($libtq,$ds_conn);	

	$v_hide = ($results["CODELST_HIDE"][$rec] == "Y"?'<font color="gray">':'');	
		$v_html .= '<tbody><TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';
	
		$v_html .= '<TD><a href="#" class="tip"><img src="./img/info_1.png">';
		$v_html .= '<span>';
		$v_html .= '<table class="ttip" width="100%">';
		$v_html .= '<tr><td width="25%" valign="top"><b>Event ID</b></td><td width="75%">'.$results["EVENT_ID"][$rec].'</td><tr>';
//		$v_html .= '<tr><td valign="top"><b>Code List Type</b></td><td>lib_type</td><tr>';				
//		$v_html .= '<tr><td valign="top"><b>Code List Type</b></td><td>'.$results["CODELST_TYPE"][$rec].'</td><tr>';
//		$v_html .= '<tr><td valign="top"><b>Sub Type</b></td><td>'.$results["CODELST_SUBTYP"][$rec].'</td><tr></tbody>';
		$v_html .= '</table>';
		$v_html .= '</span></a>';

		$v_html .= '<TD>'.$v_hide.$results["NAME"][$rec].'</TD>';		
		$v_html .= '<TD>'.$v_hide.$results["DESCRIPTION"][$rec].'</TD>';
		$v_html .= '<TD>'.$v_hide.$librs["CODELST_DESC"][0].'</TD>';		
//		$v_html .= '<TD>'.$v_hide.$results["EVENT_LIBRARY_TYPE"][$rec].'</TD>'; 
//		$v_html .= '<TD>'.$v_hide.$results["EVENT_TYPE"][$rec].'</TD>';
//		if ($v_browser == 1 || $v_dd_cb == 1) $v_html .= '<TD>'.$v_hide.$results["CODELST_CUSTOM_COL"][$rec]."&nbsp;"; 
//		$v_html .= '<td><a href=codelist_edit.php?pk_codelst='.$results["PK_CODELST"][$rec]."&tablename=".$v_tablename."&pk_codelst_maint=".$v_pk_codelst_maint.">Edit</a></td>";
/*		if(trim($librs["CODELST_SUBTYP"][0])=="default"){
			$v_html .= '<td>Restricted</td>';					
		}else{
			$v_html .= '<td><a href=codelist_edit.php?event_id='.$results["EVENT_ID"][$rec]."&tablename=".$v_tablename."&pk_codelst_maint=".$v_pk_codelst_maint.">Edit</a></td>";				
		} */
		$v_html .= '<td><a href=codelist_edit.php?event_id='.$results["EVENT_ID"][$rec]."&tablename=".$v_tablename."&pk_codelst_maint=".$v_pk_codelst_maint.">Edit</a></td>";				
		$v_html .= '</TR>';
	}
		$v_html .= '</TABLE>';

}else{
	if($results["CODELST_TYPE"][0]=="role"){
		$v_html = '<table width="100%"><tr><td width="50%">';
		$v_html .= "Total Rows: ".$results_nrows."</td>";
		$v_html .= '<td width="50%"><a href="codelist_new.php?pk_codelst_maint='.$v_pk_codelst_maint.'">Add New Code List</a>';
		$v_html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		$v_html .= '<img src="./img/seq_clist.png" onmouseover="this.src=\'./img/seq_clist_m.png\';" onmouseout="this.src=\'./img/seq_clist.png\';" onclick="if (confirm(\'Click OK to confirm code list sequencing\')) refresh(document.codelist.pk_codelst_maint.value,\'CODELST_DESC\',\'AUTO\')"/></td>';
		$v_html .= "</tr></table>";
		$v_html .= '<Table border="1" width="100%"><thead><TR>';
		$v_html .= '<TH width="2%"></TH>';
		$v_html .= '<TH width="35%" onclick="refresh(document.codelist.pk_codelst_maint.value,\'CODELST_DESC\',\'\')">Description</TH>';
		$v_html .= '<TH width="10%" onclick="refresh(document.codelist.pk_codelst_maint.value,\'CODELST_SEQ\',\'\')">Sequence</TH>';
		$v_html .= '<TH width="10%">Sub Type</TH>';
		if ($v_browser == 1 || $v_dd_cb == 1) $v_html .= '<TH width="10%">Flag</TH>'; 
		$v_html .= '<TH width="15%"></TH>';
		$v_html .= '</TR></thead>';
		for ($rec = 0; $rec < $results_nrows; $rec++){
		$v_hide = ($results["CODELST_HIDE"][$rec] == "Y"?'<font color="gray">':'');
			$v_html .= '<tbody><TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';		
			$v_html .= '<TD><a href="#" class="tip"><img src="./img/info_1.png">';
			$v_html .= '<span>';
			$v_html .= '<table class="ttip" width="100%">';
			$v_html .= '<tr><td width="25%" valign="top"><b>Primary Key</b></td><td width="75%">'.$results["PK_CODELST"][$rec].'</td><tr>';
			$v_html .= '<tr><td valign="top"><b>Code List Type</b></td><td>'.$results["CODELST_TYPE"][$rec].'</td><tr>';
			$v_html .= '<tr><td valign="top"><b>Sub Type</b></td><td>'.$results["CODELST_SUBTYP"][$rec].'</td><tr></tbody>';
			$v_html .= '</table>';
			$v_html .= '</span></a>';			
			$v_html .= '<TD>'.$v_hide.$results["CODELST_DESC"][$rec].'</TD>';
			$v_html .= '<TD>'.$v_hide.$results["CODELST_SEQ"][$rec].'</TD>'; 
			$v_html .= '<TD>'.$v_hide.$results["CODELST_SUBTYP"][$rec].'</TD>';
			if ($v_browser == 1 || $v_dd_cb == 1) $v_html .= '<TD>'.$v_hide.$results["CODELST_CUSTOM_COL"][$rec]."&nbsp;"; 
			$v_html .= '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=codelist_edit.php?pk_codelst='.$results["PK_CODELST"][$rec]."&tablename=".$v_tablename."&pk_codelst_maint=".$v_pk_codelst_maint.">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=codelist_dca.php?pk_codelst=".$results["PK_CODELST"][$rec]."&tablename=".$v_tablename."&pk_codelst_maint=".$v_pk_codelst_maint.">Define Code Access</a></td>";
			$v_html .= '</TR>';
		}		
	}else{
		//$query="select count(*) as count from $v_tablename where codelst_type = '".$v_codelst_type."'";
		//$results = executeOCIQuery($query,$ds_conn);
		$v_html = '<table width="100%"><tr><td width="50%">';
		$v_html .= "Total Rows: ".$results_nrows."</td>";
		$v_html .= '<td width="50%"><a href="codelist_new.php?pk_codelst_maint='.$v_pk_codelst_maint.'">Add New Code List</a>';
		$v_html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		$v_html .= '<img src="./img/seq_clist.png" onmouseover="this.src=\'./img/seq_clist_m.png\';" onmouseout="this.src=\'./img/seq_clist.png\';" onclick="if (confirm(\'Click OK to confirm code list sequencing\')) refresh(document.codelist.pk_codelst_maint.value,\'CODELST_DESC\',\'AUTO\')"/></td>';
		$v_html .= "</tr></table>";
		//$v_html .= '<div  style="height:350px;overflow:auto;">';
		$v_html .= '<Table border="1" width="100%"><thead><TR>';
		$v_html .= '<TH width="2%"></TH>';
		//$v_html .= '<TH width="10%">Primary Key</TH>';
		//$v_html .= '<TH width="10%">Type</TH>';
		$v_html .= '<TH width="40%" onclick="refresh(document.codelist.pk_codelst_maint.value,\'CODELST_DESC\',\'\')">Description</TH>';
		$v_html .= '<TH width="10%" onclick="refresh(document.codelist.pk_codelst_maint.value,\'CODELST_SEQ\',\'\')">Sequence</TH>';
		$v_html .= '<TH width="10%">Sub Type</TH>';
		if ($v_browser == 1 || $v_dd_cb == 1) $v_html .= '<TH width="10%">Flag</TH>'; 
		$v_html .= '<TH width="10%"></TH>';
		$v_html .= '</TR></thead>';
		for ($rec = 0; $rec < $results_nrows; $rec++){
		$v_hide = ($results["CODELST_HIDE"][$rec] == "Y"?'<font color="gray">':'');
			$v_html .= '<tbody><TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';
		
			$v_html .= '<TD><a href="#" class="tip"><img src="./img/info_1.png">';
			$v_html .= '<span>';
			$v_html .= '<table class="ttip" width="100%">';
			$v_html .= '<tr><td width="25%" valign="top"><b>Primary Key</b></td><td width="75%">'.$results["PK_CODELST"][$rec].'</td><tr>';
			$v_html .= '<tr><td valign="top"><b>Code List Type</b></td><td>'.$results["CODELST_TYPE"][$rec].'</td><tr>';
			$v_html .= '<tr><td valign="top"><b>Sub Type</b></td><td>'.$results["CODELST_SUBTYP"][$rec].'</td><tr></tbody>';
			$v_html .= '</table>';
			$v_html .= '</span></a>';
			
		//	$v_html .= '<TD>'.$v_hide.$results["PK_CODELST"][$rec].'</TD>';
		//	$v_html .= '<TD>'.$v_hide.$results["CODELST_TYPE"][$rec].'</TD>';
			$v_html .= '<TD>'.$v_hide.$results["CODELST_DESC"][$rec].'</TD>';
			$v_html .= '<TD>'.$v_hide.$results["CODELST_SEQ"][$rec].'</TD>'; 
			$v_html .= '<TD>'.$v_hide.$results["CODELST_SUBTYP"][$rec].'</TD>';
			if ($v_browser == 1 || $v_dd_cb == 1) $v_html .= '<TD>'.$v_hide.$results["CODELST_CUSTOM_COL"][$rec]."&nbsp;"; 
			$v_html .= '<td><a href=codelist_edit.php?pk_codelst='.$results["PK_CODELST"][$rec]."&tablename=".$v_tablename."&pk_codelst_maint=".$v_pk_codelst_maint.">Edit</a></td>";
			$v_html .= '</TR>';
		}
	}
		$v_html .= '</TABLE>';
}
//	$v_html .= '</div>';
OCICommit($ds_conn);
OCILogoff($ds_conn);


echo $v_html;
}
else header("location: index.php?fail=1");
?>