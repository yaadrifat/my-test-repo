PK_CODELST_MAINT#TABLE_NAME#CODELST_TYPE#CATEGORY#DESCRIPTION#ACTION#DISPLAY#BROWSER_FLAG#DD_CB_FLAG#DELETE_FLAG#DELETE_SQL
inc#str#str#str#str#str#int#int#int#int#str
##########
1#ER_CODELST#ADDTYPE##Address Type##0####
2#ER_CODELST#EM##Event Milestone##0####
3#ER_CODELST#SiteType##Site Type##0####
4#ER_CODELST#VM##Visit Milestone##0####
5#ER_CODELST#abflag#Labs#Abnormal Flag##1###1#SELECT COUNT(*) FROM ER_PATLABS WHERE fk_codelst_abflag = ?
6#ER_CODELST#advtype#Adverse Events#More Adverse Event Details##1##1#1#SELECT COUNT(*) FROM sch_adverseve WHERE fk_codelst_aetype = ?
7#ER_CODELST#blinding#Study Summary#Blinding##1###1#SELECT COUNT(*) FROM er_study WHERE fk_codelst_blind = ?
8#ER_CODELST#bloodgr#Patient Demographics#Blood Group##1###1#SELECT COUNT(*) FROM person WHERE fk_codelst_bloodgrp = ?
9#ER_CODELST#disease_site#Study Summary#Disease Site##1###1#SELECT COUNT(*) FROM er_study WHERE INSTR(study_disease_site,'?') > 0
10#ER_CODELST#education#Patient Demographics#Education##1###1#SELECT COUNT(*) FROM person WHERE fk_codelst_edu = ?
11#ER_CODELST#employment#Patient Demographics#Employment##1###1#SELECT COUNT(*) FROM person WHERE fk_codelst_employment = ?
#12#ER_CODELST#enroll_appr#Patient Study Status#Enrollment Approval##1####
#13#ER_CODELST#enroll_denied#Patient Study Status#Enrollment Denied##1####
#14#ER_CODELST#enroll_pending#Patient Study Status#Enrollment Pending##1####
15#ER_CODELST#ethnicity#Patient Demographics#Ethnicity##1###1#SELECT COUNT(*) FROM person WHERE fk_codelst_ethnicity = ? OR person_add_ethnicity = ?
16#ER_CODELST#fillformstat#Form#Form Response Status##1####
#17#ER_CODELST#followup#Patient Study Status#Reason for Followup##1###1#SELECT COUNT(*) FROM ER_PATSTUDYSTAT WHERE patstudystat_reason = ?
18#ER_CODELST#form_query#Form Query#Query Type##1####
19#ER_CODELST#form_resp#Form Query#Query Reason##1####
20#ER_CODELST#frmlibstat#Form#Form Library Status##1####
21#ER_CODELST#frmstat#Form#Form Status##1####
22#ER_CODELST#gender#Patient Demographics#Gender##1####
23#ER_CODELST#job_type#Users#Job Type##1####
24#ER_CODELST#labunit#Labs#Lab Unit##1####
25#ER_CODELST#lnk_type####0####
26#ER_CODELST#marital_st#Patient Demographics#Marital Status##1###1#SELECT COUNT(*) FROM person WHERE fk_codelst_marital = ?
27#ER_CODELST#milePayfor#Milestones#Payment For##1####
28#ER_CODELST#milepaytype#Milestones#Payment Type##1####
#29#ER_CODELST#offstudy#Patient Study Status#Off Study Reason##1###1#SELECT COUNT(*) FROM ER_PATSTUDYSTAT WHERE patstudystat_reason = ?
#30#ER_CODELST#offtreat#Patient Study Status#Off Treatment Reason##1###1#SELECT COUNT(*) FROM ER_PATSTUDYSTAT WHERE patstudystat_reason = ?
31#ER_CODELST#patStatus#Patient Study Status#Patient Study Statuses##1#1##1#SELECT COUNT(*) FROM ER_PATSTUDYSTAT WHERE fk_codelst_stat = ?
32#ER_CODELST#pat_dth_cause#Patient Demographics#Cause of Death##1###1#SELECT COUNT(*) FROM person WHERE fk_codelst_pat_dth_cause = ?
33#ER_CODELST#patient_status#Patient Demographics#Survival Status##1###1#SELECT COUNT(*) FROM person WHERE fk_codelst_pstat = ?
34#ER_CODELST#paymentCat#Milestones#Payment - Payment Type##1####
35#ER_CODELST#peridtype#Patient Demographics#More Patient Details##1##1#1#SELECT COUNT(*) FROM pat_perid WHERE fk_codekst_idtype = ?
36#ER_CODELST#phase#Study Summary#Study Phase##1###1#SELECT COUNT(*) FROM er_study WHERE fk_codelst_phase = ?
37#ER_CODELST#prim_sp#Users#Primary Specialty##1####
38#ER_CODELST#ptst_dth_stdrel#Patient Study Status#Death Related to Study##1###1#SELECT COUNT(*) FROM ER_PATPROT WHERE fk_codelst_ptst_dth_stdrel = ?
39#ER_CODELST#ptst_eval#Patient Study Status#Evaluable Status##1###1#SELECT COUNT(*) FROM ER_PATPROT WHERE fk_codelst_ptst_eval = ?
40#ER_CODELST#ptst_eval_flag#Patient Study Status#Evaluable Flag##1###1#SELECT COUNT(*) FROM ER_PATPROT WHERE fk_codelst_ptst_eval_flag = ?
41#ER_CODELST#ptst_ineval#Patient Study Status#Inevaluable Status##1###1#SELECT COUNT(*) FROM ER_PATPROT WHERE fk_codelst_ptst_ineval = ?
42#ER_CODELST#ptst_survival#Patient Study Status#Survival Status##1###1#SELECT COUNT(*) FROM ER_PATPROT WHERE fk_codelst_ptst_survival = ?
43#ER_CODELST#query_status#Form Query#Query Status##1####
44#ER_CODELST#race#Patient Demographics#Race##1###1#SELECT COUNT(*) FROM person WHERE fk_codelst_race = ? OR person_add_race = ?
45#ER_CODELST#randomization#Study Summary#Randomization##1###1#SELECT COUNT(*) FROM er_study WHERE fk_codelst_random = ?
46#ER_CODELST#report####0####
47#ER_CODELST#reptype####0####
48#ER_CODELST#research_type#Study Summary#Research Type##1###1#SELECT COUNT(*) FROM er_study WHERE fk_codelst_restype = ?
49#ER_CODELST#role#Study Team#Study Team Role##1####
50#ER_CODELST#role_type####0####
51#ER_CODELST#screenOutcome#Patient Study Status#Screening Outcome##1###1#SELECT COUNT(*) FROM ER_PATSTUDYSTAT WHERE screening_outcome = ?
52#ER_CODELST#section#Study Sections#Section##1####
53#ER_CODELST#site_type#Organization#Site Type##1####
54#ER_CODELST#skin####0####
55#ER_CODELST#sponsor#Study Summary#Sponsor Name##1###1#SELECT COUNT(*) FROM er_study WHERE fk_codelst_sponsor = ?
56#ER_CODELST#stdphase#Labs#Study Phase##1####
57#ER_CODELST#studySiteType#Study Team#Type##1####
58#ER_CODELST#study_division#Study Summary#Division##1###1#SELECT COUNT(*) FROM er_study WHERE fk_codelst_division = ?
59#ER_CODELST#study_type#Study Summary#Study Type##1###1#SELECT COUNT(*) FROM er_study WHERE fk_codelst_type = ?
60#ER_CODELST#studyaprno####0####
61#ER_CODELST#studyaprstat####0####
62#ER_CODELST#studyidtype#Study Summary#More Study Details##1##1#1#SELECT COUNT(*) FROM er_studyid WHERE fk_codelst_idtype = ?
63#ER_CODELST#studyscope#Study Summary#Study Scope##1###1#SELECT COUNT(*) FROM er_study WHERE fk_codelst_scope = ?
64#ER_CODELST#studystat#Study Status #Study Status#active,not_active,active_cls,prmnt_cls#1#1###
65#ER_CODELST#studystataprno####0####
66#ER_CODELST#studyvercat#Study Versions#Category##1####
67#ER_CODELST#studyvertype#Study Versions#Type##1####
68#ER_CODELST#tarea#Study Summary#Therapeutic Area##1###1#SELECT COUNT(*) FROM er_study WHERE fk_codelst_tarea = ?
69#ER_CODELST#teamstatus#Study Team#Study Team Status##0####
70#ER_CODELST#theme####0####
71#ER_CODELST#thoracic_Proc####0####
72#ER_CODELST#thoracic_demo####0####
73#ER_CODELST#thoracic_organ####0####
74#ER_CODELST#thoracic_prim####0####
75#ER_CODELST#treatloc#Patient Study Status#Treatment Location##1###1#SELECT COUNT(*) FROM ER_PATPROT WHERE fk_codelsttloc = ?
76#ER_CODELST#tststat#Labs#Result Status##1####
77#ER_CODELST#versionStatus#Study Versions#Version Status##1####

78#SCH_CODELST#DBTimezone     ##Database Timezone##0####
79#SCH_CODELST#adve_bdsystem  #Adverse Events###0####
80#SCH_CODELST#adve_info      #Adverse Events#Additional Information##0####
81#SCH_CODELST#adve_notify    #Adverse Events#Notified##0####
82#SCH_CODELST#adve_recovery  #Adverse Events#Recovery Description##1####
83#SCH_CODELST#adve_relation  #Adverse Events#Attribution##1####
84#SCH_CODELST#adve_severity  #Adverse Events###0####
85#SCH_CODELST#adve_type      #Adverse Events#Adverse Event Type##1####
86#SCH_CODELST#bgtline_repeat ####0####
87#SCH_CODELST#budget_template####0####
88#SCH_CODELST#category       #Budget#Category##1####
89#SCH_CODELST#cmail_type     ####0####
90#SCH_CODELST#cmail_type_pat ####0####
91#SCH_CODELST#cost_desc      #Event Library#Cost##1####
92#SCH_CODELST#crfstatus      #Event Library#CRF Status##1####
93#SCH_CODELST#currency       #Budget#Currency##1####
94#SCH_CODELST#eventstatus    #Event Library#Event Status##1####
95#SCH_CODELST#fillformstat   #Adverse Events#Form Status##1####
96#SCH_CODELST#outaction      #Adverse Events#Action##1####
97#SCH_CODELST#outcome        #Adverse Events#Outcome Information##0####
98#SCH_CODELST#sch_alert      ####0####
99#SCH_CODELST#studybgt_line  #Budget#Study Budget Line Item##1####
100#SCH_CODELST#studybgt_sec   #Budget#Study Budget Section##1####

101#ER_CODELST#cap_unit#Storage#Storage Capacity Unit##1####
102#ER_CODELST#dashboard#mapping#dashboard##0####
103#ER_CODELST#invStatus#Milestones#Invoice Status##1####
104#ER_CODELST#item_type#mapping#item_type##0####
105#ER_CODELST#milestone_stat#Milestones#Status##1####
106#ER_CODELST#milestone_type#Milestones#Type##1####
107#ER_CODELST#patLoginStat#mapping#patLoginStat##0####
108#ER_CODELST#portalstatus#Portal#Status##1####
109#ER_CODELST#rev_board#Study Status#Review Board##1####
110#ER_CODELST#spec_proctype#Specimen#Processing Type##1####
111#ER_CODELST#spec_q_unit#Specimen#Available Quantity Unit##1####
112#ER_CODELST#specimen_stat#Specimen#Status##1####
113#ER_CODELST#specimen_type#Specimen#Type##1####
114#ER_CODELST#storage_stat#Storage#Status##1####
115#ER_CODELST#store_type#Storage#Type##1####
116#ER_CODELST#studystat_out#Study Status #Outcome##1####
117#ER_CODELST#studystat_type#Study Status #Status Type##1####
118#ER_CODELST#user#Users#More User Details##1##1##
119#SCH_CODELST#budget_stat#Budget#Status##1####

#120#ER_CODELST#specdisposition#eSample#Specimen Disposition##1####
121#ER_CODELST#pathology_stat#Specimen#Pathology Status##1####
122#ER_CODELST#stor_unit_class#Storage#Storage Unit Class##1####
123#ER_CODELST#anatomic_site#Specimen#Anatomic Sites##1####
124#ER_CODELST#templatetype#Storage#Template Types##1####
#125#ER_CODELST#storecategory#eSample#Storage Categories##1####
126#ER_CODELST#tissue_side#Specimen#Tissue Side##1####
127#ER_CODELST#evtaddlcode#Event#Additional Code Details##1##1##

128#SCH_CODELST#lib_type#Event Library#Type##1####
#129#EVENT_DEF#L#Event Library#Category##1####


130#SCH_CODELST#calStatLib#Library Calendar#Status##1####
131#SCH_CODELST#calStatStd#Study Calendar#Status##1####
132#SCH_CODELST#coverage_type#Event # Coverage Type##1####
