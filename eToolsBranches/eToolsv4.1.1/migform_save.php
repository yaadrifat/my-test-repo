<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Data</title>
<script> 
function validate() {
	if (document.migration.TABLE.value == ""){
		alert("Please select a table for migration.");
		document.migration.TABLE.focus();
		return false;
	}
}
</script>	
	
<script> 
var tables = new Array();
</script>	
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
$query="select table_name as tables from dba_tables where owner in ('ERES','ESCH','EPAT') 
order by 1
";
$results = executeOCIQuery($query,$ds_conn);
for ($rec = 0; $rec < $results_nrows; $rec++){
?>
		<script>tables[<?PHP echo $rec; ?>] = <?PHP echo '"'.$results["TABLES"][$rec].'"';?>;</script>
<?PHP
	}
?>

<script language="javascript" type="text/javascript">
function refreshlist(table){

table=table.toUpperCase()
var length=table.length;
count = tables.length;
var tlist = "";
for (var i=0;i<count;i++){
	if (table == tables[i].substring(0,length)){
		tlist = tlist + '<tr onMouseOver="bgColor=\'#a4bef1\'" onMouseOut="bgColor=\'#FFFFFF\'">'+"<td ondblclick='document.migration.TABLE.value = this.innerHTML'>" + tables[i] + "</td></tr>";
	}
}


 
tlist = (tlist.length == 0) ? tlist="No tables found":"<table border='1'>"+tlist+"</table>";
//document.getElementById('tables').innerHTML = "<textarea rows=13 cols=45>"+tlist+"</textarea>";

document.getElementById('tables').innerHTML = tlist;
}
      

</script>

</head>
<body>
<div id="fedora-content">	
<div class="navigate">Migrate Data - Forms - Edit Mapping</div>
<?php



if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
$v_adaptor = $_GET["pk_vlnk_adaptor"];
$v_formname = $_GET["formname"];
$v_pk_formlib = $_GET["pk_formlib"];

?>

<FORM name="migration" action="migform_save.php" method=post onSubmit="if (validate() == false) return false;">

<input type="hidden" name="pk_adaptor" value="<?php echo $v_adaptor; ?>"></input>
<input type="hidden" name="pk_formlib" value="<?php echo $v_pk_formlib; ?>"></input>

<table border="0">
<tr><td valign="top" ><TABLE border="0">
<TR><TD >Form Name: </TD><TD><?PHP echo $v_formname ?></TD></TR>
<TR><TD >Migration Name: </TD><TD><INPUT TYPE="TEXT" NAME="mname" size=50 /></TD></TR>
<TR><TD >Table Name: </TD><TD><INPUT TYPE="TEXT" class="required" NAME="TABLE" size=50 onkeyup="refreshlist(document.migration.TABLE.value);"></INPUT></TD></TR>
</TABLE>
<BR><input type="image" name="submit" value="SUBMIT" src="./img/submit.png"  align="absmiddle" border="0" onmouseover="this.src='./img/submit_m.png';" onmouseout="this.src='./img/submit.png';" />
</td><td height="200px">

<style>DIV.tables {
height:100%;
overflow: auto;
align:left;
}</style>

<div class="tables" id="tables"></div>
</td></tr>
</table>
</form>

<?php 
} else {


$query = "insert into velink.vlnk_adapmod (pk_vlnk_adapmod,fk_vlnk_adaptor,table_name,fk_form,adapmod_name) values (velink.seq_vlnk_adapmod.nextval,".$_POST["pk_adaptor"].",trim('".$_POST['TABLE']."'),".$_POST['pk_formlib'].",'".$_POST['mname']."')";
$results = executeOCIUpdateQuery($query,$ds_conn);

$query = "grant select on ".$_POST['TABLE']." to VELINK";
$results = executeOCIQueryDDL($query,$ds_conn);

OCICommit($ds_conn);
OCILogoff($ds_conn);

echo "Data Saved Successfully !!!";

$url = "./mig_forms.php?pk_vlnk_adaptor=".$_POST['pk_adaptor'];
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";
?>
<?PHP
}

?>
</div>


</body>
</html>


<?php
}
else header("location: index.php?fail=1");
?>
		
