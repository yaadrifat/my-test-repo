<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">

    <title>Velos eTools -> Save Query</title>
</head>
<?php
include("./includes/header.php");

include_once "./adodb/adodb.inc.php";
include_once('./adodb/adodb-pager.inc.php'); 

$db = NewADOConnection("oci8");
$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);


if (!$db) die("Connection failed");


?>
<body>
<div id="fedora-content">	


<?php


$v_query = html_entity_decode(stripslashes($_POST['query']));
$v_queryname = $_POST["queryname"];
$v_querydesc = $_POST["querydesc"];


if (empty($v_query) || empty($v_queryname)) {
	echo "Query name or SQL missing, please try again";
} else {

	$rs = $db->Execute("select count(*) from ( ".$v_query.")");
	if (!$rs) {
		echo "SQL ERROR: ";
	    print $db->ErrorMsg();
	} else {

	if (empty($_POST["pk"])) {
		$recordSet = $db->Execute("select velink.seq_vlnk_queryman.nextval from dual");
		$pk = $recordSet->fields[0];
		$v_insert_sql = "insert into velink.vlnk_queryman (pk_queryman,query_name,query_desc,record_type) values (".$recordSet->fields[0].",'$v_queryname','$v_querydesc','N')";
	} else {
		$v_insert_sql = "update velink.vlnk_queryman set query_name = '$v_queryname',query_desc='$v_querydesc' where pk_queryman=".$_POST["pk"];
		$pk = $_POST["pk"];
	}
	
	$v_status = $db->Execute($v_insert_sql);
	if (!$v_status) print $db->ErrorMsg();

	$v_status = $db->UpdateClob('velink.vlnk_queryman','query',$v_query,'pk_queryman='.$pk); 
	if (!$v_status) print $db->ErrorMsg();

	echo "Query Saved";

	$url = "./query_manager.php";
	echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";
}

?>


</TABLE>
<?php
} ?>
</div>


</body>
</html>
<?php 
$db->Close();

}
else header("location: index.php?fail=1");
?>
