<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> CDUS Delete</title>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 
?>

</head>
<body>
<div id="fedora-content">	
<div class="navigate">CDUS Submission - Validate - Delete</div>
<?php



$pk_exp_datadetails = $_GET["pk_exp_datadetails"];

$query = "delete from exp_datadetails where pk_exp_datadetails=$pk_exp_datadetails";

$results = executeOCIUpdateQuery($query,$ds_conn);

OCICommit($ds_conn);
OCILogoff($ds_conn);

echo "Data Deleted Successfully !!!";

$pk_datamain = $_GET["pk_exp_datamain"];
$fk_exp_definition = $_GET["fk_exp_definition"];
	
$url = "./cdus_errlogview.php?pk_datamain=".$pk_datamain."&fk_exp_definition=".$fk_exp_definition;
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";

?>
<!-- <meta http-equiv="refresh" content="0; url=./codelist.php"> -->
<?PHP

?>
</div>
<!--


-->
</body>
</html>


<?php
}
else header("location: index.php?fail=1");
?>
		
