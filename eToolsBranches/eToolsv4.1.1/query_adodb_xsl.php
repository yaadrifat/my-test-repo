<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Publish Report</title>
<script>
function validate(){
	if (document.report.reportname.value == ""){
		alert("Report name can not be blank.");
		document.report.reportname.focus();
		return false;
	}
}
</script>
</head>
<?php
include("./includes/header.php");

include_once "./adodb/adodb.inc.php";
include_once('./adodb/adodb-pager.inc.php'); 

$db = NewADOConnection("oci8");
$db->Connect($_SESSION["DB"], "eres", $_SESSION["DS_PASS"]);


if (!$db) die("Connection failed");


?>
<body>
<div id="fedora-content">	

<div class="navigate">Publish Report</div>
<br>
<?php
if (isset($_GET['pk_queryman']) ) {
	$v_pk =  $_GET['pk_queryman'];
	echo "<FORM name='report' action=query_adodb_xsl.php method=post onsubmit='if (validate() == false) return false;'>";
	echo "<INPUT TYPE=HIDDEN NAME=pk_queryman VALUE=".$v_pk."></INPUT>";
	echo "<TABLE>";
	echo "<TR>";
	echo "<TD>Report Name:</TD>";
	echo "<TD><INPUT TYPE=TEXT class=required NAME=reportname size=50></INPUT></TD>";
	echo "</TR>";
	echo "<TD>Description:</TD>";
	echo "<TD><INPUT TYPE=TEXT NAME=reportdesc size=100></INPUT></TD>";
	echo "</TR>";
	echo "<TR><TD>&nbsp;</TD></TR>";
	echo "</TABLE>";

	$recordSet = $db->Execute("select query from velink.vlnk_queryman where pk_queryman = ".$v_pk);
	$v_query =  $recordSet->fields[0];
	$rs = $db->Execute($v_query);
	echo "<TABLE BORDER=1>";
	echo "<TH>Column Name</TH><TH>Report Column Name</TH><TH>Width(%)</TH><TH>Summarize</TH><TH>Sum Total</TH><TH>Exclude</TH>";
	$field_count = $rs->FieldCount();
	$grouping = '<option value="" selected>Select an option</option>';
	for ($i=0;$i<$field_count;$i++){
		$field = $rs->FetchField($i);
		$field_name = $field->name;
/*
		if ($i % 2) {
			echo '<TR class="even">';
		} else {
			echo '<TR>';
		}
*/		
		echo '<TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';
		echo '<TD><INPUT READONLY TYPE=TEXT NAME=field_name['.$i.'] value="'.$field_name.'" SIZE=25></INPUT></TD>';
		echo '<TD><INPUT TYPE=TEXT NAME=field_dispname['.$i.'] value="'.$field_name.'" SIZE=50></INPUT></TD>';
		echo '<TD><INPUT TYPE=TEXT NAME=field_width['.$i.'] value="10" size=2></INPUT>%</TD>';
		echo '<TD align="center"><INPUT TYPE="CHECKBOX" NAME="summarize['.$i.']"></INPUT></TD>';
		echo '<TD align="center"><INPUT TYPE="CHECKBOX" NAME="total['.$i.']"></INPUT></TD>';
		echo '<TD align="center"><INPUT TYPE="CHECKBOX" NAME="exclude['.$i.']"></TD>';
		echo "</TR>";
		$grouping .= "<option value=".$field_name.">".$field_name."</option>";
	}	
	echo "</TABLE>";
	echo "<BR>";
	
	echo "Group by: <select name=grouping1>".$grouping."</select>";
	echo "&nbsp;&nbsp;&nbsp;&nbsp;";
	echo "<select name=grouping2>".$grouping."</select>";
	echo "&nbsp;&nbsp;&nbsp;&nbsp;";
	echo "<select name=grouping3>".$grouping."</select>";

	echo "<BR>";
	echo "<BR>";
	
//	Echo '<input type="submit" name="submit" value="Submit"></input>';
?>
<input type="image" name="submit" value="submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
<?PHP
	
	echo "</FORM>";

}
if (($_SERVER['REQUEST_METHOD'] == 'POST')) {

	$v_pk_queryman = $_POST['pk_queryman'];
	$v_reportname = $_POST['reportname'];
	$v_reportdesc = $_POST['reportdesc'];
	$recordSet = $db->Execute("select query from velink.vlnk_queryman where pk_queryman = ".$v_pk_queryman);
	$v_query =  $recordSet->fields[0];
//	$v_xsl =  $recordSet->fields[1];

	if (isset($_POST['exclude'])) $v_exclude = $_POST['exclude'];
	$v_field_name = $_POST['field_name'];
	$v_field_dispname = $_POST['field_dispname'];
	$v_field_width = $_POST['field_width'];
	if (isset($_POST['summarize'])) $v_summarize = $_POST['summarize'];
	$v_grouping1 = $_POST['grouping1'];
	$v_grouping2 = $_POST['grouping2'];
	$v_grouping3 = $_POST['grouping3'];
	if (isset($_POST['total'])) $v_total = $_POST['total'];


	$v_xsl = '<?xml version="1.0"?> 
	<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>'.chr(13);

	$v_summarize_count = 0;
	$v_total_count = 0;
	for($i=0;$i<count($v_field_name);$i++){
		if (isset($v_summarize[$i])){
			$v_xsl .= '<xsl:key name="RecordsBy'.$v_field_name[$i].'" match="ROW" use="'.$v_field_name[$i].'" />'.chr(13);
			$v_summarize_count = $v_summarize_count + 1;
		}
		if (isset($v_total[$i])){
			$v_total_count = $v_total_count + 1;
		}
	}

	if (!empty($v_grouping1)) $v_group = $v_grouping1;
	
	if (!empty($v_grouping2)) {
		if (empty($v_group)){
			$v_group = $v_grouping2;
		} else {
			$v_group = $v_group.",' ',".$v_grouping2;
		}
	}

	if (!empty($v_grouping3)) {
		if (empty($v_group)){
			$v_group = $v_grouping3;
		} else {
			$v_group = $v_group.",' ',".$v_grouping3;
		}
	}

	if (!empty($v_group)) {
		if (strpos($v_group,',')){
			$v_grouping = 'concat('.$v_group.')';
		} else {
			$v_grouping = $v_group;
		}
		$v_xsl .= '<xsl:key name="groupBy" match="ROW" use="'.$v_grouping.'" />'.chr(13);
	}

	$v_xsl .= '<xsl:param name="hdrFileName" />
	<xsl:param name="ftrFileName" />
	<xsl:param name="repTitle"/>
	<xsl:param name="repName"/>
	<xsl:param name="repBy"/>
	<xsl:param name="repDate"/>
	<xsl:param name="argsStr"/>
	<xsl:param name="wd"/>
	<xsl:param name="xd"/>
	<xsl:param name="hd"/>
	<xsl:param name="cond"/>
	<xsl:param name="hdrflag"/>
	<xsl:param name="ftrflag"/>
	<xsl:template match="/">
	<HTML>
	<HEAD><TITLE> <xsl:value-of select="$repName" /> </TITLE>
	<link rel="stylesheet" href="./styles/common.css" type="text/css"/>
	<style>
	thead { display: table-header-group; }
	tfoot { display: table-footer-group; }
	</style>
	</HEAD>
	<BODY class="repBody">';
	$v_xsl .= '<xsl:if test="$cond='."'T'".'">';
	$v_xsl .= '<table width="100%" >
	<tr class="reportGreyRow">
	<td class="reportPanel"> 
	Download the report in: 
	<A href="{$wd}" >
	Word Format
	</A> 
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<A href="{$xd}" >
	Excel Format
	</A>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<xsl:text>&#xa0;</xsl:text>
	<A href="{$hd}" >
	Printer Friendly Format
	</A> 
	</td>
	</tr>
	</table>
	</xsl:if>
	<xsl:apply-templates select="ROWSET"/>
	</BODY>
	</HTML>
	</xsl:template> 

	<xsl:template match="ROWSET">
	<TABLE WIDTH="100%" >
	<xsl:if test="$hdrflag='."'1'".'">
	<TR>
	<TD WIDTH="100%" ALIGN="CENTER">
	<img src="{$hdrFileName}"/>
	</TD>
	</TR>
	</xsl:if>
	<TR>
	<TD class="reportName" WIDTH="100%" ALIGN="CENTER">
	<xsl:value-of select="$repName" />
	</TD>
	</TR>
	</TABLE>
	<TABLE>
	<TR>
	<TD class="reportData" WIDTH="100%" ALIGN="LEFT">
	Selected Date Range Filter: <xsl:value-of select="$argsStr" />
	</TD>
	</TR>
	</TABLE>';
	
	$v_xsl .= '<table WIDTH="100%" BORDER="0"><tr>'.chr(13);

	if ($v_summarize_count > 0) $table_width = (100 / $v_summarize_count);

	for($i=0;$i<count($v_field_name);$i++){
		if (isset($v_summarize[$i])){
			$v_xsl .= '<td  valign="TOP" width="'.$table_width.'%">'.chr(13);
			$v_xsl .= '<table border="1" width="100%">'.chr(13);
			$v_xsl .= '<tr><td class="reportFooter" ALIGN="CENTER" colspan = "3"> <b>By '.$v_field_dispname[$i].'</b></td></tr>'.chr(13);
			$v_xsl .= '<tr>'.chr(13);
			$v_xsl .= '<td class="reportFooter" ALIGN="LEFT">'.chr(13);
			$v_xsl .= '<table width="100%" border="0">'.chr(13);
			$v_xsl .= '<xsl:for-each select="ROW[count(. | key('."'".'RecordsBy'.$v_field_name[$i]."'".', '.$v_field_name[$i].')[1])=1]">'.chr(13);
			$v_xsl .= '<xsl:variable name="str" select="key('."'".'RecordsBy'.$v_field_name[$i]."'".', '.$v_field_name[$i].')" />'.chr(13);
			$v_xsl .= '<xsl:variable name="v_'.$v_field_name[$i].'"><xsl:value-of select="'.$v_field_name[$i].'" /></xsl:variable>'.chr(13);
			$v_xsl .= '<xsl:if test="$v_'.$v_field_name[$i].' != '." ''".'">'.chr(13);
			$v_xsl .= '<tr>'.chr(13);
			$v_xsl .= '<xsl:variable name="'.$v_field_name[$i].'_count" select="format-number(((count($str['.$v_field_name[$i].'=$v_'.$v_field_name[$i].']) div count(//ROW['.$v_field_name[$i]."!='']))*100),"."'###.00'".')"/>'.chr(13);
			$v_xsl .= '<td width = "45%" class="reportFooter" ALIGN="RIGHT"><xsl:value-of select="$v_'.$v_field_name[$i].'" />&#xa0;:</td>'.chr(13);
			$v_xsl .= '<td class="reportFooter" ALIGN="LEFT" width="5%"><xsl:value-of select="count($str['.$v_field_name[$i].'=$v_'.$v_field_name[$i].'])" /></td>'.chr(13);
			$v_xsl .= '<td class="reportFooter" ALIGN="LEFT" width="45%">'.chr(13);
			$v_xsl .= '<table width="{$'.$v_field_name[$i].'_count}%" border="0">'.chr(13);
			$v_xsl .= '<tr BGCOLOR="green">'.chr(13);
			$v_xsl .= '<td >'.chr(13);
			$v_xsl .= '<xsl:text>&#xa0; </xsl:text>'.chr(13);
			$v_xsl .= '</td> '.chr(13);
			$v_xsl .= '</tr> '.chr(13);
			$v_xsl .= '</table>'.chr(13); 
			$v_xsl .= '<td class="reportFooter" ALIGN="RIGHT" width="5%">(<xsl:value-of select="$'.$v_field_name[$i].'_count" />%)</td>'.chr(13);
			$v_xsl .= '</td>'.chr(13);
			$v_xsl .= '</tr>'.chr(13);
			$v_xsl .= '</xsl:if>'.chr(13);
			$v_xsl .= '</xsl:for-each>'.chr(13);
			$v_xsl .= '</table>'.chr(13);
			$v_xsl .= '</td>'.chr(13);
			$v_xsl .= '</tr>'.chr(13);
			$v_xsl .= '</table>'.chr(13);
			$v_xsl .= '</td>'.chr(13);

		}
	}

	$v_xsl .= '</tr>'.chr(13);
	$v_xsl .= '<tr>'.chr(13);
	$v_xsl .= '<td class="reportLabel" ALIGN="CENTER" colspan="4"><b>Total Matching Rows: <xsl:value-of select="count(//ROW)" /></b></td>'.chr(13);
	$v_xsl .= '</tr>'.chr(13);
	$v_xsl .= '</table>'.chr(13);

	$v_xsl .= '<hr class="thickLine"/>
	<TABLE WIDTH="100%" BORDER="1">
	<THEAD><TR>
	';

	for($i=0;$i<count($v_field_dispname);$i++){
		if (!isset($v_exclude[$i])){
			$field_name = $v_field_dispname[$i];
			$v_xsl .= '<TH class="reportHeading" ALIGN="CENTER" WIDTH="'.$v_field_width[$i].'%">'.$field_name.'</TH>'.chr(13);
		}
	}

	$v_xsl .= '</TR></THEAD>';

	if (empty($v_group)){
		$v_xsl .= '<xsl:apply-templates select="ROW"/>
		</TABLE>

		<hr class="thickLine" />
		<TABLE WIDTH="100%">
		<TR>
		<TD class="reportFooter" WIDTH="50%" ALIGN="LEFT">

		Report By:<xsl:value-of select="$repBy" />

		</TD>
		<TD class="reportFooter" WIDTH="50%" ALIGN="RIGHT">

		Date:<xsl:value-of select="$repDate" />

		</TD>
		</TR>
		</TABLE>
		<xsl:if test="$ftrflag='."'1'".'">
		<TABLE>
		<TR>
		<TD WIDHT="100%" ALIGN="CENTER">
		<img src="{$ftrFileName}"/>
		</TD>
		</TR>
		</TABLE>
		</xsl:if>
		</xsl:template> 

		<xsl:template match="ROW">

		<xsl:variable name="class">
		<xsl:choose>
		<xsl:when test="number(position() mod 2)=0" >reportEvenRow</xsl:when> 
		<xsl:otherwise>reportOddRow</xsl:otherwise>
		</xsl:choose> 
		</xsl:variable>
		<TBODY><TR> <xsl:attribute name="class"><xsl:value-of select="$class"/></xsl:attribute>

		';

		for($i=0;$i<count($v_field_name);$i++){
			if (!isset($v_exclude[$i])){
				$field_name = $v_field_name[$i];
				$v_xsl .= '<TD class="reportData" WIDTH="10%" ><xsl:value-of select="'.$field_name.'" /></TD>'.chr(13);
			}
		}

		$v_xsl .= '</TR></TBODY>';

		if ($v_total_count > 0){
			$v_xsl .= '<xsl:if test="position()=last()">
			<TR bgcolor="#CCCCCC">';
			for($i=0;$i<count($v_field_name);$i++){
				if (!isset($v_exclude[$i])){
					if (isset($v_total[$i])){
						$field_name = $v_field_name[$i];
						$v_xsl .= '<TD class="reportGrouping" WIDTH="10%" ><B><xsl:value-of select="sum(//'.$field_name.')" /></B></TD>'.chr(13);
					} else {
						if ($i == 0) {
							$v_xsl .= '<TD class="reportGrouping" WIDTH="10%" ><B>Sub Total</B></TD>'.chr(13);
						} else {
							$v_xsl .= '<TD class="reportGrouping" WIDTH="10%" >&#xa0;</TD>'.chr(13);
						}

					}
				}
			}
			$v_xsl .= '</TR>
			</xsl:if>';
		}

		$v_xsl .= '</xsl:template> 
		</xsl:stylesheet>';
	} else {
		// processing for grouped rows

		$v_xsl .= '<xsl:for-each select="ROW[count(. | key('."'groupBy'".', '.$v_grouping.')[1])=1]">'.chr(13);

		if (!empty($v_grouping)){
			$v_xsl .= '<TR><TD class="reportGrouping" colspan="'.count($v_field_name).'">'.chr(13);
			$v_xsl .= '<TABLE BORDER="0" width="100%"><TR>'.chr(13);
			if (!empty($v_grouping1)) {
				$v_xsl .= '<TD class="reportGrouping" width="33%">';
				
				for($i=0;$i<count($v_field_name);$i++){
					if ($v_field_name[$i] == $v_grouping1) {
						$v_xsl .= $v_field_dispname[$i];
						break;
					}
				}
				
				$v_xsl .= ': <xsl:value-of select="'.$v_grouping1.'" /></TD>'.chr(13);
			}
		
			if (!empty($v_grouping2)) {
				$v_xsl .= '<TD class="reportGrouping" width="33%">'.$v_grouping2.': <xsl:value-of select="'.$v_grouping2.'" /></TD>'.chr(13);
			}

			if (!empty($v_grouping3)) {
				$v_xsl .= '<TD class="reportGrouping" width="33%">'.$v_grouping3.': <xsl:value-of select="'.$v_grouping3.'" /></TD>'.chr(13);
			}
			$v_xsl .= '</TR></TABLE>'.chr(13);
			$v_xsl .= '</TD></TR>'.chr(13);
		}
		
		$v_xsl .= '<xsl:for-each select="key('."'groupBy'".', '.$v_grouping.')">

		<xsl:variable name="class">
		<xsl:choose>
		<xsl:when test="number(position() mod 2)=0" >reportEvenRow</xsl:when> 
		<xsl:otherwise>reportOddRow</xsl:otherwise>
		</xsl:choose> 
		</xsl:variable>
		<TR> <xsl:attribute name="class"><xsl:value-of select="$class"/></xsl:attribute>

		';

		for($i=0;$i<count($v_field_name);$i++){
			if (!isset($v_exclude[$i])){
				$field_name = $v_field_name[$i];
				$v_xsl .= '<TD class="reportData" WIDTH="10%" ><xsl:value-of select="'.$field_name.'" /></TD>'.chr(13);
			}
		}

		$v_xsl .= '</TR>'.chr(13);

		if ($v_total_count > 0){
			$v_xsl .= '<xsl:if test="position()=last()">
			<TR bgcolor="#CCCCCC">';
			for($i=0;$i<count($v_field_name);$i++){
				if (!isset($v_exclude[$i])){
					if (isset($v_total[$i])){
						$field_name = $v_field_name[$i];
						$v_xsl .= '<TD class="reportGrouping" WIDTH="10%" ><B><xsl:value-of select="sum(key('."'groupBy'".', '.$v_grouping.')/'.$field_name.')" /></B></TD>'.chr(13);
					} else {
						if ($i == 0) {
							$v_xsl .= '<TD class="reportGrouping" WIDTH="10%" ><B>Sub Total</B></TD>'.chr(13);
						} else {
							$v_xsl .= '<TD class="reportGrouping" WIDTH="10%" >&#xa0;</TD>'.chr(13);
						}
					}
				}
			}
			$v_xsl .= '</TR>
			</xsl:if>';
		}


		$v_xsl .= '</xsl:for-each></xsl:for-each>'.chr(13);
		
		if ($v_total_count > 0){
			$v_xsl .= '<TR bgcolor="#CCCCCC">'.chr(13);
			for($i=0;$i<count($v_field_name);$i++){
				if (!isset($v_exclude[$i])){
					if (isset($v_total[$i])){
						$field_name = $v_field_name[$i];
						$v_xsl .= '<TD class="reportGrouping" WIDTH="10%" ><B><xsl:value-of select="sum(//'.$field_name.')" /></B></TD>'.chr(13);
					} else {
						if ($i == 0) {
							$v_xsl .= '<TD class="reportGrouping" WIDTH="10%" ><B>Grand Total</B></TD>'.chr(13);
						} else {
							$v_xsl .= '<TD class="reportGrouping" WIDTH="10%" >&#xa0;</TD>'.chr(13);
						}
					}
				}
			}
			$v_xsl .= '</TR>'.chr(13);
		}
		
		$v_xsl .= '</TABLE>
				   </xsl:template> 
				   </xsl:stylesheet>';

	}

	$doc = new domDocument;
	$doc->preserveWhiteSpace = false;
	$doc->encoding = 'utf-8';
	$doc->loadXML($v_xsl);		
	$doc->formatOutput = true;
	$v_xsl = $doc->saveXML();
	
//	$recordSet = $db->Execute("select velink.seq_er_report.nextval from dual");
//	$v_pk =  $recordSet->fields[0];

	$v_pk_q = $db->Execute("select MAX(pk_report)+1 as pkRe from er_report");
	$v_pk = $v_pk_q->fields[0];
	$v_status = $db->Execute("insert into er_report (pk_report, rep_name, rep_desc,fk_account,rep_hide,generate_xml,rep_type,rep_filterapplicable) values ($v_pk,'$v_reportname','$v_reportdesc',0,'N',1,'rep_cstm','date')");
	if (!$v_status) print $db->ErrorMsg();
	$v_status = $db->UpdateClob('er_report','rep_sql_clob',$v_query,'pk_report='.$v_pk); 
	if (!$v_status) print $db->ErrorMsg();
	$v_status = $db->Execute("insert into er_repxsl (pk_repxsl,fk_account,fk_report,repxsl_name) values ($v_pk,0,$v_pk,'$v_reportname')");
	if (!$v_status) print $db->ErrorMsg();
	$v_status = $db->UpdateClob('er_repxsl','repxsl_xsl',$v_xsl,'pk_repxsl='.$v_pk); 
	if (!$v_status) print $db->ErrorMsg();
	
	$recordSet = $db->Execute("select velink.seq_vlnk_queryman_rep.nextval from dual");
	$v_pk_qrep =  $recordSet->fields[0];
	$v_status = $db->Execute("insert into velink.vlnk_queryman_rep (pk_queryman_rep, fk_queryman, fk_report,fk_repxsl) values ($v_pk_qrep,$v_pk_queryman,$v_pk,$v_pk)");
	if (!$v_status) print $db->ErrorMsg();
	$v_status = $db->UpdateClob('velink.vlnk_queryman_rep','query_sql',$v_query,'pk_queryman_rep='.$v_pk_qrep); 
	if (!$v_status) print $db->ErrorMsg();
	$v_status = $db->UpdateClob('velink.vlnk_queryman_rep','query_xsl',$v_xsl,'pk_queryman_rep='.$v_pk_qrep); 
	if (!$v_status) print $db->ErrorMsg();
	$v_status = $db->UpdateClob('velink.vlnk_queryman_rep','modified_xsl',$v_xsl,'pk_queryman_rep='.$v_pk_qrep); 
	if (!$v_status) print $db->ErrorMsg();

	

	echo "Report Published...";

	$url = "./query_report_manager.php";
	echo "<meta http-equiv=\"refresh\" content=\"3; url=./".$url."\">";
}


?>

</div>


</body>
</html>
<?php 
$db->Close();

}
else header("location: index.php?fail=1");
?>
