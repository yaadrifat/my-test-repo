<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html>
<head><title>Velos Link</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" type="text/css" media="print" href="css/print.css">
		<style type="text/css" media="screen">
			@import url("css/layout.css");
			@import url("css/content.css");
			@import url("css/docbook.css");
		</style>
		<meta name="MSSmartTagsPreventParsing" content="TRUE">
</head>


	<body>
		<!-- header BEGIN -->
		<div id="fedora-header">

     <table width="100%" border="0">
            <tr align="center"><td class="headertext"><BR><BR>You are connected to: <font size=4 color=green>
         <?PHP if (isset($_SESSION["DATASOURCE"])) echo $_SESSION["DATASOURCE"]." Database"; ?>
         </font> <font size=4 color=red><?PHP if (isset($_SESSION["VERSION"])) echo '{'.$_SESSION["VERSION"].'}'; ?>
         </font> </td></tr>
     </table>

<?PHP
include ("./txt-db-api.php");
	if (isset($_SESSION["MODULE"])) {
		$module = $_SESSION["MODULE"];
	} else {
		$module = "";
	}
?>

	
			<div id="fedora-header-logo">
				<a href="http://www.veloseresearch.com" id="link-internal"><img src="img/velos_etools.png" alt="Velink Project"></a>
			</div>
		</div>

		<div id="fedora-nav"></div>
		<!-- header END -->
		
		<!-- leftside BEGIN -->
		<div id="fedora-side-left">
		<div id="fedora-side-nav-label">Site Navigation:</div>	<ul id="fedora-side-nav">
<?PHP

			$db = new Database("etools");
			$result = $db->executeQuery("SELECT APP_RIGHTS from ET_GROUPS where PK_GROUPS=".$_SESSION['FK_GROUPS']);
			while ($result->next()) {
				$data = $result->getCurrentValuesAsHash();
				$v_app_rights = $data["APP_RIGHTS"];
			}
			$result = $db->executeQuery("SELECT pk_menu,menu_name,level,disp_seq,visible,page,module from ET_MENUS order by disp_seq");
			if ($result->getRowCount() > 0) {
			$v_level1 = false;
			$v_level2 = false;
			while ($result->next()) {
				$data = $result->getCurrentValuesAsHash();
				if ($data["level"] == 0){
					$pos = strpos("PAD".$v_app_rights,"|".$data["pk_menu"].":1");
					if (empty($pos)) $pos = -1;
					if (($data["visible"] == 1) && ($pos >= 0)){
						if ($v_level1) {
							echo "</li>";
						}
						if ($v_level2) {
							echo "</ul>";
							$v_level2 = false;
						}
						$v_level1 = true;
						echo '<li><a href="menus.php?page='.$data["page"].'&module='.$data["module"].'" id="link-internal">'.$data["menu_name"].'</a>';
					}
				} else {
					$pos = strpos($v_app_rights,'|'.$data["pk_menu"].":1");
					if (empty($pos)) $pos = -1;
					if (($data["visible"] == 1) && ($pos >= 0)){
						if ($data["module"] == $module) {
							$v_level2 = true;
							if ($v_level1) {
								echo '<ul>';
							}
							$v_level1 = false;
							echo '<li><strong><a href="menus.php?page='.$data["page"].'&module='.$data["module"].'" id="link-internal">'.$data["menu_name"].'</a></strong></li>';
						}
					}
				}
			}
			}
?>

		</div>
		<!-- leftside END -->

		<!-- content BEGIN -->
		<div id="fedora-middle-two">
		<!-- footer END -->
</body>
</html>
