<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.
//session_cache_limiter ('no_cache');
// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Code List</title>

</head>
<?php
include("./includes/oci_functions.php");
include ("./txt-db-api.php");
//include ("./querystring_inc.php");
if (!isset($_POST['DS'])) {
$ds = $_SESSION["DS"];
} else {
$ds = $_POST['DS'];
}
	
$db = new Database("etools");
$rs = $db->executeQuery("SELECT PK_DS,DS_NAME,DS_HOST,DS_PORT,DS_SID,DS_PASS from ET_DS where PK_DS=".$ds);
//$rs = $db->executeQuery($log_user_1.$ds);
while ($rs->next()) {
	$ds_data = $rs->getCurrentValuesAsHash();
	
	$_SESSION["DS_HOST"] = $ds_data["DS_HOST"];
	$_SESSION["DS_PORT"] = $ds_data["DS_PORT"];
	$_SESSION["DS_SID"] = $ds_data["DS_SID"];
	$_SESSION["DS_PASS"] = $ds_data["DS_PASS"];
	$_SESSION["DS"] = $ds_data["PK_DS"];
//	$_SESSION["URL"] = $ds_data["URL"];
	$db  = "(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)";
	$db .= "(HOST = ".$_SESSION["DS_HOST"].")(PORT = ".$_SESSION["DS_PORT"].")) )";
	$db .= "(CONNECT_DATA = (SERVICE_NAME = ".$_SESSION["DS_SID"].")  ) )";
	$_SESSION["DB"] = $db;
	$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $db); 
	if ($ds_conn != '') {
/*		$db  = "(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)";
		$db .= "(HOST = ".$_SESSION["DS_HOST"].")(PORT = ".$_SESSION["DS_PORT"].")) )";
		$db .= "(CONNECT_DATA = (SID = ".$_SESSION["DS_SID"].")  ) )";
		$_SESSION["DB"] = $db;
		$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $db);*/
	
		$_SESSION["DATASOURCE"] = $ds_data["DS_NAME"];
		$query_sql = "select ctrl_value from er_ctrltab where ctrl_key = 'app_version'";
		$results = executeOCIQuery($query_sql,$ds_conn);
	//	$results = executeOCIQuery($log_user_2."'app_version'",$ds_conn);
		$_SESSION["VERSION"] = $results["CTRL_VALUE"][0];
		$_SESSION["MODULE"] = "dbm";
	}
}

include("./includes/header.php");

?>
<body>
<FORM action="codelist.php" method=post>
<!-- <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	

<?PHP

if ($ds_conn == ''){
	unset($_SESSION["DATASOURCE"]);
	unset($_SESSION["VERSION"]);
	unset($_SESSION["MODULE"]);
	echo '<script> alert("Datasource failed to connect"); </script>';
	echo '<meta http-equiv="refresh" content="0; url=./datasource.php">';		
}

$query="SELECT usr_firstname || ' ' || usr_lastname AS username , usr_logname,grp_name,  add_email, add_phone, to_char(ER_USER.last_modified_date,'dd-Mon-yy hh:mi:ss') AS logged_on, (select site_name from er_site where pk_site = fk_siteid) as site_name FROM ER_USER,ER_ADD,ER_GRPS WHERE pk_add = fk_peradd AND pk_grp =   fk_grp_default AND usr_loggedinflag = 1 and trunc(er_user.last_modified_date) = trunc(sysdate)";
$results = executeOCIQuery($query,$ds_conn);
//$results = executeOCIQuery($log_displayusr,$ds_conn);

echo "<B>Logged in Users: ".$results_nrows."</B>";

?>

<Table border="1"><TR>
<TH width="10%">User Name</TH>
<TH width="15%">Organization</TH>
<TH width="10%">Login Name</TH>
<TH width="10%">Group</TH>
<TH width="20%">E-mail</TH>
<TH width="10%">Phone</TH>
<TH width="15%">Login Time</TH>
</TR>
<?php


for ($rec = 0; $rec < $results_nrows; $rec++){
?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';">
	<TD><?php echo str_replace(array("<script>","</script>"),array("",""),$results["USERNAME"][$rec]). "&nbsp;"; ?></TD>
	<TD><?php echo $results["SITE_NAME"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo str_replace(array("<script>","</script>"),array("",""),$results["USR_LOGNAME"][$rec]). "&nbsp;"; ?></TD>
	<TD><?php echo $results["GRP_NAME"][$rec] . " "; ?></TD>
	<TD><?php echo $results["ADD_EMAIL"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["ADD_PHONE"][$rec] . "&nbsp;"; ?></TD>
	<TD><?php echo $results["LOGGED_ON"][$rec] . "&nbsp;"; ?></TD>
</TR>
<?php
}
//}
?>
</TABLE>
</div>


</body>
</html>
<?php
OCICommit($ds_conn);
OCILogoff($ds_conn);

}
else header("location: index.php?fail=1");
?>
