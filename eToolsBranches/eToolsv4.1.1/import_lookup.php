<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velos eTools -> Import Lookup from File</title>

<SCRIPT language="Javascript1.2">
function validate(form,lkpnames){

	if (form.lookupname.value == ""){
		alert("Lookup Name cannot be blank.");
		form.lookupname.focus();
		return false;
	}

	if (form.uploadedfile.value == ""){
		alert("Choose a file to upload.");
		form.uploadedfile.focus();
		return false;
	}

	if (form.delimiter.value == ""){
		alert("Delimiter cannot be blank.");
		form.delimiter.focus();
		return false;
	}
	
	var v_lkpnames = new Array();
	v_lkpnames = lkpnames.split('|');
	for (var i=0;i<v_lkpnames.length;i++){
		if (v_lkpnames[i] == form.lookupname.value){
			alert("Lookup Name already exists.");
			return false;
		}
	}
	return true;
}
</SCRIPT>
	

</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<?php 

if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 

$query_sql = "select distinct lkptype_name from er_lkplib,er_lkpcol where pk_lkplib = fk_lkplib and lkpcol_table = 'ER_LKPDATA' and nvl(lkptype_type,'Y') not in ('dyn_p','dyn_s','dyn_a')";
$results = executeOCIQuery($query_sql,$ds_conn);
$v_lkpnames = "";
for ($rec = 0; $rec < $results_nrows; $rec++){
	$v_lkpnames .= $results["LKPTYPE_NAME"][$rec].'|';
}
?>

<div class="navigate">Import Static Lookup from File </div>
<form name="lookup" enctype="multipart/form-data" action="import_lookup.php" method="POST" onSubmit="if (validate(document.lookup,'<?PHP echo $v_lkpnames; ?>') == false) return false;">
<br>
<table border="0">
<tr><td>Lookup Name:</td><td><input class="required" type="text" name="lookupname" id="lookupname" maxlength="20"/></td></tr>
<tr><td>Delimited File name:</td><td><input class="required" type="file" name="uploadedfile" size="100"/></td></tr>
<tr><td>Delimiter:</td><td><input class="required" type="text" name="delimiter" size="2"/></td></tr>
<tr><td>Version:</td><td><input type="text" name="lookupVersion" maxlength="10"/></td></tr>
<tr><td>Description:</td><td><input type="text" name="lookupDesc" size="100" maxlength="250"/></td></tr>
<tr><td>Type:</td><td><input type="text" name="lookupType" maxlength="20"/></td></tr>

</table>
<BR><input type="image" name="submit" value="Submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
</form>
<div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><table border="0" summary="Note: Note">
<tr>
<th align="left">Note</th>
</tr>
<tr><td align="left" valign="top">
<p>Supported File Types: Any delimited file (delimiters such as comma, semicolon, pipe etc. are commonly used)</p>
<p>First row of the file should contain Column Name, no special characters other than '_' (underscore), should be one continuous string of characters (no space) and delimited with same delimiter as the actual data rows.</p>
</td></tr>
</table></div>

<?php } else {

  $delimiter = $_POST["delimiter"];
  $lookupName = $_POST["lookupname"];
  $lookupVersion = $_POST["lookupVersion"];
  $lookupDesc = $_POST["lookupDesc"];
  $lookupType = $_POST["lookupType"];
  $tablename = "";

  $curDir = getcwd();
  $target_path = $curDir."/upload/";

  $target_path = $target_path.basename( $_FILES['uploadedfile']['name']);

  if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
//      echo "The file ".  basename( $_FILES['uploadedfile']['name'])." has been uploaded"."<BR>";

?>
<div class="navigate">Import Static Lookup from File </div>
        <form action="import_lookup_data.php" method="POST">
<?php          echo '<input type="hidden" name="delimiter" value="'.$delimiter.'"/>';
          echo '<input type="hidden" name="lookupName" value="'.$lookupName.'"/>';
          echo '<input type="hidden" name="lookupVersion" value="'.$lookupVersion.'"/>';
          echo '<input type="hidden" name="lookupDesc" value="'.$lookupDesc.'"/>';
          echo '<input type="hidden" name="lookupType" value="'.$lookupType.'"/>';
          echo '<input type="hidden" name="fileDir" value="'.$target_path.'"/>';
?>
          <table width="100%" border="1">
          <tr>
          <th width="20%">Column Name</th>
          <th width="20%">Display Name</th>
          <th width="20%">Keyword</th>
          <th width="10%">Sequence</th>
          <th width="10%">Show</th>
          <th width="10%">Searchable</th>
          <th width="10%">Width(%)</th>
          </tr>
<?php          
			$handle = fopen($target_path, "r");
          $v_sql="";
          $dd_yn = '<option value="Y" selected>Yes</option><option value="N">No</option>';
          while (($data = fgetcsv($handle, 10000, $delimiter)) !== FALSE) {
             $num = count($data);
             $width = round(100 / $num);
             for ($c=0; $c < $num; $c++) {
                echo '<TR onMouseOver="bgColor=\'#a4bef1\';" onMouseOut="this.bgColor=\'#FFFFFF\';">';
          			echo '<td><input style="background-color:#d1cebf" readonly size="30" type="text" name="colName['.$c.']" value="'.$data[$c].'"/></td>';
          			echo '<td><input size="30" type="text" name="dispName['.$c.']" value="'.$data[$c].'"/></td>';
          			echo '<td><input size="30" type="text" name="keyword['.$c.']" value="'.$data[$c].'"/></td>';
          			echo '<td align="center"><input size="5" type="text" name="colSeq['.$c.']" value="'.($c + 1).'"/></td>';
          			echo '<td align="center"><select name="colShow['.$c.']">'.$dd_yn.'</select></td>';
          			echo '<td align="center"><select name="colSearch['.$c.']">'.$dd_yn.'</select></td>';
          			echo '<td><input size="5" type="text" name="colWidth['.$c.']" value="'.$width.'"/></td>';
                echo '</tr>';

			echo '<input type="hidden" name="colType['.$c.']" value="varchar2"/>';


             }
             break;
          }

          echo "</table>";
	?>
	<input type="image" name="submit" value="Submit" src="./img/submit.png"  align="absmiddle" border="0" onMouseOver="this.src='./img/submit_m.png';" onMouseOut="this.src='./img/submit.png';" />
	<?PHP

          echo "</form>";

        fclose($handle);



  } else{
      echo "There was an error uploading the file, please try again!"."<BR>";
  }

}

?>

	
</div>


</body>
<?php if($num == '') { ?>
	<script type="text/javascript">
	 document.getElementById("lookupname").focus();
	</script>
<?php } ?>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
