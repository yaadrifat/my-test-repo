<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Form Data</title>
</head>
<?php
include("./includes/header.php");

include_once "./adodb/adodb.inc.php";
include_once('./adodb/adodb-pager.inc.php'); 

$db = NewADOConnection("oci8");
$connect_string = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST='.$_SESSION["DS_HOST"].')(PORT='.$_SESSION["DS_PORT"].'))(CONNECT_DATA=(SID='.$_SESSION["DS_SID"].')))';
$db->Connect($connect_string, "eres", $_SESSION["DS_PASS"]);


if (!$db) die("Connection failed");

	
?>
<body>
<div id="fedora-content">	

<?PHP 
if (isset($_GET["pk_vlnk_adapmod"])) {
	$v_form_linkto = $_GET["form_linkto"];
	$v_pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];
	$v_exportFlag = (isset($_GET["flag"])?$_GET["flag"]:"");
	$v_form = $_GET["fk_form"];
	switch ($v_form_linkto) {
	case "A":
		$v_table = "ER_IMPACCTFORM";
		$v_tablepk = "PK_IMPACCTFORM";
		$v_sql = "fk_account, ";
		break;
	case "P":
		$v_table = "ER_IMPPATFORM";
		$v_tablepk = "PK_IMPPATFORM";
		$v_sql = "fk_per, ";
		break;
	case "S":
		$v_table = "ER_IMPSTUDYFORM";
		$v_tablepk = "PK_IMPSTUDYFORM";
		$v_sql = "fk_study, ";
		break;
	}
	$v_query = "select mp_sequence,form_colname,mp_dispname from er_mapform, velink.vlnk_imp_formfld where fk_field = mp_pkfld and table_colname is not null and fk_vlnk_adapmod = ".$v_pk_vlnk_adapmod;
	$rs = $db->Execute($v_query);
//	$v_sql = "";
	while (!$rs->EOF) {
		$v_sql .= $rs->fields["FORM_COLNAME"].' as "'.$rs->fields["MP_SEQUENCE"]."_".substr($rs->fields["MP_DISPNAME"],0,25).(strlen($rs->fields["MP_DISPNAME"]) > 25?"..":"").'",';
		$rs->MoveNext();
	}
	$v_sql = substr($v_sql,0,strlen($v_sql)-1);
	
	if ($v_exportFlag == -1) {
		$v_sql = "select (SELECT message from velink.vlnk_log b where status = -2 and fk_vlnk_adapmod = $v_pk_vlnk_adapmod and to_number(b.rid) = $v_tablepk and pk_vlnk_log = (select max(pk_vlnk_log) from velink.vlnk_log b where status = -2 and fk_vlnk_adapmod = $v_pk_vlnk_adapmod and to_number(b.rid) = $v_tablepk)) as error_message,".$v_sql." from ".$v_table." a where fk_form = $v_form and custom_col = ".$v_pk_vlnk_adapmod;
	} else {
		$v_sql = "select ".$v_sql." from ".$v_table." where fk_form = $v_form and custom_col = ".$v_pk_vlnk_adapmod;
	}
	
	if (strlen($v_exportFlag) > 0) {
		$v_sql .= " and export_flag = '$v_exportFlag'";
	}

	$_SESSION["sql"] = $v_sql;
} else {
	$v_sql = $_SESSION["sql"] ;
}
?>
<style>DIV.tablerows {
height:500px;
width:100%;
overflow: auto;
align:left;
}</style>

<div>
<?PHP
$rs = $db->Execute($v_sql);
if (!$rs) {
	echo "SQL ERROR: ";
	print $db->ErrorMsg();
} else {
$pager = new ADODB_Pager($db,$v_sql);
$pager->htmlSpecialChars = false;
echo "<table width='100%'><tr><td width='50%'><b>Total Rows: ".$rs->RecordCount()."</b></td>";
echo '<td width="50%"><a href="export2csv.php?data='.urlencode($v_sql).'">Export to CSV</a></td></tr></table>';
echo "<table><tr><td>";
echo "<div class='tablerows'>";
$pager->Render($rows_per_page=25); 
echo "</div>";
echo "</td></tr></table>";
}

	
?>



</div>


</body>
</html>
<?php 
$db->Close();

}
else header("location: index.php?fail=1");
?>
