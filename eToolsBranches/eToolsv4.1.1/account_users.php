<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Users</title>

<?php
include("./includes/oci_functions.php");

include("./includes/header.php");
include ("./txt-db-api.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>

<body>

<div id="fedora-content">	
<div class="navigate">Manage Account - Users</div>
<?PHP
echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=account_users_edit.php?mode=n&pk_users=0>New User</a>";

$db = new Database("etools");
$rs = $db->executeQuery("SELECT PK_USERS,FIRST_NAME,LAST_NAME,EMAIL,PHONE,GROUP_NAME FROM ET_USERS,ET_GROUPS WHERE ET_GROUPS.PK_GROUPS = ET_USERS.FK_GROUPS order by GROUP_NAME,FIRST_NAME,LAST_NAME");
//$rs = $db->executeQuery($ac_usr);
?>
<Table border="1" width="100%"><TR>
<TH>GROUP</TH>
<TH>NAME</TH>
<TH>EMAIL</TH>
<TH>PHONE</TH>
<TH>&nbsp;</TH>
</TR>
<?php


$v_counter = 0;
while ($rs->next()) {
	$data = $rs->getCurrentValuesAsHash();
	//print "<pre>"; print_r($data);
?>
	<TR onMouseOver="bgColor='#a4bef1';" onMouseOut="this.bgColor='#FFFFFF';" >
	<TD><?php echo $data["GROUP_NAME"] ?></TD>
	<TD><?php echo htmlentities($data["FIRST_NAME"]).' '. $data["LAST_NAME"]; ?></TD>
	<TD><?php echo $data["EMAIL"] ?></TD>
	<TD><?php echo $data["PHONE"] ?></TD>
	<TD>
		<?PHP
			if ($data["PK_USERS"] != 1) echo '<a href=account_users_edit.php?mode=m&pk_users='.$data["PK_USERS"].'>Edit</a>';
		?>
	</TD>
<?php
	$v_counter++;
}
?>
</TABLE>
      </div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
