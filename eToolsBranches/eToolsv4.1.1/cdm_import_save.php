<?php
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>
<html>
<head>    <title>Velos eTools -> Charge Description Master Import</title>

<script>
	function refresh_totals(total_rows,total_rowsi,total_rowsu){
		document.getElementById('total_rows').innerHTML = total_rows;
		document.getElementById('total_rowsi').innerHTML = total_rowsi;
		document.getElementById('total_rowsu').innerHTML = total_rowsu;
	}
</script>
</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");
$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]);
?>
	<body>
	<div id="fedora-content">	
	<div class="navigate">Charge Description Master Import</div>
	<br>
<?PHP

$v_libtype = $_POST["libtype"];

$v_category = $_POST["category"];
$v_event = $_POST["event"];
$v_cpt = $_POST["cpt"];
$v_desc = $_POST["desc"];
$v_notes = $_POST["notes"];
$v_budgetcat = $_POST["budgetcat"];
$v_addcodelst = $_POST["addcodelst"];

//print "<pre>"; print_r($v_addcodelst); print "</pre>"; die();

$v_addcodelst_pk = $_POST["addcodelst_pk"];
$v_site_of_service = $_POST["site_of_service"];
$v_facility = $_POST["facility"];
$v_coverage_type = $_POST["coverage_type"];
$v_coverage_type_notes = $_POST["coverage_type_notes"];
$v_filepath = $_POST["filepath"];
$v_delimiter = $_POST["delimiter"];
$v_cost = $_POST["cost"];
$v_cost_pk = $_POST["cost_pk"];
$v_currency = $_POST["currency"];
$v_user = $_POST["user"];
$v_query = "SELECT fk_account FROM er_user WHERE pk_user = $v_user";
$results = executeOCIQuery($v_query,$ds_conn);
$v_account = $results["FK_ACCOUNT"][0];

$v_category = $_POST["category"];
$v_deli = $_POST["cost"];

//print "<pre>"; print_r($_POST); print "</pre>";

$handle = fopen($v_filepath, "r");
$row = 0;
$row_inserted = 0;
$row_updated = 0;
echo 'Total rows processed: <b id="total_rows"></b><BR>';
echo 'Total rows inserted: <b id="total_rowsi"></b><BR>';
echo 'Total rows updated: <b id="total_rowsu"></b><BR><BR>';
echo 'Processing Status: <font color="red"><b id="status">In progress...</b></font><BR>';
flush();

while (($data = fgetcsv($handle, 10000, $v_delimiter)) !== FALSE) {
	if (strlen($data[$v_category]) > 0 && strlen($data[$v_event]) > 0 && strlen($data[$v_libtype]) > 0) {
		if ($row > 0) {
		//print "<pre>"; print_r($data); print "</pre>"; exit;
			
			//write library type code here
			$v_libT = trim(substr(str_replace("'","''",$data[$v_libtype]),0,50));		
			$v_query = "SELECT PK_CODELST FROM SCH_CODELST WHERE upper(trim(CODELST_DESC)) = '".strtoupper($v_libT)."' AND CODELST_TYPE = 'lib_type'";
			$results = executeOCIQuery($v_query,$ds_conn);
			$isLibNew = False;
			$isCatNew = False;

			if($results_nrows == 0){
				$isLibNew = True;
				$v_query = "SELECT SCH_CODELST_SEQ1.NEXTVAL as pk_codelst FROM dual";
				$results = executeOCIQuery($v_query,$ds_conn);
				$v_pk_codelst = $results["PK_CODELST"][0];
				if(strlen($v_libT)>=5){
					$subtype = substr($v_libT,0,5);
				}else{
					$subtype = $v_libT;				
				}		
				$v_seq = "SELECT MAX(CODELST_SEQ)+1 as SEQVAL FROM SCH_CODELST WHERE CODELST_TYPE='lib_type'";
				$results5 = executeOCIQuery($v_seq,$ds_conn);
				$v_mseq = $results5["SEQVAL"][0];	
	
				$v_query = "INSERT INTO SCH_CODELST (PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ) VALUES ('".trim($v_pk_codelst)."','lib_type','".trim($subtype)."','".trim($v_libT)."','N','".trim($v_mseq)."')";
				$results = executeOCIUpdateQuery($v_query,$ds_conn);

				$v_currval = "SELECT SCH_CODELST_SEQ1.CURRVAL as pk_codelst FROM dual";
				$results3 = executeOCIQuery($v_currval,$ds_conn);
				$v_exist_pk_codelist = $results3["PK_CODELST"][0];	
			}else{
				$v_query= "SELECT PK_CODELST FROM SCH_CODELST WHERE CODELST_TYPE='lib_type' and upper(trim(CODELST_DESC)) = '".strtoupper($v_libT)."'";
				$results2 = executeOCIQuery($v_query,$ds_conn);
				$v_exist_pk_codelist = $results2["PK_CODELST"][0];
			}
			
			// Check if event category exists.
			$v_eventName = trim(substr(str_replace("'","''",$data[$v_category]),0,4000));			

			$v_query = "SELECT EVENT_ID, EVENT_LIBRARY_TYPE FROM EVENT_DEF WHERE upper(trim(NAME)) = '".strtoupper($v_eventName)."' AND event_type = 'L' and user_id=".$v_account;
			$results = executeOCIQuery($v_query,$ds_conn);
			
			/*--------- Writing Category  --------------- */
			if ($results_nrows == 0) {
				$isCatNew = True;
				$v_query = "SELECT event_definition_seq.NEXTVAL as category_pk FROM dual";
				$results = executeOCIQuery($v_query,$ds_conn);
				$v_category_pk = $results["CATEGORY_PK"][0];
				$v_query = "INSERT INTO EVENT_DEF (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,NOTES,DURATION,USER_ID,FUZZY_PERIOD,DESCRIPTION,DISPLACEMENT,
				ORG_ID,EVENT_FLAG,CALENDAR_SHAREDWITH,DURATION_UNIT,EVENT_FUZZYAFTER,EVENT_DURATIONAFTER,EVENT_CPTCODE,EVENT_DURATIONBEFORE,CREATOR,FK_CATLIB,EVENT_LIBRARY_TYPE,EVENT_LINE_CATEGORY)
				VALUES ('".trim($v_category_pk)."','".trim($v_category_pk)."','L','".trim($v_eventName)."','',0,$v_account,0,'".trim($v_eventName)."',0,0,0,'A','D',0,'D','','D',$v_user,0,'".trim($v_exist_pk_codelist)."','".trim($v_budgetcat)."')";
				$results = executeOCIUpdateQuery($v_query,$ds_conn);

			} else {
				$v_category_pk = $results["EVENT_ID"][0];
				$v_category_lib = $results["EVENT_LIBRARY_TYPE"][0];
			}
			
			if (($isCatNew) || (!$isCatNew && !$isLibNew && $v_exist_pk_codelist==$v_category_lib)){
				$v_eventName = trim(substr(str_replace("'","''",$data[$v_event]),0,4000));
				$v_query = "SELECT EVENT_ID,CHAIN_ID FROM EVENT_DEF a WHERE event_type = 'E' and upper(trim(NAME)) = '".strtoupper($v_eventName)."' AND exists (select * from EVENT_DEF x where x.event_type = 'L' and x.event_id = a.CHAIN_ID and user_id = $v_account)";
				$results = executeOCIQuery($v_query,$ds_conn);
				$v_eventchainid = $results["CHAIN_ID"][0];				

				/*--------- Writing Event  --------------- */
				if ($results_nrows == 0) {			
					$v_query = "SELECT event_definition_seq.NEXTVAL as event_pk FROM dual";
					$results = executeOCIQuery($v_query,$ds_conn);
					$v_event_pk = $results["EVENT_PK"][0];
					$data[$v_desc] = substr($data[$v_desc], 0, 200);
					$v_query = "INSERT INTO EVENT_DEF (EVENT_ID,CHAIN_ID,EVENT_TYPE,NAME,NOTES,DURATION,USER_ID,FUZZY_PERIOD,DESCRIPTION,DISPLACEMENT,
					ORG_ID,EVENT_FLAG,CALENDAR_SHAREDWITH,DURATION_UNIT,EVENT_FUZZYAFTER,EVENT_DURATIONAFTER,EVENT_CPTCODE,EVENT_DURATIONBEFORE,CREATOR,FK_CATLIB,EVENT_LIBRARY_TYPE,EVENT_LINE_CATEGORY)
					VALUES ($v_event_pk ,$v_category_pk,'E','".trim($v_eventName)."','".str_replace("'","''",trim((isset($data[$v_notes])?$data[$v_notes]:"" )))."',0,$v_account,0,'".str_replace("'","''",trim(isset($data[$v_desc])?$data[$v_desc]:"" ))."',0,0,0,'A','D',0,'D','".str_replace("'","''",trim(isset($data[$v_cpt])?$data[$v_cpt]:"" ))."','D',$v_user,0,'".trim($v_exist_pk_codelist)."','".trim($v_budgetcat)."')";
					$results = executeOCIUpdateQuery($v_query,$ds_conn);
					$row_inserted++; // record inserted 
					
				
					//writing cost here
					for($i=0;$i<count($v_cost_pk);$i++){
						if (strlen($v_cost[$i]) > 0 ) {
							$v_costval = str_replace(",","",str_replace("$","",trim($data[$v_cost[$i]])));
							if (is_numeric($v_costval)) {
								$v_query = "INSERT INTO SCH_EVENTCOST (pk_eventcost,eventcost_value,fk_event,fk_cost_desc,fk_currency) VALUES
								   (sch_eventcost_seq.NEXTVAL,".trim($v_costval).",$v_event_pk,".trim($v_cost_pk[$i]).",".trim($v_currency).")";
								executeOCIUpdateQuery($v_query,$ds_conn);
							}
						}
					}				
	
					// write code list
					//$evt_q = "select * from er_codelst where fk_account=".$v_account." and CODELST_TYPE='evtaddlcode' and CODELST_CUSTOM_COL is NULL";
					$evt_q = "select * from er_codelst where CODELST_TYPE='evtaddlcode' and CODELST_CUSTOM_COL is NULL order by CODELST_SEQ";					
					$evtresults = executeOCIQuery($evt_q,$ds_conn);
					$evtrows = $results_nrows;
					$k=0;
					for($i=0; $i<=count($v_addcodelst_pk); $i++){
						if(strlen($v_addcodelst[$i]) > 0){
							$em_query = "SELECT SEQ_ER_MOREDETAILS.NEXTVAL as eventmore_pk FROM dual";
							$emresults = executeOCIQuery($em_query,$ds_conn);
							$em_event_pk = $emresults["EVENTMORE_PK"][0];
						
							$e_query = "SELECT EVENT_DEFINITION_SEQ.CURRVAL as event_pk FROM dual";
							$eresults = executeOCIQuery($e_query,$ds_conn);
							$e_event_pk = $eresults["EVENT_PK"][0];							
							
							if($k<$evtrows){
								$insQ = "INSERT INTO ER_MOREDETAILS (PK_MOREDETAILS, FK_MODPK, MD_MODNAME, MD_MODELEMENTPK, MD_MODELEMENTDATA, CREATOR) VALUES (".trim($em_event_pk).",".trim($e_event_pk).", 'evtaddlcode', ".trim($evtresults["PK_CODELST"][$i]).", '".trim($data[$v_addcodelst[$i]])."',".trim($v_user).")";
								executeOCIUpdateQuery($insQ,$ds_conn);							
								$k++;
							}
						}
					}				
				} else {
					if ($v_category_pk == $v_eventchainid) {
						$v_event_pk = $results["EVENT_ID"][0];
						$v_category_pk = $results["CHAIN_ID"][0];
						$data[$v_desc] = substr($data[$v_desc], 0, 200);
						$v_query = "update event_def set name='".$v_eventName."',description='".str_replace("'","''",trim((isset($data[$v_desc])?$data[$v_desc]:"" )))."',notes='".str_replace("'","''",trim(isset($data[$v_notes])?$data[$v_notes]:""))."',event_cptcode='".str_replace("'","''",trim(isset($data[$v_cpt])?$data[$v_cpt]:""))."', EVENT_LINE_CATEGORY='".$v_budgetcat."' where  EVENT_LIBRARY_TYPE= '".$v_exist_pk_codelist."' and event_id = ".$v_event_pk;
						$results = executeOCIUpdateQuery($v_query,$ds_conn);
						$row_updated++; //record updated here
						
						//updating cost here
						for($i=0;$i<count($v_cost_pk);$i++){
							if (strlen($v_cost[$i]) > 0 ) {
								$v_costval = str_replace(",","",str_replace("$","",trim($data[$v_cost[$i]])));
								if (is_numeric($v_costval)) {
									$v_query = "SELECT PK_EVENTCOST FROM SCH_EVENTCOST WHERE FK_EVENT = $v_event_pk and FK_COST_DESC = ".$v_cost_pk[$i];
									$results = executeOCIQuery($v_query,$ds_conn);
									if ($results_nrows == 0  ) {
										$v_query = "INSERT INTO SCH_EVENTCOST (pk_eventcost,eventcost_value,fk_event,fk_cost_desc,fk_currency) VALUES
										   (sch_eventcost_seq.NEXTVAL, ".trim($v_costval).",$v_event_pk,".trim($v_cost_pk[$i]).",".trim($v_currency).")";
										$results = executeOCIUpdateQuery($v_query,$ds_conn);
									} else {
										$v_query = "UPDATE SCH_EVENTCOST set eventcost_value = ".$v_costval.", FK_CURRENCY=".$v_currency." where PK_EVENTCOST = ".$results["PK_EVENTCOST"][0];
										$results = executeOCIUpdateQuery($v_query,$ds_conn);								
									}
								}
							}
						}
						//add code list here
						//$evt_q = "select * from er_codelst where fk_account=".$v_account." and CODELST_TYPE='evtaddlcode' and CODELST_CUSTOM_COL is NULL";
						$evt_q = "select * from er_codelst where CODELST_TYPE='evtaddlcode' and CODELST_CUSTOM_COL is NULL order by CODELST_SEQ";						
						$evtresults = executeOCIQuery($evt_q,$ds_conn);							
						$evtrows = $results_nrows;
						$k=0;
						for($i=0; $i<=count($v_addcodelst_pk); $i++){
							if(strlen($v_addcodelst[$i]) > 0){
								$em_query = "SELECT SEQ_ER_MOREDETAILS.NEXTVAL as eventmore_pk FROM dual";
								$emresults = executeOCIQuery($em_query,$ds_conn);
								$em_event_pk = $emresults["EVENTMORE_PK"][0];
													
								$e_event_pk = $v_event_pk;						
								if($k<$evtrows){
									$chkQ = "SELECT * from ER_MOREDETAILS where FK_MODPK=".$e_event_pk." and MD_MODELEMENTPK=".$evtresults["PK_CODELST"][$k];
									$chkrs = executeOCIQuery($chkQ,$ds_conn);	
									$chknrows = $results_nrows;
									if($chknrows>0){
										$updQ = "UPDATE ER_MOREDETAILS set MD_MODELEMENTDATA='".$data[$v_addcodelst[$i]]."' where FK_MODPK=".$e_event_pk." and MD_MODELEMENTPK=".$evtresults["PK_CODELST"][$i];
										executeOCIUpdateQuery($updQ,$ds_conn);																									
									}else{
										$insQ = "INSERT INTO ER_MOREDETAILS (PK_MOREDETAILS, FK_MODPK, MD_MODNAME, MD_MODELEMENTPK, MD_MODELEMENTDATA, CREATOR) VALUES (".trim($em_event_pk).",".trim($e_event_pk).", 'evtaddlcode', ".trim($evtresults["PK_CODELST"][$i]).", '".trim($data[$v_addcodelst[$i]])."',".trim($v_user).")";
										executeOCIUpdateQuery($insQ,$ds_conn);																
									}
									$k++;
								}
							}
						} 
					}
				}
			}
			
//			echo "curval= ".$v_event_pk; print "<br> <br>";			
			
			/*--------- Code for Site of Service & Facility Starts here  --------------- eT3.2 - 6*/ 
				
			$fk_account_query = "SELECT FK_ACCOUNT FROM ER_USER WHERE PK_USER = '".$_POST["user"]."'";
			$fk_account_query_results = executeOCIQuery($fk_account_query,$ds_conn);			
			$fk_account_id = $fk_account_query_results["FK_ACCOUNT"][0];
			
			$site_name_query = "SELECT SITE_NAME FROM ER_SITE WHERE FK_ACCOUNT = '".$fk_account_id."'";
			$site_name_query_results = executeOCIQuery($site_name_query,$ds_conn);
			
			$site_name_count = count($site_name_query_results["SITE_NAME"]);
			
			for($sn=0; $sn<$site_name_count; $sn++) {
				$match_sitename_query = "SELECT FK_CODELST_TYPE, PK_SITE FROM ER_SITE WHERE UPPER(SITE_NAME) = UPPER('".trim($data[$v_site_of_service])."') AND FK_ACCOUNT = '".$fk_account_id."'";
				$match_sitename_query_results = executeOCIQuery($match_sitename_query,$ds_conn);
				
				$match_site_codelst_query = "SELECT PK_CODELST FROM ER_CODELST WHERE PK_CODELST = '".trim($match_sitename_query_results["FK_CODELST_TYPE"][0])."' AND CODELST_TYPE ='site_type' AND CODELST_CUSTOM_COL = 'service'";
				$match_site_codelst_query_results = executeOCIQuery($match_site_codelst_query,$ds_conn);
				
				if($match_site_codelst_query_results["PK_CODELST"][0] != '') {
					$update_site_service_query = "UPDATE EVENT_DEF SET SERVICE_SITE_ID = ".trim($match_sitename_query_results["PK_SITE"][0])." WHERE EVENT_ID = ".$v_event_pk;
					$update_site_service_query_results = executeOCIUpdateQuery($update_site_service_query,$ds_conn);
				}
				
				//Facility
				$match_facility_query = "SELECT FK_CODELST_TYPE, PK_SITE FROM ER_SITE WHERE UPPER(SITE_NAME) = UPPER('".trim($data[$v_facility])."') AND FK_ACCOUNT = '".$fk_account_id."'";
				$match_facility_query_results = executeOCIQuery($match_facility_query,$ds_conn);
				
				$match_fac_codelst_query = "SELECT PK_CODELST FROM ER_CODELST WHERE PK_CODELST = '".trim($match_facility_query_results["FK_CODELST_TYPE"][0])."' AND CODELST_TYPE ='site_type' AND CODELST_CUSTOM_COL = 'service'";
				$match_fac_codelst_query_results = executeOCIQuery($match_fac_codelst_query,$ds_conn);
				
				if($match_fac_codelst_query_results["PK_CODELST"][0] != '') {
					$update_facility_service_query = "UPDATE EVENT_DEF SET FACILITY_ID = ".trim($match_facility_query_results["PK_SITE"][0])." WHERE EVENT_ID = ".$v_event_pk;
					$update_facility_service_query_results = executeOCIUpdateQuery($update_facility_service_query,$ds_conn);
				}
			}
			/*--------- Code for Site of Service & Facility Ends here  --------------- */
			
			/*--------- Code for Coverage Type & Coverage Note Starts here  --------------- eT3.2 - 6*/ 			
				$match_coverage_type_query = "SELECT PK_CODELST FROM SCH_CODELST WHERE CODELST_DESC = '".trim($data[$v_coverage_type])."' AND CODELST_TYPE = 'coverage_type'";
				$match_coverage_type_query_results = executeOCIQuery($match_coverage_type_query,$ds_conn);
				
				if($match_coverage_type_query_results["PK_CODELST"][0] != '') {
					$update_coverage_type_query = "UPDATE EVENT_DEF SET FK_CODELST_COVERTYPE = ".trim($match_coverage_type_query_results["PK_CODELST"][0]).", COVERAGE_NOTES = '".$data[$v_coverage_type_notes]."' WHERE EVENT_ID =".$v_event_pk;
					//print $update_coverage_type_query; print "<br> <br>";
					$update_coverage_type_query_query_results = executeOCIUpdateQuery($update_coverage_type_query,$ds_conn);
				}				
			/*--------- Code for  Coverage Type & Coverage Note Ends here  --------------- */
		}
		
	}
	$row++;
	echo '<script>refresh_totals('.($row - 1).','.$row_inserted.','.$row_updated.');</script>';
	flush();
}
echo '<script>document.getElementById("status").innerHTML = "Completed";</script>';

?>	
</div>
</body>
</html>
<?php
}
else header("location: index.php?fail=1");
?>