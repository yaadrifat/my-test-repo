<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> Migrate Data</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>

<div id="fedora-content">	
<div class="navigate">Migrate Data - Standard - Step 2</div>

<?PHP
if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 
?>
	<form name="mod_mapping" action="mig_viewmapping.php" method="post">
	<Table border="1"><TR>
	<TH>Module column name</TH>
	<TH>Type</TH>
	<TH>Subtype</TH>
	<TH>Table Column Name</TH>
	</TR>
	<?php

	$pk_vlnk_adapmod = $_GET["pk_vlnk_adapmod"];

	echo '<INPUT type="hidden" name="pk_vlnk_adapmod" value="'.$pk_vlnk_adapmod.'"/>';

	$query_sql = "select table_name from velink.vlnk_adapmod where pk_vlnk_adapmod = ".$pk_vlnk_adapmod;
	$results = executeOCIQuery($query_sql,$ds_conn);

	$query_sql = "select column_name from dba_tab_columns where table_name = '".strtoupper($results["TABLE_NAME"][0])."'";
	$results_cols = executeOCIQuery($query_sql,$ds_conn);
	$col_nrows = $results_nrows;


	$query_sql = "SELECT pk_vlnk_adapmodcol,column_name,map_col_name,map_type,map_subtyp,is_required FROM velink.VLNK_ADAPMODCOL,velink.VLNK_MODCOL WHERE pk_vlnk_modcol = fk_vlnk_modcol AND fk_vlnk_adapmod = ".$pk_vlnk_adapmod." order by is_required,column_name";

	$results = executeOCIQuery($query_sql,$ds_conn);

	for ($rec = 0; $rec < $results_nrows; $rec++){
		if ($results["IS_REQUIRED"][$rec] == 1){
			echo "<TR bgcolor=orange>";
		} else {
			if ($rec % 2) {
				echo '<TR class="even">';
			} else {
				echo '<TR>';
			}
		}
	?>
		<TD><?php echo $results["COLUMN_NAME"][$rec] . "&nbsp;"; ?></TD>
		<TD><?php echo $results["MAP_TYPE"][$rec] . "&nbsp;"; ?></TD>
		<TD><?php echo $results["MAP_SUBTYP"][$rec] . "&nbsp;"; ?></TD>
	<?PHP
		$dd_columns = '<option value="">Select an option</option>';
		for ($rec_col = 0; $rec_col < $col_nrows; $rec_col++){
			if ($results["MAP_COL_NAME"][$rec] == $results_cols["COLUMN_NAME"][$rec_col]){
		    	$dd_columns .= "<option selected value=".$results_cols["COLUMN_NAME"][$rec_col].">".$results_cols["COLUMN_NAME"][$rec_col]."</option>";
		    } else {
		    	$dd_columns .= "<option value=".$results_cols["COLUMN_NAME"][$rec_col].">".$results_cols["COLUMN_NAME"][$rec_col]."</option>";
		    }
		}
	?>
		<TD><?PHP echo "<select name=\"column_name[$rec]\">".$dd_columns."</select>"; ?></TD>
		<INPUT type="hidden" name="pk_vlnk_adapmodcol[<?PHP echo $rec; ?>]" value='<?PHP echo $results["PK_VLNK_ADAPMODCOL"][$rec] ?>'/>
	<?php
		echo "</TR>";
	}
	?>
	</TABLE>
	<input type="submit" name="submit" value="Submit"/>
	</form>
<?PHP
} else {
	$v_pk_vlnk_adapmodcol = $_POST["pk_vlnk_adapmodcol"];
	$v_column_name = $_POST["column_name"];
	$v_pk_vlnk_adapmod = $_POST["pk_vlnk_adapmod"];
	for ($i=0;$i<count($v_column_name);$i++) {
		$query = "update velink.vlnk_adapmodcol set map_col_name = '".$v_column_name[$i]."' where pk_vlnk_adapmodcol = ".$v_pk_vlnk_adapmodcol[$i];
		executeOCIUpdateQuery($query,$ds_conn);
	}
	echo "Mapping Saved.";

	$query_sql = "select fk_vlnk_adaptor from velink.vlnk_adapmod where pk_vlnk_adapmod = ".$v_pk_vlnk_adapmod;
	$results = executeOCIQuery($query_sql,$ds_conn);
	echo '<meta http-equiv="refresh" content="0; url=./mig_modules.php?pk_vlnk_adaptor='.$results["FK_VLNK_ADAPTOR"][0].'"> ';

}
?>
</div>


</body>
</html>
<?php
}
else header("location: ./index.php?fail=1");
?>
