<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>    <title>Velink -> Update Lookup</title>
<script>
function f_validate(doc){
	if (doc.uploadedfile.value == ""){
		alert("File name cannot be blank.");
		return false;
	}
}
</script>

</head>
<?php
include("./includes/header.php");
include("./includes/oci_functions.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
<body>
<!--      <div class="browserDefault" id="browserDefault"> -->
<div id="fedora-content">	
<?PHP
if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 



$v_query = "SELECT lkpview_name,pk_lkpview FROM er_lkpview where lkpview_name in ('ACS VAD Implant','ACS Valve Prosthesis','ACS Coefficients') ORDER BY 1";
$results = executeOCIQuery($v_query,$ds_conn);

$dd_lookup = '';
for ($rec = 0; $rec < $results_nrows; $rec++){
	$dd_lookup .= '<input ';
	if ($rec == 0) 	$dd_lookup .= ' checked ';
	$dd_lookup .= 'type=radio name=lookup value="'.$results["LKPVIEW_NAME"][$rec].'">'.$results["LKPVIEW_NAME"][$rec];
}




?>

Update Lookup data:
<form name="lookup" enctype="multipart/form-data" action="sts_acs_update_lookup.php" method="POST" onSubmit="return f_validate(document.lookup)">
<table width="100%">
<tr><td>Lookup Name:</td><td><?PHP echo $dd_lookup; ?></td></tr>
<tr><td>File name:</td><td><input type="file" name="uploadedfile" size="70"/></td></tr>
<input type="hidden" name="delimiter" value="|"/>



<tr><td><input type="submit" name="submit" value="Submit"/></td></tr>
</table>
</form>

<?PHP } else {

$delimiter = $_POST["delimiter"];
$lookup = $_POST["lookup"];
$filename = $_FILES['uploadedfile']['name'];

$checkfile = 0;
if (($lookup == 'ACS Coefficients') && (strpos('TEST'.$filename,'ModelCoefficients') > 0 )) $checkfile = 1;
if (($lookup == 'ACS VAD Implant') && (strpos('TEST'.$filename,'VADDevices') > 0 )) $checkfile = 1;
if (($lookup == 'ACS Valve Prosthesis') && (strpos('TEST'.$filename,'ValveDevices') > 0 )) $checkfile = 1;

if ($checkfile == 0) {
	echo "Selected file name does not match the Lookup Name based on STS convention. Click back and try again...";
} else {

	$v_query = "select fk_lkplib  from er_lkpview where lkpview_name = '$lookup'";
	$results = executeOCIQuery($v_query,$ds_conn);
	$v_lkplib = $results["FK_LKPLIB"][0];

	$v_query = "SELECT pk_lkpcol,lkpcol_name,lkpcol_dispval FROM er_lkpcol WHERE fk_lkplib = ".$v_lkplib." order by lkpcol_name";
	$results = executeOCIQuery($v_query,$ds_conn);


	  $curDir = getcwd();
	  $target_path = $curDir."/upload/";

	  $target_path = $target_path.basename( $_FILES['uploadedfile']['name']);

	  if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
	      echo "The file ".  basename( $_FILES['uploadedfile']['name']).
	      " has been uploaded"."<BR>";


		$results = executeOCIUpdateQuery("delete from er_lkpdata where fk_lkplib = $v_lkplib",$ds_conn);
		$handle = fopen($target_path, "r");
		$row = 0;
		while (($data = fgetcsv($handle, 10000, $delimiter)) !== FALSE) {
		      $num = count($data);
		      $row++;
			  switch ($row):
			  case 1:
			  	$version = $data[0];
			  	break;
		      case 2:
		         $colsql = "insert into er_lkpdata (pk_lkpdata, fk_lkplib,custom001,";
		         for ($c=0; $c < $num; $c++) {
		             $colsql .= 'custom'.str_pad(($c + 2),3,'0',STR_PAD_LEFT).",";
		         }
		         $colsql = substr($colsql,0,strlen($colsql)-1).") values (";
		         break;
			  default:
		       	$valsql = "seq_er_lkpdata.nextval,$v_lkplib,'$version',";
		         for ($c=0; $c < $num; $c++) {
		             $dataval = str_replace("'","''",$data[$c]);
		             $valsql .= "'$dataval',";
		         }
		         $valsql = substr($valsql,0,strlen($valsql)-1).")";
		         $sql = $colsql.$valsql;
//echo $sql."<BR>";
				$results = executeOCIUpdateQuery($sql,$ds_conn);
			   endswitch;
		}		
		
        fclose($handle);
        $row = $row + 1;
		echo "<BR>Total Rows: ".$row;
		
	  } else{
	      echo "There was an error uploading the file, please try again!"."<BR>";
	  }





	}
}
?>

	
</div>


</body>
</html>
<?php

}
else header("location: index.php?fail=1");
?>
