<?php

	
	
session_start();	// Maintain session state
header("Cache-control: private");	// Fixes IE6's back button problem.

// Check that we are logged in and an admin
if(@$_SESSION["user"]){
?>

<html>
<head>
    <title>Velos eTools -> CDUS</title>

<?php
include("./includes/oci_functions.php");

	
include("./includes/header.php");

$ds_conn = ocilogon("eres", $_SESSION["DS_PASS"], $_SESSION["DB"]); 

?>
</head>


<body>
<div id="fedora-content">	
<div class="navigate">CDUS Submission - New Study Submission</div>
	
<?php



if ($_SERVER['REQUEST_METHOD'] != 'POST'){ 

$v_exp_definition = $_GET["fk_exp_definition"];

$query_sql = "select study_number from er_study where fk_account = ".$_GET["account"]." order by study_number";
$results = executeOCIQuery($query_sql,$ds_conn);

$v_sdropdown = '<option value="" SELECTED>Select an option</option>';
for ($rec = 0; $rec < $results_nrows; $rec++){
      $v_sdropdown .= '<option value="'.$results["STUDY_NUMBER"][$rec].'">'.$results["STUDY_NUMBER"][$rec]."</option>";

}

$query_sql = "SELECT DISTINCT keyword_desc,keyword,value,keyword_html FROM EXP_PARAMS WHERE keyword IS NOT NULL AND fk_exp_definition_details IN (SELECT pk_exp_definition_details FROM EXP_DEFINITION_DETAILS WHERE fk_exp_definition = ".$v_exp_definition.")";
$results = executeOCIQuery($query_sql,$ds_conn);

$dropdown = '';

?>
	
<FORM action="cdus_mainparam.php" method=post>
<input type="hidden" name="account" value="<?PHP echo $_GET['account']; ?>">
<TABLE>
<TR>
<!--
<input type = "hidden" name="pk_exp_datadetails" value=<?php echo $pk_exp_datadetails; ?>>
<input type = "hidden" name="pk_datamain" value=<?php echo $pk_datamain; ?>>
-->
<input type = "hidden" name="fk_exp_definition" value=<?php echo $v_exp_definition; ?>>
</tr>
<?php


$v_dropdown = '';	
for ($rec = 0; $rec < $results_nrows; $rec++){
	echo "<TR>";
	echo "<TD>".$results["KEYWORD_DESC"][$rec]."</TD>";
	$v_dropdown = $results["KEYWORD_HTML"][$rec];

?>
	<TD><?php echo "<INPUT type=\"hidden\" name=\"keyword[$rec]\" value=\"".$results["KEYWORD"][$rec].'"></input>'; ?></TD>
<?PHP
	if (empty($v_dropdown)) {
		if ($results["KEYWORD"][$rec] == 'STUDYNUMBER'){
?>
	<TD><?php echo "<SELECT name=\"keyword_value[$rec]\" value=\"".$results["VALUE"][$rec].'">'.$v_sdropdown.'</SELECT>'; ?></TD>

<?PHP
			} else {
?>
	<TD><?php echo "<INPUT name=\"keyword_value[$rec]\" value=\"".$results["VALUE"][$rec].'"></input>'; ?></TD>
<?PHP
}
//		echo '<TD><INPUT size="60" type="text" name="keyword_value[$rec] value="'.$results["VALUE"][$rec].'"</TD>';
//		echo '<TD><INPUT size="60" type="text" name="keyword_value[$rec]'.$results["KEYWORD"][$rec].'" value="'.$results["VALUE"][$rec].'"</TD>';
//		<TD><?php echo "<INPUT readonly name=\"dispname[$rec]\" value=\"".$results["MP_DISPNAME"][$rec].'"></input>'; </TD>
	} else {

?>
	<TD><?php echo "<SELECT name=\"keyword_value[$rec]\" value=\"".$results["VALUE"][$rec].'">'.$v_dropdown.'</SELECT>'; ?></TD>
<?PHP

//		echo '<TD><SELECT name="keyword_value[$rec]">'.$v_dropdown.'</SELECT></TD>';

//		echo '<TD><SELECT name="'.$results["KEYWORD"][$rec].'">'.$v_dropdown.'</SELECT></TD>';
	}

	echo "</TR>";
}

?>

<td>
	<input type="submit" name="submit" value="Submit">
</td></tr>
</TABLE>
</form>

<?php 
} else {

$v_exp_definition = $_POST['fk_exp_definition'];

$v_keyword = $_POST["keyword"];
$v_keyword_value = $_POST["keyword_value"];

for ($i=0; $i < count($v_keyword); $i++){
	echo $v_keyword[$i];
	echo "  |  ";
	echo $v_keyword_value[$i];
	echo "<BR>";
	$query = "update exp_params set value = '".$v_keyword_value[$i]."' where keyword='".$v_keyword[$i]."' and fk_exp_definition_details IN (SELECT pk_exp_definition_details FROM EXP_DEFINITION_DETAILS WHERE fk_exp_definition = ".$v_exp_definition.")";
	$results = executeOCIUpdateQuery($query,$ds_conn);

	if ($v_keyword[$i] == 'STUDYNUMBER') {
		$v_studynum = $v_keyword_value[$i];
	}
	
	if ($v_keyword[$i] == 'CUTOFFDATE') {
		$v_cutoffdate = $v_keyword_value[$i];
	}

}



$query = "select count(*) as count from er_study where study_number = '$v_studynum'";
$results = executeOCIQuery($query,$ds_conn);
if ($results["COUNT"][0] == 0) {
	echo "Study number <b>$v_studynum</b> does not exist.<BR><BR>";
} else {
echo "Exporting data...<BR><BR>";

/*
$query = "update exp_params set value = '".$v_studynum."' where keyword='STUDYNUMBER' and fk_exp_definition_details IN (SELECT pk_exp_definition_details FROM EXP_DEFINITION_DETAILS WHERE fk_exp_definition = ".$v_exp_definition.")";
$results = executeOCIUpdateQuery($query,$ds_conn);

echo "Exporting data...1<BR><BR>";

$query = "update exp_params set value = '".$v_cutoffdate."' where keyword='CUTOFFDATE' and fk_exp_definition_details IN (SELECT pk_exp_definition_details FROM EXP_DEFINITION_DETAILS WHERE fk_exp_definition = ".$v_exp_definition.")";
$results = executeOCIUpdateQuery($query,$ds_conn);

echo "Exporting data...2<BR><BR>";
*/

OCICommit($ds_conn);

$query_sql = "begin PKG_CDUS.SP_PROCESS_DATA(".$v_exp_definition.",'Study Number: ".$v_studynum."    Cutoff Date: ".$v_cutoffdate."',".trim($_POST["account"])."); end;";

echo "Exporting data...3<BR><BR>";
echo $query_sql;
	
echo "Exporting data...4<BR><BR>";

$results = executeOCIUpdateQuery($query_sql,$ds_conn);

$query_sql = "select max(pk_datamain) as pk_datamain from exp_datamain where fk_exp_definition = " . $v_exp_definition;

echo "Exporting data...5<BR><BR>";
echo $query_sql;

$results = executeOCIQuery($query_sql,$ds_conn);

echo "test...";

$v_pk_datamain = $results["PK_DATAMAIN"][0];
	
echo "Data Exported...<BR><BR>";
echo "Generating Mapping...<BR><BR>";

$query_sql = "begin PKG_CDUS.SP_generate_mapping(".$v_exp_definition.",".$v_pk_datamain."); end;";
$results = executeOCIUpdateQuery($query_sql,$ds_conn);

echo "Mapping Generated...";

OCILogoff($ds_conn);

$url = "./cdus.php?fk_exp_definition=".$v_exp_definition;
echo "<meta http-equiv=\"refresh\" content=\"0; url=./".$url."\">";
?>
<!-- <meta http-equiv="refresh" content="0; url=./codelist.php"> -->
<?PHP
}
}

?>
</div>
<!--


-->
</body>
</html>


<?php
}
else header("location: index.php?fail=1");
?>
		
