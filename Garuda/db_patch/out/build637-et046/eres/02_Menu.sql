DECLARE
  index_count INTEGER;
BEGIN
  Select Count(*) Into Index_Count From Er_Object_Settings Where
    OBJECT_SUBTYPE = 'query_build_menu' and OBJECT_NAME = 'top_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'TM', 'query_build_menu', 'top_menu', 19, 1, 'Simple CBU Queries', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting query_build_menu for top_menu already exists');
  end if;
END;
/ 

commit;

DECLARE
  index_count INTEGER;
BEGIN
  Select Count(*) Into Index_Count From Er_Object_Settings Where
    OBJECT_SUBTYPE = 'simp_cbu_rpt' and OBJECT_NAME = 'query_build_menu' ;
  if (index_count = 0) then
    Insert into ER_OBJECT_SETTINGS
     (PK_OBJECT_SETTINGS, OBJECT_TYPE, OBJECT_SUBTYPE, OBJECT_NAME, OBJECT_SEQUENCE, 
      OBJECT_VISIBLE, OBJECT_DISPLAYTEXT, FK_ACCOUNT)
     Values
     (SEQ_ER_OBJECT_SETTINGS.nextval, 'M', 'simp_cbu_rpt', 'query_build_menu', 1,1 , 'CBU Queries', 0);
    dbms_output.put_line('One row created');
  else
    dbms_output.put_line('Object setting query_build_menu for top_menu already exists');
  end if;
END;
/ 

commit;


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,267,2,'02_Menu.sql',sysdate,'9.0.0 B#637-ET046');
commit;