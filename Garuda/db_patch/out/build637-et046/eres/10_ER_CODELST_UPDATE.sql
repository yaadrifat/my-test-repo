--start update er_codelst table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst where codelst_type='qury_build_rprt' and codelst_subtyp='quicksummaryrpt';
  If (V_Record_Exists = 1) Then
	update er_codelst set codelst_desc='Shipped CBUs' where codelst_type='qury_build_rprt' and codelst_subtyp='quicksummaryrpt';
commit;
  end if;
end;
/
--END--



  
INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,267,10,'10_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 B#637-ET046');
commit;