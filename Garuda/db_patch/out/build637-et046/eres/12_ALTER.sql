 --Sql for Table CB_FINAL_REV_UPLOAD_INFO to add column ATTACHMENT_TYPE
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_FINAL_REV_UPLOAD_INFO' AND column_name = 'ATTACHMENT_TYPE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_FINAL_REV_UPLOAD_INFO add ATTACHMENT_TYPE VARCHAR2(50)');
  end if;
end;
/
COMMENT ON COLUMN CB_FINAL_REV_UPLOAD_INFO.ATTACHMENT_TYPE IS 'Stores attachment type'; 
commit;


  
INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,267,12,'12_ALTER.sql',sysdate,'9.0.0 B#637-ET046');
commit;