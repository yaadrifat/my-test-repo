--Inserting records in to CB_QUESTION_GRP for FMHQ N2C Version--
--Question 1--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='m_or_f_adopted_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 1.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_medhist_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='m_or_f_adopted_medhist_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='baby_moth_fath_bld_rel_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='preg_use_dnr_egg_sperm_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='dnr_egg_sperm_fmhq_avail_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='dnr_egg_sperm_fmhq_avail_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='abn_prenat_test_rslt_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_testname')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='abn_prenat_test_rslt_testname'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='abn_prenat_test_abort_desc'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_diag_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='abn_prenat_test_rslt_diag_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.c.1--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_diag_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='abn_prenat_test_rslt_diag_desc'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='child_die_age_10_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='child_die_age_10_desc'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='baby_moth_fath_bld_rel_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_cause')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stillborn_cause'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='can_leuk_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_cl_other_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.b.1--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type_desc1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.b.1--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type_desc2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.b.1--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type_desc3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_rbc_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_wbc_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_pltlt_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Blood Disease or Disorder'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_bld_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Blood Disease or Disorder'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_bld_dises_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Blood Disease or Disorder'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_bld_dises_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Blood Disease or Disorder'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_bld_dises_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Blood Disease or Disorder'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_bld_dises_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Blood Disease or Disorder'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_bld_dises_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Blood Disease or Disorder'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_bld_dises_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Blood Disease or Disorder'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_bld_dises_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_sickle_cell_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Hemoglobin Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_sickle_cell_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_sickle_cell_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Hemoglobin Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_sickle_cell_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_thalassemia_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Hemoglobin Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_thalassemia_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_thalassemia_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Hemoglobin Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_thalassemia_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_aids_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_aids_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='severe_aut_imm_sys_disordr_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='severe_aut_imm_sys_disordr_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='severe_aut_imm_sys_disordr_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='severe_aut_imm_sys_disordr_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='severe_aut_imm_sys_disordr_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='oth_immune_dis'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='oth_immune_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='oth_immune_desc1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='oth_immune_desc2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='oth_immune_desc3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='oth_immune_desc4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='oth_immune_desc5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Acquired Immune System Disorders'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='oth_immune_desc6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chron_bld_tranf_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chron_bld_tranf_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hemolytic_anemia_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hemolytic_anemia_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spleen_removed_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spleen_removed_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 22--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='gallbladder_removed_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 22.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='gallbladder_removed_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cruetz_jakob_dis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cruetz_jakob_dis_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cruetz_jakob_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cruetz_jakob_dis_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 25--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='answ_both_moth_fath_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--Cb_QUESTION table--
--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'first_preg_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'First Pregnancy?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'first_preg_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'num_preg';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'How many other pregnancies?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'num_preg',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'num_live_births';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'How many live born?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'), 'num_live_births',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'miscrg_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Any miscarriages?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'miscrg_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'num_miscrg';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'How many miscarriages?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'num_miscrg',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/


--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'num_stillborn';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'How many stillborn?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'num_stillborn',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 22.Immune System Disorders--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_imm_sys_disordr_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Immune System Disorders?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'inher_imm_sys_disordr_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 23.Neurological Disorder--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_neuro_disordr_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Neurological Disorders?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'inher_neuro_disordr_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--END--
--Inserting records in to CB_QUESTION_GRP for FMHQ N2B Version--
--Question 1--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='first_preg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='first_preg_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_preg')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='num_preg'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_live_births')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='num_live_births'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='child_die_age_10_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='child_die_age_10_desc'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='miscrg_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_miscrg')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='num_miscrg'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stillborn_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_stillborn')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='num_stillborn'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='abn_prenat_test_rslt_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='abn_prenat_test_abort_desc'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='baby_moth_fath_bld_rel_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='preg_use_dnr_egg_sperm_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='m_or_f_adopted_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='can_leuk_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_cl_other_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b.1--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type_desc1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b.1--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type_desc2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b.1--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type_desc3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chron_bld_tranf_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chron_bld_tranf_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hemolytic_anemia_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hemolytic_anemia_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spleen_removed_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spleen_removed_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='gallbladder_removed_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='gallbladder_removed_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_rbc_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_wbc_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_pltlt_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 24--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 25--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='answ_both_moth_fath_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Inserting records in to CB_QUESTION_GRP for FMHQ N2 Version--
--Question 1--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='first_preg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='first_preg_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_preg')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='num_preg'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_live_births')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='num_live_births'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='child_die_age_10_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='child_die_age_10_desc'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='miscrg_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_miscrg')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='num_miscrg'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stillborn_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_stillborn')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='num_stillborn'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='abn_prenat_test_rslt_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='abn_prenat_test_abort_desc'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='baby_moth_fath_bld_rel_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='preg_use_dnr_egg_sperm_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Pregnancy History'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='m_or_f_adopted_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='can_leuk_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_cl_other_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b.1--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type_desc1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b.1--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type_desc2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b.1--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Cancer/Leukemia Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cancer_leuk_rel_type_desc3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chron_bld_tranf_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chron_bld_tranf_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hemolytic_anemia_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hemolytic_anemia_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spleen_removed_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spleen_removed_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='gallbladder_removed_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='gallbladder_removed_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_rbc_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Red Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='red_bld_cell_dis_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_wbc_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='White Blood Cell Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Immune Deficiencies'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Metabolic/Storage Problems'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_desc6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_pltlt_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Platelet Disease'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 24--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_rel'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc1'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc2'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc3'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc4'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc5'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc6'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 25--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Other Diseases'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='answ_both_moth_fath_ind'),(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--Inserting data into CB_FORM_Question table For MRQ N2, N2B, N2C Froms--
--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='first_preg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='first_preg_ind'),null,null, null,'1','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_preg')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_preg'),null,null, null,'2','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_live_births')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_live_births'),null,null, null,'3','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),null, null, null,'4','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'4.a',NULL,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind'),null,null, null,'5','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_miscrg')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_miscrg'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'5.a',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),null,null, null,'6','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_stillborn')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_stillborn'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'6.a',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'), null,null, null,'7','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'7.a',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind'), null,null, null,'8','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind'), null,null, null,'9','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind'),null,null, null,'10','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),null,null, null,'11','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'11.a','fmhq_res2',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res2' and CODELST_SUBTYP='babymom'),'11.b','cncr_type',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_desc1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type1'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='cncr_type' and CODELST_SUBTYP='otskincncr'),'11.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res2' and CODELST_SUBTYP='babydad'),'11.b','cncr_type',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_desc2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type2'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='cncr_type' and CODELST_SUBTYP='otskincncr'),'11.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res2' and CODELST_SUBTYP='babysib'),'11.b','cncr_type',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_desc3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type3'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='cncr_type' and CODELST_SUBTYP='otskincncr'),'11.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),null, null, null,'12','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'12.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'), null,null, null,'13','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'13.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),null,null, null,'14','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'14.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),null, null, null,'15','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'15.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'), null,null, null,'16','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'16.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymom'),'16.b','rbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydad'),'16.b','rbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babysib'),'16.b','rbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babygpa'),'16.b','rbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymomsib'),'16.b','rbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydadsib'),'16.b','rbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),null, null, null,'17','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'17.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymom'),'17.b','wbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydad'),'17.b','wbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babysib'),'17.b','wbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babygpa'),'17.b','wbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymomsib'),'17.b','wbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydadsib'),'17.b','wbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'), null,null, null,'18','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'18.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymom'),'18.b','immune_def',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydad'),'18.b','immune_def',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babysib'),'18.b','immune_def',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babygpa'),'18.b','immune_def',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymomsib'),'18.b','immune_def',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydadsib'),'18.b','immune_def',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),null,null, null,'19','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'19.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymom'),'19.b','metab_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type1'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='metab_dis' and CODELST_SUBTYP='storgdis'),'19.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydad'),'19.b','metab_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type2'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='metab_dis' and CODELST_SUBTYP='storgdis'),'19.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babysib'),'19.b','metab_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type3'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='metab_dis' and CODELST_SUBTYP='storgdis'),'19.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babygpa'),'19.b','metab_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type4'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='metab_dis' and CODELST_SUBTYP='storgdis'),'19.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymomsib'),'19.b','metab_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type5'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='metab_dis' and CODELST_SUBTYP='storgdis'),'19.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydadsib'),'19.b','metab_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type6'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='metab_dis' and CODELST_SUBTYP='storgdis'),'19.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),null,null, null,'20','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'20.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymom'),'20.b','plate_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydad'),'20.b','plate_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babysib'),'20.b','plate_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babygpa'),'20.b','plate_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymomsib'),'20.b','plate_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydadsib'),'20.b','plate_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),null,null, null,'23','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'23.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymom'),'23.b',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydad'),'23.b',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babysib'),'23.b',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babygpa'),'23.b',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymomsib'),'23.b',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydadsib'),'23.b',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind'),null,null, null,'22','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Inserting data into CB_FORM_Question table For MRQ N2B Form--
--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='first_preg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='first_preg_ind'),null,null, null,'1','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_preg')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_preg'),null,null, null,'2','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_live_births')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_live_births'),null,null, null,'3','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),null, null, null,'4','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'4.a',NULL,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind'),null,null, null,'5','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_miscrg')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_miscrg'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'5.a',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),null,null, null,'6','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_stillborn')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_stillborn'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'6.a',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'), null,null, null,'7','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'7.a',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind'), null,null, null,'8','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind'), null,null, null,'9','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind'),null,null, null,'10','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),null,null, null,'11','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'11.a','fmhq_res2',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res2' and CODELST_SUBTYP='babymom'),'11.b','cncr_type',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_desc1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type1'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='cncr_type' and CODELST_SUBTYP='otskincncr'),'11.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res2' and CODELST_SUBTYP='babydad'),'11.b','cncr_type',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_desc2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type2'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='cncr_type' and CODELST_SUBTYP='otskincncr'),'11.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res2' and CODELST_SUBTYP='babysib'),'11.b','cncr_type',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_desc3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type3'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='cncr_type' and CODELST_SUBTYP='otskincncr'),'11.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),null, null, null,'12','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'12.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'), null,null, null,'13','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'13.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),null,null, null,'14','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'14.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),null, null, null,'15','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'15.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'), null,null, null,'16','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'16.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymom'),'16.b','rbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydad'),'16.b','rbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babysib'),'16.b','rbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babygpa'),'16.b','rbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymomsib'),'16.b','rbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydadsib'),'16.b','rbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),null, null, null,'17','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'17.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymom'),'17.b','wbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydad'),'17.b','wbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babysib'),'17.b','wbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babygpa'),'17.b','wbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymomsib'),'17.b','wbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_type6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydadsib'),'17.b','wbc_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'), null,null, null,'18','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'18.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymom'),'18.b','immune_def',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydad'),'18.b','immune_def',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babysib'),'18.b','immune_def',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babygpa'),'18.b','immune_def',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymomsib'),'18.b','immune_def',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel_type6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydadsib'),'18.b','immune_def',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),null,null, null,'19','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'19.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymom'),'19.b','metab_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type1'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='metab_dis' and CODELST_SUBTYP='storgdis'),'19.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydad'),'19.b','metab_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type2'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='metab_dis' and CODELST_SUBTYP='storgdis'),'19.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babysib'),'19.b','metab_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type3'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='metab_dis' and CODELST_SUBTYP='storgdis'),'19.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babygpa'),'19.b','metab_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type4'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='metab_dis' and CODELST_SUBTYP='storgdis'),'19.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymomsib'),'19.b','metab_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type5'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='metab_dis' and CODELST_SUBTYP='storgdis'),'19.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydadsib'),'19.b','metab_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.b.1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type6'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='metab_dis' and CODELST_SUBTYP='storgdis'),'19.b.1',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),null,null, null,'20','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'20.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymom'),'20.b','plate_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydad'),'20.b','plate_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babysib'),'20.b','plate_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babygpa'),'20.b','plate_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymomsib'),'20.b','plate_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydadsib'),'20.b','plate_dis',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),null,null, null,'23','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes'),'23.a','fmhq_res3',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymom'),'23.b',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydad'),'23.b',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babysib'),'23.b',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babygpa'),'23.b',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babymomsib'),'23.b',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_type6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_CODELST from ER_CODELST where CODELST_TYPE='fmhq_res3' and CODELST_SUBTYP='babydadsib'),'23.b',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 24--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind'),null,null, null,'22','fmhq_res1',NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

-- Function to copy the values FROM FHMQ Version N2D to N2C--
DECLARE
V_FK_FORM NUMBER;
V_FK_QUESTION NUMBER;
V_QUES_SEQ VARCHAR2(15 BYTE);
V_FK_MASTER_QUES NUMBER;
V_FK_DEPENDENT_QUES NUMBER;
V_FK_DEPENDENT_QUES_VALUE VARCHAR2(400 BYTE);
V_RESPONSE_VALUE VARCHAR2(400 BYTE);
V_UNEXPECTED_RESPONSE_VALUE VARCHAR2(400 BYTE);
V_RECORDS_CNT NUMBER;
BEGIN
SELECT COUNT(*) INTO V_RECORDS_CNT FROM CB_FORM_QUESTIONS WHERE FK_FORM=(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
IF V_RECORDS_CNT=0 THEN 
SELECT PK_FORM INTO V_FK_FORM FROM CB_FORMS WHERE forms_desc='FMHQ' and version='N2C';
FOR STAT IN(SELECT FK_QUESTION,QUES_SEQ,FK_MASTER_QUES,FK_DEPENDENT_QUES,FK_DEPENDENT_QUES_VALUE,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE FROM CB_FORM_QUESTIONS WHERE FK_FORM=(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D'))
LOOP
V_FK_QUESTION := STAT.FK_QUESTION;
V_QUES_SEQ := STAT.QUES_SEQ;
V_FK_MASTER_QUES := STAT.FK_MASTER_QUES;
V_FK_DEPENDENT_QUES := STAT.FK_DEPENDENT_QUES;
V_FK_DEPENDENT_QUES_VALUE := STAT.FK_DEPENDENT_QUES_VALUE;
V_RESPONSE_VALUE := STAT.RESPONSE_VALUE;
V_UNEXPECTED_RESPONSE_VALUE := STAT.UNEXPECTED_RESPONSE_VALUE;
INSERT INTO CB_FORM_QUESTIONS(PK_FORM_QUESTION,FK_FORM,FK_QUESTION,CREATED_ON,QUES_SEQ,FK_MASTER_QUES,FK_DEPENDENT_QUES,FK_DEPENDENT_QUES_VALUE,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE)
VALUES(SEQ_CB_FORM_QUESTIONS.nextval,V_FK_FORM,V_FK_QUESTION,sysdate,V_QUES_SEQ,V_FK_MASTER_QUES,V_FK_DEPENDENT_QUES,V_FK_DEPENDENT_QUES_VALUE,V_RESPONSE_VALUE,V_UNEXPECTED_RESPONSE_VALUE);
  COMMIT;
END LOOP;
END IF;
END;
/ 
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,182,9,'09_FMHQ_Older_Forms.sql',sysdate,'9.0.0 B#637-ET002');

commit;
