--START UPDATING CB_QUESTION TABLE FOR WHITE SPACES AND EXTRA CHAR--
--Question SN 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'donate_cb_prev_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever donated or attempted to donate cord blood using your current, or a different name, to this cord blood bank?' where QUES_CODE = 'donate_cb_prev_ind';
commit;
  end if;
end;
/
--end--
--Question SN 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'refused_bld_dnr_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you, for any reason, been deferred or refused as a blood or cord blood donor, or been told not to donate blood or cord blood?' where QUES_CODE = 'refused_bld_dnr_ind';
commit;
  end if;
end;
/
--end--
--Question SN 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'refused_bld_dnr_ind_desc';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='If yes, why?' where QUES_CODE = 'refused_bld_dnr_ind_desc';
commit;
  end if;
end;
/
--end--
--Question SN 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tkn_medications_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you taken any of the following medications:' where QUES_CODE = 'tkn_medications_ind';
commit;
  end if;
end;
/
--end--
--Question SN 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'bovine_insulin_since_1980_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Insulin from cows (bovine or beef insulin) since 1980?' where QUES_CODE = 'bovine_insulin_since_1980_ind';
commit;
  end if;
end;
/
--end--
--Question SN 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'evr_tkn_pit_gh_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Growth hormone from pituitary glands ever?' where QUES_CODE = 'evr_tkn_pit_gh_ind';
commit;
  end if;
end;
/
--end--
--Question SN 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'rabies_vacc_pst_year_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Rabies vaccination in the past year?' where QUES_CODE = 'rabies_vacc_pst_year_ind';
commit;
  end if;
end;
/
--end--
--Question SN 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'shots_vacc_pst_8wk_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 8 weeks, have you had any shots or vaccinations?' where QUES_CODE = 'shots_vacc_pst_8wk_ind';
commit;
  end if;
end;
/
--end--
--Question SN 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'shots_vacc_pst_8wk_desc';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='If yes, please describe' where QUES_CODE = 'shots_vacc_pst_8wk_desc';
commit;
  end if;
end;
/
--end--
--Question SN 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cntc_smallpox_vaccine_12wk_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 12 weeks, have you had contact with someone who has received the smallpox vaccine?' where QUES_CODE = 'cntc_smallpox_vaccine_12wk_ind';
commit;
  end if;
end;
/
--end--
--Question SN 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'pst_4mth_illness_symptom_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 4 months, have you experienced two or more of the following: a fever (>100.5&#176;F or 38.06&#176;C), headache, muscle weakness, skin rash on trunk of the body, or swollen lymph glands?' where QUES_CODE = 'pst_4mth_illness_symptom_ind';
commit;
  end if;
end;
/
--end--
--Question SN 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'symptom_desc';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='If yes, please describe' where QUES_CODE = 'symptom_desc';
commit;
  end if;
end;
/
--end--
--Question SN 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cncr_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever had any type of cancer, including leukemia?' where QUES_CODE = 'cncr_ind';
commit;
  end if;
end;
/
--end--
--Question SN 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'bld_prblm_5yr_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 5 years, have you had a bleeding problem, such as hemophilia or other clotting factor deficiencies, and received human-derived clotting factor concentrates?' where QUES_CODE = 'bld_prblm_5yr_ind';
commit;
  end if;
end;
/
--end--
--Question SN 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'wnv_preg_diag_pos_test_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='During your pregnancy, have you been diagnosed with West Nile Virus or had a positive test for West Nile Virus?' where QUES_CODE = 'wnv_preg_diag_pos_test_ind';
commit;
  end if;
end;
/
--end--
--Question SN 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'viral_hepatitis_age_11_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you had a past diagnosis of clinical, symptomatic viral hepatitis after age 11?' where QUES_CODE = 'viral_hepatitis_age_11_ind';
commit;
  end if;
end;
/
--end--
--Question SN 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'para_chagas_babesiosis_leish_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever had a parasitic blood disease such as Leishmaniasis, Babesiosis, or Chagas disease or any positive tests for Chagas or T. cruzi, including screening tests?' where QUES_CODE = 'para_chagas_babesiosis_leish_ind';
commit;
  end if;
end;
/
--end--
--Question SN 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cjd_diag_neuro_unk_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever been diagnosed with Creutzfeldt-Jakob Disease (CJD), variant CJD, dementia, any degenerative or demyelinating disease of the central nervous system, or other neurological disease where the cause is unknown?' where QUES_CODE = 'cjd_diag_neuro_unk_ind';
commit;
  end if;
end;
/
--end--
--Question SN 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'bld_rlt_diag_rsk_cjd';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have any of your blood relatives ever been diagnosed with Creutzfeldt-Jakob Disease (CJD), or have you been told that your family has an increased risk for this disease?' where QUES_CODE = 'bld_rlt_diag_rsk_cjd';
commit;
  end if;
end;
/
--end--
--Question SN 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'dura_mater_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you received a dura mater (brain covering) graft?' where QUES_CODE = 'dura_mater_ind';
commit;
  end if;
end;
/
--end--
--Question SN 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'recv_xeno_tx_med_procedure_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever had a transplant or other medical procedure that involved being exposed to live cells, tissues, or organs from an animal?' where QUES_CODE = 'recv_xeno_tx_med_procedure_ind';
commit;
  end if;
end;
/
--end--
--Question SN 22--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'liv_cntc_xeno_tx_med_proc_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever lived with or had sexual contact with anyone who had a transplant or other medical procedure that involved being exposed to live cells, tissues or organs from an animal?' where QUES_CODE = 'liv_cntc_xeno_tx_med_proc_ind';
commit;
  end if;
end;
/
--end--
--Question SN 23--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'malaria_antimalr_drug_3yr_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 3 years,</b> have you had malaria?' where QUES_CODE = 'malaria_antimalr_drug_3yr_ind';
commit;
  end if;
end;
/
--end--
--Question SN 24--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'outside_us_or_canada_3yr_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 3 years,</b> have you been outside the United States or Canada?' where QUES_CODE = 'outside_us_or_canada_3yr_ind';
commit;
  end if;
end;
/
--end--
--Question SN 25--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'outside_canada_us_desc';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='If yes, please list where, when, and for how long:' where QUES_CODE = 'outside_canada_us_desc';
commit;
  end if;
end;
/
--end--
--Question SN 26--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'blood_transf_b4coll_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the 12 months prior to collection of the cord blood unit,</b> have you had a blood transfusion?' where QUES_CODE = 'blood_transf_b4coll_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 27--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tx_stemcell_12m_oth_self_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had a transplant or tissue graft from someone other than yourself, such as organ, bone marrow, stem cell, cornea, bone, skin, or other tissue?' where QUES_CODE = 'tx_stemcell_12m_oth_self_ind';
commit;
  end if;
end;
/
--end--
--Question SN 28--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tattoo_ear_skin_pierce_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had a tattoo or ear, skin, or body piercing?' where QUES_CODE = 'tattoo_ear_skin_pierce_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 29--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tattoo_shared_nonsterile_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='If yes, were shared or non-sterile inks, needles, instruments, or procedures used for the tattoo or piercing?' where QUES_CODE = 'tattoo_shared_nonsterile_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 30--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'acc_ns_stk_cut_bld_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had an accidental needle stick or have you come into contact with someone else''s blood through an open wound (for example, a cut or sore), non-intact skin, or mucous membrane (for example, into your eye, mouth, etc)?' where QUES_CODE = 'acc_ns_stk_cut_bld_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 31--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'had_treat_syph_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had or been treated for a sexually transmitted disease, including syphilis?' where QUES_CODE = 'had_treat_syph_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 32--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'given_mn_dr_for_sex_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you given money or drugs to anyone to engage in sex with you?' where QUES_CODE = 'given_mn_dr_for_sex_ind';
commit;
  end if;
end;
/
--end--
--Question SN 33--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_tk_mn_dr_pmt_sex_12m_5yr_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you engaged in sex with anyone who had taken money or drugs for sex in the past 5 years?' where QUES_CODE = 'sex_w_tk_mn_dr_pmt_sex_12m_5yr_ind';
commit;
  end if;
end;
/
--end--
--Question SN 34--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_cntc_liv_hep_b_c_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had sexual contact or lived with a person who has active or chronic viral Hepatitis B or Hepatitis C?' where QUES_CODE = 'sex_cntc_liv_hep_b_c_ind';
commit;
  end if;
end;
/
--end--
--Question SN 35--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_iv_dr_in_pst_5yr_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had sex, even once, with anyone who has used a needle to take drugs, steroids, or anything else not prescribed by a doctor in the <u>past 5 years?</u>' where QUES_CODE = 'sex_w_iv_dr_in_pst_5yr_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 36--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_man_oth_pst_5yr_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had sex with a male who has had sex with another male, even once, in the <u>past 5 years?</u>' where QUES_CODE = 'sex_w_man_oth_pst_5yr_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 37--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_clot_fact_bld_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had sex, even once, with anyone who has taken human-derived clotting factors for a bleeding problem in the <u>past 5 years?</u>' where QUES_CODE = 'sex_w_clot_fact_bld_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 38--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_hiv_aids_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had sex, even once, with anyone who has HIV/AIDS or has had a positive test for the AIDS virus?' where QUES_CODE = 'sex_w_hiv_aids_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 39--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'prison_jail_contin_72hrs_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you been in juvenile detention, lockup, jail or prison for more than 72 continuous hours?' where QUES_CODE = 'prison_jail_contin_72hrs_ind';
commit;
  end if;
end;
/
--end--
--Question SN 40--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'pst_5yr_tk_mn_dr_sex_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 5 years,</b> have you engaged in sex in exchange for money or drugs?' where QUES_CODE = 'pst_5yr_tk_mn_dr_sex_ind';
commit;
  end if;
end;
/
--end--
--Question SN 41--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'pst_5yr_iv_drug_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 5 years,</b> have you used a needle, even once, to take drugs, steroids, or anything else not prescribed for you by a doctor?' where QUES_CODE = 'pst_5yr_iv_drug_ind';
commit;
  end if;
end;
/
--end--
--Question SN 42--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'aids_hiv_screen_test_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Do you have AIDS or have you ever tested positive for HIV (including screening tests)?' where QUES_CODE = 'aids_hiv_screen_test_ind';
commit;
  end if;
end;
/
--end--
--Question SN 43--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexpln_night_sweat';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Unexplained night sweats?' where QUES_CODE = 'unexpln_night_sweat';
commit;
  end if;
end;
/
--end--
--Question SN 44--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexpln_blue_prpl_spot_kaposi_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Blue or purple spots on or under the skin or mucous membranes typical of Kaposi''s sarcoma?' where QUES_CODE = 'unexpln_blue_prpl_spot_kaposi_ind';
commit;
  end if;
end;
/
--end--
--Question SN 45--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexpln_weight_loss';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Unexplained weight loss?' where QUES_CODE = 'unexpln_weight_loss';
commit;
  end if;
end;
/
--end--
--Question SN 46--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexpln_persist_diarrhea';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Unexplained persistent diarrhea?' where QUES_CODE = 'unexpln_persist_diarrhea';
commit;
  end if;
end;
/
--end--
--Question SN 47--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexpln_cough_short_breath';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Unexplained cough or shortness of breath?' where QUES_CODE = 'unexpln_cough_short_breath';
commit;
  end if;
end;
/
--end--
--Question SN 48--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexpln_temp_over_ten_days';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Unexplained temperature higher than 100.5&#176;F (38.06&#176;C) for more than 10 days?' where QUES_CODE = 'unexpln_temp_over_ten_days';
commit;
  end if;
end;
/
--end--
--Question SN 49--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexpln_mouth_sores_wht_spt';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Unexplained persistent white spots or sores in the mouth?' where QUES_CODE = 'unexpln_mouth_sores_wht_spt';
commit;
  end if;
end;
/
--end--
--Question SN 50--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexpln_1m_lump_nk_apit_grn_mult_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Multiple lumps in your neck, armpits, or groin lasting longer than one month?' where QUES_CODE = 'unexpln_1m_lump_nk_apit_grn_mult_ind';
commit;
  end if;
end;
/
--end--
--Question SN 51--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inf_dur_preg_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Any infections during your pregnancy?' where QUES_CODE = 'inf_dur_preg_ind';
commit;
  end if;
end;
/
--end--
--Question SN 52--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'htlv_incl_screen_test_para_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever tested positive for HTLV (including screening tests) or had unexplained paraparesis (partial paralysis affecting the lower limbs)? HTLV refers to Human T-cell Lymphotropic Virus.' where QUES_CODE = 'htlv_incl_screen_test_para_ind';
commit;
  end if;
end;
/
--end--
--Question SN 53--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_contag_person_understand_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='If a person has the AIDS virus, do you understand that the person can give it to someone else even though they may feel well and have a negative AIDS test?' where QUES_CODE = 'hiv_contag_person_understand_ind';
commit;
  end if;
end;
/
--end--
--Question SN 54--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cjd_live_travel_country_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Since 1980, have you ever lived in or traveled to any country considered to be at risk for transmission of vCJD (variant Creutzfeldt-Jakob Disease)?' where QUES_CODE = 'cjd_live_travel_country_ind';
commit;
  end if;
end;
/
--end--
--Question SN 55--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'spent_uk_ge_3m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<u>From 1980 through 1996,</u> did you spend time that adds up to 3 months or more in the United Kingdom (England, Northern Ireland, Scotland, Wales, the Isle of Man, the Channel Islands, Gibraltar, or the Falkland Islands)?' where QUES_CODE = 'spent_uk_ge_3m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 56--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'recv_trnsfsn_uk_france_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>Since 1980,</b> have you received a transfusion of blood or blood components while in the U.K. or France?' where QUES_CODE = 'recv_trnsfsn_uk_france_ind';
commit;
  end if;
end;
/
--end--
--Question SN 57--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'spent_europe_ge_5y_cjd_cnt_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Since 1980, have you spent time that adds up to 5 years or more, including time spent in the UK between 1980 and 1996, in any country considered to be at risk for transmission of vCJD (variant Creutzfeldt-Jakob Disease)?' where QUES_CODE = 'spent_europe_ge_5y_cjd_cnt_ind';
commit;
  end if;
end;
/
--end--
--Question SN 58--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'us_military_dep_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>From 1980 through 1996,</b> were you a member of the U.S. military, a civilian military employee, or a dependent of either a member of the U.S. military or civilian military employee?' where QUES_CODE = 'us_military_dep_ind';
commit;
  end if;
end;
/
--end--
--Question SN 59--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'mil_base_europe_1_ge_6m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>From 1980 through 1990,</b> did you spend a <u>total of 6 months or more</u> associated with a military base in any of the following countries: United Kingdom, Belgium, Netherlands or Germany?' where QUES_CODE = 'mil_base_europe_1_ge_6m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 60--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'mil_base_europe_2_ge_6m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>From 1980 through 1996,</b> did you spend a <u>total of 6 months or more</u> associated with a military base in any of the following countries: Spain, Portugal, Turkey, Italy or Greece?' where QUES_CODE = 'mil_base_europe_2_ge_6m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 61--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_1_2_o_performed_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was HIV 1/2 + O testing performed on a maternal sample?' where QUES_CODE = 'hiv_1_2_o_performed_ind';
commit;
  end if;
end;
/
--end--
--Question SN 62--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'born_live_cam_afr_nig_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Since 1977, were you born in, have you lived in, or have you traveled to any African country listed on the form?' where QUES_CODE = 'born_live_cam_afr_nig_ind';
commit;
  end if;
end;
/
--end--
--Question SN 63--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'trav_ctry_rec_bld_or_med_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='While in one of the African countries listed on the form, did you receive a blood transfusion or any other medical treatment with a product made from blood?' where QUES_CODE = 'trav_ctry_rec_bld_or_med_ind';
commit;
  end if;
end;
/
--end--
--Question SN 64--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_born_live_in_ctry_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you had sexual contact with anyone who was born in or lived in any African country listed on the form since 1977?' where QUES_CODE = 'sex_w_born_live_in_ctry_ind';
commit;
  end if;
end;
/
--end--
--Question SN 65--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'm_or_f_adopted_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Were you and/or the baby''s father adopted at early childhood?' where QUES_CODE = 'm_or_f_adopted_ind';
commit;
  end if;
end;
/
--end--
--Question SN 66--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'm_or_f_adopted_medhist_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='If yes, is a family medical history available for you and/or the baby''s father?' where QUES_CODE = 'm_or_f_adopted_medhist_ind';
commit;
  end if;
end;
/
--end--
--Question SN 67--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'baby_moth_fath_bld_rel_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Are you and the baby''s father related, except by marriage? (e.g. first cousins)' where QUES_CODE = 'baby_moth_fath_bld_rel_ind';
commit;
  end if;
end;
/
--end--
--Question SN 68--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'preg_use_dnr_egg_sperm_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Did this pregnancy use either a donor egg or donor sperm?' where QUES_CODE = 'preg_use_dnr_egg_sperm_ind';
commit;
  end if;
end;
/
--end--
--Question SN 69--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'dnr_egg_sperm_fmhq_avail_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='If yes, is a family medical history available for the egg or sperm donor?' where QUES_CODE = 'dnr_egg_sperm_fmhq_avail_ind';
commit;
  end if;
end;
/
--end--
--Question SN 70--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'abn_prenat_test_rslt_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Ever had an abnormal prenatal test result (e.g. amniocentesis, blood test, ultrasound)?' where QUES_CODE = 'abn_prenat_test_rslt_ind';
commit;
  end if;
end;
/
--end--
--Question SN 71--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'abn_prenat_test_rslt_testname';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Which test was abnormal?' where QUES_CODE = 'abn_prenat_test_rslt_testname';
commit;
  end if;
end;
/
--end--
--Question SN 72--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'abn_prenat_test_abort_desc';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='What was the abnormal test result?' where QUES_CODE = 'abn_prenat_test_abort_desc';
commit;
  end if;
end;
/
--end--
--Question SN 73--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'abn_prenat_test_rslt_diag_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was a diagnosis made?' where QUES_CODE = 'abn_prenat_test_rslt_diag_ind';
commit;
  end if;
end;
/
--end--
--Question SN 74--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'abn_prenat_test_rslt_diag_desc';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='If yes, specify diagnosis:' where QUES_CODE = 'abn_prenat_test_rslt_diag_desc';
commit;
  end if;
end;
/
--end--
--Question SN 75--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'child_die_age_10_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Any die by age 10?' where QUES_CODE = 'child_die_age_10_ind';
commit;
  end if;
end;
/
--end--
--Question SN 76--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'child_die_age_10_desc';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='If yes, what was the cause?' where QUES_CODE = 'child_die_age_10_desc';
commit;
  end if;
end;
/
--end--
--Question SN 77--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'stillborn_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever had a stillborn child?' where QUES_CODE = 'stillborn_ind';
commit;
  end if;
end;
/
--end--
--Question SN 78--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'stillborn_cause';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='If yes, what was the cause?' where QUES_CODE = 'stillborn_cause';
commit;
  end if;
end;
/
--end--
--Question SN 79--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'can_leuk_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Cancer or leukemia' where QUES_CODE = 'can_leuk_ind';
commit;
  end if;
end;
/
--end--
--Question SN 80--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_cl_other_dises_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'inher_cl_other_dises_ind';
commit;
  end if;
end;
/
--end--
--Question SN 81--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cancer_leuk_rel_type1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Cancer (Baby''s Mother)' where QUES_CODE = 'cancer_leuk_rel_type1';
commit;
  end if;
end;
/
--end--
--Question SN 82--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cancer_leuk_rel_type_desc1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type:' where QUES_CODE = 'cancer_leuk_rel_type_desc1';
commit;
  end if;
end;
/
--end--
--Question SN 83--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cancer_leuk_rel_type2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Cancer (Baby''s Father)' where QUES_CODE = 'cancer_leuk_rel_type2';
commit;
  end if;
end;
/
--end--
--Question SN 84--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cancer_leuk_rel_type_desc2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type:' where QUES_CODE = 'cancer_leuk_rel_type_desc2';
commit;
  end if;
end;
/
--end--
--Question SN 85--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cancer_leuk_rel_type3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Cancer (Baby''s Sibling)' where QUES_CODE = 'cancer_leuk_rel_type3';
commit;
  end if;
end;
/
--end--
--Question SN 86--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cancer_leuk_rel_type_desc3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type:' where QUES_CODE = 'cancer_leuk_rel_type_desc3';
commit;
  end if;
end;
/
--end--
--Question SN 87--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_rbc_dises_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Red Blood Cell Diseases?' where QUES_CODE = 'inher_rbc_dises_ind';
commit;
  end if;
end;
/
--end--
--Question SN 88--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'red_bld_cell_dis_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'red_bld_cell_dis_rel';
commit;
  end if;
end;
/
--end--
--Question SN 89--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'red_bld_cell_dis_type1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Red Blood Cell Disease (Baby''s Mother)' where QUES_CODE = 'red_bld_cell_dis_type1';
commit;
  end if;
end;
/
--end--
--Question SN 90--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'red_bld_cell_dis_type2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Red Blood Cell Disease (Baby''s Father )' where QUES_CODE = 'red_bld_cell_dis_type2';
commit;
  end if;
end;
/
--end--
--Question SN 91--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'red_bld_cell_dis_type3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Red Blood Cell Disease (Baby''s Sibling )' where QUES_CODE = 'red_bld_cell_dis_type3';
commit;
  end if;
end;
/
--end--
--Question SN 92--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'red_bld_cell_dis_type4';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Red Blood Cell Disease (Baby''s Grandparent)' where QUES_CODE = 'red_bld_cell_dis_type4';
commit;
  end if;
end;
/
--end--
--Question SN 93--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'red_bld_cell_dis_type5';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Red Blood Cell Disease (Baby''s Mother''s Sibling)' where QUES_CODE = 'red_bld_cell_dis_type5';
commit;
  end if;
end;
/
--end--
--Question SN 94--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'red_bld_cell_dis_type6';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Red Blood Cell Disease (Baby''s Father''s Sibling)' where QUES_CODE = 'red_bld_cell_dis_type6';
commit;
  end if;
end;
/
--end--
--Question SN 95--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_wbc_dises_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='White Blood Cell Diseases?' where QUES_CODE = 'inher_wbc_dises_ind';
commit;
  end if;
end;
/
--end--
--Question SN 96--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'while_bld_cell_dis_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'while_bld_cell_dis_rel';
commit;
  end if;
end;
/
--end--
--Question SN 97--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'while_bld_cell_dis_type1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of White Blood Cell Disease (Baby''s Mother)' where QUES_CODE = 'while_bld_cell_dis_type1';
commit;
  end if;
end;
/
--end--
--Question SN 98--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'while_bld_cell_dis_type2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of White Blood Cell Disease (Baby''s Father)' where QUES_CODE = 'while_bld_cell_dis_type2';
commit;
  end if;
end;
/
--end--
--Question SN 99--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'while_bld_cell_dis_type3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of White Blood Cell Disease (Baby''s Sibling)' where QUES_CODE = 'while_bld_cell_dis_type3';
commit;
  end if;
end;
/
--end--
--Question SN 100--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'while_bld_cell_dis_type4';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of White Blood Cell Disease (Baby''s Grandparent)' where QUES_CODE = 'while_bld_cell_dis_type4';
commit;
  end if;
end;
/
--end--
--Question SN 101--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'while_bld_cell_dis_type5';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of White Blood Cell Disease (Baby''s Mother''s Sibling)' where QUES_CODE = 'while_bld_cell_dis_type5';
commit;
  end if;
end;
/
--end--
--Question SN 102--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'while_bld_cell_dis_type6';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of White Blood Cell Disease (Baby''s Father''s Sibling)' where QUES_CODE = 'while_bld_cell_dis_type6';
commit;
  end if;
end;
/
--end--
--Question SN 103--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_imm_dfc_disordr_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Immune Deficiencies?' where QUES_CODE = 'inher_imm_dfc_disordr_ind';
commit;
  end if;
end;
/
--end--
--Question SN 104--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'imune_def_ind_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'imune_def_ind_rel';
commit;
  end if;
end;
/
--end--
--Question SN 105--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'imune_def_ind_type1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Immune Deficiency (Baby''s Mother)' where QUES_CODE = 'imune_def_ind_type1';
commit;
  end if;
end;
/
--end--
--Question SN 106--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'imune_def_ind_type2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Immune Deficiency (Baby''s Father )' where QUES_CODE = 'imune_def_ind_type2';
commit;
  end if;
end;
/
--end--
--Question SN 107--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'imune_def_ind_type3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Immune Deficiency (Baby''s Sibling )' where QUES_CODE = 'imune_def_ind_type3';
commit;
  end if;
end;
/
--end--
--Question SN 108--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'imune_def_ind_type4';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Immune Deficiency (Baby''s Grandparent)' where QUES_CODE = 'imune_def_ind_type4';
commit;
  end if;
end;
/
--end--
--Question SN 109--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'imune_def_ind_type5';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Immune Deficiency (Baby''s Mother''s Sibling)' where QUES_CODE = 'imune_def_ind_type5';
commit;
  end if;
end;
/
--end--
--Question SN 110--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'imune_def_ind_type6';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Immune Deficiency (Baby''s Father''s Sibling)' where QUES_CODE = 'imune_def_ind_type6';
commit;
  end if;
end;
/
--end--
--Question SN 111--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_pltlt_dises_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Platelet Diseases?' where QUES_CODE = 'inher_pltlt_dises_ind';
commit;
  end if;
end;
/
--end--
--Question SN 112--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'platelet_dis_ind_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'platelet_dis_ind_rel';
commit;
  end if;
end;
/
--end--
--Question SN 113--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'platelet_dis_ind_type1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Platelet Disease (Baby''s Mother)' where QUES_CODE = 'platelet_dis_ind_type1';
commit;
  end if;
end;
/
--end--
--Question SN 114--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'platelet_dis_ind_type2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Platelet Disease (Baby''s Father)' where QUES_CODE = 'platelet_dis_ind_type2';
commit;
  end if;
end;
/
--end--
--Question SN 115--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'platelet_dis_ind_type3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Platelet Disease (Baby''s Sibling)' where QUES_CODE = 'platelet_dis_ind_type3';
commit;
  end if;
end;
/
--end--
--Question SN 116--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'platelet_dis_ind_type4';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Platelet Disease (Baby''s Grandparent)' where QUES_CODE = 'platelet_dis_ind_type4';
commit;
  end if;
end;
/
--end--
--Question SN 117--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'platelet_dis_ind_type5';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Platelet Disease (Baby''s Mother''s Sibling)' where QUES_CODE = 'platelet_dis_ind_type5';
commit;
  end if;
end;
/
--end--
--Question SN 118--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'platelet_dis_ind_type6';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Platelet Disease (Baby''s Father''s Sibling)' where QUES_CODE = 'platelet_dis_ind_type6';
commit;
  end if;
end;
/
--end--
--Question SN 119--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_bld_dises_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Other Blood Disease or Disorder?' where QUES_CODE = 'inher_other_bld_dises_ind';
commit;
  end if;
end;
/
--end--
--Question SN 120--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_bld_dises_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'inher_other_bld_dises_rel';
commit;
  end if;
end;
/
--end--
--Question SN 121--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_bld_dises_desc1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type: (Baby''s Mother)' where QUES_CODE = 'inher_other_bld_dises_desc1';
commit;
  end if;
end;
/
--end--
--Question SN 122--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_bld_dises_desc2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type: (Baby''s Father)' where QUES_CODE = 'inher_other_bld_dises_desc2';
commit;
  end if;
end;
/
--end--
--Question SN 123--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_bld_dises_desc3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type: (Baby''s Sibling)' where QUES_CODE = 'inher_other_bld_dises_desc3';
commit;
  end if;
end;
/
--end--
--Question SN 124--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_bld_dises_desc4';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type: (Baby''s Grandparent)' where QUES_CODE = 'inher_other_bld_dises_desc4';
commit;
  end if;
end;
/
--end--
--Question SN 125--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_bld_dises_desc5';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type: (Baby''s Mother''s Sibling)' where QUES_CODE = 'inher_other_bld_dises_desc5';
commit;
  end if;
end;
/
--end--
--Question SN 126--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_bld_dises_desc6';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type: (Baby''s Father''s Sibling)' where QUES_CODE = 'inher_other_bld_dises_desc6';
commit;
  end if;
end;
/
--end--
--Question SN 127--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_sickle_cell_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Sickle cell disease (sickle cell anemia / thalassemia)?' where QUES_CODE = 'inher_sickle_cell_ind';
commit;
  end if;
end;
/
--end--
--Question SN 128--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_sickle_cell_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'inher_sickle_cell_rel';
commit;
  end if;
end;
/
--end--
--Question SN 129--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_thalassemia_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Thalassemia?' where QUES_CODE = 'inher_thalassemia_ind';
commit;
  end if;
end;
/
--end--
--Question SN 130--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_thalassemia_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'inher_thalassemia_rel';
commit;
  end if;
end;
/
--end--
--Question SN 131--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Metabolic/Storage Disease?' where QUES_CODE = 'inher_met_stg_dises_ind';
commit;
  end if;
end;
/
--end--
--Question SN 132--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'inher_met_stg_dises_rel';
commit;
  end if;
end;
/
--end--
--Question SN 133--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_type1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Metabolic/Storage Disease (Baby''s Mother)' where QUES_CODE = 'inher_met_stg_dises_type1';
commit;
  end if;
end;
/
--end--
--Question SN 134--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_desc1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type:' where QUES_CODE = 'inher_met_stg_dises_desc1';
commit;
  end if;
end;
/
--end--
--Question SN 135--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_type2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Metabolic/Storage Disease (Baby''s Father)' where QUES_CODE = 'inher_met_stg_dises_type2';
commit;
  end if;
end;
/
--end--
--Question SN 136--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_desc2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type:' where QUES_CODE = 'inher_met_stg_dises_desc2';
commit;
  end if;
end;
/
--end--
--Question SN 137--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_type3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Metabolic/Storage Disease (Baby''s Sibling)' where QUES_CODE = 'inher_met_stg_dises_type3';
commit;
  end if;
end;
/
--end--
--Question SN 138--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_desc3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type:' where QUES_CODE = 'inher_met_stg_dises_desc3';
commit;
  end if;
end;
/
--end--
--Question SN 139--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_type4';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Metabolic/Storage Disease (Baby''s Grandparent)' where QUES_CODE = 'inher_met_stg_dises_type4';
commit;
  end if;
end;
/
--end--
--Question SN 140--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_desc4';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type:' where QUES_CODE = 'inher_met_stg_dises_desc4';
commit;
  end if;
end;
/
--end--
--Question SN 141--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_type5';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Metabolic/Storage Disease (Baby''s Mother''s Sibling)' where QUES_CODE = 'inher_met_stg_dises_type5';
commit;
  end if;
end;
/
--end--
--Question SN 142--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_desc5';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type:' where QUES_CODE = 'inher_met_stg_dises_desc5';
commit;
  end if;
end;
/
--end--
--Question SN 143--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_type6';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of Metabolic/Storage Disease (Baby''s Mother''s Sibling)' where QUES_CODE = 'inher_met_stg_dises_type6';
commit;
  end if;
end;
/
--end--
--Question SN 144--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_desc6';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type:' where QUES_CODE = 'inher_met_stg_dises_desc6';
commit;
  end if;
end;
/
--end--
--Question SN 145--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_aids_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='HIV/AIDS?' where QUES_CODE = 'hiv_aids_ind';
commit;
  end if;
end;
/
--end--
--Question SN 146--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_aids_ind_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'hiv_aids_ind_rel';
commit;
  end if;
end;
/
--end--
--Question SN 147--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'severe_aut_imm_sys_disordr_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Severe autoimmune disorder?' where QUES_CODE = 'severe_aut_imm_sys_disordr_ind';
commit;
  end if;
end;
/
--end--
--Question SN 148--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'severe_aut_imm_sys_disordr_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'severe_aut_imm_sys_disordr_rel';
commit;
  end if;
end;
/
--end--
--Question SN 149--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'severe_aut_imm_sys_disordr_type1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of severe autoimmune disorder (Baby''s Mother)' where QUES_CODE = 'severe_aut_imm_sys_disordr_type1';
commit;
  end if;
end;
/
--end--
--Question SN 150--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'severe_aut_imm_sys_disordr_type2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of severe autoimmune disorder (Baby''s Father)' where QUES_CODE = 'severe_aut_imm_sys_disordr_type2';
commit;
  end if;
end;
/
--end--
--Question SN 151--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'severe_aut_imm_sys_disordr_type3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Type of severe autoimmune disorder (Baby''s Sibling)' where QUES_CODE = 'severe_aut_imm_sys_disordr_type3';
commit;
  end if;
end;
/
--end--
--Question SN 152--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'oth_immune_dis';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Immune System Disorder (other or unknown type)?' where QUES_CODE = 'oth_immune_dis';
commit;
  end if;
end;
/
--end--
--Question SN 153--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'oth_immune_dis_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'oth_immune_dis_rel';
commit;
  end if;
end;
/
--end--
--Question SN 154--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'oth_immune_dis_desc1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type: (Baby''s Mother)' where QUES_CODE = 'oth_immune_dis_desc1';
commit;
  end if;
end;
/
--end--
--Question SN 155--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'oth_immune_dis_desc2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type: (Baby''s Father)' where QUES_CODE = 'oth_immune_dis_desc2';
commit;
  end if;
end;
/
--end--
--Question SN 156--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'oth_immune_dis_desc3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Specify type: (Baby''s Sibling)' where QUES_CODE = 'oth_immune_dis_desc3';
commit;
  end if;
end;
/
--end--
--Question SN 157--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chron_bld_tranf_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Chronic blood transfusions?' where QUES_CODE = 'chron_bld_tranf_ind';
commit;
  end if;
end;
/
--end--
--Question SN 158--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chron_bld_trans_ind_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'chron_bld_trans_ind_rel';
commit;
  end if;
end;
/
--end--
--Question SN 159--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hemolytic_anemia_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Hemolytic anemia?' where QUES_CODE = 'hemolytic_anemia_ind';
commit;
  end if;
end;
/
--end--
--Question SN 160--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hemolytic_anemia_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'hemolytic_anemia_rel';
commit;
  end if;
end;
/
--end--
--Question SN 161--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'spleen_removed_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Spleen removed to treat a blood disorder?' where QUES_CODE = 'spleen_removed_ind';
commit;
  end if;
end;
/
--end--
--Question SN 162--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'spleen_removed_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'spleen_removed_rel';
commit;
  end if;
end;
/
--end--
--Question SN 163--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'gallbladder_removed_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Gallbladder removed before age 30?' where QUES_CODE = 'gallbladder_removed_ind';
commit;
  end if;
end;
/
--end--
--Question SN 164--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'gallbladder_removed_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'gallbladder_removed_rel';
commit;
  end if;
end;
/
--end--
--Question SN 165--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cruetz_jakob_dis_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Creutzfeldt-Jakob disease (CJD)?' where QUES_CODE = 'cruetz_jakob_dis_ind';
commit;
  end if;
end;
/
--end--
--Question SN 166--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cruetz_jakob_dis_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'cruetz_jakob_dis_rel';
commit;
  end if;
end;
/
--end--
--Question SN 167--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Other serious or life-threatening diseases affecting the family?' where QUES_CODE = 'inher_other_dises_ind';
commit;
  end if;
end;
/
--end--
--Question SN 168--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relation to Baby' where QUES_CODE = 'inher_other_dises_rel';
commit;
  end if;
end;
/
--end--
--Question SN 169--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_desc1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Disease Name (Baby''s Mother)' where QUES_CODE = 'inher_other_dises_desc1';
commit;
  end if;
end;
/
--end--
--Question SN 170--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_desc2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Disease Name (Baby''s Father)' where QUES_CODE = 'inher_other_dises_desc2';
commit;
  end if;
end;
/
--end--
--Question SN 171--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_desc3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Disease Name (Baby''s Sibling)' where QUES_CODE = 'inher_other_dises_desc3';
commit;
  end if;
end;
/
--end--
--Question SN 172--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_desc4';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Disease Name (Baby''s Grandparent)' where QUES_CODE = 'inher_other_dises_desc4';
commit;
  end if;
end;
/
--end--
--Question SN 173--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_desc5';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Disease Name (Baby''s Mother''s Sibling)' where QUES_CODE = 'inher_other_dises_desc5';
commit;
  end if;
end;
/
--end--
--Question SN 174--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_desc6';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Disease Name (Baby''s Father''s Sibling)' where QUES_CODE = 'inher_other_dises_desc6';
commit;
  end if;
end;
/
--end--
--Question SN 175--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'answ_both_moth_fath_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In answering these questions, have you answered for both your family and the baby''s father''s family?' where QUES_CODE = 'answ_both_moth_fath_ind';
commit;
  end if;
end;
/
--end--
--Question SN 176--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'Hep_B_imm_glob_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Hepatitis B Immune Globulin in the last 12 months?' where QUES_CODE = 'Hep_B_imm_glob_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 177--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'evr_tkn_hmn_pit_gh_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Growth hormone from human pituitary glands ever?' where QUES_CODE = 'evr_tkn_hmn_pit_gh_ind';
commit;
  end if;
end;
/
--end--
--Question SN 178--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'shots_vacc_12wk_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 12 weeks, have you had any shots or vaccinations?' where QUES_CODE = 'shots_vacc_12wk_ind';
commit;
  end if;
end;
/
--end--
--Question SN 179--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'shots_vacc_12wk_desc';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='If yes, please describe:' where QUES_CODE = 'shots_vacc_12wk_desc';
commit;
  end if;
end;
/
--end--
--Question SN 180--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cntc_smallpox_vaccine_8wk_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 8 weeks, have you had contact with someone who has received the smallpox vaccine?' where QUES_CODE = 'cntc_smallpox_vaccine_8wk_ind';
commit;
  end if;
end;
/
--end--
--Question SN 181--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'pst_4wk_illness_symptom_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 4 months, have you experienced two or more of the following: a fever (>100.5�F or 38.0�C), headache, muscle weakness, skin rash on trunk of the body, swollen lymph glands?' where QUES_CODE = 'pst_4wk_illness_symptom_ind';
commit;
  end if;
end;
/
--end--
--Question SN 182--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'major_ill_srgry_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 12 months, have you had a major illness or surgery?' where QUES_CODE = 'major_ill_srgry_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 183--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'major_ill_desc';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='If yes, please describe:' where QUES_CODE = 'major_ill_desc';
commit;
  end if;
end;
/
--end--
--Question SN 184--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'bld_disease_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever had a bleeding condition or blood disease, including sickle cell disease?' where QUES_CODE = 'bld_disease_ind';
commit;
  end if;
end;
/
--end--
--Question SN 185--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'bld_prblm_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever had a bleeding problem, such as hemophilia or other clotting factor deficiencies, and received human-derived clotting factor concentrates?' where QUES_CODE = 'bld_prblm_ind';
commit;
  end if;
end;
/
--end--
--Question SN 186--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'west_nile_preg_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='During your pregnancy, have you been diagnosed with West Nile Virus or had a positive test for West Nile Virus?' where QUES_CODE = 'west_nile_preg_ind';
commit;
  end if;
end;
/
--end--
--Question SN 187--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'jaund_liver_hep_pos_tst_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever had yellow jaundice, liver disease, viral hepatitis, or a positive test (including screening tests) for hepatitis?' where QUES_CODE = 'jaund_liver_hep_pos_tst_ind';
commit;
  end if;
end;
/
--end--
--Question SN 188--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chagas_dises_babesiosis_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever had a parasitic blood disease such as Chagas disease or Babesiosis?' where QUES_CODE = 'chagas_dises_babesiosis_ind';
commit;
  end if;
end;
/
--end--
--Question SN 189--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cjd_diagnosis_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have any of your blood relatives ever been diagnosed with Creutzfeldt-Jakob Disease (CJD), or have you been told that your family has an increased risk for CJD?' where QUES_CODE = 'cjd_diagnosis_ind';
commit;
  end if;
end;
/
--end--
--Question SN 190--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'blood_transf_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 12 months, have you had a blood transfusion?' where QUES_CODE = 'blood_transf_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 191--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tx_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 12 months, have you had a transplant such as organ, bone marrow, stem cell, cornea, bone or other tissue?' where QUES_CODE = 'tx_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 192--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tissue_graft_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 12 months, have you had a tissue graft such as bone or skin?' where QUES_CODE = 'tissue_graft_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 193--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tattoo_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 12 months, have you had a tattoo?' where QUES_CODE = 'tattoo_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 194--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tattoo_shared_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Were shared instruments, needles or inks used for the tattoo?' where QUES_CODE = 'tattoo_shared_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 195--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'piercing_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 12 months, have you had an ear, skin, or body piercing using shared instruments or needles?' where QUES_CODE = 'piercing_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 196--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'acc_ns_stk_cntc_bld_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had an accidental needle stick or have you come into contact with someone else''s blood through an open wound (for example, a cut or sore), non-intact skin, or mucous membrane (for example, into your eye, mouth, etc)?' where QUES_CODE = 'acc_ns_stk_cntc_bld_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 197--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'had_treat_syph_gono_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had or been treated for a sexually transmitted disease, including syphilis?' where QUES_CODE = 'had_treat_syph_gono_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 198--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'give_mn_dr_pmt_for_sex_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 12 months have you given money, drugs, or other payment to anyone to have sex with you?' where QUES_CODE = 'give_mn_dr_pmt_for_sex_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 199--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_tk_mn_dr_pmt_sex_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 12 months have you had sex with anyone who has taken money, drugs, or other payment in exchange for sex in the past 5 years?' where QUES_CODE = 'sex_w_tk_mn_dr_pmt_sex_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 200--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_cntc_liv_jaund_hep_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 12 months, have you had sexual contact or lived with a person who has active or chronic viral hepatitis or yellow jaundice?' where QUES_CODE = 'sex_cntc_liv_jaund_hep_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 201--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_clotting_factor_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had sex, even once, with anyone who has taken human-derived clotting factors for a bleeding problem in the <u>past 5 years?</u>' where QUES_CODE = 'sex_w_clotting_factor_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 202--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'pst_5yr_tk_mn_dr_pmt_sex_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 5 years, have you received money, drugs, or other payment for sex?' where QUES_CODE = 'pst_5yr_tk_mn_dr_pmt_sex_ind';
commit;
  end if;
end;
/
--end--
--Question SN 203--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexpln_blue_prpl_spot';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Unexplained blue or purple spots on or under the skin or mucous membranes?' where QUES_CODE = 'unexpln_blue_prpl_spot';
commit;
  end if;
end;
/
--end--
--Question SN 204--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexpln_1m_lump_nk_apit_grn';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Lumps in your neck, armpits, or groin lasting longer than one month?' where QUES_CODE = 'unexpln_1m_lump_nk_apit_grn';
commit;
  end if;
end;
/
--end--
--Question SN 205--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'htlv_incl_screen_test_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever tested positive for HTLV (including screening tests)?' where QUES_CODE = 'htlv_incl_screen_test_ind';
commit;
  end if;
end;
/
--end--
--Question SN 206--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_contag_understand_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Do you understand that if you have the AIDS virus, you can give it to someone else even though you may feel well and have a negative AIDS test?' where QUES_CODE = 'hiv_contag_understand_ind';
commit;
  end if;
end;
/
--end--
--Question SN 207--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'live_travel_europe_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Since 1980, have you ever lived in or traveled to Europe?' where QUES_CODE = 'live_travel_europe_ind';
commit;
  end if;
end;
/
--end--
--Question SN 208--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'recv_transfusion_uk_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Since 1980, have you received a transfusion of blood or blood components while in the UK or France?' where QUES_CODE = 'recv_transfusion_uk_ind';
commit;
  end if;
end;
/
--end--
--Question SN 209--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'spent_europe_ge_5y_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Since 1980, have you spent time that adds up to 5 years or more in Europe, including time spent in the UK between 1980 and 1996?' where QUES_CODE = 'spent_europe_ge_5y_ind';
commit;
  end if;
end;
/
--end--
--Question SN 210--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'us_military_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='From 1980 through 1996, were you a member of the U.S. military, a civilian military employee, or a dependent of a member of the U.S. military?' where QUES_CODE = 'us_military_ind';
commit;
  end if;
end;
/
--end--
--Question SN 211--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'addendum_questions_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Were NMDP addendum questions asked?' where QUES_CODE = 'addendum_questions_ind';
commit;
  end if;
end;
/
--end--
--Question SN 212--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'pst_4mo_illness_symptom_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 4 months, have you experienced two or more of the following: a fever (>100.5�F or 38.0�C), headache, muscle weakness, skin rash on trunk of the body, swollen lymph glands?' where QUES_CODE = 'pst_4mo_illness_symptom_ind';
commit;
  end if;
end;
/
--end--
--Question SN 213--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'para_chagas_babesiosis_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you ever had a parasitic blood disease such as Chagas disease or Babesiosis?' where QUES_CODE = 'para_chagas_babesiosis_ind';
commit;
  end if;
end;
/
--end--
--Question SN 214--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cjd_diag_neuro_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Have you been diagnosed with Creutzfeldt-Jakob Disease (CJD) or variant CJD or do you have a degenerative neurological condition such as dementia where the cause has not been identified?' where QUES_CODE = 'cjd_diag_neuro_ind';
commit;
  end if;
end;
/
--end--
--Question SN 215--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tx_stemcell_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='In the past 12 months, have you had a transplant such as organ, bone marrow, stem cell, cornea, bone or other tissue?' where QUES_CODE = 'tx_stemcell_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 216--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'propecia_last_month_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Propecia� (finasteride) in the last month?' where QUES_CODE = 'propecia_last_month_ind';
commit;
  end if;
end;
/
--end--
--Question SN 217--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'accutane_last_month_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Accutane� (isotretinoin) in the last month?' where QUES_CODE = 'accutane_last_month_ind';
commit;
  end if;
end;
/
--end--
--Question SN 218--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'soriatane_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Soriatane� (acitretin) in the last 12 months?' where QUES_CODE = 'soriatane_12m_ind';
commit;
  end if;
end;
/
--end--
--Question SN 219--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'ever_tkn_tegison_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Tegison� (etretinate) ever?' where QUES_CODE = 'ever_tkn_tegison_ind';
commit;
  end if;
end;
/
--end--
--Question SN 220--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cms_cert_lab_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Were all screening tests performed in a CMS approved laboratory?' where QUES_CODE = 'cms_cert_lab_ind';
commit;
  end if;
end;
/
--end--
--Question SN 221--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'fda_lic_mfg_inst_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Were all screening tests performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instruction?' where QUES_CODE = 'fda_lic_mfg_inst_ind';
commit;
  end if;
end;
/
--end--
--Question SN 222--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hbsag_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='HBsAg' where QUES_CODE = 'hbsag_react_ind';
commit;
  end if;
end;
/
--end--
--Question SN 223--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hbsag_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'hbsag_date';
commit;
  end if;
end;
/
--end--
--Question SN 224--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hbsag_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'hbsag_cms';
commit;
  end if;
end;
/
--end--
--Question SN 225--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hbsag_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'hbsag_fda';
commit;
  end if;
end;
/
--end--
--Question SN 226--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hbc_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Anti-HBc' where QUES_CODE = 'anti_hbc_react_ind';
commit;
  end if;
end;
/
--end--
--Question SN 227--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hbc_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'anti_hbc_date';
commit;
  end if;
end;
/
--end--
--Question SN 228--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hbc_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'anti_hbc_cms';
commit;
  end if;
end;
/
--end--
--Question SN 229--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hbc_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'anti_hbc_fda';
commit;
  end if;
end;
/
--end--
--Question SN 230--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hcv_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Anti-HCV' where QUES_CODE = 'anti_hcv_react_ind';
commit;
  end if;
end;
/
--end--
--Question SN 231--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hcv_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'anti_hcv_date';
commit;
  end if;
end;
/
--end--
--Question SN 232--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hcv_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'anti_hcv_cms';
commit;
  end if;
end;
/
--end--
--Question SN 233--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hcv_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'anti_hcv_fda';
commit;
  end if;
end;
/
--end--
--Question SN 234--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_o_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Anti-HIV 1/2 + O' where QUES_CODE = 'anti_hiv_1_2_o_ind';
commit;
  end if;
end;
/
--end--
--Question SN 235--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_o_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'anti_hiv_1_2_o_date';
commit;
  end if;
end;
/
--end--
--Question SN 236--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_o_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'anti_hiv_1_2_o_cms';
commit;
  end if;
end;
/
--end--
--Question SN 237--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_o_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'anti_hiv_1_2_o_fda';
commit;
  end if;
end;
/
--end--
--Question SN 238--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Anti-HIV 1/2' where QUES_CODE = 'anti_hiv_1_2_ind';
commit;
  end if;
end;
/
--end--
--Question SN 239--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'anti_hiv_1_2_date';
commit;
  end if;
end;
/
--end--
--Question SN 240--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'anti_hiv_1_2_cms';
commit;
  end if;
end;
/
--end--
--Question SN 241--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_1_2_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'anti_hiv_1_2_fda';
commit;
  end if;
end;
/
--end--
--Question SN 242--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_htlv1_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Anti-HTLV I/II' where QUES_CODE = 'anti_htlv1_react_ind';
commit;
  end if;
end;
/
--end--
--Question SN 243--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_htlv1_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'anti_htlv1_date';
commit;
  end if;
end;
/
--end--
--Question SN 244--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_htlv1_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'anti_htlv1_cms';
commit;
  end if;
end;
/
--end--
--Question SN 245--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_htlv1_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'anti_htlv1_fda';
commit;
  end if;
end;
/
--end--
--Question SN 246--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_total_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Anti-CMV Total' where QUES_CODE = 'cmv_total_ind';
commit;
  end if;
end;
/
--end--
--Question SN 247--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_total_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'cmv_total_date';
commit;
  end if;
end;
/
--end--
--Question SN 248--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_total_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'cmv_total_cms';
commit;
  end if;
end;
/
--end--
--Question SN 249--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_total_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'cmv_total_fda';
commit;
  end if;
end;
/
--end--
--Question SN 250--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_1_hcv_hbv_mpx_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='NAT HIV-1 / HCV / HBV (MPX)' where QUES_CODE = 'nat_hiv_1_hcv_hbv_mpx_ind';
commit;
  end if;
end;
/
--end--
--Question SN 251--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_1_hcv_hbv_mpx_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'nat_hiv_1_hcv_hbv_mpx_date';
commit;
  end if;
end;
/
--end--
--Question SN 252--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_1_hcv_hbv_mpx_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'nat_hiv_1_hcv_hbv_mpx_cms';
commit;
  end if;
end;
/
--end--
--Question SN 253--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_1_hcv_hbv_mpx_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'nat_hiv_1_hcv_hbv_mpx_fda';
commit;
  end if;
end;
/
--end--
--Question SN 254--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='HIV-1 NAT' where QUES_CODE = 'nat_hiv_react_ind';
commit;
  end if;
end;
/
--end--
--Question SN 255--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'nat_hiv_react_date';
commit;
  end if;
end;
/
--end--
--Question SN 256--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_react_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'nat_hiv_react_cms';
commit;
  end if;
end;
/
--end--
--Question SN 257--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_react_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'nat_hiv_react_fda';
commit;
  end if;
end;
/
--end--
--Question SN 258--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_antigen_rslt';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='HIV p24 Ag' where QUES_CODE = 'hiv_antigen_rslt';
commit;
  end if;
end;
/
--end--
--Question SN 259--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_antigen_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'hiv_antigen_date';
commit;
  end if;
end;
/
--end--
--Question SN 260--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_antigen_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'hiv_antigen_cms';
commit;
  end if;
end;
/
--end--
--Question SN 261--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_antigen_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'hiv_antigen_fda';
commit;
  end if;
end;
/
--end--
--Question SN 262--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hcv_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='HCV NAT' where QUES_CODE = 'nat_hcv_react_ind';
commit;
  end if;
end;
/
--end--
--Question SN 263--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hcv_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'nat_hcv_react_date';
commit;
  end if;
end;
/
--end--
--Question SN 264--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hcv_react_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'nat_hcv_react_cms';
commit;
  end if;
end;
/
--end--
--Question SN 265--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hcv_react_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'nat_hcv_react_fda';
commit;
  end if;
end;
/
--end--
--Question SN 266--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hbv_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='HBV NAT' where QUES_CODE = 'nat_hbv_react_ind';
commit;
  end if;
end;
/
--end--
--Question SN 267--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hbv_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'nat_hbv_react_date';
commit;
  end if;
end;
/
--end--
--Question SN 268--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hbv_react_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'nat_hbv_react_cms';
commit;
  end if;
end;
/
--end--
--Question SN 269--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hbv_react_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'nat_hbv_react_fda';
commit;
  end if;
end;
/
--end--
--Question SN 270--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Syphilis' where QUES_CODE = 'syphilis_react_ind';
commit;
  end if;
end;
/
--end--
--Question SN 271--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'syphilis_react_date';
commit;
  end if;
end;
/
--end--
--Question SN 272--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_react_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'syphilis_react_cms';
commit;
  end if;
end;
/
--end--
--Question SN 273--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_react_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'syphilis_react_fda';
commit;
  end if;
end;
/
--end--
--Question SN 274--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_west_nile_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='WNV NAT' where QUES_CODE = 'nat_west_nile_react_ind';
commit;
  end if;
end;
/
--end--
--Question SN 275--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_west_nile_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'nat_west_nile_react_date';
commit;
  end if;
end;
/
--end--
--Question SN 276--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_west_nile_react_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'nat_west_nile_react_cms';
commit;
  end if;
end;
/
--end--

--Question SN 277--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_west_nile_react_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'nat_west_nile_react_fda';
commit;
  end if;
end;
/
--end--
--Question SN 278--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chagas_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Chagas' where QUES_CODE = 'chagas_react_ind';
commit;
  end if;
end;
/
--end--
--Question SN 279--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chagas_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'chagas_react_date';
commit;
  end if;
end;
/
--end--
--Question SN 280--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chagas_react_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'chagas_react_cms';
commit;
  end if;
end;
/
--end--
--Question SN 281--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chagas_react_fda';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed using FDA-licensed, approved, or cleared donor screening tests, in accordance with the manufacturer''s instructions?' where QUES_CODE = 'chagas_react_fda';
commit;
  end if;
end;
/
--end--
--Question SN 282--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_ct_sup_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Syphilis Confirmatory / Supplemental Test' where QUES_CODE = 'syphilis_ct_sup_react_ind';
commit;
  end if;
end;
/
--end--
--Question SN 283--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_ct_sup_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'syphilis_ct_sup_react_date';
commit;
  end if;
end;
/
--end--
--Question SN 284--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'syphilis_ct_sup_react_cms';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was this screening test performed in a CMS approved laboratory?' where QUES_CODE = 'syphilis_ct_sup_react_cms';
commit;
  end if;
end;
/
--end--
--Question SN 285--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'treponemal_ct_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was the syphilis confirmatory test performed using a FDA-cleared confirmatory test (FTA-ABS)?' where QUES_CODE = 'treponemal_ct_ind';
commit;
  end if;
end;
/
--end--

--Question SN 287--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_1_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'other_idm_1_react_date';
commit;
  end if;
end;
/
--end--

--Question SN 289--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_2_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'other_idm_2_react_date';
commit;
  end if;
end;
/
--end--

--Question SN 291--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_3_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'other_idm_3_react_date';
commit;
  end if;
end;
/
--end--

--Question SN 293--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_4_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'other_idm_4_react_date';
commit;
  end if;
end;
/
--end--

--Question SN 295--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_5_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'other_idm_5_react_date';
commit;
  end if;
end;
/
--end--

--Question SN 297--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'other_idm_6_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'other_idm_6_react_date';
commit;
  end if;
end;
/
--end--
--Question SN 298--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_igg_sts';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Anti-CMV (IgG or Total)' where QUES_CODE = 'cmv_igg_sts';
commit;
  end if;
end;
/
--end--
--Question SN 299--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_igg_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'cmv_igg_date';
commit;
  end if;
end;
/
--end--
--Question SN 300--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_igm_sts';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Anti-CMV (IgM)' where QUES_CODE = 'cmv_igm_sts';
commit;
  end if;
end;
/
--end--
--Question SN 301--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cmv_igm_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'cmv_igm_date';
commit;
  end if;
end;
/
--end--
--Question SN 302--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'stsyph_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='STS' where QUES_CODE = 'stsyph_react_ind';
commit;
  end if;
end;
/
--end--
--Question SN 303--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'stsyph_react_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'stsyph_react_date';
commit;
  end if;
end;
/
--end--
--Question SN 304--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_hcv_perfrmd_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='NAT Test Done for HIV-1/HCV' where QUES_CODE = 'nat_hiv_hcv_perfrmd_ind';
commit;
  end if;
end;
/
--end--
--Question SN 305--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_hcv_perfrmd_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'nat_hiv_hcv_perfrmd_date';
commit;
  end if;
end;
/
--end--
--Question SN 306--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hbsag_ct_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='HBsAg Neut' where QUES_CODE = 'hbsag_ct_ind';
commit;
  end if;
end;
/
--end--
--Question SN 307--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hbsag_ct_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'hbsag_ct_date';
commit;
  end if;
end;
/
--end--
--Question SN 308--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hcv_ct_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Anti-HCV' where QUES_CODE = 'anti_hcv_ct_ind';
commit;
  end if;
end;
/
--end--
--Question SN 309--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hcv_ct_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'anti_hcv_ct_date';
commit;
  end if;
end;
/
--end--
--Question SN 310--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_west_blot_cde';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Anti-HIV 1 Western Blot' where QUES_CODE = 'anti_hiv_west_blot_cde';
commit;
  end if;
end;
/
--end--
--Question SN 311--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hiv_west_blot_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'anti_hiv_west_blot_date';
commit;
  end if;
end;
/
--end--
--Question SN 312--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_antigen_neut_rslt';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='HIV p24 Antigen Neut' where QUES_CODE = 'hiv_antigen_neut_rslt';
commit;
  end if;
end;
/
--end--
--Question SN 313--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_antigen_neut_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'hiv_antigen_neut_date';
commit;
  end if;
end;
/
--end--
--Question SN 314--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_htlv_1_2_west_blot_cde';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Anti-HTLV I/II' where QUES_CODE = 'anti_htlv_1_2_west_blot_cde';
commit;
  end if;
end;
/
--end--
--Question SN 315--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_htlv_1_2_west_blot_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'anti_htlv_1_2_west_blot_date';
commit;
  end if;
end;
/
--end--
--Question SN 316--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'stsyph_ct_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='FTA ABS' where QUES_CODE = 'stsyph_ct_ind';
commit;
  end if;
end;
/
--end--
--Question SN 317--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'stsyph_ct_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'stsyph_ct_date';
commit;
  end if;
end;
/
--end--
--Question SN 318--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'ripa_ct_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='RIPA' where QUES_CODE = 'ripa_ct_ind';
commit;
  end if;
end;
/
--end--
--Question SN 319--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'ripa_ct_date';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Test Date' where QUES_CODE = 'ripa_ct_date';
commit;
  end if;
end;
/
--end--
--Question SN 320--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'fam_med_hist_cmplt';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Was a Family Medical History completed?' where QUES_CODE = 'fam_med_hist_cmplt';
commit;
  end if;
end;
/
--end--
--Question SN 321--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'response_med_hist_issue';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Are there any responses indicating a significant medical history issue?' where QUES_CODE = 'response_med_hist_issue';
commit;
  end if;
end;
/
--end--
--Question SN 322--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'response_med_hist_dis';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Biological Relations' where QUES_CODE = 'response_med_hist_dis';
commit;
  end if;
end;
/
--end--
--Question SN 323--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'response_med_hist_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Disease' where QUES_CODE = 'response_med_hist_rel';
commit;
  end if;
end;
/
--end--
--Question SN 324--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'unexplained_symptom_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='Do you have any of the following' where QUES_CODE = 'unexplained_symptom_ind';
commit;
  end if;
end;
/
--end--
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,182,8,'08_CB_QUESTIONS.sql',sysdate,'9.0.0 B#637-ET002');

commit;