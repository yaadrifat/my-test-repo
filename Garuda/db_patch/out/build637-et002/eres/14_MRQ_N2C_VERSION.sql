--Cb_QUESTION table--
--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'donate_bld_different_nme_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Ever donated, or attempted to donate, blood or cord blood using a different name here or anywhere else?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'donate_bld_different_nme_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'shots_vacc_4wk_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Had any shots or vaccinations in the past 4 weeks?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'shots_vacc_4wk_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cncr_cnvl_bld_prblm_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Ever had cancer, a blood disease, or a bleeding problem?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'cncr_cnvl_bld_prblm_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'jaund_liver_hepatitis_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Ever had yellow jaundice, liver disease, viral hepatitis, or a positive test for hepatitis?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'jaund_liver_hepatitis_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tegison_soriatane_psorsis_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Ever taken etretinate or acitretin for psoriasis?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'tegison_soriatane_psorsis_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'human_pit_grwth_hrmn_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Ever received human pituitary-derived growth hormone?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'human_pit_grwth_hrmn_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cjd_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Or any of your relatives ever had Creutzfeldt-Jakob Disease (CJD), or have you ever been told that your family is at an increased risk for CJD?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'cjd_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cntc_jaund_hep_gv_glb_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Had close contact with anyone who has active or chronic viral hepatitis, yellow jaundice, or have you been given Hepatitis B immune globulin (HBIG)?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'cntc_jaund_hep_gv_glb_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'transf_tx_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Received blood, or had an organ or tissue transplant or graft?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'transf_tx_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tatoo_ns_stk_pierce_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Had a tattoo, an accidental needle stick or non-sterile acupuncture, ear, skin, or body piercing?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'tatoo_ns_stk_pierce_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cntc_someone_else_bld_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Come in contact with someone else''s blood?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'cntc_someone_else_bld_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'pos_test_syphillis_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Had a positive test for syphilis?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'pos_test_syphillis_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'diag_syphillis_gonorrhea_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Been diagnosed with a sexually transmitted disease, including syphilis or gonorrhea?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'diag_syphillis_gonorrhea_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'given_mn_dr_for_sex_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Given money or drugs to anyone to have sex with you?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'given_mn_dr_for_sex_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_iv_drug_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Had sex, even once, with anyone who has taken money or drugs for sex?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'sex_w_iv_drug_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 22--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_taken_mn_dr_sex_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Had sex, even once, with anyone who has used a needle to take drugs not prescribed by a doctor?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'sex_w_taken_mn_dr_sex_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 23--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_man_sex_othr_man_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Had sex with a male who has had sex, even once, since 1977 with another male?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'sex_w_man_sex_othr_man_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 24--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_clotting_factor_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Had sex, even once, with anyone who has taken clotting factor concentrates for a bleeding problem such as hemophilia?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'sex_w_clotting_factor_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 25--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_aids_12m_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Had sex, even once, with anyone who has AIDS, or has had a positive test for the AIDS virus?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'sex_w_aids_12m_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 26--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'prison_jail_72hrs_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Been in jail or prison for more than 72 hours?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'prison_jail_72hrs_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 28--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'taken_mn_dr_for_sex_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'At any time since 1977, have you ever taken money or drugs for sex?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'taken_mn_dr_for_sex_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 29--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'iv_drug_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you ever used a needle, even once, to take drugs that were not prescribed for you by a doctor?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'iv_drug_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 30--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'clotting_factor_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you ever taken clotting factor concentrates for a bleeding problem, such as hemophilia?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'clotting_factor_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 31--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'aids_hiv_htlv_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Do you have AIDS, or have you had a positive test for HIV (the AIDS virus), or for HTLV?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'aids_hiv_htlv_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 32--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'give_cbu_test_hiv_aids_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Are you giving cord blood because you want to be tested for HIV or the AIDS virus?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'give_cbu_test_hiv_aids_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 34.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'african_country';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Name of Countries', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='txtbox'),'african_country',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 37--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inject_bovine_insulin_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you, at any time since 1980, injected yourself with bovine (beef) insulin?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'inject_bovine_insulin_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/

--Question 38--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'recv_xenotransplant_ind';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you received a xenotransplant that involves live cells, tissues or organs?', NULL, '1', '0', '1', '0', '0', NULL, '0', '1', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'),'recv_xenotransplant_ind',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--End--

--CB_question_grp--
--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='donate_bld_different_nme_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='donate_bld_different_nme_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='refused_bld_dnr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='refused_bld_dnr_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_srgry_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='major_ill_srgry_12m_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_4wk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='shots_vacc_4wk_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cncr_cnvl_bld_prblm_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cncr_cnvl_bld_prblm_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='jaund_liver_hepatitis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='jaund_liver_hepatitis_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_dises_babesiosis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='chagas_dises_babesiosis_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='malaria_antimalr_drug_3yr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='malaria_antimalr_drug_3yr_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tegison_soriatane_psorsis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='tegison_soriatane_psorsis_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='outside_us_or_canada_3yr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='outside_us_or_canada_3yr_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='human_pit_grwth_hrmn_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='human_pit_grwth_hrmn_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='dura_mater_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='dura_mater_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cjd_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cntc_jaund_hep_gv_glb_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cntc_jaund_hep_gv_glb_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='transf_tx_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='transf_tx_12m_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tatoo_ns_stk_pierce_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='tatoo_ns_stk_pierce_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cntc_someone_else_bld_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cntc_someone_else_bld_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pos_test_syphillis_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='pos_test_syphillis_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diag_syphillis_gonorrhea_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='diag_syphillis_gonorrhea_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='given_mn_dr_for_sex_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='given_mn_dr_for_sex_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind ')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_iv_drug_12m_ind '),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 22--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_taken_mn_dr_sex_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_taken_mn_dr_sex_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 23--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_man_sex_othr_man_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_man_sex_othr_man_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 24--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_clotting_factor_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_clotting_factor_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 25--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_aids_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_aids_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 26--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='prison_jail_72hrs_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='prison_jail_72hrs_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 27 QUES_CODE is not present--

--Question 28--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='taken_mn_dr_for_sex_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='taken_mn_dr_for_sex_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 29--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='iv_drug_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='iv_drug_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 30--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='clotting_factor_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='clotting_factor_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 31--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aids_hiv_htlv_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='aids_hiv_htlv_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 32--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='give_cbu_test_hiv_aids_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='give_cbu_test_hiv_aids_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 33--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_contag_understand_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_contag_understand_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 34--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='born_live_cam_afr_nig_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='born_live_cam_afr_nig_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 34.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='african_country')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='african_country'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 34.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='trav_ctry_rec_bld_or_med_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='trav_ctry_rec_bld_or_med_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 35--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_born_live_in_ctry_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_born_live_in_ctry_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 36 QUES_CODE is not present--
--Question 36.a QUES_CODE is not present--


--Question 37--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inject_bovine_insulin_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='inject_bovine_insulin_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 38--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_xenotransplant_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='recv_xenotransplant_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 39 QUES_CODE is not present--



--Question 40--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='live_travel_europe_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='live_travel_europe_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 41--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_uk_ge_3m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spent_uk_ge_3m_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 42--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind ')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='recv_transfusion_uk_ind '),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 43--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_europe_ge_5y_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spent_europe_ge_5y_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 44--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='us_military_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='us_military_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/


--Question 45--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_1_ge_6m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='mil_base_europe_1_ge_6m_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--End--
--cb_form_question--
--Question 1--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='donate_bld_different_nme_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='donate_bld_different_nme_ind'),null,null,null,'1',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 2--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='refused_bld_dnr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='refused_bld_dnr_ind'),null,null,null,'2',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 3--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_srgry_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='major_ill_srgry_12m_ind'),null,null,null,'3',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 4--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_4wk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='shots_vacc_4wk_ind'),null,null,null,'4',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 5--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cncr_cnvl_bld_prblm_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cncr_cnvl_bld_prblm_ind'),null,null,null,'5',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 6--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='jaund_liver_hepatitis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='jaund_liver_hepatitis_ind'),null,null,null,'6',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 7--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_dises_babesiosis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_dises_babesiosis_ind'),null,null,null,'7',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 8--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='malaria_antimalr_drug_3yr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='malaria_antimalr_drug_3yr_ind'),null,null,null,'8',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 9--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tegison_soriatane_psorsis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tegison_soriatane_psorsis_ind'),null,null,null,'9',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 10--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='outside_us_or_canada_3yr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='outside_us_or_canada_3yr_ind'),null,null,null,'10',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 11--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='human_pit_grwth_hrmn_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='human_pit_grwth_hrmn_ind'),null,null,null,'11',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 12--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='dura_mater_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='dura_mater_ind'),null,null,null,'12',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 13--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_ind'),null,null,null,'13',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 14--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cntc_jaund_hep_gv_glb_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cntc_jaund_hep_gv_glb_12m_ind'),null,null,null,'14',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 15--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='transf_tx_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='transf_tx_12m_ind'),null,null,null,'15',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tatoo_ns_stk_pierce_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tatoo_ns_stk_pierce_12m_ind'),null,null,null,'16',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cntc_someone_else_bld_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cntc_someone_else_bld_12m_ind'),null,null,null,'17',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pos_test_syphillis_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pos_test_syphillis_12m_ind'),null,null,null,'18',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diag_syphillis_gonorrhea_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diag_syphillis_gonorrhea_12m_ind'),null,null,null,'19',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='given_mn_dr_for_sex_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='given_mn_dr_for_sex_12m_ind'),null,null,null,'20',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind ')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind '),null,null,null,'21',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 22--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_taken_mn_dr_sex_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_taken_mn_dr_sex_12m_ind'),null,null,null,'22',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 23--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_man_sex_othr_man_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_man_sex_othr_man_12m_ind'),null,null,null,'23',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 24--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_clotting_factor_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_clotting_factor_12m_ind'),null,null,null,'24',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 25--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_aids_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_aids_12m_ind'),null,null,null,'25',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 26--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='prison_jail_72hrs_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='prison_jail_72hrs_ind'),null,null,null,'26',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 27 QUES_CODE is not present--


--Question 28--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='taken_mn_dr_for_sex_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='taken_mn_dr_for_sex_ind'),null,null,null,'28',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 29--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='iv_drug_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='iv_drug_ind'),null,null,null,'29',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 30--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='clotting_factor_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='clotting_factor_ind'),null,null,null,'30',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 31--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aids_hiv_htlv_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aids_hiv_htlv_ind'),null,null,null,'31',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 32--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='give_cbu_test_hiv_aids_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='give_cbu_test_hiv_aids_ind'),null,null,null,'32',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 33--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_contag_understand_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_contag_understand_ind'),null,null,null,'33',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 34--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='born_live_cam_afr_nig_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='born_live_cam_afr_nig_ind'),null,null,null,'34',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 34.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='african_country')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='african_country'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='born_live_cam_afr_nig_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='born_live_cam_afr_nig_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='mrq_res2' and codelst_subtyp='yes'),'34.a',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 34.b--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='trav_ctry_rec_bld_or_med_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='trav_ctry_rec_bld_or_med_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='born_live_cam_afr_nig_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='born_live_cam_afr_nig_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='mrq_res2' and codelst_subtyp='yes'),'34.b',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 35--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_born_live_in_ctry_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_born_live_in_ctry_ind'),null,null,null,'35',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 36 QUES_CODE is not present--
--Question 36.a QUES_CODE is not present--

--Question 37--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inject_bovine_insulin_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inject_bovine_insulin_ind'),null,null,null,'37',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 38--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_xenotransplant_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_xenotransplant_ind'),null,null,null,'38',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 39 QUES_CODE is not present--


--Question 40--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='live_travel_europe_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='live_travel_europe_ind'),null,null,null,'40',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 41--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_uk_ge_3m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_uk_ge_3m_ind'),null,null,null,'41',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 42--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind ')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind '),null,null,null,'42',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 43--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_europe_ge_5y_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_europe_ge_5y_ind'),null,null,null,'43',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 44--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='us_military_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='us_military_ind'),null,null,null,'44',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 45--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_1_ge_6m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_1_ge_6m_ind'),null,null,null,'45',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--End--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,182,14,'14_MRQ_N2C_VERSION.sql',sysdate,'9.0.0 B#637-ET002');

commit;