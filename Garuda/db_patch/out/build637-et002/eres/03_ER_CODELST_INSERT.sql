--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='entity_type'
    AND CODELST_SUBTYP = 'PATIENT';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'entity_type','PATIENT','PATIENT','N',NULL,SYSDATE,SYSDATE,NULL,4);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='entity_type'
    AND CODELST_SUBTYP = 'ORDER_HEADER';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'entity_type','ORDER_HEADER','ORDER_HEADER','N',NULL,SYSDATE,SYSDATE,NULL,1);
	commit;
  end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,182,3,'03_ER_CODELST_INSERT.sql',sysdate,'9.0.0 B#637-ET002');

commit;