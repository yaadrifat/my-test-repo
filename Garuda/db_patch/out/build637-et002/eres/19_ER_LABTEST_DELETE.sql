--STARTS DELETING RECORD INTO ER_LABTEST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME = 'MonoFrozenNucleatedCellCount'
    AND LABTEST_SHORTNAME = 'MONO_FRZ';
  if (v_record_exists = 1) then
     delete from ER_LABTEST where LABTEST_NAME='MonoFrozenNucleatedCellCount' and LABTEST_SHORTNAME='MONO_FRZ';
	commit;
  end if;
end;
/
----END

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,182,19,'19_ER_LABTEST_DELETE.sql',sysdate,'9.0.0 B#637-ET002');

commit;