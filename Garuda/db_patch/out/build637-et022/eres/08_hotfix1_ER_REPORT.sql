set define off;
delete er_repxsl where pk_repxsl in (161,172,178,179,185);
commit;



----UPDATE WIDGET MRQ SQL------


Update ER_REPORT set REP_SQL = 'select f_get_dynamicformdata(~1,''~2'') MRQ,f_get_dynamicformgrp(''~2'') GRP,cbb_id siteid,cbbid sitename,cord_registry_id regid,CORD_ID_NUMBER_ON_CBU_BAG,cord_local_cbu_id LOCALCBUID,cord_isbi_din_code ISTBTDIN,f_cord_add_ids(~1) ADDIDS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hlt_his_scrn''),''1'') hsh_notes,f_get_attachments(~1,f_codelst_id(''doc_categ'',''hlth_scren'')) MRQ_ATTACH,lic_status licstatus, f_get_patientid(~1) PATIENT_ID from rep_cbu_details where pk_cord=~1' where pk_report = 178;
/
	COMMIT;


	Update ER_REPORT set REP_SQL_CLOB = 'select f_get_dynamicformdata(~1,''~2'') MRQ,f_get_dynamicformgrp(''~2'') GRP,cbb_id siteid,cbbid sitename,cord_registry_id regid,CORD_ID_NUMBER_ON_CBU_BAG,cord_local_cbu_id LOCALCBUID,cord_isbi_din_code ISTBTDIN,f_cord_add_ids(~1) ADDIDS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hlt_his_scrn''),''1'') hsh_notes,f_get_attachments(~1,f_codelst_id(''doc_categ'',''hlth_scren'')) MRQ_ATTACH,lic_status licstatus, f_get_patientid(~1) PATIENT_ID from rep_cbu_details where pk_cord=~1' where pk_report = 178;
/
	COMMIT;




----UPDATE WIDGET FMHQ SQL------


Update ER_REPORT set REP_SQL = 'select f_get_dynamicformdata(~1,''~2'') FMHQ,f_get_dynamicformgrp(''~2'') FMHQ_GRP,f_get_attachments(~1,f_codelst_id(''doc_categ'',''hlth_scren'')) FMHQ_ATTACH,cbb_id siteid,cbbid sitename,cord_registry_id regid,CORD_ID_NUMBER_ON_CBU_BAG,
lic_status licstatus,cord_local_cbu_id LOCALCBUID,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hlt_his_scrn''),''1'') hsh_notes,cord_isbi_din_code ISTBTDIN,f_cord_add_ids(~1) ADDIDS, f_get_patientid(~1) PATIENT_ID from rep_cbu_details where pk_cord=~1' where pk_report = 179;
/
	COMMIT;


	Update ER_REPORT set REP_SQL_CLOB = 'select f_get_dynamicformdata(~1,''~2'') FMHQ,f_get_dynamicformgrp(''~2'') FMHQ_GRP,f_get_attachments(~1,f_codelst_id(''doc_categ'',''hlth_scren'')) FMHQ_ATTACH,cbb_id siteid,cbbid sitename,cord_registry_id regid,CORD_ID_NUMBER_ON_CBU_BAG,
lic_status licstatus,cord_local_cbu_id LOCALCBUID,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hlt_his_scrn''),''1'') hsh_notes,cord_isbi_din_code ISTBTDIN,f_cord_add_ids(~1) ADDIDS, f_get_patientid(~1) PATIENT_ID from rep_cbu_details where pk_cord=~1' where pk_report = 179;
/
	COMMIT;



----UPDATE IDM SHIPMENT PACKET SQL------


Update ER_REPORT set REP_SQL = 'SELECT  CRD.CORD_ISBI_DIN_CODE ISTBTDIN,CRD.CORD_REGISTRY_ID REGID,CRD.CORD_LOCAL_CBU_ID LOCALCBUID,F_CORD_ADD_IDS(~1) ADDIDS,CORD_ID_NUMBER_ON_CBU_BAG,F_GET_PATIENTID(~1) PATIENTID,
SITE.SITE_NAME SITENAME,SITE.SITE_ID SITEID,f_get_dynamicformdata(~1,''~2'') IDM,F_GET_FORM_TYP_DT(~1,''~2'') IDM_FORM_DATA,f_get_dynamicformgrp(''~2'') IDM_GRP FROM CB_CORD CRD, er_site site WHERE
CRD.FK_CBB_ID=SITE.PK_SITE AND CRD.PK_CORD=~1' where pk_report = 172;
/
	COMMIT;


	Update ER_REPORT set REP_SQL_CLOB = 'SELECT  CRD.CORD_ISBI_DIN_CODE ISTBTDIN,CRD.CORD_REGISTRY_ID REGID,CRD.CORD_LOCAL_CBU_ID LOCALCBUID,F_CORD_ADD_IDS(~1) ADDIDS,CORD_ID_NUMBER_ON_CBU_BAG,F_GET_PATIENTID(~1) PATIENTID,
SITE.SITE_NAME SITENAME,SITE.SITE_ID SITEID,f_get_dynamicformdata(~1,''~2'') IDM,F_GET_FORM_TYP_DT(~1,''~2'') IDM_FORM_DATA,f_get_dynamicformgrp(''~2'') IDM_GRP FROM CB_CORD CRD, er_site site WHERE
CRD.FK_CBB_ID=SITE.PK_SITE AND CRD.PK_CORD=~1' where pk_report = 172;
/
	COMMIT;





		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,221,8,'08_hotfix1_ER_REPORT.sql',sysdate,'9.0.0 B#637-ET022.01');
		commit;
