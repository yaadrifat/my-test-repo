----CREATING IDM WIDGET FORM------

delete er_repxsl where pk_repxsl=172;
commit;

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_report
    where pk_report = 186;
  if (v_record_exists = 0) then
   Insert into ER_REPORT 				       (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) values (186,'MRQ For DUMN','MRQ For DUMN','',0,null,null,'Y',null,null,1,'rep_cbu','',null,null);

COMMIT;	

	Update ER_REPORT set REP_SQL = 'select f_get_dynamicformdata(~1,''~2'') MRQ,f_get_dynamicformgrp(''~2'') GRP,cbb_id siteid,cbbid sitename,cord_registry_id regid,CORD_ID_NUMBER_ON_CBU_BAG,cord_local_cbu_id LOCALCBUID,cord_isbi_din_code ISTBTDIN,f_cord_add_ids(~1) ADDIDS,lic_status licstatus, f_get_patientid(~1) PATIENT_ID,F_GET_FORM_TYP_DT(~1,''~2'') MRQ_FORM_DATA from rep_cbu_details where pk_cord=~1' where pk_report = 186;
	COMMIT;


	Update ER_REPORT set REP_SQL_CLOB = 'select f_get_dynamicformdata(~1,''~2'') MRQ,f_get_dynamicformgrp(''~2'') GRP,cbb_id siteid,cbbid sitename,cord_registry_id regid,CORD_ID_NUMBER_ON_CBU_BAG,cord_local_cbu_id LOCALCBUID,cord_isbi_din_code ISTBTDIN,f_cord_add_ids(~1) ADDIDS,lic_status licstatus, f_get_patientid(~1) PATIENT_ID,F_GET_FORM_TYP_DT(~1,''~2'') MRQ_FORM_DATA from rep_cbu_details where pk_cord=~1' where pk_report = 186;
	COMMIT;

  end if;
end;
/
--END--
		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,221,19,'19_hotfix2_er_report.sql',sysdate,'9.0.0 B#637-ET022.02');
		commit;
