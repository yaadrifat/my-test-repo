--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'IN'
    AND CODE_TYPE= 'Response';
  if (v_record_exists = 1) then
      UPDATE 
  CB_CBU_STATUS SET CBU_STATUS_DESC='Cord Infused' where CBU_STATUS_CODE = 'IN'
    AND CODE_TYPE= 'Response';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'SO'
    AND CODE_TYPE!= 'Response';
  if (v_record_exists = 2) then
      UPDATE 
  CB_CBU_STATUS SET CBU_STATUS_DESC='Cord Shipped for Other (SO)' where CBU_STATUS_CODE = 'SO'
    AND CODE_TYPE!= 'Response';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'PC'
    AND CODE_TYPE= 'Response';
  if (v_record_exists = 1) then
      UPDATE 
  CB_CBU_STATUS SET CBU_STATUS_DESC='Patient Condition Changed, No Infusion' where CBU_STATUS_CODE = 'PC' AND CODE_TYPE= 'Response';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'QR'
    AND CODE_TYPE= 'Response';
  if (v_record_exists = 1) then
      UPDATE 
  CB_CBU_STATUS SET CBU_STATUS_DESC='Cord quarantined' where CBU_STATUS_CODE = 'QR' AND CODE_TYPE= 'Response';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'SA'
    AND CODE_TYPE= 'Response';
  if (v_record_exists = 1) then
      UPDATE 
  CB_CBU_STATUS SET CBU_STATUS_DESC='Patient Not Ready to Proceed' where CBU_STATUS_CODE = 'SA' AND CODE_TYPE= 'Response';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'SD'
    AND CODE_TYPE= 'Response';
  if (v_record_exists = 1) then
      UPDATE 
  CB_CBU_STATUS SET CBU_STATUS_DESC='Patient Died' where CBU_STATUS_CODE = 'SD' AND CODE_TYPE= 'Response';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'XP'
    AND CODE_TYPE= 'Response';
  if (v_record_exists = 1) then
      UPDATE 
  CB_CBU_STATUS SET CBU_STATUS_DESC='Cord Expired' where CBU_STATUS_CODE = 'XP' AND CODE_TYPE= 'Response';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'CA'
    AND CODE_TYPE= 'Response';
  if (v_record_exists = 1) then
      UPDATE 
  CB_CBU_STATUS SET CBU_STATUS_DESC='Cancel Request' where CBU_STATUS_CODE = 'CA' AND CODE_TYPE= 'Response';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'CB'
    AND CODE_TYPE= 'Response';
  if (v_record_exists = 1) then
      UPDATE 
  CB_CBU_STATUS SET CBU_STATUS_DESC='Infused Date Passed' where CBU_STATUS_CODE = 'CB' AND CODE_TYPE= 'Response';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'NR'
    AND CODE_TYPE= 'Response';
  if (v_record_exists = 1) then
      UPDATE 
  CB_CBU_STATUS SET CBU_STATUS_DESC='No Results Received - lab has not reported results in 30 days' where CBU_STATUS_CODE = 'NR' AND CODE_TYPE= 'Response';
	commit;
  end if;
end;
/
--END--

		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,221,17,'17_hotfix2_CB_CBU_STATUS_UPDATE.sql',sysdate,'9.0.0 B#637-ET022.02');
		commit;
