--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'ship_cont'
    AND codelst_subtyp = 'dry_ship';
  if (v_record_exists = 1) then
      UPDATE 
  ER_CODELST SET CODELST_DESC='Dry Shipper' where codelst_type = 'ship_cont'
    AND codelst_subtyp = 'dry_ship';
	commit;
  end if;
end;
/
--END--
		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,221,7,'07_hotfix1_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 B#637-ET022.01');
		commit;

