--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_40')
    AND COLUMN_NAME = 'FK_ORDER_RESOL_BY_CBB';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_40'),'PRE','(','ER_ORDER','FK_ORDER_RESOL_BY_CBB','(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''PC'')',' IN','OR',NULL,'PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_40')
    AND COLUMN_NAME = 'FK_ORDER_RESOL_BY_TC';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_40'),'PRE',NULL,'ER_ORDER','FK_ORDER_RESOL_BY_TC','(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''PC'')',' IN',NULL,')','PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_41')
    AND COLUMN_NAME = 'FK_ORDER_RESOL_BY_CBB';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_41'),'PRE','(','ER_ORDER','FK_ORDER_RESOL_BY_CBB','(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''PD'')',' IN','OR',NULL,'PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_41')
    AND COLUMN_NAME = 'FK_ORDER_RESOL_BY_TC';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_41'),'PRE',NULL,'ER_ORDER','FK_ORDER_RESOL_BY_TC','(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''PD'')',' IN',NULL,')','PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_42')
    AND COLUMN_NAME = 'FK_ORDER_RESOL_BY_CBB';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_42'),'PRE','(','ER_ORDER','FK_ORDER_RESOL_BY_CBB','(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''TC'')',' IN','OR',NULL,'PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_42')
    AND COLUMN_NAME = 'FK_ORDER_RESOL_BY_TC';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_42'),'PRE',NULL,'ER_ORDER','FK_ORDER_RESOL_BY_TC','(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''TC'')',' IN',NULL,')','PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--



--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_43')
    AND COLUMN_NAME = 'FK_ORDER_RESOL_BY_CBB';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_43'),'PRE','(','ER_ORDER','FK_ORDER_RESOL_BY_CBB','(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''TS'')',' IN','OR',NULL,'PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_43')
    AND COLUMN_NAME = 'FK_ORDER_RESOL_BY_TC';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_43'),'PRE',NULL,'ER_ORDER','FK_ORDER_RESOL_BY_TC','(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''TS'')',' IN',NULL,')','PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--

		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,221,14,'14_hotfix2_CB_ALERT_COND_INSERT.sql',sysdate,'9.0.0 B#637-ET022.02');
		commit;

