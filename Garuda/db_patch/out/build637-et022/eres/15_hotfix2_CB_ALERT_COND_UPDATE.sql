--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_31')
    AND CONDITION_VALUE = '(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''CT'')';
  if (v_record_exists = 2) then
      UPDATE 
  CB_ALERT_CONDITIONS SET CONDITION_VALUE='(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''DS'')' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_31')
    AND CONDITION_VALUE = '(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''CT'')';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_32')
    AND CONDITION_VALUE = '(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''HE'')';
  if (v_record_exists = 2) then
      UPDATE 
  CB_ALERT_CONDITIONS SET CONDITION_VALUE='(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''DU'')' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_32')
    AND CONDITION_VALUE = '(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''HE'')';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_34')
    AND CONDITION_VALUE = '(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''OR'')';
  if (v_record_exists = 2) then
      UPDATE 
  CB_ALERT_CONDITIONS SET CONDITION_VALUE='(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''LS'')' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_34')
    AND CONDITION_VALUE = '(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''OR'')';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_05')
    AND TABLE_NAME = 'TO_DATE(TO_CHAR(ER_ORDER_HEADER';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET TABLE_NAME='TO_DATE(TO_CHAR(CB_SHIPMENT',COLUMN_NAME='SCH_SHIPMENT_DATE,''Mon DD, YYYY''),''Mon DD, YYYY'')-TO_DATE(TO_CHAR(SYSDATE,''Mon DD, YYYY''),''Mon DD, YYYY'')',CONDITION_VALUE='2',ARITHMETIC_OPERATOR='=' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_05')
    AND TABLE_NAME = 'TO_DATE(TO_CHAR(ER_ORDER_HEADER';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_07')
    AND TABLE_NAME = 'TO_DATE(TO_CHAR(ER_ORDER_HEADER';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET TABLE_NAME='TO_DATE(TO_CHAR(CB_SHIPMENT',COLUMN_NAME='SCH_SHIPMENT_DATE,''Mon DD, YYYY''),''Mon DD, YYYY'')-TO_DATE(TO_CHAR(SYSDATE,''Mon DD, YYYY''),''Mon DD, YYYY'')' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_07')
    AND TABLE_NAME = 'TO_DATE(TO_CHAR(ER_ORDER_HEADER';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_08')
    AND TABLE_NAME = 'TO_DATE(TO_CHAR(ER_ORDER_HEADER';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET TABLE_NAME='TO_DATE(TO_CHAR(CB_SHIPMENT',COLUMN_NAME='SCH_SHIPMENT_DATE,''Mon DD, YYYY''),''Mon DD, YYYY'')-TO_DATE(TO_CHAR(SYSDATE,''Mon DD, YYYY''),''Mon DD, YYYY'')' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_08')
    AND TABLE_NAME = 'TO_DATE(TO_CHAR(ER_ORDER_HEADER';
	commit;
  end if;
end;
/
--END--

		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,221,15,'15_hotfix2_CB_ALERT_COND_UPDATE.sql',sysdate,'9.0.0 B#637-ET022.02');
		commit;