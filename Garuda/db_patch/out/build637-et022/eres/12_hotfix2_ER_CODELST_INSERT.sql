DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where CODELST_TYPE='specimen_type'
	 AND CODELST_SUBTYP = 'cbu';
  if (v_record_exists = 1) then
	delete from ER_CODELST where CODELST_TYPE='specimen_type' and CODELST_SUBTYP='cbu';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where CODELST_TYPE='specimen_type'
	 AND CODELST_SUBTYP = 'CBU';
  if (v_record_exists = 1) then
	delete from ER_CODELST where CODELST_TYPE='specimen_type' and CODELST_SUBTYP='CBU';
	commit;
  end if;
end;
/


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='specimen_type'
    AND CODELST_SUBTYP = 'cbu';
  if (v_record_exists = 0) then
      INSERT INTO 
		ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
		CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
		CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
		(SEQ_ER_CODELST.nextval,null,'specimen_type','cbu','CBU','N',2,'Y',null,
		null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='fund_cat_code'
    AND CODELST_SUBTYP = 'f10';
  if (v_record_exists = 0) then
      INSERT INTO 
		ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
		CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
		CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
		(SEQ_ER_CODELST.nextval,null,'fund_cat_code','f10','F10','N',12,'Y',null,
		null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--




--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_40';
  if (v_record_exists = 0) then
     INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_CUSTOM_COL2) VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_40','Patient Condition Changed, No Infusion (PC)','N',12,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)','<font face="verdana"><table><tr><td align="left"><b><111></b></td><td>:</td><td align="left"><222></td></tr><tr><td align="left">CBB ID</td><td>:</td><td align="left"><333></td></tr><tr><td align="left">CBU Registry ID</td><td>:</td><td align="left"><123></td></tr><tr><td align="left">CBU Local ID</td><td>:</td><td align="left"><666></td></tr><tr><td align="left">Request Type</td><td>:</td><td align="left"><444></td></tr><tr><td align="left">Request Date</td><td>:</td><td align="left"><777></td></tr><tr><td align="left">Alert Date</td><td>:</td><td align="left"><555></td></tr></table><br><br><p><i>This automated EmTrax communication has been sent to you as per your alert preferences. If you wish to discontinue these alerts, please access the alert management section of your profile in EmTrax.</i></p></font><table><tr><td><img alt="" src="http://marrow.org/images/email/nmdp_80.png"></td><td><img alt="" src="http://marrow.org/images/email/btm_50.png"></td></tr></table><br><hr><p><font face="Arial"><i>***This is an auto-generated e-mail***</p><p>This e-mail address is not monitored; please do not respond to this message.</i></p><br><p><i>The sender of this e-mail provides services for the NMDP and/or Be The Match pursuant to agreement and may not be authorized to bind the organization with any information, material, or content contained within this e-mail.</i></font></p>');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_41';
  if (v_record_exists = 0) then
     INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_CUSTOM_COL2) VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_41','Patient Died, No Infusion (PD)','N',13,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)','<font face="verdana"><table><tr><td align="left"><b><111></b></td><td>:</td><td align="left"><222></td></tr><tr><td align="left">CBB ID</td><td>:</td><td align="left"><333></td></tr><tr><td align="left">CBU Registry ID</td><td>:</td><td align="left"><123></td></tr><tr><td align="left">CBU Local ID</td><td>:</td><td align="left"><666></td></tr><tr><td align="left">Request Type</td><td>:</td><td align="left"><444></td></tr><tr><td align="left">Request Date</td><td>:</td><td align="left"><777></td></tr><tr><td align="left">Alert Date</td><td>:</td><td align="left"><555></td></tr></table><br><br><p><i>This automated EmTrax communication has been sent to you as per your alert preferences. If you wish to discontinue these alerts, please access the alert management section of your profile in EmTrax.</i></p></font><table><tr><td><img alt="" src="http://marrow.org/images/email/nmdp_80.png"></td><td><img alt="" src="http://marrow.org/images/email/btm_50.png"></td></tr></table><br><hr><p><font face="Arial"><i>***This is an auto-generated e-mail***</p><p>This e-mail address is not monitored; please do not respond to this message.</i></p><br><p><i>The sender of this e-mail provides services for the NMDP and/or Be The Match pursuant to agreement and may not be authorized to bind the organization with any information, material, or content contained within this e-mail.</i></font></p>');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_42';
  if (v_record_exists = 0) then
     INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_CUSTOM_COL2) VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_42','TC Decided Against Infusion (TC)','N',14,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)','<font face="verdana"><table><tr><td align="left"><b><111></b></td><td>:</td><td align="left"><222></td></tr><tr><td align="left">CBB ID</td><td>:</td><td align="left"><333></td></tr><tr><td align="left">CBU Registry ID</td><td>:</td><td align="left"><123></td></tr><tr><td align="left">CBU Local ID</td><td>:</td><td align="left"><666></td></tr><tr><td align="left">Request Type</td><td>:</td><td align="left"><444></td></tr><tr><td align="left">Request Date</td><td>:</td><td align="left"><777></td></tr><tr><td align="left">Alert Date</td><td>:</td><td align="left"><555></td></tr></table><br><br><p><i>This automated EmTrax communication has been sent to you as per your alert preferences. If you wish to discontinue these alerts, please access the alert management section of your profile in EmTrax.</i></p></font><table><tr><td><img alt="" src="http://marrow.org/images/email/nmdp_80.png"></td><td><img alt="" src="http://marrow.org/images/email/btm_50.png"></td></tr></table><br><hr><p><font face="Arial"><i>***This is an auto-generated e-mail***</p><p>This e-mail address is not monitored; please do not respond to this message.</i></p><br><p><i>The sender of this e-mail provides services for the NMDP and/or Be The Match pursuant to agreement and may not be authorized to bind the organization with any information, material, or content contained within this e-mail.</i></font></p>');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_43';
  if (v_record_exists = 0) then
     INSERT INTO ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_CUSTOM_COL2) VALUES(SEQ_ER_CODELST.nextval,'alert_resol','alert_43','Thawed in Transit (TS)','N',15,'The <Request Type> request for <CBU Registry ID> has been resolved as <Resolution Code>.','Alert the user when this Resolution has been received for a request by the TC (system)','<font face="verdana"><table><tr><td align="left"><b><111></b></td><td>:</td><td align="left"><222></td></tr><tr><td align="left">CBB ID</td><td>:</td><td align="left"><333></td></tr><tr><td align="left">CBU Registry ID</td><td>:</td><td align="left"><123></td></tr><tr><td align="left">CBU Local ID</td><td>:</td><td align="left"><666></td></tr><tr><td align="left">Request Type</td><td>:</td><td align="left"><444></td></tr><tr><td align="left">Request Date</td><td>:</td><td align="left"><777></td></tr><tr><td align="left">Alert Date</td><td>:</td><td align="left"><555></td></tr></table><br><br><p><i>This automated EmTrax communication has been sent to you as per your alert preferences. If you wish to discontinue these alerts, please access the alert management section of your profile in EmTrax.</i></p></font><table><tr><td><img alt="" src="http://marrow.org/images/email/nmdp_80.png"></td><td><img alt="" src="http://marrow.org/images/email/btm_50.png"></td></tr></table><br><hr><p><font face="Arial"><i>***This is an auto-generated e-mail***</p><p>This e-mail address is not monitored; please do not respond to this message.</i></p><br><p><i>The sender of this e-mail provides services for the NMDP and/or Be The Match pursuant to agreement and may not be authorized to bind the organization with any information, material, or content contained within this e-mail.</i></font></p>');
	commit;
  end if;
end;
/
--END--
		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,221,12,'12_hotfix2_ER_CODELST_INSERT.sql',sysdate,'9.0.0 B#637-ET022.02');
		commit;



