--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_06';
  if (v_record_exists = 1) then
      UPDATE 
  ER_CODELST SET CODELST_HIDE='Y'where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_06';
	commit;
  end if;
end;
/
--END--
		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,221,11,'11_hotfix2_er_version.sql',sysdate,'9.0.0 B#637-ET022.02');
		commit;





--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_31';
  if (v_record_exists = 1) then
      UPDATE 
  ER_CODELST SET CODELST_DESC='Damaged in Transit (DS)' where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_31';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_32';
  if (v_record_exists = 1) then
      UPDATE 
  ER_CODELST SET CODELST_DESC='Discrepant Unit (DU)' where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_32';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_34';
  if (v_record_exists = 1) then
      UPDATE 
  ER_CODELST SET CODELST_DESC='Lost in Transit (LS)' where codelst_type = 'alert_resol'
    AND codelst_subtyp = 'alert_34';
	commit;
  end if;
end;
/
--END--

		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,221,13,'13_hotfix2_ER_CODELST_UPADTE.sql',sysdate,'9.0.0 B#637-ET022.02');
		commit;
