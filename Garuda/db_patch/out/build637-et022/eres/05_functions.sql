set define off;
create or replace
FUNCTION F_HLA_CORD(pk_cord number, hla_typ NUMBER) return SYS_REFCURSOR
IS
v_entity_cord number;
p_hla_refcur SYS_REFCURSOR;
BEGIN
select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';

OPEN p_hla_refcur FOR     SELECT
                            h.fk_hla_code_id AS PKLOCUS,
                            nvl(ec1.genomic_format,' ') AS TYPE1,
                            nvl(ec2.genomic_format,' ') AS TYPE2,
                            to_char(h.hla_received_date,'Mon DD, YYYY') AS ENTRYDATE,
                            h.CB_HLA_ORDER_SEQ seqvalhla,
                            f_getuser(h.creator) usrname
                          FROM CB_HLA h ,
                            cb_antigen_encod ec1,
                            cb_antigen_encod ec2
                          WHERE h.fk_hla_antigeneid1=ec1.fk_antigen (+)
                          AND ( ec1.version         =
                            (SELECT MAX(ec1s.version)
                            FROM cb_antigen_encod ec1s
                            WHERE ec1.fk_antigen=ec1s.fk_antigen
                            )
                          OR ec1.version IS NULL)
                          AND h.fk_hla_antigeneid2 = ec2.fk_antigen (+)
                          AND (ec2.version         =
                            (SELECT MAX(ec2s.version)
                            FROM cb_antigen_encod ec2s
                            WHERE ec2.fk_antigen=ec2s.fk_antigen
                            )
                          OR ec2.version  IS NULL) AND h.ENTITY_ID  =pk_cord AND h.ENTITY_TYPE=v_entity_cord AND H.FK_SOURCE=hla_typ ORDER BY H.CB_HLA_ORDER_SEQ DESC NULLS LAST;
RETURN p_hla_refcur;
END;
/
create or replace
FUNCTION F_BEST_HLA_CORD(pk_cord number, hla_typ NUMBER) return SYS_REFCURSOR
IS
v_entity_cord NUMBER;
p_bhla_refcur SYS_REFCURSOR;
BEGIN
select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';
OPEN p_bhla_refcur FOR SELECT
                       h.fk_hla_code_id AS PKLOCUS,
                       nvl(ec1.genomic_format,' ') AS TYPE1,
                       nvl(ec2.genomic_format,' ') AS TYPE2,
                       to_char(h.hla_received_date,'Mon DD, YYYY') AS ENTRYDATE,
                       h.CB_BEST_HLA_ORDER_SEQ seqval,
                       F_CODELST_DESC(h.fk_hla_method_id) AS METHODDATA,
                       F_CODELST_DESC(h.fk_hla_code_id) AS LOCUS,
                       f_getuser(h.creator) usrname
                      FROM CB_BEST_HLA h,
                        cb_antigen_encod ec1,
                        cb_antigen_encod ec2
                      WHERE h.ENTITY_TYPE  =v_entity_cord
                      AND H.FK_SOURCE=hla_typ
                      AND h.ENTITY_ID         =pk_cord
                      AND h.fk_hla_antigeneid1=ec1.fk_antigen (+)
                      AND ( ec1.version       =
                        (SELECT MAX(ec1s.version)
                        FROM cb_antigen_encod ec1s
                        WHERE ec1.fk_antigen=ec1s.fk_antigen
                        )
                      OR ec1.version          IS NULL)
                      AND h.fk_hla_antigeneid2 = ec2.fk_antigen (+)
                      AND (ec2.version         =
                        (SELECT MAX(ec2s.version)
                        FROM cb_antigen_encod ec2s
                        WHERE ec2.fk_antigen=ec2s.fk_antigen
                        )
                      OR ec2.version IS NULL)
                      ORDER BY F_CODELST_DESC(h.fk_hla_code_id) nulls last;
RETURN p_bhla_refcur;

END;
/
create or replace
FUNCTION F_GET_PATIENTID(PKCORD NUMBER) RETURN VARCHAR2 IS
PATIENTID VARCHAR2(200);
BEGIN
SELECT RECIPIENT_INFO.RECEIPANT_ID PATIENT_ID
INTO PATIENTID
FROM ER_ORDER_HEADER HDR
LEFT OUTER JOIN ER_ORDER ORD
ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER)
LEFT OUTER JOIN er_order_receipant_info ORDER_RECIPIENT_INFO
ON (ORDER_RECIPIENT_INFO.FK_ORDER_ID = ORD.PK_ORDER)
LEFT OUTER JOIN CB_RECEIPANT_INFO RECIPIENT_INFO
ON (RECIPIENT_INFO.PK_RECEIPANT = ORDER_RECIPIENT_INFO.FK_RECEIPANT)
WHERE HDR.ORDER_ENTITYID        =PKCORD
AND HDR.PK_ORDER_HEADER        IN
  (SELECT MAX(PK_ORDER_HEADER) FROM ER_ORDER_HEADER WHERE ORDER_ENTITYID=PKCORD
  );
  
RETURN PATIENTID;
END;
/
create or replace
FUNCTION F_IS_SYS_CORD(PKCORD NUMBER) RETURN NUMBER IS
FLAG NUMBER:=0;
BEGIN
SELECT NVL(CBB.USING_CDR,0) INTO FLAG FROM CB_CORD CRD LEFT OUTER JOIN CBB CBB ON (CBB.FK_SITE=CRD.FK_CBB_ID) WHERE CRD.PK_CORD IN (PKCORD);
RETURN FLAG;
END;
/

create or replace
FUNCTION        "F_GET_DYNAMICFORMDATA" (pk_cord number,categry varchar2) return SYS_REFCURSOR IS
dynamicform_data SYS_REFCURSOR;
BEGIN
    OPEN dynamicform_data FOR
 select
                              ques.pk_questions pk_ques,
                              forq.ques_seq QUES_SEQ,
                              qgrp.fk_question_group FK_QUES_GRP,
                              NVL(forq.fk_dependent_ques,'0') DEPENT_QUES_PK,
                              NVL(forq.fk_dependent_ques_value,'0') DEPENT_QUES_VALUE,
                              ques_desc QUES_DESC,
                              formres.assess_response ASSES_RESPONSE,
                              formres.comments COMMENTS,
                              f_codelst_desc(formres.resp_code) DROPDOWN_VALUE,
                              f_codelst_desc(ques.response_type) RESPONSE_TYPE,
                              forq.fk_master_ques MASTER_QUES,
                              ques.add_comment_flag COMMENT_FLAG,
                              ques.assesment_flag ASSES_FLAG,
                              ques.unlicn_prior_to_shipment_flag UNLIC_SHIP_FLAG,
                              formres.pk_form_responses PK_FORM_RESP,
                              nvl(formres.resp_code,'0') RESPONSE_CODE,
                              case
                                  when formres.resp_val is null then '0'
                                  when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )>0 then f_codelstcommaval_to_desc(formres.resp_val,',')
                                  when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )=0 then f_codelst_desc(formres.resp_val)
                                   when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='textfield' then formres.resp_val
                              end RESPONSE_VAL,
                              NVL(formres.resp_val,'0') RESPONSE_VAL12,
                              formres.fk_form_responses FK_FORM_RESP,
                              formres.pk_assessment PK_ASSSES,
                              formres.dynaformDate DYNAFORMDATE,
                              formres.assess_remarks ASSES_NOTES,
                              ques.unlicen_req_flag UNLIC_REQ_FLAG,
                              forq.FDA_ELIGIBILITY fda_elig,
                              formres.tcvisibleflag assess_visib_flag
                              from
                              cb_form_questions forq,
                              cb_question_grp qgrp,
                              cb_questions ques
                              left outer join
                              (select
                                  frmres.pk_form_responses,
                                  frmres.fk_question,
                                  frmres.response_code resp_code,
                                  frmres.response_value resp_val,
                                  frmres.comments,
                                  frmres.fk_form_responses,
                                  asses.pk_assessment pk_assessment,
                                  f_codelst_desc(asses.assessment_for_response) assess_response,
                                  asses.assessment_remarks assess_remarks,
                                  nvl(asses.tcvisibleflag,0) tcvisibleflag,
                                  to_char(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate
                                from
                                    cb_form_responses frmres  left outer join (select pk_assessment pk_assessment,sub_entity_id sub_entity_id,assessment_remarks assessment_remarks,cb_assessment.assessment_for_response assessment_for_response,cb_assessment.TC_VISIBILITY_FLAG tcvisibleflag from cb_assessment where sub_entity_type=(select pk_codelst from  er_codelst  where  codelst_type='sub_entity_type' and codelst_subtyp=categry)) asses on( asses.sub_entity_id=frmres.pk_form_responses)
                                where
                                    frmres.entity_id=pk_cord and
                                    frmres.fk_form_version=(select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=pk_cord and fk_form=(select pk_form from cb_forms where FORMS_DESC=categry and cb_forms.is_current_flag=1)) and
                                    frmres.entity_type=(select pk_codelst from er_codelst where codelst_type='test_source' and codelst_subtyp='maternal'))    formres
                              on(formres.FK_QUESTION=ques.pk_questions)
                              where
                              ques.pk_questions=forq.fk_question and
                              ques.pk_questions=qgrp.fk_question and
                              forq.fk_form=(select pk_form from cb_forms where forms_desc=categry and IS_CURRENT_FLAG=1)  and
                              qgrp.fk_form=(select pk_form from cb_forms where forms_desc=categry and IS_CURRENT_FLAG=1)
                              order by  to_number(regexp_substr(forq.ques_seq,'^[0-9]+')),forq.ques_seq,qgrp.pk_question_grp,ques.pk_questions;
                              RETURN DYNAMICFORM_DATA;
END;
/ 
commit;

		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,221,5,'05_functions.sql',sysdate,'9.0.0 B#637-ET022');
		commit;
