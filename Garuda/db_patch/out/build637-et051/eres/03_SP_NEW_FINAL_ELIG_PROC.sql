create or replace
PROCEDURE SP_NEW_FINAL_ELIG_TASK(V_ENTITY_ID NUMBER,ELIG_QUEST_COMPL_FLAG OUT NUMBER) IS

/***************************************************************/
/*MRQ QUESTION VARIABLES*/

/*MRQ QUESTION 1*/
MRQ_Q1 NUMBER(1):=0; --MRQ_NEW_QUES_1
MRQ_Q1_ADD_DETAILS VARCHAR2(500):='';--MRQ_NEW_QUES_1_AD_DETAIL
MRQ_Q1_A NUMBER(1):=0;--MRQ_NEW_QUES_1_A


/*MRQ QUESTION 2*/
MRQ_Q2 NUMBER(1):=0;--MRQ_NEW_QUES_2
MRQ_Q2_ADD_DETAILS VARCHAR2(500):='';--MRQ_NEW_QUES_2_AD_DETAIL

/*MRQ QUESTION 3*/
MRQ_Q3 NUMBER(1):=0;--MRQ_NEW_QUES_3
MRQ_Q3_ADD_DETAILS VARCHAR2(500):='';--MRQ_NEW_QUES_3_AD_DETAIL



/***************************************************************/

/*RCDADS QUESTION VARIABLES*/

/*RCDADS QUESTION 1 */
PHY_Q1 NUMBER(1):=0;--PHY_NEW_FIND_QUES_4
PHY_Q1_ADD_DETAILS VARCHAR2(500):='';--PHY_NEW_FIND_QUES_4_AD_DETAIL
PHY_Q1_A NUMBER(1):=0;--PHY_NEW_FIND_QUES_4_A

/*RCDADS QUESTION 2 */
PHY_Q2 NUMBER(1):=0;--PHY_NEW_FIND_QUES_5
PHY_Q2_ADD_DETAILS VARCHAR2(500):='';--PHY_NEW_FIND_QUES_5_AD_DETAIL


/*RCDADS QUESTION 3 */
PHY_Q3 NUMBER(1):=0;--PHY_NEW_FIND_QUES_6
PHY_Q3_ADD_DETAILS VARCHAR2(500):='';--PHY_NEW_FIND_QUES_6_AD_DETAIL
PHY_Q3_A NUMBER(1):=0;--PHY_NEW_FIND_QUES_6_A
PHY_Q3_B NUMBER(1):=0;--PHY_NEW_FIND_QUES_6_B
PHY_Q3_C NUMBER(1):=0;--PHY_NEW_FIND_QUES_6_C
PHY_Q3_D NUMBER(1):=0;--PHY_NEW_FIND_QUES_6_D
PHY_Q3_E NUMBER(1):=0;--PHY_NEW_FIND_QUES_6_E
PHY_Q3_F NUMBER(1):=0;--PHY_NEW_FIND_QUES_6_F
PHY_Q3_G NUMBER(1):=0;--PHY_NEW_FIND_QUES_6_G
PHY_Q3_H NUMBER(1):=0;--PHY_NEW_FIND_QUES_6_H
PHY_Q3_I NUMBER(1):=0;--PHY_NEW_FIND_QUES_6_I


/***************************************************************/

/*IDM QUESTION VARIABLES*/

/*IDM QUESTION 1*/
IDM_Q1 NUMBER(1):=0;--IDM_NEW_QUES_7
IDM_Q1_ADD_DETAILS VARCHAR2(500):='';--IDM_NEW_QUES_7_AD_DETAIL
IDM_Q1_A NUMBER(1):=0;--IDM_NEW_QUES_7_A



/*IDM QUESTION 2*/
IDM_Q2 NUMBER(1):=0;--IDM_NEW_QUES_8
IDM_Q2_ADD_DETAILS VARCHAR2(500):='';--IDM_NEW_QUES_8_AD_DETAIL
IDM_Q2_A NUMBER(1):=0;--IDM_NEW_QUES_8_A


/*IDM QUESTION 3*/
IDM_Q3 NUMBER(1):=0;--IDM_NEW_QUES_9
IDM_Q3_ADD_DETAILS VARCHAR2(500):='';--IDM_NEW_QUES_9_AD_DETAIL

/***************************************************************/


/*OTHER VARIABLES*/
ELIG_QUES_FLAG NUMBER(1):=0;
IS_ELIG_QUEST_DATA NUMBER(10):=0;
VAR_YES NUMBER(1):=1;
VAR_NO NUMBER(1):=0;
MRQ_QUES_COMP_FLAG NUMBER(1):=0;
PHY_QUES_COMP_FLAG NUMBER(1):=0;
IDM_QUES_COMP_FLAG NUMBER(1):=0;
SKIP_FLAG NUMBER(1):=0;


--V_ENTITY_ID NUMBER(10):=69848;
--ELIG_QUEST_COMPL_FLAG NUMBER(1):=0;

BEGIN


 SELECT NVL(CBBTBL.CRI_REQ_ELG_QUEST,0) INTO ELIG_QUES_FLAG FROM CB_CORD CRD LEFT OUTER JOIN CBB CBBTBL ON (CRD.FK_CBB_ID=CBBTBL.FK_SITE) WHERE CRD.PK_CORD = V_ENTITY_ID;
 SELECT COUNT(*) INTO IS_ELIG_QUEST_DATA FROM CB_NEW_FINAL_DECL_ELIG ELIG_QUES WHERE ELIG_QUES.ENTITY_ID=V_ENTITY_ID;
 
 
 
 IF ELIG_QUES_FLAG=0 THEN 
     ELIG_QUEST_COMPL_FLAG:=1;
     DBMS_OUTPUT.PUT_LINE('FINAL: NEW - ELIG_QUEST_COMPL_FLAG NOT NEEDED FOR THIS CBB::::::'||ELIG_QUEST_COMPL_FLAG);
 END IF;
 
 
 IF ELIG_QUES_FLAG=1 THEN 
 
       IF IS_ELIG_QUEST_DATA=0 THEN
       
           ELIG_QUEST_COMPL_FLAG:=0;
       
       ELSIF IS_ELIG_QUEST_DATA>0 THEN
       
		    ELIG_QUEST_COMPL_FLAG:=0;
       
                   /***************************************MRQ MANIPULATION****************************************/
                    
                            SELECT 
                                NVL(ELIG_QUES.MRQ_NEW_QUES_1,''),
                                NVL(ELIG_QUES.MRQ_NEW_QUES_1_AD_DETAIL,''),
                                NVL(ELIG_QUES.MRQ_NEW_QUES_1_A,''),
                                NVL(ELIG_QUES.MRQ_NEW_QUES_2,''),
                                NVL(ELIG_QUES.MRQ_NEW_QUES_2_AD_DETAIL ,''),
 				NVL(ELIG_QUES.MRQ_NEW_QUES_3,''),
                                NVL(ELIG_QUES.MRQ_NEW_QUES_3_AD_DETAIL ,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_4,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_4_AD_DETAIL,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_4_A,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_5,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_5_AD_DETAIL,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_6 ,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_6_AD_DETAIL ,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_6_A ,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_6_B ,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_6_C ,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_6_D ,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_6_E ,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_6_F ,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_6_G ,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_6_H ,''),
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_6_I ,''),
                                NVL(ELIG_QUES.IDM_NEW_QUES_7,''),
                                NVL(ELIG_QUES.IDM_NEW_QUES_7_AD_DETAIL ,''),
                                NVL(ELIG_QUES.IDM_NEW_QUES_7_A ,''),
                                NVL(ELIG_QUES.IDM_NEW_QUES_8 ,''),
                                NVL(ELIG_QUES.IDM_NEW_QUES_8_AD_DETAIL ,''),
                                NVL(ELIG_QUES.IDM_NEW_QUES_8_A ,''),
                                NVL(ELIG_QUES.IDM_NEW_QUES_9 ,''),
                                NVL(ELIG_QUES.IDM_NEW_QUES_9_AD_DETAIL,'')
                                INTO 
                                MRQ_Q1,
                                MRQ_Q1_ADD_DETAILS,
                                MRQ_Q1_A,
                                MRQ_Q2,
                                MRQ_Q2_ADD_DETAILS,
				MRQ_Q3,
                                MRQ_Q3_ADD_DETAILS,
                                PHY_Q1,
                                PHY_Q1_ADD_DETAILS,
                                PHY_Q1_A,
                                PHY_Q2,
                                PHY_Q2_ADD_DETAILS,
                                PHY_Q3,
                                PHY_Q3_ADD_DETAILS,
                                PHY_Q3_A,
                                PHY_Q3_B,
                                PHY_Q3_C,
                                PHY_Q3_D,
                                PHY_Q3_E,
                                PHY_Q3_F,
                                PHY_Q3_G,
                                PHY_Q3_H,
                                PHY_Q3_I,
                                IDM_Q1,
                                IDM_Q1_ADD_DETAILS,
                                IDM_Q1_A,
                                IDM_Q2,
                                IDM_Q2_ADD_DETAILS,
                                IDM_Q2_A,
                                IDM_Q3,
                                IDM_Q3_ADD_DETAILS
                            FROM 
                                CB_NEW_FINAL_DECL_ELIG ELIG_QUES 
                            WHERE 
                                ELIG_QUES.ENTITY_ID=V_ENTITY_ID AND ELIG_QUES.PK_NEW_FINAL_DECL_ELIG=(SELECT MAX(PK_NEW_FINAL_DECL_ELIG) FROM CB_NEW_FINAL_DECL_ELIG WHERE ENTITY_ID=V_ENTITY_ID);
                          
                      
                      
                            IF MRQ_Q1=VAR_YES THEN
                              MRQ_QUES_COMP_FLAG:=1;
                              --DBMS_OUTPUT.PUT_LINE('Q1 CONDI 1: MRQ COMPLETE FLAG::::::'||MRQ_QUES_COMP_FLAG);
                            ELSIF MRQ_Q1=VAR_NO THEN


				IF MRQ_Q1_A=VAR_NO AND MRQ_Q1_ADD_DETAILS IS NOT NULL THEN
					MRQ_QUES_COMP_FLAG:=1;
					--DBMS_OUTPUT.PUT_LINE('Q1.A CONDI 2: MRQ COMPLETE FLAG::::::'||MRQ_QUES_COMP_FLAG);

				ELSIF MRQ_Q1_A=VAR_YES THEN
 					MRQ_QUES_COMP_FLAG:=1;
					--DBMS_OUTPUT.PUT_LINE('Q1.A  CONDI 3: MRQ COMPLETE FLAG::::::'||MRQ_QUES_COMP_FLAG);

                                ELSE
					MRQ_QUES_COMP_FLAG:=0;
					SKIP_FLAG:=1;
					--DBMS_OUTPUT.PUT_LINE('Q1 CONDI 4: MRQ COMPLETE FLAG::::::'||MRQ_QUES_COMP_FLAG);

                                END IF;


                            ELSE
					MRQ_QUES_COMP_FLAG:=0;
					SKIP_FLAG:=1;
					--DBMS_OUTPUT.PUT_LINE('Q1 CONDI 5: MRQ COMPLETE FLAG::::::'||MRQ_QUES_COMP_FLAG);

                            END IF;
                            
                            IF MRQ_Q2=VAR_YES THEN
                                  MRQ_QUES_COMP_FLAG:=1;
                                  --DBMS_OUTPUT.PUT_LINE('Q2 CONDI 6: MRQ COMPLETE FLAG::::::'||MRQ_QUES_COMP_FLAG);
                            ELSIF MRQ_Q2=VAR_NO AND MRQ_Q2_ADD_DETAILS IS NOT NULL THEN
                                  MRQ_QUES_COMP_FLAG:=1;
                                  --DBMS_OUTPUT.PUT_LINE('Q2 CONDI 7: MRQ COMPLETE FLAG::::::'||MRQ_QUES_COMP_FLAG);
                            ELSE  
                                  MRQ_QUES_COMP_FLAG:=0;
                                  SKIP_FLAG:=1;
                                  --DBMS_OUTPUT.PUT_LINE('Q2 CONDI 8: MRQ COMPLETE FLAG::::::'||MRQ_QUES_COMP_FLAG);
                            END IF;


			    IF MRQ_Q3=VAR_NO THEN
                                  MRQ_QUES_COMP_FLAG:=1;
                                  --DBMS_OUTPUT.PUT_LINE('Q3 CONDI 9: MRQ COMPLETE FLAG::::::'||MRQ_QUES_COMP_FLAG);
                            ELSIF MRQ_Q3=VAR_YES AND MRQ_Q3_ADD_DETAILS IS NOT NULL THEN
                                  MRQ_QUES_COMP_FLAG:=1;
                                  --DBMS_OUTPUT.PUT_LINE('Q3 CONDI 10: MRQ COMPLETE FLAG::::::'||MRQ_QUES_COMP_FLAG);
                            ELSE  
                                  MRQ_QUES_COMP_FLAG:=0;
                                  SKIP_FLAG:=1;
                                  --DBMS_OUTPUT.PUT_LINE('Q3 CONDI 11: MRQ COMPLETE FLAG::::::'||MRQ_QUES_COMP_FLAG);
                            END IF;

                            DBMS_OUTPUT.PUT_LINE('FINAL: NEW - MRQ COMPLETE FLAG::::::'||MRQ_QUES_COMP_FLAG);
                      
                      
                      
                      
                     /*****************************************************************************************************/
                     
                     
                     
                     /***************************************PHYSICAL FINDING MANIPULATION*********************************/
                     
                                            
                            IF PHY_Q1=VAR_YES THEN
                              PHY_QUES_COMP_FLAG:=1;
                              --DBMS_OUTPUT.PUT_LINE('Q1 CONDI 1: PHY FINIDING COMPLETE FLAG::::::'||PHY_QUES_COMP_FLAG);
                            ELSIF PHY_Q1=VAR_NO THEN

				IF PHY_Q1_A=VAR_NO AND PHY_Q1_ADD_DETAILS IS NOT NULL THEN
					PHY_QUES_COMP_FLAG:=1;
                                        --DBMS_OUTPUT.PUT_LINE('Q1.A CONDI 2: PHY FINIDING COMPLETE FLAG::::::'||PHY_QUES_COMP_FLAG);
                                ELSIF PHY_Q1_A=VAR_YES THEN
					PHY_QUES_COMP_FLAG:=1;
                                        --DBMS_OUTPUT.PUT_LINE('Q1.A CONDI 3: PHY FINIDING COMPLETE FLAG::::::'||PHY_QUES_COMP_FLAG);
                                ELSE
				       PHY_QUES_COMP_FLAG:=0;
                                       SKIP_FLAG:=1;
                                       --DBMS_OUTPUT.PUT_LINE('Q1.A CONDI 4: PHY FINIDING COMPLETE FLAG::::::'||PHY_QUES_COMP_FLAG);
                                END IF;
			   ELSE
                                    PHY_QUES_COMP_FLAG:=0;
                                    SKIP_FLAG:=1;
                                    --DBMS_OUTPUT.PUT_LINE('Q1 CONDI 5: PHY FINIDING COMPLETE FLAG::::::'||PHY_QUES_COMP_FLAG);
                            END IF;


                            
                            IF PHY_Q2=VAR_NO THEN
                                    PHY_QUES_COMP_FLAG:=1;
                                    --DBMS_OUTPUT.PUT_LINE('Q2 CONDI 7: PHY FINIDING COMPLETE FLAG::::::'||PHY_QUES_COMP_FLAG);
                            ELSIF PHY_Q2=VAR_YES AND PHY_Q2_ADD_DETAILS IS NOT NULL THEN
                                    PHY_QUES_COMP_FLAG:=1;
                                    --DBMS_OUTPUT.PUT_LINE('Q2 CONDI 8: PHY FINIDING COMPLETE FLAG::::::'||PHY_QUES_COMP_FLAG);
                            ELSE
                                    PHY_QUES_COMP_FLAG:=0;
                                    SKIP_FLAG:=1;
                                    --DBMS_OUTPUT.PUT_LINE('Q2 CONDI 9: PHY FINIDING COMPLETE FLAG::::::'||PHY_QUES_COMP_FLAG);
                            
                            END IF;
                            
                            
                            IF PHY_Q3=VAR_NO THEN
                                    PHY_QUES_COMP_FLAG:=1;
                                    --DBMS_OUTPUT.PUT_LINE('Q3 CONDI 10: PHY FINIDING COMPLETE FLAG::::::'||PHY_QUES_COMP_FLAG);
                            ELSIF PHY_Q3=VAR_YES AND PHY_Q3_ADD_DETAILS IS NOT NULL THEN
                            
                                    IF PHY_Q3_A IS NOT NULL AND PHY_Q3_B IS NOT NULL AND PHY_Q3_C IS NOT NULL AND PHY_Q3_D IS NOT NULL AND PHY_Q3_E IS NOT NULL AND PHY_Q3_F IS NOT NULL AND PHY_Q3_G IS NOT NULL AND PHY_Q3_H IS NOT NULL AND PHY_Q3_I IS NOT NULL THEN
                                              PHY_QUES_COMP_FLAG:=1;
                                              --DBMS_OUTPUT.PUT_LINE('Q3 CONDI 11: PHY FINIDING COMPLETE FLAG::::::'||PHY_QUES_COMP_FLAG);
                                    ELSE
                                              PHY_QUES_COMP_FLAG:=0;
                                              SKIP_FLAG:=1;
                                              --DBMS_OUTPUT.PUT_LINE('Q3 CONDI 12: PHY FINIDING COMPLETE FLAG::::::'||PHY_QUES_COMP_FLAG);
                                    END IF;
                            ELSE
                                    PHY_QUES_COMP_FLAG:=0;
                                    SKIP_FLAG:=1;
                                    --DBMS_OUTPUT.PUT_LINE('Q3 CONDI 13: PHY FINIDING COMPLETE FLAG::::::'||PHY_QUES_COMP_FLAG);
                                    
                            END IF;
                          
                        
                            DBMS_OUTPUT.PUT_LINE('FINAL: NEW -  PHY FINDING COMPLETE FLAG::::::'||PHY_QUES_COMP_FLAG);
                      
                      
                      
                      
                     /********************************************************************************************/
                     
                     
                    /**************************************IDM MANIPULATION***************************************/
                    
                    
                            IF IDM_Q1=VAR_YES THEN
                            
                                IDM_QUES_COMP_FLAG:=1;
                                --DBMS_OUTPUT.PUT_LINE('Q1 CONDI 1: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);
                                
                            ELSIF IDM_Q1=VAR_NO THEN
                            
                                
                                 IF IDM_Q1_A=VAR_NO AND IDM_Q1_ADD_DETAILS IS NOT NULL THEN

				       IDM_QUES_COMP_FLAG:=1;
                                       --DBMS_OUTPUT.PUT_LINE('Q1.A CONDI 2: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);

                                 ELSIF IDM_Q1_A=VAR_YES THEN
                                       
					IDM_QUES_COMP_FLAG:=1;
                                        --DBMS_OUTPUT.PUT_LINE('Q1.A CONDI 3: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);
                                 ELSE
                                       IDM_QUES_COMP_FLAG:=0;
                                       SKIP_FLAG:=1;
                                       --DBMS_OUTPUT.PUT_LINE('Q1.A CONDI 4: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);
                                 END IF;
			    ELSE 

                                IDM_QUES_COMP_FLAG:=0;
				SKIP_FLAG:=1;
                                --DBMS_OUTPUT.PUT_LINE('Q1 CONDI 5: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);

                            END IF;
                            
                            
                            
                            IF IDM_Q2=VAR_YES THEN
                              IDM_QUES_COMP_FLAG:=1;
                              --DBMS_OUTPUT.PUT_LINE('Q2 CONDI 6: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);
                            ELSIF IDM_Q2=VAR_NO THEN
                            
                                 IF IDM_Q2_A=VAR_NO AND IDM_Q2_ADD_DETAILS IS NOT NULL THEN

				       IDM_QUES_COMP_FLAG:=1;
                                       --DBMS_OUTPUT.PUT_LINE('Q2.A CONDI 7: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);

                                 ELSIF IDM_Q1_A=VAR_YES THEN
                                       
					IDM_QUES_COMP_FLAG:=1;
                                        --DBMS_OUTPUT.PUT_LINE('Q2.A CONDI 8: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);
                                 ELSE
                                       IDM_QUES_COMP_FLAG:=0;
                                       SKIP_FLAG:=1;
                                       --DBMS_OUTPUT.PUT_LINE('Q2.A CONDI 9: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);
                                 END IF;

                            
                            ELSE 
                                IDM_QUES_COMP_FLAG:=0;
				SKIP_FLAG:=1;
                                --DBMS_OUTPUT.PUT_LINE('Q2 CONDI 10: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);
                            END IF;
                            
                            
                            
                            IF IDM_Q3=VAR_NO THEN
                              IDM_QUES_COMP_FLAG:=1;
                              --DBMS_OUTPUT.PUT_LINE('Q3 CONDI 11: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);
                            ELSIF IDM_Q3=VAR_YES THEN
  
                                 IF IDM_Q3_ADD_DETAILS IS NOT NULL THEN
                                    IDM_QUES_COMP_FLAG:=1;
                                    --DBMS_OUTPUT.PUT_LINE('Q3 CONDI 12: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);
                                 ELSE
                                    IDM_QUES_COMP_FLAG:=0;
                                    SKIP_FLAG:=1;
                                    --DBMS_OUTPUT.PUT_LINE('Q3 CONDI 13: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);
                                 END IF;
                            ELSE 
                                 IDM_QUES_COMP_FLAG:=0;
				                         SKIP_FLAG:=1;
                                 --DBMS_OUTPUT.PUT_LINE('Q3 CONDI 14: IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);
                            END IF;
                            
                            DBMS_OUTPUT.PUT_LINE('FINAL: NEW - IDM COMPLETE FLAG::::::'||IDM_QUES_COMP_FLAG);
                      
                      
                    /*********************************************************************************************/
                    
                    
                    
                    IF SKIP_FLAG=0 AND MRQ_QUES_COMP_FLAG=1 AND PHY_QUES_COMP_FLAG=1 AND IDM_QUES_COMP_FLAG=1 THEN
                    
                            ELIG_QUEST_COMPL_FLAG:=1;
                            DBMS_OUTPUT.PUT_LINE('NEW - FINAL: ELIG_QUEST_COMPL_FLAG::::::'||ELIG_QUEST_COMPL_FLAG);
                    
                    END IF;
                          

       
       END IF;
     
 END IF;
END;
/

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,281,3,'03_SP_NEW_FINAL_ELIG_PROC.sql',sysdate,'9.0.0 B#637-ET051');
commit;
