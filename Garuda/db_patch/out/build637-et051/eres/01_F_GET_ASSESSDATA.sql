create or replace
function f_get_assessdata(v_assessment_id number) return varchar2 as
v_mas_ques number;
v_dep_ques number;
v_dep_qval varchar2(2000);
v_dep_ques1 number;
v_dep_qval1 varchar2(2000);
v_dep_ques2 number;
v_dep_qval2 varchar2(2000);
v_dep_ques3 number;
v_dep_qval3 varchar2(2000);
v_ques_desc varchar2(2000);
v_mq_desc varchar2(2000);
v_dq_desc varchar2(2000);
v_dq1_desc varchar2(2000);
v_dq2_desc varchar2(2000);
v_dq3_desc varchar2(2000);
v_ques number;
v_qres varchar2(2000);
v_form number;
v_result CLOB;
v_ass varchar2(4000);
v_qsec varchar2(2000);
v_dqsec varchar2(2000);
v_dqsec1 varchar2(2000);
v_dqsec2 varchar2(2000);
v_dqsec3 varchar2(2000);
v_fkformres varchar2(2000);
v_formdesc varchar2(2000);
begin
SELECT
  fk_question,
  fk_master_ques,
  fk_dependent_ques,
  fk_form,
  nvl(f_codelst_desc(fk_dependent_ques_value),''),
  ques_seq
into
  v_ques,
  v_mas_ques,
  v_dep_ques,
  v_form,
  v_dep_qval,
  v_qsec
FROM cb_form_questions
WHERE (fk_form,fk_question)=
  (SELECT fk_form,
    fk_question
  FROM cb_form_responses
  WHERE pk_form_responses=(SELECT sub_entity_id FROM cb_assessment WHERE pk_assessment=v_assessment_id)
  );
  SELECT assessment_remarks||'~'||f_codelst_desc(assessment_for_response)||'~'||case when tc_visibility_flag=1 then 'Yes' else 'No' end||'~'||column_reference into v_ass FROM cb_assessment WHERE pk_assessment=v_assessment_id;
  SELECT case when assessment_reason is not null then f_codelst_desc(assessment_reason) else assessment_reason_remarks end into v_qres FROM cb_assessment WHERE pk_assessment=v_assessment_id;
  --dbms_output.put_line('v_ques.....'||v_ques);
  --dbms_output.put_line('v_qres.....'||v_qres);
  --dbms_output.put_line('v_mas_ques.....'||v_mas_ques);

  SELECT fk_form_version into v_fkformres  FROM cb_form_responses WHERE pk_form_responses=(SELECT sub_entity_id FROM cb_assessment WHERE pk_assessment=v_assessment_id);
  SELECT forms_desc INTO v_formdesc FROM CB_FORMS where pk_form = v_form;
  
  if v_mas_ques is not null then
  select nvl(ques_desc,'') into v_mq_desc from cb_questions where pk_questions=v_mas_ques;
  end if;
  --dbms_output.put_line('v_dep_ques::::::::'||v_dep_ques);
  --dbms_output.put_line('v_form::::::::'||v_form);
 if v_ques is not null then
     select ques_desc into v_ques_desc from cb_questions where pk_questions=v_ques;
     if v_ques_desc is null then
      select response_value into v_ques_desc from cb_form_responses where pk_form_responses = (SELECT sub_entity_id FROM cb_assessment WHERE pk_assessment=v_assessment_id);
     end if;
  end if;
  if v_dep_ques is not null then
     select ques_desc into v_dq_desc from cb_questions where pk_questions=v_dep_ques;
  end if;
  if v_dep_ques is not null then
    select fk_dependent_ques,f_codelst_desc(fq.fk_dependent_ques_value),ques_seq into v_dep_ques1,v_dep_qval1,v_dqsec from cb_form_questions fq where fq.fk_form=v_form and fq.fk_question=v_dep_ques;
    if v_dep_ques1 is not null then
    select ques_desc into v_dq1_desc from cb_questions where pk_questions=v_dep_ques1;
    end if;
  end if;
  
  --dbms_output.put_line('v_dep_ques1::::::::'||v_dep_ques1);
  if v_dep_ques1 is not null then
    select fk_dependent_ques,f_codelst_desc(fq.fk_dependent_ques_value),ques_seq into v_dep_ques2,v_dep_qval2,v_dqsec1 from cb_form_questions fq where fq.fk_form=v_form and fq.fk_question=v_dep_ques1;
    if v_dep_ques2 is not null then
    select ques_desc into v_dq2_desc from cb_questions where pk_questions=v_dep_ques2;
    end if;
  end if;
  --dbms_output.put_line('v_dep_ques2:::::::::'||v_dep_ques2);
  if v_dep_ques2 is not null then
    select fk_dependent_ques,f_codelst_desc(fq.fk_dependent_ques_value),ques_seq into v_dep_ques3,v_dep_qval3,v_dqsec2 from cb_form_questions fq where fq.fk_form=v_form and fq.fk_question=v_dep_ques2;
    if v_dep_ques3 is not null then
    select ques_desc into v_dq3_desc from cb_questions where pk_questions=v_dep_ques3;
    select ques_seq into v_dqsec3 from cb_form_questions fq where fq.fk_form=v_form and fq.fk_question=v_dep_ques3;
    end if;
  end if;
  --dbms_output.put_line('Form Desc:::::::::'||v_formdesc);
  if v_formdesc='IDM' then
  --dbms_output.put_line('inside IDM:::::::::'||v_dep_ques1||':::'||v_dep_qval1);
  if v_dep_ques is not null then
      --dbms_output.put_line('inside question:::::::::'||v_dep_qval);
      select f_codelst_desc(response_code) into v_dep_qval from cb_form_responses where fk_form = v_form and fk_form_version = v_fkformres and fk_question = v_dep_ques;
      --dbms_output.put_line('After question:::::::::'||v_dep_qval);
    end if;
    if v_dep_ques1 is not null then
      --dbms_output.put_line('inside question1:::::::::'||v_dep_qval1);
      select f_codelst_desc(response_code) into v_dep_qval1 from cb_form_responses where fk_form = v_form and fk_form_version = v_fkformres and fk_question = v_dep_ques1;
      --dbms_output.put_line('After question1:::::::::'||v_dep_qval1);
    end if;
    if v_dep_ques2 is not null then
      select f_codelst_desc(response_code) into v_dep_qval2 from cb_form_responses where fk_form = v_form and fk_form_version = v_fkformres and fk_question = v_dep_ques2;
    end if;
    if v_dep_ques3 is not null then
      select f_codelst_desc(response_code) into v_dep_qval3 from cb_form_responses where fk_form = v_form and fk_form_version = v_fkformres and fk_question = v_dep_ques3;
    end if;
  end if;
  
  if v_dep_ques3 is not null then
    if v_result is not null and v_result!='~' then
     v_result:=v_result||'~'||v_dqsec3||') '||v_dq3_desc||'~'||v_dep_qval3;
    else
      v_result:=v_dqsec3||') '||v_dq3_desc||'~'||v_dep_qval3;
    end if;
    --dbms_output.put_line('11111'||v_result);
  end if;
  if v_dep_ques2 is not null then
    if v_result is not null and v_result!='~' then
      v_result:=v_result||'~'||v_dqsec2||') '||v_dq2_desc||'~'||v_dep_qval2;
    else
      v_result:=v_dqsec2||') '||v_dq2_desc||'~'||v_dep_qval2;
    end if;
    --dbms_output.put_line('222222222'||v_result);
  end if;
  if v_dep_ques1 is not null then
  --dbms_output.put_line('v_result.........'||v_result||'.......');
    if v_result is not null and v_result!='~' then
    --dbms_output.put_line('v_dq1_desc in if ::::::'||v_dq1_desc||'.........');
      v_result:=v_result||'~'||v_dqsec1||') '||v_dq1_desc||'~'||v_dep_qval1;
    else
      --dbms_output.put_line('v_dq1_desc::::::'||v_dq1_desc);
      v_result:=v_dqsec1||') '||v_dq1_desc||'~'||v_dep_qval1;
    end if;
    --dbms_output.put_line('33333333'||v_result);
  end if;
  if v_dep_ques is not null then
    if v_result is not null and v_result!='~' then
    --dbms_output.put_line('in if::::'||v_result);
      v_result:=v_result||'~'||v_dqsec||') '||v_dq_desc||'~'||v_dep_qval;
    else
    --dbms_output.put_line('in else::::'||v_result);
      v_result:=v_dqsec||') '||v_dq_desc||'~'||v_dep_qval;
    end if;
    --dbms_output.put_line('444444444'||v_result);
  end if;
  if v_ques is not null then
  --dbms_output.put_line('in if v_ques not null::::::::'||v_ques);
  --dbms_output.put_line(' v_result ::::::::'||v_result||'...');
    if v_result is not null and v_result!='~' then
    --dbms_output.put_line('in if v_result not null::::::::'||v_result);
      v_result:=v_result||'~'||v_qsec||') '||v_ques_desc||'~'||v_qres;
    else
      v_result:=v_qsec||') '||v_ques_desc||'~'||v_qres;
    end if;
    --dbms_output.put_line('5555555555'||v_result);
  end if;
  if v_ass is not null then
    if v_result is not null and v_result!='~' then
      v_result:=v_result||'~'||v_ass;
    else
      v_result:=v_ass;
    end if;
    --dbms_output.put_line('6666666666'||v_result);
  end if;
  --dbms_output.put_line('@@@@@@@'||v_result);
  return v_result;
end;
/

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,279,1,'01_F_GET_ASSESSDATA.sql',sysdate,'9.0.0 B#637-ET051');
commit;