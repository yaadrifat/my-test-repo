DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_MAILCONFIG;
  if (v_record_exists > 1) then
    UPDATE ER_MAILCONFIG SET MAILCONFIG_CC = 'null'  where MAILCONFIG_CC = '0' ;
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_MAILCONFIG;
  if (v_record_exists > 1) then
  UPDATE ER_MAILCONFIG SET MAILCONFIG_BCC=null;
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_MAILCONFIG;
  if (v_record_exists > 1) then
 UPDATE ER_MAILCONFIG SET MAILCONFIG_FROM='customersupport@veloseresearch.com';
	commit;
  end if;
end;
/

Update ER_MAILCONFIG set MAILCONFIG_CONTENT=CHR(13) ||
	'Dear [USER],</br></br></br>' || CHR(13) ||	
	CHR(13) || CHR(13) || 	
	'The Cord Import Process that you started has been completed now.</br>' || CHR(13) || CHR(13) || 
	'Please Use This Link <a href="[URL]">Cord Import</a>  to verify the process.</br></br></br>' || CHR(13) || 
	CHR(13) || 
	'Kind Regards,</br>' || CHR(13) || 
	'[CM]' || 
	CHR(13) where MAILCONFIG_MAILCODE='IMPORTNOTI';
	
	commit;
	
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,192,8,'08_ER_MAILCONFIG_UPDATE.sql',sysdate,'9.0.0 B#637-ET008');

commit;