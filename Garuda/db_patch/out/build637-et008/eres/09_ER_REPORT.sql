delete er_repxsl where pk_repxsl in (174);
commit;


update er_report set rep_sql='SELECT CRD.CORD_REGISTRY_ID REGID,
CRD.CORD_LOCAL_CBU_ID LOCALCBUID,
CRD.CORD_ISBI_DIN_CODE ISBTDIN,
CBB.SITE_NAME SITENAME,
ADDR.ADDRESS ADDRESS,
addr.address2 ADDRESS2,
ADDR.ADD_CITY CITY,
ADDR.ADD_ZIPCODE ZIPCODE,
ADDR.ADD_STATE STATE,
ADDR.ADD_COUNTRY COUNTRY,
nvl(to_char(pslip.SCH_SHIPMENT_DATE,''Mon DD, YYYY''),''Not Available'') SHIPDATE,
NVL(F_CODELST_DESC(PSLIP.FK_SAMPLE_TYPE_AVAIL),''Not Available'') SAMPLE_TYPE,
NVL(F_CODELST_DESC(PSLIP.FK_ALIQUOTS_TYPE),''Not Available'') ALIQUOTS_TYPE,
F_HLA_PACKING_SLIP(~2)  HLA
from cb_packing_slip pslip LEFT OUTER JOIN CB_CORD CRD on(pslip.fk_cord_id=crd.pk_cord) LEFT OUTER JOIN ER_SITE CBB on(pslip.cbb_id=cbb.pk_site) LEFT OUTER JOIN ER_ADD ADDR ON(CBB.FK_PERADD=ADDR.PK_ADD) LEFT OUTER JOIN ER_ORDER ORD on(pslip.fk_order_id=ord.pk_order) where pslip.pk_packing_slip=~1' where pk_report in (174);

commit;



update er_report set rep_sql_clob='SELECT CRD.CORD_REGISTRY_ID REGID,
CRD.CORD_LOCAL_CBU_ID LOCALCBUID,
CRD.CORD_ISBI_DIN_CODE ISBTDIN,
CBB.SITE_NAME SITENAME,
ADDR.ADDRESS ADDRESS,
addr.address2 ADDRESS2,
ADDR.ADD_CITY CITY,
ADDR.ADD_ZIPCODE ZIPCODE,
ADDR.ADD_STATE STATE,
ADDR.ADD_COUNTRY COUNTRY,
nvl(to_char(pslip.SCH_SHIPMENT_DATE,''Mon DD, YYYY''),''Not Available'') SHIPDATE,
NVL(F_CODELST_DESC(PSLIP.FK_SAMPLE_TYPE_AVAIL),''Not Available'') SAMPLE_TYPE,
NVL(F_CODELST_DESC(PSLIP.FK_ALIQUOTS_TYPE),''Not Available'') ALIQUOTS_TYPE,
F_HLA_PACKING_SLIP(~2)  HLA
from cb_packing_slip pslip LEFT OUTER JOIN CB_CORD CRD on(pslip.fk_cord_id=crd.pk_cord) LEFT OUTER JOIN ER_SITE CBB on(pslip.cbb_id=cbb.pk_site) LEFT OUTER JOIN ER_ADD ADDR ON(CBB.FK_PERADD=ADDR.PK_ADD) LEFT OUTER JOIN ER_ORDER ORD on(pslip.fk_order_id=ord.pk_order) where pslip.pk_packing_slip=~1' where pk_report in (174);

commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,192,9,'09_ER_REPORT.sql',sysdate,'9.0.0 B#637-ET008');

commit;