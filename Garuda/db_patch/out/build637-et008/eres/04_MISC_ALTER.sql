--STARTS ADDING COLUMN TO ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_CORD' AND COLUMN_NAME = 'SYSTEM_CORD';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_CORD ADD SYSTEM_CORD varchar2(3)';
	commit;
  end if;
end;
/
 COMMENT ON COLUMN CB_CORD.SYSTEM_CORD IS 'Stores system cord';
 
 --STARTS ADD THE COLUMN in CB_ENTITY_SAMPLES--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_ENTITY_SAMPLES'
    AND COLUMN_NAME = 'CONV_FIL_PAP_SAMP';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_ENTITY_SAMPLES add (CONV_FIL_PAP_SAMP  NUMBER(10,0))';
      commit;
  end if;
end;
/
comment on column CB_ENTITY_SAMPLES.CONV_FIL_PAP_SAMP  is 'Stores Number of Filter Paper Samples(Converted CBU)';
commit;
--end--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_ENTITY_SAMPLES'
    AND COLUMN_NAME = 'CONV_RPC_PELLETS';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_ENTITY_SAMPLES add (CONV_RPC_PELLETS  NUMBER(10,0))';
      commit;
  end if;
end;
/
comment on column CB_ENTITY_SAMPLES.CONV_RPC_PELLETS  is 'Stores Number of RBC Pellets(Converted CBU)';
commit;
--end--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_ENTITY_SAMPLES'
    AND COLUMN_NAME = 'CONV_EXTRACT_DNA';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_ENTITY_SAMPLES add (CONV_EXTRACT_DNA  NUMBER(10,0))';
      commit;
  end if;
end;
/
comment on column CB_ENTITY_SAMPLES.CONV_EXTRACT_DNA  is 'Stores Number of Extracted DNA Samples(Converted CBU)';
commit;
--end--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_ENTITY_SAMPLES'
    AND COLUMN_NAME = 'CONV_SERUM_SAMP';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_ENTITY_SAMPLES add (CONV_SERUM_SAMP  NUMBER(10,0))';
      commit;
  end if;
end;
/
comment on column CB_ENTITY_SAMPLES.CONV_SERUM_SAMP  is 'Stores Number of Serum Samples(Converted CBU)';
commit;
--end--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_ENTITY_SAMPLES'
    AND COLUMN_NAME = 'CONV_PLASMA_SAMP';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_ENTITY_SAMPLES add (CONV_PLASMA_SAMP  NUMBER(10,0))';
      commit;
  end if;
end;
/
comment on column CB_ENTITY_SAMPLES.CONV_PLASMA_SAMP  is 'Stores Number of Plasma Samples(Converted CBU)';
commit;
--end--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_ENTITY_SAMPLES'
    AND COLUMN_NAME = 'CONV_NO_OF_SEG';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_ENTITY_SAMPLES add (CONV_NO_OF_SEG  NUMBER(10,0))';
      commit;
  end if;
end;
/
comment on column CB_ENTITY_SAMPLES.CONV_NO_OF_SEG  is 'Stores Number of Segments (Converted CBU)';
commit;
--end--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_ENTITY_SAMPLES'
    AND COLUMN_NAME = 'CONV_NONVIABLE_CELL';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_ENTITY_SAMPLES add (CONV_NONVIABLE_CELL  NUMBER(10,0))';
      commit;
  end if;
end;
/
comment on column CB_ENTITY_SAMPLES.CONV_NONVIABLE_CELL  is 'Stores Number of Non-Viable Cell samples (Converted CBU)';
commit;
--end--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_ENTITY_SAMPLES'
    AND COLUMN_NAME = 'CONV_VIABLE_CELLSAMP';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_ENTITY_SAMPLES add (CONV_VIABLE_CELLSAMP  NUMBER(10,0))';
      commit;
  end if;
end;
/
comment on column CB_ENTITY_SAMPLES.CONV_VIABLE_CELLSAMP  is 'Stores Number of Viable Cell samples (Converted CBU)';
commit;
--end--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_ENTITY_SAMPLES'
    AND COLUMN_NAME = 'CONV_MATR_SERSAMP';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_ENTITY_SAMPLES add (CONV_MATR_SERSAMP  NUMBER(10,0))';
      commit;
  end if;
end;
/
comment on column CB_ENTITY_SAMPLES.CONV_MATR_SERSAMP  is 'Stores Number of Maternal Serum Samples (Converted CBU)';
commit;
--end--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_ENTITY_SAMPLES'
    AND COLUMN_NAME = 'CONV_MATR_PLASAMP';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_ENTITY_SAMPLES add (CONV_MATR_PLASAMP  NUMBER(10,0))';
      commit;
  end if;
end;
/
comment on column CB_ENTITY_SAMPLES.CONV_MATR_PLASAMP  is 'Stores Number of Maternal Plasma Samples (Converted CBU)';
commit;
--end--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_ENTITY_SAMPLES'
    AND COLUMN_NAME = 'CONV_MATR_EXTDNA_SAMP';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_ENTITY_SAMPLES add (CONV_MATR_EXTDNA_SAMP  NUMBER(10,0))';
      commit;
  end if;
end;
/
comment on column CB_ENTITY_SAMPLES.CONV_MATR_EXTDNA_SAMP  is 'Stores Number of Maternal Extracted DNA Samples (Converted CBU)';
commit;
--end--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_ENTITY_SAMPLES'
    AND COLUMN_NAME = 'CONV_MISCMATR_SAMP';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_ENTITY_SAMPLES add (CONV_MISCMATR_SAMP  NUMBER(10,0))';
      commit;
  end if;
end;
/
comment on column CB_ENTITY_SAMPLES.CONV_MISCMATR_SAMP  is 'Stores Number of Miscellaneous Maternal Samples (Converted CBU)';
commit;
--end--


DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_IMPORT_HISTORY'
    AND COLUMN_NAME = 'IMPORT_FILE_NAME';

  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_CORD_IMPORT_HISTORY modify IMPORT_FILE_NAME varchar2(200)';
      commit;
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,192,4,'04_MISC_ALTER.sql',sysdate,'9.0.0 B#637-ET008');

commit;