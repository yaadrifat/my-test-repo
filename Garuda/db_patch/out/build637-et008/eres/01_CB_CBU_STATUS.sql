--update cbu status script

--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_cbu_status
    where code_type = 'Response'
    AND cbu_status_code in('CT','HE','OR','RCT','RHE');
  if (v_record_exists = 5) then
      UPDATE CB_CBU_STATUS SET CODE_TYPE = 'Resolution' WHERE CODE_TYPE = 'Response' AND CBU_STATUS_CODE in('CT','HE','OR','RCT','RHE');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where code_type = 'Resolution'
    AND cbu_status_code ='RSN';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_CB_CBU_STATUS.nextval,'RSN','Reserved for NMDP','Resolution',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='RSN' AND CODE_TYPE='Response'),NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where code_type = 'Resolution'
    AND cbu_status_code ='NA';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_CB_CBU_STATUS.nextval,'NA','Not Available','Resolution',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='NA' AND CODE_TYPE='Response'),NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where code_type = 'Resolution'
    AND cbu_status_code ='SH';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_CB_CBU_STATUS.nextval,'SH','Shipped','Resolution',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='SH' AND CODE_TYPE='Response'),NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where code_type = 'Resolution'
    AND cbu_status_code ='AV';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_CB_CBU_STATUS.nextval,'AV','Available','Resolution',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='AV' AND CODE_TYPE='Response'),NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--




--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where code_type = 'Resolution'
    AND cbu_status_code ='AC';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_CB_CBU_STATUS.nextval,'AC','Active','Resolution',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='AC' AND CODE_TYPE='Response'),NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where code_type = 'Resolution'
    AND cbu_status_code ='CC';
  if (v_record_exists = 0) then
      INSERT INTO 
  CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_CB_CBU_STATUS.nextval,'CC','Unknown Code','Resolution',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='CC' AND CODE_TYPE='Response'),NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,192,1,'01_CB_CBU_STATUS.sql',sysdate,'9.0.0 B#637-ET008');

commit;