/* This readMe is specific to Velos eResearch version 9.0 build637-et008 */

=====================================================================================================================================
Garuda :

	1. To import arial_narrow.TTF font in the application, follow the steps :-
	
		Step 1. Copy the files arial_narrow.TTF and copy_font.bat into any directory of your system.
		Step 2. Double click on copy_font.bat and type the path of the jboss server and press enter.


	2. Cord Import email URL Setting:

		Set the application URL in [server\eresearch\deploy\velos.ear\velos.war\WEB-INF\classes\]application.properties file as follows

		garuda.application.url = <<IP Address:Port>> 
		for example 
		garuda.application.url = http://172.16.2.60:8080

	3. Additional Note:

		We have replaced one JAR file garudamailer-1.0.jar to resolve issues related to e mail configuration.

   		For development team only: Please go  to  doc\tools\maven\Jars-garuda\ in cvs  and after taking a  CVS update, double-click on storeJarsInMavenForGaruda.bat
    
    	Please make sure in server/eresearch/deploy/velos.ear/velos.war/WEB-INF/lib folder, there should be only one jar(garudamailer-1.0.jar) file related to e mails

	4. Group rights added for Upload document - Please open and save the group access rights for each Group in order to reflect the additions correctly. 
	
=====================================================================================================================================
