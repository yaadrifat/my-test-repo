 CREATE OR REPLACE TRIGGER "CB_ANTIGEN_AU1" AFTER UPDATE ON CB_ANTIGEN 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE 
  V_ROWID NUMBER(10);/*VARIABLE USED TO FETCH VALUE OF SEQUENCE */
	NEW_MODIFIED_BY VARCHAR2(200);
	NEW_CREATOR VARCHAR2(200);
	OLD_CREATOR VARCHAR2(200);
	OLD_MODIFIED_BY VARCHAR2(200);
	OLD_CODLST_ALERT VARCHAR2(200);
	NEW_CODLST_ALERT VARCHAR2(200);
	OLD_USERID VARCHAR2(200);
	NEW_USERID VARCHAR2(200);
	OLD_VALID_IND VARCHAR2(20);
	NEW_VALID_IND VARCHAR2(20);
	OLD_ACTIVE_IND VARCHAR2(20);
	NEW_ACTIVE_IND VARCHAR2(20);
	OLD_DELETEDFLAG VARCHAR2(20);
	NEW_DELETEDFLAG VARCHAR2(20);
	
  BEGIN   
     SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO V_ROWID FROM DUAL;
    
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (V_ROWID,'CB_ANTIGEN',:OLD.RID,:OLD.PK_ANTIGEN,'U',:NEW.LAST_MODIFIED_BY);
 
	IF NVL(:old.PK_ANTIGEN,0) != NVL(:new.PK_ANTIGEN,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_ANTIGEN', 'PK_ANTIGEN', :OLD.PK_ANTIGEN,:NEW.PK_ANTIGEN,NULL,NULL);
    END IF;
	 IF NVL(:old.ANTIGEN_ID,0) != NVL(:new.ANTIGEN_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_ANTIGEN', 'ANTIGEN_ID',:OLD.ANTIGEN_ID,:NEW.ANTIGEN_ID,NULL,NULL);
    END IF;
	if NVL(:old.VALID_IND,' ') != NVL(:new.VALID_IND,' ') then
	SELECT DECODE(:NEW.VALID_IND,'1','Yes','0','No')		INTO NEW_VALID_IND  FROM DUAL;
	SELECT DECODE(:OLD.VALID_IND,'1','Yes','0','No')		INTO OLD_VALID_IND  FROM DUAL;
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_ANTIGEN', 'VALID_IND',OLD_VALID_IND,NEW_VALID_IND,NULL,NULL);
    END IF;
	IF NVL(:old.ACTIVE_IND,' ') != NVL(:new.ACTIVE_IND,' ') THEN
	SELECT DECODE(:NEW.ACTIVE_IND,'1','Yes','0','No')		INTO NEW_ACTIVE_IND  FROM DUAL;
	SELECT DECODE(:OLD.ACTIVE_IND,'1','Yes','0','No')		INTO OLD_ACTIVE_IND  FROM DUAL;
      	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_ANTIGEN', 'ACTIVE_IND', OLD_ACTIVE_IND,NEW_ACTIVE_IND,NULL,NULL);
    END IF;
	IF NVL(:old.HLA_LOCUS,' ') != NVL(:new.HLA_LOCUS,' ') THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','HLA_LOCUS',:old.HLA_LOCUS,:NEW.HLA_LOCUS,NULL,NULL);
	END IF;
	IF NVL(:old.HLA_TYPING_METHOD,' ') != NVL(:new.HLA_TYPING_METHOD,' ') THEN
    PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_ANTIGEN','HLA_TYPING_METHOD',:old.HLA_TYPING_METHOD,:new.HLA_TYPING_METHOD,null,null);
	END IF;
	if NVL(:old.ALT_ANTIGEN_ID,0) != NVL(:new.ALT_ANTIGEN_ID,0) then
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','ALT_ANTIGEN_ID',:OLD.ALT_ANTIGEN_ID,:NEW.ALT_ANTIGEN_ID,NULL,NULL);
	END IF;
	if NVL(:old.LAST_UPDATED_SOURCE,' ') != NVL(:new.LAST_UPDATED_SOURCE,' ') then
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','LAST_UPDATED_SOURCE',:OLD.LAST_UPDATED_SOURCE,:NEW.LAST_UPDATED_SOURCE,NULL,NULL);
	END IF;
	if NVL(:old.CREATED_BY_SOURCE,' ') != NVL(:new.CREATED_BY_SOURCE,' ') then
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_ANTIGEN','CREATED_BY_SOURCE',:OLD.CREATED_BY_SOURCE,:NEW.CREATED_BY_SOURCE,NULL,NULL);
	END IF;
	IF NVL(:old.CREATOR,0) != NVL(:new.CREATOR,0) THEN
		SELECT f_getuser(:OLD.CREATOR)        INTO OLD_CREATOR  from dual;
		SELECT f_getuser(:NEW.CREATOR)        INTO NEW_CREATOR  from dual;
      pkg_audit_trail_module.sp_column_insert (V_ROWID,'CB_ANTIGEN', 'CREATOR',OLD_CREATOR,NEW_CREATOR,NULL,NULL);
    END IF;
	  IF NVL(:old.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:new.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
      pkg_audit_trail_module.sp_column_insert (V_ROWID,'CB_ANTIGEN', 'CREATED_ON', TO_CHAR(:old.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:new.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;     
    IF NVL(:old.ip_add,' ') != NVL(:new.ip_add,' ') THEN
      pkg_audit_trail_module.sp_column_insert (V_ROWID,'CB_ANTIGEN', 'IP_ADD', :old.ip_add, :new.ip_add,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
	SELECT f_getuser(:OLD.LAST_MODIFIED_BY)        INTO OLD_MODIFIED_BY  from dual;
	SELECT f_getuser(:NEW.LAST_MODIFIED_BY)        INTO NEW_MODIFIED_BY  from dual;
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_ANTIGEN','LAST_MODIFIED_BY',OLD_MODIFIED_BY,NEW_MODIFIED_BY,NULL,NULL);
      END IF; 
    IF NVL(:old.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:new.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
      pkg_audit_trail_module.sp_column_insert (V_ROWID,'CB_ANTIGEN', 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:old.RID,0) != NVL(:new.RID,0) THEN
      pkg_audit_trail_module.sp_column_insert (V_ROWID,'CB_ANTIGEN', 'RID', :OLD.RID, :NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:old.DELETEDFLAG,' ') != NVL(:new.DELETEDFLAG,' ') THEN
	SELECT DECODE(:NEW.DELETEDFLAG,'1','Yes','0','No')		INTO NEW_DELETEDFLAG  FROM DUAL;
	SELECT DECODE(:OLD.DELETEDFLAG,'1','Yes','0','No')		INTO OLD_DELETEDFLAG  FROM DUAL;
      pkg_audit_trail_module.sp_column_insert (V_ROWID,'CB_ANTIGEN', 'DELETEDFLAG', OLD_DELETEDFLAG, NEW_DELETEDFLAG,NULL,NULL);
    end if;	
  end;
	/
	
	INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,245,10,'10_CB_ANTIGEN_AU1.sql',sysdate,'9.0.0 B#637-ET035');
commit;