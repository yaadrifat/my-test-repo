set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_CORD.FK_CORD_CBU_INEL_REASON_INC';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_CORD.FK_CORD_CBU_INELIGIBLE_REASON_INC','Incomplete Reason');
	commit;
  end if;
end;
/
update audit_mappedvalues set to_value= 'Ineligible Reason' where mapping_type = 'Field' and from_value = 'CB_CORD.FK_CORD_CBU_INELIGIBLE_REASON';
update audit_mappedvalues set to_value= 'Unlicensed Reason' where mapping_type = 'Field' and from_value = 'CB_CORD.FK_CORD_CBU_UNLICENSED_REASON';
commit;

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,245,31,'31_AUDIT_MAPPEDVALUES.sql',sysdate,'9.0.0 B#637-ET035');
commit;