--start update cb_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='bld_rlt_diag_rsk_cjd';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; defer.<br/><br/>If Bank did not ask or Mother did not answer on or after 5-25-05 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of August 27, 2007 - question was changed to be consistent with FDA language for evaluation of maternal donor risk for CJD or vCJD.  Previous question combined maternal and family risk.  Consider responses as equivalent to current.' where ques_code='bld_rlt_diag_rsk_cjd';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='cjd_diag_neuro_unk_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; defer.<br/><br/>If Bank did not ask or Mother did not answer on or after 5-25-05 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of August 27, 2007 - question was changed to be consistent with FDA language for evaluation of maternal donor risk for CJD or vCJD.  Previous question combined maternal and family risk.  Consider responses as equivalent to current.' where ques_code='cjd_diag_neuro_unk_ind';
	commit;
  end if;
end;
/
--END--


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,245,40,'40_hotfix1_update_cb_questions.sql',sysdate,'9.0.0 B#637-ET035.01');
commit;