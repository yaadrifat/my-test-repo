create or replace 
PROCEDURE SP_AWAITNIG_HLA_RES
AS
V_ORDERID NUMBER;
V_ORDERTYPE NUMBER;
V_ORDERSTATUS_SH NUMBER;
V_ORDERSTATUS_AWT NUMBER;
BEGIN
SELECT PK_CODELST INTO V_ORDERSTATUS_SH FROM ER_CODELST WHERE CODELST_TYPE='order_status' AND codelst_subtyp='SHIP_SCH';
SELECT PK_CODELST INTO V_ORDERSTATUS_AWT FROM ER_CODELST WHERE CODELST_TYPE='order_status' AND codelst_subtyp='AWA_HLA_RES';
SELECT PK_CODELST INTO V_ORDERTYPE FROM ER_CODELST WHERE CODELST_TYPE='order_type' AND codelst_subtyp='CT';
FOR ORDERS IN (SELECT PK_ORDER,
  ord.package_slip_flag,
  TO_CHAR(SH.SCH_SHIPMENT_DATE,'Mon DD, YYYY'),
  TO_CHAR(SYSDATE,'Mon DD, YYYY')
FROM ER_ORDER ORD,
  CB_SHIPMENT SH
WHERE SH.FK_ORDER_ID=ORD.PK_ORDER
AND ord.order_status=V_ORDERSTATUS_SH
AND TO_CHAR(SH.SCH_SHIPMENT_DATE,'Mon DD, YYYY')<TO_CHAR(SYSDATE,'Mon DD, YYYY') 
AND ord.order_type=V_ORDERTYPE
and ord.prev_ct_done_flag='N')
LOOP
V_ORDERID:=ORDERS.PK_ORDER;
UPDATE ER_ORDER SET ORDER_STATUS=V_ORDERSTATUS_AWT,ORDER_STATUS_DATE=SYSDATE,PACKAGE_SLIP_FLAG='Y' WHERE PK_ORDER=V_ORDERID;
COMMIT;
END LOOP;
END;
/



begin
 DBMS_SCHEDULER.drop_job (job_name => 'CREATE_ALERTS_JOB');
 end;
 /

begin
DBMS_SCHEDULER.create_job (
    job_name        => 'CREATE_ALERTS_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN SP_AWAITNIG_HLA_RES;sp_generate_date_based_alerts;SP_CHANGE_CORD_STATUS;END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'FREQ=DAILY;BYHOUR=0;BYMINUTE=0;BYSECOND=1',
    end_date        => NULL,
    enabled         => TRUE,
    comments        => 'Job to generate date based alerts,to change order status to Awaiting HLA Results and change local cord status when shipment date become past date.');
end;
/

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,271,4,'04_SP_AWAIT_HLA_RES_PROC_AND_JOB.sql',sysdate,'9.0.0 B#637-ET048');
commit;