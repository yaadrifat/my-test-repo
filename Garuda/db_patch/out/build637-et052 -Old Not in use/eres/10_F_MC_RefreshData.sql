declare
cursor minimumcriteriadata is
     select mincriteria.pk_cord_minimum_criteria,mincriteria.FK_CORD_ID,mincriteria.VIAB_TEST_TIMING from cb_cord_minimum_criteria mincriteria;

V_TASK_FLAG NUMBER(1):=0;
begin

for criteriaData in minimumCriteriaData
loop  
  select f_is_new_task_needed(criteriadata.fk_cord_id) into v_task_flag from dual;
  --dbms_output.put_line(criteriadata.fk_cord_id||'-before -'||v_task_flag);
    IF V_TASK_FLAG=1 and  criteriaData.VIAB_TEST_TIMING is null THEN
      update cb_cord_temp_minimum_criteria set viability_result=null where fk_minimum_criteria=criteriadata.pk_cord_minimum_criteria;
      update cb_cord_minimum_criteria set saveinprogess=1,conformed_minimum_critiria=0,second_review_confirmed=0 where PK_CORD_MINIMUM_CRITERIA=criteriadata.pk_cord_minimum_criteria;
      --dbms_output.put_line(criteriadata.fk_cord_id||'--'||v_task_flag);
    end if; 
    commit;
end loop;

end;
/

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,279,10,'10_F_MC_RefreshData.sql',sysdate,'9.0.0 B#637-ET052');
commit;