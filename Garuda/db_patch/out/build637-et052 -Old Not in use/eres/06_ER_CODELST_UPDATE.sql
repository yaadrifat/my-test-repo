--START UPDATE ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'comp_req_info'
    AND codelst_subtyp = 'incomplete';
  If (V_Column_Exists = 1) Then
	Update Er_Codelst Set Codelst_Desc = 'Incomplete' Where Codelst_Type = 'comp_req_info' And Codelst_Subtyp = 'incomplete';
  commit;
   end if;
End;
/

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,279,6,'06_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 B#637-ET052');
commit;