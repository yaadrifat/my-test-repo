create or replace
FUNCTION F_GET_NEW_ELIG_QUES_DATA(V_ENTITY_ID NUMBER) RETURN SYS_REFCURSOR IS                        
ELIG_QUEST_DATA SYS_REFCURSOR;
IS_ELIGIBLE_DATA NUMBER(10):=0;
TASK_FLAG NUMBER(1):=0;
BEGIN

SELECT F_IS_NEW_TASK_NEEDED(V_ENTITY_ID) INTO TASK_FLAG FROM DUAL;


IF TASK_FLAG=1 THEN

OPEN ELIG_QUEST_DATA FOR SELECT 
                                decode(ELIG_QUES.MRQ_NEW_QUES_1,0,'NO',1,'Yes') as MRQ_Q1,
                                NVL(ELIG_QUES.MRQ_NEW_QUES_1_AD_DETAIL,'') as MRQ_Q1_ADD_DETAILS,
                                NVL(ELIG_QUES.MRQ_NEW_QUES_1_A,'') as MRQ_Q1_A,
                                decode(ELIG_QUES.MRQ_NEW_QUES_2,0,'NO',1,'Yes') as MRQ_Q2,
                                NVL(ELIG_QUES.MRQ_NEW_QUES_2_AD_DETAIL ,'') as MRQ_Q2_ADD_DETAILS,
                                decode(ELIG_QUES.MRQ_NEW_QUES_3,0,'NO',1,'Yes') as MRQ_Q3,
                                NVL(ELIG_QUES.MRQ_NEW_QUES_3_AD_DETAIL ,'') as MRQ_Q3_ADD_DETAILS,
                                decode(ELIG_QUES.PHY_NEW_FIND_QUES_4,0,'NO',1,'Yes') as PHY_Q1,
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_4_AD_DETAIL,'')  as PHY_Q1_ADD_DETAILS,
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_4_A,'') as PHY_Q1_A,
                                decode(ELIG_QUES.PHY_NEW_FIND_QUES_5,0,'NO',1,'Yes') as PHY_Q2,
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_5_AD_DETAIL,'') as PHY_Q2_ADD_DETAILS,
                                decode(ELIG_QUES.PHY_NEW_FIND_QUES_6 ,0,'NO',1,'Yes') as PHY_Q3,
                                NVL(ELIG_QUES.PHY_NEW_FIND_QUES_6_AD_DETAIL ,'') as PHY_Q3_ADD_DETAILS,
                                decode(ELIG_QUES.PHY_NEW_FIND_QUES_6_A ,0,'NO',1,'Yes') as PHY_Q3_A,
                                decode(ELIG_QUES.PHY_NEW_FIND_QUES_6_B ,0,'NO',1,'Yes') as PHY_Q3_B,
                                decode(ELIG_QUES.PHY_NEW_FIND_QUES_6_C ,0,'NO',1,'Yes') as PHY_Q3_C,
                                decode(ELIG_QUES.PHY_NEW_FIND_QUES_6_D ,0,'NO',1,'Yes') as PHY_Q3_D,
                                decode(ELIG_QUES.PHY_NEW_FIND_QUES_6_E ,0,'NO',1,'Yes') as PHY_Q3_E,
                                decode(ELIG_QUES.PHY_NEW_FIND_QUES_6_F ,0,'NO',1,'Yes') as PHY_Q3_F,
                                decode(ELIG_QUES.PHY_NEW_FIND_QUES_6_G ,0,'NO',1,'Yes') as PHY_Q3_G,
                                decode(ELIG_QUES.PHY_NEW_FIND_QUES_6_H ,0,'NO',1,'Yes') as PHY_Q3_H,
                                decode(ELIG_QUES.PHY_NEW_FIND_QUES_6_I ,0,'NO',1,'Yes') as PHY_Q3_I,
                                decode(ELIG_QUES.IDM_NEW_QUES_7,0,'NO',1,'Yes')  as IDM_Q1,
                                NVL(ELIG_QUES.IDM_NEW_QUES_7_AD_DETAIL ,'') as IDM_Q1_ADD_DETAILS,
                                NVL(ELIG_QUES.IDM_NEW_QUES_7_A ,'') as IDM_Q1_A,
                                decode(ELIG_QUES.IDM_NEW_QUES_8,0,'NO',1,'Yes') as IDM_Q2,
                                NVL(ELIG_QUES.IDM_NEW_QUES_8_AD_DETAIL ,'') as IDM_Q2_ADD_DETAILS,
                                NVL(ELIG_QUES.IDM_NEW_QUES_8_A ,'') as IDM_Q2_A,
                                decode(ELIG_QUES.IDM_NEW_QUES_9 ,0,'NO',1,'Yes') as IDM_Q3,
                                NVL(ELIG_QUES.IDM_NEW_QUES_9_AD_DETAIL,'') as IDM_Q3_ADD_DETAILS
                            FROM 
                                CB_NEW_FINAL_DECL_ELIG ELIG_QUES 
                            WHERE 
                                ELIG_QUES.ENTITY_ID=V_ENTITY_ID AND ELIG_QUES.PK_NEW_FINAL_DECL_ELIG=(SELECT MAX(PK_NEW_FINAL_DECL_ELIG) FROM CB_NEW_FINAL_DECL_ELIG WHERE ENTITY_ID=V_ENTITY_ID);
                                
                                
                                
END IF;


IF TASK_FLAG=0 THEN

OPEN ELIG_QUEST_DATA FOR  SELECT 
                                decode(ELIG_QUES.MRQ_QUES_1,0,'NO',1,'Yes') AS MRQ_Q1,
                                NVL(ELIG_QUES.MRQ_QUES_1_AD_DETAIL,'') AS MRQ_Q1_ADD_DETAILS,
                                decode(ELIG_QUES.MRQ_QUES_1_A,0,'NO',1,'Yes') AS MRQ_Q1_A,
                                decode(ELIG_QUES.MRQ_QUES_1_B,0,'NO',1,'Yes') AS MRQ_Q1_A_1,
                                decode(ELIG_QUES.MRQ_QUES_2,0,'NO',1,'Yes') AS MRQ_Q2,
                                NVL(ELIG_QUES.MRQ_QUES_2_AD_DETAIL ,'') AS MRQ_Q2_ADD_DETAILS,
                                decode(ELIG_QUES.PHY_FIND_QUES_3,0,'NO',1,'Yes') AS PHY_Q1,
                                NVL(ELIG_QUES.PHY_FIND_QUES_3_AD_DETAIL,'') AS PHY_Q1_ADD_DETAILS,
                                decode(ELIG_QUES.PHY_FIND_QUES_3_A,0,'NO',1,'Yes') AS PHY_Q1_A,
                                decode(ELIG_QUES.PHY_FIND_QUES_3_B,0,'NO',1,'Yes') AS PHY_Q1_A_1,
                                decode(ELIG_QUES.PHY_FIND_QUES_3_C,0,'NO',1,'Yes') AS PHY_Q1_B,
                                decode(ELIG_QUES.PHY_FIND_QUES_3_D ,0,'NO',1,'Yes') AS PHY_Q1_B_1,
                                decode(ELIG_QUES.PHY_FIND_QUES_4,0,'NO',1,'Yes') AS PHY_Q2,
                                NVL(ELIG_QUES.PHY_FIND_QUES_4_AD_DETAIL ,'') AS PHY_Q2_ADD_DETAILS,
                                decode(ELIG_QUES.PHY_FIND_QUES_5 ,0,'NO',1,'Yes') AS PHY_Q3,
                                NVL(ELIG_QUES.PHY_FIND_QUES_5_AD_DETAIL ,'') AS PHY_Q3_ADD_DETAILS,
                                decode(ELIG_QUES.PHY_FIND_QUES_5_A ,0,'NO',1,'Yes') AS PHY_Q3_A,
                                decode(ELIG_QUES.PHY_FIND_QUES_5_B ,0,'NO',1,'Yes') AS PHY_Q3_B,
                                decode(ELIG_QUES.PHY_FIND_QUES_5_C ,0,'NO',1,'Yes') AS PHY_Q3_C,
                                decode(ELIG_QUES.PHY_FIND_QUES_5_D ,0,'NO',1,'Yes') AS PHY_Q3_D,
                                decode(ELIG_QUES.PHY_FIND_QUES_5_E ,0,'NO',1,'Yes') AS PHY_Q3_E,
                                decode(ELIG_QUES.PHY_FIND_QUES_5_F ,0,'NO',1,'Yes') AS PHY_Q3_F,
                                decode(ELIG_QUES.PHY_FIND_QUES_5_G ,0,'NO',1,'Yes') AS PHY_Q3_G,
                                decode(ELIG_QUES.PHY_FIND_QUES_5_H ,0,'NO',1,'Yes') AS PHY_Q3_H,
                                decode(ELIG_QUES.PHY_FIND_QUES_5_I ,0,'NO',1,'Yes') AS PHY_Q3_I,
                                decode(ELIG_QUES.IDM_QUES_6 ,0,'NO',1,'Yes') AS IDM_Q1,
                                NVL(ELIG_QUES.IDM_QUES_6_AD_DETAIL ,'') AS IDM_Q1_ADD_DETAILS,
                                decode(ELIG_QUES.IDM_QUES_6_A ,0,'NO',1,'Yes') AS IDM_Q1_A,
                                decode(ELIG_QUES.IDM_QUES_6_B ,0,'NO',1,'Yes') AS IDM_Q1_A_1,
                                decode(ELIG_QUES.IDM_QUES_7 ,0,'NO',1,'Yes') AS IDM_Q2,
                                NVL(ELIG_QUES.IDM_QUES_7_AD_DETAIL ,'') AS IDM_Q2_ADD_DETAILS,
                                decode(ELIG_QUES.IDM_QUES_7_A ,0,'NO',1,'Yes') AS IDM_Q2_A,
                                decode(ELIG_QUES.IDM_QUES_7_B ,0,'NO',1,'Yes') AS IDM_Q2_A_1,
                                decode(ELIG_QUES.IDM_QUES_8 ,0,'NO',1,'Yes') AS IDM_Q3,
                                NVL(ELIG_QUES.IDM_QUES_8_AD_DETAIL ,'') AS IDM_Q3_ADD_DETAILS
                            FROM 
                                CB_FINAL_DECL_ELIG ELIG_QUES 
                            WHERE 
                                ELIG_QUES.ENTITY_ID=V_ENTITY_ID AND ELIG_QUES.PK_FINAL_DECL_ELIG=(SELECT MAX(PK_FINAL_DECL_ELIG) FROM CB_FINAL_DECL_ELIG WHERE ENTITY_ID=V_ENTITY_ID);

END IF;


RETURN ELIG_QUEST_DATA;

END;
/

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,279,2,'02_F_GET_NEWELIGQUES_DATA.sql',sysdate,'9.0.0 B#637-ET052');
commit;