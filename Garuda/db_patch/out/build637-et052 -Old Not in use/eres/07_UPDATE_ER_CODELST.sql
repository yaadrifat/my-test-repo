--update into Er_Codelst--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'viab_dec'
    AND codelst_subtyp = 'viab_accept';
  if (v_record_exists = 1) then
     UPDATE ER_CODELST SET CODELST_DESC = 'Viability result is &#8805; 75% or other potency measures are acceptable' WHERE codelst_type = 'viab_dec' AND codelst_subtyp = 'viab_accept';
	commit;
  end if;
end;
/
--End--

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,279,7,'07_UPDATE_ER_CODELST.sql',sysdate,'9.0.0 B#637-ET052');
commit;