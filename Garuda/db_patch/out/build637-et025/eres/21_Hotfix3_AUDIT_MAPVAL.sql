set define off;
update audit_mappedvalues set to_value= 'Describe' where mapping_type = 'Field' and from_value = 'ER_PATLABS.CUSTOM005';
update audit_mappedvalues set to_value= 'Describe' where mapping_type = 'Field' and from_value = 'ER_PATLABS.CUSTOM004';
update audit_mappedvalues set to_value= 'Reason for Test' where mapping_type = 'Field' and from_value = 'ER_PATLABS.FK_TEST_REASON';
update audit_mappedvalues set to_value= 'Sample Type' where mapping_type = 'Field' and from_value = 'ER_PATLABS.FK_TEST_SPECIMEN';
update audit_mappedvalues set to_value= 'Rh Type - Please Specify' where mapping_type = 'Field' and from_value = 'CB_CORD.CB_RHTYPE_OTHER_SPEC';
update audit_mappedvalues set to_value= 'Bacterial Culture Comment' where mapping_type = 'Field' and from_value = 'CB_CORD.BACT_COMMENT';
update audit_mappedvalues set to_value= 'Fungal Culture Comment' where mapping_type = 'Field' and from_value = 'CB_CORD.FUNG_COMMENT';
update audit_mappedvalues set to_value= 'Clinical/Progress Notes' where mapping_type = 'Field' and from_value = 'CB_NOTES.NOTES';
commit;

		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,227,21,'21_Hotfix3_AUDIT_MAPVAL.sql',sysdate,'9.0.0 B#637-ET025.03');
		commit;
