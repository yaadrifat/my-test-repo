create or replace
PROCEDURE SP_CHANGE_CORD_STATUS AS
V_SH NUMBER;
V_CORD NUMBER;
VAR_CORDTYPE NUMBER;
PARTIALY_SAVED_CORD NUMBER;
V_ORDER_ID NUMBER;
V_CBB NUMBER;
V_CDRUSER VARCHAR2(2);
BEGIN
SELECT PK_CBU_STATUS INTO V_SH FROM CB_CBU_STATUS CS WHERE CS.CODE_TYPE='Response' AND CS.CBU_STATUS_CODE='SH';
SELECT PK_CODELST INTO VAR_CORDTYPE FROM ER_CODELST WHERE CODELST_TYPE='entity_type' and CODELST_SUBTYP='CBU';
  FOR CRD IN(SELECT 
  DISTINCT HDR.ORDER_ENTITYID CORD_ID,SH.FK_ORDER_ID ORDER_ID,CRD.FK_CBB_ID CBB
FROM 
  CB_SHIPMENT SH, 
  ER_ORDER ORD,
  ER_ORDER_HEADER HDR,
  CB_CORD CRD
WHERE 
  SH.FK_ORDER_ID=ORD.PK_ORDER AND
  ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER AND
  HDR.ORDER_ENTITYID=CRD.PK_CORD AND
  CRD.FK_CORD_CBU_STATUS IN(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS CS WHERE CS.CODE_TYPE='Response' AND CS.CBU_STATUS_CODE NOT IN('SH','IN')) AND
  ORD.ORDER_TYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_type' AND CODELST_SUBTYP='OR') AND
  TO_DATE(TO_CHAR(SH.SHIPMENT_DATE,'Mon DD, YYYY'),'Mon DD, YYYY')<TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY') AND
  ORD.ORDER_STATUS IN(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' AND CODELST_SUBTYP IN('RESERVED','NEW','SHIP_SCH')))
LOOP
V_CORD:=CRD.CORD_ID;
V_ORDER_ID:=CRD.ORDER_ID;
V_CBB:=CRD.CBB;
SELECT NVL(USING_CDR,'0') INTO V_CDRUSER FROM ER_SITE S,CBB CB WHERE CB.FK_SITE=S.PK_SITE AND S.PK_SITE=V_CBB;
IF V_CDRUSER='0' THEN
SELECT NVL(CORD_SEARCHABLE,1) INTO PARTIALY_SAVED_CORD FROM CB_CORD WHERE PK_CORD =V_CORD;

  UPDATE CB_CORD SET FK_CORD_CBU_STATUS=V_SH,LAST_MODIFIED_BY=0,LAST_MODIFIED_DATE=SYSDATE WHERE PK_CORD=V_CORD;
  INSERT INTO CB_ENTITY_STATUS(PK_ENTITY_STATUS,ENTITY_ID,FK_ENTITY_TYPE,STATUS_TYPE_CODE,FK_STATUS_VALUE,STATUS_DATE,STATUS_REMARKS,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,RID,DELETEDFLAG,LICN_ELIG_CHANGE_REASON,ORDER_ID,IS_CORD_ENTRY_DATA) VALUES (SEQ_CB_ENTITY_STATUS.nextval,V_CORD,VAR_CORDTYPE,'cord_status_frmord',V_SH,SYSDATE,NULL,0,SYSDATE,NULL,NULL,NULL,NULL,NULL,NULL,V_ORDER_ID,PARTIALY_SAVED_CORD);
  COMMIT;
  
END IF;
END LOOP;
END;
/

		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,227,18,'18_Hotfix2_SP_CHG_CORD_STATUS.sql',sysdate,'9.0.0 B#637-ET025.02');
		commit;