begin
 DBMS_SCHEDULER.drop_job (job_name => 'CREATE_ALERTS_JOB');
 end;
 /


begin
DBMS_SCHEDULER.create_job (
    job_name        => 'CREATE_ALERTS_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN sp_generate_date_based_alerts;SP_CHANGE_CORD_STATUS;END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'freq=daily;byhour=0;byminute=0;bysecond=1',
    end_date        => NULL,
    enabled         => TRUE,
    comments        => 'Job to generate date based alerts and change local cord status when shipment date become past date.');
end;
/

begin
dbms_scheduler.run_job('CREATE_ALERTS_JOB');
end;
/

		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,227,19,'19_Hotfix2_CREATE_ALERTS_JOB.sql',sysdate,'9.0.0 B#637-ET025.02');
		commit;