--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'source_categ'
    AND codelst_subtyp = 'coop';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'source_categ','coop','COOP','N',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'source_categ'
    AND codelst_subtyp = 'mbr';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'source_categ','mbr','MBR','N',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'source_categ'
    AND codelst_subtyp = 'oth';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE) 
  VALUES(SEQ_ER_CODELST.nextval,'source_categ','oth','OTH','N',NULL,SYSDATE,SYSDATE);
	commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,213,11,'11_hotfix2_codelst_insert.sql',sysdate,'9.0.0 B#637-ET018.02');
		commit;
