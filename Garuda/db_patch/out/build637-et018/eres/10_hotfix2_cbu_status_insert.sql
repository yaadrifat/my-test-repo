--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='46' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE) VALUES(SEQ_CB_CBU_STATUS.nextval,'46','Completed','CloseReason');
	commit;
  end if;
end;
/
--END--
INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,213,10,'10_hotfix2_cbu_status_insert.sql',sysdate,'9.0.0 B#637-ET018.02');
		commit;
