delete er_repxsl where pk_repxsl in (161,167,168,177);
commit;


----UPDATE CBU SUMMARY REPORT SQL------


Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,cord_id_number_on_cbu_bag,f_cord_add_ids(~1) addids,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1,0'')ids_notes,cbbid, cbb_id, storage_loc, cbu_collection_site,race,baby_birth_dt,collec_dt,ethnicity,gender,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1'')cbu_info_notes, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1'') hla_notes,
eligible_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1'') eligibility_notes,bact_cul_strt_dt,fung_cul_strt_dt,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''process_info''),''1'') processing_notes,
hemoglobin_scrn, rh_type, lic_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1'') labsummary_notes, (select f_lab_summary(~1) from dual) lab_summary, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,bag1type,bag2type,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,cryobag_manufac,cryobag_manufac2,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots,filter_paper_sample,rbc_pellates,exacted_dna,serum_sample,plasma_sample,non_viable_cel,nonviable_final_prod,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,F_GET_DYNAMICFORMDATA(~1,''~2'') ~2,F_GET_DYNAMICFORMGRP(''~2'') IDM_GRP,F_GET_CREATELASTMOD(~1,''~2'') IDM_HISTORY,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1'') ~2_NOTES,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''cbu_info_cat''))) cbu_attachments,f_hla_cord(~1,f_codelst_id(''test_source'',''cbu'')) hla,  F_BEST_HLA_CORD(~1,f_codelst_id(''test_source'',''cbu'')) best_hla,f_hla_cord(~1,f_codelst_id(''test_source'',''maternal'')) mhla, F_BEST_HLA_CORD(~1,f_codelst_id(''test_source'',''maternal'')) mbest_hla,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''hla''))) hla_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''eligibile_cat''))) eligible_attachments,F_GET_DISTINCT_HLA_RESP(~1,f_codelst_id(''test_source'',''cbu'')) DIST_CBU,F_GET_DISTINCT_HLA_RESP(~1,f_codelst_id(''test_source'',''maternal'')) MDIST_CBU,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''infect_mark''))) ~2_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsummary_attachments,
createdon,createdBy,lastmodifiedby, last_modified_dt, proc_creator, proc_created_on, proc_last_modby,proc_last_moddt, sample_creator,sample_created_on,sample_last_moddt,F_GET_FORM_TYP_DT(~1,''~2'') IDM_FORM_DATA
FROM rep_cbu_details where pk_cord =~1' where pk_report = 161;
/
	COMMIT;


	Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,cord_id_number_on_cbu_bag,f_cord_add_ids(~1) addids,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1,0'')ids_notes,cbbid, cbb_id, storage_loc, cbu_collection_site,race,baby_birth_dt,collec_dt,ethnicity,gender,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1'')cbu_info_notes, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1'') hla_notes,
eligible_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1'') eligibility_notes,bact_cul_strt_dt,fung_cul_strt_dt,
prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''process_info''),''1'') processing_notes,
hemoglobin_scrn, rh_type, lic_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1'') labsummary_notes, (select f_lab_summary(~1) from dual) lab_summary, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,bag1type,bag2type,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,cryobag_manufac,cryobag_manufac2,
no_of_oth_rep_alliquots_f_prod, no_of_oth_rep_alliq_alter_cond, no_of_serum_mater_aliquots, no_of_plasma_mater_aliquots,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots,filter_paper_sample,rbc_pellates,exacted_dna,serum_sample,plasma_sample,non_viable_cel,nonviable_final_prod,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,F_GET_DYNAMICFORMDATA(~1,''~2'') ~2,F_GET_DYNAMICFORMGRP(''~2'') IDM_GRP,F_GET_CREATELASTMOD(~1,''~2'') IDM_HISTORY,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1'') ~2_NOTES,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''cbu_info_cat''))) cbu_attachments,f_hla_cord(~1,f_codelst_id(''test_source'',''cbu'')) hla,  F_BEST_HLA_CORD(~1,f_codelst_id(''test_source'',''cbu'')) best_hla,f_hla_cord(~1,f_codelst_id(''test_source'',''maternal'')) mhla, F_BEST_HLA_CORD(~1,f_codelst_id(''test_source'',''maternal'')) mbest_hla,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''hla''))) hla_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''eligibile_cat''))) eligible_attachments,F_GET_DISTINCT_HLA_RESP(~1,f_codelst_id(''test_source'',''cbu'')) DIST_CBU,F_GET_DISTINCT_HLA_RESP(~1,f_codelst_id(''test_source'',''maternal'')) MDIST_CBU,
f_get_attachments(~1,(f_codelst_id(''doc_categ'',''infect_mark''))) ~2_attachments, f_get_attachments(~1,(f_codelst_id(''doc_categ'',''lab_sum_cat''))) labsummary_attachments,
createdon,createdBy,lastmodifiedby, last_modified_dt, proc_creator, proc_created_on, proc_last_modby,proc_last_moddt, sample_creator,sample_created_on,sample_last_moddt,F_GET_FORM_TYP_DT(~1,''~2'') IDM_FORM_DATA
FROM rep_cbu_details where pk_cord =~1' where pk_report = 161;
/
	COMMIT;



----UPDATE CBU DETAIL REPORT SQL------



Update ER_REPORT set REP_SQL = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1,0'')ids_notes, cbbid, storage_loc, cbu_collection_site, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1,0'')cbu_info_notes,cord_id_number_on_cbu_bag,f_cord_add_ids(~1) addids,
f_hla_cord(~1,f_codelst_id(''test_source'',''cbu'')) hla,  F_BEST_HLA_CORD(~1,f_codelst_id(''test_source'',''cbu'')) best_hla,f_hla_cord(~1,f_codelst_id(''test_source'',''maternal'')) mhla, F_BEST_HLA_CORD(~1,f_codelst_id(''test_source'',''maternal'')) mbest_hla,F_GET_DISTINCT_HLA_RESP(~1,f_codelst_id(''test_source'',''cbu'')) DIST_CBU,F_GET_DISTINCT_HLA_RESP(~1,f_codelst_id(''test_source'',''maternal'')) MDIST_CBU,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1,0'') hla_notes, eligible_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1,0'') eligibility_notes,race,baby_birth_dt,collec_dt,ethnicity,gender,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
FIVE_HUMAN_ALBU_ML,TWENTYFIVE_HUMAN_ALBU_PER,TWENTYFIVE_HUMAN_ALBU_ML, PLASMALYTE_PER,PLASMALYTE_ML,OTHR_CRYOPROTECTANT_PER,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,five_dextrose_ml,filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
hemoglobin_scrn, rh_type, lic_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1,0'') labsummary_notes, (select f_lab_summary(~1) from dual) lab_summary, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, freezer_manufact, other_freezer_manufact,
FROZEN_IN, OTHER_FROZEN_CONT, NO_OF_BAGS, CRYOBAG_MANUFAC, BAGTYPE, BAG1TYPE, BAG2TYPE,   OTHER_BAG,
storage_temperature, max_vol, ctrl_rate_freezing, individual_frac,CBB_ID,F_GET_FORM_TYP_DT(~1,''~5'') IDM_FORM_DATA,F_GET_FORM_TYP_DT(~1,''~3'') MRQ_FORM_DATA,F_GET_FORM_TYP_DT(~1,''~4'') FMHQ_FORM_DATA,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hlt_his_scrn''),''1'') hsh_notes,
NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots, F_GET_DYNAMICFORMDATA(~1,''~5'') IDM,F_GET_DYNAMICFORMGRP(''~5'') IDM_GRP,F_GET_CREATELASTMOD(~1,''~5'') IDM_HISTORY, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1,0'') idm_notes, 
F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''process_info''),''1,0'') processinfo_notes,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),NULL,''0'',(SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'') ordertypedata,
createdon,createdBy,lastmodifiedby, last_modified_dt, proc_creator,  proc_created_on, proc_last_modby,proc_last_moddt, sample_creator,sample_created_on,sample_last_modby,sample_last_moddt,f_get_dynamicformdata(~1,''~3'') MRQ,f_get_dynamicformgrp(''~3'') GRP,F_GET_CREATELASTMOD(~1,''~3'') HISTORY, f_get_dynamicformdata(~1,''~4'') FMHQ,f_get_dynamicformgrp(''~4'') FMHQ_GRP,F_GET_CREATELASTMOD(~1,''~4'') FMHQ_HISTORY
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 167;
/
	COMMIT;


	Update ER_REPORT set REP_SQL_CLOB = 'select pk_cord, cord_registry_id, registry_maternal_id, cord_local_cbu_id, maternal_local_id, cord_isbi_din_code,
f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1,0'')ids_notes, cbbid, storage_loc, cbu_collection_site, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1,0'')cbu_info_notes,cord_id_number_on_cbu_bag,f_cord_add_ids(~1) addids,
f_hla_cord(~1,f_codelst_id(''test_source'',''cbu'')) hla,  F_BEST_HLA_CORD(~1,f_codelst_id(''test_source'',''cbu'')) best_hla,f_hla_cord(~1,f_codelst_id(''test_source'',''maternal'')) mhla, F_BEST_HLA_CORD(~1,f_codelst_id(''test_source'',''maternal'')) mbest_hla,F_GET_DISTINCT_HLA_RESP(~1,f_codelst_id(''test_source'',''cbu'')) DIST_CBU,F_GET_DISTINCT_HLA_RESP(~1,f_codelst_id(''test_source'',''maternal'')) MDIST_CBU,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1,0'') hla_notes, eligible_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1,0'') eligibility_notes,race,baby_birth_dt,collec_dt,ethnicity,gender,
ineligiblereason,unlicensereason,elgible_modi_dt,lic_modi_dt,lic_flag,eli_flag,eligcomments,prcsng_start_date, bact_culture , bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
FIVE_HUMAN_ALBU_ML,TWENTYFIVE_HUMAN_ALBU_PER,TWENTYFIVE_HUMAN_ALBU_ML, PLASMALYTE_PER,PLASMALYTE_ML,OTHR_CRYOPROTECTANT_PER,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,five_dextrose_ml,filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,
hemoglobin_scrn, rh_type, lic_status, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1,0'') labsummary_notes, (select f_lab_summary(~1) from dual) lab_summary, proc_name, proc_start_date, proc_termi_date, processing, automated_type,
other_processing, product_modification, other_product_modi, storage_method, freezer_manufact, other_freezer_manufact,
FROZEN_IN, OTHER_FROZEN_CONT, NO_OF_BAGS, CRYOBAG_MANUFAC, BAGTYPE, BAG1TYPE, BAG2TYPE,   OTHER_BAG,
storage_temperature, max_vol, ctrl_rate_freezing, individual_frac,CBB_ID,F_GET_FORM_TYP_DT(~1,''~5'') IDM_FORM_DATA,F_GET_FORM_TYP_DT(~1,''~3'') MRQ_FORM_DATA,F_GET_FORM_TYP_DT(~1,''~4'') FMHQ_FORM_DATA,
filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hlt_his_scrn''),''1'') hsh_notes,
NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
no_of_extr_dna_mater_aliquots, no_of_cell_mater_aliquots, F_GET_DYNAMICFORMDATA(~1,''~5'') IDM,F_GET_DYNAMICFORMGRP(''~5'') IDM_GRP,F_GET_CREATELASTMOD(~1,''~5'') IDM_HISTORY, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1,0'') idm_notes, 
F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY, f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''process_info''),''1,0'') processinfo_notes,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),NULL,''0'',(SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'') ordertypedata,
createdon,createdBy,lastmodifiedby, last_modified_dt, proc_creator,  proc_created_on, proc_last_modby,proc_last_moddt, sample_creator,sample_created_on,sample_last_modby,sample_last_moddt,f_get_dynamicformdata(~1,''~3'') MRQ,f_get_dynamicformgrp(''~3'') GRP,F_GET_CREATELASTMOD(~1,''~3'') HISTORY, f_get_dynamicformdata(~1,''~4'') FMHQ,f_get_dynamicformgrp(''~4'') FMHQ_GRP,F_GET_CREATELASTMOD(~1,''~4'') FMHQ_HISTORY
FROM rep_cbu_details where pk_cord = ~1' where pk_report = 167;
/
	COMMIT;


----UPDATE CBU COMPREHENSIVE REPORT SQL------



Update ER_REPORT set REP_SQL = 'SELECT PK_CORD, CORD_REGISTRY_ID, REGISTRY_MATERNAL_ID, CORD_LOCAL_CBU_ID, MATERNAL_LOCAL_ID, CORD_ISBI_DIN_CODE,
CBBID, STORAGE_LOC, CBU_COLLECTION_SITE,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1'')cbu_info_notes, ELIGIBLE_STATUS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1'') eligibility_notes,CORD_ID_NUMBER_ON_CBU_BAG,BABY_BIRTH_DT,ETHNICITY,COLLEC_DT,RACE,GENDER,
prcsng_start_date, bact_culture,bact_cul_strt_dt, bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1'') labsummary_notes, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,NONVIABLE_FINAL_PROD,
HEPARIN_THOU_PER,HEPARIN_THOU_ML,HEPARIN_FIVE_PER,HEPARIN_FIVE_ML,HEPARIN_TEN_PER,HEPARIN_TEN_ML,HEPARIN_SIX_PER,HEPARIN_SIX_ML,CPDA_PER,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1'')ids_notes,
hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
FIVE_HUMAN_ALBU_ML,TWENTYFIVE_HUMAN_ALBU_PER,TWENTYFIVE_HUMAN_ALBU_ML, PLASMALYTE_PER,PLASMALYTE_ML,OTHR_CRYOPROTECTANT_PER,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1'') hla_notes,(select f_lab_summary(~1) from dual) lab_summary,F_GET_FORM_TYP_DT(~1,''~5'') IDM_FORM_DATA,F_GET_FORM_TYP_DT(~1,''~3'') MRQ_FORM_DATA,F_GET_FORM_TYP_DT(~1,''~4'') FMHQ_FORM_DATA,
five_dextrose_ml,filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hlt_his_scrn''),''1'') hsh_notes,
OTHR_DILUENTS_ML,SPEC_OTHR_DILUENTS,PRODUCTCODE,NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
NO_OF_EXTR_DNA_MATER_ALIQUOTS, NO_OF_CELL_MATER_ALIQUOTS,f_hla_cord(~1,f_codelst_id(''test_source'',''cbu'')) hla,  F_BEST_HLA_CORD(~1,f_codelst_id(''test_source'',''cbu'')) best_hla,f_hla_cord(~1,f_codelst_id(''test_source'',''maternal'')) mhla, F_BEST_HLA_CORD(~1,f_codelst_id(''test_source'',''maternal'')) mbest_hla,F_GET_DISTINCT_HLA_RESP(~1,f_codelst_id(''test_source'',''cbu'')) DIST_CBU,F_GET_DISTINCT_HLA_RESP(~1,f_codelst_id(''test_source'',''maternal'')) MDIST_CBU,BACT_CUL_STRT_DT,FUNG_CUL_STRT_DT,CBB_ID,F_CORD_ID_ON_BAG(~1) IDONBAG, F_CORD_ADD_IDS(~1) ADDIDS,
INELIGIBLEREASON,UNLICENSEREASON,ELGIBLE_MODI_DT,LIC_MODI_DT,LIC_FLAG,ELI_FLAG,ELIGCOMMENTS, F_GET_DYNAMICFORMDATA(~1,''~5'') IDM,F_GET_DYNAMICFORMGRP(''~5'') IDM_GRP,F_GET_CREATELASTMOD(~1,''~5'') IDM_HISTORY,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1'') IDM_NOTES,F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),NULL,''0'',(SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'')ordertypedata, createdon,createdBy,lastmodifiedby, last_modified_dt, proc_creator, proc_created_on, proc_last_modby,proc_last_moddt, sample_creator,sample_created_on,sample_last_modby,sample_last_moddt,f_get_dynamicformdata(~1,''~3'') MRQ,f_get_dynamicformgrp(''~3'') GRP,F_GET_CREATELASTMOD(~1,''~3'') HISTORY,f_get_dynamicformdata(~1,''~4'') FMHQ,f_get_dynamicformgrp(''~4'') FMHQ_GRP,F_GET_CREATELASTMOD(~1,''~4'') FMHQ_HISTORY
FROM REP_CBU_DETAILS WHERE PK_CORD = ~1' where pk_report = 168;
/
	COMMIT;


	Update ER_REPORT set REP_SQL_CLOB = 'SELECT PK_CORD, CORD_REGISTRY_ID, REGISTRY_MATERNAL_ID, CORD_LOCAL_CBU_ID, MATERNAL_LOCAL_ID, CORD_ISBI_DIN_CODE,
CBBID, STORAGE_LOC, CBU_COLLECTION_SITE,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''cbu_info''),''1'')cbu_info_notes, ELIGIBLE_STATUS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''eligiblity''),''1'') eligibility_notes,CORD_ID_NUMBER_ON_CBU_BAG,BABY_BIRTH_DT,ETHNICITY,COLLEC_DT,RACE,GENDER,
prcsng_start_date, bact_culture,bact_cul_strt_dt, bact_comment, frz_date, fungal_culture, fung_comment, abo_blood_type,
hemoglobin_scrn, rh_type, lic_status,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''lab_sum''),''1'') labsummary_notes, processing, automated_type,
other_processing, product_modification, other_product_modi, individual_frac, no_of_bags, other_bag,NONVIABLE_FINAL_PROD,
HEPARIN_THOU_PER,HEPARIN_THOU_ML,HEPARIN_FIVE_PER,HEPARIN_FIVE_ML,HEPARIN_TEN_PER,HEPARIN_TEN_ML,HEPARIN_SIX_PER,HEPARIN_SIX_ML,CPDA_PER,
cpda_ml,cpd_per,cpd_ml,acd_per,acd_ml,othr_anti_per,othr_anti_ml,speci_othr_anti,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''id''),''1'')ids_notes,
hun_dmso_per,hun_dmso_ml,hun_glycerol_per,hun_glycerol_ml,ten_dextran_40_per,ten_dextran_40_ml,five_human_albu_per,
FIVE_HUMAN_ALBU_ML,TWENTYFIVE_HUMAN_ALBU_PER,TWENTYFIVE_HUMAN_ALBU_ML, PLASMALYTE_PER,PLASMALYTE_ML,OTHR_CRYOPROTECTANT_PER,
othr_cryoprotectant_ml,spec_othr_cryoprotectant,five_dextrose_per,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hla''),''1'') hla_notes,(select f_lab_summary(~1) from dual) lab_summary,F_GET_FORM_TYP_DT(~1,''~5'') IDM_FORM_DATA,F_GET_FORM_TYP_DT(~1,''~3'') MRQ_FORM_DATA,F_GET_FORM_TYP_DT(~1,''~4'') FMHQ_FORM_DATA,
five_dextrose_ml,filter_paper, rpc_pellets, extr_dna, serum_aliquotes, plasma_aliquotes, nonviable_aliquotes, viable_samp_final_prod, no_of_segments,point_nine_nacl_per,point_nine_nacl_ml,othr_diluents_per,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hlt_his_scrn''),''1'') hsh_notes,
OTHR_DILUENTS_ML,SPEC_OTHR_DILUENTS,PRODUCTCODE,NO_OF_OTH_REP_ALLIQUOTS_F_PROD, NO_OF_OTH_REP_ALLIQ_ALTER_COND, NO_OF_SERUM_MATER_ALIQUOTS, NO_OF_PLASMA_MATER_ALIQUOTS,
NO_OF_EXTR_DNA_MATER_ALIQUOTS, NO_OF_CELL_MATER_ALIQUOTS,f_hla_cord(~1,f_codelst_id(''test_source'',''cbu'')) hla,  F_BEST_HLA_CORD(~1,f_codelst_id(''test_source'',''cbu'')) best_hla,f_hla_cord(~1,f_codelst_id(''test_source'',''maternal'')) mhla, F_BEST_HLA_CORD(~1,f_codelst_id(''test_source'',''maternal'')) mbest_hla,F_GET_DISTINCT_HLA_RESP(~1,f_codelst_id(''test_source'',''cbu'')) DIST_CBU,F_GET_DISTINCT_HLA_RESP(~1,f_codelst_id(''test_source'',''maternal'')) MDIST_CBU,BACT_CUL_STRT_DT,FUNG_CUL_STRT_DT,CBB_ID,F_CORD_ID_ON_BAG(~1) IDONBAG, F_CORD_ADD_IDS(~1) ADDIDS,
INELIGIBLEREASON,UNLICENSEREASON,ELGIBLE_MODI_DT,LIC_MODI_DT,LIC_FLAG,ELI_FLAG,ELIGCOMMENTS, F_GET_DYNAMICFORMDATA(~1,''~5'') IDM,F_GET_DYNAMICFORMGRP(''~5'') IDM_GRP,F_GET_CREATELASTMOD(~1,''~5'') IDM_HISTORY,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1'') IDM_NOTES,F_CBU_HISTORY(~1) CBUHISTORY,F_CBU_REQ_HISTORY(~1,~2) CBUREQHISTORY,
DECODE((SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),NULL,''0'',(SELECT F_CODELST_DESC(ORD.ORDER_TYPE) FROM ER_ORDER ORD WHERE ORD.PK_ORDER=~2),''1'')ordertypedata, createdon,createdBy,lastmodifiedby, last_modified_dt, proc_creator, proc_created_on, proc_last_modby,proc_last_moddt, sample_creator,sample_created_on,sample_last_modby,sample_last_moddt,f_get_dynamicformdata(~1,''~3'') MRQ,f_get_dynamicformgrp(''~3'') GRP,F_GET_CREATELASTMOD(~1,''~3'') HISTORY,f_get_dynamicformdata(~1,''~4'') FMHQ,f_get_dynamicformgrp(''~4'') FMHQ_GRP,F_GET_CREATELASTMOD(~1,''~4'') FMHQ_HISTORY
FROM REP_CBU_DETAILS WHERE PK_CORD = ~1' where pk_report = 168;
/
	COMMIT;



----UPDATE EVALUATION HISTORY REPORT SQL------



Update ER_REPORT set REP_SQL = 'SELECT
CORD.CORD_REGISTRY_ID REGID,
CORD.CORD_LOCAL_CBU_ID LOCALCBUID,
CORD.CORD_ISBI_DIN_CODE ISTBTDIN,
CORD.REGISTRY_MATERNAL_ID MATREGID,
F_CORD_ADD_IDS(~1) ADDIDS,
F_CORD_ID_ON_BAG(~1) IDONBAG,
SITE.SITE_NAME SITENAME,
SITE.SITE_ID SITEID,
F_CORD_ELIGBLE_HISTORY(~1) ELIGHSTRY,
F_CORD_LICENCE_HISTORY(~1) LICHSTRY,CORD_ID_NUMBER_ON_CBU_BAG,
F_CORD_FINAL_REVIEW(~1) FINALREVIEW,
RECIPIENT_INFO.RECEIPANT_ID PATIENT_ID
FROM
CB_CORD CORD
LEFT OUTER JOIN er_order_receipant_info ORDER_RECIPIENT_INFO
  ON (ORDER_RECIPIENT_INFO.FK_ORDER_ID=~2)
LEFT OUTER JOIN CB_RECEIPANT_INFO RECIPIENT_INFO
  ON (RECIPIENT_INFO.PK_RECEIPANT = ORDER_RECIPIENT_INFO.FK_RECEIPANT)
LEFT OUTER JOIN
ER_SITE SITE ON(CORD.FK_CBB_ID=SITE.PK_SITE)
WHERE CORD.PK_CORD=~1' where pk_report = 177;
/
	COMMIT;


	Update ER_REPORT set REP_SQL_CLOB = 'SELECT
CORD.CORD_REGISTRY_ID REGID,
CORD.CORD_LOCAL_CBU_ID LOCALCBUID,
CORD.CORD_ISBI_DIN_CODE ISTBTDIN,
CORD.REGISTRY_MATERNAL_ID MATREGID,
F_CORD_ADD_IDS(~1) ADDIDS,
F_CORD_ID_ON_BAG(~1) IDONBAG,
SITE.SITE_NAME SITENAME,
SITE.SITE_ID SITEID,
F_CORD_ELIGBLE_HISTORY(~1) ELIGHSTRY,
F_CORD_LICENCE_HISTORY(~1) LICHSTRY,CORD_ID_NUMBER_ON_CBU_BAG,
F_CORD_FINAL_REVIEW(~1) FINALREVIEW,
RECIPIENT_INFO.RECEIPANT_ID PATIENT_ID
FROM
CB_CORD CORD
LEFT OUTER JOIN er_order_receipant_info ORDER_RECIPIENT_INFO
  ON (ORDER_RECIPIENT_INFO.FK_ORDER_ID=~2)
LEFT OUTER JOIN CB_RECEIPANT_INFO RECIPIENT_INFO
  ON (RECIPIENT_INFO.PK_RECEIPANT = ORDER_RECIPIENT_INFO.FK_RECEIPANT)
LEFT OUTER JOIN
ER_SITE SITE ON(CORD.FK_CBB_ID=SITE.PK_SITE)
WHERE CORD.PK_CORD=~1' where pk_report = 177;
/
	COMMIT;
INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,213,4,'04_ER_REPORT.sql',sysdate,'9.0.0 B#637-ET018');
		commit;