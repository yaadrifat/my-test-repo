create or replace
FUNCTION F_GET_ASSES_FLAG_LAB_SUMMARY(PK_CORDID NUMBER) RETURN NUMBER IS
FUNG_NOTDONE NUMBER;
HEMO_RES_DATA NUMBER;
CBU_ENTITY_TYPE NUMBER;
LS_SUB_ENT_TYPE NUMBER;
ASSES_FUNG_CUL NUMBER;
ASSES_HEMOG_TEST NUMBER;
RESULT_DATA1 NUMBER;
RESULT_DATA2 NUMBER;
FINAL_RESULT NUMBER:=0;
BEGIN
    SELECT PK_CODELST INTO FUNG_NOTDONE FROM ER_CODELST WHERE CODELST_TYPE='fung_cul' AND CODELST_SUBTYP='not_done';
    SELECT PK_CODELST INTO CBU_ENTITY_TYPE FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU';
    SELECT PK_CODELST INTO LS_SUB_ENT_TYPE FROM ER_CODELST WHERE CODELST_TYPE='sub_entity_type' AND CODELST_SUBTYP='LAB_SUM';
    SELECT NVL(HEMOGLOBIN_SCRN,0) INTO HEMO_RES_DATA FROM CB_CORD WHERE PK_CORD IN (PK_CORDID);

    --DBMS_OUTPUT.PUT_LINE('HEMORESP::::'||HEMO_RES_DATA);

    SELECT COUNT(*) INTO ASSES_FUNG_CUL FROM CB_ASSESSMENT ASSESS WHERE ASSESS.ENTITY_ID=PK_CORDID AND ASSESS.ENTITY_TYPE=CBU_ENTITY_TYPE AND ASSESS.SUB_ENTITY_TYPE=LS_SUB_ENT_TYPE AND ASSESS.ASSESSMENT_REASON=FUNG_NOTDONE;
    SELECT COUNT(*) INTO ASSES_HEMOG_TEST FROM CB_ASSESSMENT ASSESS WHERE ASSESS.ENTITY_ID=PK_CORDID AND ASSESS.ENTITY_TYPE=CBU_ENTITY_TYPE AND ASSESS.SUB_ENTITY_TYPE=LS_SUB_ENT_TYPE AND ASSESS.ASSESSMENT_REASON=HEMO_RES_DATA;


    SELECT CASE
           WHEN CRD.FK_CORD_FUNGAL_CUL_RESULT IS NOT NULL AND  CRD.FK_CORD_FUNGAL_CUL_RESULT!=-1  AND CRD.FK_CORD_FUNGAL_CUL_RESULT!=FUNG_NOTDONE THEN 1
           WHEN CRD.FK_CORD_FUNGAL_CUL_RESULT IS NOT NULL AND  CRD.FK_CORD_FUNGAL_CUL_RESULT!=-1  AND CRD.FK_CORD_FUNGAL_CUL_RESULT=FUNG_NOTDONE THEN
           CASE
               WHEN ASSES_FUNG_CUL>0 THEN 1
               ELSE 0
               END
           WHEN CRD.FK_CORD_FUNGAL_CUL_RESULT IS NULL OR  CRD.FK_CORD_FUNGAL_CUL_RESULT!=-1 THEN 0
           END INTO RESULT_DATA1
    FROM CB_CORD CRD WHERE CRD.PK_CORD IN (PK_CORDID);

    SELECT CASE
           WHEN CRD.HEMOGLOBIN_SCRN IS NOT NULL AND CRD.HEMOGLOBIN_SCRN NOT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE IN ('hem_path_scrn') AND CODELST_SUBTYP IN ('hemo_trait','homo_no_dis','alp_thal_sil','alp_thal_trait','not_done','unknown')) THEN 1
           WHEN CRD.HEMOGLOBIN_SCRN IS NOT NULL AND CRD.HEMOGLOBIN_SCRN IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE IN ('hem_path_scrn') AND CODELST_SUBTYP IN ('hemo_trait','homo_no_dis','alp_thal_sil','alp_thal_trait','not_done','unknown')) THEN
           CASE
               WHEN ASSES_HEMOG_TEST>0 THEN 1
               ELSE 0
           END
           WHEN CRD.HEMOGLOBIN_SCRN IS NULL THEN 0
           END INTO RESULT_DATA2
    FROM CB_CORD CRD WHERE CRD.PK_CORD IN (PK_CORDID);

    IF RESULT_DATA1=1 AND RESULT_DATA2=1 THEN
       FINAL_RESULT:=1;
    END IF;


    --DBMS_OUTPUT.PUT_LINE('FINAL RESULT::::'||RESULT_DATA1||'RESULT2 DATA::::'||RESULT_DATA2||'::FINAL RESULT:::'||FINAL_RESULT);

    RETURN FINAL_RESULT;
END;
/


create or replace
FUNCTION F_LABSUMMARY_COMPL_CHECK(PK_CORDID NUMBER) RETURN NUMBER IS 
  IS_LAB_SUMMARY_DATA NUMBER;
  LAB_SUMMARY_COMP_FLAG1 NUMBER(2):=0;
  LAB_SUMMARY_COMP_FLAG2 NUMBER(2):=0;
  LAB_SUMMARY_COMP_FLAG3 NUMBER(2):=0;
  LAB_SUMMARY_COMP_FLAG4 NUMBER(2):=0;
  PRE_PROCESSING_VAL NUMBER;
  POST_PROCESSING_VAL NUMBER;
  PRE_POST_PROCESSING_THAW_VAL NUMBER;
  FKTEST_FOR_PRE_PROC VARCHAR2(100);
  FKTEST_FOR_POST_PROC VARCHAR2(100);
  FKTEST_METHODS_NEEDED VARCHAR2(100);
  countval number(10):=0;
  LAB_SUMMARY_COMP_FLAG NUMBER(2):=0;
  PRE_POST_THAW_VALUE NUMBER(2):=0;
  RESULT_ONLY_NEEDED VARCHAR2(100);
BEGIN
  
  
  SELECT
  CASE
    WHEN CRD.PRCSNG_START_DATE        IS NOT NULL
    AND CRD.FRZ_DATE                  IS NOT NULL
    AND CRD.FK_CORD_ABO_BLOOD_TYPE    IS NOT NULL
    AND (CRD.FK_CORD_BACT_CUL_RESULT   IS NOT NULL AND CRD.FK_CORD_BACT_CUL_RESULT!=-1)
    AND CRD.BACT_CULT_DATE            IS NOT NULL
    AND CRD.FUNG_CULT_DATE            IS NOT NULL
    AND F_GET_ASSES_FLAG_LAB_SUMMARY(PK_CORDID) !=0 
    AND F_GET_RH_TYP_OTHR(PK_CORDID)  !=0
    THEN 1
    ELSE 0
  END LABSUMMARY1
  INTO LAB_SUMMARY_COMP_FLAG1
  FROM CB_CORD CRD
  WHERE CRD.PK_CORD IN (PK_CORDID);
                          
  --DBMS_OUTPUT.PUT_LINE('LAB_SUMMAR_COMP1_FLAG VALUE FOR SYSCORD::::::::::::'||LAB_SUMMARY_COMP_FLAG1);

  SELECT COUNT(*)
  INTO IS_LAB_SUMMARY_DATA
  FROM er_patlabs
  WHERE fk_specimen IN
    (SELECT fk_specimen_id FROM cb_cord WHERE pk_cord IN (PK_CORDID)
    );
  --DBMS_OUTPUT.PUT_LINE('IS_LAB_SUMMARY_DATA'|| IS_LAB_SUMMARY_DATA);
  IF IS_LAB_SUMMARY_DATA>0 THEN
  
  
  select pk_codelst INTO PRE_PROCESSING_VAL from er_codelst where codelst_type='timing_of_test' and codelst_subtyp='pre_procesing';
  select pk_codelst INTO POST_PROCESSING_VAL from er_codelst where codelst_type='timing_of_test' and codelst_subtyp='post_procesing';
  select pk_codelst INTO PRE_POST_PROCESSING_THAW_VAL from er_codelst where codelst_type='timing_of_test' and codelst_subtyp='post_proc_thaw';
  
  --DBMS_OUTPUT.PUT_LINE('IS_LAB_SUMMARY_DATA'|| IS_LAB_SUMMARY_DATA);
  
  SELECT PK_LABTEST INTO FKTEST_FOR_PRE_PROC FROM ER_LABTEST WHERE LABTEST_NAME='CBU volume (without anticoagulant/additives)' AND LABTEST_SHORTNAME='CBU_VOL';
  select rowtocol('SELECT PK_LABTEST FROM ER_LABTEST WHERE LABTEST_SHORTNAME in (''TCDAD'',''CNRBC'',''PERNRBC'',''CFUCNT'',''VIAB'')',',')  into FKTEST_FOR_POST_PROC from dual;
  select rowtocol('SELECT PK_LABTEST FROM ER_LABTEST WHERE LABTEST_SHORTNAME in (''CFUCNT'',''VIAB'')',',')  into FKTEST_METHODS_NEEDED from dual;
  select rowtocol('SELECT PK_LABTEST FROM ER_LABTEST WHERE LABTEST_SHORTNAME in (''TCDAD'',''CNRBC'',''PERNRBC'')',',') into RESULT_ONLY_NEEDED FROM DUAL;
  
  
  FOR LSUMM_DATA IN (SELECT TESTVAL.LABTEST_SEQ quest_seq,
      TESTVAL.LABTEST_NAME test_name,
      TESTVAL.LABTEST_SHORTNAME,
      LABSUMM.TEST_DATE test_date,
      F_CODELST_DESC(LABSUMM.FK_TIMING_OF_TEST) test_type,
      LABSUMM.TEST_RESULT test_result,
      NVL(F_CODELST_DESC(LABSUMM.FK_TEST_METHOD),'0') test_method,
      NVL(LABSUMM.CUSTOM005,'0') method_desc_c5,
      NVL(F_CODELST_DESC(LABSUMM.FK_TEST_REASON),'0') reason_for_test,
      NVL(LABSUMM.CUSTOM004,'0') reason_desc_c4,
      NVL(F_CODELST_DESC(LABSUMM.FK_TEST_SPECIMEN),'0') sample_type,
      NOTES TEST_METHO_OTHER_DESC,
      LABSUMM.CUSTOM001,
      LABSUMM.CUSTOM002,
      LABSUMM.CUSTOM003,
      LABSUMM.CUSTOM006,
      LABSUMM.CUSTOM007,
      labsumm.fk_test_outcome,
      LABSUMM.FK_TEST_REASON,
      LABSUMM.FK_TESTGROUP,
      LABSUMM.PK_PATLABS,
      nvl(LABSUMM.FK_TIMING_OF_TEST,'0') TIMING_OF_TEST,
      LABSUMM.FK_TESTID TESTID
    FROM ER_PATLABS LABSUMM
    LEFT OUTER JOIN ER_LABTEST TESTVAL
    ON (LABSUMM.FK_TESTID    =TESTVAL.PK_LABTEST)
    WHERE LABSUMM.FK_SPECIMEN=(SELECT FK_SPECIMEN_ID FROM CB_CORD WHERE PK_CORD IN (PK_CORDID))
    ORDER BY LABSUMM.FK_TIMING_OF_TEST,
      LABSUMM.TEST_DATE,
      TESTVAL.LABTEST_SEQ)
      
      LOOP
         countval:=countval+1;
         
         
         IF LSUMM_DATA.TIMING_OF_TEST!='0' AND LSUMM_DATA.test_date IS NOT NULL AND LSUMM_DATA.TIMING_OF_TEST = PRE_PROCESSING_VAL THEN
         
             --DBMS_OUTPUT.PUT_LINE('INSIDE IF 1:::::::::::::::');
         
             IF  LSUMM_DATA.TESTID=FKTEST_FOR_PRE_PROC AND LSUMM_DATA.test_result IS NOT NULL THEN
                  LAB_SUMMARY_COMP_FLAG2:=1;
                  --DBMS_OUTPUT.PUT_LINE('DATA IS THERE FOR * QUES IN SEC1:::::::::::::::'||LSUMM_DATA.test_name);
             END IF;
             
             IF  LSUMM_DATA.TESTID=FKTEST_FOR_PRE_PROC AND LSUMM_DATA.test_result IS NULL THEN
                  LAB_SUMMARY_COMP_FLAG2:=0;
                  --DBMS_OUTPUT.PUT_LINE('EXIT FOR TEST IN SEC1:::::::::::::::'||LSUMM_DATA.test_name);
                  EXIT;
             END IF;
         
         END IF;
         
         
         
         IF LSUMM_DATA.TIMING_OF_TEST!='0' AND LSUMM_DATA.test_date IS NOT NULL AND LSUMM_DATA.TIMING_OF_TEST = POST_PROCESSING_VAL THEN
         
             --DBMS_OUTPUT.PUT_LINE('INSIDE IF 2:::::::::::::::QUES CHECKING:::'||LSUMM_DATA.test_name||':::test id:::'||LSUMM_DATA.TESTID);
         
             IF  instr(FKTEST_FOR_POST_PROC,LSUMM_DATA.TESTID)!=0 THEN
                  --DBMS_OUTPUT.PUT_LINE('Inside passing of 1st if');
             
                  IF LSUMM_DATA.test_result IS NOT NULL THEN
                    --DBMS_OUTPUT.PUT_LINE('Inside passing of 2nd if');
                    IF INSTR(RESULT_ONLY_NEEDED,LSUMM_DATA.TESTID)!=0 THEN
                      LAB_SUMMARY_COMP_FLAG3:=1;
                      --DBMS_OUTPUT.PUT_LINE('DATA IS THERE FOR * QUES IN SEC2.1:::::::::::::::'||LSUMM_DATA.test_name);
                    END IF;
                    IF INSTR(RESULT_ONLY_NEEDED,LSUMM_DATA.TESTID)=0  AND LSUMM_DATA.test_method!='0' THEN
                      IF(LSUMM_DATA.TEST_METHOD='Other') AND LSUMM_DATA.method_desc_c5!='0' THEN
                      LAB_SUMMARY_COMP_FLAG3:=1;
                      END IF;
                      IF (LSUMM_DATA.TEST_METHOD!='Other') THEN
                      LAB_SUMMARY_COMP_FLAG3:=1;
                      END IF;
                      --DBMS_OUTPUT.PUT_LINE('DATA IS THERE FOR * QUES IN SEC2.2:::::::::::::::'||LSUMM_DATA.test_name);
                    END IF;
                  END IF;
                  
                  IF LSUMM_DATA.test_result IS NULL OR LSUMM_DATA.test_result IS NOT NULL THEN
                    
                    IF INSTR(RESULT_ONLY_NEEDED,LSUMM_DATA.TESTID)=0 AND ( LSUMM_DATA.test_method='0' OR LSUMM_DATA.test_result IS NULL )THEN
                        
                        IF(LSUMM_DATA.TEST_METHOD='Other') AND LSUMM_DATA.method_desc_c5='0' THEN
                          LAB_SUMMARY_COMP_FLAG3:=0;
                          EXIT;
                        END IF;
                        IF(LSUMM_DATA.TEST_METHOD!='Other' AND LSUMM_DATA.test_method='0') THEN
                          LAB_SUMMARY_COMP_FLAG3:=0;
                          EXIT;
                        END IF;
                    
                      --DBMS_OUTPUT.PUT_LINE('EXIT FOR QUESTION IN SEC2.1:::::::::::::::'|| LSUMM_DATA.test_name);
                    
                     END IF;
                    
                      IF  INSTR(RESULT_ONLY_NEEDED,LSUMM_DATA.TESTID)!=0 AND LSUMM_DATA.test_result IS NULL THEN
                        LAB_SUMMARY_COMP_FLAG3:=0;
                        --DBMS_OUTPUT.PUT_LINE('EXIT FOR QUESTION IN SEC2.2:::::::::::::::'|| LSUMM_DATA.test_name);
                        EXIT;
                      END IF;
                  END IF;
                  
             END IF;
         
         END IF;
         
         
         IF LSUMM_DATA.TIMING_OF_TEST!='0' AND LSUMM_DATA.test_date IS NOT NULL AND LSUMM_DATA.TIMING_OF_TEST = PRE_POST_PROCESSING_THAW_VAL AND LSUMM_DATA.test_result IS NOT NULL  THEN
         
                  --DBMS_OUTPUT.PUT_LINE('INSIDE IF 3:::::::::::::::QUEST NAME:::::'||LSUMM_DATA.test_name);
                  PRE_POST_THAW_VALUE:=1;
         
                  
                  IF LSUMM_DATA.test_result IS NOT NULL AND LSUMM_DATA.test_method!='0' AND LSUMM_DATA.reason_for_test!='0' AND LSUMM_DATA.sample_type !='0' THEN
                  
                      IF LSUMM_DATA.test_method='Other' AND LSUMM_DATA.method_desc_c5!='0' THEN
                      LAB_SUMMARY_COMP_FLAG4:=1;
                      --DBMS_OUTPUT.PUT_LINE('DATA IS THERE FOR * QUES IN SEC3.1:::::::::::::::'||LSUMM_DATA.test_name);
                      END IF;
                      IF LSUMM_DATA.reason_for_test='Other' AND LSUMM_DATA.reason_desc_c4!='0' THEN
                      LAB_SUMMARY_COMP_FLAG4:=1;
                      --DBMS_OUTPUT.PUT_LINE('DATA IS THERE FOR * QUES IN SEC3.2:::::::::::::::'||LSUMM_DATA.test_name);
                      END IF;           
			
		      IF LSUMM_DATA.test_method!='Other' AND LSUMM_DATA.reason_for_test!='Other' THEN
                      LAB_SUMMARY_COMP_FLAG4:=1;
                      END IF;           
                      
                  END IF;
                  
                  
                 IF (LSUMM_DATA.test_result IS NULL)  OR (LSUMM_DATA.test_method='0')  OR (LSUMM_DATA.reason_for_test ='0') OR LSUMM_DATA.sample_type='0' OR LSUMM_DATA.method_desc_c5='0' OR LSUMM_DATA.reason_desc_c4='0' THEN
                 
                    IF LSUMM_DATA.test_method='Other' AND LSUMM_DATA.method_desc_c5='0' THEN
                      LAB_SUMMARY_COMP_FLAG4:=0;
                      --DBMS_OUTPUT.PUT_LINE('EXIT FOR QUESTION IN SEC3.1:::::::::::::::'|| LSUMM_DATA.test_name);
                      EXIT;
                    END IF;
                    
                    IF LSUMM_DATA.reason_for_test='Other' AND LSUMM_DATA.reason_desc_c4='0' THEN
                       LAB_SUMMARY_COMP_FLAG4:=0;
                       --DBMS_OUTPUT.PUT_LINE('EXIT FOR QUESTION IN SEC3.2:::::::::::::::'|| LSUMM_DATA.test_name);
                       EXIT;
                    END IF;            
                    
                    IF LSUMM_DATA.test_method!='Other' AND LSUMM_DATA.reason_for_test!='Other' THEN
                       LAB_SUMMARY_COMP_FLAG4:=0;
                       --DBMS_OUTPUT.PUT_LINE('EXIT FOR QUESTION IN SEC3.3:::::::::::::::'|| LSUMM_DATA.test_name);
                       EXIT;
                    END IF;
                    
                    
                   
                  END IF;
                  
         END IF;
         
         
         
      END LOOP;
  
      --DBMS_OUTPUT.PUT_LINE('FINAL LAB SUMMARY COMPLETED FLAG1::'||LAB_SUMMARY_COMP_FLAG1); 
      --DBMS_OUTPUT.PUT_LINE('FINAL LAB SUMMARY COMPLETED FLAG2::'||LAB_SUMMARY_COMP_FLAG2);
      --DBMS_OUTPUT.PUT_LINE('FINAL LAB SUMMARY COMPLETED FLAG3::'||LAB_SUMMARY_COMP_FLAG3);
      --DBMS_OUTPUT.PUT_LINE('FINAL LAB SUMMARY COMPLETED FLAG4::'||LAB_SUMMARY_COMP_FLAG4);
      --DBMS_OUTPUT.PUT_LINE('FINAL PRE_POST_THAW_VALUE::'||PRE_POST_THAW_VALUE);
      
      IF LAB_SUMMARY_COMP_FLAG1=1 AND LAB_SUMMARY_COMP_FLAG2=1 AND LAB_SUMMARY_COMP_FLAG3=1 THEN
       
          IF PRE_POST_THAW_VALUE=1 AND LAB_SUMMARY_COMP_FLAG4=1 THEN 
             LAB_SUMMARY_COMP_FLAG:=1; 
          END IF;
          
          IF PRE_POST_THAW_VALUE=0 THEN 
             LAB_SUMMARY_COMP_FLAG:=1; 
          END IF;
       
      
      END IF;
      --DBMS_OUTPUT.PUT_LINE('INSIDE LOOP::::::::::::'||countval);
      --DBMS_OUTPUT.PUT_LINE('LAB SUMMARY PASSED OR NOT::::::::::::'||LAB_SUMMARY_COMP_FLAG);
    
  END IF;
  RETURN LAB_SUMMARY_COMP_FLAG;
END;
/
COMMIT;
INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,213,14,'14_hotfix2_CRI_autoCheck.sql',sysdate,'9.0.0 B#637-ET018.02');
		commit;