update audit_mappedvalues set to_value='CBU Registry ID' where mapping_type='Field' and from_value='CB_CORD.CORD_REGISTRY_ID';
commit;

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,263,3,'03_AUDIT_MAPPEDVALUES.sql',sysdate,'9.0.0 B#637-ET044');
commit;