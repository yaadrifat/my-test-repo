/* This readMe is specific to Velos eResearch Ver 1.2.7 (Build#ET043.10) */
===================================================================================================================================== 
There are some critical issues reported in  EmTrax application in the Build#ET043.

1.  22109 - Import two cords using a single file with same Maternal Id fails.
2.  21519 - CDR - CBU with Local Status OT - users without auto-defer override privileges (Group Rights) should not be able to change the Local Status of the CBU (in the Static Pane)
3.  21521 - CDR - CBU with Local Status DD or OT: when adding an Assessment of Defer or another Auto-Defer the CBU's Local Status must not change
4.  21831	EmTrax is not properly setting the Pregnancy Reference Number (preg_ref_num)	
5.  22390	Auto deferral is not working for Multiple Trait in Lab Summary widget.
6.  22395	Cord OT Auto-Deferral for Processing Information changes status to DD
7.  22398	Missing Save button on the E-sign pop up from the CBU Entry Page - Not Available CBU's widget 
8.  22429	Incorrect Change Sequence on CBU Information Audit Trail
9.  22430	Local Status  not getting updated on the static  pane for the following scenario
10. 22424  System change for OT/DD statuses not sending ChangeCBUSupplierStatus message



Due to above issues  we have made a hot fix(10) to Build#ET043. 

The hot fix contains  1 Jar file, 11 Jsp Files ,5 Class Files, two XML file  & 3  data base scripts.

 Files to be released:
 
 1. Jar files:
    1. server/eresearch/deploy/velos.ear/velos.war/WEB-INF/lib/garuda1_business-1.0.jar		

 2. JSP FILE
 1.server/eresearch/deploy/velos.ear/velos.war/jsp/cb_cbuCordInformation.jsp
 2.server/eresearch/deploy/velos.ear/velos.war/jsp/cb_idmTest.jsp
 3.server/eresearch/deploy/velos.ear/velos.war/jsp/cb_load_static_panel.jsp
 4.server/eresearch/deploy/velos.ear/velos.war/jsp/cb_cord_detail_with_rights.jsp
 5.server/eresearch/deploy/velos.ear/velos.war/jsp/modal/cb_cbuassessment.jsp
 6.server/eresearch/deploy/velos.ear/velos.war/jsp/modal/cb_cord-entry_modellabsummary.jsp
 7.server/eresearch/deploy/velos.ear/velos.war/jsp/modal/cb_cord-entry_modeltestsummary.jsp
 8.server/eresearch/deploy/velos.ear/velos.war/jsp/modal/cb_update-CbuStatus.jsp
 9.server/eresearch/deploy/velos.ear/velos.war/jsp/modal/cb_update-characteristics.jsp
10.server/eresearch/deploy/velos.ear/velos.war/jsp/cord-entry/cb_cord-entry_labsummary.jsp
11.server/eresearch/deploy/velos.ear/velos.war/jsp/cord-entry/cb_comp-reqinfo-lab-Tests.jsp
12.server/eresearch/deploy/velos.ear/velos.war/jsp/modal/cb_clinical_note.jsp
13.server/eresearch/deploy/velos.ear/velos.war/jsp/modal/cb_editor.jsp
14.server/eresearch/deploy/velos.ear/velos.war/jsp/modal/cb_update-procedure.jsp
15.server/eresearch/deploy/velos.ear/velos.war/jsp/cb_detail_clinical.jsp
16.server/eresearch/deploy/velos.ear/velos.war/jsp/cb_includes.jsp
17.server/eresearch/deploy/velos.ear/velos.war/jsp/modal/cb_update_proc_info.jsp

 3. JAVA Class Files:
 1. server/eresearch/deploy/velos.ear/velos.war/WEB-INF/classes/com/velos/ordercomponent/action/CBUAction.class
 2. server/eresearch/deploy/velos.ear/velos.war/WEB-INF/classes/com/velos/ordercomponent/action/DynaFormAction.class
 3. server/eresearch/deploy/velos.ear/velos.war/WEB-INF/classes/com/velos/ordercomponent/action/CordImportAction.class
 4. server/eresearch/deploy/velos.ear/velos.war/WEB-INF/classes/com/velos/ordercomponent/controller/CBUController.class
 5. server/eresearch/deploy/velos.ear/velos.war/WEB-INF/classes/com/velos/ordercomponent/util/VelosWebserviceXmlConstants.class
 6. server/eresearch/deploy/velos.ear/velos.war/WEB-INF/classes/com/velos/ordercomponent/util/VelosGarudaConstants.class
 7. server/eresearch/deploy/velos.ear/velos.war/WEB-INF/classes/com/velos/ordercomponent/action/ServerOrderBy.class

4. XML files:
 1. server/eresearch/deploy/ velos.ear/velos.war/WEB-INF/classes/struts-garuda.xml
 2. server/eresearch/deploy/velos.ear/velos.war/jsp/xml/releaseinfo.xml

5. DB Script files:
	eres:
	
	    1. 30_hotfix10_CreateNonSystemUser.sql
		2. 31_hotfix9_er_version.sql
		
	esch:
	
    	2. 11_hotfix9_er_version.sql 
	epat:
	
		  3. 11_hotfix9_er_version.sql




Please replace the files in Build #637- ET043 with the files in the above mentioned path. Also,Please execute the DB patch on Database Server.
===================================================================================================================================== 