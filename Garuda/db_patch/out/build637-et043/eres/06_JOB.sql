begin
 DBMS_SCHEDULER.drop_job (job_name => 'ER_NOTIFICATION_BACKUP_JOB');
 end;
 /

begin
DBMS_SCHEDULER.create_job (
    job_name        => 'ER_NOTIFICATION_BACKUP_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN SP_NOTIFICATION_BACKUP;SP_ALERT_INSTANCES_BACKUP;END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'FREQ=WEEKLY;BYDAY=SUN;BYHOUR=00;BYMINUTE=00;BYSECOND=1',
    end_date        => NULL,
    enabled         => TRUE,
    comments        => 'Job to take backup of ER_NOTIFICATION TABLE and CB_ALERT_INSTANCES tables');
end;
/

begin
dbms_scheduler.run_job('ER_NOTIFICATION_BACKUP_JOB');
end;
/



INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,261,6,'06_JOB.sql',sysdate,'9.0.0 B#637-ET043');