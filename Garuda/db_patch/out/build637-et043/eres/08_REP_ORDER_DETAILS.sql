CREATE OR REPLACE FORCE VIEW "ERES"."REP_ORDER_DETAILS" 
AS
  SELECT ord.pk_order pk_order,
    ordh.order_entityid order_cordid,
    CASE
      WHEN f_codelst_desc(ord.order_type)='CT'
      AND ORD.order_sample_at_lab        ='Y'
      THEN 'CT - Sample at Lab'
      WHEN f_codelst_desc(ord.order_type)='CT'
      AND ORD.order_sample_at_lab        ='N'
      THEN 'CT - Ship Sample'
      ELSE f_codelst_desc(ord.order_type)
    END req_type,
    NVL(TO_CHAR(ord.order_status_date,'Mon DD, YYYY'),'Not Provided') order_status_date,
    f_codelst_desc(ord.order_type) requ_type,
    HEA.ORDER_OPEN_DATE,
    to_date(TO_CHAR(sysdate,'Mon DD, YYYY'),'Mon DD, YYYY')-to_date(TO_CHAR(ordh.order_open_date,'Mon DD, YYYY'),'Mon DD, YYYY') days_active,
    TO_CHAR(ordh.order_open_date,'Mon DD, YYYY') order_request_dt,
    f_codelst_desc(ord.order_priority) priority,
    DECODE(ord.PREV_CT_DONE_FLAG,'Y','Yes', 'N','No', NULL,'') previous_cted,
    f_codelst_desc(ord.order_status) order_status,
    NVL(TO_CHAR(INFU.ind_sponser),'Not Provided') ind_sponser,
    NVL(TO_CHAR(INFU.ind_number),'Not Provided') ind_number,
    NVL(TO_CHAR(INFU.DISEASE_STAGE),'Not Provided') disease_stage,
    CASE
         WHEN ORD.ORDER_LAST_VIEWED_BY IS NULL 
	 THEN '' 
	 WHEN ORD.ORDER_LAST_VIEWED_DATE-NVL(ORD.LAST_MODIFIED_DATE,TO_DATE('01/01/1900','MM/DD/YYYY'))>0 
	 THEN lst_review.usr_logname
	 WHEN ORD.LAST_MODIFIED_DATE-ORD.ORDER_LAST_VIEWED_DATE>0 
	 THEN LST_MODIFY.usr_logname
         ELSE lst_review.usr_logname
    END LAST_REVIEW_BY,
    CASE 
	WHEN ORD.ORDER_LAST_VIEWED_DATE IS NULL
	THEN '' 
	WHEN ORD.ORDER_LAST_VIEWED_DATE-NVL(ORD.LAST_MODIFIED_DATE,TO_DATE('01/01/1900','MM/DD/YYYY'))>0
	THEN NVL(TO_CHAR(ord.ORDER_LAST_VIEWED_DATE,'Mon DD, YYYY/HH24:MI'),'Not Available')
	WHEN ORD.LAST_MODIFIED_DATE-ORD.ORDER_LAST_VIEWED_DATE>0 
	THEN NVL(TO_CHAR(ord.LAST_MODIFIED_DATE,'Mon DD, YYYY/HH24:MI'),'Not Available') 
        ELSE NVL(TO_CHAR(ord.ORDER_LAST_VIEWED_DATE,'Mon DD, YYYY/HH24:MI'),'Not Available')
    END last_review_date,
    NVL(assigned_to.usr_logname,'Unassigned') ASSIGNED_TO_USER,
    NVL(TO_CHAR(ORD.ORDER_ASSIGNED_DATE,'Mon DD, YYYY'),'Not Available') assigned_dt,
    TO_CHAR(ord.order_resol_date, 'Mon DD, YYYY') order_resolution_dt,
    resol.cbu_status_desc RESOLUTION,
    NVL(TO_CHAR(ORD.RESULT_REC_DATE,'Mon DD, YYYY'),'Not Received') result_received_dt,
    TO_CHAR(SHIPMENT.sch_shipment_date,'Mon DD, YYYY') ct_ship_dt,
    close_reason.cbu_status_desc ORDER_CLOSE_REASON,
    RECIPIENT_INFO.RECEIPANT_ID PATIENT_ID,
    RECIPIENT_INFO.PK_RECEIPANT PATIENT_ENTITY_ID,
    TRUNC(MONTHS_BETWEEN (SYSDATE,PERS.PERSON_DOB) / 12) AGE,
    F_CODELST_DESC(PERS.FK_CODELST_GENDER) PATIENT_GENDER,
    TRANS.SITE_NAME TRAN_CENTER,
    TRANS.SITE_ID TRANS_ID,
    NVL(RECIPIENT_INFO.CURRENT_DIAGNOSIS,'Not Provided') curr_diagnosis,
    SEC_TRANS.SITE_ID SEC_TRANS_ID,
    CMINFO.USER_LOGINID CM_ID,
    CMINFO.USER_FNAME
    ||''
    ||CMINFO.USER_LNAME CM_NAME,
    CMINFO.USER_MAILID CM_EMAIL,
    USER_CONTACTNO CM_PH,
    CASE
      WHEN ORD.IS_CORD_AVAIL_FOR_NMDP='Y'
      THEN 'Yes'
      WHEN ORD.IS_CORD_AVAIL_FOR_NMDP='N'
      THEN 'No'
      ELSE 'Not Provided'
    END CBU_AVAIL_DATA,
    to_date(TO_CHAR(ord.cord_avail_confirm_date,'Mon DD, YYYY'),'Mon DD, YYYY')-to_date(TO_CHAR(ordh.order_open_date,'Mon DD, YYYY'),'Mon DD, YYYY')no_of_days,
    TO_CHAR(ord.CORD_AVAILABILITY_CONFORMEDON,'Mon DD, YYYY') cord_conformed_on,
    (SELECT usr_logname
    FROM er_user
    WHERE pk_user IN (NVL(ord.CORD_AVAILABILITY_CONFORMEDBY,0))
    ) CONFORMED_BY,
    CASE
      WHEN ord.COMPLETE_REQ_INFO_TASK_FLAG='Y'
      THEN 'Completed'
      WHEN ((ord.COMPLETE_REQ_INFO_TASK_FLAG IS NULL
      OR ord.COMPLETE_REQ_INFO_TASK_FLAG      ='N')
      AND (SELECT COUNT(*)
        FROM cb_cord_complete_req_info
        WHERE FK_CORD_ID=ordh.ORDER_ENTITYID) > 0)
      THEN 'In progress'
      ELSE 'Not Started'
    END complete_req_info_stat,
    DECODE(ord.ORDER_ACK_FLAG,'Y','Complete', NULL,'Not Complete', 'N','Not Complete') order_acked,
    TO_CHAR(ord.ORDER_ACK_DATE, 'Mon DD, YYYY') order_ack_dt,
    (SELECT usr_logname
    FROM er_user
    WHERE pk_user IN (NVL(ord.ORDER_ACK_BY,0))
    ) order_ack_by,
    TO_CHAR(shipment.SCH_SHIPMENT_DATE,'Day') ct_ship_day,
    F_CODELST_DESC(FK_SHIP_COMPANY_ID) SHIPPING_COMPANY,
    SHIPMENT_TRACKING_NO TRACKING_NO,
    F_CODELST_DESC(ORD.FK_SAMPLE_TYPE_AVAIL) SAMPLE_TYPE,
    F_CODELST_DESC(ord.fk_aliquots_type) ALIQUOTS_TYPE,
    F_CODELST_DESC(ord.lab_code) LAB_CODE,
    to_date(TO_CHAR(shipment.shipment_date,'Mon DD, YYYY'),'Mon DD, YYYY')    -to_date(TO_CHAR(ordh.order_open_date,'Mon DD, YYYY'),'Mon DD, YYYY') OR_SHIP_NO_OF_DAYS,
    to_date(TO_CHAR(shipment.sch_shipment_date,'Mon DD, YYYY'),'Mon DD, YYYY')-to_date(TO_CHAR(ordh.order_open_date,'Mon DD, YYYY'),'Mon DD, YYYY') CT_SHIP_NO_OF_DAYS,
    DECODE(RECENT_HLA_TYPING_AVAIL,'Y','Yes', 'N','No', NULL,'') hla_typing_avail,
    f_codelst_desc(SWITCH_REASON) SWITCH_REASON,
    CASE
      WHEN ord.FINAL_REVIEW_TASK_FLAG='Y'
      THEN 'Completed'
      WHEN ((ord.FINAL_REVIEW_TASK_FLAG IS NULL
      OR ord.FINAL_REVIEW_TASK_FLAG      ='N')
      AND (SELECT COUNT(*)
        FROM cb_cord_final_review
        WHERE FK_CORD_ID=ordh.ORDER_ENTITYID) > 0)
      THEN 'In progress'
      ELSE 'Not Started'
    END final_review_stat,
    SCH_SHIPMENT_TIME shipment_time_or,
    f_codelst_desc(FK_SHIPPING_CONT) shipment_cont,
    NVL(TO_CHAR(EXPECT_ARR_DATE,'Mon DD, YYYY'),'Not Provided') anticipated_arr_dt,
    NVL(TO_CHAR(INFU.PROP_INFUSION_DATE,'Mon DD, YYYY'),'Not Provided') prop_infusion_dt,
    NVL(TO_CHAR(INFU.PROP_PREP_DATE,'Mon DD, YYYY'),'Not Provided') prop_prep_dt,
    NVL(TO_CHAR(INFU.PROPOSED_SHIP_DTE,'Mon DD, YYYY'),'Not Provided') prop_ship_dt,
    NVL(TO_CHAR(ORD.INFUSION_DATE,'Mon DD, YYYY'),'Not Provided') infusion_dt,
    NVL(TO_CHAR(INFU.FK_LEU_CURR_STATUS),'Not Provided') leuk_curr_status,
    NVL(TO_CHAR(INFU.NO_OF_REMISSIONS),'Not Provided') no_of_remissions,
    NVL(TO_CHAR(INFU.REC_TRANSFUSED),'Not Provided') rec_transfused,
    NVL(TO_CHAR(INFU.REC_WEIGHT),'Not Provided') rec_weight,
    f_codelst_desc(FK_TEMPERATURE_MONITER) temp_monitor,
    NVL(TO_CHAR(SHIPMENT_DATE,'Mon DD, YYYY'),'Not Provided') actual_ship_dt,
    PADLOCK_COMBINATION padlock_combi,
    ADDITI_SHIPPER_DET add_ship_data,
    f_codelst_desc(PAPERWORK_LOCATION) paper_locat,
    (SELECT usr_firstname
      ||' '
      || usr_lastname
    FROM er_user
    WHERE pk_user IN (NVL(shipment.SHIPMENT_CONFIRM_BY,0))
    ) SHIPMENT_CONFORMED_BY,
    TO_CHAR(SHIPMENT_CONFIRM_ON,'Mon DD, YYYY HH:MM AM') shipment_conformed_dt,
    (SELECT TO_CHAR(NMDP_SHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY')
    FROM cb_shipment NMDP_SHIPMENT
    WHERE NMDP_SHIPMENT.FK_ORDER_ID     =ORD.PK_ORDER
    AND NMDP_SHIPMENT.FK_SHIPMENT_TYPE IN
      (SELECT PK_CODELST
      FROM ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='NMDP'
      )
    ) nmdp_ship_dt,
    (SELECT f_codelst_desc(NMDP_SHIPMENT.FK_SHIP_COMPANY_ID)
    FROM cb_shipment NMDP_SHIPMENT
    WHERE NMDP_SHIPMENT.FK_ORDER_ID     =ORD.PK_ORDER
    AND NMDP_SHIPMENT.FK_SHIPMENT_TYPE IN
      (SELECT PK_CODELST
      FROM ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='NMDP'
      )
    ) nmdp_ship_comp,
    (SELECT NMDP_SHIPMENT.SHIPMENT_TRACKING_NO
    FROM cb_shipment NMDP_SHIPMENT
    WHERE NMDP_SHIPMENT.FK_ORDER_ID     =ORD.PK_ORDER
    AND NMDP_SHIPMENT.FK_SHIPMENT_TYPE IN
      (SELECT PK_CODELST
      FROM ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='NMDP'
      )
    ) nmdp_tracking_no,
    (SELECT DECODE(NMDP_SHIPMENT.NMDP_SHIP_EXCUSE_FLAG,'Y','Yes', 'N','No', NULL,'')
    FROM cb_shipment NMDP_SHIPMENT
    WHERE NMDP_SHIPMENT.FK_ORDER_ID     =ORD.PK_ORDER
    AND NMDP_SHIPMENT.FK_SHIPMENT_TYPE IN
      (SELECT PK_CODELST
      FROM ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='NMDP'
      )
    ) nmdp_excuse_flag,
    (SELECT
      (SELECT usr_firstname
        ||' '
        || usr_lastname
      FROM er_user
      WHERE pk_user IN (NVL(NMDP_SHIPMENT.NMDP_SHIP_EXCUSE_SUBMITTED_BY,0))
      )
    FROM cb_shipment NMDP_SHIPMENT
    WHERE NMDP_SHIPMENT.FK_ORDER_ID     =ORD.PK_ORDER
    AND NMDP_SHIPMENT.FK_SHIPMENT_TYPE IN
      (SELECT PK_CODELST
      FROM ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='NMDP'
      )
    ) nmdp_shipment_execused_by,
    (SELECT TO_CHAR(NMDP_SHIPMENT.NMDP_SHIP_EXCUSE_SUBMITTED_ON,'Mon DD, YYYY')
    FROM cb_shipment NMDP_SHIPMENT
    WHERE NMDP_SHIPMENT.FK_ORDER_ID     =ORD.PK_ORDER
    AND NMDP_SHIPMENT.FK_SHIPMENT_TYPE IN
      (SELECT PK_CODELST
      FROM ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='NMDP'
      )
    ) nmdp_excused_on,
    CASE
      WHEN ord.FK_SAMPLE_TYPE_AVAIL=
        (SELECT pk_codelst
        FROM er_codelst
        WHERE codelst_type='cbu_sample_type'
        AND codelst_subtyp='ALQT'
        )
      THEN '1'
      ELSE '0'
    END is_sample_ali
  FROM er_order ord
  LEFT OUTER JOIN er_order_header ordh
  ON (ord.fk_order_header= ordh.pk_order_header)
  LEFT OUTER JOIN ER_USER LST_REVIEW
  ON (ord.order_last_viewed_by=LST_REVIEW.PK_USER)
  LEFT OUTER JOIN ER_USER LST_MODIFY
  ON (ord.LAST_MODIFIED_BY=LST_MODIFY.PK_USER)
  LEFT OUTER JOIN ER_USER ASSIGNED_TO
  ON (ord.assigned_to=ASSIGNED_TO.PK_USER)
  LEFT OUTER JOIN CB_CBU_STATUS RESOL
  ON (ORD.FK_ORDER_RESOL_BY_TC=RESOL.PK_CBU_STATUS)
  LEFT OUTER JOIN CB_SHIPMENT SHIPMENT
  ON (SHIPMENT.FK_ORDER_ID       =ORD.PK_ORDER
  AND SHIPMENT.FK_SHIPMENT_TYPE IN
    (SELECT PK_CODELST
    FROM ER_CODELST
    WHERE CODELST_TYPE='shipment_type'
    AND CODELST_SUBTYP='CBU'
    ))
  LEFT OUTER JOIN CB_CBU_STATUS CLOSE_REASON
  ON (ORDH.ORDER_CLOSE_REASON=CLOSE_REASON.PK_CBU_STATUS)
  LEFT OUTER JOIN er_order_receipant_info ORDER_RECIPIENT_INFO
  ON (ORDER_RECIPIENT_INFO.FK_ORDER_ID=ORD.PK_ORDER)
  LEFT OUTER JOIN CB_RECEIPANT_INFO RECIPIENT_INFO
  ON (RECIPIENT_INFO.PK_RECEIPANT = ORDER_RECIPIENT_INFO.FK_RECEIPANT)
  LEFT OUTER JOIN PERSON PERS
  ON (RECIPIENT_INFO.FK_PERSON_ID=PERS.PK_PERSON)
  LEFT OUTER JOIN ER_SITE TRANS
  ON (RECIPIENT_INFO.TRANS_CENTER_ID=TRANS.PK_SITE)
  LEFT OUTER JOIN ER_SITE SEC_TRANS
  ON (RECIPIENT_INFO.SEC_TRANS_CENTER_ID=SEC_TRANS.PK_SITE)
  LEFT OUTER JOIN ER_ORDER_USERS CMINFO
  ON (ordh.ORDER_REQUESTED_BY=CMINFO.PK_ORDER_USER)
  LEFT OUTER JOIN ER_ORDER_RECEIPANT_INFO INFU
  ON (ord.PK_ORDER=INFU.FK_ORDER_ID)
  LEFT OUTER JOIN ER_ORDER_HEADER HEA
  ON(ord.fk_order_header=HEA.PK_ORDER_HEADER);
/


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,261,8,'08_REP_ORDER_DETAILS.sql',sysdate,'9.0.0 B#637-ET043');
  