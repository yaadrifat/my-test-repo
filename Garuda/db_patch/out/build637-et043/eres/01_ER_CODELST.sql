--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'cbu_sample_type'
    AND codelst_subtyp = 'FPFS';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_CUSTOM_COL1='NO_OF_SEG_AVAIL' where codelst_type = 'cbu_sample_type'
    AND codelst_subtyp = 'FPFS';
	commit;
  end if;
end;
/


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,261,1,'01_ER_CODELST.sql',sysdate,'9.0.0 B#637-ET043');
commit;
--END--