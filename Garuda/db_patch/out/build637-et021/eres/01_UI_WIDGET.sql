--START UPDATE UI_WIDGET--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'cbbProfDiv';
  if (v_record_exists = 1) then
    update UI_WIDGET set RESIZE_MAX_HEIGHT=1024,RESIZE_MAX_WIDTH=1280,RESIZE_MIN_WIDTH=200 where WIDGET_DIV_ID = 'cbbProfDiv';
commit;
  end if;
end;
/


--START UPDATE UI_WIDGET--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'allOrdersDiv';
  if (v_record_exists = 1) then
    update UI_WIDGET set RESIZE_MAX_HEIGHT=1024,RESIZE_MAX_WIDTH=1280 where WIDGET_DIV_ID = 'allOrdersDiv';
commit;
  end if;
end;
/


--START UPDATE UI_WIDGET--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'shipmentDiv';
  if (v_record_exists = 1) then
    update UI_WIDGET set RESIZE_MAX_HEIGHT=1024,RESIZE_MAX_WIDTH=1280,RESIZE_MIN_WIDTH=200 where WIDGET_DIV_ID = 'shipmentDiv';
commit;
  end if;
end;
/


--START UPDATE UI_WIDGET--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'workflowOrdDiv';
  if (v_record_exists = 1) then
    update UI_WIDGET set RESIZE_MAX_HEIGHT=1024,RESIZE_MAX_WIDTH=1280 where WIDGET_DIV_ID = 'workflowOrdDiv';
commit;
  end if;
end;
/


--START UPDATE UI_WIDGET--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'taskLstDiv';
  if (v_record_exists = 1) then
    update UI_WIDGET set RESIZE_MAX_HEIGHT=1024,RESIZE_MAX_WIDTH=1280 where WIDGET_DIV_ID = 'taskLstDiv';
commit;
  end if;
end;
/


--START UPDATE UI_WIDGET--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'activeOrdDiv';
  if (v_record_exists = 1) then
    update UI_WIDGET set RESIZE_MAX_HEIGHT=1024,RESIZE_MAX_WIDTH=1280 where WIDGET_DIV_ID = 'activeOrdDiv';
commit;
  end if;
end;
/


--START UPDATE UI_WIDGET--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'otherOrdDiv';
  if (v_record_exists = 1) then
    update UI_WIDGET set RESIZE_MAX_HEIGHT=1024,RESIZE_MAX_WIDTH=1280 where WIDGET_DIV_ID = 'otherOrdDiv';
commit;
  end if;
end;
/
		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,219,1,'01_UI_WIDGET.sql',sysdate,'9.0.0 B#637-ET021');
		commit;