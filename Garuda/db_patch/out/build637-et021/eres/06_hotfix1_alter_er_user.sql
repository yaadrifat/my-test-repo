--Alter Table Script   for Bug-13541
set define off;
DECLARE
  v_column_exists number;  
BEGIN
	Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'ER_USER'
    AND COLUMN_NAME = 'USR_CURRNTSESSID';
	
	if (v_column_exists = 0) then
		execute immediate 'ALTER TABLE ER_USER ADD (USR_CURRNTSESSID  VARCHAR(50))';
	else
		dbms_output.put_line('Column is already added in ER_USER.');
	end if;
END;
/

COMMENT ON COLUMN ER_USER.USR_CURRNTSESSID IS 'This column stores the Current Logged In Session ID';


INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,219,6,'06_hotfix1_alter_er_user.sql',sysdate,'9.0.0 B#637-ET021.01');
		commit;