--START UPDATE CB_CBU_STATUS--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'HE' AND CODE_TYPE='Resolution';
  if (v_record_exists = 1) then
    update CB_CBU_STATUS set CBU_STATUS_DESC='HE Requested (HE)' where CBU_STATUS_CODE = 'HE' AND CODE_TYPE='Resolution';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'CT' AND CODE_TYPE='Resolution';
  if (v_record_exists = 1) then
    update CB_CBU_STATUS set CBU_STATUS_DESC='CT Requested (CT)' where CBU_STATUS_CODE = 'CT' AND CODE_TYPE='Resolution';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'OR' AND CODE_TYPE='Resolution';
  if (v_record_exists = 1) then
    update CB_CBU_STATUS set CBU_STATUS_DESC='OR Requested (OR)' where CBU_STATUS_CODE = 'OR' AND CODE_TYPE='Resolution';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'RCT' AND CODE_TYPE='Resolution';
  if (v_record_exists = 1) then
    update CB_CBU_STATUS set CBU_STATUS_DESC='Repeat CT (RCT)' where CBU_STATUS_CODE = 'RCT' AND CODE_TYPE='Resolution';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'RHE' AND CODE_TYPE='Resolution';
  if (v_record_exists = 1) then
    update CB_CBU_STATUS set CBU_STATUS_DESC='Repeat HE (RHE)' where CBU_STATUS_CODE = 'RHE' AND CODE_TYPE='Resolution';
commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'RSN' AND CODE_TYPE='Resolution';
  if (v_record_exists = 1) then
    update CB_CBU_STATUS set CBU_STATUS_DESC='Reserved for NMDP (RSN)' where CBU_STATUS_CODE = 'RSN' AND CODE_TYPE='Resolution';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'NA' AND CODE_TYPE='Resolution';
  if (v_record_exists = 1) then
    update CB_CBU_STATUS set CBU_STATUS_DESC='Not Available (NA)' where CBU_STATUS_CODE = 'NA' AND CODE_TYPE='Resolution';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'SH' AND CODE_TYPE='Resolution';
  if (v_record_exists = 1) then
    update CB_CBU_STATUS set CBU_STATUS_DESC='Shipped (SH)' where CBU_STATUS_CODE = 'SH' AND CODE_TYPE='Resolution';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'AV' AND CODE_TYPE='Resolution';
  if (v_record_exists = 1) then
    update CB_CBU_STATUS set CBU_STATUS_DESC='Available (AV)' where CBU_STATUS_CODE = 'AV' AND CODE_TYPE='Resolution';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'AC' AND CODE_TYPE='Resolution';
  if (v_record_exists = 1) then
    update CB_CBU_STATUS set CBU_STATUS_DESC='Active (AC)' where CBU_STATUS_CODE = 'AC' AND CODE_TYPE='Resolution';
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'CC' AND CODE_TYPE='Resolution';
  if (v_record_exists = 1) then
    update CB_CBU_STATUS set CBU_STATUS_DESC='Unknown Code (CC)' where CBU_STATUS_CODE = 'CC' AND CODE_TYPE='Resolution';
commit;
  end if;
end;
/

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,219,14,'14_hotfix2_update_Cbustatus.sql',sysdate,'9.0.0 B#637-ET021.02');
		commit;