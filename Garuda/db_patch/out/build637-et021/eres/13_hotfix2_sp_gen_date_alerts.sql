create or replace
procedure sp_generate_date_based_alerts as
v_entity_order number;
v_orderid number;
begin
select pk_codelst into v_entity_order from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER';
 for orders in(SELECT pk_order
FROM er_order,
  er_order_header
WHERE er_order.fk_order_header=er_order_header.pk_order_header
AND TO_DATE(TO_CHAR(ER_ORDER_HEADER.ORDER_OPEN_DATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')<-2
AND (ER_ORDER.FINAL_REVIEW_TASK_FLAG='N'
OR ER_ORDER.FINAL_REVIEW_TASK_FLAG IS NULL)
AND ER_ORDER.ORDER_TYPE=(SELECT PK_CODELST
  FROM ER_CODELST
  WHERE CODELST_TYPE='order_type'
  AND CODELST_SUBTYP='OR'
  )
AND ER_ORDER.ORDER_STATUS NOT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' and CODELST_SUBTYP IN('close_ordr','resolved'))
UNION
SELECT pk_order
FROM er_order,
  er_order_header
WHERE er_order.fk_order_header=er_order_header.pk_order_header
AND TO_DATE(TO_CHAR(ER_ORDER_HEADER.ORDER_OPEN_DATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')<-7
AND (ER_ORDER.FINAL_REVIEW_TASK_FLAG='N'
OR ER_ORDER.FINAL_REVIEW_TASK_FLAG IS NULL)
AND ER_ORDER.ORDER_TYPE=(SELECT PK_CODELST
  FROM ER_CODELST
  WHERE CODELST_TYPE='order_type'
  AND CODELST_SUBTYP='OR'
  )
AND ER_ORDER.ORDER_STATUS NOT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' and CODELST_SUBTYP IN('close_ordr','resolved'))
UNION
SELECT pk_order
FROM er_order,
  er_order_header,
  cb_shipment
WHERE er_order.fk_order_header=er_order_header.pk_order_header
AND cb_shipment.fk_order_id=er_order.pk_order
AND ER_ORDER.ORDER_TYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_type' and CODELST_SUBTYP ='OR')
AND CB_SHIPMENT.FK_SHIPMENT_TYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='shipment_type' and CODELST_SUBTYP ='CBU')
AND TO_DATE(TO_CHAR(ER_ORDER_HEADER.ORDER_OPEN_DATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')<-2
AND (CB_SHIPMENT.SHIPMENT_CONFIRM='N'
OR CB_SHIPMENT.SHIPMENT_CONFIRM IS NULL)
AND ER_ORDER.ORDER_STATUS NOT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' and CODELST_SUBTYP IN('close_ordr','resolved'))
UNION
SELECT pk_order
FROM er_order,
  er_order_header,
  cb_shipment
WHERE er_order.fk_order_header=er_order_header.pk_order_header
AND cb_shipment.fk_order_id=er_order.pk_order
AND ER_ORDER.ORDER_TYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_type' and CODELST_SUBTYP ='OR')
AND CB_SHIPMENT.FK_SHIPMENT_TYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='shipment_type' and CODELST_SUBTYP ='CBU')
AND TO_DATE(TO_CHAR(ER_ORDER_HEADER.ORDER_OPEN_DATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')<-7
AND (CB_SHIPMENT.SHIPMENT_CONFIRM='N'
OR CB_SHIPMENT.SHIPMENT_CONFIRM IS NULL)
AND ER_ORDER.ORDER_STATUS NOT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' and CODELST_SUBTYP IN('close_ordr','resolved'))
UNION
SELECT pk_order
FROM er_order,
  er_order_header,
  cb_shipment
WHERE er_order.fk_order_header=er_order_header.pk_order_header
AND cb_shipment.fk_order_id=er_order.pk_order
AND ER_ORDER.RECENT_HLA_TYPING_AVAIL='Y'
AND ER_ORDER.ADDITI_TYPING_FLAG='Y'
AND (ER_ORDER.RECENT_HLA_ENTERED_FLAG IS NULL
OR ER_ORDER.RECENT_HLA_ENTERED_FLAG='N')
AND TO_DATE(TO_CHAR(CB_SHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')>1
AND ER_ORDER.ORDER_STATUS NOT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' and CODELST_SUBTYP IN('close_ordr','resolved'))
UNION
SELECT pk_order
FROM er_order,
  er_order_header,
  cb_shipment
WHERE er_order.fk_order_header=er_order_header.pk_order_header
AND cb_shipment.fk_order_id=er_order.pk_order
AND TO_DATE(TO_CHAR(ER_ORDER_HEADER.ORDER_OPEN_DATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')<-2
AND ER_ORDER.ORDER_TYPE=(SELECT PK_CODELST
  FROM ER_CODELST
  WHERE CODELST_TYPE='order_type'
  AND CODELST_SUBTYP='CT'
  )
AND CB_SHIPMENT.FK_SHIPMENT_TYPE=(SELECT PK_CODELST
  FROM ER_CODELST
  WHERE CODELST_TYPE='shipment_type'
  AND CODELST_SUBTYP='CBU'
  )
AND CB_SHIPMENT.SCH_SHIPMENT_DATE IS NULL
AND ER_ORDER.ORDER_STATUS NOT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' and CODELST_SUBTYP IN('close_ordr','resolved')))
loop
  v_orderid:=orders.pk_order;
  sp_alerts(v_orderid,v_entity_order,null);
end loop;
end;
/


begin
DBMS_SCHEDULER.create_job (
    job_name        => 'CREATE_ALERTS_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN sp_generate_date_based_alerts;END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'freq=daily;byhour=0;byminute=0;bysecond=0',
    end_date        => NULL,
    enabled         => TRUE,
    comments        => 'Job to create alerts that depends on time');
end;
/

begin
dbms_scheduler.run_job('CREATE_ALERTS_JOB');
end;
/


INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,219,13,'13_hotfix2_sp_gen_date_alerts.sql',sysdate,'9.0.0 B#637-ET021.02');
		commit;