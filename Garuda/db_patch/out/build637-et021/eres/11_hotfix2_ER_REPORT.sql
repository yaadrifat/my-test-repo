set define off;

delete er_repxsl where pk_repxsl in (178,179);
commit;


----CREATING IDM WIDGET FORM------

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_report
    where pk_report = 185;
  if (v_record_exists = 0) then
   Insert into ER_REPORT 				       (PK_REPORT,REP_NAME,REP_DESC,REP_SQL,FK_ACCOUNT,FK_CODELST_TYPE,IP_ADD,REP_HIDE,REP_COLUMNS,REP_FILTERBY,GENERATE_XML,REP_TYPE,REP_SQL_CLOB,REP_FILTERKEYWORD,REP_FILTERAPPLICABLE) values (185,'IDM widget form','IDM widget form','',0,null,null,'Y',null,null,1,'rep_cbu','',null,null);

COMMIT;	

	Update ER_REPORT set REP_SQL = 'SELECT  CRD.CORD_ISBI_DIN_CODE ISTBTDIN,CRD.CORD_REGISTRY_ID REGID,CRD.CORD_LOCAL_CBU_ID LOCALCBUID,F_CORD_ADD_IDS(~1) ADDIDS,CORD_ID_NUMBER_ON_CBU_BAG,
SITE.SITE_NAME SITENAME,SITE.SITE_ID SITEID,f_get_dynamicformdata(~1,''~2'') IDM,F_GET_FORM_TYP_DT(~1,''~2'') IDM_FORM_DATA,f_get_dynamicformgrp(''~2'') IDM_GRP,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1,0'') idm_notes,f_get_attachments(~1,f_codelst_id(''doc_categ'',''infect_mark'')) IDM_ATTACH, f_get_attachments(~1,f_codelst_id(''doc_categ'',''unit_rprt_cat'')) IDM_UNITATTACH, f_get_patientid(~1) PATIENT_ID FROM CB_CORD CRD, er_site site WHERE
CRD.FK_CBB_ID=SITE.PK_SITE AND CRD.PK_CORD=~1' where pk_report = 185;
	COMMIT;


	Update ER_REPORT set REP_SQL_CLOB = 'SELECT  CRD.CORD_ISBI_DIN_CODE ISTBTDIN,CRD.CORD_REGISTRY_ID REGID,CRD.CORD_LOCAL_CBU_ID LOCALCBUID,F_CORD_ADD_IDS(~1) ADDIDS,CORD_ID_NUMBER_ON_CBU_BAG,
SITE.SITE_NAME SITENAME,SITE.SITE_ID SITEID,f_get_dynamicformdata(~1,''~2'') IDM,F_GET_FORM_TYP_DT(~1,''~2'') IDM_FORM_DATA,f_get_dynamicformgrp(''~2'') IDM_GRP,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1,0'') idm_notes,f_get_attachments(~1,f_codelst_id(''doc_categ'',''infect_mark'')) IDM_ATTACH, f_get_attachments(~1,f_codelst_id(''doc_categ'',''unit_rprt_cat'')) IDM_UNITATTACH, f_get_patientid(~1) PATIENT_ID FROM CB_CORD CRD, er_site site WHERE
CRD.FK_CBB_ID=SITE.PK_SITE AND CRD.PK_CORD=~1' where pk_report = 185;
	COMMIT;

  end if;
end;
/



----UPDATE MRQ REPORT SQL------


Update ER_REPORT set REP_SQL = 'select f_get_dynamicformdata(~1,''~2'') MRQ,f_get_dynamicformgrp(''~2'') GRP,cbb_id siteid,cbbid sitename,cord_registry_id regid,f_cord_id_on_bag(~1) IDSONBAG,cord_local_cbu_id LOCALCBUID,cord_isbi_din_code ISTBTDIN,f_cord_add_ids(~1) ADDIDS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hlt_his_scrn''),''1'') hsh_notes,f_get_attachments(~1,f_codelst_id(''doc_categ'',''hlth_scren'')) MRQ_ATTACH,lic_status licstatus, f_get_patientid(~1) PATIENT_ID from rep_cbu_details where pk_cord=~1' where pk_report = 178;
/
	COMMIT;


	Update ER_REPORT set REP_SQL_CLOB = 'select f_get_dynamicformdata(~1,''~2'') MRQ,f_get_dynamicformgrp(''~2'') GRP,cbb_id siteid,cbbid sitename,cord_registry_id regid,f_cord_id_on_bag(~1) IDSONBAG,cord_local_cbu_id LOCALCBUID,cord_isbi_din_code ISTBTDIN,f_cord_add_ids(~1) ADDIDS,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hlt_his_scrn''),''1'') hsh_notes,f_get_attachments(~1,f_codelst_id(''doc_categ'',''hlth_scren'')) MRQ_ATTACH,lic_status licstatus, f_get_patientid(~1) PATIENT_ID from rep_cbu_details where pk_cord=~1' where pk_report = 178;
/
	COMMIT;



----UPDATE FMHQ REPORT SQL------



Update ER_REPORT set REP_SQL = 'select f_get_dynamicformdata(~1,''~2'') FMHQ,f_get_dynamicformgrp(''~2'') FMHQ_GRP,f_get_attachments(~1,f_codelst_id(''doc_categ'',''hlth_scren'')) FMHQ_ATTACH,cbb_id siteid,cbbid sitename,cord_registry_id regid,f_cord_id_on_bag(~1) IDSONBAG,
lic_status licstatus,cord_local_cbu_id LOCALCBUID,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hlt_his_scrn''),''1'') hsh_notes,cord_isbi_din_code ISTBTDIN,f_cord_add_ids(~1) ADDIDS, f_get_patientid(~1) PATIENT_ID from rep_cbu_details where pk_cord=~1' where pk_report = 179;
/
	COMMIT;


	Update ER_REPORT set REP_SQL_CLOB = 'select f_get_dynamicformdata(~1,''~2'') FMHQ,f_get_dynamicformgrp(''~2'') FMHQ_GRP,f_get_attachments(~1,f_codelst_id(''doc_categ'',''hlth_scren'')) FMHQ_ATTACH,cbb_id siteid,cbbid sitename,cord_registry_id regid,f_cord_id_on_bag(~1) IDSONBAG,
lic_status licstatus,cord_local_cbu_id LOCALCBUID,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''hlt_his_scrn''),''1'') hsh_notes,cord_isbi_din_code ISTBTDIN,f_cord_add_ids(~1) ADDIDS, f_get_patientid(~1) PATIENT_ID from rep_cbu_details where pk_cord=~1' where pk_report = 179;
/
	COMMIT;


INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,219,11,'11_hotfix2_ER_REPORT.sql',sysdate,'9.0.0 B#637-ET021.02');
		commit;
