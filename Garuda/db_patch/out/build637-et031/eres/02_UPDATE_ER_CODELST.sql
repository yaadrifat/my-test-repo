--START UPDATE ER_CODELST--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where codelst_type = 'risk_HIV1' AND CODELST_SUBTYP='sengal';
  if (v_record_exists = 1) then
    update ER_CODELST set CODELST_DESC='Senegal*' where codelst_type = 'risk_HIV1' AND CODELST_SUBTYP='sengal';
commit;
  end if;
end;
/
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,237,2,'02_UPDATE_ER_CODELST.sql',sysdate,'9.0.0 B#637-ET031');

commit;