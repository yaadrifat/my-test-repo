set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v9.0.0 Build#637-ET007' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,190,9,'09_hotfix1_er_version.sql',sysdate,'9.0.0 B#637-ET007');

commit;