--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_status'
    AND codelst_subtyp = 'HLA_RES_REC';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='HLA Results Received' where codelst_type = 'order_status'
    AND codelst_subtyp = 'HLA_RES_REC';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_status'
    AND codelst_subtyp = 'PENDING';
  if (v_record_exists = 1) then
      DELETE FROM ER_CODELST where codelst_type = 'order_status' AND codelst_subtyp = 'PENDING';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'order_status'
    AND codelst_subtyp = 'requested';
  if (v_record_exists = 1) then
      DELETE FROM ER_CODELST where codelst_type = 'order_status' AND codelst_subtyp = 'requested';
	commit;
  end if;
end;
/
--END--
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,190,1,'01_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 B#637-ET007');

commit;