DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'site_type'
    AND codelst_subtyp = 'cbb';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='site_type' and CODELST_SUBTYP='cbb';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'site_type'
    AND codelst_subtyp = 'tc';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='site_type' and CODELST_SUBTYP='tc';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'site_type'
    AND codelst_subtyp = 'cbu_storage';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='site_type' and CODELST_SUBTYP='cbu_storage';
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,190,5,'05_ER_CODELST_HIDE.sql',sysdate,'9.0.0 B#637-ET007');

commit;