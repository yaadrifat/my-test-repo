/* This readMe is specific to Velos eResearch version 9.0 build637-et010 */

=====================================================================================================================================
Garuda :
A new feature has been introduced to check the application version and database version. If the application version doesn't match
with database version, system will not allow user to continue:

Note:
Application version is defined in XML find stored in following location:
/server/eresearch/deploy/velos.ear/velos.war/jsp/xml/releaseinfo.xml
<ReleaseInfo>
	<VersionNo>v9.0.0 Build#637-ET010</VersionNo>
	<ReleaseDate>21-Sep-2012</ReleaseDate>
</ReleaseInfo>
=====================================================================================================================================
