DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Unspecified Black';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='unspec_black' where CODELST_DESC='Unspecified Black' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Unspec. Asian/Pacific Islndr';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='api' where CODELST_DESC='Unspec. Asian/Pacific Islndr' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Caribbean Hispanic';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='carhis' where CODELST_DESC='Caribbean Hispanic' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'North American or European';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='eurwrc' where CODELST_DESC='North American or European' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Hawaiian or Pacific Islander';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='hawi' where CODELST_DESC='Hawaiian or Pacific Islander' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Unspecified Hispanic';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='his' where CODELST_DESC='Unspecified Hispanic' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Multiple Black';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='mafa' where CODELST_DESC='Multiple Black' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Multiple Asian/Pacific Islndr';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='mapi' where CODELST_DESC='Multiple Asian/Pacific Islndr' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Multiple Caucasian';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='mcau' where CODELST_DESC='Multiple Caucasian' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'MidEast/No. Coast of Africa';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='menafc' where CODELST_DESC='MidEast/No. Coast of Africa' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Multiple Hispanic';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='mhis' where CODELST_DESC='Multiple Hispanic' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Multiple Native American';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='mnam' where CODELST_DESC='Multiple Native American' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Mexican or Chicano';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='mswhis' where CODELST_DESC='Mexican or Chicano' and codelst_type = 'race';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Multiple Race';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='multi' where CODELST_DESC='Multiple Race' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Unspecified Native American';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='nam' where CODELST_DESC='Unspecified Native American' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Other';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='oth' where CODELST_DESC='Other' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'South Cntrl/Cntrl Amer. Hisp.';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='scahis' where CODELST_DESC='South Cntrl/Cntrl Amer. Hisp.' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Unknown/Question Not Asked ';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='unk' where CODELST_DESC='Unknown/Question Not Asked ' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND CODELST_DESC = 'Multiple Nat. Hw/Othr Pa. Isln';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='unk' where CODELST_DESC='Multiple Nat. Hw/Othr Pa. Isln' and codelst_type = 'race';
  end if;
end;
/

commit;

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'unk';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='unk';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'api';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='api';
  end if;
end;
/




DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'carhis';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='carhis';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'eurwrc';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='eurwrc';
  end if;
end;
/



DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'hawi';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='hawi';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'his';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='his';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mafa';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mafa';
  end if;
end;
/



DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mapi';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mapi';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mcau';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mcau';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'menafc';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='menafc';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mhis';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mhis';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mnam';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mnam';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mswhis';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mswhis';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'multi';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='multi';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'nam';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='nam';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'oth';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='oth';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'scahis';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='scahis';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mhaw';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mhaw';
  end if;
end;
/



DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'unspec_black';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='unspec_black';
  end if;
end;
/

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,197,10,'10_hotfix1_er_codelst.sql',sysdate,'9.0.0 B#637-ET010.01');

commit;
