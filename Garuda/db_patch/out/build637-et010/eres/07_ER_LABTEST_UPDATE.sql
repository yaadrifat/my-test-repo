DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_SHORTNAME = 'PERTMON';
  if (v_record_exists = 1) then
      UPDATE ER_LABTEST SET LABTEST_NAME='% of CD34+ Cells in Total Mononuclear Cell Count' where LABTEST_SHORTNAME = 'PERTMON';
	commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,197,7,'07_ER_LABTEST_UPDATE.sql',sysdate,'9.0.0 B#637-ET010');

commit;