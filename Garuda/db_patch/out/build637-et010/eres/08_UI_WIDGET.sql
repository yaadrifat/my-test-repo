 --Sql for PF widget 
--UIwidget entry

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where NAME = 'ID Widget'
    AND WIDGET_DIV_ID = 'idinfoparent';
  if (v_record_exists = 0) then
 Insert Into Ui_Widget (Pk_Widget,State,Help_Url,Name,Widget_Div_Id,Description,Is_Authenticated,Is_Minizable,Is_Closable,Is_Resizeable,Is_Dragable,Is_Mouse_Hover_Enabled,Created_On,Last_Modified_Date,Version_No,Resize_Min_Height,Resize_Min_Width,Resize_Max_Height,Resize_Max_Width) 
values (SEQ_UI_WIDGET.nextval,null,null,'ID Widget','idinfoparent','ID Widget _Description','1','1','1','1','1','1',null,null,null,100,400,1024,1024);
	commit;
  end if;
end;
/


-- for entry in UI_CONTAINER_WIDGET_MAP 
DECLARE
  v_record_exists number:= 0; 
   v_record_exists_1 number :=0;
  BEGIN
   Select  count(*) into  v_record_exists  
 from   UI_WIDGET where name like 'ID Widget' ;
 if(v_record_exists != 0) then
  Select  count(*) into  v_record_exists_1  
    from UI_CONTAINER_WIDGET_MAP a, UI_WIDGET b where a.FK_WIDGET_ID = b.PK_WIDGET and b.name like 'ID Widget' ;
 if(v_record_exists_1 = 0) then
  Insert into UI_CONTAINER_WIDGET_MAP (PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values
(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from UI_WIDGET_CONTAINER where container_name='CordEntry_Next'),
(select PK_WIDGET from ui_widget where name='ID Widget'));
	commit;
  end if;
  end if;
 end;
 /


 Update ER_MAILCONFIG set MAILCONFIG_CONTENT=CHR(13) ||
    'Dear [USER],</br></br></br>' || CHR(13) ||
    CHR(13) || CHR(13) ||
    'The Cord Import Process that you started has been completed
now.</br>' || CHR(13) || CHR(13) ||
    'Please use this link<a href="[URL]">Cord Import</a>   to verify
the process.</br></br></br>' || CHR(13) ||
    CHR(13) ||
    'Thank you,</br>' || CHR(13) ||
    '[CM]' ||
    CHR(13) where MAILCONFIG_MAILCODE='IMPORTNOTI';

    commit; 

	
	INSERT INTO track_patches
VALUES(seq_track_patches.nextval,197,8,'08_UI_WIDGET.sql',sysdate,'9.0.0 B#637-ET010');

commit;