--STARTS MODIFYING COLUMN FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_ALERT_CONDITIONS' AND COLUMN_NAME = 'COLUMN_NAME';
  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_ALERT_CONDITIONS MODIFY(COLUMN_NAME VARCHAR2(255))';
	commit;
  end if;
end;
/


--STARTS MODIFYING COLUMN FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_ALERT_CONDITIONS' AND COLUMN_NAME = 'TABLE_NAME';
  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_ALERT_CONDITIONS MODIFY(TABLE_NAME VARCHAR2(255))';
	commit;
  end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,197,1,'01_MISC_ALTER.sql',sysdate,'9.0.0 B#637-ET010');

commit;
