CREATE INDEX IDX_CB_ANTIGEN_ENCOD1 ON cb_antigen_encod(fk_antigen,version)
  LOGGING
  NOPARALLEL;
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,197,13,'13_hotfix3_cb_antigen.sql',sysdate,'9.0.0 B#637-ET010.03');

commit;