set define off;
delete from er_repxsl where pk_repxsl = 171;
commit;
alter table er_user modify usr_title varchar2(100);
commit;

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,205,5,'05_ALTER_SCRIPT.sql',sysdate,'9.0.0 B#637-ET014');
		commit;
