create or replace
PROCEDURE sp_Clean_notes AS
v_pk_ctrltab number:=0;
v_total_record_count number:=0;
v_index number :=0;
V_delete_query varchar ( 1000 ) := 'Delete from er_ctrltab where pk_ctrltab in ( ';
V_last_part varchar ( 10) := ')';
V_final_query Varchar ( 2000) :='';
v_index_set varchar (20):='';

BEGIN

Select count(*) into v_total_record_count from er_ctrltab where ctrl_value = 'CB_NOTES' or ( ctrl_desc='CB_NOTES'and ctrl_value = 'G');

IF v_total_record_count > 3 THEN 

FOR CUR_NOTES IN (select pk_ctrltab from er_ctrltab where ctrl_value = 'CB_NOTES' or ( ctrl_desc='CB_NOTES'and ctrl_value = 'G') order by pk_ctrltab desc)
LOOP

dbms_output.put_line ( 'First Record - PK '||CUR_NOTES.pk_ctrltab);
if v_index >=3 then 

CASE
WHEN v_index = 3  THEN
v_index_set := to_char(CUR_NOTES.pk_ctrltab);
  
WHEN v_index > 3  THEN
 v_index_set :=v_index_set || ', '|| to_char (CUR_NOTES.pk_ctrltab);
END CASE; 

end if;
v_index := v_index + 1;


END LOOP;

END IF;

dbms_output.put_line ( 'v_index '||v_index);
dbms_output.put_line ( 'v_index_set '||v_index_set);

V_final_query := V_delete_query ||v_index_set ||V_last_part ;

dbms_output.put_line ( 'V_final_query '||V_final_query);

if v_index_set <>'' and length(v_index_set )> 0 then 

execute immediate V_final_query;
commit;
end if;

End;
/

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,205,7,'07_SP_CLEAN_NOTES.sql',sysdate,'9.0.0 B#637-ET014');
		commit;