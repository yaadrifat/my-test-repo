create or replace
TRIGGER TASK_UPDATE_FCR AFTER INSERT ON CB_CORD_FINAL_REVIEW REFERENCING OLD AS OLDVAL NEW AS NEWVAL
FOR EACH ROW
DECLARE
V_FLAG NUMBER;
V_TASK NUMBER;
V_CORDID NUMBER;
V_ORDERID NUMBER;
V_CDRUSER VARCHAR2(2);
BEGIN
dbms_output.put_line('........'||:newval.licensure_confirm_flag||'...........'||:newval.eligible_confirm_flag||'......'||:newval.final_review_confirm_flag);
V_CORDID:=:NEWVAL.FK_CORD_ID;
SELECT NVL(USING_CDR,'') INTO V_CDRUSER FROM CB_CORD CRD,ER_SITE S,CBB CB WHERE CRD.FK_CBB_ID=S.PK_SITE AND CB.FK_SITE=S.PK_SITE AND CRD.PK_CORD=V_CORDID;
  IF (V_CDRUSER=1 AND (:NEWVAL.LICENSURE_CONFIRM_FLAG=0 OR :NEWVAL.LICENSURE_CONFIRM_FLAG IS NULL OR :NEWVAL.ELIGIBLE_CONFIRM_FLAG=0 OR :NEWVAL.ELIGIBLE_CONFIRM_FLAG IS NULL OR :NEWVAL.FINAL_REVIEW_CONFIRM_FLAG IS NULL OR :NEWVAL.FINAL_REVIEW_CONFIRM_FLAG = 'N')) 
  OR (V_CDRUSER=0 AND (:NEWVAL.CONFIRM_CBB_SIGN_FLAG IS NULL OR :NEWVAL.CONFIRM_CBB_SIGN_FLAG=0 OR :NEWVAL.LICENSURE_CONFIRM_FLAG=0 OR :NEWVAL.LICENSURE_CONFIRM_FLAG IS NULL OR :NEWVAL.ELIGIBLE_CONFIRM_FLAG=0 OR :NEWVAL.ELIGIBLE_CONFIRM_FLAG IS NULL OR :NEWVAL.FINAL_REVIEW_CONFIRM_FLAG IS NULL OR :NEWVAL.FINAL_REVIEW_CONFIRM_FLAG = 'N')) THEN
  dbms_output.put_line ('INSIDE FIRST IF.........'||V_CORDID);
     FOR TASKS IN(SELECT PK_TASKID,TASK_COMPLETEFLAG,ORD.PK_ORDER ORDER_ID FROM TASK TSK,ER_ORDER ORD,ER_ORDER_HEADER HDR WHERE TSK.FK_ENTITY=ORD.PK_ORDER AND ORD.FK_ORDER_HEADER =HDR.PK_ORDER_HEADER AND HDR.ORDER_ENTITYID  =V_CORDID AND HDR.ORDER_ENTITYTYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU') AND ORD.ORDER_STATUS NOT IN(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE  ='order_status' AND CODELST_SUBTYP IN('CANCELLED','close_ordr')) AND FK_WORKFLOWACTIVITY=(SELECT PK_WORKFLOWACTIVITY FROM WORKFLOW_ACTIVITY WHERE FK_ACTIVITYID=(SELECT PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY.ACTIVITY_NAME='finalcbureviewbar') AND FK_WORKFLOWID=(SELECT PK_WORKFLOW FROM WORKFLOW WHERE WORKFLOW_NAME='OR Order Workflow')))
     LOOP
     V_FLAG:=TASKS.TASK_COMPLETEFLAG;
     V_TASK:=TASKS.PK_TASKID;
     V_ORDERID:=TASKS.ORDER_ID;
      dbms_output.put_line ('INSIDE LOOP........'||v_flag||'...........'||v_task||'....'||v_orderid);
      IF V_FLAG=1 THEN
      dbms_output.put_line ('INSIDE IF........');
        UPDATE ER_ORDER SET FINAL_REVIEW_TASK_FLAG=NULL WHERE PK_ORDER=V_ORDERID;
        UPDATE TASK SET TASK_COMPLETEFLAG=0 WHERE PK_TASKID=V_TASK;
        dbms_output.put_line ('AFTER UPDATE........');

      END IF;
     END LOOP;
  END IF;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
	dbms_output.put_line ('INSIDE EXCEPTION...........DATA NOT FIND');
END;
/

begin
 DBMS_SCHEDULER.drop_job (job_name => 'CREATE_WORKFLOW_JOB');
 end;
 /

begin
DBMS_SCHEDULER.create_job (
    job_name        => 'CREATE_WORKFLOW_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN SP_WORKFLOW_TEMP;CB_ALERTS_MAINTENANCE_PROC;END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'freq=SECONDLY;INTERVAL=10',
    end_date        => NULL,
    enabled         => TRUE,
    comments        => 'Job to create workflow.');
end;
/

begin
dbms_scheduler.run_job('CREATE_WORKFLOW_JOB');
end;
/

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,205,2,'02_Trigger.sql',sysdate,'9.0.0 B#637-ET014');
		commit;