delete from ER_LABTESTGRP where FK_TESTID in (select PK_LABTEST from ER_LABTEST where LABTEST_NAME='NAT HIV' and LABTEST_SHORTNAME = 'NATHIV') ;

delete from ER_LABTESTGRP where FK_TESTID in (select PK_LABTEST from ER_LABTEST where LABTEST_NAME='NAT HCV' and LABTEST_SHORTNAME = 'NATHCV') ;

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME='NAT HIV' and LABTEST_SHORTNAME = 'NATHIV' ;
  if (v_record_exists = 1) then
     delete from ER_LABTEST where LABTEST_NAME='NAT HIV' and LABTEST_SHORTNAME = 'NATHIV' ;
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME='NAT HCV' and LABTEST_SHORTNAME = 'NATHCV' ;
  if (v_record_exists = 1) then
     delete from ER_LABTEST where LABTEST_NAME='NAT HCV' and LABTEST_SHORTNAME = 'NATHCV' ;
	commit;
  end if;
end;
/

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,205,3,'03_ER_LABTEST.sql',sysdate,'9.0.0 B#637-ET014');
		commit;