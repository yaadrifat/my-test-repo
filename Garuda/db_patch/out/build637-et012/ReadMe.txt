/* This readMe is specific to Velos eResearch version 9.0 build637-et012 */

=====================================================================================================================================
 Cord Import email URL Setting:
Set the application URL in [server\eresearch\deploy\velos.ear\velos.war\WEB-INF\classes\]
ApplicationResources.properties file as follows

garuda.application.url = <<IP Address:Port>> 
for example 
garuda.application.url = http://172.16.2.60:8080
=====================================================================================================================================
  