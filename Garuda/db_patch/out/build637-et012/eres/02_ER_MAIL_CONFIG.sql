DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_MAILCONFIG;
  if (v_record_exists > 1) then
 UPDATE ER_MAILCONFIG SET MAILCONFIG_FROM='servicedesk@nmdp.org';
	commit;
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,201,2,'02_ER_MAIL_CONFIG.sql',sysdate,'9.0.0 B#637-ET012');

commit;