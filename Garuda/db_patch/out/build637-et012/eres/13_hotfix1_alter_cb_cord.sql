--STARTS MODIFYING COLUMN CORD_LOCAL_CBU_ID FROM CB_CORD TABLE
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'CORD_LOCAL_CBU_ID';
  if (v_column_exists =1) then
      execute immediate 'alter table CB_CORD modify( CORD_LOCAL_CBU_ID varchar2(255 char))';
  end if;
end;
/
--END

--STARTS MODIFYING COLUMN CORD_REGISTRY_ID FROM CB_CORD TABLE
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_CORD' AND column_name = 'CORD_REGISTRY_ID';
  if (v_column_exists = 1) then
      execute immediate 'alter table CB_CORD modify( CORD_REGISTRY_ID varchar2(255 char))';
  end if;
end; 
/
--END
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,201,13,'13_hotfix1_alter_cb_cord.sql',sysdate,'9.0.0 B#637-ET012.01');

commit;