--Update CB_Question table--
--MRQ Question 46 For Bug #11020--
--Question 46--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_1_2_o_performed_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ref_chart=null where QUES_CODE = 'hiv_1_2_o_performed_ind';
commit;
  end if;
end;
/
--End--
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,201,4,'04_CB_QUESTION_UPDATE.sql',sysdate,'9.0.0 B#637-ET012');

commit;