
--START UPDATE ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'hla_locus'
    AND codelst_subtyp = 'hla_dqa1';
  if (v_column_exists = 1) then
	update er_codelst set CODELST_SEQ = 12 where codelst_type = 'hla_locus' AND codelst_subtyp = 'hla_dqa1';
  end if;
end;
/


--START UPDATE ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'LAB_SUM';
  If (V_Column_Exists = 1) Then
	Update Er_Codelst Set Codelst_Desc = 'LAB SUMMARY' Where Codelst_Type = 'sub_entity_type' And Codelst_Subtyp = 'LAB_SUM';
  commit;
   end if;
End;
/

Update ER_MAILCONFIG set MAILCONFIG_CONTENT=CHR(13) ||
	'Dear [USER],</br></br></br>' || CHR(13) ||	
	CHR(13) || CHR(13) || 	
	'The Cord Import Process that you started has been completed now.</br>' || CHR(13) || CHR(13) || 
	'Please Use This Link <a href="[URL]">Cord Import</a>  to verify the process.</br></br></br>' || CHR(13) || 
	CHR(13) || 
	'Thank you,</br>' || CHR(13) || 
	'[CM]' || 
	CHR(13) where MAILCONFIG_MAILCODE='IMPORTNOTI';
	
	commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,201,1,'01_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 B#637-ET012');

commit;