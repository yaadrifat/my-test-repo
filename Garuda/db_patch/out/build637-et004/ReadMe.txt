/* This readMe is specific to Velos eResearch version 9.0 build637-et004 */

=====================================================================================================================================
Garuda :

Following patches are specific to Garuda so do not execute over eresearch database.

1)10_acces_right_delete.sql
2)11_Lookup.sql
3)12_CBUhomePage.sql
4)13_Top_Navigation_Pane.sql
5)14_Top_navigation_panel_maintenance.sql
6)15_Top_navigation_panel_Libraries_cont.sql
7)16_CBURecord_static_panel.sql
8)17_CBURecord_Quick_link_panel.sql
9)18_Order_releated_permission.sql
10)19_CBU_clinical_record.sql


=====================================================================================================================================
