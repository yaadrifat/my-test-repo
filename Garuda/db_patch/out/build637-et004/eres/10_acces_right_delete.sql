--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBBSETUP'
    or  CTRL_DESC='CB_CBBSETUP';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBBSETUP' or  CTRL_DESC='CB_CBBSETUP';

  end if;
end;
/
----END


--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='H_14'
    or  CTRL_DESC='H_14';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='H_14' or  CTRL_DESC='H_14';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_EVALHIST'
    or  CTRL_DESC='CB_EVALHIST';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_EVALHIST' or  CTRL_DESC='CB_EVALHIST';

  end if;
end;
/
----END


--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBUCOMP'
    or  CTRL_DESC='CB_CBUCOMP';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBUCOMP' or  CTRL_DESC='CB_CBUCOMP';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_DETAILCBU'
    or  CTRL_DESC='CB_DETAILCBU';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_DETAILCBU' or  CTRL_DESC='CB_DETAILCBU';

  end if;
end;
/
----END


--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBUSUM'
    or  CTRL_DESC='CB_CBUSUM';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBUSUM' or  CTRL_DESC='CB_CBUSUM';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='H_13'
    or  CTRL_DESC='H_13';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='H_13' or  CTRL_DESC='H_13';

  end if;
end;
/
----END


--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PCKNGSLIP'
    or  CTRL_DESC='CB_PCKNGSLIP';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PCKNGSLIP' or  CTRL_DESC='CB_PCKNGSLIP';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_VCTINS'
    or  CTRL_DESC='CB_VCTINS';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_VCTINS' or  CTRL_DESC='CB_VCTINS';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_APLYRESOCT'
    or  CTRL_DESC='CB_APLYRESOCT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_APLYRESOCT' or  CTRL_DESC='CB_APLYRESOCT';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CASEMNGRDIR'
    or  CTRL_DESC='CB_CASEMNGRDIR';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CASEMNGRDIR' or  CTRL_DESC='CB_CASEMNGRDIR';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_VPATANTIGENT'
    or  CTRL_DESC='CB_VPATANTIGENT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_VPATANTIGENT' or  CTRL_DESC='CB_VPATANTIGENT';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_REPORTINFUSE'
    or  CTRL_DESC='CB_REPORTINFUSE';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_REPORTINFUSE' or  CTRL_DESC='CB_REPORTINFUSE';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_SHIPDATE'
    or  CTRL_DESC='CB_SHIPDATE';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_SHIPDATE' or  CTRL_DESC='CB_SHIPDATE';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_TNCFRZN'
    or  CTRL_DESC='CB_TNCFRZN';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_TNCFRZN' or  CTRL_DESC='CB_TNCFRZN';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PATDISTATUS'
    or  CTRL_DESC='CB_PATDISTATUS';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PATDISTATUS' or  CTRL_DESC='CB_PATDISTATUS';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PATDISEASE'
    or  CTRL_DESC='CB_PATDISEASE';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PATDISEASE' or  CTRL_DESC='CB_PATDISEASE';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_SHPFRPATIDSP'
    or  CTRL_DESC='CB_SHPFRPATIDSP';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_SHPFRPATIDSP' or  CTRL_DESC='CB_SHPFRPATIDSP';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_ACTPATIENTSP'
    or  CTRL_DESC='CB_ACTPATIENTSP';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_ACTPATIENTSP' or  CTRL_DESC='CB_ACTPATIENTSP';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_QUICKLNKPANL'
    or  CTRL_DESC='CB_QUICKLNKPANL';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_QUICKLNKPANL' or  CTRL_DESC='CB_QUICKLNKPANL';

  end if;
end;
/
----END


--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PROGNOTESW'
    or  CTRL_DESC='CB_PROGNOTESW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PROGNOTESW' or  CTRL_DESC='CB_PROGNOTESW';

  end if;
end;
/
----END
--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_USERPREFW'
    or  CTRL_DESC='CB_USERPREFW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_USERPREFW' or  CTRL_DESC='CB_USERPREFW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_NMDPFUNDTABW'
    or  CTRL_DESC='CB_NMDPFUNDTABW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_NMDPFUNDTABW' or  CTRL_DESC='CB_NMDPFUNDTABW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_VIEWCBBDFLTW'
    or  CTRL_DESC='CB_VIEWCBBDFLTW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_VIEWCBBDFLTW' or  CTRL_DESC='CB_VIEWCBBDFLTW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PFLANDINGW'
    or  CTRL_DESC='CB_PFLANDINGW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PFLANDINGW' or  CTRL_DESC='CB_PFLANDINGW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PFPENDINGW'
    or  CTRL_DESC='CB_PFPENDINGW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PFPENDINGW' or  CTRL_DESC='CB_PFPENDINGW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_REQCBUHISTW'
    or  CTRL_DESC='CB_REQCBUHISTW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_REQCBUHISTW' or  CTRL_DESC='CB_REQCBUHISTW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_ORRESOLW'
    or  CTRL_DESC='CB_ORRESOLW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_ORRESOLW' or  CTRL_DESC='CB_ORRESOLW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_HERESOLW'
    or  CTRL_DESC='CB_HERESOLW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_HERESOLW' or  CTRL_DESC='CB_HERESOLW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBUSPMNTINFW'
    or  CTRL_DESC='CB_CBUSPMNTINFW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBUSPMNTINFW' or  CTRL_DESC='CB_CBUSPMNTINFW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_TCORDRDETALW'
    or  CTRL_DESC='CB_TCORDRDETALW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_TCORDRDETALW' or  CTRL_DESC='CB_TCORDRDETALW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PATCENTRECMW'
    or  CTRL_DESC='CB_PATCENTRECMW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PATCENTRECMW' or  CTRL_DESC='CB_PATCENTRECMW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_FINALCBUREVW'
    or  CTRL_DESC='CB_FINALCBUREVW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_FINALCBUREVW' or  CTRL_DESC='CB_FINALCBUREVW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_REQCLNINFOW'
    or  CTRL_DESC='CB_REQCLNINFOW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_REQCLNINFOW' or  CTRL_DESC='CB_REQCLNINFOW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CTRESOLTNW'
    or  CTRL_DESC='CB_CTRESOLTNW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CTRESOLTNW' or  CTRL_DESC='CB_CTRESOLTNW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CTSPMNTINFOW'
    or  CTRL_DESC='CB_CTSPMNTINFOW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CTSPMNTINFOW' or  CTRL_DESC='CB_CTSPMNTINFOW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_ADCBUHLATYPW'
    or  CTRL_DESC='CB_ADCBUHLATYPW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_ADCBUHLATYPW' or  CTRL_DESC='CB_ADCBUHLATYPW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBUAVAILCONW'
    or  CTRL_DESC='CB_CBUAVAILCONW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBUAVAILCONW' or  CTRL_DESC='CB_CBUAVAILCONW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CLNCLNOTESW'
    or  CTRL_DESC='CB_CLNCLNOTESW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CLNCLNOTESW' or  CTRL_DESC='CB_CLNCLNOTESW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PHYASSMNTW'
    or  CTRL_DESC='CB_PHYASSMNTW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PHYASSMNTW' or  CTRL_DESC='CB_PHYASSMNTW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_ORELMEDRECW'
    or  CTRL_DESC='CB_ORELMEDRECW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_ORELMEDRECW' or  CTRL_DESC='CB_ORELMEDRECW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_OTHERW'
    or  CTRL_DESC='CB_OTHERW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_OTHERW' or  CTRL_DESC='CB_OTHERW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_FMHQW'
    or  CTRL_DESC='CB_FMHQW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_FMHQW' or  CTRL_DESC='CB_FMHQW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_MRQW'
    or  CTRL_DESC='CB_MRQW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_MRQW' or  CTRL_DESC='CB_MRQW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_HLTHISCRNW'
    or  CTRL_DESC='CB_HLTHISCRNW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_HLTHISCRNW' or  CTRL_DESC='CB_HLTHISCRNW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_IDMW'
    or  CTRL_DESC='CB_IDMW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_IDMW' or  CTRL_DESC='CB_IDMW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_MATERNALHLA'
    or  CTRL_DESC='CB_MATERNALHLA';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_MATERNALHLA' or  CTRL_DESC='CB_MATERNALHLA';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PATIENTHLA'
    or  CTRL_DESC='CB_PATIENTHLA';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PATIENTHLA' or  CTRL_DESC='CB_PATIENTHLA';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBUHLAW'
    or  CTRL_DESC='CB_CBUHLAW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBUHLAW' or  CTRL_DESC='CB_CBUHLAW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_HLAW'
    or  CTRL_DESC='CB_HLAW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_HLAW' or  CTRL_DESC='CB_HLAW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_FDOEW'
    or  CTRL_DESC='CB_FDOEW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_FDOEW' or  CTRL_DESC='CB_FDOEW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBUASMNTW'
    or  CTRL_DESC='CB_CBUASMNTW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBUASMNTW' or  CTRL_DESC='CB_CBUASMNTW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_UMNW'
    or  CTRL_DESC='CB_UMNW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_UMNW' or  CTRL_DESC='CB_UMNW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_ELIGIBLEW'
    or  CTRL_DESC='CB_ELIGIBLEW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_ELIGIBLEW' or  CTRL_DESC='CB_ELIGIBLEW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PROINFOW'
    or  CTRL_DESC='CB_PROINFOW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PROINFOW' or  CTRL_DESC='CB_PROINFOW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_STESTINGW'
    or  CTRL_DESC='CB_STESTINGW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_STESTINGW' or  CTRL_DESC='CB_STESTINGW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PTESTINGW'
    or  CTRL_DESC='CB_PTESTINGW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PTESTINGW' or  CTRL_DESC='CB_PTESTINGW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_LABSUMW'
    or  CTRL_DESC='CB_LABSUMW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_LABSUMW' or  CTRL_DESC='CB_LABSUMW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_IDW'
    or  CTRL_DESC='CB_IDW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_IDW' or  CTRL_DESC='CB_IDW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBUINFOW'
    or  CTRL_DESC='CB_CBUINFOW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBUINFOW' or  CTRL_DESC='CB_CBUINFOW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='H_12'
    or  CTRL_DESC='H_12';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='H_12' or  CTRL_DESC='H_12';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_SHPMNTITERN'
    or  CTRL_DESC='CB_SHPMNTITERN';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_SHPMNTITERN' or  CTRL_DESC='CB_SHPMNTITERN';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_FLAGGEDITEM'
    or  CTRL_DESC='CB_FLAGGEDITEM';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_FLAGGEDITEM' or  CTRL_DESC='CB_FLAGGEDITEM';

  end if;
end;
/
----END


--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBUREPORT'
    or  CTRL_DESC='CB_CBUREPORT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBUREPORT' or  CTRL_DESC='CB_CBUREPORT';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CORDIMPORT'
    or  CTRL_DESC='CB_CORDIMPORT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CORDIMPORT' or  CTRL_DESC='CB_CORDIMPORT';

  end if;
end;
/
----END


--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBUSTATUS'
    or  CTRL_DESC='CB_CBUSTATUS';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBUSTATUS' or  CTRL_DESC='CB_CBUSTATUS';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_FINALELIGBLE'
    or  CTRL_DESC='CB_FINALELIGBLE';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_FINALELIGBLE' or  CTRL_DESC='CB_FINALELIGBLE';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_DUMN'
    or  CTRL_DESC='CB_DUMN';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_DUMN' or  CTRL_DESC='CB_DUMN';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PRODUCTINSRT'
    or  CTRL_DESC='CB_PRODUCTINSRT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PRODUCTINSRT' or  CTRL_DESC='CB_PRODUCTINSRT';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_FINALICSTAT'
    or  CTRL_DESC='CB_FINALICSTAT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_FINALICSTAT' or  CTRL_DESC='CB_FINALICSTAT';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_FINALREVIEW'
    or  CTRL_DESC='CB_FINALREVIEW';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_FINALREVIEW' or  CTRL_DESC='CB_FINALREVIEW';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_AUDIT'
    or  CTRL_DESC='CB_AUDIT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_AUDIT' or  CTRL_DESC='CB_AUDIT';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBUASSMNT'
    or  CTRL_DESC='CB_CBUASSMNT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBUASSMNT' or  CTRL_DESC='CB_CBUASSMNT';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PROGRESNOTE'
    or  CTRL_DESC='CB_PROGRESNOTE';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PROGRESNOTE' or  CTRL_DESC='CB_PROGRESNOTE';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_NOTES'
    or  CTRL_DESC='CB_NOTES';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_NOTES' or  CTRL_DESC='CB_NOTES';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='H_11'
    or  CTRL_DESC='H_11';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='H_11' or  CTRL_DESC='H_11';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CHARTS'
    or  CTRL_DESC='CB_CHARTS';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CHARTS' or  CTRL_DESC='CB_CHARTS';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PUBLISHEDREP'
    or  CTRL_DESC='CB_PUBLISHEDREP';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PUBLISHEDREP' or  CTRL_DESC='CB_PUBLISHEDREP';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_VIEWDATREQ'
    or  CTRL_DESC='CB_VIEWDATREQ';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_VIEWDATREQ' or  CTRL_DESC='CB_VIEWDATREQ';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_DATAREQUEST'
    or  CTRL_DESC='CB_DATAREQUEST';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_DATAREQUEST' or  CTRL_DESC='CB_DATAREQUEST';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_APPMSGS'
    or  CTRL_DESC='CB_APPMSGS';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_APPMSGS' or  CTRL_DESC='CB_APPMSGS';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBUTOOLS'
    or  CTRL_DESC='CB_CBUTOOLS';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBUTOOLS' or  CTRL_DESC='CB_CBUTOOLS';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_FUNDING'
    or  CTRL_DESC='CB_FUNDING';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_FUNDING' or  CTRL_DESC='CB_FUNDING';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PENDINGREQ'
    or  CTRL_DESC='CB_PENDINGREQ';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PENDINGREQ' or  CTRL_DESC='CB_PENDINGREQ';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_LETTERTEMP'
    or  CTRL_DESC='CB_LETTERTEMP';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_LETTERTEMP' or  CTRL_DESC='CB_LETTERTEMP';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_FIELDLIB'
    or  CTRL_DESC='CB_FIELDLIB';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_FIELDLIB' or  CTRL_DESC='CB_FIELDLIB';

  end if;
end;
/
----END


--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_FORMBUILDER'
    or  CTRL_DESC='CB_FORMBUILDER';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_FORMBUILDER' or  CTRL_DESC='CB_FORMBUILDER';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PATIENTFORM'
    or  CTRL_DESC='CB_PATIENTFORM';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PATIENTFORM' or  CTRL_DESC='CB_PATIENTFORM';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_DONORFORM'
    or  CTRL_DESC='CB_DONORFORM';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_DONORFORM' or  CTRL_DESC='CB_DONORFORM';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBUFORM'
    or  CTRL_DESC='CB_CBUFORM';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBUFORM' or  CTRL_DESC='CB_CBUFORM';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_FORMLIB'
    or  CTRL_DESC='CB_FORMLIB';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_FORMLIB' or  CTRL_DESC='CB_FORMLIB';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_FORMS'
    or  CTRL_DESC='CB_FORMS';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_FORMS' or  CTRL_DESC='CB_FORMS';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_REPORT'
    or  CTRL_DESC='CB_REPORT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_REPORT' or  CTRL_DESC='CB_REPORT';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_NETWORKDIR'
    or  CTRL_DESC='CB_NETWORKDIR';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_NETWORKDIR' or  CTRL_DESC='CB_NETWORKDIR';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_STAFFDIR'
    or  CTRL_DESC='CB_STAFFDIR';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_STAFFDIR' or  CTRL_DESC='CB_STAFFDIR';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CONTINFO'
    or  CTRL_DESC='CB_CONTINFO';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CONTINFO' or  CTRL_DESC='CB_CONTINFO';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_FAQ'
    or  CTRL_DESC='CB_FAQ';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_FAQ' or  CTRL_DESC='CB_FAQ';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_HELPFULLNK'
    or  CTRL_DESC='CB_HELPFULLNK';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_HELPFULLNK' or  CTRL_DESC='CB_HELPFULLNK';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_GUIDE'
    or  CTRL_DESC='CB_GUIDE';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_GUIDE' or  CTRL_DESC='CB_GUIDE';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_LINKMOP'
    or  CTRL_DESC='CB_LINKMOP';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_LINKMOP' or  CTRL_DESC='CB_LINKMOP';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_LINKSOP'
    or  CTRL_DESC='CB_LINKSOP';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_LINKSOP' or  CTRL_DESC='CB_LINKSOP';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_REFERENCES'
    or  CTRL_DESC='CB_REFERENCES';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_REFERENCES' or  CTRL_DESC='CB_REFERENCES';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_USERMANAGE'
    or  CTRL_DESC='CB_USERMANAGE';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_USERMANAGE' or  CTRL_DESC='CB_USERMANAGE';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_MYPRO'
    or  CTRL_DESC='CB_MYPRO';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_MYPRO' or  CTRL_DESC='CB_MYPRO';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PICKUP'
    or  CTRL_DESC='CB_PICKUP';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PICKUP' or  CTRL_DESC='CB_PICKUP';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_PROCEDURE'
    or  CTRL_DESC='CB_PROCEDURE';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_PROCEDURE' or  CTRL_DESC='CB_PROCEDURE';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CBBPROFILE'
    or  CTRL_DESC='CB_CBBPROFILE';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CBBPROFILE' or  CTRL_DESC='CB_CBBPROFILE';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CENTRMANAGE'
    or  CTRL_DESC='CB_CENTRMANAGE';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CENTRMANAGE' or  CTRL_DESC='CB_CENTRMANAGE';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_MAINT'
    or  CTRL_DESC='CB_MAINT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_MAINT' or  CTRL_DESC='CB_MAINT';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='H_10'
    or  CTRL_DESC='H_10';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='H_10' or  CTRL_DESC='H_10';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_REVMIN'
    or  CTRL_DESC='CB_REVMIN';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_REVMIN' or  CTRL_DESC='CB_REVMIN';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_ADD'
    or  CTRL_DESC='CB_ADD';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_ADD' or  CTRL_DESC='CB_ADD';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_LICSTAT'
    or  CTRL_DESC='CB_LICSTAT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_LICSTAT' or  CTRL_DESC='CB_LICSTAT';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_ELSTAT'
    or  CTRL_DESC='CB_ELSTAT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_ELSTAT' or  CTRL_DESC='CB_ELSTAT';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_INSTAT'
    or  CTRL_DESC='CB_INSTAT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_INSTAT' or  CTRL_DESC='CB_INSTAT';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_CLSTAT'
    or  CTRL_DESC='CB_CLSTAT';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_CLSTAT' or  CTRL_DESC='CB_CLSTAT';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_UPLDOC'
    or  CTRL_DESC='CB_UPLDOC';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_UPLDOC' or  CTRL_DESC='CB_UPLDOC';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='CB_ATREP'
    or  CTRL_DESC='CB_ATREP';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='CB_ATREP' or  CTRL_DESC='CB_ATREP';

  end if;
end;
/
----END

--STARTS DELETING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where CTRL_VALUE='H_8'
    or  CTRL_DESC='H_8';
  if (v_record_exists = 3) then
    delete from ER_CTRLTAB where CTRL_VALUE='H_8' or  CTRL_DESC='H_8';
	
  end if;
end;
/
----END
commit;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,185,10,'10_acces_right_delete.sql',sysdate,'9.0.0 B#637-ET004');

commit;