set define off

declare

/*******************
    Instrcutions: 
    
    
    1. set value of v_module to the CTRL_VALUE of the module that you need the new access right to link with. If the module does
    not exist in VelosHome page, provide a new keyword-max 15 char
    
    2. initialize v_newacc_va  - with the ctrl_value for the new access right to be available in group rights
    
    3. initialize v_newaccdesc_va  - with the ctrl_desc for the new access right to be available in group rights

    4. initialize v_newacctype_va - special keywords for new/edit or view access only. If all checkboxes needed, pass null,for special combinations pass 
    Edit:'EV', View:'V', New:'NV'
    

*****************/

  v_count number ;

 
  v_module varchar2(50);
  v_module_seq number;
  v_set_new_module boolean := false;

  type newacc_va is varray(50) of VARCHAR2(150);

  v_newacc_va newacc_va;
  v_newaccdesc_va newacc_va;
  v_newacctype_va newacc_va;

   v_null varchar2(10) := null ;

  

  v_count_newacc number;

  v_grp_seq number;
  v_rights_str varchar2(100);

  v_change_made boolean := false;
  v_grp_count number;

  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'ACCESS_SCR', pLEVEL  => Plog.LDEBUG);

begin

    --module to link the access right with

    v_module := 'MODCDRPF';


    -- ctrl_value for new access right

    v_newacc_va :=newacc_va('H_8','CB_LOOKUP','CB_ADVLOOKUP');


    -- ctrl_desc for new access right

    
    v_newaccdesc_va := newacc_va('Lookup Widget','Lookup','Advanced Lookup');

    --special keywords for new/edit or view access only
    
    v_newacctype_va := newacc_va(v_null,'V','V');


    --eResearch module

    select count(*) into v_count 
    from er_ctrltab
    where CTRL_KEY = 'module' and 
      CTRL_VALUE = v_module ;



    if v_count = 0  then

        select max(CTRL_SEQ) +1
        into v_module_seq 
        from ER_CTRLTAB 
        where CTRL_KEY = 'module';


        INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY,  CTRL_SEQ) VALUES ( 
        seq_er_ctrltab.nextval, v_module, 'CDR/PF', 'module', v_module_seq );


        update er_account
        set ac_modright = ac_modright  || '0';    

        v_change_made := true;
        else
 
           select CTRL_SEQ 
           into v_module_seq  from er_ctrltab where CTRL_KEY = 'module' and 
        CTRL_VALUE = v_module;
        
        
    end if;

    --check for existence of the first access right
    
    select count(*) into v_grp_count from er_ctrltab 
    where CTRL_KEY = 'app_rights' and 
      CTRL_VALUE = v_newacc_va(1) ;

    plog.debug(pctx,'v_grp_count:' || v_grp_count);

    plog.debug(pctx,'v_newacc_va.first:' || v_newacc_va(1) );

    
    if v_grp_count = 0 then
        
        select max(CTRL_SEQ)
               into v_grp_seq
            from er_ctrltab   
            where CTRL_KEY = 'app_rights' ;


         for k in v_newacc_va.first..v_newacc_va.last
             loop
            v_grp_seq := v_grp_seq+1;

            plog.debug(pctx,'v_newacc_va.kt:' || v_newacc_va(k) );

                 
            -- insert group rights
            INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY, CTRL_SEQ ,ctrl_custom_col1)
            VALUES ( seq_er_ctrltab.nextval, v_newacc_va(k), v_newaccdesc_va(k), 'app_rights', v_grp_seq,v_newacctype_va(k) );
            
                --------insert account type modules

            INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY,CTRL_SEQ ) VALUES ( 
            seq_er_ctrltab.nextval, 'G', v_newacc_va(k), 'acc_type_rights',v_grp_seq);
    

            --insert into hide rights

            INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY,  CTRL_SEQ) VALUES ( 
            seq_er_ctrltab.nextval, v_newacc_va(k), v_module, 'hid_rights', v_module_seq);

            v_rights_str  := v_rights_str || '0';

             end loop;


        UPDATE ER_GRPS
        SET grp_rights = grp_rights || v_rights_str ;

        v_change_made := true;

    end if; -- if group rights are not setup

     if v_change_made  then

        COMMIT;
    end if;
   
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,185,11,'11_Lookup.sql',sysdate,'9.0.0 B#637-ET004');

commit;