--CodeList Value for Older IDM Form--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res3'
    and CODELST_SUBTYP = 'react';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res3','react','Reactive','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res3'
    and CODELST_SUBTYP = 'negi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res3','negi','Non-Reactive/Negative','N',2,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res4'
    and CODELST_SUBTYP = 'confm';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res4','confm','Confirmed','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res4'
    and CODELST_SUBTYP = 'unconfm';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res4','unconfm','Unconfirmed','N',2,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res4'
    and CODELST_SUBTYP = 'not_done';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res4','not_done','Not Done','N',3,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res5'
    and CODELST_SUBTYP = 'posi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res5','posi','Positive','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res5'
    and CODELST_SUBTYP = 'negi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res5','negi','Non-Reactive/Negative','N',2,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res5'
    and CODELST_SUBTYP = 'indeter';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res5','indeter','Indeterminate','N',3,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res5'
    and CODELST_SUBTYP = 'not_done';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res5','not_done','Not Done','N',4,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res6'
    and CODELST_SUBTYP = 'react';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res6','react','Reactive','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res6'
    and CODELST_SUBTYP = 'negi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res6','negi','Non-Reactive/Negative','N',2,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res6'
    and CODELST_SUBTYP = 'indeter';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res6','indeter','Indeterminate','N',3,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res7'
    and CODELST_SUBTYP = 'react';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res7','react','Reactive','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res7'
    and CODELST_SUBTYP = 'negi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res7','negi','Non-Reactive/Negative','N',2,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res7'
    and CODELST_SUBTYP = 'not_done';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res7','not_done','Not Done','N',3,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res8'
    and CODELST_SUBTYP = 'posi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res8','posi','Positive','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res8'
    and CODELST_SUBTYP = 'negi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res8','negi','Non-Reactive/Negative','N',2,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res8'
    and CODELST_SUBTYP = 'not_done';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res8','not_done','Not Done','N',3,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res9'
    and CODELST_SUBTYP = 'posi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res9','posi','Positive','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res9'
    and CODELST_SUBTYP = 'negi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res9','negi','Non-Reactive/Negative','N',2,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res10'
    and CODELST_SUBTYP = 'posi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res10','posi','Positive','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res10'
    and CODELST_SUBTYP = 'negi';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res10','negi','Non-Reactive/Negative','N',2,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res10'
    and CODELST_SUBTYP = 'indeter';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res10','indeter','Indeterminate','N',3,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_res10'
    and CODELST_SUBTYP = 'not_done';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'idm_res10','not_done','Not Done','N',4,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
--END--
--IDM N2--
--Question 1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'test_outcome2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res6' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res6'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 8--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res8' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res8'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res8' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res8'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'test_outcome2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome2'' and CODELST_SUBTYP in(''no'',''yes'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res4'' and CODELST_SUBTYP in(''confm'',''unconfm'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res4'' and CODELST_SUBTYP in(''confm'',''unconfm'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res4'' and CODELST_SUBTYP in(''confm'',''unconfm'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res4'' and CODELST_SUBTYP in(''confm'',''unconfm'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2');
commit;
  end if;
end;
/
--IDM N2B--
--Question 1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'test_outcome2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res6' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res6'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res7' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res7'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 8--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res8' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res8'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'test_outcome2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome2'' and CODELST_SUBTYP in(''no'',''yes'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res4'' and CODELST_SUBTYP in(''confm'',''unconfm'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res4'' and CODELST_SUBTYP in(''confm'',''unconfm'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res4'' and CODELST_SUBTYP in(''confm'',''unconfm'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res4'' and CODELST_SUBTYP in(''confm'',''unconfm'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--IDM N2C--
--Question 1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'test_outcome2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res7' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res7'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 8--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res7' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res7'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res7' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res7'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'test_outcome2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome2'' and CODELST_SUBTYP in(''no'',''yes'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res10' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res10'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2C');
commit;
  end if;
end;
/
--IDM N2D--
--Question 1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'test_outcome2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res7' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res7'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 8--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res7' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res7'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res7' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res7'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'test_outcome2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome2'' and CODELST_SUBTYP in(''no'',''yes'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/

--Question 15--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res8' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res8'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 20--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res10' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res10'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--IDM N2E--
--Question 1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'test_outcome2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cms_cert_lab_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 2.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 3.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hbc_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_1_2_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res7' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_rslt') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res7'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv1_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 8--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 8.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res3'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_total_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res7' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_sts') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 9.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res7'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igm_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res7' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 10.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res7'' and CODELST_SUBTYP in(''react'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'test_outcome2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 11.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome2'' and CODELST_SUBTYP in(''no'',''yes'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_hcv_perfrmd_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hiv_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hcv_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res9' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res9'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_west_nile_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res8' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res8'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nat_hbv_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/

--Question 16--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res8' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res8'' and CODELST_SUBTYP in(''posi'',''negi'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chagas_react_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 20--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 22--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res10' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 22.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res10'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 23--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ripa_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ripa_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 23.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ripa_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	Update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''idm_res5'' and CODELST_SUBTYP in(''posi'',''negi'',''indeter'')',',') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ripa_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--END--
--Removing the unwanted test from IDM Form--
--Deleting data from cb_question_grp--
delete from cb_question_grp where fk_question=(select pk_questions from cb_questions where ques_code='nat_hiv_hcv_perfrmd_date');
--End--
--Deleting data from cb_form_questions--
delete from cb_form_questions where fk_question=(select pk_questions from cb_questions where ques_code='nat_hiv_hcv_perfrmd_date');
--End--
--Deleting data from cb_questions--
delete from cb_questions where ques_code='nat_hiv_hcv_perfrmd_date';
--End--
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,185,5,'05_IDM_Form.sql',sysdate,'9.0.0 B#637-ET004');

commit;
