--STARTS ADDING COLUMN TO CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_SHIPMENT' AND COLUMN_NAME = 'NMDP_SHIP_EXCUSE_SUBMITTED_BY';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_SHIPMENT ADD NMDP_SHIP_EXCUSE_SUBMITTED_BY NUMBER(10,0)';
	commit;
  end if;
end;
/
 COMMENT ON COLUMN CB_SHIPMENT.NMDP_SHIP_EXCUSE_SUBMITTED_BY IS 'Stores who submitted the Excuse form';

--STARTS ADDING COLUMN TO CB_SHIPMENT TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_SHIPMENT' AND COLUMN_NAME = 'NMDP_SHIP_EXCUSE_SUBMITTED_ON';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_SHIPMENT ADD NMDP_SHIP_EXCUSE_SUBMITTED_ON DATE';
	commit;
  end if;
end;
/
 COMMENT ON COLUMN CB_SHIPMENT.NMDP_SHIP_EXCUSE_SUBMITTED_ON IS 'Stores when submitted the Excuse form';

set define off;

DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'ER_USER' AND COLUMN_NAME = 'USR_TITLE';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE ER_USER ADD(USR_TITLE VARCHAR2(20))';
END IF;
END;
/
--END

COMMENT ON COLUMN ER_USER.USR_TITLE IS 'Stores title of the user';

DECLARE
  v_column_exists NUMBER :=0;
BEGIN
  SELECT COUNT(*) INTO v_column_exists FROM USER_TAB_COLS WHERE TABLE_NAME = 'ER_USER' AND COLUMN_NAME = 'FK_CODELST_TABLEROWS';
IF (v_column_exists = 0) THEN
	EXECUTE IMMEDIATE 'ALTER TABLE ER_USER ADD(FK_CODELST_TABLEROWS NUMBER)';
END IF;
END;
/
--END

COMMENT ON COLUMN ER_USER.FK_CODELST_TABLEROWS IS 'Stores number of rows to be displayed for all tables';


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,185,21,'21_MISC_ALTER.sql',sysdate,'9.0.0 B#637-ET004');

commit;