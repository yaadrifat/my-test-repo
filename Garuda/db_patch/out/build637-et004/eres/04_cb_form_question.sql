--FMHQ Form VERSION N2--
DELETE FROM CB_FORM_QUESTIONS WHERE FK_FORM=(SELECT PK_FORM FROM CB_FORMS WHERE FORMS_DESC ='FMHQ' AND VERSION='N2');
--Question 1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='first_preg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='first_preg_ind'),null,null,null,'1','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_preg')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_preg'),null,null,null,'2','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_live_births')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_live_births'),null,null,null,'3','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),null,null,null,'4','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'4.a',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind'),null,null,null,'5','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_miscrg')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_miscrg'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'5.a',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),null,null,null,'6','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_stillborn')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_stillborn'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'6.a',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),null,null,null,'7','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'7.a',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 8--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind'),null,null,null,'8','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind'),null,null,null,'9','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind'),null,null,null,'10','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),null,null,null,'11','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'12','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'12.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'13','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'13.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'14','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'14.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'16','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'16.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'17','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'17.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'18','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'18.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'19','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'19.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 20--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'20','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'20.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'21','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'21.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymom'),'21.a.1',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydad'),'21.a.2',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babysib'),'21.a.3',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babygpa'),'21.a.4',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymomsib'),'21.a.5',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydadsib'),'21.a.6',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 22--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),null,null,null,'22','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 22.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_trans_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_trans_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'22.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 23--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'),null,null,null,'23','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 23.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'23.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 24--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),null,null,null,'24','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 24.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'24.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 25--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),null,null,null,'25','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 25.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'25.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 26--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),null,null,null,'26','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 27--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sickle_cell_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sickle_cell_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'27','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 27.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sickle_cell_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sickle_cell_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sickle_cell_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'27.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 28--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thalassemia_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thalassemia_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'28','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 28.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thalassemia_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thalassemia_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thalassemia_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'28.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/

--Question 29--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'29','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 29.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'29.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/

--Question 30--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'30','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 30.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'30.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 31--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'31','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 31.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'31.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 32--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'32','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 32.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'32.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 33--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'33','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 33.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'33.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 34--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_rbc_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_rbc_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'34','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 34.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_rbc_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_rbc_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_rbc_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'34.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 35--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),null,null,null,'35','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 36--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'36','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 36.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'36.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 37--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'37','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 37.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'37.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 38--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_wbc_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_wbc_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'38','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 38.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_wbc_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_wbc_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_wbc_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'38.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 39--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),null,null,null,'39','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 40--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'40','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 40.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'40.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 41--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'41','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 41.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'41.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 42--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'42','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 42.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'42.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 43--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'43','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 43.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'43.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 44--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'44','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 44.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'44.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 45--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immune_defi_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immune_defi_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'45','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 45.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immune_defi_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immune_defi_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immune_defi_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'45.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 46--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),null,null,null,'46','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 47--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tay_sachs_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tay_sachs_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'47','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 47.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tay_sachs_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tay_sachs_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tay_sachs_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'47.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 48--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leukody_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leukody_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'48','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 48.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leukody_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leukody_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leukody_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'48.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 49--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'49','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 49.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'49.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 50--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'50','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 50.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'50.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 51--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'51','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 51.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'51.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 52--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_syn')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_syn'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'52','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 52.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_syn_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_syn_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_syn'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'52.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 53--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'53','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 53.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'53.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 54--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'54','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 54.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'54.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 55--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metabolic_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metabolic_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'55','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 55.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metabolic_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metabolic_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metabolic_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'55.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 56--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),null,null,null,'56','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 57--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanzmann_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanzmann_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'57','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 57.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanzmann_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanzmann_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanzmann_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'57.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 58--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'58','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 58.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'58.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 59--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'59','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 59.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'59.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 60--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_plat_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_plat_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'60','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 60.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_plat_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_plat_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_plat_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'60.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 61--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),null,null,null,'61','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 62--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'62','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 62.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'62.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 63--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'63','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 63.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'63.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 64--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'64','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 64.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'64.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 65--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_immun_sys')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_immun_sys'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'65','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 65.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_immun_sys_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_immun_sys_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_immun_sys'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'65.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 66--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),null,null,null,'66','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 67--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='creutz_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='creutz_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'67','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 67.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='creutz_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='creutz_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='creutz_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'67.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 68--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_neuo_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_neuo_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'68','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 68.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_neuo_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_neuo_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_neuo_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'68.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),null,null,null,'69','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'69.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymom'),'69.a.1',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydad'),'69.a.2',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babysib'),'69.a.3',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babygpa'),'69.a.4',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymomsib'),'69.a.5',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydadsib'),'69.a.6',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 70--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind'),null,null,null,'70','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--END--
--FMHQ Form VERSION N2B--
DELETE FROM CB_FORM_QUESTIONS WHERE FK_FORM=(SELECT PK_FORM FROM CB_FORMS WHERE FORMS_DESC ='FMHQ' AND VERSION='N2B');
--Question 1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='first_preg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='first_preg_ind'),null,null,null,'1','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_preg')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_preg'),null,null,null,'2','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_live_births')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_live_births'),null,null,null,'3','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),null,null,null,'4','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 4.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'4.a',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind'),null,null,null,'5','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_miscrg')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_miscrg'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='miscrg_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'5.a',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),null,null,null,'6','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_stillborn')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='num_stillborn'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'6.a',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),null,null,null,'7','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'7.a',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 8--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind'),null,null,null,'8','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 9--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind'),null,null,null,'9','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind'),null,null,null,'10','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),null,null,null,'11','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'12','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 12.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'12.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'13','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'13.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'14','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'14.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'16','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'16.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'17','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'17.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'18','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'18.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'19','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'19.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 20--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'20','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'20.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'21','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'21.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymom'),'21.a.1',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydad'),'21.a.2',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babysib'),'21.a.3',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babygpa'),'21.a.4',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymomsib'),'21.a.5',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydadsib'),'21.a.6',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 22--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),null,null,null,'22','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 22.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_trans_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_trans_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'22.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 23--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'),null,null,null,'23','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 23.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'23.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 24--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),null,null,null,'24','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 24.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'24.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 25--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),null,null,null,'25','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 25.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'25.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 26--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),null,null,null,'26','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 27--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sickle_cell_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sickle_cell_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'27','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 27.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sickle_cell_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sickle_cell_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sickle_cell_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'27.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 28--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thalassemia_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thalassemia_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'28','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 28.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thalassemia_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thalassemia_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thalassemia_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'28.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/

--Question 29--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'29','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 29.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'29.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/

--Question 30--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'30','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 30.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'30.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 31--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'31','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 31.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'31.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 32--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'32','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 32.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'32.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 33--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'33','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 33.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'33.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 34--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_rbc_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_rbc_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'34','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 34.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_rbc_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_rbc_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_rbc_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'34.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 35--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),null,null,null,'35','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 36--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'36','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 36.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'36.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 37--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'37','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 37.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'37.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 38--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_wbc_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_wbc_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'38','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 38.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_wbc_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_wbc_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_wbc_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'38.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 39--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),null,null,null,'39','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 40--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'40','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 40.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'40.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 41--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'41','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 41.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'41.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 42--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'42','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 42.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'42.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 43--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'43','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 43.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'43.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 44--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'44','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 44.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'44.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 45--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immune_defi_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immune_defi_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'45','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 45.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immune_defi_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immune_defi_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immune_defi_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'45.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 46--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),null,null,null,'46','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 47--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tay_sachs_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tay_sachs_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'47','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 47.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tay_sachs_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tay_sachs_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tay_sachs_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'47.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 48--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leukody_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leukody_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'48','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 48.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leukody_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leukody_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leukody_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'48.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 49--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'49','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 49.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'49.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 50--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'50','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 50.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'50.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 51--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'51','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 51.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'51.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 52--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_syn')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_syn'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'52','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 52.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_syn_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_syn_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_syn'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'52.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 53--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'53','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 53.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'53.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 54--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'54','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 54.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'54.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 55--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metabolic_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metabolic_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'55','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 55.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metabolic_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metabolic_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metabolic_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'55.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 56--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),null,null,null,'56','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 57--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanzmann_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanzmann_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'57','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 57.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanzmann_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanzmann_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanzmann_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'57.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 58--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'58','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 58.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'58.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 59--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'59','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 59.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'59.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 60--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_plat_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_plat_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'60','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 60.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_plat_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_plat_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_plat_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'60.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 61--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),null,null,null,'61','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 62--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'62','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 62.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'62.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 63--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'63','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 63.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'63.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 64--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'64','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 64.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'64.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 65--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_immun_sys')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_immun_sys'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'65','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 65.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_immun_sys_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_immun_sys_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='immu_sys_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_immun_sys'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'65.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 66--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),null,null,null,'66','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 67--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='creutz_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='creutz_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'67','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 67.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='creutz_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='creutz_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='creutz_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'67.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 68--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_neuo_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_neuo_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'68','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 68.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_neuo_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_neuo_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='neurolo_diso'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_neuo_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'68.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),null,null,null,'69','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'69.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymom'),'69.a.1',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydad'),'69.a.2',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babysib'),'69.a.3',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babygpa'),'69.a.4',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymomsib'),'69.a.5',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 69.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydadsib'),'69.a.6',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 70--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind'),null,null,null,'70','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--END--
--FORM VERSION N2C--
--FMHQ Form VERSION N2--
DELETE FROM CB_FORM_QUESTIONS WHERE FK_FORM=(SELECT PK_FORM FROM CB_FORMS WHERE FORMS_DESC ='FMHQ' AND VERSION='N2C');
--QUESTION 1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind'),null,null,null,'1','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--QUESTION 1.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_medhist_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_medhist_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'1.a','fmhq_res1',null,null,sysdate,null,null,null, null,null);
commit;
  end if;
end;
/
--QUESTION 2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='baby_moth_fath_bld_rel_ind'),null,null,null,'2','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--QUESTION 3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind'),null,null,null,'3','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--QUESTION 3.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='dnr_egg_sperm_fmhq_avail_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='dnr_egg_sperm_fmhq_avail_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='preg_use_dnr_egg_sperm_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'3.a','fmhq_res1',null,null,sysdate,null,null,null, null,null);
commit;
  end if;
end;
/
--QUESTION 4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),null,null,null,'4','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--QUESTION 4.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_testname')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_testname'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'4.a',null, null,null,sysdate,null,null,null, null,null);
commit;
  end if;
end;
/
--QUESTION 4.b--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_abort_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'4.b',null, null,null,sysdate,null,null,null, null,null);
commit;
  end if;
end;
/
--QUESTION 4.c--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_diag_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_diag_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'4.c','fmhq_res1', null,null,sysdate,null,null,null, null,null);
commit;
  end if;
end;
/
--QUESTION 4.c.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_diag_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_diag_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='abn_prenat_test_rslt_diag_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'4.c.1',null, null,null,sysdate,null,null,null, null,null);
commit;
  end if;
end;
/
--QUESTION 5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),null,null,null,'5','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--QUESTION 5.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'5.a',null, null,null,sysdate,null,null,null, null,null);
commit;
  end if;
end;
/
--QUESTION 6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),null,null,null,'6','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--QUESTION 6.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_cause')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_cause'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'6.a',null, null,null,sysdate,null,null,null, null,null);
commit;
  end if;
end;
/
--Question 7--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),null,null,null,'7','fmhq_res1',null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'7.a','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='brnev_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'7.a.1','fmhq_res2',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.b--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'7.b','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.b.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bone_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'7.b.1','fmhq_res2',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.c--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'7.c','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.c.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kidney_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'7.c.1','fmhq_res2',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.i--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'7.i','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.i.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='skin_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'7.i.1','fmhq_res2',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.d--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'7.d','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.d.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='thyroid_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'7.d.1','fmhq_res2',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.e--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'7.e','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.e.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hodgkin_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'7.e.1','fmhq_res2',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.f-
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'7.f','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.f.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nonhodgkin_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'7.f.1','fmhq_res2',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.g--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'7.g','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.g.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='acmleuk_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'7.g.1','fmhq_res2',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.h--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'7.h','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.h.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='aclleuk_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'7.h.1','fmhq_res2',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.j--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'7.j','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.j.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='can_leuk_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'7.j.1','fmhq_res2',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.j.1.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res2' and codelst_subtyp='babymom'),'7.j.1.a',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.j.1.b--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res2' and codelst_subtyp='babydad'),'7.j.1.b',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 7.j.1.c--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_cncr_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res2' and codelst_subtyp='babysib'),'7.j.1.c',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 8--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),null,null,null,'8','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/

--Question 8.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'8.a','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 8.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='diamond_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'8.a.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 8.b--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'8.b','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 8.b.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ellipto_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'8.b.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 8.c--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'8.c','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 8.c.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gpd_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'8.c.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 8.d--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'8.d','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 8.d.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_rbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spherocy_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'8.d.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/

--Question 9--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),null,null,null,'9','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/

--Question 9.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'9.a','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 9.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chronic_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'9.a.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 9.b--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kostmann_synd')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kostmann_synd'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'9.b','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 9.b.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kostmann_synd_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kostmann_synd_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='kostmann_synd'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'9.b.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 9.c--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='schwachman_synd')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='schwachman_synd'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'9.c','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 9.c.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='schwachman_synd_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='schwachman_synd_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='schwachman_synd'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'9.c.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 9.d--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leuko_defic')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leuko_defic'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'9.d','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 9.d.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leuko_defic_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leuko_defic_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_wbc_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='leuko_defic'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'9.d.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),null,null,null,'10','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/

--Question 10.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'10.a','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adaorpnp_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'10.a.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.b--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cid_cvid_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cid_cvid_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'10.b','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.b.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cid_cvid_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cid_cvid_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cid_cvid_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'10.b.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.c--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'10.c','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.c.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='digo_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'10.c.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.d--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hlh_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hlh_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'10.d','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.d.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hlh_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hlh_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hlh_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'10.d.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.e--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'10.e','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.e.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hypoglob_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'10.e.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.f--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'10.f','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.f.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='nezelhoff_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'10.f.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.g--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'10.g','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.g.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='scid_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'10.g.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.h--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'10.h','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 10.h.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_imm_dfc_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='wiskott_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'10.h.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),null,null,null,'11','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/

--Question 11.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='amegaka_throbo_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='amegaka_throbo_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'11.a','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='amegaka_throbo_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='amegaka_throbo_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='amegaka_throbo_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'11.a.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.b--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanz_throbo_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanz_throbo_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'11.b','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.b.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanz_throbo_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanz_throbo_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='glanz_throbo_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'11.b.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.c--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'11.c','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.c.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hereditary_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'11.c.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.d--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'11.d','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.d.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'11.d.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.e--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='throbo_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='throbo_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'11.e','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.e.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='throbo_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='throbo_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='throbo_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'11.e.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.f--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'11.f','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.f.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ataxia_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'11.f.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.g--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'11.g','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 11.g.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_pltlt_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='fanconi_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'11.g.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_ind'),null,null,null,'12','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/

--Question 12.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'12.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 12.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymom'),'12.a.1',null,null,null, sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 12.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydad'),'12.a.2',null,null,null, sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 12.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babysib'),'12.a.3',null,null,null, sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 12.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babygpa'),'12.a.4',null,null,null, sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 12.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymomsib'),'12.a.5',null,null,null, sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 12.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydadsib'),'12.a.6',null,null,null, sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_sickle_cell_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_sickle_cell_ind'),null,null,null,'13','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 13.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_sickle_cell_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_sickle_cell_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_sickle_cell_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_sickle_cell_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'13.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_thalassemia_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_thalassemia_ind'),null,null,null,'14','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 14.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_thalassemia_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_thalassemia_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_thalassemia_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_thalassemia_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'14.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),null,null,null,'15','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_synd')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_synd'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.a','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_synd_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_synd_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_synd'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.a.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.b--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_sche_synd')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_sche_synd'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.b','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.b.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_sche_synd_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_sche_synd_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hurler_sche_synd'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.b.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.c--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_synd')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_synd'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.c','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.c.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_synd_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_synd_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hunter_synd'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.c.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.d--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_synd')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_synd'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.d','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.d.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_synd_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_synd_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sanfilipo_synd'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.d.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.e--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='morquio_synd')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='morquio_synd'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.e','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.e.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='morquio_synd_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='morquio_synd_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='morquio_synd'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.e.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.f--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='maroteaux_synd')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='maroteaux_synd'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.f','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.f.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='maroteaux_synd_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='maroteaux_synd_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='maroteaux_synd'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.f.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.g--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sly_synd')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sly_synd'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.g','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.g.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sly_synd_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sly_synd_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sly_synd'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.g.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.h--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='icell_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='icell_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.h','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.h.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='icell_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='icell_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='icell_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.h.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.i--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='globoid_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='globoid_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.i','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.i.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='globoid_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='globoid_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='globoid_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.i.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.j--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metachrom_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metachrom_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.j','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.j.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metachrom_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metachrom_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='metachrom_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.j.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.k--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adrenoleuk_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adrenoleuk_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.k','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.k.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adrenoleuk_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adrenoleuk_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='adrenoleuk_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.k.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.l--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sandhoff_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sandhoff_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.l','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.l.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sandhoff_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sandhoff_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sandhoff_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.l.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.m--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='taysach_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='taysach_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.m','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.m.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='taysach_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='taysach_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='taysach_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.m.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.n--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.n','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.n.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gaucher_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.n.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.o--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='niemann_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='niemann_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.o','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.o.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='niemann_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='niemann_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='niemann_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.o.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.p--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.p','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.p.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='porphyria_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.p.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.q--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'15.q','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.q.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'15.q.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.q.1.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymom'),'15.q.1.a',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.q.1.b--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydad'),'15.q.1.b',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.q.1.c--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babysib'),'15.q.1.c',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.q.1.d--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babygpa'),'15.q.1.d',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.q.1.e--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymomsib'),'15.q.1.e',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 15.q.1.f--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='other_meto_dis_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydadsib'),'15.q.1.f',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_ind'),null,null,null,'16','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_aids_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'16.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'),null,null,null,'17','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='crohns_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='crohns_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'17.a','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='crohns_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='crohns_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='crohns_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'17.a.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17.b--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'17.b','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17.b.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='lupus_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'17.b.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17.c--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mult_scl_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mult_scl_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'17.c','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17.c.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mult_scl_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mult_scl_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mult_scl_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'17.c.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17.d--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'17.d','fmhq_res5',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 17.d.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rheum_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res5' and codelst_subtyp='yes'),'17.d.1','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis'),null,null,null,'18','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'18.a','fmhq_res2',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 18.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res2' and codelst_subtyp='babymom'),'18.a.1',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 18.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res2' and codelst_subtyp='babydad'),'18.a.2',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 18.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res2' and codelst_subtyp='babysib'),'18.a.3',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),null,null,null,'19','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_trans_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_trans_ind_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_tranf_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'19.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 20--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'),null,null,null,'20','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'20.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),null,null,null,'21','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'21.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 22--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),null,null,null,'22','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 22.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'22.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 23--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cruetz_jakob_dis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cruetz_jakob_dis_ind'),null,null,null,'23','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 23.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cruetz_jakob_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cruetz_jakob_dis_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cruetz_jakob_dis_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cruetz_jakob_dis_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'23.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 24--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),null,null,null,'24','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 24.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res1' and codelst_subtyp='yes'),'24.a','fmhq_res3',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 24.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc1'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymom'),'24.a.1',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 24.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydad'),'24.a.2',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 24.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc3'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babysib'),'24.a.3',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 24.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc4'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babygpa'),'24.a.4',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 24.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc5'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babymomsib'),'24.a.5',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 24.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc6'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel'),(select pk_codelst from ER_CODELST where CODELST_TYPE='fmhq_res3' and codelst_subtyp='babydadsib'),'24.a.6',null,null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 25--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='FMHQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='answ_both_moth_fath_ind'),null,null,null,'25','fmhq_res1',null,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,185,4,'04_cb_form_question.sql',sysdate,'9.0.0 B#637-ET004');

commit;