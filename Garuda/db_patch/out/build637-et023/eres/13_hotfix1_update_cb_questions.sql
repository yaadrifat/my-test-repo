DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hbv_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG = '1' where QUES_CODE = 'nat_hbv_react_ind';
commit;
  end if;
end;
/

		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,223,13,'13_hotfix1_update_cb_questions.sql',sysdate,'9.0.0 B#637-ET023.01');
		commit;
