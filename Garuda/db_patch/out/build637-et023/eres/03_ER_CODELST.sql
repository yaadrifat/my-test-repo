--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_05';
  if (v_record_exists = 1) then
      UPDATE 
  ER_CODELST SET CODELST_CUSTOM_COL='The Final CBU Review task must be completed prior to shipping the CBU.  Ship Date is scheduled to occur on <Shipment Scheduled Date>'where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_05';
	commit;
  end if;
end;
/
--END--
		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,223,3,'03_ER_CODELST.sql',sysdate,'9.0.0 B#637-ET023');
		commit;