set define off;
update audit_mappedvalues set to_value= 'Fungal Culture Start Date' where mapping_type = 'Field' and from_value = 'CB_CORD.FUNG_CULT_DATE';
update audit_mappedvalues set to_value= 'Bacterial Culture Start Date' where mapping_type = 'Field' and from_value = 'CB_CORD.BACT_CULT_DATE';
update audit_mappedvalues set to_value= 'Visible to TC' where mapping_type = 'Field' and from_value = 'CB_ASSESSMENT.TC_VISIBILITY_FLAG';
update audit_mappedvalues set to_value= 'Assessment Comments' where mapping_type = 'Field' and from_value = 'CB_ASSESSMENT.ASSESSMENT_REMARKS';
update audit_mappedvalues set to_value= 'Assessment Response' where mapping_type = 'Field' and from_value = 'CB_ASSESSMENT.ASSESSMENT_FOR_RESPONSE';
commit;
alter trigger er_patlabs_au1 enable;
commit;
		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,223,18,'18_hotfix2_audit_mapvalues.sql',sysdate,'9.0.0 B#637-ET023.02');
		commit;