BEGIN
    SYS.DBMS_SCHEDULER.CREATE_JOB (
            job_name => '"ERES"."CordPercentageUpdate"',
            job_type => 'PLSQL_BLOCK',
            job_action => 'BEGIN SP_UPDATE_CORD_PERCENTAGE END;',
            number_of_arguments => 0,
            start_date => '13-APR-13 11.30.37.812000 AM',
            repeat_interval => 'freq=daily;byhour=0;byminute=0;bysecond=10',
            end_date => NULL,
            job_class => 'DEFAULT_JOB_CLASS',
            enabled => true,
            auto_drop => true,
            comments => 'Job to update percentage of those cords whose hla coming through ESB');
END;
/


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,253,4,'04_JOB_CordPercentageUpdate.sql',sysdate,'9.0.0 B#637-ET039');
commit;