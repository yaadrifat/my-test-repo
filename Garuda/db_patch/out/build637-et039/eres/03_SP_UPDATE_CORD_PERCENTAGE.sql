create or replace
PROCEDURE SP_UPDATE_CORD_PERCENTAGE
IS
CORD_ID VARCHAR2(20) := 24997;
MANDATORY_FIELD_COUNT NUMBER(10) := 0;
TOTAL_FIELD_COUNT NUMBER(10) := 0;
ROW_C CB_CORD%ROWTYPE;
COLLECTION_DATE_FLAG NUMBER(1) := 0;
CBU_SAMPLE_DATA cb_entity_samples%ROWTYPE;
CBU_SAMPLE_CURSOR SYS_REFCURSOR;
CBU_SAMPLE_INIT_VAR NUMBER(10) := 0;
CB_HLA_CURSOR SYS_REFCURSOR;
CB_HLA_DATA cb_hla%ROWTYPE;
CB_HLA_INIT_VAR NUMBER(10) := 0;
CB_HLA_ROW_COUNT NUMBER(10) := 0;
HLA_A NUMBER(10) := 0;
HLA_B NUMBER(10) := 0;
HLA_DRB1 NUMBER(10) := 0;
LIC_STAT NUMBER(10) := 0;
UNL_STAT NUMBER(10) := 0;
IDM_CNT NUMBER(10) := 0;
MRQ_CNT NUMBER(10) := 0;
FMHQ_CNT NUMBER(10) := 0;
CBU_SAMPLE_ROW_COUNT NUMBER(10) := 0; 
PRE_LAB_FK_TIMING_TEST NUMBER(10) := 0;
POST_LAB_FK_TIMING_TEST NUMBER(10) := 0;
PRE_MAND_TEST_CBU_VOL NUMBER(10) := 0;
POST_MAND_TEST_UNCRCT NUMBER(10) := 0;
POST_MAND_TEST_TCDAD NUMBER(10) := 0;
POST_MAND_TEST_CNRBC NUMBER(10) := 0;
POST_MAND_TEST_PERNRBC NUMBER(10) := 0;
POST_MAND_TEST_CFUCNT NUMBER(10) := 0;
POST_MAND_TEST_VIAB NUMBER(10) := 0;
PERCENTAGE NUMBER(10) := 0;
LAB_TEST_DATA er_labtest%ROWTYPE;
PRE_LAB_TEST_RESULTS er_patlabs%ROWTYPE;
POST_LAB_TEST_RESULTS er_patlabs%ROWTYPE;
PRE_LAB_TEST_RESULTS_CNT NUMBER(10) := 0;
POST_LAB_TEST_RESULTS_CNT NUMBER(10) := 0;
LAB_TEST_CURSOR SYS_REFCURSOR;
PRE_LAB_TEST_CURSOR SYS_REFCURSOR;
POST_LAB_TEST_CURSOR SYS_REFCURSOR;
PRE_MAND_TEST_CBU_VOL_EXIST BOOLEAN := false;
POST_MAND_TEST_UNCRCT_EXIST BOOLEAN := false;
POST_MAND_TEST_TCDAD_EXIST BOOLEAN := false;
POST_MAND_TEST_CNRBC_EXIST BOOLEAN := false;
POST_MAND_TEST_PERNRBC_EXIST BOOLEAN := false;
POST_MAND_TEST_CFUCNT_EXIST BOOLEAN := false;
POST_MAND_TEST_VIAB_EXIST BOOLEAN := false;
CORD_ESB_REF CB_CORD_REF_HLA_ESB_DATA%ROWTYPE;
cord_cursor SYS_REFCURSOR;

cursor cord_ref_cursor is select * from CB_CORD_REF_HLA_ESB_DATA where status = 0;

BEGIN

--dbms_output.put_line('CHECK FOR IDS');

OPEN cord_ref_cursor;
  LOOP
     FETCH cord_ref_cursor INTO CORD_ESB_REF;
     EXIT WHEN cord_ref_cursor%NOTFOUND;
        OPEN cord_cursor FOR SELECT * FROM CB_CORD WHERE (CORD_SEARCHABLE IS NULL OR CORD_SEARCHABLE=0) AND (CORD_ENTRY_PROGRESS IS NULL OR CORD_ENTRY_PROGRESS <> 100 )  AND PK_CORD = CORD_ESB_REF.FK_CORD;
          Loop
            Fetch cord_cursor into ROW_C;
            EXIT when cord_cursor%NOTFOUND;
               if row_c.cord_registry_id is not null then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.FK_CBB_ID is not null then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.CORD_LOCAL_CBU_ID is not null then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.registry_maternal_id is not null then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.CORD_ID_NUMBER_ON_CBU_BAG is not null and row_c.CORD_ID_NUMBER_ON_CBU_BAG!= '-1' then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.FK_CORD_CBU_LIC_STATUS is not null and row_c.FK_CORD_CBU_LIC_STATUS!= '-1' then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.FK_CBB_PROCEDURE is not null and row_c.FK_CBB_PROCEDURE!= '-1' then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.FK_CBU_DEL_TYPE is not null and row_c.FK_CBU_DEL_TYPE!= '-1' then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.FK_CBU_COLL_TYPE is not null and row_c.FK_CBU_COLL_TYPE!= '-1' then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.MULTIPLE_BIRTH is not null and row_c.MULTIPLE_BIRTH!= '-1' then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.CORD_BABY_BIRTH_DATE is not null then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.FK_CORD_BABY_GENDER_ID is not null and row_c.FK_CORD_BABY_GENDER_ID!= '-1' then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;   
               if row_c.FK_CODELST_ETHNICITY is not null and row_c.FK_CODELST_ETHNICITY!= '-1' then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.FK_SPECIMEN_ID is not null then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.PRCSNG_START_DATE is not null then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.FRZ_DATE is not null then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.FK_CORD_BACT_CUL_RESULT is not null and row_c.FK_CORD_BACT_CUL_RESULT!= '-1' then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.FK_CORD_FUNGAL_CUL_RESULT is not null and row_c.FK_CORD_FUNGAL_CUL_RESULT!= '-1' then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;               
               if row_c.FK_CORD_ABO_BLOOD_TYPE is not null and row_c.FK_CORD_ABO_BLOOD_TYPE!= '-1' then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;               
               if row_c.FK_CORD_RH_TYPE is not null and row_c.FK_CORD_RH_TYPE!= '-1' then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
               if row_c.FK_CORD_CBU_ELIGIBLE_STATUS is not null then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               else
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
--CHECK CBU COLLECTION DATE-----
               if row_c.FK_SPECIMEN_ID is not null then
                 SELECT case when spec.spec_collection_date is not null then 1 else 0 end coll_date_flag into COLLECTION_DATE_FLAG FROM er_specimen SPEC WHERE spec.pk_specimen = row_c.FK_SPECIMEN_ID; 
                 if COLLECTION_DATE_FLAG=1 then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
               else
                 TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
               end if;
----------------------------
--CBU Samples-------------
               select count(*) into CBU_SAMPLE_INIT_VAR from cb_entity_samples samp where samp.entity_id = row_c.pk_cord;
               open CBU_SAMPLE_CURSOR for select samp.* from cb_entity_samples samp where samp.entity_id = row_c.pk_cord;
               if CBU_SAMPLE_INIT_VAR=0 then
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
              else
               LOOP
                FETCH CBU_SAMPLE_CURSOR into CBU_SAMPLE_DATA;        
                EXIT when CBU_SAMPLE_CURSOR%NOTFOUND;
-------------------------if CBU_SAMPLE_CURSOR%found then--------
                 if CBU_SAMPLE_DATA.FILT_PAP_AVAIL is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
                 if CBU_SAMPLE_DATA.RBC_PEL_AVAIL is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
                 if CBU_SAMPLE_DATA.NO_EXT_DNA_ALI is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
                 if CBU_SAMPLE_DATA.NO_SER_ALI is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
                 if CBU_SAMPLE_DATA.NO_PLAS_ALI is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
                  if CBU_SAMPLE_DATA.NO_NON_VIA_ALI is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
                  if CBU_SAMPLE_DATA.NO_VIA_CEL_ALI is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
                  if CBU_SAMPLE_DATA.NO_OF_SEG_AVAIL is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
                  if CBU_SAMPLE_DATA.CBU_OT_REP_CON_FIN is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
                  if CBU_SAMPLE_DATA.CBU_NO_REP_ALT_CON is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
                 if CBU_SAMPLE_DATA.NO_SER_MAT_ALI is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
                 if CBU_SAMPLE_DATA.NO_PLAS_MAT_ALI is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
                 if CBU_SAMPLE_DATA.NO_EXT_DNA_MAT_ALI is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 end if;
                 if CBU_SAMPLE_DATA.SI_NO_MISC_MAT is not null then
                    MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 else
                    TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                end if; 
               End LOOP; 
               end if;        
               close CBU_SAMPLE_CURSOR;
        
------------------------HLA---
              select pk_codelst into HLA_A from er_codelst where codelst_subtyp='hla_a' and codelst_type='hla_locus';
              select pk_codelst into HLA_B from er_codelst where codelst_subtyp='hla_b' and codelst_type='hla_locus';
              select pk_codelst into HLA_DRB1 from er_codelst where codelst_subtyp='hla_drb1' and codelst_type='hla_locus';
              
              select count(*) into CB_HLA_INIT_VAR from cb_hla hla where hla.entity_id=row_c.pk_cord and hla.entity_type= (select pk_codelst from er_codelst where codelst_subtyp='CBU' and codelst_type='entity_type') 
              and hla.fk_source = (select pk_codelst from er_codelst where codelst_subtyp='cbu' and codelst_type='test_source') ;
              
              open CB_HLA_CURSOR for select hla.* from cb_hla hla where hla.entity_id=row_c.pk_cord and hla.entity_type= (select pk_codelst from er_codelst where codelst_subtyp='CBU' and codelst_type='entity_type') 
              and hla.fk_source = (select pk_codelst from er_codelst where codelst_subtyp='cbu' and codelst_type='test_source') ;
              if CB_HLA_INIT_VAR=0 then
                 TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                 TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
              else
              Loop
                FETCH CB_HLA_CURSOR into CB_HLA_DATA;
                EXIT when CB_HLA_CURSOR%NOTFOUND;
                if CB_HLA_CURSOR%FOUND then        
                   if CB_HLA_DATA.FK_HLA_CODE_ID is not null and CB_HLA_DATA.FK_HLA_CODE_ID= HLA_A then
                      if CB_HLA_DATA.FK_HLA_METHOD_ID is not null then
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                      else
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                      end if;
                      if CB_HLA_DATA.FK_HLA_ANTIGENEID1 is not null then
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                      else
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                      end if;
                   end if;
                   if CB_HLA_DATA.FK_HLA_CODE_ID is not null and CB_HLA_DATA.FK_HLA_CODE_ID= HLA_B then
                      if CB_HLA_DATA.FK_HLA_METHOD_ID is not null then
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                      else
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                      end if;
                      if CB_HLA_DATA.FK_HLA_ANTIGENEID1 is not null then
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                      else
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                      end if;
                   end if; 
                   if CB_HLA_DATA.FK_HLA_CODE_ID is not null and CB_HLA_DATA.FK_HLA_CODE_ID= HLA_DRB1 then
                      if CB_HLA_DATA.FK_HLA_METHOD_ID is not null then
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                      else
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                      end if;
                      if CB_HLA_DATA.FK_HLA_ANTIGENEID1 is not null then
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                      else
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                      end if;
                   end if; 
                end if; 
              End Loop;
              end if;
              close CB_HLA_CURSOR;
----------------------------------
--------------------------Forms DATA---
             select pk_codelst into LIC_STAT from er_codelst where codelst_subtyp='licensed' and codelst_type='licence';
             select pk_codelst into UNL_STAT from er_codelst where codelst_subtyp='unlicensed' and codelst_type='licence';
        
             if row_c.FK_CORD_CBU_LIC_STATUS is not null and row_c.FK_CORD_CBU_LIC_STATUS=LIC_STAT then
                SELECT COUNT(*) INTO IDM_CNT FROM CB_FORM_VERSION WHERE ENTITY_ID=row_c.pk_cord AND FK_FORM IN (select pk_form from cb_forms where forms_desc='IDM' and IS_CURRENT_FLAG=1);
                if IDM_CNT=1 then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                else  
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                end if;
             else if row_c.FK_CORD_CBU_LIC_STATUS is not null and row_c.FK_CORD_CBU_LIC_STATUS=UNL_STAT then
                SELECT COUNT(*) INTO MRQ_CNT FROM CB_FORM_VERSION WHERE ENTITY_ID=row_c.pk_cord AND FK_FORM IN (select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG=1);
                SELECT COUNT(*) INTO FMHQ_CNT FROM CB_FORM_VERSION WHERE ENTITY_ID=row_c.pk_cord AND FK_FORM IN (select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG=1);
                SELECT COUNT(*) INTO IDM_CNT FROM CB_FORM_VERSION WHERE ENTITY_ID=row_c.pk_cord AND FK_FORM IN (select pk_form from cb_forms where forms_desc='IDM' and IS_CURRENT_FLAG=1);
                if IDM_CNT=1 then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                else  
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                end if;
                if MRQ_CNT=1 then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                else  
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                end if;
                if FMHQ_CNT=1 then
                  MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                else  
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                end if;
             else
             TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;    
             end if;     
             end if;
-------------------------
---------------------LAB Test-----      
              select pk_codelst into PRE_LAB_FK_TIMING_TEST from er_codelst where codelst_subtyp='pre_procesing' and codelst_type='timing_of_test';
              select pk_codelst into POST_LAB_FK_TIMING_TEST from er_codelst where codelst_subtyp='post_procesing' and codelst_type='timing_of_test';
              
              open LAB_TEST_CURSOR for select * from er_labtest where pk_labtest in (select fk_testid from er_labtestgrp where er_labtestgrp.fk_labgroup in (select pk_labgroup from er_labgroup where group_type ='PC'))  order by LABTEST_SEQ;
              LOOP
                FETCH LAB_TEST_CURSOR INTO LAB_TEST_DATA;
                EXIT WHEN LAB_TEST_CURSOR%NOTFOUND;
                 IF LAB_TEST_DATA.labtest_shortname='CBU_VOL' THEN
                    PRE_MAND_TEST_CBU_VOL := LAB_TEST_DATA.PK_LABTEST;
                 END IF;
                 IF LAB_TEST_DATA.labtest_shortname='UNCRCT' THEN
                    POST_MAND_TEST_UNCRCT := LAB_TEST_DATA.PK_LABTEST;
                 END IF;
                 IF LAB_TEST_DATA.labtest_shortname='TCDAD' THEN
                    POST_MAND_TEST_TCDAD := LAB_TEST_DATA.PK_LABTEST;
                 END IF;
                 IF LAB_TEST_DATA.labtest_shortname='CNRBC' THEN
                    POST_MAND_TEST_CNRBC := LAB_TEST_DATA.PK_LABTEST;
                 END IF;
                 IF LAB_TEST_DATA.labtest_shortname='PERNRBC' THEN
                    POST_MAND_TEST_PERNRBC := LAB_TEST_DATA.PK_LABTEST;
                 END IF;
                 IF LAB_TEST_DATA.labtest_shortname='CFUCNT' THEN
                    POST_MAND_TEST_CFUCNT := LAB_TEST_DATA.PK_LABTEST;
                 END IF;
                 IF LAB_TEST_DATA.labtest_shortname='VIAB' THEN
                    POST_MAND_TEST_VIAB := LAB_TEST_DATA.PK_LABTEST;
                 END IF;
              END LOOP;
              CLOSE LAB_TEST_CURSOR;
              SELECT COUNT(*) INTO PRE_LAB_TEST_RESULTS_CNT FROM er_patlabs WHERE fk_specimen = row_c.fk_specimen_id AND FK_TIMING_OF_TEST = PRE_LAB_FK_TIMING_TEST;
              OPEN PRE_LAB_TEST_CURSOR FOR SELECT * FROM er_patlabs WHERE fk_specimen = row_c.fk_specimen_id AND FK_TIMING_OF_TEST = PRE_LAB_FK_TIMING_TEST;
              IF(PRE_LAB_TEST_RESULTS_CNT=0) THEN      
                  TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
              ELSE
                  LOOP
                  FETCH PRE_LAB_TEST_CURSOR INTO PRE_LAB_TEST_RESULTS;          
                  EXIT WHEN PRE_LAB_TEST_CURSOR%NOTFOUND;
                  IF(PRE_LAB_TEST_RESULTS.FK_TESTID IS NOT NULL AND PRE_LAB_TEST_RESULTS.FK_TESTID=PRE_MAND_TEST_CBU_VOL ) THEN
                     PRE_MAND_TEST_CBU_VOL_EXIST := true;
                     IF PRE_LAB_TEST_RESULTS.TEST_RESULT IS NOT NULL THEN              
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                     ELSE
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                     END IF;
                  END IF;
                  END LOOP;
                  IF PRE_MAND_TEST_CBU_VOL_EXIST=false THEN
                     TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                  END IF;
              END IF;      
              CLOSE PRE_LAB_TEST_CURSOR;
              
              SELECT COUNT(*) INTO POST_LAB_TEST_RESULTS_CNT FROM er_patlabs WHERE fk_specimen = row_c.fk_specimen_id AND FK_TIMING_OF_TEST = POST_LAB_FK_TIMING_TEST;
              OPEN POST_LAB_TEST_CURSOR FOR SELECT * FROM er_patlabs WHERE fk_specimen = row_c.fk_specimen_id AND FK_TIMING_OF_TEST = POST_LAB_FK_TIMING_TEST;
              IF(POST_LAB_TEST_RESULTS_CNT=0) THEN      
                TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
              ELSE
                LOOP
                 FETCH POST_LAB_TEST_CURSOR INTO POST_LAB_TEST_RESULTS;          
                 EXIT WHEN POST_LAB_TEST_CURSOR%NOTFOUND;
                 IF(POST_LAB_TEST_RESULTS.FK_TESTID IS NOT NULL AND POST_LAB_TEST_RESULTS.FK_TESTID=POST_MAND_TEST_UNCRCT ) THEN
                     POST_MAND_TEST_UNCRCT_EXIST := true;
                     IF POST_LAB_TEST_RESULTS.TEST_RESULT IS NOT NULL THEN
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                     ELSE
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                     END IF;
                 END IF;
                 IF(POST_LAB_TEST_RESULTS.FK_TESTID IS NOT NULL AND POST_LAB_TEST_RESULTS.FK_TESTID=POST_MAND_TEST_TCDAD ) THEN
                     POST_MAND_TEST_TCDAD_EXIST := true;
                     IF POST_LAB_TEST_RESULTS.TEST_RESULT IS NOT NULL  THEN
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                     ELSE
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                     END IF;
                 END IF; 
                 IF(POST_LAB_TEST_RESULTS.FK_TESTID IS NOT NULL AND POST_LAB_TEST_RESULTS.FK_TESTID=POST_MAND_TEST_CNRBC ) THEN
                     POST_MAND_TEST_CNRBC_EXIST := true;
                     IF POST_LAB_TEST_RESULTS.TEST_RESULT IS NOT NULL THEN
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                     ELSE
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                     END IF;
                 END IF; 
                 IF(POST_LAB_TEST_RESULTS.FK_TESTID IS NOT NULL AND POST_LAB_TEST_RESULTS.FK_TESTID=POST_MAND_TEST_PERNRBC ) THEN
                     POST_MAND_TEST_PERNRBC_EXIST := true;
                     IF POST_LAB_TEST_RESULTS.TEST_RESULT IS NOT NULL THEN
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                     ELSE
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                     END IF;
                 END IF;
                 IF(POST_LAB_TEST_RESULTS.FK_TESTID IS NOT NULL AND POST_LAB_TEST_RESULTS.FK_TESTID=POST_MAND_TEST_CFUCNT ) THEN
                     POST_MAND_TEST_CFUCNT_EXIST := true;
                     IF POST_LAB_TEST_RESULTS.TEST_RESULT IS NOT NULL THEN
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                     ELSE
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                     END IF;
                     IF POST_LAB_TEST_RESULTS.FK_TEST_METHOD IS NOT NULL AND POST_LAB_TEST_RESULTS.FK_TEST_METHOD != '-1' THEN
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                     ELSE
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                     END IF;
                 END IF;
                 IF(POST_LAB_TEST_RESULTS.FK_TESTID IS NOT NULL AND POST_LAB_TEST_RESULTS.FK_TESTID=POST_MAND_TEST_VIAB ) THEN
                     POST_MAND_TEST_VIAB_EXIST := true;
                     IF POST_LAB_TEST_RESULTS.TEST_RESULT IS NOT NULL THEN
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                     ELSE
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                     END IF;
                     IF POST_LAB_TEST_RESULTS.FK_TEST_METHOD IS NOT NULL AND POST_LAB_TEST_RESULTS.FK_TEST_METHOD != '-1' THEN
                         MANDATORY_FIELD_COUNT := MANDATORY_FIELD_COUNT+1;
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                     ELSE
                         TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                     END IF;
                 END IF;
                END LOOP;
                IF POST_MAND_TEST_UNCRCT_EXIST=false THEN
                     TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                END IF;
                IF POST_MAND_TEST_TCDAD_EXIST=false THEN
                     TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                END IF;
                IF POST_MAND_TEST_CNRBC_EXIST=false THEN
                     TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                END IF;
                IF POST_MAND_TEST_PERNRBC_EXIST=false THEN
                     TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                END IF;
                IF POST_MAND_TEST_CFUCNT_EXIST=false THEN
                     TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                     TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                END IF;
                IF POST_MAND_TEST_VIAB_EXIST=false THEN
                     TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1;
                     TOTAL_FIELD_COUNT := TOTAL_FIELD_COUNT+1; 
                END IF;
              END IF;
              CLOSE POST_LAB_TEST_CURSOR;
--------------------------------
               PERCENTAGE := (MANDATORY_FIELD_COUNT*100)/TOTAL_FIELD_COUNT;
--------------dbms_output.put_line('PERCENTAGE'||PERCENTAGE);
--------------dbms_output.put_line('MANDATORY_FIELD_COUNT'||MANDATORY_FIELD_COUNT);
--------------dbms_output.put_line('TOTAL_COUNT'||TOTAL_FIELD_COUNT);               
               update cb_cord set cord_entry_progress = PERCENTAGE,last_modified_date=sysdate,last_modified_by=CORD_ESB_REF.ENTERED_BY where pk_cord = row_c.pk_cord;
               commit;
               update cb_cord_ref_hla_esb_data set status =1 where pk_cord_ref = CORD_ESB_REF.PK_CORD_REF;
               commit;               
               MANDATORY_FIELD_COUNT := 0 ;
               TOTAL_FIELD_COUNT := 0;
               PERCENTAGE := 0;
          End Loop;
        close cord_cursor;
-------------dbms_output.put_line('CHECK FOR IDS count'||MANDATORY_FIELD_COUNT);
  END LOOP;
CLOSE cord_ref_cursor;
END;
/

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,253,3,'03_SP_UPDATE_CORD_PERCENTAGE.sql',sysdate,'9.0.0 B#637-ET039');
commit;