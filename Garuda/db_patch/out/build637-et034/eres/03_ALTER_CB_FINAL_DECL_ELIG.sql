--STARTS MODIFY THE COLUMN MRQ_QUES_1_AD_DETAIL FROM CB_FINAL_DECL_ELIG TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_FINAL_DECL_ELIG'
    AND COLUMN_NAME = 'MRQ_QUES_1_AD_DETAIL'; 

  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_FINAL_DECL_ELIG MODIFY (MRQ_QUES_1_AD_DETAIL VARCHAR2(500 CHAR))';
  end if;
end;
/

--END--


--STARTS MODIFY THE COLUMN MRQ_QUES_2_AD_DETAIL FROM CB_FINAL_DECL_ELIG TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_FINAL_DECL_ELIG'
    AND COLUMN_NAME = 'MRQ_QUES_2_AD_DETAIL'; 

  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_FINAL_DECL_ELIG MODIFY (MRQ_QUES_2_AD_DETAIL VARCHAR2(500 CHAR))';
  end if;
end;
/

--END--


--STARTS MODIFY THE COLUMN PHY_FIND_QUES_3_AD_DETAIL FROM CB_FINAL_DECL_ELIG TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_FINAL_DECL_ELIG'
    AND COLUMN_NAME = 'PHY_FIND_QUES_3_AD_DETAIL'; 

  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_FINAL_DECL_ELIG MODIFY (PHY_FIND_QUES_3_AD_DETAIL VARCHAR2(500 CHAR))';
  end if;
end;
/

--END--


--STARTS MODIFY THE COLUMN PHY_FIND_QUES_4_AD_DETAIL FROM CB_FINAL_DECL_ELIG TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_FINAL_DECL_ELIG'
    AND COLUMN_NAME = 'PHY_FIND_QUES_4_AD_DETAIL'; 

  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_FINAL_DECL_ELIG MODIFY (PHY_FIND_QUES_4_AD_DETAIL VARCHAR2(500 CHAR))';
  end if;
end;
/

--END--


--STARTS MODIFY THE COLUMN PHY_FIND_QUES_5_AD_DETAIL FROM CB_FINAL_DECL_ELIG TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_FINAL_DECL_ELIG'
    AND COLUMN_NAME = 'PHY_FIND_QUES_5_AD_DETAIL'; 

  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_FINAL_DECL_ELIG MODIFY (PHY_FIND_QUES_5_AD_DETAIL VARCHAR2(500 CHAR))';
  end if;
end;
/

--END--


--STARTS MODIFY THE COLUMN IDM_QUES_6_AD_DETAIL FROM CB_FINAL_DECL_ELIG TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_FINAL_DECL_ELIG'
    AND COLUMN_NAME = 'IDM_QUES_6_AD_DETAIL'; 

  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_FINAL_DECL_ELIG MODIFY (IDM_QUES_6_AD_DETAIL VARCHAR2(500 CHAR))';
  end if;
end;
/

--END--



--STARTS MODIFY THE COLUMN IDM_QUES_7_AD_DETAIL FROM CB_FINAL_DECL_ELIG TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_FINAL_DECL_ELIG'
    AND COLUMN_NAME = 'IDM_QUES_7_AD_DETAIL'; 

  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_FINAL_DECL_ELIG MODIFY (IDM_QUES_7_AD_DETAIL VARCHAR2(500 CHAR))';
  end if;
end;
/

--END--




--STARTS MODIFY THE COLUMN IDM_QUES_8_AD_DETAIL FROM CB_FINAL_DECL_ELIG TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_FINAL_DECL_ELIG'
    AND COLUMN_NAME = 'IDM_QUES_8_AD_DETAIL'; 

  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_FINAL_DECL_ELIG MODIFY (IDM_QUES_8_AD_DETAIL VARCHAR2(500 CHAR))';
  end if;
end;
/

--END--

--STARTS MODIFY THE COLUMN FINAL_ELIGIBLE_CREATOR FROM CB_CORD_FINAL_REVIEW TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_FINAL_REVIEW'
    AND COLUMN_NAME = 'FINAL_ELIGIBLE_CREATOR'; 

  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_CORD_FINAL_REVIEW MODIFY (FINAL_ELIGIBLE_CREATOR VARCHAR2(30 CHAR))';
  end if;
end;
/

--END--


INSERT INTO track_patches VALUES(seq_track_patches.nextval,243,3,'03_ALTER_CB_FINAL_DECL_ELIG.sql',sysdate,'9.0.0 B#637-ET034');
commit;