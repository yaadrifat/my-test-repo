set define off;
update audit_mappedvalues set to_value= 'Test Description' where mapping_type = 'Field' and from_value = 'ER_PATLABS.CUSTOM002';
COMMIT;


INSERT INTO track_patches VALUES(seq_track_patches.nextval,243,10,'10_AUDIT_MAPPEDVALUES.sql',sysdate,'9.0.0 B#637-ET034');
commit;