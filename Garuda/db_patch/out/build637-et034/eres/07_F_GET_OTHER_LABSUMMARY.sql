CREATE OR REPLACE FUNCTION F_GET_OTHER_LSUMMARY(PKCORDID NUMBER) RETURN SYS_REFCURSOR IS
OTHER_LAB_RESULTS SYS_REFCURSOR;
BEGIN
    OPEN OTHER_LAB_RESULTS FOR SELECT
    ltest.labtest_name,
    patlabs.test_result,
    TO_CHAR(patlabs.test_date,'Mon DD, YYYY') test_date,
    f_codelst_desc(patlabs.fk_test_specimen) sample_type,
    nvl(patlabs.fk_timing_of_test,0) TEST_FIND1,
    nvl(PATLABS.CUSTOM006,0) TEST_FIND2,
    patlabs.pk_patlabs
    FROM 
    er_patlabs patlabs
    left outer join er_labtest ltest on (patlabs.fk_testid=ltest.pk_labtest)
    left outer join er_labtestgrp lgrp on (lgrp.fk_testid=ltest.pk_labtest)
    left outer join er_labgroup grp on (grp.pk_labgroup=lgrp.fk_labgroup)
    where grp.group_name='OTHERLABGROUP' AND grp.group_type='OLBT' and patlabs.fk_specimen = (SELECT FK_SPECIMEN_ID FROM CB_CORD WHERE PK_CORD=PKCORDID); 
    RETURN other_lab_results;
END;
/


INSERT INTO track_patches VALUES(seq_track_patches.nextval,243,7,'07_F_GET_OTHER_LABSUMMARY.sql',sysdate,'9.0.0 B#637-ET034');
commit;