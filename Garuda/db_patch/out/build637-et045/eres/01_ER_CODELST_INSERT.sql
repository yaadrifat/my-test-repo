--STARTS Insert script in ER_CODELST--
DECLARE
 index_count  number := 0;
BEGIN
 SELECT COUNT(*) INTO index_count from er_codelst
   where codelst_type = 'VersionDate'
   AND codelst_subtyp = 'Version1.3';
 IF (index_count = 0) THEN
   insert into er_codelst(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values(SEQ_ER_CODELST.nextval,null,'VersionDate','Version1.3','25-MAY-2013','N',1,'Y',null,null,null,null,sysdate,null,null,null,null);
   dbms_output.put_line('One row created');
 ELSE
   dbms_output.put_line('record is already exists');
 END IF;
END;
/
commit;


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,263,1,'01_ER_CODELST_INSERT.sql',sysdate,'9.0.0 B#637-ET045');
commit;