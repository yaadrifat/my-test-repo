--CodeList Value for Older FMHQ Form--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'mrq_res3'
    and CODELST_SUBTYP = 'yes';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_res3','yes','Yes','N',1,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'mrq_res3'
    and CODELST_SUBTYP = 'no';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_res3','no','No','N',2,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'mrq_res3'
    and CODELST_SUBTYP = 'bankdntask';
  if (v_record_exists = 0) then
	Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID, CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_res3','bankdntask','Bank did not ask question','N',3,null,null,null,null, sysdate,sysdate,null,null,null,null);
commit;
  end if;
end;
/
--End--
--start update cb_form_questions table--
--Question 1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='donate_bld_different_nme_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'mrq_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='donate_bld_different_nme_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
commit;
  end if;
end;
/
--Question 31--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='give_cbu_test_hiv_aids_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'mrq_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='give_cbu_test_hiv_aids_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,189,6,'06_ER_CODELST_INSERT.sql',sysdate,'9.0.0 B#637-ET006');

commit;