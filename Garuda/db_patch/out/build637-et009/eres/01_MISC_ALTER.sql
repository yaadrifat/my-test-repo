 --Sql for Table CB_HLA to add column CB_HLA_ORDER_SEQ
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_HLA' AND column_name = 'CB_HLA_ORDER_SEQ'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_HLA add CB_HLA_ORDER_SEQ NUMBER(10)');
  end if;
end;
/
COMMENT ON COLUMN CB_HLA.CB_HLA_ORDER_SEQ IS 'store the seq id of hlas'; 
commit;

 --Sql for Table CB_BEST_HLA to add column CB_BEST_HLA_ORDER_SEQ
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_BEST_HLA' AND column_name = 'CB_BEST_HLA_ORDER_SEQ'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_BEST_HLA add CB_BEST_HLA_ORDER_SEQ NUMBER(10)');
  end if;
end;
/
COMMENT ON COLUMN CB_BEST_HLA.CB_BEST_HLA_ORDER_SEQ IS 'store the seq id of hlas'; 
commit;

--STARTS ADDING COLUMN IN ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN

Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'ER_CODELST' AND COLUMN_NAME = 'CODELST_CUSTOM_COL2';
  if (v_column_exists = 0) then
       execute immediate 'ALTER TABLE ER_CODELST ADD CODELST_CUSTOM_COL2 VARCHAR2(4000 BYTE)';
  end if;
end;
/
--END--

COMMENT ON COLUMN ER_CODELST.CODELST_CUSTOM_COL2 IS 'This column can be used to store anything.Feel free to use.';

DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_IMPORT_HISTORY'
    AND COLUMN_NAME = 'IMPORT_FILE_NAME';

  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_CORD_IMPORT_HISTORY modify IMPORT_FILE_NAME varchar2(4000)';
      commit;
  end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,194,1,'01_MISC_ALTER.sql',sysdate,'9.0.0 B#637-ET009');

commit;