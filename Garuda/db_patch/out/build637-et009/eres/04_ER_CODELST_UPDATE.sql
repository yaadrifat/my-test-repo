DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'test_reason'
    AND codelst_subtyp = 'other';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_SEQ=6 where CODELST_TYPE='test_reason' and CODELST_SUBTYP='other';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'test_reason'
    AND codelst_subtyp = 'relTest';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_SEQ=5 where CODELST_TYPE='test_reason' and CODELST_SUBTYP='relTest';
  end if;
end;
/

--alert_01
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_01';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	A new <a href="<hiperlink>">CT</a> - request has been requested for <CBU Registry ID>.</br>Please use above link to process the request.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_01';
	commit;
  end if;
end;
/
--END--


--alert_02
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_02';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	A new <a href="<hiperlink>">CT</a> - request has been requested for <CBU Registry ID>.</br>Please use above link to process the request.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_02';
	commit;
  end if;
end;
/
--END--

--alert_03
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_03';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	A new <a href="<hiperlink>">HE</a> - request has been requested for <CBU Registry ID>.</br>Please use above link to process the request.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_03';
	commit;
  end if;
end;
/
--END--


--alert_04
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_04';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	A new <a href="<hiperlink>">OR</a> - request has been requested for <CBU Registry ID>.</br>Please use above link to process the request.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_04';
	commit;
  end if;
end;
/
--END--

--alert_05
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_05';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The Final CBU Review task for <a href="<hiperlink>"><CBU Registry ID></a> must be completed prior to shipping the CBU.  Ship Date is scheduled to occur tomorrow.</br>Please use above link to complete Final CBU Review task.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_05';
	commit;
  end if;
end;
/
--END--


--alert_06
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_06';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The Final CBU Review task for <a href="<hiperlink>"><CBU Registry ID></a> must be completed prior to shipping the CBU.  Ship Date is scheduled to occur on <Shipment Scheduled Date>.</br>Please use above link to complete Final CBU Review task.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_06';
	commit;
  end if;
end;
/
--END--

--alert_07
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_07';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	CBU Scheduled Ship Date has not been confirmed by CBB for <a href="<hiperlink>"><CBU Registry ID></a>.</br>Please use above link to confirm ship date.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_07';
	commit;
  end if;
end;
/
--END--


--alert_08
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_08';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	CBU Scheduled Ship Date has not been confirmed by CBB for<a href="<hiperlink>"><CBU Registry ID></a>.</br>Please use above link to confirm ship date.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_08';
	commit;
  end if;
end;
/
--END--

--alert_09
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_09';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	<a href="<hiperlink>">Updated HLA Typing</a> has been indicated but not entered.  Updated HLA must be entered prior to shipping sample.</br>Please use above link to enter Updated HLA.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_09';
	commit;
  end if;
end;
/
--END--

--alert_11
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_11';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	A CT Shipment Date for <a href="<hiperlink>"><CBU Registry ID></a> is overdue.</br>Please use above link to check CT Shipment date.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_11';
	commit;
  end if;
end;
/
--END--

--alert_15
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_15';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	<Request Type> Request for <a href="<hiperlink>"><CBU Registry ID></a> has been re-assigned to you.</br>Please use above link to process the request.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_15';
	commit;
  end if;
end;
/
--END--

--alert_16
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_16';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	Confirmatory Typing results are available for viewing for <a href="<hiperlink>"><CBU Registry ID></a>.</br>Please use above link to view the results.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_16';
	commit;
  end if;
end;
/
--END--


--alert_19
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_19';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The status for <a href="<hiperlink>"><CBU Registry ID></a> has changed to INFUSED.</br>Please use above link to view the results.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_19';
	commit;
  end if;
end;
/
--END--


--alert_20
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_20';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The <a href="<hiperlink>"><Request Type></a> request for <CBU Registry ID> has been modified.</br>Please use above link to view the changes.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_20';
	commit;
  end if;
end;
/
--END--


--alert_23
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_23';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	<CBU or Patient> HLA Results have been uploaded for <a href="<hiperlink>"><CBU Registry ID></a> to the clinical record.</br>Please use above link to view HLA Results.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_23';
	commit;
  end if;
end;
/
--END--


--alert_24
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_24';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The Shipment Itinerary has been uploaded for <a href="<hiperlink>"><CBU Registry ID></a> to the clinical record.</br>Please use above link to view Shipment Itinenary.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_24';
	commit;
  end if;
end;
/
--END--


--alert_25
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_25';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The Declaration of Urgent Medical Need (DUMN) form has been uploaded for <a href="<hiperlink>"><CBU Registry ID></a> to the clinical record.</br>Please use above link to view Declaration of Urgent Medical Need (DUMN).</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_25';
	commit;
  end if;
end;
/
--END--


--alert_26
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp='alert_26';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The Receipt of Cord Blood Unit form has been uploaded for <a href="<hiperlink>"><CBU Registry ID></a> to the clinical record.</br>Please use above link to view Receipt of Cord Blood Unit form.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert' AND codelst_subtyp='alert_26';
	commit;
  end if;
end;
/
--END--


--alert_29
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp='alert_29';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The <a href="<hiperlink>"><Request Type></a> request for <CBU Registry ID> has been resolved as <Resolution Code>.</br>Please use above link to acknowledge the resolution.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert_resol' AND codelst_subtyp='alert_29';
	commit;
  end if;
end;
/
--END--

--alert_30
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp='alert_30';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The <a href="<hiperlink>"><Request Type></a> request for <CBU Registry ID> has been resolved as <Resolution Code>.</br>Please use above link to acknowledge the resolution.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert_resol' AND codelst_subtyp='alert_30';
	commit;
  end if;
end;
/
--END--

--alert_31
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp='alert_31';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The <a href="<hiperlink>"><Request Type></a> request for <CBU Registry ID> has been resolved as <Resolution Code>.</br>Please use above link to acknowledge the resolution.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert_resol' AND codelst_subtyp='alert_31';
	commit;
  end if;
end;
/
--END--


--alert_32
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp='alert_32';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The <a href="<hiperlink>"><Request Type></a> request for <CBU Registry ID> has been resolved as <Resolution Code>.</br>Please use above link to acknowledge the resolution.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert_resol' AND codelst_subtyp='alert_32';
	commit;
  end if;
end;
/
--END--


--alert_33
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp='alert_33';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The <a href="<hiperlink>"><Request Type></a> request for <CBU Registry ID> has been resolved as <Resolution Code>.</br>Please use above link to acknowledge the resolution.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert_resol' AND codelst_subtyp='alert_33';
	commit;
  end if;
end;
/
--END--

--alert_34
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp='alert_34';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The <a href="<hiperlink>"><Request Type></a> request for <CBU Registry ID> has been resolved as <Resolution Code>.</br>Please use above link to acknowledge the resolution.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert_resol' AND codelst_subtyp='alert_34';
	commit;
  end if;
end;
/
--END--


--alert_35
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp='alert_35';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The <a href="<hiperlink>"><Request Type></a> request for <CBU Registry ID> has been resolved as <Resolution Code>.</br>Please use above link to acknowledge the resolution.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert_resol' AND codelst_subtyp='alert_35';
	commit;
  end if;
end;
/
--END--


--alert_36
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp='alert_36';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The <a href="<hiperlink>"><Request Type></a> request for <CBU Registry ID> has been resolved as <Resolution Code>.</br>Please use above link to acknowledge the resolution.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert_resol' AND codelst_subtyp='alert_36';
	commit;
  end if;
end;
/
--END--


--alert_37
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp='alert_37';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The <a href="<hiperlink>"><Request Type></a> request for <CBU Registry ID> has been resolved as <Resolution Code>.</br>Please use above link to acknowledge the resolution.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert_resol' AND codelst_subtyp='alert_37';
	commit;
  end if;
end;
/
--END--

--alert_38
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp='alert_38';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The <a href="<hiperlink>"><Request Type></a> request for <CBU Registry ID> has been resolved as <Resolution Code>.</br>Please use above link to acknowledge the resolution.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert_resol' AND codelst_subtyp='alert_38';
	commit;
  end if;
end;
/
--END--


--alert_39
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert_resol'
    AND codelst_subtyp='alert_39';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = 'Dear <loggedinuser>,</br></br></br>	The <a href="<hiperlink>"><Request Type></a> request for <CBU Registry ID> has been resolved as <Resolution Code>.</br>Please use above link to acknowledge the resolution.</br></br></br>Kind Regards,</br>NMDP' where codelst_type = 'alert_resol' AND codelst_subtyp='alert_39';
	commit;
  end if;
end;
/
--END--




--STARTS DELETING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type='order_status' and codelst_subtyp='CANCELLED';
  if (v_record_exists = 1) then
     delete er_codelst where codelst_type='order_status' and codelst_subtyp='CANCELLED';
	commit;
  end if;
end;
/
--END--


commit;


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_SHORTNAME = 'PERCD3';
  if (v_record_exists = 1) then
      UPDATE ER_LABTEST SET LABTEST_NAME='% CD3 Cells in Total Mononuclear Cell Count' where LABTEST_SHORTNAME = 'PERCD3';
	commit;
  end if;
end;
/
--END--
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,194,4,'04_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 B#637-ET009');

commit;