--STARTS INSERTING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_ctrltab
    where CTRL_VALUE = 'emtrax_url';
  if (v_record_exists = 0) then
      insert into er_ctrltab(PK_CTRLTAB,CTRL_VALUE,CTRL_DESC) values(seq_er_ctrltab.nextval,'emtrax_url','http://192.168.192.250:8000/velos/jsp/nmdplogin.jsp');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_ctrltab
    where CTRL_VALUE = 'email_from';
  if (v_record_exists = 0) then
      insert into er_ctrltab(PK_CTRLTAB,CTRL_VALUE,CTRL_DESC) values(seq_er_ctrltab.nextval,'email_from','customersupport@veloseresearch.com');
	commit;
  end if;
end;
/
--END--

set define off;
--STARTS UPDATING RECORD INTO ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_ctrltab where ctrl_value='CB_SHIPMNTREP' and ctrl_key='app_rights';
  if (v_record_exists = 1) then
      update ER_CTRLTAB set 
CTRL_DESC='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shipment 
Packet' where ctrl_value='CB_SHIPMNTREP' and ctrl_key='app_rights';
	commit;
  end if;
end;
/
--END--
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,194,7,'07_ER_CTRLTAB_INSERT.sql',sysdate,'9.0.0 B#637-ET009');

commit;