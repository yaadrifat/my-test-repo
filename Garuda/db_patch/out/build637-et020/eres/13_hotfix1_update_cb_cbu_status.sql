--START UPDATE CB_CBU_STATUS--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'RSO' AND CODE_TYPE='Response';
  if (v_record_exists = 1) then
    update CB_CBU_STATUS set CBU_STATUS_DESC='Reserved for Other (RSO)' where CBU_STATUS_CODE = 'RSO' AND CODE_TYPE='Response';
commit;
  end if;
end;
/

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,217,13,'13_hotfix1_update_cb_cbu_status.sql',sysdate,'9.0.0 B#637-ET020.01');
		commit;