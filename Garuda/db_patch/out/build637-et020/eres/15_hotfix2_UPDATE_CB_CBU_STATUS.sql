DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'NR' AND CODE_TYPE='Resolution';
  if (v_record_exists = 1) then
    update CB_CBU_STATUS set FK_CBU_STATUS=NULL where CBU_STATUS_CODE = 'NR' AND CODE_TYPE='Resolution';
commit;
  end if;
end;
/
INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,217,15,'15_hotfix2_UPDATE_CB_CBU_STATUS.sql',sysdate,'9.0.0 B#637-ET020.02');
		commit;