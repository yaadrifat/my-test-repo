--------------------------------------------------one value for CB_CORD
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_CORD.PREG_REF_NUM','Pregnancy reference number');


INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,217,6,'06_CB_CORD_INSERT.sql',sysdate,'9.0.0 B#637-ET020');
		commit;
