

begin
 DBMS_SCHEDULER.drop_job (job_name => 'CREATE_WORKFLOW_JOB');
 end;
 /

begin
DBMS_SCHEDULER.create_job (
    job_name        => 'CREATE_WORKFLOW_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN SP_WORKFLOW_TEMP;CB_ALERTS_MAINTENANCE_PROC;END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'freq=SECONDLY;INTERVAL=10',
    end_date        => NULL,
    enabled         => TRUE,
    comments        => 'Job to create workflow.');
end;
/

begin
dbms_scheduler.run_job('CREATE_WORKFLOW_JOB');
end;
/

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,217,20,'20_hotfix2_scheduler_jobs.sql',sysdate,'9.0.0 B#637-ET020.02');
		commit;