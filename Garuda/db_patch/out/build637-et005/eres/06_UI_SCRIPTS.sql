SET SERVEROUTPUT ON;

DECLARE
V_ROW_COUNTS NUMBER;
BEGIN
      SELECT COUNT(*) INTO V_ROW_COUNTS FROM UI_PAGE WHERE PAGE_NAME='LANDING_PAGE' AND PAGE_CODE='29';
      
      IF V_ROW_COUNTS = 0  THEN
          INSERT INTO UI_PAGE(PK_PAGE,PAGE_CODE,PAGE_NAME) VALUES (SEQ_UI_PAGE.nextval,'29','LANDING_PAGE');
          COMMIT;
          DBMS_OUTPUT.PUT_LINE('DATE INSERTED IN UI_PAGE');
      END IF;
      
END;
/


DECLARE
V_ROW_COUNTS NUMBER;
BEGIN
      SELECT COUNT(*) INTO V_ROW_COUNTS FROM UI_WIDGET WHERE NAME='CBB SUMMARY'  AND WIDGET_DIV_ID='cbbProfDiv';
      
      IF V_ROW_COUNTS = 0  THEN
         INSERT INTO UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
                        VALUES(SEQ_UI_WIDGET.nextval,'CBB SUMMARY','cbbProfDiv','CBB SUMMARY',1,1,1,1,1,1,100,400,1024,1024);
         COMMIT;
          DBMS_OUTPUT.PUT_LINE('DATE INSERTED IN UI_WIDGET DIV1');
      END IF;
      
END;
/



DECLARE
V_ROW_COUNTS NUMBER;
BEGIN
      SELECT COUNT(*) INTO V_ROW_COUNTS FROM UI_WIDGET WHERE NAME='ALL ORDERS'  AND WIDGET_DIV_ID='allOrdersDiv';
      
      IF V_ROW_COUNTS = 0  THEN
         INSERT INTO UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
                        VALUES(SEQ_UI_WIDGET.nextval,'ALL ORDERS','allOrdersDiv','ALL ORDERS',1,1,1,1,1,1,100,400,1024,1024);
        COMMIT;
          DBMS_OUTPUT.PUT_LINE('DATE INSERTED IN UI_WIDGET DIV2');
      END IF;
      
END;
/


DECLARE
V_ROW_COUNTS NUMBER;
BEGIN
      SELECT COUNT(*) INTO V_ROW_COUNTS FROM UI_WIDGET WHERE NAME='CORD SHIPMENT'  AND WIDGET_DIV_ID='shipmentDiv';
      
      IF V_ROW_COUNTS = 0  THEN
         INSERT INTO UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
                        VALUES(SEQ_UI_WIDGET.nextval,'CORD SHIPMENT','shipmentDiv','CORD SHIPMENT',1,1,1,1,1,1,100,400,1024,1024);
          COMMIT;
          DBMS_OUTPUT.PUT_LINE('DATE INSERTED IN UI_WIDGET DIV3');
      END IF;
      
END;
/


DECLARE
V_ROW_COUNTS NUMBER;
BEGIN
      SELECT COUNT(*) INTO V_ROW_COUNTS FROM UI_WIDGET WHERE NAME='WORKFLOW'  AND WIDGET_DIV_ID='workflowOrdDiv';
      
      IF V_ROW_COUNTS = 0  THEN
         INSERT INTO UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
                        VALUES(SEQ_UI_WIDGET.nextval,'WORKFLOW','workflowOrdDiv','WORKFLOW',1,1,1,1,1,1,100,400,1024,1024);
         COMMIT;
          DBMS_OUTPUT.PUT_LINE('DATE INSERTED IN UI_WIDGET DIV4');
      END IF;
      
END;
/

DECLARE
V_ROW_COUNTS NUMBER;
BEGIN
      SELECT COUNT(*) INTO V_ROW_COUNTS FROM UI_WIDGET WHERE NAME='TASK LIST'  AND WIDGET_DIV_ID='taskLstDiv';
      
      IF V_ROW_COUNTS = 0  THEN
         INSERT INTO UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
                        VALUES(SEQ_UI_WIDGET.nextval,'TASK LIST','taskLstDiv','TASK LIST',1,1,1,1,1,1,100,400,1024,1024);
          COMMIT;
          DBMS_OUTPUT.PUT_LINE('DATE INSERTED IN UI_WIDGET DIV4');
      END IF;
      
END;
/


DECLARE
V_ROW_COUNTS NUMBER;
BEGIN
      SELECT COUNT(*) INTO V_ROW_COUNTS FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='LANDING_PAGE';
      
      IF V_ROW_COUNTS = 0  THEN
          INSERT INTO UI_WIDGET_CONTAINER(PK_CONTAINER,FK_PAGE_ID,CONTAINER_NAME) VALUES (SEQ_UI_WIDGET_CONTAINER.NEXTVAL,(SELECT PK_PAGE FROM UI_PAGE WHERE PAGE_NAME='LANDING_PAGE'),'LANDING_PAGE');
          COMMIT;
          DBMS_OUTPUT.PUT_LINE('DATE INSERTED IN UI_WIDGET_CONTAINER');
      END IF;
      
END;
/


DECLARE
  V_ROW_COUNTS NUMBER;
BEGIN
    SELECT COUNT(*) INTO V_ROW_COUNTS FROM UI_CONTAINER_WIDGET_MAP
    WHERE FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='LANDING_PAGE')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='cbbProfDiv');
    
    IF(V_ROW_COUNTS = 0) THEN
      INSERT INTO  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
                                   VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='LANDING_PAGE'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='cbbProfDiv'));
    COMMIT;
     DBMS_OUTPUT.PUT_LINE('DATE INSERTED IN UI_WIDGET_CONTAINER_MAP INSERT1');
    END IF;
    
end;
/

DECLARE
  V_ROW_COUNTS NUMBER;
BEGIN
    SELECT COUNT(*) INTO V_ROW_COUNTS FROM UI_CONTAINER_WIDGET_MAP
    WHERE FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='LANDING_PAGE')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='allOrdersDiv');
    
    IF(V_ROW_COUNTS = 0) THEN
      INSERT INTO  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
                                   VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='LANDING_PAGE'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='allOrdersDiv'));
    COMMIT;
     DBMS_OUTPUT.PUT_LINE('DATE INSERTED IN UI_WIDGET_CONTAINER_MAP INSERT2');
    END IF;
    
end;
/

DECLARE
  V_ROW_COUNTS NUMBER;
BEGIN
    SELECT COUNT(*) INTO V_ROW_COUNTS FROM UI_CONTAINER_WIDGET_MAP
    WHERE FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='LANDING_PAGE')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='shipmentDiv');
    
    IF(V_ROW_COUNTS = 0) THEN
      INSERT INTO  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
                                   VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='LANDING_PAGE'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='shipmentDiv'));
    COMMIT;
     DBMS_OUTPUT.PUT_LINE('DATE INSERTED IN UI_WIDGET_CONTAINER_MAP INSERT3');
    END IF;
    
end;
/


DECLARE
  V_ROW_COUNTS NUMBER;
BEGIN
    SELECT COUNT(*) INTO V_ROW_COUNTS FROM UI_CONTAINER_WIDGET_MAP
    WHERE FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='LANDING_PAGE')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='workflowOrdDiv');
    
    IF(V_ROW_COUNTS = 0) THEN
      INSERT INTO  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
                                   VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='LANDING_PAGE'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='workflowOrdDiv'));
    COMMIT;
     DBMS_OUTPUT.PUT_LINE('DATE INSERTED IN UI_WIDGET_CONTAINER_MAP INSERT4');
    END IF;
    
end;
/

DECLARE
  V_ROW_COUNTS NUMBER;
BEGIN
    SELECT COUNT(*) INTO V_ROW_COUNTS FROM UI_CONTAINER_WIDGET_MAP
    WHERE FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='LANDING_PAGE')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='taskLstDiv');
    
    IF(V_ROW_COUNTS = 0) THEN
      INSERT INTO  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
                                   VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='LANDING_PAGE'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='taskLstDiv'));
    COMMIT;
     DBMS_OUTPUT.PUT_LINE('DATE INSERTED IN UI_WIDGET_CONTAINER_MAP INSERT5');
    END IF;
    
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,187,6,'06_UI_SCRIPTS.sql',sysdate,'9.0.0 B#637-ET005');

commit;
