--STARTS ADDING COLUMN TO CB_CORD TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_CORD' AND COLUMN_NAME = 'MATERNAL_GUID';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_CORD ADD MATERNAL_GUID VARCHAR2(50 BYTE)';
	commit;
  end if;
end;
/
 COMMENT ON COLUMN CB_CORD.MATERNAL_GUID IS 'Stores maternal GUID value';
 
 
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,187,1,'01_MISC_ALTER.sql',sysdate,'9.0.0 B#637-ET005');

commit;