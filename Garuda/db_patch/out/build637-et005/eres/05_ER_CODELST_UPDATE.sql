
delete er_codelst where codelst_type='order_status' and codelst_subtyp='open_ordr';
commit;
delete from CB_NOTES where note_seq is null;
commit;
delete from ER_REPXSL where PK_REPXSL=174;
commit;
update er_codelst set codelst_seq='1' where codelst_type='order_status' and codelst_subtyp='AWA_HLA_RES';
update er_codelst set codelst_seq='2' where codelst_type='order_status' and codelst_subtyp='CANCELLED';
update er_codelst set codelst_seq='3' where codelst_type='order_status' and codelst_subtyp='close_ordr';
update er_codelst set codelst_seq='4' where codelst_type='order_status' and codelst_subtyp='cordshipped';
update er_codelst set codelst_seq='5' where codelst_type='order_status' and codelst_subtyp='HLA_RES_REC';
update er_codelst set codelst_seq='6' where codelst_type='order_status' and codelst_subtyp='NEW';
update er_codelst set codelst_seq='7' where codelst_type='order_status' and codelst_subtyp='PENDING';
update er_codelst set codelst_seq='8' where codelst_type='order_status' and codelst_subtyp='requested';
update er_codelst set codelst_seq='9' where codelst_type='order_status' and codelst_subtyp='RESERVED';
update er_codelst set codelst_seq='10' where codelst_type='order_status' and codelst_subtyp='resolved';
update er_codelst set codelst_seq='11' where codelst_type='order_status' and codelst_subtyp='SHIP_SCH';
update er_codelst set codelst_seq='1' where codelst_type='order_type' and codelst_subtyp='CT';
update er_codelst set codelst_seq='2' where codelst_type='order_type' and codelst_subtyp='HE';
update er_codelst set codelst_seq='3' where codelst_type='order_type' and codelst_subtyp='OR';
update er_codelst set codelst_seq='4' where codelst_type='order_type' and codelst_subtyp='ALL';
update er_codelst set codelst_seq='1' where codelst_type='order_priority' and codelst_subtyp='STN';
update er_codelst set codelst_seq='2' where codelst_type='order_priority' and codelst_subtyp='URG';
commit;



update cb_cbu_status set CBU_STATUS_DESC='Active' where CBU_STATUS_CODE='AC' and CODE_TYPE='Response';
commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,187,5,'05_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 B#637-ET005');

commit;


