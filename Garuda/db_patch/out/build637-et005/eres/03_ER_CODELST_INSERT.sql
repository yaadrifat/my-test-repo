--start updating er_codelst table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'immune_def'
    and CODELST_SUBTYP = 'cis';
  if (v_record_exists = 1) then
	update ER_CODELST set CODELST_DESC = 'Combined Immunodeficiency  Syndrome (CIS)' where CODELST_TYPE = 'immune_def' and CODELST_SUBTYP = 'cis';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'immune_def'
    and CODELST_SUBTYP = 'cvid';
  if (v_record_exists = 1) then
	update ER_CODELST set CODELST_DESC = 'Common Variable Immunodeficiency Disease (CVID)' where CODELST_TYPE = 'immune_def' and CODELST_SUBTYP = 'cvid';
commit;
  end if;
end;
/
--end--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,187,3,'03_ER_CODELST_INSERT.sql',sysdate,'9.0.0 B#637-ET005');

commit;