--start updating cb_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hbsag_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_HELP = '<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - No action<br/>If Not Done - Defer<br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/>NMDP and 10-CBA requires valid test for HBsAg1' where QUES_CODE = 'hbsag_react_ind';
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'anti_hcv_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_HELP = '<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - No action<br/>If Not Done - Defer<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/>NMDP and 10-CBA requires valid test for anti-HCV.' where QUES_CODE = 'anti_hcv_react_ind';
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hcv_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_HELP = '<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - No action<br/>If Not Done - Incomplete if the collection date is on or after May 25, 2005.<br/>Required if Question 10 is Not Done.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for reactive/positive tests.<br/>Accommodates incomplete eligiblity status when testing not performed using FDA approved donor screening tests; implemented on October 1, 2011.' where QUES_CODE = 'nat_hcv_react_ind';
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hbv_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_HELP = '<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - No action<br/>If Not Done - No Action<br/>Optional if Question 10 is Not Done.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for reactive/positive tests.<br/>Optional test as of October 1, 2011.' where QUES_CODE = 'nat_hbv_react_ind';
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_west_nile_react_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_HELP = '<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - No action<br/>If Not Done - Incomplete if the collection date is on or after May 25, 2005.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for reactive/positive tests.<br/>Accommodates incomplete eligibility status when testing not performed using FDA approved donor screening tests; implemented on October 1, 2011.<br/>WNV NAT testing was required by the NMDP by October 19, 2004.<br/>' where QUES_CODE = 'nat_west_nile_react_ind';
commit;
  end if;
end;
/
--FOR FMHQ Form--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG = '1' where QUES_CODE = 'inher_other_dises_rel';
commit;
  end if;
end;
/
--end--
--start update cb_form_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set UNEXPECTED_RESPONSE_VALUE = 'fmhq_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_rel') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--end--
--FOR IDM FORM N2E--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'ripa_ct_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC = 'Chages' where QUES_CODE = 'ripa_ct_ind';
commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,187,4,'04_CB_QUESTION_UPDATE.sql',sysdate,'9.0.0 B#637-ET005');

commit;