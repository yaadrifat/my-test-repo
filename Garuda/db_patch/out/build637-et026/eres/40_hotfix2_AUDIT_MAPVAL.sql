set define off;
update audit_mappedvalues set to_value= 'Confirm CBB Signature for final CBU review' where mapping_type = 'Field' and from_value = 'CB_CORD_FINAL_REVIEW.CONFIRM_CBB_SIGN_FLAG';
update audit_mappedvalues set to_value= 'Final CBU Review' where mapping_type = 'Field' and from_value = 'CB_CORD_FINAL_REVIEW.REVIEW_CORD_ACCEPTABLE_FLAG';
update audit_mappedvalues set to_value= 'Appropriate approval obtained for CT (A, B, DRB1) completed' where mapping_type = 'FIELD' and from_value = 'CB_CORD_MINIMUM_CRITERIA.CTCOMPLETED_CHILD';
update audit_mappedvalues set to_value= 'Please specify reason for change' where mapping_type = 'Field' and from_value = 'CB_CORD.CB_IDS_CHANGE_REASON';
commit;


		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,229,39,'40_hotfix2_AUDIT_MAPVAL.sql',sysdate,'9.0.0 B#637-ET026.02');
		commit;
