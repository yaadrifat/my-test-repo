update audit_mappedvalues set to_value= 'CT (A, B, DRB1) completed' where mapping_type = 'FIELD' and from_value = 'CB_CORD_MINIMUM_CRITERIA.CTCOMPLETED';
update audit_mappedvalues set to_value= 'Appropriate approval obtained for CT (A, B, DRB1) completed' where mapping_type = 'FIELD' and from_value = 'CTCOMPLETED_CHILD';
update audit_mappedvalues set to_value= 'CBU planned arrival is prior to prep start' where mapping_type = 'FIELD' and from_value = 'CB_CORD_MINIMUM_CRITERIA.CBUPLANNED';
update audit_mappedvalues set to_value= 'Unit and recipient at least 4/6 antigen match (A, B, DRB1)' where mapping_type = 'FIELD' and from_value = 'CB_CORD_MINIMUM_CRITERIA.ANTIGEN_MATCH';
update audit_mappedvalues set to_value= 'Appropriate approval obtained for Unit and recipient at least 3/6 antigen match (antigen A, B; allele DRB1)' where mapping_type = 'FIELD' and from_value = 'CB_CORD_MINIMUM_CRITERIA.UNITRECIPIENT_CHILD';
update audit_mappedvalues set to_value= 'Appropriate approval obtained for CBU planned arrival is prior to prep start' where mapping_type = 'FIELD' and from_value = 'CB_CORD_MINIMUM_CRITERIA.CBUPLANNED_CHILD';
update audit_mappedvalues set to_value= 'Appropriate approval obtained for Patient typing reported' where mapping_type = 'FIELD' and from_value = 'CB_CORD_MINIMUM_CRITERIA.PATIENTTYPING_CHILD';
update audit_mappedvalues set to_value= 'Appropriate approval obtained for Viability (post-processing / pre-cryopreservation)' where mapping_type = 'FIELD' and from_value = 'CB_CORD_MINIMUM_CRITERIA.VIABILITY_CHILD';
update audit_mappedvalues set to_value= 'Local Cord Status' where mapping_type = 'Field' and from_value = 'CB_CORD.FK_CORD_CBU_STATUS';
update audit_mappedvalues set to_value= 'Cord Registry Status' where mapping_type = 'Field' and from_value = 'CB_CORD.CORD_NMDP_STATUS';
update audit_mappedvalues set to_value= 'Complete Required Information' where mapping_type = 'Field' and from_value = 'ER_ORDER.COMPLETE_REQ_INFO_TASK_FLAG';
update audit_mappedvalues set to_value= 'NMDP''s minimum criteria to ship a CBU and confirm' where mapping_type = 'Field' and from_value = 'CB_CORD_MINIMUM_CRITERIA.DECLARATION_RESULT';
commit;

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'FIELD' and FROM_VALUE = 'CB_CORD_MINIMUM_CRITERIA.POSTPROCESS_CHILD';
  if (v_record_exists = 0) then
	INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.NEXTVAL,'MODCDRPF','FIELD','CB_CORD_MINIMUM_CRITERIA.POSTPROCESS_CHILD','Appropriate approval obtained for Total CBU Nucleated Cell Count (post-processing/pre-cryopreservation) / kg pt weight');
	commit;
  end if;
end;
/

		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,229,26,'26_hotfix1_Audit_mappval.sql',sysdate,'9.0.0 B#637-ET026.01');
		commit;
