Declare
  v_patch_exists BOOLEAN;  
Begin
	V_Patch_Exists := F_Check_Previous_Patch('v9.0.0 Build#637-ET026.02');
	If (V_Patch_Exists) Then
		UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v9.0.0 Build#637-ET026.03'
		where CTRL_KEY = 'app_version' ;
		commit;
		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,229,44,'44_hotfix3_er_version.sql',sysdate,'9.0.0 B#637-ET026.03');
		commit;
		--dbms_output.put_line('Process successfully');
	Else
		--dbms_output.put_line('The database, where current patches are executed, is not updated to last the build : v9.0.0 Build#637-ET013.02');
		raise_application_error(-20001, 'The database, where current patches are executed, is not updated to last the build : v9.0.0 Build#637-ET026.02');
	End if;
End;
/