DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'cfu_cnt'
    AND codelst_subtyp='total';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_desc ='Total(BFUE+BM+GEMM)' where codelst_type = 'cfu_cnt' AND codelst_subtyp='total';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'test_method'
    AND codelst_subtyp='tot';
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_desc ='Total(BFUE+BM+GEMM)' where codelst_type = 'test_method' AND codelst_subtyp='tot';
  end if;
end;
/
		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,229,43,'43_hotfix2_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 B#637-ET026.02');
		commit;
