--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_05';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL1='System should alert the user if the Scheduled Ship Date is approaching and the "Final CBU Review" is not complete - 2 day prior to Scheduled Ship Date.' where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_05';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_04';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL1='User should be able to receive an alert when a new request is received for OR.' where codelst_type = 'alert'
    AND codelst_subtyp = 'alert_04';
	commit;
  end if;
end;
/
--END--

		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,229,31,'31_hotfix1_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 B#637-ET026.01');
		commit;
