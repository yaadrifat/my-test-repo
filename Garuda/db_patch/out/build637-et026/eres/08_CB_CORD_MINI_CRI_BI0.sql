create or replace TRIGGER CB_CORD_MINIMUM_CRITERIA_BI0 BEFORE INSERT ON CB_CORD_MINIMUM_CRITERIA     REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW  WHEN (NEW.rid IS NULL OR NEW.rid = 0) 
DECLARE
raid NUMBER(10);
erid NUMBER(10);
usr VARCHAR(2000);
insert_data VARCHAR2(4000);
 BEGIN
  BEGIN
 SELECT TO_CHAR(pk_user) ||', ' || usr_lastname ||', ' || usr_firstname
 INTO usr FROM er_user
 WHERE pk_user = :NEW.creator ;
 EXCEPTION WHEN NO_DATA_FOUND THEN
   USR := 'New User' ;
 END ;
 SELECT TRUNC(seq_rid.NEXTVAL)  INTO erid FROM dual;
  :NEW.rid := erid ;
  SELECT seq_audit.NEXTVAL INTO raid FROM dual;
  audit_trail.record_transaction(raid, 'CB_CORD_MINIMUM_CRITERIA',erid, 'I',usr);
       insert_data:= :NEW.PK_CORD_MINIMUM_CRITERIA || '|' ||	
to_char(:NEW.FK_CORD_ID) || '|' ||	
to_char(:NEW.BACTERIAL_TESTING_FLAG) || '|' ||	
to_char(:NEW.FUNGAL_TESTING_FLAG) || '|' ||	
to_char(:NEW.HEMOGLOBINOPTHY_FLAG) || '|' ||	
to_char(:NEW.IDMS_FLAG) || '|' ||	
to_char(:NEW.POST_PROCESS_DOSE) || '|' ||	
to_char(:NEW.POST_PROCESS_VIAB) || '|' ||	
to_char(:NEW.HLA_CONFIRM) || '|' ||	
to_char(:NEW.ANTIGENS_CONFIRM) || '|' ||	
to_char(:NEW.DECLARATION_RESULT) || '|' ||	
to_char(:NEW.SAVEINPROGESS) || '|' ||	
to_char(:NEW.CONFORMED_MINIMUM_CRITIRIA) || '|' ||	
to_char(:NEW.ANTIHIV_II_HIVP24_HIVNAT_COMP) || '|' ||	
to_char(:NEW.HBSAG_COMP) || '|' ||	
to_char(:NEW.ANTIHCV_COMP) || '|' ||	
to_char(:NEW.HLA_CNFM) || '|' ||	
to_char(:NEW.ANTIGEN_MATCH) || '|' ||	
erid || '|' ||	
to_char(:NEW.DELETEDFLAG) || '|' ||	
to_char(:NEW.HIVPNAT_COMP) || '|' ||	
to_char(:NEW.HLACHILD) || '|' ||	
to_char(:NEW.CTCOMPLETED) || '|' ||	
to_char(:NEW.CTCOMPLETED_CHILD) || '|' ||	
to_char(:NEW.ANTIGENCHILD) || '|' ||	
to_char(:NEW.UNITRECIPIENT) || '|' ||	
to_char(:NEW.UNITRECIPIENT_CHILD) || '|' ||	
to_char(:NEW.CBUPLANNED) || '|' ||	
to_char(:NEW.CBUPLANNED_CHILD) || '|' ||	
to_char(:NEW.PATIENTTYPING) || '|' ||	
to_char(:NEW.PATIENTTYPING_CHILD) || '|' ||	
to_char(:NEW.POSTPROCESS_CHILD) || '|' ||	
to_char(:NEW.VIABILITY_CHILD) || '|' ||	
to_char(:NEW.FK_ORDER_ID) || '|' ||	
to_char(:NEW.FK_RECIPIENT) || '|' ||	
usr || '|' ||	
to_char(:NEW.CREATED_ON) || '|' ||	
to_char(:NEW.LAST_MODIFIED_BY) || '|' ||	
to_char(:NEW.LAST_MODIFIED_DATE) || '|' ||	
to_char(:NEW.IP_ADD) || '|' ||	
to_char(:NEW.CT_COMPLETED_FLAG) || '|' ||	
to_char(:NEW.UNIT_RECP_FLAG) || '|' ||	
to_char(:NEW.PRIOR_TO_PREP_START) || '|' ||	
to_char(:NEW.PAT_TYPING);
 INSERT INTO audit_insert(raid, row_data) VALUES (raid, insert_data);
   END;
/
		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,229,8,'08_CB_CORD_MINI_CRI_BI0.sql',sysdate,'9.0.0 B#637-ET026');
		commit;