--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT in (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp in('alert_07','alert_08'))
    AND TABLE_NAME = 'TO_DATE(TO_CHAR(CB_SHIPMENT';
  if (v_record_exists = 2) then
      UPDATE 
  CB_ALERT_CONDITIONS SET ARITHMETIC_OPERATOR='<=' where FK_CODELST_ALERT in (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp in('alert_07','alert_08'))
    AND TABLE_NAME = 'TO_DATE(TO_CHAR(CB_SHIPMENT'; 
	commit;
  end if;
end;
/
--END--
		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,229,28,'28_hotfix1_CB_ALERT_CON_UPDATE.sql',sysdate,'9.0.0 B#637-ET026.01');
		commit;
