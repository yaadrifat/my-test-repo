/* This readMe is specific to Velos eResearch version 9.0 build637-et041 */

===================================================================================================================================== 
 Ref. Jazz Id-14849 : In order to resolve the Hibernate Jar conflict, following recommendations to be used, 
 A. Keep the following Jar files in the directory - 
 	 server\eresearch\deploy\velos.ear\velos.war\WEB-INF\lib\
 	1. hibernate-validator-3.1.0.GA.jar
 	2. hibernate-core-3.3.0.SP1.jar
 
 B. Remove the following Jar files from directory - 
 	server\eresearch\deploy\velos.ear\velos.war\WEB-INF\lib\
	1. hibernate-3.2.7.ga.jar
	2. hibernate-validator.jar
	[Note : No action is required for file hibernate-validator.jar if it doesn't exist in directory.]
=====================================================================================================================================

 