BEGIN
    SYS.DBMS_SCHEDULER.DROP_JOB(job_name => '"ERES"."CordPercentageUpdate"',
                                defer => false,
                                force => false);
END;
/							  

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,257,10,'10_DROP_JOB.sql',sysdate,'9.0.0 B#637-ET041');
commit;