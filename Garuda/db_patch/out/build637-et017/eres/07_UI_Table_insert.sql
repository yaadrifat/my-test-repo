DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'formshssaudittrail';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Audit Report','formshssaudittrail','Cord Audit Report',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Audit Trail')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='formshssaudittrail');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Audit Trail'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='formshssaudittrail'));
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'idaudittrail';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Audit Report','idaudittrail','Cord Audit Report',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Audit Trail')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='idaudittrail');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Audit Trail'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='idaudittrail'));
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'procInfoaudittrail';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Audit Report','procInfoaudittrail','Cord Audit Report',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Audit Trail')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='procInfoaudittrail');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Audit Trail'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='procInfoaudittrail'));
	commit;
  end if;
end;
/

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,211,7,'07_UI_Table_insert.sql',sysdate,'9.0.0 B#637-ET017');
		commit;