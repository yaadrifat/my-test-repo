create or replace
function f_get_assessdata(v_assessment_id number) return varchar2 as
v_mas_ques number;
v_dep_ques number;
v_dep_qval varchar2(2000);
v_dep_ques1 number;
v_dep_qval1 varchar2(2000);
v_dep_ques2 number;
v_dep_qval2 varchar2(2000);
v_dep_ques3 number;
v_dep_qval3 varchar2(2000);
v_ques_desc varchar2(2000);
v_mq_desc varchar2(2000);
v_dq_desc varchar2(2000);
v_dq1_desc varchar2(2000);
v_dq2_desc varchar2(2000);
v_dq3_desc varchar2(2000);
v_ques number;
v_qres varchar2(2000);
v_form number;
v_result varchar2(2000);
v_ass varchar2(2000);
v_qsec varchar2(2000);
v_dqsec varchar2(2000);
v_dqsec1 varchar2(2000);
v_dqsec2 varchar2(2000);
v_dqsec3 varchar2(2000);
begin
SELECT
  fk_question,
  fk_master_ques,
  fk_dependent_ques,
  fk_form,
  nvl(f_codelst_desc(fk_dependent_ques_value),''),
  ques_seq
into
  v_ques,
  v_mas_ques,
  v_dep_ques,
  v_form,
  v_dep_qval,
  v_qsec
FROM cb_form_questions
WHERE (fk_form,fk_question)=
  (SELECT fk_form,
    fk_question
  FROM cb_form_responses
  WHERE pk_form_responses=(SELECT sub_entity_id FROM cb_assessment WHERE pk_assessment=v_assessment_id)
  );
  SELECT assessment_remarks||'~'||f_codelst_desc(assessment_for_response)||'~'||case when tc_visibility_flag=1 then 'Yes' else 'No' end||'~'||column_reference into v_ass FROM cb_assessment WHERE pk_assessment=v_assessment_id;
  SELECT case when assessment_reason is not null then f_codelst_desc(assessment_reason) else assessment_reason_remarks end into v_qres FROM cb_assessment WHERE pk_assessment=v_assessment_id;
 -- dbms_output.put_line('v_ques.....'||v_ques);
  --dbms_output.put_line('v_qres.....'||v_qres);
  --dbms_output.put_line('v_mas_ques.....'||v_mas_ques);
  if v_mas_ques is not null then
  select nvl(ques_desc,'') into v_mq_desc from cb_questions where pk_questions=v_mas_ques;
  end if;
  --dbms_output.put_line('v_dep_ques::::::::'||v_dep_ques);
 --dbms_output.put_line('v_form::::::::'||v_form);
 if v_ques is not null then 
     select ques_desc into v_ques_desc from cb_questions where pk_questions=v_ques;
  end if;
  if v_dep_ques is not null then 
     select ques_desc into v_dq_desc from cb_questions where pk_questions=v_dep_ques;
  end if;
  if v_dep_ques is not null then 
    select fk_dependent_ques,f_codelst_desc(fq.fk_dependent_ques_value),ques_seq into v_dep_ques1,v_dep_qval1,v_dqsec from cb_form_questions fq where fq.fk_form=v_form and fq.fk_question=v_dep_ques;
    if v_dep_ques1 is not null then
    select ques_desc into v_dq1_desc from cb_questions where pk_questions=v_dep_ques1;
    end if;
  end if;
  --dbms_output.put_line('v_dep_ques1::::::::'||v_dep_ques1);
  if v_dep_ques1 is not null then 
    select fk_dependent_ques,f_codelst_desc(fq.fk_dependent_ques_value),ques_seq into v_dep_ques2,v_dep_qval2,v_dqsec1 from cb_form_questions fq where fq.fk_form=v_form and fq.fk_question=v_dep_ques1;
    if v_dep_ques2 is not null then
    select ques_desc into v_dq2_desc from cb_questions where pk_questions=v_dep_ques2;
    end if;
  end if;
  --dbms_output.put_line('v_dep_ques2:::::::::'||v_dep_ques2);
  if v_dep_ques2 is not null then 
    select fk_dependent_ques,f_codelst_desc(fq.fk_dependent_ques_value),ques_seq into v_dep_ques3,v_dep_qval3,v_dqsec2 from cb_form_questions fq where fq.fk_form=v_form and fq.fk_question=v_dep_ques2;
    if v_dep_ques3 is not null then
    select ques_desc into v_dq3_desc from cb_questions where pk_questions=v_dep_ques3;
    select ques_seq into v_dqsec3 from cb_form_questions fq where fq.fk_form=v_form and fq.fk_question=v_dep_ques3;
    end if;
  end if;
  if v_dep_ques3 is not null then
    if v_result is not null and v_result!='~' then
     v_result:=v_result||'~'||v_dqsec3||') '||v_dq3_desc||'~'||v_dep_qval3;
    else
      v_result:=v_dqsec3||') '||v_dq3_desc||'~'||v_dep_qval3;
    end if;
    --dbms_output.put_line('11111'||v_result);
  end if;
  if v_dep_ques2 is not null then
    if v_result is not null and v_result!='~' then
      v_result:=v_result||'~'||v_dqsec2||') '||v_dq2_desc||'~'||v_dep_qval2;
    else
      v_result:=v_dqsec2||') '||v_dq2_desc||'~'||v_dep_qval2;
    end if;
    --dbms_output.put_line('222222222'||v_result);
  end if;
  if v_dep_ques1 is not null then
  --dbms_output.put_line('v_result.........'||v_result||'.......');
    if v_result is not null and v_result!='~' then
    --dbms_output.put_line('v_dq1_desc in if ::::::'||v_dq1_desc||'.........');
      v_result:=v_result||'~'||v_dqsec1||') '||v_dq1_desc||'~'||v_dep_qval1;
    else
      --dbms_output.put_line('v_dq1_desc::::::'||v_dq1_desc);
      v_result:=v_dqsec1||') '||v_dq1_desc||'~'||v_dep_qval1;
    end if;
    --dbms_output.put_line('33333333'||v_result);
  end if;
  if v_dep_ques is not null then
    if v_result is not null and v_result!='~' then
    --dbms_output.put_line('in if::::'||v_result);
      v_result:=v_result||'~'||v_dqsec||') '||v_dq_desc||'~'||v_dep_qval;
    else
    --dbms_output.put_line('in else::::'||v_result);
      v_result:=v_dqsec||') '||v_dq_desc||'~'||v_dep_qval;
    end if;
    --dbms_output.put_line('444444444'||v_result);
  end if;
  if v_ques is not null then
  --dbms_output.put_line('in if v_ques not null::::::::'||v_ques);
  --dbms_output.put_line(' v_result ::::::::'||v_result||'...');
    if v_result is not null and v_result!='~' then
    --dbms_output.put_line('in if v_result not null::::::::'||v_result);
      v_result:=v_result||'~'||v_qsec||') '||v_ques_desc||'~'||v_qres;
    else
      v_result:=v_qsec||') '||v_ques_desc||'~'||v_qres;
    end if;
    --dbms_output.put_line('5555555555'||v_result);
  end if;
  if v_ass is not null then
    if v_result is not null and v_result!='~' then
      v_result:=v_result||'~'||v_ass;
    else
      v_result:=v_ass;
    end if;
    --dbms_output.put_line('6666666666'||v_result);
  end if;
  --dbms_output.put_line('@@@@@@@'||v_result);
  return v_result;
end;
/


create or replace
TRIGGER "ERES"."ER_ORDER_RECEIPANT_INFO_AU0"
AFTER UPDATE ON ER_ORDER_RECEIPANT_INFO
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
    v_rowid NUMBER(10);/*variable used to fetch value of sequence */
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;

    PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO',:OLD.RID,:OLD.PK_ORDER_RECEIPANT_INFO,'U',:NEW.LAST_MODIFIED_BY);

    IF NVL(:OLD.PK_ORDER_RECEIPANT_INFO,0) != NVL(:NEW.PK_ORDER_RECEIPANT_INFO,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','PK_ORDER_RECEIPANT_INFO', :OLD.PK_ORDER_RECEIPANT_INFO, :NEW.PK_ORDER_RECEIPANT_INFO,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_RECEIPANT,0) != NVL(:NEW.FK_RECEIPANT,0) THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','FK_RECEIPANT',:OLD.FK_RECEIPANT, :NEW.FK_RECEIPANT,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_ORDER_ID,0) != NVL(:NEW.FK_ORDER_ID,0) THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','FK_ORDER_ID',:OLD.FK_ORDER_ID, :NEW.FK_ORDER_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_CORD_ID,0) != NVL(:NEW.FK_CORD_ID,0) THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','FK_CORD_ID',:OLD.FK_CORD_ID, :NEW.FK_CORD_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.IND_PROTOCOL,0) != NVL(:NEW.IND_PROTOCOL,0) THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','IND_PROTOCOL',:OLD.IND_PROTOCOL, :NEW.IND_PROTOCOL,NULL,NULL);
    END IF;
    IF NVL(:OLD.IND_SPONSER,' ') != NVL(:NEW.IND_SPONSER,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','IND_SPONSER', :OLD.IND_SPONSER, :NEW.IND_SPONSER,NULL,NULL);
    END IF;
    IF NVL(:OLD.IND_NUMBER,' ') != NVL(:NEW.IND_NUMBER,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','IND_NUMBER', :OLD.IND_NUMBER, :NEW.IND_NUMBER,NULL,NULL);
    END IF;
    IF NVL(:OLD.CURRENT_DIAGNOSIS,' ') != NVL(:NEW.CURRENT_DIAGNOSIS,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','CURRENT_DIAGNOSIS', :OLD.CURRENT_DIAGNOSIS, :NEW.CURRENT_DIAGNOSIS,NULL,NULL);
    END IF;
    IF NVL(:OLD.DISEASE_STAGE,' ') != NVL(:NEW.DISEASE_STAGE,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','DISEASE_STAGE', :OLD.DISEASE_STAGE, :NEW.DISEASE_STAGE,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_TRANSPLANT_TYPE,0) != NVL(:NEW.FK_TRANSPLANT_TYPE,0) THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','FK_TRANSPLANT_TYPE',:OLD.FK_TRANSPLANT_TYPE, :NEW.FK_TRANSPLANT_TYPE,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_LEU_CURR_STATUS,0) != NVL(:NEW.FK_LEU_CURR_STATUS,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','FK_LEU_CURR_STATUS', :OLD.FK_LEU_CURR_STATUS, :NEW.FK_LEU_CURR_STATUS,NULL,NULL);
    END IF;
    IF NVL(:OLD.REC_TRANSFUSED,0) != NVL(:NEW.REC_TRANSFUSED,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','REC_TRANSFUSED', :OLD.REC_TRANSFUSED, :NEW.REC_TRANSFUSED,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROP_INFUSION_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.PROP_INFUSION_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','PROP_INFUSION_DATE',
       TO_CHAR(:OLD.PROP_INFUSION_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.PROP_INFUSION_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.PROP_PREP_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.PROP_PREP_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','PROP_PREP_DATE',
       TO_CHAR(:OLD.PROP_PREP_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.PROP_PREP_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.RES_CEL_SAM_FLAG,' ') != NVL(:NEW.RES_CEL_SAM_FLAG,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','RES_CEL_SAM_FLAG', :OLD.RES_CEL_SAM_FLAG, :NEW.RES_CEL_SAM_FLAG,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','CREATOR', :OLD.CREATOR, :NEW.CREATOR,NULL,NULL);
    END IF;
    IF NVL(:OLD.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=    NVL(:NEW.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
  IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_CBU_STATUS','LAST_MODIFIED_BY', :OLD.LAST_MODIFIED_BY, :NEW.LAST_MODIFIED_BY,NULL,NULL);
	END IF;
     IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;

     IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','IP_ADD', :OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
    END IF;
     IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','RID', :OLD.RID, :NEW.RID,NULL,NULL);
    END IF;
     IF NVL(:OLD.NO_OF_REMISSIONS,0) != NVL(:NEW.NO_OF_REMISSIONS,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','NO_OF_REMISSIONS', :OLD.NO_OF_REMISSIONS, :NEW.NO_OF_REMISSIONS,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_RECEIPANT_CONTACT1,0) != NVL(:NEW.FK_RECEIPANT_CONTACT1,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','FK_RECEIPANT_CONTACT1', :OLD.FK_RECEIPANT_CONTACT1, :NEW.FK_RECEIPANT_CONTACT1,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_RECEIPANT_CONTACT2,0) != NVL(:NEW.FK_RECEIPANT_CONTACT2,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','FK_RECEIPANT_CONTACT2', :OLD.FK_RECEIPANT_CONTACT2, :NEW.FK_RECEIPANT_CONTACT2,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_RECEIPANT_CONTACT3,0) != NVL(:NEW.FK_RECEIPANT_CONTACT3,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','FK_RECEIPANT_CONTACT3', :OLD.FK_RECEIPANT_CONTACT3, :NEW.FK_RECEIPANT_CONTACT3,NULL,NULL);
    END IF;
    IF NVL(:OLD.ORDER_PHYSICIAN,0) != NVL(:NEW.ORDER_PHYSICIAN,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','ORDER_PHYSICIAN', :OLD.ORDER_PHYSICIAN, :NEW.ORDER_PHYSICIAN,NULL,NULL);
    END IF;
    IF NVL(:OLD.REC_WEIGHT,0) != NVL(:NEW.REC_WEIGHT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','REC_WEIGHT', :OLD.REC_WEIGHT, :NEW.REC_WEIGHT,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_SITE_ID,0) != NVL(:NEW.FK_SITE_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','FK_SITE_ID', :OLD.FK_SITE_ID, :NEW.FK_SITE_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_CONTACT_PERSON,0) != NVL(:NEW.FK_CONTACT_PERSON,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','FK_CONTACT_PERSON', :OLD.FK_CONTACT_PERSON, :NEW.FK_CONTACT_PERSON,NULL,NULL);
    END IF;
    IF NVL(:OLD.DATE_TC_COMPLETED,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.DATE_TC_COMPLETED,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','DATE_TC_COMPLETED',
       TO_CHAR(:OLD.DATE_TC_COMPLETED, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.DATE_TC_COMPLETED, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.EX_VIVO_TRANSPLANT,0) != NVL(:NEW.EX_VIVO_TRANSPLANT,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','EX_VIVO_TRANSPLANT', :OLD.EX_VIVO_TRANSPLANT, :NEW.EX_VIVO_TRANSPLANT,NULL,NULL);
    END IF;
    IF NVL(:OLD.MULTI_TRANSPLANT,' ') != NVL(:NEW.MULTI_TRANSPLANT,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','MULTI_TRANSPLANT', :OLD.MULTI_TRANSPLANT, :NEW.MULTI_TRANSPLANT,NULL,NULL);
    END IF;
    IF NVL(:OLD.OTHER_TRANSPLANT,' ') != NVL(:NEW.OTHER_TRANSPLANT,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','OTHER_TRANSPLANT', :OLD.OTHER_TRANSPLANT, :NEW.OTHER_TRANSPLANT,NULL,NULL);
    END IF;
    IF NVL(:OLD.SINGLE_UNIT_TRANSPLANT,' ') != NVL(:NEW.SINGLE_UNIT_TRANSPLANT,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','SINGLE_UNIT_TRANSPLANT', :OLD.SINGLE_UNIT_TRANSPLANT, :NEW.SINGLE_UNIT_TRANSPLANT,NULL,NULL);
    END IF;
    IF NVL(:OLD.TC_CDE,0) != NVL(:NEW.TC_CDE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','TC_CDE', :OLD.TC_CDE, :NEW.TC_CDE,NULL,NULL);
    END IF;
    IF NVL(:OLD.PRODUCT_DELIVERY_TC_NAME,0) != NVL(:NEW.PRODUCT_DELIVERY_TC_NAME,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','PRODUCT_DELIVERY_TC_NAME', :OLD.PRODUCT_DELIVERY_TC_NAME, :NEW.PRODUCT_DELIVERY_TC_NAME,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROPOSED_SHIP_DTE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.PROPOSED_SHIP_DTE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','PROPOSED_SHIP_DTE',
       TO_CHAR(:OLD.PROPOSED_SHIP_DTE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.PROPOSED_SHIP_DTE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.COMPLETE_ON_BEHALF,' ') != NVL(:NEW.COMPLETE_ON_BEHALF,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','COMPLETE_ON_BEHALF', :OLD.SINGLE_UNIT_TRANSPLANT, :NEW.SINGLE_UNIT_TRANSPLANT,NULL,NULL);
    END IF;
    IF NVL(:OLD.COOP_REF,' ') != NVL(:NEW.COOP_REF,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','COOP_REF', :OLD.COOP_REF, :NEW.COOP_REF,NULL,NULL);
    END IF;
    IF NVL(:OLD.FORM_COMPLETED_BY,' ') != NVL(:NEW.FORM_COMPLETED_BY,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','FORM_COMPLETED_BY', :OLD.FORM_COMPLETED_BY, :NEW.FORM_COMPLETED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.LOCAL_ID,' ') != NVL(:NEW.LOCAL_ID,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','LOCAL_ID', :OLD.LOCAL_ID, :NEW.LOCAL_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.NMDP_ID,' ') != NVL(:NEW.NMDP_ID,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','NMDP_ID', :OLD.NMDP_ID, :NEW.NMDP_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.NMDP_RID,' ') != NVL(:NEW.NMDP_RID,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','NMDP_RID', :OLD.NMDP_RID, :NEW.NMDP_RID,NULL,NULL);
    END IF;
    IF NVL(:OLD.OTH_DESCRIBE,' ') != NVL(:NEW.OTH_DESCRIBE,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','OTH_DESCRIBE', :OLD.OTH_DESCRIBE, :NEW.OTH_DESCRIBE,NULL,NULL);
    END IF;
    IF NVL(:OLD.OTH_SPECIFY,' ') != NVL(:NEW.OTH_SPECIFY,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','OTH_SPECIFY', :OLD.OTH_SPECIFY, :NEW.OTH_SPECIFY,NULL,NULL);
    END IF;
    IF NVL(:OLD.SCU_ACTIVATED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.SCU_ACTIVATED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','SCU_ACTIVATED_DATE',
       TO_CHAR(:OLD.SCU_ACTIVATED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.SCU_ACTIVATED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.PRIORITY,0) != NVL(:NEW.PRIORITY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'ER_ORDER_RECEIPANT_INFO','PRIORITY', :OLD.PRIORITY, :NEW.PRIORITY,NULL,NULL);
    END IF;
    END ;
/
INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,211,2,'02_f_get_assessdata.sql',sysdate,'9.0.0 B#637-ET017');
		commit;