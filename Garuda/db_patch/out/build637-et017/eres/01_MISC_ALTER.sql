--Sql for Table CB_HLA to add column CB_HLA_ORDER_SEQ
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_HLA' AND column_name = 'HLA_REPORT_METHOD_CODE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_HLA add HLA_REPORT_METHOD_CODE VARCHAR2(100)');
  end if;
end;
/
COMMENT ON COLUMN CB_HLA.HLA_REPORT_METHOD_CODE IS 'HLA Reporting method code, stores from ESB'; 
commit;

 --Sql for Table CB_HLA_TEMP to add column CB_HLA_ORDER_SEQ
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_HLA_TEMP' AND column_name = 'HLA_REPORT_METHOD_CODE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_HLA_TEMP add HLA_REPORT_METHOD_CODE VARCHAR2(100)');
  end if;
end;
/
COMMENT ON COLUMN CB_HLA_TEMP.HLA_REPORT_METHOD_CODE IS 'HLA Reporting method code, stores from ESB'; 
commit;


 --Sql for Table CB_BEST_HLA to add column CB_HLA_ORDER_SEQ
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where TABLE_NAME = 'CB_BEST_HLA' AND column_name = 'HLA_REPORT_METHOD_CODE'; 
  if (v_column_exists = 0) then
      execute immediate ('alter table CB_BEST_HLA add HLA_REPORT_METHOD_CODE VARCHAR2(100)');
  end if;
end;
/
COMMENT ON COLUMN CB_BEST_HLA.HLA_REPORT_METHOD_CODE IS 'HLA Reporting method code, stores from ESB'; 
commit;

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,211,1,'01_MISC_ALTER.sql',sysdate,'9.0.0 B#637-ET017');
		commit;