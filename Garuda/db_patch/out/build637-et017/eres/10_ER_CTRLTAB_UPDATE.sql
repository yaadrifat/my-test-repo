--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where  ctrl_value ='CB_OVRIDEDEFR' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='EV' where ctrl_value ='CB_OVRIDEDEFR' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/
---End--------

--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where  ctrl_value ='H_14' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='V' where ctrl_value ='H_14' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/
---End--------

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,211,10,'10_ER_CTRLTAB_UPDATE.sql',sysdate,'9.0.0 B#637-ET017');
		commit;