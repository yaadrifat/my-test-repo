set define off;
update track_patches set db_patch_name = '01_hotfix1_er_version.sql' where app_version = '9.0.0 B#637-ET017.01' and db_ver_mjr = 211 and db_ver_mnr = 1;
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,211,3,'03_hotfix2_versionupdate.sql',sysdate,'9.0.0 B#637-ET017.02');

commit;