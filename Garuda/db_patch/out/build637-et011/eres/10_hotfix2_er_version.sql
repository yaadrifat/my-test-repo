set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v9.0.0 Build#637-ET011.02' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,199,10,'10_hotfix2_er_version.sql',sysdate,'9.0.0 B#637-ET011.02');

commit;