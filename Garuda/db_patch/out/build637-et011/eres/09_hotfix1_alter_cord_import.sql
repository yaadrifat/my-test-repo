
--STARTS ADD THE COLUMN MESSAGE_TEXT TO CB_CORD_IMPORT_MESSAGES--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_IMPORT_MESSAGES'
    and column_name = 'MESSAGE_TEXT';
  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_CORD_IMPORT_MESSAGES MODIFY (MESSAGE_TEXT varchar2(2000))';
  end if;
end;
/
--END--
--starts comment for THE COLUMN MESSAGE_TEXT TO CB_CORD_IMPORT_MESSAGES TABLE--
comment on column CB_CORD_IMPORT_MESSAGES.MESSAGE_TEXT  is 'message description of the cord import';
--end--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,199,9,'09_hotfix1_alter_cord_import.sql',sysdate,'9.0.0 B#637-ET011.01');

commit;
