
--START UPDATE ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'IDM';
  if (v_column_exists = 1) then
	update er_codelst set CODELST_DESC = 'IDM' where codelst_type = 'sub_entity_type' AND codelst_subtyp = 'IDM';
  end if;
end;
/
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'MRQ';
  if (v_column_exists = 1) then
	update er_codelst set CODELST_DESC = 'MRQ' where codelst_type = 'sub_entity_type' AND codelst_subtyp = 'MRQ';
  end if;
end;
/
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'LAB_SUM';
  if (v_column_exists = 1) then
	update er_codelst set CODELST_DESC = 'LAB SUMMERY' where codelst_type = 'sub_entity_type' AND codelst_subtyp = 'LAB_SUM';
  end if;
end;
/
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'NOTES';
  if (v_column_exists = 1) then
	update er_codelst set CODELST_DESC = 'Clinical Note' where codelst_type = 'sub_entity_type' AND codelst_subtyp = 'NOTES';
  end if;
end;
/
--END

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'bact_cul'
    AND CODELST_DESC='Unknown' ;
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET codelst_subtyp = 'unknown' where codelst_type = 'bact_cul'
    AND codelst_subtyp = 'Unknown';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'fung_cul'
    AND CODELST_DESC='Unknown' ;
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET codelst_subtyp = 'unknown' where codelst_type = 'fung_cul'
      AND codelst_subtyp = 'Unknown';
	commit;
  end if;
end;
/
--END--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'bact_cul'
    AND codelst_subtyp = 'unknown';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='bact_cul' and CODELST_SUBTYP='unknown';
 commit;
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'fung_cul'
    AND codelst_subtyp = 'unknown';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='fung_cul' and CODELST_SUBTYP='unknown';
 commit;
  end if;
end;
/

set define off;
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp'
    AND codelst_subtyp = '>=-150to<=-135';
  if (v_record_exists = 1) then
     update er_codelst set codelst_desc = '> -150 to &le; -135' where codelst_type = 'storage_temp' and codelst_subtyp = '>=-150to<=-135';

	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp'
    AND codelst_subtyp = '<-150';
  if (v_record_exists = 1) then
     update er_codelst set codelst_desc = '&le; -150' where codelst_type = 'storage_temp' and codelst_subtyp = '<-150';

	commit;
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,199,1,'01_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 B#637-ET011');

commit;