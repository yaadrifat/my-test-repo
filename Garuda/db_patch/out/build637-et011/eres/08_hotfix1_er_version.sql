set define off;

UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v9.0.0 Build#637-ET011.01' 
where CTRL_KEY = 'app_version' ; 

commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,199,8,'08_hotfix1_er_version.sql',sysdate,'9.0.0 B#637-ET011.01');

commit;