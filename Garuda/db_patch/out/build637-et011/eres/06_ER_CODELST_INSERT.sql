--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='cbu_ids'
    AND CODELST_SUBTYP = 'cbu_idonbag';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'cbu_ids','cbu_idonbag','Unique Product Identity on Bag','N',NULL,SYSDATE,SYSDATE,NULL,4);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='cbu_ids'
    AND CODELST_SUBTYP = 'cbu_isbtdin';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'cbu_ids','cbu_isbtdin','ISBT Donation Identification Number','N',NULL,SYSDATE,SYSDATE,NULL,4);
	commit;
  end if;
end;
/
--END--

--ADD COLUMN DELETED FLAG IN CB_ADDITIONAL_IDS TABLE--
Set define off;
DECLARE
  v_column_value_update number := 0;
BEGIN
  Select count(*) into v_column_value_update
    from user_tab_cols
    where TABLE_NAME = 'CB_ADDITIONAL_IDS'
    AND column_name = 'DELETEDFLAG';
  if (v_column_value_update =0) then
  execute immediate 'Alter table CB_ADDITIONAL_IDS ADD DELETEDFLAG VARCHAR2(1 BYTE)';
  end if;
end;
/
---End--------

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,199,6,'06_ER_CODELST_INSERT.sql',sysdate,'9.0.0 B#637-ET011');

commit;

