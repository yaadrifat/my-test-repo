/* This readMe is specific to Velos eResearch version 9.0 build637-et015 */

=====================================================================================================================================
  Garuda:-
  We are releasing script(10_RACE.sql) provided by the migration team which will first delete and then insert the race values.
  This change will have impact over View clinical on race retervial over CBU Information widget. For first time no data will come on that widget
  because reference get changed after execution of above script. 
=====================================================================================================================================
  