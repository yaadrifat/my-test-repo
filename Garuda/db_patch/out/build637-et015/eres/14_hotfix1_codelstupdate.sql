DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'LAB_SUM';
  if (v_column_exists = 1) then
	update er_codelst set CODELST_DESC = 'LAB SUMMARY' where codelst_type = 'sub_entity_type' AND codelst_subtyp = 'LAB_SUM';
	commit;
  end if;
end;
/
INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,207,14,'14_hotfix1_codelstupdate.sql',sysdate,'9.0.0 B#637-ET015.01');
		commit;
