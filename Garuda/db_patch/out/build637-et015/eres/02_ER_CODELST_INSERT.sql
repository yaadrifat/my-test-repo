--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='infect_mark'
    AND CODELST_SUBTYP = 'cbu';
  if (v_record_exists = 0) then
      INSERT INTO 
		ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
		CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
		CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
		(SEQ_ER_CODELST.nextval,null,'infect_mark','cbu','CBU','N',1,'Y',null,
		null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='infect_mark'
    AND CODELST_SUBTYP = 'maternal';
  if (v_record_exists = 0) then
      INSERT INTO 
		ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
		CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
		CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
		(SEQ_ER_CODELST.nextval,null,'infect_mark','maternal','Maternal','N',2,'Y',null,
		null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--
INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,207,2,'02_ER_CODELST_INSERT.sql',sysdate,'9.0.0 B#637-ET015');
		commit;
