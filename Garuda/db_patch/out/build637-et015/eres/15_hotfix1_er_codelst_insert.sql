--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'MHIS';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','MHIS','Multiple Hispanic','Y',NULL,SYSDATE,SYSDATE,NULL,43);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'MNAA';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','MNAA','Multiple Native American','Y',NULL,SYSDATE,SYSDATE,NULL,44);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'MRACE';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','MRACE','Multiple Race','Y',NULL,SYSDATE,SYSDATE,NULL,45);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'MNHI';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','MNHI','Multiple Nat. Hw/Othr Pa. Isln','Y',NULL,SYSDATE,SYSDATE,NULL,46);
	commit;
  end if;
end;
/
--END--



--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'MBLK';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','MBLK','Multiple Black','Y',NULL,SYSDATE,SYSDATE,NULL,47);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'MAPI';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','MAPI','Multiple Asian/Pacific Islndr','Y',NULL,SYSDATE,SYSDATE,NULL,48);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'MCAU';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','MCAU','Multiple Caucasian','Y',NULL,SYSDATE,SYSDATE,NULL,49);
	commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,207,15,'15_hotfix1_er_codelst_insert.sql',sysdate,'9.0.0 B#637-ET015.01');
		commit;