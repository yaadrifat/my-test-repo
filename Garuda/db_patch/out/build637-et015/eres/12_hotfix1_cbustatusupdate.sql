--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='CB' and CODE_TYPE='Response';
  if (v_record_exists = 1) then
      UPDATE CB_CBU_STATUS SET IS_LOCAL_REG = '1' where CBU_STATUS_CODE='CB' and CODE_TYPE='Response';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='SH' and CODE_TYPE='Response';
  if (v_record_exists = 1) then
      UPDATE CB_CBU_STATUS SET IS_LOCAL_REG = '1' where CBU_STATUS_CODE='SH' and CODE_TYPE='Response';
	commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,207,12,'12_hotfix1_cbustatusupdate.sql',sysdate,'9.0.0 B#637-ET015.01');
		commit;