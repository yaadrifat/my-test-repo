--Question 3.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bovine_insulin_since_1980_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = (select pk_codelst from ER_CODELST where CODELST_TYPE='mrq_res1' and codelst_subtyp='yes')
   where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='bovine_insulin_since_1980_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
   commit;
  end if;
end;
/
--End--

--Question 3.b--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='evr_tkn_pit_gh_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = (select pk_codelst from ER_CODELST where CODELST_TYPE='mrq_res1' and codelst_subtyp='yes')
   where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='evr_tkn_pit_gh_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
   commit;
  end if;
end;
/
--End--
--Question 3.c--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rabies_vacc_pst_year_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE = (select pk_codelst from ER_CODELST where CODELST_TYPE='mrq_res1' and codelst_subtyp='yes')
   where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rabies_vacc_pst_year_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
   commit;
  end if;
end;
/
--End--
INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,207,13,'13_hotfix1_cb_form_question.sql',sysdate,'9.0.0 B#637-ET015.01');
		commit;