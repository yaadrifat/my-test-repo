--Start updating cb_Questions --
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'shots_vacc_pst_8wk_desc';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG=0 where QUES_CODE = 'shots_vacc_pst_8wk_desc';
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG=0 where QUES_CODE = 'inher_other_dises_rel';
commit;
  end if;
end;
/
--Question 24.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_desc1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG=1 where QUES_CODE = 'inher_other_dises_desc1';
commit;
  end if;
end;
/
--Question 24.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_desc2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG=1 where QUES_CODE = 'inher_other_dises_desc2';
commit;
  end if;
end;
/
--Question 24.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_desc3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG=1 where QUES_CODE = 'inher_other_dises_desc3';
commit;
  end if;
end;
/
--Question 24.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_desc4';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG=1 where QUES_CODE = 'inher_other_dises_desc4';
commit;
  end if;
end;
/
--Question 24.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_desc5';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG=1 where QUES_CODE = 'inher_other_dises_desc5';
commit;
  end if;
end;
/
--Question 24.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_desc6';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG=1 where QUES_CODE = 'inher_other_dises_desc6';
commit;
  end if;
end;
/
--End--
--Question 7.a.1.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cancer_leuk_rel_type_desc1';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG=1 where QUES_CODE = 'cancer_leuk_rel_type_desc1';
commit;
  end if;
end;
/
--Question 7.a.2.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cancer_leuk_rel_type_desc2';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG=1 where QUES_CODE = 'cancer_leuk_rel_type_desc2';
commit;
  end if;
end;
/
--Question 7.a.3.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cancer_leuk_rel_type_desc3';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG=1 where QUES_CODE = 'cancer_leuk_rel_type_desc3';
commit;
  end if;
end;
/
--End--


INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,207,16,'16_hotfix1_update_cb_ques.sql',sysdate,'9.0.0 B#637-ET015.01');
		commit;