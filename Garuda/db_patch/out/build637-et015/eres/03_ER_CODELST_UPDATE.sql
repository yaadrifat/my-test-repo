DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND (CODELST_DESC = 'Unknown/Question Not Asked ' or CODELST_DESC = 'Unknown/Question Not Asked') ;
  if (v_column_exists = 1) then
Update ER_CODELST set codelst_subtyp='unkqnt' where CODELST_DESC='Unknown/Question Not Asked ' and codelst_type = 'race';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'unkqnt';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='unkqnt';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'hla_locus'
    AND codelst_subtyp = 'hla_dpa1';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='hla_locus' and CODELST_SUBTYP='hla_dpa1';
 commit;
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'hla_locus'
    AND codelst_subtyp = 'hla_dqa1';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='hla_locus' and CODELST_SUBTYP='hla_dqa1';
 commit;
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'doc_categ'
    AND codelst_subtyp = 'infect_mark';
  if (v_column_exists = 1) then
	update ER_CODELST set CODELST_DESC='IDMs' where codelst_type = 'doc_categ' AND codelst_subtyp = 'infect_mark';
	commit;
  end if;
end;
/
INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,207,3,'03_ER_CODELST_UPDATE.sql',sysdate,'9.0.0 B#637-ET015');
		commit;
