delete from sch_msgtxt where pk_msgtxt in (43,44);
commit;

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,203,1,'01_DEL_MSGTXT.sql',sysdate,'9.0.0 B#637-ET013');

commit;