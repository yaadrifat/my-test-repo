--start update cb_form_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'mrq_res3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
commit;
  end if;
end;
/
--end--
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,203,13,'13_hotfix1_update_cb_form_qes.sql',sysdate,'9.0.0 B#637-ET013.01');

commit;