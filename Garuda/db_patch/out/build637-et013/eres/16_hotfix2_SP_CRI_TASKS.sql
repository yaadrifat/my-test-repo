create or replace
PROCEDURE SP_CRI_TASKS(PK_CORDID IN NUMBER) IS    
IS_SYSTEM_USER VARCHAR2(10);
IDS_COMPL_FLAG NUMBER(2) := 0;
CBUINFO_COMPL_FLAG NUMBER(2) := 0;
PROS_COMPL_FLAG NUMBER(2) :=0;
CORD_UNLIC_FLAG NUMBER(2) :=0;
CORD_LIC_FLAG NUMBER(2) :=0;
masterques_response varchar2(1000);
idm_compl_flag number(1):=0;
pk_grp_additest number(6);
IS_IDM_DATA NUMBER(2):=0;
IS_IDM_DATA1 NUMBER(2):=0;
mrq_masterques_response varchar2(6);
mrq_compl_flag number(1):=0;
IS_MRQ_DATA NUMBER(2):=0;
IS_MRQ_DATA1 NUMBER(2):=0;
IS_LABEL_TYP NUMBER(6):=0;
fmhq_masterques_response varchar2(1000);
fmhq_res_type varchar2(50);
fmhq_compl_flag number(1):=0;
IS_PROCESSING_DATA number(1):=0;
IS_FMHQ_DATA NUMBER(2):=0;
IS_FMHQ_DATA1 NUMBER(2):=0;
IS_TXT_TYP varchar2(5):='0';
IS_MULTISELL_TYP varchar2(5):='0';
IS_DRPDOWN_TYP varchar2(5):='0';
ASSESS_PASSED NUMBER(2):=2;
FMHQ_UNEXPECTED_CODELST_DESC varchar2(70);
v_split_string emb_string.string_array;
temp_string varchar2(70);
LIC_COMPL_FLAG NUMBER(2):=0;
LAB_SUMMAR_COMP1_FLAG NUMBER(2):=0;
NOT_DONE_RES NUMBER(10):=0;
CRI_COMP_FLAG NUMBER(10):=0;
MASTER_QUES_DATA_IS_THER NUMBER(10):=0;
MASTER_QUES_DATA_IS_THER_MRQ NUMBER(10):=0;
MASTER_QUES_DATA_IS_THER_FMHQ NUMBER(10):=0;
is_data_for_cord number;
OLD_IDS_COMPL_FLAG NUMBER(2) := 0;
OLD_CBUINFO_COMPL_FLAG NUMBER(2) := 0;
OLD_PROS_COMPL_FLAG NUMBER(2) :=0;
OLD_LAB_SUMMAR_COMP1_FLAG NUMBER(2):=0;
OLD_LIC_COMPL_FLAG NUMBER(2):=0;
old_idm_compl_flag number(1):=0;
old_mrq_compl_flag number(1):=0;
old_fmhq_compl_flag number(1):=0;
DONT_ALLOW_INSERT_FLAG NUMBER(1):=1;

BEGIN    
        
      select count(*) into is_data_for_cord from cb_cord_complete_req_info where fk_cord_id=PK_CORDID;
      
      if is_data_for_cord>0 then
      
      for compreq_data IN  (select IDS_FLAG,CBU_INFO_FLAG,CBU_PROCESSING_FLAG,LAB_SUMMARY_FLAG,IDM_FLAG,MRQ_FLAG,FMHQ_FLAG,LICENSURE_FLAG from cb_cord_complete_req_info where fk_cord_id=PK_CORDID and pk_cord_complete_reqinfo=(select max(pk_cord_complete_reqinfo) from cb_cord_complete_req_info where fk_cord_id=PK_CORDID))
      loop
          OLD_IDS_COMPL_FLAG:=compreq_data.IDS_FLAG;
          OLD_CBUINFO_COMPL_FLAG:=compreq_data.CBU_INFO_FLAG;
          OLD_PROS_COMPL_FLAG:=compreq_data.CBU_PROCESSING_FLAG;
          OLD_LAB_SUMMAR_COMP1_FLAG:=compreq_data.LAB_SUMMARY_FLAG;
          OLD_LIC_COMPL_FLAG:=compreq_data.LICENSURE_FLAG;
          old_idm_compl_flag:=compreq_data.IDM_FLAG;
          old_mrq_compl_flag:=compreq_data.MRQ_FLAG;
          old_fmhq_compl_flag:=compreq_data.FMHQ_FLAG;
      end loop;
      
      end if;
      
      
        
        
        --DBMS_OUTPUT.PUT_LINE('PK_CORDID VALUE::::::::::::'||PK_CORDID);
        
        SELECT NVL(CBBTBL.USING_CDR,0) INTO IS_SYSTEM_USER FROM CB_CORD CRD LEFT OUTER JOIN CBB CBBTBL ON (CRD.FK_CBB_ID=CBBTBL.FK_SITE) WHERE CRD.PK_CORD = PK_CORDID;
        
        --DBMS_OUTPUT.PUT_LINE('IS_SYSTEM_USER VALUE::::::::::::'||IS_SYSTEM_USER);         
        
        IF IS_SYSTEM_USER=1 THEN 
                          
                            ---IDS TASKS FOR SYS CORD--- 
        
                           SELECT CASE WHEN CORD_REGISTRY_ID IS NOT NULL AND CORD_LOCAL_CBU_ID IS NOT NULL AND REGISTRY_MATERNAL_ID IS NOT NULL AND CORD_ID_NUMBER_ON_CBU_BAG IS NOT NULL THEN 1 ELSE 0 END IDSFLAG INTO IDS_COMPL_FLAG
                           FROM CB_CORD 
                           WHERE PK_CORD IN (PK_CORDID);
                           
                           
                           --DBMS_OUTPUT.PUT_LINE('IDS_COMPL_FLAG VALUE FOR SYSCORD::::::::::::'||IDS_COMPL_FLAG);
                           
                           ---CBU INFORMATION TASKS FOR SYS CORD---
                           
                           SELECT 
                           CASE WHEN 
                           (SPE.SPEC_COLLECTION_DATE IS NOT NULL OR CRD.CORD_BABY_BIRTH_DATE IS NOT NULL)  AND 
                           CRD.FK_CORD_BABY_GENDER_ID IS NOT NULL AND 
                           CRD.FK_CODELST_ETHNICITY IS NOT NULL AND 
                           CRD.MULTIPLE_BIRTH IS NOT NULL AND 
                           CRD.FK_CBU_COLL_TYPE IS NOT NULL AND 
                           CRD.FK_CBU_DEL_TYPE IS NOT NULL AND 
                           (SELECT count(*) FROM CB_MULTIPLE_VALUES RACE_DATA WHERE RACE_DATA.ENTITY_ID IN (PK_CORDID) AND RACE_DATA.ENTITY_TYPE=(SELECT CODELST_DESC FROM ER_CODELST WHERE CODELST_TYPE='entity_type' and CODELST_SUBTYP='CBU') AND RACE_DATA.FK_CODELST_TYPE='race') > 0  THEN 1 ELSE 0 END CBUINFO INTO CBUINFO_COMPL_FLAG 
                           FROM CB_CORD CRD 
                           LEFT OUTER JOIN ER_SPECIMEN SPE ON (CRD.FK_SPECIMEN_ID = SPE.PK_SPECIMEN) 
                           WHERE CRD.PK_CORD IN (PK_CORDID);
                           
                           --DBMS_OUTPUT.PUT_LINE('CBUINFO_COMPL_FLAG VALUE FOR SYSCORD::::::::::::'||CBUINFO_COMPL_FLAG);
                           
                           
                           ---PROCESSING INFORMATION TASKS FOR SYS CORD---
                           
                          SELECT 
                          COUNT(*) INTO IS_PROCESSING_DATA
                          FROM CB_CORD CRD 
                          LEFT OUTER JOIN CBB_PROCESSING_PROCEDURES PROC ON (CRD.FK_CBB_PROCEDURE=PROC.PK_PROC) 
                          LEFT OUTER JOIN CBB_PROCESSING_PROCEDURES_INFO PROC_INFO ON (PROC_INFO.FK_PROCESSING_ID=PROC.PK_PROC)
                          LEFT OUTER JOIN CB_ENTITY_SAMPLES SAMPLES ON (SAMPLES.ENTITY_ID=CRD.PK_CORD)
                          WHERE CRD.PK_CORD IN (PK_CORDID) AND SAMPLES.PK_ENTITY_SAMPLES =(SELECT MAX(PK_ENTITY_SAMPLES) FROM CB_ENTITY_SAMPLES WHERE ENTITY_ID=CRD.PK_CORD);
        
                          IF IS_PROCESSING_DATA>0 THEN
                          SELECT 
                          CASE 
                          WHEN 
                          PROC.PROC_NAME IS NOT NULL AND 
                          PROC.PROC_START_DATE IS NOT NULL AND 
                          PROC_INFO.FK_PROC_METH_ID IS NOT NULL AND 
                          PROC_INFO.FK_PRODUCT_MODIFICATION IS NOT NULL AND 
                          PROC_INFO.FK_STOR_METHOD IS NOT NULL AND
                          F_PROC_OTHER_MANUFAC(PK_CORDID) = 1 AND
                          PROC_INFO.FK_STOR_TEMP IS NOT NULL AND
                          PROC_INFO.FK_CONTRL_RATE_FREEZING IS NOT NULL AND
                          F_PROC_VIALS_BAGS_TUBS(PK_CORDID) = 1 AND
                          PROC_INFO.NO_OF_INDI_FRAC IS NOT NULL AND
                          SAMPLES.FILT_PAP_AVAIL IS NOT NULL AND
                          SAMPLES.RBC_PEL_AVAIL IS NOT NULL AND
                          SAMPLES.NO_EXT_DNA_ALI IS NOT NULL AND
                          SAMPLES.NO_SER_ALI IS NOT NULL AND
                          SAMPLES.NO_PLAS_ALI IS NOT NULL AND
                          SAMPLES.NO_NON_VIA_ALI IS NOT NULL AND
                          SAMPLES.NO_VIA_CEL_ALI IS NOT NULL AND
                          SAMPLES.NO_OF_SEG_AVAIL IS NOT NULL AND
                          SAMPLES.CBU_OT_REP_CON_FIN IS NOT NULL AND
                          SAMPLES.CBU_NO_REP_ALT_CON IS NOT NULL AND
                          SAMPLES.NO_SER_MAT_ALI IS NOT NULL AND
                          SAMPLES.NO_PLAS_MAT_ALI IS NOT NULL AND
                          SAMPLES.NO_EXT_DNA_MAT_ALI IS NOT NULL AND
                          SAMPLES.SI_NO_MISC_MAT IS NOT NULL 
                          THEN 1
                          ELSE 0 
                          END PROC_INF0_FLAG INTO PROS_COMPL_FLAG
                          FROM CB_CORD CRD 
                          LEFT OUTER JOIN CBB_PROCESSING_PROCEDURES PROC ON (CRD.FK_CBB_PROCEDURE=PROC.PK_PROC) 
                          LEFT OUTER JOIN CBB_PROCESSING_PROCEDURES_INFO PROC_INFO ON (PROC_INFO.FK_PROCESSING_ID=PROC.PK_PROC)
                          LEFT OUTER JOIN CB_ENTITY_SAMPLES SAMPLES ON (SAMPLES.ENTITY_ID=CRD.PK_CORD)
                          WHERE CRD.PK_CORD IN (PK_CORDID) AND SAMPLES.PK_ENTITY_SAMPLES =(SELECT MAX(PK_ENTITY_SAMPLES) FROM CB_ENTITY_SAMPLES WHERE ENTITY_ID=CRD.PK_CORD);
                          
                          END IF;
              
       
                          ---LABSUMMARY TASK FOR SYS CORD---
                          
                          SELECT 
                           CASE 
                                  WHEN CRD.FK_CORD_CBU_LIC_STATUS IS NOT NULL AND CRD.FK_CORD_CBU_LIC_STATUS !=-1 THEN 
                                                                         CASE  
                                                                              WHEN CRD.FK_CORD_CBU_LIC_STATUS IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='licence' AND CODELST_SUBTYP='unlicensed') THEN
                                                                                CASE 
                                                                                  WHEN (SELECT COUNT(*) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD IN (PK_CORDID))>0 THEN 1
                                                                                  ELSE 0
                                                                                END                                                           
                                                                              ELSE 1
                                                                         END
                                 ELSE 0
                           END INTO LIC_COMPL_FLAG
                           FROM CB_CORD CRD WHERE CRD.PK_CORD IN (PK_CORDID);   
                          
                          
                          ---LAB SUMMARY TASK FOR SYS CORD---
                          
                          
                          SELECT F_LABSUMMARY_COMPL_CHECK(PK_CORDID) into LAB_SUMMAR_COMP1_FLAG FROM DUAL;
                          
                          --DBMS_OUTPUT.PUT_LINE('LAB_SUMMAR_COMP1_FLAG VALUE FOR SYSCORD::::::::::::'||LAB_SUMMAR_COMP1_FLAG);
                          
                          
                          ----IDM TASKS FOR SYS CORD---
                           
                         SELECT COUNT(*) INTO IS_IDM_DATA FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='IDM' and IS_CURRENT_FLAG=1);
                          
                          
                          IF IS_IDM_DATA > 0 THEN
                          
                                 SELECT CASE WHEN DATE_MOMANS_OR_FORMFILL IS NOT NULL AND ENTITY_TYPE IS NOT NULL THEN 1 ELSE 0 END FLAG INTO IS_IDM_DATA1 FROM CB_FORM_VERSION WHERE PK_FORM_VERSION =(SELECT MAX(PK_FORM_VERSION) FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='IDM' and IS_CURRENT_FLAG=1));
                           
                                 IF IS_IDM_DATA1=1 THEN 
                                            
                                             for idm_data in ( select
                                                ques.pk_questions pk_ques,
                                                forq.ques_seq QUES_SEQ,
                                                NVL(forq.fk_dependent_ques,'0') DEPENT_QUES_PK,
                                                NVL(forq.fk_dependent_ques_value,'0') DEPENT_QUES_VALUE,
                                                f_codelst_desc(formres.resp_code) DROPDOWN_VALUE,
                                                f_codelst_desc(ques.response_type) RESPONSE_TYPE,
                                                forq.fk_master_ques MASTER_QUES,
                                                ques.unlicn_prior_to_shipment_flag UNLIC_SHIP_FLAG,
                                                nvl(formres.resp_code,'0') RESPONSE_CODE,
                                                case
                                                    when formres.resp_val is null then '0'
                                                    when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )>0 then f_codelstcommaval_to_desc(formres.resp_val,',')
                                                    when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )=0 then f_codelst_desc(formres.resp_val)
                                                    when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='textfield' then formres.resp_val
                                                end RESPONSE_VAL,
                                                NVL(formres.resp_val,'0') RESPONSE_VAL12,
                                                formres.fk_form_responses FK_FORM_RESP,
                                                nvl(formres.dynaformDate,'0') DYNAFORMDATE,
                                                ques.unlicen_req_flag UNLIC_REQ_FLAG,
                                                ques_desc QUES_DESC,
                                                formres.fk_form_version fk_form_vrson,
                                                formres.pk_cordid pk_cordid,
                                                qgrp.FK_QUESTION_GROUP fk_ques_grp,
                                                case when ques.assesment_flag = 1 and formres.resp_code is not null then
                                                           case when f_codelst_desc(formres.resp_code) in (select codelst_desc from er_codelst where codelst_type like  '%'||forq.UNEXPECTED_RESPONSE_VALUE||'%') then case when formres.pk_assessment is null then 0 when f_codelst_desc(formres.assess_reason2)=f_codelst_desc(formres.resp_code) then nvl(formres.pk_assessment,0) when f_codelst_desc(formres.assess_reason2)!=f_codelst_desc(formres.resp_code) then 0 end
                                                                when f_codelst_desc(formres.resp_code) not in (select codelst_desc from er_codelst where codelst_type like  '%'||forq.UNEXPECTED_RESPONSE_VALUE||'%') then nvl(formres.pk_assessment,1) 
                                                           end                                                
                                                     else 2
                                                end  assesment_value,
                                                ques.assesment_flag quest_assess_flag
                                                from
                                                cb_form_questions forq,
                                                cb_question_grp qgrp,
                                                cb_questions ques
                                                left outer join
                                                (select
                                                    frmres.pk_form_responses,
                                                    frmres.fk_question,
                                                    frmres.response_code resp_code,
                                                    frmres.response_value resp_val,
                                                    frmres.comments,
                                                    frmres.fk_form_responses,
                                                    asses.pk_assessment pk_assessment,
                                                    f_codelst_desc(asses.assessment_for_response) assess_response,
                                                    asses.assessment_remarks assess_remarks,
                                                    to_char(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate,
                                                    frmres.fk_form_version fk_form_version,
                                                    frmres.entity_id pk_cordid,
                                                    asses.assess_reason1 assess_reason2
                                                  from
                                                      cb_form_responses frmres  left outer join (select pk_assessment pk_assessment,sub_entity_id sub_entity_id,assessment_remarks assessment_remarks,cb_assessment.assessment_for_response assessment_for_response,cb_assessment.assessment_reason assess_reason1 from cb_assessment where sub_entity_type=(select pk_codelst from  er_codelst  where  codelst_type='sub_entity_type' and codelst_subtyp='IDM')) asses on( asses.sub_entity_id=frmres.pk_form_responses)
                                                  where
                                                      frmres.entity_id=PK_CORDID and
                                                      frmres.fk_form_version=(select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='IDM' and cb_forms.is_current_flag=1)) and
                                                      frmres.entity_type=(select pk_codelst from er_codelst where codelst_type='test_source' and codelst_subtyp='maternal'))	formres
                                                on(formres.FK_QUESTION=ques.pk_questions)
                                                where
                                                ques.pk_questions=forq.fk_question and
                                                ques.pk_questions=qgrp.fk_question and
                                                forq.fk_form=(select pk_form from cb_forms where forms_desc='IDM' and IS_CURRENT_FLAG=1)  and
                                                qgrp.fk_form=(select pk_form from cb_forms where forms_desc='IDM' and IS_CURRENT_FLAG=1)
                                                order by  qgrp.pk_question_grp,ques.pk_questions ) 
                                                loop
                                                                                                     
                                                    select pk_question_group into pk_grp_additest  from cb_question_group where question_grp_desc='Additional / Miscellaneous IDM Tests';
                                                    SELECT PK_CODELST INTO NOT_DONE_RES FROM ER_CODELST WHERE CODELST_TYPE='test_outcome1' AND CODELST_SUBTYP='not_done';
                                                    
                                                    if idm_data.DEPENT_QUES_PK='0' then
                                                           if(idm_data.RESPONSE_CODE!='0' or idm_data.dynaformDate!='0') then
                                                           --dbms_output.put_line('data is for non dependent question::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                           idm_compl_flag:=1;
                                                           end if;
                                                           
                                                           if(idm_data.RESPONSE_CODE='0' and idm_data.dynaformDate='0') or ( idm_data.assesment_value=0 ) then
                                                              if(idm_data.fk_ques_grp!=pk_grp_additest) then
                                                                --dbms_output.put_line('Inside exit of dependent quest pk 0 condit 0::::::::::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                                idm_compl_flag:=0;
                                                                EXIT;
                                                              end if;
                                                              if(idm_data.fk_ques_grp=pk_grp_additest and idm_data.RESPONSE_CODE!='0') then
                                                                dbms_output.put_line('Inside exit of dependent quest pk 0 condit 0::::::::::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                                idm_compl_flag:=0;
                                                                EXIT;
                                                              end if;
                                                           end if;
                                                           
                                                           
                                                    end if;
                                                    
                                                    
                                                   if idm_data.DEPENT_QUES_PK!='0' then
                                                         --dbms_output.put_line('master question value::::'||idm_data.DEPENT_QUES_PK);
                                                         --select nvl(formres2.RESPONSE_CODE,'0') into  masterques_response from cb_form_responses formres2 where formres2.FK_QUESTION = idm_data.DEPENT_QUES_PK and formres2.entity_id = idm_data.pk_cordid and formres2.fk_form_version = idm_data.fk_form_vrson;
                                                         --dbms_output.put_line('master question value::::'||masterques_response);
                                                         
                                                         select COUNT(*) into MASTER_QUES_DATA_IS_THER from cb_form_responses formres2 where formres2.FK_QUESTION = idm_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='IDM' and cb_forms.is_current_flag=1));
                                                         
                                                         IF MASTER_QUES_DATA_IS_THER>0 THEN
                                                          select nvl(formres2.RESPONSE_CODE,'0') into  masterques_response from cb_form_responses formres2 where formres2.FK_QUESTION = idm_data.DEPENT_QUES_PK and formres2.entity_id =PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='IDM' and cb_forms.is_current_flag=1));
                                                          --dbms_output.put_line('master question value2::::'||masterques_response);
                                                         END IF;
                                                         
                                                         IF MASTER_QUES_DATA_IS_THER=0 THEN
                                                            masterques_response:='0';
                                                         END IF;
                                                         
                                                         if masterques_response!='0'  and (instr(idm_data.DEPENT_QUES_VALUE,masterques_response) !=0 ) then
                                                         v_split_string := emb_string.split_string(idm_data.DEPENT_QUES_VALUE);
                                                         if v_split_string.count > 0 then
                                                                  for i in 1..v_split_string.count loop
                                                                      --dbms_output.put_line(v_split_string(i));
                                                                      --select f_codelst_desc(v_split_string(i)) into temp_string from dual;                        
                                                                      if idm_data.RESPONSE_CODE != '0' or idm_data.dynaformDate != '0'  and instr(masterques_response,v_split_string(i)) !=0 then
                                                                        idm_compl_flag:=1;
                                                                        --dbms_output.put_line('required data in child question::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                                      end if;
                                                                   end loop;
                                                         end if;
                                                         end if;
                                                         
                                                         
                                                         
                                                         if masterques_response!='0'  and (instr(idm_data.DEPENT_QUES_VALUE,masterques_response) !=0 ) then
                                                           
                                                            if idm_data.RESPONSE_CODE != '0' or idm_data.dynaformDate != '0' then
                                                              idm_compl_flag:=1;
                                                              --dbms_output.put_line('required data in child question::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                            end if;
                                                            
                                                            if(idm_data.RESPONSE_CODE = '0' and idm_data.dynaformDate = '0') or ( idm_data.assesment_value=0 ) then
                                                                if(idm_data.QUES_SEQ!='14') then
                                                                  
                                                                  --dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 1::::::::::::'||idm_data.RESPONSE_CODE ||' '||idm_data.dynaformDate);
                                                                  --dbms_output.put_line('Inside exit of condit 1::::::::::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                                  idm_compl_flag:=0;
                                                                  EXIT;
                                                               
                                                               end if;
                                                               
                                                               if idm_data.QUES_SEQ='14'and instr(masterques_response, NOT_DONE_RES)=0 then
                                                                  --dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 1 for quest14::::::::::::'||idm_data.RESPONSE_CODE ||' '||idm_data.dynaformDate);
                                                                  --dbms_output.put_line('Inside exit of condit seperate condi 1::::::::::::'||idm_data.QUES_SEQ ||' '||idm_data.QUES_DESC);
                                                                  idm_compl_flag:=0;
                                                                  exit;
                                                               end if;
                                                             
                                                            end if;
                                                            
                                                         end if;
                                          --               if(masterques_response='0' and instr(idm_data.DEPENT_QUES_VALUE, masterques_response)=0) then
                                          --                     dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 2::::::::::::'||idm_data.DEPENT_QUES_VALUE||' '||instr(idm_data.DEPENT_QUES_VALUE, masterques_response));
                                          --                    idm_compl_flag:=0;
                                          --                    EXIT;
                                          --               end if;
                                                           
                                                   end if;
                                                    
                                                end loop;
                                  END IF;
                              
                              END IF;
                              
                              --dbms_output.put_line('idm_compl_flag::::'||idm_compl_flag);
                              
                              
                              select case when fk_cord_cbu_lic_status in (select pk_codelst from er_codelst where codelst_type='licence' and codelst_subtyp='unlicensed') then 1 else 0 end unlicensedflag INTO CORD_UNLIC_FLAG from cb_cord where pk_cord in (PK_CORDID);
                              
                              select case when fk_cord_cbu_lic_status in (select pk_codelst from er_codelst where codelst_type='licence' and codelst_subtyp='licensed') then 1 else 0 end unlicensedflag INTO CORD_LIC_FLAG from cb_cord where pk_cord in (PK_CORDID);
                          
                              --DBMS_OUTPUT.PUT_LINE('CORD_UNLIC_FLAG VALUE FOR SYSCORD::::::::::::'||CORD_UNLIC_FLAG);
                              --DBMS_OUTPUT.PUT_LINE('CORD_LIC_FLAG VALUE FOR SYSCORD::::::::::::'||CORD_LIC_FLAG);
                              
                              
                              -------------MRQ,FMHQ FORM FOR LICENSED CORD---------------
                              
                              IF CORD_LIC_FLAG=1 THEN
                              
                              
                                          SELECT COUNT(*) INTO IS_MRQ_DATA FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG=1);
                          
                                          --- MRQ TASK FOR LICENSED SYS CORD -----
                          
                                          IF IS_MRQ_DATA > 0 THEN
                                          
                                                 SELECT CASE WHEN DATE_MOMANS_OR_FORMFILL IS NOT NULL AND ENTITY_TYPE IS NOT NULL THEN 1 ELSE 0 END FLAG INTO IS_MRQ_DATA1 FROM CB_FORM_VERSION WHERE PK_FORM_VERSION =(SELECT MAX(PK_FORM_VERSION) FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG=1));
                                           
                                                 IF IS_MRQ_DATA1=1 THEN 
                                                 
                                                 mrq_compl_flag:=1;
                                                 
                                                 END IF;
                                          END IF;
                                          
                                          --dbms_output.put_line('mrq_compl_flag FOR LICENSED CORD::::'||mrq_compl_flag);
                                          
                                          
                                          --- FMHQ TASK FOR LICENSED SYS CORD -----
                                          
                                          SELECT COUNT(*) INTO IS_FMHQ_DATA FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG=1);
                                      
                                            
                                            IF IS_FMHQ_DATA > 0 THEN
                                            
                                                   SELECT CASE WHEN DATE_MOMANS_OR_FORMFILL IS NOT NULL AND ENTITY_TYPE IS NOT NULL THEN 1 ELSE 0 END FLAG INTO IS_FMHQ_DATA1 FROM CB_FORM_VERSION WHERE PK_FORM_VERSION =(SELECT MAX(PK_FORM_VERSION) FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG=1));
                                             
                                                   IF IS_FMHQ_DATA1=1 THEN 
                                                   
                                                   fmhq_compl_flag:=1;
                                                   
                                                   END IF;
                                            END IF;
                                            
                                            
                                         --dbms_output.put_line('fmhq_compl_flag FOR LICENSED CORD::::'||fmhq_compl_flag);
                                                              
                                          
                              
                              
                              END IF;
                              
                              
                              
                              -------------MRQ,FMHQ FORM FOR UNLICENSED CORD---------------
                              
                          
                              IF CORD_UNLIC_FLAG=1 THEN
                              
                              
                                          --- MRQ TASK FOR UNLICENSED SYS CORD -----
                              
                                          SELECT COUNT(*) INTO IS_MRQ_DATA FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG=1);
                          
                          
                                          IF IS_MRQ_DATA > 0 THEN
                                          
                                                 SELECT CASE WHEN DATE_MOMANS_OR_FORMFILL IS NOT NULL AND ENTITY_TYPE IS NOT NULL THEN 1 ELSE 0 END FLAG INTO IS_MRQ_DATA1 FROM CB_FORM_VERSION WHERE PK_FORM_VERSION =(SELECT MAX(PK_FORM_VERSION) FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG=1));
                                           
                                                 IF IS_MRQ_DATA1=1 THEN 
                                                            
                                                             for mrq_data in ( select
                                                                ques.pk_questions pk_ques,
                                                                forq.ques_seq QUES_SEQ,
                                                                NVL(forq.fk_dependent_ques,'0') DEPENT_QUES_PK,
                                                                NVL(forq.fk_dependent_ques_value,'0') DEPENT_QUES_VALUE,
                                                                f_codelst_desc(formres.resp_code) DROPDOWN_VALUE,
                                                                f_codelst_desc(ques.response_type) RESPONSE_TYPE,
                                                                forq.fk_master_ques MASTER_QUES,
                                                                ques.unlicn_prior_to_shipment_flag UNLIC_SHIP_FLAG,
                                                                nvl(formres.resp_code,'0') RESPONSE_CODE,
                                                                case
                                                                    when formres.resp_val is null then '0'
                                                                    when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )>0 then f_codelstcommaval_to_desc(formres.resp_val,',')
                                                                    when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )=0 then f_codelst_desc(formres.resp_val)
                                                                    when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='textfield' then formres.resp_val
                                                                end RESPONSE_VAL,
                                                                NVL(formres.resp_val,'0') RESPONSE_VAL12,
                                                                formres.fk_form_responses FK_FORM_RESP,
                                                                nvl(formres.dynaformDate,'0') DYNAFORMDATE,
                                                                ques.unlicen_req_flag UNLIC_REQ_FLAG,
                                                                ques_desc QUES_DESC,
                                                                formres.fk_form_version fk_form_vrson,
                                                                formres.pk_cordid pk_cordid,
                                                                qgrp.FK_QUESTION_GROUP fk_ques_grp,
                                                                ques.response_type RESPONSE_TYPE_PK,
                                                                case when ques.assesment_flag = 1 and formres.resp_code is not null then
                                                                      case when f_codelst_desc(formres.resp_code) in (select codelst_desc from er_codelst where codelst_type like  '%'||forq.UNEXPECTED_RESPONSE_VALUE||'%') then case  when formres.pk_assessment is null then 0 when f_codelst_desc(formres.assess_reason2)=f_codelst_desc(formres.resp_code) then nvl(formres.pk_assessment,0) when f_codelst_desc(formres.assess_reason2)!=f_codelst_desc(formres.resp_code) then 0 end
                                                                           when f_codelst_desc(formres.resp_code) not in (select codelst_desc from er_codelst where codelst_type like  '%'||forq.UNEXPECTED_RESPONSE_VALUE||'%') then nvl(formres.pk_assessment,1) 
                                                                      end                                                
                                                                    else 2
                                                                end  assesment_value,
                                                                ques.assesment_flag quest_assess_flag
                                                                from
                                                                cb_form_questions forq,
                                                                cb_question_grp qgrp,
                                                                cb_questions ques
                                                                left outer join
                                                                (select
                                                                    frmres.pk_form_responses,
                                                                    frmres.fk_question,
                                                                    frmres.response_code resp_code,
                                                                    frmres.response_value resp_val,
                                                                    frmres.comments,
                                                                    frmres.fk_form_responses,
                                                                    asses.pk_assessment pk_assessment,
                                                                    f_codelst_desc(asses.assessment_for_response) assess_response,
                                                                    asses.assessment_remarks assess_remarks,
                                                                    to_char(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate,
                                                                    frmres.fk_form_version fk_form_version,
                                                                    asses.assess_reason1 assess_reason2,
                                                                    frmres.entity_id pk_cordid
                                                                  from
                                                                      cb_form_responses frmres  left outer join (select pk_assessment pk_assessment,sub_entity_id sub_entity_id,assessment_remarks assessment_remarks,cb_assessment.assessment_for_response assessment_for_response,cb_assessment.assessment_reason assess_reason1 from cb_assessment where sub_entity_type=(select pk_codelst from  er_codelst  where  codelst_type='sub_entity_type' and codelst_subtyp='MRQ')) asses on( asses.sub_entity_id=frmres.pk_form_responses)
                                                                  where
                                                                      frmres.entity_id=PK_CORDID and
                                                                      frmres.fk_form_version=(select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='MRQ' and cb_forms.is_current_flag=1)) and
                                                                      frmres.entity_type=(select pk_codelst from er_codelst where codelst_type='test_source' and codelst_subtyp='maternal'))	formres
                                                                on(formres.FK_QUESTION=ques.pk_questions)
                                                                where
                                                                ques.pk_questions=forq.fk_question and
                                                                ques.pk_questions=qgrp.fk_question and
                                                                forq.fk_form=(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG=1)  and
                                                                qgrp.fk_form=(select pk_form from cb_forms where forms_desc='MRQ' and IS_CURRENT_FLAG=1)
                                                                order by  qgrp.pk_question_grp,ques.pk_questions ) 
                                                                loop
                                                                    --dbms_output.put_line('question pk'||mrq_data.pk_ques);
                                                                    
                                                                    
                                                                    select PK_CODELST into IS_LABEL_TYP from ER_CODELST where CODELST_TYPE='resp_type' AND CODELST_SUBTYP='label';
                                                                    
                                                                    if mrq_data.DEPENT_QUES_PK='0' then
                                                                           if(mrq_data.RESPONSE_CODE!='0' or mrq_data.dynaformDate!='0') then
                                                                           --dbms_output.put_line('data is for non dependent question::::'||mrq_data.QUES_SEQ ||' '||mrq_data.QUES_DESC);
                                                                           mrq_compl_flag:=1;
                                                                           end if;
                                                                           
                                                                           if(mrq_data.RESPONSE_CODE='0' and mrq_data.dynaformDate='0') or ( mrq_data.assesment_value=0 ) then
                                                                              if(mrq_data.RESPONSE_TYPE_PK!=IS_LABEL_TYP) then
                                                                                --dbms_output.put_line('Inside exit of dependent quest pk 0 condit 0::::::::::::'||mrq_data.QUES_SEQ ||' '||mrq_data.QUES_DESC);
                                                                                mrq_compl_flag:=0;
                                                                                EXIT;
                                                                              end if;
                                                                           end if;
                                                                           
                                                                           
                                                                    end if;
                                                                    
                                                                    --dbms_output.put_line('data is there::::'||mrq_data.pk_ques);
                                                                    
                                                                   if mrq_data.DEPENT_QUES_PK!='0' then                                                                                                                                                             
                                                                         --dbms_output.put_line('master question value::::'||mrq_data.DEPENT_QUES_PK);
                                                                         select count(*) into  MASTER_QUES_DATA_IS_THER_MRQ from cb_form_responses formres2 where formres2.FK_QUESTION = mrq_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='MRQ' and cb_forms.is_current_flag=1));
                                                                         --dbms_output.put_line('master question value::::'||mrq_masterques_response);
                                                                         IF MASTER_QUES_DATA_IS_THER_MRQ>0 THEN
                                                                          select NVL(FORMRES2.RESPONSE_CODE,'0') into  mrq_masterques_response from cb_form_responses formres2 where formres2.FK_QUESTION = mrq_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='MRQ' and cb_forms.is_current_flag=1));
                                                                         END IF;
                                                                         IF MASTER_QUES_DATA_IS_THER_MRQ=0 THEN
                                                                            mrq_masterques_response:='0';
                                                                         END IF;
                                                                         --dbms_output.put_line('master question value::::'||mrq_masterques_response);
                                                                         if mrq_masterques_response!='0'  and instr(mrq_data.DEPENT_QUES_VALUE, mrq_masterques_response) !=0  then
                                                                           
                                                                            if mrq_data.RESPONSE_CODE != '0' or mrq_data.dynaformDate != '0' or mrq_data.RESPONSE_VAL is not null then
                                                                              mrq_compl_flag:=1;
                                                                              --dbms_output.put_line('required data in child question::::'||mrq_data.QUES_SEQ ||' '||mrq_data.QUES_DESC);
                                                                            end if;
                                                                            
                                                                            if(mrq_data.RESPONSE_CODE = '0' and mrq_data.dynaformDate = '0' and mrq_data.RESPONSE_VAL='0') or ( mrq_data.assesment_value=0 ) then
                                                                              --dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 1::::::::::::'||mrq_data.RESPONSE_CODE ||' '||mrq_data.dynaformDate);
                                                                              --dbms_output.put_line('Inside exit of condit 1::::::::::::'||mrq_data.QUES_SEQ ||' '||mrq_data.QUES_DESC);
                                                                              mrq_compl_flag:=0;
                                                                              EXIT;
                                                                            end if;
                                                                            
                                                                         end if;
                                                          --               if(mrq_masterques_response='0' and instr(mrq_data.DEPENT_QUES_VALUE, mrq_masterques_response)=0) then
                                                          --                     dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 2::::::::::::'||mrq_data.DEPENT_QUES_VALUE||' '||instr(mrq_data.DEPENT_QUES_VALUE, mrq_masterques_response));
                                                          --                    mrq_compl_flag:=0;
                                                          --                    EXIT;
                                                          --               end if;
                                                                           
                                                                   end if;
                                                                    
                                                                end loop;
                                                  END IF;
                                              
                                              END IF;
                                              
                                              --dbms_output.put_line('mrq_compl_flag FOR UNLICENSED CORD::::'||mrq_compl_flag);
                                              
                                              
                                              
                                               --- FMHQ TASK FOR UNLICENSED SYS CORD -----
                              
                                            SELECT COUNT(*) INTO IS_FMHQ_DATA FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG=1);
                                       
                                            select PK_CODELST into IS_TXT_TYP from ER_CODELST where CODELST_TYPE='resp_type' AND CODELST_SUBTYP='txtbox';
                                            select PK_CODELST into IS_MULTISELL_TYP from ER_CODELST where CODELST_TYPE='resp_type' AND CODELST_SUBTYP='mulselc';
                                            select PK_CODELST into IS_DRPDOWN_TYP from ER_CODELST where CODELST_TYPE='resp_type' AND CODELST_SUBTYP='dropdown';
                                            
                                            
                                            IF IS_FMHQ_DATA > 0 THEN
                                            
                                                   SELECT CASE WHEN DATE_MOMANS_OR_FORMFILL IS NOT NULL AND ENTITY_TYPE IS NOT NULL THEN 1 ELSE 0 END FLAG INTO IS_FMHQ_DATA1 FROM CB_FORM_VERSION WHERE PK_FORM_VERSION =(SELECT MAX(PK_FORM_VERSION) FROM CB_FORM_VERSION WHERE ENTITY_ID=PK_CORDID AND FK_FORM IN (select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG=1));
                                             
                                                   IF IS_FMHQ_DATA1=1 THEN 
                                                              
                                                               for fmhq_data in ( select
                                                                  ques.pk_questions pk_ques,
                                                                  forq.ques_seq QUES_SEQ,
                                                                  NVL(forq.fk_dependent_ques,'0') DEPENT_QUES_PK,
                                                                  NVL(forq.fk_dependent_ques_value,'0') DEPENT_QUES_VALUE,
                                                                  f_codelst_desc(formres.resp_code) DROPDOWN_VALUE,
                                                                  f_codelst_desc(ques.response_type) RESPONSE_TYPE,
                                                                  forq.fk_master_ques MASTER_QUES,
                                                                  ques.unlicn_prior_to_shipment_flag UNLIC_SHIP_FLAG,
                                                                  nvl(formres.resp_code,'0') RESPONSE_CODE,
                                                                  case
                                                                      when formres.resp_val is null then '0'
                                                                      when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )>0 then f_codelstcommaval_to_desc(formres.resp_val,',')
                                                                      when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='multiselect' and ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )=0 then f_codelst_desc(formres.resp_val)
                                                                      when formres.resp_val is not null and  f_codelst_desc(ques.response_type)='textfield' then formres.resp_val
                                                                  end RESPONSE_VAL,
                                                                  NVL(formres.resp_val,'0') RESPONSE_VAL12,
                                                                  formres.fk_form_responses FK_FORM_RESP,
                                                                  nvl(formres.dynaformDate,'0') DYNAFORMDATE,
                                                                  ques.unlicen_req_flag UNLIC_REQ_FLAG,
                                                                  ques_desc QUES_DESC,
                                                                  formres.fk_form_version fk_form_vrson,
                                                                  formres.pk_cordid pk_cordid,
                                                                  qgrp.FK_QUESTION_GROUP fk_ques_grp,
                                                                  nvl(ques.response_type,'') RESPONSE_TYPE_PK,
                                                                  forq.UNEXPECTED_RESPONSE_VALUE unexpected_resp,
                                                                  NVL(formres.assess_reason2,0) response_in_asses,
                                                                  ques.assesment_flag quest_assess_flag
                                                                  from
                                                                  cb_form_questions forq,
                                                                  cb_question_grp qgrp,
                                                                  cb_questions ques
                                                                  left outer join
                                                                  (select
                                                                      frmres.pk_form_responses,
                                                                      frmres.fk_question,
                                                                      frmres.response_code resp_code,
                                                                      frmres.response_value resp_val,
                                                                      frmres.comments,
                                                                      frmres.fk_form_responses,
                                                                      asses.pk_assessment pk_assessment,
                                                                      f_codelst_desc(asses.assessment_for_response) assess_response,
                                                                      asses.assessment_remarks assess_remarks,
                                                                      to_char(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate,
                                                                      frmres.fk_form_version fk_form_version,
                                                                      asses.assess_reason1 assess_reason2,
                                                                      frmres.entity_id pk_cordid
                                                                    from
                                                                        cb_form_responses frmres  left outer join (select pk_assessment pk_assessment,sub_entity_id sub_entity_id,assessment_remarks assessment_remarks,cb_assessment.assessment_for_response assessment_for_response,cb_assessment.assessment_reason assess_reason1 from cb_assessment where sub_entity_type=(select pk_codelst from  er_codelst  where  codelst_type='sub_entity_type' and codelst_subtyp='FMHQ')) asses on( asses.sub_entity_id=frmres.pk_form_responses)
                                                                    where
                                                                        frmres.entity_id=PK_CORDID and
                                                                        frmres.fk_form_version=(select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and cb_forms.is_current_flag=1)) and
                                                                        frmres.entity_type=(select pk_codelst from er_codelst where codelst_type='test_source' and codelst_subtyp='maternal'))	formres
                                                                  on(formres.FK_QUESTION=ques.pk_questions)
                                                                  where
                                                                  ques.pk_questions=forq.fk_question and
                                                                  ques.pk_questions=qgrp.fk_question and
                                                                  forq.fk_form=(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG=1)  and
                                                                  qgrp.fk_form=(select pk_form from cb_forms where forms_desc='FMHQ' and IS_CURRENT_FLAG=1)
                                                                  order by  qgrp.pk_question_grp,ques.pk_questions ) 
                                                                  loop
                                                                  
                                                                  
                                                                      if( fmhq_data.quest_assess_flag=1 and (fmhq_data.RESPONSE_CODE!='0' or fmhq_data.RESPONSE_VAL12!='0') and (f_codelst_desc(fmhq_data.RESPONSE_TYPE_PK)='multiselect' or f_codelst_desc(fmhq_data.RESPONSE_TYPE_PK)='dropdown') ) then
                                                                      
                                                                             --dbms_output.put_line('fmhq_data.unexpected_resp:::::::::::::::::'||fmhq_data.unexpected_resp);
                                                                             
                                                                             if f_codelst_desc(fmhq_data.RESPONSE_TYPE_PK)='dropdown' then
                                                                             
                                                                                   
                                                                                   for unexpected_res in ( select codelst_desc INTO FMHQ_UNEXPECTED_CODELST_DESC from er_codelst where codelst_type =fmhq_data.unexpected_resp)
                                                                                   LOOP
                                                                                            --DBMS_OUTPUT.put_line('unexpected_res: ' || unexpected_res.codelst_desc);
                                                                                            if unexpected_res.codelst_desc=f_codelst_desc(fmhq_data.RESPONSE_CODE) then
                                                                                                   if fmhq_data.response_in_asses!=0 AND f_codelst_desc(fmhq_data.response_in_asses)=f_codelst_desc(fmhq_data.RESPONSE_CODE) then
                                                                                               
                                                                                                      ASSESS_PASSED:=1;
                                                                                                   end if; 
                                                                                                   if fmhq_data.response_in_asses!=0 AND f_codelst_desc(fmhq_data.response_in_asses)!=f_codelst_desc(fmhq_data.RESPONSE_CODE) then
                                                                                                    
                                                                                                      ASSESS_PASSED:=0;
                                                                                                      exit;
                                                                                                   end if;   
                                                                                                   if fmhq_data.response_in_asses=0 then
                                                                                                      
                                                                                                      ASSESS_PASSED:=0;
                                                                                                      exit;
                                                                                               
                                                                                                   end if;
                                                                                            end if;
                                                                                            if unexpected_res.codelst_desc!=f_codelst_desc(fmhq_data.RESPONSE_CODE) then
                                                                                                ASSESS_PASSED:=1;
                                                                                            end if;
                                                                                   END LOOP;
                                                                                   --dbms_output.put_line('1.....FMHQ_UNEXPECTED_CODELST_DESC:::::::::::::::::'||FMHQ_UNEXPECTED_CODELST_DESC||'ASSESS_PASSED::::'||fmhq_data.QUES_SEQ ||' '||ASSESS_PASSED);
                                                                                   
                                                                            end if;
                                                                            
                                                                            
                                                                            if f_codelst_desc(fmhq_data.RESPONSE_TYPE_PK)='multiselect' and ( LENGTH(fmhq_data.RESPONSE_VAL12) - LENGTH( REPLACE(fmhq_data.RESPONSE_VAL12, ',', '') ) )=0 then
                                                                             
                                                                                   
                                                                                   for unexpected_res1 in ( select codelst_desc INTO FMHQ_UNEXPECTED_CODELST_DESC from er_codelst where codelst_type =fmhq_data.unexpected_resp)
                                                                                   LOOP
                                                                                            --DBMS_OUTPUT.put_line('unexpected_res: ' || unexpected_res1.codelst_desc);
                                                                                            if unexpected_res1.codelst_desc=f_codelst_desc(fmhq_data.RESPONSE_VAL12) then
                                                                                                   if fmhq_data.response_in_asses!=0 AND f_codelst_desc(fmhq_data.response_in_asses)=f_codelst_desc(fmhq_data.RESPONSE_VAL12) then
                                                                                                      ASSESS_PASSED:=1;
                                                                                                   end if; 
                                                                                                   if fmhq_data.response_in_asses!=0 AND f_codelst_desc(fmhq_data.response_in_asses)!=f_codelst_desc(fmhq_data.RESPONSE_VAL12) then
                                                                                                      ASSESS_PASSED:=0;
                                                                                                      --exit;
                                                                                                   end if;   
                                                                                                   if fmhq_data.response_in_asses=0 then
                                                                                                      ASSESS_PASSED:=0;
                                                                                                      --exit;
                                    
                                                                                                   end if;
                                                                                            end if;
                                                                                   END LOOP;
                                                                                   --dbms_output.put_line('2.....FMHQ_UNEXPECTED_CODELST_DESC:::::::::::::::::'||FMHQ_UNEXPECTED_CODELST_DESC||'ASSESS_PASSED::::'||fmhq_data.QUES_SEQ ||' '||ASSESS_PASSED);
                                                                                   
                                                                            end if;
                                                                            
                                                                            
                                                                             if f_codelst_desc(fmhq_data.RESPONSE_TYPE_PK)='multiselect' and ( LENGTH(fmhq_data.RESPONSE_VAL12) - LENGTH( REPLACE(fmhq_data.RESPONSE_VAL12, ',', '') ) )>0 then
                                                                             
                                                                                   
                                                                                   for unexpected_res2 in ( select codelst_desc INTO FMHQ_UNEXPECTED_CODELST_DESC from er_codelst where codelst_type =fmhq_data.unexpected_resp)
                                                                                   LOOP
                                                                                            --DBMS_OUTPUT.put_line('unexpected_res: ' || unexpected_res2.codelst_desc);
                                                                                            v_split_string := emb_string.split_string(fmhq_data.RESPONSE_VAL12);
                                                                                            if v_split_string.count > 0 then
                                                                                            for i in 1..v_split_string.count loop
                                                                                            --dbms_output.put_line(v_split_string(i));
                                                                                            select f_codelst_desc(v_split_string(i)) into temp_string from dual;                        
                                                                                                      if instr(unexpected_res2.codelst_desc,temp_string) !=0 then
                                                                                                             if fmhq_data.response_in_asses!=0 AND f_codelst_desc(fmhq_data.response_in_asses)=temp_string then
                                                                                                                ASSESS_PASSED:=1;
                                                                                                                exit;
                                                                                                             end if; 
                                                                                                             if fmhq_data.response_in_asses!=0 AND f_codelst_desc(fmhq_data.response_in_asses)!=temp_string then
                                                                                                                ASSESS_PASSED:=0;
                                                                                                                --exit;
                                                                                                             end if;   
                                                                                                             if fmhq_data.response_in_asses=0 then
                                                                                                                ASSESS_PASSED:=0;
                                                                                                                --exit;
                                                                                                             end if;
                                                                                                      end if;
                                                                                                      
                                                                                            end loop;
                                                                                            end if;     
                                                                                            
                                                                                   END LOOP;
                                                                                   --dbms_output.put_line('3.....FMHQ_UNEXPECTED_CODELST_DESC:::::::::::::::::'||FMHQ_UNEXPECTED_CODELST_DESC||'ASSESS_PASSED::::'||fmhq_data.QUES_SEQ ||' '||ASSESS_PASSED);
                                                                                   
                                                                            end if;
                                                                   
                  --                                                                  
                                                                            
                                                                            
                                                                      end if;                                                
                                                                    
                                                                     --dbms_output.put_line('ASSESS_PASSED::::'||fmhq_data.QUES_SEQ ||' '||ASSESS_PASSED);
                                                                  
                                                                  
                                                                      
                                                                      if fmhq_data.DEPENT_QUES_PK='0' then
                                                                             if(fmhq_data.RESPONSE_CODE!='0' or fmhq_data.dynaformDate!='0') then
                                                                             --dbms_output.put_line('data is for non dependent question::::'||fmhq_data.QUES_SEQ ||' '||fmhq_data.QUES_DESC);
                                                                             fmhq_compl_flag:=1;
                                                                             end if;
                                                                                                                                      
                                                                             
                                                                             if(fmhq_data.RESPONSE_CODE='0' and fmhq_data.dynaformDate='0') or ASSESS_PASSED=0 then  
                                                                                  --dbms_output.put_line('Inside exit of dependent quest pk 0 condit 0::::::::::::'||fmhq_data.QUES_SEQ ||' '||fmhq_data.QUES_DESC);
                                                                                  fmhq_compl_flag:=0;
                                                                                  EXIT;
                                                                              
                                                                             end if;
                                                                             
                                                                             
                                                                      end if;
                                                                                                             
                                                                     if fmhq_data.DEPENT_QUES_PK!='0' then                                                                                                                                                             
                                                                          
                                                                                                                                
                                                                          
                                                                          
                                                                         select 
                                                                              count(*) fmhq_res_type INTO MASTER_QUES_DATA_IS_THER_FMHQ
                                                                              from cb_form_responses formres2,cb_questions ques2 where formres2.FK_QUESTION = fmhq_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and cb_forms.is_current_flag=1)) and ques2.pk_questions=fmhq_data.DEPENT_QUES_PK;    
                                                                             
                                                                              
                                                                          IF MASTER_QUES_DATA_IS_THER_FMHQ>0 THEN
                                                                          
                                                                              select 
                                                                              CASE 
                                                                              WHEN ques2.RESPONSE_TYPE=IS_TXT_TYP OR ques2.RESPONSE_TYPE=IS_MULTISELL_TYP THEN 0
                                                                              ELSE 1
                                                                              END INTO fmhq_res_type 
                                                                              from cb_form_responses formres2,cb_questions ques2 where formres2.FK_QUESTION = fmhq_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and cb_forms.is_current_flag=1)) and ques2.pk_questions=fmhq_data.DEPENT_QUES_PK;
                                                                              
                                                                              --dbms_output.put_line('response from multiselect or textfield'||fmhq_masterques_response);
                                                                              
                                                                              
                                                                             IF fmhq_res_type=0 THEN
                                                                                  select 
                                                                                  NVL(FORMRES2.RESPONSE_VALUE,'0') INTO fmhq_masterques_response 
                                                                                  from cb_form_responses formres2,cb_questions ques2 where formres2.FK_QUESTION = fmhq_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and cb_forms.is_current_flag=1))  and ques2.pk_questions=fmhq_data.DEPENT_QUES_PK;
                                                                             END IF;
                                                                         
                                                                         
                                                                             IF fmhq_res_type=1 THEN
                                                                                select 
                                                                                  NVL(FORMRES2.RESPONSE_CODE,'0') INTO fmhq_masterques_response 
                                                                                  from cb_form_responses formres2,cb_questions ques2 where formres2.FK_QUESTION = fmhq_data.DEPENT_QUES_PK and formres2.entity_id = PK_CORDID and formres2.fk_form_version = (select MAX(PK_FORM_VERSION) from cb_form_version where entity_id=PK_CORDID and fk_form=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and cb_forms.is_current_flag=1))  and ques2.pk_questions=fmhq_data.DEPENT_QUES_PK;
                                                                             
                                                                             END IF;
                                                                          
                                                                    END IF;
                                                                          
                                                                    IF MASTER_QUES_DATA_IS_THER_FMHQ=0 THEN
                                                                              fmhq_masterques_response:='0';
                                                                    END IF;
                                                                                                                             
                                                                                                                                                                             
                                                                          --dbms_output.put_line('master question value::::'||fmhq_data.DEPENT_QUES_VALUE||'response from previous codit::::::;'||fmhq_masterques_response||'CONDITION RESULT:::::::'||instr(fmhq_masterques_response,fmhq_data.DEPENT_QUES_VALUE));
                                                                           
                                                                           if fmhq_masterques_response!='0'  and instr(fmhq_masterques_response,fmhq_data.DEPENT_QUES_VALUE) !=0 then
                                                                             
                                                                              if fmhq_data.RESPONSE_CODE != '0' or fmhq_data.dynaformDate != '0' or fmhq_data.RESPONSE_VAL12 !='0' then
                                                                                fmhq_compl_flag:=1;
                                                                                --dbms_output.put_line('required data in child question::::'||fmhq_data.QUES_SEQ ||' '||fmhq_data.QUES_DESC);
                                                                              end if;
                                                                              
                                                                              if(fmhq_data.RESPONSE_CODE = '0' and fmhq_data.dynaformDate = '0' and fmhq_data.RESPONSE_VAL='0')  or ASSESS_PASSED=0   then
                                                                                --dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 1::::::::::::'||fmhq_data.RESPONSE_CODE ||' '||fmhq_data.dynaformDate);
                                                                                --dbms_output.put_line('Inside exit of condit 1::::::::::::'||fmhq_data.QUES_SEQ ||' '||fmhq_data.QUES_DESC);
                                                                                fmhq_compl_flag:=0;
                                                                                EXIT;
                                                                              end if;
                                                                              
                                                                           end if;
                                                            --               if(fmhq_masterques_response='0' and instr(fmhq_data.DEPENT_QUES_VALUE, fmhq_masterques_response)=0) then
                                                            --                     dbms_output.put_line('Inside exit of dependent quest pk not 0 condit 2::::::::::::'||fmhq_data.DEPENT_QUES_VALUE||' '||instr(fmhq_data.DEPENT_QUES_VALUE, fmhq_masterques_response));
                                                            --                    fmhq_compl_flag:=0;
                                                            --                    EXIT;
                                                            --               end if;
                                                                             
                                                                     end if;
                                                                      
                                                                  end loop;
                                                    END IF;
                                                
                                                END IF;
                                                
                                                --dbms_output.put_line('fmhq_compl_flag FOR UNLICENSED CORD::::'||fmhq_compl_flag);
                              
                              END IF;
                          
                                   
                                     
                 
        END IF;
        
        
        
        IF IS_SYSTEM_USER=0 THEN 
        
                           ---IDS TASKS FOR NON-SYS CORD---
                           
                           SELECT CASE WHEN CORD_REGISTRY_ID IS NOT NULL AND CORD_ID_NUMBER_ON_CBU_BAG IS NOT NULL THEN 1 ELSE 0 END IDSFLAG INTO IDS_COMPL_FLAG
                           FROM CB_CORD 
                           WHERE PK_CORD IN (PK_CORDID);
                           
                           --DBMS_OUTPUT.PUT_LINE('IDS INFORMATION FLAG FOR NON-SYS CORD VALUE::::::::::::'||IDS_COMPL_FLAG);
                           
                           --LICENSURE TASK FOR NON-SYS CORD---
                           
                           SELECT 
                           CASE 
                                  WHEN CRD.FK_CORD_CBU_LIC_STATUS IS NOT NULL AND CRD.FK_CORD_CBU_LIC_STATUS !=-1 THEN 
                                                                         CASE  
                                                                              WHEN CRD.FK_CORD_CBU_LIC_STATUS IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='licence' AND CODELST_SUBTYP='unlicensed') THEN
                                                                                CASE 
                                                                                  WHEN (SELECT COUNT(*) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD IN (PK_CORDID))>0 THEN 1
                                                                                  ELSE 0
                                                                                END                                                           
                                                                              ELSE 1
                                                                         END
                                 ELSE 0
                           END INTO LIC_COMPL_FLAG
                           FROM CB_CORD CRD WHERE CRD.PK_CORD IN (PK_CORDID);   
                           
           END IF;
           
           
           
           IF is_data_for_cord!=0 THEN
            
            IF OLD_IDS_COMPL_FLAG!=IDS_COMPL_FLAG  THEN    DONT_ALLOW_INSERT_FLAG:=0; END IF;
            
            IF OLD_CBUINFO_COMPL_FLAG!=CBUINFO_COMPL_FLAG  THEN   DONT_ALLOW_INSERT_FLAG:=0; END IF;
            
            IF OLD_PROS_COMPL_FLAG!=PROS_COMPL_FLAG  THEN  DONT_ALLOW_INSERT_FLAG:=0;  END IF;
            
            IF OLD_LAB_SUMMAR_COMP1_FLAG!=LAB_SUMMAR_COMP1_FLAG  THEN  DONT_ALLOW_INSERT_FLAG:=0; END IF;
            
            IF OLD_LIC_COMPL_FLAG!=LIC_COMPL_FLAG  THEN  DONT_ALLOW_INSERT_FLAG:=0;  END IF;
            
            IF old_idm_compl_flag!=idm_compl_flag  THEN  DONT_ALLOW_INSERT_FLAG:=0;  END IF;
            
            IF old_mrq_compl_flag!=mrq_compl_flag  THEN  DONT_ALLOW_INSERT_FLAG:=0;  END IF;
            
            IF old_fmhq_compl_flag!=fmhq_compl_flag  THEN  DONT_ALLOW_INSERT_FLAG:=0; END IF;
            
       
       END IF;
       
       IF is_data_for_cord=0 THEN
            DONT_ALLOW_INSERT_FLAG:=0; 
       END IF;
      
                          --dbms_output.put_line('OLD FLAG VALUES::::IDS_COMPL_FLAG::::::'||OLD_IDS_COMPL_FLAG||'CBUINFO_COMPL_FLAG:::::::'||OLD_CBUINFO_COMPL_FLAG||'LAB_SUMMAR_COMP1_FLAG:::::::::'||OLD_LAB_SUMMAR_COMP1_FLAG||'mrq_compl_flag::::::::::::'||old_mrq_compl_flag||'fmhq_compl_flag::::::::::::'||old_fmhq_compl_flag||'idm_compl_flag::::::::::::'||old_idm_compl_flag||'CBU_PROCESSING_FLAG::::::::::::::'||OLD_PROS_COMPL_FLAG||'LIC_COMPL_FLAG:::::::::::::'||OLD_LIC_COMPL_FLAG||'DONT_ALLOW_INSERT_FLAG:::::::::'||DONT_ALLOW_INSERT_FLAG);
                          --dbms_output.put_line('NEW FLAG VALUES::::IDS_COMPL_FLAG::::::'||IDS_COMPL_FLAG||'CBUINFO_COMPL_FLAG:::::::'||CBUINFO_COMPL_FLAG||'LAB_SUMMAR_COMP1_FLAG:::::::::'||LAB_SUMMAR_COMP1_FLAG||'mrq_compl_flag::::::::::::'||mrq_compl_flag||'fmhq_compl_flag::::::::::::'||fmhq_compl_flag||'idm_compl_flag::::::::::::'||idm_compl_flag||'CBU_PROCESSING_FLAG::::::::::::::'||PROS_COMPL_FLAG||'LIC_COMPL_FLAG:::::::::::::'||LIC_COMPL_FLAG);
                           
                           
                          IF IS_SYSTEM_USER=0 AND DONT_ALLOW_INSERT_FLAG=0 THEN 
                              
                              IF(IDS_COMPL_FLAG=1 AND LIC_COMPL_FLAG=1) THEN
                                      
                                          CRI_COMP_FLAG:=1;
                                          Insert into CB_CORD_COMPLETE_REQ_INFO (PK_CORD_COMPLETE_REQINFO,COMPLETE_REQ_INFO_FLAG,IDS_FLAG,ELIGIBILITY_FLAG,LICENSURE_FLAG,CBU_INFO_FLAG,CBU_PROCESSING_FLAG,LAB_SUMMARY_FLAG,MRQ_FLAG,FMHQ_FLAG,IDM_FLAG,FK_CORD_ID,FK_ORDER_ID,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,RID,DELETEDFLAG) values 
                                                                                 (SEQ_CB_CORD_COMPLETE_REQ_INFO.nextval,CRI_COMP_FLAG,IDS_COMPL_FLAG,NULL,LIC_COMPL_FLAG,NULL,NULL,NULL,NULL,NULL,NULL,PK_CORDID,null,null,sysdate,null,null,null,null,null);
                                          commit;
                              END IF; 
                              
                              IF(IDS_COMPL_FLAG=0 OR LIC_COMPL_FLAG=0) THEN
                                      
                                          CRI_COMP_FLAG:=0;
                                          Insert into CB_CORD_COMPLETE_REQ_INFO (PK_CORD_COMPLETE_REQINFO,COMPLETE_REQ_INFO_FLAG,IDS_FLAG,ELIGIBILITY_FLAG,LICENSURE_FLAG,CBU_INFO_FLAG,CBU_PROCESSING_FLAG,LAB_SUMMARY_FLAG,MRQ_FLAG,FMHQ_FLAG,IDM_FLAG,FK_CORD_ID,FK_ORDER_ID,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,RID,DELETEDFLAG) values 
                                                                                (SEQ_CB_CORD_COMPLETE_REQ_INFO.nextval,CRI_COMP_FLAG,IDS_COMPL_FLAG,NULL,LIC_COMPL_FLAG,NULL,NULL,NULL,NULL,NULL,NULL,PK_CORDID,null,null,sysdate,null,null,null,null,null);
                                          commit;
                              END IF; 
                          
                        END IF;
                          
                       
                        IF IS_SYSTEM_USER=1 AND DONT_ALLOW_INSERT_FLAG=0 THEN 
                             
                              IF(IDS_COMPL_FLAG=1 AND CBUINFO_COMPL_FLAG=1 AND PROS_COMPL_FLAG=1 AND LAB_SUMMAR_COMP1_FLAG=1 AND mrq_compl_flag=1 AND idm_compl_flag=1 AND fmhq_compl_flag=1 AND LIC_COMPL_FLAG=1) THEN
                                  
                                      CRI_COMP_FLAG:=1;
                                      Insert into CB_CORD_COMPLETE_REQ_INFO (PK_CORD_COMPLETE_REQINFO,COMPLETE_REQ_INFO_FLAG,IDS_FLAG,ELIGIBILITY_FLAG,LICENSURE_FLAG,CBU_INFO_FLAG,CBU_PROCESSING_FLAG,LAB_SUMMARY_FLAG,MRQ_FLAG,FMHQ_FLAG,IDM_FLAG,FK_CORD_ID,FK_ORDER_ID,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,RID,DELETEDFLAG) values 
                                                                             (SEQ_CB_CORD_COMPLETE_REQ_INFO.nextval,CRI_COMP_FLAG,IDS_COMPL_FLAG,null,LIC_COMPL_FLAG,CBUINFO_COMPL_FLAG,PROS_COMPL_FLAG,LAB_SUMMAR_COMP1_FLAG,mrq_compl_flag,fmhq_compl_flag,idm_compl_flag,PK_CORDID,null,null,sysdate,null,null,null,null,null);
                                                                             
                                                                             commit;
                          
                              END IF;     
                              IF(IDS_COMPL_FLAG=0 OR CBUINFO_COMPL_FLAG=0 OR PROS_COMPL_FLAG=0 OR LAB_SUMMAR_COMP1_FLAG=0 OR mrq_compl_flag=0 OR idm_compl_flag=0 OR fmhq_compl_flag=0 OR LIC_COMPL_FLAG=0) THEN
                                  
                                      CRI_COMP_FLAG:=0;
                                      Insert into CB_CORD_COMPLETE_REQ_INFO (PK_CORD_COMPLETE_REQINFO,COMPLETE_REQ_INFO_FLAG,IDS_FLAG,ELIGIBILITY_FLAG,LICENSURE_FLAG,CBU_INFO_FLAG,CBU_PROCESSING_FLAG,LAB_SUMMARY_FLAG,MRQ_FLAG,FMHQ_FLAG,IDM_FLAG,FK_CORD_ID,FK_ORDER_ID,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,RID,DELETEDFLAG) values 
                                                                             (SEQ_CB_CORD_COMPLETE_REQ_INFO.nextval,CRI_COMP_FLAG,IDS_COMPL_FLAG,null,LIC_COMPL_FLAG,CBUINFO_COMPL_FLAG,PROS_COMPL_FLAG,LAB_SUMMAR_COMP1_FLAG,mrq_compl_flag,fmhq_compl_flag,idm_compl_flag,PK_CORDID,null,null,sysdate,null,null,null,null,null);
                                                                             
                                                                             commit;
                          
                              END IF;      
                          
                        END IF;
                        
                        
                           
                          --DBMS_OUTPUT.PUT_LINE('LICENCE COMPLETE FLAG FOR NON-SYS CORD VALUE::::::::::::'||LIC_COMPL_FLAG);
                 
       
            
EXCEPTION    

   WHEN NO_DATA_FOUND THEN dbms_output.put_line ('NO DATA FOUND IN SP_CRI_TASKS');

END; 
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,203,16,'16_hotfix2_SP_CRI_TASKS.sql',sysdate,'9.0.0 B#637-ET013.02');

commit;
