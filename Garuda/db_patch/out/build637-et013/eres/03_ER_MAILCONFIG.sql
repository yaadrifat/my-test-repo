Update ER_MAILCONFIG set MAILCONFIG_CONTENT=CHR(13) ||
	'Dear [USER],</br></br></br>' || CHR(13) ||	
	CHR(13) || CHR(13) || 	
	'This is to notify you that CBU Import: Job# [ID] has been completed on [DATE].</br>' || CHR(13) || CHR(13) || 
	'<table><tr><td align="left">CBB ID</td><td>:</td><td align="left">[CBBID]</td></tr><tr><td align="left">Start Time </td><td>:</td><td align="left">[STARTTIME]</td></tr><tr><td align="left">Completion Time</td><td>:</td><td align="left">[COMPLETIONTIME]</td></tr><tr><td align="left">Job ID</td><td>:</td><td align="left">[ID]</td></tr><tr><td align="left">Job Status</td><td>:</td><td align="left">Complete</td></tr></table><br><br><p>This automated EmTrax communication has been sent to you as per your alert preferences. If you wish to discontinue these alerts, please access the alert management section of your profile in EmTrax.</p><table><tr><td><img alt="" src="http://marrow.org/images/email/nmdp_80.png"></td><td valign="middle"><img alt="" src="http://marrow.org/images/email/btm_50.png"></td></tr></table><br><hr><p>***This is an auto-generated e-mail***</p><p>This e-mail address is not monitored; please do not respond to this message.</p><br><p>The sender of this e-mail provides services for the NMDP and/or Be The Match pursuant to agreement and may not be authorized to bind the organization with any information, material, or content contained within this e-mail.</p>' || CHR(13)  where MAILCONFIG_MAILCODE='IMPORTNOTI';
	

	
Update ER_MAILCONFIG set MAILCONFIG_SUBJECT='CBB ID: [CBBID] - CBU Import Job# [ID] started on [STARTTIME] - Status : Complete' where MAILCONFIG_MAILCODE='IMPORTNOTI';
commit;

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_MAILCONFIG
    where mailconfig_cc='null';
  if (v_record_exists > 0) then
      UPDATE ER_MAILCONFIG SET mailconfig_cc = null where mailconfig_cc='null';
	commit;
  end if;
end;
/
--END--

BEGIN
  SP_CLEAN_NOTES();
END;

/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,203,3,'03_ER_MAILCONFIG.sql',sysdate,'9.0.0 B#637-ET013');

commit;
