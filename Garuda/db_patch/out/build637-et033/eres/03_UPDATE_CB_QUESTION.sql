--start update cb_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='refused_bld_dnr_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes - Cord blood bank staff to further review cause for previous deferral with mother.  Cord blood bank medical director to evaluate cause and defer, if necessary.  However, if &#145;Y&#146; answer reveals a previous deferral for communicable disease infection/risk or xenotransplantation risk &#8594; defer.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action.' where ques_code='refused_bld_dnr_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='evr_tkn_pit_gh_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes - CJD has been documented in individuals that received human pituitary growth hormone, which was prescribed for children with delayed or impaired growth &#8594; Defer.   Donors who received recombinant growth hormone (rCH) are acceptable because the hormone is synthetic.<br/><br/>If Bank did not ask or Mother did not answer after May 25, 2005 &#8594; Incomplete Eligibility and requires Declaration of Urgent Medical Need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action.' where ques_code='evr_tkn_pit_gh_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='shots_vacc_pst_8wk_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes - Cord blood bank medical director to evaluate response and defer, if necessary.  Defer donation if the mother received a live vaccine, such as smallpox, oral polio, yellow fever, chicken pox (varicella), rubeola (measles) / mumps / rubella (MMR), or FluMist.  The donation is acceptable if the mother received rhoGAM injections.  Heparin injections for thrombosis are also acceptable.<br/><br/>If Bank did not ask or Mother did not answer on or after August 27, 2007 and  the smallpox vaccine was not addressed in this, or any, question &#8594; Incomplete Eligibility and requires Declaration of Urgent Medical Need.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 - timeframe was changed to 8 weeks to be consisten with FDA wording for smallpox vaccine.  Previous quesiton had broader timeframe of 12 weeks adn contained same physician evaluation and deferral assessment for live vaccines.  Consider responses as equivalent to current.' where ques_code='shots_vacc_pst_8wk_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='shots_vacc_pst_8wk_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes -&#8594; Defer.<br/><br/>If Bank did not ask or Mother did not asnwer on or after August 27, 2007 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 - timeframe was changed to 12 weeks to be consistent with FDA deferral for contact with recipient of smallpox vaccine.  Previous question had 8 week timeframe consistent with AABB Universal Health History form and FDA Guidance for Recommendtions for Deferral Related to Smallpox Vaccine (12-02).  Consider question as equivalent to current.' where ques_code='cntc_smallpox_vaccine_12wk_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='bld_prblm_5yr_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; Defer.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 - timeframe was changed to 5 years to be consisten with FDA language.  Previous question had broader timeframe and contained same physician evaluation and deferral assessment for bleeding problems.  Consider responses as equivalent to current.' where ques_code='bld_prblm_5yr_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='wnv_preg_diag_pos_test_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; defer donation.  Although the FDA requires deferral for 120 days following diagnosis or onset of illness, whichever is later for persons who have a medical diagnosis or suspicion of WNV infection (based on symptoms and/or laboratory results, or confirmed WNV viremia), the NMDP Quality Standards subcommittee had extended the deferral to the entire pregnancy.  A pregnant woman&#145;s immune system may not adequately clear the infection.<br/><br/>If Bank did not ask or Mother did not answer on or after August 27, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need. <br/><br/><u>Historical Evaluation</u><br/>As of August 27, 2007 &#8594; question was changed to be consistent with FDA language for evaluation of WNV infection.  Consider responses as equivalent to current.' where ques_code='wnv_preg_diag_pos_test_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='viral_hepatitis_age_11_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes - Cord Blood Bank Medical Director to evaluate clinical history and status as ineligible, if necessary.  A history of neonatal jaundice or other jaundice before the age of 11 is acceptable, unless known to be due to Hepatitis B or C.  Jandice due to bile duct obstruction (e.g. by gallstones) or jaundice associated with a past history of infectious mononucleosis are acceptable.  Accept if evidence from time of illness documents that the hepatitis was identified as being caused by hepatitis A virus, Epstein-Barr Virus (EBV), or cytomegalovirus (CMV).  Janudice of an unknown etiology must be evaluated by the Medical Directorr to determine if the donation would place the potential recipient at an increased risk for disease transmission.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of October 1 ,2011 - question was changed to be consistent with FDA wording.  Previous question had broader wording that included &#147;yellow janudice, liver disease, viral hepatitis or a positive test for hepatitis.&#148;  Previous question contained same physician evaluation and deferral assessment for Hepatitis B or C infection.  Consider responses as equivalent.' where ques_code='viral_hepatitis_age_11_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='para_chagas_babesiosis_leish_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; defer.<br/><br/><u>Historical Evaluation</u><br/>Leishmaniasis or a positive test for Chagas or T. cruzi were not specifically included in previous questions.  If you didn&#145;t ask specifically about these risks at your bank, question 11 must be answered with a response of Bank did not ask.' where ques_code='para_chagas_babesiosis_leish_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='cjd_diag_neuro_unk_ind' or ques_code='bld_rlt_diag_rsk_cjd';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; defer.<br/><br/>If Bank did not ask or Mother did not answer on or after 5-25-05 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of August 27, 2007 - question was changed to be consistent with FDA language for evaluation of maternal donor risk for CJD or vCJD.  Previous question combined maternal and family risk.  Consider responses as equivalent to current.' where ques_code='cjd_diag_neuro_unk_ind' or ques_code='bld_rlt_diag_rsk_cjd';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='dura_mater_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; defer.<br/><br/>If Bank did not ask or Mother did not answer on or after 5-25-05 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action' where ques_code='dura_mater_ind';

commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='recv_xeno_tx_med_procedure_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; defer.  Xenotransplantation is defined by the FDA as any procedure that involved the transplantation, implantation, or infusion into a human recipient of either (1) live cells, tissues, or organs from a nonhuman animal source: or (2) human body fluids, cells, tissues, or organs that have had ex vivo contact with live nonhuman animal cells, tissue or organs.<br/><br/>If Bank did not ask or Mother did not answer on or after 5-25-05 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action' where ques_code='recv_xeno_tx_med_procedure_ind';

commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='liv_cntc_xeno_tx_med_proc_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; defer.  Xenotransplantation is defined by the FDA as any procedure that involved the transplantation, implantation, or infusion into a human recipient of either (1) live cells, tissues, or organs from a nonhuman animal source: or (2) human body fluids, cells, tissues, or organs that have had ex vivo contact with live nonhuman animal cells, tissue or organs.  Living in the same household may expose the donor to blood, saliva, or other body fluids from the xenotransplant recipient, presenting possible risk of transmission of viruses or other unknown risks.<br/><br/>If Bank did not ask or Mother did not answer on or after 5-25-05 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action' where ques_code='liv_cntc_xeno_tx_med_proc_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='malaria_antimalr_drug_3yr_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; defer.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action' where ques_code='malaria_antimalr_drug_3yr_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='outside_us_or_canada_3yr_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; Cord Blood Bank Medical Director to evaluate response and defer, if necessary.<br/><br/>Review Center for Disease Control&#145;s Website (www.cdc.gov/travel/) to determine if the mother traveled to or lived in a malaria endemic area. <br/><u>Travel</u><br/>If travel to a malaria endemic area did not occur in last 12 months and mother showed no signs or symptoms of malaria - accept donation.<br/>If travel to a malaria endemic area occured in last 12 months and mother showed no signs or symptoms of malaria - accept donation and inform NMDP.<br/>If travel to a malaria endemic area occurred in last 3 years and mother showed signs or symptoms of malaria - defer donation.<br/>Donations from immigrants, refugees, citizens, or residents (lived for more than 5 years) coming from a country in which malaria is considered endemic<br/>If immigrant has been in the US for 3 years or longer and has been free of diagnosis of malaria or unexplained symptoms suggestive of malaria, accept donation.<br/>If immigrant has been in the US for less than 3 years, donation must be quarantined for three years after departure from the area if they have been free from unexplained symptoms suggestive of malaria.  The bank must follow up with the mother after the quarantine period to verify that the mother has not been diagnosed with or shown symptoms of malaria during the quarantine period.  If she has, or if the bank is unable to contact the mother, the unit must be deferred.<br/>Donors who have traveled to Iraq int eh past 12 months may have been exposed to Leishmaniasis - defer donation.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action' where ques_code='outside_us_or_canada_3yr_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='blood_transf_b4coll_12m_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; defer.  Blood transfusion includes any blood component (e.g. platelets, red blood cells, white blood cells, plasma, cryoprecipitate, or granulocytes).<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 - question was clarified to indicate transfusion prior to delivery.  Consider responses as equivalent to current.' where ques_code='blood_transf_b4coll_12m_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='tx_stemcell_12m_oth_self_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; defer.<br/><br/><u>Historical Evaluation</u><br/>As of August 27, 2007 - question was clarified to provide additional examples of organs and tissues. As of October 1, 2011 - question was clarified to further define transplantation.  Consider responses as equivalent to current.' where ques_code='tx_stemcell_12m_oth_self_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
   where ques_code='tattoo_ear_skin_pierce_12m_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; Question 22 must be answered.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 - question was modified to combine ear, skin or body piercing which were previously in separate questions.  Consider responses as equivalent to current.' where ques_code='tattoo_ear_skin_pierce_12m_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
   where ques_code='tattoo_shared_nonsterile_12m_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; defer.  If the mother is unsure whether shared instruments, needles, and/or inks where used, defer donation.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 - question was modified to include additional details about sterility and piercing which were previously in separate quesitons.  Consider responses as equivalent to current.' where ques_code='tattoo_shared_nonsterile_12m_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='acc_ns_stk_cut_bld_12m_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; Cord Blood Bank Medical Director must evaluate circumstances of exposure and potential risk of disease transmission and defer if necessary.  If a risk exists, the donation must be deferred.  Donors who report contact with someone else&#145;s blood but describe wearing appropriate and intact personal protective equipment (for example, gloves, face mask, etc.) are acceptable.  Exceptions may be made for health care professionals with appropriate documentation and evidence. <br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action.' where ques_code='acc_ns_stk_cut_bld_12m_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='had_treat_syph_12m_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; Cord Blood Bank Medical Director to evaluate and defer, if necessary.  The donation is deferred if the mother&#145;s response is affirmative for syphilis.  If the mother indicates she has any active sexually transmitted disease, genital herpes infection, and/or a significant HPV infection at the time of donation, and the infant was delivered vaginally, the donation must be deferred.  If the mother has a history of HPV on a recent pap smear without evidence of disease, then the donation is acceptable.  Donations are acceptable from a mother with an active STD (HPV and/or gential herpes) provided the infant was delivered via c-section and the membranes remained intact.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 - question was changed to be consistent with FDA wording.  Previous question had broader wording that included gonorrhea.  Previous question contained same phsyician evaluation and deferral assessment for sexually transmitted diseases.  Consider responses as equivalent.' where ques_code='had_treat_syph_12m_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='given_mn_dr_for_sex_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594;  defer.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action.' where ques_code='given_mn_dr_for_sex_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='sex_w_tk_mn_dr_pmt_sex_12m_5yr_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u>If Yes &#8594;  defer.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action.' where ques_code='sex_w_tk_mn_dr_pmt_sex_12m_5yr_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='sex_cntc_liv_hep_b_c_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594;  defer.  In certain cases, living with a person with hepatitis places a donor at risk for acquiring the disease, particularly where instruments were shared (toothbrushes, razors, or other sharps).<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 - question was changed to be consistent with FDA wording.  Previous question had broader wording that included active or chronic viral hepatits or yellow jandice.  Previous question contained same physician evaluation and deferral assessment for Hepatitis B or C infection risk.  Consider responses as equivalent.' where ques_code='sex_cntc_liv_hep_b_c_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='sex_w_iv_dr_in_pst_5yr_12m_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594;  defer.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action.' where ques_code='sex_w_iv_dr_in_pst_5yr_12m_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='sex_w_man_oth_pst_5yr_12m_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594;  defer.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action.' where ques_code='sex_w_man_oth_pst_5yr_12m_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='sex_w_clot_fact_bld_12m_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594;  defer.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of August 27, 2007 - the question was clarified to be consistent with FDA language.  The previous question asked about clotting factors in general.  However, the question contained the same physician evaluation and deferral assessment.  Consider responses as equivalent to current.' where ques_code='sex_w_clot_fact_bld_12m_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='sex_w_hiv_aids_12m_ind' or ques_code='prison_jail_contin_72hrs_ind' or ques_code='sex_w_hiv_aids_12m_ind' or ques_code='pst_5yr_tk_mn_dr_sex_ind' or ques_code='pst_5yr_iv_drug_ind' or ques_code='pst_5yr_iv_drug_ind' or ques_code='aids_hiv_screen_test_ind';
  if (v_record_exists != 0) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594;  defer.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action.' where ques_code='sex_w_hiv_aids_12m_ind' or ques_code='prison_jail_contin_72hrs_ind' or ques_code='sex_w_hiv_aids_12m_ind' or ques_code='pst_5yr_tk_mn_dr_sex_ind' or ques_code='pst_5yr_iv_drug_ind' or ques_code='pst_5yr_iv_drug_ind' or ques_code='aids_hiv_screen_test_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='unexpln_night_sweat' or ques_code='unexpln_blue_prpl_spot_kaposi_ind' or ques_code='unexpln_weight_loss' or ques_code='unexpln_persist_diarrhea' or ques_code='unexpln_cough_short_breath' or ques_code='unexpln_temp_over_ten_days' or ques_code='inf_dur_preg_ind';
  if (v_record_exists != 0) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>36a, 36c, 36d, 36e, 36f, 36i<br/>If Yes &#8594;  Cord Blood Bank Medical Director should evaluate response and defer donation if yes answer reveals signs or symptoms indicative of HIV/AIDS.<br/><br/>36b, 36g, 36h<br/>If Yes &#8594; defer.<br/><br/>If Bank did not ask or Mother did not answer on or after August 27, 2007 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/><br/>36a-36h. Previous questions contained same physician evaluation and deferral assessment for HIV/AIDS infection. consider responses as equivalent.<br/>36i.  Question added October 1, 2011.  If your bank did not include assessment of maternal donr infection during pregnancy via interview or other medical record review, question 36i must be answered with a response of Bank did not ask.' where ques_code='unexpln_night_sweat' or ques_code='unexpln_blue_prpl_spot_kaposi_ind' or ques_code='unexpln_weight_loss' or ques_code='unexpln_persist_diarrhea' or ques_code='unexpln_cough_short_breath' or ques_code='unexpln_temp_over_ten_days' or ques_code='inf_dur_preg_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='htlv_incl_screen_test_para_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br>If No &#8594; defer.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 - the question was amended for clarity for the maternal donor. The previous question contained the same physician evaluation and deferral assessment. Consider responses as equivalent to current.' where ques_code='htlv_incl_screen_test_para_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='hiv_contag_person_understand_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594;  Cord Blood Bank Medical Director should evaluate response and defer if yes answer reveals signs or symptoms of HTLV.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 &#8594; the question was clarified to be consisten with FDA language to provide an example of clinical evidence of HTLV infection. The previous question contained the same phsyician evaluation and deferral assessment. Consider responses as equivalent to current.' where ques_code='hiv_contag_person_understand_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='hiv_contag_person_understand_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; accept response. Questions 40-42 must be answered.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 - the question was clarified to define countries at risk for vCJD. Consider responses as equivalent to current.' where ques_code='cjd_live_travel_country_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='spent_uk_ge_3m_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; There is a theoretical risk of transmission of vCJD through blood of individuals who ate beef while in the UK. If Yes on or after May 25, 2005 &#8594; Ineligible and requires documentation of urgent medical need.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action.' where ques_code='spent_uk_ge_3m_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='recv_trnsfsn_uk_france_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; defer if the transfusion was in the past 12 months. If transfusion was > 12 months ago, but since 1980 &#8594; Ineligible if on or after May 25, 2005 and requires documentation of urgent medical need.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of August 27, 2007 - the question was amended to include France. If your bank did not include assessment for transfusion in France between 5-25-05 and 8-27-07, transplant center must be made aware.' where ques_code='recv_trnsfsn_uk_france_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='spent_europe_ge_5y_cjd_cnt_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; Donor may be at a theoretical risk for developing vCJD from eating beef in Europe. If Yes on or after May 25, 2005 &#8594; Ineligible and requires declaration of urgent medical need.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 - the question was clarified to define countries at risk for vCJD. Consider responses as equivalent to current.' where ques_code='spent_europe_ge_5y_cjd_cnt_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='us_military_dep_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; accept response. Questions 44-45 must be answered.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 - the question was clarified to be consistent with FDA language regarding dependants of civilian military employees which was not previously specified. Prior questions consistent with AABB Universal Health History form and FDA Guidance for CJD/vCJD Risk (most recent version 5-10). Consider responses as equivalent to current.' where ques_code='us_military_dep_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='mil_base_europe_1_ge_6m_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; accept donation. Donors may be at a theoretical risk for developing vCJD from eating beef in Europe. If Yes on or after May 25, 2005 &#8594; Ineligible and requires documentation of urgent medical need.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action.' where ques_code='mil_base_europe_1_ge_6m_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='mil_base_europe_2_ge_6m_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes &#8594; accept donation.  Donors may be at a theoretical risk for developing vCJD from eating beef in Europe. If Yes on or after May 25, 2005 &#8594; Ineligible and requires documentation of urgent medical need.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action.' where ques_code='mil_base_europe_2_ge_6m_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='hiv_1_2_o_performed_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>Question 46 will not be answered by mother - question is for cord blood bank use only and is used for logic in determing status for questions 47-49. Q47-49 must be answered regardless of the response to 46.<br/><br/><u>Historical Evaluation</u><br/>Assessment based on the HIV testing performed by the bank on each individual unit.' where ques_code='hiv_1_2_o_performed_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='born_live_cam_afr_nig_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Q46 answerd Yes:<br/>Accept all responses.<br/><br/>If Q46 answerd No:<br/>If Yes - Evaluate. For persons who were born in or lived in an African country listed in chart, defer donation. For persons who traveled to an African country listed in chart, accept response.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action; see chart for countries added October 30, 2009.' where ques_code='born_live_cam_afr_nig_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='trav_ctry_rec_bld_or_med_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Q46 answerd Yes:<br/>If Yes - Evaluate.  Defer donation if the transfusion was in the past 12 months.  If transfusion > 12 months, accept donation.<br/><br/>If Q46 answerd No:<br/>If Yes - defer.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/>Historical Evaluation<br/>Same as current evaluation and action; see chart for countries added October 30, 2009.' where ques_code='trav_ctry_rec_bld_or_med_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='sex_w_born_live_in_ctry_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Q46 answerd Yes:<br/>If Yes - Evaluate.  Defer donation if the transfusion was in the past 12 months.  If transfusion > 12 months, accept donation.<br/><br/>If Q46 answerd No:<br/>If Yes - defer.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/>Historical Evaluation<br/>Same as current evaluation and action; see chart for countries added October 30, 2009.' where ques_code='sex_w_born_live_in_ctry_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='sex_w_born_live_in_ctry_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Q46 answerd Yes:<br/>Accept all responses.<br/><br/>If Q46 answerd No:<br/>If Yes - defer. <br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action; see chart for countries added October 30, 2009.' where ques_code='sex_w_born_live_in_ctry_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='cmv_total_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Reactive/Positive - No action<br/>If Non-Reactive/Negative - No action<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/>Anti-CMV Total is not part of eligibility testing.<br/>Anti-CMV Total (total IgG + IgM) was required by the NMDP by August 27, 2007.  If Anti-CMV Total was not performed, record IgG or IgM results in Additional/Miscellaneious IDM Tests  section.<br/>' where ques_code='cmv_total_ind';
commit;
  end if;
end;
/
--END--
INSERT INTO track_patches VALUES(seq_track_patches.nextval,241,3,'03_UPDATE_CB_QUESTION.sql',sysdate,'9.0.0 B#637-ET033');
commit;