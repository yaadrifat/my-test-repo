COMMENT ON COLUMN AUDIT_COLUMN.REMARKS IS 'Stores remarks made';

COMMENT ON COLUMN AUDIT_COLUMN_MODULE.COLUMN_DISPLAY_NAME IS 'Stores the column name that will be visible over UI';

Comment On Column Audit_Error_Log.Pk_Audit_Error_Log Is 'Primary key of the table, this is generated from the Database sequence';
Comment On Column Audit_Error_Log.Module_Name Is 'Stores name of the module';
Comment On Column Audit_Error_Log.Error_Type Is 'Stores type of the error.';
Comment On Column Audit_Error_Log.Error_Source Is 'Stores the source of teh error like Function or Procedure ';
Comment On Column Audit_Error_Log.Error_Message Is 'Stores the error message.';
Comment On Column Audit_Error_Log.Error_Sql Is 'Stores the sql.';
Comment On Column Audit_Error_Log.Error_Description Is 'Sotres the error description.';
Comment On Column Audit_Error_Log.Status_Flag Is 'Stores the status of the flag';
Comment On Column Audit_Error_Log.LAST_MODIFIED_DATE Is 'This column is used for Audit Trail. Stores the date on which this row was last modified.';

COMMENT ON COLUMN AUDIT_INSERT.RAID IS 'Stores Row audit ID';

COMMENT ON COLUMN AUDIT_INSERT.ROW_DATA IS 'Stores the data of the row effected';

COMMENT ON COLUMN AUDIT_MAPPEDVALUES.PK_AUDITMAPPEDVALUE IS 'Primary key of the table, this is generated from the Database sequence';

COMMENT ON COLUMN AUDIT_ROW_MODULE.TABLE_DISPLAY_NAME IS 'Stores the display name of the table.';


Comment On Column CB_ALERTS_MAINTENANCE.ENTITY_ID Is 'Stores Id Of The Entity Like Cbu, Doner Etc';

Comment On Column CB_ALERTS_MAINTENANCE.ENTITY_TYPE Is 'Stores Type of entity';

Comment On Column CB_CORD_MINIMUM_CRITERIA.DELETEDFLAG Is 'This column denotes whether record is deleted.';


Comment On Column Cb_Cord_Complete_Req_Info.Deletedflag Is 'This column denotes whether record is deleted.';

Comment On Column Cb_Cord_Complete_Req_Info.CREATOR Is 'This Column Is Used For Audit Trail. Identifies The User Who Created This Row. Stores Pk Of Table Er_User.';

Comment On Column CB_CORD_COMPLETE_REQ_INFO.CREATED_ON Is 'This column is used for the audit trail. Stores the date on which this row was created.';

Comment On Column CB_CORD_COMPLETE_REQ_INFO.IP_ADD Is 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';

Comment On Column CB_CORD_COMPLETE_REQ_INFO.LAST_MODIFIED_BY Is 'This column is used for the audit trail. Identifies the user who last modified this row. Stores PK of table ER_USER';

Comment On Column CB_CORD_COMPLETE_REQ_INFO.LAST_MODIFIED_DATE Is 'This column is used for the audit trail. Stores the date on which this row was last modified.';

Comment On Column CB_CORD_COMPLETE_REQ_INFO.RID Is 'This column is used for the audit trail. Uniquely identifies the row in the database';

Comment On Column CB_CORD_COMPLETE_REQ_INFO.COMPLETE_REQ_INFO_FLAG Is 'This column denotes whether CRI is complete or not for the cord.';

Comment On Column CB_CORD_COMPLETE_REQ_INFO.FK_ORDER_ID Is 'This column reference of ER_Order.';


COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."PK_ALERT_INSTANCES"
IS
  'Stores sequence generated unique value';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."FK_CODELST_ALERT"
IS
  'Stores reference to er_codelst table where codelst_type=alert';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."ENTITY_ID"
IS
  'Stores entity primary key value';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."ENTITY_TYPE"
IS
  'Stores reference to er-codelst table where codelst_type=entity_type';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."ALERT_CLOSE_FLAG"
IS
  'Stores 1 if alert is viewed';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."FK_USERID"
IS
  'Stores reference to er_user table to indicate that this alert is assigned to user';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."CREATOR"
IS
  'Uses for Audit trail. Stores who created the record';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."CREATED_ON"
IS
  'Uses for Audit trail. Stores when created the record';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."LAST_MODIFIED_BY"
IS
  'Uses for Audit trail. Stores who modified the record recently';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."LAST_MODIFIED_DATE"
IS
  'Uses for Audit trail. Stores when modified the record recently';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."IP_ADD"
IS
  'Uses for Audit trail. Stores from whcih IP Address action is performed';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."DELETEDFLAG"
IS
  'Uses for Audit trail. Stores 1 if record is deleted,else stores 0 or null';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."RID"
IS
  'Uses for Audit trail. Stores sequence generated unique value';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."ALERT_TYPE"
IS
  'Stores reference to er_codelst where codelst_type=alert_type';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."MAIL_ID"
IS
  'Stores mail id where alert to be send';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."ALERT_WORDING"
IS
  'Stores what data should be display as alert';
  COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."FK_ATTACHMENT_ID"
IS
  'Storing attachment id to maintain email alerts';
  COMMENT ON TABLE "ERES"."CB_ALERT_INSTANCES_BACKUP"
Is
  'Backup table of CB_ALERT_INSTANCES';

 COMMENT ON COLUMN "ERES"."CB_ALERT_INSTANCES_BACKUP"."BACKUP_TAKEN_ON"
Is
  'Stores the date on which backup is taken.';

 COMMENT ON COLUMN "ERES"."CB_CORD_FINAL_REVIEW"."FINAL_REVIEW_CONFIRM_FLAG"
Is
  'Stores flag to determine final CBU review is done or not.';

COMMENT ON COLUMN "ERES"."CB_CORD_FINAL_REVIEW"."REVIEW_CORD_ACCEPTABLE_FLAG"
Is
  'Stores flag to wheather cord is acceptable or not.';
  
  COMMENT ON COLUMN "ERES"."CB_CORD_FINAL_REVIEW"."FK_ORDER_ID"
Is
  'Stores reference of ER_Order table.';
  
  COMMENT ON COLUMN "ERES"."CB_ANTIGEN_ENCOD"."VERSION"
Is
  'Stores the version of genomic.';
  
   COMMENT ON COLUMN "ERES"."CB_ANTIGEN_ENCOD"."VALID_IND"
Is
  'Stores wheather genomic is valid or not.';
  
   
   COMMENT ON COLUMN "ERES"."CB_ANTIGEN_ENCOD"."ACTIVE_IND"
Is
  'Stores wheather genomic is active or not.';
  
  COMMENT ON COLUMN "ERES"."CBU_UNIT_REPORT_HLA"."FK_CORD_CDR_CBU_ID"
Is
  'Reference of CB_CORD.';

COMMENT ON COLUMN "ERES"."CBU_UNIT_REPORT_HLA_REVIEW"."FK_CORD_CDR_CBU_ID"
Is
  'Reference of CB_CORD.';

COMMENT ON COLUMN "ERES"."CB_BEST_HLA"."FK_CORD_CDR_CBU_ID"
Is
  'Reference of CB_CORD.';
  
  COMMENT ON COLUMN "ERES"."CB_BEST_HLA"."ENTITY_TYPE"
Is
  'Stores type of entity id.';
  
    COMMENT ON COLUMN "ERES"."CB_BEST_HLA"."FK_HLA_ANTIGENEID2"
Is
  'HLA ANTIGENEID2 - Type 2.';
  
      COMMENT ON COLUMN "ERES"."CB_BEST_HLA"."FK_HLA_ANTIGENEID1"
Is
  'HLA ANTIGENEID1 - Type 1.';

COMMENT ON COLUMN "ERES"."CB_CORD"."IND_OTHER_NAME"
Is
  'Stores the Ind name';
  
  COMMENT ON COLUMN "ERES"."CB_CORD"."IND_OTHER_NUM"
Is
  'Stores the Ind number';
  
    COMMENT ON COLUMN "ERES"."CB_CORD"."FK_FUND_RACE_ROLLUP"
Is
  'Stores funding race roll up. Meant for ESB';
  Comment On Column Cbu_Unit_Report_Review.Cord_Serial Is 'Store serial number of CORD.';
 Comment On Column Cbu_Unit_Report_Review.Cord_Viab_Post_Process Is 'Stores Code for VIAB post process  referenced from  Code List(ER_CODELST).';
 Comment on column CBU_UNIT_REPORT_REVIEW.FCORD_PROC_METHOD IS 'Reference to ER_CODELST to store method.';


Comment On Column Cbu_Unit_Report_Review.Rid
 IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database';
 
 COMMENT ON TABLE "ERES"."AUDIT_ERROR_LOG"
Is
  'This table stores the error log';

 COMMENT ON TABLE "ERES"."CB_APP_MESSAGES"
Is
  'Table to store the application messages';



Comment On Column Cb_Entity_Status.Order_Id Is 'This column stores the reference of ER_order table';

Comment On Column CB_ENTITY_STATUS.ORDER_FK_RESOL Is 'This column stores the resolution';


Comment On Column UI_WIDGET.STATE Is 'This column is used to save the value of state.';
Comment On Column UI_WIDGET.NAME Is 'This column is used to save the value of name.';
Comment On Column UI_WIDGET.WIDGET_DIV_ID Is 'This column is used to save the value of Wiget Div ID.';
Comment On Column UI_WIDGET.HELP_URL Is 'This column is used for Url help';

Comment On Column UI_USR_WID_INSTANCE.PK_USER_WID_INST Is 'Primary Key';
Comment On Column UI_USR_WID_INSTANCE.FK_USER_ID Is 'This column is used for FK User Id';
Comment On Column UI_USR_WID_INSTANCE.HELP_URL Is 'This column is used for HELP URL';



Comment On Column UI_PAGE.HELP_URL Is 'This column is used for HELP URL';
Comment On Column UI_PAGE.SEQ Is 'This column is used for SEQ';


Comment On Column TASK.COMPLETEDON Is 'This column is used to save the complete info';

Comment On Column ER_USERSESSIONLOG.ENTITY_ID Is 'This column is used to store the entity id';
Comment On Column ER_USERSESSIONLOG.ENTITY_IDENTIFIER Is 'This column is used to store the identifier for entity';

Comment On Column ER_USERSESSIONLOG.ENTITY_TYPE Is 'This column is used to store the entity type';
Comment On Column ER_USERSESSIONLOG.ENTITY_EVENT_CODE Is 'This column is used to store the entity event code';

Comment On Column ER_USER.EMAIL Is 'This column is used to store value for email';
Comment On Column ER_USER.DELETED_FLAG Is 'This column is used to store value for deleted flag';
Comment On Column ER_USER.FK_CODELST_THEME Is 'This column is used to store the fk of codelst theme';
Comment On Column ER_USER.FK_CODELST_SKIN Is 'This column is used to store the fk of codelst skin';

Comment On Column ER_SPECIMEN.SPEC_ORIGINAL_QUANTITY Is 'This column is used to store specimen original quantity';


Comment On Column ER_REPXSL.REPXSL_FO Is 'This column is used to store repxsl info';


Comment On Column ER_REPORT.REP_COLUMNS Is 'This column is used to store rep colums value';
Comment On Column ER_REPORT.REP_FILTERBY Is 'This column is used to store rep filter by';

Comment On Column ER_CTRLTAB.CTRL_CUSTOM_COL1 Is 'This column is used to store value of control custom column 1';

Comment On Column ER_ATTACHMENTS.ACTION_FLAG Is 'This column is used to store the action flag';

Comment On Column ER_ATTACHMENTS.DELETEDFLAG Is 'This column is used to store the deleted flag';
Comment On Column ER_ATTACHMENTS.DOCUMENT_FILE Is 'This column is used to store the document file';

Comment On Column ER_ADD.ADD_CREATOR Is 'This column is used to store the creator';
Comment On Column ER_ADD.ADD_CREATOR Is 'This column is used to store the creator';


Comment On Column ER_ACCOUNT.FK_CODELST_SKIN Is 'This column is used to store the fk codelst skin';
Comment On Column ER_ACCOUNT.ACCENDDATE Is 'This column is used to store the Accenddate';

Comment On Column CB_UPLOAD_INFO.RID Is 'This column is used to store the RID';

Comment On Column CB_SHIPMENT.FK_SHIP_COMPANY_ID Is 'This column is used to store the fk of ship company id';
Comment On Column CB_SHIPMENT.SHIPMENT_CONFIRM_ON Is 'This column is used to store the shipment confirm on';

Comment On Column CB_NOTES.TAG_NOTE Is 'This column is used to store the tag on notes';
Comment On Column CB_NOTES.NOTE_SEQ_1 Is 'This column is used to store the note sequence 1';
Comment On Column CB_NOTES.NOTE_SEQ Is 'This column is used to store the note sequence';

Comment On Column CB_HLA_TEMP.FK_HLA_ANTIGENEID2 Is 'This column is used to store the fk hla antigeneid 2';
Comment On Column CB_HLA_TEMP.FK_CORD_CDR_CBU_ID Is 'This column is used to store the fk cord cdr cbu id';
Comment On Column CB_HLA_TEMP.ENTITY_TYPE Is 'This column is used to store the entity type';

Comment On Column CB_HLA.FK_CORD_CDR_CBU_ID Is 'This column is used to store the fk cord cdr cbu id';
Comment On Column CB_HLA.ENTITY_TYPE Is 'This column is used to store the entity type';
Comment On Column CB_HLA.FK_HLA_ANTIGENEID2 Is 'This column is used to store the fk hla antigeneid 2';


Comment On Column CB_GENRATED_REPORT_DETAILS.FK_REPORT_ID Is 'This column is used to store the fk of report ID';
Comment On Column CB_GENRATED_REPORT_DETAILS.TNC Is 'This column is used to store the TNC';


Comment On Column CB_FUNDING_GUIDELINES.FUNDING_ID Is 'This column is used to store the funding ID';

Comment On Column CB_FINAL_DECL_ELIG.IDM_QUES_7 Is 'This column is used to store the IDM QUES 7';
Comment On Column CB_FINAL_DECL_ELIG.PHY_FIND_QUES_5 Is 'This column is used to store the physical findiog for question 5';

--Comment On Column CB_FILTER_DATA.PHY_FIND_QUES_5 Is 'This column is used to store the physical findiog for question 5';

Comment On Column CBB_PROCESSING_PROCEDURES_INFO.NO_OF_EXTR_DNA_ALIQUOTS Is 'This column is used to store the no of extracted dna aliquots';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.NO_OF_CELL_MATER_ALIQUOTS Is 'This column is used to store the no of cell maternal aliquots';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.HUN_PER_GLYCEROL Is 'This column is used to store the hun per glycerol';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.HUN_PER_DMSO Is 'This column is used to store the hun per DMSO';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.FK_PROC_METH_ID Is 'This column is used to store the Fk procedure method ID';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.FIVE_UNIT_PER_ML_HEPARIN Is 'This column is used to store the Five unit per heparin';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.FIVE_PER_HUMAN_ALBU Is 'This column is used to store the Five per human albu';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.FIVE_PER_DEXTROSE Is 'This column is used to store the Five per dextrose';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.FILTER_PAPER Is 'This column is used to store the value for Filter paper';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.CPDA Is 'This column is used to store the value for CPDA';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.CPD Is 'This column is used to store the value for CPD';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.ACD Is 'This column is used to store the value for ACD';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.TWEN_FIVE_HUM_ALBU Is 'This column is used to store the value for Five Hum Albu';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.TOT_MATER_ALIQUOTS Is 'This column is used to store the value for total maternal aliquots';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.TOT_CBU_ALIQUOTS Is 'This column is used to store the value for total cbu aliquots';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.THOU_UNIT_PER_ML_HEPARIN Is 'This column is used to store the value for thou unit per helparin';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.TEN_UNIT_PER_ML_HEPARIN Is 'This column is used to store the value for ten unit per helparin';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.TEN_PER_DEXTRAN_40 Is 'This column is used to store the value for ten per dextran 40';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.SPECIFY_OTH_ANTI Is 'This column is used to store the value for specify other anti';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.SPEC_OTH_DILUENTS Is 'This column is used to store the value for specify other diluents';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.SPEC_OTH_CRYOPRO Is 'This column is used to store the value for specify other cryopro';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.SOP_TITLE Is 'This column is used to store the value for sop title';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.SOP_TERMI_DATE Is 'This column is used to store the value for sop termination date';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.SOP_STRT_DATE Is 'This column is used to store the value for sop start date';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.SOP_REF_NO Is 'This column is used to store the value for sop reference number';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.SIX_PER_HYDROXYETHYL_STARCH Is 'This column is used to store the value for six per hydroxyethyl starch';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.RBC_PALLETS Is 'This column is used to store the value for RBC Pallets';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.POINNT_NINE_PER_NACL Is 'This column is used to store the value for point nine per nacl';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.PLASMALYTE Is 'This column is used to store the value for PLASMALYTEl';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.OTHER_PROD_MODI Is 'This column is used to store the value for other prod modi';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.OTHER_PROCESSING Is 'This column is used to store the value for other processing';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.OTHER_FREEZ_MANUFAC Is 'This column is used to store the value for other freeze manufacturer';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.OTHER_FROZEN_CONT Is 'This column is used to store the value for other frozen cont';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.OTHER_ANTICOAGULANT Is 'This column is used to store the value for other anticoagulant';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.OTH_DILUENTS Is 'This column is used to store the value for other diluents';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.OTH_CRYOPROTECTANT Is 'This column is used to store the value for other cryoprotectant';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.NO_OF_VIABLE_CELL_ALIQUOTS Is 'This column is used to store the value for number of viable cell aliquots';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.NO_OF_SERUM_MATER_ALIQUOTS Is 'This column is used to store the value for number of serum mater aliquots';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.NO_OF_SERUM_ALIQUOTS Is 'This column is used to store the value for number of serum aliquots';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.NO_OF_SEGMENTS Is 'This column is used to store the value for number of segments';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.NO_OF_PLASMA_MATER_ALIQUOTS Is 'This column is used to store the value for number of plasma maternal aliquots';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.NO_OF_PLASMA_ALIQUOTS Is 'This column is used to store the value for number of plasma aliquots';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.NO_OF_NONVIABLE_ALIQUOTS Is 'This column is used to store the value for number of non viable aliquots';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.NO_OF_INDI_FRAC Is 'This column is used to store the value for number of indi frac';
Comment On Column CBB_PROCESSING_PROCEDURES_INFO.NO_OF_EXTR_DNA_MATER_ALIQUOTS Is 'This column is used to store the no of extracted dna maternal aliquots';

Comment On Column CBB.MEMBER Is 'This column is used to store the cbb member';
Comment On Column CBB.MICRO_CONTAM_DATE Is 'This column is used to store the micro contam date';
Comment On Column CBB.CBB_ID Is 'This column is used to store the CBB ID';
Comment On Column CBB.NMD_PRESEARCH_FLAG Is 'This column is used to store the Nmdp Presearch Flag';
Comment On Column CBB.LINKS_HOVER_FLAG Is 'This column is used to store the links hover flag';


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,277,5,'05_COMMENTS.sql',sysdate,'9.0.0 B#637-ET051');
commit;