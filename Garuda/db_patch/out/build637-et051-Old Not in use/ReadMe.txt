/* This readMe is specific to Velos eResearch version 9.0 build637-et051 */

=====================================================================================================================================
EmTrax :-
 1.Note - Since this build is meant to be deployed over a new version of JBoss so that all additional configuration files are being released. 
 		  Please make the necessary configuration settings. 
 
 2.With this release we are upgrading JBoss server from Version 5.1(GA) to 5.1.2(EAP).
 	For this we are providing  two configuration document with the release to setup 5.1.2(EAP)-
 	
 		A.JBoss_Upgrade_Guide_From_5(GA)_To_5(EAP).doc - 
 		B.Additional Configurations for EmTrax.doc - This document will guide in setting up additional configurations  specific to EmTrax.
 		
 		Note :-If this build is installed on a fresh machine you need to setup configuration file accordingly otherwise please remove dateformatSetting.js,
 		       velosConfigCustom.js and dcmsAttachment.xml from the respective location e.g.-
 		       
 		       server\eresearch\deploy\velos.ear\velos.war\jsp\xml\dcmsAttachment.xml
 		       server\eresearch\deploy\velos.ear\velos.war\jsp\js\velos\velosConfigCustom.js
 		       server\eresearch\deploy\velos.ear\velos.war\jsp\js\velos\dateformatSetting.js
 		       server\eresearch\deploy\vda-ds.xml
 		       server\eresearch\conf\gems_configuration.properties
 		       server\eresearch\conf\aithentmail_garuda.properties
 		       server\eresearch\deploy\velos.ear\velos-services.sar\META-INF\jboss-service.xml
  		       
=====================================================================================================================================

 