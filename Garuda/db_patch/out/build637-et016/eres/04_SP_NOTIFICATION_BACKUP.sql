create or replace
PROCEDURE SP_NOTIFICATION_BACKUP AS
BEGIN
INSERT INTO ER_NOTIFICATION_BACKUP SELECT N.*,sysdate FROM ER_NOTIFICATION N WHERE NOTIFY_ISSENT=1;
DELETE FROM ER_NOTIFICATION WHERE NOTIFY_ISSENT=1;
COMMIT;
EXCEPTION
WHEN others THEN
dbms_output.put_line ('Exception in SP_NOTIFICATION_BACKUP Procedure');
END;
/


begin
DBMS_SCHEDULER.create_job (
    job_name        => 'ER_NOTIFICATION_BACKUP_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN SP_NOTIFICATION_BACKUP;END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'FREQ=WEEKLY;BYDAY=SUN;BYHOUR=00;BYMINUTE=00;BYSECOND=1',
    end_date        => NULL,
    enabled         => TRUE,
    comments        => 'Job to take backup of ER_NOTIFICATION TABLE.');
end;
/

begin
dbms_scheduler.run_job('ER_NOTIFICATION_BACKUP_JOB');
end;
/
INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,209,4,'04_SP_NOTIFICATION_BACKUP.sql',sysdate,'9.0.0 B#637-ET016');
		commit;