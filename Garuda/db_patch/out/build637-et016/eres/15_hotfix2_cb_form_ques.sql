--start update cb_form_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set RESPONSE_VALUE = 'idm_res8' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cmv_igg_sts') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2B');
commit;
  end if;
end;
/
--end--
--FOR Question 6 in N2 Version--

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2') and fk_question = (select pk_questions from cb_questions where ques_code='hiv_antigen_rslt');
  if (v_record_exists = 1) then
	update cb_form_questions set response_value='idm_res3'  where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2') and fk_question = (select pk_questions from cb_questions where ques_code='hiv_antigen_rslt');
commit;
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,209,15,'15_hotfix2_cb_form_ques.sql',sysdate,'9.0.0 B#637-ET016.02');
commit;

		
