CREATE OR REPLACE TRIGGER "ERES"."CB_SHIPMENT_AU1"
AFTER UPDATE ON ERES.CB_SHIPMENT
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
	NEW_FK_SHIPMENT_TYPE VARCHAR(200);
	OLD_FK_SHIPMENT_TYPE  VARCHAR(200);
  NEW_PAPERWORKLOC VARCHAR(200);
  OLD_PAPERWORKLOC VARCHAR(200);
  NEW_SHIPCOMPANY VARCHAR(200);
  OLD_SHIPCOMPANY VARCHAR(200);
  NEW_TEMPMONITOR VARCHAR(200);
  OLD_TEMPMONITOR VARCHAR(200);
  NEW_SHIP_CONTAINER VARCHAR2(500);
  OLD_SHIP_CONTAINER VARCHAR2(500);
  NEW_CREATOR VARCHAR2(200);
  OLD_CREATOR VARCHAR2(200);
  NEW_LAST_MODIFIED_BY VARCHAR2(200);
  OLD_LAST_MODIFIED_BY VARCHAR2(200);
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;


	IF NVL(:OLD.FK_SHIPMENT_TYPE,0) !=     NVL(:NEW.FK_SHIPMENT_TYPE,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_SHIPMENT_TYPE)		INTO NEW_FK_SHIPMENT_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_FK_SHIPMENT_TYPE := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_SHIPMENT_TYPE)		INTO OLD_FK_SHIPMENT_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_FK_SHIPMENT_TYPE  := '';
   END;
  END IF;
  
 IF NVL(:OLD.CREATOR,0) !=  NVL(:NEW.CREATOR,0) THEN
    BEGIN
           SELECT F_GETUSER(:NEW.CREATOR) INTO NEW_CREATOR from dual;
   EXCEPTION WHEN OTHERS THEN
                NEW_CREATOR := '';
   END;
   BEGIN
           SELECT F_GETUSER(:OLD.CREATOR) INTO OLD_CREATOR from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_CREATOR := '';
   END;
   END IF;
   IF NVL(:OLD.LAST_MODIFIED_BY,0) !=  NVL(:NEW.LAST_MODIFIED_BY,0) THEN
    BEGIN
            SELECT F_GETUSER(:NEW.LAST_MODIFIED_BY) INTO NEW_LAST_MODIFIED_BY from dual;
   EXCEPTION WHEN OTHERS THEN
                NEW_LAST_MODIFIED_BY := '';
   END;
   BEGIN
           SELECT F_GETUSER(:OLD.LAST_MODIFIED_BY) INTO OLD_LAST_MODIFIED_BY from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_LAST_MODIFIED_BY := '';
   END;
   END IF;

  IF NVL(:OLD.PAPERWORK_LOCATION,0) !=     NVL(:NEW.PAPERWORK_LOCATION,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.PAPERWORK_LOCATION)		INTO NEW_PAPERWORKLOC  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_PAPERWORKLOC := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:OLD.PAPERWORK_LOCATION)		INTO OLD_PAPERWORKLOC  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_PAPERWORKLOC  := '';
   END;
  END IF;

  IF NVL(:OLD.FK_SHIP_COMPANY_ID,0) !=     NVL(:NEW.FK_SHIP_COMPANY_ID,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_SHIP_COMPANY_ID)		INTO NEW_SHIPCOMPANY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_SHIPCOMPANY := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_SHIP_COMPANY_ID)		INTO OLD_SHIPCOMPANY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_SHIPCOMPANY  := '';
   END;
  END IF;

   IF NVL(:OLD.FK_TEMPERATURE_MONITER,0) !=     NVL(:NEW.FK_TEMPERATURE_MONITER,0) THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.FK_TEMPERATURE_MONITER)		INTO NEW_TEMPMONITOR  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_TEMPMONITOR := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_TEMPERATURE_MONITER)		INTO OLD_TEMPMONITOR  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_TEMPMONITOR  := '';
   END;
  END IF;
  
  
  IF NVL(:OLD.FK_SHIPPING_CONT,0) != NVL(:NEW.FK_SHIPPING_CONT,0) THEN
    BEGIN
   		SELECT F_Codelst_Desc(:new.FK_SHIPPING_CONT)		INTO NEW_SHIP_CONTAINER  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_SHIP_CONTAINER := '';
   END;
   BEGIN
   		SELECT F_Codelst_Desc(:OLD.FK_SHIPPING_CONT)		INTO OLD_SHIP_CONTAINER  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 OLD_SHIP_CONTAINER  := '';
   END;
	END IF;



	IF :NEW.DELETEDFLAG = 'Y' THEN

	PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'CB_SHIPMENT',:OLD.rid,:OLD.PK_SHIPMENT,'D',:NEW.LAST_MODIFIED_BY);

	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PK_SHIPMENT',:OLD.PK_SHIPMENT,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_ORDER_ID',:OLD.FK_ORDER_ID,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_DATE', to_char(:OLD.SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDI_SHIPMENT_DATE', to_char(:OLD.ADDI_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_TRACKING_NO',:OLD.SHIPMENT_TRACKING_NO,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDI_SHIPMENT_TRACKING_NO',:OLD.ADDI_SHIPMENT_TRACKING_NO,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','CBB_ID',:OLD.CBB_ID,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','CREATOR',OLD_CREATOR,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','CREATED_ON', to_char(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','IP_ADD',:OLD.IP_ADD,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','LAST_MODIFIED_BY',OLD_LAST_MODIFIED_BY,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','LAST_MODIFIED_DATE', to_char(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','RID',:OLD.RID,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','DELETEDFLAG',:OLD.DELETEDFLAG,NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_CORD_ID',:OLD.FK_CORD_ID,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PROP_SHIPMENT_DATE', to_char(:OLD.PROP_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ATTN_NAME',:OLD.ATTN_NAME,NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_DELIVERY_ADD',:OLD.FK_DELIVERY_ADD,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDI_INFO',:OLD.ADDI_INFO,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SCH_SHIPMENT_DATE', to_char(:OLD.SCH_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_SHIPPING_CONT',OLD_SHIP_CONTAINER,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_TEMPERATURE_MONITER',OLD_TEMPMONITOR,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','EXPECT_ARR_DATE', to_char(:OLD.EXPECT_ARR_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_CONFIRM',:OLD.SHIPMENT_CONFIRM,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_CONFIRM_BY',:OLD.SHIPMENT_CONFIRM_BY,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_CONFIRM_ON', to_char(:OLD.SHIPMENT_CONFIRM_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_SHIP_COMPANY_ID',OLD_SHIPCOMPANY,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PADLOCK_COMBINATION',:OLD.PADLOCK_COMBINATION,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PAPERWORK_LOCATION',OLD_PAPERWORKLOC,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDITI_SHIPPER_DET',:OLD.ADDITI_SHIPPER_DET,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_SHIPMENT_TYPE',OLD_FK_SHIPMENT_TYPE,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','NMDP_SHIP_EXCUSE_FLAG',:OLD.NMDP_SHIP_EXCUSE_FLAG,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','DATA_MODIFIED_FLAG',:OLD.DATA_MODIFIED_FLAG,NULL,NULL,NULL);
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','NMDP_SHIP_EXCUSE_SUBMITTED_BY',:OLD.NMDP_SHIP_EXCUSE_SUBMITTED_BY,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','NMDP_SHIP_EXCUSE_SUBMITTED_ON',:OLD.NMDP_SHIP_EXCUSE_SUBMITTED_ON,NULL,NULL,NULL);
	ELSE

	PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'CB_SHIPMENT',:OLD.RID,:OLD.PK_SHIPMENT,'U',:NEW.LAST_MODIFIED_BY);

	IF NVL(:OLD.PK_SHIPMENT,0) != NVL(:NEW.PK_SHIPMENT,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PK_SHIPMENT', :OLD.PK_SHIPMENT, :NEW.PK_SHIPMENT,NULL,NULL);
  END IF;
	IF NVL(:OLD.FK_ORDER_ID,0) != NVL(:NEW.FK_ORDER_ID,0) THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_ORDER_ID',:OLD.FK_ORDER_ID, :NEW.FK_ORDER_ID,NULL,NULL);
  END IF;
  IF NVL(:OLD.SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_DATE',
       TO_CHAR(:OLD.SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
 IF NVL(:OLD.ADDI_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.ADDI_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDI_SHIPMENT_DATE',
       TO_CHAR(:OLD.ADDI_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.ADDI_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
  IF NVL(:OLD.SHIPMENT_TRACKING_NO,'') != NVL(:NEW.SHIPMENT_TRACKING_NO,'') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_TRACKING_NO',:OLD.SHIPMENT_TRACKING_NO, :NEW.SHIPMENT_TRACKING_NO,NULL,NULL);
  END IF;
   IF NVL(:OLD.ADDI_SHIPMENT_TRACKING_NO,'') != NVL(:NEW.ADDI_SHIPMENT_TRACKING_NO,'') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDI_SHIPMENT_TRACKING_NO',:OLD.ADDI_SHIPMENT_TRACKING_NO, :NEW.ADDI_SHIPMENT_TRACKING_NO,NULL,NULL);
  END IF;
   IF NVL(:OLD.CBB_ID,0) != NVL(:NEW.CBB_ID,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','CBB_ID',:OLD.CBB_ID, :NEW.CBB_ID,NULL,NULL);
  END IF;
  IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT', 'CREATOR',OLD_CREATOR, NEW_CREATOR,NULL,NULL);
  END IF;
  IF NVL(:OLD.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
  IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
  END IF;
	IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','LAST_MODIFIED_BY', OLD_LAST_MODIFIED_BY, NEW_LAST_MODIFIED_BY,NULL,NULL);
	END IF;
	IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT', 'RID', :OLD.RID, :NEW.RID,NULL,NULL);
    END IF;
	IF NVL(:OLD.DELETEDFLAG,' ') != NVL(:NEW.DELETEDFLAG,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','DELETEDFLAG', :OLD.DELETEDFLAG, :NEW.DELETEDFLAG,NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_CORD_ID,0) != NVL(:NEW.FK_CORD_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_CORD_ID', :OLD.FK_CORD_ID, :NEW.FK_CORD_ID,NULL,NULL);
    END IF;
    IF NVL(:OLD.PROP_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.PROP_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PROP_SHIPMENT_DATE',
       TO_CHAR(:OLD.PROP_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.PROP_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.ATTN_NAME,' ') != NVL(:NEW.ATTN_NAME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ATTN_NAME', :OLD.ATTN_NAME, :NEW.ATTN_NAME,NULL,NULL);
	  END IF;
    IF NVL(:OLD.FK_DELIVERY_ADD,0) != NVL(:NEW.FK_DELIVERY_ADD,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_SHIPMENT','FK_DELIVERY_ADD', :OLD.FK_DELIVERY_ADD, :NEW.FK_DELIVERY_ADD,NULL,NULL);
    END IF;
    IF NVL(:OLD.ADDI_INFO,' ') != NVL(:NEW.ADDI_INFO,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDI_INFO', :OLD.ADDI_INFO, :NEW.ADDI_INFO,NULL,NULL);
	  END IF;
     IF NVL(:OLD.SCH_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.SCH_SHIPMENT_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SCH_SHIPMENT_DATE',
       TO_CHAR(:OLD.SCH_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.SCH_SHIPMENT_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:OLD.FK_SHIPPING_CONT,0) != NVL(:NEW.FK_SHIPPING_CONT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_SHIPPING_CONT', OLD_SHIP_CONTAINER, NEW_SHIP_CONTAINER,NULL,NULL);
	  END IF;
	   IF NVL(:OLD.FK_TEMPERATURE_MONITER,0) != NVL(:NEW.FK_TEMPERATURE_MONITER,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_TEMPERATURE_MONITER', OLD_TEMPMONITOR, NEW_TEMPMONITOR,NULL,NULL);
	  END IF;
	  IF NVL(:OLD.EXPECT_ARR_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.EXPECT_ARR_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','EXPECT_ARR_DATE',
       TO_CHAR(:OLD.EXPECT_ARR_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.EXPECT_ARR_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
	IF NVL(:OLD.SHIPMENT_CONFIRM,' ') != NVL(:NEW.SHIPMENT_CONFIRM,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_CONFIRM', :OLD.SHIPMENT_CONFIRM, :NEW.SHIPMENT_CONFIRM,NULL,NULL);
	  END IF;
	 IF NVL(:OLD.SHIPMENT_CONFIRM_BY,0) != NVL(:NEW.SHIPMENT_CONFIRM_BY,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_CONFIRM_BY', :OLD.SHIPMENT_CONFIRM_BY, :NEW.SHIPMENT_CONFIRM_BY,NULL,NULL);
	  END IF;
	IF NVL(:OLD.SHIPMENT_CONFIRM_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.SHIPMENT_CONFIRM_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SHIPMENT_CONFIRM_ON',
       TO_CHAR(:OLD.SHIPMENT_CONFIRM_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.SHIPMENT_CONFIRM_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
	IF NVL(:OLD.FK_SHIP_COMPANY_ID,0) != NVL(:NEW.FK_SHIP_COMPANY_ID,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_SHIP_COMPANY_ID', OLD_SHIPCOMPANY, NEW_SHIPCOMPANY,NULL,NULL);
	  END IF;
	IF NVL(:OLD.PADLOCK_COMBINATION,' ') != NVL(:NEW.PADLOCK_COMBINATION,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PADLOCK_COMBINATION', :OLD.PADLOCK_COMBINATION, :NEW.PADLOCK_COMBINATION,NULL,NULL);
	  END IF;
	IF NVL(:OLD.PAPERWORK_LOCATION,0) != NVL(:NEW.PAPERWORK_LOCATION,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','PAPERWORK_LOCATION', OLD_PAPERWORKLOC, NEW_PAPERWORKLOC,NULL,NULL);
	  END IF;
	IF NVL(:OLD.ADDITI_SHIPPER_DET,' ') != NVL(:NEW.ADDITI_SHIPPER_DET,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','ADDITI_SHIPPER_DET', :OLD.ADDITI_SHIPPER_DET, :NEW.ADDITI_SHIPPER_DET,NULL,NULL);
	  END IF;
    IF NVL(:OLD.FK_SHIPMENT_TYPE,0) != NVL(:NEW.FK_SHIPMENT_TYPE,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','FK_SHIPMENT_TYPE',OLD_FK_SHIPMENT_TYPE,NEW_FK_SHIPMENT_TYPE,NULL,NULL);
	  END IF;
    IF NVL(:OLD.NMDP_SHIP_EXCUSE_FLAG,' ') != NVL(:NEW.NMDP_SHIP_EXCUSE_FLAG,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','NMDP_SHIP_EXCUSE_FLAG', :OLD.NMDP_SHIP_EXCUSE_FLAG, :NEW.NMDP_SHIP_EXCUSE_FLAG,NULL,NULL);
	  END IF;
    IF NVL(:OLD.DATA_MODIFIED_FLAG,' ') != NVL(:NEW.DATA_MODIFIED_FLAG,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','DATA_MODIFIED_FLAG', :OLD.DATA_MODIFIED_FLAG, :NEW.DATA_MODIFIED_FLAG,NULL,NULL);
	  END IF;
    IF NVL(:OLD.SCH_SHIPMENT_TIME,' ') != NVL(:NEW.SCH_SHIPMENT_TIME,' ') THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','SCH_SHIPMENT_TIME', :OLD.SCH_SHIPMENT_TIME, :NEW.SCH_SHIPMENT_TIME,NULL,NULL);
	  END IF;
    IF NVL(:OLD.NMDP_SHIP_EXCUSE_SUBMITTED_BY,0) != NVL(:NEW.NMDP_SHIP_EXCUSE_SUBMITTED_BY,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_SHIPMENT','NMDP_SHIP_EXCUSE_SUBMITTED_BY', :OLD.NMDP_SHIP_EXCUSE_SUBMITTED_BY, :NEW.NMDP_SHIP_EXCUSE_SUBMITTED_BY,NULL,NULL);
    END IF;
    IF NVL(:OLD.NMDP_SHIP_EXCUSE_SUBMITTED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.NMDP_SHIP_EXCUSE_SUBMITTED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_SHIPMENT','NMDP_SHIP_EXCUSE_SUBMITTED_ON',
       TO_CHAR(:OLD.NMDP_SHIP_EXCUSE_SUBMITTED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.NMDP_SHIP_EXCUSE_SUBMITTED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
  END IF;
END;
/

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,249,35,'35_CB_SHIPMENT_AU1.sql',sysdate,'9.0.0 B#637-ET037');
commit;