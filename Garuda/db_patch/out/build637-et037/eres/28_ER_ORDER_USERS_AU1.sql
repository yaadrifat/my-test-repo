CREATE OR REPLACE TRIGGER "ERES"."ER_ORDER_USERS_AU1" AFTER UPDATE ON eres.ER_ORDER_USERS 
referencing OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE 
    v_rowid NUMBER(10);/*variable used to fetch value of sequence */
    NEW_LAST_MODIFIED_BY VARCHAR2(200);
  OLD_LAST_MODIFIED_BY VARCHAR2(200);
  NEW_CREATOR VARCHAR2(200);
  OLD_CREATOR VARCHAR2(200);
BEGIN
    SELECT seq_audit_row_module.nextval INTO v_rowid FROM dual;
    IF NVL(:OLD.CREATOR,0) !=  NVL(:NEW.CREATOR,0) THEN
    BEGIN
           SELECT F_GETUSER(:NEW.CREATOR) INTO NEW_CREATOR from dual;
   EXCEPTION WHEN OTHERS THEN
                NEW_CREATOR := '';
   END;
   BEGIN
           SELECT F_GETUSER(:OLD.CREATOR) INTO OLD_CREATOR from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_CREATOR := '';
   END;
   END IF;
   IF NVL(:OLD.LAST_MODIFIED_BY,0) !=  NVL(:NEW.LAST_MODIFIED_BY,0) THEN
    BEGIN
            SELECT F_GETUSER(:NEW.LAST_MODIFIED_BY) INTO NEW_LAST_MODIFIED_BY from dual;
   EXCEPTION WHEN OTHERS THEN
                NEW_LAST_MODIFIED_BY := '';
   END;
   BEGIN
           SELECT F_GETUSER(:OLD.LAST_MODIFIED_BY) INTO OLD_LAST_MODIFIED_BY from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_LAST_MODIFIED_BY := '';
   END;
   END IF;
    pkg_audit_trail_module.sp_row_insert (v_rowid,'ER_ORDER_USERS',:OLD.rid,:OLD.PK_ORDER_USER,'U',:NEW.LAST_MODIFIED_BY);
    
     IF NVL(:OLD.PK_ORDER_USER,0) != NVL(:NEW.PK_ORDER_USER,0) THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'PK_ORDER_USER',:OLD.PK_ORDER_USER, :NEW.PK_ORDER_USER,NULL,NULL);
  END IF;
 IF NVL(:OLD.USER_LOGINID,' ') != NVL(:NEW.USER_LOGINID,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'USER_LOGINID',:OLD.USER_LOGINID, :NEW.USER_LOGINID,NULL,NULL);
  END IF; 
  IF NVL(:OLD.USER_FNAME,' ') != NVL(:NEW.USER_FNAME,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'USER_FNAME',:OLD.USER_FNAME, :NEW.USER_FNAME,NULL,NULL);
  END IF; 
  IF NVL(:OLD.USER_LNAME,' ') != NVL(:NEW.USER_LNAME,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'USER_LNAME',:OLD.USER_LNAME, :NEW.USER_LNAME,NULL,NULL);
  END IF;
  IF NVL(:OLD.USER_MAILID,' ') != NVL(:NEW.USER_MAILID,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'USER_MAILID',:OLD.USER_MAILID, :NEW.USER_MAILID,NULL,NULL);
  END IF;
  IF NVL(:OLD.USER_CONTACTNO,' ') != NVL(:NEW.USER_CONTACTNO,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'USER_CONTACTNO',:OLD.USER_CONTACTNO, :NEW.USER_CONTACTNO,NULL,NULL);
  END IF;
  IF NVL(:OLD.USER_COMMENTS,' ') != NVL(:NEW.USER_COMMENTS,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'USER_COMMENTS',:OLD.USER_COMMENTS, :NEW.USER_COMMENTS,NULL,NULL);
  END IF;
   IF NVL(:OLD.CREATOR,0) != NVL(:NEW.CREATOR,0) THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'CREATOR',OLD_CREATOR, NEW_CREATOR,NULL,NULL);
  END IF;  
   IF NVL(:OLD.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=  NVL(:NEW.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS', 'CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
  IF NVL(:OLD.IP_ADD,' ') != NVL(:NEW.IP_ADD,' ') THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);     
    END IF;
    IF nvl(:OLD.LAST_MODIFIED_BY,0) != nvl(:NEW.LAST_MODIFIED_BY,0) THEN
        pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS','LAST_MODIFIED_BY',OLD_LAST_MODIFIED_BY,NEW_LAST_MODIFIED_BY,NULL,NULL);
      END IF;       
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS', 'LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
    IF NVL(:OLD.DELETEDFLAG,0) != NVL(:NEW.DELETEDFLAG,0) THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'DELETEDFLAG',:OLD.DELETEDFLAG, :NEW.DELETEDFLAG,NULL,NULL);
    END IF;
    IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
      pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_USERS',  'RID',:OLD.RID, :NEW.RID,NULL,NULL);     
    END IF;
  END;

/


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,249,28,'28_ER_ORDER_USERS_AU1.sql',sysdate,'9.0.0 B#637-ET037');
commit;