set define off;

UPDATE CB_QUESTIONS SET QUES_DESC='In the past 12 weeks, have you had contact with someone who has received the smallpox vaccine?<br></br>(Examples of contact include physical intimacy, touching the vaccination site, touching
the bandages or covering of the vaccination site, or handling bedding or clothing that
had been in contact with the unbandaged vaccination site)' WHERE QUES_CODE='cntc_smallpox_vaccine_12wk_ind';
commit;
/



INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,249,37,'37_UPDATE_CB_QUESTIONS.sql',sysdate,'9.0.0 B#637-ET037');
commit;