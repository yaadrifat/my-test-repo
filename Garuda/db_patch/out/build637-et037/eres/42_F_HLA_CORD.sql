create or replace
FUNCTION F_HLA_CORD(pk_cord number, hla_typ NUMBER) return SYS_REFCURSOR
IS
v_entity_cord number;
p_hla_refcur SYS_REFCURSOR;
v_entity_patient NUMBER:=0;
v_patient_hla_type NUMBER;
v_patient_count NUMBER:=0;
p_entity_id NUMBER:=0;
p_entity_type NUMBER;
p_source_type NUMBER;
BEGIN
select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';
select pk_codelst into v_entity_patient from er_codelst where codelst_type='entity_type' and codelst_subtyp='PATIENT';
select pk_codelst into v_patient_hla_type from er_codelst where codelst_type='test_source' and codelst_subtyp='patient';


IF hla_typ=v_patient_hla_type THEN
SELECT COUNT(*) INTO V_PATIENT_COUNT FROM ER_ORDER_RECEIPANT_INFO INFOVAL WHERE INFOVAL.FK_ORDER_ID=(SELECT MAX(ORD.PK_ORDER) FROM ER_ORDER_HEADER HDR LEFT OUTER JOIN ER_ORDER ORD ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER) WHERE HDR.ORDER_ENTITYID=pk_cord);
IF V_PATIENT_COUNT>0 THEN
SELECT NVL(INFOVAL.FK_RECEIPANT,0) INTO P_ENTITY_ID FROM ER_ORDER_RECEIPANT_INFO INFOVAL WHERE INFOVAL.FK_ORDER_ID=(SELECT MAX(ORD.PK_ORDER) FROM ER_ORDER_HEADER HDR LEFT OUTER JOIN ER_ORDER ORD ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER) WHERE HDR.ORDER_ENTITYID=pk_cord);
END IF;
p_entity_type:=v_entity_patient;
END IF;

IF hla_typ!=v_patient_hla_type THEN
P_ENTITY_ID:=PK_CORD;
p_entity_type:=v_entity_cord;
END IF;

OPEN p_hla_refcur FOR     SELECT
                            h.fk_hla_code_id AS PKLOCUS,
                            nvl(ec1.genomic_format,' ') AS TYPE1,
                            nvl(ec2.genomic_format,' ') AS TYPE2,
                            to_char(h.hla_received_date,'Mon DD, YYYY') AS ENTRYDATE,
                            h.CB_HLA_ORDER_SEQ seqvalhla,
                            f_getuser(h.creator) usrname
                          FROM CB_HLA h ,
                            cb_antigen_encod ec1,
                            cb_antigen_encod ec2
                          WHERE h.fk_hla_antigeneid1=ec1.fk_antigen (+)
                          AND ( ec1.version         =
                            (SELECT MAX(ec1s.version)
                            FROM cb_antigen_encod ec1s
                            WHERE ec1.fk_antigen=ec1s.fk_antigen
                            )
                          OR ec1.version IS NULL)
                          AND h.fk_hla_antigeneid2 = ec2.fk_antigen (+)
                          AND (ec2.version         =
                            (SELECT MAX(ec2s.version)
                            FROM cb_antigen_encod ec2s
                            WHERE ec2.fk_antigen=ec2s.fk_antigen
                            )
                          OR ec2.version  IS NULL) AND h.ENTITY_ID  =P_ENTITY_ID AND h.ENTITY_TYPE=p_entity_type AND H.FK_SOURCE=hla_typ ORDER BY H.HLA_RECEIVED_DATE desc,h.HLA_TYPING_DATE desc;
RETURN p_hla_refcur;
END;
/


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,249,42,'42_F_HLA_CORD.sql',sysdate,'9.0.0 B#637-ET037');
commit;