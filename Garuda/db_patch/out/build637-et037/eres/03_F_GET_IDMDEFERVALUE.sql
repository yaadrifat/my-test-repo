create or replace function f_get_idmdefervalue(pk_cord number) return number as
CURSOR idmFormdata IS
     SELECT pk_questions,
  ques_desc,
  forq.fk_master_ques,
  forq.fk_dependent_ques,
  forq.fk_dependent_ques_value,
  formres.pk_form_responses,
  formres.fk_question,
  formres.resp_code,
  ques.ques_code,  
  forq.ques_seq,
  forq.defer_value
FROM cb_form_questions forq,
  cb_questions ques
LEFT OUTER JOIN
  (SELECT frmres.pk_form_responses,
    frmres.fk_question,
    frmres.response_code resp_code,
    frmres.response_value resp_val,
    frmres.comments,
    frmres.fk_form_responses,
    TO_CHAR(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate
  FROM  cb_form_responses frmres
  WHERE frmres.entity_id =pk_cord
  AND frmres.fk_form_version = (select max(pk_form_version) from cb_form_version where entity_id=pk_cord and fk_form=(SELECT pk_form FROM cb_forms WHERE forms_desc  ='IDM' AND is_current_flag=1))
  ) formres ON(formres.FK_QUESTION=ques.pk_questions)
WHERE ques.pk_questions = forq.fk_question
AND forq.fk_form =(SELECT pk_form FROM cb_forms WHERE forms_desc  ='IDM' AND is_current_flag=1)
ORDER BY to_number(regexp_substr(forq.ques_seq,'^[0-9]+')), forq.ques_seq, ques.pk_questions, formres.pk_form_responses;
v_ques6 VARCHAR2(200);
v_ques7 VARCHAR2(200);
v_ques10 VARCHAR2(200);
v_ques11 VARCHAR2(200);
v_ques12 VARCHAR2(200);
v_ques15 VARCHAR2(200);
v_ques18 VARCHAR2(200);
v_defer_flag NUMBER:=1;
v_notdone VARCHAR2(200); 
v_notdone1 VARCHAR2(200); 
v_return number:=0;
v_defer_for VARCHAR2(2000);
v_defer_val1 VARCHAR2(200);
v_defer_val2 VARCHAR2(200);
v_defer_val3 VARCHAR2(200);
v_defer_temp VARCHAR2(200);
v_posipk VARCHAR2(200); 
begin
SELECT pk_codelst INTO v_posipk FROM ER_CODELST WHERE CODELST_TYPE='test_outcome1' and codelst_subtyp='posi';
SELECT pk_codelst INTO v_notdone FROM ER_CODELST WHERE CODELST_TYPE='test_outcome1' and codelst_subtyp='not_done';
SELECT pk_codelst INTO v_notdone1 FROM ER_CODELST WHERE CODELST_TYPE='test_outcome' and codelst_subtyp='not_done';
for formdata in idmFormdata
loop
    IF formdata.ques_code='anti_hiv_1_2_o_ind' THEN
      v_ques6 :=formdata.resp_code;
    END IF;
    IF formdata.ques_code='anti_hiv_1_2_ind' THEN
      v_ques7 :=formdata.resp_code;
    END IF;
    IF formdata.ques_code='nat_hiv_1_hcv_hbv_mpx_ind' THEN
      v_ques10 :=formdata.resp_code;
    END IF;
    IF formdata.ques_code='nat_hiv_react_ind' THEN
      v_ques11 :=formdata.resp_code;
    END IF;
    IF formdata.ques_code='hiv_antigen_rslt' THEN
      v_ques12 :=formdata.resp_code;
    END IF;
    IF formdata.ques_code='syphilis_react_ind' THEN
      v_ques15 :=formdata.resp_code;
    END IF;
    IF formdata.ques_code='syphilis_ct_sup_react_ind' THEN
      v_ques18 :=formdata.resp_code;
    END IF;
end loop;
for formdata in idmFormdata
loop
    v_defer_flag := 1;
    IF formdata.defer_value IS NOT NULL AND formdata.resp_code IS NOT NULL THEN
      IF formdata.resp_code = v_notdone OR formdata.resp_code = v_notdone1 OR formdata.resp_code = v_posipk THEN
        IF formdata.ques_code='anti_hiv_1_2_o_ind' OR formdata.ques_code='anti_hiv_1_2_ind' THEN
          IF v_ques6 = v_notdone AND v_ques7 = v_notdone THEN
            v_defer_flag := 1;
          ELSE
            v_defer_flag := 0;
          END IF;
        END IF;
        IF formdata.ques_code='nat_hiv_1_hcv_hbv_mpx_ind' OR formdata.ques_code='nat_hiv_react_ind' OR formdata.ques_code='hiv_antigen_rslt' THEN
          IF v_ques10 = v_notdone AND v_ques11 = v_notdone AND v_ques12 = v_notdone THEN
            v_defer_flag := 1;
          ELSE
            v_defer_flag := 0;
          END IF;
        END IF;
        IF formdata.ques_code='syphilis_react_ind' OR formdata.ques_code='syphilis_ct_sup_react_ind' THEN
          IF v_ques15 = v_posipk AND v_ques18 = v_notdone1 THEN
            v_defer_flag := 1;
          ELSE
            v_defer_flag := 0;
          END IF;
        END IF;
      END IF;
      --DBMS_OUTPUT.PUT_LINE(formdata.ques_seq||'-->'||formdata.defer_value||'-->'||formdata.resp_code||'-->'||v_defer_flag);
      IF v_defer_flag = 1 THEN
        IF ( LENGTH(formdata.defer_value) - LENGTH( REPLACE(formdata.defer_value, ',', '') ) )=0 THEN
          IF formdata.defer_value = formdata.resp_code THEN
            --DBMS_OUTPUT.PUT_LINE(formdata.ques_seq||'-->'||formdata.defer_value||'-->'||formdata.resp_code);
             v_return := 1;
             return v_return;
          END IF;
        END IF;
        IF ( LENGTH(formdata.defer_value) - LENGTH( REPLACE(formdata.defer_value, ',', '') ) )=1 THEN
          v_defer_val1 := substr( formdata.defer_value, 0, instr(formdata.defer_value, ',')-1 );
          v_defer_val2 := substr( formdata.defer_value, instr(formdata.defer_value, ',')+1 );
          IF v_defer_val1 = formdata.resp_code OR v_defer_val2 = formdata.resp_code THEN
            --DBMS_OUTPUT.PUT_LINE(formdata.ques_seq||'-->'||formdata.defer_value||'-->'||formdata.resp_code);
            v_return := 1;
            return v_return;
          END IF;
        END IF;
        IF ( LENGTH(formdata.defer_value) - LENGTH( REPLACE(formdata.defer_value, ',', '') ) )=2 THEN
          v_defer_val1 := substr( formdata.defer_value, 0, instr(formdata.defer_value, ',')-1 );
          v_defer_temp := substr( formdata.defer_value, instr(formdata.defer_value, ',')+1 );
          v_defer_val2 := substr( v_defer_temp, 0, instr(v_defer_temp, ',')-1 );
          v_defer_val3 := substr( v_defer_temp, instr(v_defer_temp, ',')+1 );
          IF v_defer_val1 = formdata.resp_code OR v_defer_val2 = formdata.resp_code OR v_defer_val3 = formdata.resp_code THEN
            --DBMS_OUTPUT.PUT_LINE(formdata.ques_seq||'-->'||formdata.defer_value||'-->'||formdata.resp_code);
            v_return := 1;
            return v_return;
          END IF;
        END IF;
      END IF;
    END IF;  
end loop;
--DBMS_OUTPUT.PUT_LINE('RETURN:::'||v_return);
--v_return := 0;
return v_return;
end;
/


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,249,3,'03_F_GET_IDMDEFERVALUE.sql',sysdate,'9.0.0 B#637-ET037');
commit;