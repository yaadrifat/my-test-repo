--start update cb_form_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='anti_hiv_1_2_o_ind');
  if (v_record_exists = 1) then
	update cb_form_questions set DEFER_VALUE=rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''not_done'')',',') where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='anti_hiv_1_2_o_ind');
commit;
  end if;
end;
/
--END--

--start update cb_form_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='nat_hiv_1_hcv_hbv_mpx_ind');
  if (v_record_exists = 1) then
	update cb_form_questions set DEFER_VALUE=rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''not_done'')',',') where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='nat_hiv_1_hcv_hbv_mpx_ind');
commit;
  end if;
end;
/
--END--

--start update cb_form_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='nat_hiv_react_ind');
  if (v_record_exists = 1) then
	update cb_form_questions set DEFER_VALUE=rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''not_done'')',',') where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='nat_hiv_react_ind');
commit;
  end if;
end;
/
--END--

--start update cb_form_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='syphilis_react_ind');
  if (v_record_exists = 1) then
	update cb_form_questions set DEFER_VALUE=(select PK_CODELST from ER_CODELST where CODELST_TYPE='test_outcome1' and CODELST_SUBTYP ='posi') where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='syphilis_react_ind');
commit;
  end if;
end;
/
--END--


--start update cb_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions where ques_code='inher_thalassemia_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_desc='Thalassemia, such as alpha-Thalassemia or beta-Thalassemia?' where ques_code='inher_thalassemia_ind';
commit;
  end if;
end;
/
--END--

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,249,1,'01_CB_UPDATE_QUESTIONS.sql',sysdate,'9.0.0 B#637-ET037');
commit;