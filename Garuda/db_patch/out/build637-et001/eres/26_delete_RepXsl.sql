set define off;

DELETE FROM "ERES"."ER_REPXSL" WHERE PK_REPXSL = 89;

Commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,181,26,'26_delete_RepXsl.sql',sysdate,'9.0.0 B#637-ET001');

commit;