--Update ER_OBJECT_SETTINGS 

Set define off;
DECLARE
  v_column_value_update number := 0;
BEGIN
  Select count(*) into v_column_value_update
    from user_tab_cols
    where TABLE_NAME = 'ER_OBJECT_SETTINGS'
    AND column_name = 'OBJECT_VISIBLE';
  if (v_column_value_update =1) then
   UPDATE ER_OBJECT_SETTINGS SET OBJECT_VISIBLE = 1
   WHERE OBJECT_VISIBLE=0  AND OBJECT_TYPE='TM' AND OBJECT_SUBTYPE='funding_menu';
   commit;
  end if;
end;
/


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,181,21,'21_ER_Object_settings_update.sql',sysdate,'9.0.0 B#637-ET001');

commit;