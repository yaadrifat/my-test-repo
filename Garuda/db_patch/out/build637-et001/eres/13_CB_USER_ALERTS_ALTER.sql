--STARTS ADDING COLUMN TO CB_USER_ALERTS TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_USER_ALERTS' AND COLUMN_NAME = 'CT_LAB_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_USER_ALERTS ADD CT_LAB_FLAG VARCHAR2(1)';
	commit;
  end if;
end;
/

COMMENT ON COLUMN CB_USER_ALERTS.CT_LAB_FLAG IS 'Stores 1 or 0';

--STARTS ADDING COLUMN TO CB_USER_ALERTS TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_USER_ALERTS' AND COLUMN_NAME = 'CT_SHIP_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_USER_ALERTS ADD CT_SHIP_FLAG VARCHAR2(1)';
	commit;
  end if;
end;
/

COMMENT ON COLUMN CB_USER_ALERTS.CT_SHIP_FLAG IS 'Stores 1 or 0';


--STARTS ADDING COLUMN TO CB_USER_ALERTS TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_USER_ALERTS' AND COLUMN_NAME = 'HE_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_USER_ALERTS ADD HE_FLAG VARCHAR2(1)';
	commit;
  end if;
end;
/

COMMENT ON COLUMN CB_USER_ALERTS.HE_FLAG IS 'Stores 1 or 0';


--STARTS ADDING COLUMN TO CB_USER_ALERTS TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_USER_ALERTS' AND COLUMN_NAME = 'OR_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_USER_ALERTS ADD OR_FLAG VARCHAR2(1)';
	commit;
  end if;
end;
/

COMMENT ON COLUMN CB_USER_ALERTS.OR_FLAG IS 'Stores 1 or 0';

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,181,13,'13_CB_USER_ALERTS_ALTER.sql',sysdate,'9.0.0 B#637-ET001');

commit;