--STARTS DELETING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_27');
  if (v_record_exists = 1) then
      DELETE FROM CB_ALERT_CONDITIONS where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_27');
	commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,181,15,'15_CB_ALERT_COND_DELETE.sql',sysdate,'9.0.0 B#637-ET001');

commit;

