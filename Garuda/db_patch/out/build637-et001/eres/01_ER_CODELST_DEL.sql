--START UPDATING er_codelst TABLE --
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'note_type' and codelst_subtyp='non_clinic';
  if (v_record_exists = 1) then
	delete from er_codelst where codelst_type = 'note_type' and codelst_subtyp='non_clinic';
commit;
  end if;
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,181,1,'01_ER_CODELST_DEL.sql',sysdate,'9.0.0 B#637-ET001');

commit;