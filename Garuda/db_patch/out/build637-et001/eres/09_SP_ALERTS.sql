create or replace
PROCEDURE SP_ALERTS(v_entity_id number,v_entity_type number) AS
TYPE CurTyp IS REF CURSOR;  -- define weak REF CURSOR type
cur_cv   CurTyp;  -- declare cursor variable
v_alertId number;
v_alert_title varchar2(4000);
v_alert_desc varchar2(4000);
v_alert_wording varchar2(4000);
v_alert_precondition varchar2(4000) :='';
v_col_name varchar2(4000);
v_alert_precond_cnt number :=0;
v_query varchar2(4000);
sql_fieldval varchar2(4000);
var_instance_cnt number:=0;
var_instance_cnt1 number:=0;
var_instance_cnt2 number:=0;
var_instance_cnt3 number:=0;
var_instance_cnt4 number:=0;
var_instance_cnt5 number:=0;
v_userId number;
v_cordregid varchar2(255);
V_ORDERTYPE VARCHAR2(255);
v_sampleatlab varchar2(1);
V_ALERTNAME VARCHAR2(255);
v_alerttype varchar2(255);
v_resol number:=0;
v_resoldesc VARCHAR2(50 CHAR):='N/A';
v_orderId number;
v_schshipdate varchar2(50);
v_temp number;
V_ALERT_ID_TEMP NUMBER;
v_entity_order number;
v_entity_cord number;
v_notify number;
v_mail number;
BEGIN
select pk_codelst into v_notify from er_codelst where codelst_type='alert_type' and codelst_subtyp='notify';
select pk_codelst into v_mail from er_codelst where codelst_type='alert_type' and codelst_subtyp='mail';
select pk_codelst into v_entity_order from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER';
select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';
--DBMS_OUTPUT.PUT_LINE('v_entity_id::::::::::::::'||v_entity_id);
FOR CUR_ALERTS IN(select pk_codelst,CODELST_SUBTYP,codelst_desc,codelst_custom_col1,codelst_custom_col,codelst_type from er_codelst where codelst_type in('alert','alert_resol') AND CODELST_HIDE='N' order by pk_codelst)
LOOP
  v_alertId := cur_alerts.pk_codelst;
  V_ALERTNAME:=cur_alerts.codelst_subtyp;
	v_alert_title := cur_alerts.codelst_desc;
	v_alert_desc := cur_alerts.codelst_custom_col1;
	v_alert_wording :=cur_alerts.codelst_custom_col;
  v_alert_precondition := getalertcondition(v_alertid,'PRE');
  v_alerttype:=cur_alerts.codelst_type;
  var_instance_cnt :=0;
  var_instance_cnt1 :=0;
  var_instance_cnt2 :=0;
  var_instance_cnt3 :=0;
  var_instance_cnt4 :=0;
  var_instance_cnt5 :=0;
 -- dbms_output.put_line('********************************************************************************************************************************************');
--  dbms_output.put_line('alert Id :::'||v_alertid);
--    dbms_output.put_line('alert pre condition is::'||v_alert_precondition);
    
if v_alert_precondition<>' ' then
    if v_alertname in('alert_01','alert_02','alert_03','alert_04','alert_15','alert_20','alert_16','alert_29','alert_30','alert_31','alert_32','alert_33','alert_34','alert_35','alert_36','alert_37','alert_38','alert_39') then
      v_query := 'select count(*) cnt from er_order where '||v_alert_precondition;
    elsif v_alertname in('alert_05','alert_06') then
      v_query := 'select count(*) cnt from er_order,er_order_header where er_order.fk_order_header=er_order_header.pk_order_header and '||v_alert_precondition;
    elsif v_alertname in('alert_07','alert_08','alert_09','alert_10','alert_11') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_shipment where er_order.fk_order_header=er_order_header.pk_order_header and cb_shipment.fk_order_id=er_order.pk_order and '||v_alert_precondition;
    elsif v_alertname='alert_19' then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and '||v_alert_precondition;
    elsif v_alertname IN('alert_23','alert_24','alert_25','alert_26') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord,er_attachments,cb_upload_info where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and er_attachments.entity_id=cb_cord.pk_cord and er_attachments.entity_type=(select pk_codelst from er_codelst where codelst_type=''v_entity_type'' and codelst_subtyp=''CBU'') and er_attachments.pk_attachment=cb_upload_info.fk_attachmentid and '||v_alert_precondition;
    end if;
    v_query:=replace(v_query,'#keycolumn',v_entity_id);
     --dbms_output.put_line('^^^^^^^^^^^'||v_query);
    open cur_cv for v_query;
     LOOP
          FETCH cur_cv INTO sql_fieldval ;
          EXIT WHEN cur_cv%NOTFOUND;
           --dbms_output.put_line('display result  ' || sql_fieldval );
           if sql_fieldval>0 then
           if v_entity_type=v_entity_order then
              select nvl(assigned_to,0),nvl(crd.cord_registry_id,'N/A'),nvl(ord.order_sample_at_lab,'-'),case when ord.fk_order_resol_by_tc is null and ord.fk_order_resol_by_cbb is null then 0 when ord.fk_order_resol_by_tc is null and ord.fk_order_resol_by_cbb is not null then ord.fk_order_resol_by_cbb when ord.fk_order_resol_by_tc is not null and ord.fk_order_resol_by_cbb is null then ord.fk_order_resol_by_tc else 0 end into v_userid,v_cordregid,v_sampleatlab,v_resol from er_order ord left outer join er_order_header ordhdr on(ord.fk_order_header=ordhdr.pk_order_header) left outer join cb_cord crd on(ordhdr.order_entityid=crd.pk_cord) where pk_order=v_entity_id;
              --dbms_output.put_line('v_resol............'||v_resol);
              select ord.pk_order,nvl(to_char(ship.sch_shipment_date,'Mon DD, YYYY'),'N/A') into v_temp,v_schshipdate from er_order ord left outer join (select fk_order_id,sch_shipment_date from cb_shipment where fk_order_id=v_entity_id and fk_shipment_type=(select pk_codelst from er_codelst where codelst_type='shipment_type' and codelst_subtyp='CBU Shipment')) ship on(ship.fk_order_id=ord.pk_order) where ord.pk_order=v_entity_id;
            end if;
            if v_entity_type=v_entity_cord then
              select nvl(cord_registry_id,'N/A') into v_cordregid from cb_cord where pk_cord=v_entity_id;
            end if;
           if v_resol<>0 then
           --dbms_output.put_line('before getting resolution desc...............');
            select cbu_status_desc into v_resoldesc from cb_cbu_status where pk_cbu_status=v_resol;
            --dbms_output.put_line('after getting resolution desc...............'||v_resoldesc);
           end if;

           v_alert_wording:=replace(v_alert_wording,'<CBU Registry ID>',v_cordregid);
           v_alert_wording:=replace(v_alert_wording,'<Request Type>',v_ordertype);
           v_alert_wording:=replace(v_alert_wording,'<Resolution Code>',v_resoldesc);
           v_alert_wording:=replace(v_alert_wording,'<Shipment Scheduled Date>',v_schshipdate);
           if v_sampleatlab='Y' then
           v_alert_wording:=replace(v_alert_wording,v_ordertype,'CT Lab');
           elsif v_sampleatlab='N' then
           v_alert_wording:=replace(v_alert_wording,v_ordertype,'CT Ship');
           end if;
           select pk_codelst into v_alert_id_temp from er_codelst where codelst_type='alert' and codelst_subtyp='alert_20';
          -- dbms_output.put_line('v_alert_id_temp::::::::'||v_alert_id_temp);
         --  dbms_output.put_line('v_userId::::::::'||v_userid);
          -- dbms_output.put_line('select count(*) from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert='||v_alertid||' and alrt_inst.entity_id='||v_entity_id||' and alrt_inst.entity_type='||v_entity_type||' and alrt_inst.fk_userid='||v_userid||' and alrt_inst.alert_type='||v_notify);
          -- dbms_output.put_line('var_instance_cnt before::::::'||var_instance_cnt);
           select count(*) into var_instance_cnt from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.fk_userid=v_userid and alrt_inst.alert_type=v_notify;
               -- dbms_output.put_line('var_instance_cnt after:::::'||var_instance_cnt);
            if var_instance_cnt=0 or v_alert_id_temp=v_alertid then
              --  dbms_output.put_line('inside notification insert');
               insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_type,v_userid,v_notify,v_alert_wording);
                COMMIT;
            end if;
            for usrs in(select altr.FK_USER,altr.EMAIL_FLAG email_flag,altr.NOTIFY_FLAG notify_flag,altr.PROMPT_FLAG prompt_flag,addr.add_email mail,altr.ct_lab_flag ctlabflag,altr.ct_ship_flag ctshipflag,altr.he_flag heflag,altr.or_flag orflag from cb_user_alerts altr,er_user usr,er_add addr where altr.fk_user=usr.pk_user and usr.fk_peradd=addr.pk_add and fk_codelst_alert=v_alertid)
            loop
             -- dbms_output.put_line('users that has assigned alert is:::'||usrs.fk_user);
              if usrs.email_flag=1 then
            --  dbms_output.put_line('alert type is:::'||v_alerttype);
            --  dbms_output.put_line('ctlabflag is:::'||usrs.ctlabflag);
              if usrs.ctlabflag=1 and v_alerttype='alert_resol' then
            --  dbms_output.put_line('inside CT lab flag:::'||usrs.ctlabflag);
                if v_sampleatlab='Y' then
             --   dbms_output.put_line('inside ct lab flag2:::'||usrs.ctlabflag);
             --   dbms_output.put_line('select count(*) from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert='||v_alertid||' and alrt_inst.v_entity_id='||v_entity_id||' and alrt_inst.v_entity_type='||v_entity_type||' and alrt_inst.fk_userid='||v_userid||' and alrt_inst.alert_type='||v_mail);
                  select count(*) into var_instance_cnt1 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.entity_type=v_entity_order and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
                  if var_instance_cnt1=0 or v_alert_id_temp=v_alertid then
                    insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
              --    dbms_output.put_line('Email alerts implemented..ct lab');
                    insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,'admin@aithent.com',usrs.mail,v_alert_title,v_alert_wording,0);
                    COMMIT;
                  end if;
                end if;
              elsif usrs.ctshipflag=1 and v_alerttype='alert_resol' then
             -- dbms_output.put_line('inside CT ship flag:::'||usrs.ctshipflag);
             -- dbms_output.put_line(v_ordertype||'inside CT ship flag111111:::'||v_sampleatlab);
                if v_sampleatlab='N' then
              --  dbms_output.put_line('inside CT ship flag222222:::'||usrs.ctshipflag);
                  select count(*) into var_instance_cnt2 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
               --   dbms_output.put_line('var_instance_cnt2:::'||var_instance_cnt2);
                --  dbms_output.put_line(v_alert_id_temp||'='||v_alertid);
                  if var_instance_cnt2=0 or v_alert_id_temp=v_alertid then
                    insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
                    COMMIT;
                --  dbms_output.put_line('Email alerts implemented ct ship');
                    insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,'admin@aithent.com',usrs.mail,v_alert_title,v_alert_wording,0);
                    COMMIT;
                  end if;
                end if;
              elsif usrs.heflag=1 and v_alerttype='alert_resol' then
            --  dbms_output.put_line('inside he flag:::'||usrs.heflag);
                  select count(*) into var_instance_cnt3 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
                  if var_instance_cnt3=0 or v_alert_id_temp=v_alertid then
                    insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
                    COMMIT;
                --  dbms_output.put_line('Email alerts implemented he');
                    insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,'admin@aithent.com',usrs.mail,v_alert_title,v_alert_wording,0);
                    COMMIT;
                  end if;

              elsif usrs.orflag=1 and v_alerttype='alert_resol' then
             -- dbms_output.put_line('inside or flag:::'||usrs.orflag);
                  select count(*) into var_instance_cnt4 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type  and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
                  if var_instance_cnt4=0 or v_alert_id_temp=v_alertid then
                    insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
                  COMMIT;
                --  dbms_output.put_line('Email alerts implemented or');
                    insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,'admin@aithent.com',usrs.mail,v_alert_title,v_alert_wording,0);
                    COMMIT;
                  end if;

              elsif v_alerttype='alert' then
              select count(*) into var_instance_cnt5 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
             --  dbms_output.put_line('var_instance_cnt5:::'||var_instance_cnt5);
             --   dbms_output.put_line(v_alert_id_temp||'='||v_alertid);
              if var_instance_cnt5=0 or v_alert_id_temp=v_alertid then
                insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
              COMMIT;
           --   dbms_output.put_line('Email alerts implemented');
                insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,'admin@aithent.com',usrs.mail,v_alert_title,v_alert_wording,0);
                COMMIT;
              end if;
              end if;
              end if;
              end loop;
              
           end if;
      END LOOP;
    --dbms_output.put_line('alert wording is::'||v_alert_wording);
end if;
END LOOP;
if v_entity_type=v_entity_order then
  update er_order set data_modified_flag='' where pk_order=v_entity_id;
  COMMIT;
end if;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
  dbms_output.put_line ('A SELECT...INTO did not return any row.');
end;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,181,9,'09_SP_ALERTS.sql',sysdate,'9.0.0 B#637-ET001');

commit;