--START UPDATING CB_FORM_QUESTIONS TABLE--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_rel') 
    and FK_FORM = (SELECT PK_FORM FROM CB_FORMS WHERE FORMS_DESC='FMHQ' AND VERSION='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_DEPENDENT_QUES_VALUE=(select pk_codelst from er_codelst where codelst_type='fmhq_res1' and codelst_subtyp='yes') where FK_QUESTION = (SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_rel') AND FK_FORM = (SELECT PK_FORM FROM CB_FORMS WHERE FORMS_DESC='FMHQ' AND VERSION='N2D');
commit;
  end if;
end;
/
--END--
--START UPDATING ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_unres2' and CODELST_SUBTYP = 'negi';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SUBTYP='notdone',CODELST_DESC='Not Done' WHERE CODELST_TYPE = 'idm_unres2' AND CODELST_SUBTYP = 'negi';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE = 'idm_unres4'
    and CODELST_SUBTYP = 'no';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_SUBTYP='no',CODELST_DESC='No' WHERE CODELST_TYPE = 'idm_unres4' AND CODELST_SUBTYP = 'no';
commit;
  end if;
end;
/
--END--
--START UPDATING CB_QUESTION TABLE--
--Question 17--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'malaria_antimalr_drug_3yr_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 3 years,</b> have you had malaria?' where QUES_CODE = 'malaria_antimalr_drug_3yr_ind';
commit;
  end if;
end;
/
--end--
--Question 18--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'outside_us_or_canada_3yr_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 3 years,</b> have you been outside the United States or Canada?' where QUES_CODE = 'outside_us_or_canada_3yr_ind';
commit;
  end if;
end;
/
--end--
--Question 19--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'blood_transf_b4coll_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the 12 months prior to collection of the cord blood unit,</b> have you had a blood transfusion?' where QUES_CODE = 'blood_transf_b4coll_12m_ind';
commit;
  end if;
end;
/
--end--

--Question 20--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tx_stemcell_12m_oth_self_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had a transplant or tissue graft from someone other than yourself, such as organ, bone marrow, stem cell, cornea, bone, skin, or other tissue?' where QUES_CODE = 'tx_stemcell_12m_oth_self_ind';
commit;
  end if;
end;
/
--end--

--Question 21--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'tattoo_ear_skin_pierce_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had a tattoo or ear, skin, or body piercing? ' where QUES_CODE = 'tattoo_ear_skin_pierce_12m_ind';
commit;
  end if;
end;
/
--end--

--Question 23--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'acc_ns_stk_cntc_bld_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had an accidental needle stick or have you come into contact with someone else’s blood through an open wound (for example, a cut or sore), non-intact skin, or mucous membrane (for example, into your eye, mouth, etc)?' where QUES_CODE = 'acc_ns_stk_cntc_bld_12m_ind';
commit;
  end if;
end;
/
--end--

--Question 24--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'had_treat_syph_gono_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had or been treated for a sexually transmitted disease, including syphilis?' where QUES_CODE = 'had_treat_syph_gono_12m_ind';
commit;
  end if;
end;
/
--end--

--Question 25--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'given_mn_dr_for_sex_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you given money or drugs to anyone to engage in sex with you?' where QUES_CODE = 'given_mn_dr_for_sex_ind';
commit;
  end if;
end;
/
--end--

--Question 26--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_tk_mn_dr_pmt_sex_12m_5yr_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you engaged in sex with anyone who had taken money or drugs for sex in the past 5 years?' where QUES_CODE = 'sex_w_tk_mn_dr_pmt_sex_12m_5yr_ind';
commit;
  end if;
end;
/
--end--

--Question 27--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_cntc_liv_hep_b_c_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had sexual contact or lived with a person who has active or chronic viral Hepatitis B or Hepatitis C?' where QUES_CODE = 'sex_cntc_liv_hep_b_c_ind';
commit;
  end if;
end;
/
--end--

--Question 28--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_iv_dr_in_pst_5yr_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had sex, even once, with anyone who has used a needle to take drugs, steroids, or anything else not prescribed by a doctor in the <u>past 5 years?</u>' where QUES_CODE = 'sex_w_iv_dr_in_pst_5yr_12m_ind';
commit;
  end if;
end;
/
--end--

--Question 29--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_man_oth_pst_5yr_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had sex with a male who has had sex with another male, even once, in the <u>past 5 years?</u>' where QUES_CODE = 'sex_w_man_oth_pst_5yr_12m_ind';
commit;
  end if;
end;
/
--end--

--Question 30--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_clotting_factor_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had sex, even once, with anyone who has taken human-derived clotting factors for a bleeding problem in the <u>past 5 years?</u>' where QUES_CODE = 'sex_w_clotting_factor_12m_ind';
commit;
  end if;
end;
/
--end--

--Question 31--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_hiv_aids_12m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you had sex, even once, with anyone who has HIV/AIDS or has had a positive test for the AIDS virus?' where QUES_CODE = 'sex_w_hiv_aids_12m_ind';
commit;
  end if;
end;
/
--end--

--Question 32--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'prison_jail_contin_72hrs_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 12 months,</b> have you been in juvenile detention, lockup, jail or prison for more than 72 continuous hours?' where QUES_CODE = 'prison_jail_contin_72hrs_ind';
commit;
  end if;
end;
/
--end--

--Question 33--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'pst_5yr_tk_mn_dr_sex_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 5 years,</b> have you engaged in sex in exchange for money or drugs?' where QUES_CODE = 'pst_5yr_tk_mn_dr_sex_ind';
commit;
  end if;
end;
/
--end--

--Question 34--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'pst_5yr_iv_drug_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>In the past 5 years,</b> have you used a needle, even once, to take drugs, steroids, or anything else not prescribed for you by a doctor?' where QUES_CODE = 'pst_5yr_iv_drug_ind';
commit;
  end if;
end;
/
--end--

--Question 40--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'spent_uk_ge_3m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<u>From 1980 through 1996,</u> did you spend time that adds up to 3 months or more in the United Kingdom (England, Northern Ireland, Scotland, Wales, the Isle of Man, the Channel Islands, Gibraltar, or the Falkland Islands)?' where QUES_CODE = 'spent_uk_ge_3m_ind';
commit;
  end if;
end;
/
--end--

--Question 41--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'recv_trnsfsn_uk_france_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>Since 1980,</b> have you received a transfusion of blood or blood components while in the U.K. or France?' where QUES_CODE = 'recv_trnsfsn_uk_france_ind';
commit;
  end if;
end;
/
--end--

--Question 43--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'us_military_dep_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>From 1980 through 1996,</b> were you a member of the U.S. military, a civilian military employee, or a dependent of either a member of the U.S. military or civilian military employee?' where QUES_CODE = 'us_military_dep_ind';
commit;
  end if;
end;
/
--end--

--Question 44--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'mil_base_europe_1_ge_6m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>From 1980 through 1990,</b> did you spend a <u>total of 6 months or more</u> associated with a military base in any of the following countries: United Kingdom, Belgium, Netherlands or Germany?' where QUES_CODE = 'mil_base_europe_1_ge_6m_ind';
commit;
  end if;
end;
/
--end--

--Question 45--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'mil_base_europe_2_ge_6m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>From 1980 through 1996,</b> did you spend a <u>total of 6 months or more</u> associated with a military base in any of the following countries: Spain, Portugal, Turkey, Italy or Greece?' where QUES_CODE = 'mil_base_europe_2_ge_6m_ind';
commit;
  end if;
end;
/
--end--
--END--
INSERT INTO track_patches
VALUES(seq_track_patches.nextval,181,16,'16_UPDATE_FORMS.sql',sysdate,'9.0.0 B#637-ET001');

commit;
