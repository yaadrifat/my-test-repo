--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_29';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Canceled (CA)' where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_29';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_30';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Discrepant Typing (DT)' where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_30';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_31';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Confirmatory Testing Requested (CT)' where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_31';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_32';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Held Requested (HE)' where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_32';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_33';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='No Results Received – Lab has not reported results (NR)' where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_33';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_34';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Order for Shipment Requested (OR)' where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_34';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_35';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Patient Not Ready (SA)' where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_35';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_36';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Different Cord Chosen (SB)' where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_36';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_37';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Cord Incompatible (SC)' where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_37';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_38';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Patient Died (SD)' where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_38';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_39';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Other Stem Cell Source Chosen (SE)' where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_39';
	commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,181,12,'12_CODELST_UPDATE.sql',sysdate,'9.0.0 B#637-ET001');

commit;
