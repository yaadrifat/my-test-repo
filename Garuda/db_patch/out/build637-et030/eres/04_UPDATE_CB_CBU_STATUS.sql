--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = '46';
  if (v_record_exists = 1) then
      UPDATE 
  CB_CBU_STATUS SET CBU_STATUS_DESC='Completed' where CBU_STATUS_CODE = '46';
	commit;
  end if;
end;
/
--END--


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,235,4,'04_UPDATE_CB_CBU_STATUS.sql',sysdate,'9.0.0 B#637-ET030');

commit;