set define off;
update audit_mappedvalues set to_value= 'Revoked' where mapping_type = 'Field' and from_value = 'CB_NOTES.AMENDED';
COMMIT;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ASSESSMENT.ASSESSMENT_FLAG_COMMENT';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ASSESSMENT.ASSESSMENT_FLAG_COMMENT','Comments for Flag');
	commit;
  end if;
end;
/

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,235,6,'06_AUDIT_MAPPEDVALUES.sql',sysdate,'9.0.0 B#637-ET030');

commit;