create or replace
function f_hla_cord_order(pk_patient_id number) return SYS_REFCURSOR
IS
p_hla_refcur SYS_REFCURSOR;
BEGIN
    OPEN p_hla_refcur FOR select   f_codelst_desc(fk_hla_code_id) locus,  f_codelst_desc(fk_hla_method_id) method,
(select genomic_format from cb_antigen_encod where fk_antigen = hla.fk_hla_antigeneid1
and (version = nvl((select max(version) from cb_antigen_encod where fk_antigen = hla.fk_hla_antigeneid1),''))) type1,
(select genomic_format from cb_antigen_encod where fk_antigen = hla.fk_hla_antigeneid2
and (version = nvl((select max(version) from cb_antigen_encod where fk_antigen = hla.fk_hla_antigeneid1),''))) type2,
hla_typing_date, hla_received_date, F_CODELST_DESC(FK_SOURCE) as sourceval from cb_hla hla
where ENTITY_ID = pk_patient_id
and entity_type = (select pk_codelst from er_codelst where codelst_type = 'entity_type' and codelst_subtyp = 'PATIENT')
order by FK_SOURCE;
RETURN P_HLA_REFCUR;
END;
/


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,235,18,'18_FUNCTIONS.sql',sysdate,'9.0.0 B#637-ET030');

commit;