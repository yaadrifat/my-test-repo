create or replace
FUNCTION F_BEST_HLA_CORD(pk_cord number, hla_typ NUMBER) return SYS_REFCURSOR
IS
v_entity_cord NUMBER;
v_entity_patient NUMBER:=0;
v_patient_hla_type NUMBER;
v_patient_count NUMBER:=0;
p_bhla_refcur SYS_REFCURSOR;
p_entity_id NUMBER:=0;
p_entity_type NUMBER;
p_source_type NUMBER;
BEGIN
select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';
select pk_codelst into v_entity_patient from er_codelst where codelst_type='entity_type' and codelst_subtyp='PATIENT';
select pk_codelst into v_patient_hla_type from er_codelst where codelst_type='test_source' and codelst_subtyp='patient';


IF hla_typ=v_patient_hla_type THEN
SELECT COUNT(*) INTO V_PATIENT_COUNT FROM ER_ORDER_RECEIPANT_INFO INFOVAL WHERE INFOVAL.FK_ORDER_ID=(SELECT MAX(ORD.PK_ORDER) FROM ER_ORDER_HEADER HDR LEFT OUTER JOIN ER_ORDER ORD ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER) WHERE HDR.ORDER_ENTITYID=pk_cord);
IF V_PATIENT_COUNT>0 THEN
SELECT NVL(INFOVAL.FK_RECEIPANT,0) INTO P_ENTITY_ID FROM ER_ORDER_RECEIPANT_INFO INFOVAL WHERE INFOVAL.FK_ORDER_ID=(SELECT MAX(ORD.PK_ORDER) FROM ER_ORDER_HEADER HDR LEFT OUTER JOIN ER_ORDER ORD ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER) WHERE HDR.ORDER_ENTITYID=pk_cord);
END IF;
p_entity_type:=v_entity_patient;
END IF;

IF hla_typ!=v_patient_hla_type THEN
P_ENTITY_ID:=PK_CORD;
p_entity_type:=v_entity_cord;
END IF;

OPEN p_bhla_refcur FOR SELECT
                       h.fk_hla_code_id AS PKLOCUS,
                       nvl(ec1.genomic_format,' ') AS TYPE1,
                       nvl(ec2.genomic_format,' ') AS TYPE2,
                       to_char(h.hla_received_date,'Mon DD, YYYY') AS ENTRYDATE,
                       h.CB_BEST_HLA_ORDER_SEQ seqval,
                       F_CODELST_DESC(h.fk_hla_method_id) AS METHODDATA,
                       F_CODELST_DESC(h.fk_hla_code_id) AS LOCUS,
                       f_getuser(h.creator) usrname
                      FROM CB_BEST_HLA h,
                        cb_antigen_encod ec1,
                        cb_antigen_encod ec2
                      WHERE h.ENTITY_TYPE  =p_entity_type
                      AND H.FK_SOURCE=hla_typ
                      AND h.ENTITY_ID         =P_ENTITY_ID
                      AND h.fk_hla_antigeneid1=ec1.fk_antigen (+)
                      AND ( ec1.version       =
                        (SELECT MAX(ec1s.version)
                        FROM cb_antigen_encod ec1s
                        WHERE ec1.fk_antigen=ec1s.fk_antigen
                        )
                      OR ec1.version          IS NULL)
                      AND h.fk_hla_antigeneid2 = ec2.fk_antigen (+)
                      AND (ec2.version         =
                        (SELECT MAX(ec2s.version)
                        FROM cb_antigen_encod ec2s
                        WHERE ec2.fk_antigen=ec2s.fk_antigen
                        )
                      OR ec2.version IS NULL)
                      ORDER BY F_CODELST_DESC(h.fk_hla_code_id) nulls last;
RETURN p_bhla_refcur;

END;
/
---------------------------------------------------------------------------------------------------------------------

create or replace
function f_lab_summary(pk_cordid number) return SYS_REFCURSOR
IS
p_lab_refcur SYS_REFCURSOR;
v_specimen number;
begin
select fk_specimen_id into v_specimen from cb_cord where pk_cord=pk_cordid;
OPEN p_lab_refcur FOR select a.pk_labtest, a.labtest_name, a.labtest_seq
,(select  test_result from er_patlabs  where   fk_timing_of_test = f_codelst_id('timing_of_test','pre_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) pre_test_result
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where fk_timing_of_test = f_codelst_id('timing_of_test','pre_procesing') and fk_testid = a.pk_labtest and fk_specimen = v_specimen ) pre_test_date
,(select max(test_result) from er_patlabs  where  pk_patlabs=(select max(pk_patlabs) from er_patlabs where fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) post_test_result
,(select to_char(test_date,'Mon DD,YYYY') from er_patlabs where pk_patlabs=(select max(pk_patlabs) from er_patlabs where fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) post_test_date
, f_codelst_desc((select fk_test_method from er_patlabs  where   pk_patlabs=(select max(pk_patlabs) from er_patlabs where fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen))) post_testmeth
,(select custom005 from er_patlabs  where   pk_patlabs=(select max(pk_patlabs) from er_patlabs where fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) post_test_desc
,(select  test_result from er_patlabs  where pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) post_cryopres_thaw
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) post_cryopres_date
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen))) reason_thaw
, (select  custom004 from er_patlabs  where   pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) reason_desc_thaw
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where  pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen))) smpltype_thaw
, f_codelst_desc((select  fk_test_method from er_patlabs  where   pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen))) testmeth_thaw
, (select  custom005 from er_patlabs  where   pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) test_desc_thaw
,(select  test_result from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) post_cryopres_thaw1
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '1' and fk_testid = a.pk_labtest and fk_specimen = v_specimen) post_cryopres_date1
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) reason_thaw1
, (select  custom004 from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) reason_desc_thaw1
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) smpltype_thaw1
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) testmeth_thaw1
, (select  custom005 from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) test_desc_thaw1
,(select  test_result from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) post_cryopres_thaw2
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '2' and fk_testid = a.pk_labtest and fk_specimen = v_specimen) post_cryopres_date2
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) reason_thaw2
, (select  custom004 from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) reason_desc_thaw2
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) smpltype_thaw2
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) testmeth_thaw2
, (select  custom005 from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) test_desc_thaw2
,(select  test_result from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) post_cryopres_thaw3
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '3' and fk_testid = a.pk_labtest and fk_specimen = v_specimen) post_cryopres_date3
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) reason_thaw3
, (select  custom004 from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) reason_desc_thaw3
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) smpltype_thaw3
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) testmeth_thaw3
, (select  custom005 from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) test_desc_thaw3
,(select  test_result from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) post_cryopres_thaw4
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '4' and fk_testid = a.pk_labtest and fk_specimen = v_specimen) post_cryopres_date4
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) reason_thaw4
, (select  custom004 from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) reason_desc_thaw4
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) smpltype_thaw4
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) testmeth_thaw4
, (select  custom005 from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) test_desc_thaw4
,(select  test_result from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) post_cryopres_thaw5
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '5' and fk_testid = a.pk_labtest and fk_specimen = v_specimen) post_cryopres_date5
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) reason_thaw5
, (select  custom004 from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) reason_desc_thaw5
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) smpltype_thaw5
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) testmeth_thaw5
, (select  custom005 from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) test_desc_thaw5
from er_labtest a,er_labtestgrp lgrp,er_labgroup grp where lgrp.fk_labgroup=grp.pk_labgroup and lgrp.fk_testid=a.pk_labtest and grp.group_name='PrcsGrp' and grp.group_type='PC' order by labtest_seq; 
RETURN p_lab_refcur;
end;  
/

---------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION F_TC_OR_ORDER_DETAILS(ORDERID NUMBER) RETURN SYS_REFCURSOR IS
TC_OR_ORDER SYS_REFCURSOR;
BEGIN
OPEN TC_OR_ORDER FOR
SELECT
f_codelst_desc(rec.ind_protocol) IND_PROTOCOL,
NVL(TO_CHAR(rec.ind_sponser),'Not Provided') IND_SPON,
NVL(TO_CHAR(rec.ind_number),'Not Provided') IND_NO,
NVL(rec.pre_current_diagnosis,'Not Provided') CURRENT_DIAGNOSIS,
CASE 
WHEN rec.SINGLE_UNIT_TRANSPLANT IS NOT NULL AND rec.SINGLE_UNIT_TRANSPLANT='Y' THEN 'Yes'
WHEN rec.SINGLE_UNIT_TRANSPLANT IS NOT NULL AND rec.SINGLE_UNIT_TRANSPLANT='N' THEN 'No'
WHEN rec.SINGLE_UNIT_TRANSPLANT IS NULL  THEN 'Not Provided'
END SINGLE_UNIT_TRANSPLANT,
CASE
WHEN rec.MULTI_TRANSPLANT IS NOT NULL AND rec.MULTI_TRANSPLANT='Y' THEN 'Yes'
WHEN rec.MULTI_TRANSPLANT IS NOT NULL AND rec.MULTI_TRANSPLANT='N' THEN 'No'
WHEN rec.MULTI_TRANSPLANT IS NULL  THEN 'Not Provided'
END MULTI_TRANSPLANT,
CASE
WHEN rec.OTHER_TRANSPLANT IS NOT NULL AND rec.OTHER_TRANSPLANT='Y' THEN 'Yes'
WHEN rec.OTHER_TRANSPLANT IS NOT NULL AND rec.OTHER_TRANSPLANT='N' THEN 'No'
WHEN rec.OTHER_TRANSPLANT IS NULL  THEN 'Not Provided'
END OTHER_TRANSPLANT,
CASE
WHEN rec.EX_VIVO_TRANSPLANT IS NOT NULL AND rec.EX_VIVO_TRANSPLANT='Y' THEN 'Yes'
WHEN rec.EX_VIVO_TRANSPLANT IS NOT NULL AND rec.EX_VIVO_TRANSPLANT='N' THEN 'No'
WHEN rec.EX_VIVO_TRANSPLANT IS NULL  THEN 'Not Provided'
END EX_VIVO_TRANSPLANT,
NVL(TO_CHAR(rec.disease_stage),'Not Provided') DISIEASE_STATGE,
NVL(TO_CHAR(REC.NO_OF_REMISSIONS),'Not Provided') NO_OF_REMISSIONS,
NVL(TO_CHAR(rec.fk_leu_curr_status),'Not Provided') CURRENT_STAT_LEUCUMA,
NVL(TO_CHAR(rec.rec_transfused),'Not Provided') REC_TRANSFUSED,
NVL(TO_CHAR(rec.rec_weight),'Not Provided') REC_WEIGHT,
NVL(TO_CHAR(rec.prop_infusion_date,'Mon DD, YYYY'),'Not Provided') prop_infusion_date,
NVL(TO_CHAR(rec.prop_prep_date,'Mon DD, YYYY'),'Not Provided') prop_prep_date,
CASE 
WHEN rec.res_cel_sam_flag IS NOT NULL AND rec.res_cel_sam_flag='Y' THEN 'Yes' 
WHEN rec.res_cel_sam_flag IS NOT NULL AND rec.res_cel_sam_flag='N' THEN 'No' 
WHEN rec.res_cel_sam_flag IS NULL THEN 'Not Provided'
END RES_CEL_SAM_FLAG,
CASE
    WHEN per.person_fname
      ||' '
      ||per.person_lname=' '
    THEN 'Not Provided'
    WHEN per.person_fname IS NOT NULL
    AND (per.person_lname IS NULL
    OR per.person_lname    ='')
    THEN per.person_fname
    WHEN per.person_lname IS NOT NULL
    AND (per.person_fname IS NULL
    OR per.person_fname    ='')
    THEN per.person_lname
    ELSE per.person_fname
      ||' '
      ||per.person_lname
  END attn_name,
NVL(trans2.site_name,'Not Provided') CENTRE_NAME,
NVL(per.person_address1,'Not Provided') ADDR1,
NVL(per.person_address2,'Not Provided') ADDR2,
 CASE
    WHEN per.person_city
      ||'/'
      ||per.person_state='/'
    THEN 'Not Provided'
    WHEN per.person_city  IS NOT NULL
    AND (per.person_state IS NULL
    OR per.person_state    ='')
    THEN per.person_city
    WHEN per.person_state IS NOT NULL
    AND (per.person_city  IS NULL
    OR per.person_city     ='')
    THEN per.person_state
    ELSE per.person_city
      ||'/'
      ||per.person_state
  END CITY,
  NVL(per.person_state,'Not Provided') STATE,
  NVL(TO_CHAR(per.person_zip),'Not Provided') ZIPCODE,
  CASE
    WHEN per.person_country
      ||'/'
      ||TO_CHAR(per.person_zip)='/'
    THEN 'Not Provided'
    WHEN per.person_country      IS NOT NULL
    AND (TO_CHAR(per.person_zip) IS NULL
    OR TO_CHAR(per.person_zip)    ='')
    THEN per.person_country
    WHEN TO_CHAR(per.person_zip) IS NOT NULL
    AND (per.person_country      IS NULL
    OR per.person_country         ='')
    THEN TO_CHAR(per.person_zip)
    ELSE per.person_country
      ||'/'
      ||TO_CHAR(per.person_zip)
  END country,
  NVL(per.person_notes,'Not Provided') ADDCOMMENT,
    CASE
    WHEN p1.person_fname
      ||' '
      ||p1.person_lname=' '
    THEN 'Not Provided'
    WHEN p1.person_fname IS NOT NULL
    AND (p1.person_lname IS NULL
    OR p1.person_lname    ='')
    THEN p1.person_fname
    WHEN p1.person_lname IS NOT NULL
    AND (p1.person_fname IS NULL
    OR p1.person_fname    ='')
    THEN p1.person_lname
    ELSE p1.person_fname
      ||' '
      ||p1.person_lname
  END contact1_name,
  NVL(TO_CHAR(p1.person_hphone),'Not Provided') contact1_hphone,
  NVL(TO_CHAR(p1.person_bphone),'Not Provided') contact1_bphone,
  NVL(P1.PERSON_FAX,'Not Provided') CONTACT1_FAX,
  CASE
    WHEN p2.person_fname
      ||' '
      ||p2.person_lname=' '
    THEN 'Not Provided'
    WHEN p2.person_fname IS NOT NULL
    AND (p2.person_lname IS NULL
    OR p2.person_lname    ='')
    THEN p2.person_fname
    WHEN p2.person_lname IS NOT NULL
    AND (p2.person_fname IS NULL
    OR p2.person_fname    ='')
    THEN p2.person_lname
    ELSE p2.person_fname
      ||' '
      ||p2.person_lname
  END contact2_name,
  NVL(TO_CHAR(p2.person_hphone),'Not Provided') contact2_hphone,
  NVL(TO_CHAR(p2.person_bphone),'Not Provided') contact2_bphone,
  NVL(P2.PERSON_FAX,'Not Provided') CONTACT2_FAX,
  CASE
    WHEN p3.person_fname
      ||' '
      ||p3.person_lname=' '
    THEN 'Not Provided'
    WHEN p3.person_fname IS NOT NULL
    AND (p3.person_lname IS NULL
    OR p3.person_lname    ='')
    THEN p3.person_fname
    WHEN p3.person_lname IS NOT NULL
    AND (p3.person_fname IS NULL
    OR p3.person_fname    ='')
    THEN p3.person_lname
    ELSE p3.person_fname
      ||' '
      ||p3.person_lname
  END contact3_name,
  NVL(TO_CHAR(p3.person_hphone),'Not Provided') contact3_hphone,
  NVL(TO_CHAR(p3.person_bphone),'Not Provided') contact3_bphone,
  NVL(P3.PERSON_FAX,'Not Provided') CONTACT3_FAX,
  NVL(TO_CHAR(ordhdr.order_open_date,'Mon DD, YYYY'),'Not Provided') order_date,
  CASE
    WHEN phy.person_fname
      ||' '
      ||phy.person_lname=' '
    THEN 'Not Provided'
    WHEN phy.person_fname IS NOT NULL
    AND (phy.person_lname IS NULL
    OR phy.person_lname    ='')
    THEN phy.person_fname
    WHEN phy.person_lname IS NOT NULL
    AND (phy.person_fname IS NULL
    OR phy.person_fname    ='')
    THEN phy.person_lname
    ELSE phy.person_fname
      ||' '
      ||phy.person_lname
  END order_physician
FROM er_order_header ordhdr
LEFT OUTER JOIN er_order ord
ON(ord.fk_order_header=ordhdr.pk_order_header)
LEFT OUTER JOIN
  (SELECT RECEIPANT_ID,
    FK_CONTACT_PERSON,
    TRANS_CENTER_ID,
    NO_OF_REMISSIONS,
    pre.REC_WEIGHT,
    FK_RECEIPANT_CONTACT1,
    FK_RECEIPANT_CONTACT2,
    FK_RECEIPANT_CONTACT3,
    REC_HEIGHT,
    SEC_TRANS_CENTER_ID,
    ORDER_PHYSICIAN,
    FK_RECEIPANT,
    FK_ORDER_ID,
    IND_PROTOCOL,
    IND_SPONSER,
    IND_NUMBER,
    pre.CURRENT_DIAGNOSIS pre_current_diagnosis,
    re.CURRENT_DIAGNOSIS re_current_diagnosis,
    pre.DISEASE_STAGE,
    FK_TRANSPLANT_TYPE,
    FK_LEU_CURR_STATUS,
    REC_TRANSFUSED,
    PROP_INFUSION_DATE,
    PROP_PREP_DATE,
    RES_CEL_SAM_FLAG,
    EX_VIVO_TRANSPLANT,
    MULTI_TRANSPLANT,
    OTHER_TRANSPLANT,
    SINGLE_UNIT_TRANSPLANT,
    PROPOSED_SHIP_DTE,
    FK_PERSON_ID,
    pre.PRODUCT_DELIVERY_TC_NAME,
    re.pk_receipant pk_receipant
  FROM er_order_receipant_info pre,
    cb_receipant_info re
  WHERE pre.fk_receipant=re.pk_receipant
  ) rec
ON(rec.fk_order_id=ord.pk_order)
LEFT OUTER JOIN EPAT.person per
ON(rec.FK_CONTACT_PERSON=per.pk_person)
LEFT OUTER JOIN EPAT.person p1
ON(rec.fk_receipant_contact1=p1.pk_person)
LEFT OUTER JOIN er_site trans2
ON(rec.PRODUCT_DELIVERY_TC_NAME=trans2.pk_site)
LEFT OUTER JOIN er_site trans
ON(rec.trans_center_id=trans.pk_site)
LEFT OUTER JOIN er_site trans1
ON(rec.sec_trans_center_id=trans1.pk_site)
LEFT OUTER JOIN EPAT.person p2
ON(rec.fk_receipant_contact2=p2.pk_person)
LEFT OUTER JOIN EPAT.person p3
ON(rec.fk_receipant_contact3=p3.pk_person)
LEFT OUTER JOIN er_order_users ordusrs
ON(ordhdr.ORDER_REQUESTED_BY=ordusrs.PK_ORDER_USER)
LEFT OUTER JOIN er_user modifiedby
ON(ord.LAST_MODIFIED_BY=modifiedby.pk_user)
LEFT OUTER JOIN epat.person phy
ON (rec.ORDER_PHYSICIAN=phy.pk_person)
LEFT OUTER JOIN epat.person pat
ON (REC.FK_PERSON_ID=PAT.PK_PERSON)
LEFT OUTER JOIN er_user viewby
ON(ord.ORDER_LAST_VIEWED_BY=viewby.pk_user)
WHERE ord.pk_order         =ORDERID; 
RETURN TC_OR_ORDER;
END;
/

---------------------------------------------------------------------------------------------------------------------

create or replace
function f_order_notes(cordid number,reqtype VARCHAR2) return SYS_REFCURSOR
IS
v_cordorder_notes SYS_REFCURSOR;
BEGIN
    OPEN v_cordorder_notes FOR
select pk_notes, notes, subject, f_codelst_desc(fk_notes_category) notes_category, f_codelst_desc(request_type) req_type,
request_date, to_char(created_on,'Mon DD, YYYY hh:mm:ss am') date_time, F_GETUSER(CREATOR) note_user,
f_codelst_desc(fk_notes_type) note_type
from cb_notes
where entity_id = cordid
and fk_notes_type = (select pk_codelst from er_codelst where codelst_type = 'note_type' and codelst_subtyp= 'progress' )
and f_codelst_desc(request_type) = to_char(reqtype)
order by created_on desc, pk_notes asc;
RETURN v_cordorder_notes;
END;
/

---------------------------------------------------------------------------------------------------------------------
create or replace
function f_cbu_notesall(cordid number) return SYS_REFCURSOR
IS
v_cord_notes SYS_REFCURSOR;
BEGIN
    OPEN v_cord_notes FOR
select pk_notes, notes, subject, f_codelst_desc(fk_notes_category) notes_category,
DECODE(NOTE_ASSESSMENT,'tu','Temporaily Unavailable','no_cause','No Cause for Defferal','na','Not Available') NOTES_ASSESSMENT,
to_char(created_on,'Mon DD, YYYY hh:mm:ss am') date_time, F_GETUSER(CREATOR) note_user,
f_codelst_desc(fk_notes_type) note_type, decode(visibility, 0 ,'No', 1, 'Yes', 'No') visi_tc
from cb_notes
where entity_id = cordid
and fk_notes_type =  F_CODELST_ID('note_type','clinic')
order by created_on desc, pk_notes asc;
RETURN v_cord_notes;
END;
/

---------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION F_GET_TNC_FOR_ORDER(PKCORD NUMBER) RETURN VARCHAR2 IS
TNCVALUE VARCHAR2(200):='';
BEGIN
SELECT 
CASE 
  WHEN LABS.TEST_RESULT IS NOT NULL AND TST.LABTEST_SHORTNAME='UNCRCT' THEN LABS.TEST_RESULT
  WHEN LABS.TEST_RESULT IS NOT NULL AND TST.LABTEST_SHORTNAME='TCBUUN' THEN LABS.TEST_RESULT
END INTO TNCVALUE
FROM ER_PATLABS LABS
LEFT OUTER JOIN CB_CORD CRD
ON CRD.FK_SPECIMEN_ID=LABS.FK_SPECIMEN
LEFT OUTER JOIN ER_LABTEST TST
ON TST.PK_LABTEST=LABS.FK_TESTID
WHERE 
LABS.FK_TIMING_OF_TEST=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='timing_of_test' AND CODELST_SUBTYP='post_procesing') 
AND TST.LABTEST_SHORTNAME IN ('UNCRCT','TCBUUN')
AND CRD.PK_CORD=PKCORD;

RETURN TNCVALUE;
END;
/

---------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION F_GET_TC_ORD_DETAILS(ORDERID NUMBER) RETURN SYS_REFCURSOR IS
SERVICEDATA SYS_REFCURSOR;
BEGIN
OPEN SERVICEDATA FOR 
SELECT ORDSER.SERVICE_CODE SERVICECODE,
  TO_CHAR(ORDSER.REQUESTED_DATE,'Mon DD, YYYY') REQ_REC_DT,
  TO_CHAR(ORDSER.RESULTS_REC_DATE,'Mon DD, YYYY') RES_REC_DT,
  TO_CHAR(ORDSER.CANCELLED_DATE,'Mon DD, YYYY') CAN_DT,
  CASE
    WHEN ORDSER.RESULTS_REC_FLAG IS NOT NULL
    AND (ORDSER.RESULTS_REC_FLAG  ='Y'
    OR ORDSER.RESULTS_REC_FLAG    ='1')
    THEN '1'
    WHEN ORDSER.CANCELLED_FLAG IS NOT NULL
    AND (ORDSER.CANCELLED_FLAG  ='Y'
    OR ORDSER.CANCELLED_FLAG    ='1')
    THEN '2'
  END FLAGVAL,
  ORDSER.FK_ORDER_ID,
  F_CODELST_DESC(ORDSER.SERVICE_STATUS) STATUS
FROM ER_ORDER_SERVICES ORDSER
WHERE ORDSER.FK_ORDER_ID=49689
ORDER BY ORDSER.RESULTS_REC_DATE;

RETURN SERVICEDATA;
END;
/

---------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION f_cbu_reqhistory(cordid NUMBER) RETURN SYS_REFCURSOR  IS
v_cbu_reqhist SYS_REFCURSOR;
BEGIN
    OPEN v_cbu_reqhist FOR   SELECT TO_CHAR(ordhdr.order_open_date,'Mon DD, YYYY') AS Requestdate,
    CASE WHEN f_codelst_desc(ord.order_type)='CT' THEN decode(ord.order_sample_at_lab,'Y','CT - Sample at Lab','N','CT - Ship Sample') WHEN f_codelst_desc(ord.order_type)!='CT' THEN f_codelst_desc(ord.order_type) END AS requestype,
    cbustat.CBU_STATUS_DESC RESOLUTION,
    reci.receipant_id    AS patientid,
    reci.trans_center_id AS transplantcenter,
    reci.pk_receipant    AS pk_receipant,
    ord.pk_order         AS pkorder,
    crd.pk_cord          AS pkcord,
    ordhdr.fk_site_id siteid
  FROM er_order ord
  LEFT OUTER JOIN er_order_header ordhdr
  ON (ord.fk_order_header=ordhdr.pk_order_header)
  LEFT OUTER JOIN cb_cord crd
  ON(ordhdr.order_entityid=crd.pk_cord)
  LEFT OUTER JOIN cb_cbu_status cbustat
  ON(ord.fk_order_resol_by_tc =cbustat.pk_cbu_status)
  LEFT OUTER JOIN
    (SELECT RECEIPANT_ID,
      TRANS_CENTER_ID,
      PK_RECEIPANT,
      FK_ORDER_ID
    FROM cb_receipant_info reci,
      ER_ORDER_RECEIPANT_INFO PRE
    WHERE PRE.FK_RECEIPANT=RECI.PK_RECEIPANT
    ) reci
  ON(reci.fk_order_id=ord.pk_order)
  WHERE crd.pk_cord  = cordid ORDER BY ordhdr.order_open_date DESC;

RETURN v_cbu_reqhist;
END;
/

---------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION f_cbu_ordhistory(cordid NUMBER) RETURN SYS_REFCURSOR IS
v_cbu_ordhist SYS_REFCURSOR;
BEGIN
    OPEN v_cbu_ordhist FOR SELECT *
FROM
  (SELECT TO_CHAR(STATUS.STATUS_DATE,'Mon DD, YYYY') AS DATEVAL,
    CASE
      WHEN (STATUS.STATUS_TYPE_CODE='cord_status'
      OR STATUS.STATUS_TYPE_CODE   ='Cord_Nmdp_Status')
      THEN CBUSTATUS.CBU_STATUS_DESC
      WHEN (STATUS.STATUS_TYPE_CODE='eligibility'
      OR STATUS.STATUS_TYPE_CODE   ='licence')
      THEN F_CODELST_DESC(STATUS.FK_STATUS_VALUE)
    END STATUS,
    CASE
      WHEN STATUS.CREATOR=0 or STATUS.STATUS_TYPE_CODE   ='Cord_Nmdp_Status'
      THEN 'System'
      WHEN STATUS.CREATOR!=0
      THEN F_GETUSER(STATUS.CREATOR)
    END USR,
    '-' ORDTYPE,
    CASE WHEN STATUS.ADD_OR_UPDATE IS NULL OR STATUS.ADD_OR_UPDATE=0 THEN ' entered as '
         WHEN STATUS.ADD_OR_UPDATE=1 THEN ' changed to ' 
    END DESC2,
    CASE WHEN STATUS.STATUS_TYPE_CODE='cord_status' THEN 'Reason'
         WHEN STATUS.STATUS_TYPE_CODE='Cord_Nmdp_Status' THEN 'NMDP Registry Status'
         WHEN STATUS.STATUS_TYPE_CODE='eligibility' THEN 'Eligibility'
         WHEN STATUS.STATUS_TYPE_CODE='licence' THEN 'License Status'
    END DESC3,
    STATUS.STATUS_DATE,
    STATUS.STATUS_TYPE_CODE KEYCODE
  FROM CB_ENTITY_STATUS STATUS
  LEFT OUTER JOIN CB_CBU_STATUS CBUSTATUS
  ON (STATUS.FK_STATUS_VALUE=CBUSTATUS.PK_CBU_STATUS)
  WHERE (STATUS.ENTITY_ID   =cordid
  AND STATUS.FK_ENTITY_TYPE =
    (SELECT C.PK_CODELST
    FROM ER_CODELST C
    WHERE C.CODELST_TYPE='entity_type'
    AND C.CODELST_SUBTYP='CBU'
    ))
  AND( STATUS.STATUS_TYPE_CODE='cord_status'
  OR STATUS.STATUS_TYPE_CODE  ='Cord_Nmdp_Status'
  OR STATUS.STATUS_TYPE_CODE  ='eligibility'
  OR STATUS.STATUS_TYPE_CODE  ='licence')
  UNION ALL
  SELECT 
    TO_CHAR(STAT.STATUS_DATE,'Mon DD, YYYY') AS DATEVAL,
    CASE
      WHEN STAT.STATUS_TYPE_CODE IN ('task_status','task_reset' )
      THEN ACT.ACTIVITY_DESC
    END STATUS,
    CASE
      WHEN STAT.CREATOR=0
      THEN 'System'
      WHEN STAT.CREATOR!=0
      THEN F_GETUSER(STAT.CREATOR)
    END USR,
    CASE
      WHEN f_codelst_desc(ord.order_type) IS NULL
      THEN '-'
      WHEN f_codelst_desc(ord.order_type) IS NOT NULL
      AND f_codelst_desc(ord.order_type)   ='CT'
      THEN DECODE(ord.order_sample_at_lab,'Y','CT - Sample at Lab','N','CT - Ship Sample')
      WHEN f_codelst_desc(ord.order_type) IS NOT NULL
      AND f_codelst_desc(ord.order_type)  !='CT'
      THEN f_codelst_desc(ord.order_type)
    END ordertype,
    ' task ' DESC2,
    CASE
      WHEN STAT.STATUS_TYPE_CODE = 'task_status' THEN 'completed'
      WHEN STAT.STATUS_TYPE_CODE = 'task_reset' THEN 'reset'
    END DESC3,
    STAT.STATUS_DATE,
    STAT.STATUS_TYPE_CODE
  FROM CB_ENTITY_STATUS STAT
  LEFT OUTER JOIN ER_ORDER ORD
  ON (ORD.PK_ORDER =STAT.ENTITY_ID)
  LEFT OUTER JOIN ACTIVITY ACT
  ON (ACT.PK_ACTIVITY   =STAT.FK_STATUS_VALUE)
  WHERE STAT.ENTITY_ID IN
    (SELECT PK_ORDER
    FROM ER_ORDER ORD,
      ER_ORDER_HEADER ORDHDR
    WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER
    AND ORDHDR.ORDER_ENTITYID=cordid
    )
  AND STAT.FK_ENTITY_TYPE=
    (SELECT C.PK_CODELST
    FROM ER_CODELST C
    WHERE C.CODELST_TYPE='entity_type'
    AND C.CODELST_SUBTYP='ORDER'
    )
  AND ( STAT.STATUS_TYPE_CODE IN ('task_status','task_reset' ) )
  AND ACT.ACTIVITY_NAME       IN ('reqclincinfobar','finalcbureviewbar')
  UNION ALL
  SELECT TO_CHAR(FRMVR.CREATED_ON,'Mon DD, YYYY'),
    FRM.FORMS_DESC STATUS,
    F_GETUSER(FRMVR.CREATOR) USR,
    '-' ORDTYPE,
    ' form ' desc2,
    CASE WHEN FRMVR.CREATE_OR_MODIFY IS NULL OR FRMVR.CREATE_OR_MODIFY=0 THEN 'entered'
        WHEN FRMVR.CREATE_OR_MODIFY=1 THEN 'updated'
    end desc3,
    FRMVR.CREATED_ON SYSDATEVAL,
    'FORM'
  FROM CB_FORM_VERSION FRMVR
  LEFT OUTER JOIN CB_FORMS FRM
  ON (FRMVR.FK_FORM      =FRM.PK_FORM)
  WHERE FRMVR.ENTITY_ID  =cordid
  AND FRMVR.CREATED_ON  IS NOT NULL
  AND FRMVR.ENTITY_TYPE IN
    (SELECT C.PK_CODELST
    FROM ER_CODELST C
    WHERE C.CODELST_TYPE='test_source'
    AND C.CODELST_SUBTYP='maternal'
    )
  )
ORDER BY 7 DESC;
RETURN v_cbu_ordhist;
END;
/

---------------------------------------------------------------------------------------------------------------------

create or replace
function f_cbu_req_history(cordid number,orderid number) return SYS_REFCURSOR
IS
v_cord_req_hty SYS_REFCURSOR;
BEGIN
    OPEN V_CORD_REQ_HTY FOR
                        SELECT *FROM(SELECT
                        TO_CHAR(STAT.STATUS_DATE,'Mon DD, YYYY')AS DATEVAL,
                        STATUS.CBU_STATUS_DESC DESCRIPTION1,
                        CASE
                              WHEN STAT.CREATOR=0
                              THEN 'System'
                              WHEN STAT.CREATOR!=0
                              THEN F_GETUSER(STAT.CREATOR)
                        END CREATOR,
                        NVL(F_CODELST_DESC(ORD1.ORDER_TYPE),'-') ORDERTYPE,
                        STAT.STATUS_TYPE_CODE TYPECODE,
                        TO_CHAR(SYSDATE,'Mon DD, YYYY') SYSTEMDATE,
                        F_CODELST_DESC(STAT.FK_STATUS_VALUE) DESCRIPTION2,
                        STAT.STATUS_DATE
                        FROM
                        CB_ENTITY_STATUS STAT
                        LEFT OUTER JOIN
                        CB_CBU_STATUS STATUS ON(STAT.FK_STATUS_VALUE=STATUS.PK_CBU_STATUS)
                        LEFT OUTER JOIN ER_ORDER ORD1 ON(STAT.ENTITY_ID=ORD1.PK_ORDER)
                        WHERE
                        (STAT.ENTITY_ID=CORDID OR STAT.ENTITY_ID =ORDERID) AND (STAT.STATUS_TYPE_CODE='order_status' OR STAT.STATUS_TYPE_CODE='cord_status' OR STAT.STATUS_TYPE_CODE='Cord_Nmdp_Status' )
                        UNION ALL
                        SELECT
                        TO_CHAR(FRMVR.CREATED_ON,'Mon DD, YYYY') DATEVAL,
                        FRM.FORMS_DESC DESC1,
                        F_GETUSER(FRMVR.CREATOR) CREA,
                        '-',
                        'FORMS' FRMNAME,
                        TO_CHAR(sysdate,'Mon DD, YYYY') systemdate,
                        '' desc7,
                        FRMVR.CREATED_ON
                        FROM CB_FORM_VERSION FRMVR
                        LEFT OUTER JOIN CB_FORMS FRM ON (FRMVR.FK_FORM =FRM.PK_FORM)
                        WHERE FRMVR.ENTITY_ID  =CORDID AND FRMVR.CREATED_ON  IS NOT NULL AND FRMVR.ENTITY_TYPE IN (SELECT C.PK_CODELST FROM ER_CODELST C WHERE C.CODELST_TYPE='test_source' AND C.CODELST_SUBTYP='maternal')
                        )ORDER BY 8 DESC;
RETURN V_CORD_REQ_HTY;
END;
/


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,235,15,'15_FUNCTIONS.sql',sysdate,'9.0.0 B#637-ET030');

commit;