--STARTS MODIFY THE COLUMN COMMENTS IN CB_FORM_RESPONSES TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_FORM_RESPONSES'
    AND column_name = 'COMMENTS';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_FORM_RESPONSES MODIFY COMMENTS VARCHAR2(4000 BYTE);';
  end if;
end;
/
--END--


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,247,1,'01_ALTER_CB_RESPONSES.sql',sysdate,'9.0.0 B#637-ET036');
commit;