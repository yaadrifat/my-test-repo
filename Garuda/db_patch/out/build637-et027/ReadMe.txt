/* This readMe is specific to Velos eResearch version 9.0 build637-et027 */

===================================================================================================================================== 

For Bug 14505 - Large HTTP Payloads without HTTP Compression

Please replace the following content in server.xml
 
<!-- A HTTP/1.1 Connector on port 8080 -->

<Connector protocol="HTTP/1.1" port="8080" address="${jboss.bind.address}" connectionTimeout="20000" redirectPort="8443" />

with below lines

<!-- A HTTP/1.1 Connector on port 8080 -->

<Connector protocol="HTTP/1.1" port="8080" address="${jboss.bind.address}" compression="on" compressableMimeType="text/html,text/xml,text/css,text/javascript,
application/x-javascript,application/javascript" connectionTimeout="20000" redirectPort="8443" />

Location of server.xml

Drive:\JBoss-Home\server\eresearch\deploy\jbossweb.sar\server.xml

=====================================================================================================================================

  