set define off;
DECLARE
  COUNTFLAG number := 0;   COUNTFLAG1 number := 0;  COUNTFLAG2 number := 0;  COUNTFLAG3 number := 0;  COUNTFLAG4 number := 0;
  COUNTFLAG5 number := 0;  COUNTFLAG6 number := 0;  COUNTFLAG7 number := 0;  COUNTFLAG8 number := 0;   COUNTFLAG9 number := 0; 
  COUNTFLAG10 number := 0;   COUNTFLAG11 number := 0;   COUNTFLAG12 number := 0;   v_record_exists number := 0;
  V_RECORD_EXISTS1 NUMBER := 0;  v_record_exists2 number := 0;
  v_record_exists3 number := 0;v_record_exists4 number := 0;v_record_exists5 number := 0;v_record_exists6 number := 0;
  v_record_exists7 number := 0;v_record_exists8 number := 0;v_record_exists9 number := 0;v_record_exists10 number := 0;
  v_record_exists11 number := 0;v_record_exists12 number := 0;v_record_exists13 number := 0;v_record_exists14 number := 0;
  v_record_exists15 number := 0;v_record_exists16 number := 0;v_record_exists17 number := 0;v_record_exists18 number := 0;
  v_record_exists19 number := 0;v_record_exists20 number := 0;v_record_exists21 number := 0;v_record_exists22 number := 0;
  v_record_exists23 number := 0;v_record_exists24 number := 0;v_record_exists25 number := 0;v_record_exists26 number := 0;
  v_record_exists27 number := 0;v_record_exists28 number := 0;v_record_exists29 number := 0;v_record_exists30 number := 0;
  v_record_exists31 number := 0;v_record_exists32 number := 0;v_record_exists33 number := 0;v_record_exists34 number := 0;
  v_record_exists35 number := 0;v_record_exists36 number := 0;v_record_exists37 number := 0;v_record_exists38 number := 0;
  v_record_exists39 number := 0;v_record_exists40 number := 0;v_record_exists41 number := 0;v_record_exists42 number := 0;
  v_record_exists43 number := 0;
BEGIN
SELECT COUNT(*) INTO  COUNTFLAG FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB';
     if (COUNTFLAG = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Table','CBB','CBB');	 
commit;
END IF;
SELECT COUNT(*) INTO  COUNTFLAG1 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_SHP_PWRK_LOC';
     if (COUNTFLAG1 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_SHP_PWRK_LOC','Default Shipping Paper Work Location');
commit;
END IF;
SELECT COUNT(*) INTO  COUNTFLAG2 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.DEFAULT_CT_SHIPPER';
     if (COUNTFLAG2 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.DEFAULT_CT_SHIPPER','Default CT Shipper');
commit;
END IF;
SELECT COUNT(*) INTO  COUNTFLAG3 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.DEFAULT_OR_SHIPPER';
     if (COUNTFLAG3 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.DEFAULT_OR_SHIPPER','Default OR Shipper');
commit;
END IF;
SELECT COUNT(*) INTO  COUNTFLAG4 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_CBU_STORAGE';
     if (COUNTFLAG4 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_CBU_STORAGE','CBU Storage Location');
commit;
END IF;
SELECT COUNT(*) INTO  COUNTFLAG5 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_CBU_COLLECTION';
     if (COUNTFLAG5 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_CBU_COLLECTION','CBU Collection Site');
commit;
END IF;
SELECT COUNT(*) INTO  COUNTFLAG6 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.MEMBER';
     if (COUNTFLAG6 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.MEMBER','Membership Type');
commit;
END IF;
SELECT COUNT(*) INTO  COUNTFLAG7 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.EMDIS';
     if (COUNTFLAG7 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.EMDIS','EMDIS');
commit;
END IF;
SELECT COUNT(*) INTO  COUNTFLAG8 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.PICKUP_ADD_SPL_INSTRUCTION';
     if (COUNTFLAG8 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.PICKUP_ADD_SPL_INSTRUCTION','Special Instruction');
commit;
END IF;
SELECT COUNT(*) INTO  COUNTFLAG9 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.BMDW';
     if (COUNTFLAG9 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.BMDW','BMDW');
commit;
END IF;
SELECT COUNT(*) INTO  COUNTFLAG10 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.USING_CDR';
     if (COUNTFLAG10 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.USING_CDR','System User');
commit;
END IF;
SELECT COUNT(*) INTO  COUNTFLAG11 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.CBU_ASSESSMENT';
     if (COUNTFLAG11 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.CBU_ASSESSMENT','CBU Assessment Button');
commit;
END IF;
SELECT COUNT(*) INTO  COUNTFLAG12 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.NMD_PRESEARCH_FLAG';
     if (COUNTFLAG12 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.NMD_PRESEARCH_FLAG','Research Sample');
commit;
END IF;
SELECT COUNT(*) INTO  v_record_exists FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.DOMESTIC_NMDP';
     if (v_record_exists = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.DOMESTIC_NMDP','Domestic');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS1 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.UNIT_REPORT';
     if (v_record_exists1 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.UNIT_REPORT','Unit Report/RACBI Button');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS2 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_ACCREDITATION';
     if (v_record_exists2 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_ACCREDITATION','Accreditation');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS3 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.PK_CBB';
     if (v_record_exists3 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.PK_CBB','CBB ID(Primary Key)');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS4 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.CBB_ID';
     if (v_record_exists4 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.CBB_ID','CBB ID');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS5 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.CREATOR';
     if (v_record_exists5 = 0) then
Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.CREATOR','Creator');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS6 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.CREATED_ON';
     if (v_record_exists6 = 0) then
Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.CREATED_ON','Created date');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS7 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.LAST_MODIFIED_BY';
     if (v_record_exists7 = 0) then
Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.LAST_MODIFIED_BY','Last modified by');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS8 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.LAST_MODIFIED_DATE';
     if (v_record_exists8 = 0) then
Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.LAST_MODIFIED_DATE','Last modified date');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS9 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.IP_ADD';
     if (v_record_exists9 = 0) then
Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.IP_ADD','IP Address');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS10 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.DELETEDFLAG';
     if (v_record_exists10 = 0) then
Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.DELETEDFLAG','Deletedflag');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS11 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.RID';
     if (v_record_exists11 = 0) then
Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.RID','RID');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS12 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_PERSON';
     if (v_record_exists12 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_PERSON','Person Code');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS13 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_ADD';
     if (v_record_exists13 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_ADD','Address');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS14 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_SITE';
     if (v_record_exists14 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_SITE','Site Name');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS15 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.ENABLE_EDIT_ANTIGENS';
     if (v_record_exists15 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.ENABLE_EDIT_ANTIGENS','Enable Edit Antigens');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS16 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.DISPLAY_MATERNAL_ANTIGENS';
     if (v_record_exists16 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.DISPLAY_MATERNAL_ANTIGENS','Display Maternal Antigens');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS17 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.ENABLE_CBU_INVENT_FEATURE';
     if (v_record_exists17 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.ENABLE_CBU_INVENT_FEATURE','CBU Feature');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS18 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.MICRO_CONTAM_DATE';
     if (v_record_exists18 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.MICRO_CONTAM_DATE','Micro Date');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS19 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.CFU_TEST_DATE';
     if (v_record_exists19 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.CFU_TEST_DATE','Test Date');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS20 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.VIAB_TEST_DATE';
     if (v_record_exists20 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.VIAB_TEST_DATE','Viability Test Date');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS21 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.ENABLE_CORD_INFO_LOCK';
     if (v_record_exists21 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.ENABLE_CORD_INFO_LOCK','Enable cord Info Lock');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS22 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.DATE_FORMAT';
     if (v_record_exists22 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.DATE_FORMAT','Date Format');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS23 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.LOC_CBU_ID_PRE';
     if (v_record_exists23 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.LOC_CBU_ID_PRE','Local CBU Id Pre');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS24 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.LOC_MAT_ID_PRE';
     if (v_record_exists24 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.LOC_MAT_ID_PRE','Local Maternal Id Pre');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS25 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.LOC_CBU_ID_STR_NUM';
     if (v_record_exists25 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.LOC_CBU_ID_STR_NUM','Local cbu id start number');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS26 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.LOC_MAT_ID_STR_NUM';
     if (v_record_exists26 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.LOC_MAT_ID_STR_NUM','Local maternal start number');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS27 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_LOC_CBU_ASSGN_ID';
     if (v_record_exists27 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_LOC_CBU_ASSGN_ID','Local CBU Name');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS28 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_LOC_MAT_ASSGN_ID';
     if (v_record_exists28 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_LOC_MAT_ASSGN_ID','Local Maternal name');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS29 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.COMP_CORD_ENTRY';
     if (v_record_exists29 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.COMP_CORD_ENTRY','CORD Entry Flag');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS30 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.COMP_ACK_RES_TASK';
     if (v_record_exists30 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.COMP_ACK_RES_TASK','auto complete cord entry flag');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS31 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_CBU_PCK_ADD';
     if (v_record_exists31 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_CBU_PCK_ADD','CBU Address');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS32 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.USE_OWN_DUMN';
     if (v_record_exists32 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.USE_OWN_DUMN','Dumn');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS33 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.USE_FDOE';
     if (v_record_exists33 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.USE_FDOE','FDOE');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS34 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.ATTENTION';
     if (v_record_exists34 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.ATTENTION','Attention');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS35 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_TIME_FORMAT';
     if (v_record_exists35 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_TIME_FORMAT','Time Format');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS36 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_DATE_FORMAT';
     if (v_record_exists36 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_DATE_FORMAT','Date Format');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS37 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_ID_ON_BAG';
     if (v_record_exists37 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_ID_ON_BAG','ID On Bag');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS38 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.FK_DRY_SHIPPER_ADD';
     if (v_record_exists38 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.FK_DRY_SHIPPER_ADD','Dry Shipper Address');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS39 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.ATTENTION_DRY_SHIPPER';
     if (v_record_exists39 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.ATTENTION_DRY_SHIPPER','Dry Shipper');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS40 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.LINKS_HOVER_FLAG';
     if (v_record_exists40 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.LINKS_HOVER_FLAG','Hover Flag');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS41 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.PICKUP_ADD_CONTACT_LASTNAME';
     if (v_record_exists41 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.PICKUP_ADD_CONTACT_LASTNAME','Last Name');
commit;
END IF;
SELECT COUNT(*) INTO  V_RECORD_EXISTS42 FROM AUDIT_MAPPEDVALUES WHERE FROM_VALUE ='CBB.PICKUP_ADD_CONTACT_FIRSTNAME';
     if (v_record_exists42 = 0) then
     Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CBB.PICKUP_ADD_CONTACT_FIRSTNAME','First Name');
commit;
END IF;
END;
/


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,231,21,'21_Hotfix1_CBB_MappedValues.sql',sysdate,'9.0.0 B#637-ET027.01');
commit;