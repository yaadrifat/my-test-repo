set define off;
update audit_mappedvalues set to_value= 'Recipient Weight(Kg)' where mapping_type = 'Field' and from_value = 'ER_ORDER_RECEIPANT_INFO.REC_WEIGHT';
update audit_mappedvalues set to_value= 'Proposed CBU Ship Date' where mapping_type = 'Field' and from_value = 'ER_ORDER_RECEIPANT_INFO.PROPOSED_SHIP_DTE';
update audit_mappedvalues set to_value= 'Center Name' where mapping_type = 'Field' and from_value = 'ER_ORDER_RECEIPANT_INFO.PRODUCT_DELIVERY_TC_NAME';
update audit_mappedvalues set to_value= 'Attn/Name' where mapping_type = 'Field' and from_value = 'ER_ORDER_RECEIPANT_INFO.FK_CONTACT_PERSON';
commit;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Table' and FROM_VALUE = 'CB_RECEIPANT_INFO';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Table','CB_RECEIPANT_INFO','Recipient Info');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_RECEIPANT_INFO.RECEIPANT_ID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_RECEIPANT_INFO.RECEIPANT_ID','Patient ID');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_RECEIPANT_INFO.CURRENT_DIAGNOSIS';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_RECEIPANT_INFO.CURRENT_DIAGNOSIS','Current Diagnosis');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_RECEIPANT_INFO.TRANS_CENTER_ID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_RECEIPANT_INFO.TRANS_CENTER_ID','Transplant Center Name');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_RECEIPANT_INFO.SEC_TRANS_CENTER_ID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_RECEIPANT_INFO.SEC_TRANS_CENTER_ID','Secondary TC');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_RECEIPANT_INFO.TRANS_CENTER_ID_ID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_RECEIPANT_INFO.TRANS_CENTER_ID_ID','Transplant Center ID');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_RECEIPANT_INFO.PERSON_SEX';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_RECEIPANT_INFO.PERSON_SEX','Sex');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_RECEIPANT_INFO.PERSON_AGE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_RECEIPANT_INFO.PERSON_AGE','Age');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.HPHONE1';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.HPHONE1','Telephone for Contact 1');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.HPHONE2';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.HPHONE2','Telephone for Contact 2');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.HPHONE3';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.HPHONE3','Telephone for Contact 3');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.BPHONE1';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.BPHONE1','Pager/Mobile for Contact 1');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.BPHONE2';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.BPHONE2','Pager/Mobile for Contact 2');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.BPHONE3';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.BPHONE3','Pager/Mobile for Contact 3');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FAX1';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FAX1','Fax for Contact 1');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FAX2';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FAX2','Fax for Contact 2');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FAX3';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FAX3','Fax for Contact 3');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.ADDRESSLINE1';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.ADDRESSLINE1','Address Line 1');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.ADDRESSLINE2';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.ADDRESSLINE2','Address Line 2');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.CITYSTATE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.CITYSTATE','City/State/Province');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.COUNTRYZIP';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.COUNTRYZIP','Country/Zip Code');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.ADDLCOMMENT';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.ADDLCOMMENT','Additional Comments');
	commit;
  end if;
end;
/

		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,231,9,'09_RECEIPANT_INFO_MAPVAL.sql',sysdate,'9.0.0 B#637-ET027');
		commit;