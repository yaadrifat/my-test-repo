alter table AUDIT_MAPPEDVALUES modify TO_VALUE  varchar2(500);

alter table CB_FINAL_DECL_ELIG modify DELETEDFLAG varchar2(1);

alter table AUDIT_COLUMN_MODULE modify COLUMN_DISPLAY_NAME varchar2(500);

--STARTS ADDING COLUMN ASSESSMENT_FLAG_COMMENT TO CB_ASSESSMENT--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_ASSESSMENT'
    AND column_name = 'ASSESSMENT_FLAG_COMMENT';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_ASSESSMENT ADD(ASSESSMENT_FLAG_COMMENT varchar2(4000))';
  end if;
end;
/
COMMENT ON COLUMN "CB_ASSESSMENT"."ASSESSMENT_FLAG_COMMENT" IS 'This Column is used to store the Comment given for Flag';

UPDATE cb_assessment SET assessment_flag_comment=assessment_remarks,assessment_remarks=null WHERE SUB_ENTITY_TYPE = (SELECT pk_codelst FROM er_codelst where codelst_type='sub_entity_type' and codelst_subtyp='NOTES') and assessment_remarks is not null;

commit;

		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,231,1,'01_MISC_ALTER.sql',sysdate,'9.0.0 B#637-ET027');
		commit;