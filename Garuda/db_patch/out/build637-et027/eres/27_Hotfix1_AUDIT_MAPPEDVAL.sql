set define off;
update audit_mappedvalues set to_value= 'Describe' where mapping_type = 'Field' and from_value = 'ER_PATLABS.CUSTOM005';
update audit_mappedvalues set to_value= 'Test Method' where mapping_type = 'Field' and from_value = 'ER_PATLABS.FK_TEST_METHOD';
update audit_mappedvalues set to_value= 'Test' where mapping_type = 'Field' and from_value = 'ER_PATLABS.FK_TESTID';
update audit_mappedvalues set to_value= 'Switch Workflow' where mapping_type = 'Field' and from_value = 'ER_ORDER.ORDER_SAMPLE_AT_LAB';
update audit_mappedvalues set to_value= 'Reason for Workflow Switch' where mapping_type = 'Field' and from_value = 'ER_ORDER.SWITCH_REASON';
update audit_mappedvalues set to_value= 'Other Reason' where mapping_type = 'Field' and from_value = 'ER_ORDER.SWITCH_REASON_OTHER';
update audit_mappedvalues set to_value= 'Workflow Switched By' where mapping_type = 'Field' and from_value = 'ER_ORDER.SWITCH_BY';
update audit_mappedvalues set to_value= 'Workflow Switched Date/Time' where mapping_type = 'Field' and from_value = 'ER_ORDER.SWITCH_DATE';
COMMIT;


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,231,27,'27_Hotfix1_AUDIT_MAPPEDVAL.sql',sysdate,'9.0.0 B#637-ET027.01');
commit;

