--When records are inserted in CB_CORD_FINAL_REVIEW table, audit records are inserted in AUDIT_ROW_MODULE and AUDIT_COLUMN_MODULE tables
CREATE OR REPLACE TRIGGER "ERES"."CB_CORD_FINAL_REVIEW_AI0"
AFTER INSERT ON ERES.CB_CORD_FINAL_REVIEW 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	
  v_rowid NUMBER(10);/*variable used to fetch value of sequence */
	NEW_CORD_ACCEPT_FLAG VARCHAR2(200);
	NEW_LICENSURE_CONFIRM_FLAG VARCHAR2(200);
	NEW_ELIGIBLE_CONFIRM_FLAG VARCHAR2(200);
	NEW_FINAL_REVIEW_CONFIRM_FLAG VARCHAR2(200);
	NEW_CONFIRM_CBB_SIGN_FLAG VARCHAR2(200);
	newlicUsr VARCHAR2(200);
	neweligUsr VARCHAR2(200);
	NEWREVIEWUSR VARCHAR2(200);  
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM Dual;
	IF :NEW.LIC_CREATOR IS NOT NULL THEN
   BEGIN
   		SELECT GETUSER(:NEW.LIC_CREATOR)	INTO newlicUsr from dual;
   EXCEPTION WHEN OTHERS THEN
      		 newlicUsr := '';
   END;
  END IF;
  IF :NEW.ELIG_CREATOR  IS NOT NULL THEN
   BEGIN
   		SELECT GETUSER(:NEW.ELIG_CREATOR)	INTO neweligUsr from dual;
   EXCEPTION WHEN OTHERS THEN
      		 neweligUsr := '';
   END;
  END IF; 
  IF :NEW.FINAL_REVIEW_CREATOR IS NOT NULL THEN
   BEGIN
   		SELECT GETUSER(:NEW.FINAL_REVIEW_CREATOR)	INTO newreviewUsr from dual;
   EXCEPTION WHEN OTHERS THEN
      		 newreviewUsr := '';
   END;
  END IF; 	
	IF :NEW.LICENSURE_CONFIRM_FLAG  IS NOT NULL THEN
    BEGIN
   		SELECT DECODE(:NEW.LICENSURE_CONFIRM_FLAG,'1','Checked','0','Unchecked')		INTO NEW_LICENSURE_CONFIRM_FLAG  FROM DUAL;
		EXCEPTION WHEN OTHERS THEN
      		 NEW_LICENSURE_CONFIRM_FLAG := '';
	END;
    END IF;
	IF :NEW.ELIGIBLE_CONFIRM_FLAG  IS NOT NULL THEN
    BEGIN
   		SELECT DECODE(:NEW.ELIGIBLE_CONFIRM_FLAG,'1','Checked','0','Unchecked')		INTO NEW_ELIGIBLE_CONFIRM_FLAG  FROM DUAL;
		EXCEPTION WHEN OTHERS THEN
      		 NEW_ELIGIBLE_CONFIRM_FLAG := '';
   END;
    END IF;
	IF :NEW.FINAL_REVIEW_CONFIRM_FLAG IS NOT NULL THEN
    BEGIN
   		SELECT DECODE(:NEW.FINAL_REVIEW_CONFIRM_FLAG,'1','Checked','0','Unchecked')		INTO NEW_FINAL_REVIEW_CONFIRM_FLAG  FROM DUAL;
		EXCEPTION WHEN OTHERS THEN
      		 NEW_FINAL_REVIEW_CONFIRM_FLAG := '';
   END;
    END IF;
	IF :NEW.REVIEW_CORD_ACCEPTABLE_FLAG  IS NOT NULL THEN
   BEGIN
   		SELECT F_Codelst_Desc(:new.REVIEW_CORD_ACCEPTABLE_FLAG)		INTO NEW_CORD_ACCEPT_FLAG  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_CORD_ACCEPT_FLAG := '';
   END;       
  END IF;  
  IF :NEW.CONFIRM_CBB_SIGN_FLAG  IS NOT NULL THEN
    BEGIN
   		SELECT DECODE(:NEW.CONFIRM_CBB_SIGN_FLAG,1,'Checked',0,'Unchecked')		INTO NEW_CONFIRM_CBB_SIGN_FLAG  FROM DUAL;
		EXCEPTION WHEN OTHERS THEN
      		 NEW_CONFIRM_CBB_SIGN_FLAG := '';
   END;
    END IF;
	  --Inserting row in AUDIT_ROW_MODULE table.
	  PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid, 'CB_CORD_FINAL_REVIEW', :NEW.rid,:NEW.PK_CORD_FINAL_REVIEW,'I',:NEW.Creator);

	  IF :NEW.PK_CORD_FINAL_REVIEW IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','PK_CORD_FINAL_REVIEW',NULL,:NEW.PK_CORD_FINAL_REVIEW,NULL,NULL);
    END IF;
    IF :NEW.FK_CORD_ID IS NOT NULL THEN
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','FK_CORD_ID',NULL,:NEW.FK_CORD_ID,NULL,NULL);
    END IF;
    IF :NEW.LICENSURE_CONFIRM_FLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','LICENSURE_CONFIRM_FLAG',NULL,NEW_LICENSURE_CONFIRM_FLAG,NULL,NULL);
    END IF;
    IF :NEW.ELIGIBLE_CONFIRM_FLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','ELIGIBLE_CONFIRM_FLAG',NULL,NEW_ELIGIBLE_CONFIRM_FLAG,NULL,NULL);
    END IF;
    IF :NEW.FINAL_REVIEW_CONFIRM_FLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','FINAL_REVIEW_CONFIRM_FLAG',NULL,NEW_FINAL_REVIEW_CONFIRM_FLAG,NULL,NULL);
    END IF;
	IF :NEW.REVIEW_CORD_ACCEPTABLE_FLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','REVIEW_CORD_ACCEPTABLE_FLAG',NULL,NEW_CORD_ACCEPT_FLAG,NULL,NULL);
    END IF;
    IF :NEW.FK_ORDER_ID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','FK_ORDER_ID',NULL,:NEW.FK_ORDER_ID,NULL,NULL);
    END IF;
    IF :NEW.LIC_CREATOR IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','LIC_CREATOR',NULL,newlicUsr,NULL,NULL);
    END IF;
    IF :NEW.LIC_CREATED_ON IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','LIC_CREATED_ON',NULL,TO_CHAR(:NEW.LIC_CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
	END IF;
	IF :NEW.ELIG_CREATOR IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','ELIG_CREATOR',NULL,neweligUsr,NULL,NULL);
    END IF;
	IF :NEW.ELIG_CREATED_ON IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','ELIG_CREATED_ON',NULL,TO_CHAR(:NEW.ELIG_CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
	END IF;
	IF :NEW.FINAL_REVIEW_CREATOR IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','FINAL_REVIEW_CREATOR',NULL,newreviewUsr,NULL,NULL);
    END IF;
	IF :NEW.FINAL_REVIEW_CREATED_ON IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','FINAL_REVIEW_CREATED_ON',NULL,TO_CHAR(:NEW.FINAL_REVIEW_CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
	END IF;
    IF :NEW.CREATOR IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','CREATOR',NULL,:NEW.CREATOR,NULL,NULL);
    END IF;
    IF :NEW.CREATED_ON IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','CREATED_ON',NULL,TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF :NEW.LAST_MODIFIED_BY IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','LAST_MODIFIED_BY',NULL,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
    IF :NEW.LAST_MODIFIED_DATE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','LAST_MODIFIED_DATE',NULL,TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF :NEW.IP_ADD IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','IP_ADD',NULL,:NEW.IP_ADD,NULL,NULL);
    END IF;
    IF :NEW.DELETEDFLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','DELETEDFLAG',NULL,:NEW.DELETEDFLAG,NULL,NULL);
    END IF;
    IF :NEW.RID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CORD_FINAL_REVIEW','RID',NULL,:NEW.RID,NULL,NULL);
    END IF;
    IF :NEW.CONFIRM_CBB_SIGN_FLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_CORD_FINAL_REVIEW','CONFIRM_CBB_SIGN_FLAG',NULL,NEW_CONFIRM_CBB_SIGN_FLAG,NULL,NULL);
    END IF;
END;
/

INSERT INTO track_patches
VALUES(seq_track_patches.nextval,183,9,'09_CB_CORD_FINAL_REVIEW_AI0.sql',sysdate,'9.0.0 B#637-ET003');

commit;