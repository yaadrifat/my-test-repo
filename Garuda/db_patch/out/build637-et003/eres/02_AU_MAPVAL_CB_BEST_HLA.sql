Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Table','CB_BEST_HLA','BEST HLA');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.PK_BEST_HLA','Best HLA Id');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_HLA_CODE_ID','Bets HLA Code Id');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_HLA_METHOD_ID','Description');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.HLA_VALUE_TYPE1','HLA Type1');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.HLA_VALUE_TYPE2','HLA Type2');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.CREATOR','Creator');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.CREATED_ON','Created Date');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.LAST_MODIFIED_BY','Last Modified By');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.LAST_MODIFIED_DATE','Last Modified Date');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.IP_ADD','IP Address');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.DELETEDFLAG','Deleted Flag');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.RID','RID');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.ENTITY_ID','Cord Id');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.ENTITY_TYPE','Description');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_CORD_CDR_CBU_ID','CBU Id');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_HLA_OVERCLS_CODE','HLA class code');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.HLA_CRIT_OVER_DATE','HLA Critical override date');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.HLA_OVER_IND_ANTGN1','Antigen1 Indicator');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.HLA_OVER_IND_ANTGN2','Antigen2 Indicator');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_HLA_PRIM_DNA_ID','Primary DNA ID');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_HLA_PROT_DIPLOID','HLA ProteinDiplotypeListId');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_HLA_ANTIGENEID1','HLA ANTIGENEID1');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_HLA_ANTIGENEID2','HLA ANTIGENEID2');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.HLA_RECEIVED_DATE','Received Date');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.HLA_TYPING_DATE','Typing Date');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_HLA_REPORTING_LAB','Reporting LAB Reference');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_HLA_REPORT_METHOD','Description');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_ORDER_ID','Order Id');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_CORD_CT_STATUS','CT Status Id');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_SPEC_TYPE','Specimen Type');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_BEST_HLA.FK_SOURCE','Source');
INSERT INTO AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) VALUES (SEQ_AUDIT_MAPPEDVALUES.NEXTVAL,'MODCDRPF','Field','CB_BEST_HLA.HLA_TYPING_SEQ','Typing Seq');


commit;


INSERT INTO track_patches
VALUES(seq_track_patches.nextval,183,2,'02_AU_MAPVAL_CB_BEST_HLA.sql',sysdate,'9.0.0 B#637-ET003');

commit;