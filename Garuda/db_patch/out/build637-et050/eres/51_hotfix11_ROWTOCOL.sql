create or replace
FUNCTION        "ROWTOCOL" ( p_slct IN VARCHAR2,p_dlmtr IN VARCHAR2 DEFAULT ',' ) RETURN VARCHAR2 AUTHID CURRENT_USER AS

     TYPE c_refcur IS REF CURSOR;
     lc_str VARCHAR2(4000);
     lc_colval VARCHAR2(4000);
     temp_lc_str VARCHAR2(4000) :='hi';
     c_dummy c_refcur;
     l number;
    
     BEGIN
     OPEN c_dummy FOR p_slct;
     LOOP
     FETCH c_dummy INTO lc_colval;
     EXIT WHEN c_dummy%NOTFOUND;
     IF lc_colval!='Declines/No Race Selected' then
     lc_str := lc_str || p_dlmtr || lc_colval;
     else
     temp_lc_str := lc_colval;
     end if;
     END LOOP;
     if temp_lc_str = 'Declines/No Race Selected' then
     lc_str := lc_str || p_dlmtr || temp_lc_str;
     end if;
     CLOSE c_dummy;
     RETURN SUBSTR(lc_str,2);
     EXCEPTION
     WHEN OTHERS THEN
     lc_str := SQLERRM;
     IF c_dummy%ISOPEN THEN
     CLOSE c_dummy;
     END IF;
  
     RETURN lc_str;
     END;
/

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,275,51,'51_hotfix11_ROWTOCOL.sql',sysdate,'v1.3 (B#ET050.11)');

commit;