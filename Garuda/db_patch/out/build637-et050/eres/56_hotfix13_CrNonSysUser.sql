DECLARE
  v_record_exists NUMBER := 0;
  v_er_add_pk     NUMBER := 0;
BEGIN
  SELECT COUNT(*) INTO v_record_exists FROM Er_USER WHERE USR_LOGNAME ='emtrax';
  IF (v_record_exists = 0) THEN
   
    SELECT "EPAT".seq_er_add.nextval INTO v_er_add_pk FROM dual;
    INSERT INTO er_add
      ( PK_ADD
      ) VALUES
      ( v_er_add_pk
      );
    COMMIT;
    INSERT
    INTO Er_USER
      (
        PK_USER,
        -- FK_ACCOUNT,
        --  FK_SITEID,
        USR_LASTNAME,
        USR_FIRSTNAME,
        USR_LOGNAME,
        USR_PWD,
		CREATED_ON,
        USR_STAT,
        FK_PERADD,
        -- USR_CODE,
        USR_TYPE,
        USER_HIDDEN
      )
      VALUES
      (
        SEQ_ER_USER.NEXTVAL,
        --50,
        -- 545,
        'System',
        'EmTrax',
        'emtrax',
        'E02F09537BE2F241FE2DC886B198926050178E3768D9953499A832E2EF82D8BD',
		SYSDATE,
        'D',
        v_er_add_pk,
        'N',
        0
      );
    COMMIT;
  END IF;
END;
/

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,275,56,'56_hotfix13_CrNonSysUser.sql.sql',sysdate,'v1.3 (B#ET053.13)');
commit;

