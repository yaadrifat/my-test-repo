set define off

declare

/*******************
    Instrcutions: 
    
    This script is an example of how to add a new access in the middle of existing access rights. 
	
	This example is a new access rights inserted after: 'Eligibilty Summary Questions'.
	
    Execute this script in the ERES schema.
    
    1. initialize v_newacc_va  - with the ctrl_value for the new access right to be shown below Confirm Final Eligibilty Determination 
    
    2. initialize v_newaccdesc_va  - with the ctrl_desc for the new access right to be shown below Confirm Final Eligibilty Determination 
	
	3. initialize v_newaccseq_va - with the sequence of the new access right
	
	4. initialize v_newaccdefault_va - default access given to existing team for the new right
	
	5. initialize v_oldacc_va - with the sequence of the old access right after which new right is to be inserted

  *****************/

  v_count number ;

  v_change_made boolean :=false;
	
  type newacc_va is varray(50) of VARCHAR2(150);
  type oldacc_va is varray(50) of VARCHAR2(150);
  type newacc_vanum is varray(50) of Number;
  v_module varchar2(50);

  v_newacc_va newacc_va;
  v_newaccdesc_va newacc_va;
  v_newaccdefault_va newacc_va;
  v_oldacc_va oldacc_va;

  v_newaccseq_va newacc_vanum;

  v_null varchar2(10) := null ;

  

  v_eligquestion_seq number;
  v_grprights_index number;
  v_eligquestion_totalseq number;
  v_eligquestion_totalseq_g number;
  v_module_seq number;
  v_rights_str varchar2(200);

   
  v_exist_acc_count number;

  pCTX Plog.LOG_CTX := Plog.init (pSECTION => 'ACCESS_SCR', pLEVEL  => Plog.LDEBUG);

begin

     

    -- ctrl_value for new access right

    v_newacc_va :=newacc_va('CB_ELIGSUMQUEST');
	
	-- ctrl_value for old access right after  which new access right is to be added
	
	v_oldacc_va := oldacc_va('CB_FINALELIGBLE');


    -- ctrl_desc for new access right
    v_newaccdesc_va := newacc_va('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Eligibilty Summary Questions');
	
	
	--module to link the access right with

    v_module := 'MODCDRPF';

    --sequence within the rights page
	v_newaccseq_va:= newacc_vanum(13);
	
	-- Edit(2) and View(4) permission   assigned 
    v_newaccdefault_va := newacc_va('6');

    --check for existence of the first access right
    
    select count(*) into v_exist_acc_count from er_ctrltab 
    where CTRL_KEY = 'app_rights' and 
      CTRL_VALUE = v_newacc_va(1) ;
	  
	  -- fetching the index from er_grps table at which new rights string is to be inserted
	  
	 select count(*) into v_grprights_index from (select distinct ctrl_seq from er_ctrltab   
     where CTRL_KEY = 'app_rights' or  CTRL_KEY = 'hid_rights' or CTRL_KEY = 'acc_type_rights' order by ctrl_seq asc) 
     e where e.ctrl_seq<=(select ctrl_seq from er_ctrltab where ctrl_value='CB_FINALELIGBLE' and ctrl_key='app_rights');
	  

    plog.debug(pctx,'v_exist_acc_count:' || v_exist_acc_count);

    plog.debug(pctx,'v_newacc_va.first:' || v_newacc_va(1) );

    
    if v_exist_acc_count = 0 then
        
         select max(CTRL_SEQ)
		 into v_eligquestion_totalseq
		 from er_ctrltab   
		 where CTRL_KEY = 'app_rights' and CTRL_VALUE = v_oldacc_va(1);
		 
		 select max(CTRL_SEQ)
		 into v_eligquestion_totalseq_g
		 from er_ctrltab   
		 where CTRL_KEY = 'acc_type_rights' and CTRL_KEY = v_oldacc_va(1);
		 
		 select CTRL_SEQ 
         into v_module_seq  from er_ctrltab where CTRL_KEY = 'module' and 
         CTRL_VALUE = v_module;


         for k in v_newacc_va.first..v_newacc_va.last
             loop
			 
				v_eligquestion_seq := v_newaccseq_va(k);
			
				--plog.debug(pctx,'v_newacc_va.kt:' || v_newacc_va(k) );

				 update 	 ER_CTRLTAB
				 set CTRL_SEQ = CTRL_SEQ + 1
                 where CTRL_KEY = 'app_rights' and CTRL_SEQ > v_eligquestion_totalseq;
				 
				 update 	 ER_CTRLTAB
				 set CTRL_SEQ = CTRL_SEQ + 1
                 where CTRL_KEY = 'acc_type_rights' and CTRL_SEQ > v_eligquestion_totalseq;
				
				-- insert group rights
                v_eligquestion_seq := v_eligquestion_totalseq + 1 ;
				INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY, CTRL_SEQ )
				VALUES ( seq_er_ctrltab.nextval, v_newacc_va(k), v_newaccdesc_va(k), 'app_rights', v_eligquestion_seq );
				
				
				--------insert account type modules
				INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY, CTRL_SEQ )
				VALUES ( seq_er_ctrltab.nextval, 'G', v_newacc_va(k), 'acc_type_rights', v_eligquestion_seq );
				
				
				--insert into hide rights

				INSERT INTO ER_CTRLTAB ( PK_CTRLTAB, CTRL_VALUE, CTRL_DESC, CTRL_KEY,  CTRL_SEQ) VALUES ( 
				seq_er_ctrltab.nextval, v_newacc_va(k), v_module, 'hid_rights', v_module_seq);

				
				--right is inserted at the sequence of choice and move the rest to right
				
				UPDATE ER_GRPS
				set GRP_RIGHTS = substr(GRP_RIGHTS,1,(v_grprights_index)) || v_newaccdefault_va(k) || substr(GRP_RIGHTS,v_grprights_index+1)
				where rid is not null and GRP_RIGHTS is not null;

					
						 
      end loop;

        v_change_made := true;

    end if; -- if eligibilty rights are not setup
	
	if v_change_made  then

        COMMIT;
    end if;

end;
/


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,275,3,'03_ELIG_SUM_QUES_RIGHTS.sql',sysdate,'9.0.0 B#637-ET050');
commit;