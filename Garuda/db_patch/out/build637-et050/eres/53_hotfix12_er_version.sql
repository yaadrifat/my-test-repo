Declare
  v_patch_exists BOOLEAN;  
Begin
	V_Patch_Exists := F_Check_Previous_Patch('Ver 1.3 (Build#ET050.11)');
	If (V_Patch_Exists) Then
		UPDATE ER_CTRLTAB SET CTRL_VALUE = 'Ver 1.3 (Build#ET050.12)'
		where CTRL_KEY = 'app_version' ;
		commit;
		execute immediate 'INSERT INTO track_patches VALUES(seq_track_patches.nextval,275,53,''53_hotfix12_er_version.sql'',sysdate,''v1.3 (B#ET050.12)'')';
		commit;
		--dbms_output.put_line('Process successfully');
	Else
		--dbms_output.put_line('The database, where current patches are executed, is not updated to last the build : v9.0.0 Build#637-ET013.02');
		raise_application_error(-20001, 'The database, where current patches are executed, is not updated to last the build : Ver 1.3 (Build#ET050.11)');
	End if;
End;
/

