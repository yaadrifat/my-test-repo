--start update er_codelst table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst where codelst_type='qury_build_rprt' and codelst_subtyp='idsummaryrpt';
  If (V_Record_Exists = 1) Then
	update er_codelst set codelst_desc='NMDP CBU ID Summary Report' where codelst_type='qury_build_rprt' and codelst_subtyp='idsummaryrpt';
commit;
  end if;
end;
/
--END--

--start update er_codelst table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst where codelst_type='qury_build_rprt' and codelst_subtyp='antigenrpt';
  If (V_Record_Exists = 1) Then
	update er_codelst set codelst_desc='NMDP CBU Antigens Report' where codelst_type='qury_build_rprt' and codelst_subtyp='antigenrpt';
commit;
  end if;
end;
/
--END--


--start update er_codelst table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst where codelst_type='qury_build_rprt' and codelst_subtyp='licenselgblerpt';
  If (V_Record_Exists = 1) Then
	update er_codelst set codelst_desc='NMDP CBU Licensure And Eligibility Determination Report' where codelst_type='qury_build_rprt' and codelst_subtyp='licenselgblerpt';
commit;
  end if;
end;
/
--END--

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,269,5,'05_ER_CODELST_Update.sql',sysdate,'9.0.0 B#637-ET047');

commit;