set define off;
DECLARE
  COUNTFLAG NUMBER(5);
  COUNTFLAG1 number(5);

begin
    SELECT COUNT(*) INTO COUNTFLAG FROM ER_CODELST WHERE CODELST_TYPE='race' and CODELST_SUBTYP='FILII' and CODELST_DESC='Filipino (Pilipino)';
     if (COUNTFLAG = 1) then
     UPDATE ER_CODELST SET CODELST_DESC='Filipino (Philipino)' where CODELST_TYPE='race' and CODELST_SUBTYP='FILII';
commit;
END IF;
    SELECT COUNT(*) INTO COUNTFLAG1 FROM ER_CODELST WHERE CODELST_TYPE='race' and CODELST_SUBTYP='SCSEAI' and CODELST_DESC='Southease Asian';
     if (COUNTFLAG1 = 1) then
     UPDATE ER_CODELST SET CODELST_DESC='Southeast Asian' where CODELST_TYPE='race' and CODELST_SUBTYP='SCSEAI';
commit;
END IF;
end;
/

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,215,8,'08_ER_CODELST_INSERT.sql',sysdate,'9.0.0 B#637-ET019');
		commit;