DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'cord_status'
    AND codelst_subtyp = 'RO';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_DESC='Reserved for Other(RO)' where CODELST_TYPE='cord_status' and CODELST_SUBTYP='RO';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'cord_status'
    AND codelst_subtyp = 'RSO';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_DESC='Reserved for Other(RSO)' where CODELST_TYPE='cord_status' and CODELST_SUBTYP='RSO';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'cord_status'
    AND codelst_subtyp = 'CC';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_DESC='Unknown' where CODELST_TYPE='cord_status' and CODELST_SUBTYP='CC';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'store_sam_invt'
    AND codelst_subtyp = 'mnovia';
  if (v_column_exists > 1) then
delete from ER_CODELST where CODELST_TYPE='store_sam_invt' and CODELST_SUBTYP='mnovia';
  end if;
end;
/

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='store_sam_invt'
    AND CODELST_SUBTYP = 'mnovia';
  if (v_record_exists = 0) then
      INSERT INTO 
		ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
		CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
		CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
		(SEQ_ER_CODELST.nextval,null,'store_sam_invt','mnovia','Maternal Cell Aliquots','N',4,'Y',null,
		null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,215,10,'10_hotfix1_Codelst_Update.sql',sysdate,'9.0.0 B#637-ET019.01');
		commit;