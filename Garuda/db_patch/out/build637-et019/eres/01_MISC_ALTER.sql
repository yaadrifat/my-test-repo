DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'CB_CORD_IMPORT_HISTORY'  AND column_name = 'FK_SITE_ID';
   if (countFlage = 0) then
 execute immediate ('alter table CB_CORD_IMPORT_HISTORY add FK_SITE_ID number(10)');
  end if;
end;
/
 COMMENT ON COLUMN CB_CORD_IMPORT_HISTORY.FK_SITE_ID IS 'Referenece for ER_SITE table'; 
commit;

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,215,1,'01_MISC_ALTER.sql',sysdate,'9.0.0 B#637-ET019');
		commit;