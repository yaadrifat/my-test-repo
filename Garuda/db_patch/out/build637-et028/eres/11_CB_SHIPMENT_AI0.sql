create or replace
TRIGGER "ERES"."CB_SHIPMENT_AI0" AFTER INSERT ON
CB_SHIPMENT
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
	V_ROWID NUMBER(10);
  NEW_PAPERWORKLOC VARCHAR(200);
   NEW_SHIPCOMPANY VARCHAR(200);
   NEW_TEMPMONITOR VARCHAR(200);
   NEW_SHIPCONT VARCHAR(200);
   NEW_FK_SHIPMENT_TYPE VARCHAR(200);
   NEW_SHIPMENT_CONFIRM_BY VARCHAR(500);
BEGIN
     SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO V_ROWID FROM DUAL;
     IF :NEW.PAPERWORK_LOCATION is not null THEN
    BEGIN
        SELECT F_Codelst_Desc(:new.PAPERWORK_LOCATION)		INTO NEW_PAPERWORKLOC  from dual;
     EXCEPTION WHEN OTHERS THEN
           NEW_PAPERWORKLOC := '';
     END;
    END IF;

      IF :NEW.FK_SHIP_COMPANY_ID is not null THEN
    BEGIN
        SELECT F_Codelst_Desc(:new.FK_SHIP_COMPANY_ID)		INTO NEW_SHIPCOMPANY  from dual;
     EXCEPTION WHEN OTHERS THEN
           NEW_SHIPCOMPANY := '';
     END;
    END IF;

    IF :NEW.FK_TEMPERATURE_MONITER is not null THEN
    BEGIN
        SELECT F_Codelst_Desc(:new.FK_TEMPERATURE_MONITER)		INTO NEW_TEMPMONITOR  from dual;
     EXCEPTION WHEN OTHERS THEN
           NEW_TEMPMONITOR := '';
     END;
    END IF;

    IF :NEW.FK_SHIPPING_CONT is not null THEN
    BEGIN
        SELECT F_Codelst_Desc(:new.FK_SHIPPING_CONT)		INTO NEW_SHIPCONT  from dual;
     EXCEPTION WHEN OTHERS THEN
           NEW_SHIPCONT := '';
     END;
    END IF;

	  PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (V_ROWID, 'CB_SHIPMENT', :NEW.RID,:NEW.PK_SHIPMENT,'I',:NEW.CREATOR);
	IF :NEW.PK_SHIPMENT IS NOT NULL THEN
	  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_SHIPMENT','PK_SHIPMENT',NULL,:NEW.PK_SHIPMENT,NULL,NULL);
    END IF;
    IF :NEW.FK_ORDER_ID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_SHIPMENT','FK_ORDER_ID',NULL,:NEW.FK_ORDER_ID,NULL,NULL);
    END IF;
   IF :NEW.SHIPMENT_DATE IS NOT NULL THEN
  PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','SHIPMENT_DATE',NULL,TO_CHAR(:NEW.SHIPMENT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF :NEW.ADDI_SHIPMENT_DATE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_SHIPMENT','ADDI_SHIPMENT_DATE',NULL,TO_CHAR(:NEW.ADDI_SHIPMENT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF :NEW.SHIPMENT_TRACKING_NO IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_SHIPMENT','SHIPMENT_TRACKING_NO',NULL,:NEW.SHIPMENT_TRACKING_NO,NULL,NULL);
    END IF;
    IF :NEW.ADDI_SHIPMENT_TRACKING_NO IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_SHIPMENT','ADDI_SHIPMENT_TRACKING_NO',NULL,:NEW.ADDI_SHIPMENT_TRACKING_NO,NULL,NULL);
    END IF;
	IF :NEW.CBB_ID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','CBB_ID',NULL,:NEW.CBB_ID,NULL,NULL);
    END IF;
	IF :NEW.CREATOR IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','CREATOR',NULL,:NEW.CREATOR,NULL,NULL);
    END IF;
	IF :NEW.CREATED_ON IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','CREATED_ON',NULL,TO_CHAR(:NEW.CREATED_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
	IF :NEW.IP_ADD IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','IP_ADD',NULL,:NEW.IP_ADD,NULL,NULL);
    END IF;
	IF :NEW.LAST_MODIFIED_BY IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','LAST_MODIFIED_BY',NULL,:NEW.LAST_MODIFIED_BY,NULL,NULL);
    END IF;
	IF :NEW.LAST_MODIFIED_DATE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','LAST_MODIFIED_DATE',NULL,TO_CHAR(:NEW.LAST_MODIFIED_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
	IF :NEW.RID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','RID',NULL,:NEW.RID,NULL,NULL);
    END IF;
	IF :NEW.DELETEDFLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','DELETEDFLAG',NULL,:NEW.DELETEDFLAG,NULL,NULL);
    END IF;
	IF :NEW.FK_CORD_ID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','FK_CORD_ID',NULL,:NEW.FK_CORD_ID,NULL,NULL);
    END IF;
    IF :NEW.PROP_SHIPMENT_DATE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','PROP_SHIPMENT_DATE',NULL,TO_CHAR(:NEW.PROP_SHIPMENT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF :NEW.ATTN_NAME IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','ATTN_NAME',NULL,:NEW.ATTN_NAME,NULL,NULL);
    END IF;
	IF :NEW.FK_DELIVERY_ADD IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','FK_DELIVERY_ADD',NULL,:NEW.FK_DELIVERY_ADD,NULL,NULL);
    END IF;
	IF :NEW.ADDI_INFO IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','ADDI_INFO',NULL,:NEW.ADDI_INFO,NULL,NULL);
    END IF;
	IF :NEW.SCH_SHIPMENT_DATE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','SCH_SHIPMENT_DATE',NULL,TO_CHAR(:NEW.SCH_SHIPMENT_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
	IF :NEW.FK_SHIPPING_CONT IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','FK_SHIPPING_CONT',NULL,NEW_SHIPCONT,NULL,NULL);
    END IF;
	IF :NEW.FK_TEMPERATURE_MONITER IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','FK_TEMPERATURE_MONITER',NULL,NEW_TEMPMONITOR,NULL,NULL);
    END IF;
	IF :NEW.EXPECT_ARR_DATE IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_SHIPMENT','EXPECT_ARR_DATE',NULL,TO_CHAR(:NEW.EXPECT_ARR_DATE,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
	IF :NEW.SHIPMENT_CONFIRM IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','SHIPMENT_CONFIRM',NULL,:NEW.SHIPMENT_CONFIRM,NULL,NULL);
    END IF;
	IF :NEW.SHIPMENT_CONFIRM_BY IS NOT NULL THEN
       BEGIN
   		SELECT f_getuser(:NEW.SHIPMENT_CONFIRM_BY)	INTO NEW_SHIPMENT_CONFIRM_BY FROM DUAL;
   EXCEPTION WHEN OTHERS THEN
      		 NEW_SHIPMENT_CONFIRM_BY := '';
   END;
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','SHIPMENT_CONFIRM_BY',NULL,NEW_SHIPMENT_CONFIRM_BY,NULL,NULL);
    END IF;
	IF :NEW.SHIPMENT_CONFIRM_ON IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','SHIPMENT_CONFIRM_ON',NULL,TO_CHAR(:NEW.SHIPMENT_CONFIRM_ON,PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
	IF :NEW.FK_SHIP_COMPANY_ID IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','FK_SHIP_COMPANY_ID',NULL,NEW_SHIPCOMPANY,NULL,NULL);
    END IF;
	IF :NEW.PADLOCK_COMBINATION IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','PADLOCK_COMBINATION',NULL,:NEW.PADLOCK_COMBINATION,NULL,NULL);
    END IF;
	IF :NEW.PAPERWORK_LOCATION IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','PAPERWORK_LOCATION',NULL,NEW_PAPERWORKLOC,NULL,NULL);
    END IF;
	IF :NEW.ADDITI_SHIPPER_DET IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','ADDITI_SHIPPER_DET',NULL,:NEW.ADDITI_SHIPPER_DET,NULL,NULL);
    END IF;
	IF :NEW.FK_SHIPMENT_TYPE IS NOT NULL THEN
      BEGIN
        SELECT F_Codelst_Desc(:new.FK_SHIPMENT_TYPE)		INTO NEW_FK_SHIPMENT_TYPE  from dual;
     EXCEPTION WHEN OTHERS THEN
           NEW_FK_SHIPMENT_TYPE := '';
     END;
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','FK_SHIPMENT_TYPE',NULL,NEW_FK_SHIPMENT_TYPE,NULL,NULL);
    END IF;
	IF :NEW.NMDP_SHIP_EXCUSE_FLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','NMDP_SHIP_EXCUSE_FLAG',NULL,:NEW.NMDP_SHIP_EXCUSE_FLAG,NULL,NULL);
    END IF;
	IF :NEW.DATA_MODIFIED_FLAG IS NOT NULL THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID, 'CB_SHIPMENT','DATA_MODIFIED_FLAG',NULL,:NEW.DATA_MODIFIED_FLAG,NULL,NULL);
    END IF;
END;
/
INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,233,11,'11_CB_SHIPMENT_AI0.sql',sysdate,'9.0.0 B#637-ET028');
commit;