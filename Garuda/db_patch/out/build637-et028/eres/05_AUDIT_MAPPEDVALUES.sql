set define off;
update audit_mappedvalues set to_value= 'Shipment Packet Printed' where mapping_type = 'Field' and from_value = 'ER_ORDER.PACKAGE_SLIP_FLAG';
update audit_mappedvalues set to_value= 'Previously CT''d by NMDP' where mapping_type = 'Field' and from_value = 'ER_ORDER.PREV_CT_DONE_FLAG';
COMMIT;

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Table' and FROM_VALUE = 'CB_CORD_COMPLETE_REQ_INFO';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Table','CB_CORD_COMPLETE_REQ_INFO','CRI');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_CORD_COMPLETE_REQ_INFO.COMPLETE_REQ_INFO_FLAG';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_CORD_COMPLETE_REQ_INFO.COMPLETE_REQ_INFO_FLAG','Complete Required Information');
	commit;
  end if;
end;
/

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,233,5,'05_AUDIT_MAPPEDVALUES.sql',sysdate,'9.0.0 B#637-ET028');
commit;