DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'site_type'
    AND codelst_subtyp in ('cbb','tc');
	if (v_column_exists = 2) then
		Update ER_CODELST set CODELST_HIDE='N' where CODELST_TYPE='site_type' and CODELST_SUBTYP in ('cbb','tc');
		commit;
	end if;
end;
/
		INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,225,24,'24_hotfix2_er_codelst_update.sql',sysdate,'9.0.0 B#637-ET024.02');
		commit;