
alter TYPE "SITEREC_RECORD" modify attribute  sitename VARCHAR2(100) cascade

INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,225,4,'04_MISC_ALTER.sql',sysdate,'9.0.0 B#637-ET024');
		commit;
