create or replace
PACKAGE        "PKG_DATEFORMAT" AS
/******************************************************************************
   NAME:       PKG_DATEUTIL
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        1/30/2009             1. Created this package.
******************************************************************************/

  --DATE_FORMAT VARCHAR2(200) := 'dd-MON-YYYY';
   DATE_FORMAT VARCHAR2(200) := 'mm/dd/yyyy';

  --default value for empty date. usually we start the null dates from 1/1/1900. set this value according to the date format field
  DEFAULT_FOR_NULL_DATE VARCHAR2(200) := '01/01/1900';

  TIME_FORMAT varchar2(20) := 'HH24:mi:ss';

  --default value for empty future date. usually this date is set for 01/01/3000. set this value according to the date format field
  DEFAULT_FOR_FUTURE_NULL_DATE VARCHAR2(200) := '01/01/3000';
  
   --DATE_FORMAT_UI VARCHAR2(200) := 'dd-MON-YYYY';
   DATE_FORMAT_UI VARCHAR2(200) := 'Mon DD, YYYY HH:MI:SS AM';


END PKG_DATEFORMAT;
/


INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,225,08,'08_PKG_DATEFORMAT.sql',sysdate,'9.0.0 B#637-ET024');
		commit;