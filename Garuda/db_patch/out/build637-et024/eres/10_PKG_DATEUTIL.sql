create or replace
PACKAGE BODY        "PKG_DATEUTIL" IS

FUNCTION f_get_dateformat  return Varchar2
IS
   v_df varchar2(200);
    begin
        v_df := PKG_DATEFORMAT.DATE_FORMAT;

        if (v_df is null or length(v_df)=0    ) then

            v_df := 'mm/dd/yyyy';
        end if;

        return v_df;
    END;


  FUNCTION f_get_timeformat  return Varchar2
  IS
   v_tf varchar2(200);
    begin
        v_tf := PKG_DATEFORMAT.TIME_FORMAT;

        if (v_tf is null or length(v_tf)=0    ) then

            v_tf := 'HH24:mi:ss';
        end if;

        return v_tf;
    END;



  FUNCTION f_get_datetimeformat  return Varchar2
  IS
    v_dtf varchar2(200);
  Begin

        select f_get_dateformat || ' ' || f_get_timeformat
        into v_dtf
        from dual;

        return  v_dtf;

  end;



 FUNCTION f_get_null_date_str  return Varchar2
IS
   v_d varchar2(200);
    begin
        v_d := PKG_DATEFORMAT.DEFAULT_FOR_NULL_DATE;

        if (v_d is null or length(v_d)=0    ) then

            v_d := '01/01/1900';
        end if;

        return v_d;
    END;


    function f_get_first_dayofyear  return Date
IS
   v_d date;
   v_def_dateformat varchar2(50);
   v_fdate_str varchar2(50);

    begin

         select f_get_dateformat()
         into  v_def_dateformat
         from dual;

        select f_get_first_dayofyear_str into v_fdate_str from dual;


      ---select to_date(v_fdate_str,v_def_dateformat) into v_d from dual ;
      ---return v_d;
         return to_date(v_fdate_str, v_def_dateformat);

    END;

    FUNCTION f_get_first_dayofyear_str return Varchar
       IS
       v_def_dateformat varchar2(50);
       v_fdate_str varchar2(50);
        begin

         select lower(f_get_dateformat())
         into  v_def_dateformat
         from dual;

         v_fdate_str := v_def_dateformat;

         v_fdate_str  := replace(v_fdate_str ,'dd','01');
         v_fdate_str  := replace(v_fdate_str ,'mm','01');
         v_fdate_str  := replace(v_fdate_str ,'mon','JAN');
         v_fdate_str := replace(v_fdate_str ,'yyyy',TO_CHAR (SYSDATE, 'yyyy') );

         return v_fdate_str;


    END;



FUNCTION f_get_future_null_date_str  return Varchar2
IS
   v_d varchar2(200);
    begin
        v_d := PKG_DATEFORMAT.DEFAULT_FOR_FUTURE_NULL_DATE;

        if (v_d is null or length(v_d)=0    ) then

            v_d := '01/01/3000';
        end if;

        return v_d;
    END;
FUNCTION f_get_dateformat_ui  return Varchar2
IS
   v_df1 varchar2(200);
    BEGIN
        v_df1 := PKG_DATEFORMAT.DATE_FORMAT_UI;

        if (v_df1 is null or length(v_df1)=0    ) then

            v_df1 := 'Mon DD, YYYY HH:MI:SS AM';
        end if;

        RETURN V_DF1;
    END;

END PKG_DATEUTIL;
/


INSERT INTO track_patches
		VALUES(seq_track_patches.nextval,225,10,'10_PKG_DATEUTIL.sql',sysdate,'9.0.0 B#637-ET024');
		commit;