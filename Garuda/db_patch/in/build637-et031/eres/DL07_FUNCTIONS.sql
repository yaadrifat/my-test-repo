CREATE OR REPLACE FUNCTION F_TC_OR_ORDER_DETAILS(ORDERID NUMBER) RETURN SYS_REFCURSOR IS
TC_OR_ORDER SYS_REFCURSOR;
BEGIN
OPEN TC_OR_ORDER FOR
SELECT
f_codelst_desc(rec.ind_protocol) IND_PROTOCOL,
NVL(TO_CHAR(rec.ind_sponser),'Not Provided') IND_SPON,
NVL(TO_CHAR(rec.ind_number),'Not Provided') IND_NO,
NVL(rec.pre_current_diagnosis,'Not Provided') CURRENT_DIAGNOSIS,
CASE 
WHEN rec.SINGLE_UNIT_TRANSPLANT IS NOT NULL AND rec.SINGLE_UNIT_TRANSPLANT='Y' THEN 'Yes'
WHEN rec.SINGLE_UNIT_TRANSPLANT IS NOT NULL AND rec.SINGLE_UNIT_TRANSPLANT='N' THEN 'No'
WHEN rec.SINGLE_UNIT_TRANSPLANT IS NULL  THEN 'Not Provided'
END SINGLE_UNIT_TRANSPLANT,
CASE
WHEN rec.MULTI_TRANSPLANT IS NOT NULL AND rec.MULTI_TRANSPLANT='Y' THEN 'Yes'
WHEN rec.MULTI_TRANSPLANT IS NOT NULL AND rec.MULTI_TRANSPLANT='N' THEN 'No'
WHEN rec.MULTI_TRANSPLANT IS NULL  THEN 'Not Provided'
END MULTI_TRANSPLANT,
CASE
WHEN rec.OTHER_TRANSPLANT IS NOT NULL AND rec.OTHER_TRANSPLANT='Y' THEN 'Yes'
WHEN rec.OTHER_TRANSPLANT IS NOT NULL AND rec.OTHER_TRANSPLANT='N' THEN 'No'
WHEN rec.OTHER_TRANSPLANT IS NULL  THEN 'Not Provided'
END OTHER_TRANSPLANT,
CASE
WHEN rec.EX_VIVO_TRANSPLANT IS NOT NULL AND rec.EX_VIVO_TRANSPLANT='Y' THEN 'Yes'
WHEN rec.EX_VIVO_TRANSPLANT IS NOT NULL AND rec.EX_VIVO_TRANSPLANT='N' THEN 'No'
WHEN rec.EX_VIVO_TRANSPLANT IS NULL  THEN 'Not Provided'
END EX_VIVO_TRANSPLANT,
NVL(TO_CHAR(rec.disease_stage),'Not Provided') DISIEASE_STATGE,
NVL(TO_CHAR(REC.NO_OF_REMISSIONS),'Not Provided') NO_OF_REMISSIONS,
NVL(TO_CHAR(rec.fk_leu_curr_status),'Not Provided') CURRENT_STAT_LEUCUMA,
NVL(TO_CHAR(rec.rec_transfused),'Not Provided') REC_TRANSFUSED,
NVL(TO_CHAR(rec.rec_weight),'Not Provided') REC_WEIGHT,
NVL(TO_CHAR(rec.prop_infusion_date,'Mon DD, YYYY'),'Not Provided') prop_infusion_date,
NVL(TO_CHAR(rec.prop_prep_date,'Mon DD, YYYY'),'Not Provided') prop_prep_date,
CASE 
WHEN rec.res_cel_sam_flag IS NOT NULL AND rec.res_cel_sam_flag='Y' THEN 'Yes' 
WHEN rec.res_cel_sam_flag IS NOT NULL AND rec.res_cel_sam_flag='N' THEN 'No' 
WHEN rec.res_cel_sam_flag IS NULL THEN 'Not Provided'
END RES_CEL_SAM_FLAG,
CASE
    WHEN per.person_fname
      ||' '
      ||per.person_lname=' '
    THEN 'Not Provided'
    WHEN per.person_fname IS NOT NULL
    AND (per.person_lname IS NULL
    OR per.person_lname    ='')
    THEN per.person_fname
    WHEN per.person_lname IS NOT NULL
    AND (per.person_fname IS NULL
    OR per.person_fname    ='')
    THEN per.person_lname
    ELSE per.person_fname
      ||' '
      ||per.person_lname
  END attn_name,
  NVL(trans2.site_name,'Not Provided') CENTRE_NAME,
  NVL(per.person_address1,'Not Provided') ADDR1,
  NVL(per.person_address2,'Not Provided') ADDR2,
  NVL(per.person_city,'Not Provided') CITY,
  NVL(per.person_state,'Not Provided') STATE,
  NVL(TO_CHAR(per.person_zip),'Not Provided') ZIPCODE,
  NVL(per.person_country,'Not Provided') country,
  NVL(per.person_notes,'Not Provided') ADDCOMMENT,
    CASE
    WHEN p1.person_fname
      ||' '
      ||p1.person_lname=' '
    THEN 'Not Provided'
    WHEN p1.person_fname IS NOT NULL
    AND (p1.person_lname IS NULL
    OR p1.person_lname    ='')
    THEN p1.person_fname
    WHEN p1.person_lname IS NOT NULL
    AND (p1.person_fname IS NULL
    OR p1.person_fname    ='')
    THEN p1.person_lname
    ELSE p1.person_fname
      ||' '
      ||p1.person_lname
  END contact1_name,
  NVL(TO_CHAR(p1.person_hphone),'Not Provided') contact1_hphone,
  NVL(TO_CHAR(p1.person_bphone),'Not Provided') contact1_bphone,
  NVL(P1.PERSON_FAX,'Not Provided') CONTACT1_FAX,
  CASE
    WHEN p2.person_fname
      ||' '
      ||p2.person_lname=' '
    THEN 'Not Provided'
    WHEN p2.person_fname IS NOT NULL
    AND (p2.person_lname IS NULL
    OR p2.person_lname    ='')
    THEN p2.person_fname
    WHEN p2.person_lname IS NOT NULL
    AND (p2.person_fname IS NULL
    OR p2.person_fname    ='')
    THEN p2.person_lname
    ELSE p2.person_fname
      ||' '
      ||p2.person_lname
  END contact2_name,
  NVL(TO_CHAR(p2.person_hphone),'Not Provided') contact2_hphone,
  NVL(TO_CHAR(p2.person_bphone),'Not Provided') contact2_bphone,
  NVL(P2.PERSON_FAX,'Not Provided') CONTACT2_FAX,
  CASE
    WHEN p3.person_fname
      ||' '
      ||p3.person_lname=' '
    THEN 'Not Provided'
    WHEN p3.person_fname IS NOT NULL
    AND (p3.person_lname IS NULL
    OR p3.person_lname    ='')
    THEN p3.person_fname
    WHEN p3.person_lname IS NOT NULL
    AND (p3.person_fname IS NULL
    OR p3.person_fname    ='')
    THEN p3.person_lname
    ELSE p3.person_fname
      ||' '
      ||p3.person_lname
  END contact3_name,
  NVL(TO_CHAR(p3.person_hphone),'Not Provided') contact3_hphone,
  NVL(TO_CHAR(p3.person_bphone),'Not Provided') contact3_bphone,
  NVL(P3.PERSON_FAX,'Not Provided') CONTACT3_FAX,
  NVL(TO_CHAR(ordhdr.order_open_date,'Mon DD, YYYY'),'Not Provided') order_date,
  CASE
    WHEN phy.person_fname
      ||' '
      ||phy.person_lname=' '
    THEN 'Not Provided'
    WHEN phy.person_fname IS NOT NULL
    AND (phy.person_lname IS NULL
    OR phy.person_lname    ='')
    THEN phy.person_fname
    WHEN phy.person_lname IS NOT NULL
    AND (phy.person_fname IS NULL
    OR phy.person_fname    ='')
    THEN phy.person_lname
    ELSE phy.person_fname
      ||' '
      ||phy.person_lname
  END order_physician
FROM er_order_header ordhdr
LEFT OUTER JOIN er_order ord
ON(ord.fk_order_header=ordhdr.pk_order_header)
LEFT OUTER JOIN
  (SELECT RECEIPANT_ID,
    FK_CONTACT_PERSON,
    TRANS_CENTER_ID,
    NO_OF_REMISSIONS,
    pre.REC_WEIGHT,
    FK_RECEIPANT_CONTACT1,
    FK_RECEIPANT_CONTACT2,
    FK_RECEIPANT_CONTACT3,
    REC_HEIGHT,
    SEC_TRANS_CENTER_ID,
    ORDER_PHYSICIAN,
    FK_RECEIPANT,
    FK_ORDER_ID,
    IND_PROTOCOL,
    IND_SPONSER,
    IND_NUMBER,
    pre.CURRENT_DIAGNOSIS pre_current_diagnosis,
    re.CURRENT_DIAGNOSIS re_current_diagnosis,
    pre.DISEASE_STAGE,
    FK_TRANSPLANT_TYPE,
    FK_LEU_CURR_STATUS,
    REC_TRANSFUSED,
    PROP_INFUSION_DATE,
    PROP_PREP_DATE,
    RES_CEL_SAM_FLAG,
    EX_VIVO_TRANSPLANT,
    MULTI_TRANSPLANT,
    OTHER_TRANSPLANT,
    SINGLE_UNIT_TRANSPLANT,
    PROPOSED_SHIP_DTE,
    FK_PERSON_ID,
    pre.PRODUCT_DELIVERY_TC_NAME,
    re.pk_receipant pk_receipant
  FROM er_order_receipant_info pre,
    cb_receipant_info re
  WHERE pre.fk_receipant=re.pk_receipant
  ) rec
ON(rec.fk_order_id=ord.pk_order)
LEFT OUTER JOIN EPAT.person per
ON(rec.FK_CONTACT_PERSON=per.pk_person)
LEFT OUTER JOIN EPAT.person p1
ON(rec.fk_receipant_contact1=p1.pk_person)
LEFT OUTER JOIN er_site trans2
ON(rec.PRODUCT_DELIVERY_TC_NAME=trans2.pk_site)
LEFT OUTER JOIN er_site trans
ON(rec.trans_center_id=trans.pk_site)
LEFT OUTER JOIN er_site trans1
ON(rec.sec_trans_center_id=trans1.pk_site)
LEFT OUTER JOIN EPAT.person p2
ON(rec.fk_receipant_contact2=p2.pk_person)
LEFT OUTER JOIN EPAT.person p3
ON(rec.fk_receipant_contact3=p3.pk_person)
LEFT OUTER JOIN er_order_users ordusrs
ON(ordhdr.ORDER_REQUESTED_BY=ordusrs.PK_ORDER_USER)
LEFT OUTER JOIN er_user modifiedby
ON(ord.LAST_MODIFIED_BY=modifiedby.pk_user)
LEFT OUTER JOIN epat.person phy
ON (rec.ORDER_PHYSICIAN=phy.pk_person)
LEFT OUTER JOIN epat.person pat
ON (REC.FK_PERSON_ID=PAT.PK_PERSON)
LEFT OUTER JOIN er_user viewby
ON(ord.ORDER_LAST_VIEWED_BY=viewby.pk_user)
WHERE ord.pk_order         =ORDERID; 
RETURN TC_OR_ORDER;
END;
/

