DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_CORD_MINIMUM_CRITERIA.REVIEWED_BY' and  TO_VALUE='Reviewed By' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_CORD_MINIMUM_CRITERIA.REVIEWED_BY', 'Reviewed By');

commit;
   end if;
 End;
 /
 
 
 
 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_CORD_MINIMUM_CRITERIA.SECOND_REVIEW_BY' and  TO_VALUE='Second Review By' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_CORD_MINIMUM_CRITERIA.SECOND_REVIEW_BY', 'Second Review By');

commit;
   end if;
 End;
 /





DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE1_LOCUS_A' and  TO_VALUE='HLA-A, Type 1' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE1_LOCUS_A', 'HLA-A, Type 1');

COMMIT;
   end if;
 End;
 /
 
 
 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE2_LOCUS_A' and  TO_VALUE='HLA-A, Type 2' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE2_LOCUS_A', 'HLA-A, Type 2');

COMMIT;
   end if;
 End;
 /
 
 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE1_LOCUS_B' and  TO_VALUE='HLA-B, Type 1' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE1_LOCUS_B', 'HLA-B, Type 1');

COMMIT;
   end if;
 End;
 /
 
 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE2_LOCUS_B' and  TO_VALUE='HLA-B, Type 2' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE2_LOCUS_B', 'HLA-B, Type 2');

COMMIT;
   end if;
 End;
 /
 
 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE1_LOCUS_C' and  TO_VALUE='HLA-C, Type 1' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE1_LOCUS_C', 'HLA-C, Type 1');

COMMIT;
   end if;
 End;
 /
 
 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE2_LOCUS_C' and  TO_VALUE='HLA-C, Type 2' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE2_LOCUS_C', 'HLA-C, Type 2');

COMMIT;
   end if;
 End;
 /
 
 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE1_LOCUS_DRB1' and  TO_VALUE='HLA-DRB1, Type 1' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE1_LOCUS_DRB1', 'HLA-DRB1, Type 1');

COMMIT;
   end if;
 End;
 /

 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE2_LOCUS_DRB1' and  TO_VALUE='HLA-DRB1, Type 2' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE2_LOCUS_DRB1', 'HLA-DRB1, Type 2');

COMMIT;
   end if;
 End;
 / 

 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE1_LOCUS_DRB3' and  TO_VALUE='HLA-DRB3, Type 1' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE1_LOCUS_DRB3', 'HLA-DRB3, Type 1');

COMMIT;
   end if;
 End;
 /

 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE2_LOCUS_DRB3' and  TO_VALUE='HLA-DRB3, Type 2' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE2_LOCUS_DRB3', 'HLA-DRB3, Type 2');

COMMIT;
   end if;
 End;
 / 

 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE1_LOCUS_DRB4' and  TO_VALUE='HLA-DRB4, Type 1' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE1_LOCUS_DRB4', 'HLA-DRB4, Type 1');

COMMIT;
   end if;
 End;
 /
 
 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE2_LOCUS_DRB4' and  TO_VALUE='HLA-DRB4, Type 2' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE2_LOCUS_DRB4', 'HLA-DRB4, Type 2');

COMMIT;
   end if;
 End;
 / 

 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE1_LOCUS_DRB5' and  TO_VALUE='HLA-DRB5, Type 1' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE1_LOCUS_DRB5', 'HLA-DRB5, Type 1');

COMMIT;
   end if;
 End;
 /

  DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE2_LOCUS_DRB5' and  TO_VALUE='HLA-DRB5, Type 2' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE2_LOCUS_DRB5', 'HLA-DRB5, Type 2');

COMMIT;
   end if;
 End;
 /
 
 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE1_LOCUS_DQB1' and  TO_VALUE='HLA-DQB1, Type 1' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE1_LOCUS_DQB1', 'HLA-DQB1, Type 1');

COMMIT;
   end if;
 End;
 /

 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE2_LOCUS_DQB1' and  TO_VALUE='HLA-DQB1, Type 2' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE2_LOCUS_DQB1', 'HLA-DQB1, Type 2');

COMMIT;
   end if;
 End;
 / 

 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE1_LOCUS_DPB1' and  TO_VALUE='HLA-DPB1, Type 1' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE1_LOCUS_DPB1', 'HLA-DPB1, Type 1');

COMMIT;
   end if;
 End;
 /

 DECLARE
 v_record_exists number := 0;
 Begin 
  Select count(*) into v_record_exists From AUDIT_MAPPEDVALUES  where MAPPING_TYPE='Field' and FROM_VALUE='CB_HLA.HLA_VALUE_TYPE2_LOCUS_DPB1' and  TO_VALUE='HLA-DPB1, Type 2' ; 
  if (v_record_exists = 0) then 

Insert into AUDIT_MAPPEDVALUES 
(PK_AUDITMAPPEDVALUE, MODULE_NAME, MAPPING_TYPE, FROM_VALUE, TO_VALUE)
 values 
((Select Max(PK_AUDITMAPPEDVALUE)+1 from AUDIT_MAPPEDVALUES), 
'MODCDRPF','Field','CB_HLA.HLA_VALUE_TYPE2_LOCUS_DPB1', 'HLA-DPB1, Type 2');

COMMIT;
   end if;
 END;
 /
 
INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,280,3,'03_AuditMappedValue_Insert.sql',sysdate,'9.0.0 B#637-ET052');
commit;

 
 
 
 /