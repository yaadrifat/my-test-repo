Declare
  v_patch_exists BOOLEAN;  
Begin
	V_Patch_Exists := F_Check_Previous_Patch('Ver 1.4 (Build#ET051)');
	If (V_Patch_Exists) Then
		UPDATE ER_CTRLTAB SET CTRL_VALUE = 'Ver 1.4 (Build#ET052)'
		where CTRL_KEY = 'app_version' ;
		commit;
		execute immediate 'INSERT INTO track_patches VALUES(seq_track_patches.nextval,280,0,''00_er_version.sql'',sysdate,''Ver 1.4 (Build#ET052)'')';
		commit;
		--dbms_output.put_line('Process successfully');
	Else
		--dbms_output.put_line('The database, where current patches are executed, is not updated to last the build : v9.0.0 Build#637-ET013.02');
		raise_application_error(-20001, 'The database, where current patches are executed, is not updated to last the build : Ver 1.4 (Build#ET052)');
	End if;
End;
/

