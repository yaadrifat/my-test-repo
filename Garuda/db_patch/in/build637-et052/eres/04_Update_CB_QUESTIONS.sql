--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind';
  
  IF (index_count = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='<u>Current Evaluation and Action</u><br/>If Reactive/Positive - Defer<br/>If Non-Reactive/Negative - No action<br/>If Not Done - Questions 11 and 13 are required and Question 14 is optional.  Defer if Questions 10, 11 and 12 are all Not Done.<br/>If tests performed, Question 11, 13 and 14 are inactivated.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action for completed tests.<br/> Combination test added October 1, 2011 as an option for reporting NAT results. If test performed on historical inventory, results may be reported in this field.<br/>NMDP and 10-CBA require valid test for HIV-1 NAT or historical results for HIV p24 Antigen.  For units collected prior to February 28, 2004, HIV p24 was an acceptable test.<br/>', QUES_DESC='NAT HIV-1 / HCV / HBV' WHERE QUES_CODE='nat_hiv_1_hcv_hbv_mpx_ind';
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/
COMMIT;

--ENDS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS----
 
INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,280,4,'04_Update_CB_QUESTIONS.sql',sysdate,'9.0.0 B#637-ET052');
commit;
