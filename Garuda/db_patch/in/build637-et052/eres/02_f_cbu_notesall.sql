create or replace
function f_cbu_notesall(cordid number) return SYS_REFCURSOR IS
v_cord_notes SYS_REFCURSOR;
BEGIN
    OPEN v_cord_notes FOR
select notes.pk_notes, notes.notes, notes.subject, f_codelst_desc(notes.fk_notes_category) notes_category,
DECODE(notes.NOTE_ASSESSMENT,'tu','Temporaily Unavailable','no_cause','No Cause for Deferral','na','Not Available') NOTES_ASSESSMENT,
to_char(notes.created_on,'Mon DD, YYYY hh:mm:ss am') date_time, usr.usr_lastname||' '||usr.usr_firstname note_user,
f_codelst_desc(notes.fk_notes_type) note_type, decode(notes.visibility, 0 ,'No', 1, 'Yes', 'No') visi_tc
from cb_notes notes, er_user usr
where notes.entity_id = cordid
and usr.pk_user=notes.CREATOR
and notes.fk_notes_type =  F_CODELST_ID('note_type','clinic')
and (notes.amended is null or notes.amended=0)
order by notes.created_on desc, notes.pk_notes asc;
RETURN v_cord_notes;
END;
/


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,280,2,'02_f_cbu_notesall.sql',sysdate,'9.0.0 B#637-ET052');

commit;