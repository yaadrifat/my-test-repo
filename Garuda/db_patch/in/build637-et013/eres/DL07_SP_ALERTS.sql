
create or replace
PROCEDURE SP_ALERTS(v_entity_id number,v_entity_type number) AS
TYPE CurTyp IS REF CURSOR;  -- define weak REF CURSOR type
cur_cv   CurTyp;  -- declare cursor variable
v_alertId varchar2(255);
v_notify_sub varchar2(4000);
v_alert_desc varchar2(4000);
v_alert_wording varchar2(4000);
v_alert_precondition varchar2(4000) :='';
v_col_name varchar2(4000);
v_alert_precond_cnt number :=0;
v_query varchar2(4000);
sql_fieldval varchar2(4000);
var_instance_cnt number:=0;
var_instance_cnt1 number:=0;
var_instance_cnt2 number:=0;
var_instance_cnt3 number:=0;
var_instance_cnt4 number:=0;
var_instance_cnt5 number:=0;
v_userId number;
v_cordregid varchar2(255);
v_cordlocalid varchar2(255);
V_ORDERTYPE VARCHAR2(255);
v_sampleatlab varchar2(1);
v_requestdate varchar2(255);
V_ALERTNAME VARCHAR2(255);
v_alerttype varchar2(255);
v_resol number:=0;
v_resoldesc VARCHAR2(50 CHAR):='N/A';
v_orderId number;
v_schshipdate varchar2(50);
v_temp number;
V_ALERT_ID_TEMP NUMBER;
v_entity_order number;
v_entity_cord number;
v_notify number;
v_mail number;
v_mailfrom varchar2(4000);
v_url varchar2(4000);
v_email_wording varchar2(4000);
v_cbb varchar2(255);
v_alert_title varchar2(255);
v_usingcdr varchar2(2);
v_uploadalerts varchar2(255);
BEGIN
select pk_codelst into v_notify from er_codelst where codelst_type='alert_type' and codelst_subtyp='notify';
select pk_codelst into v_mail from er_codelst where codelst_type='alert_type' and codelst_subtyp='mail';
select pk_codelst into v_entity_order from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER';
select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';
select CTRL_DESC into v_url from er_ctrltab where CTRL_VALUE = 'emtrax_url';
select CTRL_DESC into v_mailfrom from er_ctrltab where CTRL_VALUE = 'email_from';
select rowtocol('select pk_codelst from er_codelst where codelst_type=''alert'' and codelst_subtyp in(''alert_23'',''alert_24'',''alert_26'',''alert_25'')',',') into v_uploadalerts from dual;
if v_entity_type=v_entity_cord then
SELECT nvl(c.using_cdr,'0') into v_usingcdr FROM cbb c,er_site s,cb_cord crd WHERE crd.fk_cbb_id=s.pk_site AND c.fk_site=s.pk_site AND crd.pk_cord=v_entity_id;
end if;
if v_entity_type=v_entity_order then
select f_get_codelstdesc(nvl(order_type,0)) into v_ordertype from er_order where pk_order=v_entity_id;
SELECT nvl(SITE_ID,''),to_char(hdr.order_open_date,'Mon DD, YYYY') into v_cbb,v_requestdate FROM er_order ord,er_order_header hdr,er_site s WHERE ord.fk_order_header=hdr.pk_order_header and hdr.fk_site_id=s.pk_site and pk_order=v_entity_id;
SELECT nvl(c.using_cdr,'0') into v_usingcdr FROM cbb c,er_site s,er_order e,er_order_header h,cb_cord crd WHERE e.fk_order_header=h.pk_order_header AND h.order_entityid=crd.pk_cord AND crd.fk_cbb_id=s.pk_site AND c.fk_site=s.pk_site AND e.pk_order=v_entity_id;
end if;
--DBMS_OUTPUT.PUT_LINE('v_ordertype::::::::::::::'||v_ordertype);
if v_usingcdr='1' then
FOR CUR_ALERTS IN(select pk_codelst,CODELST_SUBTYP,codelst_desc,codelst_custom_col1,codelst_custom_col,codelst_type,CODELST_CUSTOM_COL2 from er_codelst where codelst_type in('alert','alert_resol') AND CODELST_HIDE='N' order by pk_codelst)
LOOP
  v_alertId := cur_alerts.pk_codelst;
  V_ALERTNAME:=cur_alerts.codelst_subtyp;
	v_alert_title := cur_alerts.codelst_desc;
	v_alert_desc := cur_alerts.codelst_custom_col1;
	v_alert_wording :=cur_alerts.codelst_custom_col;
  v_alert_precondition := getalertcondition(v_alertid,'PRE');
  v_alerttype:=cur_alerts.codelst_type;
  v_email_wording:=cur_alerts.codelst_custom_col2;
  var_instance_cnt :=0;
  var_instance_cnt1 :=0;
  var_instance_cnt2 :=0;
  var_instance_cnt3 :=0;
  var_instance_cnt4 :=0;
  var_instance_cnt5 :=0;
 -- dbms_output.put_line('********************************************************************************************************************************************');
--  dbms_output.put_line('alert Id :::'||v_alertid);
--   dbms_output.put_line('alert pre condition is::'||v_alert_precondition);

if v_alert_precondition<>' ' then
    if v_alertname in('alert_01','alert_02','alert_03','alert_04','alert_15','alert_20','alert_16','alert_29','alert_30','alert_31','alert_32','alert_33','alert_34','alert_35','alert_36','alert_37','alert_38','alert_39') then
      v_query := 'select count(*) cnt from er_order where '||v_alert_precondition;
    elsif v_alertname in('alert_05','alert_06') then
      v_query := 'select count(*) cnt from er_order,er_order_header where er_order.fk_order_header=er_order_header.pk_order_header and '||v_alert_precondition;
    elsif v_alertname in('alert_07','alert_08','alert_09','alert_10','alert_11') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_shipment where er_order.fk_order_header=er_order_header.pk_order_header and cb_shipment.fk_order_id=er_order.pk_order and '||v_alert_precondition;
    elsif v_alertname='alert_19' then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and '||v_alert_precondition;
    elsif v_alertname IN('alert_23','alert_24','alert_25','alert_26') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord,er_attachments,cb_upload_info where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and er_attachments.entity_id=cb_cord.pk_cord and er_attachments.entity_type=(select pk_codelst from er_codelst where codelst_type=''entity_type'' and codelst_subtyp=''CBU'') and er_attachments.pk_attachment=cb_upload_info.fk_attachmentid and '||v_alert_precondition;
    end if;
    v_query:=replace(v_query,'#keycolumn',v_entity_id);
    --dbms_output.put_line('^^^^^^^^^^^'||v_query);
    open cur_cv for v_query;
     LOOP
          FETCH cur_cv INTO sql_fieldval ;
          EXIT WHEN cur_cv%NOTFOUND;
           --dbms_output.put_line('display result  ' || sql_fieldval );
           if sql_fieldval>0 then
           if v_entity_type=v_entity_order then
              select nvl(assigned_to,0),nvl(crd.cord_registry_id,'Not Available'),nvl(crd.cord_local_cbu_id,'Not Available'),nvl(ord.order_sample_at_lab,'-'),case when ord.fk_order_resol_by_tc is null and ord.fk_order_resol_by_cbb is null then 0 when ord.fk_order_resol_by_tc is null and ord.fk_order_resol_by_cbb is not null then ord.fk_order_resol_by_cbb when ord.fk_order_resol_by_tc is not null and ord.fk_order_resol_by_cbb is null then ord.fk_order_resol_by_tc else 0 end into v_userid,v_cordregid,v_cordlocalid,v_sampleatlab,v_resol from er_order ord left outer join er_order_header ordhdr on(ord.fk_order_header=ordhdr.pk_order_header) left outer join cb_cord crd on(ordhdr.order_entityid=crd.pk_cord) where pk_order=v_entity_id;
              --dbms_output.put_line('v_resol............'||v_resol);
              select ord.pk_order,nvl(to_char(ship.sch_shipment_date,'Mon DD, YYYY'),'N/A') into v_temp,v_schshipdate from er_order ord left outer join (select fk_order_id,sch_shipment_date from cb_shipment where fk_order_id=v_entity_id and fk_shipment_type=(select pk_codelst from er_codelst where codelst_type='shipment_type' and codelst_subtyp='CBU Shipment')) ship on(ship.fk_order_id=ord.pk_order) where ord.pk_order=v_entity_id;
            end if;
            if v_entity_type=v_entity_cord then
              select nvl(cord_registry_id,'N/A') into v_cordregid from cb_cord where pk_cord=v_entity_id;
              SELECT nvl(SITE_ID,'') into v_cbb FROM cb_cord c,er_site s WHERE c.fk_cbb_id=s.pk_site and c.pk_cord=v_entity_id;
            end if;
           if v_resol<>0 then
           --dbms_output.put_line('before getting resolution desc...............');
            select cbu_status_desc into v_resoldesc from cb_cbu_status where pk_cbu_status=v_resol;
            --dbms_output.put_line('after getting resolution desc...............'||v_resoldesc);
           end if;

--           v_email_wording:=replace(v_email_wording,'Kind Regards','Thank you');
           v_alert_wording:=replace(v_alert_wording,'<Request Type>',v_ordertype);
           v_email_wording:=replace(v_email_wording,'<444>',v_ordertype);
--           v_email_wording:=replace(v_email_wording,'<hiperlink>',v_url);
           --dbms_output.put_line('email wording in begining...............'||v_email_wording);
           v_alert_wording:=replace(v_alert_wording,'<CBU Registry ID>',v_cordregid);
           v_email_wording:=replace(v_email_wording,'<123>',v_cordregid);
           v_alert_wording:=replace(v_alert_wording,'<Resolution Code>',v_resoldesc);
--           v_email_wording:=replace(v_email_wording,'<Resolution Code>',v_resoldesc);
           v_alert_wording:=replace(v_alert_wording,'<Shipment Scheduled Date>',v_schshipdate);
--           v_email_wording:=replace(v_email_wording,'<Shipment Scheduled Date>',v_schshipdate);
            v_email_wording:=replace(v_email_wording,'<555>',to_char(sysdate,'Mon DD, YYYY'));
           if v_sampleatlab='Y' then
           v_alert_wording:=replace(v_alert_wording,'CT','CT - Sample at Lab');
           v_email_wording:=replace(v_email_wording,'CT','CT - Sample at Lab');
           --v_ordertype:=replace(v_ordertype,'CT','CT - Sample at Lab');
           elsif v_sampleatlab='N' then
           v_alert_wording:=replace(v_alert_wording,'CT','CT - Ship Sample');
           v_email_wording:=replace(v_email_wording,'CT','CT - Ship Sample');
           --v_ordertype:=replace(v_ordertype,'CT','CT - Ship Sample');

           end if;
            v_alert_wording:=replace(v_alert_wording,'<CBB ID>',v_cbb);
            v_email_wording:=replace(v_email_wording,'<333>',v_cbb);
            v_email_wording:=replace(v_email_wording,'<666>',v_cordlocalid);
            v_email_wording:=replace(v_email_wording,'<777>',v_requestdate);

           select pk_codelst into v_alert_id_temp from er_codelst where codelst_type='alert' and codelst_subtyp='alert_20';
          -- dbms_output.put_line('v_alert_id_temp::::::::'||v_alert_id_temp);
         --  dbms_output.put_line('v_userId::::::::'||v_userid);
          -- dbms_output.put_line('select count(*) from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert='||v_alertid||' and alrt_inst.entity_id='||v_entity_id||' and alrt_inst.entity_type='||v_entity_type||' and alrt_inst.fk_userid='||v_userid||' and alrt_inst.alert_type='||v_notify);
          -- dbms_output.put_line('var_instance_cnt before::::::'||var_instance_cnt);
         -- dbms_output.put_line('email wording in finally...............'||v_email_wording);
           select count(*) into var_instance_cnt from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.fk_userid=v_userid and alrt_inst.alert_type=v_notify;
               -- dbms_output.put_line('var_instance_cnt after:::::'||var_instance_cnt);
            v_notify_sub:='EmTrax Alert for CBB: '||v_cbb||'/CBU Registry ID: '||v_cordregid||'/CBU Local ID: '||v_cordlocalid||','||v_ordertype||':'||v_alert_title;
            v_email_wording:=replace(v_email_wording,'<111>',v_alert_title);
            v_email_wording:=replace(v_email_wording,'<222>',v_alert_wording);
            if var_instance_cnt=0 or v_alert_id_temp=v_alertid then
              --  dbms_output.put_line('inside notification insert');
               insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_type,v_userid,v_notify,v_alert_wording);
                COMMIT;
            end if;
           -- dbms_output.put_line('alert Id is:::'||v_alertid);
            for usrs in(select altr.FK_USER fk_user,altr.EMAIL_FLAG email_flag,altr.NOTIFY_FLAG notify_flag,altr.PROMPT_FLAG prompt_flag,addr.add_email mail,altr.ct_lab_flag ctlabflag,altr.ct_ship_flag ctshipflag,altr.he_flag heflag,altr.or_flag orflag,usr.usr_firstname||' '||usr.usr_lastname uname from cb_user_alerts altr,er_user usr,er_add addr where altr.fk_user=usr.pk_user and usr.fk_peradd=addr.pk_add and fk_codelst_alert=v_alertid)
            loop
             -- dbms_output.put_line('users that has assigned alert is:::'||usrs.fk_user);
              if usrs.email_flag=1 then
              --dbms_output.put_line('alert type is:::'||v_alerttype);
              --dbms_output.put_line('ctlabflag is:::'||usrs.ctlabflag);
              --dbms_output.put_line('usrs.fk_user:::'||usrs.fk_user);
              --dbms_output.put_line('v_userid:::'||v_userid);
              if usrs.fk_user=v_userid then
              --dbms_output.put_line('user ids matched:::');
              if usrs.ctlabflag=1 and v_alerttype='alert_resol' then
              --dbms_output.put_line('inside CT lab flag:::'||usrs.ctlabflag);
                if v_sampleatlab='Y' then
                --dbms_output.put_line('inside ct lab flag2:::'||usrs.ctlabflag);
                --dbms_output.put_line('select count(*) from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert='||v_alertid||' and alrt_inst.v_entity_id='||v_entity_id||' and alrt_inst.entity_type='||v_entity_type||' and alrt_inst.fk_userid='||v_userid||' and alrt_inst.alert_type='||v_mail);
                  select count(*) into var_instance_cnt1 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.entity_type=v_entity_order and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
                  if var_instance_cnt1=0 or v_alert_id_temp=v_alertid then
                    insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
                  --dbms_output.put_line('Email alerts implemented..ct lab');
                    v_email_wording:=replace(v_email_wording,'<loggedinuser>',usrs.uname);
                    insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT,NOTIFY_CC) values(SEQ_ER_NOTIFICATION.nextval,v_mailfrom,usrs.mail,v_notify_sub,v_email_wording,0,null);
                    COMMIT;
                  end if;
                end if;
              elsif usrs.ctshipflag=1 and v_alerttype='alert_resol' then
              --dbms_output.put_line('inside CT ship flag:::'||usrs.ctshipflag);
              --dbms_output.put_line(v_ordertype||'inside CT ship flag111111:::'||v_sampleatlab);
                if v_sampleatlab='N' then
                --dbms_output.put_line('inside CT ship flag222222:::'||usrs.ctshipflag);
                  select count(*) into var_instance_cnt2 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
                  --dbms_output.put_line('var_instance_cnt2:::'||var_instance_cnt2);
                  --dbms_output.put_line(v_alert_id_temp||'='||v_alertid);
                  if var_instance_cnt2=0 or v_alert_id_temp=v_alertid then
                    insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
                    COMMIT;
                  --dbms_output.put_line('Email alerts implemented ct ship');
                v_email_wording:=replace(v_email_wording,'<loggedinuser>',usrs.uname);
                    insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT,notify_cc) values(SEQ_ER_NOTIFICATION.nextval,v_mailfrom,usrs.mail,v_notify_sub,v_email_wording,0,null);
                    COMMIT;
                  end if;
                end if;
              elsif usrs.heflag=1 and v_alerttype='alert_resol' then
              --dbms_output.put_line('inside he flag:::'||usrs.heflag);
                  select count(*) into var_instance_cnt3 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
                  if var_instance_cnt3=0 or v_alert_id_temp=v_alertid then
                    insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
                    COMMIT;
                  --dbms_output.put_line('Email alerts implemented he');
                v_email_wording:=replace(v_email_wording,'<loggedinuser>',usrs.uname);
                    insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT,notify_cc) values(SEQ_ER_NOTIFICATION.nextval,v_mailfrom,usrs.mail,v_notify_sub,v_email_wording,0,null);
                    COMMIT;
                  end if;

              elsif usrs.orflag=1 and v_alerttype='alert_resol' then
              --dbms_output.put_line('inside or flag:::'||usrs.orflag);
                  select count(*) into var_instance_cnt4 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type  and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
                  if var_instance_cnt4=0 or v_alert_id_temp=v_alertid then
                    insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
                  COMMIT;
                  --dbms_output.put_line('Email alerts implemented or');
                v_email_wording:=replace(v_email_wording,'<loggedinuser>',usrs.uname);
                    insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT,notify_cc) values(SEQ_ER_NOTIFICATION.nextval,v_mailfrom,usrs.mail,v_notify_sub,v_email_wording,0,null);
                    COMMIT;
                  end if;

              elsif v_alerttype='alert' then
                select count(*) into var_instance_cnt5 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
               --dbms_output.put_line('var_instance_cnt5:::'||var_instance_cnt5);
                --dbms_output.put_line(v_alert_id_temp||'='||v_alertid);
                --dbms_output.put_line('v_uploadalerts='||v_uploadalerts);
              if var_instance_cnt5=0 or v_alert_id_temp=v_alertid then
                insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
              COMMIT;
              --dbms_output.put_line('Email alerts implemented');
           v_email_wording:=replace(v_email_wording,'<loggedinuser>',usrs.uname);
                insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT,notify_cc) values(SEQ_ER_NOTIFICATION.nextval,v_mailfrom,usrs.mail,v_notify_sub,v_email_wording,0,null);
                COMMIT;
              end if;
              end if;
              end if;
              end if;
              end loop;
           end if;
      END LOOP;
    --dbms_output.put_line('alert wording is::'||v_alert_wording);
end if;
END LOOP;
end if;
if v_entity_type=v_entity_order then
  update er_order set data_modified_flag='' where pk_order=v_entity_id;
  COMMIT;
end if;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
  dbms_output.put_line ('A SELECT...INTO did not return any row.');
end;
/



