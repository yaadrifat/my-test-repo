create or replace
function F_HLA_PACKING_SLIP(pk_cord number) return SYS_REFCURSOR
IS
p_hla_refcur SYS_REFCURSOR;
v_entity_cord number;
BEGIN
select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';
    OPEN p_hla_refcur FOR SELECT 
  F_CODELST_DESC(h.fk_source) AS sourceval,
  F_CODELST_DESC(h.fk_hla_code_id) AS LOCUS,
  F_CODELST_DESC(h.fk_hla_method_id) AS METHODDATA,
  nvl(ec1.genomic_format,'-') AS TYPE1,
  nvl(ec2.genomic_format,'-') AS TYPE2,
  to_char(h.hla_received_date,'Mon DD, YYYY') AS ENTRYDATE
FROM CB_HLA h ,
  cb_antigen_encod ec1,
  cb_antigen_encod ec2
WHERE h.fk_hla_antigeneid1=ec1.fk_antigen (+)
AND ( ec1.version         =
  (SELECT MAX(ec1s.version)
  FROM cb_antigen_encod ec1s
  WHERE ec1.fk_antigen=ec1s.fk_antigen
  )
OR ec1.version IS NULL) 
AND h.fk_hla_antigeneid2 = ec2.fk_antigen (+)
AND (ec2.version         =
  (SELECT MAX(ec2s.version)
  FROM cb_antigen_encod ec2s
  WHERE ec2.fk_antigen=ec2s.fk_antigen
  )
OR ec2.version  IS NULL)
AND h.ENTITY_ID  =pk_cord
AND h.ENTITY_TYPE=v_entity_cord
ORDER BY h.CB_HLA_ORDER_SEQ DESC nulls last;
RETURN P_HLA_REFCUR;
END;
/
