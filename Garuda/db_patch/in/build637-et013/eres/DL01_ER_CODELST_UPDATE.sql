set define off;

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'bact_cul'
    AND CODELST_DESC='Unknown' ;
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET codelst_subtyp = 'unknown' where codelst_type = 'bact_cul'
    AND codelst_desc = 'Unknown';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'fung_cul'
    AND CODELST_DESC='Unknown' ;
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET codelst_subtyp = 'unknown' where codelst_type = 'fung_cul'
      AND codelst_desc = 'Unknown';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'gender'
    AND CODELST_DESC='Unknown' ;
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET codelst_subtyp = 'unknown' where codelst_type = 'gender'
    AND codelst_desc = 'Unknown';
	commit;
  end if;
end;
/
--END--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'bact_cul'
    AND codelst_subtyp = 'unknown';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='bact_cul' and CODELST_SUBTYP='unknown';
 commit;
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'fung_cul'
    AND codelst_subtyp = 'unknown';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='fung_cul' and CODELST_SUBTYP='unknown';
 commit;
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'gender'
    AND codelst_subtyp = 'unknown';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='gender' and CODELST_SUBTYP='unknown';
 commit;
  end if;
end;
/


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type in('alert','alert_resol');
  if (v_record_exists = 39) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = '<font face="verdana"><table><tr><td align="left"><b><111></b></td><td>:</td><td align="left"><222></td></tr><tr><td align="left">CBB ID</td><td>:</td><td align="left"><333></td></tr><tr><td align="left">CBU Registry ID</td><td>:</td><td align="left"><123></td></tr><tr><td align="left">CBU Local ID</td><td>:</td><td align="left"><666></td></tr><tr><td align="left">Request Type</td><td>:</td><td align="left"><444></td></tr><tr><td align="left">Request Date</td><td>:</td><td align="left"><777></td></tr><tr><td align="left">Alert Date</td><td>:</td><td align="left"><555></td></tr></table><br><br><p><i>This automated EmTrax communication has been sent to you as per your alert preferences. If you wish to discontinue these alerts, please access the alert management section of your profile in EmTrax.</i></p></font><table><tr><td><img alt="" src="http://marrow.org/images/email/nmdp_80.png"></td><td><img alt="" src="http://marrow.org/images/email/btm_50.png"></td></tr></table><br><hr><p><font face="Arial"><i>***This is an auto-generated e-mail***</p><p>This e-mail address is not monitored; please do not respond to this message.</i></p><br><p><i>The sender of this e-mail provides services for the NMDP and/or Be The Match pursuant to agreement and may not be authorized to bind the organization with any information, material, or content contained within this e-mail.</i></font></p>' where codelst_type in('alert','alert_resol');
	commit;
  end if;
end;
/
--END--

