--Update Statement for CB_FORM_QUESTIONS FOR MRQ VERSION N2F AND GFC--
--QUESTION NO-6.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='symptom_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4mo_illness_symptom_ind'),FK_DEPENDENT_QUES= (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4mo_illness_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='symptom_desc') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
commit;
  end if;
end;
/
--QUESTION NO-15--

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists from CB_FORM_QUESTIONS where QUES_SEQ = '15' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_QUESTION=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_diagnosis_ind'),RESPONSE_VALUE='mrq_res1',UNEXPECTED_RESPONSE_VALUE='mrq_unres1' where QUES_SEQ = '15' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
commit;
  end if;
end;
/
--QUESTION NO-22--

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tx_stemcell_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set RESPONSE_VALUE='mrq_res1' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tx_stemcell_12m_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
commit;
  end if;
end;
/
--QUESTION NO-34--

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists from CB_FORM_QUESTIONS where QUES_SEQ = '34' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_QUESTION=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_clotting_factor_12m_ind'),RESPONSE_VALUE='mrq_res1',UNEXPECTED_RESPONSE_VALUE='mrq_unres1' where QUES_SEQ = '34' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
commit;
  end if;
end;
/
--START INSERTING RECORD IN TO CB_QUESTION_GRP TABLE --
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_diagnosis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC ='A. Illness and Medications'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cjd_diagnosis_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFC'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_diagnosis_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC ='A. Illness and Medications'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='cjd_diagnosis_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_clotting_factor_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC ='B. Behavior and Exposure Risk'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_clotting_factor_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFC'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_clotting_factor_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC ='B. Behavior and Exposure Risk'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_clotting_factor_12m_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC ='B. Behavior and Exposure Risk'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='unexplained_symptom_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2F'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--START INSERTING RECORD IN TO CB_FORM_QUESTIONS TABLE --
--Question 40--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2F'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind'),
null,null,null,'40',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 40.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_night_sweat')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_night_sweat') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
commit;
  end if;
end;
/
--Question 40.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_blue_prpl_spot')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_blue_prpl_spot') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
commit;
  end if;
end;
/
--Question 40.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_weight_loss')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_weight_loss') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
commit;
  end if;
end;
/
--Question 40.d--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_persist_diarrhea')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_persist_diarrhea') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
commit;
  end if;
end;
/
--Question 40.e--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_cough_short_breath')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_cough_short_breath') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
commit;
  end if;
end;
/
--Question 40.f--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_temp_over_ten_days')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_temp_over_ten_days') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
commit;
  end if;
end;
/
--Question 40.g--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_mouth_sores_wht_spt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_mouth_sores_wht_spt') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
commit;
  end if;
end;
/
--Question 40.h--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_1m_lump_nk_apit_grn')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_1m_lump_nk_apit_grn') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2F');
commit;
  end if;
end;
/
--END--
--QUESTION NO-6.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='symptom_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4mo_illness_symptom_ind'),FK_DEPENDENT_QUES= (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='pst_4mo_illness_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='symptom_desc') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
commit;
  end if;
end;
/
--QUESTION NO-15--

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists from CB_FORM_QUESTIONS where QUES_SEQ = '15' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_QUESTION=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cjd_diagnosis_ind'),RESPONSE_VALUE='mrq_res1',UNEXPECTED_RESPONSE_VALUE='mrq_unres1' where QUES_SEQ = '15' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
commit;
  end if;
end;
/
--QUESTION NO-22--

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tx_stemcell_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set RESPONSE_VALUE='mrq_res1' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='tx_stemcell_12m_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
commit;
  end if;
end;
/
--QUESTION NO-34--

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists from CB_FORM_QUESTIONS where QUES_SEQ = '34' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_QUESTION=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_clotting_factor_12m_ind'),RESPONSE_VALUE='mrq_res1',UNEXPECTED_RESPONSE_VALUE='mrq_unres1' where QUES_SEQ = '34' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
commit;
  end if;
end;
/
--START INSERTING RECORD IN TO CB_QUESTION_GRP TABLE --
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC ='B. Behavior and Exposure Risk'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='unexplained_symptom_ind'),
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFC'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--START INSERTING RECORD IN TO CB_FORM_QUESTIONS TABLE --
--Question 40--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='GFC'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind'),null,null,null,'40',null,NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 40.a--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_night_sweat')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_night_sweat') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
commit;
  end if;
end;
/
--Question 40.b--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_blue_prpl_spot')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_blue_prpl_spot') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
commit;
  end if;
end;
/
--Question 40.c--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_weight_loss')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_weight_loss') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
commit;
  end if;
end;
/
--Question 40.d--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_persist_diarrhea')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_persist_diarrhea') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
commit;
  end if;
end;
/
--Question 40.e--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_cough_short_breath')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_cough_short_breath') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
commit;
  end if;
end;
/
--Question 40.f--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_temp_over_ten_days')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_temp_over_ten_days') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
commit;
  end if;
end;
/
--Question 40.g--
DECLARE
  v_record_exists number := 0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_mouth_sores_wht_spt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_mouth_sores_wht_spt') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
commit;
  end if;
end;
/
--Question 40.h--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_1m_lump_nk_apit_grn')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set FK_MASTER_QUES=(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexplained_symptom_ind') where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='unexpln_1m_lump_nk_apit_grn') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='GFC');
commit;
  end if;
end;
/
--END--
--Insert CB_Questions for N2--
--Question 27--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'taken_cocaine';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Taken (snorted) cocaine through your nose?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'taken_cocaine',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 36--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'visit_or_lived_in_uk';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you visited or lived in the United Kingdom from 1980 to 1996?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'visit_or_lived_in_uk',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 36.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'spend_six_mnth_uk';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'If so, have you spent a total of more than six months from 1980 to 1996?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'spend_six_mnth_uk',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Question 39--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'rev_xenotransplant';
  if (v_record_exists = 0) then
	INSERT INTO cb_questions (PK_QUESTIONS, QUES_DESC, QUES_HELP, UNLICEN_REQ_FLAG, LICEN_REQ_FLAG, UNLICN_PRIOR_TO_SHIPMENT_FLAG, LICEN_PRIOR_SHIPMENT_FLAG, CBB_NOT_USE_SYSTEM, QUES_HOVER, ADD_COMMENT_FLAG, ASSESMENT_FLAG,  RESPONSE_TYPE, QUES_CODE, CREATOR, CREATED_ON, LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD, DELETEDFLAG, RID) VALUES(SEQ_CB_QUESTIONS.nextval, 'Have you lived with, or had close contact with, anyone who has received a xenotransplant?', NULL, '1', '0', '1', '0', '0', NULL, '1', '0', (select PK_CODELST from er_codelst where codelst_type='resp_type' and codelst_subtyp='dropdown'), 'rev_xenotransplant',NULL, sysdate, NULL, NULL, NULL, NULL, NULL);
commit;
  end if;
end;
/
--Insert CB_FORM_QUESTIONS for N2--
--Question 21--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind'),null, null,null,'21',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind'),null, null,null,'21',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind'),null, null,null,'21',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2D'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind'),null, null,null,'21',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 27--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='taken_cocaine')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='taken_cocaine'),null, null,null,'27',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 36--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk'),null, null,null,'36',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 36.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spend_six_mnth_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spend_six_mnth_uk'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk'),(select pk_codelst from ER_CODELST where CODELST_TYPE='mrq_res2' and codelst_subtyp='yes'),'36.a',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),null,null, sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 39--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant'),null, null,null,'39',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Insert CB_FORM_GRP for N2--
--Question 21--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_iv_drug_12m_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_iv_drug_12m_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_iv_drug_12m_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='sex_w_iv_drug_12m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='sex_w_iv_drug_12m_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2D'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 27--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='taken_cocaine')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='In the Last 12 Months, Have You Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='taken_cocaine'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2'),null, sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 36--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='visit_or_lived_in_uk'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 36.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spend_six_mnth_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spend_six_mnth_uk'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2'), null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 39--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='rev_xenotransplant'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2'), null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Deleting the UnWanted Test FOR MRQ N2 Form From CB_FORM_QUESTION and CB_QUESTION_GRP--
--Question 40--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='live_travel_europe_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 1) then
	delete from CB_QUESTION_GRP where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='live_travel_europe_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
commit;
  end if;
end;
/
--Question 41--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_uk_ge_3m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 1) then
	delete from CB_QUESTION_GRP where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_uk_ge_3m_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
commit;
  end if;
end;
/
--Question 42--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 1) then
	delete from CB_QUESTION_GRP where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
commit;
  end if;
end;
/
--Question 43--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_europe_ge_5y_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 1) then
	delete from CB_QUESTION_GRP where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_europe_ge_5y_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
commit;
  end if;
end;
/
--Question 44--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='us_military_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 1) then
	delete from CB_QUESTION_GRP where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='us_military_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
commit;
  end if;
end;
/
--Question 45--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_1_ge_6m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 1) then
	delete from CB_QUESTION_GRP where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_1_ge_6m_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='live_travel_europe_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 1) then
	delete from CB_FORM_QUESTIONS where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='live_travel_europe_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
commit;
  end if;
end;
/
--Question 41--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_uk_ge_3m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 1) then
	delete from CB_FORM_QUESTIONS where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_uk_ge_3m_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
commit;
  end if;
end;
/
--Question 42--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 1) then
	delete from CB_FORM_QUESTIONS where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
commit;
  end if;
end;
/
--Question 43--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_europe_ge_5y_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 1) then
	delete from CB_FORM_QUESTIONS where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spent_europe_ge_5y_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
commit;
  end if;
end;
/
--Question 44--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='us_military_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 1) then
	delete from CB_FORM_QUESTIONS where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='us_military_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
commit;
  end if;
end;
/
--Question 45--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_1_ge_6m_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
  if (v_record_exists = 1) then
	delete from CB_FORM_QUESTIONS where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='mil_base_europe_1_ge_6m_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2');
commit;
  end if;
end;
/
--Update CB_FORM_QUESTIONS for N2B--
--Question 27--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='28'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='27' where QUES_SEQ='28' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
commit;
  end if;
end;
/
--Question 27--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='28'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='27' where QUES_SEQ='28' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
commit;
  end if;
end;
/
--Question 27--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='28'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='27' where QUES_SEQ='28' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
commit;
  end if;
end;
/
--Question 28--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='29'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='28' where QUES_SEQ='29' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='29'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='28' where QUES_SEQ='29' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='29'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='28' where QUES_SEQ='29' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
commit;
  end if;
end;
/
--Question 29--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='30'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='29' where QUES_SEQ='30' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='30'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='29' where QUES_SEQ='30' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='30'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='29' where QUES_SEQ='30' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
commit;
  end if;
end;
/
--Question 30--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='31'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='30' where QUES_SEQ='31' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='31'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='30' where QUES_SEQ='31' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='31'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='30' where QUES_SEQ='31' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
commit;
  end if;
end;
/
--Question 31--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='32'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='31' where QUES_SEQ='32' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='32'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='31' where QUES_SEQ='32' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='32'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='31' where QUES_SEQ='32' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
commit;
  end if;
end;
/
--Question 32--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='33'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='32' where QUES_SEQ='33' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='33'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='32' where QUES_SEQ='33' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='33'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='32' where QUES_SEQ='33' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
commit;
  end if;
end;
/
--Question 33--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='34'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='33' where QUES_SEQ='34' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='34'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='33' where QUES_SEQ='34' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='34'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='33' where QUES_SEQ='34' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
commit;
  end if;
end;
/
--Question 33.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='34.a'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='33.a' where QUES_SEQ='34.a' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='34.a'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='33.a' where QUES_SEQ='34.a' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='34.a'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='33.a' where QUES_SEQ='34.a' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
commit;
  end if;
end;
/
--Question 34--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='34.b'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='34',FK_MASTER_QUES=null,FK_DEPENDENT_QUES=null,FK_DEPENDENT_QUES_VALUE=null where QUES_SEQ='34.b' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='34.b'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='34',FK_MASTER_QUES=null,FK_DEPENDENT_QUES=null,FK_DEPENDENT_QUES_VALUE=null where QUES_SEQ='34.b' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where QUES_SEQ='34.b'
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ='34',FK_MASTER_QUES=null,FK_DEPENDENT_QUES=null,FK_DEPENDENT_QUES_VALUE=null where QUES_SEQ='34.b' and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
commit;
  end if;
end;
/
--END--
--Question 42--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind'),null,null,null,'42',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 42--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind'),null,null,null,'42',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 42--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='MRQ' and version='N2D'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind'),null,null,null,'42',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 42--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='recv_transfusion_uk_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 42--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='recv_transfusion_uk_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 42--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='recv_transfusion_uk_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='recv_transfusion_uk_ind'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2D'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 36--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk'),null, null,null,'36',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 36.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spend_six_mnth_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spend_six_mnth_uk'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk'),(select pk_codelst from ER_CODELST where CODELST_TYPE='mrq_res2' and codelst_subtyp='yes'),'36.a',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),null,null, sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 39--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2B'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant'),null, null,null,'39',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 36--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk'),null, null,null,'36',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 36.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spend_six_mnth_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spend_six_mnth_uk'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk'),(select pk_codelst from ER_CODELST where CODELST_TYPE='mrq_res2' and codelst_subtyp='yes'),'36.a',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),null,null, sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 39--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant'),null, null,null,'39',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 36--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2D'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk'),null, null,null,'36',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 36.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spend_six_mnth_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2D'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spend_six_mnth_uk'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk'),(select pk_codelst from ER_CODELST where CODELST_TYPE='mrq_res2' and codelst_subtyp='yes'),'36.a',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),null,null, sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 39--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,(select pk_form from cb_forms where forms_desc='MRQ' and version='N2D'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant'),null, null,null,'39',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='mrq_res2'),NULL,null,sysdate,null,null, null,null,null);
commit;
  end if;
end;
/
--Question 36--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='visit_or_lived_in_uk'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2B'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 36.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spend_six_mnth_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spend_six_mnth_uk'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2B'), null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 39--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2B');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='rev_xenotransplant'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2B'), null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 36--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='visit_or_lived_in_uk'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 36.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spend_six_mnth_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spend_six_mnth_uk'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'), null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 39--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2C');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='rev_xenotransplant'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2C'), null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 36--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='visit_or_lived_in_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='visit_or_lived_in_uk'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2D'),null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 36.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spend_six_mnth_uk')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='spend_six_mnth_uk'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2D'), null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 39--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='rev_xenotransplant')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE QUESTION_GRP_DESC='Additional Questions Section'),(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='rev_xenotransplant'),(select pk_form from cb_forms where forms_desc='MRQ' and version='N2D'), null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
