--CB_QUESTION_GRP--
--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_west_blot_cde'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_west_blot_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_neut_rslt'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_neut_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv_1_2_west_blot_cde'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv_1_2_west_blot_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--CB_FORM_QUESTIONS--
--Question 16--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),null,null,null,'16',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 16.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'16.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),null,null,null,'17',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'17.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),null,null,null,'18',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'18.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),null,null,null,'19',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'19.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),null,null,null,'20',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2D'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'20.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET QUES_SEQ = '21' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET QUES_SEQ = '21.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2D');
commit;
  end if;
end;
/
--END--
--N2E--
--CB_QUESTION_GRP--
--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hbsag_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hcv_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_west_blot_cde'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_hiv_west_blot_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_neut_rslt'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='hiv_antigen_neut_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv_1_2_west_blot_cde'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='anti_htlv_1_2_west_blot_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 22--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_ind'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 22.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTION_GRP
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	INSERT INTO CB_QUESTION_GRP(PK_QUESTION_GRP,FK_QUESTION_GROUP,FK_QUESTION,FK_FORM,CREATOR,
CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) 
VALUES (SEQ_CB_QUESTION_GRP.NEXTVAL,(select PK_QUESTION_GROUP from CB_QUESTION_GROUP WHERE 
QUESTION_GRP_DESC='Confirmatory / Supplemental Tests'),
(SELECT PK_QUESTIONS FROM CB_QUESTIONS WHERE QUES_CODE='stsyph_ct_date'),
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
--CB_FORM_QUESTIONS--
--Question 17--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),null,null,null,'17',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 17.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hbsag_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'17.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 18--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),null,null,null,'18',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 18.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hcv_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'18.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),null,null,null,'19',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_hiv_west_blot_cde'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'19.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 20--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),null,null,null,'20',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_antigen_neut_rslt'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'20.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/

--Question 21--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),null,null,null,'21',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='anti_htlv_1_2_west_blot_cde'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'21.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 22--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET QUES_SEQ = '22' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 22.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 1) then
	UPDATE CB_FORM_QUESTIONS SET QUES_SEQ = '22.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stsyph_ct_date') and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
commit;
  end if;
end;
/
--Question 23--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ripa_ct_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ripa_ct_ind'),null,null,null,'23',(select DISTINCT(CODELST_TYPE) from ER_CODELST where CODELST_TYPE='test_outcome1'),null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--Question 23.a--
DECLARE
  v_record_exists number := 0;
  V_CBU_STATUS_CODE NUMBER :=0; 
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ripa_ct_date')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='IDM' and version='N2E');
  if (v_record_exists = 0) then
	Insert into CB_FORM_QUESTIONS( PK_FORM_QUESTION,FK_FORM,FK_QUESTION,FK_MASTER_QUES,FK_DEPENDENT_QUES, FK_DEPENDENT_QUES_VALUE,QUES_SEQ,RESPONSE_VALUE,UNEXPECTED_RESPONSE_VALUE,CREATOR,CREATED_ON,LAST_MODIFIED_BY, LAST_MODIFIED_DATE, IP_ADD,DELETEDFLAG,RID) values (SEQ_CB_FORM_QUESTIONS.nextval,
(select pk_form from cb_forms where forms_desc='IDM' and version='N2E'),
(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ripa_ct_date'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ripa_ct_ind'),(select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='ripa_ct_ind'),rowtocol('select PK_CODELST from ER_CODELST where CODELST_TYPE=''test_outcome1'' and CODELST_SUBTYP in(''posi'',''negi'')',','),'23.a',null,null,null,sysdate,null,null,null,null,null);
commit;
  end if;
end;
/
--END--
