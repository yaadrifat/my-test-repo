create or replace
FUNCTION f_cbu_ordhistory(cordid NUMBER) RETURN SYS_REFCURSOR IS
v_cbu_ordhist SYS_REFCURSOR;
BEGIN
    OPEN v_cbu_ordhist FOR SELECT *
FROM
  (SELECT TO_CHAR(STATUS.STATUS_DATE,'Mon DD, YYYY') AS DATEVAL,
    CASE
      WHEN (STATUS.STATUS_TYPE_CODE='cord_status'
      OR STATUS.STATUS_TYPE_CODE   ='Cord_Nmdp_Status')
      THEN CBUSTATUS.CBU_STATUS_DESC
      WHEN (STATUS.STATUS_TYPE_CODE='eligibility'
      OR STATUS.STATUS_TYPE_CODE   ='licence')
      THEN F_CODELST_DESC(STATUS.FK_STATUS_VALUE)
    END STATUS,
    CASE
      WHEN STATUS.CREATOR=0 or STATUS.STATUS_TYPE_CODE   ='Cord_Nmdp_Status'
      THEN 'System'
      WHEN STATUS.CREATOR!=0
      THEN F_GETUSER(STATUS.CREATOR)
    END USR,
    '-' ORDTYPE,
    CASE WHEN STATUS.ADD_OR_UPDATE IS NULL OR STATUS.ADD_OR_UPDATE=0 THEN ' entered as '
         WHEN STATUS.ADD_OR_UPDATE=1 THEN ' changed to ' 
    END DESC2,
    CASE WHEN STATUS.STATUS_TYPE_CODE='cord_status' THEN 'Reason'
         WHEN STATUS.STATUS_TYPE_CODE='Cord_Nmdp_Status' THEN 'CBU Status'
         WHEN STATUS.STATUS_TYPE_CODE='eligibility' THEN 'Eligibility'
         WHEN STATUS.STATUS_TYPE_CODE='licence' THEN 'License Status'
    END DESC3,
    STATUS.STATUS_DATE,
    STATUS.STATUS_TYPE_CODE KEYCODE
  FROM CB_ENTITY_STATUS STATUS
  LEFT OUTER JOIN CB_CBU_STATUS CBUSTATUS
  ON (STATUS.FK_STATUS_VALUE=CBUSTATUS.PK_CBU_STATUS)
  WHERE (STATUS.ENTITY_ID   =cordid
  AND STATUS.FK_ENTITY_TYPE =
    (SELECT C.PK_CODELST
    FROM ER_CODELST C
    WHERE C.CODELST_TYPE='entity_type'
    AND C.CODELST_SUBTYP='CBU'
    ))
  AND( STATUS.STATUS_TYPE_CODE='cord_status'
  OR STATUS.STATUS_TYPE_CODE  ='Cord_Nmdp_Status'
  OR STATUS.STATUS_TYPE_CODE  ='eligibility'
  OR STATUS.STATUS_TYPE_CODE  ='licence')
  UNION ALL
  SELECT 
    TO_CHAR(STAT.STATUS_DATE,'Mon DD, YYYY') AS DATEVAL,
    CASE
      WHEN STAT.STATUS_TYPE_CODE IN ('task_status','task_reset' )
      THEN ACT.ACTIVITY_DESC
    END STATUS,
    CASE
      WHEN STAT.CREATOR=0
      THEN 'System'
      WHEN STAT.CREATOR!=0
      THEN F_GETUSER(STAT.CREATOR)
    END USR,
    CASE
      WHEN f_codelst_desc(ord.order_type) IS NULL
      THEN '-'
      WHEN f_codelst_desc(ord.order_type) IS NOT NULL
      AND f_codelst_desc(ord.order_type)   ='CT'
      THEN DECODE(ord.order_sample_at_lab,'Y','CT - Sample at Lab','N','CT - Ship Sample')
      WHEN f_codelst_desc(ord.order_type) IS NOT NULL
      AND f_codelst_desc(ord.order_type)  !='CT'
      THEN f_codelst_desc(ord.order_type)
    END ordertype,
    ' task ' DESC2,
    CASE
      WHEN STAT.STATUS_TYPE_CODE = 'task_status' THEN 'completed'
      WHEN STAT.STATUS_TYPE_CODE = 'task_reset' THEN 'reset'
    END DESC3,
    STAT.STATUS_DATE,
    STAT.STATUS_TYPE_CODE
  FROM CB_ENTITY_STATUS STAT
  LEFT OUTER JOIN ER_ORDER ORD
  ON (ORD.PK_ORDER =STAT.ENTITY_ID)
  LEFT OUTER JOIN ACTIVITY ACT
  ON (ACT.PK_ACTIVITY   =STAT.FK_STATUS_VALUE)
  WHERE STAT.ENTITY_ID IN
    (SELECT PK_ORDER
    FROM ER_ORDER ORD,
      ER_ORDER_HEADER ORDHDR
    WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER
    AND ORDHDR.ORDER_ENTITYID=cordid
    )
  AND STAT.FK_ENTITY_TYPE=
    (SELECT C.PK_CODELST
    FROM ER_CODELST C
    WHERE C.CODELST_TYPE='entity_type'
    AND C.CODELST_SUBTYP='ORDER'
    )
  AND ( STAT.STATUS_TYPE_CODE IN ('task_status','task_reset' ) )
  AND ACT.ACTIVITY_NAME       IN ('reqclincinfobar','finalcbureviewbar')
  UNION ALL
  SELECT TO_CHAR(FRMVR.CREATED_ON,'Mon DD, YYYY'),
    FRM.FORMS_DESC STATUS,
    F_GETUSER(FRMVR.CREATOR) USR,
    '-' ORDTYPE,
    ' form ' desc2,
    CASE WHEN FRMVR.CREATE_OR_MODIFY IS NULL OR FRMVR.CREATE_OR_MODIFY=0 THEN 'entered'
        WHEN FRMVR.CREATE_OR_MODIFY=1 THEN 'updated'
    end desc3,
    FRMVR.CREATED_ON SYSDATEVAL,
    'FORM'
  FROM CB_FORM_VERSION FRMVR
  LEFT OUTER JOIN CB_FORMS FRM
  ON (FRMVR.FK_FORM      =FRM.PK_FORM)
  WHERE FRMVR.ENTITY_ID  =cordid
  AND FRMVR.CREATED_ON  IS NOT NULL
  AND FRMVR.ENTITY_TYPE IN
    (SELECT C.PK_CODELST
    FROM ER_CODELST C
    WHERE C.CODELST_TYPE='test_source'
    AND C.CODELST_SUBTYP='maternal'
    )
  )
ORDER BY 7 DESC;
RETURN v_cbu_ordhist;
END;
/
