--select count(F_CODELST_DESC(FK_STATUS_VALUE)) from CB_ENTITY_STATUS where F_CODELST_DESC(FK_STATUS_VALUE)='Incomplete'

CREATE OR REPLACE
TRIGGER CB_ENTITY_SAMPLES_AU1
AFTER UPDATE ON CB_ENTITY_SAMPLES
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW 
DECLARE 
	v_rowid NUMBER(10);/*variable used to fetch value of sequence */
	NEW_MODIFIED_BY VARCHAR2(200);
	NEW_CREATOR VARCHAR2(200);
	OLD_CREATOR VARCHAR2(200);
	OLD_MODIFIED_BY VARCHAR2(200);
BEGIN
    SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO v_rowid FROM dual;
	IF :NEW.DELETEDFLAG = 'Y' THEN
	  
	PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'CB_ENTITY_SAMPLES',:OLD.rid,:OLD.PK_ENTITY_SAMPLES,'D',:NEW.LAST_MODIFIED_BY);	
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','PK_ENTITY_SAMPLES',:OLD.PK_ENTITY_SAMPLES,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','ENTITY_ID',:OLD.ENTITY_ID,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','ENTITY_TYPE',:OLD.ENTITY_TYPE,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','FK_SAMPLE_TYPE',:OLD.FK_SAMPLE_TYPE,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','FK_SAMPLE_STATUS',:OLD.FK_SAMPLE_STATUS,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','SAMPLE_QUANTITY',:OLD.SAMPLE_QUANTITY,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','SAMPLE_VOLUME',:OLD.SAMPLE_VOLUME,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','QC_INDICATOR',:OLD.QC_INDICATOR,NULL,NULL,NULL);	
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','RECEIVE_DATE', to_char(:OLD.RECEIVE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','STORAGE_DATE', to_char(:OLD.STORAGE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);	
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','SEQUENCE',:OLD.SEQUENCE,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','CREATOR',:OLD.CREATOR,NULL,NULL,NULL);	
	PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (v_rowid,'CB_ENTITY_SAMPLES','CREATED_ON', TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);	
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','LAST_MODIFIED_BY',:OLD.LAST_MODIFIED_BY,NULL,NULL,NULL);	
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','LAST_MODIFIED_DATE', to_char(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL,NULL);	
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','IP_ADD',:OLD.IP_ADD,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','DELETEDFLAG',:OLD.DELETEDFLAG,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'RID',:OLD.RID,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','FK_SPECIMEN',:OLD.FK_SPECIMEN,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_OF_SEG_AVAIL',:OLD.NO_OF_SEG_AVAIL,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','FILT_PAP_AVAIL',:OLD.FILT_PAP_AVAIL,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','RBC_PEL_AVAIL',:OLD.RBC_PEL_AVAIL,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_EXT_DNA_ALI',:OLD.NO_EXT_DNA_ALI,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_NON_VIA_ALI',:OLD.NO_NON_VIA_ALI,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_SER_ALI',:OLD.NO_SER_ALI,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_PLAS_ALI',:OLD.NO_PLAS_ALI,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_VIA_CEL_ALI',:OLD.NO_VIA_CEL_ALI,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_EXT_DNA_MAT_ALI',:OLD.NO_EXT_DNA_MAT_ALI,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_SER_MAT_ALI',:OLD.NO_SER_MAT_ALI,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_PLAS_MAT_ALI',:OLD.NO_PLAS_MAT_ALI,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','SI_NO_MISC_MAT',:OLD.SI_NO_MISC_MAT,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','CBU_OT_REP_CON_FIN',:OLD.CBU_OT_REP_CON_FIN,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','CBU_NO_REP_ALT_CON',:OLD.CBU_NO_REP_ALT_CON,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'TOT_CBU_ALI_AVA',:OLD.TOT_CBU_ALI_AVA ,NULL,NULL,NULL);     
    PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'NO_CEL_MAT_ALI',:OLD.NO_CEL_MAT_ALI ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'TOT_MAT_ALI_AVA',:OLD.TOT_MAT_ALI_AVA ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_FILT_PAP',:OLD.CBU_FILT_PAP ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_RBC_PEL',:OLD.CBU_RBC_PEL ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_NO_EXT_DNA',:OLD.CBU_NO_EXT_DNA ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_NO_SERUM', :OLD.CBU_NO_SERUM ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_NO_PLASMA',:OLD.CBU_NO_PLASMA ,NULL,NULL,NULL);      		     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_NO_NON_VIA',:OLD.CBU_NO_NON_VIA ,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_NO_NT_REP_FIN',:OLD.CBU_NO_NT_REP_FIN ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_NO_OF_SEG',:OLD.CBU_NO_OF_SEG ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_OT_REP_CON_FIN',:OLD.CBU_OT_REP_CON_FIN ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'MT_NO_MAT_SER',:OLD.MT_NO_MAT_SER ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'MT_NO_MAT_PAL',:OLD.MT_NO_MAT_PAL ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'MT_NO_MAT_EXT_DNA',:OLD.MT_NO_MAT_EXT_DNA ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'MT_NO_MIS_MAT',:OLD.MT_NO_MIS_MAT ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_FIL_PAP_SAMP', :OLD.CONV_FIL_PAP_SAMP ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_RPC_PELLETS',:OLD.CONV_RPC_PELLETS ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_EXTRACT_DNA', :OLD.CONV_EXTRACT_DNA ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_SERUM_SAMP', :OLD.CONV_SERUM_SAMP ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_PLASMA_SAMP',:OLD.CONV_PLASMA_SAMP ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_NO_OF_SEG',:OLD.CONV_NO_OF_SEG ,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_NONVIABLE_CELL',:OLD.CONV_NONVIABLE_CELL,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_VIABLE_CELLSAMP',:OLD.CONV_VIABLE_CELLSAMP,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_MATR_SERSAMP',:OLD.CONV_MATR_SERSAMP,NULL,NULL,NULL);     
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_MATR_PLASAMP',:OLD.CONV_MATR_PLASAMP,NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_MATR_EXTDNA_SAMP',:OLD.CONV_MATR_EXTDNA_SAMP , NULL,NULL,NULL);
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_MISCMATR_SAMP',:OLD.CONV_MISCMATR_SAMP,NULL,NULL,NULL);

	ELSE	
	PKG_AUDIT_TRAIL_MODULE.SP_row_insert (v_rowid,'CB_ENTITY_SAMPLES',:OLD.RID,:OLD.PK_ENTITY_SAMPLES,'U',:NEW.LAST_MODIFIED_BY); 
	
	IF NVL(:OLD.PK_ENTITY_SAMPLES,0) !=     NVL(:NEW.PK_ENTITY_SAMPLES,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','PK_ENTITY_SAMPLES', :OLD.PK_ENTITY_SAMPLES, :NEW.PK_ENTITY_SAMPLES,NULL,NULL);
  END IF;  
	IF NVL(:OLD.ENTITY_ID,0) !=     NVL(:NEW.ENTITY_ID,0) THEN
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','ENTITY_ID',:OLD.ENTITY_ID, :NEW.ENTITY_ID,NULL,NULL);
  END IF;  
  IF NVL(:OLD.ENTITY_TYPE,0) !=     NVL(:NEW.ENTITY_TYPE,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','ENTITY_TYPE',:OLD.ENTITY_TYPE, :NEW.ENTITY_TYPE,NULL,NULL);
  END IF;  
   IF NVL(:OLD.FK_SAMPLE_TYPE,0) !=     NVL(:NEW.FK_SAMPLE_TYPE,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','FK_SAMPLE_TYPE',:OLD.FK_SAMPLE_TYPE, :NEW.FK_SAMPLE_TYPE,NULL,NULL);
  END IF;
  IF NVL(:OLD.FK_SAMPLE_STATUS,0) !=     NVL(:NEW.FK_SAMPLE_STATUS,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','FK_SAMPLE_STATUS',:OLD.FK_SAMPLE_STATUS, :NEW.FK_SAMPLE_STATUS,NULL,NULL);
  END IF;  
  IF NVL(:OLD.SAMPLE_QUANTITY,0) !=     NVL(:NEW.SAMPLE_QUANTITY,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','SAMPLE_QUANTITY',:OLD.SAMPLE_QUANTITY, :NEW.SAMPLE_QUANTITY,NULL,NULL);
  END IF;    
  IF NVL(:OLD.SAMPLE_VOLUME,0) !=     NVL(:NEW.SAMPLE_VOLUME,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','SAMPLE_VOLUME',:OLD.SAMPLE_VOLUME, :NEW.SAMPLE_VOLUME,NULL,NULL);
  END IF;  
  IF NVL(:OLD.QC_INDICATOR,' ') !=     NVL(:NEW.QC_INDICATOR,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'QC_INDICATOR',:OLD.QC_INDICATOR, :NEW.QC_INDICATOR,NULL,NULL);
  END IF;
  IF NVL(:OLD.RECEIVE_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.RECEIVE_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','RECEIVE_DATE',
       TO_CHAR(:OLD.RECEIVE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.RECEIVE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;  
  IF NVL(:OLD.STORAGE_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=    NVL(:NEW.STORAGE_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','STORAGE_DATE',
       TO_CHAR(:OLD.STORAGE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.STORAGE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);  
  END IF;  
  IF NVL(:OLD.SEQUENCE,0) !=     NVL(:NEW.SEQUENCE,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'SEQUENCE',:OLD.SEQUENCE, :NEW.SEQUENCE,NULL,NULL);
  END IF;  
  IF NVL(:OLD.CREATOR,0) !=     NVL(:NEW.CREATOR,0) THEN
		SELECT f_getuser(:OLD.CREATOR)        INTO OLD_CREATOR  from dual;
		SELECT f_getuser(:NEW.CREATOR)        INTO NEW_CREATOR  from dual;
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','CREATOR',OLD_CREATOR,NEW_CREATOR,NULL,NULL);
  END IF;  
  IF NVL(:OLD.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=     NVL(:NEW.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;    
	IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
		SELECT f_getuser(:OLD.LAST_MODIFIED_BY)        INTO OLD_MODIFIED_BY  from dual;
		SELECT f_getuser(:NEW.LAST_MODIFIED_BY)        INTO NEW_MODIFIED_BY  from dual;
        PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','LAST_MODIFIED_BY',OLD_MODIFIED_BY,NEW_MODIFIED_BY,NULL,NULL);
      END IF;
    IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=    NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES',
'LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;    
    IF NVL(:OLD.IP_ADD,' ') !=     NVL(:NEW.IP_ADD,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'IP_ADD', :OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);     
    END IF;    
     IF NVL(:OLD.DELETEDFLAG,' ') !=     NVL(:NEW.DELETEDFLAG,' ') THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','DELETEDFLAG',:OLD.DELETEDFLAG, :NEW.DELETEDFLAG,NULL,NULL);
    END IF;    
    IF NVL(:OLD.RID,0) !=     NVL(:NEW.RID,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','RID',:OLD.RID, :NEW.RID,NULL,NULL);     
    END IF;    
	IF NVL(:OLD.FK_SPECIMEN,0) !=     NVL(:NEW.FK_SPECIMEN,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','FK_SPECIMEN',:OLD.FK_SPECIMEN, :NEW.FK_SPECIMEN,NULL,NULL);
	END IF;
	IF NVL(:OLD.NO_OF_SEG_AVAIL,0) !=     NVL(:NEW.NO_OF_SEG_AVAIL,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_OF_SEG_AVAIL',:OLD.NO_OF_SEG_AVAIL, :NEW.NO_OF_SEG_AVAIL,NULL,NULL);
	END IF; 
	 IF NVL(:OLD.FILT_PAP_AVAIL,0) !=     NVL(:NEW.FILT_PAP_AVAIL,0) THEN
     PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','FILT_PAP_AVAIL',:OLD.FILT_PAP_AVAIL, :NEW.FILT_PAP_AVAIL,NULL,NULL);
	END IF;   
	IF NVL(:OLD.RBC_PEL_AVAIL,0) !=NVL(:NEW.RBC_PEL_AVAIL,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','RBC_PEL_AVAIL',:OLD.RBC_PEL_AVAIL, :NEW.RBC_PEL_AVAIL,NULL,NULL);     
    END IF;    
	IF NVL(:OLD.NO_EXT_DNA_ALI,0) !=NVL(:NEW.NO_EXT_DNA_ALI,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_EXT_DNA_ALI',:OLD.NO_EXT_DNA_ALI, :NEW.NO_EXT_DNA_ALI,NULL,NULL);     
    END IF; 
	IF NVL(:OLD.NO_NON_VIA_ALI,0) !=NVL(:NEW.NO_NON_VIA_ALI,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_NON_VIA_ALI',:OLD.NO_NON_VIA_ALI, :NEW.NO_NON_VIA_ALI,NULL,NULL);     
    END IF;    
	 IF NVL(:OLD.NO_SER_ALI,0) !=NVL(:NEW.NO_SER_ALI,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_SER_ALI',:OLD.NO_SER_ALI, :NEW.NO_SER_ALI,NULL,NULL);     
    END IF;
	IF NVL(:OLD.NO_PLAS_ALI,0) !=NVL(:NEW.NO_PLAS_ALI,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_PLAS_ALI',:OLD.NO_PLAS_ALI, :NEW.NO_PLAS_ALI,NULL,NULL);     
    END IF; 
	IF NVL(:OLD.NO_VIA_CEL_ALI ,0) !=NVL(:NEW.NO_VIA_CEL_ALI ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_VIA_CEL_ALI',:OLD.NO_VIA_CEL_ALI,:NEW.NO_VIA_CEL_ALI ,NULL,NULL);     
    END IF;  
    IF NVL(:OLD.NO_EXT_DNA_MAT_ALI ,0) !=     NVL(:NEW.NO_EXT_DNA_MAT_ALI ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_EXT_DNA_MAT_ALI',:OLD.NO_EXT_DNA_MAT_ALI,:NEW.NO_EXT_DNA_MAT_ALI ,NULL,NULL);  
    END IF;
	IF NVL(:OLD.NO_SER_MAT_ALI ,0) !=NVL(:NEW.NO_SER_MAT_ALI ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_SER_MAT_ALI',:OLD.NO_SER_MAT_ALI,:NEW.NO_SER_MAT_ALI ,NULL,NULL);     
    END IF;
	IF NVL(:OLD.NO_PLAS_MAT_ALI ,0) !=NVL(:NEW.NO_PLAS_MAT_ALI ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','NO_PLAS_MAT_ALI',:OLD.NO_PLAS_MAT_ALI,:NEW.NO_PLAS_MAT_ALI ,NULL,NULL);     
    END IF;
	IF NVL(:OLD.SI_NO_MISC_MAT ,0) !=NVL(:NEW.SI_NO_MISC_MAT ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES','SI_NO_MISC_MAT',:OLD.SI_NO_MISC_MAT,:NEW.SI_NO_MISC_MAT ,NULL,NULL);     
    END IF;
	   IF NVL(:OLD.CBU_OT_REP_CON_FIN ,0) !=NVL(:NEW.CBU_OT_REP_CON_FIN ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_OT_REP_CON_FIN',:OLD.CBU_OT_REP_CON_FIN,:NEW.CBU_OT_REP_CON_FIN ,NULL,NULL);    
    END IF;
	IF NVL(:OLD.CBU_NO_REP_ALT_CON ,0) !=NVL(:NEW.CBU_NO_REP_ALT_CON ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_NO_REP_ALT_CON',:OLD.CBU_NO_REP_ALT_CON,:NEW.CBU_NO_REP_ALT_CON ,NULL,NULL);     
    END IF;  
	IF NVL(:OLD.TOT_CBU_ALI_AVA ,0) !=NVL(:NEW.TOT_CBU_ALI_AVA ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'TOT_CBU_ALI_AVA',:OLD.TOT_CBU_ALI_AVA,:NEW.TOT_CBU_ALI_AVA ,NULL,NULL);     
    END IF;
IF NVL(:OLD.NO_CEL_MAT_ALI ,0) !=NVL(:NEW.NO_CEL_MAT_ALI ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'NO_CEL_MAT_ALI',:OLD.NO_CEL_MAT_ALI,:NEW.NO_CEL_MAT_ALI ,NULL,NULL);     
    END IF;
IF NVL(:OLD.TOT_MAT_ALI_AVA ,0) !=NVL(:NEW.TOT_MAT_ALI_AVA ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'TOT_MAT_ALI_AVA',:OLD.TOT_MAT_ALI_AVA,:NEW.TOT_MAT_ALI_AVA ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CBU_FILT_PAP ,0) !=NVL(:NEW.CBU_FILT_PAP ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_FILT_PAP',:OLD.CBU_FILT_PAP,:NEW.CBU_FILT_PAP ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CBU_RBC_PEL ,0) !=NVL(:NEW.CBU_RBC_PEL ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_RBC_PEL',:OLD.CBU_RBC_PEL,:NEW.CBU_RBC_PEL ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CBU_NO_EXT_DNA ,0) !=NVL(:NEW.CBU_NO_EXT_DNA ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_NO_EXT_DNA',:OLD.CBU_NO_EXT_DNA,:NEW.CBU_NO_EXT_DNA ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CBU_NO_SERUM ,0) !=NVL(:NEW.CBU_NO_SERUM ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_NO_SERUM',:OLD.CBU_NO_SERUM,:NEW.CBU_NO_SERUM ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CBU_NO_PLASMA ,0) !=NVL(:NEW.CBU_NO_PLASMA ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_NO_PLASMA',:OLD.CBU_NO_PLASMA,:NEW.CBU_NO_PLASMA ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CBU_NO_NON_VIA ,0) !=NVL(:NEW.CBU_NO_NON_VIA ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_NO_NON_VIA',:OLD.CBU_NO_NON_VIA,:NEW.CBU_NO_NON_VIA ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CBU_NO_NT_REP_FIN ,0) !=NVL(:NEW.CBU_NO_NT_REP_FIN ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_NO_NT_REP_FIN',:OLD.CBU_NO_NT_REP_FIN,:NEW.CBU_NO_NT_REP_FIN ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CBU_NO_OF_SEG ,0) !=NVL(:NEW.CBU_NO_OF_SEG ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_NO_OF_SEG',:OLD.CBU_NO_OF_SEG,:NEW.CBU_NO_OF_SEG ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CBU_OT_REP_CON_FIN ,0) !=NVL(:NEW.CBU_OT_REP_CON_FIN ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CBU_OT_REP_CON_FIN',:OLD.CBU_OT_REP_CON_FIN,:NEW.CBU_OT_REP_CON_FIN ,NULL,NULL);     
    END IF;
IF NVL(:OLD.MT_NO_MAT_SER ,0) !=NVL(:NEW.MT_NO_MAT_SER ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'MT_NO_MAT_SER',:OLD.MT_NO_MAT_SER,:NEW.MT_NO_MAT_SER ,NULL,NULL);     
    END IF;
IF NVL(:OLD.MT_NO_MAT_PAL ,0) !=NVL(:NEW.MT_NO_MAT_PAL ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'MT_NO_MAT_PAL',:OLD.MT_NO_MAT_PAL,:NEW.MT_NO_MAT_PAL ,NULL,NULL);     
    END IF;
IF NVL(:OLD.MT_NO_MAT_EXT_DNA ,0) !=NVL(:NEW.MT_NO_MAT_EXT_DNA ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'MT_NO_MAT_EXT_DNA',:OLD.MT_NO_MAT_EXT_DNA,:NEW.MT_NO_MAT_EXT_DNA ,NULL,NULL);     
    END IF;
IF NVL(:OLD.MT_NO_MIS_MAT ,0) !=NVL(:NEW.MT_NO_MIS_MAT ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'MT_NO_MIS_MAT',:OLD.MT_NO_MIS_MAT,:NEW.MT_NO_MIS_MAT ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CONV_FIL_PAP_SAMP ,0) !=NVL(:NEW.CONV_FIL_PAP_SAMP ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_FIL_PAP_SAMP',:OLD.CONV_FIL_PAP_SAMP,:NEW.CONV_FIL_PAP_SAMP ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CONV_RPC_PELLETS ,0) !=NVL(:NEW.CONV_RPC_PELLETS ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_RPC_PELLETS',:OLD.CONV_RPC_PELLETS,:NEW.CONV_RPC_PELLETS ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CONV_EXTRACT_DNA ,0) !=NVL(:NEW.CONV_EXTRACT_DNA ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_EXTRACT_DNA',:OLD.CONV_EXTRACT_DNA,:NEW.CONV_EXTRACT_DNA ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CONV_SERUM_SAMP ,0) !=NVL(:NEW.CONV_SERUM_SAMP ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_SERUM_SAMP',:OLD.CONV_SERUM_SAMP,:NEW.CONV_SERUM_SAMP ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CONV_PLASMA_SAMP ,0) !=NVL(:NEW.CONV_PLASMA_SAMP ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_PLASMA_SAMP',:OLD.CONV_PLASMA_SAMP,:NEW.CONV_PLASMA_SAMP ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CONV_NO_OF_SEG ,0) !=NVL(:NEW.CONV_NO_OF_SEG ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_NO_OF_SEG',:OLD.CONV_NO_OF_SEG ,:NEW.CONV_NO_OF_SEG ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CONV_NONVIABLE_CELL ,0) != NVL(:NEW.CONV_NONVIABLE_CELL ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_NONVIABLE_CELL',:OLD.CONV_NONVIABLE_CELL ,:NEW.CONV_NONVIABLE_CELL ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CONV_VIABLE_CELLSAMP ,0) !=  NVL(:NEW.CONV_VIABLE_CELLSAMP ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_VIABLE_CELLSAMP',:OLD.CONV_VIABLE_CELLSAMP,:NEW.CONV_VIABLE_CELLSAMP ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CONV_MATR_SERSAMP ,0) != NVL(:NEW.CONV_MATR_SERSAMP ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_MATR_SERSAMP',:OLD.CONV_MATR_SERSAMP,:NEW.CONV_MATR_SERSAMP ,NULL,NULL);     
    END IF;
IF NVL(:OLD.CONV_MATR_PLASAMP ,0) != NVL(:NEW.CONV_MATR_PLASAMP ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_MATR_PLASAMP',:OLD.CONV_MATR_PLASAMP,:NEW.CONV_MATR_PLASAMP ,NULL,NULL);
    END IF;
	IF NVL(:OLD.CONV_MATR_EXTDNA_SAMP ,0) != NVL(:NEW.CONV_MATR_EXTDNA_SAMP ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_MATR_EXTDNA_SAMP',:OLD.CONV_MATR_EXTDNA_SAMP,:NEW.CONV_MATR_EXTDNA_SAMP ,NULL,NULL);
    END IF;
	IF NVL(:OLD.CONV_MISCMATR_SAMP ,0) != NVL(:NEW.CONV_MISCMATR_SAMP ,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid,'CB_ENTITY_SAMPLES', 'CONV_MISCMATR_SAMP',:OLD.CONV_MISCMATR_SAMP,:NEW.CONV_MISCMATR_SAMP,NULL,NULL);
    END IF;
  END IF;  
end ;
/
