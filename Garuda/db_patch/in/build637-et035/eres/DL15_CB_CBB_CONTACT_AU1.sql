 CREATE OR REPLACE TRIGGER "CB_CBB_CONTACT_AU1" AFTER UPDATE ON CB_CBB_CONTACT 
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE 
  V_ROWID NUMBER(10);/*VARIABLE USED TO FETCH VALUE OF SEQUENCE */
	NEW_MODIFIED_BY VARCHAR2(200);
	NEW_CREATOR VARCHAR2(200);
	OLD_CREATOR VARCHAR2(200);
	OLD_MODIFIED_BY VARCHAR2(200);
	OLD_CODLST_ALERT VARCHAR2(200);
	NEW_CODLST_ALERT VARCHAR2(200);
	OLD_USERID VARCHAR2(200);
	NEW_USERID VARCHAR2(200);
	
  BEGIN   
     SELECT SEQ_AUDIT_ROW_MODULE.NEXTVAL INTO V_ROWID FROM DUAL;
    
    PKG_AUDIT_TRAIL_MODULE.SP_ROW_INSERT (V_ROWID,'CB_CBB_CONTACT',:OLD.RID,:OLD.PK_CBB_CONTACT,'U',:NEW.LAST_MODIFIED_BY);
 
	IF NVL(:old.PK_CBB_CONTACT,0) != NVL(:new.PK_CBB_CONTACT,0) THEN
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_CBB_CONTACT', 'PK_CBB_CONTACT', :OLD.PK_CBB_CONTACT, :NEW.PK_CBB_CONTACT,NULL,NULL);
    END IF;
	 IF NVL(:old.FK_SITE_ID,0) != NVL(:new.FK_SITE_ID,0) THEN
	  SELECT F_GET_ER_SITE_NAME(:OLD.FK_SITE_ID)  INTO OLD_CODLST_ALERT   FROM dual;
	  SELECT F_GET_ER_SITE_NAME(:NEW.FK_SITE_ID)  INTO NEW_CODLST_ALERT   FROM dual;
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_CBB_CONTACT', 'FK_SITE_ID',OLD_CODLST_ALERT, NEW_CODLST_ALERT,NULL,NULL);
    END IF;
	if NVL(:old.ROLE,' ') != NVL(:new.ROLE,' ') then
      PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_CBB_CONTACT', 'ROLE',:OLD.ROLE,:NEW.ROLE,NULL,NULL);
    END IF;
	IF NVL(:old.CREATOR,0) != NVL(:new.CREATOR,0) THEN
		SELECT f_getuser(:OLD.CREATOR)        INTO OLD_CREATOR  from dual;
		SELECT f_getuser(:NEW.CREATOR)        INTO NEW_CREATOR  from dual;
      pkg_audit_trail_module.sp_column_insert (V_ROWID,'CB_CBB_CONTACT', 'CREATOR',OLD_CREATOR,NEW_CREATOR,NULL,NULL);
    END IF;
	  IF NVL(:old.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:new.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
      pkg_audit_trail_module.sp_column_insert (V_ROWID,'CB_CBB_CONTACT', 'CREATED_ON', TO_CHAR(:old.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:new.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;     
    IF NVL(:old.ip_add,' ') != NVL(:new.ip_add,' ') THEN
      pkg_audit_trail_module.sp_column_insert (V_ROWID,'CB_CBB_CONTACT', 'IP_ADD', :old.ip_add, :new.ip_add,NULL,NULL);
    END IF;
    IF NVL(:OLD.LAST_MODIFIED_BY,0) != NVL(:NEW.LAST_MODIFIED_BY,0) THEN
	SELECT f_getuser(:OLD.LAST_MODIFIED_BY)        INTO OLD_MODIFIED_BY  from dual;
	SELECT f_getuser(:NEW.LAST_MODIFIED_BY)        INTO NEW_MODIFIED_BY  from dual;
        PKG_AUDIT_TRAIL_MODULE.SP_COLUMN_INSERT (V_ROWID,'CB_CBB_CONTACT','LAST_MODIFIED_BY',OLD_MODIFIED_BY,NEW_MODIFIED_BY,NULL,NULL);
      END IF; 
    IF NVL(:old.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:new.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
      pkg_audit_trail_module.sp_column_insert (V_ROWID,'CB_CBB_CONTACT', 'LAST_MODIFIED_DATE', TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
    END IF;
    IF NVL(:old.RID,0) != NVL(:new.RID,0) THEN
      pkg_audit_trail_module.sp_column_insert (V_ROWID,'CB_CBB_CONTACT', 'RID', :OLD.RID, :NEW.RID,NULL,NULL);
    END IF;
    IF NVL(:old.DELETEDFLAG,0) != NVL(:new.DELETEDFLAG,0) THEN
      pkg_audit_trail_module.sp_column_insert (V_ROWID,'CB_CBB_CONTACT', 'DELETEDFLAG', :old.DELETEDFLAG, :new.DELETEDFLAG,NULL,NULL);
    end if;
		if NVL(:old.FK_USER_ID,0) != NVL(:new.FK_USER_ID,0) then
		SELECT f_getuser(:OLD.FK_USER_ID)        INTO OLD_USERID  from dual;
		SELECT f_getuser(:NEW.FK_USER_ID)        INTO NEW_USERID  from dual;
	PKG_AUDIT_TRAIL_MODULE.SP_column_insert (v_rowid, 'CB_CBB_CONTACT','FK_USER_ID',OLD_USERID,NEW_USERID,NULL,NULL);
	END IF;	
  end;
	/