create or replace
FUNCTION f_lab_summary(
      pk_cordid NUMBER)
    RETURN SYS_REFCURSOR
  IS
    p_lab_refcur SYS_REFCURSOR;
    v_specimen NUMBER;
  BEGIN
    SELECT fk_specimen_id
    INTO v_specimen
    FROM cb_cord
    WHERE pk_cord=pk_cordid;
    OPEN p_lab_refcur FOR SELECT a.pk_labtest,
    a.labtest_name,
    a.LABTEST_SHORTNAME,
    a.labtest_seq ,
    (SELECT test_result
    FROM er_patlabs
    WHERE fk_timing_of_test = f_codelst_id('timing_of_test','pre_procesing')
    AND fk_testid           = a.pk_labtest
    AND fk_specimen         = v_specimen
    ) pre_test_result ,
    (SELECT TO_CHAR(test_date,'Mon DD,YYYY')
    FROM er_patlabs
    WHERE fk_timing_of_test = f_codelst_id('timing_of_test','pre_procesing')
    AND fk_testid           = a.pk_labtest
    AND fk_specimen         = v_specimen
    ) pre_test_date ,
    (SELECT MAX(test_result)
    FROM er_patlabs
    WHERE pk_patlabs=
      (SELECT MAX(pk_patlabs)
      FROM er_patlabs
      WHERE fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing')
      AND fk_testid           = a.pk_labtest
      AND fk_specimen         = v_specimen
      )
    ) post_test_result ,
    (SELECT TO_CHAR(test_date,'Mon DD,YYYY')
    FROM er_patlabs
    WHERE pk_patlabs=
      (SELECT MAX(pk_patlabs)
      FROM er_patlabs
      WHERE fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing')
      AND fk_testid           = a.pk_labtest
      AND fk_specimen         = v_specimen
      )
    ) post_test_date ,
    f_codelst_desc(
    (SELECT fk_test_method
    FROM er_patlabs
    WHERE pk_patlabs=
      (SELECT MAX(pk_patlabs)
      FROM er_patlabs
      WHERE fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing')
      AND fk_testid           = a.pk_labtest
      AND fk_specimen         = v_specimen
      )
    )) post_testmeth ,
    (SELECT custom005
    FROM er_patlabs
    WHERE pk_patlabs=
      (SELECT MAX(pk_patlabs)
      FROM er_patlabs
      WHERE fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing')
      AND fk_testid           = a.pk_labtest
      AND fk_specimen         = v_specimen
      )
    ) post_test_desc ,
    (SELECT test_result
    FROM er_patlabs
    WHERE pk_patlabs=
      (SELECT MAX(pk_patlabs)
      FROM er_patlabs
      WHERE custom006 = 'post_proc_thaw'
      AND fk_testid   = a.pk_labtest
      AND fk_specimen = v_specimen
      )
    ) post_cryopres_thaw ,
    (SELECT TO_CHAR(test_date,'Mon DD,YYYY')
    FROM er_patlabs
    WHERE pk_patlabs=
      (SELECT MAX(pk_patlabs)
      FROM er_patlabs
      WHERE custom006 = 'post_proc_thaw'
      AND fk_testid   = a.pk_labtest
      AND fk_specimen = v_specimen
      )
    ) post_cryopres_date ,
    f_codelst_desc(
    (SELECT fk_test_reason
    FROM er_patlabs
    WHERE pk_patlabs=
      (SELECT MAX(pk_patlabs)
      FROM er_patlabs
      WHERE custom006 = 'post_proc_thaw'
      AND fk_testid   = a.pk_labtest
      AND fk_specimen = v_specimen
      )
    )) reason_thaw ,
    (SELECT custom004
    FROM er_patlabs
    WHERE pk_patlabs=
      (SELECT MAX(pk_patlabs)
      FROM er_patlabs
      WHERE custom006 = 'post_proc_thaw'
      AND fk_testid   = a.pk_labtest
      AND fk_specimen = v_specimen
      )
    ) reason_desc_thaw ,
    f_codelst_desc(
    (SELECT fk_test_specimen
    FROM er_patlabs
    WHERE pk_patlabs=
      (SELECT MAX(pk_patlabs)
      FROM er_patlabs
      WHERE custom006 = 'post_proc_thaw'
      AND fk_testid   = a.pk_labtest
      AND fk_specimen = v_specimen
      )
    )) smpltype_thaw ,
    f_codelst_desc(
    (SELECT fk_test_method
    FROM er_patlabs
    WHERE pk_patlabs=
      (SELECT MAX(pk_patlabs)
      FROM er_patlabs
      WHERE custom006 = 'post_proc_thaw'
      AND fk_testid   = a.pk_labtest
      AND fk_specimen = v_specimen
      )
    )) testmeth_thaw ,
    (SELECT custom005
    FROM er_patlabs
    WHERE pk_patlabs=
      (SELECT MAX(pk_patlabs)
      FROM er_patlabs
      WHERE custom006 = 'post_proc_thaw'
      AND fk_testid   = a.pk_labtest
      AND fk_specimen = v_specimen
      )
    ) test_desc_thaw ,
    (SELECT test_result
    FROM er_patlabs
    WHERE custom006 = '1'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) post_cryopres_thaw1 ,
    (SELECT TO_CHAR(test_date,'Mon DD,YYYY')
    FROM er_patlabs
    WHERE custom006 = '1'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) post_cryopres_date1 ,
    f_codelst_desc(
    (SELECT fk_test_reason
    FROM er_patlabs
    WHERE custom006 = '1'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) reason_thaw1 ,
    (SELECT custom004
    FROM er_patlabs
    WHERE custom006 = '1'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) reason_desc_thaw1 ,
    f_codelst_desc(
    (SELECT fk_test_specimen
    FROM er_patlabs
    WHERE custom006 = '1'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) smpltype_thaw1 ,
    f_codelst_desc(
    (SELECT fk_test_method
    FROM er_patlabs
    WHERE custom006 = '1'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) testmeth_thaw1 ,
    (SELECT custom005
    FROM er_patlabs
    WHERE custom006 = '1'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) test_desc_thaw1 ,
    (SELECT test_result
    FROM er_patlabs
    WHERE custom006 = '2'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) post_cryopres_thaw2 ,
    (SELECT TO_CHAR(test_date,'Mon DD,YYYY')
    FROM er_patlabs
    WHERE custom006 = '2'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) post_cryopres_date2 ,
    f_codelst_desc(
    (SELECT fk_test_reason
    FROM er_patlabs
    WHERE custom006 = '2'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) reason_thaw2 ,
    (SELECT custom004
    FROM er_patlabs
    WHERE custom006 = '2'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) reason_desc_thaw2 ,
    f_codelst_desc(
    (SELECT fk_test_specimen
    FROM er_patlabs
    WHERE custom006 = '2'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) smpltype_thaw2 ,
    f_codelst_desc(
    (SELECT fk_test_method
    FROM er_patlabs
    WHERE custom006 = '2'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) testmeth_thaw2 ,
    (SELECT custom005
    FROM er_patlabs
    WHERE custom006 = '2'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) test_desc_thaw2 ,
    (SELECT test_result
    FROM er_patlabs
    WHERE custom006 = '3'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) post_cryopres_thaw3 ,
    (SELECT TO_CHAR(test_date,'Mon DD,YYYY')
    FROM er_patlabs
    WHERE custom006 = '3'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) post_cryopres_date3 ,
    f_codelst_desc(
    (SELECT fk_test_reason
    FROM er_patlabs
    WHERE custom006 = '3'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) reason_thaw3 ,
    (SELECT custom004
    FROM er_patlabs
    WHERE custom006 = '3'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) reason_desc_thaw3 ,
    f_codelst_desc(
    (SELECT fk_test_specimen
    FROM er_patlabs
    WHERE custom006 = '3'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) smpltype_thaw3 ,
    f_codelst_desc(
    (SELECT fk_test_method
    FROM er_patlabs
    WHERE custom006 = '3'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) testmeth_thaw3 ,
    (SELECT custom005
    FROM er_patlabs
    WHERE custom006 = '3'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) test_desc_thaw3 ,
    (SELECT test_result
    FROM er_patlabs
    WHERE custom006 = '4'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) post_cryopres_thaw4 ,
    (SELECT TO_CHAR(test_date,'Mon DD,YYYY')
    FROM er_patlabs
    WHERE custom006 = '4'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) post_cryopres_date4 ,
    f_codelst_desc(
    (SELECT fk_test_reason
    FROM er_patlabs
    WHERE custom006 = '4'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) reason_thaw4 ,
    (SELECT custom004
    FROM er_patlabs
    WHERE custom006 = '4'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) reason_desc_thaw4 ,
    f_codelst_desc(
    (SELECT fk_test_specimen
    FROM er_patlabs
    WHERE custom006 = '4'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) smpltype_thaw4 ,
    f_codelst_desc(
    (SELECT fk_test_method
    FROM er_patlabs
    WHERE custom006 = '4'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) testmeth_thaw4 ,
    (SELECT custom005
    FROM er_patlabs
    WHERE custom006 = '4'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) test_desc_thaw4 ,
    (SELECT test_result
    FROM er_patlabs
    WHERE custom006 = '5'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) post_cryopres_thaw5 ,
    (SELECT TO_CHAR(test_date,'Mon DD,YYYY')
    FROM er_patlabs
    WHERE custom006 = '5'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) post_cryopres_date5 ,
    f_codelst_desc(
    (SELECT fk_test_reason
    FROM er_patlabs
    WHERE custom006 = '5'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) reason_thaw5 ,
    (SELECT custom004
    FROM er_patlabs
    WHERE custom006 = '5'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) reason_desc_thaw5 ,
    f_codelst_desc(
    (SELECT fk_test_specimen
    FROM er_patlabs
    WHERE custom006 = '5'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) smpltype_thaw5 ,
    f_codelst_desc(
    (SELECT fk_test_method
    FROM er_patlabs
    WHERE custom006 = '5'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    )) testmeth_thaw5 ,
    (SELECT custom005
    FROM er_patlabs
    WHERE custom006 = '5'
    AND fk_testid   = a.pk_labtest
    AND fk_specimen = v_specimen
    ) test_desc_thaw5 FROM er_labtest a,
    er_labtestgrp lgrp,
    er_labgroup grp WHERE lgrp.fk_labgroup=grp.pk_labgroup AND lgrp.fk_testid=a.pk_labtest AND grp.group_name='PrcsGrp' AND grp.group_type='PC' order by labtest_seq;
    RETURN p_lab_refcur;
  END;
  /
