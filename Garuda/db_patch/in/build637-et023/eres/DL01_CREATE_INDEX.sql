CREATE INDEX IDX_ER_ORD_HDR_FK_SITE_ID ON ER_ORDER_HEADER(FK_SITE_ID);
CREATE INDEX IDX_ER_ORD_ORDER_STATUS ON ER_ORDER(ORDER_STATUS);
CREATE INDEX IDX_ER_ORD_ORDER_TYPE ON ER_ORDER(ORDER_TYPE);