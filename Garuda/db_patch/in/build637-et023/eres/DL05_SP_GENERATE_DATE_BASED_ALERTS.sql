create or replace
procedure sp_generate_date_based_alerts as
v_entity_order number;
v_orderid number;
begin
for orders in(
select 
  pk_order 
from 
  er_order,
  er_order_header,  
  cb_shipment 
where er_order.fk_order_header=er_order_header.pk_order_header 
and cb_shipment.fk_order_id=er_order.pk_order 
and  TO_DATE(TO_CHAR(CB_SHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')=2
AND (ER_ORDER.FINAL_REVIEW_TASK_FLAG='N'
OR ER_ORDER.FINAL_REVIEW_TASK_FLAG IS NULL)
AND ER_ORDER.ORDER_TYPE=(SELECT PK_CODELST
  FROM ER_CODELST
  WHERE CODELST_TYPE='order_type'
  AND CODELST_SUBTYP='OR'
  )
AND ER_ORDER.ORDER_STATUS NOT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' and CODELST_SUBTYP IN('close_ordr','resolved'))
UNION
SELECT pk_order
FROM er_order,
  er_order_header,
  cb_shipment
WHERE er_order.fk_order_header=er_order_header.pk_order_header
AND cb_shipment.fk_order_id=er_order.pk_order
AND ER_ORDER.ORDER_TYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_type' and CODELST_SUBTYP ='OR')
AND CB_SHIPMENT.FK_SHIPMENT_TYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='shipment_type' and CODELST_SUBTYP ='CBU')
AND TO_DATE(TO_CHAR(CB_SHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')<-2
AND (CB_SHIPMENT.SHIPMENT_CONFIRM='N'
OR CB_SHIPMENT.SHIPMENT_CONFIRM IS NULL)
AND ER_ORDER.ORDER_STATUS NOT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' and CODELST_SUBTYP IN('close_ordr','resolved'))
UNION
SELECT pk_order
FROM er_order,
  er_order_header,
  cb_shipment
WHERE er_order.fk_order_header=er_order_header.pk_order_header
AND cb_shipment.fk_order_id=er_order.pk_order
AND ER_ORDER.ORDER_TYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_type' and CODELST_SUBTYP ='OR')
AND CB_SHIPMENT.FK_SHIPMENT_TYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='shipment_type' and CODELST_SUBTYP ='CBU')
AND TO_DATE(TO_CHAR(CB_SHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')<-7
AND (CB_SHIPMENT.SHIPMENT_CONFIRM='N'
OR CB_SHIPMENT.SHIPMENT_CONFIRM IS NULL)
AND ER_ORDER.ORDER_STATUS NOT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' and CODELST_SUBTYP IN('close_ordr','resolved'))
UNION
SELECT pk_order
FROM er_order,
  er_order_header,
  cb_shipment
WHERE er_order.fk_order_header=er_order_header.pk_order_header
AND cb_shipment.fk_order_id=er_order.pk_order
AND ER_ORDER.RECENT_HLA_TYPING_AVAIL='Y'
AND ER_ORDER.ADDITI_TYPING_FLAG='Y'
AND (ER_ORDER.RECENT_HLA_ENTERED_FLAG IS NULL
OR ER_ORDER.RECENT_HLA_ENTERED_FLAG='N')
AND TO_DATE(TO_CHAR(CB_SHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')>1
AND ER_ORDER.ORDER_STATUS NOT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' and CODELST_SUBTYP IN('close_ordr','resolved'))
UNION
SELECT pk_order
FROM er_order,
  er_order_header,
  cb_shipment
WHERE er_order.fk_order_header=er_order_header.pk_order_header
AND cb_shipment.fk_order_id=er_order.pk_order
AND TO_DATE(TO_CHAR(ER_ORDER_HEADER.ORDER_OPEN_DATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')<-2
AND ER_ORDER.ORDER_TYPE=(SELECT PK_CODELST
  FROM ER_CODELST
  WHERE CODELST_TYPE='order_type'
  AND CODELST_SUBTYP='CT'
  )
AND CB_SHIPMENT.FK_SHIPMENT_TYPE=(SELECT PK_CODELST
  FROM ER_CODELST
  WHERE CODELST_TYPE='shipment_type'
  AND CODELST_SUBTYP='CBU'
  )
AND CB_SHIPMENT.SCH_SHIPMENT_DATE IS NULL
AND ER_ORDER.ORDER_STATUS NOT IN (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' and CODELST_SUBTYP IN('close_ordr','resolved')))
loop
  v_orderid:=orders.pk_order;
  sp_alerts(v_orderid,v_entity_order,null);
end loop;
end;
/
