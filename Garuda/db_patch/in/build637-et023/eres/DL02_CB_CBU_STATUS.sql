--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE = 'DT'
    AND CODE_TYPE= 'Resolution';
  if (v_record_exists = 1) then
      UPDATE 
  CB_CBU_STATUS SET FK_CBU_STATUS=(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CODE_TYPE= 'Response' AND CBU_STATUS_CODE = 'AV') where CBU_STATUS_CODE = 'DT'
    AND CODE_TYPE= 'Resolution';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_38');
  if (v_record_exists = 2) then
      UPDATE 
  CB_ALERT_CONDITIONS SET CONDITION_VALUE='(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''SD'')' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_38');
	commit;
  end if;
end;
/
--END--
