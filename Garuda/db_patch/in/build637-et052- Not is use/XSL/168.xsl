<?xml version="1.0"?> 
<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:rf="com.velos.ordercomponent.util.GetResourceBundleData" >

<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>

<xsl:variable name="props" select="rf:new()" />
<xsl:variable name="vartemp" select="'&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;'" />
<xsl:variable name="newline">
<xsl:text>
</xsl:text>
</xsl:variable>

<xsl:preserve-space elements="vartemp" />



<xsl:template name="body_css">
    <xsl:attribute name="style"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.style.data')" /></xsl:attribute>
</xsl:template>


<xsl:attribute-set name="font-style">
  <xsl:attribute name="fname"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.header.style.font')" /></xsl:attribute>
  <xsl:attribute name="size"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.header.style.fontsize')" /></xsl:attribute>
</xsl:attribute-set>

<xsl:key name="k1" match="//OTHER_TEST_RES/OTHER_TEST_RES_ROW" use="TEST_FIND1"/>
<xsl:key name="k2" match="//OTHER_TEST_RES/OTHER_TEST_RES_ROW" use="TEST_FIND2"/>


<xsl:attribute-set name="style">
  <xsl:attribute name="padding-left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.header.style.tabstyle')" /></xsl:attribute>
</xsl:attribute-set>

<xsl:template match="/">
<HTML>
<HEAD>
</HEAD>
<BODY>
<table width ="100%">
<tr><td>
<xsl:call-template name="body_css"/>
<table WIDTH="100%"  xsl:use-attribute-sets="font-style">
<tr>
<td WIDTH="100%" ALIGN="CENTER" style="rf:getPropertiesData($props, 'garuda.report.header.style.data')">
<p align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.heading2')"  disable-output-escaping="yes"/><xsl:value-of select="$newline" /></b></p>
<p align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.comprehensivereport.htitle')" /></b></p>
</td>
</tr>
</table>
<br></br>
<br></br>
<xsl:apply-templates select="ROWSET"/>
</td></tr>
</table>
</BODY>
</HTML>
</xsl:template> 


<xsl:template match="ROWSET">

<xsl:variable name="V_NEW_TASK_FLAG" select="ROW/NEW_TASK_FLAG" />

<table>
<tr>
<td align="left">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.cbbRegid')" />&#160;<xsl:value-of select="ROW/CBBID"/>
<br></br>
<br></br>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.cbbid')" />&#160;<xsl:value-of select="ROW/CBB_ID"/>
</td>
</tr>
</table>


<b><hr class="thickLine" width="100%"/></b>
<b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.title.idsno')" /></b>
<b><hr class="thickLine" width="100%"/></b>

<br></br>
<p><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.subtile1')" /></p> 
<br></br>
<table width="90%">
<tr><TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.cbuidonbag')" />&#160;<xsl:value-of select="ROW/CORD_ID_NUMBER_ON_CBU_BAG" /></TD></tr>
<tr><TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.cburegids')" />&#160;<xsl:value-of select="ROW/CORD_REGISTRY_ID" /></TD></tr>
<tr><TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.cbulocid')" />&#160;<xsl:value-of select="ROW/CORD_LOCAL_CBU_ID" /></TD></tr>
<tr><TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.isbtdin')" />&#160;<xsl:value-of select="ROW/CORD_ISBI_DIN_CODE" /></TD></tr>
<tr><td>
<xsl:for-each select="//ROW/ADDIDS/ADDIDS_ROW">
<xsl:if test="IDTYPE=rf:getPropertiesData($props, 'garuda.report.addids.cbu')">
<xsl:value-of select="ADDID_DESC" />:<xsl:value-of select="ADDIDVAL" />&#160;<br></br>
</xsl:if>
</xsl:for-each>
</td></tr>
</table>

<b><hr class="thickLine" width="90%"/></b>


<p><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.subtile2')" /></p>


<table width="90%">
<tr><td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.matregid')" />&#160;<xsl:value-of select="ROW/REGISTRY_MATERNAL_ID" /></td></tr>
<tr><td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.matlocid')" />&#160;<xsl:value-of select="ROW/MATERNAL_LOCAL_ID" /></td></tr>
<tr><td>
<xsl:for-each select="//ROW/ADDIDS/ADDIDS_ROW">
<xsl:if test="IDTYPE=rf:getPropertiesData($props, 'garuda.report.addids.maternal')">
<xsl:value-of select="ADDID_DESC" />: <xsl:value-of select="ADDIDVAL" />&#160;<br></br>
</xsl:if>
</xsl:for-each>
</td></tr>
</table>


<hr class="thickLine" width="90%"/>
<p><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.heading')" /></p><br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedby')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.category')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedon')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.notes')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.assessment')" /></TD>
</tr>
<xsl:for-each select="//ROW/IDS_NOTES/IDS_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:if test="not(//ROW/IDS_NOTES/IDS_NOTES_ROW)">
<tr colspan="5"><td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.norecordfound')" /></td></tr>
</xsl:if>
</table>
<br></br>
<br></br>
<!--<br></br>
<table>
<tr>
<td  align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.nameofperson')" />
<xsl:choose>
<xsl:when test="CREATEDBY=''">
	<xsl:value-of select="LASTMODIFIEDBY" />
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="CREATEDBY" /> 
</xsl:otherwise>
</xsl:choose>
</td>

</tr>
<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.esignprovided')" />
<xsl:choose>
<xsl:when test="LASTMODIFIEDBY=''">
	<xsl:value-of select="CREATEDBY" /> 
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="LASTMODIFIEDBY" /> 
</xsl:otherwise>

</xsl:choose>
</td>
</tr>
<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.completionofform')" />
<xsl:choose>
<xsl:when test="LAST_MODIFIED_DT=''">
	 <xsl:value-of select="CREATEDON" />
</xsl:when>
<xsl:otherwise>
	 <xsl:value-of select="LAST_MODIFIED_DT" />
</xsl:otherwise>

</xsl:choose>
</td>

</tr>
</table>-->


<b><hr class="thickLine" width="100%"/>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hcbuinfo')" />
<hr class="thickLine" width="100%"/></b>
<table width="90%">
<tr>
<td>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.birthdate')" />&#160;
<xsl:value-of select="ROW/BABY_BIRTH_DT" />
</td>
<td>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.ethnicity')" />&#160;
<xsl:value-of select="ROW/ETHNICITY" />
</td>
</tr>
<tr>
<td>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.collectiondate')" />&#160;
<xsl:value-of select="ROW/COLLEC_DT" />
</td>
<td>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.race')" />&#160;
<xsl:value-of select="ROW/RACE" />
</td>
</tr>
<tr>
<td>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.gender')" />&#160;
<xsl:value-of select="ROW/GENDER" />
</td>
<td>
</td>
</tr>
</table>

<br></br>
<hr class="thickLine" width="100%"/>
<br></br>

<p><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.heading')" /></p>

<br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedby')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.category')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedon')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.notes')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.assessment')" /></TD>
</tr>
<xsl:for-each select="//ROW/CBU_INFO_NOTES/CBU_INFO_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:if test="not(//ROW/CBU_INFO_NOTES/CBU_INFO_NOTES_ROW)">
<tr colspan="5"><td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.norecordfound')" /></td></tr>
</xsl:if>
</table>



<!--<br></br>
<font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.document.heading')" /></font> 

<br></br><br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.document.category')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfReport.document.filename')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfReport.document.createdon')" /></TD>
</tr>
<xsl:for-each select="//CBU_ATTACHMENTS/CBU_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>

</table>


<br></br>
<table>
<tr>
<td  align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.nameofperson')" />
<xsl:choose>
<xsl:when test="CREATEDBY=''">
	<xsl:value-of select="LASTMODIFIEDBY" />
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="CREATEDBY" /> 
</xsl:otherwise>
</xsl:choose>
</td>

</tr>
<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.esignprovided')" />
<xsl:choose>
<xsl:when test="LASTMODIFIEDBY=''">
	<xsl:value-of select="CREATEDBY" /> 
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="LASTMODIFIEDBY" /> 
</xsl:otherwise>

</xsl:choose>
</td>
</tr>
<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.completionofform')" />
<xsl:choose>
<xsl:when test="LAST_MODIFIED_DT=''">
	 <xsl:value-of select="CREATEDON" />
</xsl:when>
<xsl:otherwise>
	 <xsl:value-of select="LAST_MODIFIED_DT" />
</xsl:otherwise>

</xsl:choose>
</td>

</tr>
</table>-->



<br></br>
<hr class="thickLine" width="100%"/>
<p><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hhla')" /></b></p>
<hr class="thickLine" width="100%"/>
<br></br>


<p><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.cbuhla')" /></p>
<br></br>

<table border="1" bordercolor="black" width="90%">
<tr>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypea')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypeb')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypec')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb1')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb3')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb4')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb5')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladqb1')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladpb1')" /></b></td>
</tr>

<xsl:if test="//ROW/BEST_HLA/BEST_HLA_ROW">
<tr>

<td>
<xsl:for-each select="//ROW/BEST_HLA/BEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_a')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>

<td>
<xsl:for-each select="//ROW/BEST_HLA/BEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_b')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>

<td>
<xsl:for-each select="//ROW/BEST_HLA/BEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_c')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/BEST_HLA/BEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb1')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/BEST_HLA/BEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb3')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/BEST_HLA/BEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb4')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/BEST_HLA/BEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb5')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/BEST_HLA/BEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_dqb1')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/BEST_HLA/BEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_dpb1')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>
</tr>
</xsl:if>
<xsl:if test="not(//ROW/BEST_HLA/BEST_HLA_ROW)">
<tr colspan="9"><td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.norecordfound')" /></td></tr>
</xsl:if>
</table>


<br></br>

<table border="1" bordercolor="black" width="90%">
<tr>
<td align="center" colspan="2"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypingdt')" /></b></td>
<td align="center" colspan="2"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlaentrydt')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.source')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypea')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypeb')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypec')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb1')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb3')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb4')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb5')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladqb1')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladpb1')" /></b></td>
</tr>
<xsl:if test="//ROW/HLA/HLA_ROW">
<xsl:for-each select="//ROW/DIST_CBU/DIST_CBU_ROW">
<xsl:variable name="tempseqval" select="SEQDATA" />
<xsl:variable name="tempcreator" select="USRNAME1" />
<xsl:variable name="tempcreatedon1" select="CREATED1" />
<xsl:variable name="tempcreatedon2" select="CREATED2" />

<tr>
<td colspan="2" align="center">
<xsl:value-of select="$tempcreatedon1" />
</td>


<td colspan="2" align="center">
<xsl:value-of select="$tempcreatedon2" />
</td>

<td>
<xsl:value-of select="$tempcreator" />
</td>


<td>
<xsl:for-each select="//ROW/HLA/HLA_ROW">
<xsl:if test="SEQVALHLA=$tempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_a')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>

<td>
<xsl:for-each select="//ROW/HLA/HLA_ROW">
<xsl:if test="SEQVALHLA=$tempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_b')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>

</td>

<td>
<xsl:for-each select="//ROW/HLA/HLA_ROW">
<xsl:if test="SEQVALHLA=$tempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_c')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/HLA/HLA_ROW">
<xsl:if test="SEQVALHLA=$tempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb1')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/HLA/HLA_ROW">
<xsl:if test="SEQVALHLA=$tempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb3')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/HLA/HLA_ROW">
<xsl:if test="SEQVALHLA=$tempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb4')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/HLA/HLA_ROW">
<xsl:if test="SEQVALHLA=$tempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb5')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/HLA/HLA_ROW">
<xsl:if test="SEQVALHLA=$tempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_dqb1')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/HLA/HLA_ROW">
<xsl:if test="SEQVALHLA=$tempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_dpb1')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>
</tr>
</xsl:for-each>
</xsl:if>



<xsl:if test="not(//ROW/HLA/HLA_ROW)">
<tr colspan="14"><td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.norecordfound')" /></td></tr>
</xsl:if>
</table>



<br></br>
<p><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.maternalhla')" /></p>
<br></br>


<br></br>

<table border="1" bordercolor="black" width="90%">
<tr>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypea')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypeb')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypec')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb1')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb3')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb4')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb5')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladqb1')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladpb1')" /></b></td>
</tr>

<xsl:if test="not(//ROW/MBEST_HLA/MBEST_HLA_ROW)">
<tr colspan="9"><td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.norecordfound')" /></td></tr>
</xsl:if>
<xsl:if test="//ROW/MBEST_HLA/MBEST_HLA_ROW">
<tr>

<td>
<xsl:for-each select="//ROW/MBEST_HLA/MBEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_a')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>

<td>
<xsl:for-each select="//ROW/MBEST_HLA/MBEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_b')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>

<td>
<xsl:for-each select="//ROW/MBEST_HLA/MBEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_c')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/MBEST_HLA/MBEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb1')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/MBEST_HLA/MBEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb3')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/MBEST_HLA/MBEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb4')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/MBEST_HLA/MBEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb5')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/MBEST_HLA/MBEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_dqb1')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/MBEST_HLA/MBEST_HLA_ROW">
<xsl:if test="rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_dpb1')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>
</tr>
</xsl:if>

</table>


<br></br>

<table border="1" bordercolor="black" width="90%">
<tr>
<td align="center" colspan="2"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypingdt')" /></b></td>
<td align="center" colspan="2"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlaentrydt')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.source')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypea')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypeb')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlatypec')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb1')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb3')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb4')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladrb5')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladqb1')" /></b></td>
<td align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hladpb1')" /></b></td>
</tr>
<xsl:if test="//ROW/MHLA/MHLA_ROW">
<xsl:for-each select="//ROW/MDIST_CBU/MDIST_CBU_ROW">
<xsl:variable name="mtempseqval" select="SEQDATA" />
<xsl:variable name="mtempcreator" select="USRNAME1" />
<xsl:variable name="mtempcreatedon1" select="CREATED1" />
<xsl:variable name="mtempcreatedon2" select="CREATED2" />

<tr>
<td colspan="2" align="center">
<xsl:value-of select="$mtempcreatedon1" />
</td>


<td colspan="2" align="center">
<xsl:value-of select="$mtempcreatedon2" />
</td>

<td>
<xsl:value-of select="$mtempcreator" />
</td>


<td>
<xsl:for-each select="//ROW/MHLA/MHLA_ROW">
<xsl:if test="SEQVALHLA=$mtempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_a')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>

<td>
<xsl:for-each select="//ROW/MHLA/MHLA_ROW">
<xsl:if test="SEQVALHLA=$mtempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_b')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>

</td>

<td>
<xsl:for-each select="//ROW/MHLA/MHLA_ROW">
<xsl:if test="SEQVALHLA=$mtempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_c')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/MHLA/MHLA_ROW">
<xsl:if test="SEQVALHLA=$mtempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb1')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/MHLA/MHLA_ROW">
<xsl:if test="SEQVALHLA=$mtempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb3')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/MHLA/MHLA_ROW">
<xsl:if test="SEQVALHLA=$mtempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb4')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/MHLA/MHLA_ROW">
<xsl:if test="SEQVALHLA=$mtempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_drb5')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/MHLA/MHLA_ROW">
<xsl:if test="SEQVALHLA=$mtempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_dqb1')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>


<td>
<xsl:for-each select="//ROW/MHLA/MHLA_ROW">
<xsl:if test="SEQVALHLA=$mtempseqval and rf:getCodeListPkByTypeAndSubtype('hla_locus','hla_dpb1')=PKLOCUS">
<xsl:value-of select="TYPE1" />
<br></br>
<xsl:value-of select="TYPE2" />
</xsl:if>
</xsl:for-each>
</td>
</tr>


</xsl:for-each>
</xsl:if>

<xsl:if test="not(//ROW/MHLA/MHLA_ROW)">
<tr colspan="14"><td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.norecordfound')" /></td></tr>
</xsl:if>
</table>


<br></br> 
<p><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.heading')" /></p>

<br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedby')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.category')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedon')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.notes')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.assessment')" /></TD>
</tr>
<xsl:for-each select="//ROW/HLA_NOTES/HLA_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:if test="not(//ROW/HLA_NOTES/HLA_NOTES_ROW)">
<tr colspan="5"><td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.norecordfound')" /></td></tr>
</xsl:if>
</table>


<!--<br></br> 
<p><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.document.heading')" /></p>

<br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.document.category')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfReport.document.filename')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfReport.document.createdon')" /></TD>
</tr>
<xsl:for-each select="//ROW/HLA_ATTACHMENTS/HLA_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>
</table>-->




<br></br>
<hr width="100%"/>
<p><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlicensuer')" /></b></p>
<hr width="100%"/>
<br></br>


<table width="90%">
<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.licensurestat')" />&#160;<xsl:value-of select="ROW/LIC_STATUS" /></TD>
</tr>

<xsl:if test="ROW/LIC_FLAG='1' and ROW/NEW_TASK_FLAG!='1'">
<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.unlicenserreason')" />&#160;<xsl:value-of select="ROW/UNLICENSEREASON" /> 
</TD>
</tr>
</xsl:if>
</table>


<br></br>
<hr width="100%"/>
<p><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.heligiblilty')" /></b></p>
<hr width="100%"/>
<br></br>

<xsl:if test="ROW/NEW_TASK_FLAG=1">


<xsl:for-each select="//ROW/ELIG_QUEST/ELIG_QUEST_ROW">


<table width="90%">
<tr>
<td>
<p><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.mrq')" /></b></p>
</td>
</tr>
</table>


<br></br>


<!-- MRQ QUESTION's -->
<table border="1" bordercolor="black" width="90%">
<!-- MRQ QUESTION 1 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newmrqques1')" /></td>
<td>
<xsl:value-of select="MRQ_Q1" />
</td>
</tr>

<xsl:if test="MRQ_Q1=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newmrqques2')" /></td>
<td>
<xsl:if test="MRQ_Q1_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.responsetxt.yes')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newcommonques1')" />
</xsl:if>
<xsl:if test="MRQ_Q1_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.responsetxt.no')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newcommonques2')" />
</xsl:if>
</td>
</tr>
</xsl:if>

<xsl:if test="MRQ_Q1_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.responsetxt.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="MRQ_Q1_ADD_DETAILS" /></td>
</tr>
</xsl:if>


<!-- MRQ QUESTION 2 -->

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newmrqques3')" /></td>
<td><xsl:value-of select="MRQ_Q2" /></td>
</tr>

<xsl:if test="MRQ_Q2=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="MRQ_Q2_ADD_DETAILS" /></td>
</tr>
</xsl:if>


<!-- MRQ QUESTION 3 -->

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newmrqques4')" /></td>
<td><xsl:value-of select="MRQ_Q3" /></td>
</tr>

<xsl:if test="MRQ_Q3=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.yes')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="MRQ_Q3_ADD_DETAILS" /></td>
</tr>
</xsl:if>
</table>





<br></br>


<table width="90%">
<tr>
<td>
<p><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind')" /></b></p>
</td>
</tr>
</table>



<br></br>

<!-- PHYSICAL QUESTION's -->
<table border="1" bordercolor="black" width="90%">
<!-- PHYSICAL QUESTION 1 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfindcommonques2')" /></td>
<td>
<xsl:value-of select="PHY_Q1" />
</td>
</tr>

<xsl:if test="PHY_Q1=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newmrqques2')" /></td>
<td>
<xsl:if test="PHY_Q1_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.responsetxt.yes')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newcommonques1')" />
</xsl:if>
<xsl:if test="PHY_Q1_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.responsetxt.no')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newcommonques2')" />
</xsl:if>
</td>
</tr>
</xsl:if>

<xsl:if test="PHY_Q1_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.responsetxt.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="PHY_Q1_ADD_DETAILS" /></td>
</tr>
</xsl:if>


<!-- PHYSICAL QUESTION 2 -->

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newphysfind2')" /></td>
<td><xsl:value-of select="PHY_Q2" /></td>
</tr>

<xsl:if test="PHY_Q2=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.yes')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="PHY_Q2_ADD_DETAILS" /></td>
</tr>
</xsl:if>


<!-- PHYSICAL QUESTION 3 -->

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newphysfind3')" /></td>
<td><xsl:value-of select="PHY_Q3" /></td>
</tr>

<xsl:if test="PHY_Q3=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.yes')">

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newphysfind4')" /></td>
<td><xsl:value-of select="PHY_Q3_A" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newphysfind5')" /></td>
<td><xsl:value-of select="PHY_Q3_B" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newphysfind6')" /></td>
<td><xsl:value-of select="PHY_Q3_C" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newphysfind7')" /></td>
<td><xsl:value-of select="PHY_Q3_D" /></td>
</tr>


<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newphysfind8')" /></td>
<td><xsl:value-of select="PHY_Q3_E" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newphysfind9')" /></td>
<td><xsl:value-of select="PHY_Q3_F" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newphysfind10')" /></td>
<td><xsl:value-of select="PHY_Q3_G" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newphysfind11')" /></td>
<td><xsl:value-of select="PHY_Q3_H" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newphysfind12')" /></td>
<td><xsl:value-of select="PHY_Q3_I" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="PHY_Q3_ADD_DETAILS" /></td>
</tr>

</xsl:if>
</table>

<br></br>



<table width="90%">
<tr>
<td>
<p><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm')" /></b></p>
</td>
</tr>
</table>

<br></br>



<!-- IDM QUESTION's -->
<table border="1" bordercolor="black" width="90%">
<!-- IDM QUESTION 1 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newidm1')" /></td>
<td>
<xsl:value-of select="IDM_Q1" />
</td>
</tr>

<xsl:if test="IDM_Q1=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newidm2')" /></td>
<td>
<xsl:if test="IDM_Q1_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.responsetxt.yes')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newcommonques3')" />
</xsl:if>
<xsl:if test="IDM_Q1_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.responsetxt.no')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newcommonques4')" />
</xsl:if>
</td>
</tr>
</xsl:if>

<xsl:if test="IDM_Q1_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.responsetxt.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="IDM_Q1_ADD_DETAILS" /></td>
</tr>
</xsl:if>


<!-- IDM QUESTION 2 -->

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newidm3')" /></td>
<td>
<xsl:value-of select="IDM_Q2" />
</td>
</tr>

<xsl:if test="IDM_Q2=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newidm2')" /></td>
<td>
<xsl:if test="IDM_Q2_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.responsetxt.yes')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newcommonques3')" />
</xsl:if>
<xsl:if test="IDM_Q2_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.responsetxt.no')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newcommonques4')" />
</xsl:if>
</td>
</tr>
</xsl:if>

<xsl:if test="IDM_Q2_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.responsetxt.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="IDM_Q2_ADD_DETAILS" /></td>
</tr>
</xsl:if>


<!-- IDM QUESTION 3 -->

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newidm5')" /></td>
<td><xsl:value-of select="IDM_Q3" /></td>
</tr>

<xsl:if test="IDM_Q3=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.yes')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="IDM_Q3_ADD_DETAILS" /></td>
</tr>
</xsl:if>
</table>


</xsl:for-each>


<xsl:if test="not(//ROW/ELIG_QUEST/ELIG_QUEST_ROW)">


<p><b><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.mrq')" /></b></p>

<br></br>

<!-- MRQ QUESTION's -->
<table border="1" bordercolor="black" width="90%">

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newmrqques1')" /></td>
<td>
</td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newmrqques3')" /></td>
<td></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newmrqques4')" /></td>
<td></td>
</tr>

</table>




<br></br>

<table width="90%">
<tr>
<td>
<p><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind')" /></b></p>
</td>
</tr>
</table>

<br></br>

<!-- PHYSICAL QUESTION's -->
<table border="1" bordercolor="black" width="90%">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfindcommonques1')" /></td>
<td>
</td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newphysfind2')" /></td>
<td></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newphysfind3')" /></td>
<td></td>
</tr>
</table>


<br></br>


<p><b><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm')" /></b></p>

<br></br>


<!-- IDM QUESTION's -->
<table border="1" bordercolor="black" width="90%">

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newidm1')" /></td>
<td>
</td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newidm3')" /></td>
<td>
</td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.newidm5')" /></td>
<td></td>
</tr>

</table>



</xsl:if>

<br></br>
<br></br>

</xsl:if>




<xsl:if test="ROW/NEW_TASK_FLAG=0">


<xsl:for-each select="//ROW/ELIG_QUEST/ELIG_QUEST_ROW">


<p><b><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.mrq')" /></b></p>

<br></br>


<!-- MRQ QUESTION's -->
<table border="1" bordercolor="black" width="90%">

<!-- MRQ QUESTION 1 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.mrqques1')" /></td>
<td>
<xsl:value-of select="MRQ_Q1" />
</td>
</tr>

<xsl:if test="MRQ_Q1=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="MRQ_Q1_ADD_DETAILS" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.mrqques2')" /></td>
<td><xsl:value-of select="MRQ_Q1_A" /></td>
</tr>



<xsl:if test="MRQ_Q1_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.mrqques3')" /></td>
<td><xsl:value-of select="MRQ_Q1_A_1" /></td>
</tr>
</xsl:if>

</xsl:if>


<!-- MRQ QUESTION 2 -->

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.mrqques4')" /></td>
<td><xsl:value-of select="MRQ_Q2" /></td>
</tr>

<xsl:if test="MRQ_Q2=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.yes')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="MRQ_Q2_ADD_DETAILS" /></td>
</tr>
</xsl:if>
</table>


<br></br>


<!-- PHYSICAL QUESTION's -->

<table width="90%">
<tr>
<td>
<p><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfindold')" /></b></p>
</td>
</tr>
</table>


<br></br>

<table border="1" bordercolor="black" width="90%">
<!-- PHYSICAL QUESTION 1 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind0')" /></td>
<td>
<xsl:value-of select="PHY_Q1" />
</td>
</tr>

<xsl:if test="PHY_Q1=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="PHY_Q1_ADD_DETAILS" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind1')" /></td>
<td><xsl:value-of select="PHY_Q1_A" /></td>
</tr>

<xsl:if test="PHY_Q1_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind2')" /></td>
<td><xsl:value-of select="PHY_Q1_A_1" /></td>
</tr>
</xsl:if>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind3')" /></td>
<td><xsl:value-of select="PHY_Q1_B" /></td>
</tr>

<xsl:if test="PHY_Q1_B=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind4')" /></td>
<td><xsl:value-of select="PHY_Q1_B_1" /></td>
</tr>
</xsl:if>

</xsl:if>


<!-- PHYSICAL QUESTION 2 -->

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind5')" /></td>
<td><xsl:value-of select="PHY_Q2" /></td>
</tr>

<xsl:if test="PHY_Q2=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.yes')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="PHY_Q2_ADD_DETAILS" /></td>
</tr>
</xsl:if>


<!-- PHYSICAL QUESTION 3 -->

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind6')" /></td>
<td><xsl:value-of select="PHY_Q3" /></td>
</tr>

<xsl:if test="PHY_Q3=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.yes')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="PHY_Q3_ADD_DETAILS" /></td>
</tr>


<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind7')" /></td>
<td><xsl:value-of select="PHY_Q3_A" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind8')" /></td>
<td><xsl:value-of select="PHY_Q3_B" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind9')" /></td>
<td><xsl:value-of select="PHY_Q3_C" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind10')" /></td>
<td><xsl:value-of select="PHY_Q3_D" /></td>
</tr>


<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind11')" /></td>
<td><xsl:value-of select="PHY_Q3_E" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind12')" /></td>
<td><xsl:value-of select="PHY_Q3_F" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind13')" /></td>
<td><xsl:value-of select="PHY_Q3_G" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind14')" /></td>
<td><xsl:value-of select="PHY_Q3_H" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind15')" /></td>
<td><xsl:value-of select="PHY_Q3_I" /></td>
</tr>

</xsl:if>
</table>


<br></br>



<p><b><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm')" /></b></p>


<br></br>

<!-- IDM QUESTION's -->
<table border="1" bordercolor="black" width="90%">
<!-- IDM QUESTION 1 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm1')" /></td>
<td>
<xsl:value-of select="IDM_Q1" />
</td>
</tr>

<xsl:if test="IDM_Q1=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="IDM_Q1_ADD_DETAILS" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm2')" /></td>
<td><xsl:value-of select="IDM_Q1_A" /></td>
</tr>

<xsl:if test="IDM_Q1_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm3')" /></td>
<td><xsl:value-of select="IDM_Q1_A_1" /></td>
</tr>
</xsl:if>

</xsl:if>




<!-- IDM QUESTION 2 -->

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm4')" /></td>
<td>
<xsl:value-of select="IDM_Q2" />
</td>
</tr>

<xsl:if test="IDM_Q2=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="IDM_Q2_ADD_DETAILS" /></td>
</tr>

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm5')" /></td>
<td><xsl:value-of select="IDM_Q2_A" /></td>
</tr>


<xsl:if test="IDM_Q2_A=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.no')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm3')" /></td>
<td><xsl:value-of select="IDM_Q2_A_1" /></td>
</tr>
</xsl:if>

</xsl:if>




<!-- IDM QUESTION 3 -->

<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm6')" /></td>
<td><xsl:value-of select="IDM_Q3" /></td>
</tr>

<xsl:if test="IDM_Q3=rf:getPropertiesData($props, 'garuda.report.questionnaire.response.yes')">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.additionaldetail')" /></td>
<td><xsl:value-of select="IDM_Q3_ADD_DETAILS" /></td>
</tr>
</xsl:if>
</table>


</xsl:for-each>
<br></br>





<xsl:if test="not(//ROW/ELIG_QUEST/ELIG_QUEST_ROW)">


<p><b><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.mrq')" /></b></p>


<br></br>

<!-- MRQ QUESTION's -->
<table border="1" bordercolor="black" width="90%">
<!-- MRQ QUESTION 1 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.mrqques1')" /></td>
<td></td>
</tr>
<!-- MRQ QUESTION 2 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.mrqques4')" /></td>
<td></td>
</tr>
</table>

<br></br>


<!-- PHYSICAL QUESTION's -->

<table width="90%">
<tr>
<td>
<p><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfindold')" /></b></p>
</td>
</tr>
</table>

<br></br>



<table border="1" bordercolor="black" width="90%">
<!-- PHYSICAL QUESTION 1 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind0')" /></td>
<td></td>
</tr>
<!-- PHYSICAL QUESTION 2 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind5')" /></td>
<td></td>
</tr>
<!-- PHYSICAL QUESTION 3 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.physfind6')" /></td>
<td></td>
</tr>
</table>




<br></br>

<p><b><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm')" /></b></p>

<br></br>

<!-- IDM QUESTION's -->
<table border="1" bordercolor="black" width="90%">
<!-- IDM QUESTION 1 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm1')" /></td>
<td></td>
</tr>
<!-- IDM QUESTION 2 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm4')" /></td>
<td></td>
</tr>
<!-- IDM QUESTION 3 -->
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.questionnaire.level.idm6')" /></td>
<td></td>
</tr>
</table>


</xsl:if>


</xsl:if>



<table>
<tr>
<TD><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.eligibiltyDetermination')" /></b>&#160;
<xsl:if test="ROW/ELI_FLAG='3' and ROW/NEW_TASK_FLAG!=1">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.eligibility.notapplic')" /> 
</xsl:if>
<xsl:if test="ROW/ELI_FLAG!='3' or ROW/NEW_TASK_FLAG=1">
<xsl:value-of select="ROW/ELIGIBLE_STATUS" /> 
</xsl:if> 
</TD>
</tr>

<xsl:if test="ROW/ELI_FLAG='1'">
<tr>
<TD><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.inEligibleReason')" />&#160;</b><xsl:value-of select="ROW/INELIGIBLEREASON" /> 
</TD>
</tr>
</xsl:if>
<xsl:if test="ROW/ELI_FLAG='2'">
<tr>
<TD>
<b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.inEligibleReason')" />&#160;</b><xsl:value-of select="ROW/INELIGIBLEREASON" /> 
</TD>
</tr>
</xsl:if>

<xsl:if test="ROW/ELI_FLAG!='0'">
<tr>
<TD><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.eligibilityrep.comments')" />&#160;</b><xsl:value-of select="ROW/ELIGCOMMENTS" />
</TD>
</tr>
</xsl:if>
</table>

<br></br>
<p><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.heading')" /></p>

<br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedby')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.category')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedon')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.notes')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.assessment')" /></TD>
</tr>
<xsl:for-each select="//ROW/ELIGIBILITY_NOTES/ELIGIBILITY_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:if test="not(//ROW/ELIGIBILITY_NOTES/ELIGIBILITY_NOTES_ROW)">
<tr colspan="7"><td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.norecordfound')" /></td></tr>
</xsl:if>
</table>


<!--
<br></br>
<font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.document.heading')" /></font> 
<br></br><br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.document.category')" /></TD> 
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfReport.document.filename')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfReport.document.createdon')" /></TD> 
</tr>
<xsl:for-each select="//ELIGIBLE_ATTACHMENTS/ELIGIBLE_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>
</table>

<br></br>
<table>
<tr>
<td  align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.nameofperson')" />
<xsl:choose>
<xsl:when test="CREATEDBY=''">
	<xsl:value-of select="LASTMODIFIEDBY" />
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="CREATEDBY" /> 
</xsl:otherwise>
</xsl:choose>
</td>

</tr>
<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.esignprovided')" />
<xsl:choose>
<xsl:when test="LASTMODIFIEDBY=''">
	<xsl:value-of select="CREATEDBY" /> 
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="LASTMODIFIEDBY" /> 
</xsl:otherwise>

</xsl:choose>
</td>
</tr>
<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.completionofform')" />
<xsl:choose>
<xsl:when test="LAST_MODIFIED_DT=''">
	 <xsl:value-of select="CREATEDON" />
</xsl:when>
<xsl:otherwise>
	 <xsl:value-of select="LAST_MODIFIED_DT" />
</xsl:otherwise>

</xsl:choose>
</td>

</tr>
</table>
-->

<br></br>
<hr width="100%"/>
<p><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hlabsummary')" /></b></p>
<hr width="100%"/>
<br></br>

<p><xsl:value-of select="$vartemp"/><u><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cordentry.label.labsummaryProcess')" /></u></p>

<br></br>

<table width="90%">
<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.processingstrdt')" />&#160;<xsl:value-of select="ROW/PRCSNG_START_DATE" /></TD>
<TD>
<xsl:if test="ROW/BACT_CULTURE_PK!='' and ROW/BACT_CULTURE_PK!=rf:getCodeListPkByTypeAndSubtype('bact_cul','not_done')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.bactculstrdt')" />&#160;<xsl:value-of select="ROW/BACT_CUL_STRT_DT" />
</xsl:if>
</TD>
</tr>
<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.abobloodtype')" />&#160;<xsl:value-of select="ROW/ABO_BLOOD_TYPE" /></TD>  
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.fungalculture')" />&#160;<xsl:value-of select="ROW/FUNGAL_CULTURE" /></TD>  
</tr>
<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.rhtype')" />&#160;<xsl:value-of select="ROW/RH_TYPE" /></TD>
<TD>
<xsl:if test="ROW/FUNGAL_CULTURE_PK!='' and ROW/FUNGAL_CULTURE_PK!=rf:getCodeListPkByTypeAndSubtype('fung_cul','not_done')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.fungalcultstrdt')" />&#160;<xsl:value-of select="ROW/FUNG_CUL_STRT_DT" />
</xsl:if> 
</TD>
</tr>
<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.bacterialcult')" />&#160;<xsl:value-of select="ROW/BACT_CULTURE" /></TD>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hemoglobinopathytest')" />&#160;<xsl:value-of select="ROW/HEMOGLOBIN_SCRN" /></TD>
</tr>
</table>

<br></br> <br></br>


<table>

</table>

<br></br> <br></br>

<xsl:variable name="sysflag" select="ROW/SYSFLAG"/>

<xsl:variable name="NOT_NEEDED_TEST1" select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')"/>
<xsl:variable name="NOT_NEEDED_TEST2" select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')"/>
<xsl:variable name="NOT_NEED_TEST1" select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcncc')"></xsl:variable>

<xsl:variable name="TOT_CBU_NUC_CELL_COUNT" select="rf:getPropertiesData($props, 'garuda.labsummaryrep.ftestname.tcncc')"></xsl:variable>


<xsl:variable name="NOT_NEEDED_TEST">
<xsl:choose>
<xsl:when test="$sysflag='1'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')" />
</xsl:when>
</xsl:choose>
</xsl:variable> 


<table border="1" bordercolor="black" width="90%">
<tr>
<td><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.htest')" /></font></b></td>
<td align ="center"><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpreprocessingtestingstrdt')" /></font></b>&#160;&#160;<xsl:value-of select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/PRE_TEST_DATE" /></td>
</tr>
</table>
<xsl:for-each select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW">
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST1 and LABTEST_SHORTNAME!=$NOT_NEEDED_TEST2">		
			
<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:value-of select="LABTEST_NAME" /> 
</td>
<td class="reportData" align="center" nowrap="nowrap">

<xsl:if test="PRE_TEST_RESULT!=''">
<!--Result-->
<xsl:value-of select="PRE_TEST_RESULT" />

<!--choose append value-->
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbuvol')">&#160;
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbuvol.unit')"  disable-output-escaping="yes"/>
</xsl:if> 
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcncc')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcncc.unit')"  disable-output-escaping="yes"/>
</xsl:if>												   
														           
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbunccc')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbunccc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<!--end append value-->

</xsl:if>

</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>



<xsl:variable name="tempvars" ></xsl:variable>

<!-- Pre-Processing Additional Tests -->

<xsl:if test="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">


<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW[generate-id(.) = generate-id(key('k1', TEST_FIND1)[1])]">
<xsl:if test="TEST_FIND1=rf:getCodeListPkByTypeAndSubtype('timing_of_test','pre_procesing')" >
<table width="90%">
<tr>
<td align="left" >
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.name')" /> 
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>


<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">

<xsl:if test="TEST_FIND1=rf:getCodeListPkByTypeAndSubtype('timing_of_test','pre_procesing')">

<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:value-of select="LABTEST_NAME" /> 
</td>
<td>
<table>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>
<td align="center"><xsl:value-of select="TEST_RESULT" /></td>
</tr>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.type')" /></td>
<td align="center"><xsl:value-of select="SAMPLE_TYPE" /></td>
</tr>
</table>
</td>
</tr>
</table>
</xsl:if>

</xsl:for-each>
</xsl:if>

<!-- Additional Tests ends -->



<br></br> <br></br>
<table border="1" bordercolor="black" width="90%"> 
<tr>
<td><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.htest')" /></font></b></td>
<td align ="center"><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props,'garuda.labsummaryrep.hpostprocessing.precryopreservation.strdt')" /></font></b>&#160;<br></br><xsl:value-of select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/POST_TEST_DATE" />															
</td>
</tr>
</table>


<xsl:variable name="NOT_NEEDED_TEST_POST">
<xsl:choose>
<xsl:when test="$sysflag='1'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0' and //ROW/LAB_SUMMARY/LAB_SUMMARY_ROW[4]/POST_TEST_RESULT!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0' and //ROW/LAB_SUMMARY/LAB_SUMMARY_ROW[3]/POST_TEST_RESULT!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')" />
</xsl:when>
</xsl:choose>
</xsl:variable>

<xsl:for-each select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW">
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST_POST and LABTEST_SHORTNAME!=$NOT_NEED_TEST1">

<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST1 and LABTEST_SHORTNAME!=$NOT_NEEDED_TEST2">
<xsl:value-of select="LABTEST_NAME" /> 
</xsl:if>
<xsl:if test="LABTEST_SHORTNAME=$NOT_NEEDED_TEST1 or LABTEST_SHORTNAME=$NOT_NEEDED_TEST2">
<xsl:value-of select="$TOT_CBU_NUC_CELL_COUNT" /> 
</xsl:if>
</td>
<td class="reportData" align="center" nowrap="nowrap" >
<xsl:if test="LABTEST_SHORTNAME!=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt') and LABTEST_SHORTNAME!=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab')">
<xsl:if test="POST_TEST_RESULT!=''">
<xsl:value-of select="POST_TEST_RESULT" />


<!--choose append value-->

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbuvol')">&#160;
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbuvol.unit')" />
</xsl:if> 
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcncc')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcncc.unit')"  disable-output-escaping="yes"/>
</xsl:if>


<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.unknownifuncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcbuun.unit')"  disable-output-escaping="yes"/>
</xsl:if> 

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.uncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.uncrct.unit')"  disable-output-escaping="yes"/>
</xsl:if> 

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcdad')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcdad.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.totcd')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.totcd.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cfccnt.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pernrbc')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pernrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.percd.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cnrbc')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cnrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertnuc')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertnuc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd3')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.percd3.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertmon')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertmon.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbunccc')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbunccc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.fnpv')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.fnpv.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<!--end append value-->

</xsl:if>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt')">
<table>
	<tr> 
	<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>
	<td><xsl:value-of select="POST_TEST_RESULT" />
	<xsl:if test="POST_TEST_RESULT"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cfccnt.unit')"  disable-output-escaping="yes"/></xsl:if>
	</td>
	</tr>
	<tr> 
	<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.testmethod')" /></td>
	<td><xsl:value-of select="POST_TESTMETH" /></td>
	</tr>
	<xsl:if test="POST_TESTMETH=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
	<tr> 
	<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /></td>
	<td> <xsl:value-of select="POST_TEST_DESC" />  </td>
	</tr>
	</xsl:if>

</table>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab')">
<table>
	<tr> 
	<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>
	<td><xsl:value-of select="POST_TEST_RESULT" />
	<xsl:if test="POST_TEST_RESULT"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.viab.unit')"  disable-output-escaping="yes"/></xsl:if>
	</td>
	</tr>
	<tr> 
	<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.testmethod')" /></td>
	<td> <xsl:value-of select="POST_TESTMETH" />  </td>
	</tr>
	<xsl:if test="POST_TESTMETH=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
	<tr> 
	<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /></td>
	<td> <xsl:value-of select="POST_TEST_DESC" />  </td>
	</tr>
	</xsl:if>
</table>
</xsl:if>


</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>


<!-- Pre-Post Processing Additional Tests -->

<xsl:if test="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">

<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW[generate-id(.) = generate-id(key('k1', TEST_FIND1)[1])]">
<xsl:if test="TEST_FIND1=rf:getCodeListPkByTypeAndSubtype('timing_of_test','post_procesing')" >
<table width="90%">
<tr>
<td align="left" >
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.name')" /> 
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>


<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">
<xsl:if test="TEST_FIND1=rf:getCodeListPkByTypeAndSubtype('timing_of_test','post_procesing')">
<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:value-of select="LABTEST_NAME" /> 
</td>
<td>
<table>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>
<td align="center"><xsl:value-of select="TEST_RESULT" /></td>
</tr>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.type')" /></td>
<td align="center"><xsl:value-of select="SAMPLE_TYPE" /></td>
</tr>
</table>
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>
</xsl:if>

<!-- Additional Tests ends -->




<xsl:if test="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/POST_CRYOPRES_DATE != ''">
<br></br> <br></br>

<table border="1" bordercolor="black" width="90%">
<tr>
<td><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.htest')" /></font></b></td>
<td align ="center"><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw')" /></font></b>&#160;&#160;<xsl:value-of select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/POST_CRYOPRES_DATE" /></td>
</tr>
</table>



<xsl:variable name="NOT_NEEDED_TEST_POST_CRY_THAW">
<xsl:choose>
<xsl:when test="$sysflag='1'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0'  and //ROW/LAB_SUMMARY/LAB_SUMMARY_ROW[4]/POST_CRYOPRES_THAW!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0'  and //ROW/LAB_SUMMARY/LAB_SUMMARY_ROW[3]/POST_CRYOPRES_THAW!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')" />
</xsl:when>
</xsl:choose>
</xsl:variable>


<xsl:for-each select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW">
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST_POST_CRY_THAW  and LABTEST_SHORTNAME!=$NOT_NEED_TEST1">	
<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >

<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST1 and LABTEST_SHORTNAME!=$NOT_NEEDED_TEST2">
<xsl:value-of select="LABTEST_NAME" /> 
</xsl:if>
<xsl:if test="LABTEST_SHORTNAME=$NOT_NEEDED_TEST1 or LABTEST_SHORTNAME=$NOT_NEEDED_TEST2">
<xsl:value-of select="$TOT_CBU_NUC_CELL_COUNT" /> 
</xsl:if>

</td>
<td class="reportData" align="center" >
<xsl:if test="POST_CRYOPRES_THAW">
<table>
<tr>
<td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>   <td class="reportData" align="left" > <xsl:value-of select="POST_CRYOPRES_THAW" />

<!--choose append value-->

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbuvol') and POST_CRYOPRES_THAW">&#160;
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbuvol.unit')" />
</xsl:if> 
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcncc') and POST_CRYOPRES_THAW">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcncc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.unknownifuncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcbuun.unit')"  disable-output-escaping="yes"/>
</xsl:if> 

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.uncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.uncrct.unit')"  disable-output-escaping="yes"/>
</xsl:if> 

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcdad') and POST_CRYOPRES_THAW">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcdad.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.totcd') and POST_CRYOPRES_THAW">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.totcd.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cnrbc')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cnrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt') and POST_CRYOPRES_THAW">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cfccnt.unit')"  disable-output-escaping="yes"/>
</xsl:if>

															   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pernrbc') and POST_CRYOPRES_THAW">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pernrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd') and POST_CRYOPRES_THAW">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.percd.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertnuc') and POST_CRYOPRES_THAW">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertnuc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd3') and POST_CRYOPRES_THAW">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.percd3.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertmon') and POST_CRYOPRES_THAW">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertmon.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbunccc') and POST_CRYOPRES_THAW">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbunccc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.fnpv') and POST_CRYOPRES_THAW">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.fnpv.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.viab.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<!--end append value-->

</td>
</tr>

<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.reasonfortest')" /></td>   <td class="reportData" align="left" > <xsl:value-of select="REASON_THAW" /></td></tr>
<xsl:if test="REASON_THAW=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="REASON_DESC_THAW" /></td></tr>
</xsl:if>
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.sampletype')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="SMPLTYPE_THAW" /></td></tr>

<!---Version changes -->
<xsl:if test="$V_NEW_TASK_FLAG='0' or LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab') or LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.testmethod')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="TESTMETH_THAW" /></td></tr>
<xsl:if test="TESTMETH_THAW=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /> </td>   <td class="reportData" align="left"><xsl:value-of select="TEST_DESC_THAW" /> </td></tr>
</xsl:if>
</xsl:if>
<!---Version changes ends -->

</table>
</xsl:if>
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>


<!-- Pre-Post Processing Thaw Additional Tests -->

<xsl:if test="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">


<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW[generate-id(.) = generate-id(key('k2', TEST_FIND2)[1])]">
<xsl:if test="TEST_FIND2=rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.prepostthaw')" >
<table width="90%">
<tr>
<td align="left" >
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.name')" /> 
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>


<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">
<xsl:if test="TEST_FIND2=rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.prepostthaw')">
<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:value-of select="LABTEST_NAME" /> 
</td>
<td>
<table>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>
<td align="center"><xsl:value-of select="TEST_RESULT" /></td>
</tr>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.type')" /></td>
<td align="center"><xsl:value-of select="SAMPLE_TYPE" /></td>
</tr>
</table>
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>
</xsl:if>

<!-- Additional Tests ends -->
</xsl:if>



<xsl:if test="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/POST_CRYOPRES_DATE1 != ''">
<br></br> <br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<td><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.htest')" /></font></b></td>
<td align ="center"><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw')" /></font></b>&#160;&#160;<xsl:value-of select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/POST_CRYOPRES_DATE1" /></td>
</tr>
</table>


<xsl:variable name="NOT_NEEDED_TEST_POST_CRY_THAW1">
<xsl:choose>
<xsl:when test="$sysflag='1'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0'  and //ROW/LAB_SUMMARY/LAB_SUMMARY_ROW[4]/POST_CRYOPRES_THAW1!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0'  and //ROW/LAB_SUMMARY/LAB_SUMMARY_ROW[3]/POST_CRYOPRES_THAW1!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')" />
</xsl:when>
</xsl:choose>
</xsl:variable>



<xsl:for-each select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW">
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST_POST_CRY_THAW1  and LABTEST_SHORTNAME!=$NOT_NEED_TEST1">
<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST1 and LABTEST_SHORTNAME!=$NOT_NEEDED_TEST2">
<xsl:value-of select="LABTEST_NAME" /> 
</xsl:if>
<xsl:if test="LABTEST_SHORTNAME=$NOT_NEEDED_TEST1 or LABTEST_SHORTNAME=$NOT_NEEDED_TEST2">
<xsl:value-of select="$TOT_CBU_NUC_CELL_COUNT" /> 
</xsl:if>
</td>
<td class="reportData" align="center" >
<xsl:if test="POST_CRYOPRES_THAW1">
<table>
<tr>
<td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>   <td class="reportData" align="left" > <xsl:value-of select="POST_CRYOPRES_THAW1" />

<!--choose append value-->

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbuvol') and POST_CRYOPRES_THAW1">&#160;
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbuvol.unit')" />
</xsl:if> 
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcncc') and POST_CRYOPRES_THAW1">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcncc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.unknownifuncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcbuun.unit')"  disable-output-escaping="yes"/>
</xsl:if> 

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.uncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.uncrct.unit')"  disable-output-escaping="yes"/>
</xsl:if> 


<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcdad') and POST_CRYOPRES_THAW1">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcdad.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.totcd') and POST_CRYOPRES_THAW1">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.totcd.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt') and POST_CRYOPRES_THAW1">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cfccnt.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cnrbc')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cnrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pernrbc') and POST_CRYOPRES_THAW1">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pernrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd') and POST_CRYOPRES_THAW1">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.percd.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertnuc') and POST_CRYOPRES_THAW1">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertnuc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd3') and POST_CRYOPRES_THAW1">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.percd3.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertmon') and POST_CRYOPRES_THAW1">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertmon.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbunccc') and POST_CRYOPRES_THAW1">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbunccc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.fnpv') and POST_CRYOPRES_THAW1">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.fnpv.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.viab.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<!--end append value-->
</td>
</tr>

<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.reasonfortest')" /></td>   <td class="reportData" align="left"> <xsl:value-of select="REASON_THAW1" /></td></tr>
<xsl:if test="REASON_THAW1=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="REASON_DESC_THAW1" /></td></tr>
</xsl:if>
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.sampletype')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="SMPLTYPE_THAW1" /></td></tr>

<!---Version changes -->
<xsl:if test="$V_NEW_TASK_FLAG='0' or LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab') or LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.testmethod')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="TESTMETH_THAW1" /></td></tr>
<xsl:if test="TESTMETH_THAW1=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /> </td>   <td class="reportData" align="left"><xsl:value-of select="TEST_DESC_THAW1" /> </td></tr>
</xsl:if>
</xsl:if>
<!---Version changes ends -->

</table>
</xsl:if>
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>

<!-- Pre-Post Processing Thaw1 Additional Tests -->

<xsl:if test="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">

<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW[generate-id(.) = generate-id(key('k2', TEST_FIND2)[1])]">
<xsl:if test="TEST_FIND2='1'" >
<table width="90%">
<tr>
<td align="left" >
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.name')" /> 
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>


<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">
<xsl:if test="TEST_FIND2='1'">
<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:value-of select="LABTEST_NAME" /> 
</td>
<td>
<table>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>
<td align="center"><xsl:value-of select="TEST_RESULT" /></td>
</tr>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.type')" /></td>
<td align="center"><xsl:value-of select="SAMPLE_TYPE" /></td>
</tr>
</table>
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>


</xsl:if>

<!-- Additional Tests ends -->
</xsl:if>





<xsl:if test="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/POST_CRYOPRES_DATE2 != ''">
<br></br> <br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<td><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.htest')" /></font></b></td>
<td align ="center"><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw')" /></font></b>&#160;&#160;<xsl:value-of select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/POST_CRYOPRES_DATE2" /></td>
</tr>
</table>

<xsl:variable name="NOT_NEEDED_TEST_POST_CRY_THAW2">
<xsl:choose>
<xsl:when test="$sysflag='1'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0'  and //ROW/LAB_SUMMARY/LAB_SUMMARY_ROW[4]/POST_CRYOPRES_THAW2!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0'  and //ROW/LAB_SUMMARY/LAB_SUMMARY_ROW[3]/POST_CRYOPRES_THAW2!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')" />
</xsl:when>
</xsl:choose>
</xsl:variable>

<xsl:for-each select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW">
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST_POST_CRY_THAW2  and LABTEST_SHORTNAME!=$NOT_NEED_TEST1">	
<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST1 and LABTEST_SHORTNAME!=$NOT_NEEDED_TEST2">
<xsl:value-of select="LABTEST_NAME" /> 
</xsl:if>
<xsl:if test="LABTEST_SHORTNAME=$NOT_NEEDED_TEST1 or LABTEST_SHORTNAME=$NOT_NEEDED_TEST2">
<xsl:value-of select="$TOT_CBU_NUC_CELL_COUNT" /> 
</xsl:if> 
</td>
<td class="reportData" align="center" >
<xsl:if test="POST_CRYOPRES_THAW2">
<table>
<tr>
<td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>   <td class="reportData" align="left" > <xsl:value-of select="POST_CRYOPRES_THAW2" />
<!--choose append value-->

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbuvol') and POST_CRYOPRES_THAW2">&#160;
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbuvol.unit')" />
</xsl:if> 
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcncc') and POST_CRYOPRES_THAW2">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcncc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.unknownifuncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcbuun.unit')"  disable-output-escaping="yes"/>
</xsl:if> 

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.uncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.uncrct.unit')"  disable-output-escaping="yes"/>
</xsl:if>  

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcdad') and POST_CRYOPRES_THAW2">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcdad.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.totcd') and POST_CRYOPRES_THAW2">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.totcd.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt') and POST_CRYOPRES_THAW2">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cfccnt.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cnrbc')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cnrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pernrbc') and POST_CRYOPRES_THAW2">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pernrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd') and POST_CRYOPRES_THAW2">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.percd.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertnuc') and POST_CRYOPRES_THAW2">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertnuc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd3') and POST_CRYOPRES_THAW2">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.percd3.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertmon') and POST_CRYOPRES_THAW2">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertmon.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbunccc') and POST_CRYOPRES_THAW2">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbunccc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.fnpv') and POST_CRYOPRES_THAW2">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.fnpv.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.viab.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<!--end append value-->
</td>
</tr>

<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.reasonfortest')" /></td>   <td class="reportData" align="left"> <xsl:value-of select="REASON_THAW2" /></td></tr>
<xsl:if test="REASON_THAW2=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="REASON_DESC_THAW2" /></td></tr>
</xsl:if>
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.sampletype')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="SMPLTYPE_THAW2" /></td></tr>

<!---Version changes -->
<xsl:if test="$V_NEW_TASK_FLAG='0' or LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab') or LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.testmethod')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="TESTMETH_THAW2" /></td></tr>
<xsl:if test="TESTMETH_THAW2=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /> </td>   <td class="reportData" align="left"><xsl:value-of select="TEST_DESC_THAW2" /> </td></tr>
</xsl:if>
</xsl:if>
<!---Version changes ends -->

</table>
</xsl:if>
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>
<!-- Pre-Post Processing Thaw2 Additional Tests -->

<xsl:if test="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">

<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW[generate-id(.) = generate-id(key('k2', TEST_FIND2)[1])]">
<xsl:if test="TEST_FIND2='2'" >
<table width="90%">
<tr>
<td align="left" >
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.name')" /> 
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>


<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">
<xsl:if test="TEST_FIND2='2'">
<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:value-of select="LABTEST_NAME" /> 
</td>
<td>
<table>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>
<td align="center"><xsl:value-of select="TEST_RESULT" /></td>
</tr>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.type')" /></td>
<td align="center"><xsl:value-of select="SAMPLE_TYPE" /></td>
</tr>
</table>
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>

</xsl:if>

<!-- Additional Tests ends -->
</xsl:if>

<xsl:if test="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/POST_CRYOPRES_DATE3 != ''">
<br></br> <br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<td><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.htest')" /></font></b></td>
<td align ="center"><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw')" /></font></b>&#160;&#160;<xsl:value-of select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/POST_CRYOPRES_DATE3" /></td>
</tr>
</table>


<xsl:variable name="NOT_NEEDED_TEST_POST_CRY_THAW3">
<xsl:choose>
<xsl:when test="$sysflag='1'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0'  and //ROW/LAB_SUMMARY/LAB_SUMMARY_ROW[4]/POST_CRYOPRES_THAW3!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0'  and //ROW/LAB_SUMMARY/LAB_SUMMARY_ROW[3]/POST_CRYOPRES_THAW3!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')" />
</xsl:when>
</xsl:choose>
</xsl:variable>



<xsl:for-each select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW">
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST_POST_CRY_THAW3  and LABTEST_SHORTNAME!=$NOT_NEED_TEST1">	
<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST1 and LABTEST_SHORTNAME!=$NOT_NEEDED_TEST2">
<xsl:value-of select="LABTEST_NAME" /> 
</xsl:if>
<xsl:if test="LABTEST_SHORTNAME=$NOT_NEEDED_TEST1 or LABTEST_SHORTNAME=$NOT_NEEDED_TEST2">
<xsl:value-of select="$TOT_CBU_NUC_CELL_COUNT" /> 
</xsl:if>
</td>
<td class="reportData" align="center" >
<xsl:if test="POST_CRYOPRES_THAW3">
<table>
<tr>
<td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>   <td class="reportData" align="left" > <xsl:value-of select="POST_CRYOPRES_THAW3" />
<!--choose append value-->

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbuvol') and POST_CRYOPRES_THAW3">&#160;
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbuvol.unit')" />
</xsl:if> 
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.unknownifuncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcbuun.unit')"  disable-output-escaping="yes"/>
</xsl:if> 

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.uncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.uncrct.unit')"  disable-output-escaping="yes"/>
</xsl:if> 

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcdad') and POST_CRYOPRES_THAW3">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcdad.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.totcd') and POST_CRYOPRES_THAW3">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.totcd.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt') and POST_CRYOPRES_THAW3">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cfccnt.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cnrbc')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cnrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pernrbc') and POST_CRYOPRES_THAW3">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pernrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd') and POST_CRYOPRES_THAW3">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.percd.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertnuc') and POST_CRYOPRES_THAW3">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertnuc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd3') and POST_CRYOPRES_THAW3">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.percd3.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertmon') and POST_CRYOPRES_THAW3">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertmon.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbunccc') and POST_CRYOPRES_THAW3">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbunccc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.fnpv') and POST_CRYOPRES_THAW3">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.fnpv.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.viab.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<!--end append value-->
</td>
</tr>

<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.reasonfortest')" /></td>   <td class="reportData" align="left"> <xsl:value-of select="REASON_THAW3" /></td></tr>
<xsl:if test="REASON_THAW3=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="REASON_DESC_THAW3" /></td></tr>
</xsl:if>
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.sampletype')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="SMPLTYPE_THAW3" /></td></tr>
<!---Version changes -->
<xsl:if test="$V_NEW_TASK_FLAG='0' or LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab') or LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.testmethod')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="TESTMETH_THAW3" /></td></tr>
<xsl:if test="TESTMETH_THAW3=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /> </td>   <td class="reportData" align="left"><xsl:value-of select="TEST_DESC_THAW3" /> </td></tr>
</xsl:if>
</xsl:if>
<!---Version changes ends -->

</table>
</xsl:if>
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>
<!-- Pre-Post Processing Thaw3 Additional Tests -->

<xsl:if test="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">

<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW[generate-id(.) = generate-id(key('k2', TEST_FIND2)[1])]">
<xsl:if test="TEST_FIND2='3'" >
<table width="90%">
<tr>
<td align="left" >
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.name')" /> 
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>


<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">
<xsl:if test="TEST_FIND2='3'">
<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:value-of select="LABTEST_NAME" /> 
</td>
<td>
<table>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>
<td align="center"><xsl:value-of select="TEST_RESULT" /></td>
</tr>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.type')" /></td>
<td align="center"><xsl:value-of select="SAMPLE_TYPE" /></td>
</tr>
</table>
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>

</xsl:if>

<!-- Additional Tests ends -->
</xsl:if>


<xsl:if test="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/POST_CRYOPRES_DATE4 != ''">
<br></br> <br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<td><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.htest')" /></font></b></td>
<td align ="center"><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw')" /></font></b>&#160;&#160;<xsl:value-of select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/POST_CRYOPRES_DATE4" /></td>
</tr>
</table>


<xsl:variable name="NOT_NEEDED_TEST_POST_CRY_THAW4">
<xsl:choose>
<xsl:when test="$sysflag='1'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0'  and //ROW/LAB_SUMMARY/LAB_SUMMARY_ROW[4]/POST_CRYOPRES_THAW4!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0'  and //ROW/LAB_SUMMARY/LAB_SUMMARY_ROW[3]/POST_CRYOPRES_THAW4!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')" />
</xsl:when>
</xsl:choose>
</xsl:variable>



<xsl:for-each select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW">
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST_POST_CRY_THAW4  and LABTEST_SHORTNAME!=$NOT_NEED_TEST1">	
<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST1 and LABTEST_SHORTNAME!=$NOT_NEEDED_TEST2">
<xsl:value-of select="LABTEST_NAME" /> 
</xsl:if>
<xsl:if test="LABTEST_SHORTNAME=$NOT_NEEDED_TEST1 or LABTEST_SHORTNAME=$NOT_NEEDED_TEST2">
<xsl:value-of select="$TOT_CBU_NUC_CELL_COUNT" /> 
</xsl:if>
</td>
<td class="reportData" align="center" >
<xsl:if test="POST_CRYOPRES_THAW4">
<table>
<tr>
<td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>   <td class="reportData" align="left" > <xsl:value-of select="POST_CRYOPRES_THAW4" />

<!--choose append value-->

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbuvol') and POST_CRYOPRES_THAW4">&#160;
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbuvol.unit')" />
</xsl:if> 
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcncc') and POST_CRYOPRES_THAW4">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcncc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.unknownifuncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcbuun.unit')"  disable-output-escaping="yes"/>
</xsl:if> 

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.uncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.uncrct.unit')"  disable-output-escaping="yes"/>
</xsl:if>     

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcdad') and POST_CRYOPRES_THAW4">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcdad.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.totcd') and POST_CRYOPRES_THAW4">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.totcd.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt') and POST_CRYOPRES_THAW4">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cfccnt.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cnrbc')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cnrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pernrbc') and POST_CRYOPRES_THAW4">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pernrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd') and POST_CRYOPRES_THAW4">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.percd.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertnuc') and POST_CRYOPRES_THAW4">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertnuc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd3') and POST_CRYOPRES_THAW4">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertnuc.percd3')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertmon') and POST_CRYOPRES_THAW4">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertmon.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbunccc') and POST_CRYOPRES_THAW4">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbunccc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.fnpv') and POST_CRYOPRES_THAW4">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.fnpv.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab') and POST_CRYOPRES_THAW4">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.viab.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<!--end append value-->
</td>
</tr>

<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.reasonfortest')" /></td>   <td class="reportData" align="left"> <xsl:value-of select="REASON_THAW4" /></td></tr>
<xsl:if test="REASON_THAW4=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="REASON_DESC_THAW4" /></td></tr>
</xsl:if>
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.sampletype')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="SMPLTYPE_THAW4" /></td></tr>
<!---Version changes -->
<xsl:if test="$V_NEW_TASK_FLAG='0' or LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab') or LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.testmethod')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="TESTMETH_THAW4" /></td></tr>
<xsl:if test="TESTMETH_THAW4=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /> </td>   <td class="reportData" align="left"><xsl:value-of select="TEST_DESC_THAW4" /> </td></tr>
</xsl:if>
</xsl:if>
<!---Version changes ends -->

</table>
</xsl:if>
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>
<!-- Pre-Post Processing Thaw4 Additional Tests -->

<xsl:if test="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">


<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW[generate-id(.) = generate-id(key('k2', TEST_FIND2)[1])]">
<xsl:if test="TEST_FIND2='4'" >
<table width="90%">
<tr>
<td align="left" >
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.name')" /> 
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>

<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">
<xsl:if test="TEST_FIND2='4'">
<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:value-of select="LABTEST_NAME" /> 
</td>
<td>
<table>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>
<td align="center"><xsl:value-of select="TEST_RESULT" /></td>
</tr>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.type')" /></td>
<td align="center"><xsl:value-of select="SAMPLE_TYPE" /></td>
</tr>
</table>
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>

</xsl:if>

<!-- Additional Tests ends -->
</xsl:if>


<xsl:if test="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/POST_CRYOPRES_DATE5 != ''">

<br></br> <br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<td><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.htest')" /></font></b></td>
<td align ="center"><b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw')" /></font></b>&#160;&#160;<xsl:value-of select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW/POST_CRYOPRES_DATE5" />
</td>
</tr>
</table>

<xsl:variable name="NOT_NEEDED_TEST_POST_CRY_THAW5">
<xsl:choose>
<xsl:when test="$sysflag='1'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0'  and //LAB_SUMMARY/LAB_SUMMARY_ROW[4]/POST_CRYOPRES_THAW5!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')" />
</xsl:when>
<xsl:when test="$sysflag='0'  and //LAB_SUMMARY/LAB_SUMMARY_ROW[3]/POST_CRYOPRES_THAW5!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')" />
</xsl:when>
</xsl:choose>
</xsl:variable>



<xsl:for-each select="//ROW/LAB_SUMMARY/LAB_SUMMARY_ROW">
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST_POST_CRY_THAW5  and LABTEST_SHORTNAME!=$NOT_NEED_TEST1">	
<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left">
<xsl:if test="LABTEST_SHORTNAME!=$NOT_NEEDED_TEST1 and LABTEST_SHORTNAME!=$NOT_NEEDED_TEST2">
<xsl:value-of select="LABTEST_NAME" /> 
</xsl:if>
<xsl:if test="LABTEST_SHORTNAME=$NOT_NEEDED_TEST1 or LABTEST_SHORTNAME=$NOT_NEEDED_TEST2">
<xsl:value-of select="$TOT_CBU_NUC_CELL_COUNT" /> 
</xsl:if>
</td>
<td class="reportData" align="center" >
<xsl:if test="POST_CRYOPRES_THAW5">
<table>
<tr>
<td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>   <td class="reportData" align="left" > <xsl:value-of select="POST_CRYOPRES_THAW5" />
<!--choose append value-->

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbuvol') and POST_CRYOPRES_THAW5">&#160;
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbuvol.unit')" />
</xsl:if> 
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcncc') and POST_CRYOPRES_THAW5">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcncc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcbuun')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.unknownifuncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcbuun.unit')"  disable-output-escaping="yes"/>
</xsl:if> 

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.uncrct')">
&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.unit.uncorrected')" />
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.uncrct.unit')"  disable-output-escaping="yes"/>
</xsl:if>     

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.tcdad') and POST_CRYOPRES_THAW5">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.tcdad.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cnrbc')">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cnrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.totcd') and POST_CRYOPRES_THAW5">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.totcd.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt') and POST_CRYOPRES_THAW5">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cfccnt.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pernrbc') and POST_CRYOPRES_THAW5">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pernrbc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd') and POST_CRYOPRES_THAW5">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.percd.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertnuc') and POST_CRYOPRES_THAW5">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertnuc.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.percd3') and POST_CRYOPRES_THAW5">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertnuc.percd3')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.pertmon') and POST_CRYOPRES_THAW5">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.pertmon.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cbunccc') and POST_CRYOPRES_THAW5">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.cbunccc.unit')"  disable-output-escaping="yes"/>
</xsl:if>
																   
<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.fnpv') and POST_CRYOPRES_THAW5">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.fnpv.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<xsl:if test="LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab') and POST_CRYOPRES_THAW5">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.viab.unit')"  disable-output-escaping="yes"/>
</xsl:if>

<!--end append value-->
</td>
</tr>

<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.reasonfortest')" /></td>   <td class="reportData" align="left"> <xsl:value-of select="REASON_THAW5" /></td></tr>
<xsl:if test="REASON_THAW5=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="REASON_DESC_THAW5" /></td></tr>
</xsl:if>
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.sampletype')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="SMPLTYPE_THAW5" /></td></tr>
<!---Version changes -->
<xsl:if test="$V_NEW_TASK_FLAG='0' or LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.viab') or LABTEST_SHORTNAME=rf:getPropertiesData($props, 'garuda.labsummaryrep.testname.cfccnt')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.testmethod')" /> </td>   <td class="reportData" align="left"> <xsl:value-of select="TESTMETH_THAW5" /></td></tr>
<xsl:if test="TESTMETH_THAW5=rf:getPropertiesData($props, 'garuda.labsummaryrep.testmeth.other')">
<tr><td class="reportData" align="left"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hpostcryopresthaw.describe')" /> </td>   <td class="reportData" align="left"><xsl:value-of select="TEST_DESC_THAW5" /> </td></tr>
</xsl:if>
</xsl:if>
<!---Version changes ends -->

</table>
</xsl:if>
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>
<!-- Pre-Post Processing Thaw5 Additional Tests -->

<xsl:if test="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">

<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW[generate-id(.) = generate-id(key('k2', TEST_FIND2)[1])]">
<xsl:if test="TEST_FIND2='5'" >
<table width="90%">
<tr>
<td align="left" >
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.name')" /> 
</td>
</tr>
</table>
</xsl:if>
</xsl:for-each>


<xsl:for-each select="//ROW/OTHER_TEST_RES/OTHER_TEST_RES_ROW">
<xsl:if test="TEST_FIND2='5'">

<table border="1" bordercolor="black" width="90%">
<tr>
<td class="reportData" align="left" >
<xsl:value-of select="LABTEST_NAME" /> 
</td>
<td>
<table>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></td>
<td align="center"><xsl:value-of select="TEST_RESULT" /></td>
</tr>
<tr>
<td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.otherlabest.type')" /></td>
<td align="center"><xsl:value-of select="SAMPLE_TYPE" /></td>
</tr>
</table>
</td>
</tr>
</table>
</xsl:if>

</xsl:for-each>
</xsl:if>

<!-- Additional Tests ends -->
</xsl:if>

<br></br>
<p><xsl:value-of select="$vartemp"/><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.heading')" /></p>

<br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedby')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.category')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedon')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.notes')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.assessment')" /></TD>
</tr>
<xsl:for-each select="//ROW/LABSUMMARY_NOTES/LABSUMMARY_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" disable-output-escaping="yes" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:if test="not(//ROW/LABSUMMARY_NOTES/LABSUMMARY_NOTES_ROW)">
<tr colspan="7"><td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.norecordfound')" /></td></tr>
</xsl:if>
</table>

<br></br> 
<!--
<table>
<tr>
<TD><font size="3"> <xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.document.heading')" /></font></TD> 
<TD></TD> 
<TD></TD> 
</tr>
</table>

<br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.document.category')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfReport.document.filename')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfReport.document.createdon')" /></TD>
</tr>
<xsl:for-each select="../LABSUM_ATTACHMENTS/LABSUM_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>
</table>
-->



<br></br>
<hr width="100%"/>
<p><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.hprocessinginformation')" /></b></p>
<hr width="100%"/>
<br></br>


<table width="90%">
<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.processing')" />&#160;<xsl:value-of select="ROW/PROCESSING" />
</TD>
<TD>
</TD>
</tr>

<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.typeofautomatedcbu')"/>&#160;<xsl:value-of select="ROW/AUTOMATED_TYPE" />
</TD>
<TD>
<xsl:if test="ROW/OTHER_PROCESSING!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.plsspecify')" />&#160;<xsl:value-of select="ROW/OTHER_PROCESSING" />
</xsl:if>
</TD>
</tr>



<tr>
<td>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.productmodi')" />&#160;<xsl:value-of select="ROW/PRODUCT_MODIFICATION" />
</td>
<td>
<xsl:if test="ROW/OTHER_PRODUCT_MODI!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.plsspecify')" />&#160;<xsl:value-of select="ROW/OTHER_PRODUCT_MODI" /> 
</xsl:if>
</td>
</tr>


<!--- New Adding Fields1 -->


<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.storagemethod')" />&#160;<xsl:value-of select="ROW/STORAGE_METHOD" /> 
</TD>
<TD>
</TD>
</tr>

<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.freezermanufacturer')" />&#160;<xsl:value-of select="ROW/FREEZER_MANUFACT" /> 
</TD>
<td>
<xsl:if test="ROW/OTHER_FREEZER_MANUFACT!=''">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.freezermanufacturerother')" />&#160;<xsl:value-of select="ROW/OTHER_FREEZER_MANUFACT" /> 
</xsl:if>
</td>
</tr>


<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.frozenvialtubes')" />&#160;<xsl:value-of select="ROW/FROZEN_IN" /> 
</TD>
<TD>
<xsl:if test="ROW/FROZEN_IN='Other'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.plsspecify')" />&#160;<xsl:value-of select="ROW/OTHER_FROZEN_CONT" /> 
</xsl:if>
</TD>
</tr>



<!--- End of Newly Added Fields1 -->


<xsl:if test="ROW/FROZEN_IN='Bags'">
<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.noofbags')" />&#160;<xsl:value-of select="ROW/NO_OF_BAGS" /> 
</TD>
<TD>
</TD>
</tr>
</xsl:if>

</table>



<xsl:if test="ROW/FROZEN_IN='Bags' and ROW/NO_OF_BAGS!='Other'">
<table width="90%">

<tr>
<td>
<xsl:if test="ROW/NO_OF_BAGS='1 bag'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.bagtype')" />&#160;<xsl:value-of select="ROW/BAG1TYPE" /> 
</xsl:if>
<xsl:if test="ROW/NO_OF_BAGS!='1 bag'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.bagonetype')" />&#160;<xsl:value-of select="ROW/BAG1TYPE" /> 
</xsl:if>
</td>
<td></td>
</tr>

<tr>
<td>
<xsl:if test="ROW/NO_OF_BAGS='1 bag'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.cryobagmanufacturer')" />&#160;<xsl:value-of select="ROW/CRYOBAG_MANUFAC" />
</xsl:if>
<xsl:if test="ROW/NO_OF_BAGS!='1 bag'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.cryobagmanufac.bagtype1')" />&#160;<xsl:value-of select="ROW/CRYOBAG_MANUFAC" />
</xsl:if>
</td>
<td>
<xsl:if test="ROW/NO_OF_BAGS='1 bag'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.maxivolume')" /> 
&#160;<xsl:value-of select="ROW/MAX_VOL1" />
</xsl:if>
<xsl:if test="ROW/NO_OF_BAGS!='1 bag'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.bag1maxvol')" />
&#160;<xsl:value-of select="ROW/MAX_VOL1" />
</xsl:if>
</td>
</tr>

<tr>
<td>
<xsl:if test="ROW/BAG1TYPE='Other'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.plsspecify')" />&#160;<xsl:value-of select="ROW/OTHER_BAG_TYPE1" /> 
</xsl:if>
</td>
<td>
</td>
</tr>


<xsl:if test="ROW/BAG2TYPE!=''">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.bagtwotype')" />&#160;<xsl:value-of select="ROW/BAG2TYPE" /></td>
<td></td>
</tr>

<tr>
<td>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.cryobagmanufac.bagtype2')" />&#160;
<xsl:value-of select="ROW/CRYOBAG_MANUFAC2" />
</td>
<td>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.bag2maxvol')" />&#160;<xsl:value-of select="ROW/MAX_VOL2" />
</td>
</tr>

<tr>
<td>
<xsl:if test="ROW/BAG2TYPE='Other'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.plsspecify')" />&#160;<xsl:value-of select="ROW/OTHER_BAG_TYPE2" /> 
</xsl:if>
</td>
<td>
</td>
</tr>
</xsl:if>

</table>
</xsl:if>



<xsl:if test="ROW/FROZEN_IN='Bags' and ROW/NO_OF_BAGS='Other'">
<table width="90%">
<tr>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.plsspecify')" />&#160;<xsl:value-of select="ROW/OTHER_BAG" /></td>
</tr>
</table>
</xsl:if>


<!--- New Adding Fields2 -->

<table width="90%">
<tr>
<td>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.storagetemp')" />&#160;
<xsl:value-of select="ROW/STORAGE_TEMPERATURE" disable-output-escaping="yes"  /> 
</td>
</tr>

<tr>
<td>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.controlledratefreezing')" />&#160;
<xsl:value-of select="ROW/CTRL_RATE_FREEZING" /> 
</td>
</tr>

<tr>
<td>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.noofindividualfractions')" />&#160;
<xsl:value-of select="ROW/INDIVIDUAL_FRAC" /> 
</td>
</tr>
</table>


<!--- End of Newly Added Fields2 -->




<br></br>
<p><xsl:value-of select="$vartemp" /><u><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.hfrozenproductcompos')" /></u></p>
<br></br>




<table border="1" border-color="black" width="90%">

<tr bgcolor="#808080">
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.hadditivesbeforevolume')" /></TD>
<TD></TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.thousherparin')" /></TD> 
<TD>
<xsl:if test="ROW/HEPARIN_THOU_PER!=''">
<xsl:value-of select="ROW/HEPARIN_THOU_PER" />%
</xsl:if>
<xsl:if test="ROW/HEPARIN_THOU_ML!=''">
<xsl:value-of select="ROW/HEPARIN_THOU_ML" />&#160;ml
</xsl:if>
</TD>
</tr>


<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.fivethousherparin')" /></TD> 
<TD>
<xsl:if test="ROW/HEPARIN_FIVE_PER!=''">
<xsl:value-of select="ROW/HEPARIN_FIVE_PER" />%
</xsl:if>
<xsl:if test="ROW/HEPARIN_FIVE_ML!=''">
<xsl:value-of select="ROW/HEPARIN_FIVE_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.tenthousherparin')" /></TD> 
<TD>
<xsl:if test="ROW/HEPARIN_TEN_PER!=''">
<xsl:value-of select="ROW/HEPARIN_TEN_PER" />%
</xsl:if>
<xsl:if test="ROW/HEPARIN_TEN_ML!=''">
<xsl:value-of select="ROW/HEPARIN_TEN_ML" />&#160;ml
</xsl:if>
</TD>
</tr>



<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.sixperhydroxyethyl')" /></TD> 
<TD>
<xsl:if test="ROW/HEPARIN_SIX_PER!=''">
<xsl:value-of select="ROW/HEPARIN_SIX_PER" />%
</xsl:if>
<xsl:if test="ROW/HEPARIN_SIX_ML!=''">
<xsl:value-of select="ROW/HEPARIN_SIX_ML" />&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbbprocedures.label.ml')" />
</xsl:if>
</TD>
</tr>



<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.cpda')" /></TD> 
<TD>
<xsl:if test="ROW/CPDA_PER!=''">
<xsl:value-of select="ROW/CPDA_PER" />%
</xsl:if>
<xsl:if test="ROW/CPDA_ML!=''">
<xsl:value-of select="ROW/CPDA_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.cpd')" /></TD> 
<TD>
<xsl:if test="ROW/CPD_PER!=''">
<xsl:value-of select="ROW/CPD_PER" />%
</xsl:if>
<xsl:if test="ROW/CPD_ML!=''">
<xsl:value-of select="ROW/CPD_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.acd')" /></TD> 
<TD>
<xsl:if test="ROW/ACD_PER!=''">
<xsl:value-of select="ROW/ACD_PER" />%
</xsl:if>
<xsl:if test="ROW/ACD_ML!=''">
<xsl:value-of select="ROW/ACD_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.otheranticoagulant')" /></TD> 
<TD>
<xsl:if test="ROW/OTHR_ANTI_PER!=''">
<xsl:value-of select="ROW/OTHR_ANTI_PER" />%
</xsl:if>
<xsl:if test="ROW/OTHR_ANTI_ML!=''">
<xsl:value-of select="ROW/OTHR_ANTI_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<xsl:if test="ROW/OTHR_ANTI_PER!='' or ROW/OTHR_ANTI_ML!=''">
<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.specifyotheranticoag')" /></TD> 
<TD><xsl:value-of select="ROW/SPECI_OTHR_ANTI" /></TD>
</tr>
</xsl:if>

<tr bgcolor="#808080">
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.hcryoprotectansandother')" /></TD>
<TD></TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.hundrperdmso')" /></TD> 
<TD>
<xsl:if test="ROW/HUN_DMSO_PER!=''">
<xsl:value-of select="ROW/HUN_DMSO_PER" />%
</xsl:if>
<xsl:if test="ROW/HUN_DMSO_ML!=''">
<xsl:value-of select="ROW/HUN_DMSO_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.hundredperglycerol')" /></TD> 
<TD>
<xsl:if test="ROW/HUN_GLYCEROL_PER!=''">
<xsl:value-of select="ROW/HUN_GLYCEROL_PER" />%
</xsl:if>
<xsl:if test="ROW/HUN_GLYCEROL_ML!=''">
<xsl:value-of select="ROW/HUN_GLYCEROL_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.tenpercentdextran')" /></TD> 
<TD>
<xsl:if test="ROW/TEN_DEXTRAN_40_PER!=''">
<xsl:value-of select="ROW/TEN_DEXTRAN_40_PER" />%
</xsl:if>
<xsl:if test="ROW/TEN_DEXTRAN_40_ML!=''">
<xsl:value-of select="ROW/TEN_DEXTRAN_40_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.fivepercenthumanalbum')" /></TD> 
<TD>
<xsl:if test="ROW/FIVE_HUMAN_ALBU_PER!=''">
<xsl:value-of select="ROW/FIVE_HUMAN_ALBU_PER" />%
</xsl:if>
<xsl:if test="ROW/FIVE_HUMAN_ALBU_ML!=''">
<xsl:value-of select="ROW/FIVE_HUMAN_ALBU_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.twentyfivehumanalbum')" /></TD> 
<TD>
<xsl:if test="ROW/TWENTYFIVE_HUMAN_ALBU_PER!=''">
<xsl:value-of select="ROW/TWENTYFIVE_HUMAN_ALBU_PER" />%
</xsl:if>
<xsl:if test="ROW/TWENTYFIVE_HUMAN_ALBU_ML!=''">
<xsl:value-of select="ROW/TWENTYFIVE_HUMAN_ALBU_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.plasmalyte')" /></TD> 
<TD>
<xsl:if test="ROW/PLASMALYTE_PER!=''">
<xsl:value-of select="ROW/PLASMALYTE_PER" />%
</xsl:if>
<xsl:if test="ROW/PLASMALYTE_ML!=''">
<xsl:value-of select="ROW/PLASMALYTE_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.othercryoprotect')" /></TD> 
<TD>
<xsl:if test="ROW/OTHR_CRYOPROTECTANT_PER!=''">
<xsl:value-of select="ROW/OTHR_CRYOPROTECTANT_PER" />%
</xsl:if>
<xsl:if test="ROW/OTHR_CRYOPROTECTANT_ML!=''">
<xsl:value-of select="ROW/OTHR_CRYOPROTECTANT_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<xsl:if test="ROW/OTHR_CRYOPROTECTANT_PER!='' or ROW/OTHR_CRYOPROTECTANT_ML!=''">
<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.specifyothercryoprotect')" /></TD> 
<TD><xsl:value-of select="ROW/SPEC_OTHR_CRYOPROTECTANT" /></TD>
</tr>
</xsl:if>

<tr bgcolor="#808080">
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.hdiluents')" /></TD>
<TD></TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.fivepercentdextrose')" /></TD> 
<TD>
<xsl:if test="ROW/FIVE_DEXTROSE_PER!=''">
<xsl:value-of select="ROW/FIVE_DEXTROSE_PER" />%
</xsl:if>
<xsl:if test="ROW/FIVE_DEXTROSE_ML!=''">
<xsl:value-of select="ROW/FIVE_DEXTROSE_ML" />&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbbprocedures.label.ml')" />
</xsl:if>
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.pointninenacl')" /></TD> 
<TD>
<xsl:if test="ROW/POINT_NINE_NACL_PER!=''">
<xsl:value-of select="ROW/POINT_NINE_NACL_PER" />%
</xsl:if>
<xsl:if test="ROW/POINT_NINE_NACL_ML!=''">
<xsl:value-of select="ROW/POINT_NINE_NACL_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.otherdiluents')" /></TD> 
<TD>
<xsl:if test="ROW/OTHR_DILUENTS_PER!=''">
<xsl:value-of select="ROW/OTHR_DILUENTS_PER" />%
</xsl:if>
<xsl:if test="ROW/OTHR_DILUENTS_ML!=''">
<xsl:value-of select="ROW/OTHR_DILUENTS_ML" />&#160;ml
</xsl:if>
</TD>
</tr>

<xsl:if test="ROW/OTHR_DILUENTS_PER!='' or ROW/OTHR_DILUENTS_ML!=''">
<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.specifyotherdiluents')" /></TD> 
<TD><xsl:value-of select="ROW/SPEC_OTHR_DILUENTS" /></TD>
</tr>
</xsl:if>


</table>

<br></br>
<p><xsl:value-of select="$vartemp" /><u><xsl:value-of select="rf:getPropertiesData($props, 'garuda.labsummaryrep.hcbusamples')" /></u></p>
<br></br>

<table width="80%">
<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.nooffilterpapersamples')" />
&#160;<xsl:value-of select="ROW/FILTER_PAPER" /> 
</TD>
</tr>

<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.noofrbcpellets')" />
&#160;<xsl:value-of select="ROW/RPC_PELLETS" /> 
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.noofextrdnasamples')" />
&#160;<xsl:value-of select="ROW/EXTR_DNA" /> 
</TD>
</tr>

<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.noofserumsamp')" /> 
&#160;<xsl:value-of select="ROW/SERUM_ALIQUOTES" /> 
</TD>
</tr>

<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.noofplasmasampl')" />
&#160;<xsl:value-of select="ROW/PLASMA_ALIQUOTES" /> 
</TD>
</tr>

<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.noofnonviablecell')" />
&#160;<xsl:value-of select="ROW/NONVIABLE_ALIQUOTES" /> 
</TD>
</tr>


<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.noofviablesampnotrep')" />
&#160;<xsl:value-of select="ROW/NONVIABLE_FINAL_PROD" /> 
</TD>
</tr>

</table>

<br></br>
<p><xsl:value-of select="$vartemp" /><xsl:value-of select="$vartemp" /><u><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbbprocedures.label.aliqrepoffinalprod')" /></u></p>
<br></br>

<table width="80%">

<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.noofsegments')" />
&#160;<xsl:value-of select="ROW/NO_OF_SEGMENTS" /> 
</TD>
</tr>

<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.noofaliquotsfinalproduct')" />
&#160;<xsl:value-of select="ROW/NO_OF_OTH_REP_ALLIQUOTS_F_PROD" /> 
</TD>
</tr>

<tr>
<TD><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.noofaliquotsaltercondi')" />
&#160;<xsl:value-of select="ROW/NO_OF_OTH_REP_ALLIQ_ALTER_COND" /> 
</TD>
</tr>

</table>


<br></br>
<p><xsl:value-of select="$vartemp" /><u><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.hmaternalsamples')" /></u></p>
<br></br>

<table width="80%">
<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.noofmaterserumsamples')" />
&#160;<xsl:value-of select="ROW/NO_OF_SERUM_MATER_ALIQUOTS" /> 
</TD>
</tr>

<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.noofmatplasmasampl')" />
&#160;<xsl:value-of select="ROW/NO_OF_PLASMA_MATER_ALIQUOTS" /> 
</TD>
</tr>

<tr>
<TD> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.noofmatextracdnasamples')" />
&#160;<xsl:value-of select="ROW/NO_OF_EXTR_DNA_MATER_ALIQUOTS" /> 
</TD>
</tr>

<tr>
<TD>
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.processingrep.noofmiscellaneousmatsamp')" />
&#160;<xsl:value-of select="ROW/NO_OF_CELL_MATER_ALIQUOTS" /> 
</TD>
</tr>

</table>


<br></br>
<p><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.heading')" /></p>

<br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedby')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.category')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedon')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.notes')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.assessment')" /></TD>
</tr>
<xsl:for-each select="//ROW/PROCESSING_NOTES/PROCESSING_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:if test="not(//ROW/PROCESSING_NOTES/PROCESSING_NOTES_ROW)">
<tr colspan="7"><td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.norecordfound')" /></td></tr>
</xsl:if>
</table>


<!--<br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.document.category')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfReport.document.filename')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfReport.document.createdon')" /></TD>
</tr>
<xsl:for-each select="//ROW/PROCESSINFO_ATTACHMENTS/PROCESSINFO_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>
</table>


<br></br>
<table>
<tr>
<td  align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.nameofperson')" />

<xsl:choose>
<xsl:when test="ROW/SAMPLE_CREATOR!=''">
	<xsl:value-of select="ROW/SAMPLE_CREATOR" /> 
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="ROW/SAMPLE_LAST_MODBY" /> 
	
</xsl:otherwise>
</xsl:choose>
</td>

</tr>
<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.esignprovided')" />
<xsl:choose>
<xsl:when test="ROW/SAMPLE_LAST_MODBY!=''">
	<xsl:value-of select="ROW/SAMPLE_LAST_MODBY" /> 
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="ROW/SAMPLE_CREATOR" /> 

</xsl:otherwise>

</xsl:choose>
</td>
</tr>

<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.completionofform')" />
<xsl:choose>
<xsl:when test="ROW/SAMPLE_LAST_MODDT!=''">
	 <xsl:value-of select="ROW/SAMPLE_LAST_MODDT" />
</xsl:when>
<xsl:otherwise>
	  <xsl:value-of select="ROW/SAMPLE_CREATED_ON" />
</xsl:otherwise>

</xsl:choose>
</td>

</tr>
</table>-->

<hr class="thickLine" width="100%"/>
<b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.completeReqInfo.label.mrq_infull')" /></font></b> 
<hr class="thickLine" width="100%"/>
<br></br>
<p><xsl:value-of select="rf:getPropertiesData($props, 'garuda.label.dynamicform.datemomans')" />:&#160;<xsl:value-of select="//ROW/MRQ_FORM_DATA/MRQ_FORM_DATA_ROW/DATEFILLED" /></p>
<br></br>
<table border="1" bordercolor="black">
<tr>
<td colspan="3"><xsl:value-of select="rf:getPropertiesData($props,'garuda.cbufinalreview.label.flaggedItems.question')"/></td>
<td><xsl:value-of select="rf:getPropertiesData($props,'garuda.label.dynamicform.response')"/></td>
<td><xsl:value-of select="rf:getPropertiesData($props,'garuda.fmhqreport.assesment')"/></td>
<td><xsl:value-of select="rf:getPropertiesData($props,'garuda.fmhqreport.assesmentnotes')"/></td>
</tr>
</table>
<xsl:for-each select="//ROW/GRP/GRP_ROW">
	
	<table border="1" bordercolor="black"><tr bgcolor="#808080"><td colspan="4"><xsl:value-of select="QUESTION_GRP_NAME" /></td></tr></table>


		<xsl:variable name="PKGRPVAL" select="PK_QUESTION_GRP" /> 
		<xsl:for-each select="//ROW/MRQ/MRQ_ROW">
			<xsl:if test="$PKGRPVAL=FK_QUES_GRP">

					<xsl:variable name="FKDEPEN_PKVAL" select="DEPENT_QUES_PK" /> 
									
					<xsl:if test="$FKDEPEN_PKVAL='0' or $FKDEPEN_PKVAL!='0'">
					<table border="1" bordercolor="black">
					<tr>
					
			
								
								<td colspan="3">
								<xsl:if test="MASTER_QUES!=''">&#160;&#160;&#160;&#160;</xsl:if>
								<xsl:value-of select="QUES_SEQ" />.&#160;&#160;<xsl:value-of select="QUES_DESC" disable-output-escaping="yes"/></td>
								<td>
								<xsl:if test="RESPONSE_TYPE='dropdown'">
								<xsl:value-of select="DROPDOWN_VALUE" />
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
								<xsl:value-of select="RESPONSE_VAL" />
								</xsl:if>
								</td>
								<td><xsl:value-of select="ASSES_RESPONSE" /></td>
								<td>
								<xsl:if test="ASSESS_VISIB_FLAG='1'">								
<xsl:value-of select="ASSES_NOTES" />
</xsl:if>
</td>
				
		
					</tr>
					</table>
					</xsl:if>	
			</xsl:if>
		</xsl:for-each>
		

			
</xsl:for-each>

<br></br> <br></br>
<hr class="thickLine" width="100%"/>
<b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.fmhqreport.htitle')" /></font></b> 
<hr class="thickLine" width="100%"/>
<p><xsl:value-of select="rf:getPropertiesData($props,'garuda.label.dynamicform.formcompdate')" />:&#160;<xsl:value-of select="//ROW/FMHQ_FORM_DATA/FMHQ_FORM_DATA_ROW/DATEFILLED" /></p>
<br></br>
<table border="1" bordercolor="black">
<tr>
<td colspan="3"><xsl:value-of select="rf:getPropertiesData($props,'garuda.cbufinalreview.label.flaggedItems.question')"/></td>
<td><xsl:value-of select="rf:getPropertiesData($props,'garuda.label.dynamicform.response')"/></td>
<td><xsl:value-of select="rf:getPropertiesData($props,'garuda.fmhqreport.assesment')"/></td>
<td><xsl:value-of select="rf:getPropertiesData($props,'garuda.fmhqreport.assesmentnotes')"/></td>
</tr>
</table>
<xsl:for-each select="//ROW/FMHQ_GRP/FMHQ_GRP_ROW">
	
	<table border="1" bordercolor="black"><tr bgcolor="#808080"><td colspan="4"><xsl:value-of select="QUESTION_GRP_NAME" />&#160; </td></tr></table>

		<xsl:variable name="PKGRPVAL" select="PK_QUESTION_GRP" /> 
		<xsl:for-each select="//ROW/FMHQ/FMHQ_ROW">
			<xsl:if test="$PKGRPVAL=FK_QUES_GRP">

							    <xsl:variable name="FKDEPEN_PKVAL" select="DEPENT_QUES_PK" /> 

							     <xsl:if test="$FKDEPEN_PKVAL='0' or $FKDEPEN_PKVAL!='0'">
									<table border="1" bordercolor="black">
									<tr>
									<xsl:if test="$FKDEPEN_PKVAL!='0'">
									<td colspan="3"><xsl:value-of select="$vartemp"/><xsl:value-of select="QUES_SEQ" />&#160;&#160;<xsl:value-of select="QUES_DESC" /></td>
</xsl:if>
									<xsl:if test="$FKDEPEN_PKVAL='0'">
									<td colspan="3"><xsl:value-of select="QUES_SEQ" />.&#160;&#160;<xsl:value-of select="QUES_DESC" /></td>
</xsl:if>
									<td>
									<xsl:if test="RESPONSE_TYPE='dropdown'">
										<xsl:value-of select="DROPDOWN_VALUE" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									<xsl:if test="RESPONSE_TYPE='multiselect' and RESPONSE_VAL!='0'">
										<xsl:value-of select="RESPONSE_VAL" />
									</xsl:if>
									</td>
									<td><xsl:value-of select="ASSES_RESPONSE" /></td>
									<td><xsl:if test="ASSESS_VISIB_FLAG='1'">								
<xsl:value-of select="ASSES_NOTES" />
</xsl:if></td>
									</tr>
									</table>
							    </xsl:if>	
			</xsl:if>
		</xsl:for-each>
	
</xsl:for-each>
<br></br>
<p><b><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.heading')" /></b></p>

<br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedby')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.category')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedon')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.notes')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.assessment')" /></TD>
</tr>
<xsl:for-each select="//ROW/HSH_NOTES/HSH_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:if test="not(//ROW/HSH_NOTES/HSH_NOTES_ROW)">
<tr><td colspan="5" align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.norecordfound')" /></td></tr>
</xsl:if>
</table>

<br></br>
<!--
<br></br>

<xsl:for-each select="//ROW/FMHQ_HISTORY/FMHQ_HISTORY_ROW">
<table>
<tr>
<td  align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.nameofperson')" />

<xsl:choose>
<xsl:when test="CREATOR!=''">
	<xsl:value-of select="CREATOR" /> 
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="MODIFIED_BY" /> 
</xsl:otherwise>
</xsl:choose>
</td>

</tr>
<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.esignprovided')" />
<xsl:choose>
<xsl:when test="MODIFIED_BY!=''">
	<xsl:value-of select="MODIFIED_BY" /> 
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="CREATOR" /> 

</xsl:otherwise>

</xsl:choose>
</td>
</tr>

<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.completionofform')" />
<xsl:choose>
<xsl:when test="MODIFIED_ON!=''">
	 <xsl:value-of select="MODIFIED_ON" />
</xsl:when>
<xsl:otherwise>
	  <xsl:value-of select="CREATED_ON" />
</xsl:otherwise>

</xsl:choose>
</td>

</tr>
</table>
</xsl:for-each>
-->
<br></br> <br></br>
<hr class="thickLine" width="100%"/>
<b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.completeReqInfo.label.idm_infull')" /></font></b> 
<hr class="thickLine" width="100%"/>

<xsl:variable name="V_IDM_TXT_FLAG" select="ROW/IDM_TXT_FLAG" />

<br></br>
<table>
<tr>
<td colspan="2"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.idm.source')" />&#160;<xsl:value-of select="//ROW/IDM_FORM_DATA/IDM_FORM_DATA_ROW/TYPE_FRM" /></td>
<td><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.idm.bloodcolldt')" />&#160;<xsl:value-of select="//ROW/IDM_FORM_DATA/IDM_FORM_DATA_ROW/DATEFILLED" /></td>
</tr>
</table>
<xsl:variable name="CMS_VAL">
<xsl:choose>
<xsl:when test="//ROW/IDM/IDM_ROW[1]">
<xsl:for-each select="//ROW/IDM/IDM_ROW[1]">
<xsl:value-of select="DROPDOWN_VALUE" />
</xsl:for-each>
</xsl:when>
</xsl:choose>
</xsl:variable>

<xsl:variable name="FDA_VAL">
<xsl:choose>
<xsl:when test="//ROW/IDM/IDM_ROW[2]">
<xsl:for-each select="//ROW/IDM/IDM_ROW[2]">
<xsl:value-of select="DROPDOWN_VALUE" />
</xsl:for-each>
</xsl:when>
</xsl:choose>
</xsl:variable>


<xsl:for-each select="//ROW/IDM_GRP/IDM_GRP_ROW">
	
	


		<xsl:variable name="PKGRPVAL" select="PK_QUESTION_GRP" /> 
		
		<xsl:variable name="LAST_GRP">
			<xsl:choose>
			<xsl:when test="QUESTION_GRP_NAME='Additional / Miscellaneous IDM Tests'">
			<xsl:value-of select="'123'" />
			</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="SUPPLEM_GRP">
						<xsl:choose>
						<xsl:when test="QUESTION_GRP_NAME='Confirmatory / Supplemental Tests'">
						<xsl:value-of select="'456'" />
						</xsl:when>
						</xsl:choose>
		</xsl:variable>  
		
		<xsl:if test="$LAST_GRP!='123' and QUESTION_GRP_NAME!=0">
		<table border="1" >
		<tr>
		
		<td COLSPAN="2"><b><xsl:value-of select="QUESTION_GRP_NAME" /></b></td>
		<td><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.idmreport.cmslab')" /></b></td>
		<td><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.idmreport.fdakit')" /></b> </td>
		<td COLSPAN="2"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></b></td>
		<td COLSPAN="2"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.fmhqreport.assesment')" /></b></td>
		<td COLSPAN="2"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.fmhqreport.assesmentnotes')" /></b></td>
		</tr>
		</table>
		</xsl:if>

		<xsl:if test="$LAST_GRP='123'">
		<table border="1" >
		<tr>
		<td COLSPAN="2"><b><xsl:value-of select="QUESTION_GRP_NAME" /></b></td>
		<td COLSPAN="2"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbusummary.report.labsummary.result')" /></b></td>
		<td COLSPAN="2"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.fmhqreport.assesment')" /></b></td>
		<td COLSPAN="2"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.fmhqreport.assesmentnotes')" /></b></td>
		</tr>
		</table>
		</xsl:if>
		
		<xsl:for-each select="//ROW/IDM/IDM_ROW">
			
			<xsl:if test="$PKGRPVAL=FK_QUES_GRP">
					
					<xsl:variable name="FKDEPEN_PKVAL" select="DEPENT_QUES_PK" /> 
					<xsl:variable name="VAR_DEPEN_QUES_PK" select="DEPENT_QUES_PK"/> 
					<xsl:variable name="VAR_DEPEN_VALUE" select="DEPENT_QUES_VALUE"/> 

					<xsl:if test="FDA_ELIG!='' and $SUPPLEM_GRP='456' and $LAST_GRP!='123'">
					<table border="1" bordercolor="black">
					<tr>
								
								<td COLSPAN="2">
								<xsl:value-of select="QUES_DESC" />
								</td>
								<td><xsl:value-of select="following-sibling::*[2]/DROPDOWN_VALUE" /></td>
								<td><!-- <xsl:value-of select="following-sibling::*[3]/DROPDOWN_VALUE" />--></td>
								<td COLSPAN="2">
								<xsl:if test="RESPONSE_TYPE='dropdown'">
									<xsl:value-of select="DROPDOWN_VALUE" />
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
									<xsl:value-of select="RESPONSE_VAL" />
								</xsl:if>

								</td>
							
								<td COLSPAN="2"><xsl:value-of select="ASSES_RESPONSE" /></td>
								<td COLSPAN="2"><xsl:if test="ASSESS_VISIB_FLAG='1'">								
<xsl:value-of select="ASSES_NOTES" />
</xsl:if>
					</td>				
					</tr>
					</table>
					</xsl:if>
					
			
					<xsl:if test="(FDA_ELIG!='' or QUES_DESC='HIV p24 Ag') and position()!=2 and $SUPPLEM_GRP!='456' and position()!=1 and $LAST_GRP!='123'">
					<table border="1" bordercolor="black">
					<tr>
								
								<td COLSPAN="2">
									<xsl:if test="$V_IDM_TXT_FLAG=1 and QUES_SEQ=10">
									<xsl:value-of select="QUES_DESC" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.idmreport.txt.mpx')" />
									</xsl:if>
									<xsl:if test="$V_IDM_TXT_FLAG!=1 or QUES_SEQ!=10">
									<xsl:value-of select="QUES_DESC" />
									</xsl:if>
								</td>
								<td><xsl:value-of select="following-sibling::*[2]/DROPDOWN_VALUE" /></td>
								<td><xsl:value-of select="following-sibling::*[3]/DROPDOWN_VALUE" /></td>
								<td COLSPAN="2">
								<xsl:if test="RESPONSE_TYPE='dropdown'">
									<xsl:value-of select="DROPDOWN_VALUE" />
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
									<xsl:value-of select="RESPONSE_VAL" />
								</xsl:if>
								</td>
								<td COLSPAN="2"><xsl:value-of select="ASSES_RESPONSE" /></td>
								<td COLSPAN="2"><xsl:if test="ASSESS_VISIB_FLAG='1'">								
<xsl:value-of select="ASSES_NOTES" />
</xsl:if>
					</td>			
				
		
					</tr>
					</table>
					</xsl:if>


					<xsl:if test="$FKDEPEN_PKVAL='0' and position()!=2 and position()!=1 and $SUPPLEM_GRP!='456' and $LAST_GRP='123' and RESPONSE_CODE!='0'">
					<table border="1" bordercolor="black">
					<tr>
								<td COLSPAN="2"><xsl:value-of select="RESPONSE_VAL12" /></td>
								<td COLSPAN="2">
								<xsl:if test="RESPONSE_TYPE='dropdown'">
									<xsl:value-of select="DROPDOWN_VALUE" />
								</xsl:if>
								<xsl:if test="RESPONSE_TYPE='textfield' and RESPONSE_VAL!='0'">
									<xsl:value-of select="RESPONSE_VAL" />
								</xsl:if>
								</td>
								<td COLSPAN="2"><xsl:value-of select="ASSES_RESPONSE" /></td>
								<td COLSPAN="2">
<xsl:if test="ASSESS_VISIB_FLAG='1'">								
<xsl:value-of select="ASSES_NOTES" />
</xsl:if>
					</td>			
				
		
					</tr>
					</table>
					</xsl:if>
			</xsl:if>

		</xsl:for-each>
	<br></br>
	<br></br>	

	
</xsl:for-each>
<!--

<xsl:for-each select="//ROW/IDM_HISTORY/IDM_HISTORY_ROW">
<table>
<tr>
<td  align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.nameofperson')" />

<xsl:choose>
<xsl:when test="CREATOR!=''">
	<xsl:value-of select="CREATOR" /> 
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="MODIFIED_BY" /> 
	
</xsl:otherwise>
</xsl:choose>
</td>

</tr>
<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.esignprovided')" />
<xsl:choose>
<xsl:when test="MODIFIED_BY!=''">
	<xsl:value-of select="MODIFIED_BY" /> 
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="CREATOR" /> 

</xsl:otherwise>

</xsl:choose>
</td>
</tr>

<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.completionofform')" />
<xsl:choose>
<xsl:when test="MODIFIED_ON!=''">
	 <xsl:value-of select="MODIFIED_ON" />
</xsl:when>
<xsl:otherwise>
	  <xsl:value-of select="CREATED_ON" />
</xsl:otherwise>

</xsl:choose>
</td>

</tr>
</table>
</xsl:for-each>

-->

<br></br>
<p><b><xsl:value-of select="$vartemp" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.heading')" /></b></p>

<br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedby')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.category')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedon')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.notes')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.assessment')" /></TD>
</tr>
<xsl:for-each select="//ROW/IDM_NOTES/IDM_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:if test="not(//ROW/IDM_NOTES/IDM_NOTES_ROW)">
<tr><td colspan="5" align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.norecordfound')" /></td></tr>
</xsl:if>

</table>

<br></br>

<!--
<br></br> 
<p><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.document.heading')" /></p>
<br></br> 

<table border="1" bordercolor="black">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.document.category')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfReport.document.filename')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfReport.document.createdon')" /></TD>
</tr>
<xsl:for-each select="//ROW/IDM_ATTACHMENTS/IDM_ATTACHMENTS_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="CATEGORYVAL" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="FILENAME" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="CREATEDON" /></TD>
</tr>
</xsl:for-each>
</table>-->
<!--
<xsl:for-each select="//ROW/HISTORY/HISTORY_ROW">
<table>
<tr>
<td  align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.nameofperson')" />

<xsl:choose>
<xsl:when test="CREATOR!=''">
	<xsl:value-of select="CREATOR" /> 
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="MODIFIED_BY" /> 
	
</xsl:otherwise>
</xsl:choose>
</td>

</tr>
<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.esignprovided')" />
<xsl:choose>
<xsl:when test="MODIFIED_BY!=''">
	<xsl:value-of select="MODIFIED_BY" /> 
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="CREATOR" /> 

</xsl:otherwise>

</xsl:choose>
</td>
</tr>

<tr>
<td align="left" colspan="2"> 
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.idsReport.completionofform')" />
<xsl:choose>
<xsl:when test="MODIFIED_ON!=''">
	 <xsl:value-of select="MODIFIED_ON" />
</xsl:when>
<xsl:otherwise>
	  <xsl:value-of select="CREATED_ON" />
</xsl:otherwise>

</xsl:choose>
</td>

</tr>
</table>
</xsl:for-each>-->


<!--
<br></br> <br></br>
<b><font size="3"> <xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.heading')" /> </font></b> 
<br></br><br></br>
<table border="1" bordercolor="black">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedby')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.category')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedon')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.notes')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.assessment')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.reason')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.sendto')" /></TD>
</tr>
<xsl:for-each select="//ROW/IDS_NOTES/IDS_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/>&#160;<xsl:value-of select="NOTES_CATEGORY" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//ROW/CBU_INFO_NOTES/CBU_INFO_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/>&#160;<xsl:value-of select="NOTES_CATEGORY" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//ROW/HLA_NOTES/HLA_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/>&#160;<xsl:value-of select="NOTES_CATEGORY" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//ROW/ELIGIBILITY_NOTES/ELIGIBILITY_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/>&#160;<xsl:value-of select="NOTES_CATEGORY" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//ROW/LABSUMMARY_NOTES/LABSUMMARY_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/>&#160;<xsl:value-of select="NOTES_CATEGORY" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//ROW/IDM_NOTES/IDM_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/>&#160;<xsl:value-of select="NOTES_CATEGORY" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_REASON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_SENDTO" /></TD>
</tr>
</xsl:for-each>
</table>




<xsl:if test="ROW/ORDERTYPEDATA=0">
<br></br> <br></br>
<b><font size="3"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.hcbuhistory')" /></font></b> 


<br></br> <br></br>

<table border="1" bordercolor="black">
<tr>
<TD align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.date')" /></b></TD>
<TD align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.cordevent')" /></b></TD>
<TD align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.user')" /></b></TD>
<TD align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.requesttype')" /></b></TD>
</tr>

<xsl:for-each select="//ROW/CBUHISTORY/CBUHISTORY_ROW">
<tr>
<TD align="left">
<xsl:value-of select="DATEVAL" /> 
</TD>

<TD align="left">


<xsl:if test="TYPECODE='cord_status'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.msgloccordsts')" />&#160;<xsl:value-of select="DESCRIPTION1" /> 
</xsl:if>

<xsl:if test="TYPECODE='Cord_Nmdp_Status'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.msgNmdpRegStatus')" />&#160;<xsl:value-of select="DESCRIPTION1" /> 
</xsl:if>


<xsl:if test="TYPECODE='order_status'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.msgOrderstatus')" />&#160;<xsl:value-of select="DESCRIPTION2" /> 
</xsl:if>

<xsl:if test="TYPECODE='FORMS'">
<xsl:value-of select="DESCRIPTION1" />&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.msgformstatus')" />
</xsl:if>


</TD>


<TD align="left">
<xsl:if test="TYPECODE='Cord_Nmdp_Status'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbuhistory.label.system')" /> 
</xsl:if>
<xsl:if test="TYPECODE!='Cord_Nmdp_Status'">
<xsl:value-of select="CREATOR" />
</xsl:if>
</TD>

<TD align="left" >
<xsl:value-of select="ORDERTYPE" /> 
</TD>


</tr>
</xsl:for-each>
</table>
</xsl:if>


<xsl:if test="ROW/ORDERTYPEDATA=1">
<br></br> <br></br>
<b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.hreqhistory')" /></font></b> 


<br></br> <br></br>

<table border="1" bordercolor="black">
<tr>
<TD align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.date')" /></b></TD>
<TD align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.cordevent')" /></b></TD>
<TD align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.user')" /></b></TD>
<TD align="center"><b><xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.requesttype')" /></b></TD>
</tr>
<xsl:for-each select="//ROW/CBUREQHISTORY/CBUREQHISTORY_ROW">
<tr>
<TD align="left">
<xsl:value-of select="DATEVAL" /> 
</TD>

<TD align="left">


<xsl:if test="TYPECODE='cord_status'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.msgloccordsts')" />&#160;<xsl:value-of select="DESCRIPTION1" /> 
</xsl:if>

<xsl:if test="TYPECODE='Cord_Nmdp_Status'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.msgNmdpRegStatus')" />&#160;<xsl:value-of select="DESCRIPTION1" /> 
</xsl:if>


<xsl:if test="TYPECODE='order_status'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.msgOrderstatus')" />&#160;<xsl:value-of select="DESCRIPTION2" /> 
</xsl:if>

<xsl:if test="TYPECODE='FORMS'">
<xsl:value-of select="DESCRIPTION1" />&#160;<xsl:value-of select="rf:getPropertiesData($props, 'garuda.detailreport.cbuhistory.msgformstatus')" />
</xsl:if>


</TD>


<TD align="left">
<xsl:if test="TYPECODE='Cord_Nmdp_Status'">
<xsl:value-of select="rf:getPropertiesData($props, 'garuda.cbuhistory.label.system')" /> 
</xsl:if>
<xsl:if test="TYPECODE!='Cord_Nmdp_Status'">
<xsl:value-of select="CREATOR" />
</xsl:if>
</TD>

<TD align="left" >
<xsl:value-of select="ORDERTYPE" /> 
</TD>


</tr>
</xsl:for-each>
</table>
</xsl:if>-->
<br></br> <br></br>
<hr class="thickLine" width="100%"/>
<b><font size="3"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.heading1')" /></font></b> 
<hr class="thickLine" width="100%"/>
<br></br>
<table border="1" bordercolor="black" width="90%">
<tr>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedby')" /></TD>  
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.category')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.postedon')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.notes')" /></TD>
<TD align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.assessment')" /></TD>
</tr>

<xsl:for-each select="//ROW/IDS_NOTES/IDS_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//ROW/CBU_INFO_NOTES/CBU_INFO_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//ROW/HLA_NOTES/HLA_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//ROW/ELIGIBILITY_NOTES/ELIGIBILITY_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//ROW/LABSUMMARY_NOTES/LABSUMMARY_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>

<xsl:for-each select="//ROW/PROCESSING_NOTES/PROCESSING_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//ROW/IDM_NOTES/IDM_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>
<xsl:for-each select="//ROW/HSH_NOTES/HSH_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" disable-output-escaping="yes" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>

<xsl:for-each select="//ROW/POST_SHIP_NOTES/POST_SHIP_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" disable-output-escaping="yes" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>

<xsl:for-each select="//ROW/ADHOC_NOTES/ADHOC_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" disable-output-escaping="yes" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>

<xsl:for-each select="//ROW/OTHER_REC_NOTES/OTHER_REC_NOTES_ROW">
<tr>
<TD class="reportData" align="left"><xsl:value-of select="NOTE_CREATOR" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_TYPE" /><xsl:value-of select="rf:getPropertiesData($props, 'garuda.pdfreport.notes.cn')" /><xsl:value-of select="NOTES_SEQ"/></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_CREATEDON" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_DESC" disable-output-escaping="yes" /></TD>
<TD class="reportData" align="left"><xsl:value-of select="NOTES_ASSESSMENT" /></TD>
</tr>
</xsl:for-each>


<xsl:if test="not(//ROW/IDS_NOTES/IDS_NOTES_ROW) and not(//ROW/CBU_INFO_NOTES/CBU_INFO_NOTES_ROW) and not(//ROW/HLA_NOTES/HLA_NOTES_ROW) and not(//ROW/ELIGIBILITY_NOTES/ELIGIBILITY_NOTES_ROW) and not(//ROW/LABSUMMARY_NOTES/LABSUMMARY_NOTES_ROW)  and not(//ROW/PROCESSING_NOTES/PROCESSING_NOTES_ROW) and not(//ROW/IDM_NOTES/IDM_NOTES_ROW) and not(//ROW/HSH_NOTES/HSH_NOTES_ROW) and not(//ROW/POST_SHIP_NOTES/POST_SHIP_NOTES_ROW) and not(//ROW/ADHOC_NOTES/ADHOC_NOTES_ROW) and not(//ROW/OTHER_REC_NOTES/OTHER_REC_NOTES_ROW)">
<tr colspan="5"><td align="center"><xsl:value-of select="rf:getPropertiesData($props, 'garuda.report.norecordfound')" /></td></tr>
</xsl:if>

</table>


</xsl:template> 

</xsl:stylesheet>
