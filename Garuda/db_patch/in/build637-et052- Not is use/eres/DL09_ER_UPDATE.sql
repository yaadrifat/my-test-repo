--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'cbu_sample_type'
    AND codelst_subtyp = 'FPFS';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL=NULL,CODELST_CUSTOM_COL1=NULL,LAST_MODIFIED_DATE=SYSDATE where codelst_type = 'cbu_sample_type'
    AND codelst_subtyp = 'FPFS';
	commit;
  end if;
end;
/
--END--

