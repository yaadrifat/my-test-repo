--Start Update Codelist value for FMHQ Response--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'immune_def'
    AND codelst_subtyp = 'cis';
  if (v_column_exists = 1) then
	update er_codelst set CODELST_DESC = 'Combined Immunodeficiency Syndrome,Common Variable Immunodeficiency' where codelst_type = 'immune_def' AND codelst_subtyp = 'cis';
	commit;
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'immune_def'
    AND codelst_subtyp = 'cvid';
  if (v_column_exists = 1) then
    delete from er_codelst  where codelst_type = 'immune_def' AND codelst_subtyp = 'cvid';
	commit;
  end if;
end;
/

--END--



BEGIN
  SP_CLEAN_NOTES();
END;
/

