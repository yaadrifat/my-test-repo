--STARTS UPDATING RECORD FROM cb_alert_conditions TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_alert_conditions 
	where FK_CODELST_ALERT =(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert_resol' AND CODELST_SUBTYP='alert_35') and
	TABLE_NAME='ER_ORDER' and COLUMN_NAME in('FK_ORDER_RESOL_BY_CBB','FK_ORDER_RESOL_BY_TC');
  if (v_record_exists = 2) then
      update cb_alert_conditions set CONDITION_VALUE='(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE=''SA'')' where FK_CODELST_ALERT =(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert_resol' AND CODELST_SUBTYP='alert_35') and
	TABLE_NAME='ER_ORDER' and COLUMN_NAME in('FK_ORDER_RESOL_BY_CBB','FK_ORDER_RESOL_BY_TC');
	commit;
  end if;
end;
/
--END--