
--STARTS ADDING COLUMN ASSESSMENT_FLAG_COMMENT TO CB_HLA_TEMP--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_HLA_TEMP'
    AND column_name = 'HLA_TYPING_SEQ';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_HLA_TEMP ADD(HLA_TYPING_SEQ number(10))';
  end if;
end;
/
COMMENT ON COLUMN "CB_HLA_TEMP"."HLA_TYPING_SEQ" IS 'Stores HLA Typing Sequence Number';