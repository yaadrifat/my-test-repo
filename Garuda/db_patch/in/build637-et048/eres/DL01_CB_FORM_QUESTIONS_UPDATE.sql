--start update cb_form_questions table--
--MRQ Question 3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_form=(select pk_form from cb_forms where forms_desc='MRQ' and version='N2G') and fk_question = (select pk_questions from cb_questions where ques_code='tkn_medications_ind');
  if (v_record_exists = 1) then
	update cb_form_questions set unexpected_response_value='' where fk_form=(select pk_form from cb_forms where forms_desc='MRQ' and version='N2G') and fk_question = (select pk_questions from cb_questions where ques_code='tkn_medications_ind');
commit;
  end if;
end;
/
--MRQ Question 46--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_form=(select pk_form from cb_forms where forms_desc='MRQ' and version='N2G') and fk_question = (select pk_questions from cb_questions where ques_code='hiv_1_2_o_performed_ind');
  if (v_record_exists = 1) then
	update cb_form_questions set unexpected_response_value='' where fk_form=(select pk_form from cb_forms where forms_desc='MRQ' and version='N2G') and fk_question = (select pk_questions from cb_questions where ques_code='hiv_1_2_o_performed_ind');
commit;
  end if;
end;
/
--IDM Question 6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='anti_hiv_1_2_o_ind');
  if (v_record_exists = 1) then
	update cb_form_questions set unexpected_response_value='' where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='anti_hiv_1_2_o_ind');
commit;
  end if;
end;
/
--IDM Question 10--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='nat_hiv_1_hcv_hbv_mpx_ind');
  if (v_record_exists = 1) then
	update cb_form_questions set unexpected_response_value='' where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='nat_hiv_1_hcv_hbv_mpx_ind');
commit;
  end if;
end;
/
--IDM Question 12--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='hiv_antigen_rslt');
  if (v_record_exists = 1) then
	update cb_form_questions set unexpected_response_value='' where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='hiv_antigen_rslt');
commit;
  end if;
end;
/
--IDM Question 18--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='syphilis_ct_sup_react_ind');
  if (v_record_exists = 1) then
	update cb_form_questions set unexpected_response_value='' where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='syphilis_ct_sup_react_ind');
commit;
  end if;
end;
/
--END--


--start update cb_questions table--
--MRQ Question 3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions where ques_code='tkn_medications_ind';
  if (v_record_exists = 1) then
	update cb_questions set assesment_flag=0 where ques_code='tkn_medications_ind';
commit;
  end if;
end;
/
--MRQ Question 46--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions where ques_code='hiv_1_2_o_performed_ind';
  if (v_record_exists = 1) then
	update cb_questions set assesment_flag=0 where ques_code='hiv_1_2_o_performed_ind';
commit;
  end if;
end;
/
--IDM Question 6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions where ques_code='anti_hiv_1_2_o_ind';
  if (v_record_exists = 1) then
	update cb_questions set assesment_flag=0 where ques_code='anti_hiv_1_2_o_ind';
commit;
  end if;
end;
/
--IDM Question 10--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions where ques_code='nat_hiv_1_hcv_hbv_mpx_ind';
  if (v_record_exists = 1) then
	update cb_questions set assesment_flag=0 where ques_code='nat_hiv_1_hcv_hbv_mpx_ind';
commit;
  end if;
end;
/
--IDM Question 12--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions where ques_code='hiv_antigen_rslt';
  if (v_record_exists = 1) then
	update cb_questions set assesment_flag=0 where ques_code='hiv_antigen_rslt';
commit;
  end if;
end;
/
--IDM Question 18--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions where ques_code='syphilis_ct_sup_react_ind';
  if (v_record_exists = 1) then
	update cb_questions set assesment_flag=0 where ques_code='syphilis_ct_sup_react_ind';
commit;
  end if;
end;
/
--END--

