
--Script to update the sequence of ER_CODELST for Hemoglnopathy
update er_codelst set codelst_seq=2 where codelst_type='hem_path_scrn' and codelst_subtyp='alp_thal_trait';

update er_codelst set codelst_seq=3 where codelst_type='hem_path_scrn' and codelst_subtyp='alp_thal_sev';

update er_codelst set codelst_seq=4 where codelst_type='hem_path_scrn' and codelst_subtyp='alp_thal_sil';

update er_codelst set codelst_seq=5 where codelst_type='hem_path_scrn' and codelst_subtyp='beta_thalmia';

update er_codelst set codelst_seq=6 where codelst_type='hem_path_scrn' and codelst_subtyp='hemo_trait';

update er_codelst set codelst_seq=7 where codelst_type='hem_path_scrn' and codelst_subtyp='hemo_sick_dis';

update er_codelst set codelst_seq=8 where codelst_type='hem_path_scrn' and codelst_subtyp='homo_no_dis';

update er_codelst set codelst_seq=9 where codelst_type='hem_path_scrn' and codelst_subtyp='mult_trait';

update er_codelst set codelst_seq=10 where codelst_type='hem_path_scrn' and codelst_subtyp='sickle_beta';
commit;