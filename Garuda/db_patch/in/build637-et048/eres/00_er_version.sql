Declare
  v_patch_exists BOOLEAN;  
Begin
	V_Patch_Exists := F_Check_Previous_Patch('v9.0.0 Build#637-ET047');
	If (V_Patch_Exists) Then
		UPDATE ER_CTRLTAB SET CTRL_VALUE = 'v9.0.0 Build#637-ET048'
		where CTRL_KEY = 'app_version' ;
		commit;
		execute immediate 'INSERT INTO track_patches VALUES(seq_track_patches.nextval,271,0,''00_er_version.sql'',sysdate,''9.0.0 B#637-ET048'')';
		commit;
		--dbms_output.put_line('Process successfully');
	Else
		--dbms_output.put_line('The database, where current patches are executed, is not updated to last the build : v9.0.0 Build#637-ET013.02');
		raise_application_error(-20001, 'The database, where current patches are executed, is not updated to last the build : v9.0.0 Build#637-ET047');
	End if;
End;
/

update track_patches  set db_ver_mjr = 265 where app_version = '9.0.0 B#637-ET045' and db_ver_mjr = 263;
commit;
