/* This readMe is specific to Velos eResearch version 9.0 build637-et004 */

=====================================================================================================================================
Garuda :

Following patches are specific to Garuda so do not execute over eres database

1)DL10_acces_right_delete.sql
2)DL11_Lookup.sql
3)DL12_CBUhomePage.sql
4)DL13_Top_Navigation_Pane.sql
5)DL14_Top_navigation_panel_maintenance.sql
6)DL15_Top_navigation_panel_Libraries_cont.sql
7)DL16_CBURecord_static_panel.sql
8)DL17_CBURecord_Quick_link_panel.sql
9)DL18_Order_releated_permission.sql
10)DL19_CBU_clinical_record.sql


=====================================================================================================================================
