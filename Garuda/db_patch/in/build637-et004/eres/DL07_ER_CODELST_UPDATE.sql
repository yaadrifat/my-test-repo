--SQL for CBB Profile configuration


DECLARE
  v_record_exists_one number := 0;  
  v_record_exists_two number := 0;  
  v_record_exists_three number := 0;  
  v_record_exists_four number := 0;  
  v_record_exists_five number := 0;  
  v_record_exists_six number := 0;  
BEGIN
  Select count(*) into v_record_exists_one
    from er_codelst
   WHERE codelst_type='shipper' AND codelst_subtyp='others';
   
    Select count(*) into v_record_exists_two
    from er_codelst
   WHERE codelst_type='accreditation' AND codelst_subtyp='FACT';
   
    Select count(*) into v_record_exists_three
    from er_codelst
   WHERE codelst_type='accreditation' AND codelst_subtyp='NetCord_AABB';
   
    Select count(*) into v_record_exists_four
    from er_codelst
   WHERE codelst_type='shping_pwrk_loc' AND codelst_subtyp='on_tp_in_ctr_ld';
   
    Select count(*) into v_record_exists_five
    from er_codelst
   WHERE codelst_type='shping_pwrk_loc' AND codelst_subtyp='insd_otr_shl_ld';
   
    Select count(*) into v_record_exists_six
    from er_codelst
   WHERE codelst_type='shping_pwrk_loc' AND codelst_subtyp='bw_side_otr_inr';
   
   
    if (v_record_exists_one = 1) then
		update er_codelst set codelst_desc = 'Other'   where codelst_type = 'shipper' 
		and codelst_subtyp = 'others';
	commit;
  end if;
  
  if (v_record_exists_two = 1) then
		update er_codelst set CODELST_SEQ = '1'   where codelst_type = 'accreditation' 
		and codelst_subtyp = 'FACT';
	commit;
  end if;
  
  if (v_record_exists_three = 1) then
		update er_codelst set CODELST_SEQ = '2'   where codelst_type = 'accreditation' 
		and codelst_subtyp = 'NetCord_AABB';
	commit;
  end if;
  
  if (v_record_exists_four = 1) then
		update er_codelst set CODELST_SEQ = '1'   where codelst_type = 'shping_pwrk_loc' 
		and codelst_subtyp = 'on_tp_in_ctr_ld';

	commit;
  end if;
  
  if (v_record_exists_five = 1) then
		update er_codelst set CODELST_SEQ = '2'   where codelst_type = 'shping_pwrk_loc' 
		and codelst_subtyp = 'insd_otr_shl_ld';
	commit;
  end if;
  
  if (v_record_exists_six = 1) then
		update er_codelst set CODELST_SEQ = '3'   where codelst_type = 'shping_pwrk_loc' 
		and codelst_subtyp = 'bw_side_otr_inr';
	commit;
  end if;
END;
/
