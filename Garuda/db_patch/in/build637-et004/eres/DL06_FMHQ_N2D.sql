--N2D--
--Question 1.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_medhist_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '1.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='m_or_f_adopted_medhist_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 5.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '5.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='child_die_age_10_desc') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 6.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_cause')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '6.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='stillborn_cause') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 7.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '7.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_cl_other_dises_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 7.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '7.a.1' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type1') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 7.a.1.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '7.a.1.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc1') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 7.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '7.a.2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type2') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 7.a.2.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '7.a.2.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc2') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 7.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '7.a.3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type3') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 7.a.3.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '7.a.3.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cancer_leuk_rel_type_desc3') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 8.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '8.a.1' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type1') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 8.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '8.a.2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type2') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 8.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '8.a.3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type3') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 8.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '8.a.4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type4') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 8.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '8.a.5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type5') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 8.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '8.a.6' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='red_bld_cell_dis_type6') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 9.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '9.a.1' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type1') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 9.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '9.a.2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type2') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 9.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '9.a.3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type3') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 9.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '9.a.4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type4') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 9.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '9.a.5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type5') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 9.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '9.a.6' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='while_bld_cell_dis_type6') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 10.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '10.a.1' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type1') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 10.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '10.a.2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type2') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 10.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '10.a.3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type3') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 10.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '10.a.4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type4') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 10.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '10.a.5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type5') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 10.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '10.a.6' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='imune_def_ind_type6') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 11.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '11.a.1' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type1') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 11.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '11.a.2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type2') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 11.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '11.a.3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type3') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 11.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '11.a.4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type4') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 11.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '11.a.5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type5') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 11.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '11.a.6' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='platelet_dis_ind_type6') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 12.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '12.a.1' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc1') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 12.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '12.a.2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc2') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 12.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '12.a.3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc3') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 12.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '12.a.4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc4') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 12.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '12.a.5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc5') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 12.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '12.a.6' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_bld_dises_desc6') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 15.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '15.a.1' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type1') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 15.a.1.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '15.a.1.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc1') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 15.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '15.a.2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type2') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 15.a.2.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '15.a.2.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc2') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 15.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '15.a.3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type3') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 15.a.3.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '15.a.3.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc3') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 15.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '15.a.4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type4') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 15.a.4.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '15.a.4.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc4') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 15.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '15.a.5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type5') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 15.a.5.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '15.a.5.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc5') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 15.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '15.a.6' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_type6') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 15.a.6.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '15.a.6.a' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_met_stg_dises_desc6') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 17.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_type1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '17.a.1' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_type1') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 17.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_type2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '17.a.2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_type2') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 17.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_type3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '17.a.3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='severe_aut_imm_sys_disordr_type3') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 18.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '18.a.1' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc1') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 18.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '18.a.2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc2') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 18.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '18.a.3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='oth_immune_dis_desc3') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 24.a.1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc1')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '24.a.1' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc1') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 24.a.2--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc2')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '24.a.2' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc2') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 24.a.3--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc3')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '24.a.3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc3') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 24.a.4--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc4')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '24.a.4' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc4') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 24.a.5--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc5')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '24.a.5' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc5') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
--Question 24.a.6--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc6')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS SET QUES_SEQ = '24.a.6' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='inher_other_dises_desc6') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
commit;
  end if;
end;
/
