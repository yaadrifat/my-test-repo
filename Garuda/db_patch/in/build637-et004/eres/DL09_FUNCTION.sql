create or replace
function f_cbu_history(cordid number) return SYS_REFCURSOR
IS
v_cord_hty SYS_REFCURSOR;
BEGIN
    OPEN V_CORD_HTY FOR      SELECT * FROM(
                             SELECT
                             TO_CHAR(STATUS.STATUS_DATE,'Mon DD, YYYY') AS DATEVAL,
                             CBUSTATUS.CBU_STATUS_DESC DESCRIPTION1,
                              CASE
                              WHEN STATUS.CREATOR=0
                              THEN 'System'
                              WHEN STATUS.CREATOR!=0
                              THEN F_GETUSER(STATUS.CREATOR)
                             END CREATOR,
                             '-' ORDERTYPE,
                             STATUS.STATUS_TYPE_CODE TYPECODE,
                             TO_CHAR(SYSDATE,'Mon DD,YYYY') SYSTEMDATE,
                             ''  DESCRIPTION2,
                             STATUS.STATUS_DATE
                             FROM
                             CB_ENTITY_STATUS STATUS
                             LEFT OUTER JOIN CB_CBU_STATUS CBUSTATUS
                             ON (STATUS.FK_STATUS_VALUE=CBUSTATUS.PK_CBU_STATUS)
                             WHERE (STATUS.ENTITY_ID=CORDID AND STATUS.FK_ENTITY_TYPE=(SELECT C.PK_CODELST FROM ER_CODELST C WHERE C.CODELST_TYPE='entity_type' AND C.CODELST_SUBTYP='CBU'))
                             AND(STATUS.STATUS_TYPE_CODE='cord_status' OR STATUS.STATUS_TYPE_CODE='Cord_Nmdp_Status')
                             UNION ALL
                             SELECT
                             TO_CHAR(STAT.STATUS_DATE,'Mon DD, YYYY') AS DATEVAL,
                             '',
                             CASE
                              WHEN STAT.CREATOR=0
                              THEN 'System'
                              WHEN STAT.CREATOR!=0
                              THEN F_GETUSER(STAT.CREATOR)
                             END CREATOR,
                             NVL(F_CODELST_DESC(ORD.ORDER_TYPE),'-'),
                             STAT.STATUS_TYPE_CODE,
                             TO_CHAR(SYSDATE,'Mon DD, YYYY') SYSTEMDATE,
                             F_CODELST_DESC(STAT.FK_STATUS_VALUE),
                             STAT.STATUS_DATE
                             FROM
                             CB_ENTITY_STATUS STAT
                             LEFT OUTER JOIN ER_ORDER ORD
                             ON (ORD.PK_ORDER=STAT.ENTITY_ID)
                             WHERE
                             STAT.ENTITY_ID IN(SELECT PK_ORDER FROM ER_ORDER ORD,ER_ORDER_HEADER ORDHDR WHERE ORD.FK_ORDER_HEADER=ORDHDR.PK_ORDER_HEADER AND ORDHDR.ORDER_ENTITYID=CORDID)
                             AND STAT.FK_ENTITY_TYPE=(SELECT C.PK_CODELST FROM ER_CODELST C WHERE C.CODELST_TYPE='entity_type' AND C.CODELST_SUBTYP='ORDER')
                             AND STAT.STATUS_TYPE_CODE='order_status'
                             UNION ALL
                             SELECT
                             TO_CHAR(FRMVR.CREATED_ON,'Mon DD, YYYY') DATEVAL,
                             FRM.FORMS_DESC DESC1,
                             F_GETUSER(FRMVR.CREATOR) CREA,
                             '-',
                             'FORMS' FRMNAME,
                             TO_CHAR(sysdate,'Mon DD, YYYY') systemdate,
                             '' desc7,
                             FRMVR.CREATED_ON
                             FROM CB_FORM_VERSION FRMVR
                             LEFT OUTER JOIN CB_FORMS FRM ON (FRMVR.FK_FORM      =FRM.PK_FORM)
                             WHERE FRMVR.ENTITY_ID  =cordid AND FRMVR.CREATED_ON  IS NOT NULL AND FRMVR.ENTITY_TYPE IN (SELECT C.PK_CODELST FROM ER_CODELST C WHERE C.CODELST_TYPE='test_source' AND C.CODELST_SUBTYP='maternal')
                             )ORDER BY 8 DESC;
RETURN V_CORD_HTY;
END;
/


create or replace
function f_cbu_req_history(cordid number,orderid number) return SYS_REFCURSOR
IS
v_cord_req_hty SYS_REFCURSOR;
BEGIN
    OPEN V_CORD_REQ_HTY FOR
                        SELECT *FROM(SELECT
                        TO_CHAR(STAT.STATUS_DATE,'Mon DD, YYYY')AS DATEVAL,
                        STATUS.CBU_STATUS_DESC DESCRIPTION1,
                        CASE
                              WHEN STAT.CREATOR=0
                              THEN 'System'
                              WHEN STAT.CREATOR!=0
                              THEN F_GETUSER(STAT.CREATOR)
                        END CREATOR,
                        NVL(F_CODELST_DESC(ORD1.ORDER_TYPE),'-') ORDERTYPE,
                        STAT.STATUS_TYPE_CODE TYPECODE,
                        TO_CHAR(SYSDATE,'Mon DD, YYYY') SYSTEMDATE,
                        F_CODELST_DESC(STAT.FK_STATUS_VALUE) DESCRIPTION2,
                        STAT.STATUS_DATE
                        FROM
                        CB_ENTITY_STATUS STAT
                        LEFT OUTER JOIN
                        CB_CBU_STATUS STATUS ON(STAT.FK_STATUS_VALUE=STATUS.PK_CBU_STATUS)
                        LEFT OUTER JOIN ER_ORDER ORD1 ON(STAT.ENTITY_ID=ORD1.PK_ORDER)
                        WHERE
                        (STAT.ENTITY_ID=CORDID OR STAT.ENTITY_ID =ORDERID) AND (STAT.STATUS_TYPE_CODE='order_status' OR STAT.STATUS_TYPE_CODE='cord_status' OR STAT.STATUS_TYPE_CODE='Cord_Nmdp_Status' )
                        UNION ALL
                        SELECT
                        TO_CHAR(FRMVR.CREATED_ON,'Mon DD, YYYY') DATEVAL,
                        FRM.FORMS_DESC DESC1,
                        F_GETUSER(FRMVR.CREATOR) CREA,
                        '-',
                        'FORMS' FRMNAME,
                        TO_CHAR(sysdate,'Mon DD, YYYY') systemdate,
                        '' desc7,
                        FRMVR.CREATED_ON
                        FROM CB_FORM_VERSION FRMVR
                        LEFT OUTER JOIN CB_FORMS FRM ON (FRMVR.FK_FORM =FRM.PK_FORM)
                        WHERE FRMVR.ENTITY_ID  =CORDID AND FRMVR.CREATED_ON  IS NOT NULL AND FRMVR.ENTITY_TYPE IN (SELECT C.PK_CODELST FROM ER_CODELST C WHERE C.CODELST_TYPE='test_source' AND C.CODELST_SUBTYP='maternal')
                        )ORDER BY 8 DESC;
RETURN V_CORD_REQ_HTY;
END;
/

create or replace
function F_HLA_PACKING_SLIP(pk_cord number) return SYS_REFCURSOR
IS
p_hla_refcur SYS_REFCURSOR;
BEGIN
    OPEN p_hla_refcur FOR SELECT 
                            F_CODELST_DESC(FK_SOURCE) AS sourceval,
                            F_CODELST_DESC(FK_HLA_CODE_ID) AS LOCUS,
                            F_CODELST_DESC(FK_HLA_METHOD_ID) METHODDATA,  
                            hla_value_type1 TYPE1,
                            hla_value_type2 TYPE2,
                            TO_CHAR(HLA_RECEIVED_DATE,'Mon DD, YYYY') ENTRYDATE,
                            MAX(PK_HLA) MAXVAL
                          FROM cb_hla
                          WHERE ENTITY_ID = pk_cord GROUP BY F_CODELST_DESC(FK_SOURCE), F_CODELST_DESC(FK_HLA_CODE_ID), F_CODELST_DESC(FK_HLA_METHOD_ID), hla_value_type1, hla_value_type2, TO_CHAR(HLA_RECEIVED_DATE,'Mon DD, YYYY') ORDER BY F_CODELST_DESC(FK_HLA_CODE_ID);
RETURN P_HLA_REFCUR;
END;
/


COMMIT;
