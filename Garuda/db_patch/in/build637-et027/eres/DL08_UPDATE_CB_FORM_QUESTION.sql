--start update cb_form_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='treponemal_ct_ind');
  if (v_record_exists = 1) then
	update cb_form_questions set DEFER_VALUE=(SELECT pk_codelst FROM ER_CODELST WHERE CODELST_TYPE='test_outcome2' AND CODELST_SUBTYP='no')  where fk_form=(select pk_form from cb_forms where forms_desc='IDM' and version='N2F') and fk_question = (select pk_questions from cb_questions where ques_code='treponemal_ct_ind');
commit;
  end if;
end;
/
--END--
