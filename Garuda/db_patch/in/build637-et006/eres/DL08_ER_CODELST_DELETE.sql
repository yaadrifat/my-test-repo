DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'gender'
    AND codelst_subtyp = 'unknown';
  if (v_column_exists = 1) then
delete from ER_CODELST where CODELST_TYPE='gender' and CODELST_SUBTYP='unknown';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'gender'
    AND codelst_subtyp = 'other';
  if (v_column_exists = 1) then
delete from ER_CODELST where CODELST_TYPE='gender' and CODELST_SUBTYP='other';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'fung_cul'
    AND codelst_subtyp = 'unknown';
  if (v_column_exists = 1) then
delete from ER_CODELST where CODELST_TYPE='fung_cul' and CODELST_SUBTYP='unknown';
  end if;
end;
/

