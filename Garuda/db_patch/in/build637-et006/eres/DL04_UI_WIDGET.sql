 --Sql for CBU Import
--UIwidget entry

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where NAME = 'CBU Import'
    AND WIDGET_DIV_ID = 'cbuimport';
  if (v_record_exists = 0) then
 Insert into UI_WIDGET (PK_WIDGET,STATE,HELP_URL,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,IS_MOUSE_HOVER_ENABLED,CREATED_ON,LAST_MODIFIED_DATE,VERSION_NO,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
values (SEQ_UI_WIDGET.nextval,null,null,'CBU Import','cbuimport','CBU Import _Description','1','1','1','1','1','1',null,null,null,100,400,1024,1024);
	commit;
  end if;
end;

/

-- for entry in UI_CONTAINER_WIDGET_MAP 
DECLARE
  v_record_exists number:= 0; 
   v_record_exists_1 number :=0;
  BEGIN
   Select  count(*) into  v_record_exists  
 from   UI_WIDGET where name like 'CBU Import' ;
 if(v_record_exists != 0) then
  Select  count(*) into  v_record_exists_1  
    from UI_CONTAINER_WIDGET_MAP a, UI_WIDGET b where a.FK_WIDGET_ID = b.PK_WIDGET and b.name like 'CBU Import' ;
 if(v_record_exists_1 = 0) then
  Insert into UI_CONTAINER_WIDGET_MAP (PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values
(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from UI_WIDGET_CONTAINER where container_name='Progress Cord'),
(select PK_WIDGET from ui_widget where name='CBU Import'));
	commit;
  end if;
  end if;
 end;
/


 --Sql for Not Avail CBU
--UIwidget entry

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where NAME = 'Not Avail CBU'
    AND WIDGET_DIV_ID = 'notavailablecbus';
  if (v_record_exists = 0) then
 Insert into UI_WIDGET (PK_WIDGET,STATE,HELP_URL,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,IS_MOUSE_HOVER_ENABLED,CREATED_ON,LAST_MODIFIED_DATE,VERSION_NO,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
values (SEQ_UI_WIDGET.nextval,null,null,'Not Avail CBU','notavailablecbus','Not Avail CBU_Description','1','1','1','1','1','1',null,null,null,100,400,1024,1024);
	commit;
  end if;
end;

/

-- for entry in UI_CONTAINER_WIDGET_MAP 
DECLARE
  v_record_exists number:= 0; 
   v_record_exists_1 number :=0;
  BEGIN
   Select  count(*) into  v_record_exists  
 from   UI_WIDGET where name like 'Not Avail CBU' ;
 if(v_record_exists != 0) then
  Select  count(*) into  v_record_exists_1  
    from UI_CONTAINER_WIDGET_MAP a, UI_WIDGET b where a.FK_WIDGET_ID = b.PK_WIDGET and b.name like 'Not Avail CBU' ;
 if(v_record_exists_1 = 0) then
  Insert into UI_CONTAINER_WIDGET_MAP (PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values
(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from UI_WIDGET_CONTAINER where container_name='Progress Cord'),
(select PK_WIDGET from ui_widget where name='Not Avail CBU'));
	commit;
  end if;
  end if;
 end;

/

 --Sql for PF widget 
--UIwidget entry

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where NAME = 'PF Widget'
    AND WIDGET_DIV_ID = 'historydiv';
  if (v_record_exists = 0) then
 Insert into UI_WIDGET (PK_WIDGET,STATE,HELP_URL,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,IS_MOUSE_HOVER_ENABLED,CREATED_ON,LAST_MODIFIED_DATE,VERSION_NO,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
values (SEQ_UI_WIDGET.nextval,null,null,'PF Widget','historydiv','PF Widget _Description','1','1','1','1','1','1',null,null,null,100,400,1024,1024);
	commit;
  end if;
end;

/

-- for entry in UI_CONTAINER_WIDGET_MAP 
DECLARE
  v_record_exists number:= 0; 
   v_record_exists_1 number :=0;
  BEGIN
   Select  count(*) into  v_record_exists  
 from   UI_WIDGET where name like 'PF Widget' ;
 if(v_record_exists != 0) then
  Select  count(*) into  v_record_exists_1  
    from UI_CONTAINER_WIDGET_MAP a, UI_WIDGET b where a.FK_WIDGET_ID = b.PK_WIDGET and b.name like 'PF Widget' ;
 if(v_record_exists_1 = 0) then
  Insert into UI_CONTAINER_WIDGET_MAP (PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values
(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from UI_WIDGET_CONTAINER where container_name='CBU Clinical Record'),
(select PK_WIDGET from ui_widget where name='PF Widget'));
	commit;
  end if;
  end if;
 end;

 /