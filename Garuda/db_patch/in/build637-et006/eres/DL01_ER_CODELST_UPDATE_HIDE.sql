DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'unk';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='unk';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'api';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='api';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_desc = 'Caribbean Hispanic';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_SUBTYP='carhis' where CODELST_TYPE='race' AND codelst_desc = 'Caribbean Hispanic';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'carhis';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='carhis';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'eurwrc';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='eurwrc';
  end if;
end;
/



DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'hawi';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='hawi';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'his';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='his';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mafa';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mafa';
  end if;
end;
/



DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mapi';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mapi';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mcau';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mcau';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'menafc';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='menafc';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mhis';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mhis';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_desc = 'Multiple Native American';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_SUBTYP='mnam' where CODELST_TYPE='race' AND codelst_desc = 'Multiple Native American';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mnam';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mnam';
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mswhis';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mswhis';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'multi';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='multi';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'nam';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='nam';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'oth';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='oth';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'scahis';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='scahis';
  end if;
end;
/


DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'race'
    AND codelst_subtyp = 'mhaw';
  if (v_column_exists = 1) then
Update ER_CODELST set CODELST_HIDE='Y' where CODELST_TYPE='race' and CODELST_SUBTYP='mhaw';
  end if;
end;
/











