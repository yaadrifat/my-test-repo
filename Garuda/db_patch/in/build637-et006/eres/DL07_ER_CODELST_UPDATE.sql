--Update ER_CODELST for doc_categ 
Set define off;

DECLARE
  v_column_value_update number := 0;
BEGIN
  Select count(*) into v_column_value_update
    from user_tab_cols
    where TABLE_NAME = 'ER_CODELST'
    AND column_name = 'CODELST_DESC';
  if (v_column_value_update =1) then
   UPDATE ER_CODELST SET CODELST_DESC ='Receipt of CBU'
   WHERE  CODELST_TYPE='post_shpmnt' AND CODELST_SUBTYP='cbu_receipt';
   commit;
  end if;
end;
/
--END--



update er_codelst set codelst_desc = '> -135' where codelst_type = 'storage_temp' and codelst_subtyp = '>-135';
update er_codelst set codelst_desc = '>= -150 to <= -135' where codelst_type = 'storage_temp' and codelst_subtyp = '>=-150to<=-135';
update er_codelst set codelst_desc = '<= -150' where codelst_type = 'storage_temp' and codelst_subtyp = '<-150';

commit;