--STARTS ADDING COLUMN TO ER_ORDER TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'ER_ORDER' AND COLUMN_NAME = 'CORD_UNAVAIL_REASON';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ER_ORDER ADD CORD_UNAVAIL_REASON NUMBER(10,0)';
	commit;
  end if;
end;
/
 COMMENT ON COLUMN ER_ORDER.CORD_UNAVAIL_REASON IS 'Stores cord unavailability reason';
