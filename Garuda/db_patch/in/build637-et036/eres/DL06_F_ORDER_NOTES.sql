create or replace
FUNCTION f_order_notes(cordid  NUMBER,orderid NUMBER) RETURN SYS_REFCURSOR IS
  v_cordorder_notes SYS_REFCURSOR;
  BEGIN
    OPEN v_cordorder_notes FOR 
                             SELECT 
                              notes.pk_notes,
                              notes.notes,
                              notes.subject,
                              f_codelst_desc(notes.fk_notes_category) notes_category,
                              CASE WHEN f_codelst_desc(ord.order_type)='CT' THEN decode(ord.order_sample_at_lab,'Y','CT - Sample at Lab','N','CT - Ship Sample') WHEN f_codelst_desc(ord.order_type)!='CT' THEN f_codelst_desc(ord.order_type) END req_type,
                              TO_CHAR(notes.request_date,'Mon DD, YYYY') request_date,
                              TO_CHAR(notes.created_on,'Mon DD, YYYY hh:mm:ss am') date_time,
                              F_GETUSER(notes.CREATOR) note_user,
                              f_codelst_desc(notes.fk_notes_type) note_type
                            FROM cb_notes notes, er_order ord
                            WHERE ord.pk_order=orderid and notes.entity_id = cordid 
                            AND notes.fk_notes_type =
                              (SELECT pk_codelst
                              FROM er_codelst
                              WHERE codelst_type = 'note_type'
                              AND codelst_subtyp = 'progress'
                              )
                            ORDER BY notes.created_on DESC, notes.pk_notes ASC;
    RETURN v_cordorder_notes;
END;
/
