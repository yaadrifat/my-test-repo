create or replace
function f_cbu_notesall(cordid number) return SYS_REFCURSOR
IS
v_cord_notes SYS_REFCURSOR;
BEGIN
    OPEN v_cord_notes FOR
select pk_notes, notes, subject, f_codelst_desc(fk_notes_category) notes_category,
DECODE(NOTE_ASSESSMENT,'tu','Temporaily Unavailable','no_cause','No Cause for Deferal','na','Not Available') NOTES_ASSESSMENT,
to_char(created_on,'Mon DD, YYYY hh:mm:ss am') date_time, F_GETUSER(CREATOR) note_user,
f_codelst_desc(fk_notes_type) note_type, decode(visibility, 0 ,'No', 1, 'Yes', 'No') visi_tc
from cb_notes
where entity_id = cordid
and fk_notes_type =  F_CODELST_ID('note_type','clinic')
order by created_on desc, pk_notes asc;
RETURN v_cord_notes;
END;
/
