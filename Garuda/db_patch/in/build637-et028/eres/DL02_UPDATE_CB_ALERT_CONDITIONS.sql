--New CT Ship Sample Request Alert

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_01')
    AND COLUMN_NAME = 'ORDER_TYPE';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET CONDITION_VALUE='(Select PK_CODELST from er_codelst where codelst_type = ''order_type'' AND codelst_subtyp = ''CT'')',LOGICAL_OPERATOR='AND' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_01')
    AND COLUMN_NAME = 'ORDER_TYPE';
	commit;
  end if;
end;
/
--END--




--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_01')
    AND COLUMN_NAME = 'ORDER_STATUS';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_01'),'PRE',null,'ER_ORDER','ORDER_STATUS','(Select PK_CODELST from er_codelst where codelst_type = ''order_status'' AND codelst_subtyp = ''NEW'')','=',null,null,'PK_ORDER = #keycolumn',null,sysdate,null,sysdate,null,null,null);
	commit;
  end if;
end;
/
--END--

--======================================================================================================================================================


--New CT Sample At Lab Request Alert

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_02')
    AND COLUMN_NAME = 'ORDER_TYPE';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET CONDITION_VALUE='(Select PK_CODELST from er_codelst where codelst_type = ''order_type'' AND codelst_subtyp = ''CT'')',LOGICAL_OPERATOR='AND' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_02')
    AND COLUMN_NAME = 'ORDER_TYPE';
	commit;
  end if;
end;
/
--END--




--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_02')
    AND COLUMN_NAME = 'ORDER_STATUS';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_02'),'PRE',null,'ER_ORDER','ORDER_STATUS','(Select PK_CODELST from er_codelst where codelst_type = ''order_status'' AND codelst_subtyp = ''NEW'')','=',null,null,'PK_ORDER = #keycolumn',null,sysdate,null,sysdate,null,null,null);
	commit;
  end if;
end;
/
--END--

--======================================================================================================================================================


--New HE Request Alert

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_03')
    AND COLUMN_NAME = 'ORDER_TYPE';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET CONDITION_VALUE='(Select PK_CODELST from er_codelst where codelst_type = ''order_type'' AND codelst_subtyp = ''HE'')',LOGICAL_OPERATOR='AND' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_03')
    AND COLUMN_NAME = 'ORDER_TYPE';
	commit;
  end if;
end;
/
--END--




--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_03')
    AND COLUMN_NAME = 'ORDER_STATUS';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_03'),'PRE',null,'ER_ORDER','ORDER_STATUS','(Select PK_CODELST from er_codelst where codelst_type = ''order_status'' AND codelst_subtyp = ''NEW'')','=',null,null,'PK_ORDER = #keycolumn',null,sysdate,null,sysdate,null,null,null);
	commit;
  end if;
end;
/
--END--

--======================================================================================================================================================


--New OR Request Alert

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_04')
    AND COLUMN_NAME = 'ORDER_TYPE';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET CONDITION_VALUE='(Select PK_CODELST from er_codelst where codelst_type = ''order_type'' AND codelst_subtyp = ''OR'')',LOGICAL_OPERATOR='AND' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_04')
    AND COLUMN_NAME = 'ORDER_TYPE';
	commit;
  end if;
end;
/
--END--




--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_04')
    AND COLUMN_NAME = 'ORDER_STATUS';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_04'),'PRE',null,'ER_ORDER','ORDER_STATUS','(Select PK_CODELST from er_codelst where codelst_type = ''order_status'' AND codelst_subtyp = ''NEW'')','=',null,null,'PK_ORDER = #keycolumn',null,sysdate,null,sysdate,null,null,null);
	commit;
  end if;
end;
/
--END--

--======================================================================================================================================================
