
set define off;

DECLARE
  v_record_exists number:= 0;
   v_record_exists_1 number :=0;
  BEGIN
   Select  count(*) into  v_record_exists
 from   UI_WIDGET where name like 'Current Request Progress' ;
 if(v_record_exists != 0) then
  Select  count(*) into  v_record_exists_1
    from UI_CONTAINER_WIDGET_MAP a, UI_WIDGET b where a.FK_WIDGET_ID = b.PK_WIDGET and b.name like 'Current Request Progress' and FK_CONTAINER_ID=(select PK_CONTAINER from UI_WIDGET_CONTAINER where container_name='CBU Clinical Record');
 if(v_record_exists_1 = 0) then
  Insert into UI_CONTAINER_WIDGET_MAP (PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values
(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from UI_WIDGET_CONTAINER where container_name='CBU Clinical Record'),
(select PK_WIDGET from ui_widget where name='Current Request Progress'));
    commit;
  end if;
  end if;
 end;
/ 

--STARTS INSERTING RECORD INTO UI_PAGE TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_PAGE
    where PAGE_NAME = 'Processing Procedure';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_PAGE(PK_PAGE,PAGE_CODE,PAGE_NAME) 
  VALUES(SEQ_UI_PAGE.nextval,'30','Processing Procedure');
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO UI_WIDGET_CONTAINER TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET_CONTAINER
    where CONTAINER_NAME = 'Processing Procedure';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET_CONTAINER(PK_CONTAINER,FK_PAGE_ID,CONTAINER_NAME) 
  VALUES(SEQ_UI_WIDGET_CONTAINER.nextval,(SELECT PK_PAGE FROM UI_PAGE WHERE PAGE_NAME='Processing Procedure'),'Processing Procedure');
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO UI_WIDGET TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where WIDGET_DIV_ID = 'cbbProcView';
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_WIDGET(PK_WIDGET,NAME,WIDGET_DIV_ID,DESCRIPTION,IS_AUTHENTICATED,IS_MINIZABLE,IS_CLOSABLE,IS_RESIZEABLE,IS_DRAGABLE,
IS_MOUSE_HOVER_ENABLED,RESIZE_MIN_HEIGHT,RESIZE_MIN_WIDTH,RESIZE_MAX_HEIGHT,RESIZE_MAX_WIDTH) 
  VALUES(SEQ_UI_WIDGET.nextval,'Processing Procedure','cbbProcView','Processing Procedure',1,1,1,1,1,1,100,400,1024,1024);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO UI_CONTAINER_WIDGET_MAP TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_CONTAINER_WIDGET_MAP
    where FK_CONTAINER_ID = (SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Processing Procedure')
    AND FK_WIDGET_ID=(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='cbbProcView');
  if (v_record_exists = 0) then
      INSERT INTO 
  UI_CONTAINER_WIDGET_MAP(PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) 
  VALUES(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(SELECT PK_CONTAINER FROM UI_WIDGET_CONTAINER WHERE CONTAINER_NAME='Processing Procedure'),(SELECT PK_WIDGET FROM UI_WIDGET WHERE WIDGET_DIV_ID='cbbProcView'));
	commit;
  end if;
end;
/
--END--