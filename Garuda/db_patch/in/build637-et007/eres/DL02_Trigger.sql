--Trigger to update sample inventry

create or replace
TRIGGER UPDATE_SAMPLE_INVENTRY
AFTER  UPDATE OF FK_SAMPLE_TYPE_AVAIL,FK_ALIQUOTS_TYPE ON ERES.ER_ORDER
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
V_OLD_TABLE_NAME VARCHAR2(4000);
V_OLD_COLUMN_NAME VARCHAR2(4000);
V_NEW_TABLE_NAME VARCHAR2(4000);
V_NEW_COLUMN_NAME VARCHAR2(4000);
V_CORDID NUMBER;
V_ENTITY_CORD NUMBER;
V_ALIQUOTS NUMBER;
BEGIN
SELECT ORDER_ENTITYID INTO V_CORDID FROM ER_ORDER_HEADER WHERE PK_ORDER_HEADER=:NEW.FK_ORDER_HEADER;
SELECT PK_CODELST INTO V_ENTITY_CORD FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU';
SELECT PK_CODELST INTO V_ALIQUOTS FROM ER_CODELST WHERE CODELST_TYPE='cbu_sample_type' AND CODELST_SUBTYP='ALQT';

--dbms_output.put_line(':OLD.FK_SAMPLE_TYPE_AVAIL::'||:OLD.FK_SAMPLE_TYPE_AVAIL);
--dbms_output.put_line(':NEW.FK_SAMPLE_TYPE_AVAIL::'||:NEW.FK_SAMPLE_TYPE_AVAIL);

--dbms_output.put_line(':OLD.FK_ALIQUOTS_TYPE::'||:OLD.FK_ALIQUOTS_TYPE);
--dbms_output.put_line(':NEW.FK_ALIQUOTS_TYPE::'||:NEW.FK_ALIQUOTS_TYPE);

IF (:OLD.FK_SAMPLE_TYPE_AVAIL IS NULL OR :OLD.FK_SAMPLE_TYPE_AVAIL = V_ALIQUOTS) AND :NEW.FK_SAMPLE_TYPE_AVAIL IS NOT NULL AND :NEW.FK_SAMPLE_TYPE_AVAIL <> V_ALIQUOTS THEN
  SELECT CODELST_CUSTOM_COL1 INTO V_NEW_COLUMN_NAME FROM ER_CODELST WHERE PK_CODELST=:NEW.FK_SAMPLE_TYPE_AVAIL;
 -- DBMS_OUTPUT.PUT_LINE('UPDATE CB_ENTITY_SAMPLES SET '||v_new_column_name||'='|| v_new_column_name||'-1 WHERE ENTITY_ID='||V_CORDID||' AND ENTITY_TYPE='||V_ENTITY_CORD);
 EXECUTE IMMEDIATE 'UPDATE CB_ENTITY_SAMPLES SET '||v_new_column_name||'='|| v_new_column_name||'-1 WHERE ENTITY_ID='||V_CORDID||' AND ENTITY_TYPE='||V_ENTITY_CORD;
END IF;

IF :OLD.FK_SAMPLE_TYPE_AVAIL IS NOT NULL AND :OLD.FK_SAMPLE_TYPE_AVAIL <> V_ALIQUOTS AND :NEW.FK_SAMPLE_TYPE_AVAIL <> V_ALIQUOTS AND :NEW.FK_SAMPLE_TYPE_AVAIL IS NOT NULL THEN
  SELECT CODELST_CUSTOM_COL1 INTO V_OLD_COLUMN_NAME FROM ER_CODELST WHERE PK_CODELST=:OLD.FK_SAMPLE_TYPE_AVAIL;
  SELECT CODELST_CUSTOM_COL1 INTO V_NEW_COLUMN_NAME FROM ER_CODELST WHERE PK_CODELST=:NEW.FK_SAMPLE_TYPE_AVAIL;
  --DBMS_OUTPUT.PUT_LINE('UPDATE CB_ENTITY_SAMPLES SET '|| V_OLD_COLUMN_NAME || '=' || V_OLD_COLUMN_NAME || '+1 WHERE ENTITY_ID=' || V_CORDID || ' AND ENTITY_TYPE=' || V_ENTITY_CORD);
 EXECUTE IMMEDIATE 'UPDATE CB_ENTITY_SAMPLES SET ' || V_OLD_COLUMN_NAME || '=' || V_OLD_COLUMN_NAME || '+1 WHERE ENTITY_ID=' || V_CORDID || ' AND ENTITY_TYPE=' || V_ENTITY_CORD;
  --DBMS_OUTPUT.PUT_LINE('UPDATE CB_ENTITY_SAMPLES SET '|| V_NEW_COLUMN_NAME || '=' || V_NEW_COLUMN_NAME ||'-1 WHERE ENTITY_ID=' || V_CORDID || ' AND ENTITY_TYPE=' || V_ENTITY_CORD);
 EXECUTE IMMEDIATE 'UPDATE CB_ENTITY_SAMPLES SET '||v_new_column_name||'='|| v_new_column_name||'-1 WHERE ENTITY_ID='||V_CORDID||' AND ENTITY_TYPE='||V_ENTITY_CORD;
END IF;

IF :OLD.FK_SAMPLE_TYPE_AVAIL IS NOT NULL AND :OLD.FK_SAMPLE_TYPE_AVAIL <> V_ALIQUOTS AND :NEW.FK_SAMPLE_TYPE_AVAIL IS NOT NULL AND :NEW.FK_SAMPLE_TYPE_AVAIL = V_ALIQUOTS THEN
  SELECT CODELST_CUSTOM_COL1 INTO V_OLD_COLUMN_NAME FROM ER_CODELST WHERE PK_CODELST=:OLD.FK_SAMPLE_TYPE_AVAIL;
  --DBMS_OUTPUT.PUT_LINE('UPDATE CB_ENTITY_SAMPLES SET '|| V_OLD_COLUMN_NAME || '=' || V_OLD_COLUMN_NAME || '+1 WHERE ENTITY_ID=' || V_CORDID || ' AND ENTITY_TYPE=' || V_ENTITY_CORD);
 EXECUTE IMMEDIATE 'UPDATE CB_ENTITY_SAMPLES SET ' || V_OLD_COLUMN_NAME || '=' || V_OLD_COLUMN_NAME || '+1 WHERE ENTITY_ID=' || V_CORDID || ' AND ENTITY_TYPE=' || V_ENTITY_CORD;
END IF;



IF :OLD.FK_ALIQUOTS_TYPE IS NULL AND :NEW.FK_ALIQUOTS_TYPE IS NOT NULL THEN
  SELECT CODELST_CUSTOM_COL1 INTO V_NEW_COLUMN_NAME FROM ER_CODELST WHERE PK_CODELST=:NEW.FK_ALIQUOTS_TYPE;
  --DBMS_OUTPUT.PUT_LINE('UPDATE CB_ENTITY_SAMPLES SET '||v_new_column_name||'='|| v_new_column_name||'-1 WHERE ENTITY_ID='||V_CORDID||' AND ENTITY_TYPE='||V_ENTITY_CORD);
 EXECUTE IMMEDIATE 'UPDATE CB_ENTITY_SAMPLES SET '||v_new_column_name||'='|| v_new_column_name||'-1 WHERE ENTITY_ID='||V_CORDID||' AND ENTITY_TYPE='||V_ENTITY_CORD;
END IF;
IF :OLD.FK_ALIQUOTS_TYPE IS NOT NULL AND :NEW.FK_ALIQUOTS_TYPE IS NOT NULL THEN
  SELECT CODELST_CUSTOM_COL1 INTO V_OLD_COLUMN_NAME FROM ER_CODELST WHERE PK_CODELST=:OLD.FK_ALIQUOTS_TYPE;
  SELECT CODELST_CUSTOM_COL1 INTO V_NEW_COLUMN_NAME FROM ER_CODELST WHERE PK_CODELST=:NEW.FK_ALIQUOTS_TYPE;
  --DBMS_OUTPUT.PUT_LINE('UPDATE CB_ENTITY_SAMPLES SET '|| V_OLD_COLUMN_NAME || '=' || V_OLD_COLUMN_NAME || '+1 WHERE ENTITY_ID=' || V_CORDID || ' AND ENTITY_TYPE=' || V_ENTITY_CORD);
 EXECUTE IMMEDIATE 'UPDATE CB_ENTITY_SAMPLES SET ' || V_OLD_COLUMN_NAME || '=' || V_OLD_COLUMN_NAME || '+1 WHERE ENTITY_ID=' || V_CORDID || ' AND ENTITY_TYPE=' || V_ENTITY_CORD;
  --DBMS_OUTPUT.PUT_LINE('UPDATE CB_ENTITY_SAMPLES SET '|| V_NEW_COLUMN_NAME || '=' || V_NEW_COLUMN_NAME ||'-1 WHERE ENTITY_ID=' || V_CORDID || ' AND ENTITY_TYPE=' || V_ENTITY_CORD);
 EXECUTE IMMEDIATE 'UPDATE CB_ENTITY_SAMPLES SET '||v_new_column_name||'='|| v_new_column_name||'-1 WHERE ENTITY_ID='||V_CORDID||' AND ENTITY_TYPE='||V_ENTITY_CORD;
END IF;
IF :OLD.FK_ALIQUOTS_TYPE IS NOT NULL AND :NEW.FK_ALIQUOTS_TYPE IS NULL THEN
  SELECT CODELST_CUSTOM_COL1 INTO V_OLD_COLUMN_NAME FROM ER_CODELST WHERE PK_CODELST=:OLD.FK_ALIQUOTS_TYPE;
  --DBMS_OUTPUT.PUT_LINE('UPDATE CB_ENTITY_SAMPLES SET '|| V_OLD_COLUMN_NAME || '=' || V_OLD_COLUMN_NAME || '+1 WHERE ENTITY_ID=' || V_CORDID || ' AND ENTITY_TYPE=' || V_ENTITY_CORD);
 EXECUTE IMMEDIATE 'UPDATE CB_ENTITY_SAMPLES SET ' || V_OLD_COLUMN_NAME || '=' || V_OLD_COLUMN_NAME || '+1 WHERE ENTITY_ID=' || V_CORDID || ' AND ENTITY_TYPE=' || V_ENTITY_CORD;
END IF;
END;
/


create or replace
TRIGGER ER_ORDER_SERVICES_ALERT
AFTER INSERT OR UPDATE ON ERES.ER_ORDER_SERVICES
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN
INSERT INTO CB_ALERTS_MAINTENANCE(ENTITY_ID,ENTITY_TYPE) VALUES(:NEW.FK_ORDER_ID,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='ORDER'));
END;
/

create or replace
TRIGGER TASK_ENTRY AFTER UPDATE OF TASK_COMPLETEFLAG ON TASK REFERENCING OLD AS 

OLDVAL NEW AS NEWVAL
FOR EACH ROW
DECLARE
FK_ACTIVITY NUMBER;
VAR_ORDTYPE VARCHAR2(10);
BEGIN         
          IF (:NEWVAL.TASK_COMPLETEFLAG = 1) THEN       
            SELECT PK_CODELST INTO VAR_ORDTYPE FROM ER_CODELST WHERE 

CODELST_TYPE='entity_type' and CODELST_SUBTYP='ORDER';
            SELECT WRKFLW_ACT.FK_ACTIVITYID INTO FK_ACTIVITY FROM 

WORKFLOW_ACTIVITY WRKFLW_ACT WHERE 

WRKFLW_ACT.PK_WORKFLOWACTIVITY=:NEWVAL.FK_WORKFLOWACTIVITY;
            INSERT INTO 

CB_ENTITY_STATUS(PK_ENTITY_STATUS,ENTITY_ID,FK_ENTITY_TYPE,STATUS_TYPE_CODE,FK_

STATUS_VALUE,STATUS_DATE,STATUS_REMARKS,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED

_BY,LAST_MODIFIED_DATE,RID,DELETEDFLAG,LICN_ELIG_CHANGE_REASON)VALUES 

(SEQ_CB_ENTITY_STATUS.nextval,:NEWVAL.FK_ENTITY,VAR_ORDTYPE,'task_status',FK_AC

TIVITY,SYSDATE,NULL,:NEWVAL.TASK_USERORROLEID,SYSDATE,NULL,NULL,NULL,NULL,NULL,

NULL);
          END IF;
          EXCEPTION
          WHEN NO_DATA_FOUND THEN
          dbms_output.put_line ('INSIDE EXCEPTION...........DATA NOT FIND');
END;
/


