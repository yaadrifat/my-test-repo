DECLARE
  countFlage NUMBER(5);
BEGIN
  Select count(*) into countFlage from user_tab_cols  where TABLE_NAME = 'ER_LABTEST'  AND column_name = 'LABTEST_HOWER';
   if (countFlage = 0) then
 execute immediate ('alter table ER_LABTEST add LABTEST_HOWER VARCHAR2(500 BYTE)');
  end if;
end;
/
 COMMENT ON COLUMN ER_LABTEST.LABTEST_HOWER IS 'Hower Description for the Labtest Name'; 
commit;


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME ='CBU volume (without anticoagulant/additives)'
    AND LABTEST_SHORTNAME = 'CBU_VOL';
  if (v_record_exists = 1) then
      update  ER_LABTEST set LABTEST_HOWER ='Actual volume, as reported by the CBB, of cord blood without anticoagulants or other additives.The minimum value is 40.0 ml, the maximum value is 500.0 ml.'
 where LABTEST_NAME ='CBU volume (without anticoagulant/additives)'
    AND LABTEST_SHORTNAME = 'CBU_VOL';
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME ='Total CBU Nucleated Cell Count'
    AND LABTEST_SHORTNAME = 'TCNCC';
  if (v_record_exists = 1) then
      update  ER_LABTEST set LABTEST_HOWER ='Total CBU Nucleated Cell Count at the start of processing.  The minimum value is 12 x 10^7, the maximum value is 999 x 10^7.'
 where LABTEST_NAME ='Total CBU Nucleated Cell Count'
    AND LABTEST_SHORTNAME = 'TCNCC';
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME ='CBU Nucleated Cell Count Concentration'
    AND LABTEST_SHORTNAME = 'CBUNCCC';
  if (v_record_exists = 1) then
      update  ER_LABTEST set LABTEST_HOWER ='CBU Nucleated Cell Count Concentration at the start of processing.  The minimum value is 1000 x 10^3/ml, the maximum value is 50000 x 10^3/ml./This is the concentration prior to the addition of DMSO, the final count.  The minimum value is 5000 x 10^3/ml, the maximum value is 200000 x 10^3/ml.'
 where LABTEST_NAME ='CBU Nucleated Cell Count Concentration'
    AND LABTEST_SHORTNAME = 'CBUNCCC';
	commit;
  end if;
end;
/



DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME ='Final Product Volume'
    AND LABTEST_SHORTNAME = 'FNPV';
  if (v_record_exists = 1) then
      update  ER_LABTEST set LABTEST_HOWER ='Total CBU volume post processing / pre-cryopreservation, including DMSO. The minimum value is 10.0 ml, the maximum value is 600.0 ml.'
 where LABTEST_NAME ='Final Product Volume'
    AND LABTEST_SHORTNAME = 'FNPV';
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME ='Total CBU Nucleated Cell Count (unknown if uncorrected)'
    AND LABTEST_SHORTNAME = 'TCBUUN';
  if (v_record_exists = 1) then
      update  ER_LABTEST set LABTEST_HOWER ='The cryopreserved CBUs number of nucleated cells. Unknown if corrected or uncorrected for nucleated red blood cells (nRBC). The minimum value is 12 x 10^7, the maximum value is 999 x 10^7.'
 where LABTEST_NAME ='Total CBU Nucleated Cell Count (unknown if uncorrected)'
    AND LABTEST_SHORTNAME = 'TCBUUN';
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME ='Total CBU Nucleated Cell Count (uncorrected)'
    AND LABTEST_SHORTNAME = 'UNCRCT';
  if (v_record_exists = 1) then
      update  ER_LABTEST set LABTEST_HOWER ='The cryopreserved CBUs number of nucleated cells uncorrected for nucleated red blood cells (nRBC).  The minimum value is 12 x 10^7, the maximum value is 999 x 10^7.'
 where LABTEST_NAME ='Total CBU Nucleated Cell Count (uncorrected)'
    AND LABTEST_SHORTNAME = 'UNCRCT';
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME ='Total CD34+ Cell Count'
    AND LABTEST_SHORTNAME = 'TCDAD';
  if (v_record_exists = 1) then
      update  ER_LABTEST set LABTEST_HOWER ='The total of CD34+ cells in the frozen CBU.'
 where LABTEST_NAME ='Total CD34+ Cell Count'
    AND LABTEST_SHORTNAME = 'TCDAD';
	commit;
  end if;
end;
/



DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME ='% of CD34+ Cells in Total Monunuclear Cell Count'
    AND LABTEST_SHORTNAME = 'PERTMON';
  if (v_record_exists = 1) then
      update  ER_LABTEST set LABTEST_HOWER ='This field multiplied by % of Mononuclear Cell in Total Nucleated Cell Count and multiplied by Total CBU Nucleated Cell Count  should equal the Total CD34 Cell Count.'
 where LABTEST_NAME ='% of CD34+ Cells in Total Monunuclear Cell Count'
    AND LABTEST_SHORTNAME = 'PERTMON';
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME =' % of Mononuclear Cells in Total Nucleated Cell Count'
    AND LABTEST_SHORTNAME = 'PERTNUC';
  if (v_record_exists = 1) then
      update  ER_LABTEST set LABTEST_HOWER ='This field multiplied by Total CBU Nucleated Cell Countmultiplied by % of CD34 Cells in Total Mononuclear Cell Count should equal the Total CD34 Count.'
      where LABTEST_NAME =' % of Mononuclear Cells in Total Nucleated Cell Count'
    AND LABTEST_SHORTNAME = 'PERTNUC';
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME ='% CD3 Cells in Total Mononuclear Cell Count'
    AND LABTEST_SHORTNAME = 'PERCD3';
  if (v_record_exists = 1) then
      update  ER_LABTEST set LABTEST_HOWER ='This field multiplied by Total CBU Nucleated Cell Count multiplied by % of Monunuclear Cells in the Total ucleated Cell Count should equal the Total CD3 Count'
 where LABTEST_NAME ='% CD3 Cells in Total Mononuclear Cell Count'
    AND LABTEST_SHORTNAME = 'PERCD3';
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME ='CBU nRBC Absolute Number'
    AND LABTEST_SHORTNAME = 'CNRBC';
  if (v_record_exists = 1) then
      update  ER_LABTEST set LABTEST_HOWER ='The number of nucleated red blood cells at post processing.  The minimum value is 0, the maximum value is 9999.'
 where LABTEST_NAME ='CBU nRBC Absolute Number'
    AND LABTEST_SHORTNAME = 'CNRBC';
	commit;
  end if;
end;
/



DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME ='CBU nRBC %'
    AND LABTEST_SHORTNAME = 'PERNRBC';
  if (v_record_exists = 1) then
      update  ER_LABTEST set LABTEST_HOWER ='This is the CBU nRBC % post-processing.Either CBU nRBC Absolute Number or % is required. If CBU nRBC % is not entered, it will be calculated using CBU Post Processing absolute number and Total CBU Nucleated Cell Count (Uncorrected) fields.'
 where LABTEST_NAME ='CBU nRBC %'
    AND LABTEST_SHORTNAME = 'PERNRBC';
	commit;
  end if;
end;
/


