create or replace
function f_get_new_orders_count(v_param1 number, v_order_type number, v_sample_at_lab varchar2, v_all_flag varchar2) return number as
v_new_count number;
begin
v_new_count:=0;
if(v_all_flag = 'User') then
    SELECT COUNT(ord.order_status) into v_new_count
      FROM er_order ord,
        er_order_header ordhdr,
        cb_cord crd
      WHERE ord.fk_order_header=ordhdr.pk_order_header
      AND crd.fk_cbb_id       IN
        (SELECT us.fk_site
        FROM cbb c,
          er_usersite us,
          er_site s
        WHERE c.using_cdr     ='1'
        AND us.fk_user        =v_param1
        AND us.usersite_right<>0
        AND c.fk_site         =s.pk_site
        AND us.fk_site        =s.pk_site
        )
      AND ordhdr.order_entityid=crd.pk_cord
      and ord.order_type= v_order_type
      GROUP BY ord.order_type,
        ord.order_status
      HAVING f_codelst_desc(order_status)='New';
  end if;
  if(v_all_flag='All') then
  
      SELECT COUNT(ord.order_status) into v_new_count
      FROM er_order ord,
        er_order_header ordhdr,
        cb_cord crd
      WHERE ord.fk_order_header=ordhdr.pk_order_header
      AND crd.fk_cbb_id       IN (SELECT us.fk_site
                                  FROM cbb c,
                                    er_usersite us,
                                    er_site s
                                  WHERE c.using_cdr     ='1'
                                  AND us.fk_user        =v_param1
                                  AND us.usersite_right<>0
                                  AND c.fk_site         =s.pk_site
                                  AND us.fk_site        =s.pk_site)
      AND ordhdr.order_entityid=crd.pk_cord
      and ord.order_type= v_order_type
      and (ord.order_sample_at_lab = v_sample_at_lab)
      GROUP BY ord.order_type,
        ord.order_status
      HAVING f_codelst_desc(order_status)='New';
end if;
if(v_all_flag='Site') then
      SELECT COUNT(ord.order_status) into v_new_count
      FROM er_order ord,
        er_order_header ordhdr,
        cb_cord crd
      WHERE ord.fk_order_header=ordhdr.pk_order_header
      AND crd.fk_cbb_id       IN (v_param1)
      AND ordhdr.order_entityid=crd.pk_cord
      and ord.order_type= v_order_type
      and (ord.order_sample_at_lab = v_sample_at_lab)
      GROUP BY ord.order_type,
        ord.order_status
      HAVING f_codelst_desc(order_status)='New';
  
end if;
return v_new_count;
end;
/