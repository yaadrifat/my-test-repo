/* This readMe is specific to Velos eResearch version 9.0 build637-et054 */

=====================================================================================================================================
Steps to deploy velos.ear:

1. Remove the existing velos.ear forlder from loaction \server\eresearch\deploy
2. Put the latest velos.ear to the same location.
3. Execute dbScript files and 1 xsl directory with 3 files 


DB Script files:
		eres:
            			1. 00_er_version.sql
				2. 04_SP_CRI_TASKS.sql
				3. 05_ER_REPXSL.SQL
				4. 06_F_LABSUMMARY_COMPL_CHECK.sql
					
		epat:
				5. 00_er_version.sql
						
		esch: 
				6. 00_er_version.sql
						
		

XSL:
		1.  185.xsl
		2.  GENREPXSL.ctl
		3.  loadxsl.bat				

Note: Please execute scripts on database server.

 
 