set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Table' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Table','ER_ORDER_RECEIPANT_INFO','Order Recipient Information');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.PK_ORDER_RECEIPANT_INFO';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.PK_ORDER_RECEIPANT_INFO','Order Recipient Information Id');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FK_RECEIPANT';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FK_RECEIPANT','Recipient Id');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FK_ORDER_ID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FK_ORDER_ID','Order Id');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FK_CORD_ID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FK_CORD_ID','Cord Id');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.IND_PROTOCOL';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.IND_PROTOCOL','Will this shipment be facilitated under NMDP IND Protocol 10-CBA?');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.IND_SPONSER';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.IND_SPONSER','IND Sponsor');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.IND_NUMBER';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.IND_NUMBER','IND Number');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.CURRENT_DIAGNOSIS';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.CURRENT_DIAGNOSIS','Current Diagnosis');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.DISEASE_STAGE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.DISEASE_STAGE','Disease Stage (AML, ALL, or other leukemia ONLY)');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FK_TRANSPLANT_TYPE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FK_TRANSPLANT_TYPE','Type of Transplant');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FK_LEU_CURR_STATUS';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FK_LEU_CURR_STATUS','Current Status of Leukemia (CML ONLY)');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.REC_TRANSFUSED';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.REC_TRANSFUSED','Recipient Transfused (Severe Aplastic Anemia ONLY)?');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.PROP_INFUSION_DATE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.PROP_INFUSION_DATE','Proposed Infusion Date');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.PROP_PREP_DATE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.PROP_PREP_DATE','Proposed Prep Date');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.RES_CEL_SAM_FLAG';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.RES_CEL_SAM_FLAG','Residual Cellular Samples Needed?');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.CREATOR';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.CREATOR','Creator');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.CREATED_ON';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.CREATED_ON','Create On');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.IP_ADD';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.IP_ADD','IP Address');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.LAST_MODIFIED_BY';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.LAST_MODIFIED_BY','Last Modified By');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.LAST_MODIFIED_DATE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.LAST_MODIFIED_DATE','Last Modified Date');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.RID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.RID','Rid');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.NO_OF_REMISSIONS';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.NO_OF_REMISSIONS','Number of Remissions (only for AML, ALL, or other leukemia)');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FK_RECEIPANT_CONTACT1';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FK_RECEIPANT_CONTACT1','Contact Name 1');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FK_RECEIPANT_CONTACT2';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FK_RECEIPANT_CONTACT2','Contact Name 2');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FK_RECEIPANT_CONTACT3';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FK_RECEIPANT_CONTACT3','Contact Name 3');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.ORDER_PHYSICIAN';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.ORDER_PHYSICIAN','Ordering Physician');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.REC_WEIGHT';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.REC_WEIGHT','Recipient Weight');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FK_SITE_ID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FK_SITE_ID','CBB Name');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FK_CONTACT_PERSON';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FK_CONTACT_PERSON','Contact Person');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.DATE_TC_COMPLETED';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.DATE_TC_COMPLETED','TC Completed');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.EX_VIVO_TRANSPLANT';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.EX_VIVO_TRANSPLANT','Ex Vivo Expansion Transplant');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.MULTI_TRANSPLANT';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.MULTI_TRANSPLANT','Multiple Unit Transplant');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.OTHER_TRANSPLANT';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.OTHER_TRANSPLANT','Other Non-Traditional Transplant');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.SINGLE_UNIT_TRANSPLANT';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.SINGLE_UNIT_TRANSPLANT','Single Unit Transplant');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.TC_CDE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.TC_CDE','TC CDE');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.PRODUCT_DELIVERY_TC_NAME';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.PRODUCT_DELIVERY_TC_NAME','Delivery TC Name');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.PROPOSED_SHIP_DTE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.PROPOSED_SHIP_DTE','Proposed Ship Date');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.COMPLETE_ON_BEHALF';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.COMPLETE_ON_BEHALF','Completed On Behalf');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.COOP_REF';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.COOP_REF','Coop Ref');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.FORM_COMPLETED_BY';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.FORM_COMPLETED_BY','Form Completed By');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.LOCAL_ID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.LOCAL_ID','Local Id');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.NMDP_ID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.NMDP_ID','NMDP Id');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.NMDP_RID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.NMDP_RID','NMDP Rid');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.OTH_DESCRIBE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.OTH_DESCRIBE','Other Describe');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.OTH_SPECIFY';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.OTH_SPECIFY','Other Specify');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.SCU_ACTIVATED_DATE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.SCU_ACTIVATED_DATE','Activated Date');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'ER_ORDER_RECEIPANT_INFO.PRIORITY';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','ER_ORDER_RECEIPANT_INFO.PRIORITY','Priority');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Table' and FROM_VALUE = 'CB_ADDITIONAL_IDS';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Table','CB_ADDITIONAL_IDS','Additional Ids');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ADDITIONAL_IDS.PK_CB_ADDITIONAL_IDS';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ADDITIONAL_IDS.PK_CB_ADDITIONAL_IDS','');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ADDITIONAL_IDS.CB_ADDITIONAL_ID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ADDITIONAL_IDS.CB_ADDITIONAL_ID','Additional ID');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ADDITIONAL_IDS.FK_CB_ADDITIONAL_ID_TYPE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ADDITIONAL_IDS.FK_CB_ADDITIONAL_ID_TYPE','ID Type');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ADDITIONAL_IDS.CB_ADDITIONAL_ID_DESC';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ADDITIONAL_IDS.CB_ADDITIONAL_ID_DESC','ID Description');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ADDITIONAL_IDS.CREATED_ON';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ADDITIONAL_IDS.CREATED_ON','Created On');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ADDITIONAL_IDS.CREATOR';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ADDITIONAL_IDS.CREATOR','Created By');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ADDITIONAL_IDS.IP_ADD';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ADDITIONAL_IDS.IP_ADD','Ip Address');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ADDITIONAL_IDS.RID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ADDITIONAL_IDS.RID','');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ADDITIONAL_IDS.LAST_MODIFIED_BY';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ADDITIONAL_IDS.LAST_MODIFIED_BY','Last Modified By');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ADDITIONAL_IDS.LAST_MODIFIED_DATE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ADDITIONAL_IDS.LAST_MODIFIED_DATE','Last Modified Date');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ADDITIONAL_IDS.ENTITY_ID';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ADDITIONAL_IDS.ENTITY_ID','Entity Id');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ADDITIONAL_IDS.ENTITY_TYPE';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ADDITIONAL_IDS.ENTITY_TYPE','Entity Type');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_ADDITIONAL_IDS.DELETEDFLAG';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_ADDITIONAL_IDS.DELETEDFLAG','');
	commit;
  end if;
end;
/
