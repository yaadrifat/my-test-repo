--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='CC' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'CC','Unknown Code','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='CC' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='HE' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'HE','HE Requested','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='HE' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='CT' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'CT','CT Requested','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='CT' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='OR' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'OR','OR Requested','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='OR' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='DD' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'DD','Permanently unavailable Medically Deferred (DD)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='DD' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='AG' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'AG','Aliquots Gone (AG)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='AG' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='OT' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'OT','Cord Unavailable - Other Reasons (OT)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='OT' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='RO' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'RO','Reserved for Other (RO)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='RO' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='RSN' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'RSN','Reserved for NMDP','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='RSN' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='NA' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'NA','Not Available','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='NA' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='CD' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'CD','Cord Destroyed or Damaged (CD)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='CD' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='SH' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'SH','Shipped','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='SH' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='AV' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'AV','Available','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='AV' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='AC' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'AC','Active','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='AC' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='RCT' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'RCT','Repeat CT','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='RCT' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='RHE' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'RHE','Repeat HE','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='RHE' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='CA' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'CA','Cancel Request (CA)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='CA' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='CB' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'CB','Infused Date Passed (CB)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='CB' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='DT' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'DT','Discrepant Typing (DT)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='DT' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='DS' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'DS','Damaged in Transit (DS)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='DS' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='DU' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'DU','Discrepant Unit (DU)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='DU' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='IN' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'IN','Cord Infused (IN)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='IN' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='LS' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'LS','Lost in Transit (LS)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='LS' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='PC' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'PC','Patient Condition Changed, No Infusion (PC)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='PC' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='PD' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'PD','Patient Died, No Infusion (PD)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='PD' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='SA' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'SA','Patient Not Ready to Proceed (SA)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='SA' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='SB' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'SB','Different Cord Chosen (SB)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='SB' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--



--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='SC' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'SC','Cord Incompatible (SC)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='SC' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='SD' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'SD','Patient Died (SD)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='SD' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='SE' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'SE','Other Cell Source Chosen (SE)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='SE' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='TC' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'TC','TC Decided Against Infusion (TC)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='TC' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='TS' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'TS','Thawed in Transit (TS)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='TS' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='XP' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'XP','Cord Expired (XP)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='XP' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='SO' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'SO','Shipped for Other (SO)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='SO' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='QR' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'QR','Cord quarantined (QR)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='QR' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS
    where CBU_STATUS_CODE='NR' AND CODE_TYPE='CloseReason';
  if (v_record_exists = 0) then
      INSERT INTO CB_CBU_STATUS(PK_CBU_STATUS,CBU_STATUS_CODE,CBU_STATUS_DESC,CODE_TYPE,FK_CBU_STATUS) VALUES(SEQ_CB_CBU_STATUS.nextval,'NR','No Results Received - lab has not reported results in 30 days (NR)','CloseReason',(SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE='NR' AND CODE_TYPE='Resolution'));
	commit;
  end if;
end;
/
--END--
