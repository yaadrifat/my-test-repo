set define off;
update audit_mappedvalues set to_value= 'Resolution CBB Code' where mapping_type = 'Field' and from_value = 'ER_ORDER.FK_ORDER_RESOL_BY_CBB';
update audit_mappedvalues set to_value= 'Resolution TC Code' where mapping_type = 'Field' and from_value = 'ER_ORDER.FK_ORDER_RESOL_BY_TC';
update audit_mappedvalues set to_value= 'Complete Required Information' where mapping_type = 'Field' and from_value = 'ER_ORDER.COMPLETE_REQ_INFO_TASK_FLAG';
update audit_mappedvalues set to_value= 'NMDP''s minimum criteria to ship a CBU and confirm' where mapping_type = 'Field' and from_value = 'ER_ORDER.COMPLETE_REQ_INFO_TASK_FLAG';

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_NOTES.NOTES_PROGRESS';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_NOTES.NOTES_PROGRESS','Progress Note');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_NOTES.NOTES_CLINIC';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_NOTES.NOTES_CLINIC','Clinical Note');
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_CORD_MINIMUM_CRITERIA.DECLARATION_RESULT';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_CORD_MINIMUM_CRITERIA.DECLARATION_RESULT','NMDP''s minimum criteria to ship a CBU and confirm');
	commit;
  end if;
end;
/

commit;