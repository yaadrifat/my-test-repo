DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where CODELST_TYPE='lab_sum_cat'
	 AND CODELST_SUBTYP = 'product_test';
  if (v_record_exists = 1) then
	delete from ER_CODELST where CODELST_TYPE='lab_sum_cat' and CODELST_SUBTYP='product_test';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where CODELST_TYPE='lab_sum_cat'
	 AND CODELST_SUBTYP = 'sample_test';
  if (v_record_exists = 1) then
	delete from ER_CODELST where CODELST_TYPE='lab_sum_cat' and CODELST_SUBTYP='sample_test';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where CODELST_TYPE='lab_sum_cat'
	 AND CODELST_SUBTYP = 'other';
  if (v_record_exists = 1) then
	delete from ER_CODELST where CODELST_TYPE='lab_sum_cat' and CODELST_SUBTYP='other';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where CODELST_TYPE='eligibile_cat'
	 AND CODELST_SUBTYP = 'attach';
  if (v_record_exists = 1) then
	delete from ER_CODELST where CODELST_TYPE='eligibile_cat' and CODELST_SUBTYP='attach';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where CODELST_TYPE='eligibile_cat'
	 AND CODELST_SUBTYP = 'mrq_attach';
  if (v_record_exists = 1) then
	delete from ER_CODELST where CODELST_TYPE='eligibile_cat' and CODELST_SUBTYP='mrq_attach';
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where CODELST_TYPE='other_rec_cat'
	 AND CODELST_SUBTYP = 'other';
  if (v_record_exists = 1) then
	delete from ER_CODELST where CODELST_TYPE='other_rec_cat' and CODELST_SUBTYP='other';
	commit;
  end if;
end;
/



