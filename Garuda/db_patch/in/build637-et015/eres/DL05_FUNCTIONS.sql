create or replace
FUNCTION F_GET_FORM_TYP_DT(PK_CORD NUMBER, FRM_TYPE VARCHAR2) RETURN  SYS_REFCURSOR IS
FRM_DT_TYPE SYS_REFCURSOR;
BEGIN 

OPEN FRM_DT_TYPE FOR 
SELECT TO_CHAR(DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') DATEFILLED,
  F_CODELST_DESC(ENTITY_TYPE) TYPE_FRM
FROM CB_FORM_VERSION
WHERE PK_FORM_VERSION IN
  (SELECT MAX(PK_FORM_VERSION)
  FROM cb_form_version frmver
  WHERE frmver.entity_id=PK_CORD
  AND FRMVER.FK_FORM    =
    (SELECT MAX(pk_form)
    FROM cb_forms
    WHERE CB_FORMS.IS_CURRENT_FLAG='1'
    AND CB_FORMS.FORMS_DESC       =FRM_TYPE
    )
  );

RETURN FRM_DT_TYPE;
END;
/

create or replace
function F_GET_CODELSTVAL_FRM_TYP(TYPEVAL varchar2) return SYS_REFCURSOR IS
CODELST_DATA SYS_REFCURSOR;
BEGIN
    OPEN CODELST_DATA FOR       
                          SELECT PK_CODELST PK_CODELST,CODELST_DESC CODELST_DESC_VAL FROM ER_CODELST WHERE CODELST_TYPE IN ( typeval ) ORDER BY CODELST_SEQ;
RETURN CODELST_DATA;
END;
/



create or replace
function f_lab_summary(pk_cordid number) return SYS_REFCURSOR
IS
p_lab_refcur SYS_REFCURSOR;
v_specimen number;
begin
select fk_specimen_id into v_specimen from cb_cord where pk_cord=pk_cordid;
OPEN p_lab_refcur FOR select a.pk_labtest, a.labtest_name, a.labtest_seq
,(select  test_result from er_patlabs  where   fk_timing_of_test = f_codelst_id('timing_of_test','pre_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) pre_test_result
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where fk_timing_of_test = f_codelst_id('timing_of_test','pre_procesing') and fk_testid = a.pk_labtest and fk_specimen = v_specimen ) pre_test_date
,(select max(test_result) from er_patlabs  where  pk_patlabs=(select max(pk_patlabs) from er_patlabs where fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) post_test_result
,(select to_char(test_date,'Mon DD,YYYY') from er_patlabs where pk_patlabs=(select max(pk_patlabs) from er_patlabs where fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) post_test_date
, f_codelst_desc((select fk_test_method from er_patlabs  where   pk_patlabs=(select max(pk_patlabs) from er_patlabs where fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen))) post_testmeth
,(select dbms_lob.substr( notes, 4000, 1 ) from er_patlabs  where   pk_patlabs=(select max(pk_patlabs) from er_patlabs where fk_timing_of_test = f_codelst_id('timing_of_test','post_procesing') and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) post_test_desc
,(select  test_result from er_patlabs  where pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) post_cryopres_thaw
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) post_cryopres_date
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen))) reason_thaw
, (select  custom004 from er_patlabs  where   pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) reason_desc_thaw
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where  pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen))) smpltype_thaw
, f_codelst_desc((select  fk_test_method from er_patlabs  where   pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen))) testmeth_thaw
, (select  custom005 from er_patlabs  where   pk_patlabs=(select max(pk_patlabs) from er_patlabs where custom006 = 'post_proc_thaw' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) test_desc_thaw
,(select  test_result from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) post_cryopres_thaw1
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '1' and fk_testid = a.pk_labtest and fk_specimen = v_specimen) post_cryopres_date1
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) reason_thaw1
, (select  custom004 from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) reason_desc_thaw1
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) smpltype_thaw1
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) testmeth_thaw1
, (select  custom005 from er_patlabs  where   custom006 = '1' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) test_desc_thaw1
,(select  test_result from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) post_cryopres_thaw2
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '2' and fk_testid = a.pk_labtest and fk_specimen = v_specimen) post_cryopres_date2
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) reason_thaw2
, (select  custom004 from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) reason_desc_thaw2
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) smpltype_thaw2
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) testmeth_thaw2
, (select  custom005 from er_patlabs  where   custom006 = '2' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) test_desc_thaw2
,(select  test_result from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) post_cryopres_thaw3
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '3' and fk_testid = a.pk_labtest and fk_specimen = v_specimen) post_cryopres_date3
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) reason_thaw3
, (select  custom004 from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) reason_desc_thaw3
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) smpltype_thaw3
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) testmeth_thaw3
, (select  custom005 from er_patlabs  where   custom006 = '3' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) test_desc_thaw3
,(select  test_result from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) post_cryopres_thaw4
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '4' and fk_testid = a.pk_labtest and fk_specimen = v_specimen) post_cryopres_date4
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) reason_thaw4
, (select  custom004 from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) reason_desc_thaw4
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) smpltype_thaw4
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) testmeth_thaw4
, (select  custom005 from er_patlabs  where   custom006 = '4' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) test_desc_thaw4
,(select  test_result from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) post_cryopres_thaw5
,(select  to_char(test_date,'Mon DD,YYYY') from er_patlabs where custom006 = '5' and fk_testid = a.pk_labtest and fk_specimen = v_specimen) post_cryopres_date5
, f_codelst_desc((select  fk_test_reason from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) reason_thaw5
, (select  custom004 from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) reason_desc_thaw5
, f_codelst_desc((select  fk_test_specimen from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) smpltype_thaw5
, f_codelst_desc((select  fk_test_method from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen)) testmeth_thaw5
, (select  custom005 from er_patlabs  where   custom006 = '5' and   fk_testid = a.pk_labtest and   fk_specimen = v_specimen) test_desc_thaw5
from er_labtest a,er_labtestgrp lgrp,er_labgroup grp where lgrp.fk_labgroup=grp.pk_labgroup and lgrp.fk_testid=a.pk_labtest and grp.group_name='PrcsGrp' and grp.group_type='PC' order by labtest_seq; 
RETURN p_lab_refcur;
end;  
/


COMMIT;

