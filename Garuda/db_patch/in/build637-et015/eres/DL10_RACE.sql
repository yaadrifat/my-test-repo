--STARTS DELETING RECORDS FROM ER_CODELST TABLE WHERE CODELST_TYPE ='race'--
DELETE FROM ER_CODELST WHERE CODELST_TYPE ='race';
COMMIT;
--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'AFB';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','AFB','African','N',NULL,SYSDATE,SYSDATE,NULL,12);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'AAFA';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','AAFA','African American','N',NULL,SYSDATE,SYSDATE,NULL,13);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'ALANAM';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','ALANAM','Alaskan Native/Aleut','N',NULL,SYSDATE,SYSDATE,NULL,1);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'AMIND';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','AMIND','American Indian','N',NULL,SYSDATE,SYSDATE,NULL,2);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'AISC';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','AISC','American Indian So. Or Central','N',NULL,SYSDATE,SYSDATE,NULL,3);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'SCAMB';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','SCAMB','Black So. Or Central American','N',NULL,SYSDATE,SYSDATE,NULL,14);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'CARB';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','CARB','Caribbean Black','N',NULL,SYSDATE,SYSDATE,NULL,15);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'CARIBI';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','CARIBI','Caribbean Indian','N',NULL,SYSDATE,SYSDATE,NULL,4);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'NCHI';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','NCHI','Chinese','N',NULL,SYSDATE,SYSDATE,NULL,5);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'DEC';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','DEC','Declines/No Race Selected','N',NULL,SYSDATE,SYSDATE,NULL,16);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'EEURO';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','EEURO','Eastern European','N',NULL,SYSDATE,SYSDATE,NULL,21);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'FILII';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','FILII','Filipino (Pilipino)','N',NULL,SYSDATE,SYSDATE,NULL,6);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'GUAMAN';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','GUAMAN','Guamanian','N',NULL,SYSDATE,SYSDATE,NULL,17);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'HAWAII';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','HAWAII','Hawaiian','N',NULL,SYSDATE,SYSDATE,NULL,18);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'JAPI';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','JAPI','Japanese','N',NULL,SYSDATE,SYSDATE,NULL,7);
	commit;
  end if;
end;
/
--END--



--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'KORI';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','KORI','Korean','N',NULL,SYSDATE,SYSDATE,NULL,8);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'MEDIT';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','MEDIT','Mediterranean','N',NULL,SYSDATE,SYSDATE,NULL,22);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'MIDEAS';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','MIDEAS','Middle Eastern','N',NULL,SYSDATE,SYSDATE,NULL,23);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'NAMER';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','NAMER','North American','N',NULL,SYSDATE,SYSDATE,NULL,24);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'NCAFRI';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','NCAFRI','North Coast of Africa','N',NULL,SYSDATE,SYSDATE,NULL,25);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'NEURO';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','NEURO','Northern European','N',NULL,SYSDATE,SYSDATE,NULL,26);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'OPI';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','OPI','Other Pacific Islander','N',NULL,SYSDATE,SYSDATE,NULL,19);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'CAU';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','CAU','Other White','N',NULL,SYSDATE,SYSDATE,NULL,27);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'SAMOAN';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','SAMOAN','Samoan','N',NULL,SYSDATE,SYSDATE,NULL,20);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'AINDI';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','AINDI','South Asian','N',NULL,SYSDATE,SYSDATE,NULL,9);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'SCSEAI';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','SCSEAI','Southease Asian','N',NULL,SYSDATE,SYSDATE,NULL,10);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'VIET';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','VIET','Vietnamese','N',NULL,SYSDATE,SYSDATE,NULL,11);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'WEURO';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','WEURO','Western European','N',NULL,SYSDATE,SYSDATE,NULL,28);
	commit;
  end if;
end;
/

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'WCARIB';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','WCARIB','White Caribbean','N',NULL,SYSDATE,SYSDATE,NULL,29);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'WSCA';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','WSCA','White So. or Central American','N',NULL,SYSDATE,SYSDATE,NULL,30);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'AFA';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','AFA','Unspecified Black','Y',NULL,SYSDATE,SYSDATE,NULL,31);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'API';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','API','Unspec. Asian/Pacific Islndr','Y',NULL,SYSDATE,SYSDATE,NULL,32);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'CARHIS';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','CARHIS','Caribbean Hispanic','Y',NULL,SYSDATE,SYSDATE,NULL,33);
	commit;
  end if;
end;
/

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'EURWRC';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','EURWRC','North American or European','Y',NULL,SYSDATE,SYSDATE,NULL,34);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'HAWI';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','HAWI','Hawaiian or Pacific Islander','Y',NULL,SYSDATE,SYSDATE,NULL,35);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'HIS';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','HIS','Unspecified Hispanic','Y',NULL,SYSDATE,SYSDATE,NULL,36);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'MENAFC';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','MENAFC','MidEast/No. Coast of Africa','Y',NULL,SYSDATE,SYSDATE,NULL,37);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'MSWHIS';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','MSWHIS','Mexican or Chicano','Y',NULL,SYSDATE,SYSDATE,NULL,38);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'NAM';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','NAM','Unspecified Native American','Y',NULL,SYSDATE,SYSDATE,NULL,39);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'OTH';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','OTH','Other','Y',NULL,SYSDATE,SYSDATE,NULL,40);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'SCAHIS';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','SCAHIS','South Cntrl/Cntrl Amer. Hisp.','Y',NULL,SYSDATE,SYSDATE,NULL,41);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='race'
    AND CODELST_SUBTYP = 'UNK';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'race','UNK','Unknown/Question Not Asked','Y',NULL,SYSDATE,SYSDATE,NULL,42);
	commit;
  end if;
end;
/
--END--