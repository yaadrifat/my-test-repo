/* This readMe is specific to Velos eResearch version 9.0 build637-et014 */

=====================================================================================================================================
 Previous database build check:[Applies to ERES only]
 We have implemented the process to execute the database patches after validating the previous build number in the database. To accomplish this, one database
 function has to be created. So, in this build first Script would be 00_F_Check_Prev_Patch.sql. After executing this script,  the utility will start working.
 If the previous version doesn't match, the script throws the Oracle Error. This will be modified for user friendly message later.
 
=====================================================================================================================================
  