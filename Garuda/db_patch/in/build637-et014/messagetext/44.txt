<table width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
  <tr><td>
	Dear EmTrax Member,
</td></tr>
<tr><td>
&nbsp;
  </td></tr>
  <tr><td>
	Welcome to EmTrax. As per your Organization's request an account has been created for you. Your account information is displayed below:
</td></tr>
<tr><td>
&nbsp;
  </td></tr>
  <tr><td>
	 <table width="25%" cellspacing="0" cellpadding="0" border="0" align="left">
		  <tr>
		      <td>Login ID</td>
			  <td>: ~1~</td>
		  </tr>
	  </table>
</td></tr>
<tr><td>
&nbsp;
  </td></tr>
  <tr><td>
	Your password will be sent in a separate email. Please login to your account, go to the 'Personalize account section' and change your password immediately. You may of course change your password at any time should you have any concerns regarding the privacy of your information.
</td></tr>
  <tr><td>
	To begin using the EmTrax system, login to your account.
</td></tr>
<tr><td>
&nbsp;
  </td></tr>
  <tr><td>
	**** Important ****<br>
	Please remember that the above information needs to be kept in a secure place. Anyone with access to the above information can access your account information.
</td></tr>
<tr><td>
&nbsp;
  </td></tr>
  <tr><td>
	<img alt="" src="http://marrow.org/images/email/nmdp_80.png"><img alt="" src="http://marrow.org/images/email/btm_50.png">
</td></tr>
  <tr><td>	____________________________________________________________________________________
  </td></tr>
  <tr><td>
</td></tr>
  <tr><td><i>	***This is an auto-generated e-mail***<br>

	This e-mail address is not monitored; please do not respond to this message.
	The sender of this e-mail provides services for the NMDP and/or Be The Match pursuant to agreement and may not be authorized to bind the organization with any information, material, or content contained within this e-mail.</i>
	</td></tr>
</table>