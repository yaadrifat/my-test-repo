DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'uk';
  if (v_column_exists = 1) then
update ER_CODELST set CODELST_DESC='United Kingdom: England, Northern Ireland, Scotland, Wales, the Isle of Man, the Channel Islands, Gibraltar, or the Falkland Islands' where codelst_type = 'chart_vCJD' AND codelst_subtyp = 'uk';
commit;
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'chart_vCJD'
    AND codelst_subtyp = 'uyugo';
  if (v_column_exists = 1) then
    update ER_CODELST set CODELST_DESC='Yugoslavia (Federal Republic of), Kosovo, Montenegro, Serbia' where codelst_type = 'chart_vCJD' AND codelst_subtyp = 'uyugo';
commit;
  end if;
end;
/

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'risk_HIV1'
    AND codelst_subtyp = 'sengal';
  if (v_column_exists = 1) then
    update ER_CODELST set CODELST_DESC='Senegal' where codelst_type = 'risk_HIV1' AND codelst_subtyp = 'sengal';
commit;
  end if;
end;
/
