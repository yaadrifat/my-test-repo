create or replace
Function F_Check_Previous_Patch( Previousbuildno In Varchar2 )
Return BOOLEAN
As
V_Status  Boolean;
V_Last_App_Version VARCHAR2(50);
Begin
  Select Ctrl_Value Into V_Last_App_Version From  Er_Ctrltab Where  Ctrl_Key = 'app_version';
  IF (V_Last_App_Version = Previousbuildno) THEN
  V_Status:=True;
  ELSE
  V_Status:=False;
  Dbms_Output.Put_Line('Process exited!  ');
  END IF;
  RETURN V_Status;
End F_Check_Previous_Patch;