CREATE OR REPLACE FUNCTION F_IS_NEW_TASK_NEEDED(ENTITYID NUMBER) RETURN NUMBER IS
FLAG NUMBER(1):=0;
IS_ORDER_DATA NUMBER(10):=0;
IS_SHIPMENT_DATA NUMBER(10):=0;
ORDER_ID_VAL NUMBER(10):=0;
V_CORD_SHIP_DATE DATE;
COMPARE_DATE DATE;
FK_CODELST_SHIPMENT NUMBER(10):=0;
BEGIN


      SELECT COUNT(*) INTO IS_ORDER_DATA FROM ER_ORDER_HEADER HDR LEFT OUTER JOIN ER_ORDER ORD ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER ) WHERE HDR.ORDER_ENTITYID=ENTITYID;
      SELECT CODELST_DESC INTO COMPARE_DATE FROM ER_CODELST WHERE CODELST_TYPE='VersionDate' AND CODELST_SUBTYP='Version1.3';
      SELECT PK_CODELST INTO FK_CODELST_SHIPMENT FROM ER_CODELST WHERE CODELST_TYPE='shipment_type' AND CODELST_SUBTYP='CBU';

      IF IS_ORDER_DATA=0 THEN
    
        FLAG:=1;
    
      ELSIF IS_ORDER_DATA>0 THEN 
    
                SELECT MAX(ORD.PK_ORDER) INTO ORDER_ID_VAL FROM ER_ORDER_HEADER HDR LEFT OUTER JOIN ER_ORDER ORD ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER ) WHERE HDR.ORDER_ENTITYID=ENTITYID;
                
                SELECT COUNT(*) INTO IS_SHIPMENT_DATA FROM CB_SHIPMENT SHIP WHERE SHIP.FK_SHIPMENT_TYPE=FK_CODELST_SHIPMENT AND SHIP.FK_ORDER_ID=ORDER_ID_VAL;

                IF IS_SHIPMENT_DATA>0 THEN
                  SELECT SHIP.SHIPMENT_DATE INTO V_CORD_SHIP_DATE FROM CB_SHIPMENT SHIP WHERE SHIP.FK_SHIPMENT_TYPE=FK_CODELST_SHIPMENT AND SHIP.FK_ORDER_ID=ORDER_ID_VAL;
                END IF;


                IF (V_CORD_SHIP_DATE IS NULL) OR (IS_SHIPMENT_DATA=0) OR (V_CORD_SHIP_DATE IS NOT NULL AND V_CORD_SHIP_DATE>COMPARE_DATE ) THEN
                    FLAG:=1;
                END IF;
      END IF;
      
      RETURN FLAG;


END;
/


