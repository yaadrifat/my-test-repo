--start update er_codelst table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst where codelst_type='qury_build_rprt' and codelst_subtyp='quicksummaryrpt';
  If (V_Record_Exists = 1) Then
	update er_codelst set codelst_desc='Shipped CBUs' where codelst_type='qury_build_rprt' and codelst_subtyp='quicksummaryrpt';
commit;
  end if;
end;
/
--END--
