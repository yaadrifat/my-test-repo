/* This readMe is specific to Velos eResearch version 9.0 build637-et046 */

===================================================================================================================================== 
 VDA connection settings for �Query Builder�
	
	To configure VDA connection setting for Query Builder we have introduced one XML file namely "vda-ds.xml".
	This file is located under the following path-

		JBoss-Home \server\eresearch\deploy
		
		1. To run this you need to make modifications in this XML file by providing appropriate database credentials.
		At line no 7:-
		eg.- 
		<xa-datasource-property name="URL">jdbc:oracle:thin:@172.16.1.225:1521:eresdev4</xa-datasource-property> 
        <xa-datasource-property name="User">vda</xa-datasource-property> 
        <xa-datasource-property name="Password">vda123</xa-datasource-property> 
 
=====================================================================================================================================

 