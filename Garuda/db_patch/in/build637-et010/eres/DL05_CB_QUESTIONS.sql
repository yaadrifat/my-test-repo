-- INSERTING CODE LIST VALUES FOR MRQ UNEXPECTED QUESTION-
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'mrq_unres3'
    AND codelst_subtyp = 'no';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_unres3','no','No','N',1,null,null,null,null,sysdate,sysdate,null,null,null,null);
  end if;
end;
/
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'mrq_unres3'
    AND codelst_subtyp = 'bankdntask';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_unres3','bankdntask','Bank did not ask question','N',2,null,null,null,null,sysdate,sysdate,null,null,null,null);
  end if;
end;
/
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'mrq_unres3'
    AND codelst_subtyp = 'momdntans';
  if (v_column_exists = 0) then
    Insert into ER_CODELST 
(PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,CREATED_ON,IP_ADD,CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) 
values (SEQ_ER_CODELST.nextval,null,'mrq_unres3','momdntans','Mother did not answer','N',3,null,null,null,null,sysdate,sysdate,null,null,null,null);
  end if;
end;
/
--END--
--start update cb_form_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_contag_person_understand_ind')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set UNEXPECTED_RESPONSE_VALUE = 'mrq_unres3' where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hiv_contag_person_understand_ind') and FK_FORM = (select pk_form from cb_forms where forms_desc='MRQ' and version='N2G');
commit;
  end if;
end;
/
--END--
--start update cb_Questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'mil_base_europe_1_ge_6m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>From 1980 through 1990,</b> did you spend a total of <u>6 months or more</u> associated with a military base in any of the following countries: United Kingdom, Belgium, Netherlands or Germany?'  where QUES_CODE = 'mil_base_europe_1_ge_6m_ind';
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'mil_base_europe_2_ge_6m_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set QUES_DESC='<b>From 1980 through 1996,</b> did you spend a total of <u>6 months or more</u> associated with a military base in any of the following countries: Spain, Portugal, Turkey, Italy or Greece?'  where QUES_CODE = 'mil_base_europe_2_ge_6m_ind';
commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'sex_w_born_live_in_ctry_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set REF_CHART='risk_HIV1'  where QUES_CODE = 'sex_w_born_live_in_ctry_ind';
commit;
  end if;
end;
/
--END--
--START UPDATE ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'IDM';
  if (v_column_exists = 0) then
	update er_codelst set CODELST_DESC = 'IDM' where codelst_type = 'sub_entity_type' AND codelst_subtyp = 'IDM';
  end if;
end;
/
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'MRQ';
  if (v_column_exists = 0) then
	update er_codelst set CODELST_DESC = 'MRQ' where codelst_type = 'sub_entity_type' AND codelst_subtyp = 'MRQ';
  end if;
end;
/
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'LAB_SUM';
  if (v_column_exists = 0) then
	update er_codelst set CODELST_DESC = 'LAB SUMMERY' where codelst_type = 'sub_entity_type' AND codelst_subtyp = 'LAB_SUM';
  end if;
end;
/
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'sub_entity_type'
    AND codelst_subtyp = 'NOTES';
  if (v_column_exists = 0) then
	update er_codelst set CODELST_DESC = 'Clinical Note' where codelst_type = 'sub_entity_type' AND codelst_subtyp = 'NOTES';
  end if;
end;
/
--END
