--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'note_cat_pf'
    AND codelst_subtyp = 'research_smp';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='NMDP Research Sample' where codelst_type = 'note_cat_pf'
    AND codelst_subtyp = 'research_smp';
	commit;
  end if;
end;
/
--END--

--==============================================================================================================================================
--alert_01
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type in('alert','alert_resol');
  if (v_record_exists = 39) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL2 = '<table><tr><td align="left"><b><111></b></td><td>:</td><td align="left"><222></td></tr><tr><td align="left">CBB ID</td><td>:</td><td align="left"><333></td></tr><tr><td align="left">CBU Registry ID</td><td>:</td><td align="left"><123></td></tr><tr><td align="left">Request Type</td><td>:</td><td align="left"><444></td></tr><tr><td align="left">Alert Date</td><td>:</td><td align="left"><555></td></tr></table><br><br><p>This automated EmTrax communication has been sent to you as per your alert preferences. If you wish to discontinue these alerts, please access the alert management section of your profile in EmTrax.</p><table><tr><td><img alt="" src="http://marrow.org/images/email/nmdp_80.png"></td><td valign="middle"><img alt="" src="http://marrow.org/images/email/btm_50.png"></td></tr></table><br><hr><p>***This is an auto-generated e-mail***</p><p>This e-mail address is not monitored; please do not respond to this message.</p><br><p>The sender of this e-mail provides services for the NMDP and/or Be The Match pursuant to agreement and may not be authorized to bind the organization with any information, material, or content contained within this e-mail.</p>' where codelst_type in('alert','alert_resol');
	commit;
  end if;
end;
/
--END--

