DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_SHORTNAME = 'PERTMON';
  if (v_record_exists = 1) then
      UPDATE ER_LABTEST SET LABTEST_NAME='% of CD34+ Cells in Total Mononuclear Cell Count' where LABTEST_SHORTNAME = 'PERTMON';
	commit;
  end if;
end;
/
--END--