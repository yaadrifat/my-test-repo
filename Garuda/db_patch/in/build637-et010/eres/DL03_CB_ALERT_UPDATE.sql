--STARTS UPDATING RECORD FROM cb_alert_conditions TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_alert_conditions 
	where TABLE_NAME='ER_ORDER_HEADER' and COLUMN_NAME='ORDER_OPEN_DATE-SYSDATE';
  if (v_record_exists = 5) then
      update cb_alert_conditions set TABLE_NAME='TO_DATE(TO_CHAR(ER_ORDER_HEADER',COLUMN_NAME='ORDER_OPEN_DATE,''Mon DD, YYYY''),''Mon DD, YYYY'')-TO_DATE(TO_CHAR(SYSDATE,''Mon DD, YYYY''),''Mon DD, YYYY'')' where TABLE_NAME='ER_ORDER_HEADER' and COLUMN_NAME='ORDER_OPEN_DATE-SYSDATE';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM cb_alert_conditions TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_alert_conditions 
	where FK_CODELST_ALERT =(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_09') and TABLE_NAME='CB_SHIPMENT' and COLUMN_NAME='SCH_SHIPMENT_DATE-SYSDATE';
  if (v_record_exists = 1) then
      update cb_alert_conditions set TABLE_NAME='TO_DATE(TO_CHAR(CB_SHIPMENT',COLUMN_NAME='SCH_SHIPMENT_DATE,''Mon DD, YYYY''),''Mon DD, YYYY'')-TO_DATE(TO_CHAR(SYSDATE,''Mon DD, YYYY''),''Mon DD, YYYY'')' where FK_CODELST_ALERT =(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_09') and TABLE_NAME='CB_SHIPMENT' and COLUMN_NAME='SCH_SHIPMENT_DATE-SYSDATE';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM cb_alert_conditions TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_alert_conditions 
	where FK_CODELST_ALERT =(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_06') and
	TABLE_NAME='ER_ORDER' and COLUMN_NAME='FINAL_REVIEW_TASK_FLAG' and CONDITION_VALUE=' IS NULL';
  if (v_record_exists = 1) then
      update cb_alert_conditions set LOGICAL_OPERATOR='AND' where FK_CODELST_ALERT =(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_06') and
	TABLE_NAME='ER_ORDER' and COLUMN_NAME='FINAL_REVIEW_TASK_FLAG' and CONDITION_VALUE=' IS NULL';
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO cb_alert_conditions TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_alert_conditions 
	where FK_CODELST_ALERT =(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_06') and
	TABLE_NAME='ER_ORDER' and COLUMN_NAME='ORDER_TYPE' and CONDITION_VALUE='(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''order_type'' and CODELST_SUBTYP=''OR'')';
  if (v_record_exists = 0) then
      INSERT INTO cb_alert_conditions(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,KEY_CONDITION) 
VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_06'),'PRE','ER_ORDER','ORDER_TYPE','(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE=''order_type'' and CODELST_SUBTYP=''OR'')','=','PK_ORDER = #keycolumn');
	commit;
  end if;
end;
/
--END--


