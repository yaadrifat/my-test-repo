--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='m_or_f_adopted_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - review response to question 1a.<br/><br/>If no or unknown - accept response.' WHERE QUES_CODE='m_or_f_adopted_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='m_or_f_adopted_medhist_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate.  Family medical history for previous generation is acceptable.<br/><br/>If no or unknown - If there is no family medical history available, notify the transplant center.' WHERE QUES_CODE='m_or_f_adopted_medhist_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='baby_moth_fath_bld_rel_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes or unknown- evaluate.  Defer donations for blood relatives that are first degree relatives or first cousins due to possible inherited disorders.<br/><br/>If no - accept response.' WHERE QUES_CODE='baby_moth_fath_bld_rel_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='baby_moth_fath_bld_rel_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes or unknown- evaluate.  Defer donations for blood relatives that are first degree relatives or first cousins due to possible inherited disorders.<br/><br/>If no - accept response.' WHERE QUES_CODE='baby_moth_fath_bld_rel_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='preg_use_dnr_egg_sperm_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - review response to question 3a.<br/><br/>If no or unknown - accept response.' WHERE QUES_CODE='preg_use_dnr_egg_sperm_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='dnr_egg_sperm_fmhq_avail_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate.  A unit may be accepted for transplantation if the donor egg/sperm came from an organization accredited by the American Association of Tissue Bank (AATB) or equivalent international agency.<br/><br/>If no or unknown - defer. Cord blood bank medical director to evaluate response and defer if necesasry.' WHERE QUES_CODE='dnr_egg_sperm_fmhq_avail_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='abn_prenat_test_rslt_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes -review response to sub-questions.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical direction to evaluate and defer if necessary.' WHERE QUES_CODE='abn_prenat_test_rslt_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='abn_prenat_test_rslt_diag_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate.  Cord Blood Bank Medical Director must investigate any affirmative response to determine if the test result indicates a genetic predisposition that may place a potential recipient at an increased risk.  If so, the donation must be deferred.<br/><br/>If no or unknown - accept response.' WHERE QUES_CODE='abn_prenat_test_rslt_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='child_die_age_10_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate.  Cord Blood Bank Medical Director must investigate any affirmative response to determine if the cause of death is due to a denetic predisposition that may place a potential recipient at an increased risk.  If so, the donation must be deferred.  If the medical director determines there is no increased risk, the donation is acceptable.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='child_die_age_10_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='child_die_age_10_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate.  Cord Blood Bank Medical Director must investigate any affirmative response to determine if the cause of death is due to a denetic predisposition that may place a potential recipient at an increased risk.  If so, the donation must be deferred.  If the medical director determines there is no increased risk, the donation is acceptable.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='child_die_age_10_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='child_die_age_10_desc';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP=null WHERE QUES_CODE='child_die_age_10_desc';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='stillborn_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate.  Cord Blood Bank Medical Director must investigate any affirmative response to determine if the cause of death is due to a denetic predisposition that may place a potential recipient at an increased risk.  If so, the donation must be deferred.  If the medical director determines there is no increased risk, the donation is acceptable.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necesary.' WHERE QUES_CODE='stillborn_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='stillborn_cause';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP=null WHERE QUES_CODE='stillborn_cause';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='can_leuk_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate responses to 7a through 7b. Review responses to sub-questions (Biological Relation to Baby/Type of Cancer) and evaluate.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='can_leuk_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='can_leuk_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate responses to 7a through 7c. Review responses to sub-questions (Biological Relation to Baby/Type of Cancer) and evaluate.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='can_leuk_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='inher_rbc_dises_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate responses to 8a through 8b. Review responses to sub-questions (Biological Relation to Baby/Type of Red Cell Disease) and evaluate.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='inher_rbc_dises_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='inher_wbc_dises_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate responses to 9a through 9b. Review responses to sub-questions (Biological Relation to Baby/Type of White Blood Cell Disease) and evaluate.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='inher_wbc_dises_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_rel';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='Defer for baby&#145;s mother, baby&#145;s father, and/or baby&#145;s sibling.  If affirmative for baby&#145;s grandparent, baby&#145;s mother&#145;s sibling, or baby&#145;s father&#145;s sibling, cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='while_bld_cell_dis_rel';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='while_bld_cell_dis_type1' or QUES_CODE='while_bld_cell_dis_type2' or QUES_CODE='while_bld_cell_dis_type3' or QUES_CODE='while_bld_cell_dis_type4' or QUES_CODE='while_bld_cell_dis_type5' or QUES_CODE='while_bld_cell_dis_type6';
  
  IF (INDEX_COUNT = 6) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP=null WHERE QUES_CODE='while_bld_cell_dis_type1' or QUES_CODE='while_bld_cell_dis_type2' or QUES_CODE='while_bld_cell_dis_type3' or QUES_CODE='while_bld_cell_dis_type4' or QUES_CODE='while_bld_cell_dis_type5' or QUES_CODE='while_bld_cell_dis_type6';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='inher_imm_dfc_disordr_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate responses to 10a through 10b. Review responses to sub-questions (Biological Relation to Baby/Type of Immune Deficiency) and evaluate.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='inher_imm_dfc_disordr_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_rel';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='Defer for baby&#145;s mother, baby&#145;s father, and/or baby&#145;s sibling.  If affirmative for baby&#145;s grandparent, baby&#145;s mother&#145;s sibling, or baby&#145;s father&#145;s sibling, cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='imune_def_ind_rel';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='imune_def_ind_type1' or QUES_CODE='imune_def_ind_type2' or QUES_CODE='imune_def_ind_type3' or QUES_CODE='imune_def_ind_type4' or QUES_CODE='imune_def_ind_type5' or QUES_CODE='imune_def_ind_type6';
  
  IF (INDEX_COUNT = 6) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP=null WHERE QUES_CODE='imune_def_ind_type1' or QUES_CODE='imune_def_ind_type2' or QUES_CODE='imune_def_ind_type3' or QUES_CODE='imune_def_ind_type4' or QUES_CODE='imune_def_ind_type5' or QUES_CODE='imune_def_ind_type6';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='inher_pltlt_dises_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate responses to 11a through 11b. Review responses to sub-questions (Biological Relation to Baby/Type of Platelet Disease) and evaluate.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='inher_pltlt_dises_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_rel';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='Defer for baby&#145;s mother, baby&#145;s father, and/or baby&#145;s sibling.  If affirmative for baby&#145;s grandparent, baby&#145;s mother&#145;s sibling, or baby&#145;s father&#145;s sibling, cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='platelet_dis_ind_rel';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='platelet_dis_ind_type1' or QUES_CODE='platelet_dis_ind_type2' or QUES_CODE='platelet_dis_ind_type3' or QUES_CODE='platelet_dis_ind_type4' or QUES_CODE='platelet_dis_ind_type5' or QUES_CODE='platelet_dis_ind_type6';
  
  IF (INDEX_COUNT = 6) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP=null WHERE QUES_CODE='platelet_dis_ind_type1' or QUES_CODE='platelet_dis_ind_type2' or QUES_CODE='platelet_dis_ind_type3' or QUES_CODE='platelet_dis_ind_type4' or QUES_CODE='platelet_dis_ind_type5' or QUES_CODE='platelet_dis_ind_type6';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_bld_dises_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - Cord Blood Bank Medical Director must review. Review responses to sub-questions (Biological Relation to Baby/Type of Disease) and evaluate.<br/><br/>If no - accept response.' WHERE QUES_CODE='inher_other_bld_dises_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_bld_dises_desc1' or QUES_CODE='inher_other_bld_dises_desc2' or QUES_CODE='inher_other_bld_dises_desc3' or QUES_CODE='inher_other_bld_dises_desc4' or QUES_CODE='inher_other_bld_dises_desc5' or QUES_CODE='inher_other_bld_dises_desc6';
  
  IF (INDEX_COUNT = 6) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP=null WHERE QUES_CODE='inher_other_bld_dises_desc1' or QUES_CODE='inher_other_bld_dises_desc2' or QUES_CODE='inher_other_bld_dises_desc3' or QUES_CODE='inher_other_bld_dises_desc4' or QUES_CODE='inher_other_bld_dises_desc5' or QUES_CODE='inher_other_bld_dises_desc6';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='inher_sickle_cell_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes -evaluate.  Evaluation should include testing results from newborn screening results.  Defer donation if presences of Sickle Cell disease in the baby. review response to sub-question (Biological Relation to Baby).  Cord blood bank medical director to evaluate response and defer if necessary.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='inher_sickle_cell_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='inher_thalassemia_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes -evaluate.  Evaluation should include testing results from newborn screening results.  Defer donation if presences of Thalassemia disease in the baby. review response to sub-question (Biological Relation to Baby).  Cord blood bank medical director to evaluate response and defer if necessary.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='inher_thalassemia_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate responses to 15a through 15b. Review responses to sub-questions (Biological Relation to Baby/Type of Metabolic/Storage Disease) and evaluate.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='inher_met_stg_dises_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_rel';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='Defer for baby&#145;s mother, baby&#145;s father, and/or baby&#145;s sibling.  If affirmative for baby&#145;s mother, baby&#145;s father, baby&#145;s sibling, baby&#145;s grandparent, baby&#145;s mother&#145;s sibling, or baby&#145;s father&#145;s sibling, cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='inher_met_stg_dises_rel';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='inher_met_stg_dises_type1' or QUES_CODE='inher_met_stg_dises_type2' or QUES_CODE='inher_met_stg_dises_type3' or QUES_CODE='inher_met_stg_dises_type4' or QUES_CODE='inher_met_stg_dises_type5' or QUES_CODE='inher_met_stg_dises_type6';
  
  IF (INDEX_COUNT = 6) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP=null WHERE QUES_CODE='inher_met_stg_dises_type1' or QUES_CODE='inher_met_stg_dises_type2' or QUES_CODE='inher_met_stg_dises_type3' or QUES_CODE='inher_met_stg_dises_type4' or QUES_CODE='inher_met_stg_dises_type5' or QUES_CODE='inher_met_stg_dises_type6';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='hiv_aids_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes -Defer donation.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='hiv_aids_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='severe_aut_imm_sys_disordr_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate responses to 17a through 17b. Review responses to sub-questions (Biological Relation to Baby/Type of severe autoimmune disorder) and evaluate.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='severe_aut_imm_sys_disordr_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='oth_immune_dis';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate responses to 18a through 18b. Review responses to sub-questions (Biological Relation to Baby/Type of severe autoimmune disorder) and evaluate.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='oth_immune_dis';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='chron_bld_tranf_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate. Review response to sub-question (Biological Relation to Baby).   Defer for baby&#145;s mother, baby&#145;s father, and/or baby&#145;s sibling.  Notify transplant center if affirmative for baby&#145;s grandparent, baby&#145;s mother&#145;s sibling, or baby&#145;s father&#145;s sibling. If yes for baby&#145;s grandparent, baby&#145;s mother&#145;s sibling or baby&#145;s father&#145;s sibling, cord blood bank medical director to evaluate response and defer if necessary.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='chron_bld_tranf_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='hemolytic_anemia_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate. Review response to sub-question (Biological Relation to Baby).   Defer for baby&#145;s mother, baby&#145;s father, and/or baby&#145;s sibling.  Notify transplant center if affirmative for baby&#145;s grandparent, baby&#145;s mother&#145;s sibling, or baby&#145;s father&#145;s sibling. If yes for baby&#145;s grandparent, baby&#145;s mother&#145;s sibling or baby&#145;s father&#145;s sibling, cord blood bank medical director to evaluate response and defer if necessary.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='hemolytic_anemia_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='spleen_removed_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate. Review response to sub-question (Biological Relation to Baby).   Defer for baby&#145;s mother, baby&#145;s father, and/or baby&#145;s sibling.  Notify transplant center if affirmative for baby&#145;s grandparent, baby&#145;s mother&#145;s sibling, or baby&#145;s father&#145;s sibling. If yes for baby&#145;s grandparent, baby&#145;s mother&#145;s sibling or baby&#145;s father&#145;s sibling, cord blood bank medical director to evaluate response and defer if necessary.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='spleen_removed_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='gallbladder_removed_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate.  Cord Blood Bankd Medical Director must review medical history and CBC to assess for hereditary hemolytic anemia.<br/><br/>Cord blood banks may choose to defer donation in lieu of medical history review.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='gallbladder_removed_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='cruetz_jakob_dis_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - Defer donation if the baby&#145;s mother, baby&#145;s father, baby&#145;s sibling, baby&#145;s grandparent, baby&#145;s mother&#145;s sibling and/or baby&#145;s father&#145;s sibling has ever had CJD.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='cruetz_jakob_dis_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='If yes - evaluate - Cord Blood Bank Medical Director must review.<br/><br/>If no - accept response.<br/><br/>If unknown - cord blood bank medical director to evaluate response and defer if necessary.' WHERE QUES_CODE='inher_other_dises_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='inher_other_dises_desc1' or QUES_CODE='inher_other_dises_desc2' or QUES_CODE='inher_other_dises_desc3' or QUES_CODE='inher_other_dises_desc4' or QUES_CODE='inher_other_dises_desc5' or QUES_CODE='inher_other_dises_desc6';
  
  IF (INDEX_COUNT = 6) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP=null WHERE QUES_CODE='inher_other_dises_desc1' or QUES_CODE='inher_other_dises_desc2' or QUES_CODE='inher_other_dises_desc3' or QUES_CODE='inher_other_dises_desc4' or QUES_CODE='inher_other_dises_desc5' or QUES_CODE='inher_other_dises_desc6';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='htlv_incl_screen_test_para_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='<u>Current Evaluation and Action</u><br>If Yes &#8594; Cord Blood Bank Medical Director should evaluate response and defer if yes answer reveals signs or symptoms of HTLV.<br/><br/>If Bank did not ask or Mother did not answer on or after May 25, 2005 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 &#8594; the question was clarified to be consistent with FDA language to provide an example of clinical evidence of HTLV infection.  The previous question contained the same physician evaluation and deferral assessment.  Consider responses as equivalent to current.' WHERE QUES_CODE='htlv_incl_screen_test_para_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_HELP IN CB_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_QUESTIONS WHERE QUES_CODE='hiv_contag_person_understand_ind';
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_QUESTIONS SET QUES_HELP='<u>Current Evaluation and Action</u><br>If No &#8594; defer.<br/><br/><u>Historical Evaluation</u><br/>As of October 1, 2011 - the question was amended for clarity for the maternal donor. The previous question contained the same physician evaluation and deferral assessment. Consider responses as equivalent to current.' WHERE QUES_CODE='hiv_contag_person_understand_ind';
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='cancer_leuk_rel_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='7.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='cancer_leuk_rel_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='cancer_leuk_rel_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='7.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='cancer_leuk_rel_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='cancer_leuk_rel_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='7.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='cancer_leuk_rel_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='cancer_leuk_rel_type_desc1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='7.b.1' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='cancer_leuk_rel_type_desc1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='cancer_leuk_rel_type_desc2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='7.b.1' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='cancer_leuk_rel_type_desc2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='cancer_leuk_rel_type_desc3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='7.b.1' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='cancer_leuk_rel_type_desc3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='red_bld_cell_dis_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='8.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='red_bld_cell_dis_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='red_bld_cell_dis_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='8.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='red_bld_cell_dis_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='red_bld_cell_dis_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='8.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='red_bld_cell_dis_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='red_bld_cell_dis_type4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='8.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='red_bld_cell_dis_type4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='red_bld_cell_dis_type5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='8.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='red_bld_cell_dis_type5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='red_bld_cell_dis_type6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='8.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='red_bld_cell_dis_type6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='while_bld_cell_dis_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='9.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='while_bld_cell_dis_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='while_bld_cell_dis_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='9.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='while_bld_cell_dis_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='while_bld_cell_dis_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='9.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='while_bld_cell_dis_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='while_bld_cell_dis_type4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='9.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='while_bld_cell_dis_type4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='while_bld_cell_dis_type5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='9.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='while_bld_cell_dis_type5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='while_bld_cell_dis_type6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='9.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='while_bld_cell_dis_type6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='imune_def_ind_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='10.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='imune_def_ind_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='imune_def_ind_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='10.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='imune_def_ind_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='imune_def_ind_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='10.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='imune_def_ind_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='imune_def_ind_type4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='10.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='imune_def_ind_type4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='imune_def_ind_type5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='10.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='imune_def_ind_type5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='imune_def_ind_type6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='10.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='imune_def_ind_type6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='platelet_dis_ind_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='11.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='platelet_dis_ind_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='platelet_dis_ind_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='11.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='platelet_dis_ind_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='platelet_dis_ind_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='11.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='platelet_dis_ind_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='platelet_dis_ind_type4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='11.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='platelet_dis_ind_type4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='platelet_dis_ind_type5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='11.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='platelet_dis_ind_type5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='platelet_dis_ind_type6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='11.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='platelet_dis_ind_type6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_bld_dises_desc1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='12.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_bld_dises_desc1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_bld_dises_desc2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='12.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_bld_dises_desc2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_bld_dises_desc3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='12.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_bld_dises_desc3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_bld_dises_desc4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='12.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_bld_dises_desc4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_bld_dises_desc5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='12.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_bld_dises_desc5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_bld_dises_desc6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='12.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_bld_dises_desc6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='15.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='15.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='15.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_type4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='15.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_type4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_type5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='15.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_type5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_type6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='15.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_type6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_desc1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='15.b.1' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_desc1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_desc2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='15.b.1' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_desc2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_desc3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='15.b.1' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_desc3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_desc4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='15.b.1' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_desc4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_desc5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='15.b.1' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_desc5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_desc6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='15.b.1' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_met_stg_dises_desc6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='severe_aut_imm_sys_disordr_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='17.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='severe_aut_imm_sys_disordr_type1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='severe_aut_imm_sys_disordr_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='17.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='severe_aut_imm_sys_disordr_type2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='severe_aut_imm_sys_disordr_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='17.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='severe_aut_imm_sys_disordr_type3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='oth_immune_dis_desc1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='18.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='oth_immune_dis_desc1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='oth_immune_dis_desc2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='18.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='oth_immune_dis_desc2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='oth_immune_dis_desc3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='18.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='oth_immune_dis_desc3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_dises_desc1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='24.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_dises_desc1') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_dises_desc2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='24.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_dises_desc2') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_dises_desc3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='24.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_dises_desc3') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_dises_desc4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='24.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_dises_desc4') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_dises_desc5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='24.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_dises_desc5') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

--STARTS UPDATE THE COLUMN QUES_SEQ IN CB_FORM_QUESTIONS--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM CB_FORM_QUESTIONS WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_dises_desc6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
  
  IF (INDEX_COUNT = 1) THEN
    UPDATE CB_FORM_QUESTIONS SET QUES_SEQ='24.b' WHERE FK_QUESTION=(select pk_questions from cb_questions where QUES_CODE='inher_other_dises_desc6') and FK_FORM=(select pk_form from cb_forms where FORMS_DESC='FMHQ' and IS_CURRENT_FLAG='1');
    COMMIT;
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,279,8,'02_UPDATE_FORM_QUESTIONS.sql',sysdate,'9.0.0 B#637-ET053');
commit;