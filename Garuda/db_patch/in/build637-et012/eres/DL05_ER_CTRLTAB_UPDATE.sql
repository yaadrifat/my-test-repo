--STARTS UPDATING RECORD FROM ER_CTRLTAB TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CTRLTAB
    where ctrl_value='email_from';
  if (v_record_exists = 1) then
      UPDATE ER_CTRLTAB SET CTRL_DESC = 'servicedesk@nmdp.org' where ctrl_value='email_from';
	commit;
  end if;
end;
/
--END--