CREATE TABLE CB_FILTER_DATA(
PK_FILTER_DATA NUMBER(10,0),
FK_ORDER_ID NUMBER(10,0),
FK_CORD_ID NUMBER(10,0),
FK_USER_ID NUMBER(10,0),
FK_FILTER_TYPE NUMBER(10,0),
IS_FILTER_REMOVED NUMBER(10,0),
FK_SUB_MODULE NUMBER(10,0),
FILTER_MODULE NUMBER(10,0),
FILTER_NAME VARCHAR2(255 BYTE),
FILTER_PARAMS VARCHAR2(1000 BYTE),
CREATED_ON DATE,
CREATED_BY NUMBER(10,0),
RID NUMBER(10,0),
DELETEDFLAG NUMBER(2,0),
LAST_MODIFIED_BY NUMBER(10,0),
LAST_MODIFIED_ON DATE);
/
commit;



COMMENT ON TABLE CB_FILTER_DATA IS 'STORES FILTER VALUES OF LANDING AND PENDING SCREEN';

COMMENT ON COLUMN CB_FILTER_DATA.PK_FILTER_DATA IS 'Stores sequence generated unique value and acts as primary key column';
COMMENT ON COLUMN CB_FILTER_DATA.FK_ORDER_ID IS 'Stores foreign key reference to er_order table';
COMMENT ON COLUMN CB_FILTER_DATA.FK_CORD_ID IS 'Stores foreign key reference to cb_cord table';
COMMENT ON COLUMN CB_FILTER_DATA.FK_USER_ID IS 'Stores foreign key reference to ER_USER table';
COMMENT ON COLUMN CB_FILTER_DATA.FK_FILTER_TYPE IS 'Stores foreign key reference to ER_CODELST table';
COMMENT ON COLUMN CB_FILTER_DATA.IS_FILTER_REMOVED IS 'Stores flag 0 or 1 filter removed or not';
COMMENT ON COLUMN CB_FILTER_DATA.FILTER_MODULE IS 'Stores foreign key reference er_codelst module landing or pending module';
COMMENT ON COLUMN CB_FILTER_DATA.FK_SUB_MODULE IS 'Stores foreign key reference er_codelst sub module landing or pending module';
COMMENT ON COLUMN CB_FILTER_DATA.FILTER_NAME IS 'Stores name of the custom filter';
COMMENT ON COLUMN CB_FILTER_DATA.FILTER_PARAMS IS 'Store paramater value of filter data';
COMMENT ON COLUMN CB_FILTER_DATA.CREATED_ON IS 'Date when created';
COMMENT ON COLUMN CB_FILTER_DATA.CREATED_BY IS 'Stores foreign key reference to ER_USER table';
COMMENT ON COLUMN CB_FILTER_DATA.RID IS 'Stores rid value in this column';
COMMENT ON COLUMN CB_FILTER_DATA.DELETEDFLAG IS 'Stored deleted flag 0 or 1 in this column';
COMMENT ON COLUMN CB_FILTER_DATA.LAST_MODIFIED_BY IS 'Stores foreign key reference to ER_USER table';
COMMENT ON COLUMN CB_FILTER_DATA.LAST_MODIFIED_ON IS 'Stores date last modified date';


CREATE SEQUENCE SEQ_CB_FILTER_DATA MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE ;

commit;




--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='filter_module'
    AND CODELST_SUBTYP = 'pending_screen';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'filter_module','pending_screen','Pending Screen','N',NULL,SYSDATE,SYSDATE,NULL,1);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='filter_module'
    AND CODELST_SUBTYP = 'landing_page';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'filter_module','landing_page','Landing Page','N',NULL,SYSDATE,SYSDATE,NULL,1);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='filter_type'
    AND CODELST_SUBTYP = 'custom_filter';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'filter_type','custom_filter','Custom Filter','N',NULL,SYSDATE,SYSDATE,NULL,1);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='ps_submodule'
    AND CODELST_SUBTYP = 'active_request';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'ps_submodule','active_request','Active Requests','N',NULL,SYSDATE,SYSDATE,NULL,1);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='ps_submodule'
    AND CODELST_SUBTYP = 'saved_in_prog';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'ps_submodule','saved_in_prog','Saved In Progress','N',NULL,SYSDATE,SYSDATE,NULL,1);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='ps_submodule'
    AND CODELST_SUBTYP = 'not_avail_cbu';
  if (v_record_exists = 0) then
      INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD,CODELST_SEQ) 
  VALUES(SEQ_ER_CODELST.nextval,'ps_submodule','not_avail_cbu','Not Available CBU','N',NULL,SYSDATE,SYSDATE,NULL,1);
	commit;
  end if;
end;
/
--END--
