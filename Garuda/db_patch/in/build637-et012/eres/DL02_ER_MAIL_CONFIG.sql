DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_MAILCONFIG;
  if (v_record_exists > 1) then
 UPDATE ER_MAILCONFIG SET MAILCONFIG_FROM='servicedesk@nmdp.org';
	commit;
  end if;
end;
/