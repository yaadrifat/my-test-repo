create or replace 
PROCEDURE SP_ALERT_INSTANCES_BACKUP AS
var_entity_order number;
var_order_status number;
BEGIN
select pk_codelst into var_entity_order from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER';
select pk_codelst into var_order_status from er_codelst where codelst_type='order_status' and codelst_subtyp='close_ordr';
INSERT INTO cb_alert_instances_backup SELECT N.*,sysdate FROM cb_alert_instances N WHERE N.ENTITY_TYPE=var_entity_order and N.entity_id in (select pk_order from er_order where er_order.order_status=var_order_status);
DELETE FROM cb_alert_instances WHERE ENTITY_TYPE=var_entity_order and entity_id in (select pk_order from er_order where er_order.order_status=var_order_status);
COMMIT;
END;
/