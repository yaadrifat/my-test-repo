--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS 
	where CODE_TYPE='Response' AND CBU_STATUS_CODE ='DD';
  if (v_record_exists = 1) then
      update CB_CBU_STATUS set CBU_STATUS_DESC='Permanently Medically Deferred' where CODE_TYPE='Response' AND CBU_STATUS_CODE ='DD';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS 
	where CODE_TYPE='Resolution' AND CBU_STATUS_CODE ='DD';
  if (v_record_exists = 1) then
      update CB_CBU_STATUS set CBU_STATUS_DESC='Permanently Medically Deferred (DD)' where CODE_TYPE='Resolution' AND CBU_STATUS_CODE ='DD';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_CBU_STATUS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_CBU_STATUS 
	where CODE_TYPE='CloseReason' AND CBU_STATUS_CODE ='DD';
  if (v_record_exists = 1) then
      update CB_CBU_STATUS set CBU_STATUS_DESC='Permanently Medically Deferred (DD)' where CODE_TYPE='CloseReason' AND CBU_STATUS_CODE ='DD';
	commit;
  end if;
end;
/
--END--
