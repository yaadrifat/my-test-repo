--STARTS ADD THE COLUMN in CB_NOTES--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_NOTES'
    AND COLUMN_NAME = 'TAG_NOTE';

  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_NOTES add (TAG_NOTE  varchar2(1))';
      commit;
  end if;
end;
/
--END--

--STARTS ADD THE COLUMN IDS_FLAG TO CB_CORD_COMPLETE_REQ_INFO--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_COMPLETE_REQ_INFO'
    AND COLUMN_NAME = 'IDS_FLAG'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_COMPLETE_REQ_INFO add (IDS_FLAG  VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--
--starts comment for THE COLUMN IDS_FLAG TO CB_CORD_COMPLETE_REQ_INFO TABLE--
comment on column CB_CORD_COMPLETE_REQ_INFO.IDS_FLAG  is 'This column store the STATUS FLAG to the IDS';
--end--


--STARTS ADD THE COLUMN LICENSURE_FLAG TO CB_CORD_COMPLETE_REQ_INFO--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_COMPLETE_REQ_INFO'
    AND COLUMN_NAME = 'LICENSURE_FLAG'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_COMPLETE_REQ_INFO add (LICENSURE_FLAG  VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--
--starts comment for THE COLUMN IDS_FLAG TO CB_CORD_COMPLETE_REQ_INFO TABLE--
comment on column CB_CORD_COMPLETE_REQ_INFO.LICENSURE_FLAG  is 'This column store the STATUS FLAG to the LICENSURE_FLAG';
--end--



--STARTS ADD THE COLUMN ELIGIBILITY_FLAG TO CB_CORD_COMPLETE_REQ_INFO--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CB_CORD_COMPLETE_REQ_INFO'
    AND COLUMN_NAME = 'ELIGIBILITY_FLAG'; 

  if (v_column_exists = 0) then
      execute immediate 'alter table CB_CORD_COMPLETE_REQ_INFO add (ELIGIBILITY_FLAG  VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--
--starts comment for THE COLUMN IDS_FLAG TO CB_CORD_COMPLETE_REQ_INFO TABLE--
comment on column CB_CORD_COMPLETE_REQ_INFO.ELIGIBILITY_FLAG  is 'This column store the STATUS FLAG to the ELIGIBILITY_FLAG';
--end--

DECLARE
V_COLUMN_COUNT NUMBER := 0;

BEGIN
  select count(*) into V_COLUMN_COUNT from user_tab_cols where TABLE_NAME='CB_FORM_QUESTIONS' AND COLUMN_NAME='FDA_ELIGIBILITY';
  IF (V_COLUMN_COUNT = 0) THEN
    EXECUTE IMMEDIATE 'ALTER TABLE CB_FORM_QUESTIONS ADD FDA_ELIGIBILITY VARCHAR2(100)';
    COMMIT;
  END IF;
  

END;
/

COMMENT ON COLUMN CB_FORM_QUESTIONS.FDA_ELIGIBILITY IS 'Column used to store date of FDA eligibility';
COMMIT;

--STARTS ADDING COLUMN TO CB_USER_ALERTS TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_USER_ALERTS' AND COLUMN_NAME = 'CT_LAB_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_USER_ALERTS ADD CT_LAB_FLAG VARCHAR2(1)';
	commit;
  end if;
end;
/

COMMENT ON COLUMN CB_USER_ALERTS.CT_LAB_FLAG IS 'Stores 1 or 0';

--STARTS ADDING COLUMN TO CB_USER_ALERTS TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_USER_ALERTS' AND COLUMN_NAME = 'CT_SHIP_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_USER_ALERTS ADD CT_SHIP_FLAG VARCHAR2(1)';
	commit;
  end if;
end;
/

COMMENT ON COLUMN CB_USER_ALERTS.CT_SHIP_FLAG IS 'Stores 1 or 0';


--STARTS ADDING COLUMN TO CB_USER_ALERTS TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_USER_ALERTS' AND COLUMN_NAME = 'HE_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_USER_ALERTS ADD HE_FLAG VARCHAR2(1)';
	commit;
  end if;
end;
/

COMMENT ON COLUMN CB_USER_ALERTS.HE_FLAG IS 'Stores 1 or 0';


--STARTS ADDING COLUMN TO CB_USER_ALERTS TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_USER_ALERTS' AND COLUMN_NAME = 'OR_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_USER_ALERTS ADD OR_FLAG VARCHAR2(1)';
	commit;
  end if;
end;
/

COMMENT ON COLUMN CB_USER_ALERTS.OR_FLAG IS 'Stores 1 or 0';
