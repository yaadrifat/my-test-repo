--STARTS DELETING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_27');
  if (v_record_exists = 1) then
      DELETE FROM CB_ALERT_CONDITIONS where FK_CODELST_ALERT = (SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='alert' AND CODELST_SUBTYP='alert_27');
	commit;
  end if;
end;
/
--END--

