create or replace  PACKAGE   "PKG_DATEUTIL" IS

  FUNCTION f_get_dateformat  return Varchar2;

  FUNCTION f_get_timeformat  return Varchar2;
  
  FUNCTION F_GET_DATETIMEFORMAT  RETURN VARCHAR2;
     
  FUNCTION f_get_null_date_str return Varchar2;

  FUNCTION f_get_first_dayofyear return Date;

  FUNCTION f_get_first_dayofyear_str return Varchar;

  FUNCTION F_GET_FUTURE_NULL_DATE_STR RETURN VARCHAR2;
  
  FUNCTION f_get_dateformat_ui  return Varchar2;
  

END PKG_DATEUTIL;
/