set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_SHIPMENT.SHIPMENT_CONFIRM';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_SHIPMENT.SHIPMENT_CONFIRM','Shipment Confirmation');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_SHIPMENT.SHIPMENT_CONFIRM_BY';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_SHIPMENT.SHIPMENT_CONFIRM_BY','CBU Shipment Confirmed By');
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from AUDIT_MAPPEDVALUES
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_SHIPMENT.SHIPMENT_CONFIRM_ON';
  if (v_record_exists = 0) then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values 
      (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_SHIPMENT.SHIPMENT_CONFIRM_ON','CBU Shipment Confirmed By Date/Time');
	commit;
  end if;
end;
/
update audit_mappedvalues set to_value= 'Visible to TC' where mapping_type = 'Field' and from_value = 'CB_NOTES.VISIBILITY';
update audit_mappedvalues set to_value= 'Flag' where mapping_type = 'Field' and from_value = 'CB_NOTES.FLAG_FOR_LATER';
update audit_mappedvalues set to_value= 'Request Status' where mapping_type = 'Field' and from_value = 'ER_ORDER.ORDER_STATUS';
update audit_mappedvalues set to_value= 'Last Request Status Date' where mapping_type = 'Field' and from_value = 'ER_ORDER.ORDER_STATUS_DATE';
update audit_mappedvalues set to_value= 'Request Date' where mapping_type = 'Field' and from_value = 'ER_ORDER_HEADER.ORDER_OPEN_DATE';
update audit_mappedvalues set to_value= 'Actual Ship Date' where mapping_type = 'Field' and from_value = 'CB_SHIPMENT.SHIPMENT_DATE';
update audit_mappedvalues set to_value= 'Submitted By Date' where mapping_type = 'Field' and from_value = 'ER_ORDER.CORD_AVAIL_CONFIRM_DATE';
update audit_mappedvalues set to_value= 'Submitted By Date' where mapping_type = 'Field' and from_value = 'ER_ORDER.CORD_AVAILABILITY_CONFORMEDON';
update audit_mappedvalues set to_value= 'Last Reviewed By' where mapping_type = 'Field' and from_value = 'ER_ORDER.ORDER_LAST_VIEWED_BY';
update audit_mappedvalues set to_value= 'Last Reviewed Date/Time' where mapping_type = 'Field' and from_value = 'ER_ORDER.ORDER_LAST_VIEWED_DATE';
update audit_mappedvalues set to_value= 'Submitted By' where mapping_type = 'Field' and from_value = 'ER_ORDER.CORD_AVAILABILITY_CONFORMEDBY';
update audit_mappedvalues set to_value= 'Assigned Date' where mapping_type = 'Field' and from_value = 'ER_ORDER.ORDER_ASSIGNED_DATE';
update audit_mappedvalues set to_value= 'Priority' where mapping_type = 'Field' and from_value = 'ER_ORDER.ORDER_PRIORITY';
commit;
