--START UPDATE ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'date_range'
    AND codelst_subtyp = 'past_month';
  If (v_column_exists = 1) Then
	Update Er_Codelst Set Codelst_Desc = 'Previous Month'  where codelst_type = 'date_range'
    AND codelst_subtyp = 'past_month';
  commit;
   end if;
End;
/


--START UPDATE ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'qury_build_rprt'
    AND codelst_subtyp = 'antigenrpt';
  If (V_Column_Exists = 1) Then
	Update Er_Codelst Set Codelst_Desc = 'CBU HLA Report' Where Codelst_Type = 'qury_build_rprt' And Codelst_Subtyp = 'antigenrpt';
  commit;
   end if;
End;
/

--START UPDATE ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'qury_build_rprt'
    AND codelst_subtyp = 'idsummaryrpt';
  If (V_Column_Exists = 1) Then
	Update Er_Codelst Set Codelst_Desc = 'CBU ID Summary Report' Where Codelst_Type = 'qury_build_rprt' And Codelst_Subtyp = 'idsummaryrpt';
  commit;
   end if;
End;
/


--START UPDATE ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'qury_build_rprt'
    AND codelst_subtyp = 'quicksummaryrpt';
  If (V_Column_Exists = 1) Then
	Update Er_Codelst Set Codelst_Desc = 'OR Request and Shipment Details Report' Where Codelst_Type = 'qury_build_rprt' And Codelst_Subtyp = 'quicksummaryrpt';
  commit;
   end if;
End;
/


--START UPDATE ER_CODELST TABLE--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from er_codelst
    where codelst_type = 'qury_build_rprt'
    AND codelst_subtyp = 'licenselgblerpt';
  If (V_Column_Exists = 1) Then
	Update Er_Codelst Set Codelst_Desc = 'CBU Characteristics Report' Where Codelst_Type = 'qury_build_rprt' And Codelst_Subtyp = 'licenselgblerpt';
  commit;
   end if;
End;
/


--START UPDATE ER_OBJECT_SETTINGS--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    From ER_OBJECT_SETTINGS
    Where OBJECT_NAME = 'query_build_menu'
    AND OBJECT_SUBTYPE = 'simp_cbu_rpt';
  If (V_Column_Exists = 1) Then
	Update ER_OBJECT_SETTINGS Set Object_Visible = 0 Where OBJECT_NAME = 'query_build_menu'
    AND OBJECT_SUBTYPE = 'simp_cbu_rpt';
  commit;
   end if;
End;
/

--START UPDATE ER_OBJECT_SETTINGS--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    From ER_OBJECT_SETTINGS
    Where OBJECT_NAME = 'top_menu'
    AND OBJECT_SUBTYPE = 'query_build_menu';
  If (V_Column_Exists = 1) Then
	Update ER_OBJECT_SETTINGS Set OBJECT_DISPLAYTEXT = 'Simple CBU Reports' Where OBJECT_NAME = 'top_menu'
    AND OBJECT_SUBTYPE = 'query_build_menu';
  commit;
   end if;
End;
/


INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,279,8,'08_UPDATE_SCRIPT.sql',sysdate,'9.0.0 B#637-ET051');
commit;