ALTER TRIGGER CB_CORD_MINIMUM_CRITERIA_BI0 DISABLE;
ALTER TRIGGER CB_CORD_MINIMUM_CRITERIA_AU1 DISABLE;
ALTER TRIGGER CB_CORD_MINIMUM_CRITERIA_AI0 DISABLE;
--STARTS ADDING COLUMN SECOND_REVIEW_CONFIRMED TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'VIAB_TEST_TIMING';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(VIAB_TEST_TIMING NUMBER(10,0))';
  end if;
end;
/
--END--
--STARTS ADDING COLUMN SECOND_REVIEW_CONFIRMED TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'VIAB_PERCENTAGE_FLAG';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(VIAB_PERCENTAGE_FLAG NUMBER(10,0))';
  end if;
end;
/
--END--
COMMENT ON COLUMN CB_CORD_MINIMUM_CRITERIA.VIAB_TEST_TIMING IS 'This is the column to store the viability test timing';
COMMENT ON COLUMN CB_CORD_MINIMUM_CRITERIA.VIAB_PERCENTAGE_FLAG IS 'This is the column to store the viabiluty acception criteria';

ALTER TRIGGER CB_CORD_MINIMUM_CRITERIA_BI0 ENABLE;
ALTER TRIGGER CB_CORD_MINIMUM_CRITERIA_AU1 ENABLE;
ALTER TRIGGER CB_CORD_MINIMUM_CRITERIA_AI0 ENABLE;


--Insert into Er_Codelst--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'viab_dec'
    AND codelst_subtyp = 'viab_accept';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'viab_dec','viab_accept','Viability result is ≥ 75% or other potency measures are acceptable', 'N',1,'Y',NULL, SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'viab_dec'
    AND codelst_subtyp = 'viab_notaccept';
  if (v_record_exists = 0) then
     INSERT INTO 
  ER_CODELST(PK_CODELST,CODELST_TYPE,CODELST_SUBTYP,CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,CREATOR,CREATED_ON,LAST_MODIFIED_DATE,IP_ADD) 
  VALUES(SEQ_ER_CODELST.nextval,'viab_dec','viab_notaccept','Viability testing not done or potency measures are unacceptable', 'N',2,'Y',NULL, SYSDATE,SYSDATE,NULL);
	commit;
  end if;
end;
/
--end--

INSERT INTO track_patches 
VALUES(seq_track_patches.nextval,277,4,'04_ALTER_CB_MINIMUM_CRITERIA.sql',sysdate,'9.0.0 B#637-ET051');
commit;