set define off;
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    From Audit_Mappedvalues
    where MAPPING_TYPE = 'Field' and FROM_VALUE = 'CB_NOTES.REPLY';
  If (V_Record_Exists = 0) Then
      Insert into AUDIT_MAPPEDVALUES (PK_AUDITMAPPEDVALUE,MODULE_NAME,MAPPING_TYPE,FROM_VALUE,TO_VALUE) values (SEQ_AUDIT_MAPPEDVALUES.nextval,'MODCDRPF','Field','CB_NOTES.REPLY','Reply Note');
	commit;
  end if;
end;
/