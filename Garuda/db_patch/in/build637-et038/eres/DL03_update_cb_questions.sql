--start update cb_questions table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='bovine_insulin_since_1980_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>If Yes - Bovine insulin was prescribed for diabetes and may transmit CJD &#8594; Defer.<br/><br/>If Bank did not ask or Mother did not answer after May 25, 2005 &#8594; Incomplete Eligibility and requires Declaration of Urgent Medical Need.<br/><br/><u>Historical Evaluation</u><br/>Same as current evaluation and action.' where ques_code='bovine_insulin_since_1980_ind';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='unexpln_mouth_sores_wht_spt';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>36a, 36c, 36d, 36e, 36f, 36i<br/>If Yes &#8594; Cord Blood Bank Medical Director should evaluate response and defer donation if yes answer reveals signs or symptoms indicative of HIV/AIDS.<br/><br/>36b, 36g, 36h<br/>If Yes &#8594; defer.<br/><br/>If Bank did not ask or Mother did not answer on or after August 27, 2007 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u> Historical Evaluation</u><br/>36a-36h. Previous questions contained same physician evaluation and deferral assessment for HIV/AIDS infection. consider responses as equivalent.<br/>36i. Question added October 1, 2011. If your bank did not include assessment of maternal donr infection during pregnancy via interview or other medical record review, question 36i must be answered with a response of Bank did not ask.' where ques_code='unexpln_mouth_sores_wht_spt';
commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from cb_questions
    where ques_code='unexpln_1m_lump_nk_apit_grn_mult_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_help='<u>Current Evaluation and Action</u><br/>36a, 36c, 36d, 36e, 36f, 36i<br/>If Yes &#8594; Cord Blood Bank Medical Director should evaluate response and defer donation if yes answer reveals signs or symptoms indicative of HIV/AIDS.<br/><br/>36b, 36g, 36h<br/>If Yes &#8594; defer.<br/><br/>If Bank did not ask or Mother did not answer on or after August 27, 2007 &#8594; Incomplete Eligibility and requires documentation of urgent medical need.<br/><br/><u>Historical Evaluation</u><br/>36a-36h. Previous questions contained same physician evaluation and deferral assessment for HIV/AIDS infection. consider responses as equivalent.<br/>36i. Question added October 1, 2011. If your bank did not include assessment of maternal donr infection during pregnancy via interview or other medical record review, question 36i must be answered with a response of Bank did not ask.' where ques_code='unexpln_1m_lump_nk_apit_grn_mult_ind';
commit;
  end if;
end;
/
