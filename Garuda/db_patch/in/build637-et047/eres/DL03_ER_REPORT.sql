set define off;
delete er_repxsl where pk_repxsl in (186,188);
commit;


----UPDATE IDM For DUMN SQL------


Update ER_REPORT set REP_SQL = 'SELECT  CRD.CORD_ISBI_DIN_CODE ISTBTDIN,CRD.CORD_REGISTRY_ID REGID,CRD.CORD_LOCAL_CBU_ID LOCALCBUID,F_CORD_ADD_IDS(~1) ADDIDS,CORD_ID_NUMBER_ON_CBU_BAG,SITE.SITE_NAME SITENAME,SITE.SITE_ID SITEID,f_get_dynamicformdata(~1,''~2'') IDM,F_GET_FORM_TYP_DT(~1,''~2'') IDM_FORM_DATA,f_get_dynamicformgrp(''~2'') IDM_GRP,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1'') idm_notes, f_get_patientid(~1) PATIENT_ID FROM CB_CORD CRD, er_site site WHERE CRD.FK_CBB_ID=SITE.PK_SITE AND CRD.PK_CORD=~1' where pk_report = 188;
/
	COMMIT;


	Update ER_REPORT set REP_SQL_CLOB = 'SELECT  CRD.CORD_ISBI_DIN_CODE ISTBTDIN,CRD.CORD_REGISTRY_ID REGID,CRD.CORD_LOCAL_CBU_ID LOCALCBUID,F_CORD_ADD_IDS(~1) ADDIDS,CORD_ID_NUMBER_ON_CBU_BAG,SITE.SITE_NAME SITENAME,SITE.SITE_ID SITEID,f_get_dynamicformdata(~1,''~2'') IDM,F_GET_FORM_TYP_DT(~1,''~2'') IDM_FORM_DATA,f_get_dynamicformgrp(''~2'') IDM_GRP,f_cbu_notes(~1,F_CODELST_ID(''note_cat'',''infct_dis_mark''),''1'') idm_notes, f_get_patientid(~1) PATIENT_ID FROM CB_CORD CRD, er_site site WHERE CRD.FK_CBB_ID=SITE.PK_SITE AND CRD.PK_CORD=~1' where pk_report = 188;
/
	COMMIT;
