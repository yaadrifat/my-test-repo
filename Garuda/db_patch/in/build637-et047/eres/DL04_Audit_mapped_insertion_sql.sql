DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from audit_mappedvalues
    where FROM_VALUE ='CB_FINAL_REV_UPLOAD_INFO';
  
  if (v_record_exists = 0) then
      insert into audit_mappedvalues 
			(
			PK_AUDITMAPPEDVALUE,
			MODULE_NAME,
			MAPPING_TYPE,
			FROM_VALUE,
			TO_VALUE) values(
			SEQ_AUDIT_MAPPEDVALUES.nextval,
			'MODCDRPF',
			'Table',
			'CB_FINAL_REV_UPLOAD_INFO',
			'Final Reviewed Uploaded Document'
			);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from audit_mappedvalues
    where FROM_VALUE ='CB_FINAL_REV_UPLOAD_INFO.PK_FINAL_REV_UPLOAD_INFO';
  
  if (v_record_exists = 0) then
		insert into audit_mappedvalues 
		(
		PK_AUDITMAPPEDVALUE,
		MODULE_NAME,
		MAPPING_TYPE,
		FROM_VALUE,
		TO_VALUE) values(
		SEQ_AUDIT_MAPPEDVALUES.nextval,
		'MODCDRPF',
		'Field',
		'CB_FINAL_REV_UPLOAD_INFO.PK_FINAL_REV_UPLOAD_INFO',
		'Final Reviewed Uploaded Document Id'
		);
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from audit_mappedvalues
    where FROM_VALUE ='CB_FINAL_REV_UPLOAD_INFO.ENTITY_ID';
  
  if (v_record_exists = 0) then
		insert into audit_mappedvalues 
			(
			PK_AUDITMAPPEDVALUE,
			MODULE_NAME,
			MAPPING_TYPE,
			FROM_VALUE,
			TO_VALUE) values(
			SEQ_AUDIT_MAPPEDVALUES.nextval,
			'MODCDRPF',
			'Field',
			'CB_FINAL_REV_UPLOAD_INFO.ENTITY_ID',
			'Entity Id'
			);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from audit_mappedvalues
    where FROM_VALUE ='CB_FINAL_REV_UPLOAD_INFO.ENTITY_TYPE';
  
  if (v_record_exists = 0) then
	insert into audit_mappedvalues 
		(
		PK_AUDITMAPPEDVALUE,
		MODULE_NAME,
		MAPPING_TYPE,
		FROM_VALUE,
		TO_VALUE) values(
		SEQ_AUDIT_MAPPEDVALUES.nextval,
		'MODCDRPF',
		'Field',
		'CB_FINAL_REV_UPLOAD_INFO.ENTITY_TYPE',
		'Entity Type'
		);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from audit_mappedvalues
    where FROM_VALUE ='CB_FINAL_REV_UPLOAD_INFO.FK_CB_UPLOAD_INFO';
  
  if (v_record_exists = 0) then
		insert into audit_mappedvalues 
		(
		PK_AUDITMAPPEDVALUE,
		MODULE_NAME,
		MAPPING_TYPE,
		FROM_VALUE,
		TO_VALUE) values(
		SEQ_AUDIT_MAPPEDVALUES.nextval,
		'MODCDRPF',
		'Field',
		'CB_FINAL_REV_UPLOAD_INFO.FK_CB_UPLOAD_INFO',
		'CBU Uploaded Info'
		);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from audit_mappedvalues
    where FROM_VALUE ='CB_FINAL_REV_UPLOAD_INFO.FK_CB_FINAL_CBU_REVIEW';
  
  if (v_record_exists = 0) then
	insert into audit_mappedvalues 
		(
		PK_AUDITMAPPEDVALUE,
		MODULE_NAME,
		MAPPING_TYPE,
		FROM_VALUE,
		TO_VALUE) values(
		SEQ_AUDIT_MAPPEDVALUES.nextval,
		'MODCDRPF',
		'Field',
		'CB_FINAL_REV_UPLOAD_INFO.FK_CB_FINAL_CBU_REVIEW',
		'Final CBU Review'
		);
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from audit_mappedvalues
    where FROM_VALUE ='CB_FINAL_REV_UPLOAD_INFO.CREATOR';
  
  if (v_record_exists = 0) then
	  insert into audit_mappedvalues 
		(
		PK_AUDITMAPPEDVALUE,
		MODULE_NAME,
		MAPPING_TYPE,
		FROM_VALUE,
		TO_VALUE) values(
		SEQ_AUDIT_MAPPEDVALUES.nextval,
		'MODCDRPF',
		'Field',
		'CB_FINAL_REV_UPLOAD_INFO.CREATOR',
		'Created By'
		);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from audit_mappedvalues
    where FROM_VALUE ='CB_FINAL_REV_UPLOAD_INFO.CREATED_ON';
  
  if (v_record_exists = 0) then
	 insert into audit_mappedvalues 
		(
		PK_AUDITMAPPEDVALUE,
		MODULE_NAME,
		MAPPING_TYPE,
		FROM_VALUE,
		TO_VALUE) values(
		SEQ_AUDIT_MAPPEDVALUES.nextval,
		'MODCDRPF',
		'Field',
		'CB_FINAL_REV_UPLOAD_INFO.CREATED_ON',
		'Created On'
		);
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from audit_mappedvalues
    where FROM_VALUE ='CB_FINAL_REV_UPLOAD_INFO.IP_ADD';
  
  if (v_record_exists = 0) then
		insert into audit_mappedvalues 
		(
		PK_AUDITMAPPEDVALUE,
		MODULE_NAME,
		MAPPING_TYPE,
		FROM_VALUE,
		TO_VALUE) values(
		SEQ_AUDIT_MAPPEDVALUES.nextval,
		'MODCDRPF',
		'Field',
		'CB_FINAL_REV_UPLOAD_INFO.IP_ADD',
		'IP Address'
		);
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from audit_mappedvalues
    where FROM_VALUE ='CB_FINAL_REV_UPLOAD_INFO.LAST_MODIFIED_BY';
  
  if (v_record_exists = 0) then
		insert into audit_mappedvalues 
		(
		PK_AUDITMAPPEDVALUE,
		MODULE_NAME,
		MAPPING_TYPE,
		FROM_VALUE,
		TO_VALUE) values(
		SEQ_AUDIT_MAPPEDVALUES.nextval,
		'MODCDRPF',
		'Field',
		'CB_FINAL_REV_UPLOAD_INFO.LAST_MODIFIED_BY',
		'Last Modified By'
		);
	commit;
  end if;
end;
/


DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from audit_mappedvalues
    where FROM_VALUE ='CB_FINAL_REV_UPLOAD_INFO.LAST_MODIFIED_DATE';
  
  if (v_record_exists = 0) then
		insert into audit_mappedvalues 
		(
		PK_AUDITMAPPEDVALUE,
		MODULE_NAME,
		MAPPING_TYPE,
		FROM_VALUE,
		TO_VALUE) values(
		SEQ_AUDIT_MAPPEDVALUES.nextval,
		'MODCDRPF',
		'Field',
		'CB_FINAL_REV_UPLOAD_INFO.LAST_MODIFIED_DATE',
		'Last Modified Date'
		);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from audit_mappedvalues
    where FROM_VALUE ='CB_FINAL_REV_UPLOAD_INFO.RID';
  
  if (v_record_exists = 0) then
		insert into audit_mappedvalues 
		(
		PK_AUDITMAPPEDVALUE,
		MODULE_NAME,
		MAPPING_TYPE,
		FROM_VALUE,
		TO_VALUE) values(
		SEQ_AUDIT_MAPPEDVALUES.nextval,
		'MODCDRPF',
		'Field',
		'CB_FINAL_REV_UPLOAD_INFO.RID',
		'RID'
		);
	commit;
  end if;
end;
/

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from audit_mappedvalues
    where FROM_VALUE ='CB_FINAL_REV_UPLOAD_INFO.ATTACHMENT_TYPE';
  
  if (v_record_exists = 0) then
	insert into audit_mappedvalues 
	(
	PK_AUDITMAPPEDVALUE,
	MODULE_NAME,
	MAPPING_TYPE,
	FROM_VALUE,
	TO_VALUE) values(
	SEQ_AUDIT_MAPPEDVALUES.nextval,
	'MODCDRPF',
	'Field',
	'CB_FINAL_REV_UPLOAD_INFO.ATTACHMENT_TYPE',
	'Attachment Type'
	);
	commit;
  end if;
end;
/


