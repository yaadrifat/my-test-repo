create or replace
PROCEDURE SP_WORKFLOW_TEMP AS
OUTSTR VARCHAR2(100 BYTE);
var_order_id varchar2(10 CHAR);
var_order_type varchar2(10 CHAR);
v_entity_order number;
BEGIN
select pk_codelst into v_entity_order from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER';
for ORDERS in (SELECT ORDER_ID,ORDER_TYPE FROM ER_ORDER_TEMP)
loop
  var_order_id :=orders.order_id;
  var_order_type :=orders.order_type;
  SP_RESULTS_RECEIVED(VAR_ORDER_ID);
  sp_resolve_orders(var_order_id,var_order_type);
  sp_alerts(var_order_id,v_entity_order,0);
 sp_workflow(var_order_id,var_order_type,1,outstr);
 delete from er_order_temp where order_id=var_order_id;
 END LOOP;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
  DBMS_OUTPUT.PUT_LINE('Data Not Found');
END SP_WORKFLOW_TEMP;
/