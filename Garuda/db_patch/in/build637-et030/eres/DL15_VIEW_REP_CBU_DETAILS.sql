set define off;
CREATE OR REPLACE FORCE VIEW "ERES"."REP_CBU_DETAILS"
AS
  SELECT pk_cord,
    cord_registry_id,
    registry_maternal_id,
    cord_local_cbu_id,
    maternal_local_id,
    CORD_ISBI_DIN_CODE,
    CORD_ID_NUMBER_ON_CBU_BAG,
    F_CODELST_DESC(a.FK_CORD_BABY_GENDER_ID) gender,
    ROWTOCOL('SELECT F_CODELST_DESC(race.fk_codelst) raceval FROM cb_multiple_values race WHERE race.entity_id in ('
    ||a.pk_cord
    ||') and race.fk_codelst_type in (''race'') and race.fk_codelst is not null and race.entity_type=''CBU''',',') RACE,
    f_getuser(a.CREATOR) createdBy,
    TO_CHAR(a.created_on,'Mon DD, YYYY') createdon,
    f_getuser(a.last_modified_by) lastmodifiedby,
    TO_CHAR(a.last_modified_date,'Mon DD, YYYY') last_modified_dt,
    TO_CHAR(a.cord_baby_birth_date,'Mon DD, YYYY') baby_birth_dt,
    TO_CHAR(g.spec_collection_date,'Mon DD, YYYY') collec_dt,
    f_codelst_desc(a.FK_CODELST_ETHNICITY) ethnicity,
    f.CONV_FIL_PAP_SAMP filter_paper_sample,
    f.CONV_RPC_PELLETS rbc_pellates,
    f.CONV_EXTRACT_DNA exacted_dna,
    f.CONV_SERUM_SAMP serum_sample,
    f.CONV_PLASMA_SAMP plasma_sample,
    f.CONV_NO_OF_SEG conv_no_of_seg,
    f.CONV_NONVIABLE_CELL non_viable_cel,
    f.CONV_VIABLE_CELLSAMP conv_viable_cellsamp,
    f.CONV_MATR_SERSAMP conv_matr_sersamp,
    f.CONV_MATR_PLASAMP conv_matr_plasamp,
    f.CONV_MATR_EXTDNA_SAMP conv_matr_extdna_samp,
    f.CONV_MISCMATR_SAMP conv_miscmatr_samp,
    f.NO_VIA_CEL_ALI nonviable_final_prod,
    b.site_name cbbid,
    b.site_id cbb_id,
    f_codelst_desc(fk_cbu_stor_loc) storage_loc,
    c.site_name cbu_collection_site,
    f_codelst_desc(fk_cord_cbu_eligible_status) eligible_status,
    f_codelst_desc(fk_cord_cbu_lic_status) lic_status,
    TO_CHAR(prcsng_start_date,'Mon DD, YYYY') prcsng_start_date,
    prcsng_start_time,
    frz_time,
    f_codelst_desc(fk_cord_bact_cul_result) bact_culture,
    bact_comment,
    TO_CHAR(frz_date,'Mon DD, YYYY')frz_date,
    f_codelst_desc(fk_cord_fungal_cul_result) fungal_culture,
    TO_CHAR(a.bact_cult_date,'Mon DD, YYYY') bact_cul_strt_dt,
    TO_CHAR(a.fung_cult_date,'Mon DD, YYYY') fung_cul_strt_dt,
    fung_comment,
    f_codelst_desc(fk_cord_abo_blood_type) abo_blood_type,
    f_codelst_desc(hemoglobin_scrn) hemoglobin_scrn,
    f_codelst_desc(fk_cord_rh_type) rh_type,
    e.proc_name proc_name,
    TO_CHAR(e.proc_start_date,'Mon DD, YYYY') proc_start_date,
    TO_CHAR(e.proc_termi_date,'Mon DD, YYYY') proc_termi_date,
    f_codelst_desc(d.fk_proc_meth_id) processing,
    f_codelst_desc(d.fk_if_automated) automated_type,
    d.other_processing other_processing,
    f_codelst_desc(d.fk_product_modification) product_modification,
    d.other_prod_modi other_product_modi,
    f_codelst_desc(d.fk_stor_method) storage_method,
    f_codelst_desc(d.fk_freez_manufac) freezer_manufact,
    d.other_freez_manufac other_freezer_manufact,
    f_codelst_desc(d.fk_frozen_in) frozen_in,
    d.other_frozen_cont other_frozen_cont,
    f_codelst_desc(d.fk_num_of_bags) no_of_bags,
    d.cryobag_manufac cryobag_manufac,
    d.CRYOBAG_MANUFAC2 cryobag_manufac2,
    f_codelst_desc(d.fk_bagtype) bagtype,
    f_codelst_desc(d.fk_bag_1_type) bag1type,
    f_codelst_desc(d.fk_bag_2_type) bag2type,
    d.other_bag_type other_bag,
    d.THOU_UNIT_PER_ML_HEPARIN_PER heparin_thou_per,
    d.THOU_UNIT_PER_ML_HEPARIN heparin_thou_ml,
    d.FIVE_UNIT_PER_ML_HEPARIN_PER heparin_five_per,
    d.FIVE_UNIT_PER_ML_HEPARIN heparin_five_ml,
    d.TEN_UNIT_PER_ML_HEPARIN_PER heparin_ten_per,
    d.TEN_UNIT_PER_ML_HEPARIN heparin_ten_ml,
    d.SIX_PER_HYDRO_STARCH_PER heparin_six_per,
    d.SIX_PER_HYDROXYETHYL_STARCH heparin_six_ml,
    d.CPDA_PER cpda_per,
    d.CPDA cpda_ml,
    d.CPD_PER cpd_per,
    d.CPD cpd_ml,
    d.ACD_PER acd_per,
    d.ACD acd_ml,
    d.OTHER_ANTICOAGULANT_PER othr_anti_per,
    d.OTHER_ANTICOAGULANT othr_anti_ml,
    d.SPECIFY_OTH_ANTI speci_othr_anti,
    d.HUN_PER_DMSO_PER hun_dmso_per,
    d.HUN_PER_DMSO hun_dmso_ml,
    d.HUN_PER_GLYCEROL_PER hun_glycerol_per,
    d.HUN_PER_GLYCEROL hun_glycerol_ml,
    d.TEN_PER_DEXTRAN_40_PER ten_dextran_40_per,
    d.TEN_PER_DEXTRAN_40 ten_dextran_40_ml,
    d.FIVE_PER_HUMAN_ALBU_PER five_human_albu_per,
    d.FIVE_PER_HUMAN_ALBU five_human_albu_ml,
    d.TWEN_FIVE_HUM_ALBU_PER twentyfive_human_albu_per,
    d.TWEN_FIVE_HUM_ALBU twentyfive_human_albu_ml,
    d.PLASMALYTE_PER plasmalyte_per,
    d.PLASMALYTE plasmalyte_ml,
    d.OTH_CRYOPROTECTANT_PER othr_cryoprotectant_per,
    d.OTH_CRYOPROTECTANT othr_cryoprotectant_ml,
    d.SPEC_OTH_CRYOPRO spec_othr_cryoprotectant,
    d.FIVE_PER_DEXTROSE_PER five_dextrose_per,
    d.FIVE_PER_DEXTROSE five_dextrose_ml,
    d.POINNT_NINE_PER_NACL_PER point_nine_nacl_per,
    d.POINNT_NINE_PER_NACL point_nine_nacl_ml,
    d.OTH_DILUENTS_PER othr_diluents_per,
    d.OTH_DILUENTS othr_diluents_ml,
    d.SPEC_OTH_DILUENTS spec_othr_diluents,
    d.OTHER_BAG_TYPE1 OTHER_BAG_TYPE1,
    d.OTHER_BAG_TYPE2 OTHER_BAG_TYPE2,
    a.PRODUCT_CODE productcode,
    f_codelst_desc(d.fk_stor_temp) storage_temperature,
    d.max_value max_vol1,
    d.max_value2 max_vol2,
    f_codelst_desc(d.fk_contrl_rate_freezing) ctrl_rate_freezing,
    d.NO_OF_INDI_FRAC INDIVIDUAL_FRAC,
    d.NO_OF_VIABLE_SMPL_FINAL_PROD VIABLE_SAMP_FINAL_PROD,
    f_getuser(d.creator) proc_creator,
    TO_CHAR(d.created_on,'Mon DD, YYYY') proc_created_on,
    f_getuser(d.last_modified_by) proc_last_modby,
    TO_CHAR(d.last_modified_date,'Mon DD, YYYY') proc_last_moddt,
    f.filt_pap_avail filter_paper,
    f.RBC_PEL_AVAIL rpc_pellets,
    f.NO_EXT_DNA_ALI extr_dna,
    f.NO_SER_ALI serum_aliquotes,
    F.NO_PLAS_ALI PLASMA_ALIQUOTES,
    F.NO_NON_VIA_ALI NONVIABLE_ALIQUOTES,
    F.NO_OF_SEG_AVAIL NO_OF_SEGMENTS,
    f.CBU_NO_REP_ALT_CON no_of_oth_rep_alliq_alter_cond,
    F.CBU_OT_REP_CON_FIN NO_OF_OTH_REP_ALLIQUOTS_F_PROD,
    F.NO_SER_MAT_ALI NO_OF_SERUM_MATER_ALIQUOTS,
    F.NO_PLAS_MAT_ALI NO_OF_PLASMA_MATER_ALIQUOTS,
    F.NO_EXT_DNA_MAT_ALI NO_OF_EXTR_DNA_MATER_ALIQUOTS,
    f.SI_NO_MISC_MAT NO_OF_CELL_MATER_ALIQUOTS,
    f_getuser(f.creator) sample_creator,
    TO_CHAR(f.created_on,'Mon DD, YYYY') sample_created_on,
    TO_CHAR(f.last_modified_date,'Mon DD, YYYY') sample_last_moddt,
    f_getuser(f.last_modified_by) sample_last_modby,
    ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_INELIGIBLE_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD in ('
    ||a.pk_cord
    ||')',',') INELIGIBLEREASON,
    ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD in ('
    ||a.pk_cord
    ||')',',') UNLICENSEREASON,
    a.CORD_ELIGIBLE_ADDITIONAL_INFO ELIGCOMMENTS,
    f_lic_eligi_moddt(a.pk_cord, 'eligibility') ELGIBLE_MODI_DT,
    f_lic_eligi_moddt(a.pk_cord, 'licence') LIC_MODI_DT,
    DECODE(a.FK_CORD_CBU_ELIGIBLE_STATUS, (f_codelst_id('eligibility','eligible')),'0', (f_codelst_id('eligibility','ineligible')),'1', (f_codelst_id('eligibility','incomplete')),'2', (f_codelst_id('eligibility','not_appli_prior')),'3') ELI_FLAG,
    DECODE(a.FK_CORD_CBU_LIC_STATUS, (f_codelst_id('licence','licensed')),'0', (f_codelst_id('licence','unlicensed')),'1') LIC_FLAG,
    H.CBU_STATUS_DESC NMDPSTATUS,
    I.CBU_STATUS_DESC CBUSTATUS,
    ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_INELIGIBLE_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD in ('
    ||a.pk_cord
    ||')',' <br></br>') INELIGIBLEREASON1,
    ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_CORD CORD INNER JOIN CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD in ('
    ||a.pk_cord
    ||')',' <br></br>') UNLICENSEREASON1
  FROM cb_cord a
  LEFT OUTER JOIN er_site b
  ON (a.fk_cbb_id = b.pk_site)
  LEFT OUTER JOIN er_site c
  ON (a.fk_cbu_coll_site = c.pk_site)
  LEFT OUTER JOIN cbb_processing_procedures_info d
  ON (a.fk_cbb_procedure = d.fk_processing_id)
  LEFT OUTER JOIN CBB_PROCESSING_PROCEDURES E
  ON (A.FK_CBB_PROCEDURE = E.PK_PROC )
    --LEFT OUTER JOIN CB_ENTITY_SAMPLES F
  LEFT OUTER JOIN
    (SELECT *
    FROM CB_ENTITY_SAMPLES
    WHERE pk_entity_samples IN
      (SELECT MAX(pk_entity_samples) FROM cb_entity_samples GROUP BY entity_id
      )
    ) F
  ON (f.entity_id=a.pk_cord)
  LEFT OUTER JOIN ER_SPECIMEN G
  ON (a.FK_SPECIMEN_ID=G.PK_SPECIMEN)
  LEFT OUTER JOIN CB_CBU_STATUS H
  ON (A.CORD_NMDP_STATUS=H.PK_CBU_STATUS)
  LEFT OUTER JOIN CB_CBU_STATUS I
  ON (A.FK_CORD_CBU_STATUS=I.PK_CBU_STATUS);
