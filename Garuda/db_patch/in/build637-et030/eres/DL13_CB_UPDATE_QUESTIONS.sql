--Start updating cb_Questions --
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_type6';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ques_desc='Type of Metabolic/Storage Disease (Baby''s Father''s Sibling)' where QUES_CODE = 'inher_met_stg_dises_type6';
commit;
  end if;
end;
/
--Question 19.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chron_bld_trans_ind_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG='1' where QUES_CODE = 'chron_bld_trans_ind_rel';
commit;
  end if;
end;
/
--Question 20.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hemolytic_anemia_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG='1' where QUES_CODE = 'hemolytic_anemia_rel';
commit;
  end if;
end;
/
--Question 21.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'spleen_removed_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG='1' where QUES_CODE = 'spleen_removed_rel';
commit;
  end if;
end;
/
--Question 22.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'gallbladder_removed_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG='1' where QUES_CODE = 'gallbladder_removed_rel';
commit;
  end if;
end;
/
--Question 23.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cruetz_jakob_dis_rel';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set ASSESMENT_FLAG='1' where QUES_CODE = 'cruetz_jakob_dis_rel';
commit;
  end if;
end;
/
--End--
--Question 19.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_trans_ind_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set UNEXPECTED_RESPONSE_VALUE = RESPONSE_VALUE
   where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='chron_bld_trans_ind_rel') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
   commit;
  end if;
end;
/
--End--
--Question 20.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set UNEXPECTED_RESPONSE_VALUE = RESPONSE_VALUE
   where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='hemolytic_anemia_rel') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
   commit;
  end if;
end;
/
--End--
--Question 21.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set UNEXPECTED_RESPONSE_VALUE = RESPONSE_VALUE
   where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='spleen_removed_rel') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
   commit;
  end if;
end;
/
--End--
--Question 22.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set UNEXPECTED_RESPONSE_VALUE = RESPONSE_VALUE
   where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='gallbladder_removed_rel') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
   commit;
  end if;
end;
/
--End--
--Question 23.a--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_FORM_QUESTIONS
    where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cruetz_jakob_dis_rel')
    and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
  if (v_record_exists = 1) then
	update CB_FORM_QUESTIONS set UNEXPECTED_RESPONSE_VALUE = RESPONSE_VALUE
   where FK_QUESTION = (select PK_QUESTIONS from CB_QUESTIONS where QUES_CODE='cruetz_jakob_dis_rel') and FK_FORM = (select pk_form from cb_forms where forms_desc='FMHQ' and version='N2D');
   commit;
  end if;
end;
/
--End--
