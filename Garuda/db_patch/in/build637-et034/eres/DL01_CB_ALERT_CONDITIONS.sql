--Final CBU Review not complete Alert

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_05')
    AND COLUMN_NAME = 'ORDER_TYPE';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET LOGICAL_OPERATOR='AND' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_05')
    AND COLUMN_NAME = 'ORDER_TYPE';
	commit;
  end if;
end;
/
--END--




--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_05')
    AND COLUMN_NAME = 'ORDER_STATUS';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_05'),'PRE',null,'ER_ORDER','ORDER_STATUS','(Select PK_CODELST from er_codelst where codelst_type = ''order_status'' AND codelst_subtyp = ''SHIP_SCH'')','=',null,null,'PK_ORDER = #keycolumn',null,sysdate,null,sysdate,null,null,null);
	commit;
  end if;
end;
/
--END--

--======================================================================================================================================================



--Confirm Shipment not complete after 2 days Alert

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_07')
    AND COLUMN_NAME = 'ORDER_TYPE';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET CONDITION_VALUE='(Select PK_CODELST from er_codelst where codelst_type = ''order_type'' AND codelst_subtyp = ''OR'')' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_07')
    AND COLUMN_NAME = 'ORDER_TYPE';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_07')
    AND COLUMN_NAME = 'FK_SHIPMENT_TYPE';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET CONDITION_VALUE='(Select PK_CODELST from er_codelst where codelst_type = ''shipment_type'' AND codelst_subtyp = ''CBU'')' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_07')
    AND COLUMN_NAME = 'FK_SHIPMENT_TYPE';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_07')
    AND CONDITION_VALUE = '-2';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET ARITHMETIC_OPERATOR='=' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_07')
    AND CONDITION_VALUE = '-2';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_07')
    AND COLUMN_NAME = 'SHIPMENT_CONFIRM' and CONDITION_VALUE=' IS NULL';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET LOGICAL_OPERATOR='AND' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_07')
    AND COLUMN_NAME = 'SHIPMENT_CONFIRM' and CONDITION_VALUE=' IS NULL';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_07')
     AND COLUMN_NAME = 'ORDER_STATUS';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_07'),'PRE',null,'ER_ORDER','ORDER_STATUS','(Select PK_CODELST from er_codelst where codelst_type = ''order_status'' AND codelst_subtyp = ''SHIP_SCH'')','=',null,null,'PK_ORDER = #keycolumn',null,sysdate,null,sysdate,null,null,null);
	commit;
  end if;
end;
/
--END--

--======================================================================================================================================================

--Confirm Shipment not complete after 7 days Alert

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_08')
    AND COLUMN_NAME = 'ORDER_TYPE';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET CONDITION_VALUE='(Select PK_CODELST from er_codelst where codelst_type = ''order_type'' AND codelst_subtyp = ''OR'')' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_08')
    AND COLUMN_NAME = 'ORDER_TYPE';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_08')
    AND COLUMN_NAME = 'FK_SHIPMENT_TYPE';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET CONDITION_VALUE='(Select PK_CODELST from er_codelst where codelst_type = ''shipment_type'' AND codelst_subtyp = ''CBU'')' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_08')
    AND COLUMN_NAME = 'FK_SHIPMENT_TYPE';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_08')
    AND CONDITION_VALUE = '-7';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET ARITHMETIC_OPERATOR='=' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_08')
    AND CONDITION_VALUE = '-7';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_08')
    AND COLUMN_NAME = 'SHIPMENT_CONFIRM' and CONDITION_VALUE=' IS NULL';
  if (v_record_exists = 1) then
      UPDATE 
  CB_ALERT_CONDITIONS SET LOGICAL_OPERATOR='AND' where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_08')
    AND COLUMN_NAME = 'SHIPMENT_CONFIRM' and CONDITION_VALUE=' IS NULL';
	commit;
  end if;
end;
/
--END--



--STARTS UPDATING RECORD FROM CB_ALERT_CONDITIONS TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from CB_ALERT_CONDITIONS
    where FK_CODELST_ALERT = (Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_08')
     AND COLUMN_NAME = 'ORDER_STATUS';
  if (v_record_exists = 0) then
      INSERT INTO CB_ALERT_CONDITIONS(PK_ALERT_CONDITIONS,FK_CODELST_ALERT,CONDITION_TYPE,START_BRACE,TABLE_NAME,COLUMN_NAME,CONDITION_VALUE,ARITHMETIC_OPERATOR,LOGICAL_OPERATOR,END_BRACE,KEY_CONDITION,CREATOR,CREATED_ON,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,IP_ADD,DELETEDFLAG,RID) VALUES(SEQ_CB_ALERT_CONDITIONS.nextval,(Select PK_CODELST from er_codelst where codelst_type = 'alert' AND codelst_subtyp = 'alert_08'),'PRE',null,'ER_ORDER','ORDER_STATUS','(Select PK_CODELST from er_codelst where codelst_type = ''order_status'' AND codelst_subtyp = ''SHIP_SCH'')','=',null,null,'PK_ORDER = #keycolumn',null,sysdate,null,sysdate,null,null,null);
	commit;
  end if;
end;
/
--END--

--======================================================================================================================================================




