-----Sql to create Table CB_CORD_REF_HLA_ESB_DATA
Declare 
V_TAB_EXIST NUMBER(10) := 0;
BEGIN
SELECT COUNT(*) INTO V_TAB_EXIST FROM USER_TABLES WHERE TABLE_NAME like '%CB_FINAL_REV_UPLOAD_INFO%';
IF V_TAB_EXIST=0 THEN
   EXECUTE IMMEDIATE '

Create table CB_FINAL_REV_UPLOAD_INFO
(
PK_FINAL_REV_UPLOAD_INFO NUMBER(10,0) PRIMARY KEY,
ENTITY_ID NUMBER(10,0),
ENTITY_TYPE NUMBER(10,0),
FK_CB_UPLOAD_INFO NUMBER(10,0),
FK_CB_FINAL_CBU_REVIEW NUMBER(10,0),
CREATOR NUMBER(10),
CREATED_ON DATE,
IP_ADD VARCHAR2(15),
LAST_MODIFIED_BY NUMBER(10),
LAST_MODIFIED_DATE DATE,
RID NUMBER(10),
DELETEDFLAG VARCHAR2(1)
)';
END IF;
END;
/
 COMMENT ON COLUMN "CB_FINAL_REV_UPLOAD_INFO"."PK_FINAL_REV_UPLOAD_INFO" IS 'Primary Key';
 
 COMMENT ON COLUMN "CB_FINAL_REV_UPLOAD_INFO"."ENTITY_ID" IS 'This column will store the entity id';
 
 COMMENT ON COLUMN "CB_FINAL_REV_UPLOAD_INFO"."ENTITY_TYPE" IS 'This column will store type of entity.';
 
 COMMENT ON COLUMN "CB_FINAL_REV_UPLOAD_INFO"."FK_CB_UPLOAD_INFO" IS 'DCMS Attachment ID';
 
 COMMENT ON COLUMN "CB_FINAL_REV_UPLOAD_INFO"."FK_CB_FINAL_CBU_REVIEW" IS 'Stores the cord references to multiple doc';  
 
 COMMENT ON COLUMN "CB_FINAL_REV_UPLOAD_INFO"."CREATOR" IS 'This column is used for Audit Trail. The Creator identifies the user who created this row.';
 
 COMMENT ON COLUMN "CB_FINAL_REV_UPLOAD_INFO"."CREATED_ON" IS 'This column is used for Audit Trail. Stores the date on which this row was created.';
 
 COMMENT ON COLUMN "CB_FINAL_REV_UPLOAD_INFO"."LAST_MODIFIED_BY" IS 'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
 
 COMMENT ON COLUMN "CB_FINAL_REV_UPLOAD_INFO"."LAST_MODIFIED_DATE" IS 'This column is used for Audit Trail. Stores the date on which this row was last modified.';
 
 COMMENT ON COLUMN "CB_FINAL_REV_UPLOAD_INFO"."IP_ADD" IS 'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
 
 COMMENT ON COLUMN "CB_FINAL_REV_UPLOAD_INFO"."DELETEDFLAG" IS 'This column denotes whether record is deleted. ';
 
 COMMENT ON COLUMN "CB_FINAL_REV_UPLOAD_INFO"."RID" IS 'This column is used for the audit trail. The RID uniquely identifies the row in the database.';

 COMMENT ON TABLE "CB_FINAL_REV_UPLOAD_INFO"  IS 'This table stores final cbu reviewed documents information.';
 


--------------------Sequence--------------
DECLARE
  v_seq_exists number := 0;  
BEGIN
	SELECT COUNT(*) into v_seq_exists
	FROM USER_SEQUENCES 
	WHERE SEQUENCE_NAME='SEQ_CB_FINAL_REV_UPLOAD_INFO';

	if (v_seq_exists = 0) then
		execute immediate 'CREATE  SEQUENCE SEQ_CB_FINAL_REV_UPLOAD_INFO MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1';
		dbms_output.put_line('Sequence SEQ_CB_FINAL_REV_UPLOAD_INFO is created in eres schema');
	else
		dbms_output.put_line('Sequence SEQ_CB_FINAL_REV_UPLOAD_INFO exists in eres schema');
	end if;
end;
/
commit;