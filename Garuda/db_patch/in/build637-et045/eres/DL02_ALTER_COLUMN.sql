--STARTS ADD THE COLUMN CRI_REQ_ELG_QUEST TO CBB--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where  TABLE_NAME = 'CBB'
    and column_name = 'CRI_REQ_ELG_QUEST';
  if (v_column_exists = 0) then
      execute immediate 'alter table CBB add (CRI_REQ_ELG_QUEST  VARCHAR2(1 BYTE))';
      COMMIT;
  end if;
end;
/
--END--
--starts comment for THE COLUMN CRI_REQ_ELG_QUEST TO CBB TABLE--
comment on column cbb.cri_req_elg_quest  is 'This field indicates that CRI is required Eligible questionnaire or not';
COMMIT;
--end--


