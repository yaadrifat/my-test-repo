

CREATE TABLE "ERES"."CB_NEW_FINAL_DECL_ELIG"
  (
    "PK_NEW_FINAL_DECL_ELIG"        Number(10,0),
    "MRQ_NEW_QUES_1"                Varchar2(1 Byte),
    "MRQ_NEW_QUES_1_A"              Varchar2(1 Byte),
    "MRQ_NEW_QUES_1_AD_DETAIL"      Varchar2(500 Char),
    "MRQ_NEW_QUES_2"                Varchar2(1 Byte),
    "MRQ_NEW_QUES_2_AD_DETAIL"      Varchar2(500 Char),
    "MRQ_NEW_QUES_3"                Varchar2(1 Byte),
    "MRQ_NEW_QUES_3_AD_DETAIL"      Varchar2(500 Char),
    "PHY_NEW_FIND_QUES_4"           Varchar2(1 Byte),
    "PHY_NEW_FIND_QUES_4_A"         Varchar2(1 Byte),
    "PHY_NEW_FIND_QUES_4_AD_DETAIL" Varchar2(500 Char),
    "PHY_NEW_FIND_QUES_5"           Varchar2(1 Byte),
    "PHY_NEW_FIND_QUES_5_AD_DETAIL" Varchar2(500 Char),
    "PHY_NEW_FIND_QUES_6"           Varchar2(1 Byte),
    "PHY_NEW_FIND_QUES_6_A"         Varchar2(1 Byte),
    "PHY_NEW_FIND_QUES_6_B"         Varchar2(1 Byte),
    "PHY_NEW_FIND_QUES_6_C"         Varchar2(1 Byte),
    "PHY_NEW_FIND_QUES_6_D"         Varchar2(1 Byte),
    "PHY_NEW_FIND_QUES_6_E"         Varchar2(1 Byte),
    "PHY_NEW_FIND_QUES_6_F"         Varchar2(1 Byte),
    "PHY_NEW_FIND_QUES_6_G"         Varchar2(1 Byte),
    "PHY_NEW_FIND_QUES_6_H"         Varchar2(1 Byte),
    "PHY_NEW_FIND_QUES_6_I"         Varchar2(1 Byte),
    "PHY_NEW_FIND_QUES_6_AD_DETAIL" Varchar2(500 Char),
    "IDM_NEW_QUES_7"                Varchar2(1 Byte),
    "IDM_NEW_QUES_7_A"              Varchar2(1 Byte),
    "IDM_NEW_QUES_7_AD_DETAIL"      Varchar2(500 Char),
    "IDM_NEW_QUES_8"                Varchar2(1 Byte),
    "IDM_NEW_QUES_8_A"              Varchar2(1 Byte),
    "IDM_NEW_QUES_8_AD_DETAIL"      Varchar2(500 Char),
    "IDM_NEW_QUES_9"                Varchar2(1 Byte),
    "IDM_NEW_QUES_9_AD_DETAIL"      Varchar2(500 Char),
    "CREATOR"                   NUMBER(10,0),
    "CREATED_ON" DATE,
    "LAST_MODIFIED_BY" NUMBER(10,0),
    "LAST_MODIFIED_DATE" DATE,
    "IP_ADD"      VARCHAR2(15 BYTE),
    "DELETEDFLAG" VARCHAR2(1 BYTE),
    "RID"         NUMBER(15,0),
    "ENTITY_ID"   NUMBER(10,0),
    "ENTITY_TYPE" Number(20,0)
   
  );
  
COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PK_NEW_FINAL_DECL_ELIG"
IS
  'Primary Key';
  Comment On Column "ERES"."CB_NEW_FINAL_DECL_ELIG"."MRQ_NEW_QUES_1"
IS
  'This column will store MRQ Question 1';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."MRQ_NEW_QUES_1_A"
IS
  'This column will store MRQ Question 1 A';
  
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."MRQ_NEW_QUES_1_AD_DETAIL"
IS
  'This column will store MRQ Question AD details';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."MRQ_NEW_QUES_2"
IS
  'This column will store MRQ Question 2';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."MRQ_NEW_QUES_2_AD_DETAIL"
IS
  'This column will store MRQ Question 2 AD details';
 
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."MRQ_NEW_QUES_3"
IS
  'This column will store MRQ Question 2';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."MRQ_NEW_QUES_3_AD_DETAIL"
IS
  'This column will store MRQ Question 2 AD details';
 
 
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_4"
IS
  'This column will store Physical finding Question 4';

COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_4_A"
Is
  'This column will store Physical finding Question 4 A';

  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_4_AD_DETAIL"
IS
  'This column will store Physical finding Question 4 AD Detail';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_5"
IS
  'This column will store Physical finding Question 5';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_5_AD_DETAIL"
Is
  'This column will store Physical finding Question 5 AD Detail';

 COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_6"
Is
  'This column will store Physical finding Question 6';
  
 COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_6_A"
Is
  'This column will store Physical finding Question 6 A';

COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_6_B"
Is
  'This column will store Physical finding Question 6 B';


  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_6_C"
IS
  'This column will store Physical finding Question 6 C';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_6_D"
IS
  'This column will store Physical finding Question 6 D';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_6_E"
IS
  'This column will store Physical finding Question 6 E';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_6_F"
IS
  'This column will store Physical finding Question 6 F';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_6_G"
IS
  'This column will store Physical finding Question 6 G';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_6_H"
IS
  'This column will store Physical finding Question 6 H';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_6_I"
IS
  'This column will store Physical finding Question 6 I';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."PHY_NEW_FIND_QUES_6_AD_DETAIL"
IS
  'This column will store Physical finding Question 6 AD Detail';
 
  
COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."IDM_NEW_QUES_7"
IS
  'This column will store IDM Question 7';
  
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."IDM_NEW_QUES_7_A"
IS
  'This column will store IDM Question 7 A';
  
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."IDM_NEW_QUES_7_AD_DETAIL"
IS
  'This column will store IDM Question 7 AD Detail';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."IDM_NEW_QUES_8"
IS
  'This column will store IDM Question 8';
    COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."IDM_NEW_QUES_8_A"
Is
  'This column will store IDM Question 8 A';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."IDM_NEW_QUES_8_AD_DETAIL"
IS
  'This column will store IDM Question 8 AD Detail';

COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."IDM_NEW_QUES_9"
Is
  'This column will store IDM Question 9';
    
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."IDM_NEW_QUES_9_AD_DETAIL"
IS
  'This column will store IDM Question 9 AD Detail';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."CREATOR"
IS
  'This column is used for Audit Trail. The Creator identifies the user who created this row.';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."CREATED_ON"
IS
  'This column is used for Audit Trail. Stores the date on which this row was created.';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."LAST_MODIFIED_BY"
IS
  'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."LAST_MODIFIED_DATE"
IS
  'This column is used for Audit Trail. Stores the date on which this row was last modified.';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."IP_ADD"
IS
  'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."DELETEDFLAG"
IS
  'This column denotes whether record is deleted. ';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."RID"
IS
  'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."ENTITY_ID"
IS
  'pk of the entity';
  COMMENT ON COLUMN "ERES"."CB_NEW_FINAL_DECL_ELIG"."ENTITY_TYPE"
IS
  'Id of the entity like CBU, Doner etc';
  COMMENT ON TABLE "ERES"."CB_NEW_FINAL_DECL_ELIG"
Is
  'This table stores new question response realted to final declaration of eligibility.';



--------------------Sequence--------------
DECLARE
  v_seq_exists number := 0;  
BEGIN
	SELECT COUNT(*) into v_seq_exists
	FROM USER_SEQUENCES 
	WHERE SEQUENCE_NAME='SEQ_CB_NEW_FINAL_DECL_ELIG';

	if (v_seq_exists = 0) then
		execute immediate 'CREATE  SEQUENCE SEQ_CB_NEW_FINAL_DECL_ELIG MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1';
		dbms_output.put_line('Sequence SEQ_CB_NEW_FINAL_DECL_ELIG is created in eres schema');
	else
		dbms_output.put_line('Sequence SEQ_CB_NEW_FINAL_DECL_ELIG exists in eres schema');
	end if;
end;
/
commit;