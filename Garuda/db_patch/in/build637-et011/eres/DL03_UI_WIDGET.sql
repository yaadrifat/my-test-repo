
 --Sql for MRQ widget 
--UIwidget entry

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where NAME = 'MRQ Widget'
    AND WIDGET_DIV_ID = 'mrqinfoparent';
  if (v_record_exists = 0) then
 Insert Into Ui_Widget (Pk_Widget,State,Help_Url,Name,Widget_Div_Id,Description,Is_Authenticated,Is_Minizable,Is_Closable,Is_Resizeable,Is_Dragable,Is_Mouse_Hover_Enabled,Created_On,Last_Modified_Date,Version_No,Resize_Min_Height,Resize_Min_Width,Resize_Max_Height,Resize_Max_Width) 
values (SEQ_UI_WIDGET.nextval,null,null,'MRQ Widget','mrqinfoparent','MRQ Widget _Description','1','1','1','1','1','1',null,null,null,100,400,1024,1024);
	commit;
  end if;
end;
/



-- for entry in UI_CONTAINER_WIDGET_MAP 
DECLARE
  v_record_exists number:= 0; 
   v_record_exists_1 number :=0;
  BEGIN
   Select  count(*) into  v_record_exists  
 from   UI_WIDGET where name like 'MRQ Widget' ;
 if(v_record_exists != 0) then
  Select  count(*) into  v_record_exists_1  
    from UI_CONTAINER_WIDGET_MAP a, UI_WIDGET b where a.FK_WIDGET_ID = b.PK_WIDGET and b.name like 'MRQ Widget' ;
 if(v_record_exists_1 = 0) then
  Insert into UI_CONTAINER_WIDGET_MAP (PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values
(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from UI_WIDGET_CONTAINER where container_name='CordEntry_Next'),
(select PK_WIDGET from ui_widget where name='MRQ Widget'));
	commit;
  end if;
  end if;
 end;
 /
 
--Sql for FMHQ widget 
--UIwidget entry

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where NAME = 'FMHQ Widget'
    AND WIDGET_DIV_ID = 'fmhqinfoparent';
  if (v_record_exists = 0) then
 Insert Into Ui_Widget (Pk_Widget,State,Help_Url,Name,Widget_Div_Id,Description,Is_Authenticated,Is_Minizable,Is_Closable,Is_Resizeable,Is_Dragable,Is_Mouse_Hover_Enabled,Created_On,Last_Modified_Date,Version_No,Resize_Min_Height,Resize_Min_Width,Resize_Max_Height,Resize_Max_Width) 
values (SEQ_UI_WIDGET.nextval,null,null,'FMHQ Widget','fmhqinfoparent','FMHQ Widget _Description','1','1','1','1','1','1',null,null,null,100,400,1024,1024);
	commit;
  end if;
end;
/



-- for entry in UI_CONTAINER_WIDGET_MAP 
DECLARE
  v_record_exists number:= 0; 
   v_record_exists_1 number :=0;
  BEGIN
   Select  count(*) into  v_record_exists  
 from   UI_WIDGET where name like 'FMHQ Widget' ;
 if(v_record_exists != 0) then
  Select  count(*) into  v_record_exists_1  
    from UI_CONTAINER_WIDGET_MAP a, UI_WIDGET b where a.FK_WIDGET_ID = b.PK_WIDGET and b.name like 'FMHQ Widget' ;
 if(v_record_exists_1 = 0) then
  Insert into UI_CONTAINER_WIDGET_MAP (PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values
(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from UI_WIDGET_CONTAINER where container_name='CordEntry_Next'),
(select PK_WIDGET from ui_widget where name='FMHQ Widget'));
	commit;
  end if;
  end if;
 end;
 /
--Sql for IDM widget 
--UIwidget entry

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where NAME = 'IDM Widget'
    AND WIDGET_DIV_ID = 'idminfoparent';
  if (v_record_exists = 0) then
 Insert Into Ui_Widget (Pk_Widget,State,Help_Url,Name,Widget_Div_Id,Description,Is_Authenticated,Is_Minizable,Is_Closable,Is_Resizeable,Is_Dragable,Is_Mouse_Hover_Enabled,Created_On,Last_Modified_Date,Version_No,Resize_Min_Height,Resize_Min_Width,Resize_Max_Height,Resize_Max_Width) 
values (SEQ_UI_WIDGET.nextval,null,null,'IDM Widget','idminfoparent','IDM Widget _Description','1','1','1','1','1','1',null,null,null,100,400,1024,1024);
	commit;
  end if;
end;
/



-- for entry in UI_CONTAINER_WIDGET_MAP 
DECLARE
  v_record_exists number:= 0; 
   v_record_exists_1 number :=0;
  BEGIN
   Select  count(*) into  v_record_exists  
 from   UI_WIDGET where name like 'IDM Widget' ;
 if(v_record_exists != 0) then
  Select  count(*) into  v_record_exists_1  
    from UI_CONTAINER_WIDGET_MAP a, UI_WIDGET b where a.FK_WIDGET_ID = b.PK_WIDGET and b.name like 'IDM Widget' ;
 if(v_record_exists_1 = 0) then
  Insert into UI_CONTAINER_WIDGET_MAP (PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values
(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from UI_WIDGET_CONTAINER where container_name='CordEntry_Next'),
(select PK_WIDGET from ui_widget where name='IDM Widget'));
	commit;
  end if;
  end if;
 end;
 /
 
 --Sql for HLA widget 
--UIwidget entry

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where NAME = 'HLA Widget'
    AND WIDGET_DIV_ID = 'hlainfoparent';
  if (v_record_exists = 0) then
 Insert Into Ui_Widget (Pk_Widget,State,Help_Url,Name,Widget_Div_Id,Description,Is_Authenticated,Is_Minizable,Is_Closable,Is_Resizeable,Is_Dragable,Is_Mouse_Hover_Enabled,Created_On,Last_Modified_Date,Version_No,Resize_Min_Height,Resize_Min_Width,Resize_Max_Height,Resize_Max_Width) 
values (SEQ_UI_WIDGET.nextval,null,null,'HLA Widget','hlainfoparent','HLA Widget _Description','1','1','1','1','1','1',null,null,null,100,400,1024,1024);
	commit;
  end if;
end;
/



-- for entry in UI_CONTAINER_WIDGET_MAP 
DECLARE
  v_record_exists number:= 0; 
   v_record_exists_1 number :=0;
  BEGIN
   Select  count(*) into  v_record_exists  
 from   UI_WIDGET where name like 'HLA Widget' ;
 if(v_record_exists != 0) then
  Select  count(*) into  v_record_exists_1  
    from UI_CONTAINER_WIDGET_MAP a, UI_WIDGET b where a.FK_WIDGET_ID = b.PK_WIDGET and b.name like 'HLA Widget' ;
 if(v_record_exists_1 = 0) then
  Insert into UI_CONTAINER_WIDGET_MAP (PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values
(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from UI_WIDGET_CONTAINER where container_name='CordEntry_Next'),
(select PK_WIDGET from ui_widget where name='HLA Widget'));
	commit;
  end if;
  end if;
 end;
 /
 
 --Sql for ELIGIBILITY widget 
--UIwidget entry

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where NAME = 'Eligibility Widget'
    AND WIDGET_DIV_ID = 'eliginfoparent';
  if (v_record_exists = 0) then
 Insert Into Ui_Widget (Pk_Widget,State,Help_Url,Name,Widget_Div_Id,Description,Is_Authenticated,Is_Minizable,Is_Closable,Is_Resizeable,Is_Dragable,Is_Mouse_Hover_Enabled,Created_On,Last_Modified_Date,Version_No,Resize_Min_Height,Resize_Min_Width,Resize_Max_Height,Resize_Max_Width) 
values (SEQ_UI_WIDGET.nextval,null,null,'Eligibility Widget','eliginfoparent','Eligibility Widget _Description','1','1','1','1','1','1',null,null,null,100,400,1024,1024);
	commit;
  end if;
end;
/



-- for entry in UI_CONTAINER_WIDGET_MAP 
DECLARE
  v_record_exists number:= 0; 
   v_record_exists_1 number :=0;
  BEGIN
   Select  count(*) into  v_record_exists  
 from   UI_WIDGET where name like 'Eligibility Widget' ;
 if(v_record_exists != 0) then
  Select  count(*) into  v_record_exists_1  
    from UI_CONTAINER_WIDGET_MAP a, UI_WIDGET b where a.FK_WIDGET_ID = b.PK_WIDGET and b.name like 'Eligibility Widget' ;
 if(v_record_exists_1 = 0) then
  Insert into UI_CONTAINER_WIDGET_MAP (PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values
(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from UI_WIDGET_CONTAINER where container_name='CordEntry_Next'),
(select PK_WIDGET from ui_widget where name='Eligibility Widget'));
	commit;
  end if;
  end if;
 end;
 /
  --Sql for Clinical Notes widget 
--UIwidget entry

DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from UI_WIDGET
    where NAME = 'Clinical Notes Widget'
    AND WIDGET_DIV_ID = 'clnotesinfoparent';
  if (v_record_exists = 0) then
 Insert Into Ui_Widget (Pk_Widget,State,Help_Url,Name,Widget_Div_Id,Description,Is_Authenticated,Is_Minizable,Is_Closable,Is_Resizeable,Is_Dragable,Is_Mouse_Hover_Enabled,Created_On,Last_Modified_Date,Version_No,Resize_Min_Height,Resize_Min_Width,Resize_Max_Height,Resize_Max_Width) 
values (SEQ_UI_WIDGET.nextval,null,null,'Clinical Notes Widget','clnotesinfoparent','Clinical Notes Widget _Description','1','1','1','1','1','1',null,null,null,100,400,1024,1024);
	commit;
  end if;
end;
/



-- for entry in UI_CONTAINER_WIDGET_MAP 
DECLARE
  v_record_exists number:= 0; 
   v_record_exists_1 number :=0;
  BEGIN
   Select  count(*) into  v_record_exists  
 from   UI_WIDGET where name like 'Clinical Notes Widget' ;
 if(v_record_exists != 0) then
  Select  count(*) into  v_record_exists_1  
    from UI_CONTAINER_WIDGET_MAP a, UI_WIDGET b where a.FK_WIDGET_ID = b.PK_WIDGET and b.name like 'Clinical Notes Widget' ;
 if(v_record_exists_1 = 0) then
  Insert into UI_CONTAINER_WIDGET_MAP (PK_PG_CONTI_WID,FK_CONTAINER_ID,FK_WIDGET_ID) values
(SEQ_UI_CONTAINER_WIDGET_MAP.nextval,(select PK_CONTAINER from UI_WIDGET_CONTAINER where container_name='CordEntry_Next'),
(select PK_WIDGET from ui_widget where name='Clinical Notes Widget'));
	commit;
  end if;
  end if;
 end;
 /