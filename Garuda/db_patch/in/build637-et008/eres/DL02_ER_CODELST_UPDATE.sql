--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'vaibility'
    AND codelst_subtyp = 'ehi_brom';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='Ethidium Bromide' where codelst_type = 'vaibility'
    AND codelst_subtyp = 'ehi_brom';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'storage_temp'
    AND codelst_subtyp = '>=-150to<=-135';
  if (v_record_exists = 1) then
     update er_codelst set codelst_desc = '> -150 to <= -135' where codelst_type = 'storage_temp' and codelst_subtyp = '>=-150to<=-135';

	commit;
  end if;
end;
/
--END--



