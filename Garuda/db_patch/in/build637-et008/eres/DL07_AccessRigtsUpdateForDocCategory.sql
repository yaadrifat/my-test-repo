--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where  ctrl_value ='CB_UNITCAT' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='NEV' where ctrl_value ='CB_UNITCAT' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/
---End--------

--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where ctrl_value ='CB_CBUINFOCAT' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='NEV' where ctrl_value ='CB_CBUINFOCAT' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/
---End--------

--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where ctrl_value ='CB_LABSUMCAT' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='NEV' where ctrl_value ='CB_LABSUMCAT' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/
---End--------

--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where ctrl_value ='CB_PROINFOCAT' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='NEV' where ctrl_value ='CB_PROINFOCAT' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/
---End--------

--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where ctrl_value ='CB_IDMCAT' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='NEV' where ctrl_value ='CB_IDMCAT' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/
---End--------

--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where ctrl_value ='CB_HEALTHCAT' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='NEV' where ctrl_value ='CB_HEALTHCAT' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/
---End--------

--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where ctrl_value ='CB_HLACAT' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='NEV' where ctrl_value ='CB_HLACAT' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/
---End--------

--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where ctrl_value ='CB_ELIGCAT' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='NEV' where ctrl_value ='CB_ELIGCAT' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/
---End--------


--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where ctrl_value ='CB_OTHRCAT' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='NEV' where ctrl_value ='CB_OTHRCAT' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/
---End--------


--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where ctrl_value ='CB_ASSMNTCAT' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='NEV' where ctrl_value ='CB_ASSMNTCAT' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/
---End--------


--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where ctrl_value ='CB_SHPMNTCAT' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='NEV' where ctrl_value ='CB_SHPMNTCAT' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/
---End--------

--Update ER_CTRLTAB -----

Set define off;
DECLARE
  v_no_of_records number := 0;
BEGIN
  Select count(*) into v_no_of_records
    from ER_CTRLTAB
    where ctrl_value ='CB_POSTSHIPCAT' and ctrl_key ='app_rights';
  if (v_no_of_records =1) then
	update ER_CTRLTAB set CTRL_CUSTOM_COL1='NEV' where ctrl_value ='CB_POSTSHIPCAT' and ctrl_key ='app_rights';
   commit;
  end if;
end;
/

Exec sp_clean_notes;

drop procedure sp_clean_notes;
commit;
/
---End--------

