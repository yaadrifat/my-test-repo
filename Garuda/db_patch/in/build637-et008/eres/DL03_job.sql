begin
 DBMS_SCHEDULER.drop_job (job_name => 'CREATE_WORKFLOW_JOB');
 end;
 /

begin
DBMS_SCHEDULER.create_job (
    job_name        => 'CREATE_WORKFLOW_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'begin sp_workflow_temp;CB_ALERTS_MAINTENANCE_PROC;end;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'freq=SECONDLY;INTERVAL=10',
    end_date        => NULL,
    enabled         => TRUE,
    comments        => 'Job to create workflow,alerts and change order status');
end;
/


begin
dbms_scheduler.run_job('CREATE_WORKFLOW_JOB');
end;
/


