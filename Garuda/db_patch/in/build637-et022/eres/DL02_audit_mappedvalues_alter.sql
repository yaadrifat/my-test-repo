update audit_mappedvalues set to_value= 'Required Clinical Information Status' where mapping_type = 'Field' and to_value='required clinical information task';
update audit_mappedvalues set to_value= 'Cord Blood Bank ID' where mapping_type = 'Field' and to_value='CORD Blood Bank ID';
update audit_mappedvalues set to_value= 'Funded CBU' where mapping_type = 'Field' and to_value='CBU fund';
commit;