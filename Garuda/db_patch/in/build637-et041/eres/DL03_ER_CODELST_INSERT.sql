--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='qury_build_rprt'
    AND CODELST_SUBTYP = 'quicksummaryrpt';
  if (v_record_exists = 0) then
      INSERT INTO 
		ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
		CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
		CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
		(SEQ_ER_CODELST.nextval,null,'qury_build_rprt','quicksummaryrpt','NMDP CBU Quick Summary','N',3,'Y',null,
		null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='qury_build_rprt'
    AND CODELST_SUBTYP = 'idsummaryrpt';
  if (v_record_exists = 0) then
      INSERT INTO 
		ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
		CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
		CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
		(SEQ_ER_CODELST.nextval,null,'qury_build_rprt','idsummaryrpt','NMDP CBU ID Summary','N',2,'Y',null,
		null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--


--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='qury_build_rprt'
    AND CODELST_SUBTYP = 'antigenrpt';
  if (v_record_exists = 0) then
      INSERT INTO 
		ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
		CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
		CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
		(SEQ_ER_CODELST.nextval,null,'qury_build_rprt','antigenrpt','NMDP CBU Antigens','N',1,'Y',null,
		null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--

--STARTS INSERTING RECORD INTO ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_CODELST
    where CODELST_TYPE ='qury_build_rprt'
    AND CODELST_SUBTYP = 'licenselgblerpt';
  if (v_record_exists = 0) then
      INSERT INTO 
		ER_CODELST (PK_CODELST,FK_ACCOUNT,CODELST_TYPE,CODELST_SUBTYP,
		CODELST_DESC,CODELST_HIDE,CODELST_SEQ,CODELST_MAINT,RID,CREATOR,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,
		CREATED_ON,	IP_ADD,	CODELST_CUSTOM_COL,CODELST_CUSTOM_COL1,CODELST_STUDY_ROLE) values
		(SEQ_ER_CODELST.nextval,null,'qury_build_rprt','licenselgblerpt','NMDP CBU Licensure And Eligibility Determination','N',4,'Y',null,
		null,null,sysdate,sysdate,null,null,null,null);
	commit;
  end if;
end;
/
--END--