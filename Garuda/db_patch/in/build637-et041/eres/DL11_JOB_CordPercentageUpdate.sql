

BEGIN
    SYS.DBMS_SCHEDULER.CREATE_JOB (
            job_name => 'ERES.CordPercentageUpdate',
            job_type => 'STORED_PROCEDURE',
            job_action => 'SP_UPDATE_CORD_PERCENTAGE',
            number_of_arguments => 0,
            start_date => '13-APR-13 11.30.37.812000 AM',
            repeat_interval => 'FREQ=SECONDLY; INTERVAL=10;',
            end_date => NULL,
            job_class => 'DEFAULT_JOB_CLASS',
            enabled => true,
            auto_drop => true,
            comments => 'Job to update percentage of those cords whose hla coming through ESB');
END;
/
