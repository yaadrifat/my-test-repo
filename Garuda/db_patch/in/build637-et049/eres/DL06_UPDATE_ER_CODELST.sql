
--STARTS UPDATE THE COLUMN CODELST_DESC IN ER_CODELST--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM ER_CODELST WHERE CODELST_TYPE='eligibility' AND CODELST_SUBTYP='not_appli_prior';
  
  IF (index_count = 1) THEN
    UPDATE ER_CODELST SET CODELST_DESC='Not Applicable' WHERE CODELST_TYPE='eligibility' AND CODELST_SUBTYP='not_appli_prior';
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/
COMMIT;

--ENDS UPDATE THE COLUMN CODELST_DESC IN ER_CODELST----


--STARTS UPDATE THE COLUMN CODELST_DESC IN ER_CODELST--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM ER_CODELST WHERE CODELST_TYPE='eligibility' AND CODELST_SUBTYP='not_appli_prior';
  
  IF (index_count = 1) THEN
    UPDATE ER_CODELST SET CODELST_DESC='Not Applicable' WHERE CODELST_TYPE='eligibility' AND CODELST_SUBTYP='not_appli_prior';
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/
COMMIT;

--ENDS UPDATE THE COLUMN CODELST_DESC IN ER_CODELST----
