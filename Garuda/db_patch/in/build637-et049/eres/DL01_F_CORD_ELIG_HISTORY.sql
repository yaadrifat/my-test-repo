CREATE OR REPLACE FUNCTION f_cord_eligble_history(pk_cord NUMBER) RETURN SYS_REFCURSOR  IS
v_elig_hty SYS_REFCURSOR;
BEGIN
    OPEN v_elig_hty FOR SELECT TO_CHAR(STATUS.STATUS_DATE,'Mon DD, YYYY') EH_DT,
                          F_GETUSER(CREATOR) CREATOR,
                          TO_CHAR(CREATED_ON,'Mon DD,YYYY') CREATED_ON,
                          F_CODELST_DESC(STATUS.FK_STATUS_VALUE) EH_STATUS,
                          ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_ENTITY_STATUS STAT INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE STAT.PK_ENTITY_STATUS in ('
                          ||STATUS.PK_ENTITY_STATUS
                          ||')',' <br></br>') EH_INELIGIBLEREASON,
                          STATUS.LICN_ELIG_CHANGE_REASON EH_COMMENTS,
                          DECODE(STATUS.FK_STATUS_VALUE, (f_codelst_id('eligibility','eligible')),'0', (f_codelst_id('eligibility','ineligible')),'1', (f_codelst_id('eligibility','incomplete')),'2', (f_codelst_id('eligibility','not_appli_prior')),'3') ELI_FLAG
                        FROM CB_ENTITY_STATUS STATUS
                        WHERE STATUS.ENTITY_ID     =pk_cord
                        AND STATUS.STATUS_TYPE_CODE='eligibility'
                        AND STATUS.FK_STATUS_VALUE!=-1
                        ORDER BY STATUS.STATUS_DATE,
                          STATUS.PK_ENTITY_STATUS;
RETURN v_elig_hty;
END;
/
