DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'nat_hiv_1_hcv_hbv_mpx_ind';
  if (v_record_exists = 1) then
	update cb_questions set ques_desc='NAT HIV-1 / HCV / HBV' where ques_code='nat_hiv_1_hcv_hbv_mpx_ind';
commit;
  end if;
end;
/


