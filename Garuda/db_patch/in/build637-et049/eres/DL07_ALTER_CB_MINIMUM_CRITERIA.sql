ALTER TRIGGER CB_CORD_MINIMUM_CRITERIA_BI0 DISABLE;
ALTER TRIGGER CB_CORD_MINIMUM_CRITERIA_AU1 DISABLE;
ALTER TRIGGER CB_CORD_MINIMUM_CRITERIA_AI0 DISABLE;
--STARTS ADDING COLUMN REVIEWED_BY TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'REVIEWED_BY';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(REVIEWED_BY NUMBER(10))';
  end if;
end;
/
--END--
--STARTS ADDING COLUMN REVIEWED_ON TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'REVIEWED_ON';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(REVIEWED_ON DATE)';
  end if;
end;
/
--END--
--STARTS ADDING COLUMN SECOND_REVIEW_BY TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'SECOND_REVIEW_BY';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(SECOND_REVIEW_BY NUMBER(10))';
  end if;
end;
/
--END--
--STARTS ADDING COLUMN SECOND_REVIEW_ON TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'SECOND_REVIEW_ON';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(SECOND_REVIEW_ON DATE)';
  end if;
end;
/
--END--
--STARTS ADDING COLUMN SECOND_REVIEW_CONFIRMED TO CB_CORD_MINIMUM_CRITERIA--
DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA'
    AND column_name = 'SECOND_REVIEW_CONFIRMED';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ERES.CB_CORD_MINIMUM_CRITERIA ADD(SECOND_REVIEW_CONFIRMED VARCHAR2(1 BYTE))';
  end if;
end;
/
--END--
COMMENT ON COLUMN CB_CORD_MINIMUM_CRITERIA.REVIEWED_BY IS 'This is the column to store the FIRST REVIEWER';
COMMENT ON COLUMN CB_CORD_MINIMUM_CRITERIA.REVIEWED_ON IS 'This is the column to store the FIRST REVIEW DATE';
COMMENT ON COLUMN CB_CORD_MINIMUM_CRITERIA.SECOND_REVIEW_BY IS 'This is the column to store the SECOND REVIEWER';
COMMENT ON COLUMN CB_CORD_MINIMUM_CRITERIA.SECOND_REVIEW_ON IS 'This is the column to store the SECOND REVIEW DATE';
COMMENT ON COLUMN CB_CORD_MINIMUM_CRITERIA.SECOND_REVIEW_CONFIRMED IS 'This is the column to store the SECOND REVIEW FLAG';

UPDATE CB_CORD_MINIMUM_CRITERIA SET REVIEWED_BY = CREATOR WHERE LAST_MODIFIED_BY IS NULL;
UPDATE CB_CORD_MINIMUM_CRITERIA SET REVIEWED_BY = LAST_MODIFIED_BY WHERE LAST_MODIFIED_BY IS NOT NULL;
UPDATE CB_CORD_MINIMUM_CRITERIA SET REVIEWED_ON = CREATED_ON WHERE LAST_MODIFIED_DATE IS NULL;
UPDATE CB_CORD_MINIMUM_CRITERIA SET REVIEWED_ON = LAST_MODIFIED_DATE WHERE LAST_MODIFIED_DATE IS NOT NULL;
commit;
ALTER TRIGGER CB_CORD_MINIMUM_CRITERIA_BI0 ENABLE;
ALTER TRIGGER CB_CORD_MINIMUM_CRITERIA_AU1 ENABLE;
ALTER TRIGGER CB_CORD_MINIMUM_CRITERIA_AI0 ENABLE;

