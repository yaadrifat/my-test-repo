/* This readMe is specific to Velos eResearch version 9.0 build637-et009 */

=====================================================================================================================================
Garuda :

	Regarding E Mail configuration:
	
		We are releasing script to store 'EmTrax application URL' and 'E mail From' data so that this data displays in the E mails sent by the EmTrax application.
		
		--STARTS INSERTING RECORD INTO ER_CTRLTAB TABLE--
		DECLARE
		  v_record_exists number := 0;  
		BEGIN
		  Select count(*) into v_record_exists
		    from er_ctrltab
		    where CTRL_VALUE = 'emtrax_url';
		  if (v_record_exists = 0) then
		      insert into er_ctrltab(PK_CTRLTAB,CTRL_VALUE,CTRL_DESC) values(seq_er_ctrltab.nextval,'emtrax_url','<Emtrax Application URL>');
			commit;
		  end if;
		end;
		/
		--END--
		
		--STARTS INSERTING RECORD INTO ER_CTRLTAB TABLE--
		DECLARE
		  v_record_exists number := 0;  
		BEGIN
		  Select count(*) into v_record_exists
		    from er_ctrltab
		    where CTRL_VALUE = 'email_from';
		  if (v_record_exists = 0) then
		      insert into er_ctrltab(PK_CTRLTAB,CTRL_VALUE,CTRL_DESC) values(seq_er_ctrltab.nextval,'email_from','<Email From>');
			commit;
		  end if;
		end;
		/
		--END--
		Please refer db_patch/in/build637-et009/eres/DL07_ER_CTRLTAB_INSERT.sql.
		Replace the <Emtrax Application URL>,<Email From> tags with appropriate values.
		
		Example:
		
			<Email From>:'customersupport@veloseresearch.com'.
			<Emtrax Application URL>:'http://192.168.192.250:8000/velos/jsp/nmdplogin.jsp'.
			
		Also we updated the Additional configuration.doc from CVS with this information.

=====================================================================================================================================
