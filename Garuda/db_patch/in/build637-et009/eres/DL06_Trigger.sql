create or replace
TRIGGER ER_ORDER_ACK0 BEFORE UPDATE OF FK_ORDER_RESOL_BY_CBB,FK_ORDER_RESOL_BY_TC ON ER_ORDER REFERENCING OLD AS OLDVAL NEW AS NEWVAL FOR EACH ROW
DECLARE
VAR_ACK_FLAG VARCHAR2(5);
VAR_CLOSED_ORDTYPE VARCHAR2(10);
VAR_ORDERTYPE VARCHAR2(10);
VAR_SITEID NUMBER;
VAR_NEW_CBU_STATUS NUMBER;
VAR_FK_CORD NUMBER;
VAR_CORDTYPE VARCHAR2(10);
VAR_ORDER_RESOLVED NUMBER;
BEGIN
   SELECT NVL(FK_SITE_ID,0) INTO VAR_SITEID FROM ER_ORDER_HEADER WHERE PK_ORDER_HEADER=:NEWVAL.FK_ORDER_HEADER;
  -- DBMS_OUTPUT.PUT_LINE(':NEWVAL.FK_ORDER_RESOL_BY_TC:::::'||:NEWVAL.FK_ORDER_RESOL_BY_TC);
   SELECT NVL(FK_CBU_STATUS,0) INTO VAR_NEW_CBU_STATUS FROM CB_CBU_STATUS WHERE PK_CBU_STATUS=:NEWVAL.FK_ORDER_RESOL_BY_TC;
--   DBMS_OUTPUT.PUT_LINE('VAR_NEW_CBU_STATUS:::::'||VAR_NEW_CBU_STATUS);
   SELECT NVL(ORDER_ENTITYID,0) INTO VAR_FK_CORD FROM ER_ORDER_HEADER WHERE PK_ORDER_HEADER=:NEWVAL.FK_ORDER_HEADER;
  -- DBMS_OUTPUT.PUT_LINE('VAR_FK_CORD:::::'||VAR_FK_CORD);
  SELECT PK_CODELST INTO VAR_ORDERTYPE FROM ER_CODELST WHERE CODELST_TYPE='entity_type' and CODELST_SUBTYP='ORDER';
   ----UPDATE CBU STATUS PART-------
    IF NVL(:NEWVAL.FK_ORDER_RESOL_BY_TC,0) <> 0 AND :NEWVAL.FK_ORDER_RESOL_BY_TC<>NVL(:OLDVAL.FK_ORDER_RESOL_BY_TC,0) AND VAR_NEW_CBU_STATUS <> 0 AND VAR_FK_CORD <> 0 THEN
            --DBMS_OUTPUT.PUT_LINE('INSIDE IF...................');
            UPDATE CB_CORD SET FK_CORD_CBU_STATUS=VAR_NEW_CBU_STATUS WHERE PK_CORD=VAR_FK_CORD;
            --DBMS_OUTPUT.PUT_LINE('AFTER UPDATE OF CORD STATUS...................');
            SELECT PK_CODELST INTO VAR_CORDTYPE FROM ER_CODELST WHERE CODELST_TYPE='entity_type' and CODELST_SUBTYP='CBU';
            INSERT INTO CB_ENTITY_STATUS(PK_ENTITY_STATUS,ENTITY_ID,FK_ENTITY_TYPE,STATUS_TYPE_CODE,FK_STATUS_VALUE,STATUS_DATE,STATUS_REMARKS,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,RID,DELETEDFLAG,LICN_ELIG_CHANGE_REASON) VALUES (SEQ_CB_ENTITY_STATUS.nextval,VAR_FK_CORD,VAR_CORDTYPE,'cord_status',VAR_NEW_CBU_STATUS,SYSDATE,NULL,0,SYSDATE,NULL,NULL,NULL,NULL,NULL,NULL);
   END IF;
  --DBMS_OUTPUT.PUT_LINE(':NEWVAL.FK_ORDER_RESOL_BY_TC:::::'||:NEWVAL.FK_ORDER_RESOL_BY_TC);
  --DBMS_OUTPUT.PUT_LINE(':OLDVAL.FK_ORDER_RESOL_BY_TC:::::'||:OLDVAL.FK_ORDER_RESOL_BY_TC);
   ----UPDATE ORDER STATUS---------
   IF NVL(:NEWVAL.FK_ORDER_RESOL_BY_TC,0) <> 0 AND :NEWVAL.FK_ORDER_RESOL_BY_TC<>NVL(:OLDVAL.FK_ORDER_RESOL_BY_TC,0) THEN
   --DBMS_OUTPUT.PUT_LINE('IN IF 1:::::');
    SELECT NVL(PK_CODELST,0) INTO VAR_ORDER_RESOLVED FROM ER_CODELST WHERE CODELST_TYPE='order_status' AND CODELST_SUBTYP='resolved';
    :NEWVAL.ORDER_STATUS:=VAR_ORDER_RESOLVED;
    :NEWVAL.ORDER_STATUS_DATE:=SYSDATE;
    INSERT INTO CB_ENTITY_STATUS(PK_ENTITY_STATUS,ENTITY_ID,FK_ENTITY_TYPE,STATUS_TYPE_CODE,FK_STATUS_VALUE,STATUS_DATE,STATUS_REMARKS,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,RID,DELETEDFLAG,LICN_ELIG_CHANGE_REASON) VALUES (SEQ_CB_ENTITY_STATUS.nextval,:newval.PK_ORDER,VAR_ORDERTYPE,'order_status',:newval.ORDER_STATUS,SYSDATE,NULL,0,SYSDATE,NULL,NULL,NULL,NULL,NULL,NULL);
    FOR RESOL IN (SELECT PK_CBU_STATUS FROM CB_CBU_STATUS WHERE CBU_STATUS_CODE IN('AG','CD','DD','OT','QR','RO','SO') AND CODE_TYPE='Resolution')
    LOOP
    --DBMS_OUTPUT.PUT_LINE('IN LOOP:::::');
    IF :NEWVAL.FK_ORDER_RESOL_BY_TC=RESOL.PK_CBU_STATUS THEN
    --DBMS_OUTPUT.PUT_LINE('IN IF2:::::');
      SELECT CODELST.PK_CODELST INTO VAR_CLOSED_ORDTYPE FROM ER_CODELST CODELST WHERE CODELST.CODELST_TYPE='order_status' and CODELST.CODELST_SUBTYP='close_ordr';
      :NEWVAL.ORDER_ACK_FLAG := 'Y';
      :NEWVAL.ORDER_ACK_DATE := SYSDATE;
      :NEWVAL.ORDER_ACK_BY :=0;
      :NEWVAL.ORDER_STATUS := VAR_CLOSED_ORDTYPE;
      INSERT INTO CB_ENTITY_STATUS(PK_ENTITY_STATUS,ENTITY_ID,FK_ENTITY_TYPE,STATUS_TYPE_CODE,FK_STATUS_VALUE,STATUS_DATE,STATUS_REMARKS,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,RID,DELETEDFLAG,LICN_ELIG_CHANGE_REASON) VALUES (SEQ_CB_ENTITY_STATUS.nextval,:newval.PK_ORDER,VAR_ORDERTYPE,'order_status',:newval.ORDER_STATUS,SYSDATE,NULL,0,SYSDATE,NULL,NULL,NULL,NULL,NULL,NULL);
      --DBMS_OUTPUT.PUT_LINE('END LOOP:::::');
    END IF;
    END LOOP;

   END IF;


    ---AUTO ACKNOWLEDGE PART-----
    IF NVL(:NEWVAL.FK_ORDER_RESOL_BY_CBB,0) > 0 OR NVL(:NEWVAL.FK_ORDER_RESOL_BY_TC,0) > 0 THEN
    --DBMS_OUTPUT.PUT_LINE('INSIDE FIRST IF...................'||VAR_SITEID);
    SELECT NVL(COMP_ACK_RES_TASK,'') INTO VAR_ACK_FLAG FROM CBB WHERE FK_SITE=VAR_SITEID;
    --DBMS_OUTPUT.PUT_LINE('VAR_ACK_FLAG:::::'||VAR_ACK_FLAG);
         IF VAR_ACK_FLAG = 1 THEN
            SELECT CODELST.PK_CODELST INTO VAR_CLOSED_ORDTYPE FROM ER_CODELST CODELST WHERE CODELST.CODELST_TYPE='order_status' and CODELST.CODELST_SUBTYP='close_ordr';
            :NEWVAL.ORDER_ACK_FLAG := 'Y';
            :NEWVAL.ORDER_ACK_DATE := SYSDATE;
            :NEWVAL.ORDER_ACK_BY :=0;
            :NEWVAL.ORDER_STATUS := VAR_CLOSED_ORDTYPE;
            INSERT INTO CB_ENTITY_STATUS(PK_ENTITY_STATUS,ENTITY_ID,FK_ENTITY_TYPE,STATUS_TYPE_CODE,FK_STATUS_VALUE,STATUS_DATE,STATUS_REMARKS,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,RID,DELETEDFLAG,LICN_ELIG_CHANGE_REASON) VALUES (SEQ_CB_ENTITY_STATUS.nextval,:newval.PK_ORDER,VAR_ORDERTYPE,'order_status',:newval.ORDER_STATUS,SYSDATE,NULL,0,SYSDATE,NULL,NULL,NULL,NULL,NULL,NULL);
         END IF;
   END IF;
  INSERT INTO CB_ALERTS_MAINTENANCE(ENTITY_ID,ENTITY_TYPE) VALUES(:NEWVAL.PK_ORDER,VAR_ORDERTYPE);
   EXCEPTION
   WHEN NO_DATA_FOUND THEN
   dbms_output.put_line ('INSIDE EXCEPTION...........DATA NOT FIND');
END;
/

create or replace
function datediff( p_what in varchar2,p_d1 in date,p_d2 in date ) return number
as
l_result number;
begin
  select (p_d2-p_d1) * decode( upper(p_what),'SS', 24*60*60, 'MI', 24*60, 'HH', 24, NULL ) into l_result from dual;
return l_result;
end;
/

create or replace
PROCEDURE SP_ALERTS(v_entity_id number,v_entity_type number) AS
TYPE CurTyp IS REF CURSOR;  -- define weak REF CURSOR type
cur_cv   CurTyp;  -- declare cursor variable
v_alertId number;
v_alert_title varchar2(4000);
v_alert_desc varchar2(4000);
v_alert_wording varchar2(4000);
v_alert_precondition varchar2(4000) :='';
v_col_name varchar2(4000);
v_alert_precond_cnt number :=0;
v_query varchar2(4000);
sql_fieldval varchar2(4000);
var_instance_cnt number:=0;
var_instance_cnt1 number:=0;
var_instance_cnt2 number:=0;
var_instance_cnt3 number:=0;
var_instance_cnt4 number:=0;
var_instance_cnt5 number:=0;
v_userId number;
v_cordregid varchar2(255);
V_ORDERTYPE VARCHAR2(255);
v_sampleatlab varchar2(1);
V_ALERTNAME VARCHAR2(255);
v_alerttype varchar2(255);
v_resol number:=0;
v_resoldesc VARCHAR2(50 CHAR):='N/A';
v_orderId number;
v_schshipdate varchar2(50);
v_temp number;
V_ALERT_ID_TEMP NUMBER;
v_entity_order number;
v_entity_cord number;
v_notify number;
v_mail number;
v_mailfrom varchar2(4000);
v_url varchar2(4000);
v_email_wording varchar2(4000);
BEGIN
select pk_codelst into v_notify from er_codelst where codelst_type='alert_type' and codelst_subtyp='notify';
select pk_codelst into v_mail from er_codelst where codelst_type='alert_type' and codelst_subtyp='mail';
select pk_codelst into v_entity_order from er_codelst where codelst_type='entity_type' and codelst_subtyp='ORDER';
select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';
select CTRL_DESC into v_url from er_ctrltab where CTRL_VALUE = 'emtrax_url';
select CTRL_DESC into v_mailfrom from er_ctrltab where CTRL_VALUE = 'email_from';
if v_entity_type=v_entity_order then
select f_get_codelstdesc(nvl(order_type,0)) into v_ordertype from er_order where pk_order=v_entity_id;
end if;
--DBMS_OUTPUT.PUT_LINE('v_entity_id::::::::::::::'||v_entity_id);
FOR CUR_ALERTS IN(select pk_codelst,CODELST_SUBTYP,codelst_desc,codelst_custom_col1,codelst_custom_col,codelst_type,CODELST_CUSTOM_COL2 from er_codelst where codelst_type in('alert','alert_resol') AND CODELST_HIDE='N' order by pk_codelst)
LOOP
  v_alertId := cur_alerts.pk_codelst;
  V_ALERTNAME:=cur_alerts.codelst_subtyp;
	v_alert_title := cur_alerts.codelst_desc;
	v_alert_desc := cur_alerts.codelst_custom_col1;
	v_alert_wording :=cur_alerts.codelst_custom_col;
  v_alert_precondition := getalertcondition(v_alertid,'PRE');
  v_alerttype:=cur_alerts.codelst_type;
  v_email_wording:=cur_alerts.codelst_custom_col2;
  var_instance_cnt :=0;
  var_instance_cnt1 :=0;
  var_instance_cnt2 :=0;
  var_instance_cnt3 :=0;
  var_instance_cnt4 :=0;
  var_instance_cnt5 :=0;
 -- dbms_output.put_line('********************************************************************************************************************************************');
--  dbms_output.put_line('alert Id :::'||v_alertid);
--   dbms_output.put_line('alert pre condition is::'||v_alert_precondition);

if v_alert_precondition<>' ' then
    if v_alertname in('alert_01','alert_02','alert_03','alert_04','alert_15','alert_20','alert_16','alert_29','alert_30','alert_31','alert_32','alert_33','alert_34','alert_35','alert_36','alert_37','alert_38','alert_39') then
      v_query := 'select count(*) cnt from er_order where '||v_alert_precondition;
    elsif v_alertname in('alert_05','alert_06') then
      v_query := 'select count(*) cnt from er_order,er_order_header where er_order.fk_order_header=er_order_header.pk_order_header and '||v_alert_precondition;
    elsif v_alertname in('alert_07','alert_08','alert_09','alert_10','alert_11') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_shipment where er_order.fk_order_header=er_order_header.pk_order_header and cb_shipment.fk_order_id=er_order.pk_order and '||v_alert_precondition;
    elsif v_alertname='alert_19' then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and '||v_alert_precondition;
    elsif v_alertname IN('alert_23','alert_24','alert_25','alert_26') then
      v_query := 'select count(*) cnt from er_order,er_order_header,cb_cord,er_attachments,cb_upload_info where er_order.fk_order_header=er_order_header.pk_order_header and er_order_header.order_entityid=cb_cord.pk_cord and er_attachments.entity_id=cb_cord.pk_cord and er_attachments.entity_type=(select pk_codelst from er_codelst where codelst_type=''entity_type'' and codelst_subtyp=''CBU'') and er_attachments.pk_attachment=cb_upload_info.fk_attachmentid and '||v_alert_precondition;
    end if;
    v_query:=replace(v_query,'#keycolumn',v_entity_id);
    dbms_output.put_line('^^^^^^^^^^^'||v_query);
    open cur_cv for v_query;
     LOOP
          FETCH cur_cv INTO sql_fieldval ;
          EXIT WHEN cur_cv%NOTFOUND;
           --dbms_output.put_line('display result  ' || sql_fieldval );
           if sql_fieldval>0 then
           if v_entity_type=v_entity_order then
              select nvl(assigned_to,0),nvl(crd.cord_registry_id,'N/A'),nvl(ord.order_sample_at_lab,'-'),case when ord.fk_order_resol_by_tc is null and ord.fk_order_resol_by_cbb is null then 0 when ord.fk_order_resol_by_tc is null and ord.fk_order_resol_by_cbb is not null then ord.fk_order_resol_by_cbb when ord.fk_order_resol_by_tc is not null and ord.fk_order_resol_by_cbb is null then ord.fk_order_resol_by_tc else 0 end into v_userid,v_cordregid,v_sampleatlab,v_resol from er_order ord left outer join er_order_header ordhdr on(ord.fk_order_header=ordhdr.pk_order_header) left outer join cb_cord crd on(ordhdr.order_entityid=crd.pk_cord) where pk_order=v_entity_id;
              --dbms_output.put_line('v_resol............'||v_resol);
              select ord.pk_order,nvl(to_char(ship.sch_shipment_date,'Mon DD, YYYY'),'N/A') into v_temp,v_schshipdate from er_order ord left outer join (select fk_order_id,sch_shipment_date from cb_shipment where fk_order_id=v_entity_id and fk_shipment_type=(select pk_codelst from er_codelst where codelst_type='shipment_type' and codelst_subtyp='CBU Shipment')) ship on(ship.fk_order_id=ord.pk_order) where ord.pk_order=v_entity_id;
            end if;
            if v_entity_type=v_entity_cord then
              select nvl(cord_registry_id,'N/A') into v_cordregid from cb_cord where pk_cord=v_entity_id;
            end if;
           if v_resol<>0 then
           --dbms_output.put_line('before getting resolution desc...............');
            select cbu_status_desc into v_resoldesc from cb_cbu_status where pk_cbu_status=v_resol;
            --dbms_output.put_line('after getting resolution desc...............'||v_resoldesc);
           end if;
           v_alert_wording:=replace(v_alert_wording,'<Request Type>',v_ordertype);
           v_email_wording:=replace(v_email_wording,'<Request Type>',v_ordertype);
           v_email_wording:=replace(v_email_wording,'<hiperlink>',v_url);
           dbms_output.put_line('email wording in begining...............'||v_email_wording);
           v_alert_wording:=replace(v_alert_wording,'<CBU Registry ID>',v_cordregid);
           v_email_wording:=replace(v_email_wording,'<CBU Registry ID>',v_cordregid);
           v_alert_wording:=replace(v_alert_wording,'<Resolution Code>',v_resoldesc);
           v_email_wording:=replace(v_email_wording,'<Resolution Code>',v_resoldesc);
           v_alert_wording:=replace(v_alert_wording,'<Shipment Scheduled Date>',v_schshipdate);
           v_email_wording:=replace(v_email_wording,'<Shipment Scheduled Date>',v_schshipdate);
           if v_sampleatlab='Y' then
           v_alert_wording:=replace(v_alert_wording,'CT','CT - Sample at Lab');
           v_email_wording:=replace(v_email_wording,'CT','CT - Sample at Lab');
           elsif v_sampleatlab='N' then
           v_alert_wording:=replace(v_alert_wording,'CT','CT - Ship Sample');
           v_email_wording:=replace(v_email_wording,'CT','CT - Ship Sample');
           end if;
           
           
           select pk_codelst into v_alert_id_temp from er_codelst where codelst_type='alert' and codelst_subtyp='alert_20';
          -- dbms_output.put_line('v_alert_id_temp::::::::'||v_alert_id_temp);
         --  dbms_output.put_line('v_userId::::::::'||v_userid);
          -- dbms_output.put_line('select count(*) from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert='||v_alertid||' and alrt_inst.entity_id='||v_entity_id||' and alrt_inst.entity_type='||v_entity_type||' and alrt_inst.fk_userid='||v_userid||' and alrt_inst.alert_type='||v_notify);
          -- dbms_output.put_line('var_instance_cnt before::::::'||var_instance_cnt);
          dbms_output.put_line('email wording in finally...............'||v_email_wording);
           select count(*) into var_instance_cnt from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.fk_userid=v_userid and alrt_inst.alert_type=v_notify;
               -- dbms_output.put_line('var_instance_cnt after:::::'||var_instance_cnt);
            if var_instance_cnt=0 or v_alert_id_temp=v_alertid then
              --  dbms_output.put_line('inside notification insert');
               insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_type,v_userid,v_notify,v_alert_wording);
                COMMIT;
            end if;
            for usrs in(select altr.FK_USER,altr.EMAIL_FLAG email_flag,altr.NOTIFY_FLAG notify_flag,altr.PROMPT_FLAG prompt_flag,addr.add_email mail,altr.ct_lab_flag ctlabflag,altr.ct_ship_flag ctshipflag,altr.he_flag heflag,altr.or_flag orflag,usr.usr_firstname||' '||usr.usr_lastname uname from cb_user_alerts altr,er_user usr,er_add addr where altr.fk_user=usr.pk_user and usr.fk_peradd=addr.pk_add and fk_codelst_alert=v_alertid)
            loop
             -- dbms_output.put_line('users that has assigned alert is:::'||usrs.fk_user);
              if usrs.email_flag=1 then
            --  dbms_output.put_line('alert type is:::'||v_alerttype);
            --  dbms_output.put_line('ctlabflag is:::'||usrs.ctlabflag);
              if usrs.ctlabflag=1 and v_alerttype='alert_resol' then
            --  dbms_output.put_line('inside CT lab flag:::'||usrs.ctlabflag);
                if v_sampleatlab='Y' then
             --   dbms_output.put_line('inside ct lab flag2:::'||usrs.ctlabflag);
             --   dbms_output.put_line('select count(*) from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert='||v_alertid||' and alrt_inst.v_entity_id='||v_entity_id||' and alrt_inst.entity_type='||v_entity_type||' and alrt_inst.fk_userid='||v_userid||' and alrt_inst.alert_type='||v_mail);
                  select count(*) into var_instance_cnt1 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.entity_type=v_entity_order and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
                  if var_instance_cnt1=0 or v_alert_id_temp=v_alertid then
                    insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
              --    dbms_output.put_line('Email alerts implemented..ct lab');
                    v_email_wording:=replace(v_email_wording,'<loggedinuser>',usrs.uname);
                    insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,v_mailfrom,usrs.mail,v_alert_title,v_email_wording,0);
                    COMMIT;
                  end if;
                end if;
              elsif usrs.ctshipflag=1 and v_alerttype='alert_resol' then
             -- dbms_output.put_line('inside CT ship flag:::'||usrs.ctshipflag);
             -- dbms_output.put_line(v_ordertype||'inside CT ship flag111111:::'||v_sampleatlab);
                if v_sampleatlab='N' then
              --  dbms_output.put_line('inside CT ship flag222222:::'||usrs.ctshipflag);
                  select count(*) into var_instance_cnt2 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
               --   dbms_output.put_line('var_instance_cnt2:::'||var_instance_cnt2);
                --  dbms_output.put_line(v_alert_id_temp||'='||v_alertid);
                  if var_instance_cnt2=0 or v_alert_id_temp=v_alertid then
                    insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
                    COMMIT;
                --  dbms_output.put_line('Email alerts implemented ct ship');
                v_email_wording:=replace(v_email_wording,'<loggedinuser>',usrs.uname);
                    insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,v_mailfrom,usrs.mail,v_alert_title,v_email_wording,0);
                    COMMIT;
                  end if;
                end if;
              elsif usrs.heflag=1 and v_alerttype='alert_resol' then
            --  dbms_output.put_line('inside he flag:::'||usrs.heflag);
                  select count(*) into var_instance_cnt3 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
                  if var_instance_cnt3=0 or v_alert_id_temp=v_alertid then
                    insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
                    COMMIT;
                --  dbms_output.put_line('Email alerts implemented he');
                v_email_wording:=replace(v_email_wording,'<loggedinuser>',usrs.uname);
                    insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,v_mailfrom,usrs.mail,v_alert_title,v_email_wording,0);
                    COMMIT;
                  end if;

              elsif usrs.orflag=1 and v_alerttype='alert_resol' then
             -- dbms_output.put_line('inside or flag:::'||usrs.orflag);
                  select count(*) into var_instance_cnt4 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type  and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
                  if var_instance_cnt4=0 or v_alert_id_temp=v_alertid then
                    insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
                  COMMIT;
                --  dbms_output.put_line('Email alerts implemented or');
                v_email_wording:=replace(v_email_wording,'<loggedinuser>',usrs.uname);
                    insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,v_mailfrom,usrs.mail,v_alert_title,v_email_wording,0);
                    COMMIT;
                  end if;

              elsif v_alerttype='alert' then
              select count(*) into var_instance_cnt5 from cb_alert_instances alrt_inst where alrt_inst.fk_codelst_alert=v_alertid and alrt_inst.entity_id=v_entity_id and alrt_inst.entity_type=v_entity_type and alrt_inst.fk_userid=usrs.fk_user and alrt_inst.alert_type=v_mail;
             --  dbms_output.put_line('var_instance_cnt5:::'||var_instance_cnt5);
             --   dbms_output.put_line(v_alert_id_temp||'='||v_alertid);
              if var_instance_cnt5=0 or v_alert_id_temp=v_alertid then
                insert into cb_alert_instances(PK_ALERT_INSTANCES,FK_CODELST_ALERT,entity_id,entity_type,FK_USERID,ALERT_TYPE,MAIL_ID,ALERT_WORDING) values(SEQ_CB_ALERT_INSTANCES.nextval,v_alertid,v_entity_id,v_entity_order,usrs.fk_user,v_mail,usrs.mail,v_alert_wording);
              COMMIT;
           --   dbms_output.put_line('Email alerts implemented');
           v_email_wording:=replace(v_email_wording,'<loggedinuser>',usrs.uname);
                insert into er_notification(PK_NOTIFICATION,NOTIFY_FROM,NOTIFY_TO,NOTIFY_SUBJECT,NOTIFY_CONTENT,NOTIFY_ISSENT) values(SEQ_ER_NOTIFICATION.nextval,v_mailfrom,usrs.mail,v_alert_title,v_email_wording,0);
                COMMIT;
              end if;
              end if;
              end if;
              end loop;

           end if;
      END LOOP;
    --dbms_output.put_line('alert wording is::'||v_alert_wording);
end if;
END LOOP;
if v_entity_type=v_entity_order then
  update er_order set data_modified_flag='' where pk_order=v_entity_id;
  COMMIT;
end if;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
  dbms_output.put_line ('A SELECT...INTO did not return any row.');
end;
/


create or replace
TRIGGER TASK_UPDATE_CRI AFTER INSERT ON CB_CORD_COMPLETE_REQ_INFO REFERENCING OLD AS OLDVAL NEW AS NEWVAL
FOR EACH ROW
DECLARE
V_FLAG NUMBER;
V_TASK NUMBER;
V_CORDID NUMBER;
V_ORDERID NUMBER;
V_CDRUSER VARCHAR2(2);
BEGIN
dbms_output.put_line('........'||:NEWVAL.CBU_INFO_FLAG||'...........'||:NEWVAL.CBU_PROCESSING_FLAG||'......'||:NEWVAL.LAB_SUMMARY_FLAG);
V_CORDID:=:NEWVAL.FK_CORD_ID;
SELECT NVL(USING_CDR,'') INTO V_CDRUSER FROM CB_CORD CRD,ER_SITE S,CBB CB WHERE CRD.FK_CBB_ID=S.PK_SITE AND CB.FK_SITE=S.PK_SITE AND CRD.PK_CORD=V_CORDID;
  IF (V_CDRUSER=1 AND :NEWVAL.COMPLETE_REQ_INFO_FLAG = 0 OR (:NEWVAL.IDS_FLAG=0 OR :NEWVAL.CBU_INFO_FLAG=0 OR :NEWVAL.CBU_PROCESSING_FLAG=0 OR :NEWVAL.LAB_SUMMARY_FLAG=0 OR :NEWVAL.MRQ_FLAG=0 OR :NEWVAL.FMHQ_FLAG=0 OR :NEWVAL.IDM_FLAG=0)) OR (V_CDRUSER=0 AND  :NEWVAL.COMPLETE_REQ_INFO_FLAG = 0 OR (:NEWVAL.IDS_FLAG=0 OR :NEWVAL.LICENSURE_FLAG=0 OR :NEWVAL.ELIGIBILITY_FLAG=0))  THEN
  dbms_output.put_line ('INSIDE FIRST IF.........'||V_CORDID);
     FOR TASKS IN(SELECT PK_TASKID,TASK_COMPLETEFLAG,ORD.PK_ORDER ORDER_ID FROM TASK TSK,ER_ORDER ORD,ER_ORDER_HEADER HDR WHERE TSK.FK_ENTITY=ORD.PK_ORDER AND ORD.FK_ORDER_HEADER =HDR.PK_ORDER_HEADER AND HDR.ORDER_ENTITYID  =V_CORDID AND HDR.ORDER_ENTITYTYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='CBU') AND ORD.ORDER_STATUS NOT IN(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE  ='order_status' AND CODELST_SUBTYP IN('CANCELLED','close_ordr')) AND FK_WORKFLOWACTIVITY IN(SELECT PK_WORKFLOWACTIVITY FROM WORKFLOW_ACTIVITY WHERE FK_ACTIVITYID=(SELECT PK_ACTIVITY FROM ACTIVITY WHERE ACTIVITY.ACTIVITY_NAME='reqclincinfobar')))
     LOOP
     V_FLAG:=TASKS.TASK_COMPLETEFLAG;
     V_TASK:=TASKS.PK_TASKID;
     V_ORDERID:=TASKS.ORDER_ID;
      dbms_output.put_line ('INSIDE LOOP........'||v_flag||'...........'||v_task||'....'||v_orderid);
      IF V_FLAG=1 THEN
      dbms_output.put_line ('INSIDE IF........');
        UPDATE ER_ORDER SET REQ_CLIN_INFO_FLAG=NULL WHERE PK_ORDER=V_ORDERID;
        UPDATE TASK SET TASK_COMPLETEFLAG=0 WHERE PK_TASKID=V_TASK;
        dbms_output.put_line ('AFTER UPDATE........');
      END IF;
     END LOOP;
  END IF;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
	dbms_output.put_line ('INSIDE EXCEPTION...........DATA NOT FOUND');
END;
/

create or replace
procedure update_order_status as
V_SHIPSCH NUMBER;
V_ENTITY_ORDER NUMBER;
V_PACKING_SLIP_FLAG VARCHAR2(1);
V_SCH_SHIP_DATE DATE;
V_ENTITY_ID number;
V_ENTITY_TYPE number;
V_ORDER_STATUS NUMBER;
V_ORDER_TYPE NUMBER(10);
V_ORDER_TYPE_PK NUMBER(10);
begin
SELECT NVL(PK_CODELST,0) INTO V_SHIPSCH FROM ER_CODELST WHERE CODELST_TYPE='order_status' AND CODELST_SUBTYP='SHIP_SCH';
SELECT NVL(PK_CODELST,0) INTO V_ENTITY_ORDER FROM ER_CODELST WHERE CODELST_TYPE='entity_type' AND CODELST_SUBTYP='ORDER';
SELECT NVL(PK_CODELST,-1) INTO V_ORDER_TYPE_PK FROM ER_CODELST WHERE CODELST_TYPE='order_type' AND CODELST_SUBTYP='CT';
FOR ALERTS IN(SELECT ENTITY_ID,ENTITY_TYPE FROM CB_ALERTS_MAINTENANCE)
LOOP
V_ENTITY_TYPE:=ALERTS.ENTITY_TYPE;
V_ENTITY_ID:=ALERTS.ENTITY_ID;
SELECT NVL(ORDER_TYPE,0) INTO V_ORDER_TYPE FROM ER_ORDER WHERE PK_ORDER=V_ENTITY_ID;
IF V_ENTITY_TYPE=v_entity_order AND V_ORDER_TYPE=V_ORDER_TYPE_PK THEN
  SELECT NVL(PACKAGE_SLIP_FLAG,'') INTO V_PACKING_SLIP_FLAG FROM ER_ORDER WHERE PK_ORDER=V_ENTITY_ID;
  dbms_output.put_line(V_ENTITY_ID);
  SELECT NVL(ORDER_STATUS,0) INTO V_ORDER_STATUS FROM ER_ORDER WHERE PK_ORDER=V_ENTITY_ID;
  SELECT NVL(SCH_SHIPMENT_DATE,TO_DATE('01-JAN-1900', 'DD-MON-YYYY')) INTO V_SCH_SHIP_DATE FROM CB_SHIPMENT WHERE FK_ORDER_ID=V_ENTITY_ID AND FK_SHIPMENT_TYPE=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='shipment_type' AND CODELST_SUBTYP='CBU');
  IF V_ORDER_STATUS=V_SHIPSCH AND V_PACKING_SLIP_FLAG='Y' AND trunc(SYSDATE-V_SCH_SHIP_DATE)>=0 THEN
    UPDATE ER_ORDER SET ORDER_STATUS=(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' AND CODELST_SUBTYP='AWA_HLA_RES'),order_status_date=sysdate where pk_order=V_ENTITY_ID;
    INSERT INTO CB_ENTITY_STATUS(PK_ENTITY_STATUS,ENTITY_ID,FK_ENTITY_TYPE,STATUS_TYPE_CODE,FK_STATUS_VALUE,STATUS_DATE,STATUS_REMARKS,CREATOR,CREATED_ON,IP_ADD,LAST_MODIFIED_BY,LAST_MODIFIED_DATE,RID,DELETEDFLAG,LICN_ELIG_CHANGE_REASON)
    VALUES (SEQ_CB_ENTITY_STATUS.nextval,V_ENTITY_ID,V_ENTITY_TYPE,'order_status',(SELECT PK_CODELST FROM ER_CODELST WHERE CODELST_TYPE='order_status' AND CODELST_SUBTYP='AWA_HLA_RES'),SYSDATE,NULL,0,SYSDATE,NULL,NULL,NULL,NULL,NULL,NULL);
    COMMIT;
  END IF;
END IF;
end loop;
EXCEPTION
WHEN NO_DATA_FOUND THEN
  DBMS_OUTPUT.PUT_LINE('NO DATA FOUND');
end;
/



begin
 DBMS_SCHEDULER.drop_job (job_name => 'CREATE_WORKFLOW_JOB');
 end;
 /

begin
DBMS_SCHEDULER.create_job (
    job_name        => 'CREATE_WORKFLOW_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN SP_WORKFLOW_TEMP;UPDATE_ORDER_STATUS;CB_ALERTS_MAINTENANCE_PROC;END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'freq=SECONDLY;INTERVAL=10',
    end_date        => NULL,
    enabled         => TRUE,
    comments        => 'Job to create workflow,alerts and change order status');
end;
/


begin
dbms_scheduler.run_job('CREATE_WORKFLOW_JOB');
end;
/