
--Start updating cb_Questions --
--Question 1--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'm_or_f_adopted_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'm_or_f_adopted_ind';
commit;
  end if;
end;
/
--Question 02--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'baby_moth_fath_bld_rel_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'baby_moth_fath_bld_rel_ind';
commit;
  end if;
end;
/
--Question 03--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'preg_use_dnr_egg_sperm_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'preg_use_dnr_egg_sperm_ind';
commit;
  end if;
end;
/
--Question 04--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'abn_prenat_test_rslt_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'abn_prenat_test_rslt_ind';
commit;
  end if;
end;
/
--Question 05--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'child_die_age_10_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'child_die_age_10_ind';
commit;
  end if;
end;
/
--Question 06--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'stillborn_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'stillborn_ind';
commit;
  end if;
end;
/
--Question 07--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'can_leuk_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'can_leuk_ind';
commit;
  end if;
end;
/
--Question 08--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_rbc_dises_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'inher_rbc_dises_ind';
commit;
  end if;
end;
/
--Question 10--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_imm_dfc_disordr_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'inher_imm_dfc_disordr_ind';
commit;
  end if;
end;
/
--Question 11--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_pltlt_dises_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'inher_pltlt_dises_ind';
commit;
  end if;
end;
/
--Question 12--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_bld_dises_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'inher_other_bld_dises_ind';
commit;
  end if;
end;
/
--Question 13--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_sickle_cell_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'inher_sickle_cell_ind';
commit;
  end if;
end;
/
--Question 14--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_thalassemia_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'inher_thalassemia_ind';
commit;
  end if;
end;
/
--Question 15--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_met_stg_dises_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'inher_met_stg_dises_ind';
commit;
  end if;
end;
/
--Question 16--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hiv_aids_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'hiv_aids_ind';
commit;
  end if;
end;
/
--Question 17--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'severe_aut_imm_sys_disordr_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'severe_aut_imm_sys_disordr_ind';
commit;
  end if;
end;
/
--Question 18--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'oth_immune_dis';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'oth_immune_dis';
commit;
  end if;
end;
/
--Question 19--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'chron_bld_tranf_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'chron_bld_tranf_ind';
commit;
  end if;
end;
/
--Question 20--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'hemolytic_anemia_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'hemolytic_anemia_ind';
commit;
  end if;
end;
/
--Question 21--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'spleen_removed_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'spleen_removed_ind';
commit;
  end if;
end;
/
--Question 22--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'gallbladder_removed_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'gallbladder_removed_ind';
commit;
  end if;
end;
/
--Question 23--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'cruetz_jakob_dis_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'cruetz_jakob_dis_ind';
commit;
  end if;
end;
/
--Question 24--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'inher_other_dises_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'inher_other_dises_ind';
commit;
  end if;
end;
/
--Question 25--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from CB_QUESTIONS
    where QUES_CODE = 'answ_both_moth_fath_ind';
  if (v_record_exists = 1) then
	update CB_QUESTIONS set unlicn_prior_to_shipment_flag=1 where QUES_CODE = 'answ_both_moth_fath_ind';
commit;
  end if;
end;
/
