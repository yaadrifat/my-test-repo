create or replace
function f_cord_licence_history(pk_cord number) return SYS_REFCURSOR
IS
v_lic_hty SYS_REFCURSOR;
BEGIN
    OPEN v_lic_hty FOR SELECT TO_CHAR(STATUS.STATUS_DATE,'Mon DD, YYYY') LH_DT,F_CODELST_DESC(STATUS.FK_STATUS_VALUE) LH_STATUS, F_GETUSER(CREATOR) CREATOR, TO_CHAR(CREATED_ON,'Mon DD,YYYY') CREATED_ON ,ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_ENTITY_STATUS STAT INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE STAT.PK_ENTITY_STATUS in ('||STATUS.PK_ENTITY_STATUS||')',' <br></br>') LH_UNLIC_REASON,STATUS.LICN_ELIG_CHANGE_REASON LH_COMMENTS FROM CB_ENTITY_STATUS STATUS WHERE STATUS.ENTITY_ID=pk_cord AND STATUS.STATUS_TYPE_CODE='licence' AND (STATUS.IS_CORD_ENTRY_DATA IS NULL OR STATUS.IS_CORD_ENTRY_DATA=0) order by STATUS.STATUS_DATE;
RETURN v_lic_hty;
END;
/
commit;


create or replace
function f_cord_eligble_history(pk_cord number) return SYS_REFCURSOR
IS
v_elig_hty SYS_REFCURSOR;
BEGIN
    OPEN v_elig_hty FOR SELECT TO_CHAR(STATUS.STATUS_DATE,'Mon DD, YYYY') EH_DT,F_GETUSER(CREATOR) CREATOR, TO_CHAR(CREATED_ON,'Mon DD,YYYY') CREATED_ON, F_CODELST_DESC(STATUS.FK_STATUS_VALUE) EH_STATUS,ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_ENTITY_STATUS STAT INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE STAT.PK_ENTITY_STATUS in ('||STATUS.PK_ENTITY_STATUS||')',' <br></br>') EH_INELIGIBLEREASON,STATUS.LICN_ELIG_CHANGE_REASON EH_COMMENTS FROM CB_ENTITY_STATUS STATUS WHERE STATUS.ENTITY_ID=pk_cord AND STATUS.STATUS_TYPE_CODE='eligibility' AND (STATUS.IS_CORD_ENTRY_DATA IS NULL OR STATUS.IS_CORD_ENTRY_DATA=0) order by STATUS.STATUS_DATE;
RETURN v_elig_hty;
END;
/
commit;

create or replace
function f_cord_final_review(pk_cord number) return SYS_REFCURSOR
IS
v_final_review SYS_REFCURSOR;
BEGIN
    OPEN V_FINAL_REVIEW FOR
                   SELECT
                   F_CODELST_DESC(REVIEW.FK_CORD_LICENSURE_STAT) LIC_STAT,
                   F_CODELST_DESC(REVIEW.FK_CORD_ELIGIBLE_STAT) ELIG_STAT,
                   ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_ENTITY_STATUS STAT INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE STAT.PK_ENTITY_STATUS in ('||NVL(REVIEW.FK_UNLICENSED_REASON,0)||')',' <br></br>') UNLIC_REASON,
                   ROWTOCOL('SELECT F_CODELST_DESC(REASON.FK_REASON_ID) FROM CB_ENTITY_STATUS STAT INNER JOIN CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS WHERE STAT.PK_ENTITY_STATUS in ('||NVL(REVIEW.FK_INELIGIBLE_REASON,0)||')',' <br></br>') INELIG_REASON,
                   TO_CHAR(REVIEW.CREATED_ON,'Mon DD, YYYY') REVIEW_DT,
                   F_CODELST_DESC(REVIEW.REVIEW_CORD_ACCEPTABLE_FLAG) REVIEW_RESULT,
                   F_GETUSER(REVIEW.ELIG_CREATOR) USERNAME,
                   REVIEW.FINAL_REVIEW_CONFIRM_FLAG REVIEWFLAG
                   FROM
                   CB_CORD_FINAL_REVIEW REVIEW
                   WHERE
                   REVIEW.LICENSURE_CONFIRM_FLAG=1 AND 
                   REVIEW.ELIGIBLE_CONFIRM_FLAG=1 AND
                   REVIEW.REVIEW_CORD_ACCEPTABLE_FLAG IS NOT NULL AND
                   REVIEW.FK_CORD_ID=pk_cord ORDER BY REVIEW.CREATED_ON;
RETURN V_FINAL_REVIEW;
END;
/
commit;

create or replace
FUNCTION F_BEST_HLA_CORD(pk_cord number, hla_typ NUMBER) return SYS_REFCURSOR
IS
v_entity_cord NUMBER;
p_bhla_refcur SYS_REFCURSOR;
BEGIN
select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';
OPEN p_bhla_refcur FOR SELECT 
                       h.fk_hla_code_id AS PKLOCUS,
                       nvl(ec1.genomic_format,'-') AS TYPE1,
                       nvl(ec2.genomic_format,'-') AS TYPE2,
                       to_char(h.hla_received_date,'Mon DD, YYYY') AS ENTRYDATE,
                       h.CB_BEST_HLA_ORDER_SEQ seqval,
                       f_getuser(h.creator) usrname
                      FROM CB_BEST_HLA h,
                        cb_antigen_encod ec1,
                        cb_antigen_encod ec2
                      WHERE h.ENTITY_TYPE  =v_entity_cord
                      AND H.FK_SOURCE=hla_typ
                      AND h.ENTITY_ID         =pk_cord
                      AND h.fk_hla_antigeneid1=ec1.fk_antigen (+)
                      AND ( ec1.version       =
                        (SELECT MAX(ec1s.version)
                        FROM cb_antigen_encod ec1s
                        WHERE ec1.fk_antigen=ec1s.fk_antigen
                        )
                      OR ec1.version          IS NULL)
                      AND h.fk_hla_antigeneid2 = ec2.fk_antigen (+)
                      AND (ec2.version         =
                        (SELECT MAX(ec2s.version)
                        FROM cb_antigen_encod ec2s
                        WHERE ec2.fk_antigen=ec2s.fk_antigen
                        )
                      OR ec2.version IS NULL)
                      ORDER BY h.CB_BEST_HLA_ORDER_SEQ DESC nulls last; 
RETURN p_bhla_refcur;

END;
/
commit;


create or replace
FUNCTION F_HLA_CORD(pk_cord number, hla_typ NUMBER) return SYS_REFCURSOR
IS
v_entity_cord number;
p_hla_refcur SYS_REFCURSOR;
BEGIN
select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';

OPEN p_hla_refcur FOR     SELECT 
                            h.fk_hla_code_id AS PKLOCUS,
                            nvl(ec1.genomic_format,'-') AS TYPE1,
                            nvl(ec2.genomic_format,'-') AS TYPE2,
                            to_char(h.hla_received_date,'Mon DD, YYYY') AS ENTRYDATE,
                            h.CB_HLA_ORDER_SEQ seqvalhla,
                            f_getuser(h.creator) usrname
                          FROM CB_HLA h ,
                            cb_antigen_encod ec1,
                            cb_antigen_encod ec2
                          WHERE h.fk_hla_antigeneid1=ec1.fk_antigen (+)
                          AND ( ec1.version         =
                            (SELECT MAX(ec1s.version)
                            FROM cb_antigen_encod ec1s
                            WHERE ec1.fk_antigen=ec1s.fk_antigen
                            )
                          OR ec1.version IS NULL) 
                          AND h.fk_hla_antigeneid2 = ec2.fk_antigen (+)
                          AND (ec2.version         =
                            (SELECT MAX(ec2s.version)
                            FROM cb_antigen_encod ec2s
                            WHERE ec2.fk_antigen=ec2s.fk_antigen
                            )
                          OR ec2.version  IS NULL) AND h.ENTITY_ID  =pk_cord AND h.ENTITY_TYPE=v_entity_cord AND H.FK_SOURCE=hla_typ ORDER BY H.CB_HLA_ORDER_SEQ DESC NULLS LAST;
RETURN p_hla_refcur;
END;
/
commit;


create or replace
FUNCTION F_GET_DISTINCT_HLA_RESP(PKCORD NUMBER, HLA_TYP NUMBER) RETURN SYS_REFCURSOR IS 
DIST_HLA_RES SYS_REFCURSOR;
V_ENTITY_CORD NUMBER;
BEGIN
select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';

OPEN DIST_HLA_RES FOR SELECT
                        DISTINCT H.CB_HLA_ORDER_SEQ SEQDATA,
                        F_GETUSER(H.CREATOR) USRNAME1,
                        TO_CHAR(H.CREATED_ON, 'Mon DD, YYYY') CREATED1
                      FROM cb_hla h
                      WHERE 
                        h.ENTITY_ID =pkcord
                        AND h.ENTITY_TYPE =v_entity_cord
                        AND H.FK_SOURCE   =hla_typ;

RETURN DIST_HLA_RES;
END;
/
commit;
