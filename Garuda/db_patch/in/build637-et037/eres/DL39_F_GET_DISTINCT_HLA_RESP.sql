create or replace
FUNCTION F_GET_DISTINCT_HLA_RESP(PKCORD NUMBER, HLA_TYP NUMBER) RETURN SYS_REFCURSOR IS 
DIST_HLA_RES SYS_REFCURSOR;
v_entity_patient NUMBER:=0;
v_patient_hla_type NUMBER;
v_patient_count NUMBER:=0;
p_entity_id NUMBER:=0;
p_entity_type NUMBER;
p_source_type NUMBER;
v_entity_cord NUMBER;
BEGIN
select pk_codelst into v_entity_cord from er_codelst where codelst_type='entity_type' and codelst_subtyp='CBU';
select pk_codelst into v_entity_patient from er_codelst where codelst_type='entity_type' and codelst_subtyp='PATIENT';
select pk_codelst into v_patient_hla_type from er_codelst where codelst_type='test_source' and codelst_subtyp='patient';


IF hla_typ=v_patient_hla_type THEN
SELECT COUNT(*) INTO V_PATIENT_COUNT FROM ER_ORDER_RECEIPANT_INFO INFOVAL WHERE INFOVAL.FK_ORDER_ID=(SELECT MAX(ORD.PK_ORDER) FROM ER_ORDER_HEADER HDR LEFT OUTER JOIN ER_ORDER ORD ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER) WHERE HDR.ORDER_ENTITYID=PKCORD);
IF V_PATIENT_COUNT>0 THEN
SELECT NVL(INFOVAL.FK_RECEIPANT,0) INTO P_ENTITY_ID FROM ER_ORDER_RECEIPANT_INFO INFOVAL WHERE INFOVAL.FK_ORDER_ID=(SELECT MAX(ORD.PK_ORDER) FROM ER_ORDER_HEADER HDR LEFT OUTER JOIN ER_ORDER ORD ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER) WHERE HDR.ORDER_ENTITYID=PKCORD);
END IF;
p_entity_type:=v_entity_patient;
END IF;

IF hla_typ!=v_patient_hla_type THEN
P_ENTITY_ID:=PKCORD;
p_entity_type:=v_entity_cord;
END IF;

OPEN DIST_HLA_RES FOR  select SEQDATA,USRNAME1,CREATED1,CREATED2 from (SELECT
                        DISTINCT H.CB_HLA_ORDER_SEQ SEQDATA,
                        usr.usr_lastname||' '||usr.usr_firstname  USRNAME1,
                        TO_CHAR(H.HLA_TYPING_DATE, 'Mon DD, YYYY') CREATED1,
                        TO_CHAR(H.HLA_RECEIVED_DATE, 'Mon DD, YYYY') CREATED2,
                        H.HLA_TYPING_DATE dt1,
                        H.HLA_RECEIVED_DATE dt2
                      FROM cb_hla h, er_user usr
                      WHERE 
                        usr.pk_user=h.creator
                        AND h.ENTITY_ID =P_ENTITY_ID
                        AND h.ENTITY_TYPE =p_entity_type
                        AND H.FK_SOURCE   =hla_typ)order by dt1 desc,dt2 desc;
RETURN DIST_HLA_RES;
END;
/

