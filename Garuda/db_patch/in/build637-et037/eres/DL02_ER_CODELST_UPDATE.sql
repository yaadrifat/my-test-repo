--start update er_codelst table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst where codelst_type='immune_def' and codelst_subtyp='cis';
  if (v_record_exists = 1) then
	update er_codelst set codelst_desc='Combined Immunodeficiency Syndrome(CID),Common Variable Immunodeficiency(CVID)' where codelst_type='immune_def' and codelst_subtyp='cis';
commit;
  end if;
end;
/
--END--

--start update er_codelst table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst where codelst_type='immune_def' and codelst_subtyp='nezelhoff';
  if (v_record_exists = 1) then
	update er_codelst set codelst_desc='Nezeloff Syndrome' where codelst_type='immune_def' and codelst_subtyp='nezelhoff';
commit;
  end if;
end;
/
--END--

--start update er_codelst table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst where codelst_type='immune_def' and codelst_subtyp='scid';
  if (v_record_exists = 1) then
	update er_codelst set codelst_desc='Severe Combined Immunodeficiency (SCID)' where codelst_type='immune_def' and codelst_subtyp='scid';
commit;
  end if;
end;
/
--END--
--start update er_codelst table--
DECLARE
  v_record_exists number := 0;
BEGIN
  Select count(*) into v_record_exists
    from er_codelst where codelst_type='metab_dis' and codelst_subtyp='mps6';
  if (v_record_exists = 1) then
	update er_codelst set codelst_desc='Maroteaux-Lamy Syndrome (MPS VI)' where codelst_type='metab_dis' and codelst_subtyp='mps6';
commit;
  end if;
end;
/
--END--

