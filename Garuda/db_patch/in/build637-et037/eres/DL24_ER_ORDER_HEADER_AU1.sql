CREATE OR REPLACE TRIGGER "ER_ORDER_HEADER_AU1" AFTER UPDATE ON eres.ER_ORDER_HEADER 
referencing OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE 
    v_rowid NUMBER(10);/*variable used to fetch value of sequence */
  NEW_ORDER_ENTITYTYPE                 VARCHAR2(200);
  OLD_ORDER_ENTITYTYPE                 VARCHAR2(200);
  NEW_FK_ORDER_TYPE                    VARCHAR2(200);
  OLD_FK_ORDER_TYPE                    VARCHAR2(200);
  NEW_FK_ORDER_STATUS                  VARCHAR2(200);
  OLD_FK_ORDER_STATUS                  VARCHAR2(200);
  NEW_FK_SITE_ID                       VARCHAR2(200);
  OLD_FK_SITE_ID                       VARCHAR2(200);
  OLD_ORDER_REQUESTED_BY				VARCHAR2(200);
  OLD_ORDER_APPROVED_BY					VARCHAR2(200);
  NEW_ORDER_REQUESTED_BY		VARCHAR2(200);
  NEW_ORDER_APPROVED_BY			VARCHAR2(200);
  NEW_LAST_MODIFIED_BY VARCHAR2(200);
  OLD_LAST_MODIFIED_BY VARCHAR2(200);
  NEW_CREATOR VARCHAR2(200);
  OLD_CREATOR VARCHAR2(200);
BEGIN
    SELECT seq_audit_row_module.nextval INTO v_rowid FROM dual;
    IF NVL(:OLD.ORDER_ENTITYTYPE,0) !=     NVL(:NEW.ORDER_ENTITYTYPE,0) THEN
 BEGIN
           SELECT F_Codelst_Desc(:OLD.ORDER_ENTITYTYPE)        INTO OLD_ORDER_ENTITYTYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_ORDER_ENTITYTYPE := '';
   END;  
   BEGIN
           SELECT F_Codelst_Desc(:new.ORDER_ENTITYTYPE)        INTO NEW_ORDER_ENTITYTYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
                NEW_ORDER_ENTITYTYPE := '';
   END; 
   END IF;  
   IF NVL(:OLD.FK_ORDER_TYPE,0) !=     NVL(:NEW.FK_ORDER_TYPE,0) THEN
 BEGIN
           SELECT F_Codelst_Desc(:OLD.FK_ORDER_TYPE)        INTO OLD_FK_ORDER_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_FK_ORDER_TYPE := '';
   END;  
   BEGIN
           SELECT F_Codelst_Desc(:new.FK_ORDER_TYPE)        INTO NEW_FK_ORDER_TYPE  from dual;
   EXCEPTION WHEN OTHERS THEN
                NEW_FK_ORDER_TYPE := '';
   END;  
   END IF;
   IF NVL(:OLD.CREATOR,0) !=  NVL(:NEW.CREATOR,0) THEN
    BEGIN
           SELECT F_GETUSER(:NEW.CREATOR) INTO NEW_CREATOR from dual;
   EXCEPTION WHEN OTHERS THEN
                NEW_CREATOR := '';
   END;
   BEGIN
           SELECT F_GETUSER(:OLD.CREATOR) INTO OLD_CREATOR from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_CREATOR := '';
   END;
   END IF;
   IF NVL(:OLD.LAST_MODIFIED_BY,0) !=  NVL(:NEW.LAST_MODIFIED_BY,0) THEN
    BEGIN
            SELECT F_GETUSER(:NEW.LAST_MODIFIED_BY) INTO NEW_LAST_MODIFIED_BY from dual;
   EXCEPTION WHEN OTHERS THEN
                NEW_LAST_MODIFIED_BY := '';
   END;
   BEGIN
           SELECT F_GETUSER(:OLD.LAST_MODIFIED_BY) INTO OLD_LAST_MODIFIED_BY from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_LAST_MODIFIED_BY := '';
   END;
   END IF;  
   IF NVL(:OLD.FK_ORDER_STATUS,0) !=     NVL(:NEW.FK_ORDER_STATUS,0) THEN
 BEGIN
           SELECT F_Codelst_Desc(:OLD.FK_ORDER_STATUS)        INTO OLD_FK_ORDER_STATUS  from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_FK_ORDER_STATUS := '';
   END;  
   BEGIN
           SELECT F_Codelst_Desc(:new.FK_ORDER_STATUS)        INTO NEW_FK_ORDER_STATUS  from dual;
   EXCEPTION WHEN OTHERS THEN
                NEW_FK_ORDER_STATUS := '';
   END;
  END IF;
   IF NVL(:OLD.FK_SITE_ID,0) !=     NVL(:NEW.FK_SITE_ID,0) THEN
 BEGIN
           SELECT F_GET_ER_SITE_NAME(:OLD.FK_SITE_ID)        INTO OLD_FK_SITE_ID  from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_FK_SITE_ID := '';
   END;  
   BEGIN
           SELECT F_GET_ER_SITE_NAME(:new.FK_SITE_ID)        INTO NEW_FK_SITE_ID  from dual;
   EXCEPTION WHEN OTHERS THEN
                NEW_FK_SITE_ID := '';
   END; 
   END IF;
   IF NVL(:OLD.ORDER_REQUESTED_BY,0) !=     NVL(:NEW.ORDER_REQUESTED_BY,0) THEN	 
   BEGIN
   		SELECT f_getuser(:new.ORDER_REQUESTED_BY)		INTO NEW_ORDER_REQUESTED_BY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_ORDER_REQUESTED_BY := '';
   END; 
   BEGIN
           SELECT f_getuser(:OLD.ORDER_REQUESTED_BY)        INTO OLD_ORDER_REQUESTED_BY  from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_ORDER_REQUESTED_BY := '';
   END; 
   END IF;
   IF NVL(:OLD.ORDER_APPROVED_BY,0) !=     NVL(:NEW.ORDER_APPROVED_BY,0) THEN	 
   BEGIN
   		SELECT f_getuser(:new.ORDER_APPROVED_BY)		INTO NEW_ORDER_APPROVED_BY  from dual;
   EXCEPTION WHEN OTHERS THEN
   			 NEW_ORDER_APPROVED_BY := '';
   END; 
    BEGIN
           SELECT f_getuser(:OLD.ORDER_APPROVED_BY)        INTO OLD_ORDER_APPROVED_BY  from dual;
   EXCEPTION WHEN OTHERS THEN
                OLD_ORDER_APPROVED_BY := '';
   END; 
   END IF;
    pkg_audit_trail_module.sp_row_insert (v_rowid,'ER_ORDER_HEADER',:OLD.rid,:OLD.PK_ORDER_HEADER,'U',:NEW.LAST_MODIFIED_BY);
    
IF NVL(:OLD.PK_ORDER_HEADER,0) != NVL(:NEW.PK_ORDER_HEADER,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','PK_ORDER_HEADER',:OLD.PK_ORDER_HEADER, :NEW.PK_ORDER_HEADER,NULL,NULL);
  END IF;
  IF NVL(:OLD.ORDER_GUID,' ') !=  NVL(:NEW.ORDER_GUID,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','ORDER_GUID',:OLD.ORDER_GUID, :NEW.ORDER_GUID,NULL,NULL);
  END IF;
  IF NVL(:OLD.ORDER_NUM,' ') !=  NVL(:NEW.ORDER_NUM,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','ORDER_NUM',:OLD.ORDER_NUM, :NEW.ORDER_NUM,NULL,NULL);
  END IF;
  If Nvl(:Old.Order_Entityid,0) !=  Nvl(:New.Order_Entityid,0) Then
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','ORDER_ENTITYID',:OLD.ORDER_ENTITYID, :NEW.ORDER_ENTITYID,NULL,NULL);
  END IF;
  IF NVL(:OLD.ORDER_ENTITYTYPE,0) != NVL(:NEW.ORDER_ENTITYTYPE,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','ORDER_ENTITYTYPE',OLD_ORDER_ENTITYTYPE, NEW_ORDER_ENTITYTYPE,NULL,NULL);
  END IF;
 IF NVL(:OLD.ORDER_OPEN_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.ORDER_OPEN_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','ORDER_OPEN_DATE',
       TO_CHAR(:OLD.ORDER_OPEN_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.ORDER_OPEN_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
  IF NVL(:OLD.ORDER_CLOSE_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.ORDER_CLOSE_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','ORDER_CLOSE_DATE',TO_CHAR(:OLD.ORDER_CLOSE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.ORDER_CLOSE_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
 IF NVL(:OLD.FK_ORDER_TYPE,0) != NVL(:NEW.FK_ORDER_TYPE,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','FK_ORDER_TYPE',OLD_FK_ORDER_TYPE, NEW_FK_ORDER_TYPE,NULL,NULL);
  END IF;
 IF NVL(:OLD.FK_ORDER_STATUS,0) !=NVL(:NEW.FK_ORDER_STATUS,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','FK_ORDER_STATUS',OLD_FK_ORDER_STATUS, NEW_FK_ORDER_STATUS,NULL,NULL);
  END IF;
IF NVL(:OLD.ORDER_STATUS_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) != NVL(:NEW.ORDER_STATUS_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','ORDER_STATUS_DATE',
       TO_CHAR(:OLD.ORDER_STATUS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.ORDER_STATUS_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
  IF NVL(:OLD.ORDER_REMARKS,' ') !=  NVL(:NEW.ORDER_REMARKS,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','ORDER_REMARKS',:OLD.ORDER_REMARKS, :NEW.ORDER_REMARKS,NULL,NULL);
  END IF;
  IF NVL(:OLD.ORDER_REQUESTED_BY,0) !=  NVL(:NEW.ORDER_REQUESTED_BY,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','ORDER_REQUESTED_BY',OLD_ORDER_REQUESTED_BY, NEW_ORDER_REQUESTED_BY,NULL,NULL);
  END IF;
IF NVL(:OLD.ORDER_APPROVED_BY,0) !=   NVL(:NEW.ORDER_APPROVED_BY,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','ORDER_APPROVED_BY',OLD_ORDER_APPROVED_BY, NEW_ORDER_APPROVED_BY,NULL,NULL);
  END IF;  
 IF NVL(:OLD.CREATOR,0) !=   NVL(:NEW.CREATOR,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','CREATOR', OLD_CREATOR, NEW_CREATOR,NULL,NULL);
  END IF;
   IF NVL(:OLD.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=  NVL(:NEW.CREATED_ON,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','CREATED_ON',
       TO_CHAR(:OLD.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.CREATED_ON, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
 If Nvl(:OLD.LAST_MODIFIED_BY,0) != Nvl(:NEW.LAST_MODIFIED_BY,0) Then
        pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','LAST_MODIFIED_BY',OLD_LAST_MODIFIED_BY,NEW_LAST_MODIFIED_BY,NULL,NULL);
      END IF;   
   IF NVL(:OLD.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) !=
     NVL(:NEW.LAST_MODIFIED_DATE,TO_DATE(pkg_dateutil.f_get_future_null_date_str,pkg_dateutil.f_get_dateformat)) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','LAST_MODIFIED_DATE',
       TO_CHAR(:OLD.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT), TO_CHAR(:NEW.LAST_MODIFIED_DATE, PKG_DATEUTIL.F_GET_DATEFORMAT),NULL,NULL);
  END IF;
   IF NVL(:OLD.IP_ADD,' ') !=  NVL(:NEW.IP_ADD,' ') THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','IP_ADD',:OLD.IP_ADD, :NEW.IP_ADD,NULL,NULL);
  END IF;  
  IF NVL(:OLD.DELETEDFLAG,0) != NVL(:NEW.DELETEDFLAG,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','DELETEDFLAG',:OLD.DELETEDFLAG, :NEW.DELETEDFLAG,NULL,NULL);
  END IF;
  IF NVL(:OLD.RID,0) != NVL(:NEW.RID,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','RID',:OLD.RID, :NEW.RID,NULL,NULL);
  END IF;  
 IF NVL(:OLD.FK_SITE_ID,0) !=NVL(:NEW.FK_SITE_ID,0) THEN
     pkg_audit_trail_module.sp_column_insert (v_rowid,'ER_ORDER_HEADER','FK_SITE_ID',OLD_FK_SITE_ID, NEW_FK_SITE_ID,NULL,NULL);
  END IF;  
  END;
/
