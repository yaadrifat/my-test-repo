create or replace FUNCTION F_GET_TNC_FOR_ORDER(PKCORD NUMBER) RETURN VARCHAR2 IS
post_processing_pk number(10);
tnc_value1 varchar(10):='0';
tnc_value2 varchar(10):='0';
tnc_final varchar(10);
specimenidval number(10);
sytem_cord_value varchar2(3);
countvalue1 number(10);
countvalue2 number(10);
begin

select nvl(crd.FK_SPECIMEN_ID,0) into specimenidval from cb_cord crd where crd.pk_cord=PKCORD;
select crd.system_cord into sytem_cord_value from cb_cord crd where crd.pk_cord=PKCORD;
SELECT nvl(PK_CODELST,0) into post_processing_pk FROM ER_CODELST WHERE CODELST_TYPE='timing_of_test' AND CODELST_SUBTYP='post_procesing';

--dbms_output.put_line('fk_specimen_id::::::::::'||specimenidval);
--dbms_output.put_line('post_processing_pk::::::::::'||post_processing_pk);

SELECT count(*) into countvalue2
FROM ER_PATLABS LABS
LEFT OUTER JOIN ER_LABTEST TST
ON TST.PK_LABTEST=LABS.FK_TESTID
WHERE
LABS.FK_TIMING_OF_TEST=post_processing_pk
AND TST.LABTEST_SHORTNAME = 'TCBUUN'
and LABS.FK_SPECIMEN=specimenidval;



if countvalue2=1 then
SELECT nvl(labs.test_result,'0') into tnc_value2
FROM ER_PATLABS LABS
LEFT OUTER JOIN ER_LABTEST TST
ON TST.PK_LABTEST=LABS.FK_TESTID
WHERE
LABS.FK_TIMING_OF_TEST=post_processing_pk
AND TST.LABTEST_SHORTNAME = 'TCBUUN'
and LABS.FK_SPECIMEN=specimenidval;
end if;


SELECT count(*) into countvalue1
FROM ER_PATLABS LABS
LEFT OUTER JOIN ER_LABTEST TST
ON TST.PK_LABTEST=LABS.FK_TESTID
WHERE
LABS.FK_TIMING_OF_TEST=post_processing_pk
AND TST.LABTEST_SHORTNAME = 'UNCRCT'
and LABS.FK_SPECIMEN=specimenidval;


if countvalue1=1 then

SELECT nvl(labs.test_result,'0') into tnc_value1
FROM ER_PATLABS LABS
LEFT OUTER JOIN ER_LABTEST TST
ON TST.PK_LABTEST=LABS.FK_TESTID
WHERE
LABS.FK_TIMING_OF_TEST=post_processing_pk
AND TST.LABTEST_SHORTNAME = 'UNCRCT'
and LABS.FK_SPECIMEN=specimenidval;

end if;

--dbms_output.put_line('countvalue1::::::::::'||countvalue1);
--dbms_output.put_line('countvalue2::::::::::'||countvalue2);

--dbms_output.put_line('tnc_value1::::::::::'||tnc_value1);
--dbms_output.put_line('tnc_value2::::::::::'||tnc_value2);
---dbms_output.put_line('sytem_cord_value::::::::::'||sytem_cord_value);

select 
case 
when trim(crd.system_cord)='yes' then tnc_value1
when trim(crd.system_cord)='no' and tnc_value1!='0' then tnc_value1
when trim(crd.system_cord)='no' and tnc_value1='0' and tnc_value2!='0' then tnc_value2
end into tnc_final
from cb_cord crd where pk_cord=PKCORD;


--dbms_output.put_line('finalvalue::::::::::'||tnc_final);

return tnc_final;


end;
/


