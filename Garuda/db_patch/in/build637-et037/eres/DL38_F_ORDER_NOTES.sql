create or replace
FUNCTION f_order_notes(cordid  NUMBER,orderid NUMBER) RETURN SYS_REFCURSOR IS
  v_cordorder_notes SYS_REFCURSOR;
  BEGIN
    OPEN v_cordorder_notes FOR 
                             SELECT 
                              notes.pk_notes,
                              notes.notes,
                              notes.subject,
                              f_codelst_desc(notes.fk_notes_category) notes_category,
			      f_codelst_desc(ord.order_type) req_type,
                              TO_CHAR(notes.request_date,'Mon DD, YYYY') request_date,
                              TO_CHAR(notes.created_on,'Mon DD, YYYY hh:mm:ss am') date_time,
                              usr.usr_lastname||' '||usr.usr_firstname note_user,
                              f_codelst_desc(notes.fk_notes_type) note_type
                            FROM cb_notes notes, er_order ord, er_user usr
                            WHERE ord.pk_order=orderid and notes.entity_id = cordid and usr.pk_user=notes.creator
                            AND notes.fk_notes_type =
                              (SELECT pk_codelst
                              FROM er_codelst
                              WHERE codelst_type = 'note_type'
                              AND codelst_subtyp = 'progress'
                              )
                            ORDER BY notes.created_on DESC, notes.pk_notes ASC;
    RETURN v_cordorder_notes;
END;
/

