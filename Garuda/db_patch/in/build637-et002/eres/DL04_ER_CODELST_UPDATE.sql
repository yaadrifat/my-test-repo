--STARTS UPDATING RECORD INTO ER_CODELST TABLE -
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst
    where codelst_type = 'note_assess'
    AND codelst_subtyp = 'no_cause';
  if (v_record_exists = 1) then
	UPDATE ER_CODELST SET CODELST_DESC='No Cause for Deferral' where codelst_type ='note_assess'AND codelst_subtyp = 'no_cause';
	commit;
  end if;
end;
/

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_33';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_DESC='No Results Received - Lab has not reported results (NR)' where codelst_type = 'alert_resol' AND codelst_subtyp = 'alert_33';
	commit;
  end if;
end;
/
--END--

--Update ER_CODELST for doc_categ 
Set define off;

DECLARE
  v_column_value_update number := 0;
BEGIN
  Select count(*) into v_column_value_update
    from user_tab_cols
    where TABLE_NAME = 'ER_CODELST'
    AND column_name = 'CODELST_DESC';
  if (v_column_value_update =1) then
   UPDATE ER_CODELST SET CODELST_DESC ='CBU Assessment and MRQ Attachment'
   WHERE  CODELST_TYPE='doc_categ' AND CODELST_SUBTYP='cbu_asmnt_cat';
   commit;
  end if;
end;
/
--END--

DECLARE
  v_column_value_update number := 0;
BEGIN
  Select count(*) into v_column_value_update
    from user_tab_cols
    where TABLE_NAME = 'ER_CODELST'
    AND column_name = 'CODELST_DESC';
  if (v_column_value_update =1) then
   UPDATE ER_CODELST SET CODELST_DESC ='Declaration of Urgent Medical Need (signed)'
   WHERE  CODELST_TYPE='eligibile_cat' AND CODELST_SUBTYP='ur_medi_need';
   commit;
  end if;
end;
/
--END--

--STARTS UPDATE THE COLUMN CODELST_SEQ IN ER_CODELST--
DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM ER_CODELST WHERE CODELST_TYPE='cord_acceptance' AND CODELST_SUBTYP='not_acceptable';
  
  IF (index_count = 1) THEN
    UPDATE ER_CODELST SET CODELST_SEQ=1 WHERE CODELST_TYPE='cord_acceptance' AND CODELST_SUBTYP='not_acceptable';
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/
COMMIT;



DECLARE
  index_count INTEGER;
BEGIN
  SELECT COUNT(*) INTO index_count FROM ER_CODELST WHERE CODELST_TYPE='cord_acceptance' AND CODELST_SUBTYP='acceptable';
  
  IF (index_count = 1) THEN
    UPDATE ER_CODELST SET CODELST_SEQ=2 WHERE CODELST_TYPE='cord_acceptance' AND CODELST_SUBTYP='acceptable';
    dbms_output.put_line('One row UPDATED');
  ELSE
    dbms_output.put_line('INDEX COUNT IS NOT EQUALS 1 GETTING MORE RECORDS');
  END IF;
END;
/
COMMIT;

--ENDS UPDATE THE COLUMN CODELST_SEQ IN ER_CODELST--

set define off;
--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'shipper' AND codelst_subtyp = 'fedex';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL='http://www.fedex.com/Tracking?action=track&tracknumbers=' where codelst_type = 'shipper' AND codelst_subtyp = 'fedex';
	commit;
  end if;
end;
/
--END--

--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'shipper' AND codelst_subtyp = 'ups';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL='http://wwwapps.ups.com/WebTracking/processInputRequest?TypeOfInquiryNumber=T&InquiryNumber1=' where codelst_type = 'shipper' AND codelst_subtyp = 'ups';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'shipper' AND codelst_subtyp = 'quick';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL='http://www.quickonline.com/cgi-bin/WebObjects/BOLSearch?bol=' where codelst_type = 'shipper' AND codelst_subtyp = 'quick';
	commit;
  end if;
end;
/
--END--


--STARTS UPDATING RECORD FROM ER_CODELST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from er_codelst 
where codelst_type = 'shipper' AND codelst_subtyp = 'world_cour';
  if (v_record_exists = 1) then
      UPDATE ER_CODELST SET CODELST_CUSTOM_COL='http://www.worldcouriercrc.com/CRC/indexCRCloginL.htm' where codelst_type = 'shipper' AND codelst_subtyp = 'world_cour';
	commit;
  end if;
end;
/
--END--

