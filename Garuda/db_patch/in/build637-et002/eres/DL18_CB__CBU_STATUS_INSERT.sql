DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='CA' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

  insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'CA','Cancel Request (CA)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='AV' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/



DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='CB' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'CB','Infused Date Passed (CB)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='CB' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/


DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='DS' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'DS','Damaged in Transit (DS)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='CD' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/

DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='DT' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'DT','Discrepant Typing (DT)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='QR' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/

DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='DU' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'DU','Discrepant Unit (DU)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='OT' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/


DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='IN' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'IN','Cord Infused (IN)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='IN' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/



DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='LS' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'LS','Lost in Transit (LS)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='OT' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/




DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='NR' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'NR','No Results Received - lab has not reported results in 30 days (NR)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='NA' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/


DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='PC' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'PC','Patient Condition Changed, No Infusion (PC)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='SH' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/



DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='PD' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'PD','Patient Died, No Infusion (PD)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='SH' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/



DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='SA' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'SA','Patient Not Ready to Proceed (SA)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='AV' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/


DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='SB' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'SB','Different Cord Chosen (SB)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='AV' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/



DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='SC' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'SC','Cord Incompatible (SC)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='AV' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/



DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='SD' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'SD','Patient Died (SD)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='AV' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/


DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='SE' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'SE','Other Cell Source Chosen (SE)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='AV' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/

DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='TC' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'TC','TC Decided Against Infusion (TC)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='SH' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/


DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='TS' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'TS','Thawed in Transit (TS)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='SH' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/


DECLARE 
V_COUNT_ROW NUMBER;
BEGIN
 SELECT COUNT(*) INTO V_COUNT_ROW FROM CB_CBU_STATUS WHERE CODE_TYPE='Resolution' and CBU_STATUS_CODE='XP' AND (DELETEDFLAG=0 OR DELETEDFLAG IS NULL);
 
 IF V_COUNT_ROW = 0 THEN

 insert into cb_cbu_status (pk_cbu_status,cbu_status_code,cbu_status_desc,code_type,cbb_ct,cbb_he,cbb_or,is_local_reg,is_national_reg,usa,aus,is_removable_reason,is_terminal_release,is_autocomplete_status,fk_cbu_status,creator,last_modified_by,last_modified_date,created_on,ip_add,rid,deletedflag,status_code_seq)
                    values(SEQ_CB_CBU_STATUS.nextval,'XP','Cord Expired (XP)','Resolution','0','0','0','0','0','0','0','0','0','0',(select pk_cbu_status from cb_cbu_status where code_type='Response' and cbu_status_code='NA' and (deletedflag=0 or deletedflag is null)),null,null,null,sysdate,null,null,null,null);
                    
  COMMIT;
 END IF;

END;
/



update cb_cbu_status set cbu_status_desc=TRIM(cbu_status_desc)||' ('|| TRIM(cbu_status_code) ||')' where code_type='Resolution' and cbu_status_desc not like '%('||TRIM(cbu_status_code)||')%';
COMMIT;



update cb_cbu_status set cbu_status_desc=trim(CBU_STATUS_DESC);
commit;
