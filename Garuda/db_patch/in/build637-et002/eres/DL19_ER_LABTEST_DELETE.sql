--STARTS DELETING RECORD INTO ER_LABTEST TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from ER_LABTEST
    where LABTEST_NAME = 'MonoFrozenNucleatedCellCount'
    AND LABTEST_SHORTNAME = 'MONO_FRZ';
  if (v_record_exists = 1) then
     delete from ER_LABTEST where LABTEST_NAME='MonoFrozenNucleatedCellCount' and LABTEST_SHORTNAME='MONO_FRZ';
	commit;
  end if;
end;
/
----END