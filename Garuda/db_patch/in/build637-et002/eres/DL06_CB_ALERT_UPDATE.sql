

--STARTS UPDATING RECORD FROM cb_alert_conditions TABLE--
DECLARE
  v_record_exists number := 0;  
BEGIN
  Select count(*) into v_record_exists
    from cb_alert_conditions 
where COLUMN_NAME in('CORD_NMDP_STATUS','FK_CORD_CBU_STATUS','FK_ORDER_RESOL_BY_CBB','FK_ORDER_RESOL_BY_TC');
  if (v_record_exists > 0) then
      update cb_alert_conditions set ARITHMETIC_OPERATOR=' IN' where COLUMN_NAME in('CORD_NMDP_STATUS','FK_CORD_CBU_STATUS','FK_ORDER_RESOL_BY_CBB','FK_ORDER_RESOL_BY_TC');
	commit;
  end if;
end;
/
--END--