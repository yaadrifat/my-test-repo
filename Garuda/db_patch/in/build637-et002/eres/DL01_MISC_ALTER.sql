--STARTS ADDING COLUMN TO CB_HLA--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_HLA'
    AND column_name = 'HLA_TYPING_SEQ';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_HLA ADD(HLA_TYPING_SEQ NUMBER(10))';
  end if;
end;
/

--STARTS ADDING COLUMN TO CB_HLA--

DECLARE
  v_column_exists number := 0;
BEGIN
  Select count(*) into v_column_exists
    from user_tab_cols
    where TABLE_NAME = 'CB_BEST_HLA'
    AND column_name = 'HLA_TYPING_SEQ';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE CB_BEST_HLA ADD(HLA_TYPING_SEQ NUMBER(10))';
  end if;
end;
/

COMMENT ON COLUMN CB_HLA.HLA_TYPING_SEQ IS 'Stores HLA Typing Sequence Number';

COMMENT ON COLUMN CB_BEST_HLA.HLA_TYPING_SEQ IS 'Stores HLA Typing Sequence Number';

--STARTS ADDING COLUMN TO ER_ORDER_HEADER TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'ER_ORDER_HEADER' AND COLUMN_NAME = 'ORDER_CLOSE_REASON';
  if (v_column_exists = 0) then
      execute immediate 'ALTER TABLE ER_ORDER_HEADER ADD ORDER_CLOSE_REASON NUMBER(10,0)';
	commit;
  end if;
end;
/
 COMMENT ON COLUMN ER_ORDER_HEADER.ORDER_CLOSE_REASON IS 'Stores Close reason at the time of receiving resolution';
 
 --STARTS ADDING COLUMN TO ER_ORDER_HEADER TABLE--
DECLARE
  v_column_exists number := 0;  
BEGIN
  Select count(*) into v_column_exists from user_tab_cols where  TABLE_NAME = 'CB_CBU_STATUS' AND COLUMN_NAME = 'CBU_STATUS_DESC';
  if (v_column_exists = 1) then
      execute immediate 'ALTER TABLE CB_CBU_STATUS MODIFY( CBU_STATUS_DESC VARCHAR2(100 CHAR))';
	commit;
  end if;
end;
/


COMMIT;