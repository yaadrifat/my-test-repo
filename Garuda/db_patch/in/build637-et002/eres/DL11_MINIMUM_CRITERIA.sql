CREATE TABLE "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA" 
   (	"PK_MINIMUM_CRITERIA_FIELD" NUMBER(10,0), 
	"FK_MINIMUM_CRITERIA" NUMBER(10,0), 
	"BACTERIAL_RESULT" NUMBER(10,0), 
	"FUNGAL_RESULT" NUMBER(10,0), 
	"HEMOGLOB_RESULT" NUMBER(10,0), 
	"HIVRESULT" NUMBER(10,0), 
	"HIVPRESULT" NUMBER(10,0), 
	"HBSAG_RESULT" NUMBER(10,0), 
	"HBVNAT_RESULT" NUMBER(10,0), 
	"HCV_RESULT" NUMBER(10,0), 
	"T_CRUZI_CHAGAS_RESULT" NUMBER(10,0), 
	"WNV_RESULT" NUMBER(10,0), 
	"TNCKG_PT_WEIGHT_RESULT" VARCHAR2(10 BYTE), 
	"VIABILITY_RESULT" NUMBER(10,0), 
	"CREATOR" NUMBER(10,0), 
	"CREATED_ON" DATE, 
	"LAST_MODIFIED_BY" NUMBER(10,0), 
	"LAST_MODIFIED_DATE" DATE, 
	"IP_ADD" VARCHAR2(15 BYTE), 
	"RID" NUMBER(10,0), 
	"DELETEDFLAG" NUMBER(10,0)
   );
 

   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."PK_MINIMUM_CRITERIA_FIELD" IS 'Primary key';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."FK_MINIMUM_CRITERIA" IS 'This Stores the forgin key of cb_cord_minimum_criteria table';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."BACTERIAL_RESULT" IS 'This field stores the Bacterial Culture result temporarily';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."FUNGAL_RESULT" IS 'This field stores the Fungal Culture result temporarily';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."HEMOGLOB_RESULT" IS 'This field stores the Hemoglobinopathy result temporarily';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."HIVRESULT" IS 'This field stores the Anti-HIV 1/2 result temporarily';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."HIVPRESULT" IS 'This field stores the HIV p24 or HIV NAT result temporarily';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."HBSAG_RESULT" IS 'This field stores the HBsAg result temporarily';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."HBVNAT_RESULT" IS 'This field stores the HBV NAT result temporarily';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."HCV_RESULT" IS 'This field stores the Anti-HCV or HCV NAT result temporarily';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."T_CRUZI_CHAGAS_RESULT" IS 'This field stores the T cruz I / Chagas result temporarily';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."WNV_RESULT" IS 'This field stores the WNV result temporarily';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."TNCKG_PT_WEIGHT_RESULT" IS 'This field stores theTCNCC / kg pt weight temporarily';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."VIABILITY_RESULT" IS 'This field stores the Viability result temporarily';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."CREATOR" IS 'Uses for Audit trail. Stores who created the record';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."CREATED_ON" IS 'Uses for Audit trail. Stores when created the record';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."LAST_MODIFIED_BY" IS 'Uses for Audit trail. Stores who modified the record recently';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."LAST_MODIFIED_DATE" IS 'Uses for Audit trail. Stores when modified the record recently';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."IP_ADD" IS 'Uses for Audit trail. Stores from whcih IP Address action is performed';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."RID" IS 'Uses for Audit trail. Stores sequence generated unique value';
 
   COMMENT ON COLUMN "ERES"."CB_CORD_TEMP_MINIMUM_CRITERIA"."DELETEDFLAG" IS 'Uses for Audit trail. Stores 1 if record is deleted,else stores 0 or null';
 

CREATE SEQUENCE SEQ_CB_TEMP_MINIMUM_CRITERIA MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;

