CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_CRI_VIEW_ALL" ("CRI", "CBU_STATUS", "CRI_CREATION_DATE", "PK_CORD", "CORD_REGISTRY_ID", "PK_CORD_COMPLETE_REQINFO")
AS
SELECT DECODE(REQINFO.COMPLETE_REQ_INFO_FLAG,'1','YES','NO'),
    CBUSTATUS.CBU_STATUS_DESC,
    REQINFO.CREATED_ON,
    CORD.PK_CORD,
    CORD.cord_registry_id,
    reqinfo.pk_cord_complete_reqinfo
  FROM ERES.CB_CORD CORD 
  LEFT OUTER JOIN eres.cb_cbu_status CBUSTATUS ON CORD.fk_cord_cbu_status = CBUSTATUS.pk_cbu_status,
  ERES.CB_CORD_COMPLETE_REQ_INFO REQINFO 
  where REQINFO.FK_CORD_ID        = CORD.PK_CORD
  ORDER BY PK_CORD ASC,
  CREATED_ON DESC;
/  

  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."CRI"
IS
  'This column will store Complete Required Information.';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."CBU_STATUS"
IS
  'This column store the local status of the cord.';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."CRI_CREATION_DATE"
IS
  'This column store CRI creation date.';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."PK_CORD"
IS
  'Primary key of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."CORD_REGISTRY_ID"
IS
  'This column stores  Cord Registry Id';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."PK_CORD_COMPLETE_REQINFO"
IS
  'Primary key of the cb_cord_complete_req_info table';
  COMMENT ON TABLE "VDA"."ETVDA_CBU_CRI_VIEW_ALL"
IS
  'This view provides Complete Required Information of all cords';
  
INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,12,1,'01_ETVDA_CBU_CRI_VIEW_ALL.sql',sysdate,'ETVDA 1.0 Build 12');

commit;  