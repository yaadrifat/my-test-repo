--------------------------ETVDA_PATIENT_HLA_HISTORY-------------------------
CREATE OR REPLACE VIEW "VDA"."ETVDA_PATIENT_HLA_HISTORY" AS
                            SELECT
                               h.pk_hla AS PK_PATIENT_HLA,                             
                               reci.pk_receipant AS PK_RECEIPANT,
                               cord.pk_cord as pk_cord,
                               ord.pk_order as PK_ORDER,
			                         reci.receipant_id as PATIENT_ID,
                               cord.cord_registry_id as cord_registry_id,
                               CASE
                                    WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
                                    AND ORD.ORDER_SAMPLE_AT_LAB             ='Y'
                                    THEN 'CT - Sample at Lab'
                                    WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
                                    AND ORD.ORDER_SAMPLE_AT_LAB             ='N'
                                    THEN 'CT - Ship Sample'
                                    ELSE ERES.F_CODELST_DESC(ORD.ORDER_TYPE)
                               END REQUEST_TYPE,
                               TO_CHAR(HDR.ORDER_OPEN_DATE,'Mon DD, YYYY') REQUEST_DT,
                               ERES.f_codelst_desc(h.fk_hla_code_id) AS LOCUS,
                               eres.F_CODELST_DESC(h.fk_hla_method_id) AS METHODDATA,
                               nvl(ec1.genomic_format,' ') AS TYPE1,
                               nvl(ec2.genomic_format,' ') AS TYPE2,
                               to_char(h.hla_received_date,'Mon DD, YYYY') AS RECEIVED_DT,
                               to_char(H.HLA_TYPING_DATE,'Mon DD, YYYY') AS TYPING_DT,
                               eres.f_getuser(h.creator) usrname,
                               to_char(h.created_on,'Mon DD, YYYY') as CREATEDON,
                               ERES.F_GETUSER(H.LAST_MODIFIED_BY) AS LASTMODIFIEDBY,
                               to_char(H.LAST_MODIFIED_DATE,'Mon DD, YYYY') as LASTMODIFIEDDT
                           FROM 
                               eres.CB_HLA h,
                               eres.cb_antigen_encod ec1,
                               eres.cb_antigen_encod ec2,
                               eres.cb_receipant_info reci,
                               ERES.ER_ORDER_RECEIPANT_INFO RECI_INFO,
                               ERES.ER_ORDER ORD,
                               ERES.CB_CORD CORD,
                               ERES.ER_ORDER_HEADER HDR
                           WHERE 
                                h.fk_hla_antigeneid1=ec1.fk_antigen (+)
                                AND ( ec1.version         =
                                  (SELECT MAX(ec1s.version)
                                  FROM ERES.cb_antigen_encod ec1s
                                  WHERE ec1.fk_antigen=ec1s.fk_antigen
                                  )
                                OR ec1.version IS NULL)
                                AND h.fk_hla_antigeneid2 = ec2.fk_antigen (+) 
                                AND (ec2.version         =
                                  (SELECT MAX(ec2s.version)
                                  FROM ERES.cb_antigen_encod ec2s
                                  WHERE ec2.fk_antigen=ec2s.fk_antigen
                                  )
                                OR ec2.version  IS NULL)
                                AND h.ENTITY_TYPE=(select pk_codelst from ERES.er_codelst where codelst_type='entity_type' and codelst_subtyp='PATIENT') 
                                AND h.fk_source=(select pk_codelst from ERES.er_codelst where codelst_type='test_source' and codelst_subtyp='patient')
                                AND reci.pk_receipant=h.entity_id
                                AND h.entity_id =RECI_INFO.FK_RECEIPANT
                                AND RECI_INFO.FK_ORDER_ID=ORD.PK_ORDER
                                AND RECI_INFO.FK_CORD_ID=CORD.PK_CORD
                                AND ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER
                                AND ORD.ORDER_TYPE IS NOT NULL
                          ORDER BY H.CB_HLA_ORDER_SEQ DESC NULLS LAST;
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."PK_PATIENT_HLA" IS 'PK of Patient HLA (PK_HLA)';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."PK_RECEIPANT" IS 'PK of Patient (PK_RECEIPANT)';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."PK_CORD" IS 'PK of Cord (PK_CORD)';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."PK_ORDER" IS 'PK of Order (PK_ORDER)';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."PATIENT_ID" IS 'Patient Id';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."CORD_REGISTRY_ID" IS 'Cord Registry ID of Cord';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."REQUEST_TYPE" IS 'Request Type like CT-Ship Sample, CT-Sample at Lab, HE, OR';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."REQUEST_DT" IS 'Request created Date';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."TYPE1" IS 'TYPE 1 value of Best HLA';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."TYPE2" IS 'TYPE 2 value of Best HLA';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."RECEIVED_DT" IS 'HLA received date';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."TYPING_DT" IS 'HLA Typing date';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."METHODDATA" IS 'Method name like DNA,SER';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."LOCUS" IS 'Locus like A,B,C,DRB1,DRB3,DRB4,DRB5,DQB1,DPB1';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."USRNAME" IS 'Name of the user entered Best HLA';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."CREATEDON" IS 'HLA Record created Date';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."LASTMODIFIEDBY" IS 'Name of the user last modified HLA';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_HLA_HISTORY"."LASTMODIFIEDDT" IS 'Last Modified Date';
/
----------------------------------ETVDA_PATIENT_BEST_HLA---------------------
CREATE OR REPLACE VIEW "VDA"."ETVDA_PATIENT_BEST_HLA" AS             
                      SELECT
                       h.pk_best_hla as pk_best_hla,
                       reci.pk_receipant as pk_receipant,
                       RECI_INFO.FK_CORD_ID as PK_CORD,
                       RECI_INFO.FK_ORDER_ID AS PK_ORDER,
                       reci.receipant_id as PATIENT_ID,
                       cord.cord_registry_id as cord_registry_id,
                       CASE
                        WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
                        AND ORD.ORDER_SAMPLE_AT_LAB             ='Y'
                        THEN 'CT - Sample at Lab'
                        WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
                        AND ORD.ORDER_SAMPLE_AT_LAB             ='N'
                        THEN 'CT - Ship Sample'
                        ELSE ERES.F_CODELST_DESC(ORD.ORDER_TYPE)
                       END REQUEST_TYPE,
                       TO_CHAR(HDR.ORDER_OPEN_DATE,'Mon DD, YYYY') REQUEST_DT,
                       nvl(ec1.genomic_format,' ') AS TYPE1,
                       nvl(ec2.genomic_format,' ') AS TYPE2,
                       to_char(h.hla_received_date,'Mon DD, YYYY') AS RECEIVED_DT,
                       to_char(H.HLA_TYPING_DATE,'Mon DD, YYYY') AS TYPING_DT,
                       eres.F_CODELST_DESC(h.fk_hla_method_id) AS METHODDATA,
                       eres.F_CODELST_DESC(h.fk_hla_code_id) AS LOCUS,
                       eres.f_getuser(h.creator) usrname,
                       to_char(h.created_on,'Mon DD, YYYY') as CREATEDON,
                       ERES.F_GETUSER(H.LAST_MODIFIED_BY) AS LASTMODIFIEDBY,
                       to_char(H.LAST_MODIFIED_DATE,'Mon DD, YYYY') as LASTMODIFIEDDT
                      FROM 
                        ERES.CB_BEST_HLA h,
                        eres.cb_antigen_encod ec1,
                        eres.cb_antigen_encod ec2,
                        eres.cb_receipant_info reci,
                        ERES.ER_ORDER_RECEIPANT_INFO RECI_INFO,
                        ERES.ER_ORDER ORD,
                        ERES.CB_CORD CORD,
                        ERES.ER_ORDER_HEADER HDR
                      WHERE 
                      h.ENTITY_TYPE=(select pk_codelst from ERES.er_codelst where codelst_type='entity_type' and codelst_subtyp='PATIENT') 
                      AND h.fk_source=(select pk_codelst from ERES.er_codelst where codelst_type='test_source' and codelst_subtyp='patient') 
                      AND h.fk_hla_antigeneid1=ec1.fk_antigen (+)
                      AND ( ec1.version       =
                        (SELECT MAX(ec1s.version)
                        FROM eres.cb_antigen_encod ec1s
                        WHERE ec1.fk_antigen=ec1s.fk_antigen
                        )
                      OR ec1.version          IS NULL)
                      AND h.fk_hla_antigeneid2 = ec2.fk_antigen (+)
                      AND (ec2.version         =
                        (SELECT MAX(ec2s.version)
                        FROM eres.cb_antigen_encod ec2s
                        WHERE ec2.fk_antigen=ec2s.fk_antigen
                        )
                      OR ec2.version IS NULL)
                      AND reci.pk_receipant=h.entity_id
                      AND h.entity_id =RECI_INFO.FK_RECEIPANT
                      AND RECI_INFO.FK_ORDER_ID=ORD.PK_ORDER
                      AND RECI_INFO.FK_CORD_ID=CORD.PK_CORD
                      AND ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER
                      AND ORD.ORDER_TYPE IS NOT NULL
                      ORDER BY reci.pk_receipant,eres.F_CODELST_DESC(h.fk_hla_code_id) nulls last;
/

COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."PK_BEST_HLA" IS 'PK of Best HLA (PK_BEST_HLA)';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."PK_RECEIPANT" IS 'PK of Patient (PK_RECEIPANT)';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."PK_CORD" IS 'PK of Cord (PK_CORD)';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."PK_ORDER" IS 'PK of Order (PK_ORDER)';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."PATIENT_ID" IS 'Patient Id';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."CORD_REGISTRY_ID" IS 'Cord Registry ID of Cord';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."REQUEST_TYPE" IS 'Request Type like CT-Ship Sample, CT-Sample at Lab, HE, OR';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."REQUEST_DT" IS 'Request created Date';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."TYPE1" IS 'TYPE 1 value of Best HLA';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."TYPE2" IS 'TYPE 2 value of Best HLA';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."RECEIVED_DT" IS 'HLA received date';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."TYPING_DT" IS 'HLA Typing date';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."METHODDATA" IS 'Method name like DNA,SER';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."LOCUS" IS 'Locus like A,B,C,DRB1,DRB3,DRB4,DRB5,DQB1,DPB1';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."USRNAME" IS 'Name of the user entered Best HLA';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."CREATEDON" IS 'HLA Record created Date';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."LASTMODIFIEDBY" IS 'Name of the user last modified Best HLA';
/
COMMENT ON COLUMN "VDA"."ETVDA_PATIENT_BEST_HLA"."LASTMODIFIEDDT" IS 'Last Modified Date';
/                    
--------------------------------ETVDA_HRSA_FUNDED---------------------------
CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_HRSA_FUNDED" ("CBU_REGISTRY_ID", "CBU_LOCAL_ID", "RACE_SUMMARY", "CORD_CREATION_DATE", "COLLECTION_DATE", "FUNDED_CBU", "FUNDING_CATEGORY", "BABY_BIRTH_DATE", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATOR", "PK_CORD")
AS
  SELECT CORD.CORD_REGISTRY_ID,
    CORD.CORD_LOCAL_CBU_ID,
    ERES.F_CODELST_DESC(CORD.NMDP_RACE_ID),
    CORD.CORD_CREATION_DATE,
    SPCMN.SPEC_COLLECTION_DATE,
    CORD.FUNDED_CBU,
    ERES.F_CODELST_DESC(CORD.FK_FUND_CATEG),
    CORD.CORD_BABY_BIRTH_DATE,
    ERES.F_GET_USER_NAME(CORD.LAST_MODIFIED_BY),
    CORD.LAST_MODIFIED_DATE,
    ERES.F_GET_USER_NAME(CORD.CREATOR),
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD,
    ERES.ER_SPECIMEN SPCMN,
    ERES.ER_SITE SITE
  WHERE CORD.FK_CBB_ID    = SITE.PK_SITE
  AND CORD.FK_SPECIMEN_ID = SPCMN.PK_SPECIMEN;
/
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_FUNDED"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_FUNDED"."CBU_LOCAL_ID"
IS
  'Local CBU ID of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_FUNDED"."RACE_SUMMARY"
IS
  'This column provide the Race summary for the Cord ';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_FUNDED"."CORD_CREATION_DATE"
IS
  'The date on which this cord is created in the system. ';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_FUNDED"."COLLECTION_DATE"
IS
  'Collection date of the specimen of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_FUNDED"."FUNDED_CBU"
IS
  'This column provide wheather Cord is Funded or not. Yes means Cord is Funded and No means cord is not funded ';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_FUNDED"."FUNDING_CATEGORY"
IS
  'This column provide wheather category of Funding.';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_FUNDED"."BABY_BIRTH_DATE"
IS
  'This column provide birth date of Baby';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_FUNDED"."LAST_MODIFIED_BY"
IS
  'Identifies the user who last modified this row.';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_FUNDED"."LAST_MODIFIED_DATE"
IS
  'The date on which this cord was last modified. ';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_FUNDED"."CREATOR"
IS
  'The Creator identifies the user who created the cord. ';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_FUNDED"."PK_CORD"
IS
  'Primary key of Cord';
/
------------------------ETVDA_HRSA_REQUEST---------------------------------
CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_HRSA_REQUEST" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_CBU_ID", "RACE_SUMMARY", "CORD_CREATION_DATE", "COLLECTION_DATE", "FUNDED_CBU", "FUNDING_CATEGORY", "BABY_BIRTH_DATE", "INFUSION_DATE", "ETINICITY", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATOR", "OR_SHIP_DATE", "SHIPTYPE", "PATIENT_WEIGHT", "PK_CORD")
AS
  SELECT SITE.SITE_ID,
    CORD.CORD_REGISTRY_ID,
    CORD.CORD_LOCAL_CBU_ID,
    ERES.F_CODELST_DESC(CORD.NMDP_RACE_ID),
    CORD.CORD_CREATION_DATE,
    SPEC.SPEC_COLLECTION_DATE,
    CORD.FUNDED_CBU,
    ERES.F_CODELST_DESC(CORD.FK_FUND_CATEG),
    CORD.CORD_BABY_BIRTH_DATE,
    CORD.INFUSION_DATE,
    ERES.F_CODELST_DESC(CORD.FK_CODELST_ETHNICITY),
    ERES.F_GET_USER_NAME(CORD.LAST_MODIFIED_BY),
    CORD.LAST_MODIFIED_DATE,
    ERES.F_GET_USER_NAME(CORD.CREATOR),
    ship.SCH_SHIPMENT_DATE,
    Eres.F_Codelst_Desc( Ship.Fk_Shipment_Type),
    Patient_Weight,
    Cord.Pk_Cord
  FROM eres.CB_CORD CORD
  LEFT JOIN eres.er_specimen SPEC
  ON CORD.FK_SPECIMEN_ID=spec.PK_SPECIMEN
  LEFT JOIN eres.er_site site
  ON CORD.fk_cbb_id=site.PK_SITE
  LEFT OUTER JOIN
    (SELECT REC.RECEIPANT_ID PATIENTID,
      HDR.ORDER_ENTITYID CORDID,
      ORD.PK_ORDER ORDERID,
      Ord.Order_Type Order_Type,
      REC.REC_WEIGHT Patient_Weight,
      ORD.ORDER_SAMPLE_AT_LAB ORDER_SAMPLE_AT_LAB
    FROM eres.CB_RECEIPANT_INFO REC,
      eres.ER_ORDER_RECEIPANT_INFO PRE,
      eres.ER_ORDER ORD,
      eres.ER_ORDER_HEADER HDR
    WHERE ORD.FK_ORDER_HEADER =HDR.PK_ORDER_HEADER
    AND ORD.PK_ORDER          =PRE.FK_ORDER_ID
    AND REC.PK_RECEIPANT      =PRE.FK_RECEIPANT
    AND ORD.ORDER_STATUS NOT IN
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type  ='order_status'
      AND codelst_subtyp IN('close_ordr','resolved')
      )
    AND ORD.ORDER_TYPE=
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='order_type'
      AND codelst_subtyp ='OR'
      )
    ) PAT
  ON(PAT.CORDID=CORD.PK_CORD)
  LEFT JOIN eres.CB_SHIPMENT ship
  ON Ship.Fk_Order_Id=Pat.Orderid
  AND CORD.PK_CORD   =PAT.CORDID;
/
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."CBB_ID"
IS
  'CBB ID of the Cord';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."CBU_LOCAL_CBU_ID"
IS
  'Local CBU ID of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."RACE_SUMMARY"
IS
  'This column provide the Race summary for the Cord ';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."CORD_CREATION_DATE"
IS
  'The date on which this cord is created in the system. ';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."COLLECTION_DATE"
IS
  'Collection date of the specimen of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."FUNDED_CBU"
IS
  'This column provide wheather Cord is Funded or not. Yes means Cord is Funded and No means cord is not funded ';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."FUNDING_CATEGORY"
IS
  'This column provide wheather category of Funding.';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."BABY_BIRTH_DATE"
IS
  'This column provide birth date of Baby';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."INFUSION_DATE"
IS
  'Date entered by the CM once the CBU has been infused (indication by the TC)';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."ETINICITY"
IS
  'Stores the etinicity of baby';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."LAST_MODIFIED_BY"
IS
  'Identifies the user who last modified this row.';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."LAST_MODIFIED_DATE"
IS
  'The date on which this cord was last modified. ';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."CREATOR"
IS
  'The Creator identifies the user who created the cord. ';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."OR_SHIP_DATE"
IS
  'This column stores OR Ship Date';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."SHIPTYPE"
IS
  'This column stores type of Shipment';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."PATIENT_WEIGHT"
IS
  'This column stores Recipient weight';
  COMMENT ON COLUMN "VDA"."ETVDA_HRSA_REQUEST"."PK_CORD"
IS
  'Primary key of Cord';
/
-------------------------ETVDA_RACE_DETAILS-------------------------
CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_RACE_DETAILS" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "RACE", "PK_CORD")
AS
  SELECT site.site_id,
    cord.cord_registry_id ,
    cord.cord_local_cbu_id,
    eres.f_codelst_Desc(race.FK_CODELST),
    cord.pk_cord
  FROM eres.cb_cord cord,
    eres.CB_MULTIPLE_VALUES race,
    eres.er_site site
  WHERE cord.pk_cord                        = race.entity_id
  AND cord.fk_cbb_id                        = site.PK_SITE
  AND eres.f_codelst_Desc(race.FK_CODELST) IS NOT NULL;
  COMMENT ON COLUMN "VDA"."ETVDA_RACE_DETAILS"."CBB_ID"
IS
  'CBB id of cord';
  COMMENT ON COLUMN "VDA"."ETVDA_RACE_DETAILS"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_RACE_DETAILS"."CBU_LOCAL_ID"
IS
  'Local CBU ID of the the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_RACE_DETAILS"."RACE"
IS
  'Race of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_RACE_DETAILS"."PK_CORD"
IS
  'Primary key of cb_cord.';
  COMMENT ON TABLE "VDA"."ETVDA_RACE_DETAILS"
IS
  'This view provides Race Details of the cord';
/
----------------------ETVDA_GET_SHIPMENT_INFO--------------------
CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_GET_SHIPMENT_INFO" ("PK_ORDER", "PK_CORD", "CORD_REGISTRY_ID", "REASON", "NMDP_STATUS", "PK_SITE", "SITE_ID", "REQUEST_STATUS", "REQUEST_TYPE", "REQUEST_OPEN_DATE", "REQUEST_PRIORITY", "REQUEST_LAST_REVIEWED_BY", "REQUEST_LAST_REVIEWED_DATE", "REQUEST_ASSIGNED_TO", "REQUEST_ASSIGNED_DATE", "RESULT_RECEIVED_DATE", "SCHEDULED_SHIP_DATE", "SCHEDULED_SHIP_TIME", "SCHEDULED_SHIP_DAY", "SHIPPING_COMPANY", "TRACKING_NO", "SHIPMENT_CONFIRM_ON", "SHIPMENT_CONFIRMED_BY", "EXPECT_ARR_DATE", "TEMPERATURE_MONITOR", "PAPERWORK_LOCATION", "PADLOCK_COMBINATION", "ADDITIONAL_SHIPPER_DET", "ACTUAL_SHIP_DATE", "SHIPPING_CONTAINER", "NMDP_SAMPL_SCH_SHIP_DATE", "NMDP_SAMPL_SCH_SHIP_TIME", "NMDP_SAMPL_SCH_SHIP_DAY", "NMDP_SAMPL_SHIPPING_COMPANY", "NMDP_SAMPL_TRACKING_NO", "SAMPLE_TYPE", "ALIQUOTS_TYPE", "LAB_CODE", "REQUEST_ACTIVE_DATE", "PATIENT_ID", "PATIENT_ENTITY_ID", "TC_RESOLUTION", "CBB_RESOLUTION", "CLOSE_REQUEST_REASON")
AS
  SELECT ORD.PK_ORDER,
    CRD.PK_CORD,
    CRD.CORD_REGISTRY_ID,
    ERES.F_GET_CBU_STATUS(CRD.FK_CORD_CBU_STATUS) REASON,
    ERES.F_GET_CBU_STATUS(CRD.CORD_NMDP_STATUS) NMDP_STATUS,
    SITE.PK_SITE,
    SITE.SITE_ID,
    ERES.F_CODELST_DESC(ORD.ORDER_STATUS) REQUEST_STATUS,
    CASE
      WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
      AND ORD.ORDER_SAMPLE_AT_LAB             ='Y'
      THEN 'CT - Sample at Lab'
      WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
      AND ORD.ORDER_SAMPLE_AT_LAB             ='N'
      THEN 'CT - Ship Sample'
      ELSE ERES.F_CODELST_DESC(ORD.ORDER_TYPE)
    END REQUEST_TYPE,
    TO_CHAR(HDR.ORDER_OPEN_DATE, 'Mon DD, YYYY') REQUEST_OPEN_DATE,
    ERES.F_CODELST_DESC(ORD.ORDER_PRIORITY) REQUEST_PRIORITY,
    ERES.F_GET_USER_NAME(ORD.ORDER_LAST_VIEWED_BY) REQUEST_LAST_REVIEWED_BY,
    TO_CHAR(ORD.ORDER_LAST_VIEWED_DATE, 'Mon DD, YYYY') REQUEST_LAST_REVIEWED_DATE,
    ERES.F_GET_USER_NAME(ORD.ASSIGNED_TO) REQUEST_ASSIGNED_TO,
    TO_CHAR(ORD.ORDER_ASSIGNED_DATE,'Mon DD, YYYY') REQUEST_ASSIGNED_DATE,
    TO_CHAR(ORD.RESULT_REC_DATE,'Mon DD, YYYY') RESULT_RECEIVED_DATE,
    CBUSHIP.SCHEDULED_SHIP_DATE,
    CBUSHIP.SCHEDULED_SHIP_TIME,
    CBUSHIP.SCHEDULED_SHIP_DAY,
    CBUSHIP.SHIPPING_COMPANY,
    CBUSHIP.TRACKING_NO,
    CBUSHIP.SHIPMENT_CONFIRM_ON,
    CBUSHIP.SHIPMENT_CONFIRMED_BY,
    CBUSHIP.EXPECT_ARR_DATE,
    CBUSHIP.TEMPERATURE_MONITOR,
    CBUSHIP.PAPERWORK_LOCATION,
    CBUSHIP.PADLOCK_COMBINATION,
    CBUSHIP.ADDITIONAL_SHIPPER_DET,
    CBUSHIP.ACTUAL_SHIP_DATE,
    CBUSHIP.SHIPPING_CONTAINER,
    NMDPSHIP.NMDP_SAMPL_SCH_SHIP_DATE,
    NMDPSHIP.NMDP_SAMPL_SCH_SHIP_TIME,
    NMDPSHIP.NMDP_SAMPL_SCH_SHIP_DAY,
    NMDPSHIP.NMDP_SAMPL_SHIPPING_COMPANY,
    NMDPSHIP.NMDP_SAMPL_TRACKING_NO,
    ERES.F_CODELST_DESC(ORD.FK_SAMPLE_TYPE_AVAIL) SAMPLE_TYPE,
    ERES.F_CODELST_DESC(ORD.FK_ALIQUOTS_TYPE) ALIQUOTS_TYPE,
    ERES.F_CODELST_DESC(ORD.LAB_CODE) LAB_CODE,
    TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(HDR.ORDER_OPEN_DATE,'Mon DD, YYYY'),'Mon DD, YYYY') REQUEST_ACTIVE_DATE,
    RECIPIENT_INFO.RECEIPANT_ID PATIENT_ID,
    RECIPIENT_INFO.PK_RECEIPANT PATIENT_ENTITY_ID,
    ERES.F_GET_CBU_STATUS(ORD.FK_ORDER_RESOL_BY_TC) TC_RESOLUTION,
    ERES.F_GET_CBU_STATUS(ORD.FK_ORDER_RESOL_BY_CBB) CBB_RESOLUTION,
    ERES.F_GET_CBU_STATUS(HDR.ORDER_CLOSE_REASON) CLOSE_REQUEST_REASON
  FROM ERES.ER_ORDER ORD
  INNER JOIN ERES.ER_ORDER_HEADER HDR
  ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER)
  INNER JOIN ERES.CB_CORD CRD
  ON (CRD.PK_CORD=HDR.ORDER_ENTITYID)
  INNER JOIN ERES.ER_SITE SITE
  ON (SITE.PK_SITE=CRD.FK_CBB_ID)
  LEFT OUTER JOIN
    (SELECT CBUSHIPMENT.PK_SHIPMENT,
      CBUSHIPMENT.FK_ORDER_ID ORDERID,
      TO_CHAR(CBUSHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY') SCHEDULED_SHIP_DATE,
      TO_CHAR(CBUSHIPMENT.SHIPMENT_DATE,'Mon DD, YYYY') ACTUAL_SHIP_DATE,
      CBUSHIPMENT.SCH_SHIPMENT_TIME SCHEDULED_SHIP_TIME,
      TO_CHAR(CBUSHIPMENT.SCH_SHIPMENT_DATE,'Day') SCHEDULED_SHIP_DAY,
      ERES.F_CODELST_DESC(CBUSHIPMENT.FK_SHIP_COMPANY_ID) SHIPPING_COMPANY,
      CBUSHIPMENT.SHIPMENT_TRACKING_NO TRACKING_NO,
      TO_CHAR(CBUSHIPMENT.SHIPMENT_DATE,'Mon DD, YYYY') SHIPMENT_CONFIRM_ON,
      ERES.F_GETUSER(CBUSHIPMENT.SHIPMENT_CONFIRM_BY) SHIPMENT_CONFIRMED_BY,
      TO_CHAR(CBUSHIPMENT.EXPECT_ARR_DATE,'Mon DD, YYYY') EXPECT_ARR_DATE,
      ERES.F_CODELST_DESC(CBUSHIPMENT.FK_SHIPPING_CONT) SHIPPING_CONTAINER,
      ERES.F_CODELST_DESC(CBUSHIPMENT.FK_TEMPERATURE_MONITER) TEMPERATURE_MONITOR,
      ERES.F_CODELST_DESC(CBUSHIPMENT.PAPERWORK_LOCATION) PAPERWORK_LOCATION,
      CBUSHIPMENT.ADDITI_SHIPPER_DET ADDITIONAL_SHIPPER_DET,
      CBUSHIPMENT.PADLOCK_COMBINATION PADLOCK_COMBINATION
    FROM ERES.CB_SHIPMENT CBUSHIPMENT
    WHERE CBUSHIPMENT.FK_SHIPMENT_TYPE=
      (SELECT PK_CODELST
      FROM ERES.ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='CBU'
      )
    ) CBUSHIP
  ON (CBUSHIP.ORDERID=ORD.PK_ORDER)
  LEFT OUTER JOIN
    (SELECT NMDPSHIPMENT.PK_SHIPMENT,
      NMDPSHIPMENT.FK_ORDER_ID ORDERID,
      TO_CHAR(NMDPSHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY') NMDP_SAMPL_SCH_SHIP_DATE,
      NMDPSHIPMENT.SCH_SHIPMENT_TIME NMDP_SAMPL_SCH_SHIP_TIME,
      TO_CHAR(NMDPSHIPMENT.SCH_SHIPMENT_DATE,'Day') NMDP_SAMPL_SCH_SHIP_DAY,
      ERES.F_CODELST_DESC(NMDPSHIPMENT.FK_SHIP_COMPANY_ID) NMDP_SAMPL_SHIPPING_COMPANY,
      NMDPSHIPMENT.SHIPMENT_TRACKING_NO NMDP_SAMPL_TRACKING_NO
    FROM ERES.CB_SHIPMENT NMDPSHIPMENT
    WHERE NMDPSHIPMENT.FK_SHIPMENT_TYPE=
      (SELECT PK_CODELST
      FROM ERES.ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='NMDP'
      )
    ) NMDPSHIP
  ON (NMDPSHIP.ORDERID=ORD.PK_ORDER)
  LEFT OUTER JOIN ERES.ER_ORDER_RECEIPANT_INFO ORDER_RECIPIENT_INFO
  ON (ORDER_RECIPIENT_INFO.FK_ORDER_ID=ORD.PK_ORDER)
  LEFT OUTER JOIN ERES.CB_RECEIPANT_INFO RECIPIENT_INFO
  ON (RECIPIENT_INFO.PK_RECEIPANT = ORDER_RECIPIENT_INFO.FK_RECEIPANT)
  WHERE crd.fk_cord_cbu_status   IN
    (SELECT CS.pk_cbu_status
    FROM ERES.CB_CBU_STATUS CS
    WHERE CS.code_type      ='Response'
    AND CS.cbu_status_code IN('SH','IN')
    );
/
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."PK_ORDER"
IS
  'Order Primary Key';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."PK_CORD"
IS
  'Cord Primary Key';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."CORD_REGISTRY_ID"
IS
  'Cord Registry ID';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."REASON"
IS
  'Reason';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."NMDP_STATUS"
IS
  'Nmdp Status';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."PK_SITE"
IS
  'This column uniquely identifies the siteorganization. This is Primary Key for the table. ';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."SITE_ID"
IS
  'This column stores an Identification number for the site';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."REQUEST_STATUS"
IS
  'Status of the Order';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."REQUEST_OPEN_DATE"
IS
  'Stores The date when the Case Management resources started working on the Order';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."REQUEST_PRIORITY"
IS
  'Stores pk_codelst value where codelst_type id order_priority';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."REQUEST_LAST_REVIEWED_BY"
IS
  'Stores foreign key reference to er_user table. This column indicates who viewed the order lastly';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."REQUEST_LAST_REVIEWED_DATE"
IS
  'Stores date when the order view confirmed';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."REQUEST_ASSIGNED_TO"
IS
  'Cord Blood Bank assigns a search request to a CORD Link user';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."REQUEST_ASSIGNED_DATE"
IS
  'A date is entered into this field by the system when the Assigned To field is populated.';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."RESULT_RECEIVED_DATE"
IS
  'A date is entered into this field by the system when the Assigned To field is populated.';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."SCHEDULED_SHIP_DATE"
IS
  'date the sample was shipped to NMDP contract lab.';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."SCHEDULED_SHIP_TIME"
IS
  'Storing sheduled shipping time';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."SCHEDULED_SHIP_DAY"
IS
  'day the sample was shipped to NMDP contract lab.';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."SHIPPING_COMPANY"
IS
  'SHIPPING COMPANY';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."TRACKING_NO"
IS
  'sample shipment tracking number';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."SHIPMENT_CONFIRM_ON"
IS
  'DateTime when the CBB confirms the scheduled ship date';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."SHIPMENT_CONFIRMED_BY"
IS
  'SHIPMENT CONFIRMED BY USER';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."EXPECT_ARR_DATE"
IS
  'Date the Case Manager expects the CBU to arrive at the TC';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."TEMPERATURE_MONITOR"
IS
  'Type of temperature monitor utilized by the CBB';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."PAPERWORK_LOCATION"
IS
  'Stores where paper work is located in the shipper';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."PADLOCK_COMBINATION"
IS
  'Stores shippers padlock combination';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."ADDITIONAL_SHIPPER_DET"
IS
  'Stores additional details regarding return of shipper';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."ACTUAL_SHIP_DATE"
IS
  'date the sample was shipped to NMDP contract lab.';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."SHIPPING_CONTAINER"
IS
  'Type of shipping container utilized by the CBB';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."NMDP_SAMPL_SCH_SHIP_DATE"
IS
  'date the sample was shipped to NMDP contract lab.';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."NMDP_SAMPL_SCH_SHIP_TIME"
IS
  'Storing sheduled shipping time';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."NMDP_SAMPL_SCH_SHIP_DAY"
IS
  'day the sample was shipped to NMDP contract lab.';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."NMDP_SAMPL_SHIPPING_COMPANY"
IS
  'SHIPPING COMPANY';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."NMDP_SAMPL_TRACKING_NO"
IS
  'sample shipment tracking number';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."SAMPLE_TYPE"
IS
  'Stores fk codelst for sample types';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."ALIQUOTS_TYPE"
IS
  'Stores fk codelst for aliquots types';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."LAB_CODE"
IS
  'Stores foreign key reference to er_codelst table for NMDP Lab codes where CT sample is being tested';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."REQUEST_ACTIVE_DATE"
IS
  'Request Active Date';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."PATIENT_ID"
IS
  'This column stores an Identification number for the patient';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."PATIENT_ENTITY_ID"
IS
  'Stores foreign key reference to cb_receipant_info table';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."TC_RESOLUTION"
IS
  'Foreign key column.stores code of resolution code type value from code list table';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."CBB_RESOLUTION"
IS
  'Foreign key column.stores code of resolution code type value from code list table';
  COMMENT ON COLUMN "VDA"."ETVDA_GET_SHIPMENT_INFO"."CLOSE_REQUEST_REASON"
IS
  'Stores Close reason at the time of receiving resolution';
/
--------------------------------ETVDA_CBU_BEST_HLA_INFO--------------------
CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_BEST_HLA_INFO" ("CBB_ID", "CBU_REGISTRY_ID", "NO_OF_SEGMENTS", "HLA_VALUE_TYPE1", "HLA_VALUE_TYPE2", "HLA_DESC", "HLA_SOURCE", "RECEIVED_DATE", "PK_CORD", "PK_HLA", "PK_ENTITY_SAMPLES")
AS
  SELECT SITE.SITE_ID,
    C.CORD_REGISTRY_ID,
    SMPLE.NO_OF_SEG_AVAIL,
    EC1.GENOMIC_FORMAT AS HLA_TYPE1,
    EC2.GENOMIC_FORMAT AS HLA_TYPE2,
    ERES.F_CODELST_DESC(H.FK_HLA_CODE_ID),
    ERES.F_CODELST_DESC(H.FK_SOURCE),
    H.HLA_RECEIVED_DATE,
    C.PK_CORD,
    H.PK_BEST_HLA,
    SMPLE.PK_ENTITY_SAMPLES
  FROM ERES.CB_BEST_HLA H,
    ERES.CB_CORD C,
    ERES.ER_SITE SITE,
    ERES.CB_ANTIGEN_ENCOD EC1,
    ERES.CB_ANTIGEN_ENCOD EC2,
    ERES.CB_ENTITY_SAMPLES SMPLE
  WHERE H.FK_HLA_ANTIGENEID1=EC1.FK_ANTIGEN (+)
  AND ( EC1.VERSION         =
    (SELECT MAX(EC1S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC1S
    WHERE EC1.FK_ANTIGEN=EC1S.FK_ANTIGEN
    )
  OR EC1.VERSION          IS NULL)
  AND H.FK_HLA_ANTIGENEID2 = EC2.FK_ANTIGEN (+)
  AND (EC2.VERSION         =
    (SELECT MAX(EC2S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC2S
    WHERE EC2.FK_ANTIGEN=EC2S.FK_ANTIGEN
    )
  OR EC2.VERSION                                  IS NULL)
  AND H.ENTITY_ID                                  =C.PK_CORD
  AND lower(ERES.F_CODELST_DESC(H.ENTITY_TYPE))    ='cbu'
  AND SMPLE.ENTITY_ID                              =C.PK_CORD
  AND lower(ERES.F_CODELST_DESC(SMPLE.ENTITY_TYPE))='cbu'
  AND C.FK_CBB_ID                                  = SITE.PK_SITE;
/
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_BEST_HLA_INFO"."CBB_ID"
IS
  'CBB ID OF CORD';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_BEST_HLA_INFO"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_BEST_HLA_INFO"."NO_OF_SEGMENTS"
IS
  'This column will store the No of Segment Available.';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_BEST_HLA_INFO"."HLA_VALUE_TYPE1"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 1.';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_BEST_HLA_INFO"."HLA_VALUE_TYPE2"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 2.';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_BEST_HLA_INFO"."HLA_DESC"
IS
  'Description of Code Id for HLA';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_BEST_HLA_INFO"."HLA_SOURCE"
IS
  'Description of Source for HLA';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_BEST_HLA_INFO"."RECEIVED_DATE"
IS
  'HLA Received date.';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_BEST_HLA_INFO"."PK_CORD"
IS
  'Primary key of table CB_CORD';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_BEST_HLA_INFO"."PK_HLA"
IS
  'Primary key of table CB_HLA';
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_BEST_HLA_INFO"."PK_ENTITY_SAMPLES"
IS
  'Primary key of table CB_ENTITY_SAMPLES';
  COMMENT ON TABLE "VDA"."ETVDA_CBU_BEST_HLA_INFO"
IS
  'This view provides Best HLA Information of cords ';
/
--------------------------ETVDA_IDM_FORM_DATA-----------------------
CREATE OR REPLACE FORCE VIEW VDA.ETVDA_IDM_FORM_DATA (CORD_REGISTRY_ID, FORM_VERSION, QUESTION_SNO, QUESTION, RESPONSE, BLOOD_COLLECTION_DATE, COMMENTS,ASSESSMENT_RESPONSE,
   ASSESSMENT_COMMENT,
   ASSESSMENT_FLAG_FOR_LATER,
   ASSESSMENT_FLAG_COMMENT,
   ASSESSMENT_VISIBLE_TO_TC,ACTIVE_FLAG, PK_CORD, PK_FORM, PK_QUESTION, QUES_CODE, PK_FORM_VERSION,PK_ASSESSMENT,
   ASSESSMENT_SUB_ENTITY_ID,
   ASSESSMENT_SUB_ENTITY_TYPE
   )
AS
SELECT crd.cord_registry_id,
    frm.ver,
    forq.ques_seq,
    CASE
      WHEN ques_desc IS NULL THEN formres.resp_val
      WHEN ques_desc IS NOT NULL THEN ques_desc
    END Question,
    CASE
      WHEN formres.resp_code IS NOT NULL THEN ERES.f_codelst_desc(formres.resp_code)
      WHEN formres.dynaformDate IS NOT NULL THEN formres.dynaformDate
    END Response,
    fver.date_momans_or_formfill,
    formres.comments,
    ERES.f_codelst_desc(formres.assessment_res),
    formres.assessment_Comment,
    CASE 
      WHEN formres.Flag_Later IS NULL THEN formres.Flag_Later
      WHEN formres.Flag_Later='0' THEN 'No'
      WHEN formres.Flag_Later='1' THEN 'Yes'
    END Flag_Later,
    formres.flag_comment,
    CASE 
      WHEN formres.TC_Flag IS NULL THEN formres.TC_Flag
      WHEN formres.TC_Flag='0' THEN 'No'
      WHEN formres.TC_Flag='1' THEN 'Yes'
    END Flag_Later,
    CASE 
      WHEN fver.void_flag IS NULL  THEN 'Active'
      WHEN fver.void_flag='1'  THEN 'Inactive'
    END flag,
    crd.pk_cord,
    frm.pk_form,
    formres.fk_question,
    ques.ques_code,
    fver.pk_form_version,
    formres.pk_assessment,
    formres.sub_entity_id,
    formres.SUB_ENTITY_TYPE
  FROM ERES.cb_form_questions forq,
    (SELECT f.pk_form,
      f.VERSION ver,
      f.forms_desc frmdesc
    FROM ERES.cb_forms f
    WHERE forms_desc='IDM'
    ) frm,
    ERES.cb_form_version fver,
    ERES.cb_cord crd,
    ERES.cb_questions ques
  LEFT OUTER JOIN
    (SELECT frmres.pk_form_responses,
      frmres.fk_question,
      frmres.response_code resp_code,
      frmres.response_value resp_val,
      frmres.comments,
      frmres.fk_form_version,
      frmres.fk_form fk_form,
      TO_CHAR(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate,
      asses.pk_assessment,
      asses.sub_entity_id,
      asses.TC_Flag,
      asses.Flag_Later,
      asses.SUB_ENTITY_TYPE,
      asses.assessment_Comment,
      asses.assessment_res,
      asses.flag_comment
    FROM ERES.cb_form_responses frmres
  LEFT OUTER JOIN
    (SELECT 
      pk_assessment pk_assessment,
      sub_entity_id sub_entity_id,
      TC_VISIBILITY_FLAG TC_Flag,
      FLAG_FOR_LATER Flag_Later,
      ERES.f_get_codelstdesc(SUB_ENTITY_TYPE) SUB_ENTITY_TYPE,
      ASSESSMENT_REMARKS assessment_Comment,
      ASSESSMENT_FOR_RESPONSE assessment_res,
      ASSESSMENT_FLAG_COMMENT flag_comment
      FROM ERES.cb_assessment where SUB_ENTITY_TYPE=(SELECT pk_codelst  FROM ERES.er_codelst  WHERE codelst_type='sub_entity_type'  AND codelst_subtyp='IDM')
    ) asses  ON( asses.sub_entity_id=frmres.pk_form_responses )
    ) formres
  ON(formres.FK_QUESTION  =ques.pk_questions)
  WHERE ques.pk_questions =forq.fk_question
  AND fver.pk_form_version=formres.fk_form_version
  AND fver.entity_id      =crd.pk_cord
  AND forq.fk_form        =frm.pk_form
  AND fver.fk_form        =frm.pk_form
  AND formres.fk_form     =frm.pk_form;
/
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.CORD_REGISTRY_ID IS 'CORD REGISTRY ID';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.FORM_VERSION IS 'FORM VERSION DESCIPTION';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.QUESTION_SNO IS 'QUESTION S.NO';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.QUESTION IS 'QUESTION DESCIPTION';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.RESPONSE IS 'RESPONSE FOR THE QUESTION';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.BLOOD_COLLECTION_DATE IS 'IDM BLOOD COLLECTION DATE';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.COMMENTS IS 'COMMENTS FOR THE QUESTION';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.PK_CORD IS 'PRIMARY KEY OF ERES.CB_CORD TABLE';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.PK_FORM IS 'PRIMARY KEY OF ERES.CB_FORM TABLE';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.PK_QUESTION IS 'PRIMARY KEY OF ERES.CB_QUESTION TABLE';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.QUES_CODE IS 'UNIQUE CODE OF EACH QUESTION FROM ERES.CB_QUESTION TABLE';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.PK_FORM_VERSION IS 'PRIMARY KEY OF ERES.CB_FORM_VERSION TABLE';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.ACTIVE_FLAG IS 'ACTIVE/INACTIVE FLAG';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.ASSESSMENT_RESPONSE IS 'ASSESSMENT RESPONSE';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.ASSESSMENT_COMMENT IS 'ASSESSMENT COMMENT';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.ASSESSMENT_FLAG_FOR_LATER IS 'ASSESSMENT FLAG FOR LATER';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.ASSESSMENT_FLAG_COMMENT IS 'ASSESSMENT FLAG COMMENT';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.ASSESSMENT_VISIBLE_TO_TC IS 'VISIBLE TO TC FLAG';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.PK_ASSESSMENT IS 'PRIMARY KEY OF ERES.CB_ASSESSMENT TABLE';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.ASSESSMENT_SUB_ENTITY_ID IS 'FORGIN KEY OF ERES.CB_FORM_RESPONSE TABLE';
COMMENT ON COLUMN VDA.ETVDA_IDM_FORM_DATA.ASSESSMENT_SUB_ENTITY_TYPE IS 'FORM TYPE';
COMMENT ON TABLE "VDA"."ETVDA_IDM_FORM_DATA"
IS
  'This view provides IDM Form information';
/  
--END--
--VIEW TO GET THE MRQ FORM DATA--
CREATE OR REPLACE FORCE VIEW VDA.ETVDA_MRQ_FORM_DATA (CORD_REGISTRY_ID, FORM_VERSION, QUESTION_SNO, QUESTION, RESPONSE, DATE_MOM_ANS_QUES, COMMENTS,ASSESSMENT_RESPONSE,
   ASSESSMENT_COMMENT,
   ASSESSMENT_FLAG_FOR_LATER,
   ASSESSMENT_FLAG_COMMENT,
   ASSESSMENT_VISIBLE_TO_TC,ACTIVE_FLAG, PK_CORD, PK_FORM, PK_QUESTION, QUES_CODE, PK_FORM_VERSION,PK_ASSESSMENT,
   ASSESSMENT_SUB_ENTITY_ID,
   ASSESSMENT_SUB_ENTITY_TYPE
   )
AS
  SELECT crd.cord_registry_id,
    frm.ver,
    forq.ques_seq,
    CASE
      WHEN ques_desc IS NULL
      THEN formres.resp_val
      WHEN ques_desc IS NOT NULL
      THEN ques_desc
    END Question,
    CASE
      WHEN formres.resp_code IS NOT NULL THEN ERES.f_codelst_desc(formres.resp_code)
      WHEN formres.resp_val IS NOT NULL  THEN formres.resp_val
    END Response,
    fver.date_momans_or_formfill,
    formres.comments,
    ERES.f_codelst_desc(formres.assessment_res),
    formres.assessment_Comment,
    CASE 
      WHEN formres.Flag_Later IS NULL THEN formres.Flag_Later
      WHEN formres.Flag_Later='0' THEN 'No'
      WHEN formres.Flag_Later='1' THEN 'Yes'
    END Flag_Later,
    formres.flag_comment,
    CASE 
      WHEN formres.TC_Flag IS NULL THEN formres.TC_Flag
      WHEN formres.TC_Flag='0' THEN 'No'
      WHEN formres.TC_Flag='1' THEN 'Yes'
    END Flag_Later,
    CASE 
      WHEN fver.void_flag IS NULL  THEN 'Active'
      WHEN fver.void_flag='1'  THEN 'Inactive'
    END flag,
    crd.pk_cord,
    frm.pk_form,
    formres.fk_question,
    ques.ques_code,
    fver.pk_form_version,
    formres.pk_assessment,
    formres.sub_entity_id,
    formres.SUB_ENTITY_TYPE
  FROM ERES.cb_form_questions forq,
    (SELECT f.pk_form,
      f.VERSION ver,
      f.forms_desc frmdesc
    FROM ERES.cb_forms f
    WHERE forms_desc='MRQ'
    ) frm,
    ERES.cb_form_version fver,
    ERES.cb_cord crd,
    ERES.cb_questions ques
  LEFT OUTER JOIN
    (SELECT frmres.pk_form_responses,
      frmres.fk_question,
      frmres.response_code resp_code,
      frmres.response_value resp_val,
      frmres.comments,
      frmres.fk_form_version,
      frmres.fk_form fk_form,
      TO_CHAR(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate,
      asses.pk_assessment,
      asses.sub_entity_id,
      asses.TC_Flag,
      asses.Flag_Later,
      asses.SUB_ENTITY_TYPE,
      asses.assessment_Comment,
      asses.assessment_res,
      asses.flag_comment
    FROM ERES.cb_form_responses frmres
    LEFT OUTER JOIN
    (SELECT 
      pk_assessment pk_assessment,
      sub_entity_id sub_entity_id,
      TC_VISIBILITY_FLAG TC_Flag,
      FLAG_FOR_LATER Flag_Later,
      ERES.f_get_codelstdesc(SUB_ENTITY_TYPE) SUB_ENTITY_TYPE,
      ASSESSMENT_REMARKS assessment_Comment,
      ASSESSMENT_FOR_RESPONSE assessment_res,
      ASSESSMENT_FLAG_COMMENT flag_comment
      FROM ERES.cb_assessment where SUB_ENTITY_TYPE=(SELECT pk_codelst  FROM ERES.er_codelst  WHERE codelst_type='sub_entity_type'  AND codelst_subtyp='MRQ')
    ) asses  ON( asses.sub_entity_id=frmres.pk_form_responses )
    ) formres
  ON(formres.FK_QUESTION  =ques.pk_questions)
  WHERE ques.pk_questions =forq.fk_question
  AND fver.pk_form_version=formres.fk_form_version
  AND fver.entity_id      =crd.pk_cord
  AND forq.fk_form        =frm.pk_form
  AND fver.fk_form        =frm.pk_form
  AND formres.fk_form     =frm.pk_form;
/
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.CORD_REGISTRY_ID IS 'CORD REGISTRY ID';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.FORM_VERSION IS 'FORM VERSION DESCIPTION';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.QUESTION_SNO IS 'QUESTION S.NO';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.QUESTION IS 'QUESTION DESCIPTION';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.RESPONSE IS 'RESPONSE FOR THE QUESTION';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.DATE_MOM_ANS_QUES IS 'MRQ DATE MOM ANSWERED QUESTIONNAIRE';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.COMMENTS IS 'COMMENTS FOR THE QUESTION';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.PK_CORD IS 'PRIMARY KEY OF ERES.CB_CORD TABLE';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.PK_FORM IS 'PRIMARY KEY OF ERES.CB_FORM TABLE';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.PK_QUESTION IS 'PRIMARY KEY OF ERES.CB_QUESTION TABLE';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.QUES_CODE IS 'UNIQUE CODE OF EACH QUESTION FROM ERES.CB_QUESTION TABLE';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.PK_FORM_VERSION IS 'PRIMARY KEY OF ERES.CB_FORM_VERSION TABLE';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.ACTIVE_FLAG IS 'ACTIVE/INACTIVE FLAG';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.ASSESSMENT_RESPONSE IS 'ASSESSMENT RESPONSE';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.ASSESSMENT_COMMENT IS 'ASSESSMENT COMMENT';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.ASSESSMENT_FLAG_FOR_LATER IS 'ASSESSMENT FLAG FOR LATER';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.ASSESSMENT_FLAG_COMMENT IS 'ASSESSMENT FLAG COMMENT';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.ASSESSMENT_VISIBLE_TO_TC IS 'VISIBLE TO TC FLAG';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.PK_ASSESSMENT IS 'PRIMARY KEY OF ERES.CB_ASSESSMENT TABLE';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.ASSESSMENT_SUB_ENTITY_ID IS 'FORGIN KEY OF ERES.CB_FORM_RESPONSE TABLE';
COMMENT ON COLUMN VDA.ETVDA_MRQ_FORM_DATA.ASSESSMENT_SUB_ENTITY_TYPE IS 'FORM TYPE';
COMMENT ON TABLE "VDA"."ETVDA_MRQ_FORM_DATA"
IS
  'This view provides MRQ Form information';
/
--END--

--VIEW TO GET THE FMHQ FORM DATA--
CREATE OR REPLACE FORCE VIEW VDA.ETVDA_FMHQ_FORM_DATA (CORD_REGISTRY_ID, FORM_VERSION, QUESTION_SNO, QUESTION, RESPONSE, FORM_COMPLETION_DATE, COMMENTS, ASSESSMENT_RESPONSE,
  ASSESSMENT_COMMENT,
  ASSESSMENT_FLAG_FOR_LATER,
  ASSESSMENT_FLAG_COMMENT,
  ASSESSMENT_VISIBLE_TO_TC,ACTIVE_FLAG,PK_CORD, PK_FORM, PK_QUESTION, QUES_CODE, PK_FORM_VERSION, PK_ASSESSMENT,
  ASSESSMENT_SUB_ENTITY_ID,
  ASSESSMENT_SUB_ENTITY_TYPE
  )
AS
  SELECT crd.cord_registry_id,
    frm.ver,
    forq.ques_seq,
    CASE
      WHEN ques_desc IS NULL
      THEN formres.resp_val
      WHEN ques_desc IS NOT NULL
      THEN ques_desc
    END Question,
    CASE
      WHEN formres.resp_code IS NOT NULL THEN ERES.f_codelst_desc(formres.resp_code)
      WHEN formres.resp_val IS NOT NULL AND ERES.f_codelst_desc(ques.response_type) ='multiselect' AND ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )>0 THEN ERES.f_codelstcommaval_to_desc(formres.resp_val,',')
      WHEN formres.resp_val IS NOT NULL AND ERES.f_codelst_desc(ques.response_type) ='multiselect' AND ( LENGTH(formres.resp_val) - LENGTH( REPLACE(formres.resp_val, ',', '') ) )=0 THEN ERES.f_codelst_desc(formres.resp_val)
      WHEN formres.resp_val IS NOT NULL AND ERES.f_codelst_desc(ques.response_type)='textfield' THEN formres.resp_val
    END Response,
    fver.date_momans_or_formfill,
    formres.comments,
    CASE 
      WHEN formres.assessment_res IS NOT NULL AND formres.assess_reason IS NOT NULL THEN ERES.f_codelst_desc(formres.assess_reason)||' - '||ERES.f_codelst_desc(formres.assessment_res)
    END assessment_res,
    formres.assessment_Comment,
    CASE 
      WHEN formres.Flag_Later IS NULL THEN formres.Flag_Later
      WHEN formres.Flag_Later='0' THEN 'No'
      WHEN formres.Flag_Later='1' THEN 'Yes'
    END Flag_Later,
    formres.flag_comment,
    CASE 
      WHEN formres.TC_Flag IS NULL THEN formres.TC_Flag
      WHEN formres.TC_Flag='0' THEN 'No'
      WHEN formres.TC_Flag='1' THEN 'Yes'
    END Flag_Later,
    CASE 
      WHEN fver.void_flag IS NULL  THEN 'Active'
      WHEN fver.void_flag='1'  THEN 'Inactive'
    END flag,
    crd.pk_cord,
    frm.pk_form,
    formres.fk_question,
    ques.ques_code,
    fver.pk_form_version,
    formres.pk_assessment,
    formres.sub_entity_id,
    formres.SUB_ENTITY_TYPE
  FROM ERES.cb_form_questions forq,
    (SELECT f.pk_form,
      f.VERSION ver,
      f.forms_desc frmdesc
    FROM ERES.cb_forms f
    WHERE forms_desc='FMHQ'
    ) frm,
    ERES.cb_form_version fver,
    ERES.cb_cord crd,
    ERES.cb_questions ques
  LEFT OUTER JOIN
    (SELECT frmres.pk_form_responses,
      frmres.fk_question,
      frmres.response_code resp_code,
      frmres.response_value resp_val,
      frmres.comments,
      frmres.fk_form_version,
      frmres.fk_form fk_form,
      TO_CHAR(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate,
      asses.pk_assessment,
      asses.sub_entity_id,
      asses.TC_Flag,
      asses.Flag_Later,
      asses.SUB_ENTITY_TYPE,
      asses.assessment_Comment,
      asses.assessment_res,
      asses.flag_comment,
      asses.assess_reason,
      asses.assess_reason_remark
    FROM ERES.cb_form_responses frmres
    LEFT OUTER JOIN
    (SELECT 
      pk_assessment pk_assessment,
      sub_entity_id sub_entity_id,
      TC_VISIBILITY_FLAG TC_Flag,
      FLAG_FOR_LATER Flag_Later,
      ERES.f_get_codelstdesc(SUB_ENTITY_TYPE) SUB_ENTITY_TYPE,
      ASSESSMENT_REMARKS assessment_Comment,
      ASSESSMENT_FOR_RESPONSE assessment_res,
      ASSESSMENT_FLAG_COMMENT flag_comment,
      ASSESSMENT_REASON assess_reason,
      ASSESSMENT_REASON_REMARKS assess_reason_remark
      FROM ERES.cb_assessment where SUB_ENTITY_TYPE=(SELECT pk_codelst  FROM ERES.er_codelst  WHERE codelst_type='sub_entity_type'  AND codelst_subtyp='FMHQ')
    ) asses  ON( asses.sub_entity_id=frmres.pk_form_responses )
    ) formres
  ON(formres.FK_QUESTION  =ques.pk_questions)
  WHERE ques.pk_questions =forq.fk_question
  AND fver.pk_form_version=formres.fk_form_version
  AND fver.entity_id      =crd.pk_cord
  AND forq.fk_form        =frm.pk_form
  AND fver.fk_form        =frm.pk_form
  AND formres.fk_form     =frm.pk_form;
/
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.CORD_REGISTRY_ID IS 'CORD REGISTRY ID';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.FORM_VERSION IS 'FORM VERSION DESCIPTION';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.QUESTION_SNO IS 'QUESTION S.NO';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.QUESTION IS 'QUESTION DESCIPTION';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.RESPONSE IS 'RESPONSE FOR THE QUESTION';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.FORM_COMPLETION_DATE IS 'FMHQ FORM COMPLETION DATE';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.COMMENTS IS 'COMMENTS FOR THE QUESTION';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.PK_CORD IS 'PRIMARY KEY OF ERES.CB_CORD TABLE';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.PK_FORM IS 'PRIMARY KEY OF ERES.CB_FORM TABLE';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.PK_QUESTION IS 'PRIMARY KEY OF ERES.CB_QUESTION TABLE';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.QUES_CODE IS 'UNIQUE CODE OF EACH QUESTION FROM ERES.CB_QUESTION TABLE';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.PK_FORM_VERSION IS 'PRIMARY KEY OF ERES.CB_FORM_VERSION TABLE';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.ACTIVE_FLAG IS 'ACTIVE/INACTIVE FLAG';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.ASSESSMENT_RESPONSE IS 'ASSESSMENT RESPONSE';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.ASSESSMENT_COMMENT IS 'ASSESSMENT COMMENT';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.ASSESSMENT_FLAG_FOR_LATER IS 'ASSESSMENT FLAG FOR LATER';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.ASSESSMENT_FLAG_COMMENT IS 'ASSESSMENT FLAG COMMENT';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.ASSESSMENT_VISIBLE_TO_TC IS 'VISIBLE TO TC FLAG';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.PK_ASSESSMENT IS 'PRIMARY KEY OF ERES.CB_ASSESSMENT TABLE';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.ASSESSMENT_SUB_ENTITY_ID IS 'FORGIN KEY OF ERES.CB_FORM_RESPONSE TABLE';
COMMENT ON COLUMN VDA.ETVDA_FMHQ_FORM_DATA.ASSESSMENT_SUB_ENTITY_TYPE IS 'FORM TYPE';

COMMENT ON TABLE "VDA"."ETVDA_FMHQ_FORM_DATA"
IS
  'This view provides FMHQ Form information';
/
--END--
------------------ETVDA_ER_ORDER_USERS_AUDIT------------------------
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ER_ORDER_USERS_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_ORDER_USER", "USER_LOGINID") AS 
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp, usr.pk_order_user, usr.user_loginid
  FROM eres.audit_column_module acm, eres.er_order_users usr,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND arm.module_id = usr.pk_order_user
  AND arm.table_name       = 'ER_ORDER_USERS'
  AND acm.column_name NOT IN ('RID','DELETEDFLAG','USER_COMMENTS','PK_ORDER_USER','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY');
  /
     COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT"."PK_ORDER_USER"
IS
  'Primary key of er_order_users table';
  /
    COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT"."USER_LOGINID"
IS
  'Login ID of User';
  /
  COMMENT ON TABLE "VDA"."ETVDA_ER_ORDER_USERS_AUDIT"
IS
  'This view contains the information about the Order Users';
  /
------------------------ETVDA_CB_RECEIPANT_INFO_AUDIT---------------
 CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CB_RECEIPANT_INFO_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_RECEIPANT", "RECEIPANT_ID") AS 
  SELECT acm.column_name,
   acm.column_display_name,
   (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
   ) user_id,
   TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
   DECODE(arm.action,'I','Add','U','Update','-') action,
   acm.old_value,
   acm.new_value,
    arm.table_name,
    arm.timestamp, rec.pk_receipant, rec.receipant_id
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm, eres.cb_receipant_info rec
  WHERE acm.fk_row_id = arm.pk_row_id
  AND arm.module_id = rec.pk_receipant
 AND arm.table_name       = 'CB_RECEIPANT_INFO'
 AND acm.column_name NOT IN ('PK_RECEIPANT','FK_PERSON_ID','REC_WEIGHT','ENTRY_DATE','GUID','REC_HEIGHT','RID','DELETEDFLAG','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY');
  /
   COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO_AUDIT"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO_AUDIT"."PK_RECEIPANT"
IS
  'Primary key of cb_receipant_info table';
  /
    COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO_AUDIT"."RECEIPANT_ID"
IS
  'ID of Recipient';
  /

  COMMENT ON TABLE "VDA"."ETVDA_CB_RECEIPANT_INFO_AUDIT"
IS
  'This view contains the information about the Recipient';
  / 
----------------------ETVDA_ER_ORDER_USERS_AUDIT_ALL-----------------
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ER_ORDER_USERS_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_ORDER_USER", "USER_LOGINID") AS 
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp, usr.pk_order_user, usr.user_loginid
  FROM eres.audit_column_module acm, eres.er_order_users usr,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND arm.module_id = usr.pk_order_user
  AND arm.table_name       = 'ER_ORDER_USERS';
  /
     COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT_ALL"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT_ALL"."PK_ORDER_USER"
IS
  'Primary key of er_order_users table';
  /
    COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS_AUDIT_ALL"."USER_LOGINID"
IS
  'Login ID of User';
  /
  COMMENT ON TABLE "VDA"."ETVDA_ER_ORDER_USERS_AUDIT_ALL"
IS
  'This view contains the information about the Order Users';
/
-----------------ETVDA_CB_RECEPNTINFO_AUDIT_ALL-------------------------
 CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CB_RECEPNTINFO_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_RECEIPANT", "RECEIPANT_ID") AS 
  SELECT acm.column_name,
   acm.column_display_name,
   (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
   ) user_id,
   TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
   DECODE(arm.action,'I','Add','U','Update','-') action,
   acm.old_value,
   acm.new_value,
    arm.table_name,
    arm.timestamp, rec.pk_receipant, rec.receipant_id
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm, eres.cb_receipant_info rec
  WHERE acm.fk_row_id = arm.pk_row_id
  AND arm.module_id = rec.pk_receipant
 AND arm.table_name       = 'CB_RECEIPANT_INFO';
  /
   COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEPNTINFO_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEPNTINFO_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEPNTINFO_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEPNTINFO_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEPNTINFO_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEPNTINFO_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEPNTINFO_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEPNTINFO_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEPNTINFO_AUDIT_ALL"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEPNTINFO_AUDIT_ALL"."PK_RECEIPANT"
IS
  'Primary key of cb_receipant_info table';
  /
    COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEPNTINFO_AUDIT_ALL"."RECEIPANT_ID"
IS
  'ID of Recipient';
  /

  COMMENT ON TABLE "VDA"."ETVDA_CB_RECEPNTINFO_AUDIT_ALL"
IS
  'This view contains the information about the Recipient';
  / 
  
  INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,4,3,'03_NewViews.sql',sysdate,'ETVDA 1.0 Build 4');

commit;
