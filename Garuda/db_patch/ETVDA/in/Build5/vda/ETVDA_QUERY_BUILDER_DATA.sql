CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_QUERY_BUILDER_DATA" ("PK_CORD", "REGISTRY_ID", "REGISTRY_MATERNAL_ID", "CORD_LOCAL_CBU_ID", "MATERNAL_LOCAL_ID", "CORD_ISBI_DIN_CODE", "PK_SITE", "SITE_ID", "PATIENT_ID", "CORD_ID_NUMBER_ON_CBU_BAG", "CORD_HISTORIC_CBU_ID", "REQ_TYPE", "ORDER_SAMPLE_AT_LAB", "SHIP_DATE", "ORDER_OPEN_DATE", "FK_SHIPMENT_TYPE", "INFUSION_DATE", "CURRENT_DIAGNOSIS", "TRANS_SITE_NAME", "TRANS_SITE_ID", "ORDER_TYPE", "SPEC_COLLECTION_DATE", "CORD_CREATION_DATE", "CORD_REGISTRATION_DATE", "FUNDED_CBU", "CORD_NMDP_STATUS", "FK_CORD_CBU_STATUS", "FK_CORD_CBU_ELIGIBLE_STATUS", "FK_CORD_CBU_LIC_STATUS", "CORD_BABY_BIRTH_DATE", "FK_CORD_BACT_CUL_RESULT", "FK_CORD_FUNGAL_CUL_RESULT", "HEMOGLOBIN_SCRN", "FK_CBB_PROCEDURE", "FK_CODELST_RACE", "FK_CORD_CBU_INELIGIBLE_REASON", "FK_CORD_CBU_UNLICENSED_REASON", "NMDP_RACE_ID", "FK_SPECIMEN_ID", "UNLICENSED_DESC", "INELIG_INCOMP_DESC", "RACE_DESC")
AS
  SELECT DISTINCT(C.Pk_Cord),
    C.Cord_Registry_Id,
    C.Registry_Maternal_Id,
    C.Cord_Local_Cbu_Id,
    C.Maternal_Local_Id,
    C.Cord_Isbi_Din_Code,
    SITE.PK_SITE ,
    Site.Site_Id,
    Cb_Rec_Info.Receipant_Id,
    C.Cord_Id_Number_On_Cbu_Bag ,
    C.Cord_Historic_Cbu_Id,
    CASE
      WHEN ERES.f_codelst_desc(ord.order_type)='CT'
      AND ord.order_sample_at_lab             ='Y'
      THEN 'CT - Sample at Lab'
      WHEN ERES.f_codelst_desc(ord.order_type)='CT'
      AND ord.order_sample_at_lab             ='N'
      THEN 'CT - Ship Sample'
      ELSE ERES.F_Codelst_Desc(Ord.Order_Type)
    END Req_Type ,
    ORD.ORDER_SAMPLE_AT_LAB,
    SHIPMENT.sch_shipment_date SHIP_DATE,
    HDR.ORDER_OPEN_DATE ORDER_OPEN_DATE,
    SHIPMENT.FK_SHIPMENT_TYPE,
    ORD.INFUSION_DATE INFUSION_DATE,
    CB_REC_INFO.CURRENT_DIAGNOSIS CURRENT_DIAGNOSIS,
    TRANS_SITE.SITE_NAME,
    TRANS_SITE.SITE_ID,
    Ord.Order_Type ,
    spec.SPEC_COLLECTION_DATE,
    C.CORD_CREATION_DATE,
    C.CORD_REGISTRATION_DATE,
    C.FUNDED_CBU,
    C.CORD_NMDP_STATUS,
    C.FK_CORD_CBU_STATUS,
    C.Fk_Cord_Cbu_Eligible_Status,
    C.Fk_Cord_Cbu_Lic_Status,
    C.Cord_Baby_Birth_Date,
    C.Fk_Cord_Bact_Cul_Result,
    C.Fk_Cord_Fungal_Cul_Result,
    C.Hemoglobin_Scrn,
    C.Fk_Cbb_Procedure,
    C.Fk_Codelst_Race,
    C.FK_CORD_CBU_INELIGIBLE_REASON,
    C.Fk_Cord_Cbu_Unlicensed_Reason,
    C.Nmdp_Race_Id,
    C.FK_SPECIMEN_ID,
    ERES.ROWTOCOL('SELECT ERES.F_CODELST_DESC(REASON.FK_REASON_ID) FROM ERES.CB_CORD CORD INNER JOIN ERES.CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN  ERES.CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS  WHERE CORD.FK_CORD_CBU_UNLICENSED_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD ='
    ||C.PK_CORD
    ||'',',') AS UNLICENSED_DESC,
    ERES.ROWTOCOL('SELECT ERES.F_CODELST_DESC(REASON.FK_REASON_ID) FROM ERES.CB_CORD CORD INNER JOIN ERES.CB_ENTITY_STATUS STAT ON STAT.ENTITY_ID=CORD.PK_CORD INNER JOIN  ERES.CB_ENTITY_STATUS_REASON REASON ON REASON.FK_ENTITY_STATUS=STAT.PK_ENTITY_STATUS  WHERE CORD.FK_CORD_CBU_INELIGIBLE_REASON=STAT.PK_ENTITY_STATUS AND CORD.PK_CORD ='
    ||C.PK_CORD
    ||'',',') AS INELIG_INCOMP_DESC,
    ERES.ROWTOCOL('SELECT ERES.f_codelst_Desc(race.FK_CODELST)  FROM eres.CB_CORD cord, eres.CB_MULTIPLE_VALUES race WHERE cord.pk_cord  = race.entity_id and race.FK_CODELST_TYPE=''race'' and cord.pk_cord ='
    ||C.PK_CORD
    ||'',',') AS RACE_DESC
  FROM ERES.Cb_Cord C
  LEFT JOIN Eres.Er_Specimen Spec
  ON C.Fk_Specimen_Id=Spec.Pk_Specimen
  LEFT OUTER JOIN Eres.Er_Order_Header Hdr
  ON (Hdr.Order_Entityid=C.Pk_Cord)
  LEFT OUTER JOIN Eres.Er_Order Ord
  ON (Ord.Fk_Order_Header=Hdr.Pk_Order_Header )
  LEFT OUTER JOIN Eres.Er_Order_Receipant_Info Order_Rec_Info
  ON (Order_Rec_Info.Fk_Order_Id=Ord.Pk_Order)
  LEFT OUTER JOIN Eres.Cb_Receipant_Info Cb_Rec_Info
  ON(Order_Rec_Info.Fk_Receipant=Cb_Rec_Info.Pk_Receipant)
  LEFT OUTER JOIN Eres.Cb_Shipment Shipment
  ON (Shipment.Fk_Order_Id= Ord.Pk_Order )
  LEFT OUTER JOIN Eres.Er_Site Trans_Site
  ON ( Cb_Rec_Info.Trans_Center_Id=Trans_Site.Pk_Site)
  LEFT JOIN Eres.Er_Site Site
  ON C.Fk_Cbb_Id=Site.Pk_Site;
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."PK_CORD"
IS
  'Primary key of cb_cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."REGISTRY_ID"
IS
  'Registry id of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."REGISTRY_MATERNAL_ID"
IS
  'Maternal Registry ID for Cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_LOCAL_CBU_ID"
IS
  'Local CBU ID of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."MATERNAL_LOCAL_ID"
IS
  'Maternal Local ID for Cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_ISBI_DIN_CODE"
IS
  'ISBT CODE for the Cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."PK_SITE"
IS
  'This column identifies the site/organization.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."SITE_ID"
IS
  'This column identifies the site/organization.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."PATIENT_ID"
IS
  'This column stores Patient ID.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_ID_NUMBER_ON_CBU_BAG"
IS
  'UNIQUE ID Number given over Bag';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_HISTORIC_CBU_ID"
IS
  'This column stores Historical CBU ID.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."REQ_TYPE"
IS
  'This column stores Request Type.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."ORDER_SAMPLE_AT_LAB"
IS
  'This column stores Order Sample at lab.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."SHIP_DATE"
IS
  'This column stores Shipment Date.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."ORDER_OPEN_DATE"
IS
  'This column stores Open Date of Order.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FK_SHIPMENT_TYPE"
IS
  'This column stores type of Shipment.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."INFUSION_DATE"
IS
  'This column stores infusion date.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CURRENT_DIAGNOSIS"
IS
  'This column stores current diagonisis.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."TRANS_SITE_NAME"
IS
  'This column stores Site Name.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."TRANS_SITE_ID"
IS
  'This column stores Site ID.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."ORDER_TYPE"
IS
  'This column stores Order Type.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."SPEC_COLLECTION_DATE"
IS
  'Collection date of the specimen of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_CREATION_DATE"
IS
  'The date on which this cord is created in the system. ';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_REGISTRATION_DATE"
IS
  'This column stores Cord Registatrion Date';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FUNDED_CBU"
IS
  'This column provide wheather Cord is Funded or not. Yes means Cord is Funded and No means cord is not funded ';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_NMDP_STATUS"
IS
  'This column stores the NMDP status of the cord.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FK_CORD_CBU_STATUS"
IS
  'This column stores the status of the cord.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FK_CORD_CBU_ELIGIBLE_STATUS"
IS
  'This column stores the Eligibility status of the cord.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FK_CORD_CBU_LIC_STATUS"
IS
  'This column stores the Licensure status of the cord.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_BABY_BIRTH_DATE"
IS
  'This column provide birth date of Baby';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FK_CORD_BACT_CUL_RESULT"
IS
  'This column stores the Bacterial Culture Result.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FK_CORD_FUNGAL_CUL_RESULT"
IS
  'This column stores the Fungal Culture Result.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."HEMOGLOBIN_SCRN"
IS
  'This column stores the Homoglobinopathy Screening.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FK_CBB_PROCEDURE"
IS
  'This column stores procedure associated with cord.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FK_CODELST_RACE"
IS
  'This column stores reference to Race.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FK_CORD_CBU_INELIGIBLE_REASON"
IS
  'This column stores Ineligible Reason.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FK_CORD_CBU_UNLICENSED_REASON"
IS
  'This column stores Unlicensure Reason.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."NMDP_RACE_ID"
IS
  'This column stores NMDP Race ID.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FK_SPECIMEN_ID"
IS
  'This column stores Specimen ID.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."UNLICENSED_DESC"
IS
  'This column store Unlicensed Reason description.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."INELIG_INCOMP_DESC"
IS
  'This column store Ineligible/Incomplete Reason description.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."RACE_DESC"
IS
  'This column store Race Description';
  COMMENT ON TABLE "VDA"."ETVDA_QUERY_BUILDER_DATA"
IS
  'This view store Query Builder Data.';