CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA" ("PK_BEST_HLA", "FK_HLA_CODE_ID", "FK_HLA_METHOD_ID", "HLA_VALUE_TYPE1", "HLA_VALUE_TYPE2", "CREATOR", "CREATED_ON", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "IP_ADD", "DELETEDFLAG", "RID", "ENTITY_ID", "ENTITY_TYPE", "FK_CORD_CDR_CBU_ID", "FK_HLA_OVERCLS_CODE", "HLA_CRIT_OVER_DATE", "HLA_OVER_IND_ANTGN1", "HLA_OVER_IND_ANTGN2", "FK_HLA_PRIM_DNA_ID", "FK_HLA_PROT_DIPLOID", "FK_HLA_ANTIGENEID1", "FK_HLA_ANTIGENEID2", "HLA_RECEIVED_DATE", "HLA_TYPING_DATE", "FK_HLA_REPORTING_LAB", "FK_HLA_REPORT_METHOD", "FK_ORDER_ID", "FK_CORD_CT_STATUS", "FK_SPEC_TYPE", "FK_SOURCE", "HLA_TYPING_SEQ", "CB_BEST_HLA_ORDER_SEQ", "HLA_REPORT_METHOD_CODE", "HLA_TYPE1", "HLA_TYPE2", "HLA_USER")
AS
  SELECT h.PK_BEST_HLA,
    h.FK_HLA_CODE_ID,
    h.FK_HLA_METHOD_ID,
    h.HLA_VALUE_TYPE1,
    h.HLA_VALUE_TYPE2,
    h.CREATOR,
    h.CREATED_ON,
    h.LAST_MODIFIED_BY,
    h.LAST_MODIFIED_DATE,
    h.IP_ADD,
    h.DELETEDFLAG,
    h.RID,
    h.ENTITY_ID,
    h.ENTITY_TYPE,
    h.FK_CORD_CDR_CBU_ID,
    h.FK_HLA_OVERCLS_CODE,
    h.HLA_CRIT_OVER_DATE,
    h.HLA_OVER_IND_ANTGN1,
    h.HLA_OVER_IND_ANTGN2,
    h.FK_HLA_PRIM_DNA_ID,
    h.FK_HLA_PROT_DIPLOID,
    h.FK_HLA_ANTIGENEID1,
    h.FK_HLA_ANTIGENEID2,
    h.HLA_RECEIVED_DATE,
    h.HLA_TYPING_DATE,
    h.FK_HLA_REPORTING_LAB,
    h.FK_HLA_REPORT_METHOD,
    h.FK_ORDER_ID,
    h.FK_CORD_CT_STATUS,
    h.FK_SPEC_TYPE,
    h.FK_SOURCE,
    h.HLA_TYPING_SEQ,
    h.CB_BEST_HLA_ORDER_SEQ,
    h.HLA_REPORT_METHOD_CODE,
    ec1.genomic_format HLA_TYPE1 ,
    ec2.genomic_format HLA_TYPE2,
    u.usr_lastname
    || ' '
    || u.usr_firstname
  FROM ERES.CB_BEST_HLA h,
    ERES.cb_antigen_encod ec1,
    ERES.cb_antigen_encod ec2,
    ERES.er_user u
  WHERE h.fk_hla_antigeneid1=ec1.fk_antigen (+)
  AND ( ec1.version         =
    (SELECT MAX(ec1s.version)
    FROM ERES.cb_antigen_encod ec1s
    WHERE ec1.fk_antigen=ec1s.fk_antigen
    )
  OR ec1.version          IS NULL)
  AND h.fk_hla_antigeneid2 = ec2.fk_antigen (+)
  AND (ec2.version         =
    (SELECT MAX(ec2s.version)
    FROM ERES.cb_antigen_encod ec2s
    WHERE ec2.fk_antigen=ec2s.fk_antigen
    )
  OR ec2.version IS NULL)
  AND h.creator   = u.pk_user
  ORDER BY h.CB_BEST_HLA_ORDER_SEQ DESC nulls last;
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."PK_BEST_HLA"
IS
  'Primary Key';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."FK_HLA_CODE_ID"
IS
  'Code Id for HLA';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."FK_HLA_METHOD_ID"
IS
  'Method ID from Code List for HLA';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."HLA_VALUE_TYPE1"
IS
  'Value for type1 HLA';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."HLA_VALUE_TYPE2"
IS
  'Value for type2 HLA';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."CREATOR"
IS
  'This column is used for Audit Trail. The Creator identifies the user who created this row.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."CREATED_ON"
IS
  'This column is used for Audit Trail. Stores the date on which this row was created.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."LAST_MODIFIED_BY"
IS
  'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."LAST_MODIFIED_DATE"
IS
  'This column is used for Audit Trail. Stores the date on which this row was last modified.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."IP_ADD"
IS
  'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."DELETEDFLAG"
IS
  'This column denotes whether record is deleted. ';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."RID"
IS
  'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."ENTITY_ID"
IS
  'Id of the entity like CBU, Doner etc';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."ENTITY_TYPE"
IS
  'Type of entity';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."FK_HLA_OVERCLS_CODE"
IS
  'HLA Override class code.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."HLA_CRIT_OVER_DATE"
IS
  'HLA Critical override date - esb mapping.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."HLA_OVER_IND_ANTGN1"
IS
  'Indicator for override Antigen1.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."HLA_OVER_IND_ANTGN2"
IS
  'Indicator for override Antigen2';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."FK_HLA_PRIM_DNA_ID"
IS
  'Primary DNA ID - esb message.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."FK_HLA_PROT_DIPLOID"
IS
  'HLA ProteinDiplotypeListId - esb message.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."FK_HLA_ANTIGENEID1"
IS
  'HLA ANTIGENEID1 - Type 1.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."FK_HLA_ANTIGENEID2"
IS
  'HLA ANTIGENEID2 - Type 2.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."HLA_RECEIVED_DATE"
IS
  'HLA Received date.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."HLA_TYPING_DATE"
IS
  'HLA Typing date.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."FK_HLA_REPORTING_LAB"
IS
  'Reference for reporting LAB.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."FK_HLA_REPORT_METHOD"
IS
  'HLA Reporting method code, reference to ER_CODELST.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."FK_ORDER_ID"
IS
  'ORDER reference ID';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."FK_CORD_CT_STATUS"
IS
  'This column will store the fk of cord ct status table';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."FK_SPEC_TYPE"
IS
  'This field store FK of Specimen Type.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."FK_SOURCE"
IS
  'This field store source of antigen.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."HLA_TYPING_SEQ"
IS
  'Stores HLA Typing Sequence Number';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."CB_BEST_HLA_ORDER_SEQ"
IS
  'Store the seq id of hlas';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."HLA_REPORT_METHOD_CODE"
IS
  'HLA Reporting method code, stores from ESB';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."HLA_USER"
IS
  'This column stores name of the user who has created HLA';

COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."HLA_TYPE1"
IS
  'This column stores genomic format of Type1 HLA';


COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"."HLA_TYPE2"
IS
  'This column stores genomic format of Type2 HLA.';

  COMMENT ON TABLE "VDA"."ETVDA_QUERY_BUILDER_BHLA_DATA"
IS
  'This view stores BEST HLA records.';