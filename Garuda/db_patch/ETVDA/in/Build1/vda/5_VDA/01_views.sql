--EmTrax views for Version ET028 - Release Date 02-14-2013

CREATE TABLE VDA_TRACK_PATCHES
(
  PATCH_PK       NUMBER(38)                     NOT NULL,
  DB_VER_MJR     NUMBER(10,2),
  DB_VER_MNR     NUMBER(10),
  DB_PATCH_NAME  VARCHAR2(40 BYTE),
  FIRED_ON       DATE                           DEFAULT sysdate,
  APP_VERSION    VARCHAR2(20 BYTE)
)
/

COMMENT ON TABLE VDA_TRACK_PATCHES IS 'This table is used to track patches which are applied in every build.'
/

COMMENT ON COLUMN VDA_TRACK_PATCHES.PATCH_PK IS 'Primary Key'
/

COMMENT ON COLUMN VDA_TRACK_PATCHES.DB_VER_MJR IS 'Identifies database major version'
/

COMMENT ON COLUMN VDA_TRACK_PATCHES.DB_VER_MNR IS 'Identifies database minor version'
/

COMMENT ON COLUMN VDA_TRACK_PATCHES.DB_PATCH_NAME IS 'This column stores the name of the patch file fired on the database'
/

COMMENT ON COLUMN VDA_TRACK_PATCHES.FIRED_ON IS 'This column stores the date on which the database patch is fired'
/

COMMENT ON COLUMN VDA_TRACK_PATCHES.APP_VERSION IS 'This column stores the application version corresponding to this db patch.'
/


--
-- PK_TRACK_PATCHES  (Index) 
--
--  Dependencies: 
--   VDA_TRACK_PATCHES (Table)
--
CREATE UNIQUE INDEX PK_VDA_TRACK_PATCHES ON VDA_TRACK_PATCHES
(PATCH_PK)
/


-- 
-- Non Foreign Key Constraints for Table TRACK_PATCHES 
-- 
ALTER TABLE VDA_TRACK_PATCHES ADD (
  CONSTRAINT VDA_PK_TRACK_PATCHES
 PRIMARY KEY
 (PATCH_PK)
)
/

CREATE SEQUENCE SEQ_VDA_TRACK_PATCHES
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 100
  ORDER
/  




----Include all views here-------------------------------------------------


CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CB_CORD_INFO_ALL" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "COLLECTION_DATE", "ENTRY_DATE", "CORD_CREATION_DATE", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "UNIQUE_ID_ON_BAG", "ISBT_Donation_ID", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATOR", "PK_CORD")
AS
  SELECT SITE.SITE_ID,
    CORD.CORD_REGISTRY_ID,
    CORD.CORD_LOCAL_CBU_ID,
    SPCMN.SPEC_COLLECTION_DATE,
    CORD.CREATED_ON,
    CORD.CORD_CREATION_DATE,
    CORD.REGISTRY_MATERNAL_ID,
    CORD.MATERNAL_LOCAL_ID,
    CORD.CORD_ID_NUMBER_ON_CBU_BAG,
    CORD.CORD_ISBI_DIN_CODE,
    ERES.F_GET_USER_NAME(CORD.LAST_MODIFIED_BY),
    CORD.LAST_MODIFIED_DATE,
    ERES.F_GET_USER_NAME(CORD.CREATOR),
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD,
    ERES.ER_SITE SITE,
    ERES.ER_SPECIMEN SPCMN
  WHERE CORD.FK_CBB_ID    = SITE.PK_SITE
  AND CORD.FK_SPECIMEN_ID = SPCMN.PK_SPECIMEN;
  
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."CBB_ID"
IS
  'CBB ID OF CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."CBU_LOCAL_ID"
IS
  'Local CBU ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."COLLECTION_DATE"
IS
  'Collection date of the specimen of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."ENTRY_DATE"
IS
  'created on date of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."REGISTRY_MATERNAL_ID"
IS
  'Maternal Registry ID for Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."MATERNAL_LOCAL_ID"
IS
  'Maternal Local ID for Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."UNIQUE_ID_ON_BAG"
IS
  'UNIQUE ID Number given over Bag';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."LAST_MODIFIED_BY"
IS
  'Identifies the user who last modified this row.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."LAST_MODIFIED_DATE"
IS
  'The date on which this row was last modified. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."CREATOR"
IS
  'The Creator identifies the user who created the cord. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."PK_CORD"
IS
  'Primary key of cb_cord';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."ISBT_Donation_ID" 
IS
  'ISBT CODE for the Cord';
    /
  COMMENT ON TABLE "VDA"."ETVDA_CB_CORD_INFO_ALL"
IS
  'This view provides basic information of cord';

/
  
  
CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CB_CORD_INFO_PRG" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "COLLECTION_DATE", "ENTRY_DATE", "CORD_CREATION_DATE", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "UNIQUE_ID_ON_BAG", "ISBT_Donation_ID ", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATOR", "PK_CORD")
AS
  SELECT SITE.SITE_ID,
    CORD.CORD_REGISTRY_ID,
    CORD.CORD_LOCAL_CBU_ID,
    SPCMN.SPEC_COLLECTION_DATE,
    CORD.CREATED_ON,
    CORD.CORD_CREATION_DATE,
    CORD.REGISTRY_MATERNAL_ID,
    CORD.MATERNAL_LOCAL_ID,
    CORD.CORD_ID_NUMBER_ON_CBU_BAG,
    CORD.CORD_ISBI_DIN_CODE,
    ERES.F_GET_USER_NAME(CORD.LAST_MODIFIED_BY),
    CORD.LAST_MODIFIED_DATE,
    ERES.F_GET_USER_NAME(CORD.CREATOR),
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD,
    ERES.ER_SITE SITE,
    ERES.ER_SPECIMEN SPCMN
  WHERE CORD.FK_CBB_ID     = SITE.PK_SITE
  AND CORD.FK_SPECIMEN_ID  = SPCMN.PK_SPECIMEN
  AND CORD.CORD_SEARCHABLE = '0';

/

CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CB_CORD_INFO_SUBMT" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "COLLECTION_DATE", "ENTRY_DATE", "CORD_CREATION_DATE", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "UNIQUE_ID_ON_BAG", "ISBT_Donation_ID ", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATOR", "PK_CORD")
AS
  SELECT SITE.SITE_ID,
    CORD.CORD_REGISTRY_ID,
    CORD.CORD_LOCAL_CBU_ID,
    SPCMN.SPEC_COLLECTION_DATE,
    CORD.CREATED_ON,
    CORD.CORD_CREATION_DATE,
    CORD.REGISTRY_MATERNAL_ID,
    CORD.MATERNAL_LOCAL_ID,
    CORD.CORD_ID_NUMBER_ON_CBU_BAG,
    CORD.CORD_ISBI_DIN_CODE,
    ERES.F_GET_USER_NAME(CORD.LAST_MODIFIED_BY),
    CORD.LAST_MODIFIED_DATE,
    ERES.F_GET_USER_NAME(CORD.CREATOR),
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD,
    ERES.ER_SITE SITE,
    ERES.ER_SPECIMEN SPCMN
  WHERE CORD.FK_CBB_ID     = SITE.PK_SITE
  AND CORD.FK_SPECIMEN_ID  = SPCMN.PK_SPECIMEN
  AND CORD.CORD_SEARCHABLE = '1';  
/

CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ADDITIONAL_ID_ALL" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "ADDITIONAL_ID", "ADDITIONAL_ID_TYPE", "ADDITIONAL_ID_DESCRIPTION", "PK_CORD")
AS
  SELECT site.site_id,
    cord.cord_registry_id ,
    cord.cord_local_cbu_id,
    additional.CB_ADDITIONAL_ID,
    eres.f_get_codelstDesc(additional.fk_cb_additional_id_type),
    additional.cb_additional_id_desc,
    cord.pk_cord
  FROM eres.cb_cord cord,
    eres.CB_ADDITIONAL_IDS additional,
    eres.er_site site
  WHERE cord.pk_cord = additional.entity_id
  AND cord.fk_cbb_id = site.PK_SITE;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."CBB_ID"
IS
  'CBB id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."CBU_LOCAL_ID"
IS
  'Local CBU ID of the the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."ADDITIONAL_ID"
IS
  'Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."ADDITIONAL_ID_TYPE"
IS
  'Type of Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."ADDITIONAL_ID_DESCRIPTION"
IS
  'Description for addtional id';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."PK_CORD"
IS
  'Primary of cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_ADDITIONAL_ID_ALL"
IS
  'This view provides Additional id information of cord';
  /
  
  
  
  
 CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ADDITIONAL_ID_PRG" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "ADDITIONAL_ID", "ADDITIONAL_ID_TYPE", "ADDITIONAL_ID_DESCRIPTION", "PK_CORD")
AS
  SELECT site.site_id,
    cord.cord_registry_id ,
    cord.cord_local_cbu_id,
    additional.CB_ADDITIONAL_ID,
    eres.f_get_codelstDesc(additional.fk_cb_additional_id_type),
    additional.cb_additional_id_desc,
    cord.pk_cord
  FROM eres.cb_cord cord,
    eres.CB_ADDITIONAL_IDS additional,
    eres.er_site site
  WHERE cord.pk_cord       = additional.entity_id
  AND cord.fk_cbb_id       = site.PK_SITE
  AND CORD.CORD_SEARCHABLE = '0';
  /



CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ADDITIONAL_ID_SBMT" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "ADDITIONAL_ID", "ADDITIONAL_ID_TYPE", "ADDITIONAL_ID_DESCRIPTION", "PK_CORD")
AS
  SELECT site.site_id,
    cord.cord_registry_id ,
    cord.cord_local_cbu_id,
    additional.CB_ADDITIONAL_ID,
    eres.f_get_codelstDesc(additional.fk_cb_additional_id_type),
    additional.cb_additional_id_desc,
    cord.pk_cord
  FROM eres.cb_cord cord,
    eres.CB_ADDITIONAL_IDS additional,
    eres.er_site site
  WHERE cord.pk_cord       = additional.entity_id
  AND cord.fk_cbb_id       = site.PK_SITE
  AND CORD.CORD_SEARCHABLE = '1';  
/

  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CB_CORD_STATUS_ALL" ("DEFERRAL_STATUS", "NATIONAL_STATUS", "LOCAL_STATUS", "ELIGIBILTY_STATUS", "ENTRY_DATE", "PK_CORD")
AS
  SELECT ERES.F_GET_DEFFERED_STATUS(cord.FK_CORD_CBU_STATUS),
    NVL(ERES.F_GET_CBU_STATUS(cord.CORD_NMDP_STATUS),' '),
    NVL(ERES.F_GET_CBU_STATUS(cord.FK_CORD_CBU_STATUS),' '),
    NVL(ERES.f_codelst_desc(cord.FK_CORD_CBU_ELIGIBLE_STATUS),' '),
    cord.created_on,
    cord.pk_cord
  FROM ERES.CB_CORD cord;
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_ALL"."DEFERRAL_STATUS"
IS
  'Provides deferral status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_ALL"."NATIONAL_STATUS"
IS
  'Provides national status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_ALL"."LOCAL_STATUS"
IS
  'Provides local status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_ALL"."ELIGIBILTY_STATUS"
IS
  'Provides eligibility status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_ALL"."ENTRY_DATE"
IS
  'Created on date of the Cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CB_CORD_STATUS_ALL"
IS
  'This view provides status information of cord';
  /
  
  
  
 CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CB_CORD_STATUS_PRG" ("DEFERRAL_STATUS", "NATIONAL_STATUS", "LOCAL_STATUS", "ELIGIBILTY_STATUS", "ENTRY_DATE", "PK_CORD")
AS
  SELECT ERES.F_GET_DEFFERED_STATUS(cord.FK_CORD_CBU_STATUS),
    NVL(ERES.F_GET_CBU_STATUS(cord.CORD_NMDP_STATUS),' '),
    NVL(ERES.F_GET_CBU_STATUS(cord.FK_CORD_CBU_STATUS),' '),
    NVL(ERES.f_codelst_desc(cord.FK_CORD_CBU_ELIGIBLE_STATUS),' '),
    cord.created_on,
    cord.pk_cord
  FROM ERES.CB_CORD cord
  WHERE CORD.CORD_SEARCHABLE = '0';

/



CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CB_CORD_STATUS_SUBMT" ("DEFERRAL_STATUS", "NATIONAL_STATUS", "LOCAL_STATUS", "ELIGIBILTY_STATUS", "ENTRY_DATE", "PK_CORD")
AS
  SELECT ERES.F_GET_DEFFERED_STATUS(cord.FK_CORD_CBU_STATUS),
    NVL(ERES.F_GET_CBU_STATUS(cord.CORD_NMDP_STATUS),' '),
    NVL(ERES.F_GET_CBU_STATUS(cord.FK_CORD_CBU_STATUS),' '),
    NVL(ERES.f_codelst_desc(cord.FK_CORD_CBU_ELIGIBLE_STATUS),' '),
    cord.created_on,
    cord.pk_cord
  FROM ERES.CB_CORD cord
  WHERE CORD.CORD_SEARCHABLE = '1';
/


CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBB_PRO_PROC_ALL" ("CORD_REGISTRY_ID", "PROCESSING_PROCEDURE", "ENTRY_DATE", "PROC_START_DATE", "PROC_END_DATE", "FILTER_PAPER_SAMPLE", "RBC_PELLETS", "EXTRACTED_DNA_SAMPLES", "SERUM_SAMPLES", "PLASAMA_SAMPLES", "NON-VIABLE_CELL", "NON_VIA_FINAL_PRODUCT", "NO_SEGMENTS", "ALIQUOTS_CONSISTENT_CONDTION", "ALIQUOTS_ALTERNATE_CONDTION", "NO_MAT_SERUM", "NO_MAT_PLASMA", "NO_MAT_EXT_DNA", "NO_MISECLL_MAT", "PK_CORD")
AS
  SELECT CORD.CORD_REGISTRY_ID,
    PROC.PROC_NAME,
    CORD.CREATED_ON,
    PROC.PROC_START_DATE,
    PROC.PROC_TERMI_DATE,
    ENTY.FILT_PAP_AVAIL,
    ENTY.RBC_PEL_AVAIL,
    ENTY.NO_EXT_DNA_ALI,
    ENTY.NO_SER_ALI,
    ENTY.NO_PLAS_ALI,
    ENTY.NO_NON_VIA_ALI,
    ENTY.NO_VIA_CEL_ALI,
    ENTY.NO_OF_SEG_AVAIL,
    ENTY.CBU_OT_REP_CON_FIN,
    ENTY.CBU_NO_REP_ALT_CON,
    ENTY.NO_SER_MAT_ALI,
    ENTY.NO_PLAS_MAT_ALI,
    ENTY.NO_EXT_DNA_MAT_ALI,
    ENTY.SI_NO_MISC_MAT,
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD,
    ERES.CBB_PROCESSING_PROCEDURES PROC,
    ERES.CB_ENTITY_SAMPLES ENTY
  WHERE PROC.PK_PROC = CORD.FK_CBB_PROCEDURE
  AND CORD.PK_CORD   = ENTY.ENTITY_ID;
  
 / 
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBB_PRO_PROC_PRG" ("CORD_REGISTRY_ID", "PROCESSING_PROCEDURE", "ENTRY_DATE", "PROC_START_DATE", "PROC_END_DATE", "FILTER_PAPER_SAMPLE", "RBC_PELLETS", "EXTRACTED_DNA_SAMPLES", "SERUM_SAMPLES", "PLASAMA_SAMPLES", "NON-VIABLE_CELL", "NON_VIA_FINAL_PRODUCT", "NO_SEGMENTS", "ALIQUOTS_CONSISTENT_CONDTION", "ALIQUOTS_ALTERNATE_CONDTION", "NO_MAT_SERUM", "NO_MAT_PLASMA", "NO_MAT_EXT_DNA", "NO_MISECLL_MAT", "PK_CORD")
AS
  SELECT CORD.CORD_REGISTRY_ID,
    PROC.PROC_NAME,
    CORD.CREATED_ON,
    PROC.PROC_START_DATE,
    PROC.PROC_TERMI_DATE,
    ENTY.FILT_PAP_AVAIL,
    ENTY.RBC_PEL_AVAIL,
    ENTY.NO_EXT_DNA_ALI,
    ENTY.NO_SER_ALI,
    ENTY.NO_PLAS_ALI,
    ENTY.NO_NON_VIA_ALI,
    ENTY.NO_VIA_CEL_ALI,
    ENTY.NO_OF_SEG_AVAIL,
    ENTY.CBU_OT_REP_CON_FIN,
    ENTY.CBU_NO_REP_ALT_CON,
    ENTY.NO_SER_MAT_ALI,
    ENTY.NO_PLAS_MAT_ALI,
    ENTY.NO_EXT_DNA_MAT_ALI,
    ENTY.SI_NO_MISC_MAT,
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD,
    ERES.CBB_PROCESSING_PROCEDURES PROC,
    ERES.CB_ENTITY_SAMPLES ENTY
  WHERE PROC.PK_PROC       = CORD.FK_CBB_PROCEDURE
  AND CORD.PK_CORD         = ENTY.ENTITY_ID
  AND CORD.CORD_SEARCHABLE = '0';
  
 / 
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBB_PRO_PROC_SUBMT" ("CORD_REGISTRY_ID", "PROCESSING_PROCEDURE", "ENTRY_DATE", "PROC_START_DATE", "PROC_END_DATE", "FILTER_PAPER_SAMPLE", "RBC_PELLETS", "EXTRACTED_DNA_SAMPLES", "SERUM_SAMPLES", "PLASAMA_SAMPLES", "NON-VIABLE_CELL", "NON_VIA_FINAL_PRODUCT", "NO_SEGMENTS", "ALIQUOTS_CONSISTENT_CONDTION", "ALIQUOTS_ALTERNATE_CONDTION", "NO_MAT_SERUM", "NO_MAT_PLASMA", "NO_MAT_EXT_DNA", "NO_MISECLL_MAT", "PK_CORD")
AS
  SELECT CORD.CORD_REGISTRY_ID,
    PROC.PROC_NAME,
    CORD.CREATED_ON,
    PROC.PROC_START_DATE,
    PROC.PROC_TERMI_DATE,
    ENTY.FILT_PAP_AVAIL,
    ENTY.RBC_PEL_AVAIL,
    ENTY.NO_EXT_DNA_ALI,
    ENTY.NO_SER_ALI,
    ENTY.NO_PLAS_ALI,
    ENTY.NO_NON_VIA_ALI,
    ENTY.NO_VIA_CEL_ALI,
    ENTY.NO_OF_SEG_AVAIL,
    ENTY.CBU_OT_REP_CON_FIN,
    ENTY.CBU_NO_REP_ALT_CON,
    ENTY.NO_SER_MAT_ALI,
    ENTY.NO_PLAS_MAT_ALI,
    ENTY.NO_EXT_DNA_MAT_ALI,
    ENTY.SI_NO_MISC_MAT,
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD,
    ERES.CBB_PROCESSING_PROCEDURES PROC,
    ERES.CB_ENTITY_SAMPLES ENTY
  WHERE PROC.PK_PROC       = CORD.FK_CBB_PROCEDURE
  AND CORD.PK_CORD         = ENTY.ENTITY_ID
  AND CORD.CORD_SEARCHABLE = '1';
  
 / 
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_CRI_VIEW_ALL" ("CRI", "CBU_STATUS", "CRI_CREATION_DATE", "PK_CORD")
AS
  SELECT DECODE(REQINFO.COMPLETE_REQ_INFO_FLAG,'1','YES','NO'),
    CBUSTATUS.CBU_STATUS_DESC,
    REQINFO.CREATED_ON,
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD ,
    ERES.CB_CORD_COMPLETE_REQ_INFO REQINFO,
    ERES.CB_CBU_STATUS CBUSTATUS
  WHERE CORD.FK_CORD_CBU_STATUS = CBUSTATUS.PK_CBU_STATUS
  AND REQINFO.FK_CORD_ID        = CORD.PK_CORD;
  
/  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_CRI_VIEW_PRG" ("CRI", "CBU_STATUS", "CRI_CREATION_DATE", "PK_CORD")
AS
  SELECT DECODE(REQINFO.COMPLETE_REQ_INFO_FLAG,'1','YES','NO'),
    CBUSTATUS.CBU_STATUS_DESC,
    REQINFO.CREATED_ON,
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD ,
    ERES.CB_CORD_COMPLETE_REQ_INFO REQINFO,
    ERES.CB_CBU_STATUS CBUSTATUS
  WHERE CORD.FK_CORD_CBU_STATUS = CBUSTATUS.PK_CBU_STATUS
  AND REQINFO.FK_CORD_ID        = CORD.PK_CORD
  AND CORD.CORD_SEARCHABLE      = '0';
  
 / 
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT" ("CRI", "CBU_STATUS", "CRI_CREATION_DATE", "PK_CORD")
AS
  SELECT DECODE(REQINFO.COMPLETE_REQ_INFO_FLAG,'1','YES','NO'),
    CBUSTATUS.CBU_STATUS_DESC,
    REQINFO.CREATED_ON,
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD ,
    ERES.CB_CORD_COMPLETE_REQ_INFO REQINFO,
    ERES.CB_CBU_STATUS CBUSTATUS
  WHERE CORD.FK_CORD_CBU_STATUS = CBUSTATUS.PK_CBU_STATUS
  AND REQINFO.FK_CORD_ID        = CORD.PK_CORD
  AND CORD.CORD_SEARCHABLE      = '1';
  
 / 
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_HLA_INFO_ALL" ("CBB_ID", "CBU_REGISTRY_ID", "NO_OF_SEGMENTS", "HLA_VALUE_TYPE1", "HLA_VALUE_TYPE2", "HLA_DESC", "RECEIVED_DATE", "PK_CORD", "PK_HLA", "PK_ENTITY_SAMPLES")
AS
  SELECT SITE.SITE_ID,
    C.CORD_REGISTRY_ID,
    SMPLE.NO_OF_SEG_AVAIL,
    EC1.GENOMIC_FORMAT AS HLA_TYPE1,
    EC2.GENOMIC_FORMAT AS HLA_TYPE2,
    ERES.F_CODELST_DESC(H.FK_HLA_CODE_ID),
    H.HLA_RECEIVED_DATE,
    C.PK_CORD,
    H.PK_HLA,
    SMPLE.PK_ENTITY_SAMPLES
  FROM ERES.CB_HLA H,
    ERES.CB_CORD C,
    ERES.ER_SITE SITE,
    ERES.CB_ANTIGEN_ENCOD EC1,
    ERES.CB_ANTIGEN_ENCOD EC2,
    ERES.CB_ENTITY_SAMPLES SMPLE
  WHERE H.FK_HLA_ANTIGENEID1=EC1.FK_ANTIGEN (+)
  AND ( EC1.VERSION         =
    (SELECT MAX(EC1S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC1S
    WHERE EC1.FK_ANTIGEN=EC1S.FK_ANTIGEN
    )
  OR EC1.VERSION          IS NULL)
  AND H.FK_HLA_ANTIGENEID2 = EC2.FK_ANTIGEN (+)
  AND (EC2.VERSION         =
    (SELECT MAX(EC2S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC2S
    WHERE EC2.FK_ANTIGEN=EC2S.FK_ANTIGEN
    )
  OR EC2.VERSION    IS NULL)
  AND H.ENTITY_ID    =C.PK_CORD
  AND SMPLE.ENTITY_ID=C.PK_CORD
  AND C.FK_CBB_ID    = SITE.PK_SITE;
  /
  
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_HLA_INFO_PRG" ("CBB_ID", "CBU_REGISTRY_ID", "NO_OF_SEGMENTS", "HLA_VALUE_TYPE1", "HLA_VALUE_TYPE2", "HLA_DESC", "RECEIVED_DATE", "PK_CORD", "PK_HLA", "PK_ENTITY_SAMPLES")
AS
  SELECT SITE.SITE_ID,
    C.CORD_REGISTRY_ID,
    SMPLE.NO_OF_SEG_AVAIL,
    EC1.GENOMIC_FORMAT AS HLA_TYPE1,
    EC2.GENOMIC_FORMAT AS HLA_TYPE2,
    ERES.F_CODELST_DESC(H.FK_HLA_CODE_ID),
    H.HLA_RECEIVED_DATE,
    C.PK_CORD,
    H.PK_HLA,
    SMPLE.PK_ENTITY_SAMPLES
  FROM ERES.CB_HLA H,
    ERES.CB_CORD C,
    ERES.ER_SITE SITE,
    ERES.CB_ANTIGEN_ENCOD EC1,
    ERES.CB_ANTIGEN_ENCOD EC2,
    ERES.CB_ENTITY_SAMPLES SMPLE
  WHERE H.FK_HLA_ANTIGENEID1=EC1.FK_ANTIGEN (+)
  AND ( EC1.VERSION         =
    (SELECT MAX(EC1S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC1S
    WHERE EC1.FK_ANTIGEN=EC1S.FK_ANTIGEN
    )
  OR EC1.VERSION          IS NULL)
  AND H.FK_HLA_ANTIGENEID2 = EC2.FK_ANTIGEN (+)
  AND (EC2.VERSION         =
    (SELECT MAX(EC2S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC2S
    WHERE EC2.FK_ANTIGEN=EC2S.FK_ANTIGEN
    )
  OR EC2.VERSION       IS NULL)
  AND H.ENTITY_ID       =C.PK_CORD
  AND SMPLE.ENTITY_ID   =C.PK_CORD
  AND C.FK_CBB_ID       = SITE.PK_SITE
  AND C.CORD_SEARCHABLE = '0';
  
 / 
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_HLA_INFO_SUBMT" ("CBB_ID", "CBU_REGISTRY_ID", "NO_OF_SEGMENTS", "HLA_VALUE_TYPE1", "HLA_VALUE_TYPE2", "HLA_DESC", "RECEIVED_DATE", "PK_CORD", "PK_HLA", "PK_ENTITY_SAMPLES")
AS
  SELECT SITE.SITE_ID,
    C.CORD_REGISTRY_ID,
    SMPLE.NO_OF_SEG_AVAIL,
    EC1.GENOMIC_FORMAT AS HLA_TYPE1,
    EC2.GENOMIC_FORMAT AS HLA_TYPE2,
    ERES.F_CODELST_DESC(H.FK_HLA_CODE_ID),
    H.HLA_RECEIVED_DATE,
    C.PK_CORD,
    H.PK_HLA,
    SMPLE.PK_ENTITY_SAMPLES
  FROM ERES.CB_HLA H,
    ERES.CB_CORD C,
    ERES.ER_SITE SITE,
    ERES.CB_ANTIGEN_ENCOD EC1,
    ERES.CB_ANTIGEN_ENCOD EC2,
    ERES.CB_ENTITY_SAMPLES SMPLE
  WHERE H.FK_HLA_ANTIGENEID1=EC1.FK_ANTIGEN (+)
  AND ( EC1.VERSION         =
    (SELECT MAX(EC1S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC1S
    WHERE EC1.FK_ANTIGEN=EC1S.FK_ANTIGEN
    )
  OR EC1.VERSION          IS NULL)
  AND H.FK_HLA_ANTIGENEID2 = EC2.FK_ANTIGEN (+)
  AND (EC2.VERSION         =
    (SELECT MAX(EC2S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC2S
    WHERE EC2.FK_ANTIGEN=EC2S.FK_ANTIGEN
    )
  OR EC2.VERSION       IS NULL)
  AND H.ENTITY_ID       =C.PK_CORD
  AND SMPLE.ENTITY_ID   =C.PK_CORD
  AND C.FK_CBB_ID       = SITE.PK_SITE
  AND C.CORD_SEARCHABLE = '1';
  
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CORD_IMPORTED_ALL" ("CBU_REGISTRY_ID", "CBU_LOCAL_CBU_ID", "MATERNAL_REGISTRY_ID", "MATERNAL_LOCAL_ID", "CORD_CREATION_DATE", "BABY GENDER", "CBB_ID", "Created_On", "Local Status", "LICENSURE STATUS", "UNLICENSED REASON", "ELIGIBILITY DETERMINATION", "INELIGIBLE/INCOMPLETE REASON", "UNIQUE_ID_ON_BAG", "PK_CORD")
AS
  SELECT DISTINCT cord.cord_registry_id,
    cord.cord_local_cbu_id,
    cord.REGISTRY_MATERNAL_ID,
    cord.MATERNAL_LOCAL_ID,
    CORD.CORD_CREATION_DATE,
    eres.f_codelst_desc(cord.FK_CORD_BABY_GENDER_ID),
    site.site_id ,
    cord.created_on,
    eres.F_GET_CBU_STATUS(cord.FK_CORD_CBU_STATUS) AS local_status,
    eres.f_codelst_desc(cord.FK_CORD_CBU_LIC_STATUS),
    eres.F_GET_MULTIPLE_REASONS(cord.FK_CORD_CBU_UNLICENSED_REASON),
    eres.f_codelst_desc(cord.FK_CORD_CBU_ELIGIBLE_STATUS),
    eres.F_GET_MULTIPLE_REASONS(cord.FK_CORD_CBU_INELIGIBLE_REASON),
    cord.CORD_ID_NUMBER_ON_CBU_BAG,
    cord.pk_cord
  FROM eres.cb_cord cord ,
    eres.ER_SITE site,
    eres.cb_cord_import_messages cordImport
  WHERE cord.pk_cord = cordImport.fk_cord_id
  AND cord.FK_CBB_ID = site.pk_site;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."CBU_REGISTRY_ID"
IS
  'This column stores Registry ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."CBU_LOCAL_CBU_ID"
IS
  'This column stores Local ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."MATERNAL_REGISTRY_ID"
IS
  'This column stores Maternal Registry ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."CORD_CREATION_DATE"
IS
  'This column stores Maternal Registry ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."CBB_ID"
IS
  'This column stores an Identification number for the site';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."UNIQUE_ID_ON_BAG"
IS
  'This column stores eligibility status of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."PK_CORD"
IS
  'This column stores the Collection Date of the cord';
  /
  
  
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CORD_IMPORTED_PRG" ("CBU_REGISTRY_ID", "CBU_LOCAL_CBU_ID", "MATERNAL REGISTRY ID", "MATERNAL LOCAL ID", "CORD_CREATION_DATE", "BABY GENDER", "CBB_ID", "Created On", "Local Status", "LICENSURE STATUS", "UNLICENSED REASON", "ELIGIBILITY DETERMINATION", "INELIGIBLE/INCOMPLETE REASON", "UNIQUE PRODUCT IDENTITY ON BAG", "PK_CORD")
AS
  SELECT DISTINCT cord.cord_registry_id,
    cord.cord_local_cbu_id,
    cord.REGISTRY_MATERNAL_ID,
    cord.MATERNAL_LOCAL_ID,
    CORD.CORD_CREATION_DATE,
    eres.f_codelst_desc(cord.FK_CORD_BABY_GENDER_ID),
    site.site_id ,
    cord.created_on,
    eres.F_GET_CBU_STATUS(cord.FK_CORD_CBU_STATUS) AS local_status,
    eres.f_codelst_desc(cord.FK_CORD_CBU_LIC_STATUS),
    eres.F_GET_MULTIPLE_REASONS(cord.FK_CORD_CBU_UNLICENSED_REASON),
    eres.f_codelst_desc(cord.FK_CORD_CBU_ELIGIBLE_STATUS),
    eres.F_GET_MULTIPLE_REASONS(cord.FK_CORD_CBU_INELIGIBLE_REASON),
    cord.CORD_ID_NUMBER_ON_CBU_BAG,
    cord.pk_cord
  FROM eres.cb_cord cord ,
    eres.ER_SITE site,
    eres.cb_cord_import_messages cordImport
  WHERE cord.pk_cord       = cordImport.fk_cord_id
  AND cord.FK_CBB_ID       = site.pk_site
  AND cord.CORD_SEARCHABLE = '0';
  /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CORD_IMPORTED_SUBMT" ("CBU_REGISTRY_ID", "CBU_LOCAL_CBU_ID", "MATERNAL REGISTRY ID", "MATERNAL LOCAL ID", "CORD_CREATION_DATE", "BABY GENDER", "CBB_ID", "Created On", "Local Status", "LICENSURE STATUS", "UNLICENSED REASON", "ELIGIBILITY DETERMINATION", "INELIGIBLE/INCOMPLETE REASON", "UNIQUE PRODUCT IDENTITY ON BAG", "PK_CORD")
AS
  SELECT DISTINCT cord.cord_registry_id,
    cord.cord_local_cbu_id,
    cord.REGISTRY_MATERNAL_ID,
    cord.MATERNAL_LOCAL_ID,
    CORD.CORD_CREATION_DATE,
    eres.f_codelst_desc(cord.FK_CORD_BABY_GENDER_ID),
    site.site_id ,
    cord.created_on,
    eres.F_GET_CBU_STATUS(cord.FK_CORD_CBU_STATUS) AS local_status,
    eres.f_codelst_desc(cord.FK_CORD_CBU_LIC_STATUS),
    eres.F_GET_MULTIPLE_REASONS(cord.FK_CORD_CBU_UNLICENSED_REASON),
    eres.f_codelst_desc(cord.FK_CORD_CBU_ELIGIBLE_STATUS),
    eres.F_GET_MULTIPLE_REASONS(cord.FK_CORD_CBU_INELIGIBLE_REASON),
    cord.CORD_ID_NUMBER_ON_CBU_BAG,
    cord.pk_cord
  FROM eres.cb_cord cord ,
    eres.ER_SITE site,
    eres.cb_cord_import_messages cordImport
  WHERE cord.pk_cord       = cordImport.fk_cord_id
  AND cord.FK_CBB_ID       = site.pk_site
  AND cord.CORD_SEARCHABLE = '1';
  /
  
  
  
  
CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CORD_SYPHILLIS_STS_ALL" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "RESPONSE_CODE", "LOCAL_STATUS", "PK_CORD")
AS
  SELECT SITE.SITE_ID,
    C.CORD_REGISTRY_ID,
    C.CORD_LOCAL_CBU_ID,
    eres.f_codelst_desc(formresponse.response_code),
    eres.F_GET_CBU_STATUS(C.FK_CORD_CBU_STATUS) AS local_status,
    C.PK_CORD
  FROM eres.cb_form_responses formresponse,
    eres.cb_cord c,
    eres.er_site site
  WHERE formresponse.pk_form_responses=
    (SELECT MAX(pk_form_responses)
    FROM eres.cb_form_responses
    WHERE entity_id=c.pk_cord
    AND fk_question=
      (SELECT pk_questions
      FROM eres.cb_questions
      WHERE ques_code='syphilis_react_ind'
      )
    )
  AND c.FK_CBB_ID = site.pk_site;
  
 / 
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CORD_SYPHILLIS_STS_PRG" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "RESPONSE_CODE", "LOCAL_STATUS", "PK_CORD")
AS
  SELECT SITE.SITE_ID,
    C.CORD_REGISTRY_ID,
    C.CORD_LOCAL_CBU_ID,
    eres.f_codelst_desc(formresponse.response_code),
    eres.F_GET_CBU_STATUS(C.FK_CORD_CBU_STATUS) AS local_status,
    C.PK_CORD
  FROM eres.cb_form_responses formresponse,
    eres.cb_cord c,
    eres.er_site site
  WHERE formresponse.pk_form_responses=
    (SELECT MAX(pk_form_responses)
    FROM eres.cb_form_responses
    WHERE entity_id=c.pk_cord
    AND fk_question=
      (SELECT pk_questions
      FROM eres.cb_questions
      WHERE ques_code='syphilis_react_ind'
      )
    )
  AND c.FK_CBB_ID       = site.pk_site
  AND C.CORD_SEARCHABLE = '0';
  
 / 
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CORD_SYPHILLIS_STS_SUBMT" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "RESPONSE_CODE", "LOCAL_STATUS", "PK_CORD")
AS
  SELECT SITE.SITE_ID,
    C.CORD_REGISTRY_ID,
    C.CORD_LOCAL_CBU_ID,
    eres.f_codelst_desc(formresponse.response_code),
    eres.F_GET_CBU_STATUS(C.FK_CORD_CBU_STATUS) AS local_status,
    C.PK_CORD
  FROM eres.cb_form_responses formresponse,
    eres.cb_cord c,
    eres.er_site site
  WHERE formresponse.pk_form_responses=
    (SELECT MAX(pk_form_responses)
    FROM eres.cb_form_responses
    WHERE entity_id=c.pk_cord
    AND fk_question=
      (SELECT pk_questions
      FROM eres.cb_questions
      WHERE ques_code='syphilis_react_ind'
      )
    )
  AND c.FK_CBB_ID       = site.pk_site
  AND C.CORD_SEARCHABLE = '1';
  
  /
  
  

  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_PATLAB_INFO_ALL" ("CBU_REGISTRY_ID", "TEST_RESULT", "TIMING_OF_TEST", "POST_THAW_ORDER", "TEST_REASON", "SAMPLE_TYPE", "TEST_METHOD", "PK_CORD", "LABTEST_NAME")
AS
  SELECT CORD.CORD_REGISTRY_ID,
    PAT.TEST_RESULT,
    NVL(ERES.F_CODELST_DESC(PAT.FK_TIMING_OF_TEST),'POST-CRYOPRESERVATION/THAW'),
    ERES.F_GET_POST_THAW(PAT.CUSTOM006),
    ERES.F_CODELST_DESC(PAT.FK_TEST_REASON),
    ERES.F_CODELST_DESC(PAT.FK_TEST_SPECIMEN),
    ERES.F_CODELST_DESC(PAT.FK_TEST_METHOD),
    CORD.PK_CORD,
    ERES.F_LABTESTNAME(FK_TESTID)
  FROM ERES.ER_PATLABS PAT,
    ERES.CB_CORD CORD
  WHERE PAT.FK_SPECIMEN IN
    (SELECT CORD.FK_SPECIMEN_ID FROM ERES.CB_CORD
    )
  AND PAT.TEST_RESULT IS NOT NULL;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."TEST_RESULT"
IS
  'Test result for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."TIMING_OF_TEST"
IS
  'TIMING OF TEST';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."POST_THAW_ORDER"
IS
  'Order of POST-CRYOPRESERVATION/THAW';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."TEST_REASON"
IS
  'Reason for Test ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."SAMPLE_TYPE"
IS
  'Sample Type ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."TEST_METHOD"
IS
  'TEST METHOD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."PK_CORD"
IS
  'Primary key of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."LABTEST_NAME"
IS
  'Discription of test id';
  /
  COMMENT ON TABLE "VDA"."ETVDA_PATLAB_INFO_ALL"
IS
  'This view provides Patlab information of cord';
 /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_PATLAB_INFO_PRG" ("CBU_REGISTRY_ID", "TEST_RESULT", "TIMING_OF_TEST", "POST_THAW_ORDER", "TEST_REASON", "SAMPLE_TYPE", "TEST_METHOD", "PK_CORD", "LABTEST_NAME")
AS
  SELECT CORD.CORD_REGISTRY_ID,
    PAT.TEST_RESULT,
    NVL(ERES.F_CODELST_DESC(PAT.FK_TIMING_OF_TEST),'POST-CRYOPRESERVATION/THAW'),
    ERES.F_GET_POST_THAW(PAT.CUSTOM006),
    ERES.F_CODELST_DESC(PAT.FK_TEST_REASON),
    ERES.F_CODELST_DESC(PAT.FK_TEST_SPECIMEN),
    ERES.F_CODELST_DESC(PAT.FK_TEST_METHOD),
    CORD.PK_CORD,
    ERES.F_LABTESTNAME(FK_TESTID)
  FROM ERES.ER_PATLABS PAT,
    ERES.CB_CORD CORD
  WHERE PAT.FK_SPECIMEN IN
    (SELECT CORD.FK_SPECIMEN_ID FROM ERES.CB_CORD
    )
  AND PAT.TEST_RESULT     IS NOT NULL
  AND CORD.CORD_SEARCHABLE = '0';
  
 / 
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_PATLAB_INFO_SUBMT" ("CBU_REGISTRY_ID", "TEST_RESULT", "TIMING_OF_TEST", "POST_THAW_ORDER", "TEST_REASON", "SAMPLE_TYPE", "TEST_METHOD", "PK_CORD", "LABTEST_NAME")
AS
  SELECT CORD.CORD_REGISTRY_ID,
    PAT.TEST_RESULT,
    NVL(ERES.F_CODELST_DESC(PAT.FK_TIMING_OF_TEST),'POST-CRYOPRESERVATION/THAW'),
    ERES.F_GET_POST_THAW(PAT.CUSTOM006),
    ERES.F_CODELST_DESC(PAT.FK_TEST_REASON),
    ERES.F_CODELST_DESC(PAT.FK_TEST_SPECIMEN),
    ERES.F_CODELST_DESC(PAT.FK_TEST_METHOD),
    CORD.PK_CORD,
    ERES.F_LABTESTNAME(FK_TESTID)
  FROM ERES.ER_PATLABS PAT,
    ERES.CB_CORD CORD
  WHERE PAT.FK_SPECIMEN IN
    (SELECT CORD.FK_SPECIMEN_ID FROM ERES.CB_CORD
    )
  AND PAT.TEST_RESULT     IS NOT NULL
  AND CORD.CORD_SEARCHABLE = '1';

/



CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBUINFORMATION_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp ,
    arm.module_id pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id  = arm.pk_row_id
  AND (arm.table_name  = 'CB_CORD')
  AND acm.column_name IN ('FK_CORD_CBU_STATUS','FK_CODELST_ETHNICITY','MULTIPLE_BIRTH','CB_ADDITIONAL_ID','FK_CORD_BABY_GENDER_ID', 'CORD_BABY_BIRTH_DATE','FK_CBU_STOR_LOC','FK_CBU_COLL_SITE','FUNDED_CBU','FK_FUND_CATEG','FK_CBU_COLL_TYPE','FK_CBU_DEL_TYPE','NMDP_RACE_ID','FK_CBB_ID','CORD_NMDP_STATUS')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_multiple_values
    WHERE pk_multiple_values = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_multiple_values
    FROM eres.cb_multiple_values
    WHERE fk_codelst_type = 'race'
    )
  AND arm.table_name   = 'CB_MULTIPLE_VALUES')
  AND acm.column_name IN ('FK_CODELST')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT pk_cord
    FROM eres.cb_cord
    WHERE fk_specimen_id = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT FK_SPECIMEN_ID FROM eres.CB_CORD
    )
  AND arm.table_name   = 'ER_SPECIMEN')
  AND acm.column_name IN ('SPEC_COLLECTION_DATE')
  ORDER BY 9 DESC;
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBUINFORMATION_AUDIT"
IS
  'This view provides CBU Information audit details ';
  /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CLINICALNOTES_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    (SELECT entity_id
    FROM eres.cb_notes
    WHERE pk_notes    = arm.module_id
    AND fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'clinic'
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_notes
    FROM eres.cb_notes
    WHERE fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'clinic'
      )
    )
  AND arm.table_name   = 'CB_NOTES')
  AND acm.column_name IN ('NOTES_CLINIC','VISIBILITY','KEYWORD','FLAG_FOR_LATER','FK_NOTES_TYPE','FK_NOTES_CATEGORY','COMMENTS','NOTE_ASSESSMENT','AMENDED','NOTE_SEQ')
  ORDER BY arm.timestamp DESC;
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CLINICALNOTES_AUDIT"
IS
  'This view provides Clinical Notes audit details ';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_DOCUMENT_AUDIT" ("CATEGORY", "SUBCATEGORY", "USER_ID", "DATE_TIME", "ACTION", "DOCLINK", "MODULE_ID", "PK_CORD")
AS
  SELECT eres.f_codelst_desc(cu.fk_category) category,
    eres.f_codelst_desc(cu.fk_subcategory) subcategory,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Revoke','') action,
    (SELECT dcms_file_attachment_id
    FROM eres.er_attachments
    WHERE pk_attachment = arm.module_id
    ) doclink ,
    arm.module_id,
    (SELECT entity_id
    FROM eres.er_attachments
    WHERE pk_attachment = arm.module_id
    ) pk_cord
  FROM eres.audit_row_module arm,
    eres.cb_upload_info cu
  WHERE arm.module_id IN
    (SELECT pk_attachment FROM eres.er_attachments
    )
  AND arm.table_name = 'ER_ATTACHMENTS'
  AND arm.module_id  = cu.fk_attachmentid
  ORDER BY arm.timestamp DESC;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT"."CATEGORY"
IS
  'Module category in which document uploaded';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT"."SUBCATEGORY"
IS
  'Module sub category in which document uploaded';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT"."ACTION"
IS
  'Action performed on the application table (Add, Revoke)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT"."DOCLINK"
IS
  'Provides Document link when document is revoked';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT"."MODULE_ID"
IS
  'Provides Module Id';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_DOCUMENT_AUDIT"
IS
  'This view provides Document audit details ';
  /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ELIGIBILITY_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp ,
    (SELECT entity_id
    FROM eres.cb_entity_status
    WHERE pk_entity_status = arm.module_id
    AND status_type_code   = 'eligibility'
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_entity_status
    FROM eres.cb_entity_status
    WHERE status_type_code = 'eligibility'
    )
  AND arm.table_name       = 'CB_ENTITY_STATUS')
  AND acm.column_name NOT IN ('RID','IP_ADD','CREATED_ON','CREATOR','PK_ENTITY_STATUS','ENTITY_ID','IS_CORD_ENTRY_DATA','ORDER_ID','ORDER_FK_RESOL','STATUS_DATE','FK_ENTITY_TYPE','STATUS_TYPE_CODE')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_entity_status
    WHERE status_type_code='eligibility'
    AND pk_entity_status IN
      (SELECT fk_entity_status
      FROM eres.cb_entity_status_reason
      WHERE pk_entity_status_reason = arm.module_id
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_entity_status_reason
    FROM eres.cb_entity_status_reason
    WHERE fk_entity_status IN
      (SELECT pk_entity_status
      FROM eres.cb_entity_status
      WHERE status_type_code= 'eligibility'
      )
    AND arm.table_name = 'CB_ENTITY_STATUS_REASON'
    ))
  AND acm.column_name NOT IN ('RID','IP_ADD','CREATED_ON','CREATOR','PK_ENTITY_STATUS_REASON','DELETEDFLAG','FK_ATTACHMENTID','FK_FORMID','NOT_APP_PRIOR_FLAG','FK_ENTITY_STATUS')
  ORDER BY 9 DESC;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_ELIGIBILITY_AUDIT"
IS
  'This view provides Eligibility audit details ';
  /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_FINALREVIEW_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    (SELECT fk_cord_id
    FROM eres.cb_cord_final_review
    WHERE pk_cord_final_review = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_cord_final_review FROM eres.cb_cord_final_review
    )
  AND arm.table_name   = 'CB_CORD_FINAL_REVIEW')
  AND acm.column_name IN ('LICENSURE_CONFIRM_FLAG','ELIGIBLE_CONFIRM_FLAG','REVIEW_CORD_ACCEPTABLE_FLAG','CONFIRM_CBB_SIGN_FLAG')
  ORDER BY arm.timestamp DESC;
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_FINALREVIEW_AUDIT"
IS
  'This view provides Final Review audit details ';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_FMHQ_MRQ_AUDIT" ("PK_FORM_VERSION", "FORMNAME", "FVERSION", "USER_ID", "DATE_TIME", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "PK_CORD")
AS
  SELECT pk_form_version,
    (SELECT forms_desc
    FROM eres.cb_forms
    WHERE pk_form   = ver.fk_form
    AND forms_desc IN ('FMHQ','MRQ')
    ) formname,
    (SELECT version
    FROM eres.cb_forms
    WHERE pk_form   = ver.fk_form
    AND forms_desc IN ('FMHQ','MRQ')
    ) fversion,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = ver.creator
    ) user_id,
    TO_CHAR(created_on,'Mon DD, YYYY HH:MI:SS AM') date_time,
    last_modified_by,
    last_modified_date,
    entity_id pk_cord
  FROM eres.cb_form_version ver
  WHERE fk_form IN
    (SELECT pk_form
    FROM eres.cb_forms
    WHERE pk_form   = ver.fk_form
    AND forms_desc IN ('FMHQ','MRQ')
    )
  ORDER BY pk_form_version DESC;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT"."PK_FORM_VERSION"
IS
  'Primary key of cb_form_version table';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT"."FORMNAME"
IS
  'Stores form Description like MRQ/FMHQ/IDM etc';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT"."FVERSION"
IS
  'Stores form version';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT"."LAST_MODIFIED_BY"
IS
  'Stores user who Last modified the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT"."LAST_MODIFIED_DATE"
IS
  'Stores last modified date';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_FMHQ_MRQ_AUDIT"
IS
  'This view provides FMHQ and MRQ audit details ';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_HLA_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    (SELECT entity_id
    FROM eres.cb_hla
    WHERE pk_hla = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_hla FROM eres.cb_hla
    )
  AND arm.table_name       = 'CB_HLA')
  AND acm.column_name NOT IN ('RID','IP_ADD','CREATED_ON','CREATOR','PK_HLA','ENTITY_ID','DELETEDFLAG','HLA_RECEIVED_DATE','FK_HLA_ANTIGENEID1','FK_HLA_ANTIGENEID2','FK_HLA_OVERCLS_CODE','HLA_CRIT_OVER_DATE','ENTITY_TYPE','LAST_MODIFIED_BY','LAST_MODIFIED_DATE','HLA_OVER_IND_ANTGN1','HLA_OVER_IND_ANTGN2','FK_HLA_PRIM_DNA_ID','FK_HLA_PROT_DIPLOID','HLA_TYPING_DATE','FK_HLA_REPORTING_LAB','FK_HLA_REPORT_METHOD','FK_ORDER_ID','FK_CORD_CT_STATUS','FK_SPEC_TYPE','FK_CORD_CDR_CBU_ID','HLA_TYPING_SEQ','CB_HLA_ORDER_SEQ','HLA_REPORT_METHOD_CODE')
  ORDER BY arm.timestamp DESC;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_HLA_AUDIT"
IS
  'This view provides HLA audit details ';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ID_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "TSTAMP", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_COLUMN_ID", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    acm.pk_column_id,
    (arm.module_id) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id  = arm.pk_row_id
  AND (arm.table_name  = 'CB_CORD')
  AND acm.column_name IN ('CORD_REGISTRY_ID','CORD_LOCAL_CBU_ID','CORD_ID_NUMBER_ON_CBU_BAG','CORD_ISBI_DIN_CODE','MATERNAL_LOCAL_ID','REGISTRY_MATERNAL_ID','CORD_HISTORIC_CBU_ID','CB_IDS_CHANGE_REASON')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    acm.pk_column_id,
    (SELECT entity_id
    FROM eres.CB_ADDITIONAL_IDS
    WHERE PK_CB_ADDITIONAL_IDS = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT PK_CB_ADDITIONAL_IDS FROM eres.CB_ADDITIONAL_IDS
    )
  AND arm.table_name   = 'CB_ADDITIONAL_IDS')
  AND acm.column_name IN ('CB_ADDITIONAL_ID_DESC','FK_CB_ADDITIONAL_ID_TYPE','CB_ADDITIONAL_ID')
  ORDER BY 9 DESC,
    10 DESC;
	
	/
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT"."TSTAMP"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT"."PK_COLUMN_ID"
IS
  'Primary key of Audit Column table';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_IDM_AUDIT" ("PK_FORM_VERSION", "FORMNAME", "FVERSION", "USER_ID", "DATE_TIME", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "PK_CORD")
AS
  SELECT pk_form_version,
    (SELECT forms_desc
    FROM eres.cb_forms
    WHERE pk_form  = ver.fk_form
    AND forms_desc ='IDM'
    ) formname,
    (SELECT version
    FROM eres.cb_forms
    WHERE pk_form  = ver.fk_form
    AND forms_desc ='IDM'
    ) fversion,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = ver.creator
    ) user_id,
    TO_CHAR(created_on,'Mon DD, YYYY HH:MI:SS AM') date_time,
    last_modified_by,
    last_modified_date,
    entity_id pk_cord
  FROM eres.cb_form_version ver
  WHERE fk_form IN
    (SELECT pk_form
    FROM eres.cb_forms
    WHERE pk_form  = ver.fk_form
    AND forms_desc = 'IDM'
    )
  ORDER BY pk_form_version DESC;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT"."PK_FORM_VERSION"
IS
  'Primary key of cb_form_version table';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT"."FORMNAME"
IS
  'Stores form Description like MRQ/FMHQ/IDM etc';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT"."FVERSION"
IS
  'Stores form version';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT"."LAST_MODIFIED_BY"
IS
  'Stores user who Last modified the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT"."LAST_MODIFIED_DATE"
IS
  'Stores last modified date';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_IDM_AUDIT"
IS
  'This view provides IDM audit details ';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_LABSUMMARY_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_COLUMN_ID", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    acm.pk_column_id,
    (SELECT pk_cord
    FROM eres.cb_cord
    WHERE fk_specimen_id =
      (SELECT fk_specimen FROM eres.er_patlabs WHERE pk_patlabs = arm.module_id
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_patlabs
    FROM eres.er_patlabs
    WHERE fk_specimen IN
      (SELECT fk_specimen_id FROM eres.cb_cord
      )
    AND arm.table_name = 'ER_PATLABS'
    ))
  AND acm.column_name NOT IN ('RID','IP_ADD','CREATED_ON','CREATOR','PK_PATLABS','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','FK_SPECIMEN','CUSTOM006','FK_PATPROT')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    acm.pk_column_id,
    arm.module_id pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id  = arm.pk_row_id
  AND (arm.table_name  = 'CB_CORD')
  AND acm.column_name IN ('PRCSNG_START_DATE','FRZ_DATE','BACT_CULT_DATE','FK_CORD_BACT_CUL_RESULT','FK_CORD_FUNGAL_CUL_RESULT','FUNG_CULT_DATE','FK_CORD_ABO_BLOOD_TYPE','FK_CORD_RH_TYPE','HEMOGLOBIN_SCRN','CB_RHTYPE_OTHER_SPEC','BACT_COMMENT','FUNG_COMMENT')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    acm.pk_column_id,
    (SELECT entity_id
    FROM eres.cb_assessment
    WHERE pk_assessment = arm.module_id
    AND entity_type    IN (eres.f_codelst_id('entity_type','CBU'))
    AND sub_entity_id  IN (eres.f_codelst_id('fung_cul','not_done'),eres.f_codelst_id('hem_path_scrn','alp_thal_sil'),eres.f_codelst_id('hem_path_scrn','not_done'), eres.f_codelst_id('hem_path_scrn','hemo_trait'),eres.f_codelst_id('hem_path_scrn','homo_no_dis'),eres.f_codelst_id('hem_path_scrn','alp_thal_trait'),eres.f_codelst_id('hem_path_scrn','unknown'))
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_assessment
    FROM eres.cb_assessment
    WHERE entity_type IN (eres.f_codelst_id('entity_type','CBU'))
    AND sub_entity_id IN (eres.f_codelst_id('fung_cul','not_done'),eres.f_codelst_id('hem_path_scrn','alp_thal_sil'),eres.f_codelst_id('hem_path_scrn','not_done'), eres.f_codelst_id('hem_path_scrn','hemo_trait'),eres.f_codelst_id('hem_path_scrn','homo_no_dis'),eres.f_codelst_id('hem_path_scrn','alp_thal_trait'),eres.f_codelst_id('hem_path_scrn','unknown'))
    )
  AND arm.table_name   = 'CB_ASSESSMENT')
  AND acm.column_name IN ('ASSESSMENT_REMARKS','ASSESSMENT_FOR_RESPONSE','TC_VISIBILITY_FLAG','ASSESSMENT_REASON','FLAG_FOR_LATER','ASSESSMENT_FLAG_COMMENT')
  ORDER BY 9 DESC,
    10 DESC;
	/
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT"."PK_COLUMN_ID"
IS
  'Primary key of Audit Column table';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_LABSUMMARY_AUDIT"
IS
  'This view provides Lab Summary audit details ';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_LICENSURE_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD")
AS
  SELECT acm.column_name ,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_entity_status
    WHERE pk_entity_status = arm.module_id
    AND status_type_code   = 'licence'
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_entity_status
    FROM eres.cb_entity_status
    WHERE status_type_code = 'licence'
    )
  AND arm.table_name       = 'CB_ENTITY_STATUS')
  AND acm.column_name NOT IN ('RID','IP_ADD','CREATED_ON','CREATOR','DELETEDFLAG','PK_ENTITY_STATUS','ENTITY_ID','IS_CORD_ENTRY_DATA','ORDER_ID','ORDER_FK_RESOL','STATUS_DATE','FK_ENTITY_TYPE','STATUS_TYPE_CODE')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_entity_status
    WHERE status_type_code='licence'
    AND pk_entity_status IN
      (SELECT fk_entity_status
      FROM eres.cb_entity_status_reason
      WHERE pk_entity_status_reason = arm.module_id
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_entity_status_reason
    FROM eres.cb_entity_status_reason
    WHERE fk_entity_status IN
      (SELECT pk_entity_status
      FROM eres.cb_entity_status
      WHERE status_type_code= 'licence'
      )
    AND arm.table_name = 'CB_ENTITY_STATUS_REASON'
    ))
  AND acm.column_name NOT IN ('RID','IP_ADD','CREATED_ON','CREATOR','PK_ENTITY_STATUS_REASON','DELETEDFLAG','FK_ATTACHMENTID','FK_FORMID','NOT_APP_PRIOR_FLAG','FK_ENTITY_STATUS')
  ORDER BY 9 DESC;
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_LICENSURE_AUDIT"
IS
  'This view provides Licensure audit details ';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_MINCRITERIA_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD")
AS
  SELECT ACM.COLUMN_NAME,
    ACM.COLUMN_DISPLAY_NAME,
    (SELECT USR_LOGNAME FROM eres.ER_USER WHERE PK_USER = ARM.USER_ID
    ) USER_ID,
    TO_CHAR(ARM.TIMESTAMP,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(ARM.ACTION,'I','Add','U','Update','-') ACTION,
    ACM.OLD_VALUE,
    ACM.NEW_VALUE,
    ARM.TABLE_NAME,
    ARM.TIMESTAMP,
    (SELECT fk_cord_id
    FROM eres.CB_CORD_MINIMUM_CRITERIA
    WHERE PK_CORD_MINIMUM_CRITERIA = ARM.MODULE_ID
    ) pk_cord
  FROM eres.AUDIT_ROW_MODULE ARM ,
    eres.AUDIT_COLUMN_MODULE ACM
  WHERE ACM.FK_ROW_ID = ARM.PK_ROW_ID
  AND (ARM.MODULE_ID IN
    (SELECT PK_CORD_MINIMUM_CRITERIA FROM eres.CB_CORD_MINIMUM_CRITERIA
    )
  AND ARM.TABLE_NAME   = 'CB_CORD_MINIMUM_CRITERIA')
  AND ACM.COLUMN_NAME IN ('ANTIHIV_II_HIVP24_HIVNAT_COMP','HBSAG_COMP','BACTERIAL_TESTING_FLAG', 'FUNGAL_TESTING_FLAG','HEMOGLOBINOPTHY_FLAG','IDMS_FLAG','HLA_CONFIRM','ANTIGENS_CONFIRM','DECLARATION_RESULT', 'SAVEINPROGESS','CONFORMED_MINIMUM_CRITIRIA','ANTIHCV_COMP', 'HLA_CNFM','ANTIGEN_MATCH','HIVPNAT_COMP','HLACHILD', 'CTCOMPLETED','CTCOMPLETED_CHILD','ANTIGENCHILD', 'UNITRECIPIENT','UNITRECIPIENT_CHILD','CBUPLANNED', 'CBUPLANNED_CHILD','PATIENTTYPING','PATIENTTYPING_CHILD','POSTPROCESS_CHILD','VIABILITY_CHILD')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.CB_CORD_MINIMUM_CRITERIA
    WHERE PK_CORD_MINIMUM_CRITERIA IN
      (SELECT FK_MINIMUM_CRITERIA
      FROM eres.CB_CORD_TEMP_MINIMUM_CRITERIA
      WHERE PK_MINIMUM_CRITERIA_FIELD =ARM.MODULE_ID
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE ACM.FK_ROW_ID = ARM.PK_ROW_ID
  AND (ARM.MODULE_ID IN
    (SELECT PK_MINIMUM_CRITERIA_FIELD
    FROM eres.CB_CORD_TEMP_MINIMUM_CRITERIA
    WHERE FK_MINIMUM_CRITERIA=
      (SELECT PK_CORD_MINIMUM_CRITERIA FROM eres.CB_CORD_MINIMUM_CRITERIA
      )
    )
  AND ARM.TABLE_NAME   = 'CB_CORD_TEMP_MINIMUM_CRITERIA')
  AND ACM.COLUMN_NAME IN ('BACTERIAL_RESULT','FUNGAL_RESULT', 'HEMOGLOB_RESULT','HIVRESULT','HIVPRESULT','HBSAG_RESULT','HBVNAT_RESULT','HCV_RESULT','T_CRUZI_CHAGAS_RESULT', 'WNV_RESULT','TNCKG_PT_WEIGHT_RESULT','VIABILITY_RESULT')
  ORDER BY 9 DESC;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_MINCRITERIA_AUDIT"
IS
  'This view provides Minimum Criteria audit details ';
  / 
  
  COMMENT ON TABLE "VDA"."ETVDA_ID_AUDIT"
IS
  'This view provides ID audit details ';
  
  /
  
 
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ORDERDETAILS_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT order_entityid
    FROM eres.er_order_header
    WHERE pk_order_header = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order_header FROM eres.er_order_header
    )
  AND arm.table_name   = 'ER_ORDER_HEADER')
  AND acm.column_name IN ('ORDER_OPEN_DATE','FK_SITE_ID')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT order_entityid
    FROM eres.er_order_header
    WHERE pk_order_header IN
      (SELECT fk_order_header FROM eres.er_order WHERE pk_order = arm.module_id
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order
    FROM eres.er_order
    WHERE fk_order_header IN
      (SELECT pk_order_header FROM eres.er_order_header
      )
    AND arm.table_name = 'ER_ORDER'
    ))
  AND acm.column_name NOT IN ('RID','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','IP_ADD','PK_ORDER','ORDER_TYPE','FK_ORDER_HEADER', 'CBU_AVAIL_CONFIRM_FLAG','FK_ATTACHMENT','TASK_ID','TASK_NAME','DELETEDFLAG', 'CLINIC_INFO_CHECKLIST_STAT','ADDI_TEST_RESULT_AVAIL','CANCEL_CONFORM_DATE','CANCELED_BY','ACCPT_TO_CANCEL_REQ','FK_CASE_MANAGER','ORDER_NUMBER','ORDER_ENTITYID','ORDER_ENTITYTYPE','FK_ORDER_STATUS','ORDER_REMARK','ORDER_BY','FK_ORDER_ITEM_ID','FK_ORDER_TYPE','DATA_MODIFIED_FLAG','FINAL_REVIEW_TASK_FLAG','COMPLETE_REQ_INFO_TASK_FLAG')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.cb_shipment
    WHERE pk_shipment = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_shipment FROM eres.cb_shipment
    )
  AND arm.table_name       = 'CB_SHIPMENT')
  AND acm.column_name NOT IN ('PK_SHIPMENT','FK_ORDER_ID','FK_CORD_ID','RID','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','DATA_MODIFIED_FLAG','FK_SHIPMENT_TYPE','CBB_ID','FK_CORD_ID','DELETEDFLAG')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.er_order_receipant_info
    WHERE pk_order_receipant_info = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order_receipant_info FROM eres.er_order_receipant_info
    )
  AND arm.table_name       = 'ER_ORDER_RECEIPANT_INFO')
  AND acm.column_name NOT IN ('PK_ORDER_RECEIPANT_INFO','FK_RECEIPANT','FK_ORDER_ID','FK_CORD_ID','RID','ENTITY_ID','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','IP_ADD','NMDP_RID','NMDP_ID','LOCAL_ID','COOP_REF','TC_CDE','SCU_ACTIVATED_DATE','FK_SITE_ID')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM'),
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_notes
    WHERE pk_notes    = arm.module_id
    AND fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'progress'
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_notes
    FROM eres.cb_notes
    WHERE fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'progress'
      )
    )
  AND arm.table_name   = 'CB_NOTES')
  AND acm.column_name IN ('SUBJECT','KEYWORD','TAG_NOTE','NOTES_PROGRESS','FK_NOTES_CATEGORY','NOTE_VISIBLE','REQUEST_TYPE','REQUEST_DATE','AMENDED','NOTE_SEQ')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT order_entityid
    FROM eres.er_order_header
    WHERE order_requested_by = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT order_requested_by FROM eres.er_order_header
    )
  AND arm.table_name       = 'ER_ORDER_USERS')
  AND acm.column_name NOT IN ('RID','DELETEDFLAG','USER_COMMENTS','PK_ORDER_USER','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.er_order_receipant_info
    WHERE FK_RECEIPANT = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT FK_RECEIPANT FROM eres.er_order_receipant_info
    )
  AND arm.table_name       = 'CB_RECEIPANT_INFO')
  AND acm.column_name NOT IN ('PK_RECEIPANT','FK_PERSON_ID','REC_WEIGHT','ENTRY_DATE','GUID','REC_HEIGHT','RID','DELETEDFLAG','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT FK_CORD_ID
    FROM eres.CB_CORD_COMPLETE_REQ_INFO
    WHERE PK_CORD_COMPLETE_REQINFO = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT PK_CORD_COMPLETE_REQINFO FROM eres.CB_CORD_COMPLETE_REQ_INFO
    )
  AND arm.table_name   = 'CB_CORD_COMPLETE_REQ_INFO')
  AND acm.column_name IN ('COMPLETE_REQ_INFO_FLAG')
  ORDER BY 9 DESC;
  /
  
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."TIMESTAMP"
IS
  'Time of action taken';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  
  COMMENT ON TABLE "VDA"."ETVDA_ORDERDETAILS_AUDIT"
IS
  'This view provides Order Details audit details ';
  /
  

  
  
 CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_PROCESSINGINFO_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_entity_samples
    WHERE pk_entity_samples = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_entity_samples FROM eres.cb_entity_samples
    )
  AND arm.table_name       = 'CB_ENTITY_SAMPLES')
  AND acm.column_name NOT IN ('PK_ENTITY_SAMPLES','ENTITY_ID','ENTITY_TYPE','CREATOR','CREATED_ON','RID','IP_ADD','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','FK_SPECIMEN','DELETEDFLAG','FK_SAMPLE_TYPE','FK_SAMPLE_STATUS','SEQUENCE')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    arm.module_id pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id  = arm.pk_row_id
  AND (arm.table_name  = 'CB_CORD')
  AND acm.column_name IN ('FK_CBB_PROCEDURE')
  ORDER BY 9 DESC;
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT"."TIMESTAMP"
IS
  'Time of action taken';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  
  COMMENT ON TABLE "VDA"."ETVDA_PROCESSINGINFO_AUDIT"
IS
  'This view provides Processing Information audit details '; 
  /
  
  

----Include all views here-------------------------------------------------








  
INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,1,1,'01_views.sql',sysdate,'ETVDA 1.0 Build 1')
/

commit
/
