@echo off

REM Execute this script on the DB server itself
REM Set DB_TBS to the tablespace directory for example E:\VDA
REM VDA_PWD is set to vda123 by default

REM -- Set variables begins
set DB_SID=
set DB_TBS=
set SYS_PWD=
set ERES_PWD=
set ESCH_PWD=
set EPAT_PWD=
REM -- Set variables ends

echo ===Starting VDA DB script
echo ===Starting VDA DB script 1>&2> out.txt

:create
if exist done_newuserVDA.txt goto eres
echo ===Running newuserVDA...
echo ===Running newuserVDA... 1>&2>> out.txt
sqlplus "sys/%SYS_PWD%@%DB_SID% as SYSDBA" @1_newuserVDA\1_sys_VDAuser.SQL %DB_TBS% 1>&2>> out.txt
if not exist done_newuserVDA.txt goto error
echo Created VDA user successfully

:eres
if exist done_eres.txt goto esch
echo ===Running eres...
echo ===Running eres... 1>&2>> out.txt
sqlplus eres/%ERES_PWD%@%DB_SID% @run_eres.sql 1>&2>> out.txt
if not exist done_eres.txt goto error

:esch
if exist done_esch.txt goto epat
echo ===Running esch...
echo ===Running esch... 1>&2>> out.txt
sqlplus esch/%ESCH_PWD%@%DB_SID% @run_esch.sql 1>&2>> out.txt
if not exist done_esch.txt goto error

:epat
if exist done_epat.txt goto vda
echo ===Running epat...
echo ===Running epat... 1>&2>> out.txt
sqlplus epat/%EPAT_PWD%@%DB_SID% @run_epat.sql 1>&2>> out.txt
if not exist done_epat.txt goto error

:vda
if exist done_vda.txt goto eof
echo ===Running vda...
echo ===Running vda... 1>&2>> out.txt
sqlplus vda/vda123@%DB_SID% @run_vda.sql 1>&2>> out.txt
if not exist done_vda.txt goto error

echo ===All done
echo ===All done 1>&2>> out.txt
goto eof

:error
echo ===An error occurred. Exiting this script...
echo ===An error occurred. Exiting this script... 1>&2>> out.txt
goto eof

:eof
