CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB" ("PK_PATLABS", "FK_PER", "FK_TESTID", "TEST_DATE", "TEST_RESULT", "TEST_RESULT_TX", "TEST_TYPE", "TEST_UNIT", "TEST_RANGE", "TEST_LLN", "TEST_ULN", "FK_SITE", "FK_CODELST_TSTSTAT", "ACCESSION_NUM", "FK_TESTGROUP", "FK_CODELST_ABFLAG", "FK_PATPROT", "FK_EVENT_ID", "CUSTOM001", "CUSTOM002", "CUSTOM003", "CUSTOM004", "CUSTOM005", "CUSTOM006", "CUSTOM007", "CUSTOM008", "CUSTOM009", "CUSTOM010", "NOTES", "RID", "CREATOR", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATED_ON", "IP_ADD", "ER_NAME", "ER_GRADE", "FK_CODELST_STDPHASE", "FK_STUDY", "FK_SPECIMEN", "FK_TEST_OUTCOME", "CMS_APPROVED_LAB", "FDA_LICENSED_LAB", "FK_TEST_REASON", "FK_TEST_METHOD", "FT_TEST_SOURCE", "FK_TEST_SPECIMEN", "FK_TIMING_OF_TEST")
AS
  SELECT "PK_PATLABS",
    "FK_PER",
    "FK_TESTID",
    "TEST_DATE",
    "TEST_RESULT",
    "TEST_RESULT_TX",
    "TEST_TYPE",
    "TEST_UNIT",
    "TEST_RANGE",
    "TEST_LLN",
    "TEST_ULN",
    "FK_SITE",
    "FK_CODELST_TSTSTAT",
    "ACCESSION_NUM",
    "FK_TESTGROUP",
    "FK_CODELST_ABFLAG",
    "FK_PATPROT",
    "FK_EVENT_ID",
    "CUSTOM001",
    "CUSTOM002",
    "CUSTOM003",
    "CUSTOM004",
    "CUSTOM005",
    "CUSTOM006",
    "CUSTOM007",
    "CUSTOM008",
    "CUSTOM009",
    "CUSTOM010",
    "NOTES",
    "RID",
    "CREATOR",
    "LAST_MODIFIED_BY",
    "LAST_MODIFIED_DATE",
    "CREATED_ON",
    "IP_ADD",
    "ER_NAME",
    "ER_GRADE",
    "FK_CODELST_STDPHASE",
    "FK_STUDY",
    "FK_SPECIMEN",
    "FK_TEST_OUTCOME",
    "CMS_APPROVED_LAB",
    "FDA_LICENSED_LAB",
    "FK_TEST_REASON",
    "FK_TEST_METHOD",
    "FT_TEST_SOURCE",
    "FK_TEST_SPECIMEN",
    "FK_TIMING_OF_TEST"
  FROM ERES.ER_PATLABS;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."PK_PATLABS"
IS
  'Primary key for ETVDA_QUERY_BUILDER_PAT_LAB';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_PER"
IS
  'Foriegn key to the patient table';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_TESTID"
IS
  'Foriegn key to labs lookup table';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."TEST_DATE"
IS
  'Lab test performed on';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."TEST_RESULT"
IS
  'Test result - NM, ST, SN';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."TEST_RESULT_TX"
IS
  'Test result - TX';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."TEST_TYPE"
IS
  'Test result type 1=NM, 2=ST, 3=SN, 4=TX';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."TEST_UNIT"
IS
  'Test unit';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."TEST_RANGE"
IS
  'Test range in string';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."TEST_LLN"
IS
  'LOWER LIMIT NORMAL - Test range for NM and SN type labs';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."TEST_ULN"
IS
  'UPPER LIMIT NORMAL - Test range for NM and SN type labs';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_SITE"
IS
  'Sending lab facility';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_CODELST_TSTSTAT"
IS
  'Status of the test';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."ACCESSION_NUM"
IS
  'Patient accesion number';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_TESTGROUP"
IS
  'Lab group';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_CODELST_ABFLAG"
IS
  'Abnormal flag - Value from code list';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_PATPROT"
IS
  'Study for which the lab done';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_EVENT_ID"
IS
  'Event associated to lab';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."CUSTOM001"
IS
  'Custom field - used with interfaces to store client pk';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."CUSTOM002"
IS
  'Custom field for future used to store test decription.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."CUSTOM003"
IS
  'Custom field - Used to store the Test Comments';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."CUSTOM004"
IS
  'Custom field for future use';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."CUSTOM005"
IS
  'Custom field for future use';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."CUSTOM006"
IS
  'Custom field for future use';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."CUSTOM007"
IS
  'Custom field for future use';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."CUSTOM008"
IS
  'Custom field - Used to store the Test Count';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."CUSTOM009"
IS
  'Custom field for future use';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."CUSTOM010"
IS
  'Custom field for future use';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."RID"
IS
  'This column is used for the audit trail. The RID uniquely identifies the row in the database.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."CREATOR"
IS
  'This column is used for Audit Trail. The Creator identifies the user who created this row.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."LAST_MODIFIED_BY"
IS
  'This column is used for Audit Trail. The last_modified_by identifies the user who last modified this row.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."LAST_MODIFIED_DATE"
IS
  'This column is used for Audit Trail. Stores the date on which this row was last modified.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."CREATED_ON"
IS
  'This column is used for Audit Trail. Stores the date on which this row was created.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."IP_ADD"
IS
  'This column is used for Audit Trail. Stores the IP ADDRESS of the client machine.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_CODELST_STDPHASE"
IS
  'This column stores the study phase value from code list';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_STUDY"
IS
  'This column stores the study id the lab is linked to';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_SPECIMEN"
IS
  'This is the foreign key to the er_specimen table';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_TEST_OUTCOME"
IS
  'Code for test_outcome referenced from Code List(ER_CODELST).';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."CMS_APPROVED_LAB"
IS
  'This column is for the CMS APPROVED LAB.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FDA_LICENSED_LAB"
IS
  'This column is for the FDA LICENSED LAB.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_TEST_REASON"
IS
  'This column is for the FK TEST REASON.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_TEST_METHOD"
IS
  'This column is for the FK TEST METHOD.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FT_TEST_SOURCE"
IS
  'This column is for the FT TEST SOURCE.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_TEST_SPECIMEN"
IS
  'This column will store FK TEST SAMPLE.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"."FK_TIMING_OF_TEST"
IS
  'Refrence from ER_CODELST.';
  COMMENT ON TABLE "VDA"."ETVDA_QUERY_BUILDER_PAT_LAB"
IS
  'This view stors the Patlab information.';
   
INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,13,3,'03_ETVDA_QUERY_BUILDER_PAT_LAB.sql',sysdate,'ETVDA 1.0 Build 13');

commit;  