set define off;

INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,13,0,'00_er_version.sql',sysdate,'ETVDA 1.0 Build 13');

commit;