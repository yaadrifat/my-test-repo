----/*This readMe is specific to EmTrax Velos Data Analytics version ETVDA1.0 Build#5*/------------------


1. We have released --  5 new VDA views in this build. 
2. These views are required for Query tool so these should be deployed before ET046.
3. Please coneect to VDA user in emtrax database and execute the patches under "vda" Folder according to the sequence.
4. We have also released one  technical document  "ETVDA-Technical-document-Draft 1.0.0.xls" along with this build.

-----------------------------------------------------------------------------------------------------------------------------



 


	