----/*This readMe is specific to EmTrax Velos Data Analytics version ETVDA1.0 Build#4*/------------------


1. We have released 14 new VDA views in this build. Apart from that around 6 views were modified.
2. Please coneect to ERES user in emtrax database and execute the patches under "eres" Folder according to the sequence.
3. Please coneect to VDA user in emtrax database and execute the patches under "vda" Folder according to the sequence.
4. We have also released one  technical document  "ETVDA-Technical-document-Draft 1.0.0.xls" along with this build.
-----------------------------------------------------------------------------------------------------------------------------



 


	