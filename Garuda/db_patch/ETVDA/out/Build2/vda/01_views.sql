--EmTrax views for Version ET028.1- Release Date 02-21-2013 
 
 
 CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBUINFORMATION_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp ,
    arm.module_id pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id  = arm.pk_row_id
  AND (arm.table_name  = 'CB_CORD') 
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_multiple_values
    WHERE pk_multiple_values = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_multiple_values
    FROM eres.cb_multiple_values
    WHERE fk_codelst_type = 'race'
    )
  AND arm.table_name   = 'CB_MULTIPLE_VALUES')
 UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT pk_cord
    FROM eres.cb_cord
    WHERE fk_specimen_id = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT FK_SPECIMEN_ID FROM eres.CB_CORD
    )
  AND arm.table_name   = 'ER_SPECIMEN')
  ORDER BY 9 DESC;
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT_ALL"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBUINFORMATION_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBUINFORMATION_AUDIT_ALL"
IS
  'This view contains the information about the CBU Information and shows all fields in addition to the fields being displayed over Audit Trail widget.';
  /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CLINICALNOTES_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    (SELECT entity_id
    FROM eres.cb_notes
    WHERE pk_notes    = arm.module_id
    AND fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'clinic'
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_notes
    FROM eres.cb_notes
    WHERE fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'clinic'
      )
    )
  AND arm.table_name   = 'CB_NOTES')
   ORDER BY 4 DESC;
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CLINICALNOTES_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CLINICALNOTES_AUDIT_ALL"
IS
  'This view contains the information about the Clinical Notes and shows all fields in addition to the fields being displayed over Audit Trail widget.';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_DOCUMENT_AUDIT_ALL" ("CATEGORY", "SUBCATEGORY", "USER_ID", "DATE_TIME", "ACTION", "DOCLINK", "MODULE_ID", "PK_CORD")
AS
  SELECT eres.f_codelst_desc(cu.fk_category) category,
    eres.f_codelst_desc(cu.fk_subcategory) subcategory,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Revoke','') action,
    (SELECT dcms_file_attachment_id
    FROM eres.er_attachments
    WHERE pk_attachment = arm.module_id
    ) doclink ,
    arm.module_id,
    (SELECT entity_id
    FROM eres.er_attachments
    WHERE pk_attachment = arm.module_id
    ) pk_cord
  FROM eres.audit_row_module arm,
    eres.cb_upload_info cu
  WHERE arm.module_id IN
    (SELECT pk_attachment FROM eres.er_attachments
    )
  AND arm.table_name = 'ER_ATTACHMENTS'
  AND arm.module_id  = cu.fk_attachmentid
  ORDER BY 4 DESC;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT_ALL"."CATEGORY"
IS
  'Module category in which document uploaded';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT_ALL"."SUBCATEGORY"
IS
  'Module sub category in which document uploaded';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (Add, Revoke)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT_ALL"."DOCLINK"
IS
  'Provides Document link when document is revoked';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT_ALL"."MODULE_ID"
IS
  'Provides Module Id';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_DOCUMENT_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_DOCUMENT_AUDIT_ALL"
IS
  'This view contains the information about the Documents and shows all fields in addition to the fields being displayed over Audit Trail widget.';
  /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ELIGIBILITY_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp ,
    (SELECT entity_id
    FROM eres.cb_entity_status
    WHERE pk_entity_status = arm.module_id
    AND status_type_code   = 'eligibility'
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_entity_status
    FROM eres.cb_entity_status
    WHERE status_type_code = 'eligibility'
    )
  AND arm.table_name       = 'CB_ENTITY_STATUS')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_entity_status
    WHERE status_type_code='eligibility'
    AND pk_entity_status IN
      (SELECT fk_entity_status
      FROM eres.cb_entity_status_reason
      WHERE pk_entity_status_reason = arm.module_id
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_entity_status_reason
    FROM eres.cb_entity_status_reason
    WHERE fk_entity_status IN
      (SELECT pk_entity_status
      FROM eres.cb_entity_status
      WHERE status_type_code= 'eligibility'
      )
    AND arm.table_name = 'CB_ENTITY_STATUS_REASON'
    ))
  ORDER BY 9 DESC;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT_ALL"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ELIGIBILITY_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_ELIGIBILITY_AUDIT_ALL"
IS
  'This view contains the information about the Eligibility and shows all fields in addition to the fields being displayed over Audit Trail widget. ';
  /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_FINALREVIEW_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    (SELECT fk_cord_id
    FROM eres.cb_cord_final_review
    WHERE pk_cord_final_review = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_cord_final_review FROM eres.cb_cord_final_review
    )
  AND arm.table_name   = 'CB_CORD_FINAL_REVIEW')
  ORDER BY 4 DESC;
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FINALREVIEW_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_FINALREVIEW_AUDIT_ALL"
IS
  'This view contains the information about the final and shows all fields in addition to the fields being displayed over Audit Trail widget.';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_FMHQ_MRQ_AUDIT_ALL" ("PK_FORM_VERSION", "FORMNAME", "FVERSION", "USER_ID", "DATE_TIME", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "PK_CORD")
AS
  SELECT pk_form_version,
    (SELECT forms_desc
    FROM eres.cb_forms
    WHERE pk_form   = ver.fk_form
    AND forms_desc IN ('FMHQ','MRQ')
    ) formname,
    (SELECT version
    FROM eres.cb_forms
    WHERE pk_form   = ver.fk_form
    AND forms_desc IN ('FMHQ','MRQ')
    ) fversion,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = ver.creator
    ) user_id,
    TO_CHAR(created_on,'Mon DD, YYYY HH:MI:SS AM') date_time,
    last_modified_by,
    last_modified_date,
    entity_id pk_cord
  FROM eres.cb_form_version ver
  WHERE fk_form IN
    (SELECT pk_form
    FROM eres.cb_forms
    WHERE pk_form   = ver.fk_form
    AND forms_desc IN ('FMHQ','MRQ')
    )
  ORDER BY pk_form_version DESC;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT_ALL"."PK_FORM_VERSION"
IS
  'Primary key of cb_form_version table';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT_ALL"."FORMNAME"
IS
  'Stores form Description like MRQ/FMHQ/IDM etc';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT_ALL"."FVERSION"
IS
  'Stores form version';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT_ALL"."LAST_MODIFIED_BY"
IS
  'Stores user who Last modified the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT_ALL"."LAST_MODIFIED_DATE"
IS
  'Stores last modified date';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_FMHQ_MRQ_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_FMHQ_MRQ_AUDIT_ALL"
IS
  'This view contains the information about the FMHQ, MRQ and shows all fields in addition to the fields being displayed over Audit Trail widget.';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_HLA_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    (SELECT entity_id
    FROM eres.cb_hla
    WHERE pk_hla = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_hla FROM eres.cb_hla
    )
  AND arm.table_name       = 'CB_HLA')
  ORDER BY 4 DESC;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_HLA_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_HLA_AUDIT_ALL"
IS
  'This view contains the information about the HLA and shows all fields in addition to the fields being displayed over Audit Trail widget.';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ID_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "TSTAMP", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_COLUMN_ID", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    acm.pk_column_id,
    (arm.module_id) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id  = arm.pk_row_id
  AND (arm.table_name  = 'CB_CORD')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') tstamp,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    acm.pk_column_id,
    (SELECT entity_id
    FROM eres.CB_ADDITIONAL_IDS
    WHERE PK_CB_ADDITIONAL_IDS = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT PK_CB_ADDITIONAL_IDS FROM eres.CB_ADDITIONAL_IDS
    )
  AND arm.table_name   = 'CB_ADDITIONAL_IDS')
  ORDER BY 9 DESC,
    10 DESC;
	
	/
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT_ALL"."TSTAMP"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT_ALL"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT_ALL"."PK_COLUMN_ID"
IS
  'Primary key of Audit Column table';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ID_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  
   COMMENT ON TABLE "VDA"."ETVDA_ID_AUDIT_ALL"
IS
  'This view contains the information about the ID and shows all fields in addition to the fields being displayed over Audit Trail widget.';
  
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_IDM_AUDIT_ALL" ("PK_FORM_VERSION", "FORMNAME", "FVERSION", "USER_ID", "DATE_TIME", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "PK_CORD")
AS
  SELECT pk_form_version,
    (SELECT forms_desc
    FROM eres.cb_forms
    WHERE pk_form  = ver.fk_form
    AND forms_desc ='IDM'
    ) formname,
    (SELECT version
    FROM eres.cb_forms
    WHERE pk_form  = ver.fk_form
    AND forms_desc ='IDM'
    ) fversion,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = ver.creator
    ) user_id,
    TO_CHAR(created_on,'Mon DD, YYYY HH:MI:SS AM') date_time,
    last_modified_by,
    last_modified_date,
    entity_id pk_cord
  FROM eres.cb_form_version ver
  WHERE fk_form IN
    (SELECT pk_form
    FROM eres.cb_forms
    WHERE pk_form  = ver.fk_form
    AND forms_desc = 'IDM'
    )
  ORDER BY pk_form_version DESC;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT_ALL"."PK_FORM_VERSION"
IS
  'Primary key of cb_form_version table';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT_ALL"."FORMNAME"
IS
  'Stores form Description like MRQ/FMHQ/IDM etc';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT_ALL"."FVERSION"
IS
  'Stores form version';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT_ALL"."LAST_MODIFIED_BY"
IS
  'Stores user who Last modified the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT_ALL"."LAST_MODIFIED_DATE"
IS
  'Stores last modified date';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_IDM_AUDIT_ALL"
IS
  'This view contains the information about the IDM and shows all fields in addition to the fields being displayed over Audit Trail widget.';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_LABSUMMARY_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_COLUMN_ID", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    acm.pk_column_id,
    (SELECT pk_cord
    FROM eres.cb_cord
    WHERE fk_specimen_id =
      (SELECT fk_specimen FROM eres.er_patlabs WHERE pk_patlabs = arm.module_id
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_patlabs
    FROM eres.er_patlabs
    WHERE fk_specimen IN
      (SELECT fk_specimen_id FROM eres.cb_cord
      )
    AND arm.table_name = 'ER_PATLABS'
    )) 
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    acm.pk_column_id,
    arm.module_id pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id  = arm.pk_row_id
  AND (arm.table_name  = 'CB_CORD')
 UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    acm.pk_column_id,
    (SELECT entity_id
    FROM eres.cb_assessment
    WHERE pk_assessment = arm.module_id
    AND entity_type    IN (eres.f_codelst_id('entity_type','CBU'))
    AND sub_entity_id  IN (eres.f_codelst_id('fung_cul','not_done'),eres.f_codelst_id('hem_path_scrn','alp_thal_sil'),eres.f_codelst_id('hem_path_scrn','not_done'), eres.f_codelst_id('hem_path_scrn','hemo_trait'),eres.f_codelst_id('hem_path_scrn','homo_no_dis'),eres.f_codelst_id('hem_path_scrn','alp_thal_trait'),eres.f_codelst_id('hem_path_scrn','unknown'))
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_assessment
    FROM eres.cb_assessment
    WHERE entity_type IN (eres.f_codelst_id('entity_type','CBU'))
    AND sub_entity_id IN (eres.f_codelst_id('fung_cul','not_done'),eres.f_codelst_id('hem_path_scrn','alp_thal_sil'),eres.f_codelst_id('hem_path_scrn','not_done'), eres.f_codelst_id('hem_path_scrn','hemo_trait'),eres.f_codelst_id('hem_path_scrn','homo_no_dis'),eres.f_codelst_id('hem_path_scrn','alp_thal_trait'),eres.f_codelst_id('hem_path_scrn','unknown'))
    )
  AND arm.table_name   = 'CB_ASSESSMENT')
  ORDER BY 9 DESC,
    10 DESC;
	/
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT_ALL"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT_ALL"."PK_COLUMN_ID"
IS
  'Primary key of Audit Column table';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LABSUMMARY_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_LABSUMMARY_AUDIT_ALL"
IS
  'This view contains the information about the Lab Summaryand shows all fields in addition to the fields being displayed over Audit Trail widget. ';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_LICENSURE_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD")
AS
  SELECT acm.column_name ,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_entity_status
    WHERE pk_entity_status = arm.module_id
    AND status_type_code   = 'licence'
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_entity_status
    FROM eres.cb_entity_status
    WHERE status_type_code = 'licence'
    )
  AND arm.table_name       = 'CB_ENTITY_STATUS')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_entity_status
    WHERE status_type_code='licence'
    AND pk_entity_status IN
      (SELECT fk_entity_status
      FROM eres.cb_entity_status_reason
      WHERE pk_entity_status_reason = arm.module_id
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_entity_status_reason
    FROM eres.cb_entity_status_reason
    WHERE fk_entity_status IN
      (SELECT pk_entity_status
      FROM eres.cb_entity_status
      WHERE status_type_code= 'licence'
      )
    AND arm.table_name = 'CB_ENTITY_STATUS_REASON'
    ))
  ORDER BY 9 DESC;
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT_ALL"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_LICENSURE_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_LICENSURE_AUDIT_ALL"
IS
  'This view contains the information about the License and shows all fields in addition to the fields being displayed over Audit Trail widget.  ';
  /
  
CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD")
AS
  SELECT ACM.COLUMN_NAME,
    ACM.COLUMN_DISPLAY_NAME,
    (SELECT USR_LOGNAME FROM eres.ER_USER WHERE PK_USER = ARM.USER_ID
    ) USER_ID,
    TO_CHAR(ARM.TIMESTAMP,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(ARM.ACTION,'I','Add','U','Update','-') ACTION,
    ACM.OLD_VALUE,
    ACM.NEW_VALUE,
    ARM.TABLE_NAME,
    ARM.TIMESTAMP,
    (SELECT fk_cord_id
    FROM eres.CB_CORD_MINIMUM_CRITERIA
    WHERE PK_CORD_MINIMUM_CRITERIA = ARM.MODULE_ID
    ) pk_cord
  FROM eres.AUDIT_ROW_MODULE ARM ,
    eres.AUDIT_COLUMN_MODULE ACM
  WHERE ACM.FK_ROW_ID = ARM.PK_ROW_ID
  AND (ARM.MODULE_ID IN
    (SELECT PK_CORD_MINIMUM_CRITERIA FROM eres.CB_CORD_MINIMUM_CRITERIA
    )
  AND ARM.TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA')
  UNION
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.CB_CORD_MINIMUM_CRITERIA
    WHERE PK_CORD_MINIMUM_CRITERIA IN
      (SELECT FK_MINIMUM_CRITERIA
      FROM eres.CB_CORD_TEMP_MINIMUM_CRITERIA
      WHERE PK_MINIMUM_CRITERIA_FIELD =ARM.MODULE_ID
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE ACM.FK_ROW_ID = ARM.PK_ROW_ID
  AND (ARM.MODULE_ID IN
    (SELECT PK_MINIMUM_CRITERIA_FIELD
    FROM eres.CB_CORD_TEMP_MINIMUM_CRITERIA
    WHERE FK_MINIMUM_CRITERIA=
      (SELECT PK_CORD_MINIMUM_CRITERIA FROM eres.CB_CORD_MINIMUM_CRITERIA
      )
    )
  AND ARM.TABLE_NAME = 'CB_CORD_TEMP_MINIMUM_CRITERIA')
  ORDER BY 9 DESC;  

/ 
 COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
 
  COMMENT ON TABLE "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"
IS
  'This view contains the information about the Minimum Criteria and shows all fields in addition to the fields being displayed over Audit Trail widget. ';
  / 
  
 
 
 
 
 CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_MINCRITERIA_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD") AS 
SELECT ACM.COLUMN_NAME,
    ACM.COLUMN_DISPLAY_NAME,
    (SELECT USR_LOGNAME FROM eres.ER_USER WHERE PK_USER = ARM.USER_ID
    ) USER_ID,
    TO_CHAR(ARM.TIMESTAMP,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(ARM.ACTION,'I','Add','U','Update','-') ACTION,
    ACM.OLD_VALUE,
    ACM.NEW_VALUE,
    ARM.TABLE_NAME,
    ARM.TIMESTAMP,
    (SELECT fk_cord_id
    FROM eres.CB_CORD_MINIMUM_CRITERIA
    WHERE PK_CORD_MINIMUM_CRITERIA = ARM.MODULE_ID
    ) pk_cord
  FROM eres.AUDIT_ROW_MODULE ARM ,
    eres.AUDIT_COLUMN_MODULE ACM
  WHERE ACM.FK_ROW_ID = ARM.PK_ROW_ID
  AND (ARM.MODULE_ID IN
    (SELECT PK_CORD_MINIMUM_CRITERIA FROM eres.CB_CORD_MINIMUM_CRITERIA
    )
  AND ARM.TABLE_NAME   = 'CB_CORD_MINIMUM_CRITERIA')
  AND ACM.COLUMN_NAME IN ('ANTIHIV_II_HIVP24_HIVNAT_COMP','HBSAG_COMP','BACTERIAL_TESTING_FLAG', 'FUNGAL_TESTING_FLAG','HEMOGLOBINOPTHY_FLAG','IDMS_FLAG','HLA_CONFIRM','ANTIGENS_CONFIRM','DECLARATION_RESULT', 'SAVEINPROGESS','CONFORMED_MINIMUM_CRITIRIA','ANTIHCV_COMP', 'HLA_CNFM','ANTIGEN_MATCH','HIVPNAT_COMP','HLACHILD', 'CTCOMPLETED','CTCOMPLETED_CHILD','ANTIGENCHILD', 'UNITRECIPIENT','UNITRECIPIENT_CHILD','CBUPLANNED', 'CBUPLANNED_CHILD','PATIENTTYPING','PATIENTTYPING_CHILD','POSTPROCESS_CHILD','VIABILITY_CHILD')
  UNION 
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.CB_CORD_MINIMUM_CRITERIA
    WHERE PK_CORD_MINIMUM_CRITERIA IN
      (SELECT FK_MINIMUM_CRITERIA
      FROM eres.CB_CORD_TEMP_MINIMUM_CRITERIA
      WHERE PK_MINIMUM_CRITERIA_FIELD =ARM.MODULE_ID
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE ACM.FK_ROW_ID = ARM.PK_ROW_ID
  AND (ARM.MODULE_ID IN
    (SELECT PK_MINIMUM_CRITERIA_FIELD
    FROM eres.CB_CORD_TEMP_MINIMUM_CRITERIA
    WHERE FK_MINIMUM_CRITERIA in
      (SELECT PK_CORD_MINIMUM_CRITERIA FROM eres.CB_CORD_MINIMUM_CRITERIA
      )
    )
  AND ARM.TABLE_NAME   = 'CB_CORD_TEMP_MINIMUM_CRITERIA')
  AND ACM.COLUMN_NAME IN ('BACTERIAL_RESULT','FUNGAL_RESULT', 'HEMOGLOB_RESULT','HIVRESULT','HIVPRESULT','HBSAG_RESULT','HBVNAT_RESULT','HCV_RESULT','T_CRUZI_CHAGAS_RESULT', 'WNV_RESULT','TNCKG_PT_WEIGHT_RESULT','VIABILITY_RESULT')
  ORDER BY 9 DESC;
  /
   COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  COMMENT ON TABLE "VDA"."ETVDA_MINCRITERIA_AUDIT"
IS
  'This view provides Minimum Criteria audit details ';
  / 
  
  COMMENT ON TABLE "VDA"."ETVDA_MINCRITERIA_AUDIT"
IS
  'This view contains the information about the Minimum Criteria.';
  
  /
 
    CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD") AS
    select "COLUMN_NAME","COLUMN_DISPLAY_NAME","USER_ID","DATE_TIME","ACTION","OLD_VALUE","NEW_VALUE","TABLE_NAME","TIMESTAMP","PK_CORD" from (   SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT order_entityid
    FROM eres.er_order_header
    WHERE pk_order_header = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order_header FROM eres.er_order_header
    )
  AND arm.table_name   = 'ER_ORDER_HEADER')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT order_entityid
    FROM eres.er_order_header
    WHERE pk_order_header IN
      (SELECT fk_order_header FROM eres.er_order WHERE pk_order = arm.module_id
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order
    FROM eres.er_order
    WHERE fk_order_header IN
      (SELECT pk_order_header FROM eres.er_order_header
      )
    AND arm.table_name = 'ER_ORDER'
    ))
   UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.cb_shipment
    WHERE pk_shipment = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_shipment FROM eres.cb_shipment
    )
  AND arm.table_name       = 'CB_SHIPMENT')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.er_order_receipant_info
    WHERE pk_order_receipant_info = arm.module_id and 
    arm.table_name = 'ER_ORDER_RECEIPANT_INFO'
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order_receipant_info FROM eres.er_order_receipant_info
    )
  AND arm.table_name       = 'ER_ORDER_RECEIPANT_INFO')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM'),
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_notes
    WHERE pk_notes    = arm.module_id
    AND fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'progress'
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_notes
    FROM eres.cb_notes
    WHERE fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'progress'
      )
    )
  AND arm.table_name   = 'CB_NOTES')
   UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT order_entityid
    FROM eres.er_order_header
    WHERE order_requested_by = arm.module_id and  
    arm.table_name = 'ER_ORDER_USERS' 
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT order_requested_by FROM eres.er_order_header
    )
  AND arm.table_name       = 'ER_ORDER_USERS')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.er_order_receipant_info
    WHERE FK_RECEIPANT = arm.module_id
    and arm.table_name   = 'CB_RECEIPANT_INFO'
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT FK_RECEIPANT FROM eres.er_order_receipant_info
    )
  AND arm.table_name       = 'CB_RECEIPANT_INFO')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT FK_CORD_ID
    FROM eres.CB_CORD_COMPLETE_REQ_INFO
    WHERE PK_CORD_COMPLETE_REQINFO = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT PK_CORD_COMPLETE_REQINFO FROM eres.CB_CORD_COMPLETE_REQ_INFO
    )
  AND arm.table_name   = 'CB_CORD_COMPLETE_REQ_INFO')
  ORDER BY 9 DESC);
  /
  
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."TIMESTAMP"
IS
  'Time of action taken';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  
  
  
  
  COMMENT ON TABLE "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"
IS
  'This view contains the information about the Order Details and shows all fields in addition to the fields being displayed over Audit Trail widget. ';
  /
  

  
  
 CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_PROCESSINGINFO_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD")
AS
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_entity_samples
    WHERE pk_entity_samples = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_entity_samples FROM eres.cb_entity_samples
    )
  AND arm.table_name       = 'CB_ENTITY_SAMPLES')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') date_time,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    arm.module_id pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id  = arm.pk_row_id
  AND (arm.table_name  = 'CB_CORD')
  ORDER BY 9 DESC;
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT_ALL"."TIMESTAMP"
IS
  'Time of action taken';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_PROCESSINGINFO_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  
  COMMENT ON TABLE "VDA"."ETVDA_PROCESSINGINFO_AUDIT_ALL"
IS
  'This view contains the information about the Processing Information and shows all fields in addition to the fields being displayed over Audit Trail widget. '; 
  /



-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."CBB_ID"
IS
  'CBB id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."CBU_LOCAL_ID"
IS
  'Local CBU ID of the the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."ADDITIONAL_ID"
IS
  'Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."ADDITIONAL_ID_TYPE"
IS
  'Type of Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."ADDITIONAL_ID_DESCRIPTION"
IS
  'Description for addtional id';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."PK_CORD"
IS
  'Primary key of cb_cord.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_ADDITIONAL_ID_ALL"
IS
  'This view provides Additional id information of cord';
  /
   COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."CBB_ID"
IS
  'CBB id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."CBU_LOCAL_ID"
IS
  'Local CBU ID of the the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."ADDITIONAL_ID"
IS
  'Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."ADDITIONAL_ID_TYPE"
IS
  'Type of Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."ADDITIONAL_ID_DESCRIPTION"
IS
  'Description for addtional id';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."PK_CORD"
IS
  'Primary key of cb_cord.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_ADDITIONAL_ID_PRG"
IS
  'This view provides Additional id information of cords(Saved In Progress)';
  /
    COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."CBB_ID"
IS
  'CBB id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."CBU_LOCAL_ID"
IS
  'Local CBU ID of the the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."ADDITIONAL_ID"
IS
  'Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."ADDITIONAL_ID_TYPE"
IS
  'Type of Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."ADDITIONAL_ID_DESCRIPTION"
IS
  'Description for addtional id';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."PK_CORD"
IS
  'Primary key of cb_cord.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_ADDITIONAL_ID_SBMT"
IS
  'This view provides Additional id information of cords(Submitted)';
  /
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CB_CORD_INFO_ALL" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "COLLECTION_DATE", "ENTRY_DATE", "CORD_CREATION_DATE", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "UNIQUE_ID_ON_BAG", "ISBT_Donation_ID", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATOR", "PK_CORD")
AS
  SELECT SITE.SITE_ID,
    CORD.CORD_REGISTRY_ID,
    CORD.CORD_LOCAL_CBU_ID,
    SPCMN.SPEC_COLLECTION_DATE,
    CORD.CREATED_ON,
    CORD.CORD_CREATION_DATE,
    CORD.REGISTRY_MATERNAL_ID,
    CORD.MATERNAL_LOCAL_ID,
    CORD.CORD_ID_NUMBER_ON_CBU_BAG,
    CORD.CORD_ISBI_DIN_CODE,
    ERES.F_GET_USER_NAME(CORD.LAST_MODIFIED_BY),
    CORD.LAST_MODIFIED_DATE,
    ERES.F_GET_USER_NAME(CORD.CREATOR),
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD,
    ERES.ER_SITE SITE,
    ERES.ER_SPECIMEN SPCMN
  WHERE CORD.FK_CBB_ID    = SITE.PK_SITE
  AND CORD.FK_SPECIMEN_ID = SPCMN.PK_SPECIMEN;
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."CBB_ID"
IS
  'CBB ID OF CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."CBU_LOCAL_ID"
IS
  'Local CBU ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."COLLECTION_DATE"
IS
  'Collection date of the specimen of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."ENTRY_DATE"
IS
  'created on date of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."CORD_CREATION_DATE"
IS
  'The date on which this cord is created in the system. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."REGISTRY_MATERNAL_ID"
IS
  'Maternal Registry ID for Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."MATERNAL_LOCAL_ID"
IS
  'Maternal Local ID for Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."UNIQUE_ID_ON_BAG"
IS
  'UNIQUE ID Number given over Bag';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."ISBT_Donation_ID"
IS
  'ISBT CODE for the Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."LAST_MODIFIED_BY"
IS
  'Identifies the user who last modified this row.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."LAST_MODIFIED_DATE"
IS
  'The date on which this cord was last modified. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."CREATOR"
IS
  'The Creator identifies the user who created the cord. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_ALL"."PK_CORD"
IS
  'Primary key of cb_cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CB_CORD_INFO_ALL"
IS
  'This view provides basic information of cord';
  /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CB_CORD_INFO_PRG" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "COLLECTION_DATE", "ENTRY_DATE", "CORD_CREATION_DATE", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "UNIQUE_ID_ON_BAG", "ISBT_Donation_ID", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATOR", "PK_CORD")
AS
  SELECT SITE.SITE_ID,
    CORD.CORD_REGISTRY_ID,
    CORD.CORD_LOCAL_CBU_ID,
    SPCMN.SPEC_COLLECTION_DATE,
    CORD.CREATED_ON,
    CORD.CORD_CREATION_DATE,
    CORD.REGISTRY_MATERNAL_ID,
    CORD.MATERNAL_LOCAL_ID,
    CORD.CORD_ID_NUMBER_ON_CBU_BAG,
    CORD.CORD_ISBI_DIN_CODE,
    ERES.F_GET_USER_NAME(CORD.LAST_MODIFIED_BY),
    CORD.LAST_MODIFIED_DATE,
    ERES.F_GET_USER_NAME(CORD.CREATOR),
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD,
    ERES.ER_SITE SITE,
    ERES.ER_SPECIMEN SPCMN
  WHERE CORD.FK_CBB_ID     = SITE.PK_SITE
  AND CORD.FK_SPECIMEN_ID  = SPCMN.PK_SPECIMEN
  AND CORD.CORD_SEARCHABLE = '0';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."CBB_ID"
IS
  'CBB ID OF CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."CBU_LOCAL_ID"
IS
  'Local CBU ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."COLLECTION_DATE"
IS
  'Collection date of the specimen of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."ENTRY_DATE"
IS
  'created on date of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."CORD_CREATION_DATE"
IS
  'The date on which this cord is created in the system. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."REGISTRY_MATERNAL_ID"
IS
  'Maternal Registry ID for Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."MATERNAL_LOCAL_ID"
IS
  'Maternal Local ID for Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."UNIQUE_ID_ON_BAG"
IS
  'UNIQUE ID Number given over Bag';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."ISBT_Donation_ID"
IS
  'ISBT CODE for the Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."LAST_MODIFIED_BY"
IS
  'Identifies the user who last modified this row.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."LAST_MODIFIED_DATE"
IS
  'The date on which this cord was last modified. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."CREATOR"
IS
  'The Creator identifies the user who created the cord. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_PRG"."PK_CORD"
IS
  'Primary key of cb_cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CB_CORD_INFO_PRG"
IS
  'This view provides basic information of cords (Saved In Progress)';
  /
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CB_CORD_INFO_SUBMT" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "COLLECTION_DATE", "ENTRY_DATE", "CORD_CREATION_DATE", "REGISTRY_MATERNAL_ID", "MATERNAL_LOCAL_ID", "UNIQUE_ID_ON_BAG", "ISBT_Donation_ID", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "CREATOR", "PK_CORD")
AS
  SELECT SITE.SITE_ID,
    CORD.CORD_REGISTRY_ID,
    CORD.CORD_LOCAL_CBU_ID,
    SPCMN.SPEC_COLLECTION_DATE,
    CORD.CREATED_ON,
    CORD.CORD_CREATION_DATE,
    CORD.REGISTRY_MATERNAL_ID,
    CORD.MATERNAL_LOCAL_ID,
    CORD.CORD_ID_NUMBER_ON_CBU_BAG,
    CORD.CORD_ISBI_DIN_CODE,
    ERES.F_GET_USER_NAME(CORD.LAST_MODIFIED_BY),
    CORD.LAST_MODIFIED_DATE,
    ERES.F_GET_USER_NAME(CORD.CREATOR),
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD,
    ERES.ER_SITE SITE,
    ERES.ER_SPECIMEN SPCMN
  WHERE CORD.FK_CBB_ID     = SITE.PK_SITE
  AND CORD.FK_SPECIMEN_ID  = SPCMN.PK_SPECIMEN
  AND CORD.CORD_SEARCHABLE = '1';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."CBB_ID"
IS
  'CBB ID OF CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."CBU_LOCAL_ID"
IS
  'Local CBU ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."COLLECTION_DATE"
IS
  'Collection date of the specimen of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."ENTRY_DATE"
IS
  'created on date of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."CORD_CREATION_DATE"
IS
  'The date on which this cord is created in the system. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."REGISTRY_MATERNAL_ID"
IS
  'Maternal Registry ID for Cord';
  
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."MATERNAL_LOCAL_ID"
IS
  'Maternal Local ID for Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."UNIQUE_ID_ON_BAG"
IS
  'UNIQUE ID Number given over Bag';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."ISBT_Donation_ID"
IS
  'ISBT CODE for the Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."LAST_MODIFIED_BY"
IS
  'Identifies the user who last modified this row.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."LAST_MODIFIED_DATE"
IS
  'The date on which this cord was last modified. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."CREATOR"
IS
  'The Creator identifies the user who created the cord. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_INFO_SUBMT"."PK_CORD"
IS
  'Primary key of cb_cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CB_CORD_INFO_SUBMT"
IS
  'This view provides basic information of cords(Submitted)';
  /
   COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_ALL"."DEFERRAL_STATUS"
IS
  'Provides deferral status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_ALL"."NATIONAL_STATUS"
IS
  'Provides national status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_ALL"."LOCAL_STATUS"
IS
  'Provides local status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_ALL"."ELIGIBILTY_STATUS"
IS
  'Provides eligibility status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_ALL"."ENTRY_DATE"
IS
  'Created on date of the Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_ALL"."PK_CORD"
IS
  'Primary key of cb_cord.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CB_CORD_STATUS_ALL"
IS
  'This view provides status information of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_PRG"."DEFERRAL_STATUS"
IS
  'Provides deferral status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_PRG"."NATIONAL_STATUS"
IS
  'Provides national status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_PRG"."LOCAL_STATUS"
IS
  'Provides local status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_PRG"."ELIGIBILTY_STATUS"
IS
  'Provides eligibility status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_PRG"."ENTRY_DATE"
IS
  'Created on date of the Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_PRG"."PK_CORD"
IS
  'Primary key of cb_cord.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CB_CORD_STATUS_PRG"
IS
  'This view provides status information of cords (Saved In Progress)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_SUBMT"."DEFERRAL_STATUS"
IS
  'Provides deferral status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_SUBMT"."NATIONAL_STATUS"
IS
  'Provides national status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_SUBMT"."LOCAL_STATUS"
IS
  'Provides local status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_SUBMT"."ELIGIBILTY_STATUS"
IS
  'Provides eligibility status of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_SUBMT"."ENTRY_DATE"
IS
  'Created on date of the Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_CORD_STATUS_SUBMT"."PK_CORD"
IS
  'Primary key of cb_cord.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CB_CORD_STATUS_SUBMT"
IS
  'This view provides status information of cords(Submitted)';
  /
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL" ("CBU_REGISTRY_ID", "PROCESS_DATE", "PROCESSING_TIME", "BACT_CULT_DATE", "FUNG_CULT_DATE", "BACTERIAL_CULTURE", "FUNGAL_CULTURE", "ABO_TYPE", "RH_TYPE", "RHTYPE_OTHER_SPEC", "HMGLBN_TSTNG", "FREEZE_DATE", "FREEZE_TIME", "FUNG_COMMENT", "BACT_COMMENT", "PK_CORD")
AS
  SELECT CORD.CORD_REGISTRY_ID,
    CORD.PRCSNG_START_DATE,
    TO_CHAR(CORD.PRCSNG_START_DATE,'hh24:mi') AS PRCSNG_START_TIME,
    CORD.BACT_CULT_DATE,
    CORD.FUNG_CULT_DATE,
    ERES.F_CODELST_DESC(CORD.FK_CORD_BACT_CUL_RESULT),
    ERES.F_CODELST_DESC(CORD.FK_CORD_FUNGAL_CUL_RESULT),
    ERES.F_CODELST_DESC(CORD.FK_CORD_ABO_BLOOD_TYPE),
    ERES.F_CODELST_DESC(CORD.FK_CORD_RH_TYPE),
    CB_RHTYPE_OTHER_SPEC,
    ERES.F_CODELST_DESC(CORD.HEMOGLOBIN_SCRN),
    CORD.FRZ_DATE,
    TO_CHAR(CORD.FRZ_DATE,'hh24:mi') AS FRZ_TIME,
    CORD.FUNG_COMMENT,
    CORD.BACT_COMMENT,
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."PROCESS_DATE"
IS
  'PRCSNG Start date - comes with ESB message.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."PROCESSING_TIME"
IS
  'This column will store Processing Start Time.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."BACT_CULT_DATE"
IS
  'This Column will store the value of Bacterial Culture Start Date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."FUNG_CULT_DATE"
IS
  'This Column will store the value of Fungal Culture Start Date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."BACTERIAL_CULTURE"
IS
  'Code for bacterial culture result.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."FUNGAL_CULTURE"
IS
  'Code for fungal culture result';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."ABO_TYPE"
IS
  'Code for blood type';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."RH_TYPE"
IS
  'Code for RH type ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."RHTYPE_OTHER_SPEC"
IS
  'Store the description if the RH type choose Other';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."HMGLBN_TSTNG"
IS
  'This column will store Hemoglobin screening code.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."FREEZE_DATE"
IS
  'This column will store CBU freeze date';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."FREEZE_TIME"
IS
  'This column will store Freeze Time';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."FUNG_COMMENT"
IS
  'This Column will store the comment for the Fungal Culture .';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."BACT_COMMENT"
IS
  'This Column will store the comment for the Bacterial Culture.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"."PK_CORD"
IS
  'Primary key of the table CB_CORD';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBU_LAB_SUMMARY_ALL"
IS
  'This view provides basic information of Lab Summary cords';
  /

CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG" ("CBU_REGISTRY_ID", "PROCESS_DATE", "PROCESSING_TIME", "BACT_CULT_DATE", "FUNG_CULT_DATE", "BACTERIAL_CULTURE", "FUNGAL_CULTURE", "ABO_TYPE", "RH_TYPE", "RHTYPE_OTHER_SPEC", "HMGLBN_TSTNG", "FREEZE_DATE", "FREEZE_TIME", "FUNG_COMMENT", "BACT_COMMENT", "PK_CORD")
AS
  SELECT CORD.CORD_REGISTRY_ID,
    CORD.PRCSNG_START_DATE,
    TO_CHAR(CORD.PRCSNG_START_DATE,'hh24:mi') AS PRCSNG_START_TIME,
    CORD.BACT_CULT_DATE,
    CORD.FUNG_CULT_DATE,
    ERES.F_CODELST_DESC(CORD.FK_CORD_BACT_CUL_RESULT),
    ERES.F_CODELST_DESC(CORD.FK_CORD_FUNGAL_CUL_RESULT),
    ERES.F_CODELST_DESC(CORD.FK_CORD_ABO_BLOOD_TYPE),
    ERES.F_CODELST_DESC(CORD.FK_CORD_RH_TYPE),
    CB_RHTYPE_OTHER_SPEC,
    ERES.F_CODELST_DESC(CORD.HEMOGLOBIN_SCRN),
    CORD.FRZ_DATE,
    TO_CHAR(CORD.FRZ_DATE,'hh24:mi') AS FRZ_TIME,
    CORD.FUNG_COMMENT,
    CORD.BACT_COMMENT,
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD
  WHERE CORD.CORD_SEARCHABLE = '0';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."PROCESS_DATE"
IS
  'PRCSNG Start date - comes with ESB message.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."PROCESSING_TIME"
IS
  'This column will store Processing Start Time.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."BACT_CULT_DATE"
IS
  'This Column will store the value of Bacterial Culture Start Date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."FUNG_CULT_DATE"
IS
  'This Column will store the value of Fungal Culture Start Date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."BACTERIAL_CULTURE"
IS
  'Code for bacterial culture result.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."FUNGAL_CULTURE"
IS
  'Code for fungal culture result';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."ABO_TYPE"
IS
  'Code for blood type';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."RH_TYPE"
IS
  'Code for RH type ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."RHTYPE_OTHER_SPEC"
IS
  'Store the description if the RH type choose Other';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."HMGLBN_TSTNG"
IS
  'This column will store Hemoglobin screening code.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."FREEZE_DATE"
IS
  'This column will store CBU freeze date';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."FREEZE_TIME"
IS
  'This column will store Freeze Time';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."FUNG_COMMENT"
IS
  'This Column will store the comment for the Fungal Culture .';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."BACT_COMMENT"
IS
  'This Column will store the comment for the Bacterial Culture.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"."PK_CORD"
IS
  'Primary key of the table CB_CORD';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBU_LAB_SUMMARY_PRG"
IS
  'This view provides basic information of Lab Summary for "Saved In Progress" cords';
  /
  
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT" ("CBU_REGISTRY_ID", "PROCESS_DATE", "PROCESSING_TIME", "BACT_CULT_DATE", "FUNG_CULT_DATE", "BACTERIAL_CULTURE", "FUNGAL_CULTURE", "ABO_TYPE", "RH_TYPE", "RHTYPE_OTHER_SPEC", "HMGLBN_TSTNG", "FREEZE_DATE", "FREEZE_TIME", "FUNG_COMMENT", "BACT_COMMENT", "PK_CORD")
AS
  SELECT CORD.CORD_REGISTRY_ID,
    CORD.PRCSNG_START_DATE,
    TO_CHAR(CORD.PRCSNG_START_DATE,'hh24:mi') AS PRCSNG_START_TIME,
    CORD.BACT_CULT_DATE,
    CORD.FUNG_CULT_DATE,
    ERES.F_CODELST_DESC(CORD.FK_CORD_BACT_CUL_RESULT),
    ERES.F_CODELST_DESC(CORD.FK_CORD_FUNGAL_CUL_RESULT),
    ERES.F_CODELST_DESC(CORD.FK_CORD_ABO_BLOOD_TYPE),
    ERES.F_CODELST_DESC(CORD.FK_CORD_RH_TYPE),
    CB_RHTYPE_OTHER_SPEC,
    ERES.F_CODELST_DESC(CORD.HEMOGLOBIN_SCRN),
    CORD.FRZ_DATE,
    TO_CHAR(CORD.FRZ_DATE,'hh24:mi') AS FRZ_TIME,
    CORD.FUNG_COMMENT,
    CORD.BACT_COMMENT,
    CORD.PK_CORD
  FROM ERES.CB_CORD CORD
  WHERE CORD.CORD_SEARCHABLE = '1';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."PROCESS_DATE"
IS
  'PRCSNG Start date - comes with ESB message.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."PROCESSING_TIME"
IS
  'This column will store Processing Start Time.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."BACT_CULT_DATE"
IS
  'This Column will store the value of Bacterial Culture Start Date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."FUNG_CULT_DATE"
IS
  'This Column will store the value of Fungal Culture Start Date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."BACTERIAL_CULTURE"
IS
  'Code for bacterial culture result.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."FUNGAL_CULTURE"
IS
  'Code for fungal culture result';
  
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."ABO_TYPE"
IS
  'Code for blood type';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."RH_TYPE"
IS
  'Code for RH type ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."RHTYPE_OTHER_SPEC"
IS
  'Store the description if the RH type choose Other';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."HMGLBN_TSTNG"
IS
  'This column will store Hemoglobin screening code.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."FREEZE_DATE"
IS
  'This column will store CBU freeze date';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."FREEZE_TIME"
IS
  'This column will store Freeze Time';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."FUNG_COMMENT"
IS
  'This Column will store the comment for the Fungal Culture .';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."BACT_COMMENT"
IS
  'This Column will store the comment for the Bacterial Culture.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"."PK_CORD"
IS
  'Primary key of the table CB_CORD';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBU_LAB_SUMMARY_SUBMT"
IS
  'This view provides basic information of Lab Summary for "Submitted" cords';
  /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_USER_SESSION_EVENT" ("LOGIN_NAME", "LOGIN_TIME", "LOGOUT_TIME", "URL_PAGE", "USL_URLPARAM", "ENTITY_ID", "ENTITY_TYPE", "ENTITY_IDENTIFIER", "ENTITY_EVENT_CODE")
AS
  SELECT
    (SELECT users.usr_logname
    FROM eres.ER_USER users
    WHERE users.pk_user = usersession.fk_user
    ),
    TO_CHAR(UserSession.LOGIN_TIME,'Mon DD, YYYY HH:MI:SS AM'),
    TO_CHAR(UserSession.LOGOUT_TIME,'Mon DD, YYYY HH:MI:SS AM'),
    USERSESSIONLOG.USL_URL,
    USERSESSIONLOG.USL_URLPARAM,
    USERSESSIONLOG.USL_ENTITY_ID,
    USERSESSIONLOG.USL_ENTITY_TYPE,
    USERSESSIONLOG.USL_ENTITY_IDENTIFIER,
    USERSESSIONLOG.USL_ENTITY_EVENT_CODE
  FROM eres.ER_USERSESSION UserSession ,
    eres.ER_USERSESSIONLOG userSessionLog
  WHERE usersession.pk_usersession = userSessionLog.FK_USERSESSION;
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_EVENT"."LOGIN_NAME"
IS
  'This column store name from which user logged in to the applicaton.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_EVENT"."LOGIN_TIME"
IS
  'This column stores time when user logged in to the applicaton.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_EVENT"."LOGOUT_TIME"
IS
  'This column stores time when user logged out from the applicaton.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_EVENT"."URL_PAGE"
IS
  'This column stores the URL of the page accessed.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_EVENT"."USL_URLPARAM"
IS
  'This column Stores the list of parameters passed to the page.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_EVENT"."ENTITY_ID"
IS
  'This column store the pk of the entity.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_EVENT"."ENTITY_TYPE"
IS
  'This column store the type of the entity.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_EVENT"."ENTITY_IDENTIFIER"
IS
  'This column store the Identifier of the entity.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_EVENT"."ENTITY_EVENT_CODE"
IS
  'This column store the event code of the entity.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_USER_SESSION_EVENT"
IS
  'This view provides information about the events performed by User in session';
  /
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_OPEN_REQ" ("PK_ORDER", "PK_CORD", "CORD_REGISTRY_ID", "LOCAL_STATUS", "NMDP_STATUS", "SITE_ID", "REQUEST_STATUS", "REQUEST_TYPE", "REQUEST_OPEN_DATE", "REQUEST_PRIORITY", "REQUEST_LAST_REVIEWED_BY", "REQUEST_LAST_REVIEWED_DATE", "REQUEST_ASSIGNED_TO", "REQUEST_ASSIGNED_DATE", "RESULT_RECEIVED_DATE", "SCHEDULED_SHIP_DATE", "SCHEDULED_SHIP_TIME", "SCHEDULED_SHIP_DAY", "SHIPPING_COMPANY", "TRACKING_NO", "SHIPMENT_CONFIRM_ON", "SHIPMENT_CONFIRMED_BY", "NMDP_SAMPL_SCH_SHIP_DATE", "NMDP_SAMPL_SCH_SHIP_TIME", "NMDP_SAMPL_SCH_SHIP_DAY", "NMDP_SAMPL_SHIPPING_COMPANY", "NMDP_SAMPL_TRACKING_NO", "SAMPLE_TYPE", "ALIQUOTS_TYPE", "LAB_CODE", "REQUEST_ACTIVE_DATE", "PATIENT_ID", "PATIENT_ENTITY_ID")
AS
  SELECT ORD.PK_ORDER,
    CRD.PK_CORD,
    CRD.CORD_REGISTRY_ID,
    ERES.F_GET_CBU_STATUS(CRD.FK_CORD_CBU_STATUS) LOCAL_STATUS,
    ERES.F_GET_CBU_STATUS(CRD.CORD_NMDP_STATUS) NMDP_STATUS,
    SITE.SITE_ID,
    ERES.F_CODELST_DESC(ORD.ORDER_STATUS) REQUEST_STATUS,
    CASE
      WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
      AND ORD.ORDER_SAMPLE_AT_LAB             ='Y'
      THEN 'CT - Sample at Lab'
      WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
      AND ORD.ORDER_SAMPLE_AT_LAB             ='N'
      THEN 'CT - Ship Sample'
      ELSE ERES.F_CODELST_DESC(ORD.ORDER_TYPE)
    END REQUEST_TYPE,
    TO_CHAR(HDR.ORDER_OPEN_DATE, 'Mon DD, YYYY') REQUEST_OPEN_DATE,
    ERES.F_CODELST_DESC(ORD.ORDER_PRIORITY) REQUEST_PRIORITY,
    ERES.F_GET_USER_NAME(ORD.ORDER_LAST_VIEWED_BY) REQUEST_LAST_REVIEWED_BY,
    TO_CHAR(ORD.ORDER_LAST_VIEWED_DATE, 'Mon DD, YYYY') REQUEST_LAST_REVIEWED_DATE,
    ERES.F_GET_USER_NAME(ORD.ASSIGNED_TO) REQUEST_ASSIGNED_TO,
    TO_CHAR(ORD.ORDER_ASSIGNED_DATE,'Mon DD, YYYY') REQUEST_ASSIGNED_DATE,
    TO_CHAR(ORD.RESULT_REC_DATE,'Mon DD, YYYY') RESULT_RECEIVED_DATE,
    CBUSHIP.SCHEDULED_SHIP_DATE,
    CBUSHIP.SCHEDULED_SHIP_TIME,
    CBUSHIP.SCHEDULED_SHIP_DAY,
    CBUSHIP.SHIPPING_COMPANY,
    CBUSHIP.TRACKING_NO,
    CBUSHIP.SHIPMENT_CONFIRM_ON,
    CBUSHIP.SHIPMENT_CONFIRMED_BY,
    NMDPSHIP.NMDP_SAMPL_SCH_SHIP_DATE,
    NMDPSHIP.NMDP_SAMPL_SCH_SHIP_TIME,
    NMDPSHIP.NMDP_SAMPL_SCH_SHIP_DAY,
    NMDPSHIP.NMDP_SAMPL_SHIPPING_COMPANY,
    NMDPSHIP.NMDP_SAMPL_TRACKING_NO,
    ERES.F_CODELST_DESC(ORD.FK_SAMPLE_TYPE_AVAIL) SAMPLE_TYPE,
    ERES.F_CODELST_DESC(ORD.FK_ALIQUOTS_TYPE) ALIQUOTS_TYPE,
    ERES.F_CODELST_DESC(ORD.LAB_CODE) LAB_CODE,
    TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(HDR.ORDER_OPEN_DATE,'Mon DD, YYYY'),'Mon DD, YYYY') REQUEST_ACTIVE_DATE,
    RECIPIENT_INFO.RECEIPANT_ID PATIENT_ID,
    RECIPIENT_INFO.PK_RECEIPANT PATIENT_ENTITY_ID
  FROM ERES.ER_ORDER ORD
  INNER JOIN ERES.ER_ORDER_HEADER HDR
  ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER)
  INNER JOIN ERES.CB_CORD CRD
  ON (CRD.PK_CORD=HDR.ORDER_ENTITYID)
  INNER JOIN ERES.ER_SITE SITE
  ON (SITE.PK_SITE=CRD.FK_CBB_ID)
  LEFT OUTER JOIN
    (SELECT CBUSHIPMENT.PK_SHIPMENT,
      CBUSHIPMENT.FK_ORDER_ID ORDERID,
      TO_CHAR(CBUSHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY') SCHEDULED_SHIP_DATE,
      CBUSHIPMENT.SCH_SHIPMENT_TIME SCHEDULED_SHIP_TIME,
      TO_CHAR(CBUSHIPMENT.SCH_SHIPMENT_DATE,'Day') SCHEDULED_SHIP_DAY,
      ERES.F_CODELST_DESC(CBUSHIPMENT.FK_SHIP_COMPANY_ID) SHIPPING_COMPANY,
      CBUSHIPMENT.SHIPMENT_TRACKING_NO TRACKING_NO,
      TO_CHAR(CBUSHIPMENT.SHIPMENT_DATE,'Mon DD, YYYY') SHIPMENT_CONFIRM_ON,
      ERES.F_GETUSER(CBUSHIPMENT.SHIPMENT_CONFIRM_BY) SHIPMENT_CONFIRMED_BY
    FROM ERES.CB_SHIPMENT CBUSHIPMENT
    WHERE CBUSHIPMENT.FK_SHIPMENT_TYPE=
      (SELECT PK_CODELST
      FROM ERES.ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='CBU'
      )
    ) CBUSHIP
  ON (CBUSHIP.ORDERID=ORD.PK_ORDER)
  LEFT OUTER JOIN
    (SELECT NMDPSHIPMENT.PK_SHIPMENT,
      NMDPSHIPMENT.FK_ORDER_ID ORDERID,
      TO_CHAR(NMDPSHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY') NMDP_SAMPL_SCH_SHIP_DATE,
      NMDPSHIPMENT.SCH_SHIPMENT_TIME NMDP_SAMPL_SCH_SHIP_TIME,
      TO_CHAR(NMDPSHIPMENT.SCH_SHIPMENT_DATE,'Day') NMDP_SAMPL_SCH_SHIP_DAY,
      ERES.F_CODELST_DESC(NMDPSHIPMENT.FK_SHIP_COMPANY_ID) NMDP_SAMPL_SHIPPING_COMPANY,
      NMDPSHIPMENT.SHIPMENT_TRACKING_NO NMDP_SAMPL_TRACKING_NO
    FROM ERES.CB_SHIPMENT NMDPSHIPMENT
    WHERE NMDPSHIPMENT.FK_SHIPMENT_TYPE=
      (SELECT PK_CODELST
      FROM ERES.ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='NMDP'
      )
    ) NMDPSHIP
  ON (NMDPSHIP.ORDERID=ORD.PK_ORDER)
  LEFT OUTER JOIN ERES.ER_ORDER_RECEIPANT_INFO ORDER_RECIPIENT_INFO
  ON (ORDER_RECIPIENT_INFO.FK_ORDER_ID=ORD.PK_ORDER)
  LEFT OUTER JOIN ERES.CB_RECEIPANT_INFO RECIPIENT_INFO
  ON (RECIPIENT_INFO.PK_RECEIPANT = ORDER_RECIPIENT_INFO.FK_RECEIPANT)
  WHERE ORD.ORDER_STATUS         <>
    (SELECT PK_CODELST
    FROM ERES.ER_CODELST
    WHERE CODELST_TYPE='order_status'
    AND CODELST_SUBTYP='resolved'
    )
  AND ORD.ORDER_STATUS<>
    (SELECT PK_CODELST
    FROM ERES.ER_CODELST
    WHERE CODELST_TYPE='order_status'
    AND CODELST_SUBTYP='close_ordr'
    );
	/
	
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."PK_ORDER"
IS
  'Order id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."PK_CORD"
IS
  'Cord Id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."CORD_REGISTRY_ID"
IS
  'Registry Id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."LOCAL_STATUS"
IS
  'Local Status of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."NMDP_STATUS"
IS
  'NMDP Status of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."SITE_ID"
IS
  'Site Id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."REQUEST_STATUS"
IS
  'Status of request for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."REQUEST_TYPE"
IS
  'Type of the request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."REQUEST_OPEN_DATE"
IS
  'Date on which request open';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."REQUEST_PRIORITY"
IS
  'Priority of the request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."REQUEST_LAST_REVIEWED_BY"
IS
  'Name of person who reviewd the request last';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."REQUEST_LAST_REVIEWED_DATE"
IS
  'Last review date of request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."REQUEST_ASSIGNED_TO"
IS
  'Name of person to whom request is assigned to';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."REQUEST_ASSIGNED_DATE"
IS
  'Assign date of request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."RESULT_RECEIVED_DATE"
IS
  'Received date of result';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."SCHEDULED_SHIP_DATE"
IS
  'Scheduled shipment date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."SCHEDULED_SHIP_TIME"
IS
  'Scheduled shipment time.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."SCHEDULED_SHIP_DAY"
IS
  'Scheduled shipment day.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."SHIPPING_COMPANY"
IS
  'Name of the shipment company.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."TRACKING_NO"
IS
  'Tracking number.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."SHIPMENT_CONFIRM_ON"
IS
  'Date on which shipment is confirmed.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."SHIPMENT_CONFIRMED_BY"
IS
  'Name of person who has confirmed shipment.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."NMDP_SAMPL_SCH_SHIP_DATE"
IS
  'NMDP Sample Scheduled shipment date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."NMDP_SAMPL_SCH_SHIP_TIME"
IS
  'NMDP Sample Scheduled shipment time.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."NMDP_SAMPL_SCH_SHIP_DAY"
IS
  'NMDP Sample Scheduled shipment day.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."NMDP_SAMPL_SHIPPING_COMPANY"
IS
  'NMDP Sample Scheduled name of the shipment company.';
  
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."NMDP_SAMPL_TRACKING_NO"
IS
  'NMDP Sample Scheduled tracking number.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."SAMPLE_TYPE"
IS
  'Type of the sample';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."ALIQUOTS_TYPE"
IS
  'Aliquots type';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."LAB_CODE"
IS
  'Lab code';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."REQUEST_ACTIVE_DATE"
IS
  'Active date of the request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."PATIENT_ID"
IS
  'Patient ID';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_OPEN_REQ"."PATIENT_ENTITY_ID"
IS
  'Patient entity ID';
  /
  COMMENT ON TABLE "VDA"."ETVDA_OPEN_REQ"
IS
  'This view provides basic information of Order for Open Request';
  /
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_TASKS_DATA" ("CORD_REGISTRY_ID", "REQUEST_TYPE", "REQUEST_STATUS", "TASK_DESC", "TASK_COMPLETEDBY", "TASK_COMPLETED_ON", "FK_ENTITY", "TASK_COMPLETEFLAG", "PK_ORDER", "PK_CORD")
AS
  SELECT CRD.CORD_REGISTRY_ID,
    CASE
      WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
      AND ORD.ORDER_SAMPLE_AT_LAB             ='Y'
      THEN 'CT - Sample at Lab'
      WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
      AND ORD.ORDER_SAMPLE_AT_LAB             ='N'
      THEN 'CT - Ship Sample'
      ELSE ERES.F_CODELST_DESC(ORD.ORDER_TYPE)
    END REQUEST_TYPE,
    ERES.F_CODELST_DESC(ORD.ORDER_STATUS) REQUEST_STATUS,
    CASE
      WHEN TASK_DATA.ACTIVITY_NAME    ='cbuavailconfbar'
      AND (TASK_DATA.TASK_COMPLETEFLAG=1
      OR ORD.CBU_AVAIL_CONFIRM_FLAG   ='Y')
      THEN TASK_DATA.ACTIVITY_DESC
        ||' Task Completed'
      WHEN TASK_DATA.ACTIVITY_NAME      ='reqclincinfobar'
      AND (TASK_DATA.TASK_COMPLETEFLAG  =1
      OR ORD.COMPLETE_REQ_INFO_TASK_FLAG='Y')
      THEN TASK_DATA.ACTIVITY_DESC
        ||' Task Completed'
      WHEN TASK_DATA.ACTIVITY_NAME    ='finalcbureviewbar'
      AND (TASK_DATA.TASK_COMPLETEFLAG=1
      OR ORD.FINAL_REVIEW_TASK_FLAG   ='Y')
      THEN TASK_DATA.ACTIVITY_DESC
        ||' Task Completed'
      WHEN TASK_DATA.ACTIVITY_NAME    ='additityingandtest'
      AND (TASK_DATA.TASK_COMPLETEFLAG=1
      OR ORD.ADDITI_TYPING_FLAG       ='Y')
      THEN TASK_DATA.ACTIVITY_DESC
        ||' Task Completed'
      WHEN TASK_DATA.ACTIVITY_NAME    ='ctshipmentbar'
      AND (TASK_DATA.TASK_COMPLETEFLAG=1
      OR ORD.SHIPMENT_SCH_FLAG        ='Y')
      THEN TASK_DATA.ACTIVITY_DESC
        ||' Task Completed'
      WHEN TASK_DATA.ACTIVITY_NAME    ='cbushipmentbar'
      AND (TASK_DATA.TASK_COMPLETEFLAG=1
      OR ORD.CORD_SHIPPED_FLAG        ='Y')
      THEN TASK_DATA.ACTIVITY_DESC
        ||' Task Completed'
      WHEN TASK_DATA.ACTIVITY_NAME    ='ctresolutionbar'
      AND (TASK_DATA.TASK_COMPLETEFLAG=1
      OR ORD.ORDER_ACK_FLAG           ='Y')
      THEN TASK_DATA.ACTIVITY_DESC
        ||' Task Completed'
      WHEN TASK_DATA.ACTIVITY_NAME    ='shipsamplebar'
      AND (TASK_DATA.TASK_COMPLETEFLAG=1
      OR ORD.NMDP_SAMPLE_SHIPPED_FLAG ='Y')
      THEN TASK_DATA.ACTIVITY_DESC
        ||' Task Completed'
      WHEN TASK_DATA.ACTIVITY_NAME    ='genpackageslipbar'
      AND (TASK_DATA.TASK_COMPLETEFLAG=1
      OR ORD.PACKAGE_SLIP_FLAG        ='Y')
      AND ORD.ORDER_TYPE              =
        (SELECT PK_CODELST
        FROM ERES.ER_CODELST
        WHERE CODELST_TYPE='order_type'
        AND CODELST_SUBTYP='CT'
        )
      THEN TASK_DATA.ACTIVITY_DESC
        ||' Task Completed'
      WHEN TASK_DATA.ACTIVITY_NAME    ='genpackageslipbar'
      AND (TASK_DATA.TASK_COMPLETEFLAG=1
      OR ORD.PACKAGE_SLIP_FLAG        ='Y')
      AND ORD.ORDER_TYPE              =
        (SELECT PK_CODELST
        FROM ERES.ER_CODELST
        WHERE CODELST_TYPE='order_type'
        AND CODELST_SUBTYP='OR'
        )
      THEN 'ShipPacket Task Completed'
      ELSE TASK_DATA.ACTIVITY_DESC
        ||' Task not Completed'
    END TASK_DESC,
    TASK_DATA.TASK_COMPLETEDBY,
    TASK_DATA.TASK_COMPLETED_ON,
    TASK_DATA.FK_ENTITY,
    TASK_DATA.TASK_COMPLETEFLAG,
    ORD.PK_ORDER,
    CRD.PK_CORD
  FROM ERES.ER_ORDER ORD
  INNER JOIN ERES.ER_ORDER_HEADER HDR
  ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER)
  INNER JOIN ERES.CB_CORD CRD
  ON (CRD.PK_CORD=HDR.ORDER_ENTITYID)
  LEFT OUTER JOIN
    (SELECT TASK.FK_ENTITY,
      TASK.TASK_COMPLETEFLAG,
      ACTVITY.ACTIVITY_DESC,
      ACTVITY.ACTIVITY_NAME,
      CASE
        WHEN TASK.TASK_COMPLETEFLAG =1
        AND TASK.TASK_USERORROLEID IS NOT NULL
        AND TASK.TASK_USERORROLEID <>0
        THEN ERES.F_GETUSER(TASK.TASK_USERORROLEID)
        WHEN TASK.TASK_COMPLETEFLAG=1
        AND TASK.TASK_USERORROLEID =0
        THEN 'System'
        ELSE ''
      END TASK_COMPLETEDBY,
      TO_CHAR(TASK.COMPLETEDON,'Mon DD, YYYY') TASK_COMPLETED_ON,
      WF_ACTIVITY.PK_WORKFLOWACTIVITY TASK_SEQ
    FROM ERES.TASK TASK,
      ERES.WORKFLOW_ACTIVITY WF_ACTIVITY,
      ERES.ACTIVITY ACTVITY
    WHERE TASK.FK_WORKFLOWACTIVITY=WF_ACTIVITY.PK_WORKFLOWACTIVITY
    AND WF_ACTIVITY.FK_ACTIVITYID =ACTVITY.PK_ACTIVITY
    ) TASK_DATA
  ON (TASK_DATA.FK_ENTITY=ORD.PK_ORDER)
  ORDER BY ORD.PK_ORDER,
    TASK_DATA.TASK_SEQ;
	/
	
  COMMENT ON COLUMN "VDA"."ETVDA_TASKS_DATA"."CORD_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_TASKS_DATA"."REQUEST_TYPE"
IS
  'This column store type of request.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_TASKS_DATA"."REQUEST_STATUS"
IS
  'This column store status of request.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_TASKS_DATA"."TASK_DESC"
IS
  'This column store description of the task.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_TASKS_DATA"."TASK_COMPLETEDBY"
IS
  'This column store name of the person task completed by.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_TASKS_DATA"."TASK_COMPLETED_ON"
IS
  'This column store date on which task completed.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_TASKS_DATA"."FK_ENTITY"
IS
  'This column store entity Id.';
  
  COMMENT ON COLUMN "VDA"."ETVDA_TASKS_DATA"."TASK_COMPLETEFLAG"
IS
  'This column store task is completed or not.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_TASKS_DATA"."PK_ORDER"
IS
  'This column store Order Id.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_TASKS_DATA"."PK_CORD"
IS
  'This column store Cord Id.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_TASKS_DATA"
IS
  'This view provides basic information Task';
  /
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ENT_STUS_RSN" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_CBU_ID", "ELIGIBLE_STATUS", "COLLECTION_DATE", "SHIPDATE", "SHIPTYPE", "PK_CORD")
AS
  SELECT SITE.SITE_ID,
    C.CORD_REGISTRY_ID,
    C.CORD_LOCAL_CBU_ID,
    eres.f_codelst_desc(c.FK_CORD_CBU_ELIGIBLE_STATUS),
    spec.SPEC_COLLECTION_DATE,
    ship.SCH_SHIPMENT_DATE,
    eres.f_codelst_desc( ship.fk_shipment_type),
    c.pk_cord
  FROM eres.CB_CORD C
  LEFT JOIN eres.er_specimen spec
  ON c.FK_SPECIMEN_ID=spec.PK_SPECIMEN
  LEFT JOIN eres.er_site site
  ON C.fk_cbb_id=site.PK_SITE
  LEFT OUTER JOIN
    (SELECT REC.RECEIPANT_ID PATIENTID,
      HDR.ORDER_ENTITYID CORDID,
      ORD.PK_ORDER ORDERID,
      ORD.ORDER_TYPE ORDER_TYPE,
      ORD.ORDER_SAMPLE_AT_LAB ORDER_SAMPLE_AT_LAB
    FROM eres.CB_RECEIPANT_INFO REC,
      eres.ER_ORDER_RECEIPANT_INFO PRE,
      eres.ER_ORDER ORD,
      eres.ER_ORDER_HEADER HDR
    WHERE ORD.FK_ORDER_HEADER =HDR.PK_ORDER_HEADER
    AND ORD.PK_ORDER          =PRE.FK_ORDER_ID
    AND REC.PK_RECEIPANT      =PRE.FK_RECEIPANT
    AND ORD.ORDER_STATUS NOT IN
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type  ='order_status'
      AND codelst_subtyp IN('close_ordr','resolved')
      )
    AND ORD.ORDER_TYPE=
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='order_type'
      AND codelst_subtyp ='OR'
      )
    ) PAT
  ON(PAT.CORDID=C.PK_CORD)
  LEFT JOIN eres.CB_SHIPMENT ship
  ON ship.fk_order_id=PAT.ORDERID
  AND C.PK_CORD      =PAT.CORDID;
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ENT_STUS_RSN"."CBB_ID"
IS
  'This column stores an Identification number for the site';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ENT_STUS_RSN"."CBU_REGISTRY_ID"
IS
  'This column stores Registry ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ENT_STUS_RSN"."CBU_LOCAL_CBU_ID"
IS
  'This column stores Local ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ENT_STUS_RSN"."ELIGIBLE_STATUS"
IS
  'This column stores eligibility status of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ENT_STUS_RSN"."COLLECTION_DATE"
IS
  'This column stores the Collection Date of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ENT_STUS_RSN"."SHIPDATE"
IS
  'This column stores the OR shipment Date';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ENT_STUS_RSN"."SHIPTYPE"
IS
  'This column stores the type of shipment';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ENT_STUS_RSN"."PK_CORD"
IS
  'Cord Id of the cord.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_ENT_STUS_RSN"
IS
  'This view provides eligibility status of the cord shipped within particular date';
  /
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_RESOLVED_REQ" ("PK_ORDER", "PK_CORD", "CORD_REGISTRY_ID", "LOCAL_STATUS", "NMDP_STATUS", "SITE_ID", "REQUEST_STATUS", "REQUEST_TYPE", "REQUEST_OPEN_DATE", "REQUEST_PRIORITY", "REQUEST_LAST_REVIEWED_BY", "REQUEST_LAST_REVIEWED_DATE", "REQUEST_ASSIGNED_TO", "REQUEST_ASSIGNED_DATE", "RESULT_RECEIVED_DATE", "SCHEDULED_SHIP_DATE", "SCHEDULED_SHIP_TIME", "SCHEDULED_SHIP_DAY", "SHIPPING_COMPANY", "TRACKING_NO", "SHIPMENT_CONFIRM_ON", "SHIPMENT_CONFIRMED_BY", "NMDP_SAMPL_SCH_SHIP_DATE", "NMDP_SAMPL_SCH_SHIP_TIME", "NMDP_SAMPL_SCH_SHIP_DAY", "NMDP_SAMPL_SHIPPING_COMPANY", "NMDP_SAMPL_TRACKING_NO", "SAMPLE_TYPE", "ALIQUOTS_TYPE", "LAB_CODE", "REQUEST_ACTIVE_DATE", "PATIENT_ID", "PATIENT_ENTITY_ID", "TC_RESOLUTION", "CBB_RESOLUTION", "CLOSE_REQUEST_REASON")
AS
  SELECT ORD.PK_ORDER,
    CRD.PK_CORD,
    CRD.CORD_REGISTRY_ID,
    ERES.F_GET_CBU_STATUS(CRD.FK_CORD_CBU_STATUS) LOCAL_STATUS,
    ERES.F_GET_CBU_STATUS(CRD.CORD_NMDP_STATUS) NMDP_STATUS,
    SITE.SITE_ID,
    ERES.F_CODELST_DESC(ORD.ORDER_STATUS) REQUEST_STATUS,
    CASE
      WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
      AND ORD.ORDER_SAMPLE_AT_LAB             ='Y'
      THEN 'CT - Sample at Lab'
      WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
      AND ORD.ORDER_SAMPLE_AT_LAB             ='N'
      THEN 'CT - Ship Sample'
      ELSE ERES.F_CODELST_DESC(ORD.ORDER_TYPE)
    END REQUEST_TYPE,
    TO_CHAR(HDR.ORDER_OPEN_DATE, 'Mon DD, YYYY') REQUEST_OPEN_DATE,
    ERES.F_CODELST_DESC(ORD.ORDER_PRIORITY) REQUEST_PRIORITY,
    ERES.F_GET_USER_NAME(ORD.ORDER_LAST_VIEWED_BY) REQUEST_LAST_REVIEWED_BY,
    TO_CHAR(ORD.ORDER_LAST_VIEWED_DATE, 'Mon DD, YYYY') REQUEST_LAST_REVIEWED_DATE,
    ERES.F_GET_USER_NAME(ORD.ASSIGNED_TO) REQUEST_ASSIGNED_TO,
    TO_CHAR(ORD.ORDER_ASSIGNED_DATE,'Mon DD, YYYY') REQUEST_ASSIGNED_DATE,
    TO_CHAR(ORD.RESULT_REC_DATE,'Mon DD, YYYY') RESULT_RECEIVED_DATE,
    CBUSHIP.SCHEDULED_SHIP_DATE,
    CBUSHIP.SCHEDULED_SHIP_TIME,
    CBUSHIP.SCHEDULED_SHIP_DAY,
    CBUSHIP.SHIPPING_COMPANY,
    CBUSHIP.TRACKING_NO,
    CBUSHIP.SHIPMENT_CONFIRM_ON,
    CBUSHIP.SHIPMENT_CONFIRMED_BY,
    NMDPSHIP.NMDP_SAMPL_SCH_SHIP_DATE,
    NMDPSHIP.NMDP_SAMPL_SCH_SHIP_TIME,
    NMDPSHIP.NMDP_SAMPL_SCH_SHIP_DAY,
    NMDPSHIP.NMDP_SAMPL_SHIPPING_COMPANY,
    NMDPSHIP.NMDP_SAMPL_TRACKING_NO,
    ERES.F_CODELST_DESC(ORD.FK_SAMPLE_TYPE_AVAIL) SAMPLE_TYPE,
    ERES.F_CODELST_DESC(ORD.FK_ALIQUOTS_TYPE) ALIQUOTS_TYPE,
    ERES.F_CODELST_DESC(ORD.LAB_CODE) LAB_CODE,
    TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(HDR.ORDER_OPEN_DATE,'Mon DD, YYYY'),'Mon DD, YYYY') REQUEST_ACTIVE_DATE,
    RECIPIENT_INFO.RECEIPANT_ID PATIENT_ID,
    RECIPIENT_INFO.PK_RECEIPANT PATIENT_ENTITY_ID,
    ERES.F_GET_CBU_STATUS(ORD.FK_ORDER_RESOL_BY_TC) TC_RESOLUTION,
    ERES.F_GET_CBU_STATUS(ORD.FK_ORDER_RESOL_BY_CBB) CBB_RESOLUTION,
    ERES.F_GET_CBU_STATUS(HDR.ORDER_CLOSE_REASON) CLOSE_REQUEST_REASON
  FROM ERES.ER_ORDER ORD
  INNER JOIN ERES.ER_ORDER_HEADER HDR
  ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER)
  INNER JOIN ERES.CB_CORD CRD
  ON (CRD.PK_CORD=HDR.ORDER_ENTITYID)
  INNER JOIN ERES.ER_SITE SITE
  ON (SITE.PK_SITE=CRD.FK_CBB_ID)
  LEFT OUTER JOIN
    (SELECT CBUSHIPMENT.PK_SHIPMENT,
      CBUSHIPMENT.FK_ORDER_ID ORDERID,
      TO_CHAR(CBUSHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY') SCHEDULED_SHIP_DATE,
      CBUSHIPMENT.SCH_SHIPMENT_TIME SCHEDULED_SHIP_TIME,
      TO_CHAR(CBUSHIPMENT.SCH_SHIPMENT_DATE,'Day') SCHEDULED_SHIP_DAY,
      ERES.F_CODELST_DESC(CBUSHIPMENT.FK_SHIP_COMPANY_ID) SHIPPING_COMPANY,
      CBUSHIPMENT.SHIPMENT_TRACKING_NO TRACKING_NO,
      TO_CHAR(CBUSHIPMENT.SHIPMENT_DATE,'Mon DD, YYYY') SHIPMENT_CONFIRM_ON,
      ERES.F_GETUSER(CBUSHIPMENT.SHIPMENT_CONFIRM_BY) SHIPMENT_CONFIRMED_BY
    FROM ERES.CB_SHIPMENT CBUSHIPMENT
    WHERE CBUSHIPMENT.FK_SHIPMENT_TYPE=
      (SELECT PK_CODELST
      FROM ERES.ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='CBU'
      )
    ) CBUSHIP
  ON (CBUSHIP.ORDERID=ORD.PK_ORDER)
  LEFT OUTER JOIN
    (SELECT NMDPSHIPMENT.PK_SHIPMENT,
      NMDPSHIPMENT.FK_ORDER_ID ORDERID,
      TO_CHAR(NMDPSHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY') NMDP_SAMPL_SCH_SHIP_DATE,
      NMDPSHIPMENT.SCH_SHIPMENT_TIME NMDP_SAMPL_SCH_SHIP_TIME,
      TO_CHAR(NMDPSHIPMENT.SCH_SHIPMENT_DATE,'Day') NMDP_SAMPL_SCH_SHIP_DAY,
      ERES.F_CODELST_DESC(NMDPSHIPMENT.FK_SHIP_COMPANY_ID) NMDP_SAMPL_SHIPPING_COMPANY,
      NMDPSHIPMENT.SHIPMENT_TRACKING_NO NMDP_SAMPL_TRACKING_NO
    FROM ERES.CB_SHIPMENT NMDPSHIPMENT
    WHERE NMDPSHIPMENT.FK_SHIPMENT_TYPE=
      (SELECT PK_CODELST
      FROM ERES.ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='NMDP'
      )
    ) NMDPSHIP
  ON (NMDPSHIP.ORDERID=ORD.PK_ORDER)
  LEFT OUTER JOIN ERES.ER_ORDER_RECEIPANT_INFO ORDER_RECIPIENT_INFO
  ON (ORDER_RECIPIENT_INFO.FK_ORDER_ID=ORD.PK_ORDER)
  LEFT OUTER JOIN ERES.CB_RECEIPANT_INFO RECIPIENT_INFO
  ON (RECIPIENT_INFO.PK_RECEIPANT = ORDER_RECIPIENT_INFO.FK_RECEIPANT)
  WHERE ORD.ORDER_STATUS          =
    (SELECT PK_CODELST
    FROM ERES.ER_CODELST
    WHERE CODELST_TYPE='order_status'
    AND CODELST_SUBTYP='resolved'
    );
	/
	
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."PK_ORDER"
IS
  'This column store Order Id.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."PK_CORD"
IS
  'This column store Cord Id.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."CORD_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."LOCAL_STATUS"
IS
  'Local Status of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."NMDP_STATUS"
IS
  'NMDP Status of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."SITE_ID"
IS
  'Site Id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."REQUEST_STATUS"
IS
  'This column store status of request.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."REQUEST_TYPE"
IS
  'This column store type of request.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."REQUEST_OPEN_DATE"
IS
  'Date on which request open';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."REQUEST_PRIORITY"
IS
  'Priority of the request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."REQUEST_LAST_REVIEWED_BY"
IS
  'Name of person who reviewed the request last';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."REQUEST_LAST_REVIEWED_DATE"
IS
  'Last review date of request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."REQUEST_ASSIGNED_TO"
IS
  'Name of person to whom request is assigned to';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."REQUEST_ASSIGNED_DATE"
IS
  'Assign date of request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."RESULT_RECEIVED_DATE"
IS
  'Received date of result';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."SCHEDULED_SHIP_DATE"
IS
  'Scheduled shipment date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."SCHEDULED_SHIP_TIME"
IS
  'Scheduled shipment time.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."SCHEDULED_SHIP_DAY"
IS
  'Scheduled shipment day.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."SHIPPING_COMPANY"
IS
  'Name of the shipment company.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."TRACKING_NO"
IS
  'Tracking number.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."SHIPMENT_CONFIRM_ON"
IS
  'Date on which shipment is confirmed.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."SHIPMENT_CONFIRMED_BY"
IS
  'Name of person who has confirmed shipment.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."NMDP_SAMPL_SCH_SHIP_DATE"
IS
  'NMDP Sample Scheduled shipment date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."NMDP_SAMPL_SCH_SHIP_TIME"
IS
  'NMDP Sample Scheduled shipment time.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."NMDP_SAMPL_SCH_SHIP_DAY"
IS
  'NMDP Sample Scheduled shipment day.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."NMDP_SAMPL_SHIPPING_COMPANY"
IS
  'NMDP Sample Scheduled name of the shipment company.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."NMDP_SAMPL_TRACKING_NO"
IS
  'NMDP Sample Scheduled tracking number.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."SAMPLE_TYPE"
IS
  'Type of the sample';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."ALIQUOTS_TYPE"
IS
  'Aliquots type';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."LAB_CODE"
IS
  'Lab code';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."REQUEST_ACTIVE_DATE"
IS
  'Active date of the request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."PATIENT_ID"
IS
  'Patient ID';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."PATIENT_ENTITY_ID"
IS
  'Patient entity ID';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."TC_RESOLUTION"
IS
  'Transplant center resolution.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."CBB_RESOLUTION"
IS
  'CBB resolution.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_RESOLVED_REQ"."CLOSE_REQUEST_REASON"
IS
  'Close request reason.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_RESOLVED_REQ"
IS
  'This view provides basic information of Order for resolved Request';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_COMPLETED_REQ" ("PK_ORDER", "PK_CORD", "CORD_REGISTRY_ID", "LOCAL_STATUS", "NMDP_STATUS", "SITE_ID", "REQUEST_STATUS", "REQUEST_TYPE", "REQUEST_OPEN_DATE", "REQUEST_PRIORITY", "REQUEST_LAST_REVIEWED_BY", "REQUEST_LAST_REVIEWED_DATE", "REQUEST_ASSIGNED_TO", "REQUEST_ASSIGNED_DATE", "RESULT_RECEIVED_DATE", "SCHEDULED_SHIP_DATE", "SCHEDULED_SHIP_TIME", "SCHEDULED_SHIP_DAY", "SHIPPING_COMPANY", "TRACKING_NO", "SHIPMENT_CONFIRM_ON", "SHIPMENT_CONFIRMED_BY", "NMDP_SAMPL_SCH_SHIP_DATE", "NMDP_SAMPL_SCH_SHIP_TIME", "NMDP_SAMPL_SCH_SHIP_DAY", "NMDP_SAMPL_SHIPPING_COMPANY", "NMDP_SAMPL_TRACKING_NO", "SAMPLE_TYPE", "ALIQUOTS_TYPE", "LAB_CODE", "REQUEST_ACTIVE_DATE", "PATIENT_ID", "PATIENT_ENTITY_ID", "TC_RESOLUTION", "CBB_RESOLUTION", "CLOSE_REQUEST_REASON", "REQUEST_ACK_BY", "REQUEST_ACK_DATE")
AS
  SELECT ORD.PK_ORDER,
    CRD.PK_CORD,
    CRD.CORD_REGISTRY_ID,
    ERES.F_GET_CBU_STATUS(CRD.FK_CORD_CBU_STATUS) LOCAL_STATUS,
    ERES.F_GET_CBU_STATUS(CRD.CORD_NMDP_STATUS) NMDP_STATUS,
    SITE.SITE_ID,
    ERES.F_CODELST_DESC(ORD.ORDER_STATUS) REQUEST_STATUS,
    CASE
      WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
      AND ORD.ORDER_SAMPLE_AT_LAB             ='Y'
      THEN 'CT - Sample at Lab'
      WHEN ERES.F_CODELST_DESC(ORD.ORDER_TYPE)='CT'
      AND ORD.ORDER_SAMPLE_AT_LAB             ='N'
      THEN 'CT - Ship Sample'
      ELSE ERES.F_CODELST_DESC(ORD.ORDER_TYPE)
    END REQUEST_TYPE,
    TO_CHAR(HDR.ORDER_OPEN_DATE, 'Mon DD, YYYY') REQUEST_OPEN_DATE,
    ERES.F_CODELST_DESC(ORD.ORDER_PRIORITY) REQUEST_PRIORITY,
    ERES.F_GET_USER_NAME(ORD.ORDER_LAST_VIEWED_BY) REQUEST_LAST_REVIEWED_BY,
    TO_CHAR(ORD.ORDER_LAST_VIEWED_DATE, 'Mon DD, YYYY') REQUEST_LAST_REVIEWED_DATE,
    ERES.F_GET_USER_NAME(ORD.ASSIGNED_TO) REQUEST_ASSIGNED_TO,
    TO_CHAR(ORD.ORDER_ASSIGNED_DATE,'Mon DD, YYYY') REQUEST_ASSIGNED_DATE,
    TO_CHAR(ORD.RESULT_REC_DATE,'Mon DD, YYYY') RESULT_RECEIVED_DATE,
    CBUSHIP.SCHEDULED_SHIP_DATE,
    CBUSHIP.SCHEDULED_SHIP_TIME,
    CBUSHIP.SCHEDULED_SHIP_DAY,
    CBUSHIP.SHIPPING_COMPANY,
    CBUSHIP.TRACKING_NO,
    CBUSHIP.SHIPMENT_CONFIRM_ON,
    CBUSHIP.SHIPMENT_CONFIRMED_BY,
    NMDPSHIP.NMDP_SAMPL_SCH_SHIP_DATE,
    NMDPSHIP.NMDP_SAMPL_SCH_SHIP_TIME,
    NMDPSHIP.NMDP_SAMPL_SCH_SHIP_DAY,
    NMDPSHIP.NMDP_SAMPL_SHIPPING_COMPANY,
    NMDPSHIP.NMDP_SAMPL_TRACKING_NO,
    ERES.F_CODELST_DESC(ORD.FK_SAMPLE_TYPE_AVAIL) SAMPLE_TYPE,
    ERES.F_CODELST_DESC(ORD.FK_ALIQUOTS_TYPE) ALIQUOTS_TYPE,
    ERES.F_CODELST_DESC(ORD.LAB_CODE) LAB_CODE,
    TO_DATE(TO_CHAR(SYSDATE,'Mon DD, YYYY'),'Mon DD, YYYY')-TO_DATE(TO_CHAR(HDR.ORDER_OPEN_DATE,'Mon DD, YYYY'),'Mon DD, YYYY') REQUEST_ACTIVE_DATE,
    RECIPIENT_INFO.RECEIPANT_ID PATIENT_ID,
    RECIPIENT_INFO.PK_RECEIPANT PATIENT_ENTITY_ID,
    ERES.F_GET_CBU_STATUS(ORD.FK_ORDER_RESOL_BY_TC) TC_RESOLUTION,
    ERES.F_GET_CBU_STATUS(ORD.FK_ORDER_RESOL_BY_CBB) CBB_RESOLUTION,
    ERES.F_GET_CBU_STATUS(HDR.ORDER_CLOSE_REASON) CLOSE_REQUEST_REASON,
    ERES.F_GETUSER(ORD.ORDER_ACK_BY) REQUEST_ACK_BY,
    TO_CHAR(ORD.ORDER_ACK_DATE,'Mon DD, YYYY') REQUEST_ACK_DATE
  FROM ERES.ER_ORDER ORD
  INNER JOIN ERES.ER_ORDER_HEADER HDR
  ON (ORD.FK_ORDER_HEADER=HDR.PK_ORDER_HEADER)
  INNER JOIN ERES.CB_CORD CRD
  ON (CRD.PK_CORD=HDR.ORDER_ENTITYID)
  INNER JOIN ERES.ER_SITE SITE
  ON (SITE.PK_SITE=CRD.FK_CBB_ID)
  LEFT OUTER JOIN
    (SELECT CBUSHIPMENT.PK_SHIPMENT,
      CBUSHIPMENT.FK_ORDER_ID ORDERID,
      TO_CHAR(CBUSHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY') SCHEDULED_SHIP_DATE,
      CBUSHIPMENT.SCH_SHIPMENT_TIME SCHEDULED_SHIP_TIME,
      TO_CHAR(CBUSHIPMENT.SCH_SHIPMENT_DATE,'Day') SCHEDULED_SHIP_DAY,
      ERES.F_CODELST_DESC(CBUSHIPMENT.FK_SHIP_COMPANY_ID) SHIPPING_COMPANY,
      CBUSHIPMENT.SHIPMENT_TRACKING_NO TRACKING_NO,
      TO_CHAR(CBUSHIPMENT.SHIPMENT_DATE,'Mon DD, YYYY') SHIPMENT_CONFIRM_ON,
      ERES.F_GETUSER(CBUSHIPMENT.SHIPMENT_CONFIRM_BY) SHIPMENT_CONFIRMED_BY
    FROM ERES.CB_SHIPMENT CBUSHIPMENT
    WHERE CBUSHIPMENT.FK_SHIPMENT_TYPE=
      (SELECT PK_CODELST
      FROM ERES.ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='CBU'
      )
    ) CBUSHIP
  ON (CBUSHIP.ORDERID=ORD.PK_ORDER)
  LEFT OUTER JOIN
    (SELECT NMDPSHIPMENT.PK_SHIPMENT,
      NMDPSHIPMENT.FK_ORDER_ID ORDERID,
      TO_CHAR(NMDPSHIPMENT.SCH_SHIPMENT_DATE,'Mon DD, YYYY') NMDP_SAMPL_SCH_SHIP_DATE,
      NMDPSHIPMENT.SCH_SHIPMENT_TIME NMDP_SAMPL_SCH_SHIP_TIME,
      TO_CHAR(NMDPSHIPMENT.SCH_SHIPMENT_DATE,'Day') NMDP_SAMPL_SCH_SHIP_DAY,
      ERES.F_CODELST_DESC(NMDPSHIPMENT.FK_SHIP_COMPANY_ID) NMDP_SAMPL_SHIPPING_COMPANY,
      NMDPSHIPMENT.SHIPMENT_TRACKING_NO NMDP_SAMPL_TRACKING_NO
    FROM ERES.CB_SHIPMENT NMDPSHIPMENT
    WHERE NMDPSHIPMENT.FK_SHIPMENT_TYPE=
      (SELECT PK_CODELST
      FROM ERES.ER_CODELST
      WHERE CODELST_TYPE='shipment_type'
      AND CODELST_SUBTYP='NMDP'
      )
    ) NMDPSHIP
  ON (NMDPSHIP.ORDERID=ORD.PK_ORDER)
  LEFT OUTER JOIN ERES.ER_ORDER_RECEIPANT_INFO ORDER_RECIPIENT_INFO
  ON (ORDER_RECIPIENT_INFO.FK_ORDER_ID=ORD.PK_ORDER)
  LEFT OUTER JOIN ERES.CB_RECEIPANT_INFO RECIPIENT_INFO
  ON (RECIPIENT_INFO.PK_RECEIPANT = ORDER_RECIPIENT_INFO.FK_RECEIPANT)
  WHERE ORD.ORDER_STATUS          =
    (SELECT PK_CODELST
    FROM ERES.ER_CODELST
    WHERE CODELST_TYPE='order_status'
    AND CODELST_SUBTYP='close_ordr'
    );
	
	/
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."PK_ORDER"
IS
  'Order id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."PK_CORD"
IS
  'Cord Id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."CORD_REGISTRY_ID"
IS
  'Registry Id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."LOCAL_STATUS"
IS
  'Local Status of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."NMDP_STATUS"
IS
  'NMDP Status of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."SITE_ID"
IS
  'Site Id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."REQUEST_STATUS"
IS
  'Status of request for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."REQUEST_TYPE"
IS
  'Type of the request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."REQUEST_OPEN_DATE"
IS
  'Date on which request open';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."REQUEST_PRIORITY"
IS
  'Priority of the request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."REQUEST_LAST_REVIEWED_BY"
IS
  'Name of person who reviewed the request last';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."REQUEST_LAST_REVIEWED_DATE"
IS
  'Last review date of request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."REQUEST_ASSIGNED_TO"
IS
  'Name of person to whom request is assigned to';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."REQUEST_ASSIGNED_DATE"
IS
  'Assign date of request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."RESULT_RECEIVED_DATE"
IS
  'Received date of result';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."SCHEDULED_SHIP_DATE"
IS
  'Scheduled shipment date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."SCHEDULED_SHIP_TIME"
IS
  'Scheduled shipment time.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."SCHEDULED_SHIP_DAY"
IS
  'Scheduled shipment day.';
  
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."SHIPPING_COMPANY"
IS
  'Name of the shipment company.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."TRACKING_NO"
IS
  'Tracking number.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."SHIPMENT_CONFIRM_ON"
IS
  'Date on which shipment is confirmed.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."SHIPMENT_CONFIRMED_BY"
IS
  'Name of person who has confirmed shipment.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."NMDP_SAMPL_SCH_SHIP_DATE"
IS
  'NMDP Sample Scheduled shipment date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."NMDP_SAMPL_SCH_SHIP_TIME"
IS
  'NMDP Sample Scheduled shipment time.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."NMDP_SAMPL_SCH_SHIP_DAY"
IS
  'NMDP Sample Scheduled shipment day.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."NMDP_SAMPL_SHIPPING_COMPANY"
IS
  'NMDP Sample Scheduled name of the shipment company.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."NMDP_SAMPL_TRACKING_NO"
IS
  'NMDP Sample Scheduled tracking number.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."SAMPLE_TYPE"
IS
  'Type of the sample';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."ALIQUOTS_TYPE"
IS
  'Aliquots type';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."LAB_CODE"
IS
  'Lab code';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."REQUEST_ACTIVE_DATE"
IS
  'Active date of the request';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."PATIENT_ID"
IS
  'Patient ID';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."PATIENT_ENTITY_ID"
IS
  'Patient entity ID';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."TC_RESOLUTION"
IS
  'Transplant center resolution.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."CBB_RESOLUTION"
IS
  'CBB resolution.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."CLOSE_REQUEST_REASON"
IS
  'Close request reason.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."REQUEST_ACK_BY"
IS
  'This column stores name of the person who acknowledge request.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_COMPLETED_REQ"."REQUEST_ACK_DATE"
IS
  'This column stores date on which request date is acknowledged.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_COMPLETED_REQ"
IS
  'This view provides basic information of Order for Completed Request';
  /
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_USER_INFO" ("CBB_ID", "GRP_NAME", "GRP_DESC", "USR_LOGNAME", "USR_FIRSTNAME", "USR_LASTNAME", "SITE_NAME", "PK_USER", "PK_GRP")
AS
  SELECT SITE.SITE_ID,
    g.grp_name,
    g.grp_desc,
    usr.usr_logname,
    usr.usr_firstname,
    usr.usr_lastname,
    site.site_name,
    usr.PK_USER,
    g.pk_grp
  FROM eres.er_grps g
  LEFT JOIN eres.er_usrsite_grp u
  ON u.FK_GRP_ID=g.PK_GRP
  LEFT JOIN eres.er_usersite eu
  ON eu.PK_USERSITE=u.FK_USER_SITE
  LEFT JOIN eres.er_user usr
  ON usr.pk_user= u.fk_user
  LEFT JOIN eres.er_site site
  ON site.pk_site= eu.fk_site;
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_USER_INFO"."CBB_ID"
IS
  'CBB ID of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_USER_INFO"."GRP_NAME"
IS
  'This column is for storing the name of a Group.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_USER_INFO"."GRP_DESC"
IS
  'This column is for storing the description of a a group.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_USER_INFO"."USR_LOGNAME"
IS
  'This column stores the login name of the user for entering into eResearch application';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_USER_INFO"."USR_FIRSTNAME"
IS
  'This column stores the First name of the user';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_USER_INFO"."USR_LASTNAME"
IS
  'This column stores the Last name of the user';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_USER_INFO"."SITE_NAME"
IS
  'This column is used to store the name of the site';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_USER_INFO"."PK_USER"
IS
  'Primary key of table ER_USER';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_USER_INFO"."PK_GRP"
IS
  'Primary key of table ER_GRPS';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBU_USER_INFO"
IS
  'This view provides basic information of CBU user';
  /
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_USER_SESSION_LOG" ("LOGIN_NAME", "LOGIN_TIME", "LOGOUT_TIME", "IP_ADD", "SUCCESS_STATUS", "USER_ID")
AS
  SELECT users.usr_logname,
    TO_CHAR(UserSession.LOGIN_TIME,'Mon DD, YYYY HH:MI:SS AM'),
    TO_CHAR(UserSession.LOGOUT_TIME,'Mon DD, YYYY HH:MI:SS AM'),
    UserSession.IP_ADD,
    UserSession.SUCCESS_FLAG,
    UserSession.FK_USER
  FROM eres.ER_USERSESSION UserSession ,
    eres.ER_USER users
  WHERE UserSession.fk_user = users.pk_user;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_LOG"."LOGIN_NAME"
IS
  'This column store name from which user logged in to the applicaton.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_LOG"."LOGIN_TIME"
IS
  'This column stores time when user logged in to the applicaton.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_LOG"."LOGOUT_TIME"
IS
  'This column stores time when user logged out from the applicaton.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_LOG"."IP_ADD"
IS
  'This column stores IP Address of the user.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_LOG"."SUCCESS_STATUS"
IS
  'This column stores the success flag for the user session. Stores : 1 if login was successful, 0 if login was unsuccessful.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_USER_SESSION_LOG"."USER_ID"
IS
  'This column stores the User ID';
  /
  COMMENT ON TABLE "VDA"."ETVDA_USER_SESSION_LOG"
IS
  'This view provides information about User Session';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CORD_IMPORTED_ALL" ("CBU_REGISTRY_ID", "CBU_LOCAL_CBU_ID", "MATERNAL_REGISTRY_ID", "MATERNAL_LOCAL_ID", "CORD_CREATION_DATE", "BABY_GENDER", "CBB_ID", "Created_On", "Local_Status", "LICENSURE_STATUS", "UNLICENSED_REASON", "ELIGIBILITY_DETERMINATION", "INELIGIBLE/INCOMPLETE_REASON", "UNIQUE_ID_ON_BAG", "PK_CORD")
AS
  SELECT DISTINCT cord.cord_registry_id,
    cord.cord_local_cbu_id,
    cord.REGISTRY_MATERNAL_ID,
    cord.MATERNAL_LOCAL_ID,
    CORD.CORD_CREATION_DATE,
    eres.f_codelst_desc(cord.FK_CORD_BABY_GENDER_ID),
    site.site_id ,
    cord.created_on,
    eres.F_GET_CBU_STATUS(cord.FK_CORD_CBU_STATUS) AS local_status,
    eres.f_codelst_desc(cord.FK_CORD_CBU_LIC_STATUS),
    eres.F_GET_MULTIPLE_REASONS(cord.FK_CORD_CBU_UNLICENSED_REASON),
    eres.f_codelst_desc(cord.FK_CORD_CBU_ELIGIBLE_STATUS),
    eres.F_GET_MULTIPLE_REASONS(cord.FK_CORD_CBU_INELIGIBLE_REASON),
    cord.CORD_ID_NUMBER_ON_CBU_BAG,
    cord.pk_cord
  FROM eres.cb_cord cord ,
    eres.ER_SITE site,
    eres.cb_cord_import_messages cordImport
  WHERE cord.pk_cord = cordImport.fk_cord_id
  AND cord.FK_CBB_ID = site.pk_site;
    /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."CBU_REGISTRY_ID"
IS
  'This column stores Registry ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."CBU_LOCAL_CBU_ID"
IS
  'This column stores Local ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."MATERNAL_REGISTRY_ID"
IS
  'This column stores Maternal Registry ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."MATERNAL_LOCAL_ID"
IS
  'This column stores Maternal Local ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."CORD_CREATION_DATE"
IS
  'This column stores Maternal Registry ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."BABY_GENDER"
IS
  'Gender of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."CBB_ID"
IS
  'This column stores an Identification number for the site';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."Created_On"
IS
  'This column stores when cord created';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."Local_Status"
IS
  'This column stores Local status of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."LICENSURE_STATUS"
IS
  'Licensure status of of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."UNLICENSED_REASON"
IS
  'Unlicensed reason for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."ELIGIBILITY_DETERMINATION"
IS
  'Eligibility determination reason for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."INELIGIBLE/INCOMPLETE_REASON"
IS
  'Incomplete reason for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."UNIQUE_ID_ON_BAG"
IS
  'Unique Id on the bag for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_ALL"."PK_CORD"
IS
  'This column stores the Collection Date of the cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CORD_IMPORTED_ALL"
IS
  'This view provides Information of all imported cords ';
  /
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CORD_IMPORTED_PRG" ("CBU_REGISTRY_ID", "CBU_LOCAL_CBU_ID", "MATERNAL_REGISTRY_ID", "MATERNAL_LOCAL_ID", "CORD_CREATION_DATE", "BABY_GENDER", "CBB_ID", "Created_On", "Local_Status", "LICENSURE_STATUS", "UNLICENSED_REASON", "ELIGIBILITY_DETERMINATION", "INELIGIBLE/INCOMPLETE_REASON", "UNIQUE_ID_ON_BAG", "PK_CORD")
AS
  SELECT DISTINCT cord.cord_registry_id,
    cord.cord_local_cbu_id,
    cord.REGISTRY_MATERNAL_ID,
    cord.MATERNAL_LOCAL_ID,
    CORD.CORD_CREATION_DATE,
    eres.f_codelst_desc(cord.FK_CORD_BABY_GENDER_ID),
    site.site_id ,
    cord.created_on,
    eres.F_GET_CBU_STATUS(cord.FK_CORD_CBU_STATUS) AS local_status,
    eres.f_codelst_desc(cord.FK_CORD_CBU_LIC_STATUS),
    eres.F_GET_MULTIPLE_REASONS(cord.FK_CORD_CBU_UNLICENSED_REASON),
    eres.f_codelst_desc(cord.FK_CORD_CBU_ELIGIBLE_STATUS),
    eres.F_GET_MULTIPLE_REASONS(cord.FK_CORD_CBU_INELIGIBLE_REASON),
    cord.CORD_ID_NUMBER_ON_CBU_BAG,
    cord.pk_cord
  FROM eres.cb_cord cord ,
    eres.ER_SITE site,
    eres.cb_cord_import_messages cordImport
  WHERE cord.pk_cord       = cordImport.fk_cord_id
  AND cord.FK_CBB_ID       = site.pk_site
  AND cord.CORD_SEARCHABLE = '0';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."CBU_REGISTRY_ID"
IS
  'This column stores Registry ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."CBU_LOCAL_CBU_ID"
IS
  'This column stores Local ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."MATERNAL_REGISTRY_ID"
IS
  'This column stores Maternal Registry ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."MATERNAL_LOCAL_ID"
IS
  'This column stores Maternal Local ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."CORD_CREATION_DATE"
IS
  'This column stores Maternal Registry ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."BABY_GENDER"
IS
  'Gender of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."CBB_ID"
IS
  'This column stores an Identification number for the site';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."Created_On"
IS
  'This column stores when cord created';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."Local_Status"
IS
  'This column stores Local status of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."LICENSURE_STATUS"
IS
  'Licensure status of of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."UNLICENSED_REASON"
IS
  'Unlicensed reason for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."ELIGIBILITY_DETERMINATION"
IS
  'Eligibility determination reason for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."INELIGIBLE/INCOMPLETE_REASON"
IS
  'Incomplete reason for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."UNIQUE_ID_ON_BAG"
IS
  'Unique Id on the bag for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_PRG"."PK_CORD"
IS
  'This column stores the Collection Date of the cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CORD_IMPORTED_PRG"
IS
  'This view provides Information of �Saved In Progress� imported cords';
  /
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CORD_IMPORTED_SUBMT" ("CBU_REGISTRY_ID", "CBU_LOCAL_CBU_ID", "MATERNAL_REGISTRY_ID", "MATERNAL_LOCAL_ID", "CORD_CREATION_DATE", "BABY_GENDER", "CBB_ID", "Created_On", "Local_Status", "LICENSURE_STATUS", "UNLICENSED_REASON", "ELIGIBILITY_DETERMINATION", "INELIGIBLE/INCOMPLETE_REASON", "UNIQUE_ID_ON_BAG", "PK_CORD")
AS
  SELECT DISTINCT cord.cord_registry_id,
    cord.cord_local_cbu_id,
    cord.REGISTRY_MATERNAL_ID,
    cord.MATERNAL_LOCAL_ID,
    CORD.CORD_CREATION_DATE,
    eres.f_codelst_desc(cord.FK_CORD_BABY_GENDER_ID),
    site.site_id ,
    cord.created_on,
    eres.F_GET_CBU_STATUS(cord.FK_CORD_CBU_STATUS) AS local_status,
    eres.f_codelst_desc(cord.FK_CORD_CBU_LIC_STATUS),
    eres.F_GET_MULTIPLE_REASONS(cord.FK_CORD_CBU_UNLICENSED_REASON),
    eres.f_codelst_desc(cord.FK_CORD_CBU_ELIGIBLE_STATUS),
    eres.F_GET_MULTIPLE_REASONS(cord.FK_CORD_CBU_INELIGIBLE_REASON),
    cord.CORD_ID_NUMBER_ON_CBU_BAG,
    cord.pk_cord
  FROM eres.cb_cord cord ,
    eres.ER_SITE site,
    eres.cb_cord_import_messages cordImport
  WHERE cord.pk_cord       = cordImport.fk_cord_id
  AND cord.FK_CBB_ID       = site.pk_site
  AND cord.CORD_SEARCHABLE = '1';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."CBU_REGISTRY_ID"
IS
  'This column stores Registry ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."CBU_LOCAL_CBU_ID"
IS
  'This column stores Local ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."MATERNAL_REGISTRY_ID"
IS
  'This column stores Maternal Registry ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."MATERNAL_LOCAL_ID"
IS
  'This column stores Maternal Local ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."CORD_CREATION_DATE"
IS
  'This column stores Maternal Registry ID of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."BABY_GENDER"
IS
  'Gender of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."CBB_ID"
IS
  'This column stores an Identification number for the site';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."Created_On"
IS
  'This column stores when cord created';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."Local_Status"
IS
  'This column stores Local Status of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."LICENSURE_STATUS"
IS
  'Licensure status of of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."UNLICENSED_REASON"
IS
  'Unlicensed reason for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."ELIGIBILITY_DETERMINATION"
IS
  'Eligibility determination reason for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."INELIGIBLE/INCOMPLETE_REASON"
IS
  'Incomplete reason for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."UNIQUE_ID_ON_BAG"
IS
  'Unique Id on the bag for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_IMPORTED_SUBMT"."PK_CORD"
IS
  'This column stores the Collection Date of the cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CORD_IMPORTED_SUBMT"
IS
  'This view provides Information of �Submitted� cords';
  /
  
  
  -------------comments------------------------
  
  
  
   COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."CORD_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."PROCESSING_PROCEDURE"
IS
  'Name of the processing procedure.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."ENTRY_DATE"
IS
  'Date on which cord is created.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."PROC_START_DATE"
IS
  'Start date of the procedure.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."PROC_END_DATE"
IS
  'End date of the procedure.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."FILTER_PAPER_SAMPLE"
IS
  'This column will store number of filter sample paper';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."RBC_PELLETS"
IS
  'This column will store number of RBC Pallets';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."EXTRACTED_DNA_SAMPLES"
IS
  'This column will store number of Extracted DNA samples';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."SERUM_SAMPLES"
IS
  'This column will store serum samples';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."PLASAMA_SAMPLES"
IS
  'This column will store plasma samples';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."NON-VIABLE_CELL"
IS
  'This column will store non viable cell.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."NON_VIA_FINAL_PRODUCT"
IS
  'This column will store non viable  final product.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."NO_SEGMENTS"
IS
  'This column will store number of segments.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."ALIQUOTS_CONSISTENT_CONDTION"
IS
  'This column will store aliquots consistent condition.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."ALIQUOTS_ALTERNATE_CONDTION"
IS
  'This column will store alternate aliquots consistent condition. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."NO_MAT_SERUM"
IS
  'This column will store number of maternal serum.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."NO_MAT_PLASMA"
IS
  'This column will store number of maternal plasma.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."NO_MAT_EXT_DNA"
IS
  'This column will store number of maternal external DNA.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."NO_MISECLL_MAT"
IS
  'This column will store number of miscellaneous maternal.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_ALL"."PK_CORD"
IS
  'Primary key of the cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBB_PRO_PROC_ALL"
IS
  'This view provides information of Aliquots Fields of all cords ';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."CORD_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."PROCESSING_PROCEDURE"
IS
  'Name of the processing procedure.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."ENTRY_DATE"
IS
  'Date on which cord is created.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."PROC_START_DATE"
IS
  'Start date of the procedure.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."PROC_END_DATE"
IS
  'End date of the procedure.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."FILTER_PAPER_SAMPLE"
IS
  'This column will store number of filter sample paper';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."RBC_PELLETS"
IS
  'This column will store number of RBC Pallets';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."EXTRACTED_DNA_SAMPLES"
IS
  'This column will store number of Extracted DNA samples';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."SERUM_SAMPLES"
IS
  'This column will store serum samples';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."PLASAMA_SAMPLES"
IS
  'This column will store plasma samples';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."NON-VIABLE_CELL"
IS
  'This column will store non viable cell.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."NON_VIA_FINAL_PRODUCT"
IS
  'This column will store non viable  final product.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."NO_SEGMENTS"
IS
  'This column will store number of segments.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."ALIQUOTS_CONSISTENT_CONDTION"
IS
  'This column will store aliquots consistent condition.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."ALIQUOTS_ALTERNATE_CONDTION"
IS
  'This column will store alternate aliquots consistent condition. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."NO_MAT_SERUM"
IS
  'This column will store number of maternal serum.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."NO_MAT_PLASMA"
IS
  'This column will store number of maternal plasma.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."NO_MAT_EXT_DNA"
IS
  'This column will store number of maternal external DNA.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."NO_MISECLL_MAT"
IS
  'This column will store number of miscellaneous maternal.';
  
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_PRG"."PK_CORD"
IS
  'Primary key of the cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBB_PRO_PROC_PRG"
IS
  'This view provides information of Aliquots Fields of  �Saved In Progress� cords ';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."CORD_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."PROCESSING_PROCEDURE"
IS
  'Name of the processing procedure.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."ENTRY_DATE"
IS
  'Date on which cord is created.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."PROC_START_DATE"
IS
  'Start date of the procedure.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."PROC_END_DATE"
IS
  'End date of the procedure.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."FILTER_PAPER_SAMPLE"
IS
  'This column will store number of filter sample paper';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."RBC_PELLETS"
IS
  'This column will store number of RBC Pallets';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."EXTRACTED_DNA_SAMPLES"
IS
  'This column will store number of Extracted DNA samples';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."SERUM_SAMPLES"
IS
  'This column will store serum samples';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."PLASAMA_SAMPLES"
IS
  'This column will store plasma samples';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."NON-VIABLE_CELL"
IS
  'This column will store non viable cell.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."NON_VIA_FINAL_PRODUCT"
IS
  'This column will store non viable  final product.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."NO_SEGMENTS"
IS
  'This column will store number of segments.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."ALIQUOTS_CONSISTENT_CONDTION"
IS
  'This column will store aliquots consistent condition.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."ALIQUOTS_ALTERNATE_CONDTION"
IS
  'This column will store alternate aliquots consistent condition. ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."NO_MAT_SERUM"
IS
  'This column will store number of maternal serum.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."NO_MAT_PLASMA"
IS
  'This column will store number of maternal plasma.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."NO_MAT_EXT_DNA"
IS
  'This column will store number of maternal external DNA.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."NO_MISECLL_MAT"
IS
  'This column will store number of miscellaneous maternal.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"."PK_CORD"
IS
  'Primary key of the cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBB_PRO_PROC_SUBMT"
IS
  'This view provides information of Aliquots Fields of  �Submitted�  cords ';
  /
  
   COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."CRI"
IS
  'This column will store Complete Required Information.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."CBU_STATUS"
IS
  'This column store the local status of the cord.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."CRI_CREATION_DATE"
IS
  'This column store CRI creation date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."PK_CORD"
IS
  'Primary key of the cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBU_CRI_VIEW_ALL"
IS
  'This view provides Complete Required Information of all cords';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_PRG"."CRI"
IS
  'This column will store Complete Required Information.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_PRG"."CBU_STATUS"
IS
  'This column store the local status of the cord.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_PRG"."CRI_CREATION_DATE"
IS
  'This column store CRI creation date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_PRG"."PK_CORD"
IS
  'Primary key of the cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBU_CRI_VIEW_PRG"
IS
  'This view provides Complete Required Information of �Saved In Progress� cords ';
  /
  
   COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT"."CRI"
IS
  'This column will store Complete Required Information.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT"."CBU_STATUS"
IS
  'This column store the local status of the cord.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT"."CRI_CREATION_DATE"
IS
  'This column store CRI creation date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT"."PK_CORD"
IS
  'Primary key of the cord';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT"
IS
  'This view provides Complete Required Information of �Submitted� cords ';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."CBB_ID"
IS
  'CBB ID OF CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."NO_OF_SEGMENTS"
IS
  'This column will store the No of Segment Available.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."HLA_VALUE_TYPE1"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 1.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."HLA_VALUE_TYPE2"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 2.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."HLA_DESC"
IS
  'Description of Code Id for HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."RECEIVED_DATE"
IS
  'HLA Received date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."PK_CORD"
IS
  'Primary key of table CB_CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."PK_HLA"
IS
  'Primary key of table CB_HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."PK_ENTITY_SAMPLES"
IS
  'Primary key of table CB_ENTITY_SAMPLES';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBU_HLA_INFO_ALL"
IS
  'This view provides HLA Information of all cords ';
  /
  
   COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."CBB_ID"
IS
  'CBB ID OF CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."NO_OF_SEGMENTS"
IS
  'This column will store the No of Segment Available.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."HLA_VALUE_TYPE1"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 1.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."HLA_VALUE_TYPE2"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 2.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."HLA_DESC"
IS
  'Description of Code Id for HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."RECEIVED_DATE"
IS
  'HLA Received date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."PK_CORD"
IS
  'Primary key of table CB_CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."PK_HLA"
IS
  'Primary key of table CB_HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."PK_ENTITY_SAMPLES"
IS
  'Primary key of table CB_ENTITY_SAMPLES';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBU_HLA_INFO_PRG"
IS
  'This view provides HLA Information of �Saved In Progress� cords';
  /
  
    COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."CBB_ID"
IS
  'CBB ID OF CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."NO_OF_SEGMENTS"
IS
  'This column will store the No of Segment Available.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."HLA_VALUE_TYPE1"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 1.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."HLA_VALUE_TYPE2"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 2.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."HLA_DESC"
IS
  'Description of Code Id for HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."RECEIVED_DATE"
IS
  'HLA Received date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."PK_CORD"
IS
  'Primary key of table CB_CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."PK_HLA"
IS
  'Primary key of table CB_HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."PK_ENTITY_SAMPLES"
IS
  'Primary key of table CB_ENTITY_SAMPLES';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"
IS
  'This view provides HLA Information of �Submitted� cords';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_ALL"."CBB_ID"
IS
  'Order id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_ALL"."CBU_REGISTRY_ID"
IS
  'Registry Id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_ALL"."CBU_LOCAL_ID"
IS
  'Local Status of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_ALL"."RESPONSE_CODE"
IS
  'Response code';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_ALL"."LOCAL_STATUS"
IS
  'Local status of the cord.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_ALL"."PK_CORD"
IS
  'Code id of the cord.';
  
  /
  COMMENT ON TABLE "VDA"."ETVDA_CORD_SYPHILLIS_STS_ALL"
IS
  'This view provides syphilis positive test Information of all cords'; 
  /
  
  
  
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_PRG"."CBB_ID"
IS
  'Order id of cord';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_PRG"."CBU_REGISTRY_ID"
IS
  'Registry Id of cord';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_PRG"."CBU_LOCAL_ID"
IS
  'Local Status of cord';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_PRG"."RESPONSE_CODE"
IS
  'Response code';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_PRG"."LOCAL_STATUS"
IS
  'Local status of the cord.';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_PRG"."PK_CORD"
IS
  'Code id of the cord.';
  
  /
  COMMENT ON TABLE "VDA"."ETVDA_CORD_SYPHILLIS_STS_PRG"
IS
  'This view provides syphilis positive test Information of �Saved In Progress� cords ';
  
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_SUBMT"."CBB_ID"
IS
  'Order id of cord';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_SUBMT"."CBU_REGISTRY_ID"
IS
  'Registry Id of cord';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_SUBMT"."CBU_LOCAL_ID"
IS
  'Local Status of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_SUBMT"."RESPONSE_CODE"
IS
  'Response code';
  
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_SUBMT"."LOCAL_STATUS"
IS
  'Local status of the cord.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CORD_SYPHILLIS_STS_SUBMT"."PK_CORD"
IS
  'Code id of the cord.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CORD_SYPHILLIS_STS_SUBMT"
IS
  'This view provides syphilis positive test Information of �Submitted� cords ';
  /
  
CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_IDM_FORM_INFO" ("CBU_REGISTRY_ID", "QUESTION_CODE", "QUESTION_SEQ", "QUESTION", "RESPONSE", "COMMENT", "PK_CORD")
AS
  SELECT c.cord_registry_id,
    ques.ques_code,
    forq.ques_seq Question,
    CASE
      WHEN ques_desc IS NULL
      THEN formres.resp_val
      WHEN ques_desc IS NOT NULL
      THEN ques_desc
    END Question,
    CASE
      WHEN formres.resp_code IS NOT NULL
      THEN eres.f_codelst_desc(formres.resp_code)
      WHEN formres.dynaformDate IS NOT NULL
      THEN formres.dynaformDate
    END Response,
    formres.comments Comments,
    c.pk_cord
  FROM eres.cb_form_questions forq,
    eres.cb_question_grp qgrp,
    eres.cb_questions ques
  LEFT OUTER JOIN
    (SELECT frmres.pk_form_responses,
      frmres.fk_question,
      frmres.response_code resp_code,
      frmres.response_value resp_val,
      frmres.comments,
      frmres.fk_form_responses,
      frmres.fk_form_version,
      TO_CHAR(frmres.DATE_MOMANS_OR_FORMFILL,'Mon DD, YYYY') dynaformDate,
      frmres.entity_id entityid
    FROM eres.cb_form_responses frmres,
      eres.cb_cord cord
    WHERE frmres.entity_id     =cord.pk_cord
    AND frmres.fk_form_version =
      (SELECT MAX(pk_form_version)
      FROM eres.cb_form_version
      WHERE entity_id=cord.pk_cord
      )
    ) formres
  ON(formres.FK_QUESTION=ques.pk_questions)
  LEFT OUTER JOIN eres.cb_cord c
  ON (c.pk_cord           =formres.entityid)
  WHERE ques.pk_questions =forq.fk_question
  AND ques.pk_questions   =qgrp.fk_question
  AND forq.fk_form        =
    (SELECT pk_form
    FROM eres.cb_forms
    WHERE forms_desc   ='IDM'
    AND is_current_flag='1'
    )
  AND qgrp.fk_form =
    (SELECT pk_form
    FROM eres.cb_forms
    WHERE forms_desc   ='IDM'
    AND is_current_flag='1'
    );
 /
 
   COMMENT ON COLUMN "VDA"."ETVDA_IDM_FORM_INFO"."CBU_REGISTRY_ID"
IS
  'Registry Id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_FORM_INFO"."QUESTION_CODE"
IS
  'Code of the qustion.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_FORM_INFO"."QUESTION_SEQ"
IS
  'Sequence of the question.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_FORM_INFO"."QUESTION"
IS
  'Question description.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_FORM_INFO"."RESPONSE"
IS
  'Response for the question.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_FORM_INFO"."COMMENT"
IS
  'Comment in response for the question.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_IDM_FORM_INFO"."PK_CORD"
IS
  'Code id of the cord.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_IDM_FORM_INFO"
IS
  'This view provides IDM Information';
  /
  
   COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."TEST_RESULT"
IS
  'Test result for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."TIMING_OF_TEST"
IS
  'TIMING OF TEST';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."POST_THAW_ORDER"
IS
  'Order of POST-CRYOPRESERVATION/THAW';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."TEST_REASON"
IS
  'Reason for Test ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."SAMPLE_TYPE"
IS
  'Sample Type ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."TEST_METHOD"
IS
  'TEST METHOD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."PK_CORD"
IS
  'Primary key of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_ALL"."LABTEST_NAME"
IS
  'Description of test id';
  /
  COMMENT ON TABLE "VDA"."ETVDA_PATLAB_INFO_ALL"
IS
  'This view provides PATLAB Information of all cords ';
  /
  
  
   COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_PRG"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_PRG"."TEST_RESULT"
IS
  'Test result for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_PRG"."TIMING_OF_TEST"
IS
  'TIMING OF TEST';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_PRG"."POST_THAW_ORDER"
IS
  'Order of POST-CRYOPRESERVATION/THAW';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_PRG"."TEST_REASON"
IS
  'Reason for Test ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_PRG"."SAMPLE_TYPE"
IS
  'Sample Type ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_PRG"."TEST_METHOD"
IS
  'TEST METHOD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_PRG"."PK_CORD"
IS
  'Primary key of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_PRG"."LABTEST_NAME"
IS
  'Discription of test id';
  /
  COMMENT ON TABLE "VDA"."ETVDA_PATLAB_INFO_PRG"
IS
  'This view provides PATLAB Information of �Saved In Progress� cords ';
  /
  
  
   COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_SUBMT"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_SUBMT"."TEST_RESULT"
IS
  'Test result for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_SUBMT"."TIMING_OF_TEST"
IS
  'TIMING OF TEST';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_SUBMT"."POST_THAW_ORDER"
IS
  'Order of POST-CRYOPRESERVATION/THAW';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_SUBMT"."TEST_REASON"
IS
  'Reason for Test ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_SUBMT"."SAMPLE_TYPE"
IS
  'Sample Type ';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_SUBMT"."TEST_METHOD"
IS
  'TEST METHOD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_SUBMT"."PK_CORD"
IS
  'Primary key of Cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_PATLAB_INFO_SUBMT"."LABTEST_NAME"
IS
  'Description of test id';
  /
  COMMENT ON TABLE "VDA"."ETVDA_PATLAB_INFO_SUBMT"
IS
  'This view provides PATLAB Information of �Submitted� cords ';
  /
  
  
    CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ORDERDETAILS_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD") AS 
  select "COLUMN_NAME","COLUMN_DISPLAY_NAME","USER_ID","DATE_TIME","ACTION","OLD_VALUE","NEW_VALUE","TABLE_NAME","TIMESTAMP","PK_CORD" from (   SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT order_entityid
    FROM eres.er_order_header
    WHERE pk_order_header = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order_header FROM eres.er_order_header
    )
  AND arm.table_name   = 'ER_ORDER_HEADER')
  AND acm.column_name IN ('ORDER_OPEN_DATE','FK_SITE_ID')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT order_entityid
    FROM eres.er_order_header
    WHERE pk_order_header IN
      (SELECT fk_order_header FROM eres.er_order WHERE pk_order = arm.module_id
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order
    FROM eres.er_order
    WHERE fk_order_header IN
      (SELECT pk_order_header FROM eres.er_order_header
      )
    AND arm.table_name = 'ER_ORDER'
    ))
  AND acm.column_name NOT IN ('RID','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','IP_ADD','PK_ORDER','ORDER_TYPE','FK_ORDER_HEADER', 'CBU_AVAIL_CONFIRM_FLAG','FK_ATTACHMENT','TASK_ID','TASK_NAME','DELETEDFLAG', 'CLINIC_INFO_CHECKLIST_STAT','ADDI_TEST_RESULT_AVAIL','CANCEL_CONFORM_DATE','CANCELED_BY','ACCPT_TO_CANCEL_REQ','FK_CASE_MANAGER','ORDER_NUMBER','ORDER_ENTITYID','ORDER_ENTITYTYPE','FK_ORDER_STATUS','ORDER_REMARK','ORDER_BY','FK_ORDER_ITEM_ID','FK_ORDER_TYPE','DATA_MODIFIED_FLAG','FINAL_REVIEW_TASK_FLAG','COMPLETE_REQ_INFO_TASK_FLAG')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.cb_shipment
    WHERE pk_shipment = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_shipment FROM eres.cb_shipment
    )
  AND arm.table_name       = 'CB_SHIPMENT')
  AND acm.column_name NOT IN ('PK_SHIPMENT','FK_ORDER_ID','FK_CORD_ID','RID','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','DATA_MODIFIED_FLAG','FK_SHIPMENT_TYPE','CBB_ID','FK_CORD_ID','DELETEDFLAG')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.er_order_receipant_info
    WHERE pk_order_receipant_info = arm.module_id and 
    arm.table_name = 'ER_ORDER_RECEIPANT_INFO'
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order_receipant_info FROM eres.er_order_receipant_info
    )
  AND arm.table_name       = 'ER_ORDER_RECEIPANT_INFO')
  AND acm.column_name NOT IN ('PK_ORDER_RECEIPANT_INFO','FK_RECEIPANT','FK_ORDER_ID','FK_CORD_ID','RID','ENTITY_ID','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','IP_ADD','NMDP_RID','NMDP_ID','LOCAL_ID','COOP_REF','TC_CDE','SCU_ACTIVATED_DATE','FK_SITE_ID')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM'),
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_notes
    WHERE pk_notes    = arm.module_id
    AND fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'progress'
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_notes
    FROM eres.cb_notes
    WHERE fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'progress'
      )
    )
  AND arm.table_name   = 'CB_NOTES')
  AND acm.column_name IN ('SUBJECT','KEYWORD','TAG_NOTE','NOTES_PROGRESS','FK_NOTES_CATEGORY','NOTE_VISIBLE','REQUEST_TYPE','REQUEST_DATE','AMENDED','NOTE_SEQ')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT order_entityid
    FROM eres.er_order_header
    WHERE order_requested_by = arm.module_id and  
    arm.table_name = 'ER_ORDER_USERS' 
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT order_requested_by FROM eres.er_order_header
    )
  AND arm.table_name       = 'ER_ORDER_USERS')
  AND acm.column_name NOT IN ('RID','DELETEDFLAG','USER_COMMENTS','PK_ORDER_USER','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY')
  UNION ALL
  SELECT acm.column_name,
   acm.column_display_name,
   (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
   ) user_id,
   TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
   DECODE(arm.action,'I','Add','U','Update','-') action,
   acm.old_value,
   acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.er_order_receipant_info
    WHERE FK_RECEIPANT = arm.module_id
    and arm.table_name   = 'CB_RECEIPANT_INFO' 
   ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT FK_RECEIPANT FROM eres.er_order_receipant_info
  )
 AND arm.table_name       = 'CB_RECEIPANT_INFO')
 AND acm.column_name NOT IN ('PK_RECEIPANT','FK_PERSON_ID','REC_WEIGHT','ENTRY_DATE','GUID','REC_HEIGHT','RID','DELETEDFLAG','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY')
 UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT FK_CORD_ID
    FROM eres.CB_CORD_COMPLETE_REQ_INFO
    WHERE PK_CORD_COMPLETE_REQINFO = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT PK_CORD_COMPLETE_REQINFO FROM eres.CB_CORD_COMPLETE_REQ_INFO
    )
  AND arm.table_name   = 'CB_CORD_COMPLETE_REQ_INFO')
  AND acm.column_name IN ('COMPLETE_REQ_INFO_FLAG')
  ORDER BY 9 DESC);
 /
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."TIMESTAMP"
IS
  'Time of action taken';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  
  COMMENT ON TABLE "VDA"."ETVDA_ORDERDETAILS_AUDIT"
IS
  'This view provides Order Details audit details ';
  /
  
  
  
INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,1,1,'01_views.sql',sysdate,'ETVDA 1.0 Build 2')
/

commit
/
  