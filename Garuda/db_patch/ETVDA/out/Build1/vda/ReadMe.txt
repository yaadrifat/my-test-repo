Note: This Install script must be run on the DB server itself.

1) Windows:
   Open Install.bat in an editor and set all the variables mentioned near the top.

   Linux:
   Open a command prompt and cd into the directory that has Install.sh script.
   Then run this command:
   > chmod 755 Install.sh
   Open Install.sh in an editor and set all the variables mentioned near the top.
   
   The variable DB_TBS is for the tablespace directory in which the vda ora files
   are to be created. In this initial script, VDA_PWD is set to "vda123" and is used
   as such.

2) Open a command prompt, and cd into the directory where Install.bat/Install.sh lies.
   Execute the script by typing

   Windows:
   > .\Install.bat
   Linux:
   > ./Install.sh

   The log will be in out.txt.
   If you see "All done", the script has completed successfully.

3) If an error occurs, first rename the file out.txt to something else like out1.txt.

3a) View that output file to figure out where the error occurred. For the schema
    in which the error occurred, you need to manually fix the SQL script issue and 
    execute. Also, the rest of SQL scripts in that schema have to be run manually.

3b) In the directory in which this ReadMe file lies, the done_* files are supposed
    to be created in this order:
    done_newuserVDA.txt > done_eres.txt > done_esch.txt > done_epat.txt > done_vda.txt
    After you manually fix a schema, create an empty file with one of these names.
    You can re-execute the Install script; it will continue from the schema after 
    the schema you just fixed.
