#!/bin/bash

# Execute this script on the DB server itself.
# Set DB_TBS to the tablespace directory.
# VDA_PWD is set to vda123 by default.

# -- Set variables begins
export DB_SID=""
export DB_TBS=""
export SYS_PWD=""
export ERES_PWD=""
export ESCH_PWD=""
export EPAT_PWD=""
# -- Set variables ends

echo "===Starting VDA DB script"
echo "===Starting VDA DB script" 1>&2> out.txt

if [ ! -f done_newuserVDA.txt ];
then
  echo "===Running newuserVDA...";
  echo "===Running newuserVDA..." 1>&2>> out.txt;
  export SYS_LOGIN="sys/$SYS_PWD@$DB_SID as SYSDBA";
  sqlplus $SYS_LOGIN @1_newuserVDA/1_sys_VDAuser.SQL $DB_TBS 1>&2>> out.txt;
  if [ ! -f done_newuserVDA.txt ];
  then
    echo "An error occurred in newuserVDA.";
    exit -1;
  fi
  echo "Created VDA user successfully";
fi

if [ ! -f done_eres.txt ];
then
  echo "===Running eres...";
  echo "===Running eres..." 1>&2>> out.txt;
  sqlplus eres/$ERES_PWD@$DB_SID @run_eres.sql 1>&2>> out.txt;
  if [ ! -f done_eres.txt ];
  then
    echo "An error occurred in eres. Exiting...";
    exit -1;
  fi
fi

if [ ! -f done_esch.txt ];
then
  echo "===Running esch...";
  echo "===Running esch..." 1>&2>> out.txt;
  sqlplus esch/$ESCH_PWD@$DB_SID @run_esch.sql 1>&2>> out.txt
  if [ ! -f done_esch.txt ];
  then
    echo "An error occurred in esch. Exiting...";
    exit -1;
  fi
fi

if [ ! -f done_epat.txt ];
then
  echo "===Running epat...";
  echo "===Running epat..." 1>&2>> out.txt;
  sqlplus epat/$EPAT_PWD@$DB_SID @run_epat.sql 1>&2>> out.txt
  if [ ! -f done_epat.txt ];
  then
    echo "An error occurred in epat. Exiting...";
    exit -1;
  fi
fi

if [ ! -f done_vda.txt ];
then
  echo "===Running vda...";
  echo "===Running vda..." 1>&2>> out.txt
  sqlplus vda/vda123@$DB_SID @run_vda.sql 1>&2>> out.txt
  if [ ! -f done_vda.txt ];
  then
    echo "An error occurred in vda. Exiting...";
    exit -1;
  fi
fi

echo "===All done";
echo "===All done" 1>&2>> out.txt;

