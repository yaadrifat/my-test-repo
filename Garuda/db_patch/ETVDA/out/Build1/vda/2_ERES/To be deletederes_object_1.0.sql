-----------Functions to be referenced with first VDA release for EMTRAX -------
create or replace
FUNCTION        "F_GET_CBU_STATUS" (p_id VARCHAR2)
RETURN VARCHAR2 IS
  v_PK_CBU_STATUS VARCHAR2(200);
BEGIN
        v_PK_CBU_STATUS:='';       
	 SELECT CBU_STATUS_DESC INTO v_PK_CBU_STATUS FROM CB_CBU_STATUS WHERE PK_CBU_STATUS=p_id;
   IF v_PK_CBU_STATUS IS NULL THEN
   v_PK_CBU_STATUS:='';
   END IF;
  RETURN v_PK_CBU_STATUS ;
END ;

/

create or replace
FUNCTION        "F_GET_CODELSTDESC" (p_id NUMBER)
RETURN VARCHAR
IS
v_codelst_desc VARCHAR2(200);
BEGIN
	 SELECT codelst_desc INTO v_codelst_desc FROM ER_CODELST WHERE pk_codelst = p_id;
     RETURN v_codelst_desc ;
END ;

 /
 
 create or replace
FUNCTION        "F_GET_DEFFERED_STATUS" (p_id number)
RETURN VARCHAR2 IS
  v_CORD_CBU_STATUS number(5);
  v_CBU_STATUS number(5);
  
  v_DEFFERED_STATUS VARCHAR2(4);
  v_COUNT number(5);
BEGIN
v_DEFFERED_STATUS := 'NO';

select PK_CBU_STATUS into v_CBU_STATUS from CB_CBU_STATUS where trim(CBU_STATUS_CODE) ='DD' and trim(CODE_TYPE) = 'Response';

  
   if p_id = v_CBU_STATUS then
      v_DEFFERED_STATUS := 'Yes';
     
   else
      v_DEFFERED_STATUS := 'No';
     
   end if;
   RETURN v_DEFFERED_STATUS ;
END ;

/


create or replace
FUNCTION        "F_GET_ER_LABTEST_DESC" (P_ID VARCHAR2)
RETURN VARCHAR2
IS
  V_ER_LABTEST VARCHAR2(200);
BEGIN
	 SELECT LABTEST_NAME INTO V_ER_LABTEST FROM ER_LABTEST WHERE PK_LABTEST = P_ID;
  RETURN V_ER_LABTEST ;
END ;

/

create or replace
FUNCTION "F_GET_POST_THAW" (pid varchar2)
RETURN VARCHAR2 IS
v_post_thaw_value VARCHAR2(50);
  
BEGIN
	 

    case pid
          when  '1'  then v_post_thaw_value := '2';  
          when  '2' then v_post_thaw_value := '3';
          when  '3' then v_post_thaw_value := '4';
          when  '4' then v_post_thaw_value := '5';
          when  '5' then v_post_thaw_value := '6';
          when  '6' then v_post_thaw_value := '7';
          when 'post_proc_thaw' then v_post_thaw_value := '1';
   else v_post_thaw_value := '';
    end case;
  
        RETURN v_post_thaw_value;
END ;

/

CREATE OR REPLACE
FUNCTION        "F_GET_USER_NAME" (P_ID VARCHAR2)
RETURN VARCHAR2
IS
  V_ER_LABTEST VARCHAR2(200);
BEGIN

  SELECT ER_USER.USR_LOGNAME INTO V_ER_LABTEST  FROM ER_USER WHERE PK_USER = P_ID;	
  
  RETURN V_ER_LABTEST ;
END ;


/

create or replace
FUNCTION        "F_LABTESTNAME" (lab_id VARCHAR2)
RETURN VARCHAR2
IS
  v_labtest_name VARCHAR2(200);
BEGIN
	 SELECT labtest_name INTO v_labtest_name FROM ER_LABTEST WHERE pk_labtest = lab_id;
  RETURN v_labtest_name ;
END ;

/