declare

    v_sql Varchar2(4000);
    
begin
    

    for k in (select object_name from all_objects where object_type='TABLE' and object_name not like 'BIN$%' and owner in ('ERES') )
    loop
    
            v_sql := 'grant select on ERES.' || k.object_name || ' to VDA' ;
            
             EXECUTE IMMEDIATE v_sql;
             
    end loop;
    
   
    
    for k in (select object_name from all_objects where object_type in ( 'PACKAGE' )and object_name not like 'BIN$%' and owner in ('ERES') )
    loop
    
            v_sql := 'grant execute on ERES.' || k.object_name || ' to VDA' ;
            
             EXECUTE IMMEDIATE v_sql;
             
    end loop;
    
   
     
end;
/

