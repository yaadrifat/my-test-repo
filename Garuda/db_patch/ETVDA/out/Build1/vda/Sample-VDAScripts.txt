--This SQL provides the list of all cords where complete required informantion is incomplete
Select * from ETVDA_CBU_CRI_VIEW_ALL where CBU_STATUS = 'Available' and CRI = 'NO';

--This SQL provides the status of cords where Viability test result is < 85%
Select * from ETVDA_CB_CORD_STATUS_ALL cordStatus, ETVDA_PATLAB_INFO_ALL patlabs  where cordStatus.DEFERRAL_STATUS = 'Yes' and patlabs.LABTEST_NAME ='Viability'
and patlabs.TEST_RESULT < 85 ;

--Lab Test results = Final Product Volume for all cords. This includes the all timing of test. 
Select cord.cbb_id, cord.cbu_registry_id, cord.cbu_local_id, CORD.ENTRY_DATE, patlabs.labtest_name, patlabs.test_result from ETVDA_CB_CORD_INFO_ALL Cord , ETVDA_PATLAB_INFO_ALL patlabs where patlabs.pk_cord = cord.pk_cord 
and patlabs.LABTEST_NAME = 'Final Product Volume';

--All Ineligible cords shipped between - Jan 1, 2012 to Dec 31, 2012
Select  *  from ETVDA_ENT_STUS_RSN where eligible_status='Ineligible' and collection_date < '25-MAY-2005' and shipdate between '1-JAN-2014' and '31-DEC-2014';

---All Cords Syphillis results - Heavy Query
Select * from ETVDA_CORD_SYPHILLIS_STS_ALL SYPHILLIS, ETVDA_CB_CORD_INFO_ALL cord where cord.collection_date >  '01-OCT-2011' and cord.collection_date < '20-OCT-2011' and cord.pk_cord = syphillis.pk_cord;

--Al Cords having Processing procedure = sand and entered between Jan 3, 10 to Jan 06, 2010. 
Select * from ETVDA_CBB_PRO_PROC_ALL where PROCESSING_PROCEDURE like '%sand%' and ENTRY_DATE between '03-JAN-10' and '06-JAN-10';