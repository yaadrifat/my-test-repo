--------------------------Readme file to create the VDA user and views------------------------------

1. The purpose of this release package is to create a user VDA to present the data stored in ERES/ESCH/EPAT users schema. 
2. The VDA user will be created in the same Oracle global database where ERES/ESCH/EPAT schema exist.


Given below are the steps to be followed for this process:
Step:1
   a. There are central database objects referenced by VDA views and these database objects should be present in the respective schema(eres,esch & epat).
   b. The above mentioned functions are already released in the  ET028HotFix#1. So before execution of this VDA related Package, the eResearch database should be upgraded to ET028HotFix1.
Step:2
Creation of  VDA user.For this follow the instructions from ReadMe.txt under :\ 02192013_releaseETVDA1.0\VDA\...
Step:3
After successful creation of VDA user, you can test the Views in this schema by using sample queries mentioned in the "Sample-VDAScripts.txt"  underfollowing path:\ 02192013_releaseETVDA1.0\VDA\...


 


	