CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_QUERY_BUILDER_DATA" ("PK_CORD", "REGISTRY_ID", "REGISTRY_MATERNAL_ID", "CORD_LOCAL_CBU_ID", "MATERNAL_LOCAL_ID", "CORD_ISBI_DIN_CODE", "PK_SITE", "SITE_ID", "PATIENT_ID", "CORD_ID_NUMBER_ON_CBU_BAG", "CORD_HISTORIC_CBU_ID", "REQ_TYPE", "ORDER_SAMPLE_AT_LAB", "SHIP_DATE", "ORDER_OPEN_DATE", "FK_SHIPMENT_TYPE", "INFUSION_DATE", "CURRENT_DIAGNOSIS", "TRANS_SITE_NAME", "TRANS_SITE_ID", "ORDER_TYPE", "SPEC_COLLECTION_DATE", "CORD_CREATION_DATE", "CORD_REGISTRATION_DATE", "FUNDED_CBU", "CORD_NMDP_STATUS", "FK_CORD_CBU_STATUS", "FK_CORD_CBU_ELIGIBLE_STATUS", "FK_CORD_CBU_LIC_STATUS", "CORD_BABY_BIRTH_DATE", "FK_CORD_BACT_CUL_RESULT", "FK_CORD_FUNGAL_CUL_RESULT", "HEMOGLOBIN_SCRN", "FK_CBB_PROCEDURE", "FK_CODELST_RACE", "NMDP_RACE_ID", "FK_SPECIMEN_ID", "UNLICENSED_DESC", "INELIG_INCOMP_DESC", "RACE_DESC", "FUNDING_CATEGORY", "CORD_SEARCHABLE")
AS
  SELECT DISTINCT(C.Pk_Cord),
    C.Cord_Registry_Id,
    C.Registry_Maternal_Id,
    C.Cord_Local_Cbu_Id,
    C.Maternal_Local_Id,
    C.Cord_Isbi_Din_Code,
    SITE.PK_SITE ,
    Site.Site_Id,
    Cb_Rec_Info.Receipant_Id,
    C.Cord_Id_Number_On_Cbu_Bag ,
    C.Cord_Historic_Cbu_Id,
    ord.order_type Req_Type,
    ORD.ORDER_SAMPLE_AT_LAB,
    SHIPMENT.sch_shipment_date SHIP_DATE,
    HDR.ORDER_OPEN_DATE ORDER_OPEN_DATE,
    SHIPMENT.FK_SHIPMENT_TYPE,
    ORD.INFUSION_DATE INFUSION_DATE,
    CB_REC_INFO.CURRENT_DIAGNOSIS CURRENT_DIAGNOSIS,
    TRANS_SITE.SITE_NAME,
    TRANS_SITE.SITE_ID,
    Ord.Order_Type ,
    spec.SPEC_COLLECTION_DATE,
    C.CORD_CREATION_DATE,
    C.CORD_REGISTRATION_DATE,
    C.FUNDED_CBU,
    C.CORD_NMDP_STATUS,
    C.FK_CORD_CBU_STATUS,
    C.Fk_Cord_Cbu_Eligible_Status,
    C.Fk_Cord_Cbu_Lic_Status,
    C.Cord_Baby_Birth_Date,
    C.Fk_Cord_Bact_Cul_Result,
    C.Fk_Cord_Fungal_Cul_Result,
    C.Hemoglobin_Scrn,
    C.Fk_Cbb_Procedure,
    C.Fk_Codelst_Race,
    C.Nmdp_Race_Id,
    C.FK_SPECIMEN_ID,
    C.FK_CORD_CBU_UNLICENSED_REASON,
    C.FK_CORD_CBU_INELIGIBLE_REASON,
    '' RACE_DESC,
    C.FK_FUND_CATEG,
	C.CORD_SEARCHABLE
  FROM ERES.Cb_Cord C
  LEFT JOIN Eres.Er_Specimen Spec
  ON C.Fk_Specimen_Id=Spec.Pk_Specimen
  LEFT OUTER JOIN Eres.Er_Order_Header Hdr
  ON (Hdr.Order_Entityid=C.Pk_Cord)
  LEFT OUTER JOIN Eres.Er_Order Ord
  ON (Ord.Fk_Order_Header=Hdr.Pk_Order_Header )
  LEFT OUTER JOIN Eres.Er_Order_Receipant_Info Order_Rec_Info
  ON (Order_Rec_Info.Fk_Order_Id=Ord.Pk_Order)
  LEFT OUTER JOIN Eres.Cb_Receipant_Info Cb_Rec_Info
  ON(Order_Rec_Info.Fk_Receipant=Cb_Rec_Info.Pk_Receipant)
  LEFT OUTER JOIN Eres.Cb_Shipment Shipment
  ON (Shipment.Fk_Order_Id= Ord.Pk_Order )
  LEFT OUTER JOIN Eres.Er_Site Trans_Site
  ON ( Cb_Rec_Info.Trans_Center_Id=Trans_Site.Pk_Site)
  LEFT JOIN Eres.Er_Site Site
  ON C.Fk_Cbb_Id=Site.Pk_Site;
  /
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."PK_CORD"
IS
  'Primary key of cb_cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."REGISTRY_ID"
IS
  'Registry id of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."REGISTRY_MATERNAL_ID"
IS
  'Maternal Registry ID for Cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_LOCAL_CBU_ID"
IS
  'Local CBU ID of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."MATERNAL_LOCAL_ID"
IS
  'Maternal Local ID for Cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_ISBI_DIN_CODE"
IS
  'ISBT CODE for the Cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."PK_SITE"
IS
  'This column identifies the site/organization.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."SITE_ID"
IS
  'This column identifies the site/organization.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."PATIENT_ID"
IS
  'This coloumn stores Patient ID.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_ID_NUMBER_ON_CBU_BAG"
IS
  'UNIQUE ID Number given over Bag';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_HISTORIC_CBU_ID"
IS
  'This coloumn stores Historical CBU ID.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."REQ_TYPE"
IS
  'This coloumn stores Request Type.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."ORDER_SAMPLE_AT_LAB"
IS
  'This coloumn stores Order Sample at lab.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."SHIP_DATE"
IS
  'This coloumn stores Shipment Date.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."ORDER_OPEN_DATE"
IS
  'This coloumn stores Open Date of Order.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FK_SHIPMENT_TYPE"
IS
  'This coloumn stores type of Shipment.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."INFUSION_DATE"
IS
  'This coloumn stores infusion date.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CURRENT_DIAGNOSIS"
IS
  'This coloumn stores current diagonisis.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."TRANS_SITE_NAME"
IS
  'This coloumn stores Site Name.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."TRANS_SITE_ID"
IS
  'This coloumn stores Site ID.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."ORDER_TYPE"
IS
  'This coloumn stores Order Type.';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."SPEC_COLLECTION_DATE"
IS
  'Collection date of the specimen of the cord';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_CREATION_DATE"
IS
  'The date on which this cord is created in the system. ';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."FUNDING_CATEGORY"
IS
  'This Column used to store the funding category';
  COMMENT ON COLUMN "VDA"."ETVDA_QUERY_BUILDER_DATA"."CORD_SEARCHABLE"
IS
  'This Column will store wheather the cord is searchable or not';
  
  INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,9,1,'01_ETVDA_QUERY_BUILDER_DATA.sql',sysdate,'ETVDA 1.0 Build 9');

commit;