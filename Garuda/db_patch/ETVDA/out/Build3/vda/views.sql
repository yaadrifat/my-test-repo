--EmTrax views for Version ET028.1- Release Date 02-27-2013 
CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ADDITIONAL_ID_ALL" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "ADDITIONAL_ID", "ADDITIONAL_ID_TYPE", "ADDITIONAL_ID_DESCRIPTION", "PK_CORD")
AS
  SELECT site.site_id,
    cord.cord_registry_id ,
    cord.cord_local_cbu_id,
    additional.CB_ADDITIONAL_ID,
    eres.f_get_codelstDesc(additional.fk_cb_additional_id_type),
    additional.cb_additional_id_desc,
    cord.pk_cord
  FROM eres.cb_cord cord,
    eres.CB_ADDITIONAL_IDS additional,
    eres.er_site site
  WHERE cord.pk_cord          = additional.entity_id
  AND cord.fk_cbb_id          = site.PK_SITE
  AND additional.deletedflag IS NULL;
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."CBB_ID"
IS
  'CBB id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."CBU_LOCAL_ID"
IS
  'Local CBU ID of the the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."ADDITIONAL_ID"
IS
  'Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."ADDITIONAL_ID_TYPE"
IS
  'Type of Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."ADDITIONAL_ID_DESCRIPTION"
IS
  'Description for addtional id';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_ALL"."PK_CORD"
IS
  'Primary key of cb_cord.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_ADDITIONAL_ID_ALL"
IS
  'This view provides Additional id information of cord';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ADDITIONAL_ID_PRG" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "ADDITIONAL_ID", "ADDITIONAL_ID_TYPE", "ADDITIONAL_ID_DESCRIPTION", "PK_CORD")
AS
  SELECT site.site_id,
    cord.cord_registry_id ,
    cord.cord_local_cbu_id,
    additional.CB_ADDITIONAL_ID,
    eres.f_get_codelstDesc(additional.fk_cb_additional_id_type),
    additional.cb_additional_id_desc,
    cord.pk_cord
  FROM eres.cb_cord cord,
    eres.CB_ADDITIONAL_IDS additional,
    eres.er_site site
  WHERE cord.pk_cord          = additional.entity_id
  AND cord.fk_cbb_id          = site.PK_SITE
  AND additional.deletedflag IS NULL
  AND CORD.CORD_SEARCHABLE    = '0';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."CBB_ID"
IS
  'CBB id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."CBU_LOCAL_ID"
IS
  'Local CBU ID of the the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."ADDITIONAL_ID"
IS
  'Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."ADDITIONAL_ID_TYPE"
IS
  'Type of Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."ADDITIONAL_ID_DESCRIPTION"
IS
  'Description for addtional id';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_PRG"."PK_CORD"
IS
  'Primary key of cb_cord.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_ADDITIONAL_ID_PRG"
IS
  'This view provides Additional id information of cords(Saved In Progress)';
  /
  
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ADDITIONAL_ID_SBMT" ("CBB_ID", "CBU_REGISTRY_ID", "CBU_LOCAL_ID", "ADDITIONAL_ID", "ADDITIONAL_ID_TYPE", "ADDITIONAL_ID_DESCRIPTION", "PK_CORD")
AS
  SELECT site.site_id,
    cord.cord_registry_id ,
    cord.cord_local_cbu_id,
    additional.CB_ADDITIONAL_ID,
    eres.f_get_codelstDesc(additional.fk_cb_additional_id_type),
    additional.cb_additional_id_desc,
    cord.pk_cord
  FROM eres.cb_cord cord,
    eres.CB_ADDITIONAL_IDS additional,
    eres.er_site site
  WHERE cord.pk_cord          = additional.entity_id
  AND additional.deletedflag IS NULL
  AND cord.fk_cbb_id          = site.PK_SITE
  AND CORD.CORD_SEARCHABLE    = '1';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."CBB_ID"
IS
  'CBB id of cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."CBU_LOCAL_ID"
IS
  'Local CBU ID of the the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."ADDITIONAL_ID"
IS
  'Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."ADDITIONAL_ID_TYPE"
IS
  'Type of Addtional Id  for the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."ADDITIONAL_ID_DESCRIPTION"
IS
  'Description for addtional id';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ADDITIONAL_ID_SBMT"."PK_CORD"
IS
  'Primary key of cb_cord.';
  /
  COMMENT ON TABLE "VDA"."ETVDA_ADDITIONAL_ID_SBMT"
IS
  'This view provides Additional id information of cords(Submitted)';
  /
   
  
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_HLA_INFO_ALL" ("CBB_ID", "CBU_REGISTRY_ID", "NO_OF_SEGMENTS", "HLA_VALUE_TYPE1", "HLA_VALUE_TYPE2", "HLA_DESC", "HLA_SOURCE", "RECEIVED_DATE", "PK_CORD", "PK_HLA", "PK_ENTITY_SAMPLES")
AS
  SELECT SITE.SITE_ID,
    C.CORD_REGISTRY_ID,
    SMPLE.NO_OF_SEG_AVAIL,
    EC1.GENOMIC_FORMAT AS HLA_TYPE1,
    EC2.GENOMIC_FORMAT AS HLA_TYPE2,
    ERES.F_CODELST_DESC(H.FK_HLA_CODE_ID),
    ERES.F_CODELST_DESC(H.FK_SOURCE),
    H.HLA_RECEIVED_DATE,
    C.PK_CORD,
    H.PK_HLA,
    SMPLE.PK_ENTITY_SAMPLES
  FROM ERES.CB_HLA H,
    ERES.CB_CORD C,
    ERES.ER_SITE SITE,
    ERES.CB_ANTIGEN_ENCOD EC1,
    ERES.CB_ANTIGEN_ENCOD EC2,
    ERES.CB_ENTITY_SAMPLES SMPLE
  WHERE H.FK_HLA_ANTIGENEID1=EC1.FK_ANTIGEN (+)
  AND ( EC1.VERSION         =
    (SELECT MAX(EC1S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC1S
    WHERE EC1.FK_ANTIGEN=EC1S.FK_ANTIGEN
    )
  OR EC1.VERSION          IS NULL)
  AND H.FK_HLA_ANTIGENEID2 = EC2.FK_ANTIGEN (+)
  AND (EC2.VERSION         =
    (SELECT MAX(EC2S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC2S
    WHERE EC2.FK_ANTIGEN=EC2S.FK_ANTIGEN
    )
  OR EC2.VERSION                                  IS NULL)
  AND H.ENTITY_ID                                  =C.PK_CORD
  AND lower(ERES.F_CODELST_DESC(H.ENTITY_TYPE))    ='cbu'
  AND SMPLE.ENTITY_ID                              =C.PK_CORD
  AND lower(ERES.F_CODELST_DESC(SMPLE.ENTITY_TYPE))='cbu'
  AND C.FK_CBB_ID                                  = SITE.PK_SITE;
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."CBB_ID"
IS
  'CBB ID OF CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."NO_OF_SEGMENTS"
IS
  'This column will store the No of Segment Available.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."HLA_VALUE_TYPE1"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 1.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."HLA_VALUE_TYPE2"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 2.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."HLA_DESC"
IS
  'Description of Code Id for HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."HLA_SOURCE"
IS
  'Description of Source for HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."RECEIVED_DATE"
IS
  'HLA Received date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."PK_CORD"
IS
  'Primary key of table CB_CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."PK_HLA"
IS
  'Primary key of table CB_HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_ALL"."PK_ENTITY_SAMPLES"
IS
  'Primary key of table CB_ENTITY_SAMPLES';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBU_HLA_INFO_ALL"
IS
  'This view provides HLA Information of all cords ';
  /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_HLA_INFO_PRG" ("CBB_ID", "CBU_REGISTRY_ID", "NO_OF_SEGMENTS", "HLA_VALUE_TYPE1", "HLA_VALUE_TYPE2", "HLA_DESC", "HLA_SOURCE", "RECEIVED_DATE", "PK_CORD", "PK_HLA", "PK_ENTITY_SAMPLES")
AS
  SELECT SITE.SITE_ID,
    C.CORD_REGISTRY_ID,
    SMPLE.NO_OF_SEG_AVAIL,
    EC1.GENOMIC_FORMAT AS HLA_TYPE1,
    EC2.GENOMIC_FORMAT AS HLA_TYPE2,
    ERES.F_CODELST_DESC(H.FK_HLA_CODE_ID),
    ERES.F_CODELST_DESC(H.FK_SOURCE),
    H.HLA_RECEIVED_DATE,
    C.PK_CORD,
    H.PK_HLA,
    SMPLE.PK_ENTITY_SAMPLES
  FROM ERES.CB_HLA H,
    ERES.CB_CORD C,
    ERES.ER_SITE SITE,
    ERES.CB_ANTIGEN_ENCOD EC1,
    ERES.CB_ANTIGEN_ENCOD EC2,
    ERES.CB_ENTITY_SAMPLES SMPLE
  WHERE H.FK_HLA_ANTIGENEID1=EC1.FK_ANTIGEN (+)
  AND ( EC1.VERSION         =
    (SELECT MAX(EC1S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC1S
    WHERE EC1.FK_ANTIGEN=EC1S.FK_ANTIGEN
    )
  OR EC1.VERSION          IS NULL)
  AND H.FK_HLA_ANTIGENEID2 = EC2.FK_ANTIGEN (+)
  AND (EC2.VERSION         =
    (SELECT MAX(EC2S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC2S
    WHERE EC2.FK_ANTIGEN=EC2S.FK_ANTIGEN
    )
  OR EC2.VERSION                                  IS NULL)
  AND H.ENTITY_ID                                  =C.PK_CORD
  AND lower(ERES.F_CODELST_DESC(H.ENTITY_TYPE))    ='cbu'
  AND SMPLE.ENTITY_ID                              =C.PK_CORD
  AND lower(ERES.F_CODELST_DESC(SMPLE.ENTITY_TYPE))='cbu'
  AND C.FK_CBB_ID                                  = SITE.PK_SITE
  AND C.CORD_SEARCHABLE                            = '0';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."CBB_ID"
IS
  'CBB ID OF CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."NO_OF_SEGMENTS"
IS
  'This column will store the No of Segment Available.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."HLA_VALUE_TYPE1"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 1.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."HLA_VALUE_TYPE2"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 2.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."HLA_DESC"
IS
  'Description of Code Id for HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."HLA_SOURCE"
IS
  'Description of Source for HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."RECEIVED_DATE"
IS
  'HLA Received date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."PK_CORD"
IS
  'Primary key of table CB_CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."PK_HLA"
IS
  'Primary key of table CB_HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_PRG"."PK_ENTITY_SAMPLES"
IS
  'Primary key of table CB_ENTITY_SAMPLES';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBU_HLA_INFO_PRG"
IS
  'This view provides HLA Information of �Saved In Progress� cords';
  /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_HLA_INFO_SUBMT" ("CBB_ID", "CBU_REGISTRY_ID", "NO_OF_SEGMENTS", "HLA_VALUE_TYPE1", "HLA_VALUE_TYPE2", "HLA_DESC", "HLA_SOURCE", "RECEIVED_DATE", "PK_CORD", "PK_HLA", "PK_ENTITY_SAMPLES")
AS
  SELECT SITE.SITE_ID,
    C.CORD_REGISTRY_ID,
    SMPLE.NO_OF_SEG_AVAIL,
    EC1.GENOMIC_FORMAT AS HLA_TYPE1,
    EC2.GENOMIC_FORMAT AS HLA_TYPE2,
    ERES.F_CODELST_DESC(H.FK_HLA_CODE_ID),
    ERES.F_CODELST_DESC(H.FK_SOURCE),
    H.HLA_RECEIVED_DATE,
    C.PK_CORD,
    H.PK_HLA,
    SMPLE.PK_ENTITY_SAMPLES
  FROM ERES.CB_HLA H,
    ERES.CB_CORD C,
    ERES.ER_SITE SITE,
    ERES.CB_ANTIGEN_ENCOD EC1,
    ERES.CB_ANTIGEN_ENCOD EC2,
    ERES.CB_ENTITY_SAMPLES SMPLE
  WHERE H.FK_HLA_ANTIGENEID1=EC1.FK_ANTIGEN (+)
  AND ( EC1.VERSION         =
    (SELECT MAX(EC1S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC1S
    WHERE EC1.FK_ANTIGEN=EC1S.FK_ANTIGEN
    )
  OR EC1.VERSION          IS NULL)
  AND H.FK_HLA_ANTIGENEID2 = EC2.FK_ANTIGEN (+)
  AND (EC2.VERSION         =
    (SELECT MAX(EC2S.VERSION)
    FROM ERES.CB_ANTIGEN_ENCOD EC2S
    WHERE EC2.FK_ANTIGEN=EC2S.FK_ANTIGEN
    )
  OR EC2.VERSION                                  IS NULL)
  AND H.ENTITY_ID                                  =C.PK_CORD
  AND lower(ERES.F_CODELST_DESC(H.ENTITY_TYPE))    ='cbu'
  AND SMPLE.ENTITY_ID                              =C.PK_CORD
  AND lower(ERES.F_CODELST_DESC(SMPLE.ENTITY_TYPE))='cbu'
  AND C.FK_CBB_ID                                  = SITE.PK_SITE
  AND C.CORD_SEARCHABLE                            = '1';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."CBB_ID"
IS
  'CBB ID OF CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."CBU_REGISTRY_ID"
IS
  'Registry id of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."NO_OF_SEGMENTS"
IS
  'This column will store the No of Segment Available.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."HLA_VALUE_TYPE1"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 1.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."HLA_VALUE_TYPE2"
IS
  'This column will store the Genomic Format of HLA antigen for HLA_TYPE 2.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."HLA_DESC"
IS
  'escription of Code Id for HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."HLA_SOURCE"
IS
  'Description of Description for HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."RECEIVED_DATE"
IS
  'HLA Received date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."PK_CORD"
IS
  'Primary key of table CB_CORD';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."PK_HLA"
IS
  'Primary key of table CB_HLA';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"."PK_ENTITY_SAMPLES"
IS
  'Primary key of table CB_ENTITY_SAMPLES';
  /
  COMMENT ON TABLE "VDA"."ETVDA_CBU_HLA_INFO_SUBMT"
IS
  'This view provides HLA Information of �Submitted� cords';
  /
  
  -----------------------
  
  
   CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CB_RECEIPANT_INFO" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_RECEIPANT", "RECEIPANT_ID") AS 
  SELECT acm.column_name,
   acm.column_display_name,
   (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
   ) user_id,
   TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
   DECODE(arm.action,'I','Add','U','Update','-') action,
   acm.old_value,
   acm.new_value,
    arm.table_name,
    arm.timestamp, rec.pk_receipant, rec.receipant_id
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm, eres.cb_receipant_info rec
  WHERE acm.fk_row_id = arm.pk_row_id
  AND arm.module_id = rec.pk_receipant
 AND arm.table_name       = 'CB_RECEIPANT_INFO'
 AND acm.column_name NOT IN ('PK_RECEIPANT','FK_PERSON_ID','REC_WEIGHT','ENTRY_DATE','GUID','REC_HEIGHT','RID','DELETEDFLAG','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY');
  /
   COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO"."PK_RECEIPANT"
IS
  'Primary key of cb_receipant_info table';
  /
    COMMENT ON COLUMN "VDA"."ETVDA_CB_RECEIPANT_INFO"."RECEIPANT_ID"
IS
  'ID of Recipient';
  /
 
  COMMENT ON TABLE "VDA"."ETVDA_CB_RECEIPANT_INFO"
IS
  'This view contains the information about the Recipient';
  / 
  
  
  
    CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ER_ORDER_USERS" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_ORDER_USER", "USER_LOGINID") AS 
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp, usr.pk_order_user, usr.user_loginid
  FROM eres.audit_column_module acm, eres.er_order_users usr,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND arm.module_id = usr.pk_order_user
  AND arm.table_name       = 'ER_ORDER_USERS'
  AND acm.column_name NOT IN ('RID','DELETEDFLAG','USER_COMMENTS','PK_ORDER_USER','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY');
  /
  
     COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS"."PK_ORDER_USER"
IS
  'Primary key of er_order_users table';
  /
    COMMENT ON COLUMN "VDA"."ETVDA_ER_ORDER_USERS"."USER_LOGINID"
IS
  'Login ID of User';
  /
 
  COMMENT ON TABLE "VDA"."ETVDA_ER_ORDER_USERS"
IS
  'This view contains the information about the Order Users';
  /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD") AS 
  SELECT ACM.COLUMN_NAME,
    ACM.COLUMN_DISPLAY_NAME,
    (SELECT USR_LOGNAME FROM eres.ER_USER WHERE PK_USER = ARM.USER_ID
    ) USER_ID,
    TO_CHAR(ARM.TIMESTAMP,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(ARM.ACTION,'I','Add','U','Update','-') ACTION,
    ACM.OLD_VALUE,
    ACM.NEW_VALUE,
    ARM.TABLE_NAME,
    ARM.TIMESTAMP,
    (SELECT fk_cord_id
    FROM eres.CB_CORD_MINIMUM_CRITERIA
    WHERE PK_CORD_MINIMUM_CRITERIA = ARM.MODULE_ID
    ) pk_cord
  FROM eres.AUDIT_ROW_MODULE ARM ,
    eres.AUDIT_COLUMN_MODULE ACM
  WHERE ACM.FK_ROW_ID = ARM.PK_ROW_ID
  AND (ARM.MODULE_ID IN
    (SELECT PK_CORD_MINIMUM_CRITERIA FROM eres.CB_CORD_MINIMUM_CRITERIA
    )
  AND ARM.TABLE_NAME = 'CB_CORD_MINIMUM_CRITERIA')
  UNION
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.CB_CORD_MINIMUM_CRITERIA
    WHERE PK_CORD_MINIMUM_CRITERIA IN
      (SELECT FK_MINIMUM_CRITERIA
      FROM eres.CB_CORD_TEMP_MINIMUM_CRITERIA
      WHERE PK_MINIMUM_CRITERIA_FIELD =ARM.MODULE_ID
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE ACM.FK_ROW_ID = ARM.PK_ROW_ID
  AND (ARM.MODULE_ID IN
    (SELECT PK_MINIMUM_CRITERIA_FIELD
    FROM eres.CB_CORD_TEMP_MINIMUM_CRITERIA
    WHERE FK_MINIMUM_CRITERIA in
      (SELECT PK_CORD_MINIMUM_CRITERIA FROM eres.CB_CORD_MINIMUM_CRITERIA
      )
    )
  AND ARM.TABLE_NAME = 'CB_CORD_TEMP_MINIMUM_CRITERIA')
  ORDER BY 9 DESC;
 /
 
 COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."TIMESTAMP"
IS
  'Time of action taken';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
 
  COMMENT ON TABLE "VDA"."ETVDA_MINCRITERIA_AUDIT_ALL"
IS
  'This view contains the information about the Minimum Criteria and shows all fields in addition to the fields being displayed over Audit Trail widget. ';
  / 
  
  
  
    CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ORDERDETAILS_AUDIT" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD") AS 
  select "COLUMN_NAME","COLUMN_DISPLAY_NAME","USER_ID","DATE_TIME","ACTION","OLD_VALUE","NEW_VALUE","TABLE_NAME","TIMESTAMP","PK_CORD" from (   SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT order_entityid
    FROM eres.er_order_header
    WHERE pk_order_header = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order_header FROM eres.er_order_header
    )
  AND arm.table_name   = 'ER_ORDER_HEADER')
  AND acm.column_name IN ('ORDER_OPEN_DATE','FK_SITE_ID','ORDER_REQUESTED_BY')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT order_entityid
    FROM eres.er_order_header
    WHERE pk_order_header IN
      (SELECT fk_order_header FROM eres.er_order WHERE pk_order = arm.module_id
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order
    FROM eres.er_order
    WHERE fk_order_header IN
      (SELECT pk_order_header FROM eres.er_order_header
      )
    AND arm.table_name = 'ER_ORDER'
    ))
  AND acm.column_name NOT IN ('RID','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','IP_ADD','PK_ORDER','ORDER_TYPE','FK_ORDER_HEADER', 'CBU_AVAIL_CONFIRM_FLAG','FK_ATTACHMENT','TASK_ID','TASK_NAME','DELETEDFLAG', 'CLINIC_INFO_CHECKLIST_STAT','ADDI_TEST_RESULT_AVAIL','CANCEL_CONFORM_DATE','CANCELED_BY','ACCPT_TO_CANCEL_REQ','FK_CASE_MANAGER','ORDER_NUMBER','ORDER_ENTITYID','ORDER_ENTITYTYPE','FK_ORDER_STATUS','ORDER_REMARK','ORDER_BY','FK_ORDER_ITEM_ID','FK_ORDER_TYPE','DATA_MODIFIED_FLAG','FINAL_REVIEW_TASK_FLAG','COMPLETE_REQ_INFO_TASK_FLAG')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.cb_shipment
    WHERE pk_shipment = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_shipment FROM eres.cb_shipment
    )
  AND arm.table_name       = 'CB_SHIPMENT')
  AND acm.column_name NOT IN ('PK_SHIPMENT','FK_ORDER_ID','FK_CORD_ID','RID','IP_ADD','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','DATA_MODIFIED_FLAG','FK_SHIPMENT_TYPE','CBB_ID','FK_CORD_ID','DELETEDFLAG')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.er_order_receipant_info
    WHERE pk_order_receipant_info = arm.module_id and 
    arm.table_name = 'ER_ORDER_RECEIPANT_INFO'
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order_receipant_info FROM eres.er_order_receipant_info
    )
  AND arm.table_name       = 'ER_ORDER_RECEIPANT_INFO')
  AND acm.column_name NOT IN ('PK_ORDER_RECEIPANT_INFO','FK_RECEIPANT','FK_ORDER_ID','FK_CORD_ID','RID','ENTITY_ID','CREATED_ON','CREATOR','LAST_MODIFIED_DATE','LAST_MODIFIED_BY','IP_ADD','NMDP_RID','NMDP_ID','LOCAL_ID','COOP_REF','TC_CDE','SCU_ACTIVATED_DATE','FK_SITE_ID')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM'),
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_notes
    WHERE pk_notes    = arm.module_id
    AND fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'progress'
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_notes
    FROM eres.cb_notes
    WHERE fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'progress'
      )
    )
  AND arm.table_name   = 'CB_NOTES')
  AND acm.column_name IN ('SUBJECT','KEYWORD','TAG_NOTE','NOTES_PROGRESS','FK_NOTES_CATEGORY','NOTE_VISIBLE','REQUEST_TYPE','REQUEST_DATE','AMENDED','NOTE_SEQ')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT FK_CORD_ID
    FROM eres.CB_CORD_COMPLETE_REQ_INFO
    WHERE PK_CORD_COMPLETE_REQINFO = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT PK_CORD_COMPLETE_REQINFO FROM eres.CB_CORD_COMPLETE_REQ_INFO
    )
  AND arm.table_name   = 'CB_CORD_COMPLETE_REQ_INFO')
  AND acm.column_name IN ('COMPLETE_REQ_INFO_FLAG')
  ORDER BY 9 DESC);
 /
 
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."TABLE_NAME"
IS
  'Audited table name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."TIMESTAMP"
IS
  'Time of action taken';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  
  COMMENT ON TABLE "VDA"."ETVDA_ORDERDETAILS_AUDIT"
IS
  'This view provides Order Details audit details ';
  /
  
  
  
  
  
    CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL" ("COLUMN_NAME", "COLUMN_DISPLAY_NAME", "USER_ID", "DATE_TIME", "ACTION", "OLD_VALUE", "NEW_VALUE", "TABLE_NAME", "TIMESTAMP", "PK_CORD") AS
    select "COLUMN_NAME","COLUMN_DISPLAY_NAME","USER_ID","DATE_TIME","ACTION","OLD_VALUE","NEW_VALUE","TABLE_NAME","TIMESTAMP","PK_CORD" from (   SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT order_entityid
    FROM eres.er_order_header
    WHERE pk_order_header = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order_header FROM eres.er_order_header
    )
  AND arm.table_name   = 'ER_ORDER_HEADER')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT order_entityid
    FROM eres.er_order_header
    WHERE pk_order_header IN
      (SELECT fk_order_header FROM eres.er_order WHERE pk_order = arm.module_id
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order
    FROM eres.er_order
    WHERE fk_order_header IN
      (SELECT pk_order_header FROM eres.er_order_header
      )
    AND arm.table_name = 'ER_ORDER'
    )) 
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.cb_shipment
    WHERE pk_shipment = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_shipment FROM eres.cb_shipment
    )
  AND arm.table_name       = 'CB_SHIPMENT')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT fk_cord_id
    FROM eres.er_order_receipant_info
    WHERE pk_order_receipant_info = arm.module_id and 
    arm.table_name = 'ER_ORDER_RECEIPANT_INFO'
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_order_receipant_info FROM eres.er_order_receipant_info
    )
  AND arm.table_name       = 'ER_ORDER_RECEIPANT_INFO')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM'),
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT entity_id
    FROM eres.cb_notes
    WHERE pk_notes    = arm.module_id
    AND fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'progress'
      )
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT pk_notes
    FROM eres.cb_notes
    WHERE fk_notes_type =
      (SELECT pk_codelst
      FROM eres.er_codelst
      WHERE codelst_type ='note_type'
      AND codelst_subtyp = 'progress'
      )
    )
  AND arm.table_name   = 'CB_NOTES')
  UNION ALL
  SELECT acm.column_name,
    acm.column_display_name,
    (SELECT usr_logname FROM eres.er_user WHERE pk_user = arm.user_id
    ) user_id,
    TO_CHAR(arm.timestamp,'Mon DD, YYYY HH:MI:SS AM') DATE_TIME,
    DECODE(arm.action,'I','Add','U','Update','-') action,
    acm.old_value,
    acm.new_value,
    arm.table_name,
    arm.timestamp,
    (SELECT FK_CORD_ID
    FROM eres.CB_CORD_COMPLETE_REQ_INFO
    WHERE PK_CORD_COMPLETE_REQINFO = arm.module_id
    ) pk_cord
  FROM eres.audit_column_module acm,
    eres.audit_row_module arm
  WHERE acm.fk_row_id = arm.pk_row_id
  AND (arm.module_id IN
    (SELECT PK_CORD_COMPLETE_REQINFO FROM eres.CB_CORD_COMPLETE_REQ_INFO
    )
  AND arm.table_name   = 'CB_CORD_COMPLETE_REQ_INFO') 
  ORDER BY 9 DESC);
  /
  
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."COLUMN_NAME"
IS
  'Column name of the the audited column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."COLUMN_DISPLAY_NAME"
IS
  'Column Display Name displays the UI Label name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."USER_ID"
IS
  'User Id gives who update, insert or delete the data';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."DATE_TIME"
IS
  'Gives time of action taken in the application date format';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."ACTION"
IS
  'Action performed on the application table (I, U, D)';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."OLD_VALUE"
IS
  'Old data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."NEW_VALUE"
IS
  'New data value of the affected column';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."TABLE_NAME"
IS
  'Audited table name';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."TIMESTAMP"
IS
  'Time of action taken';
  /
  
  COMMENT ON COLUMN "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"."PK_CORD"
IS
  'Primary key of cb_cord table';
  /
  
  COMMENT ON TABLE "VDA"."ETVDA_ORDERDETAILS_AUDIT_ALL"
IS
  'This view provides Order Details audit details ';
  /
  
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_CRI_VIEW_ALL" ("CRI", "CBU_STATUS", "CRI_CREATION_DATE", "PK_CORD","CORD_REGISTRY_ID","PK_CORD_COMPLETE_REQINFO") AS 
SELECT DECODE(REQINFO.COMPLETE_REQ_INFO_FLAG,'1','YES','NO'),
    CBUSTATUS.CBU_STATUS_DESC,
    REQINFO.CREATED_ON,
    CORD.PK_CORD, CORD.cord_registry_id, reqinfo.pk_cord_complete_reqinfo
  FROM ERES.CB_CORD CORD ,
    ERES.CB_CORD_COMPLETE_REQ_INFO REQINFO,
    ERES.CB_CBU_STATUS CBUSTATUS
  WHERE CORD.FK_CORD_CBU_STATUS = CBUSTATUS.PK_CBU_STATUS
  AND REQINFO.FK_CORD_ID        = CORD.PK_CORD ORDER BY PK_CORD ASC, CREATED_ON DESC;
  /
     COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."CRI"
IS
  'This column will store Complete Required Information.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."CBU_STATUS"
IS
  'This column store the local status of the cord.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."CRI_CREATION_DATE"
IS
  'This column store CRI creation date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."PK_CORD"
IS
  'Primary key of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."CORD_REGISTRY_ID"
IS
  'This column stores  Cord Registry Id';
  /
COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_ALL"."PK_CORD_COMPLETE_REQINFO"
IS
  'Primary key of the cb_cord_complete_req_info table';
  /  
  COMMENT ON TABLE "VDA"."ETVDA_CBU_CRI_VIEW_ALL"
IS
  'This view provides Complete Required Information of all cords';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_CRI_VIEW_PRG" ("CRI", "CBU_STATUS", "CRI_CREATION_DATE", "PK_CORD","CORD_REGISTRY_ID","PK_CORD_COMPLETE_REQINFO") AS 
SELECT DECODE(REQINFO.COMPLETE_REQ_INFO_FLAG,'1','YES','NO'),
    CBUSTATUS.CBU_STATUS_DESC,
    REQINFO.CREATED_ON,
    CORD.PK_CORD, CORD.cord_registry_id, reqinfo.pk_cord_complete_reqinfo
  FROM ERES.CB_CORD CORD ,
    ERES.CB_CORD_COMPLETE_REQ_INFO REQINFO,
    ERES.CB_CBU_STATUS CBUSTATUS
  WHERE CORD.FK_CORD_CBU_STATUS = CBUSTATUS.PK_CBU_STATUS
  AND REQINFO.FK_CORD_ID        = CORD.PK_CORD
	AND CORD.CORD_SEARCHABLE      = '0'
  ORDER BY PK_CORD ASC, CREATED_ON DESC;
  /
     COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_PRG"."CRI"
IS
  'This column will store Complete Required Information.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_PRG"."CBU_STATUS"
IS
  'This column store the local status of the cord.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_PRG"."CRI_CREATION_DATE"
IS
  'This column store CRI creation date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_PRG"."PK_CORD"
IS
  'Primary key of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_PRG"."CORD_REGISTRY_ID"
IS
  'This column stores  Cord Registry Id';
  /
COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_PRG"."PK_CORD_COMPLETE_REQINFO"
IS
  'Primary key of the cb_cord_complete_req_info table';
  /  
  COMMENT ON TABLE "VDA"."ETVDA_CBU_CRI_VIEW_PRG"
IS
  'This view provides Complete Required Information of �Saved In Progress� cords';
  /
  
  
  CREATE OR REPLACE FORCE VIEW "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT" ("CRI", "CBU_STATUS", "CRI_CREATION_DATE", "PK_CORD","CORD_REGISTRY_ID","PK_CORD_COMPLETE_REQINFO") AS 
SELECT DECODE(REQINFO.COMPLETE_REQ_INFO_FLAG,'1','YES','NO'),
    CBUSTATUS.CBU_STATUS_DESC,
    REQINFO.CREATED_ON,
    CORD.PK_CORD, CORD.cord_registry_id, reqinfo.pk_cord_complete_reqinfo
  FROM ERES.CB_CORD CORD ,
    ERES.CB_CORD_COMPLETE_REQ_INFO REQINFO,
    ERES.CB_CBU_STATUS CBUSTATUS
  WHERE CORD.FK_CORD_CBU_STATUS = CBUSTATUS.PK_CBU_STATUS
  AND REQINFO.FK_CORD_ID        = CORD.PK_CORD
	AND CORD.CORD_SEARCHABLE      = '1'
  ORDER BY PK_CORD ASC, CREATED_ON DESC;
  /
     COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT"."CRI"
IS
  'This column will store Complete Required Information.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT"."CBU_STATUS"
IS
  'This column store the local status of the cord.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT"."CRI_CREATION_DATE"
IS
  'This column store CRI creation date.';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT"."PK_CORD"
IS
  'Primary key of the cord';
  /
  COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT"."CORD_REGISTRY_ID"
IS
  'This column stores  Cord Registry Id';
  /
COMMENT ON COLUMN "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT"."PK_CORD_COMPLETE_REQINFO"
IS
  'Primary key of the cb_cord_complete_req_info table';
  /  
  COMMENT ON TABLE "VDA"."ETVDA_CBU_CRI_VIEW_SUBMT"
IS
  'This view provides Complete Required Information of �Submitted� cords ';
  /
  
INSERT INTO VDA_TRACK_PATCHES
(
  PATCH_PK       ,
  DB_VER_MJR     ,
  DB_VER_MNR     ,
  DB_PATCH_NAME  ,
  FIRED_ON       ,
  APP_VERSION    
) VALUES(SEQ_VDA_TRACK_PATCHES.nextval,3,1,'views.sql',sysdate,'ETVDA 1.0 Build 3')

commit;