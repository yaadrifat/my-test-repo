----/*This readMe is specific to EmTrax Velos Data Analytics version ETVDA1.0 Build#3*/------------------


1. We have released 2 new VDA views in this build. Apart from that around 12 views were modified.
2. Please coneect to VDA user in emtrax database and execute the patches unnder "vda" Folder.
3. Around 6 bugs have been fixed and the same has been mentioned in the release notes.
4. We have also released one  technical document  "ETVDA-Technical-document-Draft 1.0.0.xls" along with this build.
-----------------------------------------------------------------------------------------------------------------------------



 


	