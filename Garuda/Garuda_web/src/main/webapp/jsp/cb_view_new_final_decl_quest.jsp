<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", 0); 
String val = request.getParameter("readonly");
request.setAttribute("readonly",val);
%>
<script>
$j(document).ready(function(){
	
	disableFormFlds("finalDeclNewQuestRO");
	
});
function updateElgStas1(title,url,bredth,length){
	//$j('#finalDeclQstns').html('');
	var patientId = $j('#patientId').val();
			var patient_data = '';
			if(patientId!="" && patientId != null && patientId != "undefined"){
				patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
			}
			title=title+patient_data;
	showModal(title,url,bredth,length);
}
function resetValue(arr){
	 for(var j=0; j<arr.length;j++){
		 $j("#"+arr[j]+ " input[type=radio]").each(function(){
			 $j(this).attr('checked',false);
		 });
		 $j("#"+arr[j]).find("textarea").each(function(){
			 $j(this).val("");           
		 });		
	 }
 }

 function limitText(txtField, maxLength) {
		if (txtField.value.length > maxLength) {
			txtField.value = txtField.value.substring(0, maxLength)
		} 
	}

  function showQuestionWithDetail(val,detailid,quesid,quesidb){
	  var arr = new Array();
	  arr[0] = detailid;
	  arr[1] = quesid;
	  arr[2] = quesidb;
	  if(val=='false'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
         $j("#"+quesidb).show();
     }else if(val=='true'){
    	 $j("#"+detailid).hide();
         $j("#"+quesid).hide();
         $j("#"+quesidb).hide();
         resetValue(arr);
     }
  }

  function showYesQuestionWithDetail(val,detailid,quesid){
	  var arr = new Array();
	  arr[0] = detailid;
	  arr[1] = quesid;
	  if(val=='true'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
     }else if(val=='false'){
    	 $j("#"+detailid).hide();
         $j("#"+quesid).hide();
         resetValue(arr);
     }
  }

  function showMultipeQuestionWithDetail(val,detailid,quesid,quesid1,quesid2,quesid3){
	  var arr = new Array();
	  arr[0] = detailid;
	  arr[1] = quesid;
	  arr[2] = quesid1;
	  arr[3] = quesid2;
	  arr[4] = quesid3;
	  if(val=='false'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
         $j("#"+quesid1).show();
         $j("#"+quesid2).show();
         $j("#"+quesid3).show();
     }else if(val=='true'){
         $j("#"+detailid).hide();
         $j("#"+quesid).hide();
         $j("#"+quesid1).hide();
         $j("#"+quesid2).hide();
         $j("#"+quesid3).hide();
         resetValue(arr);
     }
  }

  function showMultipeYesQuestionWithDetail(val,detailid,quesid,quesid1,quesid3,quesid4,quesid5,quesid6,quesid7,quesid8,quesid9){
	  var arr = new Array();
	  arr[0] = detailid;
	  arr[1] = quesid;
	  arr[2] = quesid1;
	  arr[3] = quesid3;
	  arr[4] = quesid4;
	  arr[5] = quesid5;
	  arr[6] = quesid6;
	  arr[7] = quesid7;
	  arr[8] = quesid8;
	  arr[9] = quesid9;	  
	  if(val=='true'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
         $j("#"+quesid1).show();
         $j("#"+quesid3).show();
         $j("#"+quesid4).show();
         $j("#"+quesid5).show();
         $j("#"+quesid6).show();
         $j("#"+quesid7).show();
         $j("#"+quesid8).show();
         $j("#"+quesid9).show();
         
     }else if(val=='false'){
         $j("#"+detailid).hide();
         $j("#"+quesid).hide();
         $j("#"+quesid1).hide();
         $j("#"+quesid3).hide();
         $j("#"+quesid4).hide();
         $j("#"+quesid5).hide();
         $j("#"+quesid6).hide();
         $j("#"+quesid7).hide();
         $j("#"+quesid8).hide();
         $j("#"+quesid9).hide();
         resetValue(arr); 
     }
  }

  function showQuestion(val,quesid,detailId){
	  var arr = new Array();
	  arr[1] = quesid;	
	  arr[2] = detailId;
	  if(val=='false'){
	         $j("#"+quesid).show();
	        
	  }else if(val=='true'){
	         $j("#"+quesid).hide();
	         $j("#"+detailId).hide();
	         resetValue(arr);
	  }
  }
  function showUserMessage(val){
	  if(val=='true'){
		  var str = "<s:text name="garuda.cbu.quest.exitAlert"/>";
		  jConfirm(str, '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
			    if(r==true){
			    	//submitform('cdrentryform'); 
			    	//window.location='cdrcbuView?cdrCbuPojo.cordID='+$j("#ncordId").val()+'&orderId='+$j("#norderId").val()+'&orderType='+$j("#norderType").val()+'&pkcordId='+$j("#npkcordId").val();
			    	submitpost('cdrcbuView',{'cdrCbuPojo.cordID':$j("#ncordId").val(),'orderId':$j("#norderId").val(),'orderType':$j("#norderType").val(),'pkcordId':$j("#npkcordId").val()});
			    	
				}			    
		 });
	  }
  }

  function showMultipleQuestion(val,quesid,quesid1){
	  var arr = new Array();
	  arr[1] = quesid;
	  arr[2] = quesid1;	  
	  if(val=='false'){
		     $j("#"+quesid).show();
	         $j("#"+quesid1).show();
	  }else if(val=='false'){
	         $j("#"+quesid).hide();
	         $j("#"+quesid1).hide();
	         resetValue(arr);
	  }
  }

  function showDeatil(val,detailid){
	  var arr = new Array();
	  arr[0] = detailid;	  
	  if(val=='false'){
	         $j("#"+detailid).show();
	  }else if(val=='true'){
	         $j("#"+detailid).hide();
	         resetValue(arr);
	  }
  }

  function showYesDeatil(val,detailid){
	  var arr = new Array();
	  arr[0] = detailid;	  
	  if(val=='true'){
	         $j("#"+detailid).show();
	  }else if(val=='false'){
	         $j("#"+detailid).hide();
	         resetValue(arr);
	  }
  }

  function showNoDeatil(val,detailid){
	  var arr = new Array();
	  arr[0] = detailid;	  
	  if(val=='false'){
	         $j("#"+detailid).show();
	  }else if(val=='true'){
	         $j("#"+detailid).hide();
	         resetValue(arr);
	  }
  }
  
  function showhelpmsg(id){
		msg=$j('#'+id+'_msg').html();
		overlib(msg,CAPTION,'Help');
		$j('#overDiv').css('z-index','99999');
	}

	function showMessage(val){
		if(val=='false'){
			if(($j("#srFkCordCbuEligible1").val()==$j("#srIncompleteReasonId1").val()) && checkEligDates('collDate1','defDatVal1')){
				alert("<s:text name="garuda.cbu.quest.incompleteAlert"/>");
			}
		}
	}
	
</script>
<input type="hidden" name="srFkCordCbuEligible" id="srFkCordCbuEligible1" value="<s:property value="cdrCbuPojo.fkCordCbuEligible"></s:property>" />
<input type="hidden" name="srIncompleteReasonId" id="srIncompleteReasonId1" value="<s:property value="cdrCbuPojo.incompleteReasonId"></s:property>" />
<s:date name="cdrCbuPojo.specimen.specCollDate" id="dtPick1" format="yyyy-MM-dd" />	
<s:hidden name="collDate" id="collDate1" value="%{dtPick}"></s:hidden>
<s:hidden name="defDatVal" id="defDatVal1" value="2005-05-25"></s:hidden>
<input type="hidden" name="ncordId" value="<s:property value="cdrCbuPojo.cordID"/>" id="ncordId1" />
<input type="hidden" name="npkcordId" value="<s:property value="cdrCbuPojo.cordID"/>" id="npkcordId1" />
<input type="hidden" name="norderId" value="<s:property value="#request.orderId"/>" id="norderId1"/>
<input type="hidden" name="norderType" value="<s:property value="#request.orderType"/>"  id="norderType1" />
<input type = "hidden" name="" value=""/>
<div id="finalDeclNewQuestRO">
   <table width="100%" cellpadding="0" cellspacing="0" class="displaycdr">
       <tr>
           <td valign="top" width="35%">
               <s:text name="garuda.questionnaire.level.mrq_screnning" />
           </td>
           <td style="border: 1px solid #EDECEB;" width="65%" bgcolor="#7DCFD5">
              <table width="100%" cellpadding="0" cellspacing="0" >
                <tr valign="top">
                    <td width="75%"><s:text name="garuda.questionnaire.level.newmrqques1" /></td>
                    <td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio value='declNewPojo.mrqNewQues1' id="mrqques11" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.mrqNewQues1}','elgFcrChanges');showQuestion(this.value,'mrqQues1atr1','mrqNewQues1adddetail1')"></s:radio></div></td>
                </tr>

				 <tr id="mrqNewQues1adddetail1" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 <td width="25%">
                        <textarea  class="addQuestMandatory" rows="5" cols="20" value="declNewPojo.mrqNewQues1AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.mrqNewQues1AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.mrqNewQues1AddDetail}"/></textarea>
                   </td>
                </tr>
                <tr id="mrqQues1atr1" style="display: none;" valign="top">
                    <td width="95%"  style="padding-left: 45px;"><s:text name="garuda.questionnaire.level.newmrqques2" />
                    	
                    </td>
                    <td width="5%" style="padding:0px">
                    	<div class="addQuestMandatory">
                    	<s:radio value="declNewPojo.mrqNewQues1a" id="mrqQues1a" list="#{'true':'CBB confirms - not able to obtain this additional information</br>','false':'CBB will be able to complete the missing information'}" onclick="showNoDeatil(this.value,'mrqNewQues1adddetail');isChangeDone(this,'%{declNewPojo.mrqNewQues1a}','elgFcrChanges');" ></s:radio>
                    	</div>
                    </td>                    
                </tr>
            
                <tr valign="top">
                   	<td width="75%"><s:text name="garuda.questionnaire.level.newmrqques3" />
                  		
                   	<td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory">
                   		<s:radio value="declNewPojo.mrqNewQues2" id="mrqQues21" list="#{'true':'Yes','false':'NO'}" onclick="showNoDeatil(this.value,'mrqQues2adddetail1');isChangeDone(this,'%{declNewPojo.mrqNewQues2}','elgFcrChanges');"></s:radio></div>
                   </td>
                </tr>
                <tr id="mrqQues2adddetail1" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 <td  width="25%">
                        <textarea rows="5" cols="20" class="addQuestMandatory" value="declNewPojo.mrqNewQues2AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.mrqNewQues2AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.mrqNewQues2AddDetail}"/></textarea>
                   </td>
                </tr>
                <tr valign="top">
                   	<td   width="75%"><s:text name="garuda.questionnaire.level.newmrqques4" />
                  		
                   	<td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory">
                   		<s:radio value="declNewPojo.mrqNewQues3" id="mrqQues21" list="#{'true':'Yes','false':'NO'}" onclick="showYesDeatil(this.value,'mrqQues3adddetail1');isChangeDone(this,'%{declNewPojo.mrqNewQues2}','elgFcrChanges');"></s:radio></div>
                   </td>
                </tr>
                <tr id="mrqQues3adddetail1" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 <td width="25%">
                        <textarea class="addQuestMandatory" rows="5" cols="20" value="declNewPojo.mrqNewQues3AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.mrqNewQues3AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.mrqNewQues3AddDetail}"/></textarea>
                   </td>
                </tr>
              </table>
       		</td>
       </tr>
       <tr>
       		<td colspan="2" height="5%"></td>
       </tr>
       <tr>
           <td valign="top" width="35%">
               <s:text name="garuda.questionnaire.level.physNewfind" /> 
           </td>
           <td style="border: 1px solid #EDECEB;" bgcolor="#7DCFD5" width="65%">
              <table width="100%" cellpadding="0" cellspacing="0" >
                <tr valign="top">
                   <td width="75%"><s:text name="garuda.questionnaire.level.newphysfind0" /></td>
                   <td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio value="declNewPojo.phyNewFindQues4" id="phyNewFindQues41" list="#{'true':'Yes','false':'NO'}" onclick="showQuestion(this.value,'phyNewFindQues4atr1','phyNewFindQues1adddetail1');showMessage(this.value);isChangeDone(this,'%{declNewPojo.phyNewFindQues4}','elgFcrChanges');"></s:radio></div></td>
                </tr>
                
                <tr id="phyNewFindQues1adddetail1" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 <td width="25%">
                        <textarea class="addQuestMandatory" rows="5" cols="20" value="declNewPojo.phyNewFindQues4AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.phyNewFindQues4AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.phyNewFindQues4AddDetail}"/></textarea>
                   </td>
                </tr>
             
                <tr id="phyNewFindQues4atr1" style="display: none;" valign="top">
                    <td width="95%" style="padding-left: 45px;"><s:text name="garuda.questionnaire.level.newphysfind1" />
                   		
                    </td>
                    <td   width="5%" style="padding:0px">
                    	<div class="addQuestMandatory">
                    	<s:radio value="declNewPojo.phyNewFindQues4A" id="phynewFindQues4a" list="#{'true':'CBB confirms - not able to obtain this additional information</br>','false':'CBB will be able to complete the missing information'}" onclick="showNoDeatil(this.value,'phyNewFindQues1adddetail');isChangeDone(this,'%{declNewPojo.phyNewFindQues4A}','elgFcrChanges');" ></s:radio>
                    	</div>
                    </td>                    
                </tr>
                
       
               
                <tr valign="top">
                   	<td width="75%"><s:text name="garuda.questionnaire.level.newphysfind2" />
                   		
                   	</td>
                   	<td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory">
                   		<s:radio value="declNewPojo.phyNewFindQues5" id="phyNewFindQues51" list="#{'true':'Yes','false':'NO'}" onclick="showYesDeatil(this.value,'phyNewFindQues5AddDetail1');isChangeDone(this,'%{declNewPojo.phyNewFindQues5AddDetail}','elgFcrChanges');"></s:radio></div>
                   	</td>
                </tr>
                <tr id="phyNewFindQues5AddDetail1" style="display: none;">
                	<td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                	<td  width="25%">
                    	<textarea class="addQuestMandatory" rows="5" cols="20" value="declNewPojo.phyNewFindQues5AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.phyNewFindQues5AddDetail}" escapeJavaScript="true"/>','elgFcrChanges');" ><s:property value="%{declNewPojo.phyNewFindQues5AddDetail}"/></textarea>
                   </td>
                </tr>
                <tr valign="top">
                   	<td width="75%"><s:text name="garuda.questionnaire.level.newphysfind3" />
                   		
                   	</td>
                   	<td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory">
                   		<s:radio value="declNewPojo.phyNewFindQues6" id="phyNewFindQues61" list="#{'true':'Yes','false':'NO'}" 
                   				onclick="showMultipeYesQuestionWithDetail(this.value,'phyNewFindQues6AddDetail1','phyNewFindQues6atr1','phyNewFindQues6btr1','phyNewFindQues6ctr1','phyNewFindQues6dtr1','phyNewFindQues6etr1','phyNewFindQues6ftr1','phyNewFindQues6gtr1','phyNewFindQues6htr1','phyNewFindQues6itr1');isChangeDone(this,'%{declNewPojo.phyNewFindQues6}','elgFcrChanges');"></s:radio></div>
                   </td>
                </tr>
                <tr id="phyNewFindQues6atr1" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind4" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio value="declNewPojo.phyNewFindQues6a" id="phyNewFindQues6a1" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6a}','elgFcrChanges');"></s:radio></div></td>                    
                </tr>
                <tr id="phyNewFindQues6btr1" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind5" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio value="declNewPojo.phyNewFindQues6b" id="phyNewFindQues6b1" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6b}','elgFcrChanges');" ></s:radio></div></td>                    
                </tr>
                <tr id="phyNewFindQues6ctr1" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind6" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio value="declNewPojo.phyNewFindQues6c" id="phyNewFindQues6c1" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6c}','elgFcrChanges');" ></s:radio></div></td>                    
                </tr>
                <tr id="phyNewFindQues6dtr1" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind7" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio value="declNewPojo.phyNewFindQues6d" id="phyNewFindQues6d1" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6d}','elgFcrChanges');" ></s:radio></div></td>                    
                </tr>
                <tr id="phyNewFindQues6etr1" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind8" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio value="declNewPojo.phyNewFindQues6e" id="phyNewFindQues6e1" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6e}','elgFcrChanges');" ></s:radio></div></td>                    
                </tr>
                <tr id="phyNewFindQues6ftr1" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind9" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio value="declNewPojo.phyNewFindQues6f" id="phyNewFindQues6f1" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6f}','elgFcrChanges');" ></s:radio></div></td>                    
                </tr>
                <tr id="phyNewFindQues6gtr1" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind10" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio value="declNewPojo.phyNewFindQues6g" id="phyNewFindQues6g1" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6g}','elgFcrChanges');" ></s:radio></div></td>                    
                </tr>
                <tr id="phyNewFindQues6htr1" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind11" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio value="declNewPojo.phyNewFindQues6h" id="phyNewFindQues6h1" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6h}','elgFcrChanges');" ></s:radio></div></td>                    
                </tr>
                <tr id="phyNewFindQues6itr1" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind12" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio value="declNewPojo.phyNewFindQues6i" id="phyNewFindQues6i1" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6i}','elgFcrChanges');" ></s:radio></div></td>                    
                </tr>
                 <tr id="phyNewFindQues6AddDetail1" style="display: none;" valign="top" >
                	<td  width="75%" style="padding-left: 25px;">
                		<s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                	<td width="25%" style="padding:0px">
                    	<textarea class="addQuestMandatory" rows="5" cols="20" value="declNewPojo.phyNewFindQues6AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.phyNewFindQues6AddDetail}" escapeJavaScript="true"/>','elgFcrChanges');" ><s:property value="%{declNewPojo.phyNewFindQues6AddDetail}"/></textarea>
                   	</td>
                </tr>
              </table>
           </td>
       </tr>
       <tr>
       		<td colspan="2" height="5%"></td>
       </tr>
       <tr>
           <td valign="top" width="35%">
               <s:text name="garuda.questionnaire.level.idm" /> 
           </td>
           <td style="border: 1px solid #EDECEB;" bgcolor="#7DCFD5" width="65%">
               <table width="100%" cellpadding="0" cellspacing="0" >
                   <tr valign="top">
	                   <td width="75%"><s:text name="garuda.questionnaire.level.newidm1" /></td>
	                   <td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio value="declNewPojo.idmNewQues7" id="idmNewQues71" list="#{'true':'Yes','false':'NO'}" 
	                   onclick="showQuestion(this.value,'idmNewQues7atr1','idmNewQues7AddDetail1');isChangeDone(this,'%{declNewPojo.idmNewQues7}','elgFcrChanges');"></s:radio></div></td>
                   </tr>
                   
               <tr id="idmNewQues7AddDetail1" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 <td width="25%">
                        <textarea  class="addQuestMandatory" rows="5" cols="20" value="declNewPojo.idmNewQues7AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.idmNewQues7AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.idmNewQues7AddDetail}"/></textarea>
                   </td>
                </tr>
                   
                   <tr id="idmNewQues7atr1" style="display: none;" valign="top">
    	                <td width="95%" style="padding-left: 45px;"><s:text name="garuda.questionnaire.level.newidm2" />
	    	                
            	        </td>
                	    <td   width="5%" style="padding:0px">
                    		
                    		<div class="addQuestMandatory">
                    		<s:radio value="declNewPojo.idmNewQues7a" id="idmNewQues7a" list="#{'true':'CBB confirms - not able to complete the missing tests</br>','false':'CBB will be able to complete the missing tests'}" onclick="showNoDeatil(this.value,'idmNewQues7AddDetail');isChangeDone(this,'%{declNewPojo.idmNewQues7a}','elgFcrChanges');" ></s:radio>
                    		
                    		</div>
                    	</td>                    
                   </tr>
          
                   
                   
                   <tr valign="top">
	                   <td width="75%"><s:text name="garuda.questionnaire.level.newidm3" /></td>
	                   <td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio value="declNewPojo.idmNewQues8" id="idmNewQues81" list="#{'true':'Yes','false':'NO'}" 
	                   onclick="showQuestion(this.value,'idmNewQues8atr1','idmNewQues8AddDetail1');isChangeDone(this,'%{declNewPojo.idmNewQues8}','elgFcrChanges');"></s:radio></div></td>
                   </tr>
                   
                    <tr id="idmNewQues8AddDetail1" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 	<td width="25%">
                        <textarea class="addQuestMandatory" rows="5" cols="20" value="declNewPojo.idmNewQues8AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.idmNewQues8AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.idmNewQues8AddDetail}"/></textarea>
                   	</td>
                	</tr>
                
                   
                   <tr id="idmNewQues8atr1" style="display: none;" valign="top">
    	                <td width="95%" style="padding-left: 45px;"><s:text name="garuda.questionnaire.level.newidm2" />
	    	                
            	        </td>
                	    <td   width="5%" style="padding:0px">
                    		<div class="addQuestMandatory">
                    		<s:radio value="declNewPojo.idmNewQues8a" id="idmNewQues8a" list="#{'true':'CBB confirms - not able to complete the missing tests</br>','false':'CBB will be able to complete the missing tests'}" onclick="showNoDeatil(this.value,'idmNewQues8AddDetail');isChangeDone(this,'%{declNewPojo.idmNewQues8a}','elgFcrChanges');" ></s:radio>
                    		
                    		</div>
                    	</td>                    
                   </tr>
                   
                <tr valign="top">
                   	<td width="75%"><s:text name="garuda.questionnaire.level.newidm5" />
                  		
                   	<td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory">
                   		<s:radio value="declNewPojo.idmNewQues9" id="idmNewQues91" list="#{'true':'Yes','false':'NO'}" onclick="showYesDeatil(this.value,'idmNewQues9AddDetail1');isChangeDone(this,'%{declNewPojo.idmNewQues9}','elgFcrChanges');"></s:radio></div>
                   </td>
                </tr>
                <tr id="idmNewQues9AddDetail1" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 <td width="25%">
                        <textarea class="addQuestMandatory" rows="5" cols="20" value="declNewPojo.idmNewQues9AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.idmNewQues9AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.idmNewQues9AddDetail}"/></textarea>
                   </td>
                </tr>
                <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
                </s:if>
                <s:else>
                
         
               <s:if test="esignFlag == 'review'">
     			<s:if test = "#request.shipMentFlag==true">
    			<br></br>
    	  		 <s:if test="hasEditPermission(#request.eligbleSumQues)==true">  
    			<tr>
    			<td width="75%">&nbsp;</td>
   				<td width align="right" width="25%"><button type="button" onclick="updateElgStas1('<s:text name="garuda.cdrcbuview.label.eligibility_determination"/>'+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','updateFinalEligibleStatus?module=CBU&esignFlag=modalFCR&resetFun=reset&licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR_EDIT_ELIGIBLE" />','390','900');"><s:text name="garuda.common.lable.edit" /></button></td>
    			</tr>
    			</s:if>
    			</s:if>
    			</s:if>
    			</s:else>
                 
               </table>
           </td>
       </tr>
       <tr>
       		<td colspan="2" height="5%"></td>
       </tr>
    </table>
</div>
<div id="helpmsg1" style="display: none;">
	<div id=mrq1a_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incompleteInfo"/></td></tr>
   		</table>
	</div>
	<div id=mrq1b_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=mrq2_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.ineligibleMatRisk"/></td></tr>
   		</table>
	</div>
	<div id=physfind3a_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incompReleventMedRecord"/></td></tr>
   		</table>
	</div>
	<div id=physfind3b_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=physfind3c_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incompPhysicalFindings"/></td></tr>
   		</table>
	</div>
	<div id=physfind3d_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=physfind4_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.ineligibleReleventMedRecord"/></td></tr>
   		</table>
	</div>
	<div id=physfind5_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.imeligiblePhysicalFindings"/></td></tr>
   		</table>
	</div>
	<div id=idm6a_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incomInfectDiseaseMarker"/></td></tr>
   		</table>
	</div>
	<div id=idm6b_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=idm7a_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incomInfectDiseaseMarker"/></td></tr>
   		</table>
	</div>
	<div id=idm7b_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=idm8_msg1>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.ineligibleInfectDiseaseMarker"/></td></tr>
   		</table>
	</div>
</div>
<s:if test="declNewPojo.mrqNewQues1!=null && declNewPojo.mrqNewQues1==false">
    <script>
          showQuestion('false','mrqQues1atr1','','1');
    </script>
</s:if>
<s:if test="declNewPojo.mrqNewQues1!=null && declNewPojo.mrqNewQues1==false">
    <script>
          showNoDeatil('false','mrqNewQues1adddetail1','1');
    </script>
</s:if>
<s:if test="declNewPojo.mrqNewQues2!=null && declNewPojo.mrqNewQues2==false">
    <script>
         showNoDeatil('false','mrqQues2adddetail1','1');
    </script>
</s:if>
<s:if test="declNewPojo.mrqNewQues3!=null && declNewPojo.mrqNewQues3==true">
    <script>
         showYesDeatil('true','mrqQues3adddetail1','1');
    </script>
</s:if>
<s:if test="declNewPojo.phyNewFindQues4!=null && declNewPojo.phyNewFindQues4==false">
    <script>
        showQuestion('false','phyNewFindQues4atr1','','1');
    </script>
</s:if>
<s:if test="declNewPojo.phyNewFindQues4!=null && declNewPojo.phyNewFindQues4==false">
    <script>
        showNoDeatil('false','phyNewFindQues1adddetail1','1');
    </script>
</s:if>
<s:if test="declNewPojo.phyNewFindQues5!=null && declNewPojo.phyNewFindQues5==true">
    <script>
        showYesDeatil('true','phyNewFindQues5AddDetail1','1');
    </script>
</s:if>

<s:if test="declNewPojo.phyNewFindQues6!=null && declNewPojo.phyNewFindQues6==true">

    <script>
        showMultipeYesQuestionWithDetail('true','phyNewFindQues6AddDetail1','phyNewFindQues6atr1','phyNewFindQues6btr1','phyNewFindQues6ctr1','phyNewFindQues6dtr1','phyNewFindQues6etr1','phyNewFindQues6ftr1','phyNewFindQues6gtr1','phyNewFindQues6htr1','phyNewFindQues6itr1','1');
    </script>
</s:if>
<s:if test="declNewPojo.idmNewQues7!=null && declNewPojo.idmNewQues7==false">
    <script>
        showQuestion('false','idmNewQues7atr1','','1');
    </script>
</s:if>
<s:if test="declNewPojo.idmNewQues7!=null && declNewPojo.idmNewQues7==false">
    <script>
       showQuestion('false','idmNewQues7AddDetail1','','1');
    </script>
</s:if>

<s:if test="declNewPojo.idmNewQues8!=null && declNewPojo.idmNewQues8==false">
    <script>
      showQuestion('false','idmNewQues8atr1','','1');
    </script>
</s:if>
<s:if test="declNewPojo.idmNewQues8!=null && declNewPojo.idmNewQues8==false">
    <script>
      showQuestion('false','idmNewQues8AddDetail1','','1');
    </script>
</s:if>


<s:if test="declNewPojo.idmNewQues9!=null && declNewPojo.idmNewQues9==true">
    <script>    
     showYesDeatil('true','idmNewQues9AddDetail1','1');
    </script>
</s:if>
  <s:if test="hasEditPermission(#request.eligbleSumQues)==false">
  <script>
     $j("#finalDeclNewQuestRO").each(function(){
    	  $j(this).find('input:radio').attr('disabled', true);
     });  
  </script> 
  </s:if>
  <s:if test="hasViewPermission(#request.eligbleSumQues)==false">
  <script>
      //$j('#finalDeclNewReadOnly').css('display','none');
     $j('#finalDeclNewQuestRO').detach();
  </script> 
  </s:if>