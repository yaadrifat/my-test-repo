<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%@page import="com.velos.ordercomponent.business.util.VelosMidConstants"%>
<jsp:include page="cb_track_session_logging.jsp" />
<%
	HttpSession tSession = request.getSession(true); 
	GrpRightsJB grpRights =null;
	if (tSession.getAttribute("LocalGRights")!=null){
	 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
	}else
	{
	 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	}
	int updateINDstat = 0;
	int updateCBUstatus = 0;
	int updateEligStat=0;
	int updateLicStat=0;
	int updateCliStat=0;
	int updateUploadDoc=0;
	int updateUnitRepStat=0;
	int updateMinCriteria=0;
	int updateCBUAssment=0;
	int updateCBUReport=0;
	int updateAuditTrail=0;
	int updateFinalCBURev=0;
	int updateFlaggedItem=0;
	int updateShipmentItern=0;
	int updateProductInsert=0;
	int updateQuickLnkPanel=0;
	int updateCBUInfowidget=0;
	int updateCBUInfoS=0;
	int updateIDwidget=0;
	int updateProInfoWidget=0;
	int updateInfectDisease=0;
	int updateidmForm =0;
	int updateCbuHLA=0;
	int updateHLA_CBU=0;
	int updateHLA_Matrnl=0;
	int updateHLA_Patient=0;
	int updateHlthHis=0;
	int updateHistry_Mrq=0;
	int updateHistry_FMHQ=0;
	int updateLabSmmry=0;
	int updateOthrTstRslt=0;
	int updateClnclNotes=0;
	int updateElgblty=0;
	int vClnclNote=0;
	int crtePckngSlip =0;
	int viewNote =0;
	int staicPanl =0;

	int viewPatienttc = 0;
	int vPatientAntgn = 0;
	int CaseMngrDir = 0;
	int viewCbuAvailCon = 0;
	int viewRqstCBUhis = 0;
	int viewreqclincinfo = 0;
	int viewNotes = 0;
	int viewHEResol= 0;
	int vPrgrssNote = 0;
	int applyResotoCT = 0;
	int cmpltReqInfo = 0;
	
	int viewFnlCBURvw = 0;
	int viewTCOrdrDetail = 0;
	int viewTCORDetail = 0;
	int viewORResolution = 0;
	int viewPrgrssNotes = 0;
	int viewCBUShipmnt = 0;
	int currntReqst = 0;
	int viewAddTypbyCBB = 0;
	int viewCTResolution = 0;
	int viewCTshipmnt= 0;
	int vCTinstns = 0;
	int assignOrders=0;
	int switchFlow=0;
	int updtPendngReq=0;
	int updtNmdpSmpl=0;
	int overRideDeferred=0;
	
	int viewCTShipReport=0;
	int viewCTSampReport=0;
	int viewHEReport=0;
	int viewORReport=0;
	
	if(grpRights.getFtrRightsByValue("CB_CTSHPREPORT")!=null && !grpRights.getFtrRightsByValue("CB_CTSHPREPORT").equals(""))
	{
		viewCTShipReport = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CTSHPREPORT"));
	}
	else
		viewCTShipReport = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CTLABREPORT")!=null && !grpRights.getFtrRightsByValue("CB_CTLABREPORT").equals(""))
	{
		viewCTSampReport = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CTLABREPORT"));
	}
	else
		viewCTSampReport = 4;
	
	
	if(grpRights.getFtrRightsByValue("CB_HEREPORT")!=null && !grpRights.getFtrRightsByValue("CB_HEREPORT").equals(""))
	{
		viewHEReport = Integer.parseInt(grpRights.getFtrRightsByValue("CB_HEREPORT"));
	}
	else
		viewHEReport = 4;
	
	if(grpRights.getFtrRightsByValue("CB_ORREPORT")!=null && !grpRights.getFtrRightsByValue("CB_ORREPORT").equals(""))
	{
		viewORReport = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ORREPORT"));
	}
	else
		viewORReport = 4;

	if(grpRights.getFtrRightsByValue("CB_INSTAT")!=null && !grpRights.getFtrRightsByValue("CB_INSTAT").equals(""))
	{updateINDstat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_INSTAT"));}
	else
		updateINDstat = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUSTATUS")!=null && !grpRights.getFtrRightsByValue("CB_CBUSTATUS").equals(""))
	{updateCBUstatus = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUSTATUS"));}
	else
		updateCBUstatus = 4;
	
	if(grpRights.getFtrRightsByValue("CB_ELSTAT")!=null && !grpRights.getFtrRightsByValue("CB_ELSTAT").equals(""))
	{updateEligStat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ELSTAT"));}
	else
		updateEligStat = 4;
	
	if(grpRights.getFtrRightsByValue("CB_LICSTAT")!=null && !grpRights.getFtrRightsByValue("CB_LICSTAT").equals(""))
	{updateLicStat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_LICSTAT"));}
	else
		updateLicStat = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CLSTAT")!=null && !grpRights.getFtrRightsByValue("CB_CLSTAT").equals(""))
	{updateCliStat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CLSTAT"));}
	else
		updateCliStat = 4;
	
	if(grpRights.getFtrRightsByValue("CB_UPLDOC")!=null && !grpRights.getFtrRightsByValue("CB_UPLDOC").equals(""))
	{	updateUploadDoc = Integer.parseInt(grpRights.getFtrRightsByValue("CB_UPLDOC"));}
	else
		updateUploadDoc = 4;
	
	if(grpRights.getFtrRightsByValue("CB_ATREP")!=null && !grpRights.getFtrRightsByValue("CB_ATREP").equals(""))
	{	updateUnitRepStat = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ATREP"));}
	else
		updateUnitRepStat = 4;
	
	if(grpRights.getFtrRightsByValue("CB_REVMIN")!=null && !grpRights.getFtrRightsByValue("CB_REVMIN").equals(""))
	{	updateMinCriteria = Integer.parseInt(grpRights.getFtrRightsByValue("CB_REVMIN"));}
	else
		updateMinCriteria = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUASSMNT")!=null && !grpRights.getFtrRightsByValue("CB_CBUASSMNT").equals(""))
	{updateCBUAssment = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUASSMNT"));}
	else
		updateCBUAssment = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUREPORT")!=null && !grpRights.getFtrRightsByValue("CB_CBUREPORT").equals(""))
	{updateCBUReport = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUREPORT"));}
	else
		updateCBUReport = 4;
	
	if(grpRights.getFtrRightsByValue("CB_AUDIT")!=null && !grpRights.getFtrRightsByValue("CB_AUDIT").equals(""))
	{updateAuditTrail = Integer.parseInt(grpRights.getFtrRightsByValue("CB_AUDIT"));}
	else
		updateAuditTrail = 4;
	
	if(grpRights.getFtrRightsByValue("CB_FINALREVIEW")!=null && !grpRights.getFtrRightsByValue("CB_FINALREVIEW").equals(""))
	{updateFinalCBURev = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FINALREVIEW"));}
	else
		updateFinalCBURev = 4;
	
	if(grpRights.getFtrRightsByValue("CB_FLAGGEDITEM")!=null && !grpRights.getFtrRightsByValue("CB_FLAGGEDITEM").equals(""))
	{updateFlaggedItem = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FLAGGEDITEM")); }
	else
		updateFlaggedItem = 4;
	
	if(grpRights.getFtrRightsByValue("CB_SHPMNTITERN")!=null && !grpRights.getFtrRightsByValue("CB_SHPMNTITERN").equals(""))
	{updateShipmentItern = Integer.parseInt(grpRights.getFtrRightsByValue("CB_SHPMNTITERN")); }
	else
		updateFlaggedItem = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PRODUCTINSRT")!=null && !grpRights.getFtrRightsByValue("CB_PRODUCTINSRT").equals(""))
	{updateProductInsert = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PRODUCTINSRT")); }
	else
		updateProductInsert = 4;
	
	if(grpRights.getFtrRightsByValue("CB_QUICKLNKPANL")!=null && !grpRights.getFtrRightsByValue("CB_QUICKLNKPANL").equals(""))
	{updateQuickLnkPanel = Integer.parseInt(grpRights.getFtrRightsByValue("CB_QUICKLNKPANL")); }
	else
		updateQuickLnkPanel = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUINFOW")!=null && !grpRights.getFtrRightsByValue("CB_CBUINFOW").equals(""))
	{updateCBUInfowidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUINFOW")); }
	else
		updateCBUInfowidget = 4;
	if(grpRights.getFtrRightsByValue("CB_CBUINFOS")!=null && !grpRights.getFtrRightsByValue("CB_CBUINFOS").equals(""))
	{updateCBUInfoS = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUINFOS")); }
	else
		updateCBUInfoS = 4;
	
	if(grpRights.getFtrRightsByValue("CB_IDW")!=null && !grpRights.getFtrRightsByValue("CB_IDW").equals(""))
	{updateIDwidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_IDW")); }
	else
		updateIDwidget = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PROINFOW")!=null && !grpRights.getFtrRightsByValue("CB_PROINFOW").equals(""))
	{	updateProInfoWidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PROINFOW")); }
	else
		updateProInfoWidget = 4;
	
	if(grpRights.getFtrRightsByValue("CB_IDMW")!=null && !grpRights.getFtrRightsByValue("CB_IDMW").equals(""))
	{	updateInfectDisease = Integer.parseInt(grpRights.getFtrRightsByValue("CB_IDMW")); }
	else
		updateInfectDisease = 4;
	
	if(grpRights.getFtrRightsByValue("CB_IDMFORM")!=null && !grpRights.getFtrRightsByValue("CB_IDMFORM").equals(""))
	{	updateidmForm = Integer.parseInt(grpRights.getFtrRightsByValue("CB_IDMFORM")); }
	else
		updateidmForm = 4;
	
	if(grpRights.getFtrRightsByValue("CB_HLAW")!=null && !grpRights.getFtrRightsByValue("CB_HLAW").equals(""))
	{	updateCbuHLA = Integer.parseInt(grpRights.getFtrRightsByValue("CB_HLAW")); }
	else
		updateCbuHLA = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUHLAW")!=null && !grpRights.getFtrRightsByValue("CB_CBUHLAW").equals(""))
	{	updateHLA_CBU = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUHLAW")); }
	else
		updateHLA_CBU = 4;
	
	if(grpRights.getFtrRightsByValue("CB_MATERNALHLA")!=null && !grpRights.getFtrRightsByValue("CB_MATERNALHLA").equals(""))
	{	updateHLA_Matrnl = Integer.parseInt(grpRights.getFtrRightsByValue("CB_MATERNALHLA")); }
	else
		updateHLA_Matrnl = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PATIENTHLA")!=null && !grpRights.getFtrRightsByValue("CB_PATIENTHLA").equals(""))
	{	updateHLA_Patient = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PATIENTHLA")); }
	else
		updateHLA_Patient = 4;
	
	if(grpRights.getFtrRightsByValue("CB_HLTHISCRNW")!=null && !grpRights.getFtrRightsByValue("CB_HLTHISCRNW").equals(""))
	{	updateHlthHis = Integer.parseInt(grpRights.getFtrRightsByValue("CB_HLTHISCRNW")); }
	else
		updateHlthHis = 4;
			
	if(grpRights.getFtrRightsByValue("CB_MRQW")!=null && !grpRights.getFtrRightsByValue("CB_MRQW").equals(""))
	{	updateHistry_Mrq = Integer.parseInt(grpRights.getFtrRightsByValue("CB_MRQW")); }
	else
		updateHistry_Mrq = 4;
	
	if(grpRights.getFtrRightsByValue("CB_FMHQW")!=null && !grpRights.getFtrRightsByValue("CB_FMHQW").equals(""))
	{	updateHistry_FMHQ = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FMHQW")); }
	else
		updateHistry_FMHQ = 4;
	
	if(grpRights.getFtrRightsByValue("CB_LABSUMW")!=null && !grpRights.getFtrRightsByValue("CB_LABSUMW").equals(""))
	{	updateLabSmmry = Integer.parseInt(grpRights.getFtrRightsByValue("CB_LABSUMW")); }
	else
		updateLabSmmry = 4;
	
	if(grpRights.getFtrRightsByValue("CB_OTHERW")!=null && !grpRights.getFtrRightsByValue("CB_OTHERW").equals(""))
	{	updateOthrTstRslt = Integer.parseInt(grpRights.getFtrRightsByValue("CB_OTHERW")); }
	else
		updateOthrTstRslt = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CLNCLNOTESW")!=null && !grpRights.getFtrRightsByValue("CB_CLNCLNOTESW").equals(""))
	{	updateClnclNotes = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CLNCLNOTESW")); }
	else
		updateClnclNotes = 4;
	
	if(grpRights.getFtrRightsByValue("CB_ELIGIBLEW")!=null && !grpRights.getFtrRightsByValue("CB_ELIGIBLEW").equals(""))
	{	updateElgblty = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ELIGIBLEW")); }
	else
		updateElgblty = 4;
	
	if(grpRights.getFtrRightsByValue("CB_NOTES")!=null && !grpRights.getFtrRightsByValue("CB_NOTES").equals(""))
	{	vClnclNote = Integer.parseInt(grpRights.getFtrRightsByValue("CB_NOTES"));}
	else
		vClnclNote = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PCKNGSLIP")!=null && !grpRights.getFtrRightsByValue("CB_PCKNGSLIP").equals(""))
	{	crtePckngSlip = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PCKNGSLIP"));}
	else
		crtePckngSlip = 4;
	if(grpRights.getFtrRightsByValue("CB_NOTESB")!=null && !grpRights.getFtrRightsByValue("CB_NOTESB").equals(""))
	{	viewNote = Integer.parseInt(grpRights.getFtrRightsByValue("CB_NOTESB"));}
	else
		viewNote = 4;	
	if(grpRights.getFtrRightsByValue("CB_STATPANEL")!=null && !grpRights.getFtrRightsByValue("CB_STATPANEL").equals(""))
	{	staicPanl = Integer.parseInt(grpRights.getFtrRightsByValue("CB_STATPANEL"));}
	else
		staicPanl = 4;	
	
	
	if(grpRights.getFtrRightsByValue("CB_PATCENTRECMW")!=null && !grpRights.getFtrRightsByValue("CB_PATCENTRECMW").equals(""))
	{	viewPatienttc = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PATCENTRECMW"));}
	else
		viewPatienttc = 4;
	
	if(grpRights.getFtrRightsByValue("CB_VPATANTIGENT")!=null && !grpRights.getFtrRightsByValue("CB_VPATANTIGENT").equals(""))
	{	vPatientAntgn = Integer.parseInt(grpRights.getFtrRightsByValue("CB_VPATANTIGENT"));}
	else
		vPatientAntgn = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CASEMNGRDIR")!=null && !grpRights.getFtrRightsByValue("CB_CASEMNGRDIR").equals(""))
	{	CaseMngrDir = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CASEMNGRDIR"));}
	else
		CaseMngrDir = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUAVAILCONW")!=null && !grpRights.getFtrRightsByValue("CB_CBUAVAILCONW").equals(""))
	{	viewCbuAvailCon = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUAVAILCONW"));}
	else
		viewCbuAvailCon = 4;
	
	if(grpRights.getFtrRightsByValue("CB_REQCBUHISTW")!=null && !grpRights.getFtrRightsByValue("CB_REQCBUHISTW").equals(""))
	{	viewRqstCBUhis= Integer.parseInt(grpRights.getFtrRightsByValue("CB_REQCBUHISTW"));}
	else
		viewRqstCBUhis = 4;
	
	if(grpRights.getFtrRightsByValue("CB_REQCLNINFOW")!=null && !grpRights.getFtrRightsByValue("CB_REQCLNINFOW").equals(""))
	{	viewreqclincinfo= Integer.parseInt(grpRights.getFtrRightsByValue("CB_REQCLNINFOW"));}
	else
		viewreqclincinfo = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PROGNOTESW")!=null && !grpRights.getFtrRightsByValue("CB_PROGNOTESW").equals(""))
	{	viewNotes  = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PROGNOTESW"));}
	else
		viewNotes = 4;
	
	if(grpRights.getFtrRightsByValue("CB_HERESOLW")!=null && !grpRights.getFtrRightsByValue("CB_HERESOLW").equals(""))
	{	viewHEResol= Integer.parseInt(grpRights.getFtrRightsByValue("CB_HERESOLW"));}
	else
		viewHEResol = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PROGRESNOTE")!=null && !grpRights.getFtrRightsByValue("CB_PROGRESNOTE").equals(""))
	{	vPrgrssNote = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PROGRESNOTE"));}
	else
		vPrgrssNote = 4;
	
	if(grpRights.getFtrRightsByValue("CB_APLYRESOCT")!=null && !grpRights.getFtrRightsByValue("CB_APLYRESOCT").equals(""))
	{	applyResotoCT = Integer.parseInt(grpRights.getFtrRightsByValue("CB_APLYRESOCT"));}
	else
		applyResotoCT = 4;
	if(grpRights.getFtrRightsByValue("CB_COMPREQINFO")!=null && !grpRights.getFtrRightsByValue("CB_COMPREQINFO").equals(""))
	{	cmpltReqInfo = Integer.parseInt(grpRights.getFtrRightsByValue("CB_COMPREQINFO"));}
	else
		cmpltReqInfo = 4;
	
	if(grpRights.getFtrRightsByValue("CB_FINALCBUREVW")!=null && !grpRights.getFtrRightsByValue("CB_FINALCBUREVW").equals(""))
	{	viewFnlCBURvw= Integer.parseInt(grpRights.getFtrRightsByValue("CB_FINALCBUREVW"));}
	else
		viewFnlCBURvw = 4;
	
	if(grpRights.getFtrRightsByValue("CB_TCORDRDETALW")!=null && !grpRights.getFtrRightsByValue("CB_TCORDRDETALW").equals(""))
	{	viewTCOrdrDetail = Integer.parseInt(grpRights.getFtrRightsByValue("CB_TCORDRDETALW"));}
	else
		viewTCOrdrDetail = 4;
	
	if(grpRights.getFtrRightsByValue("CB_TCDETALORW")!=null && !grpRights.getFtrRightsByValue("CB_TCDETALORW").equals(""))
	{	viewTCORDetail = Integer.parseInt(grpRights.getFtrRightsByValue("CB_TCDETALORW"));}
	else
		viewTCORDetail = 4;
	
	if(grpRights.getFtrRightsByValue("CB_ORRESOLW")!=null && !grpRights.getFtrRightsByValue("CB_ORRESOLW").equals(""))
	{	viewORResolution = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ORRESOLW"));}
	else
		viewORResolution = 4;
	
	if(grpRights.getFtrRightsByValue("CB_PROGNOTESW")!=null && !grpRights.getFtrRightsByValue("CB_PROGNOTESW").equals(""))
	{	viewPrgrssNotes  = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PROGNOTESW"));}
	else
		viewPrgrssNotes = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUSPMNTINFW")!=null && !grpRights.getFtrRightsByValue("CB_CBUSPMNTINFW").equals(""))
	{viewCBUShipmnt  = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUSPMNTINFW"));}
	else
		viewCBUShipmnt = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CURREQPRO")!=null && !grpRights.getFtrRightsByValue("CB_CURREQPRO").equals(""))
	{currntReqst  = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CURREQPRO"));}
	else
		currntReqst = 4;

	if(grpRights.getFtrRightsByValue("CB_ADCBUHLATYPW")!=null && !grpRights.getFtrRightsByValue("CB_ADCBUHLATYPW").equals(""))
	{	viewAddTypbyCBB = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ADCBUHLATYPW"));}
	else
		viewAddTypbyCBB = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CTRESOLTNW")!=null && !grpRights.getFtrRightsByValue("CB_CTRESOLTNW").equals(""))
	{	viewCTResolution = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CTRESOLTNW"));}
	else
		viewCTResolution = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CTSPMNTINFOW")!=null && !grpRights.getFtrRightsByValue("CB_CTSPMNTINFOW").equals(""))
	{	viewCTshipmnt= Integer.parseInt(grpRights.getFtrRightsByValue("CB_CTSPMNTINFOW"));}
	else
		viewCTshipmnt = 4;
	
	if(grpRights.getFtrRightsByValue("CB_VCTINS")!=null && !grpRights.getFtrRightsByValue("CB_VCTINS").equals(""))
	{	vCTinstns = Integer.parseInt(grpRights.getFtrRightsByValue("CB_VCTINS"));}
	else
		vCTinstns = 4;
	if(grpRights.getFtrRightsByValue("CB_PFPENDINGW")!=null && !grpRights.getFtrRightsByValue("CB_PFPENDINGW").equals(""))
	{	assignOrders = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PFPENDINGW"));}
	else
		assignOrders = 4;
	if(grpRights.getFtrRightsByValue("CB_SWITCHWORK")!=null && !grpRights.getFtrRightsByValue("CB_SWITCHWORK").equals(""))
	 {	switchFlow = Integer.parseInt(grpRights.getFtrRightsByValue("CB_SWITCHWORK"));}
	else
		switchFlow = 4;
	if(grpRights.getFtrRightsByValue("CB_PENDINGREQ")!=null && !grpRights.getFtrRightsByValue("CB_PENDINGREQ").equals(""))
	 {	updtPendngReq = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PENDINGREQ"));}
	else
		updtPendngReq = 4;
	
	if(grpRights.getFtrRightsByValue("CB_NMDPSAMPLE")!=null && !grpRights.getFtrRightsByValue("CB_NMDPSAMPLE").equals(""))
	 {	updtNmdpSmpl = Integer.parseInt(grpRights.getFtrRightsByValue("CB_NMDPSAMPLE"));}
	else
		updtNmdpSmpl = 4;
	if(grpRights.getFtrRightsByValue("CB_OVRIDEDEFR")!=null && !grpRights.getFtrRightsByValue("CB_OVRIDEDEFR").equals(""))
	 {	overRideDeferred = Integer.parseInt(grpRights.getFtrRightsByValue("CB_OVRIDEDEFR"));}
	else
		overRideDeferred = 4;

	request.setAttribute("updateINDstat",updateINDstat);
	request.setAttribute("updateCBUstatus",updateCBUstatus);
	request.setAttribute("updateEligStat",updateEligStat);
	request.setAttribute("updateLicStat",updateLicStat);
	request.setAttribute("updateCliStat",updateCliStat);
	request.setAttribute("updateUploadDoc",updateUploadDoc);
	request.setAttribute("updateUnitRepStat",updateUnitRepStat);	
	request.setAttribute("updateMinCriteria",updateMinCriteria);
	request.setAttribute("updateCBUAssment",updateCBUAssment);
	request.setAttribute("updateCBUReport",updateCBUReport);
	request.setAttribute("updateFlaggedItem",updateFlaggedItem);
	request.setAttribute("updateAuditTrail",updateAuditTrail);
	request.setAttribute("updateFinalCBURev",updateFinalCBURev);
	request.setAttribute("updateFlaggedItem",updateFlaggedItem);
	request.setAttribute("updateShipmentItern",updateShipmentItern);
	request.setAttribute("updateProductInsert",updateProductInsert);
	request.setAttribute("updateQuickLnkPanel",updateQuickLnkPanel);
	request.setAttribute("updateCBUInfowidget",updateCBUInfowidget);
	request.setAttribute("updateCBUInfoS",updateCBUInfoS);
	request.setAttribute("updateIDwidget",updateIDwidget);
	request.setAttribute("updateProInfoWidget",updateProInfoWidget);
	request.setAttribute("updateInfectDisease",updateInfectDisease);
	request.setAttribute("updateidmForm",updateidmForm);
	request.setAttribute("updateCbuHLA",updateCbuHLA);
	request.setAttribute("updateHLA_CBU",updateHLA_CBU);
	request.setAttribute("updateHLA_Matrnl",updateHLA_Matrnl);
	request.setAttribute("updateHLA_Patient",updateHLA_Patient);
	request.setAttribute("updateHlthHis",updateHlthHis);	
	request.setAttribute("updateHistry_Mrq",updateHistry_Mrq);
	request.setAttribute("updateHistry_FMHQ",updateHistry_FMHQ);
	request.setAttribute("updateLabSmmry",updateLabSmmry);
	request.setAttribute("updateOthrTstRslt",updateOthrTstRslt);
	request.setAttribute("updateClnclNotes",updateClnclNotes);
	request.setAttribute("updateElgblty",updateElgblty);
	request.setAttribute("vClnclNote",vClnclNote);
	request.setAttribute("crtePckngSlip",crtePckngSlip);
	request.setAttribute("viewNote",viewNote);
	request.setAttribute("staicPanl",staicPanl);
	
	request.setAttribute("viewPatienttc",viewPatienttc);
	request.setAttribute("vPatientAntgn",vPatientAntgn);
	request.setAttribute("CaseMngrDir",CaseMngrDir);
	request.setAttribute("viewCbuAvailCon",viewCbuAvailCon);
	request.setAttribute("viewRqstCBUhis",viewRqstCBUhis);
	request.setAttribute("viewreqclincinfo",viewreqclincinfo);
	request.setAttribute("viewNotes",viewNotes);
	request.setAttribute("viewHEResol",viewHEResol);
	request.setAttribute("vPrgrssNote",vPrgrssNote);
	request.setAttribute("applyResotoCT",applyResotoCT);
	request.setAttribute("cmpltReqInfo",cmpltReqInfo);
	
	request.setAttribute("viewFnlCBURvw",viewFnlCBURvw);
	request.setAttribute("viewTCOrdrDetail",viewTCOrdrDetail);
	request.setAttribute("viewTCORDetail",viewTCORDetail);
	request.setAttribute("viewORResolution",viewORResolution);
	request.setAttribute("viewPrgrssNotes",viewPrgrssNotes);
	request.setAttribute("viewCBUShipmnt",viewCBUShipmnt);
	request.setAttribute("currntReqst",currntReqst);
	request.setAttribute("viewCTResolution",viewCTResolution);
	request.setAttribute("viewAddTypbyCBB",viewAddTypbyCBB);
	request.setAttribute("viewCTshipmnt",viewCTshipmnt);
	request.setAttribute("vCTinstns",vCTinstns);
	request.setAttribute("assignOrders",assignOrders);
	request.setAttribute("switchFlow",switchFlow);
	request.setAttribute("updtPendngReq",updtPendngReq);
	request.setAttribute("updtNmdpSmpl",updtNmdpSmpl);
	request.setAttribute("overRideDeferred",overRideDeferred);
	
	request.setAttribute("viewCTShipReport",viewCTShipReport);
	request.setAttribute("viewCTSampReport",viewCTSampReport);
	request.setAttribute("viewHEReport",viewHEReport);
	request.setAttribute("viewORReport",viewORReport);

%>
<script>
/*********autoDeferesign*************/
var element_Id="";
var element_Name="";
var mutex_Lock=false;
var autoDeferField_1="";
var autoDeferField_2="";
var autoDeferField_3="";
var autoDeferField_4="";
var elementValue ="";
var refresh_DivNo="";
var refreshDiv_Id = new Array();
var autoDeferFormSubmitParam = new Array();
var assessLinknFlag_Id = new Array();

$j(function(){
    $j("#esign_cancel").click(function(){
        if(element_Id !=""){
        	$j("#"+element_Id).removeClass('labChanges');
            $j("#"+element_Id).val(elementValue);
        	//$j("#"+element_Id+" option[value="+elementValue+"]").attr("selected", "selected");
        	parent.document.getElementById(element_Id).focus();
        if(element_Id=='bacterialResult'){
        	 bacterial(elementValue);
        }
        else if(element_Id=='fungalResultModal'){
        	fungal(elementValue);
        	if(assessLinknFlag_Id[0]=='true'){
        		$j("."+assessLinknFlag_Id[1]).show();
        		$j("."+assessLinknFlag_Id[2]).hide();
        	}
        }
        else if(element_Id=='hemoglobinScrnTest' && assessLinknFlag_Id[0]=='true'){
        		$j("."+assessLinknFlag_Id[1]).show();
        		$j("."+assessLinknFlag_Id[2]).hide();
        }
        }     
        else{
            var x = document.getElementsByName(element_Name);  
        	$j('input[name="'+element_Name+'"]').val(elementValue);
        	(x.length == 2 )?(x[1].focus()):x[0].focus();
        }
      jQuery("#modalEsign1").dialog("destroy");
        $j('#modalEsign1').css('display',"none");
		$j('#cordIdEntryValid1').css('display',"none");
		$j('#cordIdEntryMinimum1').css('display',"none");
		$j('#cordIdEntryPass1').css('display',"none");
		$j("#submitcdrsearch1").attr("disabled","disabled");
		 mutex_Lock = false;
		 element_Id ="";
		 element_Name ="";
   });

   $j("#submitcdrsearch1").click(function(){
         if(mutex_Lock == true){
         jQuery("#modalEsign1").dialog("destroy");
         
         if(autoDeferFormSubmitParam.length == 3){
           autoDeferModalFormSubmitRefreshDiv(autoDeferFormSubmitParam[0],autoDeferFormSubmitParam[1],autoDeferFormSubmitParam[2]);
          // refreshMultipleDivsOnViewClinical(refresh_DivNo,'<s:property value="cdrCbuPojo.cordID"/>');
           var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID='+'<s:property value="cdrCbuPojo.cordID"/>'+'&orderId='+<s:property value="orderId"/>+'&orderType='+'<s:property value="orderType"/>'+'&pkcordId='+'<s:property value="cdrCbuPojo.cordID"/>'+'&currentReqFlag=true';
   		     loadMoredivs(urlb,'viewcbutoppanel','cbuCharacteristicsMain','update','status');//url,divname,formId,updateDiv,statusDiv

         }
         else if(autoDeferFormSubmitParam.length == 5){             
        	 autoDeferModalFormSubmitRefreshDiv(autoDeferFormSubmitParam[0],autoDeferFormSubmitParam[1],autoDeferFormSubmitParam[2],autoDeferFormSubmitParam[3],autoDeferFormSubmitParam[4]);
             refreshMultipleDivsOnViewClinical(refresh_DivNo,'<s:property value="cdrCbuPojo.cordID"/>');                          
         }  
         else if(refresh_DivNo == 1056 && autoDeferFormSubmitParam.length == 3){
        	 loadDivWithFormSubmitDefer(autoDeferFormSubmitParam[0],autoDeferFormSubmitParam[1],autoDeferFormSubmitParam[3]);
			 refreshMultipleDivsOnViewClinical(refresh_DivNo,'<s:property value="cdrCbuPojo.cordID"/>');
             
         }
         else if(autoDeferFormSubmitParam.length == 8){
        	 modalFormPageRequest(autoDeferFormSubmitParam[0],autoDeferFormSubmitParam[1],autoDeferFormSubmitParam[2],autoDeferFormSubmitParam[3],autoDeferFormSubmitParam[4],autoDeferFormSubmitParam[5],autoDeferFormSubmitParam[6],autoDeferFormSubmitParam[7]);
        	 var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID='+'<s:property value="cdrCbuPojo.cordID"/>'+'&orderId='+<s:property value="orderId"/>+'&orderType='+'<s:property value="orderType"/>'+'&pkcordId='+'<s:property value="cdrCbuPojo.cordID"/>'+'&currentReqFlag=true';
   		     loadMoredivs(urlb,'viewcbutoppanel','cbuCharacteristicsMain','update','status');//url,divname,formId,updateDiv,statusDiv
         }
        if(refreshDiv_Id.length==2){
        	$j("#"+refreshDiv_Id[0]).css('display','block');
			$j("#"+refreshDiv_Id[1]).css('display','none');
        }
        else if(refreshDiv_Id.length!=0){
        	$j("#"+refreshDiv_Id[0]).css('display','none');
        }
         mutex_Lock = false;
        $j('#modalEsign1').css('display',"none");
		$j('#cordIdEntryValid1').css('display',"none");
		$j('#cordIdEntryMinimum1').css('display',"none");
		$j('#cordIdEntryPass1').css('display',"none");
		$j("#submitcdrsearch1").attr("disabled","disabled");
		
		refresh_DivNo="";
		element_Id ="";
		element_Name = "";
		autoDeferField_1="";
		autoDeferField_2="";
		autoDeferField_3="";
		elementValue="";
		refreshDiv_Id.length=0;
		autoDeferFormSubmitParam.length=0;
     }    
   });
});

/*********autoDeferesign*************/
function vidSaved(id){
	$j("#"+id).show();
}

function vid(num){
$j("#showDropDownDn"+num).show();
}

function showProductInsertWindow(url){
	window.open(url,"ProductInsertReport","height=700,width=1000,scrollbars=1,resizable = 1");
}

function showModelWindow(){
	var var_orderId=$j("#pkcord").val();
	showModal('Update Cord Status','updateCordStatus?orderId='+var_orderId,'300','400');
}

function minimumcriteria(cordId){
	setEmptyOrders();
	var patientId = $j('#patientId').val();
	if(patientId=="" || patientId == null || patientId=="undefined"){
		patientId=""
	}
	var idOnBag = $j('#idonBagHidden').val();
	var divIdd="leftcont";
	var divTitle="Minimum Criteria";
	var cbuRegID='<s:property value="cdrCbuPojo.registryId"/>';
	var url="minimumcriteria?cdrCbuPojo.cordID="+cordId+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&moduleEntityId='+cordId+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU"/>&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_MINIMUMCRITERIA"/>';
	var title = '<div id="notesPopup"><div style="float:left"><s:text name="garuda.minimumcriteria.label.widgetname"/> <s:text name="garuda.message.modal.headerregid"/> <s:property value="cdrCbuPojo.registryId" /> , <s:text name="garuda.cbuentry.label.idbag"/> : '+idOnBag+', <s:text name="garuda.recipient&tcInfo.label.recipientid"/> : '+patientId+'</div><div style="float:right"><span class="ui-icon ui-icon-print" onclick="printQuicklinksWidget(\''+divIdd+'\',\''+cbuRegID+'\',\''+divTitle+'\');"></span></div></div>';
	showModals(title,url,'500','1100','cbuMinimumCriteria',true,true);
	$j('#ui-dialog-title-cbuMinimumCriteria').width('95%');
}

function loadPage(url){  
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	$j('#main').html(result);
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
}
function flaggedItemsforPrint(){
	var divIdd="flaggeddivparent";
	var cbuRegID='<s:property value="cdrCbuPojo.registryId"/>';
	var divTitle='FlaggedItems';
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	showModal('<div id="notesPopup"><div style="float:left"><s:text name="garuda.cbufinalreview.label.flaggedItemsSummarty"></s:text>:<s:property value="cdrCbuPojo.registryId"/>'+patient_data+'</div><div style="float:right"><span class="ui-icon ui-icon-print" onclick="printQuicklinksWidget(\''+divIdd+'\',\''+cbuRegID+'\',\''+divTitle+'\');"></span></div></div>','getcbuFlaggedItems?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=flagged','400','750');
	$j('#ui-dialog-title-modelPopup1').width('98%');
}
function notesListforPrint(fcr){
	var divIdd="clinicalNoteFilterDiv";
	var cbuRegID='<s:property value="cdrCbuPojo.registryId"/>';
	var divTitle='Notes';
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	if(fcr=='fcrcomplete'){
	showModal('<div id="notesPopup"><div style="float:left"><s:text name="garuda.header.label.clinicalNote" />:<s:property value="cdrCbuPojo.registryId"/>'+patient_data+'</div><div style="float:right"><span class="ui-icon ui-icon-print" onclick="printQuicklinksWidget(\''+divIdd+'\',\''+cbuRegID+'\',\''+divTitle+'\');"></span></div></div>','quickNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&orderId=<s:property value="orderId" />&orderType=<s:property value="orderType" />&finalReview=true','500','850');
	}
	else{
	//showModal('<s:text name="garuda.header.label.clinicalNote"/> <s:property value="cdrCbuPojo.registryId"/>'+spacetext+'<div style="float:right"><span class="ui-icon ui-icon-print" onclick="clickheretoprint(\''+divIdd+'\',\''+cbuRegID+'\');"></span></div>','quickNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&siteName = <s:property value="cdrCbuPojo.site.siteName"/>','500','850');
		showModal('<div id="notesPopup"><div style="float:left"><s:text name="garuda.header.label.clinicalNote"/>:<s:property value="cdrCbuPojo.registryId"/>'+patient_data+'</div><div style="float:right"><span class="ui-icon ui-icon-print" onclick="printQuicklinksWidget(\''+divIdd+'\',\''+cbuRegID+'\',\''+divTitle+'\');"></span></div></div>','quickNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&siteName = <s:property value="cdrCbuPojo.site.siteName"  escapeJavaScript="true"/>&orderId=<s:property value="orderId" />&orderType=<s:property value="orderType" />','500','850');
	}
	$j('#ui-dialog-title-modelPopup1').width('98%');
}
function showEsignAssignTo(el){
	
	var t=$j(el);
	var obj=t.offset();
	var xOffset=180;
	var yOffset=30;
	if(navigator.appName=="Microsoft Internet Explorer"){yOffset=yOffset+15;}
	
	if($j('#orderType').val()=='HE'){
		xOffset=xOffset+200;
	}
	var oldAssignto=$j('#hAssignTo').val();
	
	
	if($j(el).val()!=""){
	
		var reAssignto=$j(el).val();
		
		if(oldAssignto!=null && oldAssignto!=undefined && oldAssignto!='undefined' && oldAssignto!=reAssignto){
			
			$j("#ctassignto")
			.css({left:obj.left-xOffset,top:obj.top+yOffset})
			.fadeIn("fast").show()
			.addClass('dateDivClass');
			$j('#ctassignto').show();
			
		}
	
		
	}
	
}

function showEsignconfirmShipment(el){
	if($j("#finalReviewFlag").val()=="Y" && $j("#completeReqInfoFlag").val()=="1"){
	var t=$j(el);
	var obj=t.offset();
	var xOffset=180;
	var yOffset=30;
	if(navigator.appName=="Microsoft Internet Explorer"){yOffset=yOffset+15;}
	$j("#confirmShipmentDiv")
			.css({left:obj.left-xOffset,top:obj.top+yOffset})
			.fadeIn("fast").show()
			.addClass('dateDivClass');
	$j('#confirmShipmentDiv').show();
	}else{
		alert('<s:text name="garuda.cbu.detail.shipmentConfirm"/>');
	}
}


function submitAssignto(){
	$j('#ctassignto').hide();
	var assigntoval=$j('#userIdval').val();
	setEmptyOrders();
		
	if(assigntoval!=null && assigntoval!=""){
		loaddiv('updateAssignOrder?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&assigntopk='+assigntoval+'&uflag=assignedto','currentRequestProgress');
		//setpfprogressbar();
    	//getprogressbarcolor();	
	}
	else{
		alert("<s:text name="garuda.cbu.detail.selectUserAlert"/>");
	}
	
	
}
function saveCordAvailForNmdp(){
	var var_cordId=$j("#pkcordId").val();
	var var_cbuAvail=$j('input:radio[name=iscordavailfornmdp]:checked').val();
	setEmptyOrders();
	//var var_unavailres=$j("#unavailReason").val();
	if(validateCordAvailConfirm()){
		loadMoredivs('updateCordAvailForNmdp?cordId='+var_cordId+'&cbuavail='+var_cbuAvail+'&pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val(),'ctcbuavailconfbarDiv,currentRequestProgress,taskCompleteFlagsDiv','ctOrderDetailsForm');
		setpfprogressbar();
    	getprogressbarcolor();
   		historyTblOnLoad();
		var ordType=$j('#orderType').val();
		var flag=$j('#lockflag').val();
		if(var_cbuAvail=="N" && (flag!=null && flag!='0')){
			var alldiv;
			var resoldiv;
			if(ordType=="CT"){
				alldiv="ctorderdiv";
				resoldiv="ctresolsubDiv";
			}
			else if(ordType=="HE"){
				alldiv="heorderdiv";
				resoldiv="openOrderheresol";
			}
			else if(ordType=="OR"){
				alldiv="Ororderdiv";
				resoldiv="orOrderresol";
			}
		}
	}
}
function showAntigensModal(receipantId,pkreceipant){
	showModal('Antigens Details for Recipient '+receipantId,'viewAntigens?receipantId='+pkreceipant+'&cdrCbuPojo.cordID='+$j("#pkcordId").val(),'300','700');
}
function constructTable(){
/*	var today = new Date();
	
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();

	var h=today.getHours();
	var mn=today.getMinutes()+1;

	$j( "#datepicker5" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker6" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( ".datepicker" ).datepicker({
		dateFormat: 'M dd, yy',
		minDate: new Date(y, m, d),
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true,
        prevText: '',
        nextText: '',
        closeText: 'Clear',
        currentText: 'Today'});
	$j( "#datepicker4" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	*/
	switchwf_flag=0;
	getDatePic();
	$j('#datepicker13').datepicker({
		dateFormat: 'M dd, yy',
		ampm: true
	});
	$j('#excuseformsubmitdate').datepicker({
		dateFormat: 'M dd, yy',
		ampm: true
	});
	getprogressbarcolor();
	//$j(".accordion").accordion();
	
	
	
	if($j("#datepicker15").val()!=null && $j("#datepicker15").val()!='' && $j("#datepicker15").val()!='undefined'){
		getDayfromshipdate($j("#datepicker15").val(),'cbushimentdayDiv');
	}
	if($j("#ctShipDateId").val()!=null && $j("#ctShipDateId").val()!='' && $j("#ctShipDateId").val()!='undefined'){
		getDayfromshipdate($j("#ctShipDateId").val(),'ctshimentdayDiv');
	}
	
	$j("#formsListTable").dataTable({"bSort": false,"bRetrieve": true,
		"bDestroy" :true});
	$j("#formsListTable1").dataTable({"bSort": false,"bRetrieve": true,
		"bDestroy" :true});
}

$j(function() {
	switchwf_flag=0;
	 $j('.lookupmenu :text').removeAttr("disabled");
	getDatePic();
	//datePicship();
	
	 if($j("#pkViaOther").val()==$j("#viabSelected").val()){
		 $j("#viabilityView").show();
		}else{
			$j("#viabilityView").hide();
			}

		if($j("#pkOther").val()==$j("#cfuSelected").val()){
		 $j("#cfuCountView").show();
		}else{
			$j("#cfuCountView").hide();
		}
	/*var today = new Date();
	
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();

	var h=today.getHours();
	var mn=today.getMinutes()+1;

	$j( "#datepicker5" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker6" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker7" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker4" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});*/
	/*$j('#datepicker3').datetimepicker({
		dateFormat: 'M dd, yy',
		ampm: true
	});*/
	
	$j('#datepicker13').datepicker({
		dateFormat: 'M dd, yy',
		ampm: true
	});
	$j('#excuseformsubmitdate').datepicker({
		dateFormat: 'M dd, yy',
		ampm: true
	});
	
	
		
	
	/*var v_resolbytc=$j('#ResolutionTC').val();
	var v_resolbycbb=$j('#hresolcbb').val();
	
	if((ordType!=null && ordType!='') && (v_resolbytc!=null && v_resolbytc!='') ||(v_resolbycbb!=null && v_resolbycbb!='')){
		var alldiv;
		var resoldiv;
		if(ordType=="CT"){
			alldiv="ctorderdiv";
			resoldiv="ctresolsubDiv";
		}
		else if(ordType=="HE"){
			alldiv="heorderdiv";
			resoldiv="openOrderheresol";
		}
		else if(ordType=="OR"){
			alldiv="Ororderdiv";
			resoldiv="orOrderresol";
		}
		fndisable(ordType,alldiv,resoldiv);
	}
	if((ordType!=null && ordType!='') && (flag!=null && flag!='0')){
		$j('#esignTbl').hide();
	}
	// code for disable shipment widget content in CT and OR when resolution is cancelled
	if($j('#resolbyTcval').val()==$j('#pkCanceled').val()){
		
		$j('#ctshipmentbarDiv').find('textfield').attr('disabled',true);
		$j('#ctshipmentbarDiv').find('input:text').attr('readonly',true);
		$j('#ctshipmentbarDiv').find('textarea').attr('disabled',true);
		$j('#ctshipmentbarDiv').find('input:text').removeClass('datepic'); 
		$j('#ctshipmentbarDiv').find('input:text').removeAttr('id');
		$j('#ctshipmentbarDiv').find('select').attr("disabled",true);
		$j('#ctshipmentbarDiv').find('a').attr("disabled",true)
		.addClass("ui-button-disabled ui-state-disabled")
		.attr("onclick","")
		.removeAttr('href')
		.removeAttr("onclick");
		$j('#ctshipmentbarDiv').find('button').addClass("ui-button-disabled ui-state-disabled")
		.attr("disabled",true);
		$j('#ctshipmentbarDiv').find('input:button').addClass("ui-button-disabled ui-state-disabled")
		.attr("disabled",true);
		
		
	}*/
	checkResolution();
});

function checkResolution(){
	var ctresoledit='<s:property value="hasEditPermission(#request.viewCTResolution)"/>';
	var heresoledit='<s:property value="hasEditPermission(#request.viewHEResol)"/>';
	var orresoledit='<s:property value="hasEditPermission(#request.viewORResolution)"/>';
	var resolbytc='<s:property value="orderPojo.resolByTc"/>';
	var resolbycbb='<s:property value="orderPojo.resolByCbb"/>';
	if((resolbytc!=null && resolbytc!='') || (resolbycbb!=null && resolbycbb!='')){
		var orderType=$j("#orderType").val();
		var msg="<s:text name="garuda.cbu.message.this"/> "+orderType+" <s:text name="garuda.cbu.label.reqForCord"/> "+$j('#registryId').val()+" <s:text name="garuda.cbu.label.ackCordReleased"/>";
		var pkcancel='<s:property value="pkCanceled"/>';//$j("#pkCanceled").val();
		var ackord='<s:property value="cancelled_ack_orders"/>';
		var schship='<s:property value="tskShipmentInfocompl"/>';
		var genpack='<s:property value="tskGenPackageSlipcompl"/>';
		var shipconfirm='<s:property value="tskShipmentConfirmcompl"/>';
		var nmdpship='<s:property value="tskSampleShipmentcompl"/>';
		var ackflag='<s:property value="orderPojo.ack_order"/>';
		if(((orderType=='CT' && ctresoledit=='true') || (orderType=='HE' && heresoledit=='true')) && ackflag!='Y'){
			if(((resolbytc!=null && resolbytc!='') || (resolbycbb!=null && resolbycbb!='')) && ($j("#tskAcknowledgeResolcompl").val()!=1 && $j("#acknowledgeFlag").val()!='Y')){
				if(confirm(msg)){
					openmodaltoacknowledgeResol();
				}
			}
		}else if(orderType=='OR' && orresoledit=='true' && ackflag!='Y'){
			if(confirm(msg)){
				openmodaltoacknowledgeResol();
			}
					
		}
	}
}

function fndisable(ordType,alldiv,resoldiv){
	$j('#openOrdercbuInfo').find('a').attr("disabled",true)
									.addClass("ui-button-disabled ui-state-disabled")
									.attr("onclick","")
									.removeAttr('href')
									.removeAttr("onclick");
	$j('#openOrdercbuInfo').find('button').addClass("ui-button-disabled ui-state-disabled")
	.attr("disabled",true);
	$j('#'+alldiv).find('button').addClass("ui-button-disabled ui-state-disabled")
	.attr("disabled",true);
    $j('#'+alldiv).find('input:button').attr("disabled",true);
    $j('#'+alldiv).find('a').attr("onclick","")
                    .attr("disabled",true)
                    .addClass("ui-button-disabled ui-state-disabled");
	$j('#'+alldiv).find('select').attr("disabled",true);
	$j('#'+alldiv).find('input:button').addClass("ui-button-disabled ui-state-disabled");
	$j('#'+alldiv).find('input:text').attr("disabled",true);
	$j('#'+alldiv).find('input:radio').attr("disabled",true)
                     .addClass("ui-button-disabled ui-state-disabled");
	$j('#'+alldiv).find('textfield').addClass("ui-button-disabled ui-state-disabled");
	//$j('#'+resoldiv).find('button').attr("disabled",false).removeClass("ui-button-disabled ui-state-disabled");	
	if(ordType=="CT"){
		
		$j('#completeReqInfobar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#ctshipmentbar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#additityingandtest').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#genpackageslipbar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#resRecbar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#Resolutionbar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#Acknowledgebar').css("background-color","#D2D2D2").css("border","#D2D2D2");
        $j('#'+alldiv).find('img').attr("onclick","");
        //Enabling Resolution part
	}
	if(ordType=="HE"){
		$j('#completeReqInfobar').css("background-color","#D2D2D2").css("border","#D2D2D2");
		$j('#Resolutionbar').css("background-color","#D2D2D2").css("border","#D2D2D2");
		$j('#Acknowledgebar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	}
	if(ordType=="OR"){
		$j('#completeReqInfobar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#finalReviewbar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#ctshipmentbar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#genpackageslipbar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#cordShipedbar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#genpackageslipbar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#nmdpsamplebar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#Resolutionbar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    $j('#Acknowledgebar').css("background-color","#D2D2D2").css("border","#D2D2D2");
	    
        $j('#'+alldiv).find('textarea').attr("disabled",true);
        $j('#shipsamplebarDiv').find('input:checkbox').attr("disabled",true);
	}	

	 $j(".disable_esign").hide();

	 $j('#cbuavailtd').attr("href","#cbuavailconfbarDiv")
	    .removeAttr("disabled")
	    .removeClass("ui-button-disabled ui-state-disabled");
     /*$j('#resoltd').attr("href","#ctshipmentbarDiv")
				   .removeAttr("disabled")
	 			   .removeClass("ui-button-disabled ui-state-disabled");*/
     $j('#openOrdercbuhis').find('input:radio').removeAttr("disabled")
	    										  .removeClass("ui-button-disabled ui-state-disabled");
}

function getAliquotsType(sampleType){
	var var_pkaliquots=$j("#pkaliquots").val();
	if(var_pkaliquots!=null && var_pkaliquots!="" && sampleType!=null && sampleType!="" && var_pkaliquots==sampleType){
		$j("#aliquotstypeDiv").show();
	}
	else{
		$j("#aliquotstypeDiv").hide();
		$j("#AliquotsType").val("");
	}
}



function getSampleCount(aliquotType){
	setEmptyOrders();
	if(aliquotType!=null && aliquotType!=''){
		//refreshDiv('getCodelstCustCol?pkCodelst='+aliquotType+'&cdrCbuPojo.cordID='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val(),'sampleInventoryDiv','cbuCordInformationForm');
		loadMoredivs('getCodelstCustCol?pkCodelst='+aliquotType+'&cdrCbuPojo.cordID='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val(),'sampleInventoryDiv','ctOrderDetailsForm');
	}
	
	if(aliquotType!=null && aliquotType!='' && $j("#sampleCount").val()!=null && $j("#sampleCount").val()!="" && $j("#sampleCount").val()<=0){
		alert('<s:text name="garuda.cbu.message.sampleNotExist"/>');
	}
	
}

function ctShipmentValidity(){
	if(switchwf_flag==0){
		if(validateCtShipment()){
			updateCtshipment();
			
		}
	}else if(switchwf_flag==1){
		setEmptyOrders();
		/*refreshDiv('updateCtshipment?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+
				'&switchWfFlag='+switchwf_flag+'&shipmentType='+$j("#pkCbuShipment").val()+'&prevSampleType='+$j("#prevSampleType").val()+'&prevAliqType='+$j("#prevAliqType").val(),'cbuCordInfoMainDiv','cbuCordInformationForm');*/
				loadMoredivs('updateCtshipment?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+
						'&switchWfFlag='+switchwf_flag+'&shipmentType='+$j("#pkCbuShipment").val()+'&prevSampleType='+$j("#prevSampleType").val()+'&prevAliqType='+$j("#prevAliqType").val()+'&pkPackingSlip='+$j('#pkPackingSlipId').val(),'currentRequestProgress,ctShipmentContentDiv,taskCompleteFlagsDiv','ctOrderDetailsForm');
		getAliquotsType($j("#SamTypeAvailable").val());
	}
	setpfprogressbar();
	getprogressbarcolor();
	historyTblOnLoad();
}

function updateCtshipment(){
	setEmptyOrders();
	/*refreshDiv('updateCtshipment?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+
			'&schshipdate='+$j("#ctShipDateId").val()+'&shipcompany='+$j("#shipCompany").val()+'&trackNo='+$j("#trackNo").val()+'&samtype='+$j("#SamTypeAvailable").val()+'&aliqtype='+$j("#AliquotsType").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&sampleCount='+$j("#sampleCount").val()+'&labCode='+$j("#labCode").val()+'&prevSampleType='+$j("#prevSampleType").val()+'&prevAliqType='+$j("#prevAliqType").val(),'cbuCordInfoMainDiv','cbuCordInformationForm');*/
			loadMoredivs('updateCtshipment?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+
					'&schshipdate='+$j("#ctShipDateId").val()+'&shipcompany='+$j("#shipCompany").val()+'&trackNo='+$j("#trackNo").val()+'&samtype='+$j("#SamTypeAvailable").val()+'&aliqtype='+$j("#AliquotsType").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&sampleCount='+$j("#sampleCount").val()+'&labCode='+$j("#labCode").val()+'&prevSampleType='+$j("#prevSampleType").val()+'&prevAliqType='+$j("#prevAliqType").val()+'&pkPackingSlip='+$j('#pkPackingSlipId').val(),'currentRequestProgress,ctShipmentContentDiv,taskCompleteFlagsDiv','ctOrderDetailsForm');
		getAliquotsType($j("#SamTypeAvailable").val());
		var shipmentdate=$j('#ctShipDateId').val();
		if(shipmentdate!=null && shipmentdate!='' && shipmentdate!='undefined'){
			getDayfromshipdate(shipmentdate);
		}
		setpfprogressbar();
    	getprogressbarcolor();
   		historyTblOnLoad();
}
function updateCbuShipment(){
	setEmptyOrders();
	//refreshDiv('updateCtshipment?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&shipcompany='+$j("#fkshipingCompanyId").val()+'&trackNo='+$j("#shipmentTrackingNo").val()+'&shipCont='+$j("#fkShippingContainer").val()+'&tempmoniter='+$j("#fkTempMoniter").val()+'&schshiptime='+$j("#shedShipmentTime").val()+'&padlockCombi='+$j("#padlockCombination").val()+'&paperworkLoc='+$j("#paperworkLoc").val()+'&additiShiiperDet='+$j("#additiShiiperDet").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&schshipdate='+$j("#datepicker15").val(),'cbuCordInfoMainDiv','cbuCordInformationForm');
	loadMoredivs('updateCtshipment?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&shipcompany='+$j("#fkshipingCompanyId").val()+'&trackNo='+$j("#shipmentTrackingNo").val()+'&shipCont='+$j("#fkShippingContainer").val()+'&tempmoniter='+$j("#fkTempMoniter").val()+'&schshiptime='+$j("#shedShipmentTime").val()+'&padlockCombi='+$j("#padlockCombination").val()+'&paperworkLoc='+$j("#paperworkLoc").val()+'&additiShiiperDet='+$j("#additiShiiperDet").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&schshipdate='+$j("#datepicker15").val(),'currentRequestProgress,cbuShipmentInfoContentDiv,taskCompleteFlagsDiv','ororderdetailsForm');
	setpfprogressbar();
	getprogressbarcolor();
	historyTblOnLoad();
	validateTime();
}
function updateNmdpShipment(){
	var checkedflag=$j("#nmdpShipExcuse").attr('checked');
	if(checkedflag){
		checkedflag='Y';
	}else{
		checkedflag='N';
	
	}
	setEmptyOrders();
	//refreshDiv('updateNmdpshipment?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&shipcompany='+$j("#fkNmdpshipingCompanyId").val()+'&trackNo='+$j("#researchSampleshipmentTrackingNo").val()+'&shipmentType='+$j("#pkNmdpShipment").val()+'&shipmentDate='+$j("#datepicker3").val()+'&nmdpShipExcuse='+checkedflag,'cbuCordInfoMainDiv','cbuCordInformationForm');
	loadMoredivs('updateNmdpshipment?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&shipcompany='+$j("#fkNmdpshipingCompanyId").val()+'&trackNo='+$j("#researchSampleshipmentTrackingNo").val()+'&shipmentType='+$j("#pkNmdpShipment").val()+'&shipmentDate='+$j("#datepicker3").val()+'&nmdpShipExcuse='+checkedflag,'currentRequestProgress,nmdpResearchSampleContentDiv,taskCompleteFlagsDiv','ororderdetailsForm');
	setpfprogressbar();
	getprogressbarcolor();
	historyTblOnLoad();
}


function openModalforHlaTyping(checkedFlag){
	if($j("#tskConfirmCbuAvailcompl").val()=="1"){
	var flag=$j('#hlaTypingAvailFlag').val();
	var flag1=$j('input:radio[name=HlaTypingAvail]:checked').val();
	if(flag1=='Y' && checkedFlag=='button'){
		jConfirm('<s:text name="garuda.cbu.detail.hlaupdateconfirm"/>', '<s:text name="garuda.ctOrderDetail.portletname.addTyping&Testing"/>', function(r) {
		    if(r){
				showModal('<s:text name="garuda.pf.modalwindowheader.updatehla"/><s:property value="cdrCbuPojo.registryId" />','updateHlas?update=False&source=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE)" />&entityId=<s:property value="cdrCbuPojo.cordID" />&entityType=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_TYPE" />&entitySubtype=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_SUB_TYPE" />&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&HlaTypingAvail='+flag1+'&sampleAtLab=<s:property value="sampleAtLab"/>','800','900');
		    }
		});
	}else if(flag=='Y' && checkedFlag=='link'){
		showModal('<s:text name="garuda.pf.modalwindowheader.updatehla"/><s:property value="cdrCbuPojo.registryId" />','updateHlas?update=False&source=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE)" />&entityId=<s:property value="cdrCbuPojo.cordID" />&entityType=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_TYPE" />&entitySubtype=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_SUB_TYPE" />&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&HlaTypingAvail='+flag+'&sampleAtLab=<s:property value="sampleAtLab"/>','500','900');
	}else if((flag==null || flag=='' || flag=='N') && checkedFlag=='link'){
		alert("<s:text name="garuda.cbu.detail.hlaInfoConfirm"/>");
	}else if((flag1==null || flag1=='' || flag1=='N') && checkedFlag=='button'){
	}
	}else{
		alert('<s:text name="garuda.common.validation.cordavailconfirm"/>');
	}
	
}

function getconfirmAlert(flag){
	if(flag=='Y'){
	jConfirm('<s:text name="garuda.common.dialog.canConfirm"/>', '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
    //jAlert('Confirmed: ' + r, 'Confirmation Results');
    jAlert('<s:text name="garuda.cbu.lab.labSummaryAlert"/>');
});
}	
}
function enableAcknowledgeDate(value){
	var temp = $j('#pkTempUnavailable').val();
	if(value == temp ){
		$j("#acknowledgeDiv").attr('style','display:block');
	}
	else{
		$j("#acknowledgeDiv").attr('style','display:none');
	}
}
$j(document).ready(function(){
	
	maximizePage();
	Fnresol();
	
    var vordStat=$j('#orderstatus').val();
    var pkresolved=$j('#pkresolveid').val();
	var vflag=$j('#flag').val();
	$j('button,input[type=submit],input[type=button]').button();
	var ordType=$j('#orderType').val();
	if(ordType=='CT' || ordType=='HE' || ordType=='OR' ){
		$j("#cbuRHistory1").attr('checked',true);
		$j('#cReqhistory').show();
		historyTblOnLoad();
		 
	}
	openDivbyDefault();
});
function f_callCompleReqInfo(cbuRegId,pkcordId,ordId,ordType){
	fn_CRIGetShowModals('<s:text name="garuda.completeReqInfo.label.modal.title"/>:<s:property value="cdrCbuPojo.registryId" />','completeReqInfo?esignFlag=review&cdrCbuPojo.cordID='+pkcordId+'&orderId='+ordId+'&orderType='+ordType+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_CRI" />','570','1100','dialogForCRI');
}

function fn_ResetHistorySort(){
	if($j('#history_sort_col').val()!=undefined && $j('#history_sort_col').val()!=null && $j('#history_sort_col').val()!=""){
			
			$j('#history_sort_col').val('');
			$j('#history_sort_type').val('');
			var oTable = $j('#cbuhistorytbl').dataTable();
			oTable.fnSort( [ [1,'desc'] ] );
			oTable.fnDraw();
		}
	} 

function historyTblOnLoad(){

	var staticpanelwidth=$j(".viewcbutoppanel").width();
	var ordertype=$j('#orderType').val();
	if(ordertype=='CT' || ordertype=='HE' || ordertype=='OR'){
		staticpanelwidth=$j(".viewcbutoppanel").width();
		staticpanelwidth=(staticpanelwidth/2);
	}
	var staticpanelheight=$j(".viewcbutoppanel").height();
	var columnwidth=staticpanelwidth-5;
	if(!$j('#cbuhistorytbl').hasClass('loadedCM')){
		fn_events1('cbuhistorytbl','targetall1','ulSelectColumn1','showsRowcbuhistorytbl','PENDING');
		$j('#cbuhistorytbl').addClass('loadedCM');
	}
	
	var is_all_entries=$j('#showsRowcbuhistorytbl').val();
	var pageNo=$j('#paginateWrapper5').find('.currentPage').text();
	var showEntries=$j('#history_tbl_se').val();
	var RecCount=$j('#htentries').val();
	var sort=1;
	var sortDir="desc";
	var SortParm=$j('#history_sort_col').val();
	var seachTxt="";
	var preTxt=$j('#history_search_txt').val();
	
	if((preTxt!=null && $j.trim(preTxt)!="")){
		seachTxt=preTxt;
	}
	

	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#history_sort_col').val();
		sortDir=$j('#history_sort_typ').val();
	}	
	

	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#history_tbl_se').val(is_all_entries);
		showEntries=$j('#history_tbl_se').val();
	}
	
	showEntriesTxt='Show <select name="showsRow" id="showsRowcbuhistorytbl" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+RecCount+'">ALL</option></select> Entries';
	
	
	if(showEntries=='ALL'){

					hstrytbl1 =	$j('#cbuhistorytbl').dataTable({
						"sScrollY": staticpanelheight,
						"sScrollX":columnwidth,
						"sDom": "frtiS",
						"sAjaxSource": 'getJsonPagination.action?TblFind=historytbl&allEntries=ALL&pkcordId='+$j("#pkcordId").val()+'&pkorderid='+$j("#orderId").val(),
						"bServerSide": true,
						"bProcessing": true,
						"bRetrieve" : true,
						"bDestroy": true,
						"oSearch": {"sSearch": seachTxt},
						"aaSorting": [[ sort, sortDir ]],
						"bSortCellsTop": true,
						"bDeferRender": true,
						"sAjaxDataProp": "aaData",
						"aoColumnDefs": [
										   {		
												"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
						                	 	return "";
						                  }},
						                  {		
						                	    "aTargets": [1], "mDataProp": function ( source, type, val ) {
					                	 	     return source[0];
					                	  }},
						                  { 	
					                	         "aTargets": [2], "mDataProp": function ( source, type, val ) { 
					                	    	 return source[1];
						                  }},
										  {
						                		"aTargets": [3], "mDataProp": function ( source, type, val ) { 
												 return source[2];
										  }},
										  {
												 "aTargets": [4], "mDataProp": function ( source, type, val ) {
											 	 return source[3];
										  }}
										],
					"fnInitComplete": function(){
												 hstrytbl1.fnAdjustColumnSizing(false);
										},
										"fnDrawCallback": function() { 
											             setSortData('historyTHeadTr','history_sort_col','history_sort_typ');
											             ColManForAll('cbuhistorytbl','targetall1','true');
														 $j('#cbuhistorytbl_wrapper').find('.dataTables_scrollFoot').hide();
													     $j('#cbuhistorytbl_info').show();
														 $j("#showsRowcbuhistorytbl option:contains('ALL')").attr('selected', 'selected');
										}
					});  
					$j('#cbuhistorytbl_filter').before(showEntriesTxt);
					
	
		}else{
	
			        //alert("columnwidth::::"+columnwidth);
			        
					hstrytbl1 =	$j('#cbuhistorytbl').dataTable({
						"sScrollY": staticpanelheight,
						"sScrollX":columnwidth,
						"sAjaxSource": 'getJsonPagination.action?TblFind=historytbl&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&pkcordId='+$j("#pkcordId").val()+'&pkorderid='+$j("#orderId").val(),
						"bServerSide": true,
						"bProcessing": true,
						"oSearch": {"sSearch": seachTxt},
						"bRetrieve" : true,
						"bDestroy": true,
						"aaSorting": [[ sort, sortDir ]],
						"bSortCellsTop": true,
						"bDeferRender": true,
						"sAjaxDataProp": "aaData",
						"aoColumnDefs": [
										   {		
												"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
						                	 	return "";
						                  }},
						                  {		
						                	    "aTargets": [1], "mDataProp": function ( source, type, val ) {
					                	 	     return source[0];
					                	  }},
						                  { 	
					                	         "aTargets": [2], "mDataProp": function ( source, type, val ) { 
					                	    	 return source[1];
						                  }},
										  {
						                		"aTargets": [3], "mDataProp": function ( source, type, val ) { 
												 return source[2];
										  }},
										  {
												 "aTargets": [4], "mDataProp": function ( source, type, val ) {
											 	 return source[3];
										  }}
										  		
										],
					"fnInitComplete": function(){
											hstrytbl1.fnAdjustColumnSizing(false);
										},
										"fnDrawCallback": function() { 
															setSortData('historyTHeadTr','history_sort_col','history_sort_typ');
															ColManForAll('cbuhistorytbl','targetall1','true');
										}
					});  
    }
	$j('#cbuhistorytbl_info').hide();
	$j('#cbuhistorytbl_paginate').hide();
	$j('#cbuhistorytbl_length').empty();
	$j('#cbuhistorytbl_length').replaceWith(showEntriesTxt);
	if($j('#hentries').val()!=null || $j('#hentries').val()!=""){
		$j('#showsRowcbuhistorytbl').val($j('#hentries').val());
		}
	$j('#showsRowcbuhistorytbl').change(function(){
		var textval=$j('#showsRowcbuhistorytbl :selected').html();
		$j('#history_tbl_se').val(textval);
		paginationFooter(0,"getCbuHistory,showsRowcbuhistorytbl,temp,cbuCordInformationForm,cbuHistoryForm,historyTblOnLoad,inputs");
	});
	
	
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('position','static');
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('z-index','1000');
	
	/*	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').after('<div style="clear:both" class="clearDiv"></div>');

 	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHeadInner table').width(columnwidth);
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollFootInner table').width(columnwidth);
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').find('.dataTables_scrollHeadInner table').width(columnwidth);
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollBody table').width(columnwidth); */
	
	
	$j('#cbuhistorytbl_wrapper').css('overflow','hidden');
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollBody').scroll(function(){
		$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('overflow','hidden');
	});
	$j('#cbuhistorytbl_wrapper').find('#thSelectColumn div').click(function(){
		$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	});
	
	if(! $j("#cbuRHistory1").is(':checked')){
		$j("#cbuHistory1").attr('checked',true);
		$j('#chistory').show();
	}

	if(!$j('#cbuReqHistoryTbl').hasClass('loadedCM')){
		fn_events('cbuReqHistoryTbl','targetall2','ulSelectColumn2');
		$j('#cbuReqHistoryTbl').addClass('loadedCM');
	}
	
	
	$j('.detailhstry').css('max-height',staticpanelheight);
	
	$j('#cbuHistory1').bind('click',function(ui){
		if(!$j('#cbuHistory1').hasClass('alignmentModi')){
		var oTable = $j('div.dataTables_scrollBody> #cbuhistorytbl', ui.panel).dataTable();
		if ( oTable.length > 0 ) {
			oTable.fnAdjustColumnSizing(false);
			$j('#cbuHistory1').addClass('alignmentModi');
		}
		}
	});
	
	
	
	$j('#cbuhistorytbl_filter').find(".searching1").keyup(function(e){
		
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#history_search_txt').val(val);
		  if(e.keyCode==13)
		  {
				  paginationFooter(0,"getCbuHistory,showsRowcbuhistorytbl,temp,cbuCordInformationForm,cbuHistoryForm,historyTblOnLoad,inputs");
		  }
	});

	
	$j('.orderHistorydata').bind('click',function(ui){	
		if(!$j('.orderHistorydata').hasClass('loaded')){
		var oTable = $j('div.dataTables_scrollBody> #cbuhistorytbl', ui.panel).dataTable();
		if ( oTable.length > 0 ) {
			oTable.fnAdjustColumnSizing(false);
			$j('.orderHistorydata').addClass('loaded');
		}
		}
	}); 
	
	
}
function getUnavailReason(val){
	 if(val=='N'){
		 $j("#esignForCordAvail").hide();
		 closeModal();
		showcbustatus($j("#pkcordId").val(),'','<s:property value="cdrCbuPojo.registryId"/>');
	}else{
		$j("#esignForCordAvail").show();
	}		
}

function validateCordAvailConfirm(){
	var iscordavail=$j('input:radio[name=iscordavailfornmdp]:checked').val();
	if(iscordavail==null || iscordavail==''){
		$j("#errorcordAvailConfirmDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.cbu.cord.nmdpAvailable"/>");
		 });
		$j("#errorcordAvailConfirmDiv").show();
		return false;
	}
	
	else{
		$j("#errorcordAvailConfirmDiv").find("span").each(function(){
      		$j(this).text("");
	 });
		$j("#errorcordAvailConfirmDiv").hide();
	
		return true;
	}
}
function setpfprogressbar(){
	$j("#cbuavailconfbar").progressbar({value: 100});
	$j("#completeReqInfobar").progressbar({value: 100});
	$j("#additityingandtest").progressbar({value: 100});
	$j("#resRecbar").progressbar({value: 100});
	$j("#Resolutionbar").progressbar({value: 100});
	$j("#Acknowledgebar").progressbar({value: 100});
	$j("#ctshipmentbar").progressbar({value: 100});
	$j("#ctshipdatebar").progressbar({value: 100});
	$j("#genpackageslipbar").progressbar({value: 100});
	$j('#finalReviewbar').progressbar({value: 100});
	$j('#cordShipedbar').progressbar({value: 100});
	$j('#nmdpsamplebar').progressbar({value: 100});
	
	/********Code for Searchable Select Box for invoking after every refresh of Current Request Progress*********/
	getSearchableSelect('userIdval','id');
	/*****************/
	
}
function getprogressbarcolor(){

	
	var currentOrdStat=$j('#orderstatus').val();
	var disableUnComplTask=0;
	var todate='';
	var schshipdat='';
	if($j("#schshipdate").val()!=null && $j("#schshipdate").val()!=""){
		schshipdat=$j.datepicker.parseDate("M dd, yy",$j("#schshipdate").val());
		todate=$j.datepicker.parseDate("M dd, yy",$j("#todate").val());
	}
	
	if(currentOrdStat=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED)" />' || currentOrdStat=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)" />'){
		disableUnComplTask=1;
	}
	
	
	if($j("#tskConfirmCbuAvailcompl").val()==1 || $j("#cordavailflag").val()=='Y'){
		$j("#cbuavailconfbar").addClass('progressbarselected');
		$j("#cbuavailconfbar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	
	
	
	/**Differtiate unneed task**/
	
	if(disableUnComplTask==1 && ( $j("#tskConfirmCbuAvailcompl").val()!=1 && $j("#cordavailflag").val()!='Y')){
		$j("#cbuavailconfbar").addClass('UnCompleTask');
		$j("#cbuavailconfbar").find('div').after('<img src="images/tsk_nt_needed.png"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	
	/***************************/
	
	if($j("#tskAdditiTypingcompl").val()==1 || ($j("#hlaTypingAvailFlag").val()!=null && $j("#hlaTypingAvailFlag").val()!='')){
		$j("#additityingandtest").addClass('progressbarselected');
		$j("#additityingandtest").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"  ></img>');
	}
	
	/**Differtiate unneed task**/
	
	if(disableUnComplTask==1 && ($j("#tskAdditiTypingcompl").val()!=1 && ($j("#hlaTypingAvailFlag").val()==null || $j("#hlaTypingAvailFlag").val()==''))){
		$j("#additityingandtest").addClass('UnCompleTask');
		$j("#additityingandtest").find('div').after('<img src="images/tsk_nt_needed.png"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	} 
	
	/***************************/
	
	
	
	if($j("#tskShipmentInfocompl").val()==1 || $j("#shipschFlag").val()=='Y'){
		$j("#ctshipmentbar").addClass('progressbarselected');
		$j("#ctshipmentbar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	
	/*******Resetting Shipment information flag to not completed *******/
	if($j("#tskShipmentInfocompl").val()!=1 && $j("#shipschFlag").val()!='Y'){
		$j("#ctshipmentbar").removeClass('progressbarselected');
	}
	/*********************************************/
	
	
	/**Differtiate unneed task**/
	
	if(disableUnComplTask==1 && ($j("#tskShipmentInfocompl").val()!=1 && $j("#shipschFlag").val()!='Y')){
		$j("#ctshipmentbar").addClass('UnCompleTask');
		$j("#ctshipmentbar").find('div').after('<img src="images/tsk_nt_needed.png"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	if(schshipdat!=null && schshipdat!='' && schshipdat<todate){
		$j("#ctshipdatebar").addClass('progressbarselected');
		$j("#ctshipdatebar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	if(disableUnComplTask==1 && (schshipdat==null || schshipdat=='' || schshipdat>=todate)){
		$j("#ctshipdatebar").addClass('UnCompleTask');
		$j("#ctshipdatebar").find('div').after('<img src="images/tsk_nt_needed.png"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	if(schshipdat==null || schshipdat=='' || schshipdat>=todate){
		$j("#ctshipdatebar").removeClass('progressbarselected');
	}
	
	/***************************/
	
	if($j("#tskGenPackageSlipcompl").val()==1 || $j("#packageslipFlag").val()=='Y'){
		$j("#genpackageslipbar").addClass('progressbarselected');
		$j("#genpackageslipbar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	
	/*******Resetting Pakage Slip to not completed *******/
	if($j("#tskGenPackageSlipcompl").val()!=1 && $j("#packageslipFlag").val()!='Y'){
		$j("#genpackageslipbar").removeClass('progressbarselected');
	}
	/*********************************************/

	
	/**Differtiate unneed task**/
	
	if(disableUnComplTask==1 && ($j("#tskGenPackageSlipcompl").val()!=1 && $j("#packageslipFlag").val()!='Y')){
		$j("#genpackageslipbar").addClass('UnCompleTask');
		$j("#genpackageslipbar").find('div').after('<img src="images/tsk_nt_needed.png"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	
	/***************************/
	
	
	
	if($j("#tskShipmentConfirmcompl").val()==1 || $j("#cordShipedFlag").val()=='Y'){
		$j("#cordShipedbar").addClass('progressbarselected');
		$j("#cordShipedbar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass" class="printClass"  ></img>');
	}
	
	/**Differtiate unneed task**/
	
	if(disableUnComplTask==1 && ($j("#tskShipmentConfirmcompl").val()!=1 && $j("#cordShipedFlag").val()!='Y')){
		$j("#cordShipedbar").addClass('UnCompleTask');
		$j("#cordShipedbar").find('div').after('<img src="images/tsk_nt_needed.png"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	/***************************/
	
	
	if($j("#tskReqClincInfocompl").val()==1 || $j("#completeReqInfoFlag").val()=='Y'){
		$j("#completeReqInfobar").addClass('progressbarselected');
		$j("#completeReqInfobar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"  ></img>');
	}else{
		$j("#completeReqInfobar").removeClass('progressbarselected');
	}
	
	
	
	if($j("#tskSampleShipmentcompl").val()==1 || $j("#nmdpSampleShipped").val()=='Y'){
		if($j("#nmdpsamplebar").hasClass('UnCompleTask')){
			$j("#nmdpsamplebar").removeClass('UnCompleTask')
		}
		$j("#nmdpsamplebar").addClass('progressbarselected');
		$j("#nmdpsamplebar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"  ></img>');
	}
	
	
	/**Differtiate unneed task**/
	
	if(disableUnComplTask==1 && ($j("#tskSampleShipmentcompl").val()!=1 && $j("#nmdpSampleShipped").val()!='Y')){
		$j("#nmdpsamplebar").addClass('UnCompleTask');
		$j("#nmdpsamplebar").find('div').after('<img src="images/tsk_nt_needed.png"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	/***************************/
	
	
	if($j("#tskFinalCbuReviewcompl").val()==1 || $j("#finalReviewTaskFlag").val()=='Y'){
		$j("#finalReviewbar").addClass('progressbarselected');
		$j("#finalReviewbar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"  ></img>');
	}else{
		$j("#finalReviewbar").removeClass('progressbarselected');
	}
	
	
	/**Differtiate unneed task**/
	
	if(disableUnComplTask==1 && ($j("#tskFinalCbuReviewcompl").val()!=1 && $j("#finalReviewTaskFlag").val()!='Y')){
		$j("#finalReviewbar").addClass('UnCompleTask');
		$j("#finalReviewbar").find('div').after('<img src="images/tsk_nt_needed.png"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	/***************************/
	
	
	if($j("#recRecDateFlag").val()!='Not Available' && ($j("#recRecDateFlag").val()!=null && $j("#recRecDateFlag").val()!='')){
		$j("#resRecbar").addClass('progressbarselected');
		$j("#resRecbar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"  ></img>');
	}
	
	/**Differtiate unneed task**/
	
	if(disableUnComplTask==1 && ($j("#recRecDateFlag").val()=='Not Available' || ($j("#recRecDateFlag").val()==null || $j("#recRecDateFlag").val()==''))){
		$j("#resRecbar").addClass('UnCompleTask');
		$j("#resRecbar").find('div').after('<img src="images/tsk_nt_needed.png"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	
	/***************************/
	
	
	
	if($j("#resolByTCFlag").val()!=null && $j("#resolByTCFlag").val()!=''){
		$j("#Resolutionbar").addClass('progressbarselected');
		$j("#Resolutionbar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"  ></img>');
	}
	if($j("#resolByCBBFlag").val()!=null && $j("#resolByCBBFlag").val()!=''){
		$j("#Resolutionbar").addClass('progressbarselected');
		$j("#Resolutionbar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"  ></img>');
	}
	if($j("#tskAcknowledgeResolcompl").val()==1 || $j("#acknowledgeFlag").val()=='Y' || '<s:property value="orderPojo.ack_order"/>'=='Y'){
		$j("#Acknowledgebar").addClass('progressbarselected');
		$j("#Acknowledgebar").find('div').after('<img src="images/pfprogressbar.jpg"  class="printClass" style="display: none;width: 100%; height: 30px;border: none;"  ></img>');
	}
}

function showReqClinicInfo(){
	setEmptyOrders();
	fn_CRIGetShowModals('Required Clinical Information Checklist for CBU Registry ID:<s:property value="cdrCbuPojo.registryId"/>','completeReqInfo?esignFlag=review&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_CRI" />','550','1100','dialogForCRI');
}

function showFinalCbuReviewInfo(){
	setEmptyOrders();
	
		if($j("#completeReqInfoFlag").val() == "1"){
			if ( $j.browser.msie ) {
			if (document.all) {
				   var xMax = screen.width, yMax = screen.height;
				  }
				  else if (document.layers) {
				   var xMax = window.outerWidth, yMax = window.outerHeight;
				  }
				  else {
				   var xMax = 640, yMax=480;
				  } 
				window.open('finalCbuReview?licenceUpdate=False&esignFlag=review&cdrCbuPojo.cordID='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR" />','test','width='+(window.screen.availWidth)*5/6+',height='+(window.screen.availHeight)*5/6+',top=0,left=0,resizable=yes');
		}
		else{
			window.open('finalCbuReview?licenceUpdate=False&esignFlag=review&cdrCbuPojo.cordID='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR" />');
		}
		}
		else{
			alert("<s:text name='garuda.cbu.CRI.alert'/>");
		}
}

function setEmptyOrders(){
	var orderId = $j("#orderId").val();
	var orderType = $j("#orderType").val();

	if(typeof(orderId)=="undefined"){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")!=-1){
		$j("#orderId").val("");
		orderId="";
	}
	
	if(typeof(orderType)=="undefined"){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")!=-1){
		$j("#orderType").val("");
		orderType="";
	}
}
function showModalCbuStatus(title,url,height,width)
{
	
	$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
	if($j('#modelPopup1').html()==null){
		var obj="<div id='modelPopup1'></div>";
		$j('body').append(obj);
	}
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title = title + patient_data;
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#modelPopup1").html(progressMsg);
		$j("#modelPopup1").dialog(
				   {autoOpen: false,
					 open: function(event, ui) {
					   $j(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
					   $j(this).closest('.ui-dialog').find('.ui-dialog-title').append('<span  class="ui-dialog-titlebar-close ui-corner-all" style="cursor: pointer;"><span class="ui-icon ui-icon-closethick"/></span>');
					   $j('.ui-dialog-titlebar-close').bind("mouseover",function(){ 
						    $j(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').addClass('ui-state-hover');
						});
						$j('.ui-dialog-titlebar-close').bind("mouseout",function(){ 
						    $j(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-state-hover');
						});
						$j('.ui-dialog-titlebar-close').bind("click",function(){ 
							  conformcloseModal();
						});
					 },   
					title: title  ,
					resizable: true,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {						
		      		    jQuery("#modelPopup1").dialog("destroy");
				    }
				   }
				  ); 
		$j("#modelPopup1").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#modelPopup1").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
	$j('.progress-indicator').css( 'display', 'none' );
	
}
function showcbustatus(cordId,applyResolFlag,regisId, autoDeferRollbackStatus){

	var cbustatustype=$j("#cbustatustype").val();
	setEmptyOrders();
	var isCordAvail=$j('input:radio[name=iscordavailfornmdp]:checked').val();
	if(typeof(isCordAvail)=='undefined'){
		$j('input:radio[name=iscordavailfornmdp]:checked').val("");
		isCordAvail="";
	}	
	if($j("#orderId").val()!=null && $j("#orderId").val()!=''){
		applyResolFlag='N';
	}
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	
	var url='updateCbuStatus?licenceUpdate=False&cbustatusin=IS_LOCAL_REG&cdrCbuPojo.cordID='+cordId+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&applyResol='+applyResolFlag+'&iscordavailfornmdp='+isCordAvail+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_CBU_STATUS" />&autoDeferRollbackStatus='+autoDeferRollbackStatus;
	showModalCbuStatus('<s:text name="garuda.common.title.cbustatustitle"/>:<s:property value="cdrCbuPojo.registryId" />',url,'450','700');
} 
	function editTest(rowIndex,pkPatLabs){
		  var specCollDate = $j("#datepicker1").val();
		  var specimenId =$j("#fkSpecimenId").val();
		  showModal('Edit Test','editAddNewTestLabs?rowIndex='+rowIndex+'&specimenId='+specimenId+'&pkPatLabs='+pkPatLabs+'&specCollDate='+specCollDate,'500','700'); 
	}
	function refreshResol(url,divname1,divname2,formId){
		//$('.progress-indicator').css( 'display', 'block' );
		if($j("#"+formId).valid()){
		$j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        data : $j("#"+formId).serialize(),
	        success: function (result){
	        	$j('.ui-datepicker').html("");
	        	//$('.tabledisplay').html("");
	            var $response=$j(result);
	            //query the jq object for the values
	            var errorpage = $response.find('#errorForm').html();
	           // alert(oneval);
	           // var subval = $response.find('#sub').text();
	            if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{
	            	$j("#"+divname1).replaceWith($j('#'+divname1, $j(result)));
	            	$j("#"+divname2).replaceWith($j('#'+divname2, $j(result)));
	            	constructTable();
	            }
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }
		});
	  }
	}

function Fnresol(){

	var today = new Date();
	
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();

	var h=today.getHours();
	var mn=today.getMinutes()+1;
	$j( "#availdatepicker" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	if($j('#availdtval').val()!=""){
		$j("#availdatepicker").attr('disabled',"true");
	}
	if($j('#ResoluAck').val()!=null && $j('#ResoluAck').val()!=""){
		$j("#ResoluAck").attr('disabled',"true");
	}
	
	if($j('#ResoluAck').val()=='Y'){
		$j("#ResoluAck").attr('checked',"true");
		$j('#esignTbl').css('display','none');
	}
	
	if($j('#rflag').val()=="True"){
		$j("#ResolutionCBB").val($j('#resolutionCode1').val());
	}
}
function validateResol(){
var flag=0;
	
	if($j('#ResolutionCBB').val()=="" && $j('#ResolutionTC').val()==null){
		$j('#showerrormsg').attr('style','display:block');
		var msg="<s:text name="garuda.cbu.resolution.selectResol"/>";
		$j('#errormsg').html(msg);
		flag=1;
	}
	if($j('#availdatepicker').val()==""){
			var msg="<s:text name="garuda.common.validation.availabledate"/>";
			$j('#errordate').html(msg);
			flag=1;
	}
	if($j('#ResolutionCBB').val()==null && $j('#ResolutionTC').val()==""){
		$j('#showerrormsg').attr('style','display:block');
		var msg="<s:text name="garuda.cbu.resolution.notReceived"/>";
		$j('#errormsg').html(msg);
		flag=1;
		if(!$j('#ResoluAck').is(':checked')){
			$j('#showerrormsg').attr('style','display:block');
			var msg="<s:text name="garuda.cbu.resolution.resoluAck"/>";
			$j('#errorcheck').html(msg);
			flag=1;
		}
	}
	
		
	
	if(flag==0){	
		if($j('#orderType').val()=='CT')submitCTResolution();
		if($j('#orderType').val()=='HE')submitHeResolution();
		if($j('#orderType').val()=='OR')submitcbuOrdResolution();
	}
	
}

function getDayfromshipdate(shipdate,daydiv){
	var shipdate1="";
	if(shipdate!=null && shipdate!=""){
		shipdate1=shipdate.substring(0,12);
	}
	
	var dateNo="-1";
	
	if(shipdate1!=null && shipdate1!=""){
		shipdate1 = dateFormat(shipdate1,'mmm dd, yyyy');
		var dat=$j.datepicker.parseDate("M dd, yy",shipdate1);
			dateNo=dat.getDay();
		}
	if(dateNo==0){
	$j("#"+daydiv).find("span").each(function(){
  		$j(this).text("<s:text name="garuda.cbu.shipment.shipmentDaySun"/>");
	 });
	}
	if(dateNo==1){
	$j("#"+daydiv).find("span").each(function(){
  		$j(this).text("<s:text name="garuda.cbu.shipment.shipmentDayMon"/>");
	 });
	}
	if(dateNo==2){
		$j("#"+daydiv).find("span").each(function(){
	  		$j(this).text("<s:text name="garuda.cbu.shipment.shipmentDayTue"/>");
		 });
		}
	if(dateNo==3){
		$j("#"+daydiv).find("span").each(function(){
	  		$j(this).text("<s:text name="garuda.cbu.shipment.shipmentDayWed"/>");
		 });
		}
	if(dateNo==4){
		$j("#"+daydiv).find("span").each(function(){
	  		$j(this).text("<s:text name="garuda.cbu.shipment.shipmentDayThurs"/>");
		 });
		}
	if(dateNo==5){
		$j("#"+daydiv).find("span").each(function(){
	  		$j(this).text("<s:text name="garuda.cbu.shipment.shipmentDayFri"/>");
		 });
		}
	if(dateNo==6){
		$j("#"+daydiv).find("span").each(function(){
	  		$j(this).text("<s:text name="garuda.cbu.shipment.shipmentDaySat"/>");
		 });
		}
	
}


function connectToShipperWebsite(shipper,trackid){
	try{
	if(shipper!=null && shipper!=""){
		setEmptyOrders();
		getShipperUrlasJson('getShipperUrl?pkCodelst='+shipper+'&cdrCbuPojo.cordID='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val())
	}
	var url=$j("#shipperUrl").val();
	if(url!=null && url!=""){
		if(shipper==$j("#pkworldcourier").val()){
			window.open(url,'','toolbar=no,scrollbars=yes,resizable=yes,status=yes,width=950,height=700');
		}
		else{
			window.open(url+trackid,'','toolbar=no,scrollbars=yes,resizable=yes,status=yes,width=950,height=700');
		}
	}
	else{
		alert("<s:text name="garuda.cbu.shipment.shipperWebsite"/>");
	}
	}catch(err){
		alert(err.message);
	}
}
function getShipperUrlasJson(url){
	$j.ajax({
        type: "POST",
        url : url,
        async:false,
        dataType: 'json',
        success: function (result){
        	var resulobj = result.codeLstCustColLst;
        	if(resulobj != null){	
				//alert("inside message true"+resulobj);
				var shipperUrl;				
				$j(resulobj).each(function(i,v){
					shipperUrl = v[0];
				});
			//	alert("elem:"+shipperUrl);
			//	alert("result.pkworldcourier:::"+result.pkworldcourier);
				$j('#shipperUrl').val(shipperUrl);
				$j('#pkworldcourier').val(result.pkworldcourier);
       		 }
        }
 	});

	
}

function showCbbDetails(cbbid,siteId){
	showModal('Cord Blood Bank ID:<s:property value="cdrCbuPojo.site.siteIdentifier"/>','showCBBDefaults?cbbDefault='+cbbid+'&diableCBBEditAndPrintFlag=1'+'&moduleEntityId='+cbbid+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBB" />&moduleEntityIdentifier='+siteId+'&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_CBB" />','400','500');
}
function callPending(){
	window.location="pendingOrders";
}

function changeWorkflow(){
	if($j("#packageslipFlag").val()=='Y' || ($j("#resolByTCFlag").val()!=null && $j("#resolByTCFlag").val()!='') || 
			$j("#acknowledgeFlag").val()=='Y' ||($j("#recRecDateFlag").val()!='Not Available' && $j("#recRecDateFlag").val()!=null && $j("#recRecDateFlag").val()!='')){
		alert('You can not switch the Workflow');
		}
	else{
		if($j("#ctShipDateId").val()!=null && $j("#ctShipDateId").val()!=''){
			jConfirm('The CT Ship Date for '+$j("#registryId").val()+' must be removed\n before switching the CT workflow', 'Switch Workflow', function(r) {
			    if(r){
					switchwf_flag=1;
					openmodaltoeditshipment();
			    }
			});
		}else{
			setEmptyOrders();
			showModals('Confirm Switch Workflow','openSwitchWorkflow?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&sampleAtLab='+$j("#sampleAtLab").val(),'400','700','switchwfmodaldiv');
		}
	}

}
function getsearchResultsBack(){
	//window.location='getMainCBUDetails?cbuid='+$j("#cbuid").val();
	document.getElementById('sampleform').submit();
}



var ctOrderDetFlag=0,currReqflag=0,cordavailconfirmflag=0;
function getCaseManagerPatientDetails(){
	setEmptyOrders();
	if($j('#openOrderrecTcInfospan').is(".ui-icon-plusthick") && ctOrderDetFlag==0){
		
		loadMoredivs('getCaseManagerDetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val(),'caseManagerPatientandTCdetailsDiv','cbuCordInformationForm');
		ctOrderDetFlag=1;
		
	}
	else if($j('#openOrderrecTcInfospan').is(".ui-icon-minusthick")) {
		$j('#caseManagerPatientandTCdetailsDiv').show();
	}
}

function getCurrentRequestProgressDetails(divIds){
	setEmptyOrders();
	if($j('#currentReqProSpan').is(".ui-icon-plusthick")){
		if(currReqflag==0){		
			loadMoredivsPF('getCurrReqDetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val(),divIds,'cbuCordInformationForm');
			currReqflag=1;
			ctOrderDetFlag=1;
			cttcorderdetflag=1;
			ctshipdetflag=1;
			ortcdetflag=1;
			cbushipflag=1;
			addititypingflag=1;
			var sampleType=$j("#SamTypeAvailable").val();
			$j("#userIdval").width("100px");
			getSearchableSelect('userIdval','id');
			getAliquotsType(sampleType);
			setpfprogressbar();
			getprogressbarcolor();
			getDayfromshipdate($j("#datepicker15").val(),'cbushimentdayDiv');
			getDayfromshipdate($j("#ctShipDateId").val(),'ctshimentdayDiv');
       		historyTblOnLoad();
		}
	}
	else if($j('#currentReqProSpan').is(".ui-icon-minusthick")) {
		$j('#currentRequestProgressMainDiv').show();
	}
}

function updateLastViewBy(){
	loadMoredivs('updateLastViewBy?orderId='+$j("#orderId").val(),'dummyDiv','cbuCordInformationForm');
}

function getCordAvailDetails(){
	setEmptyOrders();
	if($j('#cordAvailspan').is(".ui-icon-plusthick")){
		if(cordavailconfirmflag==0){		
			loadMoredivs('getCordAvailDetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val(),'ctcbuavailconfbarDiv','cbuCordInformationForm');
			cordavailconfirmflag=1;
		}
	}
	else if($j('#cordAvailspan').is(".ui-icon-minusthick")) {
		$j('#ctcbuavailconfbarDiv').show();
	}
	getprogressbarcolor();
}

var ctshipdetflag=0;
function getctshipmentDetails(){
	setEmptyOrders();
	if($j('#ctshipmentspan').is(".ui-icon-plusthick")){
		if(ctshipdetflag==0){
			loadMoredivs('getctshipdetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&sampleAtLab='+$j("#sampleAtLabVal").val()+'&pkcordId='+$j("#pkcordId").val(),'ctShipmentInfoContentDiv,quicklinksDiv,additityingandtestcontentDiv','cbuCordInformationForm');
			ctshipdetflag=1;
			addititypingflag=1;
		}
		var sampleType=$j("#SamTypeAvailable").val();
		getAliquotsType(sampleType);
		getDayfromshipdate($j("#ctShipDateId").val(),'ctshimentdayDiv');
	}
	else if($j('#ctshipmentspan').is(".ui-icon-minusthick")) {
		$j('#ctShipmentContentDiv').show();
	}
	
	
}

var cbushipflag=0;
function getcbushimentDetails(){
	setEmptyOrders();
	if($j('#cbushipspan').is(".ui-icon-plusthick")){
		if(cbushipflag==0){
			loadMoredivs('getctshipdetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val(),'cbuShipmentInfoContentDiv,quicklinksDiv','cbuCordInformationForm');
			cbushipflag=1;
		}
		getDayfromshipdate($j("#datepicker15").val(),'cbushimentdayDiv');
	}
	else if($j('#cbushipspan').is(".ui-icon-minusthick")) {
		$j('#cbuShipmentInfoContentDiv').show();
	}
}



function openmodaltoeditshipment(){
	if($j("#tskConfirmCbuAvailcompl").val()=="1"){
		setEmptyOrders();
		//showModal('<s:text name="garuda.pf.modalwindowheader.ctshipinfo"/><s:property value="cdrCbuPojo.registryId" />','getModalctshipdetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val(),'500','700');
		fn_CRIGetShowModals('<s:text name="garuda.pf.modalwindowheader.ctshipinfo"/><s:property value="cdrCbuPojo.registryId" />','getModalctshipdetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val()+'&moduleEntityId='+$j("#orderId").val()+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_CT_SHIPMENT" />','500','900','ctshipmodaldiv');
	}else{
		alert('<s:text name="garuda.common.validation.cordavailconfirm"/>');
	}
}


function openModalForCbuShipment(){
	if($j("#tskConfirmCbuAvailcompl").val()=="1"){
		setEmptyOrders();
		fn_CRIGetShowModals('<s:text name="garuda.pf.modalwindowheader.cbushipinfo"/><s:property value="cdrCbuPojo.registryId" />','getcbushipdetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val()+'&moduleEntityId='+$j("#orderId").val()+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_CBU_SHIPMENT" />','500','900','cbushipmodaldiv');
	}else{
		alert('<s:text name="garuda.common.validation.cordavailconfirm"/>');
	}
}

function openmodaltoeditnmdpshipment(){
	if($j("#tskConfirmCbuAvailcompl").val()=="1"){
		setEmptyOrders();
		fn_CRIGetShowModals('<s:text name="garuda.pf.modalwindowheader.nmdpshipinfo"/><s:property value="cdrCbuPojo.registryId" />','getnmdpshipdetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val()+'&moduleEntityId='+$j("#orderId").val()+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_NMDP_SHIPMENT" />','500','900','nmdpshipmodaldiv');
	}else{
		alert('<s:text name="garuda.common.validation.cordavailconfirm"/>');
	}
}

function openmodaltoacknowledgeResol(){
			setEmptyOrders();
			fn_CRIGetShowModals('<s:text name="garuda.pendingorderpage.label.ackresolution"/><s:property value="cdrCbuPojo.registryId" />','getOrderResoldetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val()+'&moduleEntityId='+$j("#orderId").val()+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_RESOLUTION" />','500','700','resolmodaldiv');
}
var resolutionFlag=0;
function getResolutionDetails(divId){
	setEmptyOrders();
	if($j('#ctResoultionspan').is(".ui-icon-plusthick")){
		if(resolutionFlag==0){
			loadMoredivs('getResolutionDetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val(),divId,'cbuCordInformationForm');
			resolutionFlag=1;
		}
	}
	else if($j('#ctResoultionspan').is(".ui-icon-minusthick")) {
		$j('#ctresolsubDiv').show();
	}
	
}


var ortcdetflag=0;
function getcbushipmentDetails(){
	setEmptyOrders();
	if($j('#ortcorderdetailsspan').is(".ui-icon-plusthick")){
		if(ortcdetflag==0){
			loadMoredivs('getctshipdetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val(),'ortcorderdetailscontent','cbuCordInformationForm');
			ortcdetflag=1;
		}
	}
	else if($j('#ortcorderdetailsspan').is(".ui-icon-minusthick")) {
		$j('#ortcorderdetailscontent').show();
	}
	
	
}

var nmdpshipflag=0;

function getnmdpshipmentdetails(){
	setEmptyOrders();
	if($j('#nmdpshipspan').is(".ui-icon-plusthick")){
		if(nmdpshipflag==0){
			loadMoredivs('getnmdpshipmentdetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val(),'nmdpResearchSampleContentDiv','cbuCordInformationForm');
			nmdpshipflag=1;
		}
	}
	else if($j('#nmdpshipspan').is(".ui-icon-minusthick")) {
		$j('#nmdpResearchSampleContentDiv').show();
	}
	disableExcuseFlag();
	disableShipmentDet();
}


function printShipmentPocket(){
	setEmptyOrders();
	$j("#genpackageslipbar").addClass('progressbarselected');
	window.open('productInsert?cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&repId=172&repName=Infectious Disease Markers Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:IDM','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
}
function getCdrClinicalData(){
	//var url = "loadClinicalDataFromPF?cdrCbuPojo.cordID="+'<s:property value="cdrCbuPojo.cordID" />';
	//window.location=url;
	submitpost('loadClinicalDataFromPF',{'cdrCbuPojo.cordID':'<s:property value="cdrCbuPojo.cordID" />','moduleEntityId':$j("#orderId").val(),'moduleEntityType':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER"/>','moduleEntityIdentifier':'<s:property value="cdrCbuPojo.registryId" />','moduleEntityEventCode':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_CBU" />'});
	//var url = 'loadClinicalDataFromPF?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID"/>';
	//loadPageByGetRequset(url,'main');
	
}


function togglePFDivs(div1,div2,methodName,divId){
	if(methodName!=null && methodName!='' && methodName!='undefined'){
		if(methodName=='getResolutionDetails'){
			getResolutionDetails(divId);
			}else{
			window[methodName]();
			}
	}
	if($j('#'+div1).hasClass('ui-icon-plusthick')){
		$j('#'+div1).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
		$j('#'+div2).show();
	}
}


function openDivbyDefault(){
	var divId='<s:property value="orderPageDivId"/>';
	if(divId!=null && divId!=''){
		if(divId=='cbuavailconfbarDiv'){
			togglePFDivs('cordAvailspan','ctcbuavailconfbarDiv','getCordAvailDetails');
		}else if(divId=='reqclincinfobarDiv'){
			togglePFDivs('reqclinspanid','ctReqClinInfoContentDiv','');
		}else if(divId=='additityingandtestDiv'){
			togglePFDivs('additiTypingspan','additityingandtestcontentDiv','getAdditiTypingDet');
		}else if(divId=='ctshipmentbarDiv'){
			togglePFDivs('ctshipmentspan','ctShipmentInfoContentDiv','getctshipmentDetails');
		}else if(divId=='ctresolutionbarDiv'){
			togglePFDivs('ctResoultionspan','ctresolsubDiv','getResolutionDetails','ctresolsubDiv');
		}else if(divId=='heresolutionbarDiv'){
			togglePFDivs('ctResoultionspan','heresolsubDiv','getResolutionDetails','heresolsubDiv');
		}else if(divId=='orresolutionbarDiv'){
			togglePFDivs('ctResoultionspan','orresolsubDiv','getResolutionDetails','orresolsubDiv');
		}else if(divId=='finalcbureviewbarDiv'){
			togglePFDivs('finalreviewspanid','finalreviewcontentDiv','');
		}else if(divId=='cbushipmentbarDiv'){
			togglePFDivs('cbushipspan','cbuShipmentInfoContentDiv','getcbushimentDetails');
		}else if(divId=='nmdpresearchSampleDiv'){
			togglePFDivs('nmdpshipspan','nmdpResearchSampleContentDiv','getnmdpshipmentdetails');
		}else if(divId=='cttcorderdetcontentdiv'){
			togglePFDivs('cttcorderdetspan','cttcorderdetcontentdiv','getcttcorderdetails');
		}
		
	}
}
function completeReqInf(cordId){
	var divIdd="completeTd";
	var cbuRegID='<s:property value="cdrCbuPojo.registryId"/>';
	var divTitle='Complete Required Information';
	setEmptyOrders();
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	showModals('<div id="notesPopup"><div style="float:left"><s:text name="garuda.completeReqInfo.label.modal.title"/>:'+$j("#registryId").val()+patient_data+'</div><div style="float:right"><span class="ui-icon ui-icon-print" onclick="printQuicklinksWidget(\''+divIdd+'\',\''+cbuRegID+'\',\''+divTitle+'\');"></span></div></div>','completeReqInfo?esignFlag=review&cdrCbuPojo.cordID='+cordId+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_CRI" />','570','1100','dialogForCRI');
	$j('#ui-dialog-title-dialogForCRI').width('98%');
}
</script>
<s:hidden name="history_search_txt" id="history_search_txt"/>
<s:hidden name="history_sort_col" id="history_sort_col"/>
<s:hidden name="history_sort_typ" id="history_sort_typ"/>
<s:hidden name="history_tbl_se" id="history_tbl_se"/>
<s:set name="tempVal" value="cbuFinalReviewPojo.finalReviewConfirmStatus"></s:set>
<s:form id ="sampleform" name="sampleform" action="getMainCBUDetails" method="post">
<s:hidden name="cbuid" id="cbuid"/>
<s:hidden name="backToSearchRes" id="backToSearchRes"></s:hidden>
</s:form>
<s:form id="cbuCordInformationForm">
<div id="cbuCordInfoMainDiv">

	
	<s:hidden name="checkflag" id="flag"></s:hidden>
	<s:hidden name="resolFlag" id="rflag"></s:hidden>
	<s:hidden name="pkcbuTempUnavail" id="pkTempUnavailable"></s:hidden>
	<s:hidden name="pkCbuAvail" id="pkCbuAvail"></s:hidden>
	<s:hidden name="cdrCbuPojo.cordID" id="pkcordId"></s:hidden>
	<s:hidden name="cdrCbuPojo.numberOnCbuBag" id="numberOnCbuBag"></s:hidden>
	<s:hidden name="cdrCbuPojo.fkSpecimenId" id="fkSpecimenId"></s:hidden>
	<s:hidden name="orderId" id="orderId"></s:hidden>
	<s:hidden name="orderType" id="orderType"></s:hidden>
	<s:hidden name="sampleAtLab" id="sampleAtLab"></s:hidden>
    <s:hidden name="cdrCbuPojo.registryId" id="registryId"></s:hidden> 
	<s:hidden name="clinicalNotePojo.pkNotes" />
	<s:hidden name="cbustatustype" id="cbustatustype"/>
	<s:hidden name="pkaliquots" id="pkaliquots"></s:hidden>
	<s:hidden name="resolutionCodeval" id="resolutionCode1"/>
	<s:hidden name="pktempunvail1" id="pktempunvail1"/>
	<s:hidden name="cdate" id="cdate"></s:hidden>
	<s:hidden name="pkNew"></s:hidden>
	<s:hidden name="pkPending"></s:hidden>
	<s:hidden name="pkShipmentSch"></s:hidden>
	<s:hidden name="pkAwaitHlaResult"></s:hidden>
	<s:hidden name="pkHlaResRec"></s:hidden>
	<s:hidden name="pkResolved" id="pkresolveid"></s:hidden>
	<s:hidden name="pkCanceled" id="pkCanceled"></s:hidden>
	<s:hidden name="pkCbuStatus_AC" id="pkCbuStatus_AC"></s:hidden>
	<s:hidden name="pkCbuStatus_CS" id="pkCbuStatus_CS"></s:hidden>
	<s:hidden name="%{cdrCbuPojo.cordID}" id="inputs" ></s:hidden>
	<s:hidden name="orderPojo.orderStatus" id="orderstatus"></s:hidden>
	<s:hidden name="isCBBMem" id="isCBBMem" value="%{cdrCbuPojo.site.isCBBMember()}"></s:hidden>
	<s:hidden name="cdrCbuPojo.unLicesePkid" id="unLicensedPk"></s:hidden>
	<s:hidden name="cdrCbuPojo.cbuLicStatus" id="licenseid"></s:hidden>
	<s:hidden name="orderPojo.orderCancellationAcceptFlag" id="cancellationFlag"></s:hidden>
	<s:hidden name="orderPojo.iscordavailfornmdp" id="iscordavailfornmdp"></s:hidden>
	<s:hidden name="minimumCriteriaComleteFlag" id="minimumCriteriaComleteFlagId"></s:hidden>
	<s:hidden name="orderPojo.resolByTc" id="resolbyTcval"></s:hidden>
	<s:hidden name="orderPojo.resolByCbb" id="resolbyCBBval"></s:hidden>
	<s:hidden name="orderPojo.ack_order" id="isAcknowledged"></s:hidden>
	<%-- <s:hidden name="cdrCbuPojo.registryId" id="cburegistryid"></s:hidden> --%>
	<s:hidden name="orderlockflag" id="lockflag"></s:hidden>
	<s:hidden name="indSponsorNoPk" id="indSponsorNoPk"></s:hidden>
	<s:hidden name="orderPojo.sampleAtLab" id="sampleAtLabVal"></s:hidden>
	
<div id="shipperUrlDiv">
	<s:hidden name="shipperUrl" id="shipperUrl"></s:hidden>
	<s:hidden name="pkworldcourier" id="pkworldcourier"/>
</div>

<div id="sampleInventoryDiv">
	
	<s:if test="codeLstCustColLst!=null && codeLstCustColLst.size()>0">
	<s:iterator value="codeLstCustColLst" var="rowVal">
		<s:hidden name="tableName" id="tableName"></s:hidden>
		<s:hidden name="columnName" id="columnName"></s:hidden>
	</s:iterator>
	</s:if>
	<s:hidden name="sampleCount" id="sampleCount"></s:hidden>
</div>
<s:if test="hasViewPermission(#request.staicPanl)==true">	
<div class="viewcbutoppanel" id="viewcbutoppanel">
	<!-- <table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td>-->
				<s:hidden name="cdrCbuPojo.cbuLicStatus" id="licstus"></s:hidden>
				<s:hidden name="cdrCbuPojo.fkCordCbuUnlicenseReason" id="unlic"></s:hidden>
				<s:hidden name="cdrCbuPojo.defferedPkCode" id="defferId"></s:hidden>
				<s:hidden name="cdrCbuPojo.clinicalStatus" id="clStatus"></s:hidden>
				<s:hidden name="cdrCbuPojo.cdrCbuId" id="cdrCbuId"></s:hidden>
				<s:hidden name="cdrCbuPojo.specimen.specCollDate"></s:hidden>
				<s:hidden name="cdrCbuPojo.site.siteId" id="cbbId"></s:hidden>
				<s:hidden name="clinicalNotePojo.pkNotes" />
				<s:hidden name="clinicalNotePojo.reply" />
				<s:hidden name="clinicalNotePojo.noteSeq"/>
				<s:hidden name="clinicalNotePojo.fkNotesType"/>
			<div class="colheading">
			
			<table>
				<tr>
					<%--<td align="left">
						<s:if test="orderId==null || orderId==''">
						<span><s:text name="garuda.cdrcbuview.label.detailresult.cbu_information" /><s:property value="cdrCbuPojo.registryId"/></span>
						</s:if>
					</td> --%>
					<s:if test="backToSearchRes!=null && backToSearchRes!='' && orderId!=null && orderId!=''">
					<td align="right">
						<input type="button" onclick="getsearchResultsBack();"
						value='<s:text name="garuda.cdrcbuview.label.button.bactosearchres" />'/>
					</td>
					</s:if>
				</tr>
			</table>
			</div>
			<div class="columnDiv" style="width: 40%">
			<s:if test="hasViewPermission(#request.updateCBUInfoS)==true">  
			<div class="outterDiv">
			<div class="colheading">
				<span><s:text name="garuda.cdrcbuview.label.cbu_information" /></span>
			</div>
			<div class="innerDiv">
			<table cellpadding="0" cellspacing="0" border="0" align="center">
				<tr>
					<td align="left" width="31%"><s:text name="garuda.cdrcbuview.label.registry_id"/>:
					<!-- <s:if test="#request.registryIdOnBag==true">
						   <span style="color: red;">(<s:text name="garuda.cdrcbuview.label.id_on_bag"/>)</span>
						</s:if>-->
						</td>
					<td align="left" width="26%">
					<s:if test="orderId!=null && orderId!=''"><a href="#" onclick="getCdrClinicalData();"><s:property value="cdrCbuPojo.registryId" /></a></s:if>
					<s:else><s:property value="cdrCbuPojo.registryId" /></s:else>
					</td>
					<td>&nbsp</td>
					<td align="left" width="20%"><s:text
					name="garuda.cdrcbuview.label.collection_date" />:</td>
					<td align="left" width="25%">
					<s:if test="%{cdrCbuPojo.specimen.specCollDate != null }">
						<s:text name="collection.date">
							<s:param name="value" value="cdrCbuPojo.specimen.specCollDate" />
						</s:text>
					</s:if>
					</td>
				</tr>
				<tr>
					<%-- <td align="left"><s:text
						name="garuda.cdrcbuview.label.isbt_din" />:
					   <s:if test="#request.isbtDinOnBag==true">
						   <span style="color: red;">(ID On Bag)</span>
					   </s:if>		
					</td>
					<td align="left"><s:property
						value="cdrCbuPojo.cordIsbiDinCode" /></td>
					<td>&nbsp</td> --%>
					<s:if test="%{cdrCbuPojo.specimen.specCollDate == null }">
					<td align="left"><s:text name="garuda.cbuentry.label.birthdate" />:</td>
					<td align="left">
					<s:date name="CdrCbuPojo.birthDate" id="datepickerbbb"
						format="MMM dd, yyyy" /> <s:property value="%{datepickerbbb}" />
					</td>
					<td>&nbsp</td>
					</s:if>
					<td align="left"><s:text
						name="garuda.cbbdefaults.lable.cbb"></s:text>:</td>
					<td align="left"><a href="#" onclick="showCbbDetails('<s:property value='cdrCbuPojo.site.siteId' />','<s:property value="cdrCbuPojo.site.siteIdentifier"/>');"  onmouseover="return overlib('<strong><s:property value="cdrCbuPojo.site.siteName"  escapeJavaScript="true" /></strong>',CAPTION,'CBB Name');" onmouseout="return nd();"><s:property
						value="cdrCbuPojo.site.siteIdentifier"/></a></td>
				</tr>
				<tr>
					
					<!-- <td><s:text name="garuda.cbuentry.label.localcbuid"/>:
				     </td>
				  	<td><s:property value="cdrCbuPojo.localCbuId"/>
				  	</td> -->
				  	<td width="31%"><s:text name="garuda.cdrcbuview.label.id_on_bag"/>:</td>
				  	<td width="26%"><s:property value="cdrCbuPojo.numberOnCbuBag"/> <s:hidden id="idonBagHidden" value="%{cdrCbuPojo.numberOnCbuBag}"/></td>
				  	<td>&nbsp;</td>
				  	<td><s:text name="garuda.cbuentry.label.tncfrozenpostproc"/>:</td>
				  	<td>
				  	<s:if test="tncFrozenVal!=null && tncFrozenVal!=''">
				  	   <s:property value="tncFrozenVal"/>&nbsp;<s:text name="garuda.minimumcriteria.tncunit"></s:text>
				  	</s:if>
				  
				  	<%-- <s:if test="#request.tncFrozenVal==null && #request.tncFrozenVal=='' && #request.tncFrozenValNxt!=null && #request.tncFrozenValNxt!='' ">
				  	 <s:property value="#request.tncFrozenValNxt"/>/<s:property value="cdrCbuPojo.frozenPatWt"/>
				  	</s:if> --%>
				  	</td>
				  
				  	<td align="left"><!--<s:text
						name="garuda.cdrcbuview.label.isbt_product_code"></s:text>:--></td>
					<td align="left"><!--<s:property
						value="cdrCbuPojo.cordIsitProductCode" />--></td>
					<td>&nbsp</td> 
					 <td align="left"><!--<s:text
						name="garuda.cdrcbuview.label.tnc_frozen"></s:text>:</td>
					<td align="left"><s:property value="cdrCbuPojo.tncFrozen" />--></td>
				</tr>
				<tr>
				   <td width="31%"><s:text name="garuda.cbuentry.label.localcbuid"/>:
				     </td>
				  	<td width="26%"><s:property value="cdrCbuPojo.localCbuId"/>
				  	</td>
					<td align="left" colspan="6"></td>
				</tr>
			</table>
			</div>
			</div>
			</s:if>
			<div id="indshowdiv">
			<s:if test="patientInfoLst!=null && patientInfoLst.size()>0">
			<s:iterator value="patientInfoLst" var="rowVal">
			<s:if test="%{#rowVal[5]!=null}">
					<s:hidden name="patientId" id="patientId" value="%{#rowVal[5]}"></s:hidden>
				</s:if>
			<s:if test="%{(#rowVal[9]!=null && #rowVal[9]!='') || (#rowVal[8]!=null && #rowVal[8]!='')}">
			<div class="outterDiv">			
			<div class="colheading">
				<span><s:text name="garuda.cbuentry.label.ind" /></span>
			</div>
			<div class="innerDiv">
			<table>
				<tr>
					<td>
						<s:text name="garuda.cbuentry.label.ind_number" />:<s:property
							value="%{#rowVal[9]}" />
						
					</td>
					<!-- <td rowspan="2" valign="middle">
					<s:if test="hasEditPermission(#request.updateINDstat)==true">
					<a class="actionbutton" href="#" name="eligibleEdit" id="eligibleEdit" 
					onclick="return false;showModal('Update IND','updateINDstatus?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','370','450');">
					<span><s:text name="garuda.common.lable.edit"></s:text></span>
					</a>
					<input type="button" 
						value="" 
						onclick="">
						</input>
					</s:if>
					</td>
				</tr>
				<tr>-->
					<td>
						<s:text name="garuda.cbuentry.label.ind_sponsor" />:<s:property
							value="%{#rowVal[8]}" />
					</td>
				</tr>
			</table>
			</div>
			</div>
			</s:if>
			</s:iterator>
			</s:if>
			</div>
			</div>
			
			<div class="columnDiv" style="width: 20%" id="eligiLiceDiv">
			<s:if test="hasViewPermission(#request.updateEligStat)==true">	
			
			<div id="eligibilitydiv" class="outterDiv">
			
			<div class="colheading">
				<span><s:text name="garuda.cdrcbuview.label.eligibility_determination" /></span>
			</div>
				<div id="eligibility" class="innerDiv">
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
					  	<s:if test="CdrCbuPojo.fkCordCbuEligible!=null && CdrCbuPojo.fkCordCbuEligible!=''">
							<s:if
								test="CdrCbuPojo.fkCordCbuEligible==CdrCbuPojo.fkCordCbuEligibleReason">
								<td>
								<s:property value="CdrCbuPojo.eligibilitydes" /></td>
							</s:if>
							<s:else>
								<td onmouseover="return overlib('<strong><s:iterator value="getMultipleIneligibleReasons(CdrCbuPojo.fkCordCbuEligibleReason,null,0)" var="colVal" status="val"><s:property value="getCodeListDescById(#colVal)" /><br></s:iterator></strong>',CAPTION,'Reason For <s:property value="CdrCbuPojo.eligibilitydes" />');" onmouseout="return nd();">
								<s:property value="CdrCbuPojo.eligibilitydes"  />
								 </td>
							</s:else>
						</s:if>
						<s:if test="CdrCbuPojo.fkCordCbuEligible==null || CdrCbuPojo.fkCordCbuEligible==''">
						      <td>
						          <s:text name="garuda.common.lable.notyetdetermined" />
						      </td>						
						</s:if>
					<!-- </tr>
					<tr>-->
						<td align="right">
						
						<s:if test='cbuFinalReviewPojo.eligibleConfirmFlag == 1 && cbuFinalReviewPojo.licensureConfirmFlag == 1 && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y"'>
							<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
	    					</s:if>
	    					<s:else>
		    					<s:if test="hasEditPermission(#request.updateEligStat)==true">
										<a href="#" name="licenseEdit"  
											onclick="CRI_GetShowModal('<s:text name="garuda.cbuentry.label.viewFinalEligible.header"/>:<s:property value="cdrCbuPojo.registryId"/>','viewFinalEligibility?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_FINAL_ELIGIBILITY" />&licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />','400','800');">
											<span><s:text name="garuda.cbuentry.label.viewFinalEligible"/></span>
										</a>
										<!-- <input type="button" name="licenseEdit" value="<s:text name="garuda.cbuentry.label.viewFinalEligible"/>"
										onclick="showModal('<s:text name="garuda.cbuentry.label.viewFinalEligible"/>','viewFinalEligibility?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal&orderId=<s:property value="orderId"/>','400','500');">
										-->	
								</s:if>	
							</s:else>				
						</s:if>
							<s:else>
								<s:if test="hasEditPermission(#request.updateEligStat)==true">
								 	<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
								    </s:if>
								    <s:else>
									<a class="actionbutton" href="#" name="eligibleEdit"  
										onclick="CRI_GetShowModal('<s:text name="garuda.cdrcbuview.label.eligibility_determination"/>'+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','updateEligiblestatus?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_ELIGIBILITY" />&licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />','390','800');">
										<span><s:text name="garuda.common.lable.edit"/></span>
									</a>
									</s:else>
								<!-- <input type="button" name="eligibleEdit"
									value="Edit"
									onclick="showModal('Eligibility Determination','updateEligiblestatus?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal','390','786');">
								-->
								</s:if>
							</s:else>
						</td>
					</tr>
				</table>
				</div>
			</div>
				
			</s:if>
			<s:if test="hasViewPermission(#request.updateLicStat)==true">
			<div id="licensure_status" class="outterDiv">
			<div class="colheading">
				<span><s:text name="garuda.cdrcbuview.label.licensure_status" /></span>
			</div>
			<div id="licence" class="innerDiv">
			<table>
				<tr>
				<s:if
							test="cdrCbuPojo.cbuLicStatus==cdrCbuPojo.fkCordCbuUnlicenseReason ||cdrCbuPojo.fkCordCbuUnlicenseReason==null || cdrCbuPojo.cbuLicStatus==null || cdrCbuPojo.cbuLicStatus<=0 || cdrCbuPojo.cbuLicStatus==''">
							<td>
							<s:property value="cdrCbuPojo.license" /></td>
							
				</s:if>
				<s:else>
				<s:if test="%{isTasksRequired(cdrCbuPojo.cordID)==false}"> 
					<td onmouseover="return overlib('<strong><s:iterator value="getMultipleIneligibleReasons(CdrCbuPojo.fkCordCbuUnlicenseReason,null,0)" var="colVal" status="val"><s:property value="getCodeListDescById(#colVal)" /><br></s:iterator></strong>',CAPTION,'Reason For <s:property value="cdrCbuPojo.license" />');" onmouseout="return nd();">
								<s:property value="cdrCbuPojo.license" />
					</td>
					</s:if>
					<s:else>
					<td>
								<s:property value="cdrCbuPojo.license" />
					</td>
					</s:else>
				</s:else>		
					<td>
					<s:if test='cbuFinalReviewPojo.eligibleConfirmFlag == 1 && cbuFinalReviewPojo.licensureConfirmFlag == 1 && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y"'>
						 <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
					    </s:if>
					    <s:else>
					    <s:if test="hasEditPermission(#request.updateLicStat)==true">
							<a href="#" name="licenseEdit"  
								onclick="CRI_GetShowModal('<s:text name="garuda.cbuentry.label.viewFinalLic"/>:<s:property value="cdrCbuPojo.registryId"/>','viewFinalLicense?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_FINAL_LICENSE" />&licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />','400','700');">
									<span><s:text name="garuda.cbuentry.label.viewFinalLicensure"/></span>
							</a>
						<!-- <input type="button" name="licenseEdit" value="<s:text name="garuda.cbuentry.label.viewFinalLic"/>"
							onclick="showModal('<s:text name="garuda.cbuentry.label.viewFinalLic"/>','viewFinalLicense?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal&orderId=<s:property value="orderId"/>','400','500');">					
						-->
						</s:if>
						</s:else>
					</s:if>
					<s:else>
					<s:if test="hasEditPermission(#request.updateLicStat)==true">
													 <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
												    </s:if>
												    <s:else>
														<a class="actionbutton" href="#" name="licenseEdit"
															onclick="CRI_GetShowModal('<s:text name="garuda.message.modal.licenceupdatestatus"/>'+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" /> ','updateStatusLicense?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_LICENSE" />&licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />','400','700');">
															<span><s:text name="garuda.common.lable.edit" /></span>
														</a>
													</s:else>
													<!-- <input type="button" name="licenseEdit" value="Edit"
						onclick="showModal('Update Licensure Status','updateStatusLicense?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal','400','500');">
					
					-->
					</s:if></s:else>
					</td>
				</tr>
			</table>
			</div>
			</div>
			</s:if>			
			</div>	
	
			<div style="width: 40%;float: left;" id="cbustatdiv">
			<div class="columnDiv" style="width: 60%">
			<s:if test="hasViewPermission(#request.updateCBUstatus)==true">
			<div class="outterDiv">
			<div class="colheading">
				<span><s:text name="garuda.cbuentry.label.cbu_status"/></span>
			</div>
			<div class="innerDiv">
			<table>
				<tr id="cordlocalstatusdiv">
					<td align="left" width="30%">
						<s:property value="getCbuStatusDescByPk(cdrCbuPojo.cordnmdpstatus)"/>
					</td>
					<td align="left" width="40%">
						<span><s:text name="garuda.pendingorderpage.label.reason"/>:</span><s:property value="getCbuStatusDescByPk(cdrCbuPojo.fkCordCbuStatus)"/>
						<s:if test="cdrCbuPojo.cordCbuStatuscode!=null">
							(<s:property value="cdrCbuPojo.cordCbuStatuscode"/>)
						</s:if>
						<s:hidden id="cordCbuStatus" name="cdrCbuPojo.cordCbuStatuscode"></s:hidden>
					</td>
					<s:if test="#request.iscbbsameOrg==true && (cdrCbuPojo.fkCordCbuStatus!=getCbuStatusPkByCode('DD') || hasEditPermission(#request.overRideDeferred)==true) && cdrCbuPojo.fkCordCbuStatus!=getCbuStatusPkByCode('SH') && cdrCbuPojo.fkCordCbuStatus!=getCbuStatusPkByCode('IN')">
						<s:if test="hasEditPermission(#request.updateCBUstatus)==true && cdrCbuPojo.site.isCDRUser()==true">
							<td align="right" width="30%">
								<a class="actionbutton" href="#" name="eligibleEdit"  id="cbustausEdit"
									onclick="showcbustatus(<s:property value="cdrCbuPojo.cordID" />,'','<s:property value="cdrCbuPojo.registryId"/>','false')">
									<span><s:text name="garuda.common.lable.edit"/></span>
								</a>
							</td>
						</s:if>
					</s:if>
				</tr>
			</table>
			</div>
			</div>
			</s:if>
			<s:if test='cdrCbuPojo.cordnmdpstatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@AC) || cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)'>
			<s:if test="patientInfoLst!=null && patientInfoLst.size()>0">
			<div class="outterDiv">
			<div class="colheading">
				<span><s:text name="garuda.cdrcbuview.label.patient_information" /></span>
			</div>
			<div class="innerDiv">
			
				<s:iterator value="patientInfoLst" var="rowVal">
				<s:if test='%{#rowVal[0]!=null && getCodeListDescById(orderPojo.orderType)=="OR" && orderPojo.resolByTc==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN,@com.velos.ordercomponent.util.VelosGarudaConstants@RESOLUTION)}'>
					<strong><s:text name="garuda.cbu.label.infused"></s:text>:<s:if test="%{#rowVal[5]!=null}"><s:property value="%{#rowVal[5]}"/></s:if></strong>
				</s:if>
				<s:elseif test='%{#rowVal[7]!=null && getCodeListDescById(orderPojo.orderType)=="OR"}'>
					<strong><s:text name="garuda.cbu.label.cordShipped"></s:text>:<s:if test="%{#rowVal[5]!=null}"><s:property value="%{#rowVal[5]}"/></s:if></strong>
				</s:elseif>
				<s:else>
					<s:if test="cdrCbuPojo.cordnmdpstatus==pkCbuStatus_AC">
						<strong><s:text name="garuda.cbu.label.patientActive"></s:text>:<s:if test="%{#rowVal[5]!=null}"><s:property value="%{#rowVal[5]}"/></s:if></strong>
					</s:if>
				</s:else>
				
				<table cellpadding="0" cellspacing="0" border="0" align="center">
				<tr>
					<td><s:text name="garuda.cdrcbuview.label.patient_disease" />:</td>
					<td align="left">
						<s:if test="%{#rowVal[2]!=null}"><s:property value="%{#rowVal[2]}"/></s:if>
					</td>
				</tr>
				<tr>
					<td><s:text name="garuda.cdrcbuview.label.patient_disease_status" />:</td>
					<td align="left">
						<s:if test="%{#rowVal[3]!=null}"><s:property value="%{#rowVal[3]}"/></s:if>
					</td>
				</tr>
				<tr>
					<td><s:text name="garuda.cbuentry.label.tncfrozenpatwt" />:</td>
					<td align="left"><s:if test='%{getCodeListDescById(orderPojo.orderType)=="OR"}'><s:if test="%{#rowVal[4]!=null && #rowVal[4]!=0 && testCalVal1!=null}"><s:property value="%{testCalVal1}"/>&nbsp;<s:text name="garuda.minimumcriteria.tncunit"></s:text>&nbsp;/&nbsp;<s:text name="garuda.common.unit.kg"></s:text></s:if></s:if></td>
				</tr>
				<s:if test='%{#rowVal[7]!=null && getCodeListDescById(orderPojo.orderType)=="OR"}'>
					<tr>
					<td><s:text	name="garuda.minimumcriteria.label.shippedon" />:</td>
					<td align="left"><s:if test="%{#rowVal[6]!=null}"><s:property value="%{#rowVal[6]}"/></s:if></td>
				</tr>
				</s:if>
				<s:if test='%{#rowVal[0]!=null && getCodeListDescById(orderPojo.orderType)=="OR"}'>
				<s:if test="cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)">
					<tr>
					<td><s:text	name="garuda.minimumcriteria.label.reportinfu" />:</td>
					<td align="left"><s:if test="%{#rowVal[1]!=null}"><s:property value="%{#rowVal[1]}"/></s:if></td>
				</tr>
				</s:if>
				</s:if>
				
			</table>
			</s:iterator>
			</div>
			</div>
			</s:if>
			</s:if>
			</div>
			<div class="columnDiv" style="width: 35%" id="reqInfoFinalReviewDiv">
			<s:hidden name="cbuCompleteReqInfoPojo.completeReqinfoFlag" id="completeReqInfoFlag"></s:hidden>
			<div class="outterDiv">
			<div class="colheading">
				<span>&nbsp;</span>
			</div>
			<s:if test="hasViewPermission(#request.cmpltReqInfo)==true">
				<div class="innerDiv" id="CRIDiv">
			<table>
			<tr>
						<s:if test="cbuCompleteReqInfoPojo.completeReqinfoFlag==1">
						<td><img src="images/tick_mark.gif" /></td>
						<td><a href="#" onclick="completeReqInf('<s:property value="cdrCbuPojo.cordID" />')" style="text-decoration: none;">
							<s:text name="garuda.completeReqInfo.label.widgetname" /></a></td>
						</s:if>
						
						<s:if test="cbuCompleteReqInfoPojo == null || cbuCompleteReqInfoPojo.completeReqinfoFlag==null ||  cbuCompleteReqInfoPojo.completeReqinfoFlag==0">
						<td><img src="images/exclamation.jpg" width="20" height="16"/></td>
						<td><a href="#" onclick="completeReqInf('<s:property value="cdrCbuPojo.cordID" />')">
							<s:text name="garuda.completeReqInfo.label.widgetname" /></a></td>
						</s:if>
					
			</tr>
			</table>
			</div>
			<div class="innerDiv">
			<table style="display: none;" id="CRICompleted">
			<tr>
						<td><img src="images/tick_mark.gif" /></td>
						<td><a href="#" onclick="completeReqInf('<s:property value="cdrCbuPojo.cordID" />')" style="text-decoration: none;">
							<s:text name="garuda.completeReqInfo.label.widgetname" /></a></td>
			</tr>
			</table>	
			<table style="display: none;" id="CRINotCompleted">
			<tr>			
						<td><img src="images/exclamation.jpg" width="20" height="16"/></td>
						<td><a href="#" style="text-decoration: none;" onclick="completeReqInf('<s:property value="cdrCbuPojo.cordID" />')">
							<s:text name="garuda.completeReqInfo.label.widgetname" /></a></td>
			</tr>
			</table>
			</div>
			</s:if>
			</div>
			<s:if test='cbuCompleteReqInfoPojo.completeReqinfoFlag==1 && cbuFinalReviewPojo.eligibleConfirmFlag == 1 && cbuFinalReviewPojo.licensureConfirmFlag == 1 && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y"'>
			<div class="outterDiv">
			<div class="innerDiv">
			<table>
			<tr>
				
				<s:hidden name="cdrCbuPojo.reviewConfirmStatus" id="reviewConfirmStatus"/>
				<td>
				<img src="images/tick_mark.gif" />
				</td>
				<td>
				<a href="#" style="text-decoration: none;" onclick="showFinalCbuReviewInfo();"><s:text name="garuda.cbuentry.label.cbufinalReviewFinish" /></a>
				</td>
				
			</tr>
			</table>
			</div>
			</div>
			</s:if>
			<s:elseif test='%{getCodeListDescById(orderPojo.orderType)=="OR"}'>
			<div class="outterDiv">
			<div class="innerDiv">
			<table>
			<tr>
				
				<s:hidden name="cdrCbuPojo.reviewConfirmStatus" id="reviewConfirmStatus"/>
				<td>
				<img src="images/exclamation.jpg" />
				</td>
				<td>
				<a href="#" style="text-decoration: none;" onclick="showFinalCbuReviewInfo();"><s:text name="garuda.cbuentry.label.static.cbufinalReview" /></a>
				</td>
				
			</tr>
			</table>
			</div>
			</div>
			</s:elseif>
			<s:if test='cbuCompleteReqInfoPojo.completeReqinfoFlag==1 && cbuFinalReviewPojo.eligibleConfirmFlag == 1 && cbuFinalReviewPojo.licensureConfirmFlag == 1 && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y"'>
			<div class="outterDiv">
			<div class="colheading">
			<!-- 	<button type="button" onclick="alert('Functionality Not yet Provided!')">
				<s:text name="garuda.cdrcbuview.label.button.bactosearchres" /></button> -->
				<span>&nbsp;</span>
			</div>
			<s:if test='(cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.inEligiblePkid || cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId) && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y"'>
			<div class="innerDiv" id="DUMNDiv">
			<table>
			<tr>
						<s:property value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@URG_MED_NEED_CODESUBTYPE]"/>
						<s:if test="(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE].size > 0)">
						<s:set name="inFlag" value="0" scope="request"/>
						<s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]" var="rowVal">
						<s:set name="codeLstSubTyp" value="#rowVal[12]" scope="request"/>
						<s:set name="docId" value="%{#rowVal[5]}" scope="request"/>
						<s:set name="codeLstPkUrgMN" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@URG_MED_NEED_CODESUBTYPE)" scope="request"/>
						<s:if test="#request.inFlag==0">
						<s:if test="#request.codeLstSubTyp!=null && #request.codeLstSubTyp==#request.codeLstPkUrgMN">
						<td><img src="images/tick_mark.gif" /></td>
						<td><a href="#"  onclick="window.open('getAttachmentFromDcms?docId=<s:property value="#request.docId"/>&signed=true','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');" style="text-decoration: none;">
							<s:text name="garuda.cdrcbuview.label.signeddumn" /></a></td>
							<s:set name="inFlag" value="1" scope="request"/>
						</s:if></s:if>
						</s:iterator>
						<s:if test="#request.inFlag==0">
						<td><img src="images/exclamation.jpg" width="20" height="16"/></td>
						<td><a href="#"  style="text-decoration: none;">
							<s:text name="garuda.cdrcbuview.label.dumnlbl" /></a></td>
						</s:if>
						</s:if>
						<s:else>
						<td><img src="images/exclamation.jpg" width="20" height="16"/></td>
						<td><a href="#"  style="text-decoration: none;">
							<s:text name="garuda.cdrcbuview.label.dumnlbl" /></a></td>
						</s:else>
					
			</tr>
			</table>
			</div>
			</s:if>
			
			</div>
			</s:if>
	</div>
	</div>	
	<!-- </td>
	</tr>
	</table>-->
</div>
</s:if>
<s:if test="hasViewPermission(#request.updateQuickLnkPanel)==true">
<div class="quicklinkpanel columnDiv" id="quicklinksDiv">
	<!-- <table width="100%" cellpadding="0" cellspacing="0"
		style="padding-top: 5px; padding-left: 5px;">
		<tr bordercolor="black">
			<th Style="text-align: left;" >-->
			<table style="padding-left: 2px;">
			<tr>
			<td width="5%" nowrap="nowrap">
			<s:text	name="garuda.cdrcbuview.label.quicklinks" />			
			</td>
			<td width="95%">
		      <s:if test="hasViewPermission(#request.updateUnitRepStat)==true && cdrCbuPojo.site.stausUnitReport()==true">
				<input type="button" onclick="CRI_GetShowModal('<s:text name="garuda.cbuentry.label.unitreportracbi"/> <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId"/>','viewUnitReport?cordID=<s:property value="cdrCbuPojo.cordID" />&cbuid=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />','500','700');"
				value='<s:text name="garuda.cdrcbuview.label.button.unitreport" />'/>
			   </s:if>
		   <s:if test="hasViewPermission(#request.viewNote)==true">
			<s:if test='(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)) '>
				<input type="button" onclick="notesListforPrint('fcrcomplete');"
				value='<s:text name="garuda.cdrcbuview.label.button.addclinicalnotes" />'/>
			</s:if>
			<s:else>
			<input type="button" onclick="notesListforPrint('fcrnotcomplete');"
				value='<s:text name="garuda.cdrcbuview.label.button.addclinicalnotes" />'/>
			</s:else>
            </s:if>
		            <s:if test="hasViewPermission(#request.updateUploadDoc)==true">
				<input type="button" onclick="CRI_GetShowModal('<s:text name="garuda.cdrcbuview.label.button.uploaddocument"/> <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId"/>','uploaddocument?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>','500','600');"
				value='<s:text name="garuda.cdrcbuview.label.button.uploaddocument" />'/>
		           </s:if>
			         <s:if test="hasViewPermission(#request.updateCBUReport)==true">
				<input type="button" onclick="CRI_GetShowModal('<s:text name="garuda.report.label.report"/> <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId"/>','reports?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId" />&orderType=<s:property value="orderType" />','400','650');" value="<s:text name="garuda.cdrcbuview.label.button.printcbureport"/>"></input>
			         </s:if>
			         <s:if test="hasViewPermission(#request.updateAuditTrail)==true">
				<input type="button" onclick="fn_CRIGetShowModals('<s:text name="garuda.cdrcbuview.label.button.audittrail"/> <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','revokedUnitReports?entityId=<s:property value="cdrCbuPojo.cordID" />&cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />','600','1000','auditModalDiv');"
				value="<s:text name="garuda.cdrcbuview.label.button.audittrail"/>"></input>
	                 </s:if>
			<!--<s:if test="hasEditPermission(#request.updateAuditTrail)==true">
				<input type="button" onclick="showModal('Audit','getAuditTrailByEntityId?entityId=<s:property value="cdrCbuPojo.cordID" />&viewName=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ROW_LEVEL"/>','500','700');"
				value="Audit"></input>
			</s:if>				
			--><%-- <s:if test="hasEditPermission(#request.updateCliStat)==true">	<button type="button"
				onclick="showModal('Update Clinical Status','updateClinicalStatus?licenceUpdate=False&cdrCbuPojo.clinicalStatus=<s:property value="cdrCbuPojo.clinicalStatus" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','400','500');"><s:text
				name="garuda.cdrcbuview.label.button.updateclinicalstatus" /></button></s:if> --%>
	      <s:if test="hasViewPermission(#request.updateFinalCBURev)==true">
					<s:if
						test='FCRCompletedFlag==true || (cbuCompleteReqInfoPojo.completeReqinfoFlag == 1 && orderType=="OR")'>
						<input type="button" id="finalReviewBtn"
							value="<s:text name="garuda.cdrcbuview.label.button.compfinaleligibility"/>"
							onclick="showFinalCbuReviewInfo();"></input>
					</s:if>
				</s:if> <s:if test="hasViewPermission(#request.updateMinCriteria)==true">
					<s:if test='(MCCompletedFlag==true || orderType=="OR")'>
						<input type="button" id="mincriteriaBtn"
							onclick="minimumcriteria('<s:property value="cdrCbuPojo.cordID" />');"
							value='<s:text name="garuda.minimumcriteria.label.widgetname" />' />
					</s:if>
				</s:if> <s:if
					test='(CBUAssessmentFlag==true || getCodeListDescById(orderPojo.orderType)=="OR") && cdrCbuPojo.site.usingAssessment()==true'>
					<input type="button"
						onclick="CRI_GetShowModal('<s:text name="garuda.header.label.cbuAssessment"/>:<s:property value="cdrCbuPojo.registryId"/>','cbuAssessment?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','400','600');"
						value="<s:text name="garuda.unitreport.label.eligibility.cbu_assessment"/>"></input>
				</s:if> 
		   
	        <s:if test="hasViewPermission(#request.updateFlaggedItem)==true">
			 <input type="button" onclick="flaggedItemsforPrint();"
					value="<s:text name="garuda.unitreport.label.eligibility.flaggeditems"/>"></input>
	        </s:if>
		    <s:if test="hasViewPermission(#request.updateShipmentItern)==true">
				<s:hidden name="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPMENT_ITER_CODESUBTYPE].size" id="shpItern"></s:hidden>
				<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPMENT_ITER_CODESUBTYPE].size>0">
				 	<input type="button"  onclick="CRI_GetShowModal('<s:text name="garuda.header.label.shipmentItinerary"/>:<s:property value="cdrCbuPojo.registryId"/>','getShipmentItern?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />','400','500');" 
						 value="<s:text name="garuda.unitreport.label.eligibility.shipmentiteneary"/>"></input>
				</s:if>
            </s:if>
		<%-- <s:if test="hasEditPermission(#request.updateProductInsert)==true">
			 <button type="button" onclick="javascript:showProductInsertWindow('productInsert?cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&repId=172&repName=Infectious Disease Markers Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:IDM')">
					<s:text name="garuda.productinsert.label.product_productinsert" /></button>	 
		</s:if> --%>
			<%-- <input type="button" onclick="showModal('Product Insert','productInsert?cordID=<s:property value="cdrCbuPojo.cordID" />','900','900');" <!-- <s:if test='cbuFinalReviewPojo.eligibleConfirmFlag != 1 || #request.minimumCriteriaComleteFlag != 1'>disabled="disabled"</s:if> -->
					value="<s:text name="garuda.productinsert.label.product_productinsert" />"></input> --%> 
	
		<s:if test='orderPojo.resolByTc!=pkCanceled'>
		<s:if test="shipmentInfoLst!=null && shipmentInfoLst.size()==1">
		<s:iterator value="shipmentInfoLst" var="rowVal">
		<s:if test='%{#rowVal[11]=="Y"}'>
			<s:if test='orderType=="CT" && orderPojo.sampleAtLab=="N"'>
			<s:if test="(orderPojo.iscordavailfornmdp!=@com.velos.ordercomponent.business.util.VelosMidConstants@FLAG_N && orderPojo.orderStatus!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)) && (orderPojo.orderStatus!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED))">
			 <input type="button" onclick="javascript:createPackageSlip()" 
			 value="<s:text name="garuda.ctShipmentInfo.button.label.createpackageslip" />"></input>
			 </s:if> 
		    </s:if>
		    </s:if>
		    </s:iterator>
		    </s:if>
		</s:if>
		<!--<input type="button" onclick="callPending()" 
			 value="<s:text name="garuda.cbu.label.pendingRequest" />"></input>
		--><!--<input type="button" onclick="showModal('Uploaded Documents','uploadedDocuments?cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />','400','800');" 
			 value="<s:text name="garuda.uploaddoc.label.uploadeddocument" />"></input>							
			-->
			<s:hidden name="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@POST_SHPMNT_CODESUBTYPE].size" id="cbuReceipt"></s:hidden>
				<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@POST_SHPMNT_CODESUBTYPE].size>0">
				 	<input type="button"  onclick="CRI_GetShowModal('<s:text name="garuda.header.label.cbureceipt"/>:<s:property value="cdrCbuPojo.registryId"/>','getCbuReceipt?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />','400','800');" 
						 value="<s:text name="garuda.cdrcbuview.label.button.cbureceipt"/>"></input>
				</s:if>
		</td>
		</tr>
	</table>
</div>
</s:if>
<div id="taskCompleteFlagsDiv">
	<s:hidden name="tskConfirmCbuAvailcompl" id="tskConfirmCbuAvailcompl"/>
	<s:hidden name="tskReqClincInfocompl" id="tskReqClincInfocompl"></s:hidden>
	<s:hidden name="tskFinalCbuReviewcompl" id="tskFinalCbuReviewcompl"></s:hidden>
	<s:hidden name="tskShipmentInfocompl" id="tskShipmentInfocompl"></s:hidden>
	<s:hidden name="tskGenPackageSlipcompl" id="tskGenPackageSlipcompl"></s:hidden>
	<s:hidden name="tskAdditiTypingcompl" id="tskAdditiTypingcompl"></s:hidden>
	<s:hidden name="tskShipmentConfirmcompl" id="tskShipmentConfirmcompl"></s:hidden>
	<s:hidden name="tskSampleShipmentcompl" id="tskSampleShipmentcompl"></s:hidden>
	<s:hidden name="tskAcknowledgeResolcompl" id="tskAcknowledgeResolcompl"></s:hidden> 
</div>
<div class="columnDiv cbuDetailsDiv">
<s:if test="orderType=='CT'">
	<div class="col_100 maincontainer ">
	<div class="col_100">
	<jsp:include page="pf_CT_OrderDetails.jsp"></jsp:include>
	</div>
	</div>
</s:if>
<s:elseif test="orderType=='HE'">
	<div class="col_100 maincontainer ">
	<div class="col_100">
	<jsp:include page="pf_HE_OrderDetails.jsp"></jsp:include>
	</div>
	</div>
</s:elseif>
<s:elseif test="orderType=='OR'">
	<div class="col_100 maincontainer ">
	<div class="col_100">
	<jsp:include page="pf_OR_OrderDetails.jsp"></jsp:include>
	</div>
	</div>
</s:elseif>
</div>
</div>
</s:form>
<div style="display: none;" id="modalEsign1">
   <table bgcolor="#cccccc" width="100%">
         <tr bgcolor="#cccccc" valign="baseline">
		   <td width="50%">
                 <table width="98%" border="0" cellspacing="0" cellpadding="0" bgcolor="#cccccc">
					<tr valign="baseline" bgcolor="#cccccc">
					    <td>
						<span id="cordIdEntryValid1" style="display: none;" class="validation-fail"><s:text name="garuda.common.esign.invalid"/></span>
						<span id="cordIdEntryMinimum1" style="display: none;" class="validation-fail"><s:text name="garuda.common.esign.minimumLength"/></span>
						<span id="cordIdEntryPass1" style="display: none;" class="validation-pass"><s:text name="garuda.common.esign.valid"/></span>
						</td>
						<td nowrap="nowrap">
						<s:text name="garuda.esignature.lable.esign" /><span style="color:red;">*</span></td>
						<td><s:password name="userPojo.signature" id="esignAutoDefer" size="10" style="font-style:bold; color:#000000;" onkeyup="validateSign(this.value,'submitcdrsearch1','cordIdEntryValid1','cordIdEntryMinimum1','cordIdEntryPass1')" cssStyle="width:100px"></s:password>
						</td>		
					</tr>
				</table>
			</td>
	           <td width="50%" align="right" valign="top">
	         <%--    <s:if test="hasEditPermission(#request.overRideDeferred)==true && (deferedCord==null || deferedCord==false)"> --%>
	              <input type="button" id="submitcdrsearch1" disabled="disabled" value="<s:text name="garuda.unitreport.label.button.submit"/>"/>            
	          <%--  </s:if> --%>
	           <input type="button" id="esign_cancel" value="<s:text name="garuda.common.lable.cancel"/>"/>					          
	       </td>
        </tr>
   </table>	
</div>
<script>
function CRI_GetShowModal(title,url,hight,width){
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title = title + patient_data;
	showModal(title,url,hight,width);
}

function fn_CRIGetShowModals(title,url,hight,width,divid){
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title = title + patient_data;
	showModals(title,url,hight,width,divid);
}

var updateStatic ={
		
		proc:function(){
			var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID='+'<s:property value="cdrCbuPojo.cordID"/>'+'&orderId='+<s:property value="orderId"/>+'&orderType='+'<s:property value="orderType"/>'+'&pkcordId='+'<s:property value="cdrCbuPojo.cordID"/>'+'&currentReqFlag=true';
  		     loadMoredivs(urlb,'viewcbutoppanel','cbuCharacteristicsMain','update','status');//url,divname,formId,updateDiv,statusDiv
		
		}
		
};
</script>