<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<jsp:include page="./cb_track_session_logging.jsp" />
<s:if test="hasViewPermission(#request.updateHLA_CBU)==true">	
	 <s:set name="tempVal" value="cbuFinalReviewPojo.finalReviewConfirmStatus"></s:set>
			<div id="cbubesthlainfocontent" onclick="toggleDiv('cbubesthlainfo')"
					class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
					style="text-align: center;"><span
					class="ui-icon ui-icon-triangle-1-s"></span> <s:text
					name="garuda.cbuentry.label.CBU"></s:text></div>
				<div id="cbubesthlainfo" style="width:100%; overflow-x: scroll; overflow-y:hidden;">
				   <table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults11">
						 <thead>
						   <tr>
						      <th></th>			   			   
							   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							       <th>
							          <s:property value="description.replace('HLA-','')"/>
							       </th>				     
							   </s:iterator>
						   </tr>
						 </thead>			 
						 <tbody>
						    <tr>
							    <td>
									 <s:text name="garuda.cbu.label.besthla"/>								            
								</td>								    
								  <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							            <s:iterator value="#request.bestHlas" var="bhla">
									             <s:if test="pkCodeId==fkHlaCodeId">
									                 <s:set name="bhlaCheck" value="%{'true'}" />
										             <td>
											            <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
										                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>	  
												     </td>	
											      </s:if>				      										      	        	
									        </s:iterator> 
									        <s:if test="%{#bhlaCheck!='true'}">
											         <td></td>
											</s:if>	
											<s:set name="bhlaCheck" value="%{'false'}" />
								  </s:iterator>
							</tr>
						   </tbody>
						   <tfoot>
							    <tr>
									<td colspan="13"></td>
								</tr>
							</tfoot>
						</table>
						<table width="100%" style="padding-top: 50px;">
						      <tr>
						         <td></td>
						      </tr>
						</table>
						<table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults10">
							 <thead>
							   <tr>
							      <th><s:text name="garuda.cbuentry.label.typingdate"/></th>	
							      <th><s:text name="garuda.cbuentry.label.recieveddate"/></th>
							      <th><s:text name="garuda.cbuentry.label.source"/></th>		   			   
								   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
								       <th>
								          <s:property value="description.replace('HLA-','')"/>
								       </th>				     
								   </s:iterator>
							   </tr>
							 </thead>			 
							 <tbody>	
							    <s:iterator value="#request.cbuHlaMap">
							        <s:set name="cbuHlaMapKey" value="key" />
							        <tr>				
							        <td>
							            <s:if test="#request.cbuHlaMap[#cbuHlaMapKey][0].hlaTypingDate !=null">
							                <s:set name="cbuhlatypedate" value="#request.cbuHlaMap[#cbuHlaMapKey][0].hlaTypingDate" />
									        <s:date name="cbuhlatypedate" id="cbuhlatypedate" format="MMM dd, yyyy" />
									        <s:property value="%{#request.cbuhlatypedate}"/>									       
							            </s:if>									        
							        </td>
							        <td>
							            <s:if test="#request.cbuHlaMap[#cbuHlaMapKey][0].hlaRecievedDate !=null">
									        <s:set name="cbuhlarecievedate" value="#request.cbuHlaMap[#cbuHlaMapKey][0].hlaRecievedDate" />
									        <s:date name="cbuhlarecievedate" id="cbuhlarecievedate" format="MMM dd, yyyy" />
									        <s:property value="%{#request.cbuhlarecievedate}"/>									        
									    </s:if>
							        </td>
							        <td>
							           <!--<s:if test="#request.cbuHlaMap[#cbuHlaMapKey][0].fkSource !=null && #request.cbuHlaMap[#cbuHlaMapKey][0].fkSource!=''">
							               <s:set name="cbuhlasource" value="#request.cbuHlaMap[#cbuHlaMapKey][0].fkSource" />
								           <s:property value="getCodeListDescById(#cbuhlasource)"/>
								       </s:if>
								       
								     -->
								     <!-- Code fix for HLA last modified user issue -->
								     
								     <s:if test="#request.cbuHlaMap[#cbuHlaMapKey][0].lastModifiedBy !=null && #request.cbuHlaMap[#cbuHlaMapKey][0].lastModifiedBy!=''">
							               <s:set name="cbuhlaModifiedBy" value="#request.cbuHlaMap[#cbuHlaMapKey][0].lastModifiedBy" />
								           <s:property value="getUserNameById(#cbuhlaModifiedBy)"/>
								       </s:if>
								       
								        <s:elseif test="#request.cbuHlaMap[#cbuHlaMapKey][0].createdBy!=null && #request.cbuHlaMap[#cbuHlaMapKey][0].createdBy!=''">
							               <s:set name="cbuhlacreator" value="#request.cbuHlaMap[#cbuHlaMapKey][0].createdBy" />
								           <s:property value="getUserNameById(#cbuhlacreator)"/>
								       </s:elseif>
								    <!--  =========================================================================  -->   
							        </td>
							        
							        <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">					       
								            <s:iterator value="#request.cbuHlaMap[#cbuHlaMapKey]" var="bhla">
									             <s:if test="pkCodeId==fkHlaCodeId">
									                 <s:set name="hlaCheck" value="%{'true'}" />
										             <td>
											            <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
										                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>	  
												     </td>	
											      </s:if>				      										      	        	
									        </s:iterator> 
									        <s:if test="%{#hlaCheck!='true'}">
											         <td></td>
											</s:if>	
											<s:set name="hlaCheck" value="%{'false'}" />	    								          
									 </s:iterator> 
									 </tr>
								 </s:iterator>
							   </tbody>
							   <tfoot>
								    <tr>
										<td colspan="13"></td>
									</tr>
								</tfoot>
			              </table>	
			              <table width="100%" cellpadding="0" cellspacing="0" border="0">
						    <tr>
						       <td align="right">
						             <s:if test="hasEditPermission(#request.updateHLA_CBU)==true && cdrCbuPojo.site.isCDRUser()==true">
						             		
						              <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
   										 </s:if>
  										  <s:else>
						       		  	 <input type="button" name="editHLA" class="button" onclick="fn_showModalHla('<s:text name="garuda.cbu.cbuhlatype.edit"/> <s:text name="garuda.message.modal.headerregid"/> <s:property value="cdrCbuPojo.registryId" />','updateHlas?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CLINICAL_RECORD_HLA_MATERNAL" />&update=False&source=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE)" />&entityId=<s:property value="cdrCbuPojo.cordID" />&entityType=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_TYPE" />&entitySubtype=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_SUB_TYPE" />','500','900');" value="<s:text name="garuda.common.lable.edit"/>"></input>
						       		  </s:else>
						             </s:if>
						       </td>
						    </tr>
						</table>
				</div>			
			     </s:if>
			     <s:if test="hasViewPermission(#request.updateHLA_Matrnl)==true">	
			    <div>		
				<div id="matbesthlainfocontent" onclick="toggleDiv('matbesthlainfo')"
					class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
					style="text-align: center;"><span
					class="ui-icon ui-icon-triangle-1-s"></span> <s:text
					name="garuda.cbuentry.label.maternal"></s:text></div>				
				<div id="matbesthlainfo" style="width:100%; overflow-x: scroll; overflow-y:hidden;">
				   <!--<table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults10">
						 <thead>
						   <tr>
						      <th></th>
						      <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							       <th>
							          <s:property value="description"/>
							       </th>				     
							   </s:iterator>
						   </tr>
						 </thead>			 
						 <tbody>	
						        <s:if test="fkSource==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_CODESUBTYPE)">
								    <tr>
								        <td>Best HLA</td>				    		         
									        <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
									             <s:if test="pkCodeId==fkHlaCodeId">
										             <td>
											            <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
										                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>	  
												     </td>	
											      </s:if>
											      <s:else>
											          <td>
											          </td>	
											      </s:else>			        	
									        </s:iterator>	
								   </tr>
								</s:if> 
						   </tbody>
						   <tfoot>
							    <tr>
									<td colspan="13"></td>
								</tr>
							</tfoot>
						</table>
						<table width="100%" style="padding-top: 50px;">
						      <tr>
						         <td></td>
						      </tr>
						</table>
						-->
						<table><tr><td></td></tr></table>
						<table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults10">
							 <thead>
							   <tr>
							      <th><s:text name="garuda.cbuentry.label.typingdate"/></th>	
							      <th><s:text name="garuda.cbuentry.label.recieveddate"/></th>
							      <th><s:text name="garuda.cbuentry.label.source"/></th>							   		   			   
								   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
								       <th>
								          <s:property value="description.replace('HLA-','')"/>
								       </th>				     
								   </s:iterator>
							   </tr>
							 </thead>			 
							 <tbody>	
							    <s:iterator value="#request.matHlaMap">
							        <s:set name="matHlaMapKey" value="key" />
							        <tr>				
							        <td>
							            <s:if test="#request.matHlaMap[#matHlaMapKey][0].hlaTypingDate !=null">
							                <s:set name="mathlatypedate" value="#request.matHlaMap[#matHlaMapKey][0].hlaTypingDate" />
									        <s:date name="mathlatypedate" id="mathlatypedate" format="MMM dd, yyyy" />
									        <s:property value="%{mathlatypedate}"/>
							            </s:if>									        
							        </td>
							        <td>
							            <s:if test="#request.matHlaMap[#matHlaMapKey][0].hlaRecievedDate !=null">
									        <s:set name="mathlarecievedate" value="#request.matHlaMap[#matHlaMapKey][0].hlaRecievedDate" />
									        <s:date name="mathlarecievedate" id="mathlarecievedate" format="MMM dd, yyyy" />
									        <s:property value="%{mathlarecievedate}"/>
									    </s:if>
							        </td>
							        <td>
							           <!--<s:if test="#request.matHlaMap[#matHlaMapKey][0].fkSource !=null && #request.matHlaMap[#matHlaMapKey][0].fkSource!=''">
							               <s:set name="mathlasource" value="#request.matHlaMap[#matHlaMapKey][0].fkSource" />
								           <s:property value="getCodeListDescById(#mathlasource)"/>
								       </s:if>
								       -->
								       
								       <s:if test="#request.matHlaMap[#matHlaMapKey][0].lastModifiedBy !=null && #request.matHlaMap[#matHlaMapKey][0].lastModifiedBy!=''">
							               <s:set name="cbumathlaModifiedBy" value="#request.matHlaMap[#matHlaMapKey][0].lastModifiedBy" />
								           <s:property value="getUserNameById(#cbumathlaModifiedBy)"/>
								       </s:if>
								       
								       <s:elseif test="#request.matHlaMap[#matHlaMapKey][0].createdBy !=null && #request.matHlaMap[#matHlaMapKey][0].createdBy!=''">
							               <s:set name="mathlacreator" value="#request.matHlaMap[#matHlaMapKey][0].createdBy" />
								           <s:property value="getUserNameById(#mathlacreator)"/>
								       </s:elseif>
							        </td>					        					       
								    <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
									     	<s:iterator value="#request.matHlaMap[#matHlaMapKey]" var="bhla">
									             <s:if test="pkCodeId==fkHlaCodeId">
									             <s:set name="matCheck" value="%{'true'}" />
										             <td>
										                <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
										                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>										            
												     </td>	
											      </s:if>										      		        	
									        </s:iterator>    
									        <s:if test="%{#matCheck!='true'}">
											         <td></td>
											</s:if>	
											<s:set name="matCheck" value="%{'false'}" />    
									 </s:iterator> 
									 </tr>
								 </s:iterator>
							   </tbody>
							   <tfoot>
								    <tr>
										<td colspan="13"></td>
									</tr>
								</tfoot>
						</table>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
						    <tr>
						       <td align="right">
						           <s:if test="hasEditPermission(#request.updateHLA_Matrnl)==true && cdrCbuPojo.site.isCDRUser()==true">
						           		 
									<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
						           </s:if>
						           <s:else>
						           <input type="button" name="editHLA1" class="button" onclick="fn_showModalHla('<s:text name="garuda.cbu.maternalhlatype.edit"/> <s:text name="garuda.message.modal.headerregid"/> <s:property value="cdrCbuPojo.registryId" />','updateHlas?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CLINICAL_RECORD_HLA_CBU" />&update=False&source=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_CODESUBTYPE)" />&entityId=<s:property value="cdrCbuPojo.cordID" />&entityType=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_TYPE" />&entitySubtype=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_SUB_TYPE" />','500','900');" value="<s:text name="garuda.common.lable.edit"/>"></input>
						           </s:else>
						           </s:if>
						           
						       </td>
						    </tr>
						</table>
				</div>
				</div>			
			  </s:if>
			  <s:if test='cdrCbuPojo.cordnmdpstatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@AC) || cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)'>
			  <s:if test="hasViewPermission(#request.updateHLA_Patient)==true">
			  <div>						         
				<div id="patbesthlainfocontent" onclick="toggleDiv('patbesthlainfo')"
					class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
					style="text-align: center;"><span
					class="ui-icon ui-icon-triangle-1-s"></span> <s:text
					name="garuda.cbu.label.patienthla"></s:text><s:iterator value="patientInfoLst" var="rowVal">
	<s:if test="%{#rowVal[5]!=null}"><s:property value="%{#rowVal[5]}"/></s:if></s:iterator></div>
				<div id="patbesthlainfo" style="width:100%; overflow-x: scroll; overflow-y:hidden;">
				   <table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults10">
						 <thead>
						   <tr>
						      <th></th>   			   
							   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							       <th>
							          <s:property value="description.replace('HLA-','')"/>
							       </th>				     
							   </s:iterator>
						   </tr>
						 </thead>			 
						 <tbody>
						    <tr>
							  <td><s:text name="garuda.cbu.label.besthla"/></td>
						      <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							            <s:iterator value="#request.patientBestHlas" var="bhla">
									             <s:if test="pkCodeId==fkHlaCodeId">
									                 <s:set name="phlaCheck" value="%{'true'}" />
										             <td>
											            <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
    									                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>	  
												     </td>	
											      </s:if>				      										      	        	
									        </s:iterator> 
									        <s:if test="%{#phlaCheck!='true'}">
											         <td></td>
											</s:if>	
											<s:set name="phlaCheck" value="%{'false'}" />
								  </s:iterator>
							 </tr> 
						   </tbody>
						   <tfoot>
							    <tr>
									<td colspan="13"></td>
								</tr>
							</tfoot>
						</table>
						<table width="100%" style="padding-top: 50px;">
						      <tr>
						         <td></td>
						      </tr>
						</table>
						<table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults10">
								 <thead>
								   <tr>
								      <th><s:text name="garuda.cbuentry.label.typingdate"/></th>	
							          <th><s:text name="garuda.cbuentry.label.recieveddate"/></th>
							          <th><s:text name="garuda.cbuentry.label.source"/></th>							   		   			   
									   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
									       <th>
									          <s:property value="description.replace('HLA-','')"/>
									       </th>				     
									   </s:iterator>
								   </tr>
								 </thead>			 
								 <tbody>	
								    <s:iterator value="#request.patHlaMap">
								        <s:set name="patHlaMapKey" value="key" />
								        <tr>				
								        <td>
								            <s:if test="#request.patHlaMap[#patHlaMapKey][0].hlaTypingDate !=null">
								                <s:set name="pathlatypedate" value="#request.patHlaMap[#patHlaMapKey][0].hlaTypingDate" />
										        <s:date name="pathlatypedate" id="pathlatypedate" format="MMM dd, yyyy" />
										        <s:property value="%{pathlatypedate}"/>
								            </s:if>									        
								        </td>
								        <td>
								            <s:if test="#request.patHlaMap[#patHlaMapKey][0].hlaRecievedDate !=null">
										        <s:set name="pathlarecievedate" value="#request.patHlaMap[#patHlaMapKey][0].hlaRecievedDate" />
										        <s:date name="pathlarecievedate" id="pathlarecievedate" format="MMM dd, yyyy" />
										        <s:property value="%{pathlarecievedate}"/>
										    </s:if>
								        </td>
								        <td>
								           <!--<s:if test="#request.patHlaMap[#patHlaMapKey][0].fkSource !=null && #request.patHlaMap[#patHlaMapKey][0].fkSource!=''">
								               <s:set name="pathlasource" value="#request.patHlaMap[#patHlaMap][0].fkSource" />
									           <s:property value="getCodeListDescById(#pathlasource)"/>
									       </s:if>
									       --><s:if test="#request.patHlaMap[#patHlaMapKey][0].createdBy !=null && #request.patHlaMap[#patHlaMapKey][0].createdBy!=''">
							               <s:set name="pathlacreator" value="#request.patHlaMap[#patHlaMapKey][0].createdBy" />
								           <s:property value="getUserNameById(#pathlacreator)"/>
								       </s:if>
								        </td>							        					       
									    <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
										     	<s:iterator value="#request.patHlaMap[#patHlaMapKey]" var="bhla">
										             <s:if test="pkCodeId==fkHlaCodeId">
										             <s:set name="patCheck" value="%{'true'}" />
											             <td>
												            <table>
											                    <tr>
											                        <td>
											                            <s:property value="hlaType1" />
											                        </td>
											                    </tr>
											                     <tr>
											                        <td>
											                            <s:property value="hlaType2" />
											                        </td>
											                    </tr>
											                </table>	  
													     </td>	
												      </s:if>										      		        	
										        </s:iterator>    
										        <s:if test="%{#patCheck!='true'}">
												         <td></td>
												</s:if>	
												<s:set name="patCheck" value="%{'false'}" />    
										 </s:iterator> 
										 </tr>
									 </s:iterator>
								   </tbody>
								   <tfoot>
									    <tr>
											<td colspan="13"></td>
										</tr>
									</tfoot>
					     </table>
						 <table><tr><td></td></tr></table>
				</div>	
			  </div>		
			</s:if>
			</s:if>
			<div>			
			<div id="hlanotecontent" onclick="toggleDiv('hlanote')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cdrcbuview.label.button.hlaclinicalnotes"></s:text></div>
			<div id="hlanote" style=" width: 463px; overflow: auto;">
			     <div id="loadHlaClinicalNoteDiv">
					<s:set name="cbuCateg" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CLINIC_CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_NOTE_CATEGORY_CODESUBTYPE) " />
					<jsp:include page="modal/cb_load_clinicalNote.jsp"></jsp:include>
				  </div>
			<s:if test="hasNewPermission(#request.vClnclNote)==true">
			 <s:if test='(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)) '>
				 	<button type="button" disabled="disabled"><s:text
				name="garuda.cdrcbuview.label.button.addnotes" /></button>
				 </s:if>
			<s:else>
			<button type="button"
				onclick="fn_showModalHlaForNotes('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','clinicalNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cat=HLA_NOTE_CATEGORY_CODESUBTYPE','550','850','notesModalsDiv');"><s:text
				name="garuda.cdrcbuview.label.button.addnotes" /></button>
				 </s:else>
				
				 </s:if>
			</div>	
			</div>
			<div id="loadHlaDocInfoDiv">
			<s:if test="(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_CODESUBTYPE].size>0 && hasViewPermission(#request.viewHlaCat)==true)||(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size > 0 && hasViewPermission(#request.viewUnitRepCat)==true)">
			<div id="loadHlaDocInfocontent" onclick="toggleDiv('loadHlaDocInfo')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;">
				<span class="ui-icon ui-icon-triangle-1-s"></span> 
				<s:text name="garuda.uploaddoc.label.button.hlauploaddocument" />
			</div>
			<div id="loadHlaDocInfo">      
			<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_CODESUBTYPE].size>0 ||#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null ||#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size>0">
			<s:if test="hasViewPermission(#request.viewHlaCat)==true">
			<s:set name="cbuDocCateg" value="@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_CODESUBTYPE" scope="request" />
			</s:if>			
						<jsp:include page="modal/cb_load_upload_document.jsp">
							<jsp:param value="<%=request.getAttribute(\"cbuDocCateg\")%>" name="cbuDocCateg"/>
						</jsp:include>						
			</s:if>
			</div>
		  </s:if>
		  </div>
		  <script>
		  function fn_showModalHla(title,url,hight,width){
			    var patientId = $j('#patientId').val();
				var patient_data = '';
				if(patientId!="" && patientId != null && patientId != "undefined"){
					patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
				}
				title=title+patient_data;
				showModal(title,url,hight,width)
				}
		  function fn_showModalHlaForNotes(title,url,hight,width,id){
			    var patientId = $j('#patientId').val();
				var patient_data = '';
				if(patientId!="" && patientId != null && patientId != "undefined"){
					patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
				}
				title=title+patient_data;
				showModals(title,url,hight,width,id);
				}
		  </script>