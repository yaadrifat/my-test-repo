<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.eres.web.site.SiteJB" %>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.address.AddressJB" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<head>
<s:head />
<script type="text/javascript">
<%! String sessUserId=null; %>
<%
	HttpSession tSession = request.getSession(true); 
	GrpRightsJB grpRights = null;
	if (tSession.getAttribute("LocalGRights")!=null){
		 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
		}else
		{
		 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
		}
	int cbuMyProRght = 0;
	if(grpRights.getFtrRightsByValue("CB_MYPRO")!=null && !grpRights.getFtrRightsByValue("CB_MYPRO").equals(""))
	 {cbuMyProRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_MYPRO"));}
	else
		cbuMyProRght = 4;	
	request.setAttribute("cbuMyProRght",cbuMyProRght);	
	sessUserId =	(String) tSession.getValue("userId");	
%>
var currentUser="<%=sessUserId%>";
function updateinfo(val){
	if(val !=null && currentUser !=null)
	{	
		if(val == currentUser)
		{
		document.getElementById("userAlertsForm").action="userdetails.jsp?mode=M&srcmenu=tdmenubaritem13&selectedTab=1&pname=ulinkBrowser";	
		}else
		{
		document.getElementById("userAlertsForm").action="userdetails.jsp?mode=M&srcmenu=tdMenuBarItem2&userId="+val+"&userType=";
		}
		document.getElementById("userAlertsForm").submit();
	}
		
}
$j(document).ready(function(){
	setTimeout(function() { $j('#hideMsg').fadeOut('fast'); }, 10000);
	$j("#resolAlertsDiv").hide();
	var chkBoxLen=$j('.email :checkbox').length;
	$j('.email input[type=checkbox]').each(function(index,el) {
		if(index==chkBoxLen-1 && $j(el).attr('checked')){
			$j("#resolAlertsDiv").show();
		}
	});
	$j('.notify input[type=checkbox]').each(function(index,el) {
		if(index==chkBoxLen-1 && $j(el).attr('checked')){
			$j("#resolAlertsDiv").show();
		}
	});
	$j('.prompt input[type=checkbox]').each(function(index,el) {
		if(index==chkBoxLen-1 && $j(el).attr('checked')){
			$j("#resolAlertsDiv").show();
		}
	});
	$j('.email input[type=checkbox]').each(function(index,el) {
		if(index==chkBoxLen-2 && $j(el).attr('checked')){
			$j("#resolAlertsDiv").show();
		}
	});
	$j('.notify input[type=checkbox]').each(function(index,el) {
		if(index==chkBoxLen-2 && $j(el).attr('checked')){
			$j("#resolAlertsDiv").show();
		}
	});
	$j('.prompt input[type=checkbox]').each(function(index,el) {
		if(index==chkBoxLen-2 && $j(el).attr('checked')){
			$j("#resolAlertsDiv").show();
		}
	});
	
});

function getChecked(checkBoxName){
	if($j('input[name='+checkBoxName+'Header]').attr('checked')){
		$j('.'+checkBoxName+' input[type=checkbox]').each(function(index,el) {
			$j(el).attr('checked',true);
		});
		$j("#"+checkBoxName+$j("#pkresolAllAlert").val()).attr('checked',true);
		getResolAlerts($j('#pkresolAllAlert').val(),'email');
		$j("#"+checkBoxName+$j("#pkresolCustAlert").val()).attr('checked',false);
	}
	else{
		$j('.'+checkBoxName+' input[type=checkbox]').each(function(index,el) {
			$j(el).attr('checked',false);
			getResolAlerts($j('#pkresolAllAlert').val(),'email');
		});
	}	
}
function getResolAlerts(alertId,checkBoxName){
	var pkresolCustAlert=$j('#pkresolCustAlert').val();
	var pkresolAllAlert=$j('#pkresolAllAlert').val();
	var id='resol'+checkBoxName+'flag';
	if(alertId==pkresolCustAlert){
		if($j("#"+checkBoxName+alertId).attr("checked")){
			$j("#resolAlertsDiv").show();
			$j("#"+checkBoxName+pkresolAllAlert).attr('disabled',true);
		}else{
			$j('#'+id+' input[type=checkbox]').each(function(index,el) {
				$j(el).attr('checked',false);
				$j(el).attr('disabled',false);
			});
			$j("#"+checkBoxName+pkresolAllAlert).attr('disabled',false);
		}
	}	
	if(alertId==pkresolAllAlert){
		if($j("#"+checkBoxName+alertId).attr("checked")){
			$j('.'+id+' input[type=checkbox]').each(function(index,el) {
				$j(el).attr('checked',true);
				$j("#"+checkBoxName+pkresolCustAlert).attr('disabled',true);
			});
			$j("#resolAlertsDiv").show();
		}else{
			$j('.'+id+' input[type=checkbox]').each(function(index,el) {
				$j(el).attr('checked',false);
				$j("#"+checkBoxName+pkresolCustAlert).attr('disabled',false);
				$j("#"+checkBoxName+pkresolAllAlert).attr('disabled',false);
			});
		}
	}
	if(!$j("#email"+pkresolCustAlert).attr("checked") && !$j("#notify"+pkresolCustAlert).attr("checked") && !$j("#prompt"+pkresolCustAlert).attr("checked") && !$j("#email"+pkresolAllAlert).attr("checked") && !$j("#notify"+pkresolAllAlert).attr("checked") && !$j("#prompt"+pkresolAllAlert).attr("checked")){
		$j("#resolAlertsDiv").hide();
		$j('#resolAlertsDiv input[type=checkbox]').each(function(index,el) {
			$j(el).attr('checked',false);
		});
	}
}

</script>
</head>

<div class="col_100 maincontainer ">
<div class="col_100">
<s:form  id="userAlertsForm" name="userAlertsForm" action="submitUserAlerts">
<s:hidden name="pkresolCustAlert" id="pkresolCustAlert"></s:hidden>
<s:hidden name="pkresolAllAlert" id="pkresolAllAlert"></s:hidden>
<table>
<tr>
<td>
<div id="alertsDiv">
	<div class="column">
	<div class="portlet" id="userprofileparent">
	<div id="userprofile" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
	<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
	<span class="ui-icon ui-icon-minusthick"></span>
	<!--<span class="ui-icon ui-icon-close" ></span>-->
	<s:text name="garuda.unitreport.label.user_name_profile"/>
	</div><div class="portlet-content">
	<div id="studyDashDiv1" >   
	<s:hidden name="userId" value="%{userPojo.userId}"></s:hidden>
	        <fieldset>
	        
	         <table cellpadding="0" cellspacing="0" border="0"
			style="width: 50%; height: auto;" class="col_100 " align="left">
	         	<tr>
				   <td style="width: 40%;"><s:text name="garuda.unitreport.label.user_name"/>:</td>
				   <td style="width: 60%;" align="left"><b><s:property value="userPojo.lastName" />&nbsp;<s:property value="userPojo.firstName" /></b></td> 
				</tr>
				
				<tr>
				    <td ><s:text name="garuda.unitreport.label.User_id"/>:</td>
				    <td><b> <s:property value="userPojo.loginId"/></b>
				   
				    
				</tr>
			
				<tr>
				    <td ><s:text name="User Phone"/>:</td>
				    <td><b> <s:property value="userPojo.address.phone"/></b></td>
				</tr>
				<tr>
				    <td ><s:text name="Phone Extension"/>:</td>
				    <td><b> <s:property value="userPojo.address.ext"/></b></td>
				</tr>
				
				<tr>
				    <td ><s:text name="User Cell Phone"/>:</td>
				    <td><b> <s:property value="userPojo.address.mobile"/></b></td>
				</tr>
				
				<tr>
				    <td ><s:text name="User Fax"/>:</td>
				    <td><b> <s:property value="userPojo.address.fax"/></b></td>
				</tr>
				<tr>
				    <td ><s:text name="garuda.unitreport.label.title"/>:</td>
				    <td><b><s:property value="userPojo.userTitle"/></b> </td>
				</tr>
				<tr>
				    <td ><s:text name="garuda.unitreport.label.permission_role"/>:</td>
				    <td><b><s:property value="userPojo.group.grpName"/></b> </td>
				</tr>
			
				<tr>
				    <td ><s:text name="CBB ID(s) Assigned To"/></td>
				    <td><b></b> </td>
				</tr>
				<!-- userdetails.jsp?mode=M&srcmenu= &userId= &userType= -->
				<tr> <td>
	<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr"  width="100%">
	<thead>
		<tr>			
			<th width="25%">
				<s:text name="CBB ID"></s:text>
			</th>
		</tr>
	</thead>
	<tbody>
		<s:iterator value="#request.CbbList" status="row" >
		<tr>
		<td>
		<s:property	value="siteIdentifier" />
		</td>
		</tr>
		</s:iterator>
	</tbody>
	</table>
			</td> <td></td></tr>
				
				<tr>
				 <td colspan="2" align="right">
				  <s:if test="hasEditPermission(#request.cbuMyProRght)==true">
					<button type="button" onclick="updateinfo(<s:property value="userPojo.userId"/>);"><s:text name="garuda.unitreport.label.update_my_info"/></button>
				  </s:if>
				 </td>
				</tr>
			</table>
			</fieldset>
			
			
		</div></div></div></div></div>
		</div>

</td>
</tr>
<tr>
<td>
<div>
<div class="column">
	<div class="portlet" id="useralertsparent">
	<div id="useralerts" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
	<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
	<span class="ui-icon ui-icon-minusthick"></span>
	<!--<span class="ui-icon ui-icon-close" ></span>-->
	 <s:text name="garuda.unitreport.label.manage_my_alerts"/>
	</div><div class="portlet-content">
	<table cellpadding="0" cellspacing="0" align="center">
		 <tr>
			 <td align="center">
				  <span id="hideMsg" class="error" >
				 	 <s:property value="message"></s:property>
				  </span>
			 </td>
		 </tr>
	</table>
  <fieldset>
  <table style="width: 100%;"  border="0" align="left" cellpadding="0" cellspacing="0" >
  <tr class="">
  <td colspan="4">
  <s:text name="garuda.unitreport.label.alert_user_email"/>:<b> <s:property value="userPojo.address.email" /> </b>
  <s:if test="hasEditPermission(#request.cbuMyProRght)==true">
  <a href="#" onclick="updateinfo(<s:property value="userPojo.userId"/>);"> <!-- onclick="showModal('Update User E-Mail ID','updateusermailid?mailUpdate=False&userPojo.userId=<s:property value="userPojo.userId"/>','250','480');" --> <font color="red"><s:text name="garuda.unitreport.label.modify"/></font></a>
  </s:if>
  </td>
  </tr>
  <tr >
  <td colspan="4">
  <s:text name="garuda.unitreport.label.selectEmail_to_forward"/>:
  </td>
  </tr>
 
  <tr >
<td colspan="4">
<div id="searchTble">
<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults" width="100%">
	<thead>
		<tr>			
			<th width="25%">
				<s:text name="garuda.common.title.alertTitle"></s:text>
			</th>
			<th width="75%" colspan="5" align="left">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="garuda.common.label.email"></s:text><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<s:checkbox name="emailHeader" id='emailHeader' onclick="getChecked('email');"/>
			</th>
		</tr>
	</thead>
	<tbody>
	
	<s:if test="userAlertsLst!=null && userAlertsLst.size()>0">
		<s:iterator value="userAlertsLst" var="rowVal" status="row" >
		<tr id='<s:property value="subType"/>'>
			<td width="25%">
				<span onmouseout="return nd();" onmouseover="return overlib('<s:property value="%{#rowVal[1]}"/>',CAPTION,'Alert Description');">
					<strong><s:property value="%{#rowVal[0]}"/></strong>
					<s:hidden name="userAlertList[%{#row.index}].fkCodelstId" id="alert%{#rowVal[5]}" value="%{#rowVal[5]}"></s:hidden>
					<s:hidden name="userAlertList[%{#row.index}].fkUserId" value="%{userPojo.userId}"></s:hidden>
				</span>		
			</td>
			<td align="left" width="15%" class="email">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<s:if test="%{#rowVal[2]==true}">
				<s:checkbox name="userAlertList[%{#row.index}].emailFlag" id='email%{#rowVal[5]}'  value="%{#rowVal[2]}" onclick="getResolAlerts('%{#rowVal[5]}','email');" checked="true"/>
			</s:if>
			<s:else>
				<s:checkbox name="userAlertList[%{#row.index}].emailFlag" id='email%{#rowVal[5]}' value="%{#rowVal[2]}" onclick="getResolAlerts('%{#rowVal[5]}','email');"/>
			</s:else>
			</td>
			<td width="20%"></td>
			<td width="10%"></td>
			<td width="10%"></td>
			<td width="20%"></td>
		</tr>
		</s:iterator>
	</s:if>	
		  
		
	</tbody>
	<tfoot>
	<tr>
		<td colspan="2"></td>
	</tr>
	</tfoot>
	
</table>
<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults" width="100%">
	<tr id="resolAlertsDiv">
			<td colspan="5">
			
			<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults" width="100%">
			<thead>
				<tr>			
					<th width="25%" align="center">
						<s:text name="garuda.common.title.alertTitle"></s:text>
					</th>
					<th width="15%" align="center">
						<s:text name="garuda.common.label.email"></s:text>
					</th>
					<th width="15%" align="center">
						<s:text name="garuda.orOrderDetail.portletname.ctlaborder"></s:text><br/><s:checkbox name="ctlabHeader" id='ctlabHeader' onclick="getChecked('ctlab');"/>
					</th>
					<th width="15%" align="center">
						<s:text name="garuda.orOrderDetail.portletname.ctshiporder"></s:text><br/><s:checkbox name="ctshipHeader" id='ctshipHeader' onclick="getChecked('ctship');"/>
					</th>
					<th width="15%" align="center">
						<s:text name="garuda.heOrderDetail.portletname.heOrder"></s:text><br/><s:checkbox name="heHeader" id='heHeader' onclick="getChecked('he');"/>
					</th>
					<th width="15%" align="center">
						<s:text name="garuda.orOrderDetail.portletname.orOrder"></s:text><br/><s:checkbox name="orHeader" id='orHeader' onclick="getChecked('or');"/>
					</th>
				</tr>
			</thead>
			<tbody>
				<s:if test="userResolAlertsLst!=null && userResolAlertsLst.size()>0">
					<s:iterator value="userResolAlertsLst" var="rowVal" status="row">
				<tr>
					<td width="25%">
						<strong><s:property value="%{#rowVal[0]}"/></strong>
						<s:hidden name="userResolAlertList[%{#row.index}].fkCodelstId" id="alertResol%{#rowVal[5]}" value="%{#rowVal[5]}"></s:hidden>
						<s:hidden name="userResolAlertList[%{#row.index}].fkUserId" value="%{userPojo.userId}"></s:hidden>		
					</td>
					<td align="center" width="15%" class="resolemailflag">
					<s:if test="%{#rowVal[2]==true}">
						<s:checkbox name="userResolAlertList[%{#row.index}].emailFlag" id='email%{#rowVal[5]}' checked="true"/>
						<s:hidden id="hemail[%{#row.index}]" value="%{#rowVal[2]}" name="hemail"></s:hidden>
					</s:if>
					<s:else>
						<s:checkbox name="userResolAlertList[%{#row.index}].emailFlag" id='email%{#rowVal[5]}'/>
					</s:else>
					</td>
					<td width="15%" align="center" class="ctlab">
					<s:if test="%{#rowVal[7]==true}">
						<s:checkbox name="userResolAlertList[%{#row.index}].ctlabFlag" id='ctlab%{#rowVal[5]}' checked="true"/>
					</s:if>
					<s:else>
						<s:checkbox name="userResolAlertList[%{#row.index}].ctlabFlag" id='ctlab%{#rowVal[5]}'/>
					</s:else>
					</td>
					<td width="15%" align="center" class="ctship">
					<s:if test="%{#rowVal[8]==true}"> 
						<s:checkbox name="userResolAlertList[%{#row.index}].ctshipFlag" id='ctship%{#rowVal[5]}' checked="true"/>
					</s:if>
					<s:else>
						<s:checkbox name="userResolAlertList[%{#row.index}].ctshipFlag" id='ctship%{#rowVal[5]}'/>
					</s:else>
					</td>
					<td width="15%" align="center" class="he">
					<s:if test="%{#rowVal[9]==true}">
						<s:checkbox name="userResolAlertList[%{#row.index}].heFlag" id='he%{#rowVal[5]}' checked="true"/>
					</s:if>
					<s:else>
						<s:checkbox name="userResolAlertList[%{#row.index}].heFlag" id='he%{#rowVal[5]}'/>
					</s:else>
					</td>
					<td width="15%" align="center" class="or">
					<s:if test="%{#rowVal[10]==true}">
						<s:checkbox name="userResolAlertList[%{#row.index}].orFlag" id='or%{#rowVal[5]}' checked="true"/>
					</s:if>
					<s:else>
						<s:checkbox name="userResolAlertList[%{#row.index}].orFlag" id='or%{#rowVal[5]}'/>
					</s:else>
					</td>
				</tr>
				</s:iterator>
				</s:if>
				</tbody>
			</table>
			
			</td>
		</tr>
</table>
</div>
</td>
</tr>
<tr >
  <td colspan="4" align="right" >
  <s:if test="hasEditPermission(#request.cbuMyProRght)==true">
  <button type="submit"><s:text name="garuda.unitreport.label.button.submit"/></button>
  </s:if>
 </td>
</tr>
</table>
</fieldset>
    
    
<div class="clear"></div><br/><br/>
	        
	        </div>
	       
	        </div>
	        </div>
	        </div>
	</div>

</td>
</tr>
</table>
		
		
</s:form>

</div>
</div>