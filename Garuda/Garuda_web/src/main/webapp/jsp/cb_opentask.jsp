<%@ taglib prefix="s"  uri="/struts-tags"%>
<script>
$j(function() {
	$j( "#datepicker" ).datepicker();
});


function constructTable() {
		 $j('#searchResults').dataTable();
	 
	
	}
 function beforecdrcbuscreen(cdrcbuId,pkextinfo,codeId){
	 	
			showCdrCbuScreen(cdrcbuId,pkextinfo,codeId);
			updateBreadcrumb('Assign Task','loadpage(\'assigntask\')');
		}
		function showCdrCbuScreen(formname,cdrcbuId,pkextinfo,codeId){
			$j('#cdrcbuId').val(cdrcbuId);
			$j('#pkextinfo').val(pkextinfo);
			$j('#codeId').val(codeId);
			submitform(formname);
		  }
	
	 function showDiv()
	 {
				$j('#searchresultparent').css('display','block');
				refreshDiv('getTaskDetails','searchTble','searchCordInfo');
	 }				
</script>

<div class="col_100 maincontainer ">
<div class="col_100">
<table width="100%" id="container1" >
<tr><td>
	<div class="column">
	<div class="portlet" id="cdrsearchparent">
		<div id="cdrsearch" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			<span class="ui-icon ui-icon-minusthick"></span>
			<!--<span class="ui-icon ui-icon-close" ></span>-->
			<s:text name="garuda.cbuentry.label.cdrcbusearch" />
			</div>
		<div class="portlet-content">
		<div id="studyDashDiv1" > 
  
				
	        <s:form id="searchCordInfo" method="post">
			         <table cellpadding="0" cellspacing="0" border="0"
					style="width: 100%; height: auto;" class="col_100 ">         
			         	<tr>
						   <td ><s:text name="garuda.cbuentry.label.cbuid" />:</td>
						   <td><s:textfield  name="cdrCbuPojo.cdrCbuId" placeholder="CBU ID" onkeypress="return enter2Refreshdiv(event);"/></td> 
						   <td><button type="button" onclick="showDiv();"><s:text name="garuda.uploaddoc.label.search"/></button> </td>
						</tr>				
					</table>
	        	</s:form>
	        	
		</div></div></div></div></div>
</td></tr>
</table>
        
			
<table width="100%" id="container2" ><tr><td>
<div class="column">

<div class="portlet" id="searchresultparent" style="display: none;"><div id="searchresult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span><!--<span class="ui-icon ui-icon-close" ></span>--><s:text name="garuda.cbuentry.label.cdrcbusearchresult" /></div><div class="portlet-content">
<s:form name="search" id="search" action="assigntask" method="post">
<s:hidden name="cdrcbuId" id="cdrcbuId"></s:hidden>
<s:hidden name="pkextinfo" id="pkextinfo"></s:hidden>
<s:hidden name="codeId" id="codeId"></s:hidden>
<div id="searchTble">

<table style="height:100%"  border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults" >
       
	
                <thead>
                      <tr>
                        <th class="th2"  scope="col"><s:text name="garuda.opentask.label.orderid" /></th>
                        <th class="th3"  scope="col"><s:text name="garuda.opentask.label.cbuid" /></th>
                        <th class="th4"  scope="col"><s:text name="garuda.opentask.label.assign" /></th>
                        <th class="th5"  scope="col"></th>
                      </tr>
			         </thead>  
			  <tbody>     
			 <s:iterator value="cordInfoList" var="rowVal" status="row">
				<tr>
					<td></td>		
					<td><a href="#" onclick="javaScript:return showCdrCbuScreen('search','<s:property value="%{#rowVal[1]}" />','<s:property value="%{#rowVal[2]}" />','<s:property value="%{#rowVal[3]}" />')" ><s:property value="%{#rowVal[1]}" /></a></td>
					<td><a href="#" onclick="javaScript:return showCdrCbuScreen('search','<s:property value="%{#rowVal[1]}" />','<s:property value="%{#rowVal[2]}" />','<s:property value="%{#rowVal[3]}" />')" ><!--<s:text name="garuda.opentask.label.yes" /> --></a></td>
					<td><a href="#" onclick="javaScript:return showCdrCbuScreen('search','<s:property value="%{#rowVal[1]}" />','<s:property value="%{#rowVal[2]}" />','<s:property value="%{#rowVal[3]}" />')" ><s:text name="garuda.opentask.label.goTo" /></a></td>
					
				</tr>
			
			 </s:iterator>
              </tbody>  
				  <tfoot>
					<tr><td colspan="6"></td></tr>
				  </tfoot>
                
                </table>
	
        </div>
        </s:form>
		</div>
       
         </div>

</div>
</div>
</td></tr>     
       
 </table>       
</div>
</div>