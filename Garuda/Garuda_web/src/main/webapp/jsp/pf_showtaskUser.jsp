<%@ taglib prefix="s"  uri="/struts-tags"%>
<script>
$j(function() {
	$j( "#datepicker" ).datepicker({changeMonth: true,
		changeYear: true});
});


function constructTable() {
	  // alert("ready Volunteer")
		 $j('#searchResults').dataTable();
		 var tdObj = $j('#searchResults').find('tbody').find("tr:eq(0)").find("td:eq(0)");
			if($j(tdObj).hasClass('dataTables_empty')){
				$j('#searchResults_info').hide();
				$j('#searchResults_paginate').hide();
			} 
	}

$j(document).ready(function(){

	

});


function showAddWidget(url){
	 url = url + "?pageId=1" ; 
	 showAddWidgetModal(url);
}


</script>
<form>
<div class="col_100 maincontainer ">
<div class="col_100">
<div>
<div class="portlet" id="mailresultdiv">
<s:iterator value="lstTask" var="rowVal" status="row">

		<div id="mailresult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
				<span class="ui-icon ui-icon-minusthick"></span>
				<!--<span class="ui-icon ui-icon-close" ></span>-->
				<s:text name="garuda.entryTask.label.heading" />
			</div>
		<div class="portlet-content">
		<div id="cbuDashDiv1">
	<div>
		
		<table border="0" align="center" cellpadding="0" cellspacing="0"
			height="100%" style="width: 500px;">
			<tr>
				<td  style="width: auto"></td>
				<td  style="width: auto"><s:text name="garuda.entryTask.label.fname" /></td>
				<td  style="width: auto"><s:text name="garuda.entryTask.label.lname" /></td>
				<td  style="width: auto"><s:text name="garuda.entryTask.label.mname" /></td>
			</tr>
			<tr>
				<td ><s:text name="garuda.entryTask.label.name" />:</td>
				<td ><b><s:property value="%{#rowVal[2]}" /></b></td>
				<td ><b><s:property value="%{#rowVal[3]}" /></b></td>
				<td ><b><s:property value="%{#rowVal[4]}" /></b></td>
			</tr>
			
			<tr>
				<td  style="width: auto"><s:text name="garuda.entryTask.label.loginid" />:</td>
				<td  colspan="3" style="width: auto"><b><s:property value="%{#rowVal[1]}" /></b></td>
			</tr>
			<tr>
				<td  style="width: auto"><s:text name="garuda.entryTask.label.mailid" />:</td>
				<td  colspan="3" style="width: auto"><b><s:property value="%{#rowVal[5]}" /></b></td>
			</tr>
		</table>
		<br />
		<br />
		<br />
		<table>
		<tr>
		<td>
		<fieldset>
		<legend ><b><s:text name="garuda.entryTask.label.tasktobe" /></b></legend>
		<br />
			<table border="0" align="center" cellpadding="0" cellspacing="0"
				height="100%" style="width: 400px;">
				<tr>
					<td  colspan="2" style="width: auto;"><s:text name="garuda.entryTask.label.reqid" />:</td>
					<td  colspan="2" style="width: auto;"><b><s:property value="%{#rowVal[6]}" /></b></td>
				</tr>
				<tr>
					<td  colspan="2" style="width: auto;"><s:text name="garuda.entryTask.label.reqtype" />:</td>
					<td  colspan="2" style="width: auto;"><b><s:property value="%{#rowVal[7]}" /></b></td>
				</tr>
				<tr>
					<td  colspan="2" style="width: auto;"><s:text name="garuda.entryTask.label.link" />:</td>
					<td  colspan="2" style="width: auto;"><b><a href="#"><s:text name="garuda.entryTask.label.addinfo" /></a></b></td>
				</tr>
			</table>
		</fieldset>
		</td>
		</tr>
		<tr><td>
		<br />
		<!--<input type="button" value="Send E-mail" onclick="getmail('<s:property value="%{#rowVal[5]}" />');" />-->
		<input type="button" value="<s:text name="garuda.entryTask.label.sendmail"/>" onclick="showModal('Email To DATA ENTRY for the CBU ID <s:property value="%{#rowVal[8]}" />','sendMails?cbuID=<s:property value="%{#rowVal[8]}" /> &mailid=<s:property value="%{#rowVal[5]}" />','334','567')" />
		</td>
		</tr>
		</table>
		</div>
	
</div></div></div>
</s:iterator>
</div>
</div>

</div>
</div></form>