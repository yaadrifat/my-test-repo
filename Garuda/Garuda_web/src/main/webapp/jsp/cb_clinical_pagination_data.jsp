<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="userLB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<script>
function mainonload(){    
	
	if($j('#completeReqInfoFlag').val()=="1"){
		$j('#finalReviewBtn').removeAttr('disabled');
	}
	else{
		$j('#finalReviewBtn').attr('disabled', true);
		}
	
	

	if($j('#completeReqInfoFlag').val()=="1"){
		$j('#finalReviewBtn').removeAttr('disabled');
	}
	else{
		$j('#finalReviewBtn').attr('disabled', true);
		}

	
	
	$j('#searchResults').dataTable(); 
	$j('#searchResults1').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
		},
		"bRetrieve": true,
		"bDestroy" :true
	} );
	$j('#searchResults1_length').empty();
	$j('#searchResults1_filter').empty();
	
$j('#searchResults2').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
		},
		"bRetrieve": true,
		"bDestroy" :true
	} );

$j('#searchResults2_length').empty();
$j('#searchResults2_filter').empty();

$j('#searchResults3').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
} );

$j('#searchResults3_length').empty();
$j('#searchResults3_filter').empty();

$j('#searchResults4').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
});
$j('#searchResults4_length').empty();
$j('#searchResults4_filter').empty();

$j('#searchResults5').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
});
$j('#searchResults5_length').empty();
$j('#searchResults5_filter').empty();

$j('#searchResults6').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
});
$j('#searchResults6_length').empty();
$j('#searchResults6_filter').empty();

$j('#searchResults7').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
});
$j('#searchResults7_length').empty();
$j('#searchResults7_filter').empty();

$j('#searchResults8').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
});
$j('#searchResults8_length').empty();
$j('#searchResults8_filter').empty();

$j('#searchResults9').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
});

$j('#searchResults9_length').empty();
$j('#searchResults9_filter').empty();
//$j('#searchResults10').dataTable();

$j('#maternalHlaResults').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	}
});
/*
$j('#idmtestResults').dataTable({
	"oLanguage": {
		"sEmptyTable": "Test Results not available"
	}
});*/

$j('button,input[type=submit],input[type=button]').button();
$j( "#datepicker" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	changeYear: true});
$j( "#datepicker1" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	changeYear: true});
$j( "#datepicker2" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	changeYear: true});

}

function lookupDataTable(){
	//fn_events('tablelookup1','targetall','ulSelectColumn');
	maindivwidth=$j('.maincontainer').width();
	var orderwrapper=maindivwidth*1.00;
	var temp1=orderwrapper*0.96;
	var scr_hgh=screen.height*0.20;
	var temp=scr_hgh;
	$j('#tablelookup1').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
		},
		"iDisplayLength":1000,
		/* "sScrollY": temp,
		"sScrollX": temp1, */
		"bSortCellsTop": true,
		"bDeferRender": true,
		"bRetrieve" : true,
		"bDestroy": true, 
		"bAutoWidth": false
	});
	 $j('#tablelookup1_info').hide();
		$j('#tablelookup1_paginate').hide();
		$j('#tablelookup1_length').empty();
		//$j('#tablelookup1_length').replaceWith('Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;" ><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="paginateSearch.getiTotalRows()"/>">All</option></select> Entries');
		$j('#tablelookup1_length').replaceWith('Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;" ><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries');
		if($j('#entries').val()!=null || $j('#entries').val()!=""){
			$j('#showsRow').val($j('#entries').val());
			}
		$j('#showsRow').change(function(){
			var selectedText = $j('#showsRow :selected').html();
	        if(selectedText=="All"){
	        	forAllLookUpGrid(temp);
	        }else{
	        	$j('#tableBody').empty();
				paginationFooter(0,"getMainCBUDetailsPaginationData,showsRow,inputs,Tableform,searchTblediv,lookupDataTable",'<s:property value="cbuCount"/>','<s:property value="paginateModule"/>');
	        }
		});
		var inputs = $j('#inputs').val();
	if($j.trim(inputs)!="")
	{
		$j("#searchTblediv td:contains('"+inputs.toLowerCase()+"')").each(function (i, el) {
			 highlightContent(el, inputs.toLowerCase());
	    });
		$j("#searchTblediv td:contains('"+inputs.toUpperCase()+"')").each(function (i, el) {
			highlightContent(el, inputs.toUpperCase());
	    });
	}    
	    $j('.lookupmenu :text').removeAttr("disabled");
	    
	    $j('#tablelookup1_filter').find('input:text').keyup(function(){
			f_countRows('tablelookup1','1','fromsearchall','empty3','empty1','empty2','empty3','empty4');
		});
	       
}
function fn_ResetLookupSort(){
if($j('#lookup_tbl_sort_col').val()!=undefined && $j('#lookup_tbl_sort_col').val()!=null && $j('#lookup_tbl_sort_col').val()!=""){
		
		$j('#lookup_tbl_sort_col').val('');
		$j('#lookup_tbl_sort_type').val('');
		var oTable = $j('#tablelookup1').dataTable();
		oTable.fnSort( [ [0,'asc'] ] );
		oTable.fnDraw();
	}
}
$j(function() {
	mainonload();
	//lookupDataTable();
	//settingwidth();
});

function loadCordData(cordID){
	var url = "loadClinicalData?cdrCbuPojo.cordID="+cordID;
	loadPageByGetRequset(url,'cordSelectedData');
}

function forAllLookUpGrid(){

	
	//var showEntriesTxt='Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="paginateSearch.getiTotalRows()"/>" selected="selected">All</option></select> Entries';
	var showEntriesTxt='Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries';
	var is_all_entries=$j('#showsRow').val();
	var pageNo=$j('#paginateWrapper1').find('.currentPage').text();
	var showEntries=$j('#lookup_tbl_sentries').val();
	var totalEntries=$j('#tentries').val();
	var sort=0;
	var sortDir="asc";
	var SortParm=$j('#lookup_tbl_sort_col').val();
	var seachTxt="";
	var preTxt=$j('#lookup_search_txt').val();
	
	if((preTxt!=null && $j.trim(preTxt)!="")){
		seachTxt=preTxt;
	}
	
	
	//alert("SortParm::::"+SortParm)

	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#lookup_tbl_sort_col').val();
		sortDir=$j('#lookup_tbl_sort_type').val();
	}	
	

	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#lookup_tbl_sentries').val(is_all_entries);
		showEntries=$j('#lookup_tbl_sentries').val();
	}
	
	
	if(showEntries=='All'){
		
		otable =  $j('#tablelookup1').dataTable({
			"sScrollY": "100px", 
	        "sDom": "frtiS",
	        "sAjaxSource": 'getJsonPagination.action?TblFind=lookup&allEntries=ALL&paginateModule=<s:property value="paginateModule"/>&cbuCount=<s:property value="cbuCount"/>&cbuid='+$j("#inputs").val(),
	        "bServerSide": true,
	        "bProcessing": true,
	        "bRetrieve" : true,
	        "bDestroy": true,
	        "aaSorting": [[ sort, sortDir ]],
	        "oSearch": {"sSearch": seachTxt},
	        "bDeferRender": true,
	        "bAutoWidth": false,
	        "sAjaxDataProp": "aaData",
	        "oLanguage": {
	        	"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
			},
	        "aoColumnDefs": [
	                          {
	                              "aTargets": [0],"mDataProp": function ( source, type, val ) {
	                            	    var str="";
	                            	    var text="";
	                            	    if(source[6]!='null' && source[6]!=null){
	                            	    	text = source[6].replace(/'/g,"&#145;");
	                            	    }
	                            	    //alert(text);
	                            	    if(source[1]!='null' && source[1]!=null){
	                            		str = "<span style='width:100%;height:100%' onmouseover='return overlib(\"<strong>"+text+"</strong>\",CAPTION,\"CBB Name\");' onmouseout='return nd();'><input type='hidden' name='pkcordIdInAll' value='"+source[0]+"'/>";
	                                	str += "<div>"+source[1]+"</div></span>";
	                            	    }
	                                	return str;
	                             }},
	                            {
	                                    "aTargets": [1], "mDataProp": function ( source, type, val ) {
	                                    var str="";
	                                    if(source[2]!='null' && source[2]!=null)
	                                    str="<span><div>"+source[2]+"</div></span>";
	                                    return str;
	                             }},
	                          	{
	                                 "aTargets": [2], "mDataProp": function ( source, type, val ) {
	                                	var str="";
	                                	if(source[3]!='null' && source[3]!=null)
	                                	str="<span><div>"+source[3]+"</div></span>";
	                                    return str;
	                             }},
	                             {
	                                 "aTargets": [3], "mDataProp": function ( source, type, val ) {
	                                	var str="";
	                                	if(source[4]!='null' && source[4]!=null)
	                                	str="<span><div>"+source[4]+"</div></span>";
	                                    return str;
	                             }},
	                             {
	                                 "aTargets": [4], "mDataProp": function ( source, type, val ) {
	                                	var str="";
	                                	if(source[5]!='null' && source[5]!=null)
	                                	str="<span><div>"+source[5]+"</div></span>";
	                                    return str;
	                             }},
	                             {
	                                 "aTargets": [5], "mDataProp": function ( source, type, val ) {
	                                	var str="";
	                                	if(source[6]!='null' && source[6]!=null)
	                                	str="<span><div>"+source[6]+"</div></span>";
	                                    return str;
	                             }},
	                             {
	                                 "aTargets": [6], "mDataProp": function ( source, type, val ) {
	                                	var str="";
	                                	if(source[7]!='null' && source[7]!=null)
	                                	str="<span><div>"+source[7]+"</div></span>";
	                                    return str;
	                             }}
	                             
	                        ],
	                        "fnDrawCallback": function() {
	                            $j('#tablelookup1_wrapper').find('.dataTables_scrollFoot').hide();
	                            $j('#tablelookup1_info').show();
	                            $j("#showsRow option:contains('All')").attr('selected', 'selected');
	                            $j('#tablelookup1_wrapper').find('.dataTables_scrollHeadInner').find('table').css('position','');
	                            var inputs = $j('#inputs').val();
	                        	var chk_arr =  document.getElementsByName("pkcordIdInAll");
	                     	 	var chklength = chk_arr.length;  
								var k;
		 		                for(k=0;k< chklength;k++){
	                   	          	var parObj = chk_arr[k].parentNode.parentNode.parentNode;
	                   	         	$j(parObj).bind("click",function(){
	                       	         	var inputVal = $j(this).find("input[name='pkcordIdInAll']").val();
	                       	         //	console.log("inputVal::"+inputVal);
	                   	         		loadCordData(inputVal);
	                       	         }).addClass('hotspot');	
	                     	  	}
	                        	if($j.trim(inputs)!="")
	                        	{
	                        		$j("#searchTblediv td span div:contains('"+inputs.toLowerCase()+"'), #searchTblediv td span div:contains('"+(inputs.toLowerCase()).replace(/-/gi,"")+"')").each(function (i, el) {
			                        	highlightContentForAll(el, inputs.toLowerCase());
	                        	    });

		                        	$j("#searchTblediv td span div:contains('"+inputs.toUpperCase()+"'), #searchTblediv td span div:contains('"+(inputs.toUpperCase()).replace(/-/gi,"")+"')").each(function (i, el) {
			                        	highlightContentForAll(el, inputs.toUpperCase());
	                        	    });

		                        	$j("#searchTblediv td span div:contains('-')").each(function (i, el) {
			                        	var content=$j(el).text().toLowerCase();
			                        	content=content.replace(/-/gi,"");
			                        	var inputtxt=(inputs.toLowerCase()).replace(/-/gi,"");
			                        	if(content.indexOf(inputtxt)!=-1){
				                        	highlightContentForAll(el, inputs.toLowerCase());
				                        }
			                        	else{
				                        	return;
			                        	}
	                        	    });
	                        	}
	                        	$j('.lookupmenu :text').removeAttr("disabled");
	                        	setSortData('lookupTblThead','lookup_tbl_sort_col','lookup_tbl_sort_type');
	                        }
	    });
		$j('#tablelookup1_filter').before(showEntriesTxt);
		
	}else{
		
		otable =  $j('#tablelookup1').dataTable({
	        "sAjaxSource": 'getJsonPagination.action?TblFind=lookup&allEntries=ALL&paginateModule=<s:property value="paginateModule"/>&cbuCount=<s:property value="cbuCount"/>&cbuid='+$j("#inputs").val()+'&iDisplayStart=0&iDisplayLength='+is_all_entries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries,
	        "bServerSide": true,
	        "bProcessing": true,
	        "bRetrieve" : true,
	        "bDestroy": true,
	        "aaSorting": [[ sort, sortDir ]],
	        "oSearch": {"sSearch": seachTxt},
	        "bDeferRender": true,
	        "bAutoWidth": false,
	        "sAjaxDataProp": "aaData",
	        "oLanguage": {
	        	"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
			},
	        "aoColumnDefs": [
	                          {
	                              "aTargets": [0],"mDataProp": function ( source, type, val ) {
	                            	    var str="";	     
	                            	    var text="";
	                            	    if(source[6]!='null' && source[6]!=null){
	                            	    	text = source[6].replace(/'/g,"&#145;");
	                            	    }
	                            	    if(source[1]!='null' && source[1]!=null){
	                            		str = "<span style='width:100%;height:100%' onmouseover='return overlib(\"<strong>"+text+"</strong>\",CAPTION,\"CBB Name\");' onmouseout='return nd();'><input type='hidden' name='pkcordIdInAll' value='"+source[0]+"'/>";
	                                	str += "<div>"+source[1]+"</div></span>";
	                            	    }
	                                	return str;
	                             }},
	                            {
	                                    "aTargets": [1], "mDataProp": function ( source, type, val ) {
	                                     var str="";
	                                     if(source[2]!='null' && source[2]!=null)
	                                     str="<span><div>"+source[2]+"</div></span>";
	                                     return str;
	                             }},
	                          	{
	                                 "aTargets": [2], "mDataProp": function ( source, type, val ) {
	                                	var str="";
	                                	if(source[3]!='null' && source[3]!=null)
	                                	str="<span><div>"+source[3]+"</div></span>";
	                                    return str;
	                             }},
	                             {
	                                 "aTargets": [3], "mDataProp": function ( source, type, val ) {
	                                	var str="";
	                                	if(source[4]!='null' && source[4]!=null)
	                                	str="<span><div>"+source[4]+"</div></span>";
	                                    return str;
	                             }},
	                             {
	                                 "aTargets": [4], "mDataProp": function ( source, type, val ) {
	                                	var str="";
	                                	if(source[5]!='null' && source[5]!=null)
	                                	str="<span><div>"+source[5]+"</div></span>";
	                                    return str;
	                             }},
	                             {
	                                 "aTargets": [5], "mDataProp": function ( source, type, val ) {
	                                	var str="";
	                                	if(source[6]!='null' && source[6]!=null)
	                                	str="<span><div>"+source[6]+"</div></span>";
	                                    return str;
	                             }},
	                             {
	                                 "aTargets": [6], "mDataProp": function ( source, type, val ) {
	                                	var str="";
	                                	if(source[7]!='null' && source[7]!=null)
	                                	str="<span><div>"+source[7]+"</div></span>";
	                                    return str;
	                             }}
	                             
	                        ],
	                        "fnDrawCallback": function() {
	                            $j('#tablelookup1_wrapper').find('.dataTables_scrollHeadInner').find('table').css('position','');
	                            var inputs = $j('#inputs').val();
	                        	var chk_arr =  document.getElementsByName("pkcordIdInAll");
	                     	 	var chklength = chk_arr.length;  
								var k;
		 		                for(k=0;k< chklength;k++){
	                   	          	var parObj = chk_arr[k].parentNode.parentNode.parentNode;
	                   	         	$j(parObj).bind("click",function(){
	                       	         	var inputVal = $j(this).find("input[name='pkcordIdInAll']").val();
	                   	         		loadCordData(inputVal);
	                       	         }).addClass('hotspot');	
	                     	  	}
	                        	if($j.trim(inputs)!="")
	                        	{
		                        	
		                        	$j("#searchTblediv td span div:contains('"+inputs.toLowerCase()+"'), #searchTblediv td span div:contains('"+(inputs.toLowerCase()).replace(/-/gi,"")+"')").each(function (i, el) {
			                        	highlightContentForAll(el, inputs.toLowerCase());
	                        	    });

		                        	$j("#searchTblediv td span div:contains('"+inputs.toUpperCase()+"'), #searchTblediv td span div:contains('"+(inputs.toUpperCase()).replace(/-/gi,"")+"')").each(function (i, el) {
			                        	highlightContentForAll(el, inputs.toUpperCase());
	                        	    });

		                        	$j("#searchTblediv td span div:contains('-')").each(function (i, el) {
			                        	var content=$j(el).text().toLowerCase();
			                        	content=content.replace(/-/gi,"");
			                        	var inputtxt=(inputs.toLowerCase()).replace(/-/gi,"");
			                        	if(content.indexOf(inputtxt)!=-1){
			                        		highlightContentForAll(el, inputs.toLowerCase());
				                        }
			                        	else{
				                        	return;
			                        	}
	                        	    });
                      	
	                        	}
	                        	$j('.lookupmenu :text').removeAttr("disabled");
	                        	setSortData('lookupTblThead','lookup_tbl_sort_col','lookup_tbl_sort_type');
	                        	$j('#tablelookup1_info').hide();
	                 			$j('#tablelookup1_paginate').hide();
	                        }
	    });
		$j('#tablelookup1_length').replaceWith(showEntriesTxt);
		
	}
    
    $j('#showsRow').change(function(){
        var selectedText = $j('#showsRow :selected').html();
        //alert("value:::::::::::::::::"+selectedText)
        $j('#lookup_tbl_sentries').val(selectedText);
        paginationFooter(0,"getMainCBUDetailsPaginationData,showsRow,inputs,Tableform,searchTblediv,forAllLookUpGrid",'<s:property value="cbuCount"/>','<s:property value="paginateModule"/>');
    }); 
    
    $j('#tablelookup1_filter').find(".searching1").keyup(function(e){
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#lookup_search_txt').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"getMainCBUDetailsPaginationData,showsRow,inputs,Tableform,searchTblediv,forAllLookUpGrid","0",'<s:property value="paginateModule"/>');
		  }
	});
}
function highlightContentForAll(el, inputs){
	var startIndex,endIndex;
	var text = $j(el).text();
	var str = text;
	var highLightText = "";
	var indices = [];
	var nonmatchindices = [];
	for(var i=0; i<str.length;i++) {
	    if (str[i] === "-") indices.push(i);
	}
	dashInput = (inputs.replace(/-/gi,"")).toLowerCase();
	dashContent = (text.replace(/-/gi,"")).toLowerCase();
	for(var i=0; i<str.length;i++) {
	    if (str[i] !== dashContent[i]) {
		    nonmatchindices.push(i);
		}
	}
	highLightText=dashContent.substring((dashContent.indexOf(dashInput)),((dashContent.indexOf(dashInput))+dashInput.length));
	if(text.indexOf('-')!=-1){
	startIndex=(dashContent.indexOf(dashInput));
	for(var i=0; i<indices.length;i++) {
		if(indices[i]<=startIndex){
			startIndex+=1;
		}
	}
	endIndex=startIndex+(dashInput.length);
	for(var i=0; i<indices.length;i++) {
		if(indices[i]>=startIndex && indices[i]<=endIndex){
			var dash = "-";
			var position = indices[i]-startIndex;
			highLightText = [highLightText.slice(0, position), dash, highLightText.slice(position)].join('');
			endIndex=startIndex+(highLightText.length);
			var lastIndex = highLightText.slice(-1);
			if(lastIndex=="-"){
				highLightText=highLightText.substring(0,(highLightText.length)-1);
			}
			if(highLightText.charAt(0)=="-"){
				highLightText=highLightText.substring(1,(highLightText.length));
			}
		}
	}
	}
	if(dashContent.indexOf(dashInput)!=-1){
		var pattern = new RegExp(highLightText, 'gi');
		text = text.replace(pattern ,"<span class='highlightCont'>"+highLightText+"</span>");
	}
    $j(el).html(text);
}
</script>
<input type="hidden" name="entries" id="entries" value=<%=paginateSearch.getiShowRows() %> />
<input type="hidden" name="tentries" id="tentries" value=<%=paginateSearch.getiTotalRows() %> />
<form>
<div align="right"><a href="#" onclick="fn_ResetLookupSort()"  id="resetSortLookup"><s:text name="garuda.pendingorderpage.label.resetsort"/></a></div><br>
<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="tablelookup1">
		<thead>
			<tr id="lookupTblThead">
				<th><s:text name="garuda.cbuentry.label.registrycbuid" /></th>
				<th><s:text name="garuda.cbuentry.label.registrymaternalid" /></th>
				<th><s:text name="garuda.cbuentry.label.localcbuid" /></th>
				<th><s:text name="garuda.cbuentry.label.localmaternalid" /></th>
				<th><s:text name="garuda.cbuentry.label.isbtDin" /></th>
				<th><s:text name="garuda.cbuentry.label.CBB" /></th>
				<th><s:text name="garuda.recipient&tcInfo.label.recipientid" /></th>
			</tr>

		</thead>
		<tbody id="tableBody">
		<s:iterator value="cordInfoList" var="rowVal" status="row">	
					<tr id="<s:property	value="%{#row.index}"/>" class="hotspot" onclick="loadCordData('<s:property value="%{#rowVal[0]}"/>')" >
						<s:if test="%{checkTokenExistence(#rowVal[1],inputId)==true}">
						<td class="highlight_match" onmouseover="return overlib('<strong><s:property value="%{#rowVal[6]}"  /></strong>',CAPTION,'CBB Name');highlight('<s:property	value="%{#row.index}"/>');" onmouseout="return nd();removeHighlight('<s:property	value="%{#row.index}"/>');" ><s:if test="%{#rowVal[1]!=null}"><s:property value="%{#rowVal[1]}"  /></s:if></td>
						</s:if>
						<s:else>
						<td onmouseover="return overlib('<strong><s:property value="%{#rowVal[6]}"  escapeJavaScript="true"/></strong>',CAPTION,'CBB Name');highlight('<s:property	value="%{#row.index}"/>');" onmouseout="return nd();removeHighlight('<s:property	value="%{#row.index}"/>');" ><div style="display:none"><s:property value="%{removeSymbol(#rowVal[1],'-')}"/></div><s:if test="%{#rowVal[1]!=null}"><s:property value="%{#rowVal[1]}"  /></s:if></td>
						</s:else>
										
						<s:if test="%{checkTokenExistence(#rowVal[2],inputId)==true}">
						<td class="highlight_match"><div style="display:none"><s:property value="%{removeSymbol(#rowVal[2],'-')}"/></div><s:property value="%{#rowVal[2]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[2]!=null}"><div style="display:none"><s:property value="%{removeSymbol(#rowVal[2],'-')}"/></div><s:property value="%{#rowVal[2]}"  /></s:if></td>
						</s:else>
						
						<s:if test="%{checkTokenExistence(#rowVal[3],inputId)==true}">
						<td class="highlight_match"><s:property value="%{#rowVal[3]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[3]!=null}"><s:property value="%{#rowVal[3]}"  /></s:if></td>
						</s:else>
						
						<s:if test="%{checkTokenExistence(#rowVal[4],inputId)==true}">
						<td class="highlight_match"><s:property value="%{#rowVal[4]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[4]!=null}"><s:property value="%{#rowVal[4]}"  /></s:if></td>
						</s:else>
						
						<s:if test="%{checkTokenExistence(#rowVal[5],inputId)==true}">
						<td class="highlight_match"><s:property value="%{#rowVal[5]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[5]!=null}"><s:property value="%{#rowVal[5]}"  /></s:if></td>
						</s:else>
						
						<s:if test="%{checkTokenExistence(#rowVal[6],inputId)==true}">
						<td class="highlight_match"><s:property value="%{#rowVal[6]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[6]!=null}"><s:property value="%{#rowVal[6]}"  /></s:if></td>
						</s:else>
						
						<s:if test="%{checkTokenExistence(#rowVal[7],inputId)==true}">
						<td class="highlight_match"><s:property value="%{#rowVal[7]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[7]!=null}"><s:property value="%{#rowVal[7]}"  /></s:if></td>
						</s:else>
					</tr>
				</s:iterator>
		</tbody>
		
		<tfoot>
			<tr><td colspan="8"></td></tr>			
			<tr><td colspan="3">
		    <jsp:include page="paginationFooter.jsp">
		  	<jsp:param value="searchTblediv" name="divName"/>
		  	<jsp:param value="Tableform" name="formName"/>
		  	<jsp:param value="showsRow" name="showsRecordId"/>
		  	<jsp:param value="getMainCBUDetailsPaginationData" name="url"/>
		  	<jsp:param value="inputs" name="cbuid"/>
		  	<jsp:param value="workflow" name="paginateSearch"/>
		  	<jsp:param value="forAllLookUpGrid" name="bodyOnloadFn"/>
		  	<jsp:param value="1" name="idparam"/>
		  </jsp:include>
    </td>
    <td colspan="5"></td>
    </tr>
		</tfoot>
		
	</table>
	</form>