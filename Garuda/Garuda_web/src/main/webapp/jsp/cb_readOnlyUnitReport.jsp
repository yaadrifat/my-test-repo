
<%@ taglib prefix="s"  uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><s:text name="garuda.common.footer.velos"/></title>


</head>
<body>
<table width="100%" border="1">
<tr><td>
<div class="headerStyle row" id="header">
				<table width="100%">
				<tr align="right">
				    <td><button type="button" onClick="window.close()"><s:text name="garuda.common.close"/></button> </td>
				</tr></table>
</div>
<s:form action=""
	id="CBUUnitRptForm" name="CBUUnitRptForm">
	<table class="tabledisplay" align="center" border="0">
		<tr><th><s:text name="garuda.unitreport.label.heading" /></th>
		</tr>
		
	</table>

	<table width="100%" class="tabledisplay" align="center" border="0">
		<tbody>
			<tr><td><hr align="center" width="100%"></hr></td></tr>
			<tr>
				<td align="center"><s:text name="garuda.unitreport.label.sublabel" />
				<s:text	name="cdrCbuPojo.cdrCbuId" /></td>
			</tr>
		</tbody>
	</table>
	 
	 <table width="100%">
	 		<tr>
	 			<b><s:text name="garuda.unitreport.label.cbuinfo.subheading" /></b>
			</tr>			
			<tr></tr>


			<tr>
				<td ><s:text name="garuda.unitreport.label.regunitid" /></td>
				<td ><s:property value="cdrCbuPojo.registryId" /></td>

				<td ><s:text name="garuda.unitreport.label.idnocbubag" /></td>
				<td ><s:property value="cdrCbuPojo.numberOnCbuBag"   /></td>
			</tr>
		
			<tr>
				<td ><s:text name="garuda.unitreport.label.localcbuid" /></td>
				<td ><s:property value="cdrCbuPojo.localCbuId"  /></td>

				<td ><s:text name="garuda.unitreport.label.collectiondate" /></td>
				<td ><s:text name="collection.date" >
						<s:param value="cdrCbuPojo.specimen.specCollDate" />
					</s:text> </td>
			</tr>
			<tr></tr>
	</table>
	
	<b><s:text name="garuda.unitreport.label.prodinfo.subheading" /></b>
	
	<table width="100%">
				<tr></tr>
		
				<tr>
					<td ><s:text
						name="garuda.unitreport.label.cellviability" /></td>
					<td colspan="2"><s:property value="getCbuUnitReportPojo().cordViabPostProcess" /></td>	
					<td colspan="2"><s:text
						name="garuda.unitreport.label.method" /></td>
					<td colspan="2"><s:property value="getCodeListDescById(getCbuUnitReportPojo().cordViabMethod)" />
					</td>
					<td colspan="2"><s:text
						name="garuda.unitreport.label.testdate" /></td>
					<td ><s:date name="getCbuUnitReportPojo().cordViabPostTestDate"
						id="strcordViabPostTestDate" format="MMM dd, yyyy" />
						<s:property  value="%{strcordViabPostTestDate}" />
					</td><td colspan="2"></td>	
	
				</tr>
				
				<tr>
					<td ><s:text
						name="garuda.unitreport.label.cfupostprocessingcount" /></td>
					<td colspan="2"><s:property value="getCbuUnitReportPojo().cordCfuPostProcessCount"  /></td>	
					<td colspan="2"><s:text
						name="garuda.unitreport.label.method" /></td>
					<td colspan="2"><s:property value="getCodeListDescById(getCbuUnitReportPojo().CordCfuPostMethod)"   />					
					</td>
					<td colspan="2"><s:text name="garuda.unitreport.label.testdate" /></td>
					<td ><s:date name="getCbuUnitReportPojo().cordCfuPostTestDate"
						id="cordCfuPostTestDate" format="MMM dd, yyyy" />
						<s:property  value="%{cordCfuPostTestDate}" />
					</td><td colspan="2"></td>	

				</tr>
		
		
				<tr>
					<td ><s:text
						name="garuda.unitreport.label.cbupostprocessingnrbc" /></td>
					<td colspan="4"><s:property value="getCbuUnitReportPojo().cordCbuPostProcessCount"  /></td><td colspan="2"></td>	
					<td colspan="2"><s:text
						name="garuda.unitreport.label.testdate" /></td>
					<td ><s:date name="getCbuUnitReportPojo().cordCbuPostTestDate"
						id="cordCbuPostTestDate" format="MMM dd, yyyy" />
						<s:property value="%{cordCbuPostTestDate}" /></td><td colspan="2"></td>
				</tr>
		
				<tr>
					<td ><s:text
						name="garuda.unitreport.label.bacterialculture" /></td>
					<td colspan="2"><s:property value="getCodeListDescById(getCbuUnitReportPojo().cordBacterialCul)"  /></td>
					<td colspan="2"><s:text
						name="garuda.unitreport.label.fungalculture" /></td>
					<td colspan="2"><s:property value="getCodeListDescById(getCbuUnitReportPojo().cordFungalCul)"  /></td><td colspan="2"></td>					
				</tr>
	
				<tr>
					<td ><s:text
						name="garuda.unitreport.label.hemoglobinopathyscr" /></td>
					<td ><s:property value="getCodeListDescById(cbuUnitReportPojo.cordHemogScreen)"  /></td>		
				</tr>
				<tr></tr>
	</table>
	
	<table >
		
			
				
					<b><s:text name="garuda.unitreport.label.hlainfo.subheading" /></b>
						<tr></tr>
						<tr>
							<td  ><s:text
								name="garuda.unitreport.question.hlatype" /></td>
							<td ><s:radio
								name="cordHlaExtPojo.cordHlaTwice" disabled="true"
								list="#{'1':'Yes','0':'No'}" /></td>
						</tr>


						<tr>
							<td ><s:text
								name="garuda.unitreport.question.hlasamples" /></td>
							<td ><s:radio
								name="cordHlaExtPojo.cordIndHlaSamples" disabled="true"
								list="#{'1':'Yes','0':'No'}" /></td>
						</tr>

						<tr>
							<td ><s:text
								name="garuda.unitreport.question.hlalab" /></td>
							<td ><s:radio
								name="cordHlaExtPojo.cordHlaSecondLab" disabled="true"
								list="#{'1':'Yes','0':'No'}" /></td>
						</tr>
						<tr>
							<td ><s:text
								name="garuda.unitreport.question.hlacontguoussegment" /></td>
							<td ><s:radio
								name="cordHlaExtPojo.cordHlaContiSeg" disabled="true"
								list="#{'1':'Yes','0':'No'}" /></td>
						</tr>
						<tr>
							<td ><s:text
								name="garuda.unitreport.question.hlarelease" /></td>
							<td ><s:radio
								name="cordHlaExtPojo.cordHlaContiSegBefRel" disabled="true"
								list="#{'1':'Yes','0':'No'}" /></td>


						</tr>
						<tr>
							<td ><s:text
								name="garuda.unitreport.question.hlabeforerelease" /></td>
							<td ><s:radio
								name="cordHlaExtPojo.cordHlaIndBefRel" disabled="true"
								list="#{'1':'Yes','0':'No'}" /></td>

						</tr>

						<tr>
							<td ><s:text
								name="garuda.unitreport.question.unitrequestedshipment" /></td>
							<td ><s:property value="getCodeListDescById(cordHlaExtPojo.cordHlaContiSegNum)"   /></td>

						</tr>
						
	</table>
						
	<table width="50%" >
						
						<b><s:text name="garuda.unitreport.label.processinginfo.subheading" /></b>
						<tr>
							<td><s:text name="garuda.unitreport.label.processingmethod" /></td>
							<td><s:property value="getCodeListDescById(cbuUnitReportPojo.cordProcMethod)"  />
									</td>
						</tr>
						
						<tr>

							<td><s:text name="garuda.unitreport.label.typeofbagused" /></td>
							<td><s:property value="getCodeListDescById(cbuUnitReportPojo.cordTypeBag)"  /></td>

						</tr>

						<tr>

							<td><s:text name="garuda.unitreport.label.productmodification" /></td>
							<td><s:property value="getCodeListDescById(cbuUnitReportPojo.cordProdModification)"  /></td>

						</tr>
						
						<tr>
							<td style="vertical-align: middle"><s:text
								name="garuda.unitreport.label.plsexplain" /></td>
							<td><s:property 
								value="cbuUnitReportPojo.cordExplainNotes"></s:property ></td>
						</tr>


						<tr>
							<td style="vertical-align: middle"><s:text
								name="garuda.unitreport.label.additionalnotes" /></td>
							<td><s:property 
								value="cbuUnitReportPojo.cordAddNotes"></s:property></td>
						</tr>
							
				
				
						<tr>
							<td ><s:text
							name="garuda.unitreport.label.completedby" /></td>
							<td ><s:property value="cbuUnitReportPojo.cordCompletedBy" /></td>
						</tr>
						
						<tr>
							<td ><s:text name="garuda.common.label.lstmodifiedon" /></td>
							<td ><s:date name="cdrCbuPojo.lastModifiedOn"
							id="lastModifiedOn" format="MMM dd, yyyy" />
							<s:property  value="%{lastModifiedOn}" />
							</td>
						</tr>
						<tr></tr>
	</table>
	
	

</s:form>
<div class="footerStyle" id="footer">

</div>
</td>
</tr>
</table>
</body>
</html>