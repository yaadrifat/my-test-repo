<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<script>
function updateElgStas(title,url,bredth,length){
//$j('#finalDeclQstns').html('');
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title=title+patient_data;
showModal(title,url,bredth,length);
}
$j(function(){
	if('<s:property value="CdrCbuPojo.eligibilitydes" />'=='Not Applicable'){
		if($j("#newQnairePF").length>0){
			$j("#newQnairePF").hide();
			$j("#summTxt").hide();
		}
	}
});
</script>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<s:set name="tempVal" value="cbuFinalReviewPojo.finalReviewConfirmStatus"></s:set>
<s:hidden name="eligModalFlag" id="eligModalFlag"/>
                                        <div id="eligNonEligData">
                                              <table id="finalDeclQstns">
													<tr>
												        <td colspan="2">
															<strong><s:text id="summTxt" name="garuda.common.label.summarize"/></strong>
												        </td>
												    </tr>
													<s:if test='#request.isTasksRequiredFlag == false '>
													<tr>
												        <td colspan="3">
												            <s:hidden name="declPojo.pkEligDecl"></s:hidden>
												            <jsp:include page="cord-entry/cb_view_final_decl_quest.jsp" >
												               <jsp:param value="true" name="readonly"/>
												            </jsp:include>
												        </td>
												    </tr>
												    </s:if>
												    <s:elseif test='#request.isTasksRequiredFlag == true '>
													    <tr>
													        <td colspan="2">
													        <div id="newQnairePF">
													        <s:if test='eligModalFlag=="Y"'>
													        <s:hidden name="declPojo.pkEligDecl"></s:hidden>
													            <jsp:include page="cb_new_final_decl_quest.jsp">
													            <jsp:param value="true" name="readonly"/>
													            
													            
												            </jsp:include>
												            </s:if>
												            <s:else>
												                <s:hidden name="declPojo.pkEligDecl"></s:hidden>
													            <jsp:include page="cb_view_new_final_decl_quest.jsp">
													            <jsp:param value="true" name="readonly"/>
													            
												            </jsp:include>
												            </s:else>
												            
												            </div>
													        </td>
													    </tr>
													    </s:elseif>
												</table>
                                              <table style="table-layout:fixed">
												    <tr>
												        <td>
												        <strong><s:text name="garuda.cdrcbuview.label.eligibility_determination" /></strong>:
												        </td>
												        <td>
												          <strong><s:property value="CdrCbuPojo.eligibilitydes" /></strong>
												        </td>
												        <td>
												     	<div id ="cbuAssmentEligibleDiv">
												          <s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBUASSESSMENT_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBUASSESSMENT_CODESUBTYPE].size > 0 && cdrCbuPojo.site.isCDRUser()==false">
												               <!--<s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]" var="rowVal" status="row">
												                   <s:if test="%{#rowVal[11]}==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@cbu_assessment)">
												                      <s:set name="cbuAssesmentCheck" value="%{'True'}" scope="request" />
												                   </s:if>
												                </s:iterator>
												                <s:if test="%{#cbuAssesmentCheck!='True'}">									                
																	<a href="#"><s:text	name="garuda.unitreport.label.eligibility.cbu_assessment" /></a>
																</s:if>
																-->
																<s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBUASSESSMENT_CODESUBTYPE]" var="rowVal" status="row">
																		<s:set name="cbuAssesmentCheck" value="%{'True'}" scope="request" />
																		<s:set name="docId" value="%{#rowVal[5]}" scope="request"/>																							 	
																</s:iterator>	
															     <s:if test="#request.cbuAssesmentCheck=='True'">
															         <a href="javascript:void(0);" onclick="window.open('getAttachmentFromDcms?docId=<s:property value="#request.docId"/>&signed=true','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><s:text name="garuda.unitreport.label.eligibility.cbu_assessment" /></a>
															     </s:if>    												
												           </s:if>
												           </div>												            
												        </td>												  
												    </tr>
													    <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.inEligiblePkid || cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId">
												        <tr>
													        <td valign="top">
													        <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.inEligiblePkid">
													           <strong><s:text name="garuda.cdrcbuview.label.ineligible_reason" /></strong>:
													        </s:if>
													        <s:elseif test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId">
													        	<strong><s:text name="garuda.cdrcbuview.label.incomplete_reason" /></strong>:
													        </s:elseif>
													        </td>
													        <td colspan="2">
													           <table width="100%" cellpadding="0" cellspacing="0">
														             <s:iterator value="#request.reasonPojos" >
														             <s:set name="reasonId" value="fkReasonId" />
														              <tr>
														                  <td>
														                     <strong><s:property value="getCodeListDescById(#reasonId)" /></strong>
														                  </td>
														              </tr>
															        </s:iterator>
													           </table>	           
													        </td>										        
													   </tr>
												</s:if>
												<s:elseif test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.notCollectedToPriorReasonId">
												<tr>
													        <td valign="top">
													        	<strong><s:text name="garuda.cdrcbuview.label.ineligible_reason_notes" /></strong>:
													       </td>
													        <td colspan="2" style="word-wrap: break-word;">
													           <s:property value="cordAdditionalInfo"/>	           
													        </td>										        
													   </tr>
												</s:elseif>	
										</table>
										<table>
										 <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
   										 </s:if>
   											 <s:else>
										    <tr>
										      <td align="right">
								               <s:if test="hasEditPermission(#request.updateElgblty)==true">
								                 <input type="button" name="eligibleEdit"  value="<s:text name="garuda.common.lable.edit"/>"
											     onclick="updateElgStas('<s:text name="garuda.cdrcbuview.label.eligibility_determination"/>  <s:text name="garuda.message.modal.headerregid"/> <s:property value="cdrCbuPojo.registryId" />','updateEligiblestatus?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CLINICAL_RECORD_ELIGIBILITY" />&licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal','390','900');">
											   </s:if>
											  </td>
											</tr>
											</s:else>
										</table> 
										<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE].size > 0">
													<div id="eligibleinfo1">
													<div id="searchTble1">							
														   <table>							    	  
															    <tr>
																     <td colspan="3">
																          <table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults4">
															                 <thead>
																				<tr>
																					<th class="th2" scope="col"><s:text
																						name="garuda.uploaddoc.label.date" /></th>
																					<th class="th3" scope="col"><s:text
																						name="garuda.openorder.label.description" /></th>
																					<th class="th5" scope="col"><s:text
																						name="garuda.uploaddoc.label.view" /></th>
																					<th><s:text name="garuda.common.lable.modify" /></th>
																					<th><s:text name="garuda.common.lable.revoke" /></th>
																				</tr>
																			</thead>
																			<tbody>															
																				<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]!=null">
																	                <s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]" var="rowVal" status="row">
																	                   <s:if test="%{#rowVal[12]}==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@FINAL_DEC_OF_ELIG_CODESUBTYPE)">
																	                      <tr>
																							 <td><s:date name="%{#rowVal[11]}" id="uploadDate"
																								format="MMM dd, yyyy" /><s:property value="%{#uploadDate}" /></td>
																							 <td><s:property value="%{#rowVal[14]}" /></td>
																							 <td><a href="#"><img src="images/attachment-icon.png"
																								onclick="javascript: return showDocument('<s:property value="%{#rowVal[6]}" />')" /></a></td>
																						 	 <td><button type="button" onclick="javascript:modifyDocument('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[7]}"/>','<s:property value="%{#rowVal[4]}"/>','<s:property value="%{#rowVal[6]}" />');"><s:text name="garuda.common.lable.modify" /></button></td>
								
																							 <td><button type="button" onclick="javascript: revokeDocument('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[7]}"/>','<s:property value="%{#rowVal[6]}" />')" ><s:text name="garuda.common.lable.revoke" /></button></td>
																						  </tr>
																	                   </s:if>
																	                </s:iterator>											                
																	            </s:if>
																			</tbody>
																			<tfoot>
																				<tr>
																					<td colspan="6"></td>
																				</tr>
																			</tfoot>
																		</table>
																     </td>
																</tr>
														</table>
														</div></div>
												</s:if>	 
                                  </div>
                                  
								
								<s:if test='%{#tempVal=="Y"}'>
								<s:if test='(cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.inEligiblePkid || cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId) && (cbuFinalReviewPojo.finalReviewConfirmStatus == "Y")'>
										<div id="dumnwidgetcontent" onclick="toggleDiv('dumnwidget')"
											class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
											style="text-align: center;"><span
											class="ui-icon ui-icon-triangle-1-s"></span> <s:text
											name="garuda.unitreport.label.eligibility.decl_urg_med_need" /></div>
										<div id="dumnwidget">
										 <table>
			                                 <tr>			                 
			                                    <td align="left"> 
			                                    <s:if test="cdrCbuPojo.site.isCDRUser()==true">                           
			                                       	<a href="#" style="cursor:pointer;" onclick="showPdfFormsWithMrqForSystem('<s:property value="cdrCbuPojo.CordID"/>','<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@PDF_DUMN"/>')"><s:text name="garuda.pdf.nmdp.viewdumn"/></a>
			                                      </s:if>
			                                      <s:else>
			                                      	<a href="#" style="cursor:pointer;" onclick="showPdfFormsWithMrq('<s:property value="cdrCbuPojo.CordID"/>','<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@PDF_DUMN"/>')"><s:text name="garuda.pdf.nmdp.viewdumn"/></a>
			                                      </s:else>	                       
			                                    </td>
			                                 </tr>
			                            </table>									 
										</div>
								</s:if>	
								</s:if>
								<s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.notCollectedToPriorReasonId && cbuFinalReviewPojo!=null && cbuFinalReviewPojo.eligibleConfirmFlag == 1">
								      <div id="notcbuassessmentcontent" onclick="toggleDiv('notcbuassessment')"
											class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
											style="text-align: center;"><span
											class="ui-icon ui-icon-triangle-1-s"></span> <s:text
											name="garuda.unitreport.label.eligibility.not_cbu_assessment" /></div>
									  <div id="notcbuassessment">
									    <table>
			                                 <tr>
			                                    <td align="left">
			                                        <a href="#" style="cursor:pointer;" onclick="showPdfForms('<s:property value="cdrCbuPojo.CordID"/>','<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@PDF_NOTIFI_CBU_ASSESS"/>')"><s:text name="garuda.pdf.nmdp.viewnotificbuassesment"/></a>
			                                    </td>
			                                 </tr>
			                            </table>									    
									  </div>
								</s:if> 
								
								<s:if test='%{#tempVal=="Y"}'>
									<table width="100%" cellspacing="0" cellpadding="0">
									    <tr>
									      <td>								         					
										       	<div id="finadeceligcontent" onclick="toggleDiv('finadecelig')"
												class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
												style="text-align: center;"><span
												class="ui-icon ui-icon-triangle-1-s"></span>
												<s:text name="garuda.cdrcbuview.label.final_decl_eligibility" /></div>
												<div id="finadecelig">
													<div >
							                            <table>
							                                 <tr>
							                                    <td align="left">
							                                        <a href="#" style="cursor:pointer;" onclick="showPdfForms('<s:property value="cdrCbuPojo.CordID"/>','<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@PDF_FDOE"/>')"><s:text name="garuda.pdf.nmdp.viewfdoe"/></a>
							                                    </td>
							                                 </tr>
							                            </table>
												        <table>
														 <tr>
															  <td colspan="3">
															         <table>	
																		   <tr>
																		        <td>
																			 		<s:set name="eligibleReviewName" value="%{cbuFinalReviewPojo.eligCreatedBy}" scope="request"/>
																			 		  <strong><s:text name="garuda.fdoe.level.completed"/></strong>
																			 		  <s:if test="#request.eligibleReviewName!=null && #request.eligibleReviewName!='' ">
																			 			<%
																							String finaldetails = request.getAttribute("eligibleReviewName").toString();
																			 			    UserJB user1 = new UserJB();
																							user1.setUserId(EJBUtil.stringToNum(finaldetails));
																							user1.getUserDetails();													
																						%>	
																					   <strong> <%=user1.getUserLastName()+" "+user1.getUserFirstName() %></strong>
																					</s:if>
																					<strong>
																					  <s:text name="garuda.cbufinalreview.label.on"/>  
																					  <s:date name="%{cbuFinalReviewPojo.eligCreatedOn}"	id="eligibleReviewDate" format="MMM dd, yyyy" /><s:property value="%{eligibleReviewDate}"/>
																				    </strong>
																			    </td>
																		   </tr>
																	</table>
															  </td>
														</tr>													
													</table>
												</div>	    						         
												<div>
												         <jsp:include page="cord-entry/cb_fdoe_section.jsp">
												            <jsp:param value="true" name="readonly"/>
												         </jsp:include>					 
												</div>
												</div>
									        </td>
									    </tr>
									  </table>
								</s:if>
									<s:if test='%{#tempVal=="Y"}'>
									<table width="100%" cellspacing="0" cellpadding="0">
									    <tr>
									      <td>
												<div id="prodtaglevelcontent" onclick="toggleDiv('prodtaglevel')"
												class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
												style="text-align: center;"><span
												class="ui-icon ui-icon-triangle-1-s"></span>
												<s:text name="garuda.cdrcbuview.label.product_tag_level" /></div>
												<div id="prodtaglevel">	
												    <jsp:include page="cord-entry/cb_product_tag.jsp">
												       <jsp:param value="true" name="readonly"/>
												    </jsp:include>
												</div>
									        </td>
									    </tr>
									  </table>
								</s:if>	
				<div id="eligibilitynotecontent" onclick="toggleDiv('eligibilitynote')"
					class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
					style="text-align: center;">
					<span class="ui-icon ui-icon-triangle-1-s"></span> 
					<s:text name="garuda.cdrcbuview.label.button.eligibilityclinicalnotes" />
				</div>				
				<div id="eligibilitynote" style=" width: 463px; height: 100%;">
				<div id="loadEligibilityClinicalNoteDiv">
				        <s:set name="cbuCateg" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CLINIC_CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBLE_NOTE_CATEGORY_CODESUBTYPE) " />
						<jsp:include page="modal/cb_load_clinicalNote.jsp"></jsp:include>
				</div>
			<div>
			<s:if test="hasNewPermission(#request.vClnclNote)==true">
				<s:if test='(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)) '>
				<button type="button" disabled="disabled"><s:text name="garuda.cdrcbuview.label.button.addnotes" /></button>
				</s:if>
				<s:else>
			   <button type="button"
				onclick="fn_showModalEligForNotes('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','clinicalNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cat=ELIGIBLE_NOTE_CATEGORY_CODESUBTYPE','550','850','notesModalsDiv');"><s:text
				name="garuda.cdrcbuview.label.button.addnotes" /></button>
				</s:else>
			</s:if>
			</div>
			</div>
			
						<!--<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults8" >
							<thead>
										<tr>
											<th><s:text name="garuda.common.lable.date" /></th>
											<th><s:text name="garuda.cdrcbuview.label.user_name" /></th>
											<th><s:text name="garuda.cdrcbuview.label.eligible_determin" /></th>
											<th><s:text name="garuda.cdrcbuview.label.eligible_reason" /></th>
										</tr>
									</thead>
									<tbody>
										<s:iterator value="lstEligibility" var="rowVal" status="row">
										<tr>
											<s:iterator value="rowVal" var="cellValue" status="col" >
												<s:if test="%{#col.index == 0}">
													<s:date name="%{#cellValue}"
													id="statusDate" format="MMM dd, yyyy" />
													<td><s:property value="%{statusDate}"/></td>
												</s:if>
												<s:if test="%{#col.index == 1}">
												 <s:set name="cellValue1" value="cellValue" scope="request"/>
													<td>
													<%--
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName()--%>										
													</td>
												</s:if>
												<s:if test="%{#col.index == 2}">																							
												<td>
												  <s:property value="getCodeListDescById(#cellValue)" />
												</td>
												</s:if>
												<s:if test="%{#col.index == 3}">
													<td><s:iterator value="getMultipleIneligibleReasons(#cellValue)" var="colVal" status="val"><s:property value="getCodeListDescById(#colVal)" /><br></s:iterator></td>
												</s:if>
											</s:iterator>
										</tr>
										</s:iterator>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="4"></td>
										</tr>
									</tfoot>
			</table>

				-->
				<div id="loadEligibilityDocInfoDiv">
				<s:if test="(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE].size > 0 && hasViewPermission(#request.viewEligbCat)==true)">
				<div id="loadEligibilityDocInfocontent" onclick="toggleDiv('loadEligibilityDocInfo')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;">
				<span class="ui-icon ui-icon-triangle-1-s"></span> 
				<s:text name="garuda.uploaddoc.label.button.eligibilityuploaddocument" />
				</div>
					<div id="loadEligibilityDocInfo">					
					      <s:set name="cbuDocCateg" value="@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE" scope="request" />
					      <div id="loadEligbilityRevokDoc">
							  <jsp:include page="modal/cb_load_upload_document.jsp">
								  <jsp:param value="<%=request.getAttribute(\"cbuDocCateg\")%>" name="cbuDocCateg"/>
								  <jsp:param value="loadEligbilityRevokDoc" name="replaceDivId"/>
							  </jsp:include>
						  </div>
                 
                   </div>
                 </s:if>
                 </div>
                 <script>
                 function fn_showModalEligForNotes(title,url,hight,width,id){
                	    var patientId = $j('#patientId').val();
						var patient_data = '';
						if(patientId!="" && patientId != null && patientId != "undefined"){
							patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
						}
						title=title+patient_data;
                		showModals(title,url,hight,width,id);
                		}
                 </script>