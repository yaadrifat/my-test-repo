<%@ taglib prefix="s"  uri="/struts-tags"%>
<script>
function highlight(id){
   $j('#'+id).addClass('high');
}

function removeHighlight(id){
	$j('#'+id).removeClass('high');	
}

 function showsearchTble(){
	 	document.getElementById('searchTble').setAttribute("style","display:true");
 }
 
 function beforecdrcbuscreen(registryId, cbbname, elegdeter, cdrcbuId){
		showCdrCbuScreen(registryId, cbbname, elegdeter, cdrcbuId);
	}
	
 function showCdrCbuScreen(formname,cordID){
		$j('#cordID').val(cordID);
		submitform(formname);
 }
 
 function constructTable() {
			 $j('#searchResults').dataTable();
		}

 function showDiv()
 {
			$j('#searchresultparent').css('display','block');
			refreshDiv('getNewCbuDetails','searchTble','cbuentry');
 }

</script>
<div class="col_100 maincontainer "><div class="col_100">
<div class="column">



		<div class="portlet" id="cdrsearchparent">
		<div id="cdrsearch" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span><!--<span class="ui-icon ui-icon-close" ></span>--><s:text name="garuda.cbuentry.label.cdrcbusearch" /></div><div class="portlet-content">
		<div id="studyDashDiv1" >   
		        <form id="cbuentry">
		         <table cellpadding="0" cellspacing="0" border="0"
				style="width: 100%; height: auto;" class="col_100">         
		         	<tr>
					   <td ><s:text name="garuda.cbuentry.label.cbuid" />:</td>
					   <td ><s:textfield  name="cdrCbuPojo.cdrCbuId" placeholder="CBU ID" onkeypress="return enter2Refreshdiv(event);"/></td> 
					    <td><button type="button" onclick="showDiv();"><s:text name="garuda.uploaddoc.label.search"/></button> </td>
					</tr>				
				</table>
		        </form>	
			</div></div></div></div>

	</div>

	<div class="column">		
		<div class="portlet" id="searchresultparent" style="display: none;"><div id="searchresult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span><!--<span class="ui-icon ui-icon-close" ></span>--><s:text name="garuda.cbuentry.label.cdrcbusearchresult" /></div><div class="portlet-content">
		<s:form action="cdrcbuView" id="search" name="search" method="POST" >
		<s:hidden name="cdrCbuPojo.cordID" id="cordID"></s:hidden>
		<div id="searchTble">
		
		<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults" >
			<thead>
				<tr>
					<th><s:text name="garuda.cbuentry.label.registrycbuid" /></th>
					<th><s:text name="garuda.cbuentry.label.CBB" /></th>
					<th><s:text name="garuda.cbuentry.label.EligibilityDetermination" /></th>
					<th><s:text name="garuda.cbuentry.label.status" /></th>
					<th><s:text name="garuda.cbuentry.label.active4patiests" /></th>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="lstObject" var="rowVal" status="row">
					<tr id="<s:property	value="%{#row.index}"/>" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')" onclick="javaScript:return showCdrCbuScreen('search','<s:property value="%{#rowVal[0]}" />')" >
						<td><s:property
							value="%{#rowVal[1]}" /></td>
		
						<td><s:property
							value="%{#rowVal[6]}" /></td>
		
						<td><s:property
							value="%{#rowVal[17]}" /></td>
							
						<td><s:property 
							value="%{#rowVal[18]}" /></td>
							
						<td><s:property 
							value="%{#rowVal[19]}" /> </td>
		
					</tr>
				</s:iterator>
			</tbody>
		
			<tfoot>
				<tr>
					<td colspan="3"></td>
				</tr>
			</tfoot>
		
		</table>
		</div></s:form></div>
</div></div>
</div>
</div></div>