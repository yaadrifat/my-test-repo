<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<%
String contextpath=request.getContextPath();
%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.ordercomponent.helper.UserAlertsHelper" %>
<script>
var cotextpath = "<%=contextpath%>";
</script>
<%

	String GARUDA_CBU = "CBU";
	String GARUDA_PRODUCTFULLFILLMENT = "PRODUCTFULLFILLMENT";
	String GARUDA_ADMIN = "ADMIN";
	UserJB userInfo = (UserJB)session.getAttribute("currentUser");
	String skin = "";
	
%>

<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<%@ page language="java" import="java.util.*,com.velos.eres.web.user.UserJB, com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	int patientRights = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
	int studyRights = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	UserJB userB1 = (UserJB) tSession.getValue("currentUser");
	String accName = (String) tSession.getValue("accName");
	String accId = (String) tSession.getValue("accountId");
	accName=(accName==null)?"default":accName;
	String accSkinId = "";
	String usersSkinId = userB1.getUserSkin();
	String userSkin = "";
	String accSkin = (String) tSession.getValue("accSkin");
	skin = "default";
	userSkin= codeLst.getCodeCustomCol(StringUtil.stringToNum(usersSkinId) );
	accSkin= codeLst.getCodeCustomCol(StringUtil.stringToNum(accSkinId) );

	if (accSkin == null || accSkin.equals("") || accSkin.equals("default") ){
		accSkin = "accSkin";
	} else {
		skin = accSkin;
	}

	if (userSkin != null && !userSkin.equals("") ){
		skin = userSkin;
	}
}
%>
 
<!-- Garuda Common CSS -->
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/velosCDR.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/<%=skin%>/Garuda_skin.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/general.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/portlet.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/jquery.alerts.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/pending-order.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/jquery.time.slider.css" media="screen"/>

<!-- Common CSS -->
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/forms.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/clickmenu.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/styles.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/demo_table.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/jquery.treeview.css" type="text/css" />


<%@page import="com.velos.ordercomponent.business.pojoobjects.CBBPojo"%>
<html>
<head>
<title><s:text name="garuda.common.footer.velos"/></title>

<script>
  // $j( "input:submit, button", ".column" ).button();		 
   
   function getdata(){
		
	     window.print(); 
	     window.close(); 
		
	}
</script>
</head>
<style>
@media print
{
tbody {page-break-after:always}
}
</style>
<body onload="getdata();">
<s:form cssClass="maincontainer">
				  
 <table width="100%" cellspacing="0" cellpadding="0" border="0" id="status">
  <tr>
			    <td><div class="headerStyle row" id="header">
				<table width="100%">
				<tr align="right">
				    <td><button type="button" onClick="window.close()"><s:text name="garuda.common.close"/></button> </td>
				</tr></table>
				</div>
	</tr>
	
	
	
	
	
	
	<tr>			
	<td>	
	      <s:if test="hasViewPermission(#request.viewCbbSetup)==true">
		    <fieldset>
					<legend><s:text name="garuda.cbbdefaults.lable.cbbsetup" /></legend>
						  <table cellpadding="0" cellspacing="0" width="100%" align="left">
								<tr>
									<td>
										<s:text name="garuda.pendingorderpage.label.cddid" />:     				
						   			</td>
									<td >
										<s:textfield name="site.siteIdentifier" readonly="true" onkeydown="cancelBack();"  />
									</td>		
									<td>
										<s:text name="garuda.cbbdefaults.lable.cbbregistryname" />:     				
						   			</td>
									<td >
										<s:textfield name="site.siteName" readonly="true" onkeydown="cancelBack();"  />
									</td>
									
						        </tr>
						        <tr>
						        	<td>
						   				<s:text name="garuda.cbbdefaults.lable.cbbsystemuser" />:
									</td>
						        	<td>
						        		<strong><s:if test="cbbPojo.cdrUser==true"><s:text name="garuda.opentask.label.yes"/></s:if><s:elseif test="cbbPojo.cdrUser==false"><s:text name="garuda.clinicalnote.label.no"/></s:elseif></strong>
						        	</td>
						            <td>
						   				<s:text name="garuda.cbbdefaults.lable.bmdwcode" />:
									</td>
						        	<td>
						        		<s:textfield name="cbbPojo.bmdw" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
						        		
						        	</td>
						        	
						        </tr>		
						         <tr>
						         	<td>
						   				<s:text name="garuda.cbbdefaults.lable.emdis" />:
									</td>
						        	<td>
						        		<s:textfield name="cbbPojo.emdis" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
						        	</td>
						            <td>
						   				<s:text name="garuda.cbbdefaults.lable.membershiptype" />:
									</td>
						        	<td>
						        		<!--<strong><s:if test="cbbPojo.allowMember==true">Yes</s:if><s:elseif test="cbbPojo.allowMember==false">No</s:elseif></strong>
						        	--><s:select headerKey="9" headerValue=" " disabled="true"
														list="#{'1':'Member', '0':'Non Member', '2':'Participating', '3':'Other'}" 
														name="cbbPojo.allowMember" 	 />
									</td>
						        	
						        </tr>
						        <tr>
						            <td>
						   				<s:text name="garuda.cbbdefaults.lable.cbuassessmentbutton" />:
									</td>
						        	<td>
						        		<strong><s:if test="cbbPojo.cbuAssessment==true"><s:text name="garuda.opentask.label.yes"/></s:if><s:elseif test="cbbPojo.cbuAssessment==false"><s:text name="garuda.clinicalnote.label.no"/></s:elseif></strong>
						        	</td>
						        	<td>
						   				<s:text name="garuda.cbbdefaults.lable.unitreportbutton" />:
									</td>
						        	<td>
						        		<strong><s:if test="cbbPojo.unitReport==true"><s:text name="garuda.opentask.label.yes"/></s:if><s:elseif test="cbbPojo.unitReport==false"><s:text name="garuda.clinicalnote.label.no"/></s:elseif></strong>
						        	</td>
						        </tr>
							     <tr>
								         	<td>
								   				<s:text name="garuda.cbbdefaults.lable.researchsample" />:
											</td>
								        	<td>
								        		<strong><s:if test="cbbPojo.nmdpResearch==true"><s:text name="garuda.opentask.label.yes"/></s:if><s:elseif test="cbbPojo.nmdpResearch==false"><s:text name="garuda.clinicalnote.label.no"/></s:elseif></strong>
								        	</td>
									          <td><s:text name="garuda.cbbdefaults.lable.domestic"></s:text>:</td>
									          <td><strong><s:if test="cbbPojo.isDomesticCBB==true"><s:text name="garuda.opentask.label.yes"/></s:if><s:elseif test="cbbPojo.isDomesticCBB==false"><s:text name="garuda.clinicalnote.label.no"/></s:elseif></strong></td>
								        					        	
								        </tr>   		                                              			          
						        </table>
						   </fieldset>
						   </s:if>
						   </td>
						   </tr>
	
	<tr>			
	<td>		    
	     <s:if test="hasViewPermission(#request.veiwCbbInfo)==true">
	          <fieldset>
					<legend><s:text name="garuda.cbbdefaults.lable.cbbinformation" /></legend>
						  <table cellpadding="0" cellspacing="0" width="100%" align="left">
								<tr>
									<!--<td>
										<s:text name="garuda.cbbdefaults.lable.cbbid" />:     				
						   			</td>
									<td >
										<s:textfield name="site.siteIdentifier" readonly="true" onkeydown="cancelBack();"  />
									</td>		
									--><td>
										<s:text name="garuda.cbbdefaults.lable.cbbname" />:     				
						   			</td>
									<td >
										<s:textfield name="site.siteName" readonly="true" onkeydown="cancelBack();"  />
									</td>
									<td colspan = "2">
						        	&nbsp;
						        	</td>
									
						        </tr><!--
						        <tr>
						        	<td colspan = "2">
						        	&nbsp;
						        	</td>					        	
						        	
						        	<td>
										<s:text name="garuda.cbbdefaults.lable.OrgType" />:     				
						   			</td>
									<td >
										<s:textfield name="cbbType" readonly="true" onkeydown="cancelBack();" />
						        
						        </td>
						        </tr>
						        
						        
						       --><tr>
						        	<td>
						   				<s:text name="garuda.cbbdefaults.lable.coordinatorlastname" />:
									</td>
						        	<td>
						        		<input type="text" name="lastName" value="<s:property value="cbbPojo.person.personLname" />"  readonly="readonly"/>
						        	</td>
						            <td>
						   				<s:text name="garuda.cbbdefaults.lable.coordinatorfirstname" />:
									</td>
						        	<td>
						        		<input type="text" name="firstName" value="<s:property value="cbbPojo.person.personFname"/>"  readonly="readonly"/>
						        	</td>
						        	
						        </tr>		
						         <tr>
						         	<td>
						   				<s:text name="garuda.cbbdefaults.lable.phone" />:
									</td>
						        	<td>
						        		<s:textfield name="site.address.phone" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
						        	</td>
						            <td>
						   				<s:text name="garuda.cbbdefaults.lable.phoneext" />:
									</td>
						        	<td>
						        		<s:textfield name="site.address.ext" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
						        	</td>
						        	
						        </tr>
						        <tr>
						            <td>
						   				<s:text name="garuda.cbbdefaults.lable.fax" />:
									</td>
						        	<td>
						        		<s:textfield name="site.address.fax" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
						        	</td>
						        	<td>
						   				<s:text name="garuda.cbbdefaults.lable.email" />:
									</td>
						        	<td>
						        		<s:textfield name="site.address.email" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
						        	</td>
						        </tr>
							        <jsp:include page="cb_address.jsp" flush="true">
							           <jsp:param value="site.address" name="prefix"/>
							        </jsp:include>			                                              			          
						        </table>
						   </fieldset>
						   </s:if>
						   </td>
						   </tr>
  			
					<tr >
<td>
    <s:if test="hasViewPermission(#request.viewCbbpick)==true">
	      <fieldset>
					<legend><s:text name="garuda.cbbdefaults.lable.cbuPickupAddress" /></legend>
					 <table cellpadding="0" cellspacing="0" width="100%" align="left">
							<!--<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.address.contact"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.attention" readonly="true" onkeydown="cancelBack();"  /></td>
								      	  <td colspan="2"></td>
							</tr>
							
							
								--><tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.address.contact.lastName"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.pickUpAddLastName" readonly="true" onkeydown="cancelBack();"  /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.address.contact.firstName"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.pickUpAddFirstName" readonly="true" onkeydown="cancelBack();"  /></td>
							</tr>	
							
							
							<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.contact.phone" ></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.address.phone"  readonly="true" onkeydown="cancelBack();"  /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.contact.email"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.address.email"  readonly="true" onkeydown="cancelBack();" /></td>
							</tr>	      
								<jsp:include page="cb_address.jsp" flush="true">
						           <jsp:param value="cbbPojo.address" name="prefix"/>
						           <jsp:param value="Yes" name="hideFields"/>
						        </jsp:include>
						        
						        <tr>
						        		<td width="30%"><s:text name="garuda.common.label.lstmodifiedon"></s:text>:</td>
								          <td width="20%">
								         <s:date name="cbbPojo.address.lastModifiedOn" id="pickUpAddLstModifdDT" format="MMM dd, yyyy / HH:mm" />
												<s:textfield name="pickUpAddLstModifdDT" readonly="true" onkeydown="cancelBack();"  />
								          </td>
								        	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.lastmodifiedby"></s:text>:</td>
								        	 <td width="20%">
												<s:if test="cbbPojo.address.lastModifiedBy!=null">
												<s:set name="cellValuePickup" value="cbbPojo.address.lastModifiedBy" scope="request"/>	
													<% 
													if(request.getAttribute("cellValuePickup")!=null)
													{
														String cellValuePickup = request.getAttribute("cellValuePickup").toString();
														UserJB userB = new UserJB();
														UserAlertsHelper userHelp = new UserAlertsHelper();
														userB.setUserId(EJBUtil.stringToNum(cellValuePickup));
														userB.getUserDetails();													
															//String userNamePickup = userB.getUserLastName()+" "+userB.getUserFirstName();
															String userNamePickup = userHelp.getLogNameByUserId(new Long (EJBUtil.stringToNum(cellValuePickup)));
																request.setAttribute("userNamePickupRd",userNamePickup);
													}		%>									
													</s:if>
													<s:else>
														  <s:set name="cellValuePickup" value="cbbPojo.address.createdBy" scope="request"/>	
													<% 
													if(request.getAttribute("cellValuePickup")!=null)
													{
														String cellValuePickup = request.getAttribute("cellValuePickup").toString();
														UserJB userB = new UserJB();
														UserAlertsHelper userHelp = new UserAlertsHelper();
														userB.setUserId(EJBUtil.stringToNum(cellValuePickup));
														userB.getUserDetails();													
															//String userNamePickup = userB.getUserLastName()+" "+userB.getUserFirstName();
															String userNamePickup = userHelp.getLogNameByUserId(new Long (EJBUtil.stringToNum(cellValuePickup)));
																request.setAttribute("userNamePickupRd",userNamePickup);
													}		%>		
														  
													</s:else>																									
												<s:textfield name="#request.userNamePickupRd" readonly="true" onkeydown="cancelBack();"  />
											</td>
								         
							</tr><!--
							
							 <tr>
								          <td width="30%"><s:text name="garuda.cbuentry.label.cdrcbucreby"></s:text>:</td>
								         
								          <td width="20%">
												<s:if test="cbbPojo.address.createdBy!=null">
												<s:set name="cellValuePickAddCreatedBy" value="cbbPojo.address.createdBy" scope="request"/>	
													<% 
														String cellValuePickAddCreatedBy = request.getAttribute("cellValuePickAddCreatedBy").toString();
														UserJB userBB = new UserJB();
														userBB.setUserId(EJBUtil.stringToNum(cellValuePickAddCreatedBy));
														userBB.getUserDetails();													
															%>	
															<%String userNamePickAddCreatedBy = userBB.getUserLastName()+" "+userBB.getUserFirstName();
																request.setAttribute("userNamePickAddCreatedByRd",userNamePickAddCreatedBy);
															%>									
													</s:if>
												<s:textfield name="#request.userNamePickAddCreatedByRd" readonly="true" onkeydown="cancelBack();"  />
											</td>	          
								          
								          
								          
								      	  <td width="30%"><s:text name="garuda.cbuentry.label.cdrcbucreon"></s:text>:</td>
								         					          
								            <td width="20%">
								          <s:date name="cbbPojo.address.createdOn" id="PickUpCreatedDT" format="MMM dd, yyyy / HH:MM" />
												<s:textfield name="PickUpCreatedDT" readonly="true" onkeydown="cancelBack();"  />
								          </td>
							</tr>			
							--><tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.address.special.instruction"></s:text>:</td>
								          <td width="20%"><s:textarea name="cbbPojo.pickUpAddSplInstuction" cols="25" rows="3" readonly="true" onkeydown="cancelBack();"  /></td>
								      	  <td colspan="2"></td>
								      	  
								      	  
							</tr>	 
								       
									</table>
    </fieldset>
    </s:if>
    </td>
    </tr>
    <tr >
<td>
    <s:if test="hasViewPermission(#request.viewDryShip)==true">
	       <fieldset>
					<legend><s:text name="garuda.cbbdefaults.lable.dryShipperReturnAddtess" /></legend>
					 <table cellpadding="0" cellspacing="0" width="100%" align="left">
							<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.attentionorg"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.attentiondDryShipper" readonly="true" onkeydown="cancelBack();"  /></td>
								      	  <td colspan="2"></td>
							</tr>
								      
								<jsp:include page="cb_address.jsp" flush="true">
						           <jsp:param value="cbbPojo.addressDryShipper" name="prefix"/>
						           <jsp:param value="Yes" name="hideFields"/>
						        </jsp:include>
						        
						        
						         <tr>
								         
								            <td width="30%"><s:text name="garuda.common.label.lstmodifiedon"></s:text>:</td>
								          <td width="20%">
								          <s:date name="cbbPojo.addressDryShipper.lastModifiedOn" id="dryShipperLstModifdDT" format="MMM dd, yyyy / HH:mm" />
												<s:textfield name="dryShipperLstModifdDT" readonly="true" onkeydown="cancelBack();"  />
								          </td>
								           <td width="30%"><s:text name="garuda.cbbdefaults.lable.lastmodifiedby"></s:text>:</td>
								          <td width="20%">
												<s:if test="cbbPojo.addressDryShipper.lastModifiedBy!=null">
												<s:set name="cellValueDryShipper" value="cbbPojo.addressDryShipper.lastModifiedBy" scope="request"/>	
													<% 
													if(request.getAttribute("cellValueDryShipper")!=null)
													{
														String cellValueDryShipper = request.getAttribute("cellValueDryShipper").toString();
														UserJB userB = new UserJB();
														UserAlertsHelper userHelp = new UserAlertsHelper();
														userB.setUserId(EJBUtil.stringToNum(cellValueDryShipper));
														userB.getUserDetails();													
														//String userNameDryshipper = userB.getUserLastName()+" "+userB.getUserFirstName();
															String userNameDryshipper = userHelp.getLogNameByUserId(new Long (EJBUtil.stringToNum(cellValueDryShipper)));
																request.setAttribute("userNameDryshipperRd",userNameDryshipper);
													}		%>									
													</s:if>
													<s:else>
														  <s:set name="cellValueDryShipper" value="cbbPojo.addressDryShipper.createdBy" scope="request"/>	
													<% 
													if(request.getAttribute("cellValueDryShipper")!=null)
													{
														String cellValueDryShipper = request.getAttribute("cellValueDryShipper").toString();
														UserJB userB = new UserJB();
														UserAlertsHelper userHelp = new UserAlertsHelper();
														userB.setUserId(EJBUtil.stringToNum(cellValueDryShipper));
														userB.getUserDetails();													
															
															//String userNameDryshipper = userB.getUserLastName()+" "+userB.getUserFirstName();
															String userNameDryshipper = userHelp.getLogNameByUserId(new Long (EJBUtil.stringToNum(cellValueDryShipper)));
																request.setAttribute("userNameDryshipperRd",userNameDryshipper);
													}		%>		
														  
													</s:else>
												<s:textfield name="#request.userNameDryshipperRd" readonly="true" onkeydown="cancelBack();"  />
											</td>	          
							</tr><!--	
														
							 <tr>
								          <td width="30%"><s:text name="garuda.cbuentry.label.cdrcbucreby"></s:text>:</td>
								         
								          <td width="20%">
												<s:if test="cbbPojo.addressDryShipper.createdBy!=null">
												<s:set name="cellValueDryShipperCreatedBy" value="cbbPojo.addressDryShipper.createdBy" scope="request"/>	
													<% 
														String cellValueDryShipperCreatedBy = request.getAttribute("cellValueDryShipperCreatedBy").toString();
														UserJB userBB = new UserJB();
														userBB.setUserId(EJBUtil.stringToNum(cellValueDryShipperCreatedBy));
														userBB.getUserDetails();													
															%>	
															<%String userNameDryshipperCreatedBy = userBB.getUserLastName()+" "+userBB.getUserFirstName();
																request.setAttribute("userNameDryshipperCreatedByRd",userNameDryshipperCreatedBy);
															%>									
													</s:if>
												<s:textfield name="#request.userNameDryshipperCreatedByRd" readonly="true" onkeydown="cancelBack();"  />
											</td>	          
								          
								          
								          
								      	  <td width="30%"><s:text name="garuda.cbuentry.label.cdrcbucreon"></s:text>:</td>
								         					          
								            <td width="20%">
								          <s:date name="cbbPojo.addressDryShipper.createdOn" id="dryShipperCreatedDT" format="MMM dd, yyyy / HH:MM" />
												<s:textfield name="dryShipperCreatedDT" readonly="true" onkeydown="cancelBack();"  />
								          </td>
							</tr>			
															       
									--></table>
    </fieldset>
    </s:if>
    </td>
    </tr>    
    <tr>
  <td>
      <s:if test="hasViewPermission(#request.ViewCbbDeflt)==true">
          <fieldset>
					<legend><s:text name="garuda.cbbdefaults.lable.cbbdefaults" /></legend>
			  <table cellpadding="0" cellspacing="0" width="100%" align="left">
			  
			  								<tr>
								        		<td><s:text name="garuda.cbudefaults.label.storage.location"></s:text>:</td>
								          	  <td ><s:select name="fkCbuStorage" disabled="true" list="cbuStrLocationList" headerKey="-1" headerValue=""  listKey="siteId"  listValue="siteName"></s:select>
								         	 </td >
								         	 <td ><s:text name="garuda.cbudefaults.label.collection.site"></s:text>:</td>
								         	 <td ><s:select name="fkCbuCollection" disabled="true" list="cbuCollectionList" headerKey="-1" headerValue="" listKey="siteId"  listValue="siteName"></s:select>
								          </td>
								       </tr>
								       <tr>								      
											 <!--<td><s:text name="garuda.cbuentry.label.idbag"></s:text>:</td>
											 <td> 				 
											 <s:set name="idbagCount" value="%{cbuOnBag.length}" scope="request" />
												   <s:iterator value="#request.cbuOnBag" var="idbagv" status="stat">
												        <s:set name="rPkCodeId1" value="%{#idbagv}" scope="request" />
												        <s:property value="getCodeListDescByPk(#request.rPkCodeId1)"/>
												         <s:if test="#stat.index < #request.idbagCount-1"> ,</s:if>
											   	  </s:iterator>	  
											 </td>
											-->
											<td ><s:text name="garuda.cbbdefaults.lable.defaultCTShipper"></s:text>:</td>
								            <td ><s:select name="cbbPojo.defaultCTShipper" headerKey="" headerValue=""list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPER]" listKey="pkCodeId" listValue="description" disabled="true"></s:select> </td>
											<td ><s:text name="garuda.cbbdefaults.lable.defaultORShipper"></s:text>:</td>
								            <td ><s:select name="cbbPojo.defaultORShipper"  headerValue="" headerKey="" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPER]" listKey="pkCodeId" listValue="description" disabled="true"></s:select></td>
										</tr><!--			  							
								       <tr>
								          <td><s:text name="garuda.cbbdefaults.lable.enableeditanitgens"></s:text>:</td>
								          <td><strong><s:if test="cbbPojo.enableEditAntigens==true">Yes</s:if><s:elseif test="cbbPojo.enableEditAntigens==false">No</s:elseif></strong>
								          </td>
								          <td><s:text name="garuda.cbbdefaults.lable.displayMaternalAntigens"></s:text>:</td>
								          <td><strong><s:if test="cbbPojo.displayMaternalAntigens==true">Yes</s:if><s:elseif test="cbbPojo.displayMaternalAntigens==false">No</s:elseif></strong>
								          </td>
								       </tr>
								       <tr>
				          					<td width="30%"><s:text name="garuda.cbbdefaults.lable.enableautocompletecordentry"></s:text>:</td>
				          					<td width="20%"><strong><s:if test="cbbPojo.enableAutoCompleteCordEntry==true">Yes</s:if><s:elseif test="cbbPojo.enableAutoCompleteCordEntry==false">No</s:elseif></strong>
				          					</td>
				          					<td width="20%"><s:text name="garuda.cbbdefaults.lable.enableautocompleteackrestask"></s:text>:</td>
				          					<td width="30%"><strong><s:if test="cbbPojo.enableAutoCompleteAckResTask==true">Yes</s:if><s:elseif test="cbbPojo.enableAutoCompleteAckResTask==false">No</s:elseif></strong>
				          					</td>
				          					
				       					</tr>
								       <tr>
								          <td><s:text name="garuda.cbbdefaults.lable.enableCbuInventFeature"></s:text>:</td>
								          <td><strong><s:if test="cbbPojo.enableCbuInventFeature==true">Yes</s:if><s:elseif test="cbbPojo.enableCbuInventFeature==false">No</s:elseif></strong>
								          </td>								          
								         </tr>
								       --><tr>
								          <!--<td><s:text name="garuda.cbbdefaults.lable.defaultMicroContamDateToProcDate"></s:text>:</td>
								          <td><strong><s:if test="cbbPojo.defaultMicroContamDateToProcDate==true">Yes</s:if><s:elseif test="cbbPojo.defaultMicroContamDateToProcDate==false">No</s:elseif></strong>
								          </td>
								          -->
								         
								          <td width="20%"><s:text name="garuda.cbbdefaults.lable.defaultShippingPaperWorkLoc"></s:text>:</td>
				       	 				  <td width="30%"><s:select name="cbbPojo.fkDefaultShippingPaperWorkLocation" headerValue="" headerKey="" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DEFAULT_SHIPPING_PAPER_WRK_LOC]" disabled="true" listKey="pkCodeId" listValue="description"></s:select></td>
								          <td width="20%"><s:text name="garuda.cbbdefaults.lable.accreditation"></s:text>:</td>
						 				  <td width="30%">						 					
						 					<s:set name="accreCount" value="%{fkAccreditation.length}" scope="request" />
											   <s:iterator value="#request.fkAccreditation" var="accv" status="stat">
											        <s:set name="rPkCodeId" value="%{#accv}" scope="request" />
											        <s:property value="getCodeListDescByPk(#request.rPkCodeId)"/>
											         <s:if test="#stat.index < #request.accreCount-1"> ,</s:if>
										   	  </s:iterator>	    
						 				  </td>
								       </tr><!--
								       <tr>
								          <td><s:text name="garuda.cbbdefaults.lable.defaultCfuTestDateToProcDate"></s:text>:</td>
								          <td><strong><s:if test="cbbPojo.defaultCfuTestDateToProcDate==true">Yes</s:if><s:elseif test="cbbPojo.defaultCfuTestDateToProcDate==false">No</s:elseif></strong>
								          </td>
								          <td><s:text name="garuda.cbbdefaults.lable.defaultViabTestDateToProcDate"></s:text>:</td>
								          <td><strong><s:if test="cbbPojo.defaultViabTestDateToProcDate==true">Yes</s:if><s:elseif test="cbbPojo.defaultViabTestDateToProcDate==false">No</s:elseif></strong>
								          </td>
								       </tr>
								       <tr>
								          <td><s:text name="garuda.cbbdefaults.lable.EnableCordInfoLock"></s:text>:</td>
								          <td><strong><s:if test="cbbPojo.cordInfoLock==true">Yes</s:if><s:elseif test="cbbPojo.cordInfoLock==false">No</s:elseif></strong>
								          </td>
								          
						 				</tr>
								       <tr>
								          <td><s:text name="garuda.cbbdefaults.lable.allowmember"></s:text>:</td>
								          <td><strong><s:if test="cbbPojo.allowMember==true">Yes</s:if><s:elseif test="cbbPojo.allowMember==false">No</s:elseif></strong></td>
								          <td><s:text name="garuda.cbbdefaults.lable.usingcdr"></s:text>:</td>
								          <td><strong><s:if test="cbbPojo.cdrUser==true">Yes</s:if><s:elseif test="cbbPojo.cdrUser==false">No</s:elseif></strong></td>
								       </tr>
								       <tr>
								          <td><s:text name="garuda.cbbdefaults.lable.domestic"></s:text>:</td>
								          <td><strong><s:if test="cbbPojo.isDomesticCBB==true">Yes</s:if><s:elseif test="cbbPojo.isDomesticCBB==false">No</s:elseif></strong></td>
								          <td><s:text name="garuda.cbbdefaults.lable.useowndumn"></s:text>:</td>
								          <td><strong><s:if test="cbbPojo.useOwnDumn==true">Yes</s:if><s:elseif test="cbbPojo.useOwnDumn==false">No</s:elseif></strong></td>
								       </tr>
								        <tr>
								          <td><s:text name="garuda.cbbdefaults.lable.fdoe"></s:text>:</td>
								          <td><strong><s:if test="cbbPojo.useFdoe==true">Yes</s:if><s:elseif test="cbbPojo.useFdoe==false">No</s:elseif></strong></td>
								          <td colspan="2"></td>
								        </tr>
								        
								          <tr>
								          <td>&nbsp;</td>
								          <td>&nbsp;</td>								          
								          <td width="20%"><s:text name="garuda.cbbdefaults.lable.defaultShippingPaperWorkLoc"></s:text>:</td>
				       	 				  <td width="30%"><s:select name="cbbPojo.fkDefaultShippingPaperWorkLocation" headerValue="" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DEFAULT_SHIPPING_PAPER_WRK_LOC]" disabled="true" listKey="pkCodeId" listValue="description"></s:select></td>
								       </tr>
								       <tr>
								       <td width="30%"><s:text name="garuda.cbbdefaults.lable.dateFormat"></s:text>:</td>
				       	 				<td width="20%"><s:select name="cbbPojo.fkDateFormat"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DATE_FORMAT]" listKey="pkCodeId" listValue="description" disabled="true"></s:select></td>
						 				<td width="20%"><s:text name="garuda.cbbdefaults.lable.timeFormat"></s:text>:</td>
						 				<td width="30%"><s:select name="cbbPojo.fkTimeFormat" headerKey="" headerValue="" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TIME_FORMAT]" listKey="pkCodeId" disabled="true" listValue="description"></s:select></td>
						 				
					 
									</tr>
									--></table>
</fieldset>
</s:if>
					</td>
					</tr>	
    <!--<tr>
<td>
	<fieldset>
					<legend><s:text name="garuda.cbbdefaults.lable.localidassignment" /></legend>
					 <table cellpadding="0" cellspacing="0" width="100%" align="left">
								      <tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.localcbuidassignmentassgnusing"></s:text> </td>
								          <td width="20%"><s:select name="cbbPojo.localcbuassgnmentid" headerKey="" headerValue="" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LOCAL_ASSIGNMENT_ID]" disabled="true" listKey="pkCodeId" listValue="description" onchange="localassignId(this.value,'#localcbuassignid');"></s:select></td>
								      	  <td colspan="2"></td>
								      </tr>
								      <tr>
								      	<td colspan="4">
								      	
								       		<div id="localcbuassignid"  <s:if test="#request.autoMaticSequencingLocalAssgnId!=cbbPojo.localcbuassgnmentid">style="display: none;"</s:if>>
								       		<table>
								       		<tr>
								      		<td width="30%">
								   				<s:text name="garuda.cbbdefaults.lable.localcbuidprefix" />:
											</td>
								        	<td width="20%">
								        		<s:textfield name="cbbPojo.localCbuIdPrefix" readonly="true" onkeydown="cancelBack();"  />
								        	</td>
								        	<td width="30%">
								   				<s:text name="garuda.cbbdefaults.lable.localcbuidstrtnum" />:
											</td>
								        	<td width="20%">
								        		<s:textfield name="cbbPojo.localCbuIdStrtNum" readonly="true" onkeydown="cancelBack();"  cssClass="positive"/>
								        	</td>
								        	</tr>
								        	</table></div>
								        </td>
								      		
								      </tr>
								      <tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.localmaternalidassignmentassgnusing"></s:text> </td>
								          <td width="20%"><s:select name="cbbPojo.localmaternalassgnmentid" headerKey="" headerValue="" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LOCAL_ASSIGNMENT_ID]" listKey="pkCodeId" listValue="description" onchange="localassignId(this.value,'#localmaternalassignid');"></s:select></td>
								       	  <td colspan="2"></td>
								       </tr>
								     
								       <tr>
								       	<td colspan="4">
								       		<div id="localmaternalassignid" <s:if test="#request.autoMaticSequencingLocalAssgnId!=cbbPojo.localmaternalassgnmentid">style="display: none;"</s:if>>
								       		<table>
								       		<tr>
								      		<td width="30%">
								   				<s:text name="garuda.cbbdefaults.lable.localmaternalidprefix" />:
											</td>
								        	<td width="20%">
								        		<s:textfield name="cbbPojo.localMaternalIdPrefix" readonly="true" onkeydown="cancelBack();"  />
								        	</td>
								        	<td width="30%">
								   				<s:text name="garuda.cbbdefaults.lable.localmaternalidstrtnum" />:
											</td>
								        	<td width="20%">
								        		<s:textfield name="cbbPojo.localMaternalIdStrtNum" readonly="true" onkeydown="cancelBack();"  cssClass="positive"/>
								        	</td>
								        	</tr>
								        	</table></div>
								        </td>
								      </tr>
								      
								       
									</table>
    </fieldset>
    </td>
    </tr>	
			    
			 --><tr>
			    <td>
			        <s:if test="hasViewPermission(#request.ViewLastmodby)==true">
			         <fieldset>
					     <legend><s:text name="garuda.cbbdefaults.lable.lastmodifiedby" /></legend>
						  <table cellpadding="0" cellspacing="0" width="100%" align="left">
										<tr>		
											<td width="25%">
												<s:text name="garuda.unitreport.label.User_id" />:     				
								   			</td>
											<td width="25%">
												<s:if test="cbbPojo.lastModifiedBy!=null">
												<s:set name="cellValue1" value="cbbPojo.lastModifiedBy" scope="request"/>	
													<% 
													if(request.getAttribute("cellValue1")!=null)
													{	
														String cellValue1 = request.getAttribute("cellValue1").toString();
														UserJB userB = new UserJB();
														UserAlertsHelper userHelp = new UserAlertsHelper();
														userB.setUserId(EJBUtil.stringToNum(cellValue1));
														userB.getUserDetails();													
															//String userName = userB.getUserLastName()+" "+userB.getUserFirstName();
															String userName = userHelp.getLogNameByUserId(new Long (EJBUtil.stringToNum(cellValue1)));
																request.setAttribute("userName",userName);
													}		%>									
													</s:if>
												<s:textfield name="#request.userName" readonly="true" onkeydown="cancelBack();"  />
											</td>
											<td width="25%">
												<s:text name="garuda.clinicalnote.label.dateTime" />:     				
								   			</td>
											<td width="25%">
												<s:date name="cbbPojo.lastModifiedOn" id="createdOn1" format="MMM dd, yyyy / HH:mm" />
												<s:textfield name="createdOn1" readonly="true" onkeydown="cancelBack();"  />
											</td>
								        </tr>
								        </table>
						   </fieldset>
						   </s:if>
						   </td>
						   </tr>									   
						  </table>
										
						</s:form></body></html>