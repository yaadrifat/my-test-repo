<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.business.pojoobjects.CBBPojo"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<jsp:include page="cb_track_session_logging.jsp" />

<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/><script>
   $j( "input:submit, button", ".column" ).button();		 

   jQuery.validator.addMethod("numberwithindigit", function(value, element) {
	    return (parseInt(value.length) == 5 || parseInt(value.length) == 9 || parseInt(value.length)== 0);
	}, "<s:text name="garuda.cbu.cbb.addresszip"/>");

   jQuery.validator.addMethod("checkphonenumber", function(value, element) {	
	var  isPhoneFormat = 1 ;
	isPhoneFormat = 1 ;
	
	for(i=0;i<parseInt(value.length);i++)
	{		
		if(value.charAt(i)!='0')
		{			
			if(!parseInt(value.charAt(i)))
			{
				
				if(value.charAt(i)!='-' ) 
				{
					isPhoneFormat = -1;
				}	
			}
			
		}	
	}
	
	    return ((parseInt(value.length) == 12 || parseInt(value.length)== 0 ) &&  (isPhoneFormat == 1) );
	}, "<s:text name="garuda.edit_cbb.label.validPhoneNum"/>");
	
	
   jQuery.validator.addMethod("specialCharCheck", function(value, element) {
	  	 var flag = true;
		 var iChars = "!@#$%^&*()+=[]\\\';,./{}|\":<>?~_"; 
		   for (var i = 0; i < value.length; i++) {
		  	if (iChars.indexOf(value.charAt(i)) != -1) {		  	 
		  		flag = false;
		  		break;
		  	}
		  }
		   return flag;
		   
	    return (flag);
	}, "<s:text name="garuda.cbbdefaults.lable.splchar"/>");



   jQuery.validator.addMethod("specialCharCk", function(value, element) {
	  	 var flag = true;
		 var iChars = "\"\\"; 
		   for (var i = 0; i < value.length; i++) {
		  	if (iChars.indexOf(value.charAt(i)) != -1) {		  	 
		  		flag = false;
		  		break;
		  	}
		  }
		   return flag;
		   
	    return (flag);
	}, "Special characters   \\ \" are not allowed");


	
   
   $j(function(){
		$j("#cbbdefaultform").validate({
			invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    },
	    errorPlacement: function(error, element) {
			   if ( element.is(":radio") )
		    	error.insertAfter(element.next().next().next());
			  else 
	            error.insertAfter( element);	
		    },

	    
				
			rules:{	
					"site.siteName" : {required:true,specialCharCk:true},
					"cbbPojo.cdrUser" : {required:true},
					"cbbPojo.cbuAssessment" : {required:true},
					"cbbPojo.unitReport" : {required:true},
					"site.address.zip" : {specialCharCheck:true},
					"cbbPojo.address.zip" : {specialCharCheck:true},
					"cbbPojo.address.email" : {email:true},
					"site.address.email" : {email:true},					
					"cbbPojo.address.phone" : {checkphonenumber:true},
					"site.address.phone" : {checkphonenumber:true},
					//"fkAccreditation" : {required:true},
					"cbbPojo.cbuAssessment" : {required:true},
					"cbbPojo.unitReport" : {required:true},
					"site.siteIdentifier" : {required:true,specialCharCheck:true},
					
					//"site.siteName" : {required:true},
					
					//"cbbPojo.attention" : {required:true},
					//"cbbPojo.pickUpAddLastName" : {required:true},
					//"cbbPojo.pickUpAddFirstName" : {required:true},
					//"cbbPojo.address.phone" : {required:true,checkphonenumber:true},					
					//"cbbPojo.address.address1" : {required:true},
					//"cbbPojo.address.address2" : {required:true},
				//	"cbbPojo.address.city" : {specialCharCheck:true},
				//	"cbbPojo.address.state" : {specialCharCheck:true},
				//	"cbbPojo.address.country" : {specialCharCheck:true},

				//	"cbbPojo.addressDryShipper.city" : {specialCharCheck:true},
				//	"cbbPojo.addressDryShipper.state" : {specialCharCheck:true},
				//	"cbbPojo.addressDryShipper.country" : {specialCharCheck:true},
					
				//	"site.address.city" : {specialCharCheck:true},
				//	"site.address.state" : {specialCharCheck:true},
				//	"site.address.country" : {specialCharCheck:true},
					
					
					//"cbbPojo.address.state" : {required:true},	
					//"cbbPojo.attentiondDryShipper" : {required:true},
					//"cbbPojo.addressDryShipper.address1" : {required:true},
					//"cbbPojo.addressDryShipper.address2" : {required:true},
					//"cbbPojo.addressDryShipper.city" : {required:true},
					//"cbbPojo.addressDryShipper.state" : {required:true},
					"cbbPojo.cdrUser" : {required:true},
					"cbbPojo.addressDryShipper.zip" : {specialCharCheck:true}
				},
				messages:{
					//"site.siteName": "<s:text name="garuda.cbu.cbb.defaults.siteNameRequired"/>",
					"cbbPojo.cdrUser":"<s:text name="garuda.cbu.cbb.defaults.cdrUserRequired"/>",
					"cbbPojo.cbuAssessment":"<s:text name="garuda.cbu.cbb.defaults.cbuAssessmentRequired"/>",
					"cbbPojo.unitReport":"<s:text name="garuda.cbu.cbb.defaults.unitReportRequired"/>",
					//"site.address.zip":"<s:text name="garuda.cbu.cbb.addresszip"/>",
					//"cbbPojo.address.zip":"<s:text name="garuda.cbu.cbb.addresszip"/>",
					//"cbbPojo.addressDryShipper.zip":"<s:text name="garuda.cbu.cbb.addresszip"/>",
					"cbbPojo.address.email":"<s:text name="garuda.common.validation.addressemail"/>",
					//"fkAccreditation":"<s:text name="garuda.cbu.cbb.accreditation"/>",
					"site.address.email":"<s:text name="garuda.common.validation.addressemail"/>"
						
				}
			});
		});

	function submitCBBForm(){
		isPickUpOrDryshipperAddChange();
		if($j("#cbbdefaultform").valid()){
		modalFormSubmitRefreshDiv('cbbdefaultform','updateCBBDefaults','cbbform');
		}
	}

	function isPickUpOrDryshipperAddChange()
	{		
		if(document.getElementsByName("cbbPojo.attention")[0].value == document.getElementsByName("cbbPojo.attention")[0].defaultValue
&& document.getElementsByName("cbbPojo.address.phone")[0].value == document.getElementsByName("cbbPojo.address.phone")[0].defaultValue
&& document.getElementsByName("cbbPojo.address.email")[0].value == document.getElementsByName("cbbPojo.address.email")[0].defaultValue
&& document.getElementsByName("cbbPojo.address.address1")[0].value == document.getElementsByName("cbbPojo.address.address1")[0].defaultValue
&& document.getElementsByName("cbbPojo.address.address2")[0].value == document.getElementsByName("cbbPojo.address.address2")[0].defaultValue
&& document.getElementsByName("cbbPojo.address.city")[0].value == document.getElementsByName("cbbPojo.address.city")[0].defaultValue
&& document.getElementsByName("cbbPojo.address.state")[0].value == document.getElementsByName("cbbPojo.address.state")[0].defaultValue
&& document.getElementsByName("cbbPojo.address.zip")[0].value == document.getElementsByName("cbbPojo.address.zip")[0].defaultValue
&& document.getElementsByName("cbbPojo.address.country")[0].value == document.getElementsByName("cbbPojo.address.country")[0].defaultValue
&& document.getElementsByName("cbbPojo.pickUpAddSplInstuction")[0].value == document.getElementsByName("cbbPojo.pickUpAddSplInstuction")[0].defaultValue
		)
		{
			//alert("value not Change");
		}
		else{
			document.getElementsByName("ispickupAddUpdated")[0].value="true";
			
			
		}	


		if(document.getElementsByName("cbbPojo.attentiondDryShipper")[0].value == document.getElementsByName("cbbPojo.attentiondDryShipper")[0].defaultValue
				&& document.getElementsByName("cbbPojo.addressDryShipper.address1")[0].value == document.getElementsByName("cbbPojo.addressDryShipper.address1")[0].defaultValue
				&& document.getElementsByName("cbbPojo.addressDryShipper.address2")[0].value == document.getElementsByName("cbbPojo.addressDryShipper.address2")[0].defaultValue
				&& document.getElementsByName("cbbPojo.addressDryShipper.city")[0].value == document.getElementsByName("cbbPojo.addressDryShipper.city")[0].defaultValue
				&& document.getElementsByName("cbbPojo.addressDryShipper.state")[0].value == document.getElementsByName("cbbPojo.addressDryShipper.state")[0].defaultValue
				&& document.getElementsByName("cbbPojo.addressDryShipper.zip")[0].value == document.getElementsByName("cbbPojo.addressDryShipper.zip")[0].defaultValue
				&& document.getElementsByName("cbbPojo.addressDryShipper.country")[0].value == document.getElementsByName("cbbPojo.addressDryShipper.country")[0].defaultValue
						)
						{
							//alert("addressDryShipper value not Change");
						}
						else{
							document.getElementsByName("isDryShipperAddUpdated")[0].value="true";
							
							
						}	

		
	}
	
	function localassignId(value,divname){
    	
   	 if(value == document.getElementById("autoMaticSequencingLocalAssgnId").value)
   		{
   		 
   		 $j(divname).css('display','block');
   		 
   		 }
   	 else{
   		 
   		 $j(divname).css('display','none');
   	 }
	  
  } 
	$j(".positive").numeric({ negative: false }, function() { alert("<s:text name="garuda.common.cbu.negativeValueAlert"/>"); this.value = ""; this.focus(); });

/*	$j('.openOrganisation').popupWindow({ 		
		centerBrowser:1 
		}); */

		$j("#cbbPickAdd").keydown(function(e) {
			  var val = $j("#cbbPickAdd").val();
			  if(e.keyCode!=8 && e.keyCode!=46 )
			  {
				  if(val.length==3 || val.length==7){
					  if(e.which!=109){
						   $j("#cbbPickAdd").val(val+"-");
					  }
				  }
			  }
		});

		$j("#sitePh").keydown(function(e) {
			  var val = $j("#sitePh").val();
			  if(e.keyCode!=8 && e.keyCode!=46 )
			  {
				  if(val.length==3 || val.length==7){
					  if(e.which!=109){
						   $j("#sitePh").val(val+"-");
					  }
				  }
			  }
		});

		function closeModal(){
			paginationFooter(defaultPageNojs,'getCbbDetails,showsRow,inputs,Tableform,searchTble,manageProfile','','')
			if(document.getElementsByName("rfreshPageThis")[0].value == "true")
			{
				showSelectedCBBByRefresh(<s:property value="site.siteId"/>);
			}
			 $j("#modelPopup1").dialog("close");
			
			 $j("#modelPopup1").dialog("destroy");
			 }

		 function constructTable(){}


		 function showModalOneNew(title,url,height,width,collSite)
		 {
		 	//alert('S');
		 	$j('.progress-indicator').css( 'display', 'block' );
		 	if(height==null || height==""){
		 		height = 650;
		 	}
		 	if(width==null || width==""){
		 		width = 750;
		 	}
		 var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
		 	
		 	$j("#modelPopup2").html(progressMsg);
		 		$j("#modelPopup2").dialog(
		 				   {autoOpen: false,
		 					title: title  ,
		 					resizable: true,
		 					closeText: '',
		 					closeOnEscape: false ,
		 					modal: true, width : width, height : height,
		 					close: function() {
		 						//$(".ui-dialog-content").html("");
		 						//jQuery("#subeditpop").attr("id","subeditpop_old");
		 						showprogressMgs();
		 						var siteType ="";
									if(collSite== false)
									{	
										siteType="strLoc";
									}
									else	
									{
										siteType="strCol";
									}
								refreshStorageLocCollectionSite("refreshStrLocation?SiteType="+siteType+"&siteId=<s:property value='site.siteId' />");
								closeprogressMsg();
		 		      	//	jQuery("#modelPopup2").dialog("destroy");
		 				    }
		 				   }
		 				  ); 
		 		$j("#modelPopup2").dialog("open");
		 	$j.ajax({
		         type: "POST",
		         url: url,
		        // async:false,
		         success: function (result, status, error){
		 	       	$j("#modelPopup2").html(result);
		         	       	
		         },
		         error: function (request, status, error) {
		         	alert("Error " + error);
		             alert(request.responseText);
		         }

		 	});
		 	$j('.progress-indicator').css( 'display', 'none' );
		 }

 function checkCbbId(url){
				//url += '&siteId=<s:property value="site.siteId"/>&cbuCollectionPro=<s:property value="cbuCollectionPro"/>'	
				//alert("url = "+url);
				if(document.getElementsByName("site.siteIdentifier")[0].value != document.getElementsByName("site.siteIdentifier")[0].defaultValue)	    
				{
					showprogressMgs();
						$j.ajax({
					        type: "POST",
					        url: url,
					      
					        success: function (result){
					            var $response=$j(result);
					            var errorpage = $response.find('#errorForm').html();
					            if(errorpage != null &&  errorpage != "" ){
					            }else{

						           // alert("result.CbbIdExists =  "+result.cbbIdExists)
					            	if(result.cbbIdExists=='Yes')
					            	{	
					            		alert('CBB ID '+$j('#siteIdentifierID').val()+' already exist');
					            		$j('#siteIdentifierID').val('');
					            		setTimeout('closeprogressMsg()',1000);
					            	}
					            	else{
					            		 closeprogressMsg();
					            	}
					            }
					        },
					        error: function (request, status, error) {
					        	alert("Error " + error);
					        	$j(processingSaveDialog).dialog("close");	
					           // alert(request.responseText);
					        	closeprogressMsg();
					        }

						});	
						
			}
 }
		
</script>

<script>

$j(document).ready(function(){
	
	 $j('#word_count').each(function(){
	     //maximum limit of characters allowed.
	     var maxlimit = 200;
	     // get current number of characters
	     var length = $j(this).val().length;
	     if(length >= maxlimit) {
	   $j(this).val($j(this).val().substring(0, maxlimit));
	   length = maxlimit;
	  }
	     // update count on page load
	     $j(this).parent().find('#counter').html( (maxlimit - length) + ' characters left');
	     // bind on key up event
	     $j(this).keyup(function(){
	  // get new length of characters
	  var new_length = $j(this).val().length;
	  if(new_length >= maxlimit) {
	    $j(this).val($j(this).val().substring(0, maxlimit));
	    //update the new length
	    new_length = maxlimit;
	   }
	  // update count
	  $j(this).parent().find('#counter').html( (maxlimit - new_length) + ' characters left');
	     });
	 });
});
</script>


<s:form id="cbbdefaultform">
<s:hidden name="cbbPojo.person.personId" ></s:hidden>
<s:hidden name="cbbPojo.pkCbbId" ></s:hidden>
<s:hidden name="cbbPojo.personId" ></s:hidden>
<s:hidden name="site.sitePerAdd" ></s:hidden>
<s:hidden name="site.siteId" ></s:hidden>
<s:hidden name="cbbPojo.enableEditAntigens" ></s:hidden>
<s:hidden name="cbbPojo.displayMaternalAntigens" ></s:hidden>
<s:hidden name="cbbPojo.enableCbuInventFeature" ></s:hidden>
<s:hidden name="cbbPojo.defaultMicroContamDateToProcDate" ></s:hidden>
<s:hidden name="cbbPojo.defaultCfuTestDateToProcDate" ></s:hidden>
<s:hidden name="cbbPojo.defaultViabTestDateToProcDate" ></s:hidden>
<s:hidden name="ispickupAddUpdated" value="false"></s:hidden>
<s:hidden name="isDryShipperAddUpdated" value="false"></s:hidden>
<s:hidden name="cbbPojo.address.addressId" ></s:hidden>
<s:hidden name="cbbPojo.addressDryShipper.addressId" ></s:hidden>
<s:hidden name="cbbPojo.attention" ></s:hidden>
<!--<s:hidden name="cbbPojo.allowMember" ></s:hidden>
<s:hidden name="cbbPojo.cdrUser" ></s:hidden>
<s:hidden name="cbbPojo.isDomesticCBB" ></s:hidden>
<s:hidden name="cbbPojo.useOwnDumn" ></s:hidden>
<s:hidden name="cbbPojo.useFdoe" ></s:hidden>
--><s:hidden name="site.address.addressId" ></s:hidden>
<s:hidden name="#request.nmdpCbuIdLocalAssgnId" id="nmdpCbuIdLocalAssgnId"/>
  <s:hidden name="#request.autoMaticSequencingLocalAssgnId" id="autoMaticSequencingLocalAssgnId"/>
  <s:hidden name="#request.userDefindeLocalAssgnId" id="userDefindeLocalAssgnId"/>
 
 
 <!--<s:hidden name="cbbPojo.isDomesticCBB" ></s:hidden>
 --><s:hidden name="cbbPojo.useOwnDumn" ></s:hidden> 
 <s:hidden name="cbbPojo.useFdoe" ></s:hidden>  
 <s:hidden name="cbbPojo.enableAutoCompleteCordEntry" ></s:hidden> 
 <s:hidden name="cbbPojo.enableAutoCompleteAckResTask" ></s:hidden> 
 <s:hidden name="cbbPojo.cordInfoLock" ></s:hidden>
 <s:hidden name="cbbPojo.localcbuassgnmentid" ></s:hidden>
 <s:hidden name="cbbPojo.localCbuIdPrefix" ></s:hidden>
 <s:hidden name="cbbPojo.localCbuIdStrtNum" ></s:hidden>
 <s:hidden name="cbbPojo.localmaternalassgnmentid" ></s:hidden>
 <s:hidden name="cbbPojo.localMaternalIdPrefix" ></s:hidden>
 <s:hidden name="cbbPojo.localMaternalIdStrtNum" ></s:hidden>
 <s:hidden name="cbbPojo.fkDateFormat" ></s:hidden>
 <s:hidden name="cbbPojo.fkTimeFormat" ></s:hidden>
 
  
<div class="column">			
			<div id="update" style="display: none;" >
  
		  <jsp:include page="cb_update.jsp">
		    <jsp:param value="garuda.cbbprocedures.msg.cbbDefault" name="message"/>
		  </jsp:include>	   
	
</div>	  
				  
 <table width="100%" cellspacing="0" cellpadding="0" border="0" id="status">
 
 <tr>
    <td>
<div id="tabCbbSetup">
    <fieldset>
					<legend><s:text name="garuda.cbbdefaults.lable.cbbsetup" /></legend>
						  <table cellpadding="0" cellspacing="0" width="100%" align="left">
								<tr>
									<td>
										<s:text name="garuda.pendingorderpage.label.cddid" />:  <span class="error">*</span>				
						   			</td>
									<td >
										<s:textfield name="site.siteIdentifier" maxlength="10" id = "siteIdentifierID" onblur="checkCbbId('cbbIdExist?cbbId='+this.value)" />
									</td>		
									<td>
										<s:text name="garuda.cbbdefaults.lable.cbbregistryname" />:<span style="color:red;">*</span>     				
						   			</td>
									<td >
										<s:textfield name="site.siteName" maxlength="50" />
									</td>
									
						        </tr>
						        
						        <tr>		
									<td >
										<s:text name="garuda.cbbdefaults.lable.cbbsystemuser" />:<span style="color:red;">*</span>																			
									</td>
									<td >
										<s:radio name="cbbPojo.cdrUser" list="#{'true':'Yes','false':'No'}" ></s:radio>
									</td>								
									<td>
										<s:text name="garuda.cbbdefaults.lable.bmdwcode" />:     				
						   			</td>
									<td >
										<s:textfield name="cbbPojo.bmdw" maxlength="10" />
									</td>		
						        </tr>
						        
						        
						        <tr>
						        	<td>
										<s:text name="garuda.cbbdefaults.lable.emdis" />:     				
						   			</td>
									<td >
										<s:textfield name="cbbPojo.emdis"  maxlength="10"/>
									</td>
						        
						        	<td >
										<s:text name="garuda.cbbdefaults.lable.membershiptype"></s:text>:
									</td>
				          			<td >
				          				<!--<s:radio name="cbbPojo.allowMember" list="#{'true':'Yes','false':'NO'}" ></s:radio>
				          				--><s:select headerKey="9" headerValue="Select"
														list="#{'1':'Member', '0':'Non-Member', '2':'Participating', '3':'Other'}" 
														name="cbbPojo.allowMember" 
														 />
				          			</td>	
									
						        </tr>
						        
						        
						        <tr>
						        	<td>
						   				<s:text name="garuda.cbbdefaults.lable.cbuassessmentbutton" />:<span style="color:red;">*</span>
									</td>
						        	<td>
						        		<s:radio name="cbbPojo.cbuAssessment" list="#{'true':'Yes','false':'No'}" ></s:radio>
						        	</td>
						            <td>
						   				<s:text name="garuda.cbbdefaults.lable.unitreportbutton" />:<span style="color:red;">*</span>
									</td>
						        	<td>
						        		<s:radio name="cbbPojo.unitReport" list="#{'true':'Yes','false':'No'}" ></s:radio>
						        	</td>
						        	
						        </tr>
						       
						         <tr>
						         	<td>
						   				<s:text name="garuda.cbbdefaults.lable.researchsample" />:
									</td>
						        	<td>
						        		<s:radio name="cbbPojo.nmdpResearch" list="#{'true':'Yes','false':'No'}" ></s:radio>
						        	</td>
						         	  <td ><s:text name="garuda.cbbdefaults.lable.domestic"></s:text>:</td>
						 			 <td ><s:radio name="cbbPojo.isDomesticCBB" list="#{'true':'Yes','false':'No'}" ></s:radio></td>
						        					        	
						        </tr>
						       <tr>
						        	<td colspan="2">
						        		<s:text name="garuda.cbbdefaults.lable.CRIReqElgQuest" />:<s:checkbox name="cbbPojo.criReqElgQuest" id="criReqFlag" onchange="addCRIReqValue();" />
						        	</td>
						        	<td colspan="2"></td>
						        </tr>
						          
						        </table>
   </fieldset>
</div>
   </td>
   </tr>

  <tr>
    <td>
    <div  id="tabcbinfo">
    <fieldset>
					<legend><s:text name="garuda.cbbdefaults.lable.cbbinformation" /></legend>
						  <table cellpadding="0" cellspacing="0" width="100%" align="left">
								<!--<tr>		
									<td>
										<s:text name="garuda.cbbdefaults.lable.cbbname" />:     				
						   			</td>
									<td >
										<s:textfield name="site.siteName"  />
									</td>
									<td>
										<s:text name="garuda.cbbdefaults.lable.cbbid" />:     				
						   			</td>
									<td >
										<s:textfield name="site.siteIdentifier" readonly="true" onkeydown="cancelBack();"  />
									</td>
						        </tr>
						        
						        <tr>		
									<td colspan ="2">																			
										<a href="<s:property value="linkUrl" />" target="_blank"><s:property value="linkDesc"/></a>		 
						   			</td>									
									<td>
										<s:text name="garuda.cbbdefaults.lable.OrgType" />:     				
						   			</td>
									<td >
										<s:textfield name="cbbType"  readonly="true" onkeydown="cancelBack();" />
									</td>
						        </tr>

						        --><tr>
						        	<td>
						   				<s:text name="garuda.cbbdefaults.lable.coordinatorlastname" />:
									</td>
						        	<td>
						        		<input type="text" name="lastName" value="<s:property value="cbbPojo.person.personLname" />" maxlength="100" />
						        	</td>
						            <td>
						   				<s:text name="garuda.cbbdefaults.lable.coordinatorfirstname" />:
									</td>
						        	<td>
						        		<input type="text" name="firstName" value="<s:property value="cbbPojo.person.personFname"/>"  maxlength="100"/>
						        	</td>
						        	
						        </tr>
						       
						         <tr>
						         	<td>
						   				<s:text name="garuda.cbbdefaults.lable.phone" />:
									</td>
						        	<td>
						        		<s:textfield name="site.address.phone" id="sitePh" maxlength="100"></s:textfield>
						        	</td>
						            <td>
						   				<s:text name="garuda.cbbdefaults.lable.phoneext" />:
									</td>
						        	<td>
						        		<s:textfield name="site.address.ext"  maxlength="20"></s:textfield>
						        	</td>						        	
						        </tr>
						        <tr>
						            <td>
						   				<s:text name="garuda.cbbdefaults.lable.fax" />:
									</td>
						        	<td>
						        		<s:textfield name="site.address.fax" maxlength="100"></s:textfield>
						        	</td>
						        	<td>
						   				<s:text name="garuda.cbbdefaults.lable.email" />:
									</td>
						        	<td>
						        		<s:textfield name="site.address.email" maxlength="100" ></s:textfield>
						        	</td>
						        </tr>
						        
						        <tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.address1"></s:text>:</td>
								          <td width="20%"><s:textfield name="site.address.address1" maxlength="50" /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.address2"></s:text>:</td>
								          <td width="20%"><s:textfield name="site.address.address2" maxlength="50" /></td>
							</tr>	
							
								<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.city"></s:text>:</td>
								          <td width="20%"><s:textfield name="site.address.city" maxlength="30" /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.state"></s:text>:</td>
								          <td width="20%"><s:textfield name="site.address.state" maxlength="30" /></td>
							</tr>		
							
							<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.zipcode"></s:text>:</td>
								          <td width="20%"><s:textfield name="site.address.zip" maxlength="15" /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.country"></s:text>:</td>
								          <td width="20%"><s:textfield name="site.address.country" maxlength="30" /></td>
							</tr>		
						          
						        </table>
   </fieldset>
   </div>
   </td>
   </tr>
<tr>
<td>
    <div  id="tabcbupck">
	<fieldset>
					<legend><s:text name="garuda.cbbdefaults.lable.cbuPickupAddress" /></legend>
					 <table cellpadding="0" cellspacing="0" width="100%" align="left">
							<!--<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.address.contact"></s:text>:<span style="color:red;">*</span></td>
								          <td width="20%"><s:textfield name="cbbPojo.attention" /></td>
								      	  <td colspan="2"></td>
								      	  
								      	  
							</tr>
							--><tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.address.contact.lastName"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.pickUpAddLastName" maxlength="30" /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.address.contact.firstName"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.pickUpAddFirstName" maxlength="30" /></td>
							</tr>	
							  
							
							<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.contact.phone"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.address.phone" id="cbbPickAdd" maxlength="100" /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.contact.email"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.address.email" maxlength="100" /></td>
							</tr>
							<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.address1"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.address.address1" maxlength="50" /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.address2"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.address.address2" maxlength="50" /></td>
							</tr>	
							
								<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.city"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.address.city" maxlength="30" /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.state"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.address.state"  maxlength="30" /></td>
							</tr>		
							
							<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.zipcode"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.address.zip" maxlength="15" /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.country"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.address.country"  maxlength="30" /></td>
							</tr>		      
								<!--<jsp:include page="cb_editable-address.jsp" flush="true">
						           <jsp:param value="cbbPojo.address" name="prefix"/>
						           <jsp:param value="Yes" name="hideFields"/>
						        </jsp:include>
						       
							  --><tr>
									          <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.address.special.instruction"></s:text>:</td>
									          <td colspan="2"><s:textarea id="word_count" name="cbbPojo.pickUpAddSplInstuction" cols="50" rows="3"/>
									          <br/>
												<span id="counter"></span>
									          
									          </td>
									      	  <td width="20%"></td>
									      	  
									      	  
								</tr>	  
								       
									</table>
    </fieldset>
  </div>
    </td>
    </tr>
    <tr>
<td>
   <div id="tabdryship">
	<fieldset>
					<legend><s:text name="garuda.cbbdefaults.lable.dryShipperReturnAddtess" /></legend>
					 <table cellpadding="0" cellspacing="0" width="100%" align="left">
							<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.attentionorg"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.attentiondDryShipper" maxlength="25" /></td>
								          <td width="30%">&nbsp;</td>
								          <td width="20%">&nbsp;</td>	
								      	  
							</tr>
							
							<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.address1"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.addressDryShipper.address1" maxlength="50" /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.address2"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.addressDryShipper.address2" maxlength="50" /></td>
							</tr>	
							
								<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.city"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.addressDryShipper.city" maxlength="30" /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.state"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.addressDryShipper.state" maxlength="30" /></td>
							</tr>		
							
							<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.zipcode"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.addressDryShipper.zip"  maxlength="15"/></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.country"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbbPojo.addressDryShipper.country" maxlength="30" /></td>
							</tr>		      
							
								<!--<jsp:include page="cb_editable-address.jsp" flush="true">
						           <jsp:param value="cbbPojo.addressDryShipper" name="prefix"/>
						           <jsp:param value="Yes" name="hideFields"/>
						        </jsp:include>
								      
								      
								       
									--></table>
    </fieldset>
    </div>
    </td>
    </tr>
   
    <tr>
  <td>
   <div id="tabcbbDflt">
   <fieldset>
					<legend><s:text name="garuda.cbbdefaults.lable.cbbdefaults" /></legend>
			  <table cellpadding="0" cellspacing="0" width="100%" align="left" border="0" >
			            <s:if test="hasNewPermission(#request.ViewCbbDeflt)==true">
			  			 <tr> 
						   <td colspan="2"><a target="_blank" href="#" onClick="showModalOneNew('Add Storage Location','addCbuStLoCbuCoSite?site.siteId=<s:property value="site.siteId" />&site.siteParent=<s:property value="site.siteParent" />&cbuCollectionPro=false ',330,650,false)" ><s:text name="garuda.cbbdefaults.lable.addCbuStorLocation"></s:text></a></td>
			                <td colspan="2">
			                <a target="_blank" href="#" onClick="showModalOneNew('Add Collection site','addCbuStLoCbuCoSite?site.siteId=<s:property value="site.siteId" />&site.siteParent=<s:property value="site.siteParent" />&cbuCollectionPro=true ',330,650,true)" ><s:text name="garuda.cbbdefaults.lable.addCbuCollSite"></s:text></a>
			               </td>           
          				 </tr>	
          				 </s:if>		  
			  			<!--<tr>
							 <td width="30%"><s:text name="garuda.cbudefaults.label.storage.location"></s:text>:</td>
							 <td width="20%"> <s:select list="sitePojoArr"  headerKey="-1" headerValue="Select" name="fkCbuStorage" listKey="siteId" title="siteName" listValue="siteName"> </s:select></td>
							 <td width="20%"><s:text name="garuda.cbudefaults.label.collection.site"></s:text>:</td>
							 <td width="30%"><s:select list="CollSitePojoArr"  name="fkCbuCollection"  headerKey="-1" headerValue="Select" listKey="siteId"  listValue="siteName"> </s:select></td>
						</tr>
						--><tr>
							 <td ><s:text name="garuda.cbudefaults.label.storage.location"></s:text>:</td>
							 <td >
								<select name="fkCbuStorage" id="fkCbuStorageId">
								<option value="-1">Select</option>
        							<s:iterator value="sitePojoArr" var="line" status="rows">	
							            <option value="<s:property value="siteId"/>" title="<s:property value="siteName"/>"><s:property value="siteName"/></option>
							        </s:iterator>
    							</select>
						
							</td>
							 <td ><s:text name="garuda.cbudefaults.label.collection.site"></s:text>:</td>
							 <td >
							 
							 	<select name="fkCbuCollection" id="fkCbuCollectionId" >
								<option value="-1">Select</option>
        							<s:iterator value="CollSitePojoArr" var="line" status="rows">	
							            <option value="<s:property value="siteId"/>" title="<s:property value="siteName"/>"><s:property value="siteName"/></option>
							        </s:iterator>
    							</select>
							 	
							
							 </td>
						</tr>
						<!--<tr>
							
							<td colspan ="3"></td>
						
						</tr>
						--><tr><!--
							 <td><s:text name="garuda.cbuentry.label.idbag"></s:text>:</td>
							 <td> <s:select name="cbuOnBag" multiple="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_IDS]" listKey="pkCodeId" listValue="description" ></s:select>
							 </td>
							 -->
							
						  <td ><s:text name="garuda.cbbdefaults.lable.defaultCTShipper"></s:text>:</td>
				          <td ><s:select name="cbbPojo.defaultCTShipper" headerKey="" headerValue="Select" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPER]" listKey="pkCodeId" listValue="description" ></s:select> </td>
						  <td ><s:text name="garuda.cbbdefaults.lable.defaultORShipper"></s:text>:</td>
				          <td ><s:select name="cbbPojo.defaultORShipper" headerKey="" headerValue="Select" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPER]" listKey="pkCodeId" listValue="description" ></s:select></td>
							 
							 
							 
						</tr>
			 			<!--<tr>
				          <td width="30%"><s:text name="garuda.cbbdefaults.lable.enableeditanitgens"></s:text>:</td>
				          <td width="20%"><s:radio name="cbbPojo.enableEditAntigens" list="#{'true':'Yes','false':'NO'}" ></s:radio>
				          </td>
				          <td width="20%"><s:text name="garuda.cbbdefaults.lable.displayMaternalAntigens"></s:text>:</td>
				          <td width="30%"><s:radio name="cbbPojo.displayMaternalAntigens" list="#{'true':'Yes','false':'NO'}" ></s:radio>
				           </td>
				       </tr>
				     
				       --><!--<tr>
				          <td width="30%"><s:text name="garuda.cbbdefaults.lable.enableautocompletecordentry"></s:text>:</td>
				          <td width="20%"><s:radio name="cbbPojo.enableAutoCompleteCordEntry" list="#{'true':'Yes','false':'NO'}" ></s:radio>
				          </td>
				          <td width="20%"><s:text name="garuda.cbbdefaults.lable.enableautocompleteackrestask"></s:text>:</td>
				          <td width="30%"><s:radio name="cbbPojo.enableAutoCompleteAckResTask" list="#{'true':'Yes','false':'NO'}" ></s:radio>
				           </td>
				       </tr>
				       <tr>
				          <td width="30%"><s:text name="garuda.cbbdefaults.lable.enableCbuInventFeature"></s:text>:</td>
				          <td width="20%"><s:radio name="cbbPojo.enableCbuInventFeature" list="#{'true':'Yes','false':'NO'}" ></s:radio>
				           </td>
				           </tr>-->				          
				       <tr>
				          <!--<td width="30%"><s:text name="garuda.cbbdefaults.lable.defaultMicroContamDateToProcDate"></s:text>:</td>
				          <td width="20%"><s:radio name="cbbPojo.defaultMicroContamDateToProcDate" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
				          -->
				          				           
				          <td width="14%"><s:text name="garuda.cbbdefaults.lable.defaultShippingPaperWorkLoc"></s:text>:</td>
				       	 <td width="39%"><s:select name="cbbPojo.fkDefaultShippingPaperWorkLocation" headerKey="" headerValue="Select" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DEFAULT_SHIPPING_PAPER_WRK_LOC]" listKey="pkCodeId" listValue="description"></s:select></td>
				          <td width="10%"><s:text name="garuda.cbbdefaults.lable.accreditation"></s:text>:</td>
						 <td width="37%"><s:select name="fkAccreditation" multiple="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ACCREDITITAON]" listKey="pkCodeId" listValue="description"></s:select></td>
				       </tr><!--
				       <tr>
				          <td width="30%"><s:text name="garuda.cbbdefaults.lable.defaultCfuTestDateToProcDate"></s:text>:</td>
				          <td width="20%"><s:radio name="cbbPojo.defaultCfuTestDateToProcDate" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
				          <td width="20%"><s:text name="garuda.cbbdefaults.lable.defaultViabTestDateToProcDate"></s:text>:</td>
				          <td width="30%"><s:radio name="cbbPojo.defaultViabTestDateToProcDate" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
				       </tr>
				       --><!--<tr>
				          <td width="30%"><s:text name="garuda.cbbdefaults.lable.EnableCordInfoLock"></s:text>:</td>
				          <td width="20%"><s:radio name="cbbPojo.cordInfoLock" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
				           
				           <td width="50%" colspan="2">
				           </td>
				          
						 
				           </tr>
				       <tr>
				          <td width="30%"><s:text name="garuda.cbbdefaults.lable.allowmember"></s:text>:</td>
				          <td width="20%"><s:radio name="cbbPojo.allowMember" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
				          				         
				          <td width="20%"><s:text name="garuda.cbbdefaults.lable.usingcdr"></s:text>:</td>
				          <td width="30%"><s:radio name="cbbPojo.cdrUser" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
				       </tr>
				       --><!--<tr>
						  <td width="30%"><s:text name="garuda.cbbdefaults.lable.domestic"></s:text>:</td>
						  <td width="20%"><s:radio name="cbbPojo.isDomesticCBB" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
						  <td width="20%"><s:text name="garuda.cbbdefaults.lable.useowndumn"></s:text>:</td>
						  <td width="30%"><s:radio name="cbbPojo.useOwnDumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
					  </tr>
					  <tr>
						 <td width="30%"><s:text name="garuda.cbbdefaults.lable.fdoe"></s:text>:</td>
						 <td width="20%"><s:radio name="cbbPojo.useFdoe" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>
						 <td colspan="2"></td>
					  </tr>
						 --><!--<tr>
						 <td>&nbsp;</td>
						 <td>&nbsp;</td>
						 <td width="20%"><s:text name="garuda.cbbdefaults.lable.defaultShippingPaperWorkLoc"></s:text>:</td>
				       	 <td width="30%"><s:select name="cbbPojo.fkDefaultShippingPaperWorkLocation" headerKey="" headerValue="Select" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DEFAULT_SHIPPING_PAPER_WRK_LOC]" listKey="pkCodeId" listValue="description"></s:select></td>
					  </tr>
					  --><!--<tr>
					  	 <td width="30%"><s:text name="garuda.cbbdefaults.lable.dateFormat"></s:text>:</td>
				       	 <td width="20%"><s:select name="cbbPojo.fkDateFormat"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DATE_FORMAT]" listKey="pkCodeId" listValue="description" disabled="true"></s:select></td>		
						 <td width="20%"><s:text name="garuda.cbbdefaults.lable.timeFormat"></s:text>:</td>
						 <td width="30%"><s:select name="cbbPojo.fkTimeFormat" headerKey="" headerValue="Select" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TIME_FORMAT]" listKey="pkCodeId" listValue="description"></s:select></td>
						
					 
					</tr>
					--></table>	
</fieldset>
</div>
</td>
</tr>
<!--<tr>
<td>
	<fieldset>
	
		<legend><s:text name="garuda.cbbdefaults.lable.localidassignment" /></legend>
					 <table cellpadding="0" cellspacing="0" width="100%" align="left">
								      <tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.localcbuidassignmentassgnusing"></s:text> </td>
								          <td width="20%"><s:select name="cbbPojo.localcbuassgnmentid" headerKey="" headerValue="Select" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LOCAL_ASSIGNMENT_ID]" listKey="pkCodeId" listValue="description" onchange="localassignId(this.value,'#localcbuassignid');"></s:select></td>
								      	  <td colspan="2"></td>
								      </tr>
								      <tr>
								      	<td colspan="4">
								      	
								       		<div id="localcbuassignid"  <s:if test="#request.autoMaticSequencingLocalAssgnId!=cbbPojo.localcbuassgnmentid">style="display: none;"</s:if>>
								       		<table>
								       		<tr>
								      		<td width="30%">
								   				<s:text name="garuda.cbbdefaults.lable.localcbuidprefix" />:
											</td>
								        	<td width="20%">
								        		<s:textfield name="cbbPojo.localCbuIdPrefix" />
								        	</td>
								        	<td width="30%">
								   				<s:text name="garuda.cbbdefaults.lable.localcbuidstrtnum" />:
											</td>
								        	<td width="20%">
								        		<s:textfield name="cbbPojo.localCbuIdStrtNum" cssClass="positive"/>
								        	</td>
								        	</tr>
								        	</table></div>
								        </td>
								      		
								      </tr>
								      <tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.localmaternalidassignmentassgnusing"></s:text> </td>
								          <td width="20%"><s:select name="cbbPojo.localmaternalassgnmentid" headerKey="-1" headerValue="Select" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LOCAL_ASSIGNMENT_ID]" listKey="pkCodeId" listValue="description" onchange="localassignId(this.value,'#localmaternalassignid');"></s:select></td>
								       	  <td colspan="2"></td>
								       </tr>
								     
								       <tr>
								       	<td colspan="4">
								       		<div id="localmaternalassignid" <s:if test="#request.autoMaticSequencingLocalAssgnId!=cbbPojo.localmaternalassgnmentid">style="display: none;"</s:if>>
								       		<table>
								       		<tr>
								      		<td width="30%">
								   				<s:text name="garuda.cbbdefaults.lable.localmaternalidprefix" />:
											</td>
								        	<td width="20%">
								        		<s:textfield name="cbbPojo.localMaternalIdPrefix" />
								        	</td>
								        	<td width="30%">
								   				<s:text name="garuda.cbbdefaults.lable.localmaternalidstrtnum" />:
											</td>
								        	<td width="20%">
								        		<s:textfield name="cbbPojo.localMaternalIdStrtNum" cssClass="positive"/>
								        	</td>
								        	</tr>
								        	</table></div>
								        </td>
								      </tr>
								      
								       
									</table>
    </fieldset>
    </td>
    </tr>
   --><tr>
      <td>
          <table bgcolor="#cccccc">
			  <tr valign="baseline">
			        <td ><jsp:include page="cb_esignature.jsp" flush="true"></jsp:include></td>
			        <td align="right">
			           	<input type="button" onclick="submitCBBForm()" disabled="disabled" id="submit" value="<s:text name="garuda.unitreport.label.button.submit"/>" / >
			        </td><td><input type="button" onclick="closeModal();" value="<s:text name="garuda.unitreport.label.button.cancel"/>" / >
			       </td>
              </tr>
           </table>
      </td>
   </tr>
  </table></div>
<s:if test="hasViewPermission(#request.viewCbbSetup)==false">
<script>
	$j('#tabCbbSetup').css('display','none');
</script>
</s:if>
<s:if test="hasViewPermission(#request.veiwCbbInfo)==false">
<script>
	$j('#tabcbinfo').css('display','none');
</script>
</s:if>
 <s:if test="hasViewPermission(#request.viewCbbpick)==false">
 <script>
	$j('#tabcbupck').css('display','none');
</script>
 </s:if>
 <s:if test="hasViewPermission(#request.viewDryShip)==false">
<script>
	$j('#tabdryship').css('display','none');
</script>
 </s:if>
  <s:if test="hasViewPermission(#request.ViewCbbDeflt)==false">
<script>
	$j('##request.ViewCbbDeflt').css('display','none');
</script>
 </s:if>
<s:if test="hasEditPermission(#request.viewCbbSetup)==false">
  <script>
     $j("#tabCbbSetup").each(function(){
    	 $j(this).find('input').attr('readonly', 'readonly');  
    	 $j(this).find('select').attr('disabled', true);  
    	 $j(this).find('input:radio').attr('disabled', true);
     });  
  </script> 
  <s:hidden name="cbbPojo.allowMember" ></s:hidden>
  <s:hidden name="cbbPojo.cdrUser" ></s:hidden>
  <s:hidden name="cbbPojo.cbuAssessment" ></s:hidden>
  <s:hidden name="cbbPojo.unitReport" ></s:hidden>
  <s:hidden name="cbbPojo.nmdpResearch" ></s:hidden>
  <s:hidden name="cbbPojo.isDomesticCBB" ></s:hidden>
</s:if>
<s:if test="hasEditPermission(#request.veiwCbbInfo)==false">
  <script>
     $j("#tabcbinfo").each(function(){
    	 $j(this).find('input').attr('readonly', 'readonly');    	
     });  
  </script> 
</s:if>
<s:if test="hasEditPermission(#request.viewCbbpick)==false">
  <script>
     $j("#tabcbupck").each(function(){
    	 $j(this).find('input').attr('readonly', 'readonly');  
    	 $j(this).find('textarea').attr('readonly', 'readonly'); 	
     });  
  </script> 
</s:if>
<s:if test="hasEditPermission(#request.viewDryShip)==false">
  <script>
     $j("#tabdryship").each(function(){
    	 $j(this).find('input').attr('readonly', 'readonly');    	
     });  
  </script> 
</s:if>
<s:if test="hasEditPermission(#request.ViewCbbDeflt)==false">
  <script>
     $j("#tabcbbDflt").each(function(){
    	 $j(this).find('input').attr('readonly', 'readonly'); 
    	 $j(this).find('select').attr('disabled', true);
     });  
  </script> 
   <s:hidden name="fkCbuStorage" ></s:hidden>
   <s:hidden name="fkCbuCollection" ></s:hidden>
   <s:hidden name="cbbPojo.defaultCTShipper" ></s:hidden>
   <s:hidden name="cbbPojo.defaultORShipper" ></s:hidden>
   <s:hidden name="cbbPojo.fkDefaultShippingPaperWorkLocation" ></s:hidden>
   <s:hidden name="rfreshPageThis" value="true" ></s:hidden>
</s:if>
<s:else>
    <s:hidden name="rfreshPageThis" value="false" ></s:hidden>
</s:else>
<script type="text/javascript">
     $j("#fkCbuStorageId option[value=<s:property value="#request.fkCbuStorage"/>]").attr("selected", "selected");
     $j("#fkCbuCollectionId option[value=<s:property value="#request.fkCbuCollection"/>]").attr("selected", "selected");
     function addCRIReqValue(){
    	 var confirm=$j("#criReqFlag").is(":checked");
    	 if(confirm==true){
    		 $j("#criReqFlag").val(true);
         }else{
        	 $j("#criReqFlag").val(false);
         }
     }
</script>
</s:form>
