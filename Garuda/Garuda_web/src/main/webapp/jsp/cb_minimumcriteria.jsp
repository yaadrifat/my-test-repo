<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.ordercomponent.util.GetResourceBundleData,com.velos.eres.web.user.UserJB" %>
<% GetResourceBundleData propData = new GetResourceBundleData();%>
<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<jsp:include page="./cb_track_session_logging.jsp" />
<%
	UserJB minuserB = (UserJB) session.getAttribute("currentUser");
	request.setAttribute("currentMinUser",minuserB.getUserId());
%>
<script type="text/javascript">
$j(document).ready(function(){
	OnLoadData();
	if('<s:property value="hasEditPermission(#request.updateMinCriteria)"/>'=="false"){
		$j("#minicri :radio").attr('disabled','disabled');
		$j("#minicri input[type=text]").attr('disabled','disabled');
		$j("#minicri input[type=password]").attr('disabled','disabled');
		$j("#trClose").show();
		$j("#trEsign").hide();
	}
});
function refreshMiniDiv(url,divname,formId){
	showprogressMgs();	
	$j(':input').removeAttr('disabled');
	$j(':radio').removeAttr('disabled');
		$j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        data : $j("#"+formId).serialize(),
	        success: function (result){
	        	$j('.ui-datepicker').html("");
	            var $response=$j(result);
	            var errorpage = $response.find('#errorForm').html();
	            if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{
	            	$j("#cbuMinimumCriteria").html(result);
	            	OnLoadData();
	            }
	        },
	        error: function (request, status, error) {
	        	closeprogressMsg();
	        	alert("Error " + error);
	            alert(request.responseText);
	        }
		});
	closeprogressMsg();
}
function OnLoadData(){
	if($j("#saveDoneMiniCri").val()!="true"){
		$j('<br></br>').insertBefore('#cbuMinimumCriteria');	
	}
	$j(".positive").numeric({ negative: false }, function() { alert('<%=propData.getPropertiesData("garuda.common.cbu.negativeValueAlert")%>'); this.value = ""; this.focus(); });
	$j('#pkcordId').val($j('#cordId').val());
	checkviab();
	checktncweg();
	radiocheck();
	btnchange();
	$j('#esign').val('');
	$j('#minisubmit').attr('disabled',true);
	if($j('#membercord').val()=="false"){
		if($j('#declarationresult'+$j('#declntmeets').val()).attr('checked')){
			$j('#tdcbustatus').show();
		}
	}
	checkval();
    var names = {};
    $j('#minicri :radio').each(function(index){
    	names[$j(this).attr('name')] = true;
    });
   	var count = 0;
    $j.each(names, function(key, value){
        if($j('input[name='+key+']').parent().parent().is(':visible')){
        	if(!$j('input[name='+key+']').is(':checked')){
	            count++;
	        }
	        if($j("#conformedminimumcriteria").val()=='true'){
        		$j('input[name='+key+']').attr('disabled',true);
	        }
        }
    });
	if($j('#tncweg').val()!="" && $j("#conformedminimumcriteria").val()=='true'){
	    $j('#tncweg').attr("disabled","disabled");
		$j('#postprocviab').attr("disabled","disabled");
    }
	var firstReviewer='declarationresult'+'<s:property value="cbuMinimumCriteriaPojo.declarationresult"/>';
	if($j('#'+firstReviewer).is(":checked") && $j('#saveinprogress').val()=='false' && $j("#conformedminimumcriteria").val()=='true'){
		if($j('#currentMinUser').val()==$j('#reviewBy').val()){
			$j('#'+firstReviewer).attr("disabled",true);
			$j("#trEsign").hide();
   			$j('#trClose').show();
		}
	}
	if($j("#isNewMiniChangeReq").val()=="true"){
		if(count==0 && $j('#tncweg').val()!="" && $j('#secondReviewer').is(":checked")){
			$j('#secondReviewer').attr("disabled",true);
	   		$j("#trEsign").hide();
	   		$j('#trClose').show();
	   	}
	}else{
	    if(count==0 && $j('#tncweg').val()!=""){
	    	$j("#trEsign").hide();
	   		$j('#trClose').show();
	   	}
	}

	$j('#cbuMinimumCriteria').bind( "dialogresize", function(event, ui) {
    	var dialogWidth = $j('#cbuMinimumCriteria').parent().width();
        var dialogHeight = $j('#cbuMinimumCriteria').parent().height();
    	var rightcontHeight = dialogHeight - 100;
    	var leftcontHeight = dialogHeight - 100;
        var leftcontWidth;
        var rightcontWidth;
        
    	var visibleFlag = false;
    	var divId = null;
    	 $j('.rightcont').each(function(){
             if($j(this).is(':visible')){
            	 visibleFlag = true;
            	 divId = $j(this).find('.portletstatus:first');
             }
         });
    	if(visibleFlag == true && divId!=null){
    		leftcontWidth = (dialogWidth/2) - 10;
    		rightcontWidth = leftcontWidth - 50;
    		$j(divId).width(rightcontWidth).height(rightcontHeight);
    		$j('#leftcont').width(leftcontWidth).height(leftcontHeight);
        }
    });
}
function setdivposition(parentID){
	var dialogHeight = $j('#cbuMinimumCriteria').parent().height();
	var dialogWidth = $j('#cbuMinimumCriteria').parent().width();
	var leftcontHeight = dialogHeight - 100;
	var rightcontHeight = dialogHeight - 100;
	var leftcontWidth = (dialogWidth/2)-10;
	var rightcontWidth = leftcontWidth - 50; 
	$j('#leftcont').width(leftcontWidth).height(leftcontHeight);
	if(parentID!="" && parentID!=null && parentID!='undefined'){
		$j(parentID).width(rightcontWidth).height(rightcontHeight);
	}else{
		$j('#rightcont').width(rightcontWidth).height(rightcontHeight);
	}
}
function showchildmsg(){
	if($j('#hlachildq'+$j('#minicretistus').val()).attr('checked') && $j('#hlacfm'+$j('#minicretistus').val()).attr('checked')){
		var obj=$j("#test").find('#hlachildq'+$j('#minicretistus').val()+'+label');
		$j(obj).addClass("notmet");
		$j('#hlacfmNote1').show();
	}else{
		var obj=$j("#test").find('#hlachildq'+$j('#minicretistus').val()+'+label');
		$j(obj).removeClass("notmet");
		$j('#hlacfmNote1').hide();
		
	}
	if($j('#ctchildq'+$j('#minicretistus').val()).attr('checked') && $j('#ctcompleted'+$j('#minicretistus').val()).attr('checked')){
		var obj=$j("#test").find('#ctchildq'+$j('#minicretistus').val()+'+label');
		$j(obj).addClass("notmet");
		$j('#ctchildmsg').show();
	}else{
		var obj=$j("#test").find('#ctchildq'+$j('#minicretistus').val()+'+label');
		$j(obj).removeClass("notmet");
		$j('#ctchildmsg').hide();
	}
	
	if($j('#antigenchildq'+$j('#minicretistus').val()).attr('checked') && $j('#antigenmch'+$j('#minicretistus').val()).attr('checked')){
		var obj=$j("#test").find('#antigenchildq'+$j('#minicretistus').val()+'+label');
		$j(obj).addClass("notmet");
		$j('#antigenchildmsg').show();
	}else{
		var obj=$j("#test").find('#antigenchildq'+$j('#minicretistus').val()+'+label');
		$j(obj).removeClass("notmet");
		$j('#antigenchildmsg').hide();
	}
	
	if($j('#unitrecpchildq'+$j('#minicretistus').val()).attr('checked') && $j('#unitrecp'+$j('#minicretistus').val()).attr('checked')){
		var obj=$j("#test").find('#unitrecpchildq'+$j('#minicretistus').val()+'+label');
		$j(obj).addClass("notmet");
		$j('#unitrecpchildmsg').show();
	}else{
		var obj=$j("#test").find('#unitrecpchildq'+$j('#minicretistus').val()+'+label');
		$j(obj).removeClass("notmet");
		$j('#unitrecpchildmsg').hide();
	}

	if($j('#cbuplannedchildq'+$j('#minicretistus').val()).attr('checked') && $j('#cbuplanned'+$j('#minicretistus').val()).attr('checked')){
		var obj=$j("#test").find('#cbuplannedchildq'+$j('#minicretistus').val()+'+label');
		$j(obj).addClass("notmet");
		$j('#cbuplannedchildmsg').show();
	}else{
		var obj=$j("#test").find('#cbuplannedchildq'+$j('#minicretistus').val()+'+label');
		$j(obj).removeClass("notmet");
		$j('#cbuplannedchildmsg').hide();
	}
	
	if($j('#patienttypchildq'+$j('#minicretistus').val()).attr('checked') && $j('#patienttyp'+$j('#minicretistus').val()).attr('checked')){
		var obj=$j("#test").find('#patienttypchildq'+$j('#minicretistus').val()+'+label');
		$j(obj).addClass("notmet");
		$j('#patienttypchildmsg').show();
	}else{
		var obj=$j("#test").find('#patienttypchildq'+$j('#minicretistus').val()+'+label');
		$j(obj).removeClass("notmet");
		$j('#patienttypchildmsg').hide();
	}

	if($j('#postprocesschildq'+$j('#minicretistus').val()).attr('checked') && $j('#tncweg').val()<1.0){
		var obj=$j("#test").find('#postprocesschildq'+$j('#minicretistus').val()+'+label');
		$j(obj).addClass("notmet");
		$j('#postprocesschildmsg').show();
		$j('#tncweg').removeClass('apObtained');
	}else{
		var obj=$j("#test").find('#postprocesschildq'+$j('#minicretistus').val()+'+label');
		$j(obj).removeClass("notmet");
		$j('#postprocesschildmsg').hide();
		if($j('input[name=cbuMinimumCriteriaPojo.postprocesschild]').is(':checked')){
			$j('#tncweg').addClass('apObtained');
		}else{
			$j('#tncweg').removeClass('apObtained');
		}
	}

	if($j("#isNewMiniChangeReq").val()=="true"){
		if($j('#viabchildq'+$j('#minicretistus').val()).attr('checked') && $j("#viabPerFlag"+$j("#viab_notaccept").val()).attr('checked')){
			var obj=$j("#test").find('#viabchildq'+$j('#minicretistus').val()+'+label');
			$j(obj).addClass("notmet");
			$j('#viabilitychildmsg').show();
			$j('#postprocviab').removeClass('apObtained');
		}else{
			var obj=$j("#test").find('#viabchildq'+$j('#minicretistus').val()+'+label');
			$j(obj).removeClass("notmet");
			$j('#viabilitychildmsg').hide();
			if($j('input[name=cbuMinimumCriteriaPojo.viabchild]').is(':checked') && $j('#postprocviab').hasClass("notmet")){
				$j('#postprocviab').addClass('apObtained');
			}else{
				$j('#postprocviab').removeClass('apObtained');
			}
		}
	}else{
		if($j('#viabchildq'+$j('#minicretistus').val()).attr('checked') && $j('#postprocviab').val()<75){
			var obj=$j("#test").find('#viabchildq'+$j('#minicretistus').val()+'+label');
			$j(obj).addClass("notmet");
			$j('#viabilitychildmsg').show();
			$j('#postprocviab').removeClass('apObtained');
		}else{
			var obj=$j("#test").find('#viabchildq'+$j('#minicretistus').val()+'+label');
			$j(obj).removeClass("notmet");
			$j('#viabilitychildmsg').hide();
			if($j('input[name=cbuMinimumCriteriaPojo.viabchild]').is(':checked')){
				$j('#postprocviab').addClass('apObtained');
			}else{
				$j('#postprocviab').removeClass('apObtained');
			}
		}
	}
}
function showchild(){

	if($j('#hlacfm'+$j('#minicretistus').val()).attr('checked')){
		$j('#hlachild').show();
	}else{
		$j('#hlachild').hide();
		$j('input[name=cbuMinimumCriteriaPojo.hlacnfmchild]').attr('checked',false);
	}

	if($j('#ctcompleted'+$j('#minicretistus').val()).attr('checked')){
		$j('#ctchild').show();
	}else{
		$j('#ctchild').hide();
		$j('input[name=cbuMinimumCriteriaPojo.ctcompletedchild]').attr('checked',false);
	}

	if($j('#antigenmch'+$j('#minicretistus').val()).attr('checked')){
		$j('#antigenchild').show();
	}else{
		$j('#antigenchild').hide();
		$j('input[name=cbuMinimumCriteriaPojo.antigenmatchchild]').attr('checked',false);
	}

	if($j('#unitrecp'+$j('#minicretistus').val()).attr('checked')){
		$j('#unitrecpchild').show();
	}else{
		$j('#unitrecpchild').hide();
		$j('input[name=cbuMinimumCriteriaPojo.unitrecpchild]').attr('checked',false);
	}

	if($j('#cbuplanned'+$j('#minicretistus').val()).attr('checked')){
		$j('#cbuplannedchild').show();
	}else{
		$j('#cbuplannedchild').hide();
		$j('input[name=cbuMinimumCriteriaPojo.cbuplannedchild]').attr('checked',false);
	}

	if($j('#patienttyp'+$j('#minicretistus').val()).attr('checked')){
		$j('#patienttypchild').show();
	}else{
		$j('#patienttypchild').hide();
		$j('input[name=cbuMinimumCriteriaPojo.patienttypchild]').attr('checked',false);
	}
	if($j("#isNewMiniChangeReq").val()=="true"){
		if($j("#viabPerFlag"+$j("#viab_notaccept").val()).attr('checked')){
			$j('#viabilitychild').show();
			$j('#postprocviabnote').show();
			$j("#test").find("#viabPerFlag"+$j("#viab_notaccept").val()+"+label").css('background-color','#D49DAB');
			$j("#test").find("#viabPerFlag"+$j("#viab_notaccept").val()+"+label").css('padding-top','0px');
		}else{
			$j('#viabilitychild').hide();
			$j('#postprocviabnote').hide();
			$j('input[name=cbuMinimumCriteriaPojo.viabchild]').attr('checked',false);
			$j("#test").find("#viabPerFlag"+$j("#viab_notaccept").val()+"+label").css('background-color','#FFFFFF');
			$j("#test").find("#viabPerFlag"+$j("#viab_notaccept").val()+"+label").css('padding-top','0px');
		}
	}
	showchildmsg();
}

function radiocheck(){
	if($j('#'+$j('#bacterialstatuspos').val()).attr('checked')){
		var obj=$j("#test").find('#'+$j('#bacterialstatuspos').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#bacterialUnres").text(responseTxt);
		$j('#bacterialNote1').show();
	}else{
		var obj=$j("#test").find('#'+$j('#bacterialstatuspos').val()+'+label');
		$j(obj).removeClass("notmet");
	}

	if($j('#'+$j('#bacterialStatusnt').val()).attr('checked')){
		var obj=$j("#test").find('#'+$j('#bacterialStatusnt').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#bacterialUnres").text(responseTxt);
		$j('#bacterialNote1').show();
	}else{
		var obj=$j("#test").find('#'+$j('#bacterialStatusnt').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#'+$j('#bacterialStatusnt').val()).attr('checked') && !$j('#'+$j('#bacterialstatuspos').val()).attr('checked')){
		$j('#bacterialNote1').hide();
	}

	if($j('#'+$j('#fungalStatuspos').val()).attr('checked')){
		var obj=$j("#test").find('#'+$j('#fungalStatuspos').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#fungalUnres").text(responseTxt);
		$j('#fungalNote1').show();
	}else{
		var obj=$j("#test").find('#'+$j('#fungalStatuspos').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#'+$j('#fungalStatuspos').val()).attr('checked')){
		$j('#fungalNote1').hide();
	}

	if($j('#'+$j('#beta_thalmia').val()).attr('checked')){
		var obj=$j("#test").find('#'+$j('#beta_thalmia').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#hemoUnres").text(responseTxt);
		$j('#hemoNote1').show();
	}else{
		var obj=$j("#test").find('#'+$j('#beta_thalmia').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if($j('#'+$j('#sickle_beta').val()).attr('checked')){
		var obj=$j("#test").find('#'+$j('#sickle_beta').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#hemoUnres").text(responseTxt);
		$j('#hemoNote1').show();
	}else{
		var obj=$j("#test").find('#'+$j('#sickle_beta').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if($j('#'+$j('#hemo_sick_dis').val()).attr('checked')){
		var obj=$j("#test").find('#'+$j('#hemo_sick_dis').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#hemoUnres").text(responseTxt);
		$j('#hemoNote1').show();
	}else{
		var obj=$j("#test").find('#'+$j('#hemo_sick_dis').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if($j('#'+$j('#alp_thal_sev').val()).attr('checked')){
		var obj=$j("#test").find('#'+$j('#alp_thal_sev').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#hemoUnres").text(responseTxt);
		$j('#hemoNote1').show();
	}else{
		var obj=$j("#test").find('#'+$j('#alp_thal_sev').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if($j('#'+$j('#mult_trait').val()).attr('checked')){
		var obj=$j("#test").find('#'+$j('#mult_trait').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#hemoUnres").text(responseTxt);
		$j('#hemoNote1').show();
	}else{
		var obj=$j("#test").find('#'+$j('#mult_trait').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#'+$j('#beta_thalmia').val()).attr('checked') && !$j('#'+$j('#sickle_beta').val()).attr('checked') && !$j('#'+$j('#hemo_sick_dis').val()).attr('checked') && !$j('#'+$j('#alp_thal_sev').val()).attr('checked') && !$j('#'+$j('#mult_trait').val()).attr('checked')){
		$j('#hemoNote1').hide();
	}

	if($j('#hivresult'+$j('#idmposiPk').val()).attr('checked')){
		var obj=$j("#test").find('#hivresult'+$j('#idmposiPk').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#hivresultUnres").text(responseTxt);
		$j('#hivresultNote1').show();
	}else{
		var obj=$j("#test").find('#hivresult'+$j('#idmposiPk').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#hivresult'+$j('#idmposiPk').val()).attr('checked')){
		$j('#hivresultNote1').hide();
	}

	if($j('#hbsag'+$j('#idmposiPk').val()).attr('checked')){
		var obj=$j("#test").find('#hbsag'+$j('#idmposiPk').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#hbsagUnres").text(responseTxt);
		$j('#hbsagNote1').show();
	}else{
		var obj=$j("#test").find('#hbsag'+$j('#idmposiPk').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#hbsag'+$j('#idmposiPk').val()).attr('checked')){
		$j('#hbsagNote1').hide();
	}

	if($j('#hivp'+$j('#idmposiPk').val()).attr('checked')){
		var obj=$j("#test").find('#hivp'+$j('#idmposiPk').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#hivpUnres").text(responseTxt);
		$j('#hivpNote1').show();
	}else{
		var obj=$j("#test").find('#hivp'+$j('#idmposiPk').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#hivp'+$j('#idmposiPk').val()).attr('checked')){
		$j('#hivpNote1').hide();
	}

	if($j('#hbvnat'+$j('#idmposiPk').val()).attr('checked')){
		var obj=$j("#test").find('#hbvnat'+$j('#idmposiPk').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#hbvnatUnres").text(responseTxt);
		$j('#hbvnatNote1').show();
	}else{
		var obj=$j("#test").find('#hbvnat'+$j('#idmposiPk').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#hbvnat'+$j('#idmposiPk').val()).attr('checked')){
		$j('#hbvnatNote1').hide();
	}

	if($j('#hcv'+$j('#idmposiPk').val()).attr('checked')){
		var obj=$j("#test").find('#hcv'+$j('#idmposiPk').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#hcvUnres").text(responseTxt);
		$j('#hcvNote1').show();
	}else{
		var obj=$j("#test").find('#hcv'+$j('#idmposiPk').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#hcv'+$j('#idmposiPk').val()).attr('checked')){
		$j('#hcvNote1').hide();
	}

	if($j('#tcruzichages'+$j('#idmposiPk').val()).attr('checked')){
		var obj=$j("#test").find('#tcruzichages'+$j('#idmposiPk').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#tcruzichagesUnres").text(responseTxt);
		$j('#tcruzichagesNote1').show();
	}else{
		var obj=$j("#test").find('#tcruzichages'+$j('#idmposiPk').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#tcruzichages'+$j('#idmposiPk').val()).attr('checked')){
		$j('#tcruzichagesNote1').hide();
	}

	if($j('#wnc'+$j('#idmposiPk').val()).attr('checked')){
		var obj=$j("#test").find('#wnc'+$j('#idmposiPk').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#wncUnres").text(responseTxt);
		$j('#wncNote1').show();
	}else{
		var obj=$j("#test").find('#wnc'+$j('#idmposiPk').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#wnc'+$j('#idmposiPk').val()).attr('checked')){
		$j('#wncNote1').hide();
	}
	if($j('#antihiv'+$j('#minicretistus').val()).attr('checked')){
		var obj=$j("#test").find('#antihiv'+$j('#minicretistus').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#antihivUnres").text(responseTxt);
		$j('#antihivNote1').show();
	}else{
		var obj=$j("#test").find('#antihiv'+$j('#minicretistus').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#antihiv'+$j('#minicretistus').val()).attr('checked')){
		$j('#antihivNote1').hide();
	}
	if($j('#hivpnat'+$j('#minicretistus').val()).attr('checked')){
		var obj=$j("#test").find('#hivpnat'+$j('#minicretistus').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#hivpnatUnres").text(responseTxt);
		$j('#hivpnatNote1').show();
	}else{
		var obj=$j("#test").find('#hivpnat'+$j('#minicretistus').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#hivpnat'+$j('#minicretistus').val()).attr('checked')){
		$j('#hivpnatNote1').hide();
	}
	
	if($j('#hbsagc'+$j('#minicretistus').val()).attr('checked')){
		var obj=$j("#test").find('#hbsagc'+$j('#minicretistus').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#hbsagcUnres").text(responseTxt);
		$j('#hbsagcNote1').show();
	}else{
		var obj=$j("#test").find('#hbsagc'+$j('#minicretistus').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#hbsagc'+$j('#minicretistus').val()).attr('checked')){
		$j('#hbsagcNote1').hide();
	}
	if($j('#antihcv'+$j('#minicretistus').val()).attr('checked')){
		var obj=$j("#test").find('#antihcv'+$j('#minicretistus').val()+'+label');
		$j(obj).addClass("notmet");
		var responseTxt=$j(obj).text();
		$j("#antihcvcUnres").text(responseTxt);
		$j('#antihcvcNote1').show();
	}else{
		var obj=$j("#test").find('#antihcv'+$j('#minicretistus').val()+'+label');
		$j(obj).removeClass("notmet");
	}
	if(!$j('#antihcv'+$j('#minicretistus').val()).attr('checked')){
		$j('#antihcvcNote1').hide();
	}

	if($j('#hlachildq'+$j('#minicretistus').val()).attr('checked')){
		$j('#hlacfmNote1').show();
	}else{
		var obj=$j("#test").find('#hlachildq'+$j('#minicretistus').val()+'+label');
		$j(obj).removeClass("notmet");
		
	}
	if(!$j('#hlachildq'+$j('#minicretistus').val()).attr('checked')){
		$j('#hlacfmNote1').hide();
	}
	if(!$j('#ctcompleted'+$j('#minicretistus').val()).attr('checked')){
		$j('#ctcompletedNote1').hide();
	}
	
	if(!$j('#antigenmch'+$j('#minicretistus').val()).attr('checked')){
		$j('#antigenmchNote1').hide();
	}
	
	if(!$j('#unitrecp'+$j('#minicretistus').val()).attr('checked')){
		$j('#unitrecpNote1').hide();
	}

	if(!$j('#cbuplanned'+$j('#minicretistus').val()).attr('checked')){
		$j('#cbuplannedNote1').hide();
	}

	if(!$j('#patienttyp'+$j('#minicretistus').val()).attr('checked')){
		$j('#patienttypNote1').hide();
	}
}
function resetfields(){
	$j('#minidecl input[type=radio]').each(function(index,el) {
		$j(el).removeAttr('checked');
		$j(el).attr('disabled',true);
	});
	$j('#esign').val('');
}
function checkval(){
    var names = {};
    $j('#test :radio').each(function(){
    	names[$j(this).attr('name')] = true;
    });
   	var count = 0;
    $j.each(names, function(key, value){
        if($j('input[name='+key+']').parent().parent().is(':visible')){
        	if(!$j('input[name='+key+']').is(':checked')){
        		if(($j('input[name='+key+']').attr("name"))== "cbuMinimumCriteriaPojo.viabTestTiming"){
	            	if($j('#postprocviab').val()!="" && $j('#postprocviab').val()!=null){
        				count++;
	            	}else{
	            		//count will not increase for Viability timing radio buttons while viability is empty
	            	}
        		}else{
        			count++;
        		}
	        }
        }
    });
    var notMetLen = $j('#test').find('.notmet').length;
    var obtain = $j('#test').find('.apObtained').length;
    notMetLen = parseInt(notMetLen - obtain);
    if(count==0){
		if($j('#tncweg').val()!=""){
			if(notMetLen==0){
				$j('input[name=cbuMinimumCriteriaPojo.declarationresult]').attr('disabled',false);
			}else{
				$j('#declarationresult'+$j('#declmeets').val()).attr('disabled',true);
				$j('#declarationresult'+$j('#declntmeets').val()).attr('disabled',false);
			}
		}else{
			$j('input[name=cbuMinimumCriteriaPojo.declarationresult]').attr('disabled',true);
			$j('input[name=cbuMinimumCriteriaPojo.declarationresult]').attr('checked',false);
			$j('#btnsave').text('<%=propData.getPropertiesData("garuda.common.save")%>');
			$j("#confrmCritia").hide();
		}
	}else if(count!=0){
		$j('input[name=cbuMinimumCriteriaPojo.declarationresult]').attr('disabled',true);
		$j('input[name=cbuMinimumCriteriaPojo.declarationresult]').attr('checked',false);
		$j('#btnsave').text('<%=propData.getPropertiesData("garuda.common.save") %>');
		$j("#confrmCritia").hide();
	}
}

function btnchange(){
	var chkedLen=$j('#minidecl :radio:checked').length;
	if(chkedLen==null || chkedLen<1){
		$j("#btnsave").text('<%=propData.getPropertiesData("garuda.common.save")%>');
		$j("#saveinprogress").val(true);
		$j("#conformedminimumcriteria").val(false);
	}
	else{
		$j("#btnsave").text('<%=propData.getPropertiesData("garuda.common.lable.confirm")%>');
		$j("#saveinprogress").val(false);
		$j("#conformedminimumcriteria").val(true);   
	}
	if($j('#membercord').val()=="false"){
		if($j('#declarationresult'+$j('#declntmeets').val()).attr('checked')){
				$j('#tdcbustatus').show();
			}else{
				$j('#tdcbustatus').hide();
			}
	}
}
function cleardiv(){
	$j('#MC_divlabParent').hide();
	$j('#MC_dividmParent').hide();
	$j('#MC_divhlaParent').hide();
	$j('#MC_MinishipmentInfo').hide();
}
function getRighSideForm(divId,sourceId){
	showprogressMgs();
	cleardiv();
	var divId = divId;
	var parentID = $j('#'+divId).parent();
	if(sourceId == 'hla'){
		var url = "load_cb_load_hla_data";
		url+= "?cdrCbuPojo.cordID=<s:property value='cdrCbuPojo.cordID'/>&dataToBeLoaded=38&moduleEntityId=<s:property value='cdrCbuPojo.cordID'/>&moduleEntityType=<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU'/>&moduleEntityIdentifier=<s:property value='cdrCbuPojo.registryId' />&moduleEntityEventCode=<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_MINI_HLA'/>";
		loadPageByGetRequset(url,divId);	
		$j("#MC_divhlaParent").show();
	}
	if(sourceId == 'labsummary'){
		var url="getLabSummary?cdrCbuPojo.cordID=<s:property value='cdrCbuPojo.cordID' />&esignFlag=review&orderId="+$j("#orderId").val()+"&orderType="+$j("#orderType").val()+"&flagisMinimumCri=true&moduleEntityId=<s:property value='cdrCbuPojo.cordID'/>&moduleEntityType=<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU'/>&moduleEntityIdentifier=<s:property value='cdrCbuPojo.registryId' />&moduleEntityEventCode=<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_MINI_LABSUMMARY'/>";
		loadPageByGetRequset(url,divId);	
		$j('#'+divId).find('#loadLabSummaryClinicalNoteDiv').find('a').each(function(){
	           $j(this).hide();	
	    });
		$j("#MC_divlabParent").show();
		$j("#detaillabDivData").attr("style","overflow:none;margin-right:5px;");
	}
	if(sourceId == 'idm'){
		var url = "load_cb_load_idm_data";
		url+= "?cdrCbuPojo.cordID=<s:property value='cdrCbuPojo.cordID'/>&dataToBeLoaded=290&formDataToBeLoaded=8";
		loadPageByGetRequset(url,divId);
		var url1 = 'getDynamicFrm?formId=IDM&isMinimumCriteria=1&iseditable=1&entityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID"/>&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU"/>&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_MINI_IDM"/>';	
		$j.ajax({
	        type: "POST",
	        url: url1,
	        async:false,
	        success: function (result){
	           $j('#cbuMinimumCriteria').find("#idmtestinfoforcbu").html(result);
	           $j("#MC_dividmParent").show();
	        }
		});
	}
	if(sourceId == 'shipInfo'){
		$j("#MC_MinishipmentInfo").show();
	}
	if(sourceId != 'idm'){
		$j("#"+divId+" input[type=text]").attr('disabled',true);
		$j("#"+divId+" select").attr('disabled',true);
		$j("#"+divId+" input[type=radio]").attr('disabled',true);
		$j("#"+divId+" input[type=checkbox]").attr('disabled',true);
		if(sourceId !='labsummary'){
			$j('#'+divId+' a').attr('href',"#");
			$j('#'+divId+' a').removeAttr('onclick');
		}
	}
	$j('#submit').attr('id','dummyButton');
	$j('#'+divId+' button').hide();
	$j("#"+divId+" input[type=button]").hide();
	$j('#eSignTable').hide();
	setdivposition(parentID);
	$j('#'+divId).find('.portlet-header').each(function(){
		$j(this).find('span').removeAttr('onclick');
		if($j(this).find('span').is(".ui-icon-triangle-1-s") || $j(this).is(".ui-icon-triangle-1-e")){
			$j(this).bind('click',function(){
				$j(this).toggleClass("ui-icon-triangle-1-s").toggleClass("ui-icon-triangle-1-e");
				var nextDiv = $j(this).next('div');
				if($j(this).find('span').is(".ui-icon-triangle-1-s")){
					$j(nextDiv).show();
				}else if($j(this).find('span').is(".ui-icon-triangle-1-e")){
					$j(nextDiv).hide();
				}
			});
		}
	});
	closeprogressMsg();
}
function checkviab(){
	
		if($j('#postprocviab').val()<75 && ($j('#postprocviab').val()!=null && $j('#postprocviab').val()!="")){
			if($j("#isNewMiniChangeReq").val()=="false"){
				$j('#postprocviabnote').show();
				$j('#viabilitychild').show();
			}else if($j("#isNewMiniChangeReq").val()=="true"){
				$j("#viabPerFlag"+$j("#viab_accept").val()).attr('checked',false);
			}
			if($j('#postprocviab').val()!=null && $j('#postprocviab').val()!=""){
				$j('#postprocviab').addClass('notmet');
			}
		}else if(($j('#postprocviab').val()!=null && $j('#postprocviab').val()!="") && ($j('#postprocviab').val()>=75)){
			if($j("#isNewMiniChangeReq").val()=="false"){
				$j('#postprocviabnote').hide();
				$j('#viabilitychild').hide();
				$j('#viabilitychildmsg').hide();
			}else if($j("#isNewMiniChangeReq").val()=="true"){
				$j("#viabPerFlag"+$j("#viab_accept").val()).attr('checked',true);
			}
			$j('#postprocviab').removeClass('notmet');
			$j('input[name=cbuMinimumCriteriaPojo.viabchild]').removeAttr('checked');
		}
	
	showchild();
	checkval();
}
function checktncweg(){
	if($j('#tncweg').val()!=""){
		if($j('#tncweg').val()<1.0){
			$j('#tncnote').show();
			$j('#postprocesschild').show();
		//	$j('#tncweg').css('background-color','#D49DAB');
			$j('#tncweg').addClass('notmet');
		}else{
			$j('#tncnote').hide();
			$j('#postprocesschild').hide();
			$j('#postprocesschildmsg').hide();
			//$j('#tncweg').css('background-color','#DFDFDF');
			$j('#tncweg').removeClass('notmet');
			$j('input[name=cbuMinimumCriteriaPojo.postprocesschild]').removeAttr('checked');
		}
	}
	showchild();
	checkval();
}
function saveorconfirmresult(cordId,formid){
	$j("#cordID").val(cordId);
	$j('#minicritestatus').val(true);
	if($j("#membercord").val()=="false"){
		if($j('#declarationresult'+$j('#declntmeets').val()).attr('checked')){
			if($j('#cbuStatus').val()!=""){
				$j('.checklist').removeAttr('disabled');
				//document.getElementById(formid).submit();
				refreshMiniDiv('saveminicriteria',formid,formid);
				if($j("#isNewMiniChangeReq").val()=="true" && $j('#saveinprogress').val()=='false' && $j("#conformedminimumcriteria").val()=='true'){
					$j("#trEsign").hide();
		   			$j('#trClose').show();
				}
			}else{
				$j('#tdcbustatus_err').show();
			}
		}else{
			$j('.checklist').removeAttr('disabled');
			//document.getElementById(formid).submit();
			refreshMiniDiv('saveminicriteria',formid,formid);
			if($j("#isNewMiniChangeReq").val()=="true"){
				$j("#trEsign").hide();
	   			$j('#trClose').show();
			}
		}
	}else{
		$j('.checklist').removeAttr('disabled');
		//document.getElementById(formid).submit();
		refreshMiniDiv('saveminicriteria',formid,formid);
		if($j("#isNewMiniChangeReq").val()=="true" && $j('#saveinprogress').val()=='false' && $j("#conformedminimumcriteria").val()=='true'){
			
			$j("#trEsign").hide();
   			$j('#trClose').show();
		}
	}
}

function checkcheckbox(value){
	$j('#'+value).val(true);
}
function checkcbustus(){
	if($j('cbuStatus').val()!=""){
		$j('#tdcbustatus_err').hide();
	}else{
		$j('#tdcbustatus_err').show();
	}
}
function goback(){
	closeModals('cbuMinimumCriteria');
}
function makeEditable(){
	if($j("#isNewMiniChangeReq").val()=="true"){
		if($j('#secondReviewer').is(":checked")){
		jProceed('<s:text name="garuda.minimumcriteria.editwarningmsg"/>', '<s:text name="garuda.minimumcriteria.warningtitle"/>',
			function(r) {
				if (r == false) {
				}else if(r == true){
					$j(".tempvalue").removeAttr('disabled');
					$j("#trClose").hide();
					$j("#trEsign").show();
					$j('#confrmCritia').hide();
					$j('#secondReviewRow').hide();
					$j('#secondReviewer').removeAttr('checked');
					$j('input[name=cbuMinimumCriteriaPojo.declarationresult]').removeAttr('checked');
					checkval();
				}
		});
		}
		else{
			if($j("#conformedminimumcriteria").val()=="true" && $j("#saveinprogress").val()=="false"){
			jProceed('<s:text name="garuda.minimumcriteria.editwarningmsg1reviewer"/>', '<s:text name="garuda.minimumcriteria.warningtitle"/>',
					function(r) {
						if (r == false) {
						}else if(r == true){
							$j(".tempvalue").removeAttr('disabled');
							$j("#trClose").hide();
							$j("#trEsign").show();
							$j('#confrmCritia').hide();
							$j('#secondReviewRow').hide();
							$j('#secondReviewer').removeAttr('checked');
							$j('input[name=cbuMinimumCriteriaPojo.declarationresult]').removeAttr('checked');
							checkval();
						}
				});
			}
		}
	}else{
		$j(".tempvalue").removeAttr('disabled');
		$j("#trClose").hide();
		$j("#trEsign").show();
		$j('#confrmCritia').hide();
		$j('input[name=cbuMinimumCriteriaPojo.declarationresult]').removeAttr('checked');
		checkval();
	}
}
</script>

<s:form id="minicri" name="minicri">
	<s:hidden name="cdrCbuPojo.cordID" id="cordId" />
	<s:hidden name="cordID" id="cordID" value="%{cdrCbuPojo.cordID}"/>
	<s:hidden name="pkcordId" id="pkcordId" value="%{cdrCbuPojo.cordID}"/>
	<s:hidden name="orderId" id="orderId"></s:hidden>
	<s:hidden name="orderType" id="orderType"></s:hidden>
	<s:hidden name="bacterialStatuspos" id="bacterialstatuspos" />
	<s:hidden name="bacterialStatusnt" id="bacterialStatusnt" />
	<s:hidden name="fungalStatuspos" id="fungalStatuspos" />
	<s:hidden name="hemoglobinhomo" id="hemoglobinhomo" />
	<s:hidden name="hemoglobinhetro" id="hemoglobinhetro" />
	<s:hidden name="idmposiPk" id="idmposiPk" />
	<s:hidden name="idmindeterPk" id="idmindeterPk" />
	<s:hidden name="minicretistus" id="minicretistus" />
	<s:hidden name="membercord" id="membercord" />
	<s:hidden name="minideclntmeetspk" id="declntmeets"/>
	<s:hidden name="minideclmeetspk" id="declmeets"/>
	<s:hidden name="minicritestatus" id="minicritestatus"/>
	<s:hidden name="beta_thalmia" id="beta_thalmia" />
	<s:hidden name="sickle_beta" id="sickle_beta" />
	<s:hidden name="hemo_sick_dis" id="hemo_sick_dis" />
	<s:hidden name="alp_thal_sev" id="alp_thal_sev" />
	<s:hidden name="mult_trait" id="mult_trait" />
	<s:hidden name="pktnctest" id="pktnctest"></s:hidden>
	<s:hidden name="pkvaibility" id="pkvaibility"/>
	<s:hidden name="cbuMinimumCriteriaPojo.pkcordminimumcriteria"/>
	<s:hidden name="cbuMinimumCriteriaPojo.fkCordId" value="%{cdrCbuPojo.cordID}"/>
	<s:hidden name="cbuMinimumCriteriaPojo.fkOrderId" value="%{orderId}"/>
	<s:hidden name="cbuMinimumCriteriaPojo.saveinprogress" id="saveinprogress"/>
	<s:hidden name="cbuMinimumCriteriaPojo.conformedminimumcriteria" id="conformedminimumcriteria"/>	
	<s:hidden name="minimumCriteriaTempPojo.pkMinimumCritField"/>
	<s:hidden name="cbuMinimumCriteriaPojo.fkRecipient" id="fkRecipient"/>
	<s:hidden name="cbuMinimumCriteriaPojo.patentId" id="MinipatentId"/>
	<s:hidden name="cbuMinimumCriteriaPojo.ReviewBy" id="reviewBy"/>
	<s:hidden name="cbuMinimumCriteriaPojo.ReviewOn" id="reviewOn"/>
	<s:hidden name="saveDoneMiniCri" id="saveDoneMiniCri"/>
	<s:hidden name="currentMinUser" id="currentMinUser" value="%{#request.currentMinUser}"></s:hidden>
	<s:hidden id="isNewMiniChangeReq" value="%{isNewMiniChangeReq}"></s:hidden>
	<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VIAB_DECLARE]">
		<s:hidden id="%{subType}" value="%{pkCodeId}"></s:hidden>
	</s:iterator>
	
	<div id="minimumcriteriaparentdiv">
		<div style="overflow: hidden;">
		<table width="100%">
		<tr>
		<td width="50%" valign="top">
		<div id='leftcont'>
		<table>
			<tr>
				<td>
					<div id="test">
						<table>
							<tr>
								<td colspan="3"><strong><%=propData.getPropertiesData("garuda.minimumcriteria.label.reviewmsg")%></strong>:</td>
							</tr>
							<tr>
								<td width="5%">
									<img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.bacterialtest")%>')"onmouseout="return nd()">
								</td>
								<td width="35%">
									<u onclick="getRighSideForm('MC_divlabcont','labsummary');" style="cursor: pointer; padding-right: 5px;vertical-align: top;">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialtesting")%>
									</u>
								</td>
								<td width="60%">
									<s:if test="cdrCbuPojo.bacterialResult==null || cdrCbuPojo.bacterialResult==-1">
										<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS]" name="minimumCriteriaTempPojo.bacterialResult" cssClass="checklist tempvalue"
									 			 listKey="pkCodeId" listValue="description" id="" onclick="radiocheck();checkval();"/>
									</s:if>
									<s:else>
										<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS]" name="minimumCriteriaTempPojo.bacterialResult" cssClass="checklist"
									 			 listKey="pkCodeId" listValue="description" id="" disabled="true" onclick="radiocheck();checkval();" value="%{cdrCbuPojo.bacterialResult}"/>
									</s:else>
								 </td>
							</tr>
							<tr id="bacterialNote1" style="display: none;">
								<td></td>
								<td colspan="2">
									<span class="error">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
									</span>
									<span class="error" id="bacterialUnres">
									</span>
									<span class="error">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
									</span>
								</td>
							</tr>
							<tr>
								<td>
									<img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.fungaltest")%>')" onmouseout="return nd()">
								</td>
								<td>
									<u onclick="getRighSideForm('MC_divlabcont','labsummary');" style="cursor: pointer; padding-right: 5px;vertical-align: top;">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.fungaltesting")%>
									</u>
								</td>
								<td>
									<s:if test="cdrCbuPojo.fungalResult==null || cdrCbuPojo.fungalResult == -1">
										<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS]" name="minimumCriteriaTempPojo.fungalResult" cssClass="checklist tempvalue"
									 			 listKey="pkCodeId" listValue="description" id="" onclick="radiocheck();checkval();"/>
									</s:if>
									<s:else>
										<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS]" name="minimumCriteriaTempPojo.fungalResult" cssClass="checklist"
									 			 listKey="pkCodeId" listValue="description" disabled="true" id="" onclick="radiocheck();checkval();" value="%{cdrCbuPojo.fungalResult}"/>
									</s:else>
								 </td>
							</tr>
							<tr id="fungalNote1" style="display: none;">
								<td></td>
								<td colspan="2">
									<span class="error">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
									</span>
									<span class="error" id="fungalUnres">
									</span>
									<span class="error">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
									</span>
								</td>
							</tr>
							<tr>
								<td valign="top">
								<img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<s:text name="garuda.minimumcriteria.helpmesg.hemoglob"/>')" onmouseout="return nd()">
								</td>
								<td valign="top">
									<u onclick="getRighSideForm('MC_divlabcont','labsummary');" style="cursor: pointer; padding-right: 5px;">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.hemoglobin")%> 
									</u>
								</td>
								<td class="breakcheckbox">
									<s:if test="%{cdrCbuPojo.hemoglobinScrn == null || cdrCbuPojo.hemoglobinScrn==-1 }">
										<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN]">
					    					<s:radio list="#{'pkCodeId':'description'}" name="minimumCriteriaTempPojo.hemoglobresult" cssClass="checklist tempvalue"
													listKey="pkCodeId"  listValue="description" onclick="radiocheck();checkval();" id=""/><br/>
										</s:iterator>
									</s:if>
									<s:else>
										<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN]">
					    					<s:radio list="#{'pkCodeId':'description'}" name="minimumCriteriaTempPojo.hemoglobresult" value="%{cdrCbuPojo.hemoglobinScrn}" cssClass="checklist"
													listKey="pkCodeId"  listValue="description" onclick="radiocheck();checkval();" id="" disabled="true"/><br/>
										</s:iterator>
									</s:else>
								</td>
							</tr>
							<tr id="hemoNote1" style="display: none;">
								<td></td>
								<td colspan="2">
									<span class="error">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
									</span>
									<span class="error" id="hemoUnres">
									</span>
									<span class="error">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
									</span>
								</td>
							</tr>
							<tr>
								<td></td>
								<td colspan="2">
									<u onclick="getRighSideForm('MC_dividmcont','idm');" style="cursor: pointer;">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.idms")%>
									</u>
								</td>
							</tr>
							<tr>
								<td></td>
								<td colspan="2">
									<table class="breakcheckbox" width="100%">
										<s:set name="flag1" value="0" scope="request"></s:set>
										<s:set name="flag1name" value="" scope="request"></s:set>
										<s:iterator value="%{#request.mrqFormList}" var="rowval">
											<s:if test="%{#rowval[20]=='anti_hiv_1_2_ind' || #rowval[20]=='anti_hiv_1_2_o_ind'}">
												<s:if test="%{#rowval[15]!=null && #rowval[15]!='' && #rowval[15]==idmposiPk}">
													<s:set name="flag1" value="1" scope="request"></s:set>
													<s:set name="flag1name" value="%{#rowval[20]}" scope="request"></s:set>
												</s:if>
											</s:if>
										</s:iterator>
										<s:if test="%{#request.flag1!=1}">
											<s:iterator value="%{#request.mrqFormList}" var="rowval">
												<s:if test="%{#rowval[20]=='anti_hiv_1_2_ind' || #rowval[20]=='anti_hiv_1_2_o_ind'}">
													<s:if test="%{#rowval[15]!=null && #rowval[15]!='' && #rowval[15]==idmnegiPk}">
														<s:set name="flag1" value="1" scope="request"></s:set>
														<s:set name="flag1name" value="%{#rowval[20]}" scope="request"></s:set>
													</s:if>
												</s:if>
											</s:iterator>
										</s:if>
										<s:if test="%{#request.flag1!=1}">
											<s:iterator value="%{#request.mrqFormList}" var="rowval">
												<s:if test="%{#rowval[20]=='anti_hiv_1_2_ind' || #rowval[20]=='anti_hiv_1_2_o_ind'}">
													<s:if test="%{#rowval[15]!=null && #rowval[15]!='' && #rowval[15]==pkTestNotDone}">
														<s:set name="flag1" value="1" scope="request"></s:set>
														<s:set name="flag1name" value="%{#rowval[20]}" scope="request"></s:set>
													</s:if>
												</s:if>
											</s:iterator>
										</s:if>
										<s:if test="%{#request.flag1name==null}">
											<s:set name="flag1name" value="%{'anti_hiv_1_2_ind'}" scope="request"></s:set>
										</s:if>
										<s:iterator value="%{#request.mrqFormList}" var="rowval">
										<s:if test="%{#rowval[20]==#request.flag1name}">
											<tr>
												<td><img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.antihiv1/2")%>')"onmouseout="return nd()"></td>
												<td width="42%">
													<span style="padding-right: 5px;vertical-align: top;">
													<s:if test="%{#rowval[20]==#request.flag1name}">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.antihiv1/2")%>
													</s:if>
													</span>
												</td>
												<td width="58%">
														<s:if test="%{#rowval[15]==null}">
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.hivresult" list="#{'pkCodeId':'description'}" listKey="pkCodeId" cssClass="checklist tempvalue"
								    										listValue="description" id="hivresult" onclick="radiocheck();checkval();"/><br/>
																	</s:iterator>
														</s:if>
														<s:else>
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.hivresult" list="#{'pkCodeId':'description'}" listKey="pkCodeId" disabled="true" cssClass="checklist"
								    									listValue="description" id="hivresult" onclick="radiocheck();checkval();" value="%{#rowval[15]}"/><br/>
															</s:iterator>
														</s:else>
												</td>
											</tr>
											<tr id="hivresultNote1" style="display: none;">
												<td colspan="3">
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
													</span>
													<span class="error" id="hivresultUnres">
													</span>
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
													</span>
												</td>
											</tr>
										</s:if>
										</s:iterator>
										<s:set name="flag1" value="0" scope="request"></s:set>
										<s:set name="flag1name" value="" scope="request"></s:set>
										<s:iterator value="%{#request.mrqFormList}" var="rowval">
											<s:if test="%{#rowval[20]=='hiv_antigen_rslt' || #rowval[20]=='nat_hiv_react_ind' || #rowval[20]=='nat_hiv_1_hcv_hbv_mpx_ind'}">
												<s:if test="%{#rowval[15]!=null && #rowval[15]!='' && #rowval[15]==idmposiPk}">
													<s:set name="flag1" value="1" scope="request"></s:set>
													<s:set name="flag1name" value="%{#rowval[20]}" scope="request"></s:set>
												</s:if>
											</s:if>
										</s:iterator>
										<s:if test="%{#request.flag1!=1}">
											<s:iterator value="%{#request.mrqFormList}" var="rowval">
												<s:if test="%{#rowval[20]=='hiv_antigen_rslt' || #rowval[20]=='nat_hiv_react_ind' || #rowval[20]=='nat_hiv_1_hcv_hbv_mpx_ind'}">
													<s:if test="%{#rowval[15]!=null && #rowval[15]!='' && #rowval[15]==idmnegiPk}">
														<s:set name="flag1" value="1" scope="request"></s:set>
														<s:set name="flag1name" value="%{#rowval[20]}" scope="request"></s:set>
													</s:if>
												</s:if>
											</s:iterator>
										</s:if>
										<s:if test="%{#request.flag1!=1}">
											<s:iterator value="%{#request.mrqFormList}" var="rowval">
												<s:if test="%{#rowval[20]=='hiv_antigen_rslt' || #rowval[20]=='nat_hiv_react_ind' || #rowval[20]=='nat_hiv_1_hcv_hbv_mpx_ind'}">
													<s:if test="%{#rowval[15]!=null && #rowval[15]!=''  && #rowval[15]==pkTestNotDone}">
														<s:set name="flag1" value="1" scope="request"></s:set>
														<s:set name="flag1name" value="%{#rowval[20]}" scope="request"></s:set>
													</s:if>
												</s:if>
											</s:iterator>
										</s:if>
										<s:if test="%{#request.flag1name==null}">
											<s:set name="flag1name" value="%{'hiv_antigen_rslt'}" scope="request"></s:set>
										</s:if>
										<s:iterator value="%{#request.mrqFormList}" var="rowval">
										<s:if test="%{#rowval[20]==#request.flag1name}">
											<tr>
												<td>
														<img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.hivp24hivnat")%>')"onmouseout="return nd()">
												</td>
												<td width="42%">
													<span style="padding-right: 5px;vertical-align: top;">
													<s:if test="%{#rowval[20]==#request.flag1name}">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.hivp24orhivnat")%>
													</s:if>
													</span>
												</td>
												<td width="58%">
														<s:if test="%{#rowval[15]==null}">
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.hivp" list="#{'pkCodeId':'description'}" listKey="pkCodeId" cssClass="checklist tempvalue"
								    										listValue="description" id="hivp" onclick="radiocheck();checkval();"/><br/>
															</s:iterator>
														</s:if>
														<s:else>
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.hivp" list="#{'pkCodeId':'description'}" listKey="pkCodeId" disabled="true" cssClass="checklist"
								    									listValue="description" id="hivp" onclick="radiocheck();checkval();" value="%{#rowval[15]}"/><br/>
															</s:iterator>
														</s:else>
												</td>
											</tr>
											<tr id="hivpNote1" style="display: none;">
												<td colspan="3">
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
													</span>
													<span class="error" id="hivpUnres">
													</span>
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
													</span>
												</td>
											</tr>
										</s:if>
										</s:iterator>
										<s:iterator value="%{#request.mrqFormList}" var="rowval">
										<s:if test="%{#rowval[20]=='hbsag_react_ind'}">
											<tr>
												<td><img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.hbsag")%>')"onmouseout="return nd()"></td>
												<td width="42%">
													<span style="padding-right: 5px;vertical-align: top;">
													<s:if test="%{#rowval[20]=='hbsag_react_ind'}">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.hbsag")%>
													</s:if>
													</span>
												</td>
												<td width="58%">
														<s:if test="%{#rowval[15]==null}">
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.hbsag" list="#{'pkCodeId':'description'}" listKey="pkCodeId" cssClass="checklist tempvalue"
								    										listValue="description" id="hbsag" onclick="radiocheck();checkval();"/><br/>
															</s:iterator>
														</s:if>
														<s:else>
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.hbsag" list="#{'pkCodeId':'description'}" listKey="pkCodeId" disabled="true" cssClass="checklist"
								    									listValue="description" id="hbsag" onclick="radiocheck();checkval();" value="%{#rowval[15]}"/><br/>
															</s:iterator>
														</s:else>
												</td>
											</tr>
											<tr id="hbsagNote1" style="display: none;">
												<td colspan="3">
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
													</span>
													<span class="error" id="hbsagUnres">
													</span>
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
													</span>
												</td>
											</tr>
										</s:if>
										</s:iterator>
										<s:set name="flag1" value="0" scope="request"></s:set>
										<s:set name="flag1name" value="" scope="request"></s:set>
										<s:iterator value="%{#request.mrqFormList}" var="rowval">
											<s:if test="%{#rowval[20]=='nat_hbv_react_ind' || #rowval[20]=='nat_hiv_1_hcv_hbv_mpx_ind'}">
												<s:if test="%{#rowval[15]!=null && #rowval[15]!='' && #rowval[15]==idmposiPk}">
													<s:set name="flag1" value="1" scope="request"></s:set>
													<s:set name="flag1name" value="%{#rowval[20]}" scope="request"></s:set>
												</s:if>
											</s:if>
										</s:iterator>
										<s:if test="%{#request.flag1!=1}">
											<s:iterator value="%{#request.mrqFormList}" var="rowval">
												<s:if test="%{#rowval[20]=='nat_hbv_react_ind' || #rowval[20]=='nat_hiv_1_hcv_hbv_mpx_ind'}">
													<s:if test="%{#rowval[15]!=null && #rowval[15]!='' && #rowval[15]==idmnegiPk}">
														<s:set name="flag1" value="1" scope="request"></s:set>
														<s:set name="flag1name" value="%{#rowval[20]}" scope="request"></s:set>
													</s:if>
												</s:if>
											</s:iterator>
										</s:if>
										<s:if test="%{#request.flag1!=1}">
											<s:iterator value="%{#request.mrqFormList}" var="rowval">
												<s:if test="%{#rowval[20]=='nat_hbv_react_ind' || #rowval[20]=='nat_hiv_1_hcv_hbv_mpx_ind'}">
													<s:if test="%{#rowval[15]!=null && #rowval[15]!='' && #rowval[15]==pkTestNotDone}">
														<s:set name="flag1" value="1" scope="request"></s:set>
														<s:set name="flag1name" value="%{#rowval[20]}" scope="request"></s:set>
													</s:if>
												</s:if>
											</s:iterator>
										</s:if>
										<s:if test="%{#request.flag1name==null}">
											<s:set name="flag1name" value="%{'nat_hbv_react_ind'}" scope="request"></s:set>
										</s:if>
										<s:iterator value="%{#request.mrqFormList}" var="rowval">
										<s:if test="%{#rowval[20]==#request.flag1name}">
											<tr>
												<td><img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.hbvnat")%>')"onmouseout="return nd()"></td>
												<td width="42%">
													<span style="padding-right: 5px;vertical-align: top;">
													<s:if test="%{#rowval[20]==#request.flag1name}">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.hbvnat")%>
													</s:if>
													</span>
												</td>
												<td width="58%">
														<s:if test="%{#rowval[15]==null}">
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.hbvnat" list="#{'pkCodeId':'description'}" listKey="pkCodeId" cssClass="checklist tempvalue"
								    										listValue="description" id="hbvnat" onclick="radiocheck();checkval();"/><br/>
															</s:iterator>
														</s:if>
														<s:else>
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.hbvnat" list="#{'pkCodeId':'description'}" listKey="pkCodeId" disabled="true" cssClass="checklist"
								    									listValue="description" id="hbvnat" onclick="radiocheck();checkval();" value="%{#rowval[15]}"/><br/>
															</s:iterator>
														</s:else>
												</td>
											</tr>
											<tr id="hbvnatNote1" style="display: none;">
												<td colspan="3">
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
													</span>
													<span class="error" id="hbvnatUnres">
													</span>
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
													</span>
												</td>
											</tr>
										</s:if>
										</s:iterator>
										<s:set name="flag1" value="0" scope="request"></s:set>
										<s:set name="flag1name" value="" scope="request"></s:set>
										<s:iterator value="%{#request.mrqFormList}" var="rowval">
											<s:if test="%{#rowval[20]=='nat_hcv_react_ind' || #rowval[20]=='nat_hiv_1_hcv_hbv_mpx_ind' || #rowval[20]=='anti_hcv_react_ind'}">
												<s:if test="%{#rowval[15]!=null && #rowval[15]!='' && #rowval[15]==idmposiPk}">
													<s:set name="flag1" value="1" scope="request"></s:set>
													<s:set name="flag1name" value="%{#rowval[20]}" scope="request"></s:set>
												</s:if>
											</s:if>
										</s:iterator>
										<s:if test="%{#request.flag1!=1}">
											<s:iterator value="%{#request.mrqFormList}" var="rowval">
												<s:if test="%{#rowval[20]=='nat_hcv_react_ind' || #rowval[20]=='nat_hiv_1_hcv_hbv_mpx_ind' || #rowval[20]=='anti_hcv_react_ind'}">
													<s:if test="%{#rowval[15]!=null && #rowval[15]!='' && #rowval[15]==idmnegiPk}">
														<s:set name="flag1" value="1" scope="request"></s:set>
														<s:set name="flag1name" value="%{#rowval[20]}" scope="request"></s:set>
													</s:if>
												</s:if>
											</s:iterator>
										</s:if>
										<s:if test="%{#request.flag1!=1}">
											<s:iterator value="%{#request.mrqFormList}" var="rowval">
												<s:if test="%{#rowval[20]=='nat_hcv_react_ind' || #rowval[20]=='nat_hiv_1_hcv_hbv_mpx_ind' || #rowval[20]=='anti_hcv_react_ind'}">
													<s:if test="%{#rowval[15]!=null && #rowval[15]!='' && #rowval[15]==pkTestNotDone}">
														<s:set name="flag1" value="1" scope="request"></s:set>
														<s:set name="flag1name" value="%{#rowval[20]}" scope="request"></s:set>
													</s:if>
												</s:if>
											</s:iterator>
										</s:if>
										<s:if test="%{#request.flag1name==null}">
											<s:set name="flag1name" value="%{'nat_hcv_react_ind'}" scope="request"></s:set>
										</s:if>
										<s:iterator value="%{#request.mrqFormList}" var="rowval">
										<s:if test="%{#rowval[20]==#request.flag1name}">
											<tr>
												<td><img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.antihcv")%>')"onmouseout="return nd()"></td>
												<td width="42%">
													<span style="padding-right: 5px;vertical-align: top;">
													<s:if test="%{#rowval[20]==#request.flag1name}">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.antihcvorhcvnat")%>
													</s:if>
													</span>
												</td>
												<td width="58%">
														<s:if test="%{#rowval[15]==null}">
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.hcv" list="#{'pkCodeId':'description'}" listKey="pkCodeId" cssClass="checklist tempvalue"
								    										listValue="description" id="hcv" onclick="radiocheck();checkval();"/><br/>
															</s:iterator>
														</s:if>
														<s:else>
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.hcv" list="#{'pkCodeId':'description'}" listKey="pkCodeId" disabled="true" cssClass="checklist"
								    									listValue="description" id="hcv" onclick="radiocheck();checkval();" value="%{#rowval[15]}"/><br/>
															</s:iterator>
														</s:else>
												</td>
											</tr>
											<tr id="hcvNote1" style="display: none;">
												<td colspan="3">
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
													</span>
													<span class="error" id="hcvUnres">
													</span>
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
													</span>
												</td>
											</tr>
										</s:if>
										</s:iterator>
										<s:iterator value="%{#request.mrqFormList}" var="rowval">
										<s:if test="%{#rowval[20]=='chagas_react_ind'}">
											<tr>
												<td><img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.tcruzi")%>')"onmouseout="return nd()"></td>
												<td width="42%">
													<span style="padding-right: 5px;vertical-align: top;">
													<s:if test="%{#rowval[20]=='chagas_react_ind'}">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.tcruzichagas")%>
													</s:if>
													</span>
												</td>
												<td width="58%">
														<s:if test="%{#rowval[15]==null}">
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.tcruzichages" list="#{'pkCodeId':'description'}" listKey="pkCodeId" cssClass="checklist tempvalue"
								    										listValue="description" id="tcruzichages" onclick="radiocheck();checkval();"/><br/>
															</s:iterator>
														</s:if>
														<s:else>
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.tcruzichages" list="#{'pkCodeId':'description'}" listKey="pkCodeId" disabled="true" cssClass="checklist"
								    									listValue="description" id="tcruzichages" onclick="radiocheck();checkval();" value="%{#rowval[15]}"/><br/>
															</s:iterator>
														</s:else>
												</td>
											</tr>
											<tr id="tcruzichagesNote1" style="display: none;">
												<td colspan="3">
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
													</span>
													<span class="error" id="tcruzichagesUnres">
													</span>
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
													</span>
												</td>
											</tr>
										</s:if>
										</s:iterator>
										<s:iterator value="%{#request.mrqFormList}" var="rowval">
										<s:if test="%{#rowval[20]=='nat_west_nile_react_ind'}">
											<tr>
												<td><img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.wnv")%>')"onmouseout="return nd()"></td>
												<td width="42%">
													<span style="padding-right: 5px;vertical-align: top;">
													<s:if test="%{#rowval[20]=='nat_west_nile_react_ind'}">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.wnv")%>
													</s:if>
													</span>
												</td>
												<td width="58%">
														<s:if test="%{#rowval[15]==null}">
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.wnc" list="#{'pkCodeId':'description'}" listKey="pkCodeId" cssClass="checklist tempvalue"
								    										listValue="description" id="wnc" onclick="radiocheck();checkval();"/><br/>
															</s:iterator>
														</s:if>
														<s:else>
															<s:iterator value="#application.codeListValues[#rowval[8]]">
								    							<s:radio name="minimumCriteriaTempPojo.wnc" list="#{'pkCodeId':'description'}" listKey="pkCodeId" disabled="true" cssClass="checklist"
								    									listValue="description" id="wnc" onclick="radiocheck();checkval();" value="%{#rowval[15]}"/><br/>
															</s:iterator>
														</s:else>
												</td>
											</tr>
											<tr id="wncNote1" style="display: none;">
												<td colspan="3">
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
													</span>
													<span class="error" id="wncUnres">
													</span>
													<span class="error">
														<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
													</span>
												</td>
											</tr>
										</s:if>
										</s:iterator>
										<tr>
											<td><img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.antihivcmp")%>')"onmouseout="return nd()"></td>
											<td width="42%">
												<span style="padding-right: 5px;vertical-align: top;"><%=propData.getPropertiesData("garuda.minimumcriteria.label.hivi/ii")%></span>
											</td>
											<td width="58%">
												<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]" cssClass="tempvalue"
													listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.antihivcmp" id="antihiv" onclick="radiocheck();checkval();" />
											</td>
										</tr>
										<tr id="antihivNote1" style="display: none;">
											<td></td>
											<td colspan="2">
												<span class="error">
													<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
												</span>
												<span class="error" id="antihivUnres">
												</span>
												<span class="error">
													<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
												</span>
											</td>
										</tr>
										<tr>
											<td><img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.hivp24cmp")%>')"onmouseout="return nd()"></td>
											<td width="42%">
												<span style="padding-right: 5px;vertical-align: top;"><%=propData.getPropertiesData("garuda.minimumcriteria.label.hivpnat")%></span>
											</td>
											<td width="58%">
												<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]" cssClass="tempvalue"
													 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.hivpnatcmp" id="hivpnat" onclick="radiocheck();checkval();"/>
											</td>
										</tr>
										<tr id="hivpnatNote1" style="display: none;">
											<td></td>
											<td colspan="2">
												<span class="error">
													<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
												</span>
												<span class="error" id="hivpnatUnres">
												</span>
												<span class="error">
													<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
												</span>
											</td>
										</tr>
										<tr>
											<td><img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.hbsagcmp")%>')"onmouseout="return nd()"></td>
											<td width="42%">
												<span style="padding-right: 5px;vertical-align: top;"><%=propData.getPropertiesData("garuda.minimumcriteria.label.hbsagcomp")%></span>
											</td>
											<td width="58%">
													<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]" cssClass="tempvalue"
															 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.hbsagcmp" id="hbsagc" onclick="radiocheck();checkval();"/>
											</td>
										</tr>
										<tr id="hbsagcNote1" style="display: none;">
											<td></td>
											<td colspan="2">
												<span class="error">
													<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
												</span>
												<span class="error" id="hbsagcUnres">
												</span>
												<span class="error">
													<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
												</span>
											</td>
										</tr>
										<tr>
											<td><img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.antihcvcmp")%>')"onmouseout="return nd()"></td>
											<td width="42%">
												<span style="padding-right: 5px;vertical-align: top;"><%=propData.getPropertiesData("garuda.minimumcriteria.label.antihcvcomp")%></span>
											</td>
											<td width="58%">
													<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]" cssClass="tempvalue"
															 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.antihcvcmp" id="antihcv" onclick="radiocheck();checkval();"/>
											</td>
										</tr>
										<tr id="antihcvcNote1" style="display: none;">
											<td></td>
											<td colspan="2">
												<span class="error">
													<%=propData.getPropertiesData("garuda.minimumcriteria.label.note1")%>
												</span>
												<span class="error" id="antihcvcUnres">
												</span>
												<span class="error">
													<%=propData.getPropertiesData("garuda.minimumcriteria.label.bactrialnote")%>
												</span>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="4%">
						    		<img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<s:text name="garuda.minimumcriteria.helpmesg.tncwg"/>')"onmouseout="return nd()">
			    				</td>
					    		<td width="41%">
						    		<u onclick="getRighSideForm('MC_divlabcont','labsummary');" style="cursor: pointer;">
						    			<%=propData.getPropertiesData("garuda.minimumcriteria.label.tnckg")%>
						    		</u>
								</td>
								<td width="55%">
								<s:iterator value="patientInfoLst" var="rowVal">
									<s:if test="%{#rowVal[4]!=null && #rowVal[4]!=0 && testCalVal1!=null}">
						    			<s:textfield name="minimumCriteriaTempPojo.tncptwg" id="tncweg"  disabled="true" cssClass="positive checklist" value="%{testCalVal1}"/>
						    		</s:if>
						    		<s:else>
					    				<s:textfield name="minimumCriteriaTempPojo.tncptwg" id="tncweg" onblur="checktncweg();" maxlength="4" cssClass="positive checklist tempvalue"/>
					    			</s:else>
					    		</s:iterator>
									&nbsp;<%=propData.getPropertiesData("garuda.minimumcriteria.tncunit")%>&nbsp;/&nbsp;<%=propData.getPropertiesData("garuda.common.unit.kg")%>
								</td>
							</tr>
							<tr id="tncnote" style="display: none;">
								<td></td>
								<td colspan="2">
									<span class="error"><%=propData.getPropertiesData("garuda.minimumcriteria.label.tncnote")%></span>
								</td>
							</tr>
							<tr id="postprocesschild" style="display: none;">
								<td></td>
								<td>
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.approvalobt")%>
								</td>
								<td>
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]" cssClass="tempvalue"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.postprocesschild" id="postprocesschildq"
											 onclick="showchildmsg();checkval();" />
								</td>
							</tr>
							<tr id="postprocesschildmsg" style="display: none;"><td></td>
								<td colspan="2" style="color: red"><%=propData.getPropertiesData("garuda.minimumcriteria.label.obtainapproval")%></td>
							</tr>
							<tr>
								<td width="5%">
								<s:if test="%{isNewMiniChangeReq==true}">
									<img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.viabilitynew")%>')"onmouseout="return nd()">
								</s:if>
								<s:else>
									<img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.viability")%>')"onmouseout="return nd()">
								</s:else>
								</td>
								<td width="40%">
									<u onclick="getRighSideForm('MC_divlabcont','labsummary');" style="cursor: pointer;">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.postviab")%>
										<s:if test="%{isNewMiniChangeReq==false}">
											&nbsp;<%=propData.getPropertiesData("garuda.minimumcriteria.label.postviabtime")%>
										</s:if>
									</u>
								</td>
								<td width="55%">
								<s:if test="%{isNewMiniChangeReq==true}">
									<s:if test="%{miniCriViabResult!=null && miniCriViabResult!=''}">
										<s:textfield name="minimumCriteriaTempPojo.viabresult" id="postprocviab" onblur="checkviab();" maxlength="3" cssClass="positive checklist" value="%{miniCriViabResult}" disabled="true"/>
										<%=propData.getPropertiesData("garuda.cbbprocedures.label.percentage")%>&nbsp;[<s:property value="%{getCodeListDescById(miniCriViabTestTiming)}"/>]
										<s:hidden name="cbuMinimumCriteriaPojo.viabTestTiming" value="%{miniCriViabTestTiming}"></s:hidden>
									</s:if>
									<s:else>
										<table>
											<tr>
												<td>
													<s:textfield name="minimumCriteriaTempPojo.viabresult" id="postprocviab" onblur="checkviab();" maxlength="3" cssClass="positive checklist tempvalue"/>
													<%=propData.getPropertiesData("garuda.cbbprocedures.label.percentage")%>
												</td>
												<td>
												<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TIMING_OF_TEST]">
													<s:if test="%{subType=='post_procesing' || subType=='post_proc_thaw'}">
														<s:radio list="#{'pkCodeId':'description' }"
												 			listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.viabTestTiming" id="viabTestTiming"
												 			onclick="checkval();" cssClass="tempvalue"/><br/>
											 		</s:if>
												</s:iterator>
												</td>
											</tr>
										</table>
									</s:else>
								</s:if>
								<s:else>
									<s:set name="flag" value="0"></s:set>
									<s:iterator value="postTestList">
										<s:if test="fktestid==pkvaibility">
										<s:set name="flag" value="1"></s:set>
											<s:if test="testresult==null">
												<s:textfield name="minimumCriteriaTempPojo.viabresult" id="postprocviab" onblur="checkviab();" maxlength="3" cssClass="positive checklist tempvalue"/>
											</s:if>
											<s:else>
												<s:textfield name="minimumCriteriaTempPojo.viabresult" id="postprocviab" onblur="checkviab();" maxlength="3" cssClass="positive checklist" value="%{testresult}" disabled="true"/>
											</s:else>
										</s:if>
									</s:iterator>
									<s:if test="#flag==0">
								    		<s:if test="testresult==null">
												<s:textfield name="minimumCriteriaTempPojo.viabresult" id="postprocviab" onblur="checkviab();" maxlength="3" cssClass="positive checklist tempvalue"/>
											</s:if>
											<s:else>
												<s:textfield name="minimumCriteriaTempPojo.viabresult" id="postprocviab" onblur="checkviab();" maxlength="3" cssClass="positive checklist" value="%{testresult}" disabled="true"/>
											</s:else>
							    	</s:if>
										<%=propData.getPropertiesData("garuda.cbbprocedures.label.percentage")%>
								</s:else>
								</td>
							</tr>
							<s:if test="%{isNewMiniChangeReq==true}">
							<tr>
								<td></td><td></td>
								<td>
									<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VIAB_DECLARE]">
										<s:radio list="#{'pkCodeId':'description' }"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.viabPerFlag" id="viabPerFlag"
											 onclick="showchild();checkval();" cssClass="tempvalue"/><br/>
									</s:iterator>
								</td>
							</tr>
							</s:if>
							<tr id="postprocviabnote" style="display: none;">
								<td></td>
								<s:if test="%{isNewMiniChangeReq==true}">
									<td>&nbsp;</td>
									<td>
										<span class="error">
											<%=propData.getPropertiesData("garuda.minimumcriteria.label.postviabnotenew" )%>
										</span>
									</td>
								</s:if>
								<s:else>
									<td colspan="2">
										<span class="error"><%=propData.getPropertiesData("garuda.minimumcriteria.label.postviabnote" )%></span>
									</td>
								</s:else>
							</tr>
							<tr id="viabilitychild" style="display: none;">
								<td></td>
								<td>
									<%=propData.getPropertiesData("garuda.minimumcriteria.label.approvalobt")%>
								</td>
								<td>
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.viabchild" id="viabchildq"
											 onclick="showchildmsg();checkval();" cssClass="tempvalue"/>
								</td>
							</tr>
							<tr id="viabilitychildmsg" style="display: none;"><td></td>
								<td colspan="2" style="color: red">
									<%=propData.getPropertiesData("garuda.minimumcriteria.label.obtainapproval")%>
								</td>
							</tr>
							<tr valign="top">
								<td width="4%">
									<img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.hla")%>')"onmouseout="return nd()">
								</td>
								<td width="40%">
									<u onclick="getRighSideForm('MC_hladivcont','hla');" style="cursor: pointer; padding-right: 5px;vertical-align: top;">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.identifytyping")%>
									</u>
								</td>
								<td width="56%">
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]" cssClass="tempvalue"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.hlacnfm" id="hlacfm" onclick="showchild();checkval();"/>
								</td>
							</tr>
							<tr id="hlachild" style="display: none;">
								<td width="4%"></td>
								<td width="40%">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.willrestpostship")%>
								</td>
								<td width="56%">
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]" cssClass="tempvalue"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.hlacnfmchild" id="hlachildq" onclick="radiocheck();showchild();checkval();"/>
								</td>
							</tr>
							<tr id="hlacfmNote1" style="display: none;">
								<td></td>
								<td colspan="2">
									<span class="error">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.postshipment")%>
									</span>
								</td>
							</tr>
							<tr>
								<td>	
									<img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.ct")%>')"onmouseout="return nd()">
								</td>
								<td>
									<u onclick="getRighSideForm('MC_hladivcont','hla');" style="cursor: pointer;padding-right: 5px;vertical-align: top;">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.ctcmp")%>
									</u>
								</td>
								<td>
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]" cssClass="tempvalue"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.ctcompleted" id="ctcompleted"
											 onclick="radiocheck();showchild();checkval();"/>
								</td>
							</tr>
							<tr id="ctcompletedNote1" style="display: none;">
							<td></td>
								<td colspan="2">
									<span class="error">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.postviabnote")%>
									</span>
								</td>
							</tr>
							<tr id="ctchild" style="display: none;">
								<td></td>
								<td>
									<%=propData.getPropertiesData("garuda.minimumcriteria.label.approvalobt")%>
								</td>
								<td>
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.ctcompletedchild" id="ctchildq"
											 onclick="showchildmsg();checkval();" cssClass="tempvalue"/>
								</td>
							</tr>
							<tr id="ctchildmsg" style="display: none;"><td></td>
								<td colspan="2" style="color: red"><%=propData.getPropertiesData("garuda.minimumcriteria.label.obtainapproval")%></td>
							</tr>
							<tr>
								<td width="4%">
									<img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.unitreport4/6")%>')"onmouseout="return nd()">
								</td>
								<td width="40%">
									<u onclick="getRighSideForm('MC_hladivcont','hla');" style="cursor: pointer;padding-right: 5px;vertical-align: top;">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.antigenmatch" )%>
									</u>
								</td>
								<td width="56%">
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.antigenmatch" id="antigenmch"
											 onclick="radiocheck();showchild();checkval();" cssClass="tempvalue"/>
								</td>
							</tr>
							<tr id="antigenmchNote1" style="display: none;">
							<td></td>
								<td colspan="2">
									<span class="error">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.postviabnote")%>
									</span>
								</td>
							</tr>
							<tr id="antigenchild" style="display: none;">
								<td></td>
								<td>
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.approvalobt")%>
								</td>
								<td>
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.antigenmatchchild" id="antigenchildq"
											 onclick="showchildmsg();checkval();" cssClass="tempvalue"/>
								</td>
							</tr>
							<tr id="antigenchildmsg" style="display: none;"><td></td>
								<td colspan="2" style="color: red"><%=propData.getPropertiesData("garuda.minimumcriteria.label.obtainapproval")%></td>
							</tr>
							<tr>
								<td width="5%">
									<img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.unitreport3/6")%>')"onmouseout="return nd()">
								</td>
								<td>
									<u onclick="getRighSideForm('MC_hladivcont','hla');" style="cursor: pointer; padding-right: 5px;vertical-align: top;">	
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.unitrecp")%>
									</u>
								</td>
								<td>
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.unitrecp" id="unitrecp"
											 onclick="radiocheck();showchild();checkval();" cssClass="tempvalue"/>
								</td>
							</tr>
							<tr id="unitrecpNote1" style="display: none;">
							<td></td>
								<td colspan="2">
									<span class="error">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.postviabnote")%>
									</span>
								</td>
							</tr>
							<tr id="unitrecpchild" style="display: none;">
								<td></td>
								<td>
									<%=propData.getPropertiesData("garuda.minimumcriteria.label.approvalobt")%>
								</td>
								<td>
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.unitrecpchild" id="unitrecpchildq"
											 onclick="showchildmsg();checkval();" cssClass="tempvalue"/>
								</td>
							</tr>
							<tr id="unitrecpchildmsg" style="display: none;"><td></td>
								<td colspan="2" style="color: red">
									<%=propData.getPropertiesData("garuda.minimumcriteria.label.obtainapproval")%>
								</td>
							</tr>				
							<tr>
								<td width="5%">
									<img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<%=propData.getPropertiesData("garuda.minimumcriteria.helpmesg.cbuarrival")%>')"onmouseout="return nd()">
								</td>
								<td>
									<u onclick="getRighSideForm('MC_divShipcont','shipInfo');" style="cursor: pointer; padding-right: 5px;vertical-align: top;">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.cbuprepstart")%>
									</u>
								</td>
								<td>
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.cbuplanned" id="cbuplanned"
											 onclick="radiocheck();showchild();checkval();" cssClass="tempvalue"/>
								</td>
							</tr>
							<tr id="cbuplannedNote1" style="display: none;">
								<td></td>
								<td colspan="2">
									<span class="error">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.postviabnote")%>
									</span>
								</td>
							</tr>
							<tr id="cbuplannedchild" style="display: none;">
								<td></td>
								<td>
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.approvalobt")%>
								</td>
								<td>
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.cbuplannedchild" id="cbuplannedchildq"
											 onclick="showchildmsg();checkval();" cssClass="tempvalue"/>
								</td>
							</tr>
							<tr id="cbuplannedchildmsg" style="display: none;"><td></td>
								<td colspan="2" style="color: red"><%=propData.getPropertiesData("garuda.minimumcriteria.label.obtainapproval")%></td>
							</tr>
											
							<tr>
								<td width="5%">
									<img height="15px" src="./images/help_24x24px.png" onmouseover="return overlib('<s:text name="garuda.minimumcriteria.helpmesg.pathla"/>')"onmouseout="return nd()">
								</td>
								<td>
									<u onclick="getRighSideForm('MC_hladivcont','hla');" style="cursor: pointer; padding-right: 5px;vertical-align: top;">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.pattypereport")%>
									</u>						
								</td>
								<td>
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.patienttyp" id="patienttyp"
											 onclick="radiocheck();showchild();checkval();" cssClass="tempvalue"/>
								</td>
							</tr>
							<tr id="patienttypNote1" style="display: none;">
								<td></td>
								<td colspan="2">
									<span class="error">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.postviabnote")%>
									</span>
								</td>
							</tr>
							<tr id="patienttypchild" style="display: none;">
								<td></td>
								<td>
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.approvalobt")%>
								</td>
								<td>
									<s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_FIELDS]"
											 listKey="pkCodeId" listValue="description" name="cbuMinimumCriteriaPojo.patienttypchild" id="patienttypchildq"
											 onclick="showchildmsg();checkval();" cssClass="tempvalue"/>
								</td>
							</tr>
							<tr id="patienttypchildmsg" style="display: none;"><td></td>
								<td colspan="2" style="color: red"><%=propData.getPropertiesData("garuda.minimumcriteria.label.obtainapproval")%></td>
							</tr>				
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<table>
						<tr>
							<s:if test="%{isNewMiniChangeReq==true}">
								<td nowrap="nowrap" valign="top">
									[<%=propData.getPropertiesData("garuda.minimumcriteria.firstreviewlabel")%>]</td>
							</s:if>
							<td <s:if test="%{isNewMiniChangeReq==false}">colspan='2'</s:if>>
								<div class="innerdiv">
									<%=propData.getPropertiesData("garuda.minimumcriteria.label.declaration")%>
									<s:property value="cdrCbuPojo.registryId"/>
								</div>
							</td>
						</tr>
						<tr>
							<s:if test="%{isNewMiniChangeReq==true}">
								<td></td>
							</s:if>
							<td <s:if test="%{isNewMiniChangeReq==false}">colspan='2'</s:if>>
								<table>
									<tr class="breakcheckbox">
										<td nowrap="nowrap" id="minidecl">
											<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MINIMUM_CRITERIA_DECLARATION]">
												<s:radio list="#{'pkCodeId':'description' }" onclick="btnchange();" cssClass="tempvalue"
													 name="cbuMinimumCriteriaPojo.declarationresult" listKey="pkCodeId" listValue="description" id="declarationresult"/><br/>
											</s:iterator>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td></td>
							<td id="tdcbustatus" style="display: none;" >
								<s:if test="%{MC_cbuStatus!=null && MC_cbuStatus.size()>0}">
									<s:select name="cbuStatus" id="cbuStatus" list="MC_cbuStatus"
										  listKey="pkcbustatus" listValue="cbuStatusDesc" headerKey="" headerValue="Select" onchange="checkcbustus();" />
									<br/>
									<span class="error" id="tdcbustatus_err" style="display:none">
										<%=propData.getPropertiesData("garuda.minimumcriteria.label.selectstatus")%>
									</span>
								</s:if>
							
						</tr>
						<tr id="confrmCritia">
							<s:if test="%{isNewMiniChangeReq==true}">
								<td></td>
							</s:if>
							<td>
								<s:set name="secondReviewFlag" value="0"></s:set>
								<s:if test="%{cbuMinimumCriteriaPojo.declarationresult!=null}">
									<s:set name="secondReviewFlag" value="1"></s:set>
									<s:if test="%{cbuMinimumCriteriaPojo.ReviewBy!=null && cbuMinimumCriteriaPojo.ReviewOn!=null}">
										<s:set name="minimumCriteriaCreator" value="%{cbuMinimumCriteriaPojo.ReviewBy}" scope="request"/>
										<s:date name="%{cbuMinimumCriteriaPojo.ReviewOn}" id="createdDate" format="MMM dd, yyyy" />
									</s:if>
									<s:else>
										<s:set name="minimumCriteriaCreator" value="#request.currentMinUser" scope="request"/>
										<s:date name="%{cbuMinimumCriteriaPojo.ReviewOn}" id="createdDate" format="MMM dd, yyyy" />
									</s:else>
									
							 			<%
											String finaldetails = request.getAttribute("minimumCriteriaCreator").toString();
											UserJB minUser = new UserJB();
											minUser.setUserId(EJBUtil.stringToNum(finaldetails));
											minUser.getUserDetails();													
										%>
									<s:if test="%{isNewMiniChangeReq==true}"><%=propData.getPropertiesData("garuda.minimumcriteria.completedby")%></s:if><s:else>Minimum Criteria Completed By </s:else>	<%=minUser.getUserLastName()+" "+minUser.getUserFirstName()%> <%=propData.getPropertiesData("garuda.cbufinalreview.label.on")%>  
									<s:property value="%{createdDate}"/>
								</s:if>
								<s:hidden id="secondReviewFlag" value="#secondReviewFlag"></s:hidden>
							</td>
							<s:if test="%{isNewMiniChangeReq==false}">
								<td></td>
							</s:if>
						</tr>
						<s:if test="%{isNewMiniChangeReq==true}">
							<tr>
								<td nowrap="nowrap" valign="top">[<%=propData.getPropertiesData("garuda.minimumcriteria.secondreviewlabel")%>]</td>
								<td>
									<%=propData.getPropertiesData("garuda.minimumcriteria.secondreviewdec")%>
									<s:if test="#secondReviewFlag==1 && #request.currentMinUser != #request.minimumCriteriaCreator">
										<s:checkbox name="cbuMinimumCriteriaPojo.reviewConformed" id="secondReviewer"></s:checkbox>
									</s:if>
									<s:else>
										<s:checkbox name="cbuMinimumCriteriaPojo.reviewConformed" id="secondReviewer" disabled="true"></s:checkbox>
									</s:else>
									<br/>
									<div id="secondReviewRow">
										<s:if test="%{cbuMinimumCriteriaPojo.reviewConformed==1}">
											<s:if test="%{cbuMinimumCriteriaPojo.secondReviewBy!=null && cbuMinimumCriteriaPojo.secondReviewOn!=null}">
												<s:set name="minimumCriteriaReviewer" value="%{cbuMinimumCriteriaPojo.secondReviewBy}" scope="request"/>
												<s:date name="%{cbuMinimumCriteriaPojo.secondReviewOn}" id="reviewDate" format="MMM dd, yyyy" />
											</s:if>
											<s:else>
												<s:set name="minimumCriteriaReviewer" value="#request.currentMinUser" scope="request"/>
												<s:date name="%{cbuMinimumCriteriaPojo.secondReviewOn}" id="reviewDate" format="MMM dd, yyyy" />
											</s:else>
									 			<%
													String finaldetails = request.getAttribute("minimumCriteriaReviewer").toString();
													UserJB minUser = new UserJB();
													minUser.setUserId(EJBUtil.stringToNum(finaldetails));
													minUser.getUserDetails();													
												%>	<br/>
											<%=propData.getPropertiesData("garuda.minimumcriteria.secondreviewcompletedby")%> <%=minUser.getUserLastName()+" "+minUser.getUserFirstName()%> <%=propData.getPropertiesData("garuda.cbufinalreview.label.on")%>  
											<s:property value="%{reviewDate}"/>
										</s:if>
									</div>
								</td>
							</tr>
						</s:if>
						<tr bgcolor="#cccccc" id="trEsign">
							<td>
								<jsp:include page="./cb_esignature.jsp" flush="true">
					   				<jsp:param value="minisubmit" name="submitId"/>
									<jsp:param value="miniinvalid" name="invalid" />
									<jsp:param value="miniminimum" name="minimum" />
									<jsp:param value="minipass" name="pass" />
									<jsp:param value="miniesign" name="esign" />
				   				</jsp:include>
				   			</td>
							<td>
								<table>
									<tr>			
										<td width="20%" align="right">
											<s:if test="cbuMinimumCriteriaPojo.conformedminimumcriteria==true">
												<button type="button" id="minisubmit" disabled="disabled" onclick="saveorconfirmresult('<s:property value="cdrCbuPojo.cordID" />','minicri');">
													<span id="btnsave"><s:text	name="garuda.common.lable.confirm"/></span>
												</button>
											</s:if>
											<s:else>
												<button type="button" id="minisubmit" disabled="disabled" onclick="saveorconfirmresult('<s:property value="cdrCbuPojo.cordID" />','minicri');">
													<span id="btnsave"><s:text	name="garuda.common.save"/></span>
												</button>
											</s:else>
										</td>
										<td>
											<button type="button" id="cancel" onclick="goback();">
											<s:text	name="garuda.common.lable.cancel" /></button>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr style="display: none;" id="trClose" >
							<td colspan="2" align="center"><hr>
							<s:if test="hasEditPermission(#request.updateMinCriteria)==true">
								<button type="button" onclick="makeEditable();">
									<s:text	name="garuda.common.lable.edit" />&nbsp;&nbsp;&nbsp;
								</button>&nbsp;&nbsp;
								</s:if>
								<button type="button" id="close" onclick="goback();">
									<s:text	name="garuda.common.close" />
								</button>
							
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	</td>
	<td style="width: 50%;" valign="top">
	<div id="rightcont">
		<div id="MC_divlabParent" style="display:none;" class="rightcont">
			<div  id="labsummarywid" class="portlet portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
				<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<s:text	name="garuda.cordentry.label.labsummary" />
				</div>
				<div class="portlet-content" id="MC_divlabcont"></div>
			</div>
		</div>
	<div id="MC_dividmParent" style="display:none" class="rightcont">
		<div id="dividms" class="portlet portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
				<s:text	name="garuda.cdrcbuview.label.infectious_disease_markers" /></div>
				<div class="portlet-content" id="MC_dividmcont"></div>
		</div>
	</div>
	<div id="MC_MinishipmentInfo" style="display:none">
		<div id="MiniShipcont" class="portlet portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
				<s:text	name="garuda.minimumcriteria.cbuarrivalTitle" /></div>
				<div class="portlet-content" id="MC_divShipcont">
					<table>
						<tr>
							<td><b><%=propData.getPropertiesData("garuda.currentReqProgress.label.proposedprepationdate")%></b></td>
							<td><b>:</b></td>
							<td>
								<s:if test='cbuMinimumCriteriaPojo.prop_prep_date!=null && cbuMinimumCriteriaPojo.prop_prep_date!=""'>
									<s:property value="cbuMinimumCriteriaPojo.prop_prep_date"/>
								</s:if>
								<s:else>
									<%=propData.getPropertiesData("garuda.currentReqProgress.label.notprovided")%>
								</s:else>
							</td>
						</tr>
						<tr>
							<td><b><%=propData.getPropertiesData("garuda.cbuShipment.label.cbusheduledshipdate")%></b></td>
							<td><b>:</b></td>
							<td>
								<s:if test='cbuMinimumCriteriaPojo.shipdate!=null && cbuMinimumCriteriaPojo.shipdate!=""'>
									<s:property value="cbuMinimumCriteriaPojo.shipdate"/>
								</s:if>
								<s:else>
									<%=propData.getPropertiesData("garuda.currentReqProgress.label.notprovided")%>
								</s:else>
							</td>
						</tr>
					</table>
				</div>
		</div>
	</div>
	<div id="MC_divhlaParent" style="display:none;" class="rightcont">
		<div id="hlawid" class="portlet portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
				<s:text	name="garuda.cdrcbuview.label.hla" />
			</div>
			<div class="portlet-content" id="MC_hladivcont"></div>
		</div>
	</div>
	<div id="nonmember" style="display: none">
		<s:hidden name="unitReportListsize" id="listsize" value="%{unitReportList.size()}"></s:hidden>
		<s:if test="%{unitReportList!=null && unitReportList.size()>0}">
			<table border="0" style="overflow: hidden;">
				<tr>
					<s:iterator value="unitReportList" var="cbuunitreport" status="row">
						<s:set name="attachId" value="attachmentId" />
						<td nowrap="nowrap" id="<s:property	value="%{#row.index}"/>" onclick="javascript: return showDocumentForNonMembers('nonmember','<s:property value="getDcmsFileAttchId(#attachId)" />','<s:property	value="%{#row.index}"/>','<s:property value="getDcmsFileAttchType(#attachId)"/>','<s:property value="getDcmsAttchFileName(#attachId)"/>')" class="tabnormal">
							<s:date name="createdOn" id="createdOn1" format="MMM dd, yyyy" />
							<s:property  value="%{createdOn1}" />
						</td>
					</s:iterator>
				</tr>
			</table>
		</s:if>
		<s:else>
			<div id="errorpage">
				<div style="margin-top: 10px; padding: 0.5em 0.5em; width: 90%;" class="ui-state-highlight ui-corner-all"> 
					<strong><%=propData.getPropertiesData("garuda.minimumcriteria.label.noattachmentmsg" )%> </strong>
				</div>
			</div>
		</s:else>
		<fieldset>
			<div id="downloaddoc"  style="padding-top: 5px;">
				<s:hidden name="filepath" id="filepath"></s:hidden>
			<div id="downloaddoc1"  style="padding-top: 5px;"></div>
			<div id="errorpage" style="display: none;">
				<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
					<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
					<strong><%=propData.getPropertiesData("garuda.minimumcriteria.label.serverdownmsg" )%> </strong></p>
				</div>
			</div>
			</div>
		</fieldset>
	</div>
	</div>
	</td>
	</tr>
	</table>
</div>
</div>
</s:form>