<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.util.GarudaMessages" %>

<%
	String contextpath = request.getContextPath();
	String code = "";
%>
<%@ page import="com.velos.ordercomponent.util.VelosGarudaConstants"%>

<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/dateFormat.js"></script>

<script>
	var subtype="";

	   
<%--
window.onload = function(e){      
    document.getElementById('callresolver').value='true';	
}--%>
	
/*function constructTable() {	  
		 $j('#searchResults').dataTable();
		 var tdObj = $j('#searchResults').find('tbody').find("tr:eq(0)").find("td:eq(0)");
			if($j(tdObj).hasClass('dataTables_empty')){
				$j('#searchResults_info').hide();
				$j('#searchResults_paginate').hide();
			} 
}*/
	
function getCodelistValues(value){
	 var contextpath = "<%=contextpath%>";
	 var subtype = "";
	  $j.ajax({
			type : "GET",
			async : false,
			url : 'getSubtype?catPkCodeId='+ value,
			success : function(result) {
			subtype = result.subtype;	
			}
		});
	  return subtype;
	
} 
function showFieldsSubCat(){
	

	var subcatPkCodeId=document.getElementById("labTestName").value;
	
	var subtype1=getCodelistValues(subcatPkCodeId);
	
		if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_CODESUBTYPE" />' && (subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE" />' || subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@PATIENT_CODESUBTYPE" />'))
			{
			  document.getElementById("ver_type").style.display = 'block';  
			
			}
		 else{
				  document.getElementById("ver_type").style.display = 'none';  
				 
			  	}	 	
		if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_REC_CODESUBTYPE" />' &&(subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@OR_RELE_MED_REC_CODESUBTYPE" />' ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_RECORD_CODESUBTYPE" />'))
				{
				  document.getElementById("rec_date").style.display = 'block';  
				}
			 else{
					  document.getElementById("rec_date").style.display = 'none';  
					
				 }
		if((subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@HEALTH_HISt_SCR_CODESUBTYPE" />' &&(subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@MRQ_CODESUBTYPE" />' ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FMHQ_CODESUBTYPE" />'
			 ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_CODESUBTYPE_HLTH" />'))
			 ||(( subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE"/>' &&(subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_RISK_QUESTIONAAIRE" />' ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@URG_MED_NEED_CODESUBTYPE" />'
			 ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FINAL_DEC_OF_ELIG_CODESUBTYPE" />' ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_ELIG_CODESUBTYPE" />'))) 
			 || (subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@POST_SHPMNT_CODESUBTYPE" />' 
			 &&(subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_RECEIPT_CODESUBTYPE" />' ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@POST_SHPMNT_OTHER_CODESUBTYPE" />')))
		{
		  document.getElementById("com_date").style.display = 'block';  
		}
		else{
			  document.getElementById("com_date").style.display = 'none';  
			
		 }
}
function checkSelectedUser(obj){
	
	var orderList = document.getElementsByName('checkbox');
		
	val="'0'";
		
	if(orderList.length>0) {	
	for(var i=0;i<orderList.length;i++){
		if(orderList[i].checked){
			val = val + "," + orderList[i].value;
			} 
		}
	}
		
	val = val.replace("'0',","");
	var orderIds = val.split(",");
	document.forms["searchOpenOrders2"].elements["orderid"].value=orderIds;
	
}
		

$j(function(){
	
	$j("#searchOpenOrders2").validate({
		invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    },
		rules:{			
				"cbuCheck":{required:true},
				"checkbox":"required",
				"categoryName":{checkselect:true},
				"labTestName":{checkselect:
				{
				  depends: function(element){
					  return ($j("#labTestName").is(':visible'));
						}
					}
				},
				"cbUploadInfoPojo.strcompletionDate":{required:
				{
				  depends: function(element){
					  return ($j("#com_date").is(':visible'));
						}
					},dpDate:true
				},
				"cbUploadInfoPojo.strprocessDate":{required:
				{
				  depends: function(element){
					  return ($j("#pro_date").is(':visible'));
						}
					},dpDate:true
				},
				"cbUploadInfoPojo.strreceivedDate":{required:
				{
				  depends: function(element){
					  return ($j("#rec_date").is(':visible'));
						}
					},dpDate:true
				},
				/*"cbUploadInfoPojo.veficationTyping":{checkselect:
				{
				  depends: function(element){
					  return ($j("#ver_type").is(':visible'));
						}
					}
				},*/
				"cbUploadInfoPojo.description":"required",
				"cbUploadInfoPojo.strtestDate":{required:
				{
				  depends: function(element){
					  return ($j("#test").is(':visible'));
						}
					},dpDate:true
				},
				"cbUploadInfoPojo.strreportDate":{required:
				{
				  depends: function(element){
					  return ($j("#repo_date").is(':visible'));
						}
					},dpDate:true
				},
				//"timestamp":"required"
				"attachmentFlag":{required:true},
				/*"attachmentFlag":{required:{
											depends: function(element){
														return $j("#browseFlag").val()==1;
													}
											}
								}*/
				"uploadingFlag":{required:true}	
			},
			messages:{
			"cbuCheck":"<s:text name="garuda.common.validation.selectradiobtn"/>",
				"checkbox":"<s:text name="garuda.common.validation.checkbox"/>",
				"categoryName":" <s:text name="garuda.common.validation.categoryname"/>",
				"labTestName":"<s:text name="garuda.common.validation.labtestname"/>",
				"cbUploadInfoPojo.description":"<s:text name="garuda.common.validation.description"/>",
				"cbUploadInfoPojo.strcompletionDate":{required:"<s:text name="garuda.common.validation.completionDate"/>"},
				"cbUploadInfoPojo.strtestDate":{required:"<s:text name="garuda.common.validation.testdate"/>"},
				"cbUploadInfoPojo.strprocessDate":{required:"<s:text name="garuda.common.validation.processdate"/>"},
				"cbUploadInfoPojo.strreceivedDate":{required:"<s:text name="garuda.common.validation.receiveddate"/>"},
				"cbUploadInfoPojo.strreportDate":{required:"<s:text name="garuda.common.validation.reportdate"/>"},
				//"cbUploadInfoPojo.veficationTyping":"<s:text name="garuda.common.validation.veficationtyping"/>",
			    //"timestamp":"Please Select a file to upload"
				"attachmentFlag":"<s:text name="garuda.common.validation.attachment"/>",
				"uploadingFlag":"<s:text name="garuda.common.validation.uploading"/>"
			},
				errorPlacement: function(error, element) {
				if (element.is(":radio")) {
	            $j('#tablelookup1').append(error);
				}
				else{
					 error.insertAfter(element);
				}
			}
		});
	});

function showFields(){

	var orderList = document.getElementsByName('checkbox');
	var catPkCodeId=document.getElementById("categoryName").value;
	
	var categoryName=$j("select[id$='categoryName'] :selected").text();
	//alert($j("select[id$='categoryName'] :selected").text());
	//alert($j('#categoryName :selected').attr('selected')=='selected');
	subtype= getCodelistValues(catPkCodeId);
	
	if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE" />' || subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_INFORMATION_CODESUBTYPE" />' 
	     || subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE" />'|| subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_INFO_CODESUBTYPE" />'
	     ||subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_ASMNT_CODESUBTYPE"/>' || subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPMENT_ITER_CODESUBTYPE"/>')
    {		
		
	  	$j('#testname').css('display','none');
			for(var i=0;i<orderList.length;i++){
				orderList[i].disabled=true;
			
		}
	}
	else{
			
			if(catPkCodeId!=-1){
						for(var i=0;i<orderList.length;i++){
						orderList[i].disabled=true;
		 			}
			loaddiv('populateDropDown?opt='+catPkCodeId,'testname');	
			$j('#testname').css('display','block');	
			
			}else{
			
			$j('#testname').css('display','none');
			for(var i=0;i<orderList.length;i++){
			orderList[i].disabled=true;
		}}

	 }
	
		
	//to show other fields
		if((categoryName!='Select'))
		{
			document.getElementById("ver_type").style.display = 'none'; 
		    document.getElementById("descript").style.display = 'block';
		 	   if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE" />'
			 	             || subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_ASMNT_CODESUBTYPE"/>'){
				  document.getElementById("com_date").style.display = 'block';  
		 		 }
		 	 	else{
				  document.getElementById("com_date").style.display = 'none';  
		  			}
		 	  if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE" />'|| subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@INFECT_DIS_MARK_CODESUBTYPE" />' ||subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE" />'|| 
		 			  subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_CODESUBTYPE" />'){
				  document.getElementById("test").style.display = 'block'; 
			 	 }
			  else{
				  document.getElementById("test").style.display = 'none';  
			  	}
		 	 if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_INFO_CODESUBTYPE" />'){
				  document.getElementById("pro_date").style.display = 'block'; 
			 	 }
			  else{
				  document.getElementById("pro_date").style.display = 'none';  
			  	}
		 	if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_INFORMATION_CODESUBTYPE"/>'){
				  
				  document.getElementById("repo_date").style.display = 'block';
			 	 }
			  else{
				  
				  document.getElementById("repo_date").style.display = 'none'; 
			  	}
		 	if(subtype!='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_REC_CODESUBTYPE" />'){
				  document.getElementById("rec_date").style.display = 'none'; 
			 	 }
		 	
		}
		else{
			
			 document.getElementById("descript").style.display = 'none';
			 document.getElementById("com_date").style.display = 'none';
			 document.getElementById("test").style.display = 'none';  
			 document.getElementById("pro_date").style.display = 'none'; 
			 document.getElementById("rec_date").style.display = 'none';
			 document.getElementById("repo_date").style.display = 'none'; 
			 for(var i=0;i<orderList.length;i++){
					orderList[i].disabled=false;}
		}
	
		
		
}
function saveCategoryName(obj){
	var categoryName=document.forms["searchOpenOrders2"].elements["categoryName"].value;
 	document.forms["searchOpenOrders2"].elements["categoryId"].value=categoryName;
}

function checkLabTestName(comp) {
	
	var subCategoryName=document.forms["searchOpenOrders2"].elements["labTestName"].value;
	
		document.forms["searchOpenOrders2"].elements["subcatid"].value=subCategoryName;	
}	

	


function saveOrderAttachment(){
	var iframe = document.getElementById('frame1');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;

	if($j("#searchOpenOrders2").valid()){
		
	if($j("#browseFlag").val()==1){
		
		if(innerDoc.getElementById('fileUploadStatus').value == 1) {

    		document.getElementById('frame1').contentWindow.getFileStatus();
			
			if(innerDoc.getElementById('FileId1').value!=null && innerDoc.getElementById('FileId1').value!=''){

				document.getElementById('frame1').contentWindow.document.attachdocument.submit(); 

				   // alert("File Uploaded Successfully!");	
		     		//saveCall();
	
		        } 
			 else {
				 alert('<s:text name="garuda.cbu.openorder.uploadFailed"/>');
			 }
		}
		else{
			alert('<s:text name="garuda.cbu.openorder.uploadFailed"/>');				
		}
		
    }
	else{
		saveCall();
	}
}
}


function saveCall(){
	
	
	loadDivWithFormSubmitOrg('saveAttachment','main','searchOpenOrders2');
	alert('<s:text name="garuda.cbu.fileattach.uploadSuccessAlert"/>');
	$j('#searchResults').dataTable();
	 var tdObj = $j('#searchResults').find('tbody').find("tr:eq(0)").find("td:eq(0)");
		if($j(tdObj).hasClass('dataTables_empty')){
			$j('#searchResults_info').hide();
			$j('#searchResults_paginate').hide();
		} 
	$j('#searchresult').css('display','block'); 
	$j("label").show();	
	showCbuDiv();
	
}
function showDiv(value)
{			

	
	loaddiv('getOpenOrderDetails?cdrCbuPojo.cordID='+value,'searchTble');
		
			document.getElementById("openorderDiv2").style.display = 'block';
			//$j('#openorderDiv2').css('display','block');
			//refreshDiv('getOpenOrderDetails','searchTble','searchOpenOrders1');
	/**
	  * Character Counter for inputs and text areas showing characters left.
	  */
	 $j('#word_count').each(function(){
	     //maximum limit of characters allowed.
	     var maxlimit = 200;
	     // get current number of characters
	     var length = $j(this).val().length;
	     if(length >= maxlimit) {
	   $j(this).val($j(this).val().substring(0, maxlimit));
	   length = maxlimit;
	  }
	     // update count on page load
	     $j(this).parent().find('#counter').html( (maxlimit - length) + ' <s:text name="garuda.cbu.cordentry.charleft"/>');
	     // bind on key up event
	     $j(this).keyup(function(){
	  // get new length of characters
	  var new_length = $j(this).val().length;
	  if(new_length >= maxlimit) {
	    $j(this).val($j(this).val().substring(0, maxlimit));
	    //update the new length
	    new_length = maxlimit;
	   }
	  // update count
	  $j(this).parent().find('#counter').html( (maxlimit - new_length) + ' <s:text name="garuda.cbu.cordentry.charleft"/>');
	     });
	 });

	getDatePic();
	
	$j('#searchResults1').dataTable(); 
		
}

function showCbuDiv()
{
	refreshDiv('openOrderData?paginateModule=viewClinical&inputs='+$j("#inputs").val(),'searchTblediv','searchOpenOrders1');
	//document.getElementById('searchresult').style.display = 'block';
	$j('#searchresult').css('display','block');

	//document.getElementById("openorderDiv2").style.display = 'none';		
	$j('#openorderDiv2').css('display','none');		
}

function disableDiv() {
	var orderList = document.getElementsByName('checkbox');
	
	var count=0;
	for(var i=0;i<orderList.length;i++){
		if(orderList[i].checked){
			count++;
		
		} 
	}

	if(count>0) {
		document.getElementById("categoryName").disabled = true;
		$j("label").hide();
	} else {
		document.getElementById("categoryName").disabled = false;
	}
	
	
	
}
function showAddWidget(url){
	 url = url + "?pageId=4" ; 
	 showAddWidgetModal(url);
}
function resetField(){

	document.getElementById('datepicker1').value="";
	document.getElementById('datepicker2').value="";
	document.getElementById('datepicker3').value="";
	document.getElementById('datepicker4').value="";	
	document.getElementById('datepicker5').value="";	
}


function validateForms(){
	
	if($j("#searchOpenOrders2").valid()){
		showMessage($j("#regid").val(),$j("select[id$='categoryName'] :selected").text());
	}
}

function showMessage(value1,value2)
{
    var str1 ="<s:text name="garuda.cbu.openorder.continueConfirm"/> ";
	  var str2="<br> <s:text name="garuda.uploaddoc.label.documentcategory"/>&nbsp: ";
		jConfirm(str1+value1+str2+value2, '<s:text name="garuda.common.dialog.confirm"/>', function(answer) {
		   if(answer){
		           saveOrderAttachment();	
			}			    
		});	

}

function loadDivWithFormSubmitOrg(url,divname,formId){
	$j('#progress-indicator').css( 'display', 'block' );  
	if($j("#"+formId).valid()){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{           	
            	//$j("#"+divname).html(result);            	
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	}
	$j('#progress-indicator').css( 'display', 'none' );	

}

$j(function(){
	$j("#searchOpenOrders1").validate({
	rules:{			
		"cdrCbuPojo.cdrCbuId":{
				required:true,
				minlength:3
			}
		},
	 messages:{
		"cdrCbuPojo.cdrCbuId":{
			  required : "<s:text name="garuda.common.id.required"/>",
                 minlength : "<s:text name="garuda.cbu.cordentry.inputIdminlength"/>"
			}
	 	}
});
});
function searchAction(e)
{
var key = window.event ? e.keyCode : e.which;
if (key == 13) //enter
{
	if($j("#searchOpenOrders1").valid()){
          showCbuDiv();
          return false;
	}
}
return true;
} 
function searchResult(formname)
{
	if($j("#"+formname).valid()){
		showCbuDiv();
	}
}

</script>
<s:hidden name="udoc_search_txt" id="udoc_search_txt"/>
<s:hidden name="udoc_tbl_sentries" id="udoc_tbl_sentries"/>
<s:hidden name="udoc_tbl_sort_col" id="udoc_tbl_sort_col"/>
<s:hidden name="udoc_tbl_sort_type" id="udoc_tbl_sort_type"/>
<body  class="yui-skin-sam" >
<input type="hidden" name="dcmsIpAddress" id="dcmsIpAddress" value="<s:property value="%{getDCMSIPAddress()}" />" />
<s:if test="#request.update==true">
	<table border="0" align="left" cellpadding="0" cellspacing="0"
		class="displaycdr">
		<tr>
			<td>
			<div style="margin-top: 10px; padding: 0.5em 0.5em;"
				class="ui-state-highlight ui-corner-all">
			<p style="color: #1589FF;"><span
				style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-check"></span> <strong><s:text name="garuda.cbu.openorder.labResult_upload_succ"/> </strong></p>
			</div>
			</td>
		</tr>
	</table>
</s:if>
<div class="col_100 maincontainer"  id="maincontainer" style="display:block;" >
<div class="col_100">

<div class="portlet" id="openordersearchparent">
<div id="openordersearch"
	class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
<div
	class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
<span class="ui-icon ui-icon-minusthick"></span> <!--<span class="ui-icon ui-icon-close" ></span>-->
<s:text name="garuda.openorder.label.searchCBU" /></div>
<div class="portlet-content">

<div id="openorderDiv1">
<s:form id="searchOpenOrders1" method="post" onsubmit="return false;">
	<input type="hidden" name="iShowRows" id="iShowRows" value="5"/>
	<input type="hidden" name="iPageNo" id="iPageNo" value="0"/>
	<s:hidden name="cdrCbuPojo.cordID" id="cordID"></s:hidden>
	<s:hidden name="cdrCbuPojo.registryId" id="registryId" value ="<s:property value='#request.registryID'/>"></s:hidden>
	<%--<s:hidden name="cdrCbuPojo.cdrCbuId" id="cbusearchid"></s:hidden>--%>
	<table class="displaycdr">
		<tr>	
		    <td align="right" width="41%">
				<input type="radio" name="sourceType" id="cbu" checked="checked" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES,@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE)"/>" ><s:text name="garuda.advancelookup.label.cbu"/>
				<input type="radio" name="sourceType" id="maternal" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES,@com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_CODESUBTYPE)"/>" ><s:text name="garuda.advancelookup.label.maternal"/>
			</td>
			<%-- <td align="right"><s:text name="garuda.cbuentry.label.cbuid" />:</td>--%>
			<td width="20%"><s:textfield name="cbuid" id="inputs" placeholder="CBU ID"
				onkeypress="return searchAction(event);" /></td>
			<td>
			<input type="button" onclick="javascript:return searchResult('searchOpenOrders1');" value="<s:text 	name="garuda.opentask.label.search"/>" />
			</td>

		</tr>
	</table>
</s:form> 
<s:form id="searchOpenOrders2" name="searchOpenOrders2" method="post">
	<!--<div id="searchresult" style="display: none;">
	<div id="searchCbuTble">
	<table border="0" cellpadding="0" cellspacing="0" class="displaycdr"
		id="searchResults">
		<thead>
			<tr>
			<th width="15%;"><s:text name="garuda.openorder.label.select" /></th>
			    <th width="15%;"><s:text name="garuda.cbuentry.label.registrycbuid" /></th>
			    <th width="15%;"><s:text name="garuda.cbuentry.label.maternalregistryid" /></th>
				<th width="15%;"><s:text name="garuda.cbuentry.label.localcbuid" /></th>
				<th width="15%;"><s:text name="garuda.cbuentry.label.localmaternalid" /></th>
				<th width="15%;"><s:text name="garuda.cbuentry.label.isbtDin" /></th>				
				<th width="15%;"><s:text name="garuda.cbuentry.label.cbubag" /></th>				
			</tr>
		</thead>
		<tbody>

			<s:iterator value="lstObject" var="cdrcbu" status="row">

				<tr style="width: 100%;" id="<s:property	value="%{#row.index}"/>"
					onMouseOver="highlight('<s:property	value="%{#row.index}"/>')"
					onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')">

					<td width="10%;"><input type="radio" name="cbuCheck" id="cbuCheck"
						value="<s:property value="cordID" />"
						onclick="javascript:showDiv(this.value);" /></input></td>
						<s:hidden id="cordId" value="cordID" />
                    <td><s:property value="registryId"/></td>
                    <td><s:property value="registryMaternalId" /></td>
					<td><s:property value="localCbuId" /></td>
					<td><s:property value="localMaternalId" /></td>
					<td><s:property value="cordIsbiDinCode" /></td>					
					<td><s:property value="numberOnCbuBag" /></td>					
					<%-- <td><s:property
					value="cordID" /></td> --%>
				</tr>
			</s:iterator>
		</tbody>
	<tfoot>
			<tr><td colspan="8"></td></tr>			
			<tr><td colspan="3">
		    <jsp:include page="paginationFooter.jsp">
		  	<jsp:param value="searchCbuTble" name="divName"/>
		  	<jsp:param value="searchOpenOrders1" name="formName"/>
		  	<jsp:param value="showsRow" name="showsRecordId"/>
		  	<jsp:param value="getMainCBUDetailsPaginationData" name="url"/>
		  	<jsp:param value="inputs" name="cbuid"/>
		  	<jsp:param value="workflow" name="paginateSearch"/>
		  	<jsp:param value="constructTable" name="bodyOnloadFn"/>
		  	<jsp:param value="1" name="idparam"/>
		  </jsp:include>
    </td>
    <td colspan="5"></td>
    </tr>
		</tfoot>
	</table>
	</div>
	</div>-->
	
    <div id="searchTblediv">
         <%--<jsp:include page="pf_openorders_datatable.jsp"></jsp:include>--%>
	</div>

	<div id="openorderDiv2" class="col_100 left"
		style="display: none; width: 100%;">

	<div class="col_100">
	<div id="searchTble" style="width: 100%;">
	<hr></hr>
	<div id="results">

	<%-- <table width="100%" border="0" cellpadding="0" cellspacing="0"
		class="displaycdr">

		<tbody>
			<tr></tr>
			<tr>
				<td align="left"><strong><s:text
					name="garuda.openorder.label.outstandingorder"  id="cbuid"/><s:property
					value="cdrCbuPojo.cdrCbuId" /></strong></td>
			</tr>
		</tbody>
	</table>
	</div>
	<div id="ordertable">


	<table width="100%" border="0" cellpadding="0" cellspacing="0"
		class="displaycdr" id="searchResults1">
		<thead>
			<tr>
				<th><s:text name="garuda.openorder.label.select" /></th>
				<th><s:text name="garuda.openorder.label.ordernumber" /></th>
				<th><s:text name="garuda.openorder.label.orderdetails" /></th>
				<th><s:text name="garuda.openorder.label.orderdate" /></th>
				<th><s:text name="garuda.openorder.label.status" /></th>
			</tr>
		</thead>
		<tbody>


			<s:if test="openOrderList.size()==0 && openOrderList==null">
				<tr align="center">
					<td><s:text name="garuda.openorder.label.openorder" /></td>
				</tr>
			</s:if>
			<s:iterator value="openOrderList" status="openorder" var="rowVal">

				<tr>

					<td><s:checkbox name="checkbox" id="checkbox"
						fieldValue="%{#rowVal[4]}" onchange="checkSelectedUser(this);"
						onclick="javascript:disableDiv();" /></td>

					<s:hidden id="cordId" value="%{#rowVal[5]}" />



					<td><s:property value="%{#rowVal[0]}" /></td>



					<td><s:property value="%{#rowVal[1]}" /></td>



					<td><s:date name="%{#rowVal[2]}" id="orderDate"
						format="MMM dd, yyyy" /><s:property value="%{#orderDate}" /></td>--%>



					<%-- <td><s:property value="%{#rowVal[3]}" /></td> --%>
					<%--   <td><s:property value="%{#rowVal[5]}" /></td> --%>


				<%--</tr>
			</s:iterator>


		</tbody>

		<tfoot>
			<tr>
				<td colspan="3"></td>
			</tr>
		</tfoot>

	</table>
	</div>--%>
	<div>
	<div><input type="hidden" id="orderid" name="orderid" value="" />
	<input type="hidden" id="subcatid" name="subcatid" value="" /> <input
		type="hidden" id="categoryId" name="categoryId" value="" />
		<input type="hidden" id="cordCheckId" value="<s:property value='cdrCbuPojo.cordID'/>" />
		<input type="hidden" id="regid" value="<s:property value='#request.registryID'/>" />
		<input type="hidden" name="saveFinalOrderAttachFlagId" id="saveFinalOrderAttachFlagId" >
		</div>


	<table width="100%" border="0" align="left" cellpadding="0"
		cellspacing="0" class="displaycdr">

		<tr>
			<td>

			<div id ="check" >
			<table width="100%" border="0" align="left" cellpadding="0"
				cellspacing="0" class="displaycdr">

				<%-- <tr>
					<td><strong><s:text
						name="garuda.openorder.label.otherclinicaltest" /></strong></td>
				</tr>--%>
				<tr>
					<td width="50%" style="vertical-align: top">

					<div>
					<table border="0" align="left" cellpadding="0" cellspacing="0"
						class="displaycdr" width="100%">
						<tr>
							<td style="width: 40%;" align="left"><s:text
								name="garuda.openorder.label.clinicalcat" />:<span style="color: red;">*</span></td>
							<td style="vertical-align: top;">
							<s:if test="#request.docCategWithRights!=null && #request.docCategWithRights.size>0">
								<s:select name="categoryName" id="categoryName" headerKey="-1" headerValue="Select"	list="#request.docCategWithRights"
									listKey="pkCodeId" listValue="description"	onChange="javascript:saveCategoryName(this);showFields();resetField();">
								</s:select> <!--  <input type="hidden" id="catId" value=<s:property value="#application.codeListMapByIds[24142]" /> /> -->
                              </s:if>
                              <s:else>
                                <select name="categoryName" id="categoryName"  >
                                  <option value="-1"><s:text name="garuda.openorder.label.select" /></option>
                                </select>								
                              </s:else>
							</td>


						</tr>
					</table>
					</div>
					</td>
					<td width="50%" style="vertical-align: top">

					<div id="testname" style="display: none;">
					<table border="0" align="left" cellpadding="0" cellspacing="0"
						class="displaycdr" width="100%">
						<tr>
							<td style="vertical-align: top; width: 40%;"><s:text
								name="garuda.openorder.label.subcategory" /><span style="color: red;">*</span></td>
							<td style="vertical-align: top;"><s:if
								test="testNameList==null">
								<s:select headerKey="0" headerValue="Select" name="namelist"
									list="#{'1':'','2':''}" value="1" />
							</s:if> <s:if test="testNameList!=null">
								<s:select id="labTestName" headerKey="-1" headerValue="Select"
									name="labTestName" list="testNameList" listKey="pkCodeId"
									listValue="description"
									onchange="checkLabTestName(this);showFieldsSubCat();" />
							</s:if></td>

						</tr>
					</table>
					</div>
					
					<div id="repo_date" style="display: none;">
					<table border="0" align="left" cellpadding="0" cellspacing="0"
						class="displaycdr" width="100%">
						<tr>
							<td width="30%;"><s:text
								name="garuda.openorder.label.reportdate" />:<span style="color: red;">*</span></td>
							<td>
				    			<s:textfield name="cbUploadInfoPojo.strreportDate" id="datepicker5" readonly="true" onkeydown="cancelBack();"  cssClass="datePicWOMinDate" onfocus="onFocusCall(this);"/>
			    		</td>
						</tr>
					</table>
					</div>
					

					</td>
				</tr>

				<tr>
					<td width="50%">
					<div id="descript" style="display: none;">
					<table border="0" align="left" cellpadding="0" cellspacing="0"
						class="displaycdr" width="100%">
						<tr>
							<td style="width: 40%;"><s:text
								name="garuda.openorder.label.description" />:<span style="color: red;">*</span></td>
							<td><s:textarea name="cbUploadInfoPojo.description"
								id="word_count" cols="37" rows="5" /> <br />
							<span id="counter"></span></td>
						</tr>
					</table>
					</div>
					</td>

					<td width="50%">

					<div id="com_date" style="display: none;">
					<table border="0" align="left" cellpadding="0" cellspacing="0"
						class="displaycdr" width="100%">
						<tr>
							<td width="30%;"><s:text
								name="garuda.openorder.label.completiondate" />:<span style="color: red;">*</span></td>
							<td>
				    			<s:textfield name="cbUploadInfoPojo.strcompletionDate" id="datepicker1" readonly="true" onkeydown="cancelBack();"  cssClass="datePicWOMinDate" onfocus="onFocusCall(this);"/>
			    		</td>
						</tr>
					</table>
					</div>

					<div id="test" style="display: none;">
					<table border="0" align="left" cellpadding="0" cellspacing="0"
						class="displaycdr" width="100%">
						<tr>
							<td align="left" style="width: 30%;"><s:text
								name="garuda.openorder.label.testdate" />:<span style="color: red;">*</span></td>
							<td>
								<s:textfield name="cbUploadInfoPojo.strtestDate" id="datepicker2" readonly="true" onkeydown="cancelBack();"  cssClass="datePicWOMinDate" onfocus="onFocusCall(this);"/></td>
						</tr>
					</table>
					</div>

					<div id="pro_date" style="display: none;">
					<table border="0" align="left" cellpadding="0" cellspacing="0"
						class="displaycdr" width="100%">
						<tr>
							<td width="30%;"><s:text
								name="garuda.openorder.label.processingdate" />:<span style="color: red;">*</span></td>
							<td>
								<s:textfield name="cbUploadInfoPojo.strprocessDate" id="datepicker3" readonly="true" onkeydown="cancelBack();"  cssClass="datePicWOMinDate" onfocus="onFocusCall(this);"/></td>
						</tr>
					</table>
					</div>

					<div id="rec_date" style="display: none;">
					<table border="0" align="left" cellpadding="0" cellspacing="0"
						class="displaycdr" width="100%">
						<tr>
							<td width="30%;"><s:text
								name="garuda.openorder.label.receiveddate" />:<span style="color: red;">*</span></td>
							<td>
								<s:textfield name="cbUploadInfoPojo.strreceivedDate" id="datepicker4" readonly="true" onkeydown="cancelBack();"  cssClass="datePicWMaxDate" onfocus="onFocusCall(this)"/></td>
						</tr>
					</table>
					</div>
					</td>
				</tr>
				<tr>
					<td width="50%">
					<div id="ver_type" style="display: none;">
					<table border="0" align="left" cellpadding="0" cellspacing="0"
						class="displaycdr" width="100%">
						<tr>
							<td style="width: 40%;"><!--<s:text
								name="garuda.openorder.label.verificationtyping" />:--></td>
							<td><!--<s:select name="cbUploadInfoPojo.veficationTyping"
								id="verificationTyping" headerKey="-1" headerValue="Select"
								list="#{'1':'YES','0':'NO'}" value="0">
							</s:select>--></td>
						</tr>
					</table>
					</div>
				
					</td>




				</tr>



			</table>
			</div>




			</td>
		</tr>
		<tr>
			<td>

			<div id="fileupload">

			<table width="100%" border="0" align="left" cellpadding="0"
				cellspacing="0" class="displaycdr">
				<tr>
					<td><%-- <input type=text value=<%=request.getParameter("cordID")%> /> --%>


					<s:hidden id="attachmentFlag" name="attachmentFlag" value=""></s:hidden>
					<s:hidden id="uploadingFlag" name="uploadingFlag" value="true"></s:hidden>
					<s:hidden id="browseFlag" name="browseFlag" value="0"></s:hidden></td>
				</tr>
				<tr>
					<td><iframe id="frame1" width="60%" height="80px"
						frameborder="0" style="background: #F7F7F7;" scrolling="no"
						src="cb_fileattach.jsp?attCodeType=attach_type&attCodeSubType=test_rprt&codeEntityType=entity_type&codeEntitySubType=CBU">
					</iframe><s:hidden name="timestamp" id="attachId" /></td>
				</tr>
			</table>
			</div>
			</td>
		</tr>


	</table>
	<table border="0" align="left" cellpadding="0" cellspacing="0">
		<tr bgcolor="#cccccc">
			<td><jsp:include page="cb_esignature.jsp" flush="true"></jsp:include></td>
			<td align="left"><s:hidden
				name="orderAttachmentPojo.attachmentId" id="attachmentId" />
			<button type="button"
				onclick="javascript:validateForms();"
				id="submit" disabled="disabled"><s:text
				name="garuda.openorder.label.button.upload" /></button>
			<button type="reset" onclick="javascript:return showCbuDiv();"><s:text
				name="garuda.openorder.label.button.cancel" /></button>
			</td>
		</tr>
	</table>
	</div>
	</div>

	</div>
	</div>
	</div>
</s:form></div>
</div>
</div>
</div>
</div>
</div>
</body>
