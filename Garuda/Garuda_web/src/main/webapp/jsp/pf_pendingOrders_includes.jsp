<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.business.util.VelosMidConstants"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch1" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>

<% String contextpath=request.getContextPath();%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<%
HttpSession tSession = request.getSession(true);
GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
String accId = null;
String logUsr = null;
if (sessionmaint.isValidSession(tSession))
{
    accId = (String) tSession.getValue("accountId");
    logUsr = (String) tSession.getValue("userId");
    request.setAttribute("username",logUsr);
}

String cellValue1="";
if(request.getAttribute("username")!=null && !request.getAttribute("username").equals("")){
	cellValue1 = request.getAttribute("username").toString();
}
UserJB userB = new UserJB();
userB.setUserId(EJBUtil.stringToNum(cellValue1));
userB.getUserDetails();			
String lname=userB.getUserDetails().getUserLastName();
String fname=userB.getUserDetails().getUserFirstName();
String logname=userB.getUserDetails().getUserLoginName();

int assignOrders = 0;
int viewFunding = 0;
int actReqst = 0;
int othrReqst = 0;
int notAvailCbu = 0;
int svdinPrgrss = 0;
int viewCTResolution = 0;
int viewORResolution = 0;
int viewHEResol= 0;

if(grpRights.getFtrRightsByValue("CB_PFPENDINGW")!=null && !grpRights.getFtrRightsByValue("CB_PFPENDINGW").equals(""))
{	assignOrders = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PFPENDINGW"));}
else
	assignOrders = 4;

if(grpRights.getFtrRightsByValue("CB_FUNDING")!=null && !grpRights.getFtrRightsByValue("CB_FUNDING").equals(""))
{	viewFunding = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FUNDING"));}
else
	viewFunding = 4;

if(grpRights.getFtrRightsByValue("CB_ACTIVEREQ")!=null && !grpRights.getFtrRightsByValue("CB_ACTIVEREQ").equals(""))
{	actReqst = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ACTIVEREQ"));}
else
	actReqst = 4;

if(grpRights.getFtrRightsByValue("CB_OTHREQ")!=null && !grpRights.getFtrRightsByValue("CB_OTHREQ").equals(""))
{	othrReqst = Integer.parseInt(grpRights.getFtrRightsByValue("CB_OTHREQ"));}
else
	othrReqst = 4;
if(grpRights.getFtrRightsByValue("CB_NOTAVAIL")!=null && !grpRights.getFtrRightsByValue("CB_NOTAVAIL").equals(""))
{	notAvailCbu = Integer.parseInt(grpRights.getFtrRightsByValue("CB_NOTAVAIL"));}
else
	notAvailCbu = 4;
if(grpRights.getFtrRightsByValue("CB_INPROGCBU")!=null && !grpRights.getFtrRightsByValue("CB_INPROGCBU").equals(""))
{	svdinPrgrss = Integer.parseInt(grpRights.getFtrRightsByValue("CB_INPROGCBU"));}
else
	svdinPrgrss = 4;

if(grpRights.getFtrRightsByValue("CB_ORRESOLW")!=null && !grpRights.getFtrRightsByValue("CB_ORRESOLW").equals(""))
{        viewORResolution = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ORRESOLW"));}
else
        viewORResolution = 4;
if(grpRights.getFtrRightsByValue("CB_CTRESOLTNW")!=null && !grpRights.getFtrRightsByValue("CB_CTRESOLTNW").equals(""))
{        viewCTResolution = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CTRESOLTNW"));}
else
        viewCTResolution = 4;
if(grpRights.getFtrRightsByValue("CB_HERESOLW")!=null && !grpRights.getFtrRightsByValue("CB_HERESOLW").equals(""))
{        viewHEResol= Integer.parseInt(grpRights.getFtrRightsByValue("CB_HERESOLW"));}
else
        viewHEResol = 4;

request.setAttribute("assignOrders",assignOrders);
request.setAttribute("viewFunding",viewFunding);
request.setAttribute("actReqst",actReqst);
request.setAttribute("othrReqst",othrReqst);
request.setAttribute("notAvailCbu",notAvailCbu);
request.setAttribute("svdinPrgrss",svdinPrgrss);
request.setAttribute("ctResolRht",viewCTResolution);
request.setAttribute("heResolRht",viewHEResol);
request.setAttribute("orResolRht",viewORResolution);
%>
<%-- <script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/pending_script.js"></script> --%>
<script type="text/javascript">
$j(function(){
getDatePic();
var validator = $j("#dummyform").validate({
	invalidHandler: function(form, validator) {
        var errors = validator.numberOfInvalids();
        if (errors) {
            validator.errorList[0].element.focus();
        }
    }
});
$j('#Searchbox').focus(function() {
	if($j(this).val()=='Enter Any CBB'){
    	$j(this).val('');
	}
});
$j('#Searchbox').blur(function() {
   if($j(this).val()==''){
   		$j(this).val('Enter Any CBB');
   }
});
jQuery(function() {
	  jQuery('#dateEnd, #dateStart').datepicker('option', {
	    beforeShow: customRange
	  }).focus(function(){
		  if(!$j("#ui-datepicker-div").is(":visible"))
		  		customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
});
jQuery(function() {
	  jQuery('#dateEnd1, #dateStart1').datepicker('option', {
	    beforeShow: customRange1
	  }).focus(function(){
		  if(!$j("#ui-datepicker-div").is(":visible"))
		  		customRange1(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
});
});
function deferedPendingReq(url){
	window.location=url;	
}
function resetValidForm(formId){
	$j("#"+formId).validate().resetForm();
}
function pendingOrdersOnLoad(){


	fn_events1('tablelookup','targetall','ulSelectColumn','showsRowtablelookup','PENDING');
	var all_entries_flag=$j('#ord_allentries_flag').val();
	
	var maindivwidth=$j('.maincontainer').width();
	var temp1=maindivwidth*0.97;
	var temp=screen.height*0.25;
	var ordTable ="";
	var RecCount=Number($j('#tpendingEntries').val());
	var tempPkUser=new Array();
	var tempSite=new Array();
	var tempLogName=new Array();
	var pageNo=$j('#paginateWrapper1').find('.currentPage').text();
	var showEntriesTxt;
	var sort=0;
	var sortDir="asc";
	var seachTxt=$j('#ord_filter_value').val();
	//showEntriesTxt='Show <select name="showsRow" id="showsRowtablelookup" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+$j('#tpendingEntries').val()+'" >ALL</option></select> Entries';
	showEntriesTxt='Show <select name="showsRow" id="showsRowtablelookup" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries';
	if($j('#ord_sortParam').val()!=undefined && $j('#ord_sortParam').val()!=null && $j('#ord_sortParam').val()!=""){
		sort=$j('#ord_sortParam').val();
		sortDir=$j('#ord_sort_type').val();
	}	
	
	
	if(all_entries_flag=='ALL'){				
		$j.ajax({
			"dataType": 'json',
			"type": "POST",
			"url":  'getJsonAssignTo.action?pkSiteId='+$j('#cbbdrpdwn').val(),
			"async":true,
			"success":function(result){
				     var temp=result.assignUsrLst;
				     var k;
				     for(var i=0;i<temp.length;i++){
				    	 k=temp[i];
				    	 tempPkUser[i]=k[0];
				    	 tempSite[i]=k[1];
				    	 tempLogName[i]=k[2];
				     }
			 }
	    });
		
		var heightvar=screen.height*0.25;
			
		o2Table =	$j('#tablelookup').dataTable({
			"sScrollY": temp,
			"sDom": "frtiS",
			"sScrollX": temp1,
			"sAjaxSource": 'getJsonPagination.action?TblFind=psOrders'+$j('#allparams').val()+'&allEntries=ALL',
			"bServerSide": true,
			"aaSorting": [[ sort, sortDir ]],
			"bProcessing": true,
			"bRetrieve" : true,
			"bDestroy": true,
			"oSearch": {"sSearch": seachTxt},
			"bSortCellsTop": true,
			"bDeferRender": true,
			"bAutoWidth": false,
			"sAjaxDataProp": "aaData",
			"aoColumnDefs": [
							   {		
									"aTargets": [0],"bSortable": false, "mDataProp": function ( source, type, val  ){
			                	 	  return "<input type='checkbox' name='selectrow' id='selectrow' value='"+source[12]+"'></input>";
			                  }},
			                  {		
			                	  
			                	  "aTargets": [1], "mDataProp": function ( source, type, val){
			                		  var temp="<a href='#' onclick='getSiteDetails(\""+source[14]+"\",\""+source[0]+"\")'>"+source[0]+"</a>"+"<input type='hidden' class='pkcordHid' name='hpkcordId' value='"+source[13]+"' id='hpkcordId' ></input>";
		                	 	      return  temp;
		                	  }},
		                	  { 	
	                	    	    "aTargets": [2], "mDataProp": function ( source, type, val ){ 
	                	    	   	 return source[1];
		                      }},
			                  { 	
		                	    	 "aTargets": [3], "mDataProp": function ( source, type, val ){ 
		                	    	 return "<a href='#' onclick='getCordDetails(\""+source[13]+"\",\""+source[2]+"\")'>"+source[2]+"</a>";
			                  }},
							  {
			                		   "aTargets": [4], "mDataProp": function ( source, type, val){ 
			                		   temp=source[3]+"<input type='hidden' name='hReqType' value='"+source[18]+"' id='hReqType' class='reqTypeHid' ></input>";
									   return temp;
							  }},
							  {
									   "aTargets": [5], "mDataProp": function ( source, type, val ){
								 	   return source[4];
							  }},
							  {
							  			"aTargets": [6], "mDataProp": function ( source, type, val ){ 
								  		return source[15];
							  }},
							  {
						  			"aTargets": [7], "mDataProp": function ( source, type, val ){ 
							  		return source[5];
						      }},
						      {
						  			"aTargets": [8], "mDataProp": function ( source, type, val ){ 
							  		return source[6];
						      }},
						      {
						  			"aTargets": [9], "mDataProp": function ( source, type, val ){ 
							  		return "<a href='#' onclick='loadGroup(\""+source[12]+"\",\""+source[4]+"\",\""+source[13]+"\",\""+source[14]+"\",\""+source[2]+"\")'>"+source[9]+"</a>";
						      }},
						      {
						  			"aTargets": [10], "mDataProp": function ( source, type, val ){ 
						  				
						  		    var temp3="";
						  		    var temp="";
						  			var editFlag='<s:property value="hasEditPermission(#request.assignOrders)"/>';
						  			var viewFlag='<s:property value="hasViewPermission(#request.assignOrders)"/>';
						  			
						  			
						  			for(var j=0;j<tempPkUser.length;j++){
						  			    	
								    	 if(tempSite[j]==source[14]){
								    		 
								    		 if(tempPkUser[j]==source[10]){
								    			 temp3=temp3+"<option value='"+tempPkUser[j]+"' selected='selected' >"+tempLogName[j]+"</option>";
								    		 }else{
								    			 temp3=temp3+"<option value='"+tempPkUser[j]+"'>"+tempLogName[j]+"</option>";
								    		 }
								    		
								          }
					  			    }
						  			 
						  			if(editFlag=='true'){
						  			   temp="<select name='assigntodataTbl' id='assigntodataTbl' class='assignsel' style='width:auto;' onchange='setAssignAction(this)'><option value=''>Unassigned</option>"+temp3+"</select>"+
							             "<input type='hidden' name='hassignTo' class='assignHid' value='"+source[10]+"' id='hassignTo'></input>";
						  				
						  			 }else if(viewFlag=='true'){
						  				temp="<select name='assigntodataTbl' id='assigntodataTbl' disabled='disabled' class='assignsel' style='width:auto' onchange='setAssignAction(this)'><option value=''>Unassigned</option>"+temp3+"</select>";
						  			 } 
							  		return temp;
						      }},
						      {
						    	  "aTargets": [11], "mDataProp": function ( source, type, val) {
						    		  var temp="";
						    		 if(source[8]!="null" && source[8]!=""){
						    			  temp="<select name='ackResult' id='ackResult' class='ackdyn' onchange='setAckAction(this)'/><option>-select-</option><option  value='"+source[7]+"'>Y</option></select>";
						    		 } 
								  return temp;	 
						      }},
						      {
						  			"aTargets": [12], "mDataProp": function ( source, type, val ) { 
							  		return source[17];
						      }},
						      {
						  			"aTargets": [13], "mDataProp": function ( source, type, val ) { 
							  		return source[8];
						      }},
						      {
						  			"aTargets": [14], "mDataProp": function ( source, type, val ) { 
							  		return source[16];
						      }}
							],
		"fnInitComplete": function(){
			
							},
							"fnDrawCallback": function() { 

								OrdTableDrawCallBack();
								$j('#tablelookup_wrapper').find('.dataTables_scrollFoot').hide();
								$j('#tablelookup_info').show();
								$j("#showsRowtablelookup option:contains('ALL')").attr('selected', 'selected');
								showStatusFilterText('activeStatFilterFlagTxt','activeStatFilterFlag');
							}
							
							
		});  
		$j('#tablelookup_filter').before(showEntriesTxt);
		
			
	}else{
	
	
		$j.ajax({
			"dataType": 'json',
			"type": "POST",
			"url":  'getJsonAssignTo.action?pkSiteId='+$j('#cbbdrpdwn').val(),
			"async":true,
			"success":function(result){
				     var temp=result.assignUsrLst;
				     var k;
				     for(var i=0;i<temp.length;i++){
				    	 k=temp[i];
				    	 tempPkUser[i]=k[0];
				    	 tempSite[i]=k[1];
				    	 tempLogName[i]=k[2];
				     }
			 }
	    });
	    
		o2Table =	$j('#tablelookup').dataTable({
			"sScrollY": temp,
			"sScrollX": temp1,
			"sAjaxSource": 'getJsonPagination.action?TblFind=psOrders'+$j('#allparams').val()+'&allEntries=ALL'+'&iDisplayStart=0&iDisplayLength='+all_entries_flag+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+all_entries_flag,
			"bServerSide": true,
			"bProcessing": true,
			"bRetrieve" : true,
			"bDestroy": true,
			"bSortCellsTop": true,
			"oSearch": {"sSearch": seachTxt},
			"aaSorting": [[ sort, sortDir ]],
			"bDeferRender": true,
			"bAutoWidth": false,
			"sAjaxDataProp": "aaData",
			"aoColumnDefs": [
							   {		
									"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val  ){
			                	 	 return "<input type='checkbox' name='selectrow' id='selectrow' value='"+source[12]+"'></input>";
			                  }},
			                  {		
			                	  
			                	  "aTargets": [1], "mDataProp": function ( source, type, val){
			                		  var temp="<a href='#' onclick='getSiteDetails(\""+source[14]+"\",\""+source[0]+"\")'>"+source[0]+"</a>"+"<input type='hidden' class='pkcordHid' name='hpkcordId' value='"+source[13]+"' id='hpkcordId' ></input>";
		                	 	      return  temp;
		                	  }},
		                	  { 	
	                	    	    "aTargets": [2], "mDataProp": function ( source, type, val ){ 
	                	    	   	 return source[1];
		                      }},
			                  { 	
		                	    	 "aTargets": [3], "mDataProp": function ( source, type, val ){ 
		                	    	 return "<a href='#' onclick='getCordDetails(\""+source[13]+"\",\""+source[2]+"\")'>"+source[2]+"</a>";
			                  }},
							  {
			                		   "aTargets": [4], "mDataProp": function ( source, type, val){ 
			                		   temp=source[3]+"<input type='hidden' name='hReqType' value='"+source[18]+"' id='hReqType' class='reqTypeHid' ></input>";
									   return temp;
							  }},
							  {
									   "aTargets": [5], "mDataProp": function ( source, type, val ){
								 	   return source[4];
							  }},
							  {
							  			"aTargets": [6], "mDataProp": function ( source, type, val ){ 
								  		return source[15];
							  }},
							  {
						  			"aTargets": [7], "mDataProp": function ( source, type, val ){ 
							  		return source[5];
						      }},
						      {
						  			"aTargets": [8], "mDataProp": function ( source, type, val ){ 
							  		return source[6];
						      }},
						      {
						  			"aTargets": [9], "mDataProp": function ( source, type, val ){ 
							  		return "<a href='#' onclick='loadGroup(\""+source[12]+"\",\""+source[4]+"\",\""+source[13]+"\",\""+source[14]+"\",\""+source[2]+"\")'>"+source[9]+"</a>";
						      }},
						      {
						  			"aTargets": [10], "mDataProp": function ( source, type, val ){ 
						  				
						  		    var temp3="";
						  		    var temp="";
						  			var editFlag='<s:property value="hasEditPermission(#request.assignOrders)"/>';
						  			var viewFlag='<s:property value="hasViewPermission(#request.assignOrders)"/>';
						  			
						  			
						  			for(var j=0;j<tempPkUser.length;j++){
						  			    	
								    	 if(tempSite[j]==source[14]){
								    		 
								    		 if(tempPkUser[j]==source[10]){
								    			 temp3=temp3+"<option value='"+tempPkUser[j]+"' selected='selected' >"+tempLogName[j]+"</option>";
								    		 }else{
								    			 temp3=temp3+"<option value='"+tempPkUser[j]+"'>"+tempLogName[j]+"</option>";
								    		 }
								    		
								          }
					  			    }
						  			 
						  			if(editFlag=='true'){
						  			   temp="<select name='assigntodataTbl' id='assigntodataTbl' class='assignsel' style='width:auto' onchange='setAssignAction(this)'><option value=''>Unassigned</option>"+temp3+"</select>"+
							             "<input type='hidden' name='hassignTo' class='assignHid' value='"+source[10]+"' id='hassignTo'></input>";
						  				
						  			 }else if(viewFlag=='true'){
						  				temp="<select name='assigntodataTbl' id='assigntodataTbl'  disabled='disabled' class='assignsel' style='width:auto' onchange='setAssignAction(this)'><option value=''>Unassigned</option>"+temp3+"</select>";
						  			 }
							  		return temp;
						      }},
						      {
						    	  "aTargets": [11], "mDataProp": function ( source, type, val) {
						    		  var temp="";
						    		  if(source[8]!="null" && source[8]!=""){
						    			  temp="<select name='ackResult' id='ackResult' class='ackdyn' onchange='setAckAction(this)'/><option>-select-</option><option  value='"+source[7]+"'>Y</option></select>";
						    		  }
								  return temp;	 
						      }},
						      {
						  			"aTargets": [12], "mDataProp": function ( source, type, val ) { 
							  		return source[17];
						      }},
						      {
						  			"aTargets": [13], "mDataProp": function ( source, type, val ) { 
							  		return source[8];
						      }},
						      {
						  			"aTargets": [14], "mDataProp": function ( source, type, val ) { 
							  		return source[16];
						      }}
							],
							"fnDrawCallback": function() { 
								
								OrdTableDrawCallBack();
								$j("#showsRowtablelookup").val(all_entries_flag);
								showStatusFilterText('activeStatFilterFlagTxt','activeStatFilterFlag');			 
							}
							
							
		});  
		
		
	}
	
	
	$j('#tablelookup_info').hide();
	$j('#tablelookup_paginate').hide();
	$j('#tablelookup_length').empty();
	$j('#tablelookup_length').replaceWith(showEntriesTxt);
	
	$j('#showsRowtablelookup').change(function(){
		getPSOrdesParam();
		var all_entries_flag=$j('#showsRowtablelookup :selected').text();
		$j('#ord_allentries_flag').val(all_entries_flag);
		paginationFooter(0,"getPSOrders,showsRowtablelookup,temp,pendingOrderForm,pendingTblDiv,pendingOrdersOnLoad,pendingFilter,pfSiteId,pfOrdertype,allparams,ord_sortParam,ord_allentries_flag");
		//getHiddedValues();
	});

	var empty='';
	$j('#targetall').removeAttr('overflow');
	//$j('#tablelookup').find('thead').html(empty);
	
	$j('#resetSortOrder').click(function(){
		
		if($j('#ord_sortParam').val()!=undefined && $j('#ord_sortParam').val()!=null && $j('#ord_sortParam').val()!=""){
			$j('#ord_sortParam').val('');
			$j('#ord_sort_type').val('');
			var oTable = $j('#tablelookup').dataTable();
			oTable.fnSort( [ [0,'asc'] ] );
			oTable.fnDraw();
		}
	});
	
	getPSOrdesParam();
	$j('#tablelookup_wrapper').find('.dataTables_scrollHead').css('position','static');
	$j('#tablelookup_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	$j('#tablelookup_wrapper').find('.dataTables_scrollHead').css('z-index','1000');
	
	var scrollbody_html=$j('#tablelookup_wrapper').find('.dataTables_scrollHead').html();
	$j('#tablelookup_wrapper').find('.dataTables_scrollHead').after('<div style="clear:both"></div>');
	
	$j('#tablelookup_wrapper').css('overflow-x','hidden');
	$j('#tablelookup_wrapper').css('overflow-y','visible');
	$j('#tablelookup_wrapper').find('.dataTables_scrollBody').scroll(function(){
		if($j('#targetall').is(':visible')){
			$j('#targetall').fadeOut();
		}
		$j('#tablelookup_wrapper').find('.dataTables_scrollHead').css('overflow','hidden');
	});
	$j('#tablelookup_wrapper').find('#thSelectColumn div').click(function(){
		$j('#tablelookup_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	});
	$j("#sib").click(function(){
	 		$j('#activeOrdDivparent').find(':checkbox').attr('checked', this.checked);
	});
	fnAlignment();
	getHiddedValues();
	
	getSearchableSelect('assignto','id');
	//getSearchableSelect('searchableSelect','class');
		
	$j('#tablelookup_wrapper').find('#assignToTh div').click(function(){
		$j('#tablelookup_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	});
	
	$j('#tablelookup_filter').find(".searching1").keydown(function(e){
		var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#ord_filter_value').val(val);
		  if(e.keyCode==13 )
		  { 
			    if(val!=null && val!=""){
			    	$j('#activeFilterFlag').val('1');
			    }
			    paginationFooter(0,"getPSOrders,showsRowtablelookup,temp,pendingOrderForm,pendingTblDiv,pendingOrdersOnLoad,pendingFilter,pfSiteId,pfOrdertype,allparams,ord_sortParam,ord_allentries_flag");
				
		  }
	});

}

function OrdTableDrawCallBack(){
	
   $j('#pendingTheadTr').find('th').each(function(i){
		if(i!=0){
			$j(this).removeAttr("onClick");
		}
   });
	
   $j('#pendingTheadTr').find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#ord_sortParam').val(i);
			$j('#ord_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#ord_sortParam').val(i);
			$j('#ord_sort_type').val('desc');
		}
   });


 ColManForAll('tablelookup','targetall','false');
 
 
 var chk_arr =  document.getElementsByName("selectrow");
 var chklength = chk_arr.length;  
 //alert("length:::"+chklength)

 for(k=0;k< chklength;k++)
 {
     chk_arr[k].id = 'selectrow'+k;
 } 


 var chk_arr = document.getElementsByName("hpkcordId");
 var chklength = chk_arr.length;  
 for(k=0;k< chklength;k++)
 {
     chk_arr[k].id = 'hpkcordId'+k;
 } 
 
 var chk_arr = document.getElementsByName("hReqType");
 var chklength = chk_arr.length;  
 for(k=0;k< chklength;k++)
 {
     chk_arr[k].id = 'hReqType'+k;
 } 
 
 var chk_arr = document.getElementsByName("assigntodataTbl");
 var chklength = chk_arr.length;  
 for(k=0;k< chklength;k++)
 {
     chk_arr[k].id = 'assigntodataTbl'+k;
 } 
 
 var chk_arr = document.getElementsByName("hassignTo");
 var chklength = chk_arr.length;  
 for(k=0;k< chklength;k++)
 {
     chk_arr[k].id = 'hassignTo'+k;
 } 
 var chk_arr = document.getElementsByName("ackResult");
 var chklength = chk_arr.length;  
 for(k=0;k< chklength;k++)
 {
     chk_arr[k].id = 'ackResult'+k;
 } 
 $j('#tablelookup_info').hide();
 //$j('#tablelookup_wrapper').find('.dataTables_scrollFoot').show();
 if($j('#new_Ord_filterFlag').val()=='1'){
	 showFilter();
 }
	
}
function notAvailCbuTblOnLoad(){
	
	
	
	fn_events1('NA-Cbu-tbl','targetall2','ulSelectColumn2','showsRowNA-Cbu-tbl','PENDING');
	
	var all_entries_flag=$j('#nacbu_allentries_flag').val();
	var maindivwidth=$j('.maincontainer').width();
	var temp1=maindivwidth*0.90;
	var temp=screen.height*0.15;
	var NAcbuTable ="";
	naCBULoad2();
	var pageNo=$j('#paginateWrapper2').find('.currentPage').text();
	var showEntriesTxt;
	var sort=0;
	var sortDir="asc";
	var seachTxt=$j('#nacbu_filter_value').val();
	if($j('#naCBU_sortParam').val()!=undefined && $j('#naCBU_sortParam').val()!=null && $j('#naCBU_sortParam').val()!=""){
		sort=$j('#naCBU_sortParam').val();
		sortDir=$j('#naCBU_sort_type').val();
	}	
	//showEntriesTxt='Show <select name="showsRow" id="showsRowNA-Cbu-tbl" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+$j('#tnaCBUEntries').val()+'" >ALL</option></select> Entries';
	showEntriesTxt='Show <select name="showsRow" id="showsRowNA-Cbu-tbl" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries';
	
	if(all_entries_flag=='ALL'){
		
		//var NAcbuTable =$j('#NA-Cbu-tbl').dataTable().fnDestroy();
		
		NAcbuTable =	$j('#NA-Cbu-tbl').dataTable({
			"sScrollY": "150px",
			"sDom": "frtiS",
			"sAjaxSource": 'getJsonPagination.action?TblFind=psNotavail'+$j('#allparamsNaCBU').val()+'&allEntries=ALL',
			"bServerSide": true,
			"bProcessing": true,
			"bRetrieve" : true,
			"bDestroy": true,
			"aaSorting": [[ sort, sortDir ]],
			"oSearch": {"sSearch": seachTxt},
			"bSortCellsTop": true,
			"bDeferRender": true,
			"bAutoWidth": false,
			"sAjaxDataProp": "aaData",
			"iDisplayLength":1000,
			"aoColumnDefs": [
							   {		
									"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
					                	   
			                	 		return "";
			                  }},
			                  {		
			                	  
			                	  "aTargets": [1], "mDataProp": function ( source, type, val ) {
		                	 		return "<a href='#' onclick='getSiteDetails(\""+source[7]+"\",\""+source[0]+"\");' >"+source[0]+"</a>";
		                	 		
		                	 		//return source[0];
		                	  }},
			                  { 	
		                	    	    "aTargets": [2], "mDataProp": function ( source, type, val ) { 
		                	    	    	var temp="";
		                	    	    	if(source[11]=="null" || source[11]=="0"){
		                	    	    		temp="<a href='#' onclick='submitpost(\"cordEditDataEntry\",{\"cdrCbuPojo.cordID\":\""+source[6]+"\"});' >"+source[1]+"</a>";
		                	    	    	}
		                	    	    	else if(source[11]!="null" && source[11]=="1"){
		                	    	    		temp="<a href='#' onclick='getCordDetails(\""+source[6]+"\",\""+source[1]+"\")'>"+source[1]+"</a>";
		                	    	    	}
		                	    	    	return temp; 
			                  }},
							  {
			                		 "aTargets": [3], "mDataProp": function ( source, type, val ) { 
									  return source[2];
							  }},
							  {
									    "aTargets": [4], "mDataProp": function ( source, type, val ) {
								 	   return source[ 3 ];
							  }},
							  {
							  			"aTargets": [5], "mDataProp": function ( source, type, val ) { 
								  		return source[4];
							  }},
							  {
						  			"aTargets": [6], "mDataProp": function ( source, type, val ) { 
							  		return source[5];
						      }}
							  		
							],
		"fnInitComplete": function(){
			
							},
							"fnDrawCallback": function() { 
										    $j('#naCBUTheadTr').find('th').each(function(i){
													if(i!=0){
														$j(this).removeAttr("onClick");
													}
											 });
											 
										     naCBUSort();
										     ColManForAll('NA-Cbu-tbl','targetall2','false');
										     naCBULoad2();
										     $j("#showsRowNA-Cbu-tbl option:contains('ALL')").attr('selected', 'selected');
										     $j('#NA-Cbu-tbl_wrapper').find('.dataTables_scrollFoot').hide();
										     $j('#NA-Cbu-tbl_info').show();
										     $j('#NA-Cbu-tbl_wrapper').find('.dataTables_scrollBody').find('div').css('border','none');
										     $j("#showsRowNA-Cbu-tbl option:contains('ALL')").attr('selected', 'selected');
											 if($j('#new_Other_filterFlag').val()=='1'){
												 showOtherFilter();
										     }
											 showStatusFilterText('naCBUStatFilterFlagTxt','naCBUStatFilterFlag');
						   
							}
		});  
		$j('#NA-Cbu-tbl_filter').before(showEntriesTxt);
		//naCBULoad2();
		
	}else{
		//var NAcbuTable =$j('#NA-Cbu-tbl').dataTable().fnDestroy();
		
		NAcbuTable =	$j('#NA-Cbu-tbl').dataTable({
			"sScrollY": temp,
			"sAjaxSource": 'getJsonPagination.action?TblFind=psNotavail'+$j('#allparamsNaCBU').val()+'&allEntries=ALL'+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+all_entries_flag,
			"bServerSide": true,
			"bProcessing": true,
			"oSearch": {"sSearch": seachTxt},
			"bRetrieve" : true,
			"bDestroy": true,
			"aaSorting": [[ sort, sortDir ]],
			"bSortCellsTop": true,
			"bDeferRender": true,
			"bAutoWidth": false,
			"sAjaxDataProp": "aaData",
			"iDisplayLength":1000,
			"aoColumnDefs": [
							   {		
									"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
					                	   
			                	 		return "";
			                  }},
                              {		
			                	  
			                	  "aTargets": [1], "mDataProp": function ( source, type, val ) {
		                	 		return "<a href='#' onclick='getSiteDetails(\""+source[7]+"\",\""+source[0]+"\");' >"+source[0]+"</a>";
		                	  }},
			                  { 	
		                	    	    "aTargets": [2], "mDataProp": function ( source, type, val ) { 
		                	    	    	var temp="";
		                	    	    	if(source[11]=="null" || source[11]=="0"){
		                	    	    		temp="<a href='#' onclick='getCordEntry(\""+source[6]+"\",\""+source[1]+"\")' >"+source[1]+"</a>";
		                	    	    	}
		                	    	    	else if(source[11]!="null" && source[11]=="1"){
		                	    	    		temp="<a href='#' onclick='getCordDetails(\""+source[6]+"\",\""+source[1]+"\")'>"+source[1]+"</a>";
		                	    	    	}
		                	    	    	return temp; 
			                  }},
							  {
			                		 "aTargets": [3], "mDataProp": function ( source, type, val ) { 
									  return source[2];
							  }},
							  {
									    "aTargets": [4], "mDataProp": function ( source, type, val ) {
								 	   return source[ 3 ];
							  }},
							  {
							  			"aTargets": [5], "mDataProp": function ( source, type, val ) { 
								  		return source[4];
							  }},
							  {
						  			"aTargets": [6], "mDataProp": function ( source, type, val ) { 
							  		return source[5];
						      }}
							  		
							],
		"fnInitComplete": function(){
			
							},
							"fnDrawCallback": function() { 
										    $j('#naCBUTheadTr').find('th').each(function(i){
													if(i!=0){
														$j(this).removeAttr("onClick");
													}
											 });
										     naCBUSort();
										     ColManForAll('NA-Cbu-tbl','targetall2','false');
										     $j('#NA-Cbu-tbl_wrapper').find('.dataTables_scrollBody').find('div').css('border','none');									     
										     naCBULoad2();
										     if($j('#new_Other_filterFlag').val()=='1'){
										    	 showOtherFilter();
										     }
										     showStatusFilterText('naCBUStatFilterFlagTxt','naCBUStatFilterFlag');
							}
		              }); 
		//naCBULoad2();
		
		
	}
	$j('#resetOtherNA').click(function(){
		if($j('#naCBU_sortParam').val()!=undefined && $j('#naCBU_sortParam').val()!=null && $j('#naCBU_sortParam').val()!=""){
			$j('#naCBU_sortParam').val('');
			$j('#naCBU_sort_type').val('');
			var oTable = $j('#NA-Cbu-tbl').dataTable();
			oTable.fnSort( [ [0,'asc'] ] );
			oTable.fnDraw();
		}
	});
	
}
function naCBUSort(){
	
	$j('#naCBUTheadTr').find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#naCBU_sortParam').val(i);
			$j('#naCBU_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#naCBU_sortParam').val(i);
			$j('#naCBU_sort_type').val('desc');
		}
     });
}
function naCBULoad2(){
	//var showEntriesTxt='Show <select name="showsRow" id="showsRowNA-Cbu-tbl" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+$j('#tnaCBUEntries').val()+'" >ALL</option></select> Entries';
	var showEntriesTxt='Show <select name="showsRow" id="showsRowNA-Cbu-tbl" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries';
	
	$j("#NA-Cbu-tbl_wrapper").addClass("pendingtabsdiv");
	$j('#NA-Cbu-tbl_info').hide();
	$j('#NA-Cbu-tbl_paginate').hide();
	$j('#NA-Cbu-tbl_length').empty();
	$j('#NA-Cbu-tbl_length').replaceWith(showEntriesTxt);
	
	if($j('#naCBUEntries').val()!=null || $j('#naCBUEntries').val()!=""){
		$j('#showsRowNA-Cbu-tbl').val($j('#naCBUEntries').val());
	}
	$j('#showsRowNA-Cbu-tbl').change(function(){
		getPS_NA_CBUParam();
		var all_entries_flag=$j('#showsRowNA-Cbu-tbl :selected').html();
		$j('#nacbu_allentries_flag').val(all_entries_flag);
		paginationFooter(0,"getPSNACBU,showsRowNA-Cbu-tbl,temp,pendingOrderForm,not-available-cbu,notAvailCbuTblOnLoad,naCBUflag,allparamsNaCBU,naCBU_sortParam,nacbu_allentries_flag");
		removingCSS();
		getHiddedValues();
	});
	
	var empty='';
	$j('#targetall2').removeAttr('overflow');
	
	$j('#NA-Cbu-tbl_wrapper').find('.dataTables_scrollHead').css('position','static');
	$j('#NA-Cbu-tbl_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	$j('#NA-Cbu-tbl_wrapper').find('.dataTables_scrollHead').css('z-index','1000');
	
	$j('#NA-Cbu-tbl_wrapper').find('.dataTables_scrollHead').after('<div style="clear:both" class="clearDiv"></div>');
	
	
	$j('#NA-Cbu-tbl_wrapper').find('.dataTables_scroll table').width(temp1);
	$j('#NA-Cbu-tbl_wrapper').find('.dataTables_scrollHeadInner table').width(temp1);
	
	
	$j('#NA-Cbu-tbl_wrapper').css('overflow','hidden');
	$j('#NA-Cbu-tbl_wrapper').find('.dataTables_scrollBody').scroll(function(){
		if($j('#targetall2').is(':visible')){
			$j('#targetall2').fadeOut();
		}
		$j('#NA-Cbu-tbl_wrapper').find('.dataTables_scrollHead').css('overflow','hidden');
	});
	$j('#NA-Cbu-tbl_wrapper').find('#thSelectColumn2 div').click(function(){
		$j('#NA-Cbu-tbl_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	});
	
	
	$j('#NA-Cbu-tbl_filter').find(".searching1").keydown(function(e){
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#nacbu_filter_value').val(val);
		  if(e.keyCode==13 )
		  { 
			    if(val!=null && val!=""){
			    	$j('#naCBUFilterFlag').val('1');
			    }
			    paginationFooter(0,"getPSNACBU,showsRowNA-Cbu-tbl,temp,pendingOrderForm,not-available-cbu,notAvailCbuTblOnLoad,naCBUflag,allparamsNaCBU,naCBU_sortParam,nacbu_allentries_flag");
		  }
	});
	
		
	removingCSS();
	fnAlignment();
	highlight_tab('not-avail-anchor');
	getPS_NA_CBUParam();
	getHiddedValues();
	
	
}
function removingCSS(){
	$j('#tabs').find('.dataTables_scroll').addClass("pendingtabs");
	$j('#tabs').find('.dataTables_scrollHead').addClass("pendingtabsdiv");
	$j('#tabs').find('.dataTables_scrollHead div').addClass("pendingtabsdiv");
	$j('#tabs').find('.clearDiv').addClass("pendingtabsdiv");
	$j('#tabs').find('.dataTables_scrollBody').addClass("pendingtabsdiv");
	$j('#tabs').find('.dataTables_scrollFoot').addClass("pendingtabsdiv");
	$j('#tabs').find('.dataTables_scrollFoot div').addClass("pendingtabsdiv");
}
function cordTblOnLoad(){
	
	fn_events1('cordentrytbl','targetall1','ulSelectColumn1','showsRowcordentrytbl','PENDING');
	
	var maindivwidth=$j('.maincontainer').width();
	var temp1=maindivwidth*0.90;
	var temp=screen.height*0.15;
	var cordTable="";
	var all_entries_flag=$j('#sip_allentries_flag').val();
	var showEntriesTxt;
	var seachTxt=$j('#sip_filter_value').val();
	//var RecCount=$j('#tcordEntries').val();
	getPS_SIPParam();
	var pageNo=$j('#paginateWrapper3').find('.currentPage').text();
	var sort=0;
	var sortDir="asc";
	if($j('#sip_sortParam').val()!=undefined && $j('#sip_sortParam').val()!=null && $j('#sip_sortParam').val()!=""){
		sort=$j('#sip_sortParam').val();
		sortDir=$j('#sip_sort_type').val();
	}	
	//showEntriesTxt='Show <select name="showsRow" id="showsRowcordentrytbl" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+$j('#tcordEntries').val()+'" >ALL</option></select> Entries';
	showEntriesTxt='Show <select name="showsRow" id="showsRowcordentrytbl" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries';
	
	if(all_entries_flag=='ALL'){
		
		cordTable =	$j('#cordentrytbl').dataTable({
			"sScrollY": temp,
			"sDom": "frtiS",
			"sAjaxSource": 'getJsonPagination.action?TblFind=psSavInProg'+$j('#allparamSIP').val()+'&allEntries=ALL',
			"bServerSide": true,
			"bProcessing": true,
			"bRetrieve" : true,
			"bDestroy": true,
			"aaSorting": [[ sort, sortDir ]],
			"oSearch": {"sSearch": seachTxt},
			"bSortCellsTop": true,
			"bDeferRender": true,
			"bAutoWidth": false,
			"sAjaxDataProp": "aaData",
			"aoColumnDefs": [
							   {		
									"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
					                	   
			                	 		return "";
			                  }},
			                  {		
			                	    "aTargets": [1], "mDataProp": function ( source, type, val ) {
		                	 		return "<a href='#' onclick='getSiteDetails(\""+source[7]+"\",\""+source[6]+"\")' >"+source[6]+"</a>";
		                	  }},
			                  { 	
		                	    	"aTargets": [2], "mDataProp": function ( source, type, val ) { 
		                	    	return "<a href='#' onclick='getCordEntry(\""+source[5]+"\",\""+source[0]+"\")'>"+source[0]+"</a>";
		                	    	    		
			                  }},
							  {
			                		 "aTargets": [3], "mDataProp": function ( source, type, val ) { 
									  return source[1];
							  }},
							  {
									   "aTargets": [4], "mDataProp": function ( source, type, val ) {
								 	   return source[ 2 ];
							  }},
							  {
							  			"aTargets": [5], "mDataProp": function ( source, type, val ) { 
								  		return source[3];
							  }},
							  {
						  			"aTargets": [6], "mDataProp": function ( source, type, val ) { 
							  		return source[4];
						      }}
							],
		"fnInitComplete": function(){
			
							},
							"fnDrawCallback": function() { 
										    $j('#cordTheadTr').find('th').each(function(i){
													if(i!=0){
														$j(this).removeAttr("onClick");
													}
											 });
										     sipSort();
										     ColManForAll('cordentrytbl','targetall1','false');
										    
											 $j('#cordentrytbl_wrapper').find('.dataTables_scrollFoot').hide();
											 $j('#cordentrytbl_wrapper').find('.dataTables_scrollBody').find('div').css('border','none');
										     $j('#cordentrytbl_info').show();
										     $j("#showsRowcordentrytbl option:contains('ALL')").attr('selected', 'selected');
										     if($j('#new_Other_filterFlag').val()=='1'){
										    	 showOtherFilter();
										     }
										     showStatusFilterText('cordStatFilterFlagTxt','cordStatFilterFlag');
							}
		});  
		$j('#cordentrytbl_filter').before(showEntriesTxt);
		
	}else{
		
		cordTable =	$j('#cordentrytbl').dataTable({
			"sScrollY": temp,
			/* "sDom": "frtiS", */
			"sAjaxSource": 'getJsonPagination.action?TblFind=psSavInProg'+$j('#allparamSIP').val()+'&allEntries=ALL'+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+all_entries_flag,
			"bServerSide": true,
			"bProcessing": true,
			"bRetrieve" : true,
			"bDestroy": true,
			"aaSorting": [[ sort, sortDir ]],
			"oSearch": {"sSearch": seachTxt},
			"bSortCellsTop": true,
			"bDeferRender": true,
			"bAutoWidth": false,
			"sAjaxDataProp": "aaData",
			"aoColumnDefs": [
							   {		
									"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
					                	   
			                	 		return "";
			                  }},
			                  {		
			                	    "aTargets": [1], "mDataProp": function ( source, type, val ) {
		                	 		return "<a href='#' onclick='getSiteDetails(\""+source[7]+"\",\""+source[6]+"\")' >"+source[6]+"</a>";
		                	  }},
			                  { 	
		                	    	"aTargets": [2], "mDataProp": function ( source, type, val ) { 
		                	    	return "<a href='#' onclick='getCordEntry(\""+source[5]+"\",\""+source[0]+"\")'>"+source[0]+"</a>";
		                	    	    		
			                  }},
							  {
			                		 "aTargets": [3], "mDataProp": function ( source, type, val ) { 
									  return source[1];
							  }},
							  {
									   "aTargets": [4], "mDataProp": function ( source, type, val ) {
								 	   return source[ 2 ];
							  }},
							  {
							  			"aTargets": [5], "mDataProp": function ( source, type, val ) { 
								  		return source[3];
							  }},
							  {
						  			"aTargets": [6], "mDataProp": function ( source, type, val ) { 
							  		return source[4];
						      }}
							],
		"fnInitComplete": function(){
			
							},
							"fnDrawCallback": function() { 
										    $j('#cordTheadTr').find('th').each(function(i){
													if(i!=0){
														$j(this).removeAttr("onClick");
													}
											 });
										     sipSort();
										     ColManForAll('cordentrytbl','targetall1','false');
											 $j('#cordentrytbl_wrapper').find('.dataTables_scrollBody').find('div').css('border','none');
											 $j('#cordentrytbl_length').replaceWith(showEntriesTxt);
										     if($j('#new_Other_filterFlag').val()=='1'){
										    	 showOtherFilter();
										     }
										     showStatusFilterText('cordStatFilterFlagTxt','cordStatFilterFlag');
							}
		});  
		
		
	}
	
	
	
	var empty='';
	$j('#targetall1').removeAttr('overflow');
	
	$j("#cordentrytbl_wrapper").addClass("pendingtabsdiv");
	$j('#cordentrytbl_info').hide();
	$j('#cordentrytbl_paginate').hide();
	$j('#cordentrytbl_length').empty();
	$j('#cordentrytbl_length').replaceWith(showEntriesTxt);

	if($j('#cordEntries').val()!=null || $j('#cordEntries').val()!=""){
		$j('#showsRowcordentrytbl').val($j('#cordEntries').val());
	}
	$j('#showsRowcordentrytbl').change(function(){
		var all_entries_flag=$j('#showsRowcordentrytbl :selected').html();
		$j('#sip_allentries_flag').val(all_entries_flag);
		getPS_SIPParam();
		paginationFooter(0,"getPSSIP,showsRowcordentrytbl,temp,pendingOrderForm,cord-entry,cordTblOnLoad,sipflag,allparamSIP,sip_sortParam,sip_allentries_flag");
		getHiddedValues();
	});
	
	$j('#resetOtherSIP').click(function(){
		if($j('#sip_sortParam').val()!=undefined && $j('#sip_sortParam').val()!=null && $j('#sip_sortParam').val()!=""){
			$j('#sip_sortParam').val('');
			$j('#sip_sort_type').val('');
			var oTable = $j('#cordentrytbl').dataTable();
			oTable.fnSort( [ [0,'asc'] ] );
			oTable.fnDraw();
		}
	});
	
	$j('#cordentrytbl_wrapper').find('.dataTables_scrollHead').css('position','static');
	$j('#cordentrytbl_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	$j('#cordentrytbl_wrapper').find('.dataTables_scrollHead').css('z-index','1000');
	
	$j('#cordentrytbl_wrapper').find('.dataTables_scrollHead').after('<div style="clear:both" class="clearDiv"></div>');
	$j('#cordentrytbl_wrapper').find('.dataTables_scrollHeadInner table').width(temp1);
	$j('#cordentrytbl_wrapper').find('.dataTables_scrollFootInner table').width(temp1);
	
	
	$j('#cordentrytbl_wrapper').css('overflow','hidden');
	$j('#cordentrytbl_wrapper').find('.dataTables_scrollBody').scroll(function(){
		if($j('#targetall1').is(':visible')){
			$j('#targetall1').fadeOut();
		}
		$j('#cordentrytbl_wrapper').find('.dataTables_scrollHead').css('overflow','hidden');
	});
	$j('#cordentrytbl_wrapper').find('#thSelectColumn1 div').click(function(){
		$j('#cordentrytbl_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	});
	
	$j('#cordentrytbl_filter').find(".searching1").keydown(function(e){
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#sip_filter_value').val(val);
		  if(e.keyCode==13 )
		  { 
			    if(val!=null && val!=""){
			    	$j('#cordFilterFlag').val('1');
			    }
			    paginationFooter(0,"getPSSIP,showsRowcordentrytbl,temp,pendingOrderForm,cord-entry,cordTblOnLoad,sipflag,allparamSIP,sip_sortParam,sip_allentries_flag");
		  }
	});
	
	getPS_SIPParam();
	getHiddedValues();
	fnAlignment();
	removingCSS();
}
function sipSort(){
	
	$j('#cordTheadTr').find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#sip_sortParam').val(i);
			$j('#sip_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#sip_sortParam').val(i);
			$j('#sip_sort_type').val('desc');
		}
     });
	
}
function createElemnt(spanId,clearEl_Id1,clearEl_Id2,tag,tblfind){
	
	
	var anchors = document.getElementById(spanId).getElementsByTagName('a').length;
	var num =anchors;
	var newnum=anchors+1;
	var idval='';
	var noofdayflag=0;
	var percentflag=0;
	var dateflag=0;

	$j("#"+spanId).find('a').each(function(){
		
		var idval=$j(this).attr("id");
		if(idval=='ClearNoofDays'+tblfind)
			noofdayflag=1;
		if(idval=='ClearDate'+tblfind)
			dateflag=1;
		if(idval=='Clearpercent'+tblfind)
			percentflag=1;
		
	});
	if(tag=='Noofdays' && idval!='ClearNoofDays'+tblfind && noofdayflag==0){
		 var element = document.createElement("a");
	     element.setAttribute("href", "#");
	     varid="ClearNoofDays"+tblfind;
		 element.setAttribute("id", varid);
		 element.setAttribute("name","Clear_# Of Days");
		 $j(element).bind("click",function(){
				$j('#'+clearEl_Id1).val('');
				fnDrawOrd();
				fnDrawCord();
		 });
		 var nfilter = document.getElementById(spanId);
		 nfilter.appendChild(element);
		 var spanvar='<span>Clear_# Of Days</span>';
		 $j('#ClearNoofDays'+tblfind).prepend(spanvar);
		 $j('#ClearNoofDays'+tblfind).before('<span>&nbsp;&nbsp;</span>');
		 
		
	}
	if(tag=='sipPercent' && idval!='Clearpercent'+tblfind && percentflag==0){
		 var element = document.createElement("a");
	     element.setAttribute("href", "#");
	     varid="Clearpercent"+tblfind;
		 element.setAttribute("id", varid);
		 element.setAttribute("name","Clear_% Complete");
		 $j(element).bind("click",function(){
				$j('#'+clearEl_Id1).val('');
				fnDrawOrd();
				fnDrawCord();
		 });
		 var nfilter = document.getElementById(spanId);
		 nfilter.appendChild(element);
		 var spanvar='<span>Clear_% Complete</span>';
		 $j('#Clearpercent'+tblfind).prepend(spanvar);
		 $j('#Clearpercent'+tblfind).before('<span>&nbsp;&nbsp;</span>');
		 
		
	}
	if(tag=='Date' && idval!='ClearDate'+tblfind && dateflag==0){
		 var element = document.createElement("a");
	     element.setAttribute("href", "#");
	     varid="ClearDate"+tblfind;
		 element.setAttribute("id", varid);
		 element.setAttribute("name","Clear_DateRange");
		 $j(element).bind("click",function(){
				$j("#"+clearEl_Id1).val('');
				$j("#"+clearEl_Id2).val('');
				fnDrawOrd();
				fnDrawCord();
		 });
		 var nfilter = document.getElementById(spanId);
		 nfilter.appendChild(element);
		 var spanvar='<span>Clear_DateRange</span>';
		 $j('#ClearDate'+tblfind).prepend(spanvar);
		 $j('#ClearDate'+tblfind).before('<span>&nbsp;&nbsp;</span>');
		 
	}
	
}
function customRange(input) {
	  if (input.id == 'dateEnd') {
	    /*return {
	      minDate: jQuery('#dateStart').datepicker("getDate")
	    };*/
	    if($j("#dateStart").val()!="" && $j("#dateStart").val()!=null){
	    var minTestDate=new Date($j("#dateStart").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd').datepicker( "option", "minDate", "");
		 }
	  } else if (input.id == 'dateStart') {
		  if($j("#dateEnd").val()!="" && $j("#dateEnd").val()!=null){
		  var minTestDate=new Date($j("#dateEnd").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
	   /* return {
	      maxDate: jQuery('#dateEnd').datepicker("getDate")
	    };*/
		  $j('#dateStart').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart').datepicker( "option", "maxDate", null);
			 }
	  }
	 
}
function highlight_tab(id){

	var anchor_arr=new Array();
	anchor_arr[0]="not-avail-anchor";
	anchor_arr[1]="cord-entry-anchor";
	anchor_arr[2]="funding-anchor";
	
	var DivId=new Array();
	DivId[0]="not-available-cbu";
	DivId[1]="cord-entry";
	DivId[2]="funding";
	
	var anchor_id=id;
	
	
	$j.each(anchor_arr,function(i){
		if(this==anchor_id){
			$j('#'+this).parent().addClass('highlight-tab');
		}
		else{
			$j('#'+this).parent().removeClass('highlight-tab');
			$j('#'+this).parent().addClass('grey-style');
		}
	});
	
}


function customRange1(input) {
	  /*if (input.id == 'dateEnd1') {
	    return {
	      minDate: jQuery('#dateStart1').datepicker("getDate")
	    };
	  } else if (input.id == 'dateStart1') {
	    return {
	      maxDate: jQuery('#dateEnd1').datepicker("getDate")
	    };
	  }*/
	 if (input.id == 'dateEnd1') {
		 if($j("#dateStart1").val()!="" && $j("#dateStart1").val()!=null){
		    var minTestDate=new Date($j("#dateStart1").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
			  $j('#dateEnd1').datepicker( "option", "minDate", new Date(y1,m1,d1));
		 }else{
			 $j('#dateEnd1').datepicker( "option", "minDate", "");
			}
		  } else if (input.id == 'dateStart1') {
			  if($j("#dateEnd1").val()!="" && $j("#dateEnd1").val()!=null){
			 	var minTestDate=new Date($j("#dateEnd1").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
			  	$j('#dateStart1').datepicker( "option", "maxDate", new Date(y1,m1,d1));
			  }else{
				  $j('#dateStart1').datepicker( "option", "maxDate", null);
			}
		  }
}
function hideTabDiv(divid){
	
	var DivId=new Array();
	DivId[0]="not-available-cbu";
	DivId[1]="cord-entry";
	DivId[2]="funding";

	$j.each(DivId,function(){
		if(this==divid){
			$j('#'+this).removeClass('ui-tabs-hide');
		}
		else{
			$j('#'+this).addClass('ui-tabs-hide');
		}
	});
	
	
	if(!$j('#cord-entry-anchor').hasClass('loaded') && divid==DivId[1] && $j('#cord-entry').is(':visible')){
		//alert("inside hideTabDiv sip"+$j('#lastFilterValSIP').val())
		var showEntries="";
		if(!$j('#cord-entry-anchor').hasClass('loaded')){
			showEntries=$j('#SE_SIP').val();
			
		}else{
			showEntries=$j('#showsRowcordentrytbl').val();
		}
		var all_entries_flag=$j('#SE_SIP_txt').val();
		loadmultiplediv('getPSSIP?pkSiteId='+$j('#cbbdrpdwn').val()+'&iShowRows='+showEntries+$j('#lastFilterValSIP').val()+'&allEntries='+$j('#showsRowcordentrytbl :selected').text(),'cord-entry,newFilterSIP');
		$j('#sip_allentries_flag').val(all_entries_flag);
		cordTblOnLoad();
		removingCSS();
		$j('#cord-entry-anchor').addClass('loaded');
	}
	
	if(!$j('#not-avail-anchor').hasClass('loaded') && divid==DivId[0] && $j('#not-available-cbu').is(':visible')){
		//alert("inside hideTabDiv nacbu"+$j('#lastFilterValnaCBU').val())
		var showEntries="";
		if(!$j('#not-avail-anchor').hasClass('loaded')){
			showEntries=$j('#SE_NACBU').val();
		}else{
			showEntries=$j('#showsRowNA-Cbu-tbl').val();
		}
		var all_entries_flag=$j('#SE_NACBU_txt').val();
		$j('#nacbu_allentries_flag').val(all_entries_flag);
		loadmultiplediv('getPSNACBU?pkSiteId='+$j('#cbbdrpdwn').val()+'&iShowRows='+showEntries+$j('#lastFilterValnaCBU').val()+'&allEntries='+$j('#showsRowNA-Cbu-tbl :selected').text(),'not-available-cbu,newFilterNACBU');
		notAvailCbuTblOnLoad();
		removingCSS();
		$j('#not-avail-anchor').addClass('loaded');
	}
	
	if($j('#cord-entry').is(':visible')){
		$j('#resetOtherNA').hide();
		$j('#resetOtherSIP').show();
		$j('#newFilterNACBU').hide('slow');
		$j('#newFilterSIP').show('slow');
	}
	if($j('#not-available-cbu').is(':visible')){
		$j('#newFilterNACBU').show('slow');
		$j('#newFilterSIP').hide('slow');
		$j('#resetOtherNA').show('slow');
		$j('#resetOtherSIP').hide('slow');
	}
	
}

function fundingOnLoad(){
	 $j('#fundingtbl').dataTable({"iDisplayLength":1000,"oLanguage": {"sSearch": "<s:text name="garuda.common.message.searchWith"/>","sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"	} });
	    //fn_events1('fundingtbl','targetall2','ulSelectColumn2');
		$j("#fundingtbl").addClass("pendingtabsdiv");
		$j('#fundingtbl_info').hide();
		$j('#fundingtbl_paginate').hide();
		$j('#fundingtbl_length').empty();
		$j('#fundingtbl_length').replaceWith('Show <select name="showsRow" id="showsRowfunding" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option></select> Entries');

		if($j('#fundingEntries').val()!=null || $j('#fundingEntries').val()!=""){
			$j('#showsRowfunding').val($j('#fundingEntries').val());
			}
		$j('#showsRowfunding').change(function(){
			//paginationFooter(0,"pendingOrders,showsRowNA-Cbu-tbl,temp,pendingOrderForm,not-available-cbu,notAvailCbuTblOnLoad");
		});
		highlight_tab('funding-anchor');
	 
	
	
}

function constructTable(){
	
}

function fnCreateSelectFund( aData,j){
	var r='<select id="HeaderSelectFund'+j+'"><option value="">select</option>', i, iLen=aData.length;
	for ( i=0 ; i<iLen ; i++ )
	{
		r += '<option value="'+aData[i]+'">'+aData[i]+'</option>';
	}
	return r+='</select>';
}

/******************************************************************************************************************************/
function fnResetOrdFilter(){
	
	var cbb=$j('#cbbdrpdwn').val();
	
	var reqType="";
	var orderPriority="";
	var orderStatus="";
	var closeReason="";
	var resolution="";
	var assignTo="";
	var reviewBy="";
	var noOfdays="";
	var dateStart="";
	var dateEnd="";
	var dontflag='0';
	var ordersparam="";
	var sampleAtlab="";
	var cbbid="";
	var cbbSate="";
	var cbuRegId="";
	var cbuLocId="";
	var acknowl="";
	var reqTypeAll="0";
	resetValidForm("dummyForm");
	$j('#activeStatFilterFlag').val('0');
	reqType=$j('#orderType1 :selected').text();
	orderPriority=$j('#orderPriority1').val();
	orderStatus=$j('#orderStatus1').val();
	closeReason=$j('#closeReason1').val();
	resolution=$j('#resolution1').val();
	assignTo=$j('#assignTo1').val();
	reviewBy=$j('#reviewby').val();
	noOfdays=$j('#noOfdaystxt').val();
	dateStart=$j('#dateStart').val();
	dateEnd=$j('#dateEnd').val();
	cbbid=$j('#siteSeq1').val();
	cbbSate=$j('#satellite1').val();
	cbuRegId=$j('#regId1').val();
	cbuLocId=$j('#locId1').val();
	acknowl=$j('#ackresolution1').val();
	
	if(reqType=='<%=VelosMidConstants.CT_LAB_ORDER%>'){ 
		sampleAtlab='Y';
	}
	if(reqType=='<%=VelosMidConstants.CT_SHIP_ORDER%>'){ 
		sampleAtlab='N';
	}
	if(reqType=='ALL'){ 
		reqTypeAll='1';
	}
	if(reqType!=''){ 
		reqType=$j('#orderType1').val();
	}
	
	
	
	if($j('#dateStart').val()!="" && $j('#dateEnd').val()!=""){
		startDate=parseDateValue($j('#dateStart').val());
		startEnd=parseDateValue($j('#dateEnd').val());
		
	}
	if(IsNumeric(noOfdays)){
		noOfdays=$j('#noOfdaystxt').val();
	}
	var previousFilterFlag=$j('#activeFilterFlag').val();
	if(previousFilterFlag=='1'){
		var all_entries_flag=$j('#showsRowtablelookup :selected').text();
		ordersparam='&pkSiteId='+$j('#cbbdrpdwn').val();
		$j('#allparams').val(ordersparam);
		$j('#orderType1').val('');
		$j('#orderPriority1').val('');
		$j('#orderStatus1').val('');
		$j('#closeReason1').val('');
		$j('#resolution1').val('');
		$j('#assignTo1').val('');
		$j('#reviewby').val('');
		$j('#noOfdaystxt').val('');
		$j('#dateStart').val('');
		$j('#dateEnd').val('');
		$j('#siteSeq1').val('');
		$j('#satellite1').val('');
		$j('#regId1').val('');
		$j('#locId1').val('');
		$j('#ackresolution1').val('');
		$j('#activeFilterFlag').val('');
		$j('#ord_allentries_flag').val(all_entries_flag);
		var temp=getSessionForOrders();
		loaddiv('getPSOrders?iShowRows='+$j('#showsRowtablelookup').val()+$j('#allparams').val()+'&allEntries='+all_entries_flag+temp,'pendingTblDiv');
		pendingOrdersOnLoad();
		$j('#allparams').val(ordersparam); 
	}
	$j('#orderType1').val('');
	$j('#orderPriority1').val('');
	$j('#orderStatus1').val('');
	$j('#closeReason1').val('');
	$j('#resolution1').val('');
	$j('#assignTo1').val('');
	$j('#reviewby').val('');
	$j('#noOfdaystxt').val('');
	$j('#dateStart').val('');
	$j('#dateEnd').val('');
	$j('#activeFilterFlag').val('');
	$j('#siteSeq1').val('');
	$j('#satellite1').val('');
	$j('#regId1').val('');
	$j('#locId1').val('');
	$j('#ackresolution1').val('');
	$j('#customFilters').val('');
	$j('#newFilter').find('select').val('');
    /* f_countRows('tablelookup','1','empty','HeaderSelectOrd','noOfdaystxt','dateStart','dateEnd','emptyper'); */
} 
function fnResetCordEntryFilter(){
	   var i=1;
	   var temp;
	   var selectboxArry=[];
	   if($j('#cord-entry').is(':visible')){
		   var dateStart1="";
			var dateEnd1="";
			var noOfdays="";
			var percentageComplete="";
			var dontflag=0;
			var sipParams="";
			var siteId="";
			var regId="";
			var locId="";
			resetValidForm("dummyForm");
			$j('#cordStatFilterFlag').val('0');
			dateStart1=$j('#dateStart1').val();
			dateEnd1=$j('#dateEnd1').val();
			noOfdays=$j('#noOfdaystxt1').val();
			percentageComplete=$j('#sippercentval').val();
			siteId=$j('#sipsiteId1').val();
			regId=$j('#sipregId1').val();
			locId=$j('#siplocId1').val();
			
			if($j('#dateStart1').val()!="" && $j('#dateEnd1').val()!=""){
				dateStart1=parseDateValue($j('#dateStart1').val());
				dateEnd1=parseDateValue($j('#dateEnd1').val());
				
			}
			
			if(IsNumeric(noOfdays)){
				noOfdays=$j('#noOfdaystxt1').val();
			}
			
			
			if(IsNumeric(percentageComplete)){
				percentageComplete=$j('#sippercentval').val();
			}
			
			var previousFilterVal=$j('#cordFilterFlag').val();
			
			if(previousFilterVal=="1"){
				var all_entries_flag=$j('#showsRowcordentrytbl :selected').text();
				sipParams='&pkSiteId='+$j('#cbbdrpdwn').val();
				$j('#allparamSIP').val(sipParams);
				$j('#sip_allentries_flag').val(all_entries_flag);
				loaddiv('getPSSIP?iShowRows='+$j('#showsRowcordentrytbl').val()+$j('#sip_sortParam').val()+$j('#allparamSIP').val()+'&allEntries='+all_entries_flag,'cord-entry');
				$j('#cordFilterFlag').val('');
				cordTblOnLoad();
				$j('#allparamSIP').val(sipParams);
			}
			$j('#dateStart1').val('');
			$j('#dateEnd1').val('');
			$j('#noOfdaystxt1').val('');
			$j('#sippercentval').val('');
			$j('#sipsiteId1').val('');
			$j('#sipregId1').val('');
			$j('#siplocId1').val('');
			$j('#customFiltersNACBU').val('');
			$j('#newFilterSIP').find('select').val('');
		}
	    else if($j('#not-available-cbu').is(':visible')){
	    	var cbuStatus="";
			var cbuReason="";
			var removedBy="";
			var nacbuparams="";
			var siteId="";
			var regId="";
			var locId="";
			$j('#naCBUStatFilterFlag').val('0');
			cbuStatus=$j('#cbuStatus1').val();
			cbuReason=$j('#cbuReason1').val();
			removedBy=$j('#reviewby1').val();
			siteId=$j('#nasiteId1').val();
			regId=$j('#naregId1').val();
			locId=$j('#nalocId1').val();
			var previousFilterVal=$j('#naCBUFilterFlag').val();
			
			if(previousFilterVal=="1"){
				var all_entries_flag=$j('#showsRowNA-Cbu-tbl :selected').text();
				nacbuparams='&pkSiteId='+$j('#cbbdrpdwn').val();
				$j('#allparamsNaCBU').val(nacbuparams);
				$j('#nacbu_allentries_flag').val(all_entries_flag);
				loaddiv('getPSNACBU?iShowRows='+$j('#showsRowNA-Cbu-tbl').val()+$j('#allparamsNaCBU').val()+$j('#naCBU_sortParam').val()+'&allEntries='+all_entries_flag,'not-available-cbu');
				notAvailCbuTblOnLoad();
				removingCSS();
				$j('#naCBUFilterFlag').val('');
				$j('#allparamsNaCBU').val(nacbuparams);
				$j('#not-available-cbu').addClass('loaded'); 
			}
			$j('#cbuStatus1').val('');
			$j('#cbuReason1').val('');
			$j('#reviewby1').val('');
			$j('#nasiteId1').val('');
			$j('#naregId1').val('');
			$j('#nalocId1').val('');
			$j('#customFilterssip').val('');
			$j('#newFilterNACBU').find('select').val('');

	    }
	    else if($j('#funding').is(':visible')){
	    	var fundingCols = $j("#fundingtbl").find('tr')[0].cells.length;
	    	while(i<fundingCols){
				var temp='#HeaderSelectFund'+i;
				selectboxArry=$j(temp).val();
				$j(temp).val('');
				var fundtbl = $j('#fundingtbl').dataTable();
				fundtbl.fnFilter('', i );
				i++;
			}

	    }
			
} 
function getSessionForOrders(){
	 var activeShowEntriestxt=$j("#showsRowtablelookup :selected").html();
	 var activeShowEntries=$j("#showsRowtablelookup").val();
	 var tbl1_site_filter=$j('#allparams').val();
	 var lastSelectCBB=$j('#cbbdrpdwn').val();
	 var param1="";
	 var str=tbl1_site_filter.indexOf("&reqType1");
	  //alert("fil1:"+tbl1_site_filter+"fil2:::::"+tbl2_site_filter+"fil3:::"+tbl3_site_filter);
	  if(str!=-1){
		  var len=tbl1_site_filter.length;
		  var removedStr1=tbl1_site_filter.substring(str,len);
		  param1=removedStr1.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
		  if(param1==""){
			  param1=tbl1_site_filter.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
		  }
	  }
	var paramTmp='&actOrd_SE='+activeShowEntries+'&actOrd_fval='+param1+'&last_cbb='+lastSelectCBB+'&actOrd_SE_txt='+activeShowEntriestxt;
	return paramTmp;
}
function fnAlignment(){
	if(navigator.appName=="Microsoft Internet Explorer"){
		$j('#ordRequestFset').addClass("activereq-fieldset-ie");
	}
	else{
		$j('#ordRequestFset').addClass("activereq-fieldset");
		$j('#OthrReqfieldset').addClass("otherreq-fielset");
	}
	
	
	$j('#noDiv').find('input:button').addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
	$j('#dateDiv').find('input:button').addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
	$j('#noDiv1').find('input:button').addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
	$j('#dateDiv1').find('input:button').addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
	$j('#perCentageDiv').find('input:button').addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
	$j('#customFilterDiv').find('input:button').addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
	$j('#customOFilterDiv').find('input:button').addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
	$j('#esignTbl').find('input:submit').addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
	$j('#pass').hide();	
	
	 
}
function createOtherfilter(){

	fn_FilterOthers();
	
	if($j('#cord-entry').is(':visible')){
		
		if($j('#allparamSIP').val()!="" && $j('#cordFilterFlag').val()=="1"){
			showOtherFilter();
		}
		else{
			alert('<s:text name="garuda.common.pendingpage.alert1"/>');
		}
	}
	else if($j('#not-available-cbu').is(':visible')){
		
		if($j('#allparamsNaCBU').val()!="" && $j('#naCBUFilterFlag').val()=="1"){
			showOtherFilter();
		}
		else{
			alert('<s:text name="garuda.common.pendingpage.alert1"/>');
		}

	}

}
function showOtherFilter(){
	var t=$j('.demo'); 
	var obj=t.offset();
	var xOffset=700;
	var yOffset=100;
	if(navigator.appName=="Microsoft Internet Explorer"){yOffset=yOffset+15;}
	$j('#customOFilterDiv')
			.css({left:obj.left+xOffset,top:obj.top-yOffset})
			.fadeIn("fast").show()
			.addClass('dateDivClass');
	
	$j('#new_Other_filterFlag').val('1');	
}
function createActivefilter(){
	
	fn_FilterOrders();
	
	
	if($j('#allparams').val()!="" && $j('#activeFilterFlag').val()=="1"){
		
		showFilter();
	}
	else{
		alert('<s:text name="garuda.common.pendingpage.alert1"/>');
	}
	    
}
function showFilter(){
   if($j('#allparams').val()!="" && $j('#activeFilterFlag').val()=="1"){
		
		var t=$j('#activeOrdDiv'); 
		var obj=t.offset();
		var xOffset=700;
		var yOffset=80;
		if(navigator.appName=="Microsoft Internet Explorer"){yOffset=yOffset+15;}
		$j('#customFilterDiv')
				.css({left:obj.left+xOffset,top:obj.top+yOffset})
				.fadeIn("fast").show()
				.addClass('dateDivClass');
		$j('#new_Ord_filterFlag').val('1');
	}
}
function calling(){
	
	var fname=$j('#filtername').val();
	var filterValues="";
	var userid='<%=logUsr%>';
	if(fname=="" || fname==null){
		alert('<s:text name="garuda.common.pendingpage.alert2"/>');
	}
	else{
		filterValues=$j('#allparams').val();
		fnResetOrdFilter();
		var str=filterValues.indexOf("&reqType1");
		var len=filterValues.length;
		var removedStr=filterValues.substring(str,len);
		var sub_module='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@SUBMODULE_PENDING,@com.velos.ordercomponent.util.VelosGarudaConstants@SUBMODULE_PENDING_ORDERS)"/>';
		var param=removedStr.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>'); 
		var module='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@MODULE_PENDING"/>';
		var url='customFilterInsert?modulevalue='+module+'&fparam='+param+'&fkuser='+userid+"&filname="+fname+'&subModule='+sub_module;
		loaddiv(url,'newFilter');
		$j('#new_Ord_filterFlag').val('');
		$j('#filtername').val('');
	}
	
}
function callingOther(){

		var fname=$j('#filtername1').val();
		var userid='<%=logUsr%>';

		if(fname=="" || fname==null){
			alert('<s:text name="garuda.common.pendingpage.alert2"/>');
		}
		else{
			
			if($j('#cord-entry').is(':visible')){
				filterValues=$j('#allparamSIP').val();
				fnResetCordEntryFilter();
				var module='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@MODULE_PENDING"/>';
				var param=filterValues.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
				var sub_module='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@SUBMODULE_PENDING,@com.velos.ordercomponent.util.VelosGarudaConstants@SUBMODULE_PENDING_SIP)"/>';
				var url='customFilterInsert?modulevalue='+module+'&fparam='+param+'&fkuser='+userid+"&filname="+fname+'&subModule='+sub_module;
				loaddiv(url,'newFilterSIP');
				$j('#new_Other_filterFlag').val('');
				$j('#filtername1').val('');
			}
			else if($j('#not-available-cbu').is(':visible')){
				filterValues=$j('#allparamsNaCBU').val();
				fnResetCordEntryFilter();
				var str=filterValues.indexOf("&nastatus");
				var len=filterValues.length;
				var removedStr=filterValues.substring(str,len);
				var module='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@MODULE_PENDING"/>';
				var param=removedStr.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
				var sub_module='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@SUBMODULE_PENDING,@com.velos.ordercomponent.util.VelosGarudaConstants@SUBMODULE_PENDING_NACBU)"/>';
				var url='customFilterInsert?modulevalue='+module+'&fparam='+param+'&fkuser='+userid+"&filname="+fname+'&subModule='+sub_module;
				loaddiv(url,'newFilterNACBU');
				$j('#new_Other_filterFlag').val('');
				$j('#filtername1').val('');
			}
			
			
		
		}
	
}
function submitAssingnTo(){
	var asign_init=0;	
	var ack_init=0;	
	var asign_ack_init=0;	
	var can_ack_init=0;	
	var assign_ordId="";
	var ack_ordId="";
	var can_ack_ordId="";
	var assignTo="";
	var ackVal="";
	var ordflag=0;
	var reqType="";
	var pkcord=0;
	var checkboxCount=0;
	var tempId="";
	var tempOrdId="";
	var tempassign="";
	var hasRight=0;
	var ackCTRght='<s:property value="hasEditPermission(#request.ctResolRht)"/>';
	var ackHERght='<s:property value="hasEditPermission(#request.heResolRht)"/>';
	var ackORRght='<s:property value="hasEditPermission(#request.orResolRht)"/>';
	
	
	
	
	$j("#tableLookupTbody").find(':checkbox').each(function (i, el){
		 if($j(el).is(':checked')==true){
			checkboxCount=checkboxCount+1;
			tempId=$j(el).attr("id");
			tempOrdId=$j(el).val();
		}
	}); 
	//alert("checkboxCount"+checkboxCount);
	if(checkboxCount==1){
		ordflag=1;
		assign_ordId=tempOrdId;
		var checkBoxId='selectrow';
		var tempInd=checkBoxId.length;
		var index=tempId.substr(tempInd,tempId.length-1);
		assignTo=$j('#assigntodataTbl'+index).val();
		ackval=$j('#ackResult'+index).val();
		var ordType=$j('#hReqType'+index).val();
		var dontflag=0;
		var actionDirectTo="";
		tempassign=$j('#hassignTo'+index).val();
		var doneflag=false;
	    
		
		
		if(assignTo=='undefined' || assignTo==undefined){
			assignTo='';
		}
		if(ackval=='undefined' || ackval==undefined){
			ackval='';
		}
		
		if(assignTo=='' && ackval==''){
			alert('<s:text name="garuda.common.pendingpage.alertcond13"/>');
			dontflag=1;
			
		} 
		if(assignTo!='' || ackval!=''){
			
			/* if($j('#assigntodataTbl'+index).hasClass('assignTo_Action') && $j('#ackResult'+index).hasClass('ack_Action')){
				alert('<s:text name="garuda.common.pendingpage.alertcond13"/>');
				$j('#assigntodataTbl'+index).val(tempassign);
				$j('#ackResult'+index).val('');
				$j('#assigntodataTbl'+index).removeClass('assignTo_Action');
				$j('#ackResult'+index).removeClass('ack_Action');
				ackval='';
				assignTo='';
				dontflag=1;
				doneflag=true;
				
			} */
			if(!$j('#assigntodataTbl'+index).hasClass('assignTo_Action') && !$j('#ackResult'+index).hasClass('ack_Action') && doneflag==false){
				alert('<s:text name="garuda.common.pendingpage.alertcond13"/>');
				dontflag=1;
			}
			//alert("$j('#assigntodataTbl'+index).value"+$j('#assigntodataTbl'+index).hasClass('assignTo_Action'))
			if($j('#assigntodataTbl'+index).hasClass('assignTo_Action')){
				actionDirectTo='AssignTo';
			}
			if($j('#ackResult'+index).hasClass('ack_Action')){
				actionDirectTo='AckOrder';
			}
			if($j('#assigntodataTbl'+index).hasClass('assignTo_Action') && $j('#ackResult'+index).hasClass('ack_Action')){
				actionDirectTo='AssignToAckOrder';
			}
			
		}
		
		//alert("action Direct:::::::::::::::::::::::::::::::"+actionDirectTo)
		
		if(assignTo!='' && assignTo!=undefined && dontflag==0 && (actionDirectTo=='AssignTo' || actionDirectTo=='AssignToAckOrder')){
			
			asign_init=1;
			
		}
		
		
		if(ordType=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CTORDER"/>' && ackCTRght=='true'){
			hasRight=1;
		}
		if(ordType=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@HEORDER"/>' && ackHERght=='true'){
			hasRight=1;
		}
		if(ordType=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@ORORDER"/>' && ackORRght=='true'){
			hasRight=1;
		}
		
		
			
		if(ackval!='' && ackval!=undefined && dontflag==0 && hasRight==1 && (actionDirectTo=='AckOrder'|| actionDirectTo=='AssignToAckOrder')){
			
			ack_init=1;
			ack_ordId=$j("#"+tempId).val();
			pkcord=$j('#hpkcordId'+index).val();
			
		}
		
		
		if(actionDirectTo=='AckOrder' && hasRight==0 && dontflag==0){
			alert('<s:text name="garuda.common.pendingpage.alertcond11"/>');
		}
		
	}
	if(checkboxCount>1){

		assigntoval=$j('#assignto').val();
		ackresultval=$j('#assign-ackResult').val();
        //alert("assigntoval"+assigntoval+"ackresultval"+ackresultval)
		if(assigntoval=="" && ackresultval==""){
			alert("<s:text name="garuda.common.pendingpage.alert4"/>");
			$j('#assignto').val('');
			$j('#assign-ackResult').val('');
			ordflag=1;
		}else{ 

		
					$j("#tableLookupTbody").find(':checkbox').each(function (i, el) {
				
					        if($j(el).is(':checked')==true && $j(el).val()!="" ){
					        		var id=$j(this).attr("id");
					        		assigntoval=$j('#assignto').val();
					        		ackresultval=$j('#assign-ackResult').val();
						        	if(assigntoval!=""){
							        		assignTo=$j('#assignto').val();
								        	idval=$j(el).val();
								        	
								        	if(asign_init==0){
								        		assign_ordId=idval;
									        	asign_init=1;
									        }else{
									        	assign_ordId=assign_ordId+","+idval;
										    }
							        	
							        }
		
						        	if(ackresultval!=""){
						        		var id=$j(this).attr("id");
						        		var checkBoxId='selectrow';
						        		var tempInd=checkBoxId.length;
						        		var index=id.substr(tempInd,id.length-1);
						        		ackVal=$j('#assign-ackResult').val();
						        		vpkcord=$j('#hpkcordId'+index).val();
						        		vreqType=$j('#hReqType'+index).val();
							        	ack_idval=$j(el).val();
							     
							        	if(vreqType=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CTORDER"/>' && ackCTRght=='true'){
							    			hasRight=1;
							    		}
							    		if(vreqType=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@HEORDER"/>' && ackHERght=='true'){
							    			hasRight=1;
							    		}
							    		if(vreqType=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@ORORDER"/>' && ackORRght=='true'){
							    			hasRight=1;
							    		}
							        	
							    		if(hasRight==1){
							    			
							    			if(ack_init==0){
								        		ack_ordId=ack_idval;
								        		pkcord=vpkcord;
								        		reqType=vreqType;
								        		ack_init=1;
									        }else{
									        	ack_ordId=ack_ordId+","+ack_idval;
									        	pkcord=pkcord+","+vpkcord;
								        		reqType=reqType+","+vreqType;
										    }
							    			
							    		}
							    		if(hasRight==0){
							    			
							    			if(can_ack_init==0){
							    				can_ack_ordId=ack_idval;
							    				can_ack_init=1;
									        }else{
									        	can_ack_ordId=can_ack_ordId+","+ack_idval;
										    }
							    		}
							        	
							        }
						        	
						        	ordflag=1;
					        }
					        
				    });
					if(ack_init==0 && can_ack_init==1 && hasRight==0){
			        	alert('<s:text name="garuda.common.pendingpage.alertcond11"/>');
			        }
		} 
        
	}
	if(asign_init==1 && ack_init==1){
		asign_ack_init=1;
	}
    if(asign_init!=0 || ack_init!=0 ){
    	
    	var pageNo=$j('#paginateWrapper1').find('.currentPage').text();
    	var showRow=$j('#showsRowtablelookup').val();
    	var all_entries_flag=$j('#showsRowtablelookup :selected').html();
    	$j('#ord_allentries_flag').val(all_entries_flag);
    	
    	//alert("asign_init"+asign_init+"ack_init"+ack_init+"asign_ack_init"+asign_ack_init)
    	
    	if(asign_init!=0 && asign_ack_init==0){
    		//alert("inside assign to update url:::::::"+'updateAssignTo?ordIdval='+assign_ordId+'&assigntoval='+assignTo+'&uflag=assignedto&iPageNo='+pageNo+'&iShowRows='+showRow+$j('#allparams').val());
        	loaddiv('updateAssignTo?ordIdval='+assign_ordId+'&assigntoval='+assignTo+'&uflag=assignedto&iPageNo='+pageNo+'&iShowRows='+showRow+$j('#allparams').val()+'&allEntries='+all_entries_flag,'pendingTblDiv');
    	}
        if(ack_init!=0 && asign_ack_init==0){
        	//alert("inside ack update url:::::::"+'updateAssignTo?ordIdval='+ack_ordId+'&acknowledge='+ackVal+'&uflag=acknowledge&cordpk='+pkcord+$j('#allparams').val()+'&iPageNo='+pageNo+'&iShowRows='+showRow);
        	loaddiv('updateAssignTo?ordIdval='+ack_ordId+'&acknowledge='+ackVal+'&uflag=acknowledge&cordpk='+pkcord+$j('#allparams').val()+'&iPageNo='+pageNo+'&iShowRows='+showRow+'&allEntries='+all_entries_flag,'pendingTblDiv');
        }
        if(asign_ack_init!=0){
        	//alert("inside assign ack update url:::::::"+'updateAssignTo?ordIdval='+ack_ordId+'&assigntoval='+assignTo+'&uflag=assign_acknowledge&cordpk='+pkcord+$j('#allparams').val()+'&iPageNo='+pageNo+'&iShowRows='+showRow+$j('#allparams').val());
        	loaddiv('updateAssignTo?ordIdval='+ack_ordId+'&assigntoval='+assignTo+'&uflag=assign_acknowledge&cordpk='+pkcord+$j('#allparams').val()+'&iPageNo='+pageNo+'&iShowRows='+showRow+$j('#allparams').val()+'&allEntries='+all_entries_flag,'pendingTblDiv');
        }
        //var all_entries_flag=$j('#showsRowtablelookup :selected').html();
    	pendingOrdersOnLoad();
    	fnAlignment();
    	//alert("assign_ordId"+assign_ordId);
    	$j('#esign').val("");
    	$j('#assignto').val("");
    	$j('#assign-ackResult').val("");
    	$j('#pendingAsignpass').css("display","none");
    	if(can_ack_ordId!=""){
    		if(can_ack_ordId.indexOf(",")!=-1){
    			cancel_order_arry=can_ack_ordId.split(",");
    			for(var i = 0; i < cancel_order_arry.length; i++)
    			{
    			
    				$j('#activeOrdDivparent').find(':checkbox').each(function(){
        				var checkbox_val=$j(this).val();
        				if($j.trim(cancel_order_arry[i])==checkbox_val){
        					$j(this).attr('checked','checked');
        				}
        			});
    			 
    			}

    			
    		}else{
    			$j('#activeOrdDivparent').find(':checkbox').each(function(){
    				var checkbox_val=$j(this).val();
    				if(checkbox_val==can_ack_ordId){
    					$j(this).attr('checked','checked');
    				}
    			});
    		}
    		
    	}

    	if($j('#canackorders').val()!=""){
    		var cancel_order_pk=$j('#canackorders').val();
    		if(cancel_order_pk.indexOf(",")!=-1){
    			cancel_order_arry=cancel_order_pk.split(",");
    			for(var i = 0; i < cancel_order_arry.length; i++)
    			{
    			
    				$j('#activeOrdDivparent').find(':checkbox').each(function(){
        				var checkbox_val=$j(this).val();
        				if($j.trim(cancel_order_arry[i])==checkbox_val){
        					$j(this).attr('checked','checked');
        				}
        			});
    			 
    			}

    			
    		}else{
    			$j('#activeOrdDivparent').find(':checkbox').each(function(){
    				var checkbox_val=$j(this).val();
    				if(checkbox_val==cancel_order_pk){
    					$j(this).attr('checked','checked');
    				}
    			});
    			
    			
    		}
    		
    		if(checkboxCount==1){
    			if(reqType!="OR")
    				alert('<s:text name="garuda.common.pendingpage.alertreas"/> \n <s:text name="garuda.common.pendingpage.alertcond1"/>');
    			else
    				alert('<s:text name="garuda.common.pendingpage.alertreas"/> \n <s:text name="garuda.common.pendingpage.alertcond1"/> \n <s:text name="garuda.common.pendingpage.alertcond2"/>\n <s:text name="garuda.common.pendingpage.alertcond3"/> \n <s:text name="garuda.common.pendingpage.alertcond9"/>');
    		}else{
    			alert('<s:text name="garuda.common.pendingpage.alertreas1"/> \n <s:text name="garuda.common.pendingpage.alertcond1"/> <s:text name="garuda.common.pendingpage.alertcond12"/> \n <s:text name="garuda.common.pendingpage.alertcond2"/><s:text name="garuda.common.pendingpage.alertcond6"/>\n <s:text name="garuda.common.pendingpage.alertcond3"/><s:text name="garuda.common.pendingpage.alertcond6"/> \n <s:text name="garuda.common.pendingpage.alertcond8"/><s:text name="garuda.common.pendingpage.alertcond6"/> \n');
    		}
    		var cids=$j('#canackorders').val();
        	
        }
    	if(($j('#canackorders').val()=="" || $j('#canackorders').val()==null) && ack_init!=0){
        	alert('<s:text name="garuda.common.pendingpage.alert9"/>');
        }
    }
    if(ordflag==0){
        alert('<s:text name="garuda.common.pendingpage.alert10"/>');
    }
}

function parseDateValue(rawDate) {

	/* var mon=[{month  : 'Jan',mno  : '01'},
	         {month  : 'Feb',mno  : '02'},
	         {month  : 'Mar',mno  : '03'},
	         {month  : 'Apr',mno  : '04'},
	         {month  : 'May',mno  : '05'},
	         {month  : 'Jun',mno  : '06'},
	         {month  : 'Jul',mno  : '07'},
	         {month  : 'Aug',mno  : '08'},
	         {month  : 'Sep',mno  : '09'},
	         {month  : 'Oct',mno  : '10'},
	         {month  : 'Nov',mno  : '11'},
	         {month  : 'Dec',mno  : '12'}
	        ];
   var nmon=0;
	$j.each(mon,function(){
			if($j(this).attr('month')==month){
				nmon=$j(this).attr('mno');
			}
	});
	var parsedDate= year + nmon + dt; */
	var year= rawDate.substring(7,12);
	var month= rawDate.substring(0,3);
	var dt= rawDate.substring(4,6);
	var parsedDate=dt+"-"+month+"-"+$j.trim(year);
	//alert("parsedDate"+parsedDate);
	return $j.trim(parsedDate);
}
function showDiv(el,id,id1,id2){
	var t=$j(el);
	var obj=t.offset();
	var xOffset=180;
	var yOffset=30;
	var val1="";
	var val2="";
	
	if(id=='dateDiv'){
		val1=$j('#pStartDtVal').val();
		val2=$j('#pEndDtVal').val();
	}
	if(id=='dateDiv1'){
		val1=$j('#sippStartDtVal').val();
		val2=$j('#sippEndDtVal').val();
	}
	
    //alert("----"+val1+"---"+val2);
	
	$j('#'+id1).val(val1);
	$j('#'+id2).val(val2);
	
	if($j('.dataEnteringDiv').is(':visible')){
		$j('.dataEnteringDiv').hide();
	}
	if(navigator.appName=="Microsoft Internet Explorer"){yOffset=yOffset+15;}
	$j("#"+id)
			.css({left:obj.left-xOffset,top:obj.top+yOffset})
			.fadeIn("fast").show()
			.addClass('dateDivClass');
	$j("#"+id).show();
	if(val1==null || val1==""){
		$j('#'+id1).focus();	
	}
	
		
}
function showNoDiv(el,id,id1){
	var t=$j(el);
	var obj=t.offset();
	var xOffset=180;
	var yOffset=30;
	if(id=='perCentageDiv'){
		xOffset=180+170;
		yOffset=30;
	}
	if($j('.dataEnteringDiv').is(':visible')){
		$j('.dataEnteringDiv').hide();
	}
	if(navigator.appName=="Microsoft Internet Explorer"){yOffset=yOffset+15;}
	$j("#"+id)
			.css({left:obj.left-xOffset,top:obj.top+yOffset})
			.fadeIn("fast").show()
			.addClass('dateDivClass');
	//alert("id"+id+"id1"+id1)
	$j("#"+id).show();
	$j('#'+id1).focus();
}
function fnclear(id1,id2){
	$j('#'+id1).val('');
	$j('#'+id2).val('');
}
function getSiteDetails(pksite,siteid){
	//window.location='getCbbDetails?flagval=123&pksiteid='+pksite;
	submitpost('getCbbDetails',{'flagval':'123','pksiteid':pksite,'moduleEntityId':pksite,'moduleEntityType':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBB" />','moduleEntityIdentifier':siteid,'moduleEntityEventCode':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_CBB" />'});
}
function getCordDetails(pkcord,regId){
	//var url = "loadClinicalDataFromPF?cdrCbuPojo.cordID="+pkcord;
	//window.location=url;
	submitpost('loadClinicalDataFromPF',{'cdrCbuPojo.cordID':pkcord,'moduleEntityId':pkcord,'moduleEntityType':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />','moduleEntityIdentifier':regId,'moduleEntityEventCode':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_CBU" />'});
}
function loadGroup(pkorder,ordertype1,pkcord,fkCbbId,regId)
{
	
	var url="getGroupsByIdsForOrderType?fkCbbId="+fkCbbId+"&orderType="+ordertype1+"&orderId="+pkorder+"&pkcordId="+pkcord+"&userId="+"<s:property value='#request.userId'/>"+"&regIdval="+regId;
	loadPageByGetRequset(url,'usersitegrpsdropdown');
	
}
function getCordEntry(pkcord,regId){
	//window.location='cordEditDataEntry?cdrCbuPojo.cordID='+pkcord;
	submitpost('openWholeCordEntry',{'cdrCbuPojo.cordID':pkcord,'moduleEntityId':pkcord,'moduleEntityType':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />','moduleEntityIdentifier':regId,'moduleEntityEventCode':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_CBU" />'});
}
function showContent(el,DivId,flag){

	if(!$j(el).hasClass('firstLoad')){
		
			var cbb=$j('#cbbdrpdwn').val();
			
			if((cbb!=null && cbb!='')){ 
				$j('#siteIdval').val(cbb);
				if(flag=='flag_activeOrders'){
					if($j('#pendingFilter').val()!=null && $j('#pendingFilter').val()!="" && $j('#pendingFilter').val()=='1'){
						siteIdval=$j('#pfSiteId').val();
						orderType=$j('#pfOrdertype').val();
						//loaddiv('getPSOrders?pkSiteId='+cbb+'&filterPending=1&fkordertype='+orderType+'&pksiteid='+siteIdval+'&iShowRows='+$j('#SE_Pending').val()+'&reqType1='+orderType+'&orderStatus1='+'<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@NEW)"/>'+'&sampleAtlab1='+$j('#pfSampleAtlab').val(),'pendingTblDiv');
						tempParam='&pkSiteId='+cbb+'&filterPending=1&fkordertype='+orderType+'&pksiteid='+siteIdval+'&iShowRows='+$j('#SE_Pending').val()+'&reqType1='+orderType+'&orderStatus1='+'<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@NEW)"/>'+'&sampleAtlab1='+$j('#pfSampleAtlab').val();
						$j('#allparams').val(tempParam);
						var all_entries_flag=$j('#SE_Pending_txt').val();
						$j('#ord_allentries_flag').val(all_entries_flag);
						$j('#activeFilterFlag').val('1');
						
					}else{
						var sv_ord_fval='<%=session.getAttribute("v_actOrd_fval")%>';
						var param1="";
						if(sv_ord_fval!='null' && sv_ord_fval!=''){
							param1=sv_ord_fval.replace(/----/gi,'&');
							$j('#allparams').val(param1);
							$j('#activeFilterFlag').val('1');
							$j('#activeStatFilterFlag').val('1');
						}
						//loaddiv('getPSOrders?pkSiteId='+cbb+'&iShowRows='+$j('#SE_Pending').val()+param1+'&allEntries='+$j('#SE_Pending_txt').val(),'pendingTblDiv');
						var all_entries_flag=$j('#SE_Pending_txt').val();
						$j('#ord_allentries_flag').val(all_entries_flag);
						param1='&pkSiteId='+cbb+param1;
						$j('#allparams').val(param1);
						
					}
					pendingOrdersOnLoad();
					
				}
				if(flag=='flag_otherOrders'){
					
					var tempParam="";
					if($j('#not-available-cbu').is(':visible')){	
						//alert("ddd"+$j('#SE_NACBU_txt').val())
						var all_entries_flag=$j('#SE_NACBU_txt').val();
						$j('#nacbu_allentries_flag').val(all_entries_flag);
						loadmultiplediv('getPSNACBU?pkSiteId='+cbb+'&iShowRows='+$j('#SE_NACBU').val()+$j('#lastFilterValnaCBU').val()+'&allEntries='+all_entries_flag,'not-available-cbu,newFilterNACBU');
						tempParam='&pkSiteId='+cbb+$j('#lastFilterValnaCBU').val();
						$j('#allparamsNaCBU').val(tempParam);
						$j('#resetOtherNA').show('slow');
						$j('#resetOtherSIP').hide('slow');
						notAvailCbuTblOnLoad();
						$j('#not-avail-anchor').addClass('loaded');
					}
					if($j('#cord-entry').is(':visible')){
						//alert('inside show content sip1'+$j('#lastFilterValSIP').val());
						loadmultiplediv('getPSSIP?pkSiteId='+cbb+'&iShowRows='+$j('#SE_SIP').val()+$j('#lastFilterValSIP').val()+'&allEntries='+all_entries_flag,'cord-entry,newFilterSIP');
						var all_entries_flag=$j('#SE_SIP_txt').val();
						tempParam='&pkSiteId='+cbb+$j('#lastFilterValSIP').val();
						$j('#allparamSIP').val(tempParam);
						$j('#sip_allentries_flag').val(all_entries_flag);
						cordTblOnLoad();
						$j('#resetOtherNA').hide('slow');
						$j('#resetOtherSIP').show('slow');
						$j('#cord-entry-anchor').addClass('loaded');
					}
					if(!$j('#cord-entry').is(':visible') && !$j('#cord-entry').is(':visible') && !$j('#funding').is(':visible') && $j.cookie('activeTab')){
						
						 highlight_tab($j.cookie('activeTab'));
						 var idval=$j.cookie('activeTab');
						 $j('#'+idval).triggerHandler("click");
					 	 if(idval=='not-avail-anchor'){
					 			//alert("ddd"+$j('#SE_NACBU_txt').val())
					 			var all_entries_flag=$j('#SE_NACBU_txt').val();
								$j('#nacbu_allentries_flag').val(all_entries_flag);
					 			loadmultiplediv('getPSNACBU?pkSiteId='+cbb+'&iShowRows='+$j('#SE_NACBU').val()+$j('#lastFilterValnaCBU').val()+'&allEntries='+all_entries_flag,'not-available-cbu,newFilterNACBU');
								tempParam='&pkSiteId='+cbb+$j('#lastFilterValnaCBU').val();
								$j('#allparamsNaCBU').val(tempParam);
								notAvailCbuTblOnLoad();
								$j('#resetOtherNA').show('slow');
								$j('#resetOtherSIP').hide('slow');
								$j('#not-avail-anchor').addClass('loaded');
								//$j('#showsRowNA-Cbu-tbl').val($j('#SE_NACBU').val());

						 }
						 if(idval=='cord-entry-anchor'){
								//alert('inside show content sip2'+$j('#lastFilterValSIP').val());
								var all_entries_flag=$j('#SE_SIP_txt').val();
								tempParam='&pkSiteId='+cbb+$j('#lastFilterValSIP').val();
								$j('#allparamSIP').val(tempParam);
								loadmultiplediv('getPSSIP?pkSiteId='+cbb+'&iShowRows='+$j('#SE_SIP').val()+$j('#lastFilterValSIP').val()+'&allEntries='+all_entries_flag,'cord-entry,newFilterSIP');
								$j('#sip_allentries_flag').val(all_entries_flag);
								cordTblOnLoad();
								$j('#resetOtherNA').hide('slow');
								$j('#resetOtherSIP').show('slow');								
								$j('#cord-entry-anchor').addClass('loaded');
								$j('#showsRowcordentrytbl').val($j('#SE_SIP').val());
						 } 
						
						 
					}else if(!$j('#cord-entry').is(':visible') && !$j('#cord-entry').is(':visible') && !$j('#funding').is(':visible') && !$j.cookie('activeTab')){
						var all_entries_flag=$j('#SE_NACBU_txt').val();
						//alert("ddd"+$j('#SE_NACBU_txt').val())
						$j('#nacbu_allentries_flag').val(all_entries_flag);
						tempParam='&pkSiteId='+cbb+$j('#lastFilterValnaCBU').val();
						$j('#allparamsNaCBU').val(tempParam);
						loadmultiplediv('getPSNACBU?pkSiteId='+cbb+'&iShowRows='+$j('#SE_NACBU').val()+$j('#lastFilterValnaCBU').val()+'&allEntries='+all_entries_flag,'not-available-cbu,newFilterNACBU');					
						notAvailCbuTblOnLoad();
						$j('#resetOtherNA').show('slow');
						$j('#resetOtherSIP').hide('slow');
						$j('#not-avail-anchor').addClass('loaded');
						
					}
					
				}
				
				$j(el).addClass('firstLoad');
				
			}
			
	}
	
	
} 
function getFilterDataForOtherReq(){
	var sv_nacbu_fval='<%=session.getAttribute("v_naCBU_fval")%>';
	var sv_sip_fval='<%=session.getAttribute("v_SIP_fval")%>';
	var param1="";
	var param2="";
	if(sv_nacbu_fval!='null' && sv_nacbu_fval!=''){
		param1=sv_nacbu_fval.replace(/----/gi,'&');
		param1='&pkSiteId='+$j('#cbbdrpdwn').val()+param1;
		$j('#allparamsNaCBU').val(param1);
		$j('#naCBUFilterFlag').val('1');
		$j('#naCBUStatFilterFlag').val('1');
	}
	$j('#lastFilterValnaCBU').val(param1);
	$j('#allparamsNaCBU').val(param1);
	if(sv_sip_fval!='null' && sv_sip_fval!=''){
		$j('#cordStatFilterFlag').val('1');
		param2=sv_sip_fval.replace(/----/gi,'&');
		$j('#allparamSIP').val(param2);
		$j('#cordFilterFlag').val('1');
	}
	$j('#lastFilterValSIP').val(param2);
}
function getPSOrdesParam(){
	
	if($j('#allparams').val()==null || $j('#allparams').val()=="" || $j('#allparams').val()==undefined){
		var all_entries_flag=$j('#showsRowtablelookup :selected').text();
		var temp=getSessionForOrders();
		orderparams='&pkSiteId='+$j('#cbbdrpdwn').val()+temp;
		$j('#allparams').val(orderparams);
	}
	//alert("inside ordersparam:"+$j('#allparams').val());

}
function getCBBdetails(pksite){
	setTimeout(function(){
		showprogressMgs();
	},0);
	setTimeout(function(){
	var visibleCount=0;
	var cbb=$j('#cbbdrpdwn').val();
	$j('.ui-icon-minusthick').each(function(i){
		visibleCount=visibleCount+1;
	});
	$j('.ui-icon-plusthick').each(function(i){
		$j(this).removeClass('firstLoad');
	});
	$j('#allparamsNaCBU').val('');
	$j('#allparams').val('');
	$j('#allparamSIP').val('');
	$j('.loaded').each(function(i){
		if($j(this).attr("id")!="cord-entry-anchor"){
			$j(this).removeClass('loaded');
		}
	});
	var allCBB='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_SITE"/>';
	var allCBBFlag=0;
	var countCBB=0;
	//alert("pksite"+pksite)
	$j('#cbbdrpdwn :selected').each(function(i){
		temp=$j(this).val();
		if(temp==allCBB){
			allCBBFlag=1;
		}
		countCBB++;
	});
	//alert("countCBB::"+countCBB+"allCBBFlag"+allCBBFlag)
	if(countCBB>1 && allCBBFlag==1){
		$j('#cbbdrpdwn').val(allCBB);
		pksite=allCBB;
	}
	
	if(countCBB==0){
		$j('#is_cbb_selected').val('0');
	}

	if($j('#pfSiteId').val()!=pksite){
		 $j('#pendingFilter').val('');
		 $j('#pfSiteId').val('');
		 $j('#pfOrdertype').val('');
	}
	if(visibleCount==0 && cbb!=null && cbb!='null'){
		
		var cbbval=$j('#is_cbb_selected').val();
		if(cbbval=='0'){
			//alert("inside expected.....");
			setTimeout(function(){
				getPSOrdesParam();
				var all_entries_flag=$j('#ord_allentries_flag').val();
				loaddiv('getPSOrders?iShowRows='+all_entries_flag+$j('#allparams').val(),'pendingTblDiv');
				$j('.activeOrder').triggerHandler('click');
				$j('.activeOrder').addClass('firstLoad');
				//pendingOrdersOnLoad();
			},0);
			
		
		}else{
			$j('.activeOrder').triggerHandler('click');
			/*$j('.otherRequestWid').triggerHandler('click');
			removingCSS();*/
			$j('.activeOrder').addClass('firstLoad');
			/*$j('.otherRequestWid').addClass('firstLoad');*/
			
		}
		
		
	}else{
		if($j('#pendingTblDiv').is(':visible')){
			getPSOrdesParam();
			var all_entries_flag=$j('#showsRowtablelookup :selected').text();
			$j('#ord_allentries_flag').val(all_entries_flag);
			loaddiv('getPSOrders?iShowRows='+$j('#showsRowtablelookup').val()+$j('#allparams').val(),'pendingTblDiv');
			pendingOrdersOnLoad();
		}
		if($j('#not-available-cbu').is(':visible')){
			getPS_NA_CBUParam();
			var all_entries_flag=$j('#showsRowNA-Cbu-tbl :selected').html();
			$j('#nacbu_allentries_flag').val(all_entries_flag);
			loaddiv('getPSNACBU?&iShowRows='+$j('#showsRowNA-Cbu-tbl').val()+'&allEntries='+all_entries_flag+$j('#allparamsNaCBU').val(),'not-available-cbu');
			notAvailCbuTblOnLoad();
			removingCSS();
			$j('#not-available-cbu').addClass('loaded');
		}
		if($j('#cord-entry').is(':visible')){
			getPS_SIPParam();
			var all_entries_flag=$j('#showsRowcordentrytbl :selected').html();
			$j('#sip_allentries_flag').val(all_entries_flag);
			loadmultiplediv('getPSSIP?&iShowRows='+$j('#showsRowcordentrytbl').val()+'&allEntries='+all_entries_flag+$j('#allparamSIP').val(),'cord-entry');
			cordTblOnLoad();
			$j('#cord-entry-anchor').addClass('loaded');
		}
		
	}
     },100);
	setTimeout(function(){
		closeprogressMsg();
	},0);
}
function ExecuteCustomFilterOrd(value1){
	
	if(value1!=""){
		ordersparam='&pkSiteId='+$j('#cbbdrpdwn').val()+value1;
		$j('#allparams').val(ordersparam);
		var all_entries_flag=$j('#showsRowtablelookup :selected').text();
		$j('#ord_allentries_flag').val(all_entries_flag);
		loaddiv('getPSOrders?iShowRows='+$j('#showsRowtablelookup').val()+$j('#allparams').val(),'pendingTblDiv');
		$j('#activeFilterFlag').val('1');
		$j('#allparams').val(ordersparam); 
		pendingOrdersOnLoad();
		$j('#customFilters').val(value1);
		$j('#activeFilterFlag').val('1');
		$j('#filtername').val('');
		$j('#activeStatFilterFlag').val('1');
	}
		
}
function ExecuteCustomFilterNA(value1){
	
	if(value1!=""){
		nacbuparams='&pkSiteId='+$j('#cbbdrpdwn').val()+value1;
		$j('#allparamsNaCBU').val(nacbuparams);
		var all_entries_flag=$j('#showsRowNA-Cbu-tbl :selected').text();
		$j('#nacbu_allentries_flag').val(all_entries_flag);
		loaddiv('getPSNACBU?iShowRows='+$j('#showsRowNA-Cbu-tbl').val()+$j('#allparamsNaCBU').val(),'not-available-cbu');
		$j('#naCBUFilterFlag').val('1');
		notAvailCbuTblOnLoad();
		removingCSS();
		$j('#allparamsNaCBU').val(nacbuparams);
		$j('#not-available-cbu').addClass('loaded');  
		$j('#customFiltersNACBU').val(value1);
		$j('#filtername1').val('');
		$j('#naCBUStatFilterFlag').val('1');
	}
	
}
function ExecuteCustomFilterSIP(value1){
	
	if(value1!=""){
		$j('#cordStatFilterFlag').val('1');
			sipParams='&pkSiteId='+$j('#cbbdrpdwn').val()+value1;
			$j('#allparamSIP').val(sipParams);
			var all_entries_flag=$j('#showsRowcordentrytbl :selected').text();
			$j('#sip_allentries_flag').val(all_entries_flag);
			loaddiv('getPSSIP?iShowRows='+$j('#showsRowcordentrytbl').val()+$j('#allparamSIP').val(),'cord-entry');
			$j('#cordFilterFlag').val('1');
			cordTblOnLoad(); 
			$j('#allparamSIP').val(sipParams);
			$j('#customFilterssip').val(value1);
			$j('#filtername1').val('');
	}
	
}
function fn_FilterOrders(){
	
	var reqType="";
	var orderPriority="";
	var orderStatus="";
	var closeReason="";
	var resolution="";
	var assignTo="";
	var reviewBy="";
	var noOfdays="";
	var dateStart="";
	var dateEnd="";
	var dontflag='0';
	var ordersparam="";
	var sampleAtlab="";
	var cbbid="";
	var cbbSate="";
	var cbuRegId="";
	var cbuLocId="";
	var acknowl="";
	var reqTypeAll="0";
	var FilterValFlag="0";
	var dateData="0";
	var noofdaysData="0";
	$j('#activeStatFilterFlag').val('1');
	reqType=$j('#orderType1 :selected').text();
	orderPriority=$j('#orderPriority1').val();
	orderStatus=$j('#orderStatus1').val();
	closeReason=$j('#closeReason1').val();
	resolution=$j('#resolution1').val();
	assignTo=$j('#assignTo1').val();
	reviewBy=$j('#reviewby').val();
	noOfdays=$j('#noOfdaystxt').val();
	dateStart=$j('#dateStart').val();
	dateEnd=$j('#dateEnd').val();
	cbbid=$j.trim($j('#siteSeq1').val());
	cbbSate=$j.trim($j('#satellite1').val());
	cbuRegId=$j.trim($j('#regId1').val());
	cbuLocId=$j.trim($j('#locId1').val());
	acknowl=$j('#ackresolution1').val();
	
	if(reqType=='<%=VelosMidConstants.CT_LAB_ORDER%>'){ 
		sampleAtlab='Y';
	}
	if(reqType=='<%=VelosMidConstants.CT_SHIP_ORDER%>'){ 
		sampleAtlab='N';
	}
	if(reqType=='ALL'){ 
		reqTypeAll='1';
	}
	if(reqType!=''){ 
		reqType=$j('#orderType1').val();
	}
	
	
	
	
	if($j('#dateStart').val()!="" && $j('#dateEnd').val()!=""){
		var dateElements = $j('.datepic').get();
		  $j.each(dateElements, function(){
		customRange(this);
		  });
		if($j('#dummyForm').valid()){
		dateStart=parseDateValue($j('#dateStart').val());
		dateEnd=parseDateValue($j('#dateEnd').val());
		}
		else{
			$j('#dateDiv').show();
			dontflag=1;
			}
		dateData="1";
		
	}else if( ($j('#dateStart').val()!="" && $j('#dateEnd').val()=="" ) || ($j('#dateStart').val()=="" && $j('#dateEnd').val()!="")){
		
		alert("Please enter correct Date Range");
		showDiv($j('#ordRange'),"dateDiv","dateStart","dateEnd");
		dontflag=1;
		   
	}
	if(noOfdays!=""){
		if(IsNumeric(noOfdays)){
			noOfdays=$j('#noOfdaystxt').val();
			noofdaysData="1";
		}else{
			alert("Please enter numerical value only");
			$j('#noOfdaystxt').val('');
			showNoDiv($j('#noOfdays'),"noDiv","noOfdaystxt");
			dontflag=1;
		}
	}
	
	
	if(reqType!="" || orderPriority!="" || orderStatus!="" || closeReason!="" || resolution!="" || assignTo!="" || $j.trim(cbbid)!="" || $j.trim(cbbSate)!="" || cbuRegId!="" || cbuLocId!="" || acknowl!="" || dateData!="0" || noofdaysData!="0" || reviewBy!=""){
		
		FilterValFlag="1";
		
	}
	
	if(dontflag==0 && FilterValFlag=="1"){
		var all_entries_flag=$j('#showsRowtablelookup :selected').text();
		ordersparam='&pkSiteId='+$j('#cbbdrpdwn').val()+'&reqType1='+reqType+'&orderPriority1='+orderPriority+'&orderStatus1='+orderStatus+'&closeReason1='+closeReason+'&resolution1='+resolution+'&assignTo1='+assignTo+'&reviewBy1='+reviewBy+'&dateStart1='+dateStart+'&dateEnd1='+dateEnd+'&noOfdays1='+noOfdays+"&sampleAtlab1="+sampleAtlab+"&siteSeq="+cbbid+"&satellite="+cbbSate+"&regId="+cbuRegId+"&locId="+cbuLocId+"&ackresolution="+acknowl+"&reqTypeAllFlag="+reqTypeAll;
		$j('#allparams').val(ordersparam);
		$j('#ord_allentries_flag').val(all_entries_flag);
		var temp=getSessionForOrders();
		loaddiv('getPSOrders?iShowRows='+$j('#showsRowtablelookup').val()+$j('#allparams').val()+'&allEntries='+all_entries_flag+temp,'pendingTblDiv');
		$j('#activeFilterFlag').val('1');
		$j('#allparams').val(ordersparam); 
		pendingOrdersOnLoad();
		
	}
	
	
}
function getPS_SIPParam(){
	
	if($j('#allparamSIP').val()==null || $j('#allparamSIP').val()=="" || $j('#allparamSIP').val()==undefined){
		//var all_entries_flag=$j('#showsRowcordentrytbl :selected').text();
		sipParams='&pkSiteId='+$j('#cbbdrpdwn').val();
		$j('#allparamSIP').val(sipParams);
	}
	
}
function getPS_NA_CBUParam(){
	if($j('#allparamsNaCBU').val()==null || $j('#allparamsNaCBU').val()=="" || $j('#allparamsNaCBU').val()==undefined){
		//var all_entries_flag=$j('#showsRowNA-Cbu-tbl :selected').text();
		nacbuparams='&pkSiteId='+$j('#cbbdrpdwn').val();
		$j('#allparamsNaCBU').val(nacbuparams);
	}
}
function fn_FilterOthers(){
	
	if($j('#not-available-cbu').is(':visible')){
		
		var cbuStatus="";
		var cbuReason="";
		var removedBy="";
		var nacbuparams="";
		var siteId="";
		var regId="";
		var locId="";
		$j('#naCBUStatFilterFlag').val('1');
		cbuStatus=$j('#cbuStatus1').val();
		cbuReason=$j('#cbuReason1').val();
		removedBy=$j('#reviewby1').val();
		siteId=$j.trim($j('#nasiteId1').val());
		regId=$j.trim($j('#naregId1').val());
		locId=$j.trim($j('#nalocId1').val());
		
		if(cbuStatus!="" || cbuReason!="" || removedBy!="" || siteId!="" || regId!="" || locId!=""){
			var all_entries_flag=$j('#showsRowNA-Cbu-tbl :selected').text();
			nacbuparams='&pkSiteId='+$j('#cbbdrpdwn').val()+'&nastatus='+cbuStatus+'&nareason='+cbuReason+'&naremovedby='+removedBy+'&nasiteId='+siteId+'&naregId='+regId+'&nalocId='+locId;
			//alert("nacbuparams inside going action="+nacbuparams);
		 	$j('#allparamsNaCBU').val(nacbuparams);
			$j('#nacbu_allentries_flag').val(all_entries_flag);
			loaddiv('getPSNACBU?iShowRows='+$j('#showsRowNA-Cbu-tbl').val()+$j('#allparamsNaCBU').val()+'&allEntries='+all_entries_flag,'not-available-cbu');
			notAvailCbuTblOnLoad();
			removingCSS();
			$j('#naCBUFilterFlag').val('1');
			$j('#allparamsNaCBU').val(nacbuparams);
			$j('#not-available-cbu').addClass('loaded');  
			
		}
		
	}
	if($j('#cord-entry').is(':visible')){
		
		var dateStart1="";
		var dateEnd1="";
		var noOfdays="";
		var percentageComplete="";
		var siteId="";
		var regId="";
		var locId="";
		var dontflag=0;
		var sipParams="";
		var filterValFlag="0";
		var dateData="0";
		var noofdaysData="0";
		var percentageData="0";
		$j('#cordStatFilterFlag').val('1');
		dateStart1=$j('#dateStart1').val();
		dateEnd1=$j('#dateEnd1').val();
		noOfdays=$j.trim($j('#noOfdaystxt1').val());
		percentageComplete=$j.trim($j('#sippercentval').val());
		siteId=$j.trim($j('#sipsiteId1').val());
		regId=$j.trim($j('#sipregId1').val());
		locId=$j.trim($j('#siplocId1').val());
		
		if($j.trim($j('#dateStart1').val())!="" && $j.trim($j('#dateEnd1').val())!=""){
			var dateElements = $j('.datepic').get();
			  $j.each(dateElements, function(){
			customRange1(this);
			  });
			if($j('#dummyForm').valid()){
			dateStart1=parseDateValue($j('#dateStart1').val());
			dateEnd1=parseDateValue($j('#dateEnd1').val());
			}
			else{
				$j('#dateDiv1').show();
				}
			dateData="1";
			
		}else if( ($j('#dateStart1').val()!="" && $j('#dateEnd1').val()=="" ) || ($j('#dateStart1').val()=="" && $j('#dateEnd1').val()!="")){
			
			alert("Please enter correct Date Range");
			dontflag=1;
			   
		}
		
		if(IsNumeric(noOfdays)){
			noOfdays=$j('#noOfdaystxt1').val();
			noofdaysData="1";
		}else if(noOfdays==""){
			noofdaysData="0";
		}else{
			alert("Please enter numerical value in No of Days");
			dontflag=1;
		}
		
		
		if(IsNumeric(percentageComplete)){
			percentageComplete=$j('#sippercentval').val();
			percentageData="1";
		}else if(percentageComplete==""){
			percentageData="0";
		}else{
			alert("Please enter numerical value in Percentage completion");
			dontflag=1;
		}
		
		
		if(siteId!="" || regId!="" || locId!="" || dateData!="0" || noofdaysData!="0" || percentageData!="0"){
			filterValFlag="1";
		}
		
		if(dontflag==0 && filterValFlag=="1"){
			var all_entries_flag=$j('#showsRowcordentrytbl :selected').text();
			sipParams='&pkSiteId='+$j('#cbbdrpdwn').val()+'&sippercent='+percentageComplete+'&sipnoofdays='+noOfdays+'&sipStartDt='+dateStart1+'&sipEndDt='+dateEnd1+'&sipsiteId='+siteId+'&sipregId='+regId+'&siplocId='+locId;
			$j('#allparamSIP').val(sipParams);
			$j('#sip_allentries_flag').val(all_entries_flag);
			loaddiv('getPSSIP?iShowRows='+$j('#showsRowcordentrytbl').val()+$j('#allparamSIP').val()+'&allEntries='+all_entries_flag,'cord-entry');
			$j('#cordFilterFlag').val('1');
			cordTblOnLoad(); 
			$j('#allparamSIP').val(sipParams); 
		}
		
	}
	
}
function ShowEntries_Sort_Data(){
	
	var sv_ord_se='<%=session.getAttribute("v_actOrd_SE")%>';
	var sv_nacbu_se='<%=session.getAttribute("v_naCBU_SE")%>';
	var sv_sip_se='<%=session.getAttribute("v_SIP_SE")%>';
	
	var sv_ord_se_txt='<%=session.getAttribute("v_actOrd_SE_txt")%>';
	var sv_nacbu_se_txt='<%=session.getAttribute("v_naCBU_SE_txt")%>';
	var sv_sip_se_txt='<%=session.getAttribute("v_SIP_SE_txt")%>';
	
	if(sv_ord_se!=null  && sv_ord_se!='' && sv_ord_se!='null' && sv_ord_se!='0'){
	    $j('#SE_Pending').val(sv_ord_se);
	}else{
		$j('#SE_Pending').val('10');
	}
	if(sv_nacbu_se!=null && sv_nacbu_se!='' && sv_nacbu_se!='null' && sv_nacbu_se!='undefined' && sv_nacbu_se!='0'){
	    $j('#SE_NACBU').val(sv_nacbu_se);
	}else{
		$j('#SE_NACBU').val('10');
	}
	if(sv_sip_se!=null  && sv_sip_se!='' && sv_sip_se!='null' && sv_sip_se!='undefined' && sv_sip_se!='0'){
	    $j('#SE_SIP').val(sv_sip_se);
	}else{
		  $j('#SE_SIP').val('10');
	}
	
	
	
	if(sv_ord_se_txt!=null  && sv_ord_se_txt!='' && sv_ord_se_txt!='null' && sv_ord_se_txt!='0'){
		$j('#SE_Pending_txt').val(sv_ord_se_txt);
	}else{
		$j('#SE_Pending_txt').val('10');
	}
	if(sv_nacbu_se_txt!=null && sv_nacbu_se_txt!='' && sv_nacbu_se_txt!='null' && sv_nacbu_se_txt!='undefined' && sv_nacbu_se_txt!='0'){
	    $j('#SE_NACBU_txt').val(sv_nacbu_se_txt);
	}else{
		$j('#SE_NACBU_txt').val('10');
	}
	if(sv_sip_se_txt!=null  && sv_sip_se_txt!='' && sv_sip_se_txt!='null' && sv_sip_se_txt!='undefined' && sv_sip_se_txt!='0'){
	    $j('#SE_SIP_txt').val(sv_sip_se_txt);
	}else{
		  $j('#SE_SIP_txt').val('10');
	}
	
	
	var sv_ord_sc='<%=session.getAttribute("v_actOrd_SC")%>';
	var sv_nacbu_sc='<%=session.getAttribute("v_NACBU_SC")%>';
	var sv_sip_sc='<%=session.getAttribute("v_SIP_SC")%>';
	
	var sv_ord_st='<%=session.getAttribute("v_actOrd_ST")%>';
	var sv_nacbu_st='<%=session.getAttribute("v_NACBU_ST")%>';
	var sv_sip_st='<%=session.getAttribute("v_SIP_ST")%>';
	
	if(sv_ord_sc!=null  && sv_ord_sc!='' && sv_ord_sc!='null' && sv_ord_sc!='0'){
		$j('#ord_sortParam').val(sv_ord_sc);
		$j('#ord_sort_type').val(sv_ord_st);
	}
	if(sv_nacbu_sc!=null  && sv_nacbu_sc!='' && sv_nacbu_sc!='null' && sv_nacbu_sc!='0'){
		$j('#naCBU_sortParam').val(sv_nacbu_sc);
		$j('#naCBU_sort_type').val(sv_nacbu_st);
	}
	if(sv_sip_sc!=null  && sv_sip_sc!='' && sv_sip_sc!='null' && sv_sip_sc!='0'){
		$j('#sip_sortParam').val(sv_sip_sc);
		$j('#sip_sort_type').val(sv_sip_st);
	}
	
	
	//alert("value::::::::"+$j('#SE_Pending').val()+":txtvalue:::"+$j('#SE_Pending_txt').val()+":svalue:"+sv_ord_se_txt)
}
function togglecbbIdDiv(obj){
	$j(obj).toggleClass("ui-icon-minusthick-lp").toggleClass("ui-icon-plusthick-lp");
	$j(obj).parent().next('div').toggle();
} 
function fn_setSort_ord(label,el){
	
	var sort_to="";
	var sorted=false;
	$j("#ord_header").val(label);
	var pagno=$j('#ordTblFoot div:first').find('b').find('.currentPage').text();
	if($j(el).hasClass('sorting') || $j(el).hasClass('sorting_desc')){
		sort_to='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SORT_ASC"/>';
		var temp_sort_Param='&'+label+'='+sort_to;
		$j('#ord_sortParam').val(temp_sort_Param);
		$j("#ord_sort_type").val('sorting_asc');
		paginationFooter(pagno,"getPSOrders,showsRowtablelookup,temp,pendingOrderForm,pendingTblDiv,pendingOrdersOnLoad,pendingFilter,pfSiteId,pfOrdertype,allparams,ord_sortParam,ord_allentries_flag");
		sorted=true;
	}
	if($j(el).hasClass('sorting_asc') && !sorted){
		sort_to='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SORT_DES"/>';
		var temp_sort_Param='&'+label+'='+sort_to;
		$j('#ord_sortParam').val(temp_sort_Param);
		$j("#ord_sort_type").val('sorting_desc');
		paginationFooter(pagno,"getPSOrders,showsRowtablelookup,temp,pendingOrderForm,pendingTblDiv,pendingOrdersOnLoad,pendingFilter,pfSiteId,pfOrdertype,allparams,ord_sortParam,ord_allentries_flag");
		sorted=true;
	}
}
function fn_setSort_naCBU(label,el){
	var sort_to="";
	var sorted=false;
	var pageno=$j('#naTblFoot div:first').find('b').find('.currentPage').text();
	$j("#naCBU_header").val(label);
	if($j(el).hasClass('sorting') || $j(el).hasClass('sorting_desc')){
		sort_to='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SORT_ASC"/>';
		var temp_sort_Param='&'+label+'='+sort_to;
		$j('#naCBU_sortParam').val(temp_sort_Param);
		$j("#naCBU_sort_type").val('sorting_asc');
		paginationFooter(pageno,"getPSNACBU,showsRowNA-Cbu-tbl,temp,pendingOrderForm,not-available-cbu,notAvailCbuTblOnLoad,naCBUflag,allparamsNaCBU,naCBU_sortParam,nacbu_allentries_flag");
		sorted=true;
	}
	if($j(el).hasClass('sorting_asc') && !sorted){
		sort_to='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SORT_DES"/>';
		var temp_sort_Param='&'+label+'='+sort_to;
		$j('#naCBU_sortParam').val(temp_sort_Param);
		$j("#naCBU_sort_type").val('sorting_desc');
		paginationFooter(pageno,"getPSNACBU,showsRowNA-Cbu-tbl,temp,pendingOrderForm,not-available-cbu,notAvailCbuTblOnLoad,naCBUflag,allparamsNaCBU,naCBU_sortParam,nacbu_allentries_flag");
		sorted=true;
	}
	
}
function fn_setSort_sip(label,el){
	var sort_to="";
	var sorted=false;
	$j("#sip_header").val(label);
	var pageno=$j('#sipTblFoot div:first').find('b').find('.currentPage').text(); 
	if($j(el).hasClass('sorting') || $j(el).hasClass('sorting_desc')){
		sort_to='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SORT_ASC"/>';
		var temp_sort_Param='&'+label+'='+sort_to;
		$j('#sip_sortParam').val(temp_sort_Param);
		$j("#sip_sort_type").val('sorting_asc');
		paginationFooter(pageno,"getPSSIP,showsRowcordentrytbl,temp,pendingOrderForm,cord-entry,cordTblOnLoad,sipflag,allparamSIP,sip_sortParam,sip_allentries_flag");
		sorted=true;
	}
	if($j(el).hasClass('sorting_asc') && !sorted){
		sort_to='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SORT_DES"/>';
		var temp_sort_Param='&'+label+'='+sort_to;
		$j('#sip_sortParam').val(temp_sort_Param);
		$j("#sip_sort_type").val('sorting_desc');
		paginationFooter(pageno,"getPSSIP,showsRowcordentrytbl,temp,pendingOrderForm,cord-entry,cordTblOnLoad,sipflag,allparamSIP,sip_sortParam,sip_allentries_flag");
		sorted=true;
	}
	
}
function setAssignAction(el){
	if($j(el).val()!=''){
		$j(el).addClass('assignTo_Action');
	}
	
}
function setAckAction(el){
	if($j(el).val()!=''){
		$j(el).addClass('ack_Action');
	}
}
function cancelCustomFilter(){
	$j('#customOFilterDiv').hide();$j('#new_Other_filterFlag').val('');
}
function cancelCustomFilterOrd(){
	$j('#customFilterDiv').hide();$j('#new_Ord_filterFlag').val('');
}
function printWidgets(divData,printDiv){
	var otherDivFlag="0";
	if(printDiv=="printOtherDiv" && !$j("#otherSpan").hasClass('ui-icon-minusthick')){
		$j("#otherSpan").trigger("click");
		otherDivFlag="1";
	}
	setTimeout(function(){
	$j('#'+printDiv).removeClass('hide-data');
	if(divData == 'activeOrdDiv'){
		$j('#noNameDiv').html($j('#'+printDiv).html());
		tableAlignmentInPrint('noNameDiv','tablelookup');
		clickheretoprint('noNameDiv','');
	}else if(divData=='otherOrdDiv'){
		var idOfPrint = document.getElementById("printingdiv").value;
		var sourceFile = '<div class="portlet-header">'+$j("#printingdivTitle").val()+'</div><br/>'+$j("#"+idOfPrint).html();
		$j('#noNameDiv').html(sourceFile);
		var prId = idOfPrint.split("_");
		if(prId[0]!=''){
			tableAlignmentInPrint('noNameDiv',prId[0]);
		}
		clickheretoprint('noNameDiv','');
	}
	else{
	clickheretoprint(divData,'');
	}
	$j('#'+printDiv).addClass('hide-data');
	if(otherDivFlag=="1"){
		$j("#otherSpan").trigger("click");
	}
	},3000);
}
function printPendingTab(printDiv,title){
	document.getElementById("printingdiv").value = printDiv;
	document.getElementById("printingdivTitle").value = title;
}

</script>