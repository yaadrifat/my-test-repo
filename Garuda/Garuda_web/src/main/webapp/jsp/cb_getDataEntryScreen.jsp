<%@ taglib prefix="s"  uri="/struts-tags"%>
<script>
var cotextpath =cotextpath; 
function newfileupload(){
	//$j("#newdocpop").dialog(mode);
	var url = cotextpath+"/openfileupload.action?attCodeType=ATTACHMENT_TYPE&attCodeSubType=UNIT_REPORT&codeEntityType=ENTITY_TYPE&codeEntitySubType=CBU&cordID=<s:property value='cdrCbuPojo.cordID' />";
	window.open( url);				
 }
 



var attid= $j("#attachmentid").val();
var pkcordextinfoid = $j("#pkcordextinfoid").val();
var pkhlaextinfo = $j("#pkhlaextinfo").val();

function saveCBUUnitReport(cdrcbuId){

	/* if($j('#completedby').val()==null || $j('#completedby').val()==""){
     alert("Please enter Completed By!");
     $j('#completedby').focus();
    }else { */
       // alert(attid +"-"+pkcordextinfoid+"-"+pkhlaextinfo);
    	if($j("#CBUUnitRptForm").valid()){ 	
		$j.ajax({
			type : "POST",
			async : false,
			url : 'saveCBUUnit?cdrcbuId='+cdrcbuId+'&attachmentid='+attid+'&pkcordextinfoid='+pkcordextinfoid+'&pkhlaextinfo='+pkhlaextinfo,
			data : $j("#CBUUnitRptForm").serialize(),
			success : function(result) {
				//showUnitReport("close");
				$j('#main').html(result);
				
			}
		
		});
		
    }
    
}

$j(function(){
	$j("#CBUUnitRptForm").validate({
			
		rules:{			
				//"cbuUnitReporttempPojo.cordViabPostProcess" : {lessthanHundred : true },
				"cbuUnitReporttempPojo.cordViabPostProcess" : {greaterThanZero : true },
				//"cbuUnitReporttempPojo.cordCfuPostProcessCount" : {lessthanHundred : true },
				"cbuUnitReporttempPojo.cordCfuPostProcessCount" : {greaterThanZero : true },
				//"cbuUnitReporttempPojo.cordCbuPostProcessCount" : {lessthanHundred : true },
				"cbuUnitReporttempPojo.cordCbuPostProcessCount" : {greaterThanZero : true },
				"cbuUnitReporttempPojo.cordCompletedBy":"required"
			},
			messages:{
				"cbuUnitReporttempPojo.cordViabPostProcess":"<s:text name="garuda.cbu.unitreport.viabpostprocesbetween0to100"/>",
				"cbuUnitReporttempPojo.cordCfuPostProcessCount":"<s:text name="garuda.cbu.unitreport.cfupostprocesscountbetween0to100"/>",
				"cbuUnitReporttempPojo.cordCbuPostProcessCount":"<s:text name="garuda.cbu.unitreport.CbuPostProcessCountbetween0to100"/>",
				"cbuUnitReporttempPojo.cordCompletedBy":"<s:text name="garuda.cbu.unitreport.completedby"/>"
			}
		});
	});

$j(function() {
	var today = new Date();
	
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();

	var h=today.getHours();
	var mn=today.getMinutes()+1;


	$j( "#datepicker1" ).datepicker({
			dateFormat: 'M dd, yy',
			maxDate: new Date(y, m, d)
		});
	
	$j( "#datepicker2" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker3" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	//$j( "#datepicker4" ).datepicker({dateFormat: 'M dd, yy'});
	
});
function show(checkboxName,divName) {
	if(checkboxName.checked == true) {
		document.getElementById(divName).style.display='block';
	} else {
		document.getElementById(divName).style.display='none';
	}
}
function checkBx1(){
	
	var checkBox=document.getElementById("cfuStatusCheckbox");
	
	  if(checkBox.checked==true){
		
	   document.getElementById("isCfu").value=1;
	  }
	   else{
		   document.getElementById("isCfu").value=0;
		   }
	  show(checkBox,"viab2");
}
function checkBx2(){
	
	var checkBox1=document.getElementById("viabStatusCheckbox");
	
	  if(checkBox1.checked==true){
		 
	   document.getElementById("isViab").value=1;
	  }
	   else{
		   document.getElementById("isViab").value=0;
		   }
	  show(checkBox1,"viab");
}
function checkBx3(){
	
	var checkBox2=document.getElementById("cbuStatusCheckbox");
	
	  if(checkBox2.checked==true){
		//  alert(checkBox2.checked);
	   document.getElementById("isCbu").value=1;
	   
	  }
	   else{
		   document.getElementById("isCbu").value=0;
		   }
	  show(checkBox2,"viab3");
}

$j(document).ready(function(){
	
	//$j("#cfuStatusCheckbox").attr("checked",false);
	//$j("#viabStatusCheckbox").attr("checked",false);
//$j("#cbuStatusCheckbox").attr("checked",false);
	checkBx2();
	checkBx1();
	checkBx3();
	});
	
/* jQuery.validator.addMethod("lessthanHundred", function(value, element) {
    return this.optional(element) || (parseFloat(value) < 100);
}, "Percentage must be less than Hundred"); */

jQuery.validator.addMethod("greaterThanZero", function(value, element) {
    return this.optional(element) || (parseFloat(value) <= 100.00);
}, "<s:text name="garuda.cbu.widget.percentage"/>");


function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if ((charCode > 31 && (charCode < 46 || charCode > 57)) || charCode==45)
      return false;

   return true;
}

function showAddWidget(url){
	 url = url + "?pageId=13" ; 
	 showAddWidgetModal(url);
}
</script>




<div class="col_100 maincontainer ">
<div class="col_100">



<s:form action=""
	id="CBUUnitRptForm" name="CBUUnitRptForm">
	<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
	<s:hidden name="attachmentid" id="attachmentid" value="%{attachmentid}"></s:hidden>
	
	<s:hidden name="pkcordextinfoid" id="pkcordextinfoid" value="%{pkcordextinfoid}"></s:hidden>
	<s:hidden name="pkhlaextinfo" id="pkhlaextinfo" value="%{pkhlaextinfo}"></s:hidden>
	<table  border="0" align="center" cellpadding="0" cellspacing="0"
		id="unitreportrequest" class="tabledisplay">
		<thead>
			<tr>
				<th align="center"><s:text
					name="garuda.unitreport.label.heading" /></th>

			</tr>
		</thead>
	</table>

	<div id="fileupload" class="tabledisplay">
	<table class="tabledisplay" align="center" border="0">
		<tbody>
			<tr>
				<td><s:text name="garuda.unitreport.label.sublabel" /><s:property
					value="cdrCbuPojo.cdrCbuId" /></td>
			</tr>
		</tbody>
	</table>
	</div>
	 <div class="portlet" id="cbuinfodiv">
			  <div id="cbuinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			  <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			  <span class="ui-icon ui-icon-minusthick"></span><span class="ui-icon ui-icon-close" ></span>
			  <s:text name="garuda.unitreport.label.cbuinfo.subheading" /></div>
			  <div class="portlet-content">
			  <div id="cbuinfo1" >				
	<table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="2" class="tabledisplay">


		<tr>
			<td><s:text name="garuda.unitreport.label.regunitid" /></td>
			<td><s:textfield 
				disabled="true" id="regid" name="cdrCbuPojo.registryId" /></td>

			<td><s:text name="garuda.unitreport.label.idnocbubag" /></td>
			<td><s:textfield 
				disabled="true" name="cdrCbuPojo.numberOnCbuBag" id="cbubagid" /></td>
		</tr>
		<tr>
			<td><s:text name="garuda.unitreport.label.localcbuid" /></td>
			<td><s:textfield 
				disabled="true" name="cdrCbuPojo.localCbuId" id="cbuid" /></td>

			<td><s:text name="garuda.unitreport.label.collectiondate" /></td>
			<td><s:date name="cdrCbuPojo.cbuCollectionDate"
				id="colletionDate" format="MMM dd, yyyy" /> <s:textfield
				name="colletionDate" 
				disabled="true" id="cordCollectionDate" value="%{colletionDate}" /></td>
		</tr>

	</table>
	</div>
	</div>
	</div>
	</div>
	
	<div class="portlet" id="productinfodiv">
			<div id="productinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			<span class="ui-icon ui-icon-minusthick"></span><span class="ui-icon ui-icon-close" ></span>
			<s:text name="garuda.unitreport.label.prodinfo.subheading" /></div>
			<div class="portlet-content">
			<div id="productinfo1">
		<table border="0" cellpadding="0" cellspacing="0" width="1000px">
			<tbody>
				<tr>
			      <td width="150px"><s:text name="garuda.unitreport.label.cellviability" /></td>
					<td width="100px"><s:textfield name="cbuUnitReporttempPojo.cordViabPostProcess" size="10" onKeyPress="return isNumberKey(event)" /></td>
					<td width="10px">%</td>
			      <td width="100px"><s:text name="garuda.unitreport.label.method" /></td>
			      <td width="150px">
			      
			       <s:select name="cbuUnitReporttempPojo.cordViabMethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_VIAB_METHOD]" 
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />
			      
			      </td>
			      <td  width="100px"><s:text name="garuda.unitreport.label.testdate" /></td>
			      <td width="100px"><s:textfield name="cbuUnitReporttempPojo.strcordViabPostTestDate" class="datepic" id="datepicker1" readonly="true" onkeydown="cancelBack();" /></td>
			      
			      <td width="100px"><s:text name="garuda.unitreport.label.or" /></td>
			      <td width="100px"><s:text name="garuda.unitreport.label.notdone" /></td>
			      
			      
			      <td width="90px">
			      <s:checkbox name="cordViabPostStatus" id="viabStatusCheckbox" onclick="checkBx2()" />
					<s:hidden name="cbuUnitReporttempPojo.cordViabPostStatus" id="isViab"  />
			      </td>
			      </tr>
			</tbody>
		</table>

	<div id="viab" style="display: none">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">

		 <tr>
			      <td width="40%"><s:text name="garuda.unitreport.question.priortest" /></td>
			      <td width="10%"><s:radio name="cbuUnitReporttempPojo.cordViabPostQues1" list="#{'1':'Yes','0':'No'}" /></td>
			      <td width="40%"><s:text name="garuda.unitreport.question2.priortest2" /></td>
			      <td width="10%"><s:radio name="cbuUnitReporttempPojo.cordViabPostQues2" list="#{'1':'Yes','0':'No'}" /></td>
			       
			    </tr>
	</table>
	</div>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
			      <td width="150px"><s:text name="garuda.unitreport.label.cfupostprocessingcount"  /></td>
			      <td width="100px"><s:textfield name="cbuUnitReporttempPojo.cordCfuPostProcessCount" size="10" onKeyPress="return isNumberKey(event)" /></td>
			      <td width="10px"></td>
			      <td width="100px"><s:text name="garuda.unitreport.label.method" /></td>
			      <td width="150px">
			      
			     <s:select name="cbuUnitReporttempPojo.cordCfuPostMethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_POST_METHOD]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />
			      </td>
			      <td width="100px"><s:text name="garuda.unitreport.label.testdate" /></td>
			      <td width="100px"><s:textfield name="cbuUnitReporttempPojo.strcordCfuPostTestDate"  id="datepicker2" readonly="true" onkeydown="cancelBack();" /></td>
			     
			     <td width="100px"><s:text name="garuda.unitreport.label.or" /></td>
			      <td width="100px"><s:text name="garuda.unitreport.label.notdone" /></td>
			     
			      <td width="90px"><s:checkbox name="cordCfuPostStatus" id="cfuStatusCheckbox" onclick="checkBx1()" />
					  <s:hidden name="cbuUnitReporttempPojo.cordCfuPostStatus" id="isCfu" />
					  </td>
				</tr>
		</tbody>
	</table>

	<div id="viab2" style="display: none">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">

		<tr>	  
			      <td width="40%"><s:text name="garuda.unitreport.question.priortest" /></td>
			    
			      <td width="10%"><s:radio name="cbuUnitReporttempPojo.cordCfuPostQues1" list="#{'1':'Yes','0':'No'}" /></td>
			      
			      <td width="40%"><s:text name="garuda.unitreport.question2.priortest2" /></td>
			       <td width="10%"><s:radio name="cbuUnitReporttempPojo.cordCfuPostQues2" list="#{'1':'Yes','0':'No'}" /></td>
			        
			    </tr>
	</table>
	</div>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>    
			       <td width="150px"><s:text name="garuda.unitreport.label.cbupostprocessingnrbc" /></td>
			      <td width="100px"><s:textfield name="cbuUnitReporttempPojo.cordCbuPostProcessCount" size="10" onKeyPress="return isNumberKey(event)" /></td>
			      <td width="10px"></td>
			      
			      <td width="100px"><s:text name="garuda.unitreport.label.testdate" /></td>
			      <td width="150px"><s:textfield name="cbuUnitReporttempPojo.strcordCbuPostTestDate" id="datepicker3" readonly="true" onkeydown="cancelBack();"  /></td>      
			      <td width="100px"><s:text name="garuda.unitreport.label.or" /></td>
			      <td width="100px"><s:text name="garuda.unitreport.label.notdone" /></td>
			      <td width="100px">
			      <s:checkbox name="cordCbuPostStatus" id="cbuStatusCheckbox" onclick="checkBx3()" />
					<s:hidden name="cbuUnitReporttempPojo.cordCbuPostStatus" id="isCbu"  />
			      
			      </td>
			      <td width="100px"></td>
			      <td width="90px"></td>
			      </tr>
		</tbody>
	</table>

	<div id="viab3" style="display: none">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		 <tr>
			      <td  width="40%"><s:text name="garuda.unitreport.question.priortest" /></td>
			      
			      <td width="10%"><s:radio name="cbuUnitReporttempPojo.cordCbuPostQues1" list="#{'1':'Yes','0':'No'}" /></td>
			      
			       <td  width="40%"><s:text name="garuda.unitreport.question2.priortest2" /></td>
			       <td  width="10%"><s:radio name="cbuUnitReporttempPojo.cordCbuPostQues2" list="#{'1':'Yes','0':'No'}" /></td>
			    </tr>
	</table>
	</div>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			        <td width="150px"><s:text name="garuda.unitreport.label.bacterialculture"/></td>
				  <td width="100px"><s:select name="cbuUnitReporttempPojo.cordBacterialCul" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />
			</td><td width="10px"></td>
			      <td width="100px"><s:text name="garuda.unitreport.label.fungalculture"/></td>
				  <td width="150px"><s:select name="cbuUnitReporttempPojo.cordFungalCul" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />
			</td>
			      <td width="100px"></td>
			      <td width="100px"></td>
			      <td width="100px"></td>
			      <td width="100px"></td>
			      <td width="90px"></td>
			    </tr>
		<tr>
			<td ><s:text name="garuda.unitreport.label.hemoglobinopathyscr"/></td>
								<td><s:select name="cbuUnitReporttempPojo.cordHemogScreen" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />

		</tr>


	</table>
	</div>
	</div>
	</div>
	</div>
	<div class="portlet" id="hlainfodiv">
	<div id="hlainfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
	<span class="ui-icon ui-icon-minusthick"></span><span class="ui-icon ui-icon-close" ></span>
	<br/>
		</div>						 
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
					<div class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
				
				<s:text name="garuda.unitreport.label.hlainfo.subheading" /></div>
			<div class="portlet-content">
			<div id="hlainfo1">
					
					
				

					<table width="100%" border="0" cellpadding="0" cellspacing="0">
					    	<tr>
								<td width="75%" ><s:text name="garuda.unitreport.question.hlatype" /></td>
							    <td align="right" width="25%"><s:radio  name="cordHlaExttempPojo.cordHlaTwice"  list="#{'1':'Yes','0':'No'}" />
								</td>
							</tr>
							
					        	
					        <tr ><td width="75%"><s:text name="garuda.unitreport.question.hlasamples"/></td>
								 <td align="right" width="25%"><s:radio name="cordHlaExttempPojo.cordIndHlaSamples" list="#{'1':'Yes','0':'No'}" />
								</td>
							</tr>
							<tr>
								<tr><td width="75%"><s:text name="garuda.unitreport.question.hlalab"/></td>
								 <td align="right" width="25%"><s:radio name="cordHlaExttempPojo.cordHlaSecondLab" list="#{'1':'Yes','0':'No'}" />
								</td>
							</tr>
							<tr>
								<td width="75%"><s:text name="garuda.unitreport.question.hlacontguoussegment"/></td>
								 <td align="right" width="25%"><s:radio name="cordHlaExttempPojo.cordHlaContiSeg" list="#{'1':'Yes','0':'No'}" />
								</td>
								
								<tr>
								<td width="75%"><s:text name="garuda.unitreport.question.hlarelease"/></td>
								 <td align="right" width="25%"><s:radio name="cordHlaExttempPojo.cordHlaContiSegBefRel" list="#{'1':'Yes','0':'No'}" />
								</td>
								
								
							</tr>		
							<tr>
								<td width="75%"><s:text name="garuda.unitreport.question.hlabeforerelease"/></td>
								 <td align="right" width="25%"><s:radio name="cordHlaExttempPojo.cordHlaIndBefRel" list="#{'1':'Yes','0':'No'}" />
								</td>
								
								</tr>
								
								<tr>
								<td width="75%"><s:text name="garuda.unitreport.question.unitrequestedshipment"/></td>
								<td align="right" width="25%"><s:select name="cordHlaExttempPojo.CordHlaContiSegNum" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NUM_OF_SEGMENTS]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />
						</td>
					</tr>
					</table>
					
					<br/><br/><br/><br/><br/><br/>
					</div>
					</div>
					</div>
					
					</td>
				<td>
				
			<div class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
				
				<s:text name="garuda.unitreport.label.processinginfo.subheading" /></div>
			<div class="portlet-content">
			<div id="processinginfo1">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">


				<tr>
								<td><s:text name="garuda.unitreport.label.processingmethod" /></td>
							    <td><s:select name="cbuUnitReporttempPojo.cordProcMethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_METHOD]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" /></td></tr>
						<tr>
							
					        	<td><s:text name="garuda.unitreport.label.typeofbagused"/></td>
								<td><s:select name="cbuUnitReporttempPojo.cordTypeBag" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TYPE_OF_BAG]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" /></td>
								
							</tr>
						
						<tr>
							
					        	<td><s:text name="garuda.unitreport.label.productmodification"/></td>
								<td>
								<s:select name="cbuUnitReporttempPojo.cordProdModification" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PRODUCT_MODIFICATION]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />
								</td>
								
							</tr>
						
						<tr><td></td>
						<td></td>
						
						</tr>
						<tr><td><s:text name="garuda.unitreport.label.plsexplain" /></td>
						<td><textarea rows="3" cols="15" name="cbuUnitReporttempPojo.cordExplainNotes"></textarea> </td></tr>
						
						
						<tr>
						<td><s:text name="garuda.unitreport.label.additionalnotes"/></td>
						<td><textarea rows="4" cols="20" name="cbuUnitReporttempPojo.cordAddNotes"></textarea></td>
						</tr>


			</table>

			</div>
			</div>
			</div></td></tr></table></div>	</div>
	
	

<div class="portlet" id="attachmentdiv">
	<div id="attachment" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
	<span class="ui-icon ui-icon-minusthick"></span><span class="ui-icon ui-icon-close" ></span>
	<br/>
		</div>
					
			<div id="attachment1">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						 <tr style="display: none;"><td></td>
			          <td>
			            <a href="#" onclick="javascript:newfileupload();"><s:text name="garuda.unitreport.label.attachtxt"/></a>
			          </td>
					  <td></td>
			       </tr>
			       <tr>
			          <td>
			          <s:text name="garuda.unitreport.label.completedby"/>
			          </td>
			          <td>			          
			            <s:textfield name="cbuUnitReporttempPojo.cordCompletedBy"  id="completedby" />
			          </td>
			          <td>
			          <s:text name="garuda.unitreport.label.completedstatus"/>
			         </td>
			         <td>
			       			<s:select name="cbuUnitReporttempPojo.cordCompletedStatus" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@COMPLETED_STATUS]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />
					</td>
			          
			       </tr>

					</table>
					</div></div></div>
										
	<table width="100%" cellspacing="0" cellpadding="0" class="tabledisplay">
			      
			       <tr align="center">
			          <td>           
			          </td>
			          <td><button type="button" onclick="javascript:saveCBUUnitReport('<s:property value="cdrCbuPojo.cdrCbuId"/>');" ><s:text name="garuda.unitreport.label.button.submit"/></button> 
			            <%-- <button type="button" ><s:text name="garuda.unitreport.label.button.export"/></button>  --%>
			            <button type="button" onClick=" alert('<s:text name="garuda.cbu.idmTest.alertSubmit"/>');">
			            <s:text name="garuda.unitreport.label.button.cancel"/></button> 
			          </td>
			       </tr>
			    </table>
	<br />
	<br />


</s:form>
</div>
</div>