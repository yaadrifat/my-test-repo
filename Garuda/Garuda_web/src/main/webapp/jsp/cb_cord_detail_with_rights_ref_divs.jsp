<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<s:if test="hasViewPermission(#request.staicPanl)==true">
	<div id="loadStaticPanelDiv">
		<jsp:include page="cb_load_static_panel.jsp"></jsp:include>
	</div>
</s:if>
<s:if test="hasViewPermission(#request.updateCBUInfowidget)==true">			
	<div id="loadCbuCharacteristicDiv" >
		<!-- Load cb_viewcbuinfo.jsp and edit button of edit cbu characteristics on click of this widget and notes data -->	
		<jsp:include page="cb_load_cbu_characteristic.jsp"></jsp:include>								   
	</div>
</s:if>
<s:if test="hasViewPermission(#request.updateIDwidget)==true">
	<div id="loadCordIdsDataDiv" >
		<!-- Load cbu ids data and notes on click of this widget -->
		<jsp:include page="cb_load_eligibility_final_cbu_review.jsp"></jsp:include>	
	</div>						
</s:if>
<s:if test="hasViewPermission(#request.updateLabSmmry)==true">
	<div id="loadLabSummaryDiv" >
		<!-- Load Lab summary data on click of this widget and notes data -->
		<jsp:include page="cb_labSummeryForCordInfo.jsp"></jsp:include>	
	</div>
</s:if>
<s:if test="hasViewPermission(#request.updateProInfoWidget)==true">
	<div id="loadProcessingProcedureDataDiv" >
	    <!-- Load processign procedures data and documents and notes on click of this widget -->
	    <jsp:include page="cb_load_cord_proc_info.jsp"></jsp:include>	
	</div>
</s:if>				
<s:if test="hasViewPermission(#request.updateElgblty)==true">
	<div id="loadEligibilityDiv" >
		<!-- Load document upload data for eligibility and final cbu review eligibility questions and dumn data with product tag  on click of this widget -->
		<jsp:include page="cb_load_eligibility_final_cbu_review.jsp"></jsp:include>	
	</div>
</s:if>		
<s:if test="hasViewPermission(#request.updateCbuHLA)==true">
	<div id="loadHlaDataDiv" >
		<!-- Load hla data and documents and notes on click of this widget -->
		<jsp:include page="cb_load_hla_data.jsp"></jsp:include>	
	</div>
</s:if>
<s:if test="hasViewPermission(#request.updateInfectDisease)==true">	
	<div id="loadIdmDataDiv" >
			<jsp:include page="cb_load_idm_data.jsp"></jsp:include>	
	</div>
</s:if>
<s:if test="hasViewPermission(#request.updateHlthHis)==true">
	 <div id="loadFormsDataDiv" >
		<!-- Load fmhq and mrq and health screening notes and health screening documnets data on click of this widget -->
		<jsp:include page="cb_load_health_history_screen.jsp"></jsp:include>	
	</div>
</s:if>
<div id="loadOthersDataDiv" >
		<!-- Load others notes data on click of this widget -->
		<jsp:include page="cb_load_other_data.jsp"></jsp:include>	
</div>
<s:if test="hasViewPermission(#request.updateClnclNotes)==true">
	<div id="loadWholeNotesDiv" >
		<!-- Load all notes (include cb_note_list.jsp) click of this widget -->
	    <jsp:include page="cb_load_note_list.jsp"></jsp:include>	
	</div>
</s:if>