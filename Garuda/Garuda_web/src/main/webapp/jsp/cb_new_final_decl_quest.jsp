<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", 0); 
String val = request.getParameter("readonly");
request.setAttribute("readonly",val);
%>
<script>
$j(document).ready(function(){
	if('<s:property value="esignFlag"/>'== '<s:text name="review"/>' && ('<s:property value="cdrCbuPojo.fkCordCbuStatus"/>'=='<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH)"/>' || '<s:property value="cdrCbuPojo.fkCordCbuStatus"/>'=='<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)"/>')){
		$j("#finalDeclNewReadOnly textarea").attr('disabled', 'disabled');
	}
	if(('<s:property value="esignFlag"/>'=='reviewCriView') && ('<s:property value="#request.readonly"/>'=='true')){
		
		$j("#finalDeclNewReadOnly textarea").attr('disabled','disabled');
	}
});
$j("#finalDeclNewReadOnly input[type=radio]").click(function(){
	if(('<s:property value="esignFlag"/>'=='reviewCriEdit') && ('<s:property value="cdrCbuPojo.site.isReqElgQuest()"/>'=='true')){
	if($j(this).closest('div').hasClass("addQuestMandatory")){
		$j(this).closest('div').css("border", "none");
		}
	}
});
$j("#finalDeclNewReadOnly textarea").keyup(function(){
	if(('<s:property value="esignFlag"/>'=='reviewCriEdit') && ('<s:property value="cdrCbuPojo.site.isReqElgQuest()"/>'=='true')){
	if($j.trim($j(this).val())!="" && $j.trim($j(this).val())!=null){
	if($j(this).hasClass("addQuestMandatory")){
		$j(this).css("border", "none");
		}
	}
	else{
		$j(this).css("border", "1px solid red");
	}
	}
});
 function resetValue(arr){
	 for(var j=0; j<arr.length;j++){
		 $j("#"+arr[j]+ " input[type=radio]").each(function(){
			 $j(this).attr('checked',false);
		 });
		 $j("#"+arr[j]).find("textarea").each(function(){
			 $j(this).val("");           
		 });		
	 }
 }

 function limitText(txtField, maxLength) {
		if (txtField.value.length > maxLength) {
			txtField.value = txtField.value.substring(0, maxLength)
		} 
	}


 function showQuestionWithDetail(val,detailid,quesid){
	  var arr = new Array();
	  arr[0] = detailid;
	  arr[1] = quesid;
	 
	  if(val=='false'){
        $j("#"+detailid).show();
        $j("#"+quesid).show();
       
    }else if(val=='true'){
   	 $j("#"+detailid).hide();
        $j("#"+quesid).hide();
       
        resetValue(arr);
    }
 }

  function showYesQuestionWithDetail(val,detailid,quesid,highLightFlg){
	  var arr = new Array();
	  arr[0] = detailid;
	  arr[1] = quesid;
	  if(val=='true'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
     }else if(val=='false'){
    	 $j("#"+detailid).hide();
         $j("#"+quesid).hide();
         resetValue(arr);
     }
  }

  function showMultipeQuestionWithDetail(val,detailid,quesid,quesid1,quesid2,quesid3,highLightFlg){
	  var arr = new Array();
	  arr[0] = detailid;
	  arr[1] = quesid;
	  arr[2] = quesid1;
	  arr[3] = quesid2;
	  arr[4] = quesid3;
	  if(val=='false'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
         $j("#"+quesid1).show();
         $j("#"+quesid2).show();
         $j("#"+quesid3).show();
     }else if(val=='true'){
         $j("#"+detailid).hide();
         $j("#"+quesid).hide();
         $j("#"+quesid1).hide();
         $j("#"+quesid2).hide();
         $j("#"+quesid3).hide();
         resetValue(arr);
     }
  }

  function showMultipeYesQuestionWithDetail(val,detailid,quesid,quesid1,quesid3,quesid4,quesid5,quesid6,quesid7,quesid8,quesid9,highLightFlg){
	  var arr = new Array();
	  arr[0] = detailid;
	  arr[1] = quesid;
	  arr[2] = quesid1;
	  arr[3] = quesid3;
	  arr[4] = quesid4;
	  arr[5] = quesid5;
	  arr[6] = quesid6;
	  arr[7] = quesid7;
	  arr[8] = quesid8;
	  arr[9] = quesid9;	  
	  if(val=='true'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
         $j("#"+quesid1).show();
         $j("#"+quesid3).show();
         $j("#"+quesid4).show();
         $j("#"+quesid5).show();
         $j("#"+quesid6).show();
         $j("#"+quesid7).show();
         $j("#"+quesid8).show();
         $j("#"+quesid9).show();
     }else if(val=='false'){
         $j("#"+detailid).hide();
         $j("#"+quesid).hide();
         $j("#"+quesid1).hide();
         $j("#"+quesid3).hide();
         $j("#"+quesid4).hide();
         $j("#"+quesid5).hide();
         $j("#"+quesid6).hide();
         $j("#"+quesid7).hide();
         $j("#"+quesid8).hide();
         $j("#"+quesid9).hide();
         resetValue(arr); 
     }
  }

  function showQuestion(val,quesid,detailId,highLightFlg){
	  var arr = new Array();
	  arr[1] = quesid;	
	  arr[2] = detailId;
	  if(val=='false'){
	         $j("#"+quesid).show();
	  }else if(val=='true'){
	         $j("#"+quesid).hide();
	         $j("#"+detailId).hide();
	         resetValue(arr);
	  }
  }
  
  function showUserMessage(val){
	  if(val=='true'){
		  var str = "<s:text name="garuda.cbu.quest.exitAlert"/>";
		  jConfirm(str, '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
			    if(r==true){
			    	//submitform('cdrentryform'); 
			    	//window.location='cdrcbuView?cdrCbuPojo.cordID='+$j("#ncordId").val()+'&orderId='+$j("#norderId").val()+'&orderType='+$j("#norderType").val()+'&pkcordId='+$j("#npkcordId").val();
			    	submitpost('cdrcbuView',{'cdrCbuPojo.cordID':$j("#ncordId").val(),'orderId':$j("#norderId").val(),'orderType':$j("#norderType").val(),'pkcordId':$j("#npkcordId").val()});
			    	
				}			    
		 });
	  }
  }

  function showMultipleQuestion(val,quesid,quesid1,highLightFlg){
	  var arr = new Array();
	  arr[1] = quesid;
	  arr[2] = quesid1;	  
	  if(val=='false'){
		     $j("#"+quesid).show();
	         $j("#"+quesid1).show();
	  }else if(val=='false'){
	         $j("#"+quesid).hide();
	         $j("#"+quesid1).hide();
	         resetValue(arr);
	  }
  }

  function showDeatil(val,detailid,highLightFlg){
	  var arr = new Array();
	  arr[0] = detailid;	  
	  if(val=='false'){
	         $j("#"+detailid).show();
	  }else if(val=='true'){
	         $j("#"+detailid).hide();
	         resetValue(arr);
	  }
  }

  function showYesDeatil(val,detailid,highLightFlg){
	  var arr = new Array();
	  arr[0] = detailid;	  
	  if(val=='true'){
	         $j("#"+detailid).show();
	  }else if(val=='false'){
	         $j("#"+detailid).hide();
	         resetValue(arr);
	  }
  }

  function showNoDeatil(val,detailid,highLightFlg){
	  var arr = new Array();
	  arr[0] = detailid;	  
	  if(val=='false'){
	         $j("#"+detailid).show();
	  }else if(val=='true'){
	         $j("#"+detailid).hide();
	         resetValue(arr);
	  }
  }
  
  function showhelpmsg(id){
		msg=$j('#'+id+'_msg').html();
		overlib(msg,CAPTION,'Help');
		$j('#overDiv').css('z-index','99999');
	}

	function showMessage(val){
		if(val=='false'){
			if(($j("#srFkCordCbuEligible").val()==$j("#srIncompleteReasonId").val()) && checkEligDates('collDate','defDatVal')){
				alert("<s:text name="garuda.cbu.quest.incompleteAlert"/>");
			}
		}
	}
	
</script>
<input type="hidden" name="srFkCordCbuEligible" id="srFkCordCbuEligible" value="<s:property value="cdrCbuPojo.fkCordCbuEligible"></s:property>" />
<input type="hidden" name="srIncompleteReasonId" id="srIncompleteReasonId" value="<s:property value="cdrCbuPojo.incompleteReasonId"></s:property>" />
<s:date name="cdrCbuPojo.specimen.specCollDate" id="dtPick" format="yyyy-MM-dd" />	
<s:hidden name="collDate" id="collDate" value="%{dtPick}"></s:hidden>
<s:hidden name="defDatVal" id="defDatVal" value="2005-05-25"></s:hidden>
<input type="hidden" name="ncordId" value="<s:property value="cdrCbuPojo.cordID"/>" id="ncordId" />
<input type="hidden" name="npkcordId" value="<s:property value="cdrCbuPojo.cordID"/>" id="npkcordId" />
<input type="hidden" name="norderId" value="<s:property value="#request.orderId"/>" id="norderId"/>
<input type="hidden" name="norderType" value="<s:property value="#request.orderType"/>"  id="norderType" />
<input type = "hidden" name="" value=""/>
<div id="finalDeclNewReadOnly">
   <table width="100%" cellpadding="0" cellspacing="0" class="displaycdr">
       <tr>
           <td valign="top" width="35%">
               <s:text name="garuda.questionnaire.level.mrq_screnning" /> 
           </td>
           <td style="border: 1px solid #EDECEB;" width="65%" bgcolor="#7DCFD5">
              <table width="100%" cellpadding="0" cellspacing="0" >
                <tr valign="top">
                    <td width="75%"><s:text name="garuda.questionnaire.level.newmrqques1" /></td>
                    <td nowrap="nowrap" width="25%" style="padding:0px"><div id='mrqNewQues1div' class="addQuestMandatory"><s:radio name="declNewPojo.mrqNewQues1" id="mrqques1" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.mrqNewQues1}','elgFcrChanges');showQuestionWithDetail(this.value,'mrqQues1atr','mrqNewQues1adddetail')"></s:radio>
                    <br/><label class="error" for="declNewPojo.mrqNewQues1" id="declNewPojo.mrqNewQues1_error" style="display:none" generated="true"></label>
                    </div></td>
                </tr>
	                <tr id="mrqNewQues1adddetail" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 <td width="25%">
                        <textarea class="addQuestMandatory" rows="5" cols="20" name="declNewPojo.mrqNewQues1AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.mrqNewQues1AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.mrqNewQues1AddDetail}"/></textarea>
                   </td>
                </tr>
                <tr id="mrqQues1atr" style="display: none;" valign="top">
                    <td width="95%"  style="padding-left: 45px;"><s:text name="garuda.questionnaire.level.newmrqques2" />
                    	
                    </td>
                    <td width="5%" style="padding:0px">
                    	<div class="addQuestMandatory">
                    	<s:radio name="declNewPojo.mrqNewQues1a" id="mrqQues1a" list="#{'true':'CBB confirms - not able to obtain this additional information</br>','false':'CBB will be able to complete the missing information'}" onclick="isChangeDone(this,'%{declNewPojo.mrqNewQues1a}','elgFcrChanges');" ></s:radio>
                    	<br/><label class="error" for="declNewPojo.mrqNewQues1a" id="declNewPojo.mrqNewQues1a_error" style="display:none" generated="true"></label>
                    	</div>
                    </td>                    
                </tr>

                <tr valign="top">
                   	<td width="75%"><s:text name="garuda.questionnaire.level.newmrqques3" />
                  		
                   	<td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory">
                   		<s:radio name="declNewPojo.mrqNewQues2" id="mrqQues2" list="#{'true':'Yes','false':'NO'}" onclick="showNoDeatil(this.value,'mrqQues2adddetail');isChangeDone(this,'%{declNewPojo.mrqNewQues2}','elgFcrChanges');"></s:radio></div>
                   		<br/><label class="error" for="declNewPojo.mrqNewQues2" id="declNewPojo.mrqNewQues2_error" style="display:none" generated="true"></label>
                   </td>
                </tr>
                <tr id="mrqQues2adddetail" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 <td  width="25%">
                        <textarea rows="5" cols="20" class="addQuestMandatory" name="declNewPojo.mrqNewQues2AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.mrqNewQues2AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.mrqNewQues2AddDetail}"/></textarea>
                   </td>
                </tr>
                <tr valign="top">
                   	<td   width="75%"><s:text name="garuda.questionnaire.level.newmrqques4" />
                  		
                   	<td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory">
                   		<s:radio name="declNewPojo.mrqNewQues3" id="mrqQues2" list="#{'true':'Yes','false':'NO'}" onclick="showYesDeatil(this.value,'mrqQues3adddetail');isChangeDone(this,'%{declNewPojo.mrqNewQues3}','elgFcrChanges');"></s:radio></div>
                   		<br/><label class="error" for="declNewPojo.mrqNewQues3" id="declNewPojo.mrqNewQues3_error" style="display:none" generated="true"></label>
                   </td>
                </tr>
                <tr id="mrqQues3adddetail" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 <td width="25%">
                        <textarea class="addQuestMandatory" rows="5" cols="20" name="declNewPojo.mrqNewQues3AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.mrqNewQues3AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.mrqNewQues3AddDetail}"/></textarea>
                   </td>
                </tr>
              </table>
       		</td>
       </tr>
       <tr>
       		<td colspan="2" height="5%"></td>
       </tr>
       <tr>
           <td valign="top" width="35%">
               <s:text name="garuda.questionnaire.level.physNewfind" /> 
           </td>
           <td style="border: 1px solid #EDECEB;" bgcolor="#7DCFD5" width="65%">
              <table width="100%" cellpadding="0" cellspacing="0" >
                <tr valign="top">
                   <td width="75%"><s:text name="garuda.questionnaire.level.newphysfind0" /></td>
                   <td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio name="declNewPojo.phyNewFindQues4" id="phyNewFindQues4" list="#{'true':'Yes','false':'NO'}" onclick="showQuestionWithDetail(this.value,'phyNewFindQues4atr','phyNewFindQues1adddetail');isChangeDone(this,'%{declNewPojo.phyNewFindQues4}','elgFcrChanges');">
                   </s:radio><br/><label class="error" for="declNewPojo.phyNewFindQues4" id="declNewPojo.phyNewFindQues4_error" style="display:none" generated="true"></label></div></td>
                </tr>
                 <tr id="phyNewFindQues1adddetail" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 <td width="25%" >
                        <textarea class="addQuestMandatory" rows="5" cols="20" name="declNewPojo.phyNewFindQues4AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.phyNewFindQues4AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.phyNewFindQues4AddDetail}"/></textarea>
                   </td>
                </tr>
                <tr id="phyNewFindQues4atr" style="display: none;" valign="top">
                    <td width="95%" style="padding-left: 45px;"><s:text name="garuda.questionnaire.level.newphysfind1" />
                   		
                    </td>
                    <td width="5%" style="padding:0px">
                    	<div class="addQuestMandatory">
                    	<s:radio name="declNewPojo.phyNewFindQues4A" id="phynewFindQues4a" list="#{'true':'CBB confirms - not able to obtain this additional information</br>','false':'CBB will be able to complete the missing information'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues4A}','elgFcrChanges');" ></s:radio>
                    	<br/><label class="error" for="declNewPojo.phyNewFindQues4A" id="declNewPojo.phyNewFindQues4A_error" style="display:none" generated="true"></label>
                    	</div>
                    </td>                    
                </tr>
                
           
               
                <tr valign="top">
                   	<td width="75%"><s:text name="garuda.questionnaire.level.newphysfind2" />
                   		
                   	</td>
                   	<td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory">
                   		<s:radio name="declNewPojo.phyNewFindQues5" id="phyNewFindQues5" list="#{'true':'Yes','false':'NO'}" onclick="showYesDeatil(this.value,'phyNewFindQues5AddDetail');isChangeDone(this,'%{declNewPojo.phyNewFindQues5AddDetail}','elgFcrChanges');"></s:radio>
                   		<br/><label class="error" for="declNewPojo.phyNewFindQues5" id="declNewPojo.phyNewFindQues5_error" style="display:none" generated="true"></label>
                   		</div>
                   	</td>
                </tr>
                <tr id="phyNewFindQues5AddDetail" style="display: none;">
                	<td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                	<td  width="25%">
                    	<textarea class="addQuestMandatory" rows="5" cols="20" name="declNewPojo.phyNewFindQues5AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.phyNewFindQues5AddDetail}" escapeJavaScript="true"/>','elgFcrChanges');" ><s:property value="%{declNewPojo.phyNewFindQues5AddDetail}"/></textarea>
                   </td>
                </tr>
                <tr valign="top">
                   	<td width="75%"><s:text name="garuda.questionnaire.level.newphysfind3" />
                   		
                   	</td>
                   	<td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory">
                   		<s:radio name="declNewPojo.phyNewFindQues6" id="phyNewFindQues6" list="#{'true':'Yes','false':'NO'}" 
                   				onclick="showMultipeYesQuestionWithDetail(this.value,'phyNewFindQues6AddDetail','phyNewFindQues6atr','phyNewFindQues6btr','phyNewFindQues6ctr','phyNewFindQues6dtr','phyNewFindQues6etr','phyNewFindQues6ftr','phyNewFindQues6gtr','phyNewFindQues6htr','phyNewFindQues6itr');isChangeDone(this,'%{declNewPojo.phyNewFindQues6}','elgFcrChanges');"></s:radio>
                   				<br/><label class="error" for="declNewPojo.phyNewFindQues6" id="declNewPojo.phyNewFindQues6_error" style="display:none" generated="true"></label>
                   				</div>
                   </td>
                </tr>

                <tr id="phyNewFindQues6atr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind4" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio name="declNewPojo.phyNewFindQues6a" id="phyNewFindQues6a" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6a}','elgFcrChanges');"></s:radio>
                    <br/><label class="error" for="declNewPojo.phyNewFindQues6a" id="declNewPojo.phyNewFindQues6a_error" style="display:none" generated="true"></label>
                    </div></td>                    
                </tr>
                <tr id="phyNewFindQues6btr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind5" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio name="declNewPojo.phyNewFindQues6b" id="phyNewFindQues6b" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6b}','elgFcrChanges');" ></s:radio>
                    <br/><label class="error" for="declNewPojo.phyNewFindQues6b" id="declNewPojo.phyNewFindQues6b_error" style="display:none" generated="true"></label>
                    </div></td>                    
                </tr>
                <tr id="phyNewFindQues6ctr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind6" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio name="declNewPojo.phyNewFindQues6c" id="phyNewFindQues6c" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6c}','elgFcrChanges');" ></s:radio>
                    <br/><label class="error" for="declNewPojo.phyNewFindQues6c" id="declNewPojo.phyNewFindQues6c_error" style="display:none" generated="true"></label>
                    </div></td>                    
                </tr>
                <tr id="phyNewFindQues6dtr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind7" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio name="declNewPojo.phyNewFindQues6d" id="phyNewFindQues6d" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6d}','elgFcrChanges');" ></s:radio>
                    <br/><label class="error" for="declNewPojo.phyNewFindQues6d" id="declNewPojo.phyNewFindQues6d_error" style="display:none" generated="true"></label>
                    </div></td>                    
                </tr>
                <tr id="phyNewFindQues6etr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind8" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio name="declNewPojo.phyNewFindQues6e" id="phyNewFindQues6e" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6e}','elgFcrChanges');" ></s:radio>
                    <br/><label class="error" for="declNewPojo.phyNewFindQues6e" id="declNewPojo.phyNewFindQues6e_error" style="display:none" generated="true"></label>
                    </div></td>                    
                </tr>
                <tr id="phyNewFindQues6ftr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind9" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio name="declNewPojo.phyNewFindQues6f" id="phyNewFindQues6f" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6f}','elgFcrChanges');" ></s:radio>
                    <br/><label class="error" for="declNewPojo.phyNewFindQues6f" id="declNewPojo.phyNewFindQues6f_error" style="display:none" generated="true"></label>
                    </div></td>                    
                </tr>
                <tr id="phyNewFindQues6gtr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind10" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio name="declNewPojo.phyNewFindQues6g" id="phyNewFindQues6g" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6g}','elgFcrChanges');" ></s:radio>
                    <br/><label class="error" for="declNewPojo.phyNewFindQues6g" id="declNewPojo.phyNewFindQues6g_error" style="display:none" generated="true"></label>
                    </div></td>                    
                </tr>
                <tr id="phyNewFindQues6htr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind11" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio name="declNewPojo.phyNewFindQues6h" id="phyNewFindQues6h" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6h}','elgFcrChanges');" ></s:radio>
                    <br/><label class="error" for="declNewPojo.phyNewFindQues6h" id="declNewPojo.phyNewFindQues6h_error" style="display:none" generated="true"></label>
                    </div></td>                    
                </tr>
                <tr id="phyNewFindQues6itr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.newphysfind12" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio name="declNewPojo.phyNewFindQues6i" id="phyNewFindQues6i" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declNewPojo.phyNewFindQues6i}','elgFcrChanges');" ></s:radio>
                    <br/><label class="error" for="declNewPojo.phyNewFindQues6i" id="declNewPojo.phyNewFindQues6i_error" style="display:none" generated="true"></label>
                    </div></td>                    
                </tr>
                 <tr id="phyNewFindQues6AddDetail" style="display: none;" valign="top" >
                	<td  width="75%" >
                		<s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                	<td width="25%" style="padding:0px">
                    	<textarea class="addQuestMandatory" rows="5" cols="20" name="declNewPojo.phyNewFindQues6AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.phyNewFindQues6AddDetail}" escapeJavaScript="true"/>','elgFcrChanges');" ><s:property value="%{declNewPojo.phyNewFindQues6AddDetail}"/></textarea>
                   	</td>
                </tr>
              </table>
           </td>
       </tr>
       <tr>
       		<td colspan="2" height="5%"></td>
       </tr>
       <tr>
           <td valign="top" width="35%">
               <s:text name="garuda.questionnaire.level.idm" />
           </td>
           <td style="border: 1px solid #EDECEB;" bgcolor="#7DCFD5" width="65%">
               <table width="100%" cellpadding="0" cellspacing="0" >
                   <tr valign="top">
	                   <td width="75%"><s:text name="garuda.questionnaire.level.newidm1" /></td>
	                   <td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio name="declNewPojo.idmNewQues7" id="idmNewQues7" list="#{'true':'Yes','false':'NO'}" 
	                   onclick="showQuestionWithDetail(this.value,'idmNewQues7atr','idmNewQues7AddDetail');isChangeDone(this,'%{declNewPojo.idmNewQues7}','elgFcrChanges');"></s:radio>
	                   <br/><label class="error" for="declNewPojo.idmNewQues7" id="declNewPojo.idmNewQues7_error" style="display:none" generated="true"></label>
	                   </div></td>
                   </tr>
                   <tr id="idmNewQues7AddDetail" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 <td width="25%">
                        <textarea  class="addQuestMandatory" rows="5" cols="20" name="declNewPojo.idmNewQues7AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.idmNewQues7AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.idmNewQues7AddDetail}"/></textarea>
                   </td>
                </tr>
                   <tr id="idmNewQues7atr" style="display: none;" valign="top">
    	                <td width="95%" style="padding-left: 45px;"><s:text name="garuda.questionnaire.level.newidm2" />
	    	                
            	        </td>
                	    <td   width="5%" style="padding:0px">
                    		<div class="addQuestMandatory">
                    		<s:radio name="declNewPojo.idmNewQues7a" id="idmNewQues7a" list="#{'true':'CBB confirms - not able to complete the missing tests</br>','false':'CBB will be able to complete the missing tests'}" onclick="isChangeDone(this,'%{declNewPojo.idmNewQues7a}','elgFcrChanges');" ></s:radio>
                    		<br/><label class="error" for="declNewPojo.idmNewQues7a" id="declNewPojo.idmNewQues7a_error" style="display:none" generated="true"></label>
                    		</div>
                    	</td>                    
                   </tr>
          
                   
                   
                   <tr valign="top">
	                   <td width="75%"><s:text name="garuda.questionnaire.level.newidm3" /></td>
	                   <td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory"><s:radio name="declNewPojo.idmNewQues8" id="idmNewQues8" list="#{'true':'Yes','false':'NO'}" 
	                   onclick="showQuestionWithDetail(this.value,'idmNewQues8atr','idmNewQues8AddDetail');isChangeDone(this,'%{declNewPojo.idmNewQues8}','elgFcrChanges');"></s:radio>
	                   <br/><label class="error" for="declNewPojo.idmNewQues8" id="declNewPojo.idmNewQues8_error" style="display:none" generated="true"></label>
	                   </div></td>
                   </tr>
                      <tr id="idmNewQues8AddDetail" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 <td width="25%">
                        <textarea class="addQuestMandatory" rows="5" cols="20" name="declNewPojo.idmNewQues8AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.idmNewQues8AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.idmNewQues8AddDetail}"/></textarea>
                   </td>
                </tr>
                   <tr id="idmNewQues8atr" style="display: none;" valign="top">
    	                 <td width="95%" style="padding-left: 45px;"><s:text name="garuda.questionnaire.level.newidm2" />
	    	                
            	        </td>
                	   <td   width="5%" style="padding:0px">
                    		<div class="addQuestMandatory">
                    		<s:radio name="declNewPojo.idmNewQues8a" id="idmNewQues8a" list="#{'true':'CBB confirms - not able to complete the missing tests</br>','false':'CBB will be able to complete the missing tests'}" onclick="isChangeDone(this,'%{declNewPojo.idmNewQues8a}','elgFcrChanges');" ></s:radio>
                    		<br/><label class="error" for="declNewPojo.idmNewQues8a" id="declNewPojo.idmNewQues8a_error" style="display:none" generated="true"></label>
                    		</div>
                    	</td>                    
                   </tr>
                 
                
                <tr valign="top">
                   	<td width="75%"><s:text name="garuda.questionnaire.level.newidm5" />
                  		
                   	<td nowrap="nowrap" width="25%" style="padding:0px"><div class="addQuestMandatory">
                   		<s:radio name="declNewPojo.idmNewQues9" id="idmNewQues9" list="#{'true':'Yes','false':'NO'}" onclick="showYesDeatil(this.value,'idmNewQues9AddDetail');isChangeDone(this,'%{declNewPojo.idmNewQues9}','elgFcrChanges');"></s:radio>
                   		<br/><label class="error" for="declNewPojo.idmNewQues9" id="declNewPojo.idmNewQues9_error" style="display:none" generated="true"></label>
                   		</div>
                   </td>
                </tr>
                <tr id="idmNewQues9AddDetail" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/><span style="color: red;">*</span></td>
                 <td width="25%">
                        <textarea class="addQuestMandatory" rows="5" cols="20" name="declNewPojo.idmNewQues9AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declNewPojo.idmNewQues9AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declNewPojo.idmNewQues9AddDetail}"/></textarea>
                   </td>
                </tr>
               
                 
               </table>
           </td>
       </tr>
       <tr>
       		<td colspan="2" height="5%"></td>
       </tr>
    </table>
</div>
<div id="helpmsg" style="display: none;">
	<div id=mrq1a_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incompleteInfo"/></td></tr>
   		</table>
	</div>
	<div id=mrq1b_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=mrq2_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.ineligibleMatRisk"/></td></tr>
   		</table>
	</div>
	<div id=physfind3a_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incompReleventMedRecord"/></td></tr>
   		</table>
	</div>
	<div id=physfind3b_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=physfind3c_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incompPhysicalFindings"/></td></tr>
   		</table>
	</div>
	<div id=physfind3d_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=physfind4_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.ineligibleReleventMedRecord"/></td></tr>
   		</table>
	</div>
	<div id=physfind5_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.imeligiblePhysicalFindings"/></td></tr>
   		</table>
	</div>
	<div id=idm6a_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incomInfectDiseaseMarker"/></td></tr>
   		</table>
	</div>
	<div id=idm6b_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=idm7a_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incomInfectDiseaseMarker"/></td></tr>
   		</table>
	</div>
	<div id=idm7b_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=idm8_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.ineligibleInfectDiseaseMarker"/></td></tr>
   		</table>
	</div>
</div>
<s:if test="(#request.readonly=='true') && (esignFlag=='reviewCriView')">
	  <script>
		  disableFormFlds("finalDeclNewReadOnly");	 
	  </script>
</s:if>
<s:if test="declNewPojo.mrqNewQues1!=null && declNewPojo.mrqNewQues1==false">
    <script>
          showQuestion('false','mrqQues1atr','','1');
    </script>
</s:if>
<s:if test="declNewPojo.mrqNewQues1!=null && declNewPojo.mrqNewQues1==false">
    <script>
          showNoDeatil('false','mrqNewQues1adddetail','1');
    </script>
</s:if>
<s:if test="declNewPojo.mrqNewQues2!=null && declNewPojo.mrqNewQues2==false">
    <script>
         showNoDeatil('false','mrqQues2adddetail','1');
    </script>
</s:if>
<s:if test="declNewPojo.mrqNewQues3!=null && declNewPojo.mrqNewQues3==true">
    <script>
         showYesDeatil('true','mrqQues3adddetail','1');
    </script>
</s:if>
<s:if test="declNewPojo.phyNewFindQues4!=null && declNewPojo.phyNewFindQues4==false">
    <script>
        showQuestion('false','phyNewFindQues4atr','','1');
    </script>
</s:if>
<s:if test="declNewPojo.phyNewFindQues4!=null && declNewPojo.phyNewFindQues4==false">
    <script>
        showNoDeatil('false','phyNewFindQues1adddetail','1');
    </script>
</s:if>
<s:if test="declNewPojo.phyNewFindQues5!=null && declNewPojo.phyNewFindQues5==true">
    <script>
        showYesDeatil('true','phyNewFindQues5AddDetail','1');
    </script>
</s:if>

<s:if test="declNewPojo.phyNewFindQues6!=null && declNewPojo.phyNewFindQues6==true">

    <script>
        showMultipeYesQuestionWithDetail('true','phyNewFindQues6AddDetail','phyNewFindQues6atr','phyNewFindQues6btr','phyNewFindQues6ctr','phyNewFindQues6dtr','phyNewFindQues6etr','phyNewFindQues6ftr','phyNewFindQues6gtr','phyNewFindQues6htr','phyNewFindQues6itr','1');
    </script>
</s:if>
<s:if test="declNewPojo.idmNewQues7!=null && declNewPojo.idmNewQues7==false">
    <script>
        showQuestion('false','idmNewQues7atr','','1');
    </script>
</s:if>
<s:if test="declNewPojo.idmNewQues7!=null && declNewPojo.idmNewQues7==false">
    <script>
       showQuestion('false','idmNewQues7AddDetail','','1');
    </script>
</s:if>

<s:if test="declNewPojo.idmNewQues8!=null && declNewPojo.idmNewQues8==false">
    <script>
      showQuestion('false','idmNewQues8atr','','1');
    </script>
</s:if>
<s:if test="declNewPojo.idmNewQues8!=null && declNewPojo.idmNewQues8==false">
    <script>
      showQuestion('false','idmNewQues8AddDetail','','1');
    </script>
</s:if>


<s:if test="declNewPojo.idmNewQues9!=null && declNewPojo.idmNewQues9==true">
    <script>    
     showYesDeatil('true','idmNewQues9AddDetail','1');
    </script>
</s:if>
  <s:if test="hasEditPermission(#request.eligbleSumQues)==false">
  <script>
     $j("#finalDeclNewReadOnly").each(function(){
    	  $j(this).find('input:radio').attr('disabled', true);
     });  
  </script> 
  </s:if>
  
  <s:if test="hasViewPermission(#request.eligbleSumQues)==false">
  <script>
      //$j('#finalDeclNewReadOnly').css('display','none');
     $j('#finalDeclNewReadOnly').css('display','none');
     
  </script> 
  </s:if>
  <script>
  if((('<s:property value="esignFlag"/>'=='reviewCriEdit') || ('<s:property value="esignFlag"/>'=='reviewCriView')) && ('<s:property value="cdrCbuPojo.site.isReqElgQuest()"/>'=='true')){
		 
		var val,index;
		$j('.addQuestMandatory').each(function(){
			
			if(($j(this).get(0).tagName).toLowerCase()=='textarea'){
				if('<s:property value="esignFlag"/>'=='reviewCriEdit'){
					if($j.trim($j(this).val())=="" && $j(this).is(':visible')){
						$j(this).css("border", "1px solid red");
					}
				}
				else if('<s:property value="esignFlag"/>'=='reviewCriView'){
					if($j.trim($j(this).val())==""){
						$j(this).css("border", "1px solid red");
					}
				}
			}
			else{
				val=0,index=0;
				$j(this).find('input[type="radio"]').each(function(i,el){
					
					 index=index+1;
					 
					 var isChecked = $j(el).attr("checked");
					   	if(isChecked){
						  	val=val+1;
					  	}
					  	if(index==2){
					  		return false;
					  	}
				});
				if('<s:property value="esignFlag"/>'=='reviewCriEdit'){
					if(val==0 && index==2 && $j(this).is(':visible')){
						$j(this).css("border", "1px solid red");
					}
				}
				else if('<s:property value="esignFlag"/>'=='reviewCriView'){
					if(val==0 && index==2){
						$j(this).css("border", "1px solid red");
					}
				}
			}
		});
		
	}
  </script>
