<%@page import="com.velos.ordercomponent.business.util.Utilities"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@ page import="com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<jsp:include page="cb_track_session_logging.jsp" />
<%String contextpath = request.getContextPath()+"/jsp/"; %>
<s:set name="finalCordId" value="cdrCbuPojo.cordID"></s:set>
<script type="text/javascript">
/*var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/> <img class=\"asIsImage\" src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
var processingSaveDialog = document.createElement('div');
$j(processingSaveDialog).attr("id",'progressMsg');
processingSaveDialog.innerHTML=progressMsg;
$j(processingSaveDialog).dialog({autoOpen: false,
	resizable: false, width:400, height:90,closeOnEscape: false,
	modal: true}).siblings('.ui-dialog-titlebar').remove();		
*/
<%
HttpSession tSession = request.getSession(true); 
GrpRightsJB grpRights =null;
if (tSession.getAttribute("LocalGRights")!=null){
 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
}else
{
 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
}

int conLicStus=0;
int conElgble=0;
int conCBBsign=0;
int conFinalRev=0;
if(grpRights.getFtrRightsByValue("CB_FINALICSTAT")!=null && !grpRights.getFtrRightsByValue("CB_FINALICSTAT").equals(""))
{	conLicStus = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FINALICSTAT"));}
else
	conLicStus = 4;
if(grpRights.getFtrRightsByValue("CB_FINALELIGBLE")!=null && !grpRights.getFtrRightsByValue("CB_FINALELIGBLE").equals(""))
{	conElgble = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FINALELIGBLE"));}
else
	conElgble = 4;
if(grpRights.getFtrRightsByValue("CB_FREVFRMCBB")!=null && !grpRights.getFtrRightsByValue("CB_FREVFRMCBB").equals(""))
{	conCBBsign = Integer.parseInt(grpRights.getFtrRightsByValue("CB_FREVFRMCBB"));}
else
	conCBBsign = 4;
if(grpRights.getFtrRightsByValue("CB_CONFREVIEW")!=null && !grpRights.getFtrRightsByValue("CB_CONFREVIEW").equals(""))
{	conFinalRev = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CONFREVIEW"));}
else
	conFinalRev = 4;
request.setAttribute("conLicStus",conLicStus);
request.setAttribute("conElgble",conElgble);
request.setAttribute("conCBBsign",conCBBsign);
request.setAttribute("conFinalRev",conFinalRev);
%>
function showPrevious(val){
	//$j(processingSaveDialog).dialog("open");
	var orderId=$j.trim($j("#orderId").val());
	if(typeof(orderId)=='undefined'){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")==0){
		$j("#orderId").val("");
		orderId="";
		}
	var orderType=$j.trim($j("#orderType").val());
	if(typeof(orderType)=='undefined'){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")==0){
		$j("#orderType").val("");
		orderType="";
		}
	
	if(orderId=="" && orderType==""){
		window.opener.location='loadClinicalDataFromPF?cdrCbuPojo.cordID='+val+'&pkcordId='+val;
		window.close();
		}
	else{
		window.opener.location='getUserRightForGrpForOrder?cdrCbuPojo.cordID='+val+'&orderId='+orderId+'&orderType='+orderType+'&pkcordId='+val;
		window.close();
		}
	//$j(processingSaveDialog).dialog("destroy");
	//$j(processingSaveDialog).remove();
}

function RefreshPrevious(val){
	//$j(processingSaveDialog).dialog("open");
	var orderId=$j.trim($j("#orderId").val());
	if(typeof(orderId)=='undefined'){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")==0){
		$j("#orderId").val("");
		orderId="";
		}
	var orderType=$j.trim($j("#orderType").val());
	if(typeof(orderType)=='undefined'){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")==0){
		$j("#orderType").val("");
		orderType="";
		}
	
	if(orderId=="" && orderType==""){
		window.opener.location='loadClinicalDataFromPF?cdrCbuPojo.cordID='+val+'&pkcordId='+val;
		}
	else{
		window.opener.location='getUserRightForGrpForOrder?cdrCbuPojo.cordID='+val+'&orderId='+orderId+'&orderType='+orderType+'&pkcordId='+val;
		}
	//$j(processingSaveDialog).dialog("destroy");
	//$j(processingSaveDialog).remove();
}


function checkval(){
	/*  var flag=0;
    $j('#test input[type=checkbox]').each(function(index,el) {
        var s=$j(el).attr('checked');
        if(s==false){
                flag=1;
        }
        if(flag!=1){
        $j('#finalReviewChbdiv input[type=checkbox]').each(function(index,el) {
                    $j(el).attr('disabled',false);
                    $j('#finalReviewChba').attr('disabled',false);
			
                });
            }
        else if(flag==1){
                $j('#finalReviewChbdiv input[type=checkbox]').each(function(index,el) {
		    $j(el).attr('checked',false);
                    $j(el).attr('disabled',true);
		//$j('input[name=finalReviewDesc]').attr('disabled', true);
		$j('input[name=fkCordAcceptance]').attr('disabled', true);
                });
            }
        checkFinalCheck();
    });
	*/
	 checkFinalCheck();
} 

function checkFinalCheck(){
	if ($j('input[name="eligiblestatus"]').attr('checked') == true && $j('input[name="licensurestatus"]').attr('checked') == true) {
		//$j('input[name=finalReviewDesc]').attr('disabled', false);
		$j('input[name=acceptance]').attr('disabled', false);
 		$j('#esign').attr('disabled', false);
	}
	else{
		//$j('input[name=finalReviewDesc]').attr('disabled','true');
		$j('input[name=acceptance]').attr('disabled',true);
 		$j('#esign').attr('disabled', true);
 		$j('#esign').val("");
	}
	if($j('#finalReviewConfirmStatus').val()=="Y"){
		$j('input[name=acceptance]').attr('disabled',true);
		}	
}
function refreshDiv2(url,divname,formId){
	showprogressMgs();
	//alert("url ::"+url +"-- divName::"+divname+"  -- formid::"+formId);
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
            var $response=$j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
            	constructTable();
				checkedOrnotchecked();
            }
        },
        error: function (request, status, error) {
        	closeprogressMsg();
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
	closeprogressMsg();
}	
</script>
<script type="text/javascript">


$j(document).ready(function(){    
	$j('#pkcordId').val($j('#cordId').val());
	hideDiv();
	$j('input[name=licensurestatus]').attr({'disabled': true});
	$j('input[name=eligiblestatus]').attr({'disabled': true});
	$j('input[name=cbbSignstatus]').attr({'disabled': true});	
});

function checkedOrnotchecked(){
	
	if($j('#licensureConfirmFlag').val() == 1){
		$j('input[name=licensurestatus]').attr({'disabled': true,'checked':true});
		//$j(".licensureTD a").removeAttr('onclick');
	}

	if($j('#eligibleConfirmFlag').val() == 1){
		$j('input[name=eligiblestatus]').attr({'disabled': true,'checked':true});
		//$j(".eligibleTD a").removeAttr('onclick');
	}
	
	if($j('#confirmCBBSignatureFlag').val() == 1){
		$j('input[name=cbbSignstatus]').attr({'disabled': true,'checked':true});
		//$j(".licensureTD a").removeAttr('onclick');
	}
	if($j('#licensureConfirmFlag').val() == 1 && $j('#eligibleConfirmFlag').val() == 1 && $j('#confirmCBBSignatureFlag').val() == 1){
		$j('input[name=acceptance]').attr({'disabled': false});
	}
	else if($j('#licensureConfirmFlag').val() == 1 && $j('#eligibleConfirmFlag').val() == 1){
		$j('input[name=acceptance]').attr({'disabled': false});
	}
	if($j('#finalReviewConfirmStatus').val() == 'Y'){
		//$j('input[name=finalReviewChb]').attr({'disabled': true,'checked':true});
		$j('input[name=acceptance]').attr({'disabled': true});
		//$j("#finalReviewChbdiv a").removeAttr('onclick');
	}
	
	checkval();
}
function showrightdiv(url,divname){
	/*var progressMsge="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/> <img class=\"asIsImage\" src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	var processingSaveDialoge = document.createElement('div');
	$j(processingSaveDialoge).attr("id",'progressMsge');
	processingSaveDialoge.innerHTML=progressMsge;
	$j(processingSaveDialoge).dialog({autoOpen: false,
		resizable: false, width:400, height:90,closeOnEscape: false,
		modal: true}).siblings('.ui-dialog-titlebar').remove();	
	$j(processingSaveDialoge).dialog("open");*/
	showprogressMgs();
	hideDiv();
	//alert(url);
	refreshDiv2(url,divname,'finalCBUReview');	
	$j('#'+divname).css('display','block');
	//loadPageByGetRequset(url,divname);
	/*$j(processingSaveDialoge).dialog("destroy");
	$j(processingSaveDialoge).remove();*/
	closeprogressMsg();
}

function showrightdiv1(url,divname){
	showprogressMgs();
	hideDiv();
	//refreshDiv2(url,divname,'finalCBUReview');
	loadPageByGetRequset(url,divname);	
	$j('#'+divname).css('display','block');
	closeprogressMsg();
}
function hideDivliceli(){
	$j('#eligibleDiv').css('display','none');
	$j('#licensureDiv').css('display','none');
}
function constructTable(){
	$j("#elistshistry").dataTable();
	$j("#licensehistory").dataTable();
}
function hideDiv(){
	$j("#esign").val("");
	$j("#finalsubmit").attr("disabled", true); 
	$j("#finalpass").css('display','none'); 
	$j("#finalminimum").css('display','none');
	$j("#finalinvalid").css('display','none'); 
	$j('#eligibleDiv').css('display','none');
	$j('#licensureDiv').css('display','none');
	$j('#flaggedItemsDiv').css('display','none');
	$j('#nonmemberPdfDiv').css('display','none');
	//if($j('#acceptable').val()!=$j('input[name=acceptance]:radio:checked').val()){
	//$j('#acceptableDesc').show();
		//}
	checkedOrnotchecked();
	//confirmCBUStatus('<s:property value="cbuFinalReviewPojo.reviewCordAcceptance"/>');
/*	if('<s:property value="cbuFinalReviewPojo.eligibleConfirmFlag"/>' == '1' && '<s:property value="cbuFinalReviewPojo.licensureConfirmFlag"/>' == '1' && '<s:property value="cbuFinalReviewPojo.finalReviewConfirmStatus"/>' == 'Y'){
		$j('.displayTrue').attr("disabled",false);
	}
	else{
		$j('.displayTrue').attr("disabled",true);
		}
*/	
}

function submitFinalCbuReview(formname){
	$j('#first_finalreivew').removeAttr('disabled');
	//alert("Do you want to submit "+formname+" Form");
	//$j('#licenceUpdate').val("True");
	//alert("-------------::"+$j('#fkCordAcceptance').val());
	var usr = '<s:property value="cdrCbuPojo.site.isCDRUser()"/>';
	//alert(usr +"|||---"+$j('#fcrAccpt').val()+"|||---"+$j('#CBBSign').val());
	if(usr=='false' && $j('#CBBSign').val()!=1){
		//alert("CBBSign");
		$j('#cbberror').html('<s:text name="garuda.cbu.review.confirmCBBsign"/> <s:text name="garuda.cbu.review.taskNeedTocomple"/>');
		$j('#eligerror').html("");
		$j('#licerror').html("");
		$j('#accepterror').html("");
	}
	else if($j('#eliStat').val()!=1){
		//alert("eliStat");
		$j('#eligerror').html('<s:text name="garuda.finalreview.label.confirmfinaleligibledetermin"/> <s:text name="garuda.cbu.review.taskNeedTocomple"/>');
		$j('#cbberror').html("");
		$j('#licerror').html("");
		$j('#accepterror').html("");
	}
	else if($j('#licstat').val()!=1){
		//alert("licstat");
		$j('#licerror').html('<s:text name="garuda.finalreview.label.confirmlicenstatus"/> <s:text name="garuda.cbu.review.taskNeedTocomple"/>');
		$j('#cbberror').html("");
		$j('#eligerror').html("");
		$j('#accepterror').html("");
	}
	else if($j('#fcrAccpt').val()!=1){
		//alert("acceptance");
		$j('#accepterror').html('<s:text name="garuda.cbu.cbb.cordacceptance"/>');
		$j('#cbberror').html("");
		$j('#eligerror').html("");
		$j('#licerror').html("");
	}
	else {
		//submitform(formname);
		$j('#accepterror').html("");
		$j('#cbberror').html("");
		$j('#eligerror').html("");
		$j('#licerror').html("");
		var x="submitFinalReviewAcceptable?licenceUpdate=True";
		 document.getElementById(formname).action=x;
		 setTimeout(function(){
		 	document.getElementById(formname).submit();
		 },0);
		 setTimeout(function(){
			RefreshPrevious('<s:property value="cdrCbuPojo.cordID"/>');
		 },2500);
	}
}
function activeFinalReview(){
		hideDivliceli();
		/*if ($j('#finalReviewChb').is(':disabled') == false) {
			$j('input[name=finalReviewChb]').attr('checked', true);
        		}*/ 
		//$j('input[name=finalReviewChb]').attr('checked', true);
		}


function refreshDiv1(url,divname,formId){
	//$('.progress-indicator').css( 'display', 'block' );
	//alert("url ::"+url +"-- divName::"+divname+"  -- formid::"+formId);
	
	if($j("#"+formId).valid()){
		
		showprogressMgs();
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
            var $response=$j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
                if(divname=='first_finalreivew'){
                	$j("#"+divname).html(result);
                 }
                else{
                	var newDivContent=$j(result).find("#"+divname).html();
            		$j("#"+divname).html(newDivContent);
                 }
		checkval();
		hideDiv();
            	//$j('#pkcordId').val($j('#cordId').val());
            }
        },
        error: function (request, status, error) {
        	closeprogressMsg();
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
	closeprogressMsg();
  }
}	

function confirmCBUStatus(val){
	$j('#fcrAccpt').val("1");
	$j('#accepterror').html("");
	var value= $j.trim(val);
	$j("#fkCordAcceptance").val(value);
	$j("#acceptance").attr("disabled", "disabled");
	var usr = '<s:property value="cdrCbuPojo.site.isCDRUser()"/>';
	if(usr=='false' && $j('#CBBSign').val()!=1){
		//alert("CBBSign");
		$j('#cbberror').html('<s:text name="garuda.cbu.review.confirmCBBsign"/> <s:text name="garuda.cbu.review.taskNeedTocomple"/>');
		$j('#eligerror').html("");
		$j('#licerror').html("");
		$j('#accepterror').html("");
	}
	else if($j('#notAcceptable').val()==value){
	$j('#acceptableDesc').hide();	
	$j('#finalCBUSubmit').hide();
	var cbustatustype=$j("#cbustatustype").val();
	var orderId=$j.trim($j("#orderId").val());
	if(typeof(orderId)=='undefined'){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")==0){
		$j("#orderId").val("");
		orderId="";
		}
	var orderType=$j.trim($j("#orderType").val());
	if(typeof(orderType)=='undefined'){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")==0){
		$j("#orderType").val("");
		orderType="";
		}
	var applyResolFlag = 'N';
	var cordId = $j('#cordId').val();
	var isCordAvail=null;
	var resetFun = "reset";
	var upiob = $j('#numberOnCbuBag').val();
	var upiob_data = '';
	if(upiob!="" && upiob != null && upiob != "undefined"){
		upiob_data = ', <s:text name="garuda.cbuentry.label.idbag"/>:'+upiob;
	}
	var url='updateCbuStatus?licenceUpdate=False&cbustatusin=IS_LOCAL_REG&resetFun='+resetFun+'&cdrCbuPojo.cordID='+cordId+'&orderId='+orderId+'&orderType='+orderType+'&applyResol='+applyResolFlag+'&iscordavailfornmdp='+isCordAvail+'&cordAcceptId='+value;
	showModalForCBU('<s:text name="garuda.cbustatus.label.updatecbustatus"/>:<s:property value="cdrCbuPojo.registryId"/>'+upiob_data,url,'450','800');	
	}else{
		//$j('#acceptableDesc').show();
		$j('#finalCBUSubmit').show();
		}	
}

function forClearOrders(){

	var orderId=$j.trim($j("#orderId").val());
	if(typeof(orderId)=='undefined'){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")==0){
		$j("#orderId").val("");
		orderId="";
		}
	var orderType=$j.trim($j("#orderType").val());
	if(typeof(orderType)=='undefined'){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")==0){
		$j("#orderType").val("");
		orderType="";
		}
	
}
function showNonmemberPdf(){
	//$j(processingSaveDialoge).dialog("open");
	hideDiv();
	$j('#downloaddoc1').html("");
	$j('#nonmemberPdfDiv').css('display','block');
	var el = $j('#nonmemberPdfDiv').find("#0");
	el.click();
	$j(el).triggerHandler("click");
	//$j(processingSaveDialoge).dialog("destroy");
	//$j(processingSaveDialoge).remove();
}
function showModalForCBU(title,url,height,width)
{
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/> <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#modelPopup1").html(progressMsg);
		$j("#modelPopup1").dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#modelPopup1").dialog("destroy");
				    }
				   }
				  ); 
		$j("#modelPopup1").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data:$j("#finalCBUReview").serialize(),
        success: function (result, status, error){
	       	$j("#modelPopup1").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
}
$j(function(){
	$j(".subportlet").width($j(".subportlet").width());
});

/***
 * Widget : Document selection screen
 * Author : Anurag Upadhyay
 * purpose: This method check the "Document selection screen" popup open criteria 
 ***/

var ModalDialog={
		    autoOpen: true,
			resizable: true,
			closeText: '',
			title:'',
		    closeOnEscape: false ,
			modal: true,width : 750, height : 650,
			close: function() {     					
     		jQuery("#modelPopup1").dialog("destroy");
		    }
      }

var selectdocstatus=false; 
var SelectDocPopup={
              eligideterm:'<s:property value="CdrCbuPojo.fkCordCbuEligible"/>',
              
              old_fristquest:'<s:property value="declPojo.idmQues6"/>',
              old_secondquest:'<s:property value="declPojo.idmQues7"/>',
              old_thirdquest:'<s:property value="declPojo.idmQues8"/>',
             
              new_fristquest:'<s:property value="declNewPojo.idmNewQues7"/>',
              new_secondquest:'<s:property value="declNewPojo.idmNewQues8"/>',
              new_thirdquest:'<s:property value="declNewPojo.idmNewQues9"/>',
              
              checkEligiDeterm:function(){
               return ((this.eligideterm==='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON)"/>' || this.eligideterm==='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INCOMPLETE_REASON)"/>' )?true:false);
              },
              checkQuestStatus:function(){
                  if(<s:property value="isTasksRequired(#finalCordId)"/>){
                	  return ((this.new_fristquest==='false' || this.new_secondquest==='false' || this.new_thirdquest==='true')?true:false);
                  }
                  else{
                      return ((this.old_fristquest==='false' || this.old_secondquest==='false' || this.old_thirdquest==='true')?true:false);
                  }
              },
              checkSystemUserStatus:function(){
                  return ($j("#non_system_cord").val()===true || $j("#non_system_cord").val()==='true')?true:false;
              },
              checkPopupStatus:function(form){   
                  if(!(FinRevAssoDocsFlag.statusFlag) && !(this.checkSystemUserStatus()) && this.checkEligiDeterm() && this.checkQuestStatus()){
                	  OpenModalPopup.modalPopup();
                  }
                  else{ 
                        submitFinalCbuReview(form);
                      }
              }
}

var OpenModalPopup={
		modalPopup:function(){			
			fn_showAssociatedDocs('Select IDM Attachment <s:text name="garuda.message.modal.headerregid"/>: <s:property value="cdrCbuPojo.registryId" />','associatedDocuments?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cbuFinalReviewPojo.pkCordFinalReview=<s:property value="cbuFinalReviewPojo.pkCordFinalReview"/>','550','800','modelPopup2','','','','y');
    }
}

var FinRevAssoDocsFlag={		  
             statusFlag:false
}
function fn_showAssociatedDocs(title,url,hight,width,divname,max,min,sidex,sidey){
	var upiob = $j('#numberOnCbuBag').val();
	var upiob_data = '';
	if(upiob!="" && upiob != null && upiob != "undefined"){
		upiob_data = ', <s:text name="garuda.cbuentry.label.idbag"/>:'+upiob;
	}
	title=title+upiob_data;
	showModalscontent(title,url,hight,width,divname,max,min,sidex,sidey);
}
</script>

<style>
#nonmemberPdfDiv table{
width:auto;
}
.tabnormal{
	background-color:#8db4dc;
}
.tabactive{
	background-color:#6b9ed1;
}
#nonmemberPdfDiv td {
    padding: 5px;
    border: 1px solid #1A598B;
    -moz-border-radius: 4px 4px 0 0;
    cursor: pointer;
}
#nonmemberPdfDiv td:hover {
    background-color: #6b9ed1;
}
#nonmemberPdfDiv fieldset {
    -moz-border-radius: 0 4px 4px 4px;
    border: 1px solid #1A598B;
    margin: 0;
}

.finalreviewmodel{
width:100%;
height:100%;
margin:0;

}/*
#finalreview{
width:100%;
}
.finalresult  {
	width: 59%;
	margin-left:41%;
	position:absolute;
}*/
.finalreview td{
padding:2px;
}

.finalreview ul li {
	list-style: none;
	padding: 5px;
	margin-left: 50px;
}
.finalreview li{
background:none
}
.finalreview form label{
height:auto;
}
#ccFCR{
margin-left:55px;
}
</style>                     
<div id="first_finalreivew">
<div class="portlet">
			<div class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all toggleWidgetClass">
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">				
             <s:text name="garuda.cbufinalreview.label.widgetname" /><span class="ui-icon ui-icon-print" onclick="clickheretoprint('first_finalreivew','<s:property value="cdrCbuPojo.registryId"/>')"></span><span class="ui-icon ui-icon-minusthick"></span></div>
<div class="finalreviewmodel">
<div class="portlet-content">
<div class="finalreview" id="finalreview" >
<table align="center">
<tr><td>
</td></tr>
<tr>
<td align="right">
<s:text name="garuda.cbufinalreview.label.regId"/>:
</td>
<td align="left">
<font style="border: 1px solid #EDECEB;background-color:#7DCFD5;size: 1em; "><s:property value="cdrCbuPojo.registryId"/></font>
</td>
<td align="right">
<s:text name="garuda.cbufinalreview.label.idonbag"/>:
</td>
<td align="left">
<font style="border: 1px solid #EDECEB;background-color:#7DCFD5;size: 1em; "><s:property value="cdrCbuPojo.numberOnCbuBag"/></font>
</td>
<td align="right">
<s:text name="garuda.cbufinalreview.label.patientId"/>:
</td>
<td align="left">
<s:if test="patientInfoLst!=null && patientInfoLst.size()>0">
<s:iterator value="patientInfoLst" var="rowVal">
<font style="border: 1px solid #EDECEB;background-color:#7DCFD5;size: 1em; "><s:property value="%{#rowVal[5]}"/></font>
<input type="hidden" id="pat_Id" value=<s:property value="%{#rowVal[5]}"/> >
</s:iterator>
</s:if>
</td>
</tr>
</table>
<table>
<tr>
<td width="50%" height="100%" style="vertical-align: top;">

	<s:form id="finalCBUReview">

	<table width="100%">			
	<tr>
	<td>
		<s:hidden name="cbustatustype" id="cbustatustype"/>
		<s:hidden name="cdrCbuPojo.cordID" id="cordId" />
		<s:hidden name="pkcordId" id="pkcordId" />
		<s:hidden name="orderId" id="orderId"></s:hidden>
		<s:hidden name="orderType" id="orderType"></s:hidden>
		<s:hidden name="cbuFinalReviewPojo.confirmCBBSignatureFlag" id="confirmCBBSignatureFlag"></s:hidden>
		<s:hidden name="cbuFinalReviewPojo.licensureConfirmFlag" id="licensureConfirmFlag"></s:hidden>
		<s:hidden name="cbuFinalReviewPojo.eligibleConfirmFlag" id="eligibleConfirmFlag"></s:hidden>
		<s:hidden name="cbuFinalReviewPojo.finalReviewConfirmStatus" id="finalReviewConfirmStatus"></s:hidden>
		<input type="hidden" name="notAcceptable" id="notAcceptable" value=<s:property value="#request.notAcceptable"/> />
		<s:hidden name="fkCordAcceptance" id="fkCordAcceptance"/>
		<s:hidden id="non_system_cord" value="%{cdrCbuPojo.site.isCDRUser()}"/>		
		<div class="subportlet">
		   <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all togglesubWidgetClass">
				<s:text name="garuda.cbufinalreview.label.flaggedItemsSummarty"></s:text><span class="ui-icon ui-icon-triangle-1-s"></span></div>
		<div id="flaggedItems" class="subportlet-content">			
		<jsp:include page="modal/cb_cbu_flaggedItems.jsp"></jsp:include>
		</div>
		<div id="test" class="subportlet-content">
		<div id="confirmCBBSign" style="float: left;position: relative;width: 100%">
		<table width="100%">
			<tr>
				<td class="confirmCBBSignTD">
				<s:if test="cdrCbuPojo.site.isCDRUser()==false">
					<div style="display: none"><s:if test="cbuFinalReviewPojo.confirmCBBSignatureFlag == 1" ><s:checkbox name="cbbSignstatus" checked="true" onclick="checkval();"></s:checkbox></s:if>
						<s:else><s:checkbox name="cbbSignstatus" onclick="checkval();"></s:checkbox></s:else></div>
						<s:if test="hasViewPermission(#request. conCBBsign)==true && hasEditPermission(#request. conCBBsign)==false ">
						 <a href="#" onclick=""><s:text name="garuda.cbu.review.confirmCBBsign" /></a>
						 </s:if>
						 <s:elseif test="hasEditPermission(#request.conCBBsign)==true">
						 <a href="#" onclick="showNonmemberPdf()"><s:text name="garuda.cbu.review.confirmCBBsign" /></a>
						 </s:elseif>
						<s:if test="cbuFinalReviewPojo.confirmCBBSignatureFlag == 1" ><img src="images/tick_mark.gif"/></s:if>
						<s:if test="cbuFinalReviewPojo.confirmCBBSignatureFlag == 1">
						<table>	
						 <tr><td>
						 		<s:set name="confirmCBBSignName" value="%{cbuFinalReviewPojo.confirmCBBSignCreator}" scope="request"/>
						 			<%
										
						 				String confirmCBBSign="";
										if (request.getAttribute("confirmCBBSignName") != null && !request.getAttribute("confirmCBBSignName").equals("")) {
											confirmCBBSign = request.getAttribute("confirmCBBSignName").toString();
			 							}
										UserJB user = new UserJB();
										user.setUserId(EJBUtil.stringToNum(confirmCBBSign));
										user.getUserDetails();													
									%>	
								<s:text name="garuda.cbufinalreview.label.completedBy"/> <%=user.getUserLastName()+" "+user.getUserFirstName() %> <s:text name="garuda.cbufinalreview.label.on"/>  
								<s:date name="%{cbuFinalReviewPojo.confirmCBBSignCreatedOn}" id="confirmCBBSignDate" format="MMM dd, yyyy" /><s:property value="%{confirmCBBSignDate}"/>
							</td></tr>
						</table>
					</s:if>
					</s:if>
				</td>
			</tr>
			<tr>
				<td>
					<span class="error"><label id="cbberror"></label></span>
				</td>
			</tr>
		</table>
		</div><br/>
		<div id="isEligibleDiv" style="float: left;position: relative;width: 100%">
		<table width="100%">
			<tr>
				<td class="eligibleTD">
					<div style="display: none"><s:if test="cbuFinalReviewPojo.eligibleConfirmFlag == 1" ><s:checkbox name="eligiblestatus" checked="true" onclick="checkval();"></s:checkbox></s:if>
					<s:else><s:checkbox name="eligiblestatus" onclick="checkval();"></s:checkbox></s:else></div>
					 <s:if test="hasViewPermission(#request.conElgble)==true ">
					   <a href="#" id="eligibleLink" onclick="javascript:showrightdiv1('cbuFinalEligibilityStatus?esignFlag=review&status=eligble&licenceUpdate=false&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR_VIEW_ELIGIBLE" />','eligibleDiv');checkval();">
					   <s:text	name="garuda.finalreview.label.confirmfinaleligibledetermin"></s:text>
					    </a>
					  </s:if>
					<s:if test="cbuFinalReviewPojo.eligibleConfirmFlag == 1"><img src="images/tick_mark.gif"/><s:hidden name="eliStat" value="1" id="eliStat"/></s:if><s:else><s:hidden name="eliStat" id="eliStat"/></s:else>
					<s:if test="cbuFinalReviewPojo.eligibleConfirmFlag == 1">
					<table>	
					 <tr><td>
					 		<s:set name="eligibleReviewName" value="%{cbuFinalReviewPojo.eligCreatedBy}" scope="request"/>
					 			<%
					 			String finaldetails="";
								if (request.getAttribute("eligibleReviewName") != null && !request.getAttribute("eligibleReviewName").equals("")) {
									finaldetails = request.getAttribute("eligibleReviewName").toString();
	 							}
									UserJB user = new UserJB();
									user.setUserId(EJBUtil.stringToNum(finaldetails));
									user.getUserDetails();													
								%>	
							<s:text name="garuda.cbufinalreview.label.completedBy"/> <%=user.getUserLastName()+" "+user.getUserFirstName() %> <s:text name="garuda.cbufinalreview.label.on"/>  
							<s:date name="%{cbuFinalReviewPojo.eligCreatedOn}"	id="eligibleReviewDate" format="MMM dd, yyyy" /><s:property value="%{eligibleReviewDate}"/>
						</td></tr>
					</table>
					</s:if>
				</td>
			</tr>
			<tr>
				<td>
					<span class="error"><label id="eligerror"></label></span>
				</td>
			</tr>
		</table>
		</div><br/>
		<div id="isLicenseDiv" style="float: left;position: relative;width: 100%">
		<table width="100%">
			<tr>
				<td class="licensureTD">
					<div style="display: none"><s:if test="cbuFinalReviewPojo.licensureConfirmFlag == 1" ><s:checkbox name="licensurestatus" checked="true" onclick="checkval();"></s:checkbox></s:if>
					<s:else><s:checkbox name="licensurestatus" onclick="checkval();"></s:checkbox></s:else></div>
					 <s:if test="hasViewPermission(#request.conLicStus)==true">	
					 <a href="#" id="licensureLink" onclick="javascript:showrightdiv('submitLicensureFinalReview?esignFlag=review&orderId=<s:property value="orderId"/>&licenceUpdate=false&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR_VIEW_LICENSURE" />','licensureDiv');checkval();"> 
					 <s:text name="garuda.finalreview.label.confirmlicenstatus" /></a>
					</s:if>
					<s:if test="cbuFinalReviewPojo.licensureConfirmFlag == 1" ><img src="images/tick_mark.gif"/><s:hidden name="licstat" value="1" id="licstat" /></s:if><s:else><s:hidden name="licstat" id="licstat"/></s:else>
					<s:if test="cbuFinalReviewPojo.licensureConfirmFlag == 1" >
					<table>	
					 <tr><td>
							<s:set name="licensureReviewName" value="%{cbuFinalReviewPojo.licCreatedBy}" scope="request"/>
					 			<%
						 			String finaldetails="";
									if (request.getAttribute("licensureReviewName") != null && !request.getAttribute("licensureReviewName").equals("")) {
										finaldetails = request.getAttribute("licensureReviewName").toString();
		 							}
									UserJB user = new UserJB();
									user.setUserId(EJBUtil.stringToNum(finaldetails));
									user.getUserDetails();													
								%>	
							<s:text name="garuda.cbufinalreview.label.completedBy"/> <%=user.getUserLastName()+" "+user.getUserFirstName() %> <s:text name="garuda.cbufinalreview.label.on"/>  
							<s:date name="%{cbuFinalReviewPojo.licCreatedOn}" id="licensureReviewDate" format="MMM dd, yyyy" /><s:property value="%{licensureReviewDate}"/>
						</td></tr>
					</table>
					</s:if>
				</td>
			</tr>
			<tr>
				<td>
					<span class="error"><label id="licerror"></label></span>
				</td>
			</tr>
		</table>
		</div>	
		<table width="100%">
			<!--
			<tr>
				<td id="finalReviewChbdiv">
				<s:if test='cbuFinalReviewPojo.finalReviewConfirmStatus == "Y"'>
				<s:checkbox name="finalReviewChb" checked="true" id="finalReviewChb" disabled="true" onclick="checkFinalCheck();"></s:checkbox>
				</s:if>
				<s:else>
				<s:checkbox name="finalReviewChb" id="finalReviewChb" disabled="true" onclick="checkFinalCheck();"></s:checkbox>
				</s:else>
					<s:text name="garuda.common.lable.finalCbuReview"></s:text>
					<s:if test='cbuFinalReviewPojo.finalReviewConfirmStatus == "Y"'><img src="images/tick_mark.gif"/></s:if>
				</td>
				
			</tr>	
			<tr><td>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I have reviewed source documentation or other information available from the
			review of the cord blood bank-specific relevant medical records on file, along with any identified clinical
			evidence of relevant communicable disease agents and diseases obtained from review of available relevant medical
			records at the time of delivery, which would include relevant physical findings.</td>
			</tr>
			 -->
			 <tr>
			 <td>
			 <s:text name="garuda.cbu.review.license"/>
			 </td>
			 </tr>
			<tr>
			<td>
			<s:text name="garuda.cbu.label.cbuRegId"/> <s:property value="cdrCbuPojo.registryId" /> <s:text name="garuda.common.label.is"/>:
			</td>
			</tr>
			<tr>
			<td>
					<table id="ccFCR">	
						<tr>
							<td colspan="2">
								 <s:if test="hasEditPermission(#request.conFinalRev)==true">
								<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ACCEPTABLE]">
								  <s:radio name="acceptance" list="#{'pkCodeId':'description'}" listKey="pkCodeId" listValue="description" id="acceptance" value="cbuFinalReviewPojo.reviewCordAcceptance" onclick="confirmCBUStatus(this.value);"/><br/>
							</s:iterator>
							     </s:if>
							     	 <s:else>
								<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ACCEPTABLE]">
								   <s:radio  list="#{'pkCodeId':'description'}" listKey="pkCodeId" listValue="description" disabled ="true" value="cbuFinalReviewPojo.reviewCordAcceptance" /><br/>
							</s:iterator>
							     </s:else>
							<s:hidden name="fcrAccpt" id="fcrAccpt"></s:hidden>
							</td>
						</tr>
						<tr>
							<td width="5%">&nbsp;</td>
							<td>
							<s:text name="garuda.cbu.review.unitAcceptable"/>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<span class="error"><label id="accepterror"></label></span>
							</td>
						</tr>
					</table>
			</td>
			</tr>
				<tr >
				<s:if test='cbuFinalReviewPojo.eligibleConfirmFlag == 1 && cbuFinalReviewPojo.licensureConfirmFlag == 1 && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y"' >
				<td align="center">
							<s:set name="finalReviewName" value="%{cbuFinalReviewPojo.finalReviewCreatedBy}" scope="request"/>
					 			<%
						 			String finaldetails="";
									if (request.getAttribute("finalReviewName") != null && !request.getAttribute("finalReviewName").equals("")) {
										finaldetails = request.getAttribute("finalReviewName").toString();
		 							}
									UserJB user = new UserJB();
									user.setUserId(EJBUtil.stringToNum(finaldetails));
									user.getUserDetails();													
								%>	
							<s:text name="garuda.cbufinalreview.label.CBUFinalreviewcompletedBy"></s:text> <%=user.getUserLastName()+" "+user.getUserFirstName()%> <s:text name="garuda.cbufinalreview.label.on"/>  
							<s:date name="%{cbuFinalReviewPojo.finalReviewCreatedOn}" id="finalReviewDate" format="MMM dd, yyyy" /><s:property value="%{finalReviewDate}"/>
				<br/>
				 <table bgcolor="#cccccc" width="100%">
					<tr bgcolor="#cccccc">
						<td valign="baseline" width="100%" align="center">   
						   <form><input type="button" onclick="showPrevious(<s:property value='cdrCbuPojo.cordID'/>);"  value="<s:text name="garuda.pendingorderpage.label.close"/>" /></form>
						</td> 		 
					</tr>
				</table>
				</td>
				</s:if>
				<s:else>
				<td align="center">
				<form>
			<table width="98%" border="0" cellspacing="0" cellpadding="0" id="finalCBUSubmit" bgcolor="#cccccc" style="display: none;">
			 <tr valign="baseline" bgcolor="#cccccc">
			   <td><jsp:include page="./cb_esignature.jsp" flush="true">
	   					<jsp:param value="finalsubmit" name="submitId"/>
						<jsp:param value="finalinvalid" name="invalid" />
						<jsp:param value="finalminimum" name="minimum" />
						<jsp:param value="finalpass" name="pass" />
   					</jsp:include>
   				</td>	
			<td align="center" colspan="2">
			   <input type="button" disabled="disabled" id="finalsubmit" onclick="SelectDocPopup.checkPopupStatus('finalCBUReview')"  value="<s:text name="garuda.common.lable.confirm"/>" />
			   <input type="button"  onclick="showPrevious(<s:property value='cdrCbuPojo.cordID'/>);" value="<s:text name="garuda.openorder.label.button.cancel"/>" />
			   </td>	
			</tr>
			</table>
			</form>
			</td>
			</s:else>
		</tr>
		</table>
		</div>
		</div>
	</td>
	</tr>
	</table>
	</s:form>
</td>
<td width="50%" style="vertical-align: top; border-left: 1px solid black;">
<div id="finalresult" class="finalresult">
<div id="licensureDiv" style="display: none;">
<div id="licensurecontent" 
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><s:text
				name="garuda.cdrcbuview.label.licensure_status"></s:text></div>
<jsp:include page="modal/cb_update-licence-status.jsp"></jsp:include>
</div>
<div id="eligibleDiv" style="display: none;">
<jsp:include page="modal/cb_cbu_final_eligibility-status.jsp"></jsp:include>
</div>
<s:if test="cdrCbuPojo.site.isCDRUser()==false">
<div id="nonmemberPdfDiv" style="display: none">
		<s:if test="%{getAttachments(cdrCbuPojo.cordID, getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CATEGORY, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_ASMNT_CODESUBTYPE),null)!=null && getAttachments(cdrCbuPojo.cordID, getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CATEGORY, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_ASMNT_CODESUBTYPE),null).size()>0}">
		<table border="0" style="overflow: hidden;">
		<tr>
		<s:hidden name="CBBSign" value="1" id="CBBSign"/>
			<s:iterator value="getAttachments(cdrCbuPojo.cordID, getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CATEGORY, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_ASMNT_CODESUBTYPE),null)" var="rowVal" status="row">
			<s:set name="idOfAttachment" value="%{#rowVal[0]}" />
			<td nowrap="nowrap" id="<s:property	value="%{#row.index}"/>" onclick="javascript: return showDocumentForNonMembers('nonmemberPdfDiv','<s:property value="getDCMSAttachId(#idOfAttachment)" />','<s:property	value="%{#row.index}"/>','<s:property value="getDCMSAttachType(#idOfAttachment)"/>','<s:property value="getDCMSAttachFileName(#idOfAttachment)"/>')">
							<s:date name="%{#rowVal[1]}" id="created_On" format="MMM dd, yyyy" />
							<s:property  value="%{created_On}" />
					</td>
			</s:iterator>
			</tr></table>
		</s:if>
		<s:else>
		<s:hidden name="CBBSign" id="CBBSign" value="0"/>
				<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
					<strong><s:text name="garuda.minimumcriteria.label.noattachmentmsg" /> </strong>
				</div>
		</s:else>
<fieldset>
<div id="downloaddoc"  style="padding-top: 5px;">
<s:hidden name="filepath" id="filepath"></s:hidden>
<div id="downloaddoc1"  style="padding-top: 5px;">

</div>
<div id="errorpage" style="display: none;">
	<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
		<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
		<strong><s:text name="garuda.minimumcriteria.label.serverdownmsg" /> </strong></p>
	</div>
</div>

</div>
</fieldset>
</div>
</s:if>
<div id="flaggedItemsDiv" style="display: none;">
<jsp:include page="modal/cb_detailFlaggedItems.jsp"></jsp:include>
</div>
</div></td></tr>
</table></div>
</div></div>
</div></div></div>