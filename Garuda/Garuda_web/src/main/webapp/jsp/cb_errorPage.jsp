<%@ taglib prefix="s"  uri="/struts-tags"%>
<form>
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr >
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.minimumcriteria.label.serverdownmsg" /> </strong></p>
			</div>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td>
			<input type="button" onclick="window.close();" value="<s:text name="garuda.common.close"/>" />
		</td>
	</tr>
</table>
</form>