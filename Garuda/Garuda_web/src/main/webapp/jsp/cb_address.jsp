<%@taglib prefix="s" uri="/struts-tags" %>
<%
String prefix = (String)request.getParameter("prefix");
prefix = prefix + ".";
request.setAttribute("prefix",prefix);

%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", 0); 
%>
<%@page import="com.velos.ordercomponent.business.pojoobjects.CBBPojo"%><script>
  // $j( "input:submit, button", ".column" ).button();
		 
  
</script>

    <tr>
      <td>
      <s:text name="garuda.cbbdefaults.lable.address1">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}address1" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
      <td>
      <s:text name="garuda.cbbdefaults.lable.address2">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}address2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
    </tr>
    <tr>
      <td>
      <s:text name="garuda.cbbdefaults.lable.city">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}city" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
      <td>
      <s:text name="garuda.cbbdefaults.lable.state">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}state" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
    </tr>
    <tr>
      <td>
      <s:text name="garuda.cbbdefaults.lable.zipcode">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}zip" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
      <td>
      <s:text name="garuda.cbbdefaults.lable.country">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}country" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
    </tr>
    <s:if test="#request.hideFields!=Yes">
    <tr>
      <td>
      <s:text name="garuda.cbbdefaults.lable.phone">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}phone" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
      <td>
      <s:text name="garuda.cbbdefaults.lable.fax">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}fax" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>      
    </tr>
    <tr>
      <td>
      <s:text name="garuda.cbbdefaults.lable.pager">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}pager" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
      <td>
      <s:text name="garuda.cbbdefaults.lable.mobile">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}mobile" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>      
    </tr>
    <tr>
      <td>
      <s:text name="garuda.cbbdefaults.lable.email">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}email" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
      <td colspan="2"></td>
    </tr>
  </s:if>
