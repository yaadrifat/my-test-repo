<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction"%>
<%@page import="com.velos.ordercomponent.business.util.Utilities"%>
<%@ page
	import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id="modCtlDao" scope="request"
	class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<%
	HttpSession tSession = request.getSession(true);
	GrpRightsJB grpRights = (GrpRightsJB) tSession
			.getAttribute("GRights");
	String accId = null;
	String logUsr = null;
	if (sessionmaint.isValidSession(tSession)) {
		accId = (String) tSession.getValue("accountId");
		logUsr = (String) tSession.getValue("userId");
		request.setAttribute("username", logUsr);
	}

	
	if (tSession.getAttribute("LocalGRights")!=null){
	 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
	}else
	{
	 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	}
	int assignOrders=0;
	if(grpRights.getFtrRightsByValue("CB_PFPENDINGW")!=null && !grpRights.getFtrRightsByValue("CB_PFPENDINGW").equals(""))
	{	assignOrders = Integer.parseInt(grpRights.getFtrRightsByValue("CB_PFPENDINGW"));}
	else
		assignOrders = 4;
	request.setAttribute("assignOrders",assignOrders);
%>
<script>
function addPrintContent(divId){
	var sourceFile ="";
	var patientStr=$j.trim($j("#patientId").val());
	var regidStr='<s:property value="cdrCbuPojo.registryId"/>';
	$j("#printDiv").html(sourceFile);
	sourceFile +="<center><h3>";
	 if(regidStr!="" && regidStr!="undefined"){
	 sourceFile +="<u>CBU Registry ID:"+regidStr+"</u>";
	}
	 if(patientStr!="" && patientStr!="undefined"){
	 	 sourceFile +="&nbsp;&nbsp;&nbsp;<u>Patient ID:"+patientStr+"</u>";
	 }
	 sourceFile +="</h3></center><br/>"+$j('#'+divId).html();
	 var source = $j("#printDiv").html(sourceFile);
	 $j("#printDiv").hide();
	 clickheretoprint('printDiv','','1');
}
</script>
<s:hidden name="cdrCbuPojo.registryId" id="registryId"/>
<input type="hidden" id="orid" value='<s:property value="orderPojo.pk_OrderId"/>' />
<input type="hidden" id="ortype" value='<s:property value="getCodeListDescById(orderPojo.orderType)"/>' />
<s:if test='getCodeListDescById(orderPojo.orderType)=="CT"'>
<div class="portlet" id="openOrdercbuInfoparent">
			<div id="openOrdercbuInfo"
				class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			<span class="ui-icon ui-icon-print" onclick="getCurrent();addPrintContent('openOrdercbuInfo')"></span>
			<span class="ui-icon ui-icon-plusthick" id="currentReqProSpan" onclick="getCurrent();"></span>
			<!--<span class="ui-icon ui-icon-minusthick"></span> --> <s:text
				name="garuda.ctOrderDetail.portletname.currentReqProgress"></s:text>

			</div>
			<div class="portlet-content" style="display: none;" id="currentRequestProgressMainDiv">
			
			<s:hidden
				name="tskConfirmCbuAvailcompl" id="tskConfirmCbuAvailcompl"></s:hidden>
			<s:hidden name="tskReqClincInfocompl" id="tskReqClincInfocompl"></s:hidden>
			<s:hidden name="tskFinalCbuReviewcompl" id="tskFinalCbuReviewcompl"></s:hidden>
			<s:hidden name="tskShipmentInfocompl" id="tskShipmentInfocompl"></s:hidden>
			<s:hidden name="tskGenPackageSlipcompl" id="tskGenPackageSlipcompl"></s:hidden>
			<s:hidden name="tskAdditiTypingcompl" id="tskAdditiTypingcompl"></s:hidden>
			<s:hidden name="tskShipmentConfirmcompl" id="tskShipmentConfirmcompl"></s:hidden>
			<s:hidden name="tskSampleShipmentcompl" id="tskSampleShipmentcompl" value=""></s:hidden>
			<s:hidden name="tskAcknowledgeResolcompl"
				id="tskAcknowledgeResolcompl"></s:hidden>
				<table><tr>
				   <td colspan="10" rowspan="5" align="right">
					        
					        <s:iterator value="ctOrderDetailsList" var="rowVal">
					        <s:if test='%{#rowVal[2]!=null && #rowVal[2]=="N"}'>
					         <s:if test="hasViewPermission(#request.viewCTShipReport)==true">
					         	<button type="button" onclick="window.open('cbuPdfReports?cdrCbuPojo.CordID=<s:property value="cdrCbuPojo.CordID"/>&repId=180&repName=NMDP CT-Ship Sample Detail Report&reqDate=<s:property value="%{#rowVal[45]}" />&selDate=&params=<s:property value="orderId"/>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><s:text
								name="garuda.ordersreport.button.label"></s:text></button>
							</s:if>
							</s:if>
							<s:if test='%{#rowVal[2]!=null && #rowVal[2]=="Y"}'>
							<s:if test="hasViewPermission(#request.viewCTSampReport)==true">
								<button type="button" onclick="window.open('cbuPdfReports?cdrCbuPojo.CordID=<s:property value="cdrCbuPojo.CordID"/>&repId=180&repName=NMDP CT-Ship Sample Detail Report&reqDate=<s:property value="%{#rowVal[45]}" />&selDate=&params=<s:property value="orderId"/>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><s:text
								name="garuda.ordersreport.button.label"></s:text></button>
							</s:if>
							</s:if>
							</s:iterator>
								
					</td>
					</tr>
					</table>
				
				<!--<s:if
				test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
				<table>
					<tr>
						<td colspan="10" rowspan="5" align="left">
						<button type="button" onclick="changeWorkflow()"><s:text
							name="garuda.currentReqProgress.label.switchworkflow"></s:text></button>
						</td>
					</tr>
				</table>
			</s:if>--> <s:if test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
				<s:iterator value="ctOrderDetailsList" var="rowVal">
				<s:hidden value="%{#rowVal[71]}" name="switchData" id="switchData"/>
			<div id="switchWorkflowDetailsDiv" style="display: none;" class="switchWrkflowCls">
			<table>
				<tr>
					<td><b><s:text
						name="garuda.switchworkflow.label.switchreason" /></b></td>
					<td><s:if test="%{#rowVal[71]!=null && #rowVal[71]!=''}">
						<s:if test="%{#rowVal[71]==getCodeListDescById(switchRsnOtherPk)}">
							<s:property value="%{#rowVal[72]}" />
							<br>
						</s:if>
						<s:else>
							<s:property value="%{#rowVal[71]}" />
						</s:else>
					</s:if> <s:else>
						<s:property value="%{#rowVal[72]}" />
						<br>
					</s:else></td>
				</tr>
				<tr>
					<td><b><s:text
						name="garuda.switchworkflow.label.switchdate" /></b></td>
					<td><s:property value="%{#rowVal[73]}" /></td>
				</tr>
				<tr>
					<td><b><s:text name="garuda.switchworkflow.label.switchby" /></b>
					</td>
					<td><s:set name="workflowSwitchedby" value="%{#rowVal[74]}"
						scope="request" /> <%
 	String workflowswitchuser = "0";
 				if (request.getAttribute("workflowSwitchedby") != null
 						&& !request.getAttribute("workflowSwitchedby")
 								.equals("")) {
 					workflowswitchuser = request.getAttribute(
 							"workflowSwitchedby").toString();
 				}
 				UserJB userB = new UserJB();
 				userB.setUserId(EJBUtil.stringToNum(workflowswitchuser));
 				userB.getUserDetails();
 %> <%=userB.getUserLastName() + " "
								+ userB.getUserFirstName()%></td>
				</tr>
			</table>
		</div>
					<s:if test='%{#rowVal[2]!=null && #rowVal[2]=="N"}'>
						<table>
							<tr style="font-style: italic; color: blue; font-size: 10px">
								<td width="9%" height="20px"></td>
								<td width="9%" align="center"><a
									href="#" onclick="loadOrderDetails('cbuavailconfbarDiv');"><s:property
									value="tskConfirmCbuAvail" /></a></td>
								<td width="9%" align="center"><a
									href="#" onclick="loadOrderDetails('reqclincinfobarDiv');"><s:property
									value="tskReqClincInfo" /></a></td>
								<td width="9%" align="center"><a
									href="#" onclick="loadOrderDetails('additityingandtestDiv');"><s:property
									value="tskAdditiTyping" /></a></td>
								<td width="9%" align="center"><a
									href="#" onclick="loadOrderDetails('ctshipmentbarDiv');"><s:property
									value="tskShipmentInfo" /></a></td>
								<td width="9%" align="center"><a
									href="#" onclick="loadOrderDetails('ctshipmentbarDiv');"><s:property
									value="tskGenPackageSlip" /></a></td>
								<td width="9%" align="center"><a 
									href="#ctshipmentbarDiv" onclick="loadOrderDetails('ctshipmentbarDiv');"><s:text
									name="garuda.ctOrderDetail.progressbar.sampleshipped" /></a></td>
								<td width="9%" align="center"></td>
								<td width="9%" align="center"></td>
								<td width="9%" align="center"><a
									href="#" onclick="loadOrderDetails('ctresolutionbarDiv');"><s:property
									value="tskAcknowledgeResol" /></a></td>
								<td width="10%" style="font-size: 12px; font: bold;"
									align="center" rowspan="4"><s:if test="%{#rowVal[48]>2}">
									<span style="color: red; font-size: 30px"><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:if> <s:else>
									<span style="font-size: 30px""><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:else></td>
							</tr>
							<tr>
								<td width="9%" ></td>
								<td width="9%" id="cbuavailconfbar"></td>
								<td width="9%" id="completeReqInfobar"></td>
								<td width="9%" id="additityingandtest"></td>
								<td width="9%" id="ctshipmentbar"></td>
								<td width="9%" id="genpackageslipbar"></td>
								<td width="9%" id="ctshipdatebar"></td>
								<td width="9%" id="resRecbar"></td>
								<td width="9%" id="Resolutionbar"></td>
								<td width="9%" id="Acknowledgebar"></td>

							</tr>
							<tr>
								<td width="9%"></td>
								<td width="9%" align="left"><img src="images/sort_asc.png" /></td>
								<td width="9%" align="left"><img src="images/sort_asc.png" /></td>
								<td width="9%" align="right"></td>
								<td width="9%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="9%"></td>
								<td width="9%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="9%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="9%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="9%" align="right"><img
									src="images/sort_asc.png" /></td>

							</tr>
							<tr style="font-style: italic; color: green; font-size: 10px">
								<td width="9%"></td>
								<td width="9%" align="left" style=""><s:property
									value="msNew" /></td>
								<td width="9%" align="left"><s:property value="msReserved" /></td>
								<td width="9%" align="right"></td>
								<td width="9%" align="right"><s:property
									value="msShipmentsch" /></td>
								<td width="9%"></td>
								<td width="9%" align="right"><s:property
									value="msAwaitHlaRes" /></td>
								<td width="9%" align="right"><s:property
									value="msHlaResRece" /></td>
								<td width="9%" align="right"><s:property
									value="msResolved" /></td>
								<td width="9%" align="right"><s:property
									value="msComplete" /></td>

							</tr>
						</table>
					</s:if>
					<s:elseif test='%{#rowVal[2]!=null && #rowVal[2]=="Y"}'>
						<table>
							<tr style="font-style: italic; color: blue; font-size: 10px">
								<td width="10%" height="20px"></td>
								<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('cbuavailconfbarDiv');"><s:property
									value="tskConfirmCbuAvail" /></a></td>
								<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('reqclincinfobarDiv');"><s:property
									value="tskReqClincInfo" /></a></td>
								<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('additityingandtestDiv');"><s:property
									value="tskAdditiTyping" /></a></td>
								<td width="10%" align="center"></td>
								<td width="10%" align="center"></td>
								<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('ctresolutionbarDiv');"><s:property
									value="tskAcknowledgeResol" /></a></td>
								<td width="10%" style="font-size: 12px; font: bold;"
									align="center" rowspan="4"><s:if test="%{#rowVal[48]>2}">
									<span style="color: red; font-size: 30px"><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:if> <s:else>
									<span style="font-size: 30px""><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:else></td>
							</tr>
							<tr>
								<td width="10%" ></td>
								<td width="10%" id="cbuavailconfbar"></td>
								<td width="10%" id="completeReqInfobar"></td>
								<td width="10%" id="additityingandtest"></td>
								<td width="10%" id="resRecbar"></td>
								<td width="10%" id="Resolutionbar"></td>
								<td width="10%" id="Acknowledgebar"></td>

							</tr>
							<tr>
								<td width="10%"></td>
								<td width="10%" align="left"><img src="images/sort_asc.png" /></td>
								<td width="10%" align="left"><img src="images/sort_asc.png" /></td>
								<td width="10%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="10%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="10%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="10%" align="right"><img
									src="images/sort_asc.png" /></td>

							</tr>
							<tr style="font-style: italic; color: green; font-size: 10px">
								<td width="10%"></td>
								<td width="10%" align="left" style=""><s:property
									value="msNew" /></td>
								<td width="10%" align="left"><s:property value="msReserved" /></td>
								<td width="10%" align="right"><s:property
									value="msAwaitHlaRes" /></td>
								<td width="10%" align="right"><s:property
									value="msHlaResRece" /></td>
								<td width="10%" align="right"><s:property
									value="msResolved" /></td>
								<td width="10%" align="right"><s:property
									value="msComplete" /></td>

							</tr>
						</table>
					</s:elseif>
				</s:iterator>
			</s:if> <s:if test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
				<table>
					<tr>
						<td width="100%" colspan="3">
						<fieldset>
						<div id="currentRequestProgress">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="10%" align="left"><strong><s:text
									name="garuda.cbuHistory.label.requesttype" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:hidden name="sampleAtLab" id="sampleAtLab"
											value="%{#rowVal[2]}"></s:hidden>
										<s:if test='%{#rowVal[2]!=null && #rowVal[2]=="Y"}'>
											<a href="#" onclick="loadOrderDetails();"><s:text name="garuda.orOrderDetail.portletname.ctlaborder"></s:text></a>
											<s:hidden id="labOrder" value="Y" />
										</s:if>
										<s:if test='%{#rowVal[2]!=null && #rowVal[2]=="N"}'>
											<a href="#" onclick='loadOrderDetails();'><s:text name="garuda.orOrderDetail.portletname.ctshiporder"></s:text></a>
											<s:hidden id="labOrder" value="N" />
										</s:if>
										<s:if
											test='%{(#rowVal[71]!=null && #rowVal[71]!="") || (#rowVal[72]!=null && #rowVal[72]!="")}'>
											<img id="switchworkflowdetId" height="15px"
												src="./images/help_24x24px.png"
												onmouseover="showSwitchWorkflowHelpMsg('switchWorkflowDetailsDiv')"
												onmouseout="return nd();">
										</s:if>
									</s:iterator>
								</s:if></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.requeststatus"></s:text>:</strong>
								</td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<span style="color: red;"> <s:property
											value="%{#rowVal[0]}" /></span>
									</s:iterator>
								</s:if></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.reqStatusDate"></s:text>:</strong>
								</td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test='#rowVal[1]!=null && #rowVal[1]!=""'>
											<s:property value="%{#rowVal[1]}" />
										</s:if>
									</s:iterator>
								</s:if></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.resolbytc" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test='#rowVal[57]!=null && #rowVal[57]!=""'>
											<s:property value="getCbuStatusDescByPk(#rowVal[57])" />
											<s:hidden name="resolByTCFlag" id="resolByTCFlag" value="%{#rowVal[57]}"></s:hidden>
											<s:hidden name="resolByCBBFlag" id="resolByCBBFlag"	value="%{#rowVal[58]}"></s:hidden>
										</s:if>
										<s:elseif test='#rowVal[58]!=null && #rowVal[58]!=""'>
											<s:property value="getCbuStatusDescByPk(#rowVal[58])" />
											<s:hidden name="resolByTCFlag" id="resolByTCFlag" value="%{#rowVal[57]}"></s:hidden>
											<s:hidden name="resolByCBBFlag" id="resolByCBBFlag"	value="%{#rowVal[58]}"></s:hidden>
									</s:elseif>
									</s:iterator>
								</s:if></td>
							</tr>
							<tr>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.ctrequestdate" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test="#rowVal[45]!=null && #rowVal[45]!=''">
											<s:property value="%{#rowVal[45]}" />
										</s:if>
										<s:hidden id="requestedDate" value="%{#rowVal[45]}" />
									</s:iterator>
								</s:if></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.assignedto" /></strong>:</td>
								<td width="15%" align="left">
								<s:if test="lstUsers!=null && lstUsers.size()>0">
									<s:hidden name="assignLstSize" id="assignLstSize" value="%{lstUsers.size()}"/>
								</s:if>
								<s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">									
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:hidden name="hAssignTo" id="hAssignTo" value="%{#rowVal[3]}"/>
										<s:if test='#rowVal[3]!=null && #rowVal[3]!=""'>
											<s:if test="lstUsers!=null && lstUsers.size()>0">
												<s:if test="hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true">
													<s:if test="hasEditPermission(#request.assignOrders)!=true">
																	<select id="userIdval" name="userIdval"  style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test='%{#rowVal[3]==#tempVal}' >selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
																	 </select>
															
													</s:if>
													<s:else>
														<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">-Select-</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test='%{#rowVal[3]==#tempVal}' >selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
													    </select>
													</s:else>
												</s:if>
											</s:if>

										</s:if>
										<s:elseif
											test="(hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true) && (#rowVal[3]==null || #rowVal[3]=='')">
											<s:if test="lstUsers!=null && lstUsers.size()>0">
												<s:if test="hasEditPermission(#request.assignOrders)!=true">
													<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
													</select>
												</s:if>
												<s:else>
													<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
													</select>
												</s:else>
											</s:if>
										</s:elseif>
									</s:iterator>
								</s:if> <s:else>

									<s:if
										test="hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true">
										<s:if test="lstUsers!=null && lstUsers.size()>0">
											<s:if test="hasEditPermission(#request.assignOrders)!=true">
												<select id="userIdval" name="userIdval"  style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
													</select>
											</s:if>
											<s:else>
												<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[1]}"/></option>
																			</s:iterator>
												</select>
											</s:else>
										</s:if>
									</s:if>
								</s:else>								
								</td>
								<s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if
											test='#rowVal[3]!=null && #rowVal[3]!="" && #rowVal[4]!="Not Provided"'>
											<td width="10%" align="left"><strong><s:text
												name="garuda.currentReqProgress.label.assigneddate" />:</strong></td>
											<td width="15%" align="left"><s:property
												value="%{#rowVal[4]}" /></td>
										</s:if>
										<s:else>
											<td width="10%" align="left"></td>
											<td width="15%" align="left"></td>
										</s:else>
									</s:iterator>
								</s:if>
								<s:else>
									<td width="10%" align="left"></td>
									<td width="15%" align="left"></td>
								</s:else>
								<td width="10%" align="left"><strong><s:text
									name="garuda.heResolution.label.resolutiondate" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="orderResolLst!=null && orderResolLst.size>0">
									<s:iterator value="orderResolLst" var="rowVal">
										<s:if test="#rowVal[2]!=null && #rowVal[2]!=''">
											<s:property value="%{#rowVal[2]}" />
										</s:if>
									</s:iterator>
								</s:if></td>
							</tr>
							<tr>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.priority"></s:text>:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test="#rowVal[64]!=null && #rowVal[64]!=''">
											<s:property value="%{#rowVal[64]}" />
										</s:if>
										<s:else>
											<s:text name="garuda.currentReqProgress.label.notprovided" />
										</s:else>
									</s:iterator>
								</s:if><s:else>
									<s:text name="garuda.currentReqProgress.label.notprovided" />
								</s:else></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.lastrequestreviewedby" />:</strong>
								</td>

								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test="#rowVal[5]!=null && #rowVal[5]!=''">
											<s:property value="%{#rowVal[5]}" />
										</s:if>
									</s:iterator>
								</s:if></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.lastrevieweddate" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:hidden name="recRecDateFlag" id="recRecDateFlag"
											value="%{#rowVal[65]}"></s:hidden>
										<s:if test='#rowVal[6]!=null && #rowVal[6]!=""'>
											<s:property value="%{#rowVal[6]}" />
										</s:if>
									</s:iterator>
								</s:if></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.closereason" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test="%{#rowVal[75]!=null && #rowVal[75]!=''}">
											<s:property value="getCbuStatusDescByPk(#rowVal[75])" />
										</s:if>
										<s:hidden name="recentHlaId" id="recentHlaId" value="%{#rowVal[66]}"></s:hidden>
									</s:iterator>
								</s:if></td>
							</tr>
							<tr>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.prevctd" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test="%{#rowVal[67]!=null && #rowVal[67]!=''}">
											<s:if test='%{#rowVal[67]=="Y"}'>
												<s:text name="garuda.clinicalnote.label.yes"></s:text>
											</s:if>
											<s:elseif test='%{#rowVal[67]=="N"}'>
												<s:text name="garuda.clinicalnote.label.no"></s:text>
											</s:elseif>
										</s:if>
									</s:iterator>
								</s:if> <s:else>
									<s:text name="garuda.currentReqProgress.label.notprovided" />
								</s:else></td>
								<td width="10%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test='%{#rowVal[2]!=null && #rowVal[2]=="N"}'>
											<strong><s:text
												name="garuda.ctShipmentInfo.label.ctshipdate" />:</strong>
										</s:if>
									</s:iterator>
								</s:if></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test='%{#rowVal[2]!=null && #rowVal[2]=="N"}'>
										<s:hidden name="recHlaEnteredFlag" id="recHlaEnteredFlag" value="%{#rowVal[60]}"></s:hidden>
										<s:hidden name="hlaTypingAvailFlag" id="hlaTypingAvailFlag"	value="%{#rowVal[59]}"></s:hidden>
											<s:if
												test="shipmentInfoLst!=null && shipmentInfoLst.size()>0">
												<s:iterator value="shipmentInfoLst" var="rowVal">
													<s:property value="%{#rowVal[0]}" />
													<s:hidden name="dataModifiedFlag" id="dataModifiedFlag"	value="%{#rowVal[24]}"></s:hidden>
													<s:hidden name="ctShipDateId" id="ctShipDateId" value="%{#rowVal[0]}" />
													<input type="hidden" name="todate" id="todate" value="<%=Utilities.getFormattedDate(new Date(),"MMM dd, yyyy")%>" >
													<s:hidden name="shipmentPojo.fkshipingCompanyId" id="shipCompany" value="%{#rowVal[2]}"></s:hidden>
													<s:hidden name="shipmentPojo.shipmentTrackingNo" id="trackNo" value="%{#rowVal[3]}"></s:hidden>
													<s:hidden name="SamTypeAvailable" id="SamTypeAvailable" value="%{#rowVal[5]}"></s:hidden>
													<s:hidden name="AliquotsType" id="AliquotsType" value="%{#rowVal[6]}"></s:hidden>
													<s:hidden name="pkShipmentId" id="pkShipmentId"	value="%{#rowVal[25]}"></s:hidden>
													<s:hidden name="pkPackingSlip" id="pkPackingSlipId"></s:hidden>
													<s:hidden name="pkCbuShipment" id="pkCbuShipment"></s:hidden>
												</s:iterator>
											</s:if>
											
										</s:if>
									</s:iterator>
								</s:if></td>
								<td width="10%" align="left"></td>
								<td width="15%" align="left"></td>
								<td width="10%" align="left"><a
									href="#" onclick="loadOrderDetails('cttcorderdetcontentdiv');"><strong><s:text
									name="garuda.ctShipmentInfo.label.resultsrecdate" />:</strong></a></td>
								<td width="15%" align="left"><s:set name="orderServicesLstflag" value="0" scope="request"/>
								<s:if test="resRecDatesCnt==0">
									<s:if test="orderServicesLst!=null && orderServicesLst.size()>0">
										<s:iterator value="orderServicesLst" var="orderServicesPojo">
											<s:if test='resRecDateStr!=null && !resRecDateStr.equals("") && !resRecDateStr.equals("-") && #request.orderServicesLstflag=="0"'>
												<s:property value="resRecDateStr" />
												<s:set name="orderServicesLstflag" value="1" scope="request"/>
												<br>
											</s:if>
										</s:iterator>
									</s:if>
								</s:if></td>
							</tr>
						</table>
						</div>
						</fieldset>
						</td>
					</tr>
				</table>
			</s:if></div>
			</div>
			</div>
			<div id="ctassignto" style="display: none;">
								<table bgcolor="#cccccc" class="tabledisplay disable_esign"
									id="">
									<tr bgcolor="#cccccc" valign="baseline">
										<td width="70%"><jsp:include page="cb_esignature.jsp"
											flush="true">
											<jsp:param value="submitAssignto_CT" name="submitId" />
											<jsp:param value="invalidAssignto" name="invalid" />
											<jsp:param value="minimumAssignto" name="minimum" />
											<jsp:param value="passAssignto" name="pass" />
										</jsp:include></td>
										<td align="left" width="30%"><input type="button"
											id="submitAssignto_CT" onclick="submitAssignto();"
											value="<s:text name="garuda.unitreport.label.button.submit"/>"
											disabled="disabled" /> <input type="button" id="closediv"
											onclick="$j('#ctassignto').hide();"
											value="<s:text name="garuda.common.close"/>" /></td>
									</tr>
								</table>
			</div>
</s:if>
<s:elseif test='getCodeListDescById(orderPojo.orderType)=="HE"'>
<div class="portlet" id="openOrdercbuInfoparent">	
		<div id="openOrdercbuInfo"
			class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
		<div
			class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
		<span class="ui-icon ui-icon-print" onclick="getCurrent();addPrintContent('openOrdercbuInfo','')"></span>
		<span class="ui-icon ui-icon-plusthick" id="currentReqProSpan" onclick="getCurrent();"></span>
		<!--<span class="ui-icon ui-icon-minusthick"></span>--> <s:text
			name="garuda.ctOrderDetail.portletname.currentReqProgress"></s:text>
		</div>
		<div class="portlet-content" id="currentRequestProgressMainDiv"  style="display: none;">
		<s:hidden name="tskConfirmCbuAvailcompl" id="tskConfirmCbuAvailcompl"></s:hidden>
		<s:hidden name="tskReqClincInfocompl" id="tskReqClincInfocompl"></s:hidden>
		<s:hidden name="tskFinalCbuReviewcompl" id="tskFinalCbuReviewcompl"></s:hidden>
		<s:hidden name="tskShipmentInfocompl" id="tskShipmentInfocompl"></s:hidden>
		<s:hidden name="tskGenPackageSlipcompl" id="tskGenPackageSlipcompl"></s:hidden>
		<s:hidden name="tskAdditiTypingcompl" id="tskAdditiTypingcompl"></s:hidden>
		<s:hidden name="tskShipmentConfirmcompl" id="tskShipmentConfirmcompl"></s:hidden>
		<s:hidden name="tskSampleShipmentcompl" id="tskSampleShipmentcompl"></s:hidden>
		<s:hidden name="tskAcknowledgeResolcompl"
			id="tskAcknowledgeResolcompl"></s:hidden>
		<table>
					<tr>
						<td colspan="10" rowspan="5" align="right">
						    <s:if test="hasViewPermission(#request.viewHEReport)==true">
						    <s:if test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
										
									<button type="button" onclick="window.open('cbuPdfReports?cdrCbuPojo.CordID=<s:property value="cdrCbuPojo.CordID"/>&repId=182&repName=NMDP HE Detail Report&reqDate=<s:property value="%{#rowVal[45]}" />&selDate=&params=<s:property value="orderId"/>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><s:text
									name="garuda.ordersreport.button.label"></s:text></button>
							</s:iterator>
							</s:if>
							</s:if>
						</td>
					</tr>
		</table>
		<table>
			<tr style="font-style: italic; color: blue; font-size: 10px">
				<td width="10%" height="20px"></td>
				<td width="20%" align="center"><a
									href="#" onclick="loadOrderDetails('cbuavailconfbarDiv');"><s:property value="tskConfirmCbuAvail" /></a></td>
				<td width="20%" align="center"><a
									href="#" onclick="loadOrderDetails('reqclincinfobarDiv');"><s:property value="tskReqClincInfo" /></a></td>
				<td width="20%" align="center"></td>
				<td width="20%" align="center"><a
									href="#" onclick="loadOrderDetails('heresolutionbarDiv');"><s:property value="tskAcknowledgeResol" /></a></td>
				<td width="10%" style="font-size: 12px; font: bold;" align="center"
					rowspan="4"><s:if
					test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
					<s:iterator value="ctOrderDetailsList" var="rowVal">
						<s:if test="%{#rowVal[48]>2}">
							<span style="color: red; font-size: 30px"><s:property
								value="%{#rowVal[48]}" /></span>
							<br />
							<s:text name="garuda.currentReqProgress.label.noofdays" />
						</s:if>
						<s:else>
							<span style="font-size: 30px""><s:property
								value="%{#rowVal[48]}" /></span>
							<br />
							<s:text name="garuda.currentReqProgress.label.noofdays" />
						</s:else>
					</s:iterator>
				</s:if></td>
			</tr>
			<tr>
				<td width="10%"></td>
				<td width="20%" id="cbuavailconfbar"></td>
				<td width="20%" id="completeReqInfobar"></td>
				<td width="20%" id="Resolutionbar"></td>
				<td width="20%" id="Acknowledgebar"></td>

			</tr>
			<tr>
				<td width="10%"></td>
				<td width="20%">
				<table>
					<tr>
						<td width="10%" align="left"><img src="images/sort_asc.png"
							align="left" /></td>
						<td width="10%" align="right"><img src="images/sort_asc.png"
							align="right" /></td>
					</tr>
				</table>

				</td>
				<td width="20%" align="right"></td>
				<td width="20%" align="right"><img src="images/sort_asc.png" /></td>
				<td width="20%" align="right"><img src="images/sort_asc.png" /></td>

			</tr>
			<tr style="font-style: italic; color: green; font-size: 10px">
				<td width="10%"></td>
				<td width="20%">
				<table>
					<tr style="font-style: italic; color: green; font-size: 10px">
						<td width="10%" align="left"><s:property value="msNew" /></td>
						<td width="10%" align="right"><s:property value="msReserved" />
						</td>
					</tr>
				</table>
				</td>
				<td width="20%" align="right"></td>
				<td width="20%" align="right"><s:property value="msResolved" /></td>
				<td width="20%" align="right"><s:property value="msComplete" /></td>

			</tr>
		</table>
		<div id="currentRequestProgress">
		<table>
			<tr>
				<td width="100%" colspan="3">
				<fieldset>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="10%" align="left"><strong><s:text
							name="garuda.cbuHistory.label.requesttype" />:</strong></td>
						<td width="15%" align="left"><a href="#" onclick='loadOrderDetails();'><s:text
							name="garuda.heOrderDetail.portletname.heOrder" /></a></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.requeststatus" />:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="%{#rowVal[0]!=null && #rowVal[0]!=''}">
									<span style="color: red;"><s:property
										value="%{#rowVal[0]}" /></span>
								</s:if>
							</s:iterator>
						</s:if></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.reqStatusDate"></s:text>:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test='#rowVal[1]!=null && #rowVal[1]!=""'>
									<s:property value="%{#rowVal[1]}" />
								</s:if>
							</s:iterator>
						</s:if></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.resolbytc" />:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test='#rowVal[57]!=null && #rowVal[57]!=""'>
											<s:property value="getCbuStatusDescByPk(#rowVal[57])" />
											<s:hidden name="resolByTCFlag" id="resolByTCFlag" value="%{#rowVal[57]}"></s:hidden>
											<s:hidden name="resolByCBBFlag" id="resolByCBBFlag"	value="%{#rowVal[58]}"></s:hidden>
										</s:if>
										<s:elseif test='#rowVal[58]!=null && #rowVal[58]!=""'>
											<s:property value="getCbuStatusDescByPk(#rowVal[58])" />
											<s:hidden name="resolByTCFlag" id="resolByTCFlag" value="%{#rowVal[57]}"></s:hidden>
											<s:hidden name="resolByCBBFlag" id="resolByCBBFlag"	value="%{#rowVal[58]}"></s:hidden>
									</s:elseif>
							</s:iterator>
						</s:if></td>
					</tr>
					<tr>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.herequestdate" />:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="%{#rowVal[45]!=null && #rowVal[45]!=''}">
									<s:property value="%{#rowVal[45]}" />
									<s:hidden id="requestedDate" value="%{#rowVal[45]}" />
								</s:if>
							</s:iterator>
						</s:if></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.assignedto" /></strong>:</td>
						<td width="15%" align="left">
						<s:if test="lstUsers!=null && lstUsers.size()>0">
									<s:hidden name="assignLstSize" id="assignLstSize" value="%{lstUsers.size()}"/>
						</s:if>
						<s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:hidden name="hAssignTo" id="hAssignTo" value="%{#rowVal[3]}"/>
								<s:if test='#rowVal[3]!=null && #rowVal[3]!=""'>
									<s:if test="lstUsers!=null && lstUsers.size()>0">
										<s:if
											test="hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true">
											<s:if test="hasEditPermission(#request.assignOrders)!=true">
												<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test='%{#rowVal[3]==#tempVal}' >selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
												</select>
											</s:if>
											<s:else>
												<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test='%{#rowVal[3]==#tempVal}' >selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
												</select>
											</s:else>
										</s:if>
									</s:if>

								</s:if>
								<s:elseif
									test="(hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true) && (#rowVal[3]==null || #rowVal[3]=='')">
									<s:if test="lstUsers!=null && lstUsers.size()>0">
										<s:if test="hasEditPermission(#request.assignOrders)!=true">
											<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' ><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
											</select>
										</s:if>
										<s:else>
											<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" >
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' ><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
											</select>
										</s:else>
									</s:if>
								</s:elseif>
							</s:iterator>
						</s:if> <s:else>

							<s:if
								test="hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true">
								<s:if test="lstUsers!=null && lstUsers.size()>0">
									<s:if test="hasEditPermission(#request.assignOrders)!=true">
										<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
										</select>
									</s:if>
									<s:else>
										<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
										</select>
									</s:else>
								</s:if>
							</s:if>
						</s:else>
						</td>
						<s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if
									test='#rowVal[3]!=null && #rowVal[3]!="" && #rowVal[4]!="Not Provided"'>
									<td width="10%" align="left"><strong><s:text
										name="garuda.currentReqProgress.label.assigneddate" />:</strong></td>
									<td width="15%" align="left"><s:if
										test="#rowVal[4]!=null && #rowVal[4]!=''">
										<s:property value="%{#rowVal[4]}" />
									</s:if> <s:else>
										<s:text name="garuda.currentReqProgress.label.notprovided" />
									</s:else>
								</s:if>
								<s:else>
									<td width="10%" align="left"></td>
									<td width="15%" align="left"></td>
								</s:else>
							</s:iterator>
						</s:if>
						<s:else>
							<td width="10%" align="left"></td>
							<td width="15%" align="left"></td>
						</s:else>
						<td width="10%" align="left"><strong><s:text
							name="garuda.heResolution.label.resolutiondate" />:</strong></td>
						<td width="15%" align="left"><s:if
							test="orderResolLst!=null && orderResolLst.size>0">
							<s:iterator value="orderResolLst" var="rowVal">
								<s:if test="#rowVal[2]!=null && #rowVal[2]!=''">
									<s:property value="%{#rowVal[2]}" />
								</s:if>
							</s:iterator>
						</s:if></td>
					</tr>
					<tr>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.priority"></s:text>:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="%{#rowVal[64]!=null && #rowVal[64]!=''}">
									<s:property value="%{#rowVal[64]}" />
								</s:if>
								<s:else>
									<s:text name="garuda.currentReqProgress.label.notprovided" />
								</s:else>
							</s:iterator>
						</s:if> <s:else>
							<s:text name="garuda.currentReqProgress.label.notprovided" />
						</s:else></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.lastrequestreviewedby" />:</strong>
						</td>

						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test='#rowVal[5]!=null && #rowVal[5]!=""'>
									<s:property value="%{#rowVal[5]}" />
								</s:if>
							</s:iterator>
						</s:if></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.lastrevieweddate" />:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:hidden name="recRecDateFlag" id="recRecDateFlag"
									value="%{#rowVal[65]}"></s:hidden>
								<s:if test="#rowVal[6]!=null && #rowVal[6]!=''">
									<s:property value="%{#rowVal[6]}" />
								</s:if>
							</s:iterator>
						</s:if></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.closereason" />:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="%{#rowVal[75]!=null && #rowVal[75]!=''}">
									<s:property value="getCbuStatusDescByPk(#rowVal[75])" />
								</s:if>
							</s:iterator>
						</s:if></td>
					</tr>
				</table>
				</fieldset>
				</td>
			</tr>
		</table>
		</div>
		<!-- <br></br>
		<br></br>
		<br></br> -->
		</div>
		</div>
		</div>
		<div id="ctassignto" style="display: none;">
						<table bgcolor="#cccccc" class="tabledisplay disable_esign" id="">
							<tr bgcolor="#cccccc" valign="baseline">
								<td width="70%"><jsp:include page="cb_esignature.jsp"
									flush="true">
									<jsp:param value="submitAssignto_HE" name="submitId" />
									<jsp:param value="invalidAssignto" name="invalid" />
									<jsp:param value="minimumAssignto" name="minimum" />
									<jsp:param value="passAssignto" name="pass" />
								</jsp:include></td>
								<td align="left" width="30%"><input type="button"
									id="submitAssignto_HE" onclick="submitAssignto();"
									value="<s:text name="garuda.unitreport.label.button.submit"/>"
									disabled="disabled" /> <input type="button" id="closediv"
									onclick="$j('#ctassignto').hide();"
									value="<s:text name="garuda.common.close"/>" /></td>
							</tr>
						</table>
		</div>
</s:elseif>
<s:elseif test='getCodeListDescById(orderPojo.orderType)=="OR"'>
<div class="portlet" id="openOrdercbuInfoparent">
			<div id="openOrdercbuInfo"
				class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			<span class="ui-icon ui-icon-print" onclick="getCurrent();addPrintContent('openOrdercbuInfo','')"></span>
			<span class="ui-icon ui-icon-plusthick" id="currentReqProSpan" onclick="getCurrent();"></span>
			<!--<span class="ui-icon ui-icon-minusthick"></span>--> <s:text
				name="garuda.ctOrderDetail.portletname.currentReqProgress" /></div>
			<div class="portlet-content" id="currentRequestProgressMainDiv"  style="display: none;">
			<s:hidden name="tskConfirmCbuAvailcompl" id="tskConfirmCbuAvailcompl"></s:hidden>
			<s:hidden name="tskReqClincInfocompl" id="tskReqClincInfocompl"></s:hidden>
			<s:hidden name="tskFinalCbuReviewcompl" id="tskFinalCbuReviewcompl"></s:hidden>
			<s:hidden name="tskShipmentInfocompl" id="tskShipmentInfocompl"></s:hidden>
			<s:hidden name="tskGenPackageSlipcompl" id="tskGenPackageSlipcompl"></s:hidden>
			<s:hidden name="tskAdditiTypingcompl" id="tskAdditiTypingcompl"></s:hidden>
			<s:hidden name="tskShipmentConfirmcompl" id="tskShipmentConfirmcompl"></s:hidden>
			<s:hidden name="tskSampleShipmentcompl" id="tskSampleShipmentcompl"></s:hidden>
			<s:hidden name="tskAcknowledgeResolcompl" id="tskAcknowledgeResolcompl"></s:hidden>
			<table>
					<tr>
						<td colspan="10" rowspan="5" align="right">
						     <s:if test="hasViewPermission(#request.viewORReport)==true">
						     <s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
											<button type="button" onclick="window.open('cbuPdfReports?cdrCbuPojo.CordID=<s:property value="cdrCbuPojo.CordID"/>&repId=183&repName=NMDP OR Detail Report&reqDate=<s:property value="%{#rowVal[45]}" />&selDate=&params=<s:property value="orderId"/>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><s:text
											name="garuda.ordersreport.button.label"></s:text></button>
									</s:iterator>
								</s:if>
							</s:if>
						</td>
					</tr>
		    </table> 
			<s:if test='NMDPResearchSampleFlag==true'>
				<table>
					<tr style="font-style: italic; color: blue;">
						<td width="5%" height="20px"></td>
						<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('cbuavailconfbarDiv');"><s:property value="tskConfirmCbuAvail" /></a></td>
						<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('reqclincinfobarDiv');"><s:property
							value="tskReqClincInfo" /></a></td>
						<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('finalcbureviewbarDiv');"><s:property
							value="tskFinalCbuReview" /></a></td>
						<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('cbushipmentbarDiv');"><s:property
							value="tskShipmentInfo" /></a></td>
						<td width="10%" align="center"><s:if
							test="tskGenPackageSlip!=null && tskGenPackageSlip!=''">
							<a href="#" onclick="loadOrderDetails('cbushipmentbarDiv');"><s:text
								name="garuda.cbuShipment.label.createshipmentpacket" /></a>
						</s:if></td>
						<td width="10%" align="center"><a href="#" onclick="loadOrderDetails('cbushipmentbarDiv');"><s:property
							value="tskShipmentConfirm" /></a></td>
						<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('nmdpresearchSampleDiv');"><s:property
							value="tskSampleShipment" /></a></td>
						<td width="10%" align="center"></td>
						<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('orresolutionbarDiv');"><s:property value="tskAcknowledgeResol" /></a></td>
						<td width="5%" align="center" rowspan="4"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="%{#rowVal[48]>2}">
									<span style="color: red; font-size: 30px"><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:if>
								<s:else>
									<span style="font-size: 30px""><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:else>
							</s:iterator>
						</s:if></td>
					</tr>
					<tr>
						<td width="5%"></td>
						<td width="10%" id="cbuavailconfbar"></td>
						<td width="10%" id="completeReqInfobar"></td>
						<td width="10%" id="finalReviewbar"></td>
						<td width="10%" id="ctshipmentbar"></td>
						<td width="10%" id="genpackageslipbar"></td>
						<td width="10%" id="cordShipedbar"></td>
						<td width="10%" id="nmdpsamplebar"></td>
						<td width="10%" id="Resolutionbar"></td>
						<td width="10%" id="Acknowledgebar"></td>

					</tr>
					<tr>
						<td width="5%"></td>
						<td width="10%" align="left"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="left"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>

					</tr>
					<tr style="font-style: italic; color: green;">
						<td width="5%"></td>
						<td width="10%" align="left" style=""><s:property
							value="msNew" /></td>
						<td width="10%" align="left"><s:property value="msReserved" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><s:property
							value="msShipmentsch" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><s:property
							value="msCordShipped" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><s:property value="msResolved" /></td>
						<td width="10%" align="right"><s:property value="msComplete" /></td>

					</tr>
				</table>
			</s:if> <s:else>
				<table>
					<tr style="font-style: italic; color: blue;">
						<td width="5%" height="20px"></td>
						<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('cbuavailconfbarDiv');"><s:property value="tskConfirmCbuAvail" /></a></td>
						<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('reqclincinfobarDiv');"><s:property
							value="tskReqClincInfo" /></a></td>
						<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('finalcbureviewbarDiv');"><s:property
							value="tskFinalCbuReview" /></a></td>
						<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('cbushipmentbarDiv');"><s:property
							value="tskShipmentInfo" /></a></td>
						<td width="10%" align="center"><s:if
							test="tskGenPackageSlip!=null && tskGenPackageSlip!=''">
							<a href="#" onclick="loadOrderDetails('cbushipmentbarDiv');"><s:text
								name="garuda.cbuShipment.label.createshipmentpacket" /></a>
						</s:if></td>
						<td width="10%" align="center"><a href="#" onclick="loadOrderDetails('cbushipmentbarDiv');"><s:property
							value="tskShipmentConfirm" /></a></td>
						<td width="10%" align="center"></td>
						<td width="10%" align="center"><a
									href="#" onclick="loadOrderDetails('orresolutionbarDiv');"><s:property value="tskAcknowledgeResol" /></a></td>
						<td width="15%" align="center" rowspan="4"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="%{#rowVal[48]>2}">
									<span style="color: red; font-size: 30px"><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:if>
								<s:else>
									<span style="font-size: 30px""><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:else>
							</s:iterator>
						</s:if></td>
					</tr>
					<tr>
						<td width="5%"></td>
						<td width="10%" id="cbuavailconfbar"></td>
						<td width="10%" id="completeReqInfobar"></td>
						<td width="10%" id="finalReviewbar"></td>
						<td width="10%" id="ctshipmentbar"></td>
						<td width="10%" id="genpackageslipbar"></td>
						<td width="10%" id="cordShipedbar"></td>
						<td width="10%" id="Resolutionbar"></td>
						<td width="10%" id="Acknowledgebar"></td>

					</tr>
					<tr>
						<td width="5%"></td>
						<td width="10%" align="left"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="left"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>

					</tr>
					<tr style="font-style: italic; color: green;">
						<td width="5%"></td>
						<td width="10%" align="left" style=""><s:property
							value="msNew" /></td>
						<td width="10%" align="left"><s:property value="msReserved" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><s:property
							value="msShipmentsch" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><s:property
							value="msCordShipped" /></td>
						<td width="10%" align="right"><s:property value="msResolved" /></td>
						<td width="10%" align="right"><s:property value="msComplete" /></td>

					</tr>
				</table>
			</s:else>
			<table>
				<tr>
					<td width="100%" colspan="3">
					<fieldset>
					<div id="currentRequestProgress">
					<table>
						<tr>
							<td width="10%"><strong><s:text
								name="garuda.cbuHistory.label.requesttype" />:</strong></td>
							<td width="15%"><a href="#" onclick='loadOrderDetails();'><s:text
								name="garuda.orOrderDetail.portletname.orOrder"></s:text></a></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.requeststatus"></s:text>:</strong>
							</td>
							<td width="15%"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<span style="color: red;"> <s:property
										value="%{#rowVal[0]}" /></span>
								</s:iterator>
							</s:if></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.reqStatusDate"></s:text>:</strong>
							</td>
							<td width="15%"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="#rowVal[1]!=null && #rowVal[1]!=''">
									<s:property value="%{#rowVal[1]}" />
								</s:if>
								</s:iterator>
							</s:if></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.resolbytc" />:</strong></td>
							<td width="15%"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test='#rowVal[61]!=null && #rowVal[61]!=""'>
											<s:hidden name="nmdpSampleShipped" id="nmdpSampleShipped" value="%{#rowVal[61]}"></s:hidden>
										</s:if>		
										<s:if test='#rowVal[57]!=null && #rowVal[57]!=""'>
											<s:property value="getCbuStatusDescByPk(#rowVal[57])" />
											<s:hidden name="resolByTCFlag" id="resolByTCFlag" value="%{#rowVal[57]}"></s:hidden>
										</s:if>
										<s:elseif test='#rowVal[58]!=null && #rowVal[58]!=""'>
											<s:property value="getCbuStatusDescByPk(#rowVal[58])" />
											<s:hidden name="resolByTCFlag" id="resolByTCFlag" value="%{#rowVal[57]}"></s:hidden>
											<s:hidden name="resolByCBBFlag" id="resolByCBBFlag"	value="%{#rowVal[58]}"></s:hidden>
									</s:elseif>
								</s:iterator>
							</s:if></td>
						</tr>
						<tr>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.orrequestdate" />:</strong></td>
							<td width="15%"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<s:property value="%{#rowVal[45]}" />
									<s:hidden id="requestedDate" value="%{#rowVal[45]}" />
								</s:iterator>
							</s:if></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.assignedto" />:</strong></td>
							<td width="15%">
							<s:if test="lstUsers!=null && lstUsers.size()>0">
									<s:hidden name="assignLstSize" id="assignLstSize" value="%{lstUsers.size()}"/>
						    </s:if>
							<s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:hidden name="hAssignTo" id="hAssignTo" value="%{#rowVal[3]}"/>
									<s:if test='#rowVal[3]!=null && #rowVal[3]!=""'>
										<s:if test="lstUsers!=null && lstUsers.size()>0">
											<s:if
												test="hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true">
												<s:if test="hasEditPermission(#request.assignOrders)!=true">
													<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">-Select-</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test='%{#rowVal[3]==#tempVal}' >selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
													</select>
												</s:if>
												<s:else>
													<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">-Select-</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test='%{#rowVal[3]==#tempVal}' >selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
													</select>
												</s:else>
											</s:if>
										</s:if>

									</s:if>
									<s:elseif
										test="(hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true) && (#rowVal[3]==null || #rowVal[3]=='')">
										<s:if test="lstUsers!=null && lstUsers.size()>0">
											<s:if test="hasEditPermission(#request.assignOrders)!=true">
												<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' ><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
													</select>
											</s:if>
											<s:else>
												<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' ><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
												</select>
											</s:else>
										</s:if>
									</s:elseif>
								</s:iterator>
							</s:if> <s:else>

								<s:if
									test="hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true">
									<s:if test="lstUsers!=null && lstUsers.size()>0">
										<s:if test="hasEditPermission(#request.assignOrders)!=true">
											<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' ><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
													</select>
										</s:if>
										<s:else>
											<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' ><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
											</select>
										</s:else>
									</s:if>
								</s:if>
							</s:else>
							</td>
							<s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<s:if test='#rowVal[3]!=null && #rowVal[3]!=""'>
										<td width="10%" align="left"><strong><s:text
											name="garuda.currentReqProgress.label.assigneddate" />:</strong></td>
										<td width="15%" align="left"><s:property
											value="%{#rowVal[4]}" /></td>
									</s:if>
									<s:else>
										<td width="10%" align="left"></td>
										<td width="15%" align="left"></td>
									</s:else>
								</s:iterator>
							</s:if>
							<s:else>
								<td width="10%" align="left"></td>
								<td width="15%" align="left"></td>
							</s:else>
							<td width="10%" align="left"><strong><s:text
								name="garuda.heResolution.label.resolutiondate" />:</strong></td>
							<td width="15%" align="left"><s:if
								test="orderResolLst!=null && orderResolLst.size>0">
								<s:iterator value="orderResolLst" var="rowVal">
									<s:if test="#rowVal[2]!=null && #rowVal[2]!=''">
										<s:property value="%{#rowVal[2]}" />
									</s:if>
								</s:iterator>
							</s:if></td>
						</tr>
						<tr>
							<td width="10%" align="left"><strong><s:text
								name="garuda.currentReqProgress.label.priority"></s:text>:</strong></td>
							<td width="15%" align="left"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<s:if test="#rowVal[64]!=null && #rowVal[64]!=''">
										<s:property value="%{#rowVal[64]}" />
									</s:if>
									<s:else>
										<s:text name="garuda.currentReqProgress.label.notprovided" />
									</s:else>
								</s:iterator>
							</s:if><s:else>
								<s:text name="garuda.currentReqProgress.label.notprovided" />
							</s:else></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.lastrequestreviewedby" />:</strong>
							</td>
							<td width="15%"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<s:if test='#rowVal[5]!=null && #rowVal[5]!=""'>
										<s:property value="%{#rowVal[5]}" />
									</s:if>
								</s:iterator>
							</s:if></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.lastrevieweddate" />:</strong></td>
							<td width="15%"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<s:if test="#rowVal[6]!=null && #rowVal[6]!=''">
									<s:property value="%{#rowVal[6]}" />
								</s:if>
								</s:iterator>
							</s:if></td>
							<td width="10%" align="left"><strong><s:text
								name="garuda.currentReqProgress.label.closereason" />:</strong></td>
							<td width="15%" align="left"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<s:if test="%{#rowVal[75]!=null && #rowVal[75]!=''}">
										<s:property value="getCbuStatusDescByPk(#rowVal[75])" />
									</s:if>
								</s:iterator>
							</s:if></td>
						</tr>
						<tr>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.propcbushipdate" />:</strong></td>
							<td width="15%"><s:if
								test="shipmentInfoLst!=null && shipmentInfoLst.size()>0">
								<s:iterator value="shipmentInfoLst" var="rowVal1">
									<s:if test="#rowVal[27]!=null && #rowVal[27]!=''">
										<s:property value="%{#rowVal1[27]}" />
									</s:if>
									<s:else>
										<s:text name="garuda.currentReqProgress.label.notprovided"></s:text>
									</s:else>
								</s:iterator>
							</s:if> <s:else>
								<s:text name="garuda.currentReqProgress.label.notprovided"></s:text>
							</s:else></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.propprepdate" />:</strong></td>
							<td width="15%"><s:if
								test="shipmentInfoLst!=null && shipmentInfoLst.size()>0">
								<s:iterator value="shipmentInfoLst" var="rowVal1">
									<s:if test="#rowVal[26]!=null && #rowVal[26]!=''">
										<s:property value="%{#rowVal[26]}" />
									</s:if>
									<s:else>
										<s:text name="garuda.currentReqProgress.label.notprovided"></s:text>
									</s:else>
								</s:iterator>
							</s:if> <s:else>
								<s:text name="garuda.currentReqProgress.label.notprovided"></s:text>
							</s:else></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.propinfusiondate" />:</strong></td>
							<td width="15%"><s:if
								test="shipmentInfoLst!=null && shipmentInfoLst.size()>0">
								<s:iterator value="shipmentInfoLst" var="rowVal1">
									<s:if test="#rowVal[25]!=null && #rowVal[25]!=''">
										<s:property value="%{#rowVal[25]}" />
									</s:if>
									<s:else>
										<s:text name="garuda.currentReqProgress.label.notprovided"></s:text>
									</s:else>
								</s:iterator>
							</s:if> <s:else>
								<s:text name="garuda.currentReqProgress.label.notprovided"></s:text>
							</s:else></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.infusiondate" />:</strong></td>
							<td width="15%"><s:if
								test="shipmentInfoLst!=null && shipmentInfoLst.size()>0">
								<s:iterator value="shipmentInfoLst" var="rowVal1">
									<s:if test="#rowVal[56]!=null && #rowVal[56]!=''">
										<s:property value="%{#rowVal[56]}" />
									</s:if>
								</s:iterator>
							</s:if></td>
						</tr>
					</table>
					</div>
					</fieldset>
					</td>
				</tr>
			</table>
			</div>
			</div>
			</div>
			<div id="ctassignto" style="display: none;">
							<table bgcolor="#cccccc" class="tabledisplay disable_esign" id="">
								<tr bgcolor="#cccccc" valign="baseline">
									<td width="70%"><jsp:include page="cb_esignature.jsp"
										flush="true">
										<jsp:param value="submitAssignto_OR" name="submitId" />
										<jsp:param value="invalidAssignto" name="invalid" />
										<jsp:param value="minimumAssignto" name="minimum" />
										<jsp:param value="passAssignto" name="pass" />
									</jsp:include></td>
									<td align="left" width="30%"><input type="button"
										id="submitAssignto_OR" onclick="submitAssignto();"
										value="<s:text name="garuda.unitreport.label.button.submit"/>"
										disabled="disabled" /> <input type="button" id="closediv"
										onclick="$j('#ctassignto').hide();" value="Close" /></td>
								</tr>
							</table>
			</div>
</s:elseif>
<div id="printDiv" style="hidden: true"></div>