<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>

<jsp:include page="cb_user_rights.jsp"></jsp:include>
<s:set name="tempVal" value="cbuFinalReviewPojo.finalReviewConfirmStatus"></s:set>
<div id="procinfo">
			<table cellpadding="0" cellspacing="0" width="100%">
			    <tr align="left">
			       <td width="20%">
			          <s:text name="garuda.cbbprocedures.label.cbbprocessingprocedures" />:
			       </td>
			       <td width="30%">
			          <s:if test="#request.cbbProceduresList!=null">
			              <s:select disabled="true" name="cdrCbuPojo.fkCbbProcedure" listKey="pkProcId" listValue="procName" list="#request.cbbProceduresList" headerKey="-1" headerValue="Select"></s:select>
			           </s:if>     
					</td>
					<!--<s:if test="cdrCbuPojo.fkCbbProcedure != null && cdrCbuPojo.fkCbbProcedure != -1">
						<td colspan="2">
							<a href="javascript:void(0);" onclick="showModal('View CBB Procedure','refreshCBBProcedures?cbbProcedures.pkProcId=<s:property value='cdrCbuPojo.fkCbbProcedure'/>','500','850');"><s:text name="garuda.common.lable.view"></s:text> </a>
					    </td>
					</s:if>
					<s:else>
					       <td colspan="2"></td>
				    </s:else>
			    -->
			      <td colspan="2"></td>
			    </tr>
			    <!-- 
				<tr align="left">
					<td width="20%;"><s:text name="garuda.cbuentry.label.tncfrozen" />:</td>
					<td width="30%;"><s:textfield readonly="true" onkeydown="cancelBack();"  name="CdrCbuPojo.tncFrozen"
						id="tncfrozen" readonly="true" onkeydown="cancelBack();"  /></td>
					<td width="20%;"><s:text name="garuda.cbuentry.label.tncfrozenpatwt" />:
					</td>
					<td width="30%;"><s:textfield readonly="true" onkeydown="cancelBack();" 
						name="CdrCbuPojo.frozenPatWt" id="tncfrozenpatwt" maxlength="5"
						readonly="true" onkeydown="cancelBack();"  /></td>
				</tr>
				<tr align="left">
					<td width="20%;"><s:text name="garuda.cbuentry.label.totcelfrozen" />:</td>
					<td width="30%;"><s:textfield readonly="true" onkeydown="cancelBack();"  name="CdrCbuPojo.cellCount"
						id="totcelfrozen" maxlength="5" readonly="true" onkeydown="cancelBack();"  /></td>

				</tr>
				-->
			</table>
			<table>
				<tr align="right">
					<td colspan="4">
					     <s:if test="hasEditPermission(#request.updateProInfoWidget)==true && cdrCbuPojo.site.isCDRUser()==true">
					           		 
									 <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
    </s:if>
    <s:else>
					<input type="button"
						name="procedureEdit" value="<s:text name="garuda.common.lable.edit"/>"
						onclick="fn_showModalProcedure('<s:text name="garuda.completeReqInfo.label.editCbuProcedure"/> <s:text name="garuda.message.modal.headerregid"/> <s:property value="cdrCbuPojo.registryId" />','updateProcedure?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','500','800');">
						</s:else>
					     </s:if>
					</td>
				</tr>
			</table>
			<table>
				<tr align="right">
					<td colspan="4">
					     <jsp:include page="cord-entry/cb_procedure_refresh.jsp" >
					        <jsp:param value="true" name="sampInventReadOnly"/>
					     </jsp:include>
					</td>
				</tr>
			</table>
			
			</div>	 
  
		    	<div >		  
						     <table width="100" cellpadding="0" cellspacing="0" border="0">
						     			  <!--<tr>
											    <td><s:text name="garuda.cordentry.label.serMatAli"></s:text>:</td>
												<td><s:textfield readonly="true" onkeydown="cancelBack();"  name="entitySamplesPojo.serMatAli" cssClass="positive" maxlength="2"></s:textfield></td>
												
												<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.plasMatAli"></s:text>:</td>	
												<td><s:textfield readonly="true" onkeydown="cancelBack();"  name="entitySamplesPojo.plasMatAli" cssClass="positive" maxlength="2"></s:textfield></td>
										  </tr>
						     	
						     			  <tr>
											    <td><s:text name="garuda.cordentry.label.extDnaMat"></s:text>:</td>	
											    <td><s:textfield readonly="true" onkeydown="cancelBack();"  name="entitySamplesPojo.extDnaMat" cssClass="positive" maxlength="2"></s:textfield></td>
											
											    <td style="padding-left:10px;"><s:text name="garuda.cbbprocedures.label.noMiscMat"></s:text>:</td>	
												<td><s:textfield readonly="true" onkeydown="cancelBack();"  name="entitySamplesPojo.noMiscMat" cssClass="positive" maxlength="2"></s:textfield></td>
										  </tr>
						    
				--><tr align="right">
			      <td colspan="4">
			        <s:if test="hasEditPermission(#request.updateProInfoWidget)==true && cdrCbuPojo.site.isCDRUser()==true">
			       <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
			        </s:if>
			        <s:else>
			        <input type="button" name="procedureEdit1"
			        onclick="fn_showModalProcedure('EDIT CBU and Maternal Samples for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','updateProcessInfo?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_PROC_INFO" />&update=False&entityId=<s:property value="cdrCbuPojo.cordID"/>&entityType=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_TYPE" />&entitySubtype=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_SUB_TYPE" />&cordentry=true','540','900');"
			        value="<s:text name="garuda.common.lable.edit"/>"></input>
			        
			        </s:else>
			       </s:if>
			      </td>
			    </tr>
			 </table>	
		</div>
		
		
				<div id="procccingNotes">
				
				<div id="processnotecontent" onclick="toggleDiv('processnote')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cdrcbuview.label.button.processinfoclinicalnotes" /></div>				
			<div id="processnote" style=" width: 463px; height: 100%;">
			<div id="loadProcessingProcedureClinicalNoteDiv">
					<s:set name="cbuCateg" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CLINIC_CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESS_NOTE_CATEGORY_CODESUBTYPE) " />
					<jsp:include page="modal/cb_load_clinicalNote.jsp"></jsp:include>
			</div>
			  <s:if test="hasNewPermission(#request.vClnclNote)==true">
				 <s:if test='(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)) '>
				  	<button type="button" disabled="disabled"><s:text
				name="garuda.cdrcbuview.label.button.addnotes" /></button>
				  </s:if>
				    <s:else>
						<button type="button"
						onclick="fn_showModalProcForNotes('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','clinicalNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cat=PROCESS_NOTE_CATEGORY_CODESUBTYPE','550','850','notesModalsDiv');"><s:text
						name="garuda.cdrcbuview.label.button.addnotes" /></button>
				  </s:else>
			</s:if>
			</div>
			   </div> 
			<!--lab summary TBD			-->
			<div id="loadProcessingDocInfoDiv">
			<s:if test =" (#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_INFO_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_INFO_CODESUBTYPE].size>0 && hasViewPermission(#request.viewProcesInfoCat)==true)||(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size > 0 && hasViewPermission(#request.viewUnitRepCat)==true)">		
			<div id="loadProcessingDocInfocontent" onclick="toggleDiv('loadProcessingDocInfo')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;">
				<span class="ui-icon ui-icon-triangle-1-s"></span> 
				<s:text name="garuda.uploaddoc.label.button.processinfouploaddocument" />
			</div>
			<div id="loadProcessingDocInfo" >
			<s:if test =" #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_INFO_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_INFO_CODESUBTYPE].size>0 ||#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null ||#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size>0">
			<s:if test="hasViewPermission(#request.viewProcesInfoCat)==true">
			<s:set name="cbuDocCateg" value="@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_INFO_CODESUBTYPE"  scope="request" />
			</s:if>			
						<jsp:include page="modal/cb_load_upload_document.jsp">
							<jsp:param value="<%=request.getAttribute(\"cbuDocCateg\")%>" name="cbuDocCateg"/>
						</jsp:include>						
			
			</s:if>
			</div>	
		 	</s:if>		
	</div>
	<script>
	function fn_showModalProcedure(title,url,hight,width){
		var patientId = $j('#patientId').val();
		var patient_data = '';
		if(patientId!="" && patientId != null && patientId != "undefined"){
			patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
		}
		title=title+patient_data;
		showModal(title,url,hight,width)
		}
	function fn_showModalProcForNotes(title,url,hight,width,id){
		var patientId = $j('#patientId').val();
		var patient_data = '';
		if(patientId!="" && patientId != null && patientId != "undefined"){
			patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
		}
		title=title+patient_data;
		showModals(title,url,hight,width,id);
		}
			</script>