<%@taglib prefix="s" uri="/struts-tags"%><%
String prefix = (String)request.getParameter("prefix");
prefix = prefix + ".";
request.setAttribute("prefix",prefix);
//String hideFields=request.getParameter("hideFields");
%>
<%@page import="com.velos.ordercomponent.business.pojoobjects.CBBPojo"%><script>
   $j( "input:submit, button", ".column" ).button();
		 
  
</script>

    <tr>
      <td>
      <s:text name="garuda.cbbdefaults.lable.address1">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}address1" ></s:textfield></td>
      <td>
      <s:text name="garuda.cbbdefaults.lable.address2">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}address2" ></s:textfield></td>
    </tr>
    <tr>
      <td>
      <s:text name="garuda.cbbdefaults.lable.city">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}city" ></s:textfield></td>
      <td>
      <s:text name="garuda.cbbdefaults.lable.state">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}state" ></s:textfield></td>
    </tr>
    <tr>
      <td>
      <s:text name="garuda.cbbdefaults.lable.zipcode">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}zip" ></s:textfield></td>
      <td>
      <s:text name="garuda.cbbdefaults.lable.country">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}country" ></s:textfield></td>
    </tr>
    <s:if test="#request.hideFields!=Yes">
    <tr>
      <td>
      <s:text name="garuda.cbbdefaults.lable.phone">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}phone" ></s:textfield></td>      
      <td>
      <s:text name="garuda.cbbdefaults.lable.fax">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}fax" ></s:textfield></td>      
    </tr>
    <tr>
      <td>
      <s:text name="garuda.cbbdefaults.lable.pager">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}pager" ></s:textfield></td>
      <td>
      <s:text name="garuda.cbbdefaults.lable.mobile">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}mobile" ></s:textfield></td>      
    </tr>
    <tr>
      <td>
      <s:text name="garuda.cbbdefaults.lable.email">
      </s:text>:</td>
      <td>
      <s:textfield name="%{#request.prefix}email" ></s:textfield></td>
      <td colspan="2"></td>
    </tr>
	</s:if>  
