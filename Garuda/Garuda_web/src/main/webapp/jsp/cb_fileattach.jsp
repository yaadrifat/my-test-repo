<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="java.io.File"%>
<%@page import="javax.swing.text.Document"%>
<%@page import="java.lang.reflect.Array"%>
<html>
<head>
<%
	String	contextpath1=request.getContextPath()+"/jsp/";
	String attCodeType = request.getParameter("attCodeType");
	String attCodeSubType = request.getParameter("attCodeSubType");
	String codeEntityType = request.getParameter("codeEntityType");
	String codeEntitySubType = request.getParameter("codeEntitySubType");
	request.setAttribute("attCodeType", attCodeType);
	request.setAttribute("attCodeSubType", attCodeSubType);
	request.setAttribute("codeEntityType", codeEntityType);
	request.setAttribute("codeEntitySubType", codeEntitySubType);
	String filePath=com.velos.eres.service.util.Configuration.ERES_HOME + "../server/eresearch/deploy/dcmsAttachment.xml";
	
	
%>
<link rel="stylesheet" type="text/css" href="<%=contextpath1%>styles/jquery-ui-1.8.7.custom.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath1%>styles/velosCDR.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath1%>js/jquery/themes/nmdp/common/forms.css" media="screen" />
<script type="text/javascript" src="<%=contextpath1%>js/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<%=contextpath1%>js/jquery/common/scripts.js"></script>
<script type="text/javascript" src="<%=contextpath1%>js/jquery/common/dcmsUtilities.js"></script>
<script type="text/javascript" src="<%=contextpath1%>js/jquery/ui/jquery-ui-1.8.7.custom.js"></script>

  <script>
  var $j = jQuery;
  var documentId ='<s:property value="attachmentPojo.timestamp"/>';
  
  var progressflag=false;
  var increasepercentflag=false;
  /* ********************** PROGRESSBAR ************************************** */
  function progressbarr(){
    progressflag=false;
    increasepercentflag=false;
  
  $j('#rightcheck').remove();   
  if(navigator.appName == 'Microsoft Internet Explorer')  
  $j('#showprogress').append('<div id="progressbar" style="width:287px; height:5px; border: solid 1px black; font-size:0px"></div>'); 
  else
  $j('#showprogress').append('<div id="progressbar" style="width:287px; height:3px; border: solid 1px black; font-size:0px"></div>');
  
  var speed=3;
  var status=Math.floor((Math.random()*10)+83);
    
  $(document).ready(function() {
    $j("#progressbar").progressbar();
	var value = 0;
	//var status=90;
var timer = setInterval (function ()
{
if(progressflag){
$("div#progressbar").progressbar ("value", value);
$j('#uploadstatus').html ("<p >"+value+"%</p>");
if(value  <= status){
value++;}
if(increasepercentflag){

status++;
value++;
}
}
if (value > 100){ 
clearInterval (timer);
$j('#progressbar').remove();
$j('p').remove();

$j('#uploadstatus').append('<img style="vertical-align: middle;" src="images/tick_mark.gif" border="0" id="rightcheck"/>');
}
}, speed);
	
  });

}

/* ************** progressbar code ended here ************************** */

if(documentId != "" && documentId!=null){
	parent.document.getElementById("attachId").value = documentId;
}
var vardcmsFlag=null;
var varpkDcmsFlag=null;
var insertUrl=null;
//var userName=null;
//var pass=null;
/* var dcmsXml = readXml1("dcmsAttachment.xml");
$(dcmsXml).find("dcms").each(function() {
	insertUrl=$(this).find("dcmsIpAddress").text();
	insertUrl=$j.trim(insertUrl);
	//userName=$(this).find("userName").text();
	//pass=$(this).find("password").text();
	
}); */
var DcmsSaveServletUrl = "/FileData-war/DcmsSaveServlet?";
var SignedServletUrl = "getSignedURL?docId=";
var FileStatusUrl = "/FileData-war/DcmsFileStatusServlet?";
var cryptoReq = "&CryptoReq=";
var testAppInitialUrl=null;
var dcmsInitialUrl=null;
var fileUploadFlag='<s:property value="#request.fileUpload"/>';
var ajaxUrl = ajaxUrlclosure();


//This method id called when user selects a file through browse button on html
//and clicks OK
function onFileSelect(elementId, formId,docFieldId,fileId) {
    insertUrl=parent.document.getElementById("dcmsIpAddress").value;
    testAppInitialUrl=insertUrl;
    dcmsInitialUrl=insertUrl;
   	 var handleSuccess = function(o) {
     var responseObj =  eval(o.responseText);
     var signedUrl = dcmsInitialUrl + DcmsSaveServletUrl+responseObj[0].signedUrl;
     var ajaxC = uploadClosure(fileId, elementId);
     ajaxC.startRequest(formId,signedUrl);
 };
 var handleFailure = function(o) {
	 var responseObj =  eval(o.responseText);
     alert('<s:text name="garuda.file.submit.noSignedUrl"/>');

 };
 var docId = document.getElementById(docFieldId).value;
 var isCryptReq = false;
 //ajaxUrl.startRequest("GET",testAppInitialUrl+SignedServletUrl+docId+cryptoReq+isCryptReq, handleSuccess, handleFailure, null);
 ajaxUrl.startRequest("GET",SignedServletUrl+docId+cryptoReq+isCryptReq, handleSuccess, handleFailure, null);
}
//This is a closure to send ajax post request, to a URL.to get signed url
function ajaxUrlclosure(){
 var successH = null;
 var failureH = null;
 var handleSuccess = function(o) {
     successH(o);
 };
 var handleFailure = function(o) {
     failureH(o);
 };
 var callback = {
     success:handleSuccess,
     failure:handleFailure,
     cache:false
 };

 return {
     startRequest:function(methodType, url, successHandler, failureHandler, param) {
         successH = successHandler;
         failureH = failureHandler;
         // Start the transaction.
         YAHOO.util.Connect.asyncRequest(methodType, url, callback, param);
     }
 };
}

//This is a closure to send file upload POST request.
function uploadClosure(fileID, statusKey){
 var uploadHandler = function(response) {
	$j("#fileUploadStatus").val(1);
	refreshDiv('getDcmsLog','dcmsLogDiv','attachdocument');
	getFileStatus();
     
 }
 var callback = {
     upload:uploadHandler
 };

 return {
     startRequest:function(formName,signedUrl) {
         YAHOO.util.Connect.setForm(formName,true);
         // Start the transaction.
         YAHOO.util.Connect.asyncRequest('POST', signedUrl,callback, null);
     }
 };
}

function fileStatus(response) {
    var responseObj =  eval(response);
    if(responseObj[0].status_code == 0){
        $j("#FileId1").val(responseObj[0].fileId);
    }
}


// This method sends a GET request to dcms server for verifying all document Ids status.
function getFileStatus() {
    var docId = document.getElementById('dcmsattachmentId').value;
    var handleSuccess = function(o) {
	   increasepercentflag=true;
	  //alert('increasepercentflag:'+increasepercentflag);
        var responseObj =  eval(o.responseText);
        var signedUrl = dcmsInitialUrl + FileStatusUrl+responseObj[0].signedUrl;
        YAHOO.util.Get.script(signedUrl, {
            onSuccess: gSuccess,
            scope    : this
        });
    };
    var handleFailure = function(o) {};
    var isCryptReq = false;
    ajaxUrl.startRequest("GET",SignedServletUrl+docId+cryptoReq+isCryptReq,
        handleSuccess, handleFailure, null);
}

function gSuccess(o){
    o.purge();  // immediately removes script node after executing it
	 parent.document.getElementById("uploadingFlag").value = "true";
}
  
	
	
    function trackFile(){
	           progressbarr();
	           progressflag=true;
		
	       /*
			*getting Dcms Attachment Id from sequence SQ_DCMS_FILE_UPLOAD
			*and refreshing div having 'dcmsattachmentId' component
			*/
				refreshDiv('getDcmsAttachmentId','fileAttachmentIdDiv','attachdocument');
	        	var attid=$j("#dcmsattachmentId").val();
	        //variable comes from velos constants
	       		vardcmsFlag=$j("#dcmsFlag").val();
	        //variable comes from Garuda_Code_List table
	        	varpkDcmsFlag=$j("#pkDcmsFlag").val();
	        	
	        //comparing both to check whether file uploads in DCMS server or in Velos database
	    	if(vardcmsFlag!=varpkDcmsFlag){
	    		document.forms["attachdocument"].submit();
	    	}
	    	else if(vardcmsFlag==varpkDcmsFlag){    
	        	//uploading file in DCMS server
				parent.document.getElementById("attachmentFlag").value = true;//in case when file selected and set attachement flag true show msg please select file will disappear
				parent.document.getElementById("uploadingFlag").value = "";//remove uploading value to blank 
	    		onFileSelect('fileUpload','attachdocument','dcmsattachmentId','FileId1');
	    	}
		
        
	}

   function showSubmit(){
    $j("#show").css('display','block');
    }
    
   function refreshDiv(url,divname,formId){
    	$j.ajax({
            type: "POST",
            url: url,
            async:false,
            data : $j("#"+formId).serialize(),
            success: function (result){
                var $response=$j(result);
                var errorpage = $response.find('#errorForm').html();
                if(errorpage != null &&  errorpage != "" ){
                	$j('#main').html(result);
                }else{
                	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
                }
            },
            error: function (request, status, error) {
            	alert("Error " + error);
                alert(request.responseText);
            }

    	});
    	

    }


  function changeUploadFlag(){
      
   	 var tempfilename = $("#fileUpload").val();
   	//$j("#fileName").val(tempfilename.substring(tempfilename.lastIndexOf("\\")+1));
   	 var index = tempfilename.lastIndexOf(".");
   	 if(index != -1){
       		 tempfilename = tempfilename.substring(index+1);
   	 }
	if(tempfilename=='pdf'||tempfilename=='PDF'||tempfilename=='jpg'||tempfilename=='JPG'||tempfilename=='jpeg'||tempfilename=='JPEG'||tempfilename=='doc'||tempfilename=='DOC' ||tempfilename=='docx'||tempfilename=='DOCX'){
		parent.document.getElementById("browseFlag").value = 1;
  	 	trackFile();
	}
	else{
		alert("<s:text name="garuda.cbu.fileattach.filetypealert"/>");
		parent.document.getElementById("attachmentFlag").value = "";
		 $j('#rightcheck').remove();		
		 $j('.fileinputs').html('<input type="file" name="fileUpload"  id="fileUpload"  onchange="changeUploadFlag();" style="position:relative;" size="44%"/ >');
		}   	
  }
  function fileAttachOnload(){
		 if(fileUploadFlag){
				
				var finalorder = parent.document.getElementById("saveFinalOrderAttachFlagId").value;
				//alert("finalorder:::"+finalorder);				
					if(finalorder!=null && finalorder==1){
						
						parent.saveCall1();
					}else{
						parent.saveCall();
						}
				
				}
		 }

</script>
</head>
<body onload="fileAttachOnload();">
<form id="attachdocument" method="POST" action="attachdocument"
	enctype="multipart/form-data" name="attachdocument">
	<s:hidden name="attachmentPojo.attachmentId"></s:hidden>
	<input type="hidden" name="attCodeType" value="<s:property value="#request.attCodeType"/>" />
	<input type="hidden" name="attCodeSubType" value="<s:property value="#request.attCodeSubType"/>" /> 
	<input type="hidden" name="codeEntityType" value="<s:property value="#request.codeEntityType"/>" /> 
	<input type="hidden" name="codeEntitySubType" value="<s:property value="#request.codeEntitySubType"/>" />  
	<s:hidden id="FileId1"></s:hidden>
	<s:hidden name="targetFolder" value="1=0"></s:hidden>
	<s:hidden name="UploadFlags" value="1#"></s:hidden>
	<s:hidden name="dcmsTask" value="INSERT"></s:hidden>
	<s:hidden id="fileUploadStatus"></s:hidden>
	<div class="maincontainer">
<fieldset>
	<table border="0" align="left" cellpadding="0" cellspacing="0" >
	<s:if test="#request.fileUpload==true">
		<!--<tr><td colspan="2">
							<div class="ui-state-highlight ui-corner-all"> 
							<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
							<strong>File Attach Successfully!</strong></p>
							</div></td></tr>	
		-->
	<tr>
			<td colspan="4" nowrap="nowrap" width="100%"><strong><s:text name="garuda.cbu.fileattach.uploadedFileName"/> <s:property value="#request.path" /> </strong></td>
    </tr>
	</s:if>
	<!--<tr class="add"> <td></td>
			<td > 
			 	  <button type="button" onclick="addMoreFiles('filetable')" >Add More Attachment</button> 
			  	
			</td>
		</tr>
		-->
		
	<tr height="30%" >
		<td width="25%;"><s:text name="garuda.unitreport.label.selectyourfile" /><span style="color: red;">*</span></td>		
		<td   id="uploadstatus"><td>
		<td nowrap="nowrap" width="75%;">
		<!--<label class="cabinet"> 
		<input type="file" name="fileUpload" class="file" id="fileUpload" onchange="changeUploadFlag();" style="position: relative;" />
		</label>
		
		--><div class="fileinputs" >
	    <input type="file" name="fileUpload"  id="fileUpload"  onchange="changeUploadFlag();" style="position:relative;" size="44%"/ >
	<!-- <div class="fakefile">
		<input type="text" style="vertical-align:top; height:25px;background-color:transparent;" disabled="disabled" size="41%" id="fileName"/>
		<img src="images/Browse_new.gif" alt="<s:text name="garuda.common.label.browse"/>" style="vertical-align: middle;"/>
	 </div> --> 
        </div> 
		
		<s:div id="fileAttachmentIdDiv">
		    <input type="hidden" name="timestamp" value="<s:property value="attachmentPojo.timestamp"/>" />
			<s:hidden id="dcmsattachmentId" name="dcmsattachmentId"></s:hidden>
			<s:hidden id="dcmsFlag" name="dcmsFlag"></s:hidden>
			<s:hidden name="pkDcmsFlag" id="pkDcmsFlag"></s:hidden>
		</s:div>
		<s:div id="dcmsLogDiv">
		</s:div>
		</td>
	</tr>
	<tr><td></td><td></td>
	<td colspan="4" id="showprogress" style="vertical-align:top;">
	       
	</td>
	</tr>
	<tr id="show" style="display: none;">
		<td></td>
		<td>
		<button type="button" onclick="javascript:trackFile();"><s:text name="garuda.cbu.fileattach.attachFiles"/></button>
		</td>
	</tr>
	</table></fieldset>
	</div>
</form>
</body>
</html>