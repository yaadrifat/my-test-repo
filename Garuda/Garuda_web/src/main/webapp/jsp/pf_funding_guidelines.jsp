<%@ taglib prefix="s"  uri="/struts-tags"%>

<div class="portlet-content">
							  	<table border="0" cellpadding="0" cellspacing="0" class="displaycdr" id="fundguideline" >
								
									<thead>
										<tr>
											<th><s:text name="garuda.fundingNMDP.label.fundingGuidlineId" /></th>
											<th><s:text name="garuda.fundingNMDP.label.fundingCategory" /></th>
											<th><s:text name="garuda.fundingNMDP.label.birthDate" /></th>
											<th><s:text name="garuda.fundingNMDP.label.fundingDescription" /></th>
											<th><s:text name="garuda.fundingNMDP.label.fundingStatus" /></th>
										</tr>
									</thead>
									<tbody>
										<s:if test="#request.editGuidlinesFlag!='true'">									
											<s:iterator value="fundingGuidelinespojoLst" status="row">
														<tr id="<s:property	value="%{#row.index}"/>" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')">
														    <td><s:property	value="fundingId" /></td>
											                <td><s:property	value="getCodeListDescById(fkFundCateg)" /></td>
											                <td><s:date name="fundingBdate"
																								id="datepicker1" format="MMM dd, yyyy" /> <s:property
																								value="%{datepicker1}" /></td>
											                <td><s:property	value="description" /></td>
											                <td><s:property	value="getCodeListDescById(fundingStatus)" /></td>               
											           </tr>
													</s:iterator>										
										</s:if>
										<s:if test="#request.editGuidlinesFlag == 'true'">								
												<s:iterator value="fundingGuidelinespojoLst" status="row">
												
															<tr id="<s:property	value="%{#row.index}"/>" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')">
															    <td><s:property	value="fundingId" /></td>
												                <td><s:property	value="getCodeListDescById(fkFundCateg)" /></td>
												                <td><s:date name="fundingBdate"	id="datepicker1" format="MMM dd, yyyy" /><s:property
																								value="%{datepicker1}" /></td>
												                <td><s:property	value="description" /></td>
												                <td><s:select name="fundingGuidelinespojoLst[%{#row.index}].fundingStatus" id="fundingGuidLinespojoLst[%{#row.index}].fundingStatus" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNDING_STATUS]"
																		listKey="pkCodeId" listValue="description" /></td>       
																		<s:hidden name="fundingGuidelinespojoLst[%{#row.index}].pkFundingGuidelin"/>
																		<s:hidden name="fundingGuidelinespojoLst[%{#row.index}].fundingId"/>
																		<s:hidden name="fundingGuidelinespojoLst[%{#row.index}].fkFundCateg"/>
																		<s:hidden name="fundingGuidelinespojoLst[%{#row.index}].fundingBdate"/>
																		<s:hidden name="fundingGuidelinespojoLst[%{#row.index}].description"/>
																		<s:hidden name="fundingGuidelinespojoLst[%{#row.index}].fkCbbId"/>       
												            </tr>
												</s:iterator>
												<tr>
												<td colspan="5" align="right">
													 <button type="button" id="saveEditGuidelines"  onclick="updateCBBGuidlines();"><s:text
													name="garuda.common.save" />
													</button>
													<button type="button" onclick="showSelectedCBBGuidlines('<s:property value="#request.cbbId"/>','0');"><s:text
													name="garuda.common.lable.cancel" />
													</button>
												</td>
												</tr>
										</s:if>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="5"></td>
										</tr>
									</tfoot>
								</table>
								<s:if test="#request.message !=null  && #request.message!=''">
											     <div id ="addeditfunding">
										 			<table class="displaycdr">
													 	<tr>
														 	<td align="right">
														 	<span style="color: red;"><s:property value="#request.message"/></span>
														 	</td>
													 	</tr>
										 			</table>
								 				</div>
											</s:if>
								<s:if test="#request.message ==null  || #request.message==''">
								 	<div id ="addeditfunding">
									 	<table class="displaycdr">
										 	<tr>
											 	<td align="right">
											 	<button type="button" onclick="editCBBGuidlines('<s:property value="#request.cbbId"/>','<s:property value="fundingGuidelinespojoLst.size()"/>');"><s:text
														name="garuda.funding.label.editStatus" />
														</button>
												<button type="button" onclick="addCBBGuidlines('<s:property value="#request.cbbId"/>','<s:property value="fundingActiveGuidelinespojoLst.size()"/>');"><s:text
														name="garuda.funding.label.addFundingGuidelines" />
														</button>
											 	</td>
										 	</tr>
									 	</table>
								 	</div>
								 </s:if>
					     </div>