<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<jsp:include page="cb_track_session_logging.jsp" />
<div id="loadWholeNotesDiv">
	<jsp:include page="modal/cb_note_list.jsp"></jsp:include>
</div>
     <s:if test="hasNewPermission(#request.vClnclNote)==true">
    <s:if test='(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)) '>
<button type="button" 
	onclick="fn_showModalForNotes('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','clinicalNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&finalReview=true','500','850','notesModalsDiv');"><s:text
name="garuda.cdrcbuview.label.button.addnotes" /></button>
</s:if>
<s:else>
	<button type="button"
onclick="fn_showModalForNotes('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','clinicalNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','500','850','notesModalsDiv');"><s:text
name="garuda.cdrcbuview.label.button.addnotes" /></button>
</s:else>
 </s:if>
 <script>
		 function fn_showModalForNotes(title,url,hight,width,id){
			 var patientId = $j('#patientId').val();
				var patient_data = '';
				if(patientId!="" && patientId != null && patientId != "undefined"){
					patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
				}
				title=title+patient_data;
				showModals(title,url,hight,width,id);
			}
		 </script>