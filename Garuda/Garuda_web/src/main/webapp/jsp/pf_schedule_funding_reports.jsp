<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>

<%
String scheduleFlag = (String)request.getParameter("scheduleFlag");
request.setAttribute("scheduleFlag",scheduleFlag);
%>
<script>

$j(".positive").numeric({ negative: false }, function() { alert("<s:text name="garuda.common.cbu.negativeValueAlert"/>"); this.value = ""; this.focus(); });
 
 function setNotificationDate(txtFldId,pickerId,val){
	 if( $j("#datepicker"+pickerId).val()==""|| $j("#datepicker"+pickerId).val()==null){}
	 else{
		 val=$j("#notificationBeforeId"+txtFldId).val();
		 if(val=="" || val== null){
			 $j("#notificgenafterDate"+txtFldId).hide(); 
		 }
		 else if(val<=99){
		 $j("#notificgenafterDate"+txtFldId).show();
		 var currentDate = new Date($j("#datepicker"+pickerId).val());
		 var numberOfDaysToAdd = parseInt(val)+1;
		 currentDate.setDate(currentDate.getDate() + numberOfDaysToAdd);
		 var toDayDate1 = currentDate.toUTCString().split(" "); 
		 toDayDate=toDayDate1[2]+' '+toDayDate1[1]+' ,'+toDayDate1[3];
		 $j("#notificationgenDate"+txtFldId).val(toDayDate);
		 }
		 else{
			 alert("<s:text name="garuda.common.cbu.numericValueRange"/>");
			 $j("#notificationBeforeId"+txtFldId).val("");
			 $j("#notificationgenDate"+txtFldId).val("");
		 }
	 }
	
}
/*  
  function enableNotify(fieldId){
	  $j('#notificationBeforeId'+fieldId+':input').removeAttr('readonly');
	 //$j("#notificationBeforeId0").romoveAttr("disabled");
	 
 }  */
/*  $j(function(){
		$j("#cbuFunding").validate({
		rules:{			
			"fundingSchedulePojo1[0].dateToGenerate":"required",
			"fundingSchedulePojo1[0].cbbRegistrationDate":"required"
		},
	 	messages:{
			 "fundingSchedulePojo1[0].dateToGenerate":"Please select date",
			"fundingSchedulePojo1[0].cbbRegistrationDate":"Please select date"
	 	}
	});

	});   */

$j("#cbbforSchedule").click(function(){
	$j("#additionalrepdateCol").show();
	$j("#addAdditionalRepDateBut").show();
	$j("#saveScheduleBut").show();
});

$j(function(){
	var today = new Date();
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();

$j("#datepicker19").datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
changeYear: true,prevText: '',nextText: ''});

$j("#datepicker12").datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
	changeYear: true,prevText: '',nextText: ''});
$j("#datepicker10").datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
	changeYear: true,prevText: '',nextText: ''});

$j("#datepicker11").datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true,prevText: '',nextText: ''});				


$j("#scheduleFundingRep").validate({
	rules:{
		"fundingSchedulePojo1[0].dateToGenerate":"required",
		"fundingSchedulePojo1[0].cbbRegistrationDate":"required",
		"fundingSchedulePojo1[1].dateToGenerate":"required",
		"fundingSchedulePojo1[1].cbbRegistrationDate":"required",
		"fundingSchedulePojo1[2].dateToGenerate":"required",
		"fundingSchedulePojo1[2].cbbRegistrationDate":"required",
		"fundingSchedulePojo1[3].dateToGenerate":"required",
		"fundingSchedulePojo1[3].cbbRegistrationDate":"required",
		"fundingSchedulePojo1[4].dateToGenerate":"required",
		"fundingSchedulePojo1[4].cbbRegistrationDate":"required"
		},
	 messages:{
		 "fundingSchedulePojo1[0].dateToGenerate":"<s:text name="garuda.common.validation.selectdate"/>",
		"fundingSchedulePojo1[0].cbbRegistrationDate":"<s:text name="garuda.common.validation.selectdate"/>",
		"fundingSchedulePojo1[1].dateToGenerate":"<s:text name="garuda.common.validation.selectdate"/>",
		"fundingSchedulePojo1[1].cbbRegistrationDate":"<s:text name="garuda.common.validation.selectdate"/>",
		"fundingSchedulePojo1[2].dateToGenerate":"<s:text name="garuda.common.validation.selectdate"/>",
		"fundingSchedulePojo1[2].cbbRegistrationDate":"<s:text name="garuda.common.validation.selectdate"/>",
		"fundingSchedulePojo1[3].dateToGenerate":"<s:text name="garuda.common.validation.selectdate"/>",
		"fundingSchedulePojo1[3].cbbRegistrationDate":"<s:text name="garuda.common.validation.selectdate"/>",
		"fundingSchedulePojo1[4].dateToGenerate":"<s:text name="garuda.common.validation.selectdate"/>",
		"fundingSchedulePojo1[4].cbbRegistrationDate":"<s:text name="garuda.common.validation.selectdate"/>"
	 	}
	});
	
});
	
var additionalRepDateCount = 0;
var n = 0;
var x = 0;

function addAdditionalReportDate(){
	
	$j('#additionalrepdateCol').each(function(){
		
		var today = new Date();
		var d = today.getDate();
		var m = today.getMonth();
		var y = today.getFullYear();
       
	   if(additionalRepDateCount==0){
        n = 2;
        x = 3;
        additionalRepDateCount = 0;
       }else{
        n = n+2;
        x = x+2;
       }
	   additionalRepDateCount++;
	  
	   var $td = $j(this);    	   
	   var tds = '';
	   tds += '<fieldset><table>';
	   tds += '<tr><td><s:text name="garuda.fundingNMDP.label.datetogenfunrep"/><span style="color: red;">*</span></td><td><s:date name="dateToGenerate'+additionalRepDateCount+'" id="datepicker'+n+'" format="MMM dd, yyyy" /><s:textfield readonly="true" onkeydown="cancelBack();"  name="fundingSchedulePojo1['+additionalRepDateCount+'].dateToGenerate" id="datepicker'+n+'"  value="%{dateToGenerate'+additionalRepDateCount+'}" onchange="setNotificationDate('+additionalRepDateCount+','+n+',this.value);"></s:textfield></td></tr>'
    	   +'<tr><td><s:text name="garuda.fundingNMDP.label.cbuRegDate"/><span style="color: red;">*</span></td><td><s:date name="cbbRegistrationDate'+additionalRepDateCount+'" id="datepicker'+x+'" format="MMM dd, yyyy" /><s:textfield readonly="true" onkeydown="cancelBack();"  name="fundingSchedulePojo1['+additionalRepDateCount+'].cbbRegistrationDate" id="datepicker'+x+'"  value="%{cbbRegistrationDate'+additionalRepDateCount+'}"></s:textfield></td></tr>'
    	   +'<tr><td><s:text name="garuda.fundingNMDP.label.sendNotificationafterdays"/></td><td><s:textfield name="fundingSchedulePojo1['+additionalRepDateCount+'].notificationBefore" cssClass="positive" id="notificationBeforeId'+additionalRepDateCount+'" onchange="setNotificationDate('+additionalRepDateCount+','+n+',this.value);"></s:textfield><s:text name="garuda.fundingNMDP.label.sendNotificationafterdays1"/></td></tr>'
		   +'<tr id="notificgenafterDate'+additionalRepDateCount+'" style="display: none"><td><s:text name="garuda.fundingNMDP.label.notificgenafterDate"></s:text></td><td><s:textfield value="%{notificationgenDate}" id="notificationgenDate'+additionalRepDateCount+'"/></td></tr>';	   
    tds += '</table></fieldset>';
    	   $j(this).append(tds);   
    $j("#datepicker"+n).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
    			 changeYear: true,prevText: '',nextText: ''});
    $j("#datepicker"+x).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		 changeYear: true,prevText: '',nextText: ''});
   });	
	 if(additionalRepDateCount==4){
		  $j("#addAdditionalRepDate").attr("disabled","disabled");
	     } 
 }
 
 function saveAdditionalReportDate(){
	 if($j("#scheduleFundingRep").valid()){
	 loadDivWithFormSubmit('addFundingReportSchedule','fundingReportScheduleds','scheduleFundingRep');
	 $j("#scheduleFundingReports").html("");
	 	var $jtabs = $j('#tabs').tabs();
	 	$jtabs.tabs('select', 2);
	 	fundingrptsPaginateTable();
 	}
 }
 function editScheduledFundingRepMeth(scheduledFundingId){
	 loadPageByGetRequset('editScheduledFundingRep?scheduledFundingId='+scheduledFundingId,'scheduleFundingReports');
		var $jtabs = $j('#tabs').tabs();
	 	$jtabs.tabs('select', 1);
		$j("#additionalrepdateCol").show(); 
		$j("#addAdditionalRepDateBut").hide();
		$j("#saveScheduleBut").show();
		//$j("#cbbforSchedule").attr("disabled","disabled");
	}
 function fundingrptsPaginateTable(){
		$j('#fundingrptscheduled').dataTable({
			"oLanguage": {
				"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
			},
			"iDisplayLength":100,
			"bRetrieve": true,
			"bDestroy" :true
			
		});
		 $j('#fundingrptscheduled_info').hide();
			$j('#fundingrptscheduled_paginate').hide();
			$j('#fundingrptscheduled_length').empty();
			$j('#fundingrptscheduled_length').replaceWith('Show <select name="showsRow1" id="showsRow1" style="width:auto; height:25px;" ><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> Entries');
			if($j('#entries1').val()!=null || $j('#entries1').val()!=""){
				$j('#showsRow1').val($j('#entries1').val());
				}
			$j('#showsRow1').change(function(){
				paginationFooter(0,"getLstFundingReportSchedule,showsRow1,temp,notrequired,fundingReportScheduleds,fundingrptsPaginateTable");
			});
	}
</script>
<s:form id="scheduleFundingRep" action="" method="post">
				<s:if test="#request.scheduleFlag=='true' || #request.editscheduleFlag=='true'">
				<div id="scheduleFundingReport">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
		          		<tr><td colspan="3"><b><s:text name="garuda.fundingNMDP.label.Secbbfunrepgeneration"/></b></td></tr>
		          		<tr>
			          		<td valign="top" align="left" width="25%" id="cbbLstForsch">
			          			<s:if test="#request.cbbAvailForSchLst.size()>0">
			          				<s:select id="cbbforSchedule" list="#request.cbbAvailForSchLst" name="cbbforSchedule" cssStyle="height:150px;width:250px;"  listKey="siteId"  listValue="siteName" multiple="true" ></s:select>
			          			</s:if>
			          			<s:else>
			          				<s:text name="garuda.funding.message.noGuidelinesDefined"/>
			          			</s:else>
			          		</td>
			          		<td id="additionalrepdateCol" style="display: none" align="right" width="50%">
			          				<fieldset>
				          				<table id="addReportDateTable">
				          					<tr>
				          						<td>
				          							<s:text name="garuda.fundingNMDP.label.datetogenfunrep"/><span style="color: red;">*</span>
				          						</td>
				          						<td>
				          							<s:date name="dateToGenerateJ" id="datepicker10" format="MMM dd, yyyy" />
													<s:textfield readonly="true" onkeydown="cancelBack();"  name="fundingSchedulePojo1[0].dateToGenerate"  id="datepicker19"  value="%{datepicker10}" onchange="setNotificationDate(0,19,this.value);"></s:textfield>
				          						</td>
				          					</tr>
				          					<tr>
				          						<td>
				          							<s:text name="garuda.fundingNMDP.label.cbuRegDate"/><span style="color: red;">*</span>
				          						</td>
				          						<td>
				          							<s:date name="cbbRegistrationDateJ" id="datepicker11" format="MMM dd, yyyy" />
													<s:textfield readonly="true" onkeydown="cancelBack();"  name="fundingSchedulePojo1[0].cbbRegistrationDate" id="datepicker12"  value="%{datepicker11}" ></s:textfield>
				          						</td>
				          					</tr>
				          					<tr>
				          						<td>
				          							<s:text name="garuda.fundingNMDP.label.sendNotificationafterdays"/>
				          						</td>
				          						<td>
				          							<s:textfield name="fundingSchedulePojo1[0].notificationBefore"  cssClass="positive" id="notificationBeforeId0" onkeyup="setNotificationDate(0,19,this.value);"></s:textfield><s:text name="garuda.fundingNMDP.label.sendNotificationafterdays1"/>
				          						</td>
				          					</tr>
				          					<tr id="notificgenafterDate0" style="display: none">
				          						<td>
				          							<s:text name="garuda.fundingNMDP.label.notificgenafterDate"></s:text>
				          						</td>
				          						<td>
				          							<s:textfield value="%{notificationgenDate}" readonly="true" onkeydown="cancelBack();"  id="notificationgenDate0"/>
				          						</td>
				          					</tr>
				          				</table>
			          				</fieldset>
			          		</td>
			          		<td id="addAdditionalRepDateBut" valign="bottom" align="right" width="25%" style="display: none">
							            <button type="button" id="addAdditionalRepDate" onclick="addAdditionalReportDate();"><s:text name="garuda.fundingNMDP.button.addAdditionalRepDateButton"></s:text></button>
							</td>
		          		</tr>
		          		<tr>
		          			<td colspan="2"/>
		          			<td id="saveScheduleBut" style="display: none"><table><tr>
		          			<td align="right">
		          				<button type="button" id="saveAdditionalRepDate" onclick="saveAdditionalReportDate();"><s:text name="garuda.common.save"></s:text></button>
							</td>
		          			<td>
		          				<button type="button" id="cancelAdd" onclick="getLstScheduleFundingReport();"><s:text name="garuda.common.lable.cancel"></s:text></button>
		          			</td></tr></table>
		          			</td>
		          		</tr>
		          	</table>
		          	<s:if test="#request.editscheduleFlag=='true'"><s:hidden name="fundingSchedulePojo1[0].pkFundingSchedule" id="fundingSchedulePojo1[0].pkFundingSchedule"></s:hidden></s:if>
		          </div></s:if>
		          <s:else>
		          <div id="fundingReportScheduled">
		          	<input type="hidden" name="entries1" id="entries1" value=<%=paginateSearch.getiShowRows() %> />
		          	<table border="0" cellpadding="0" cellspacing="0" width="100%" class="displaycdr" id="fundingrptscheduled" >
						<thead>
							<tr>
								<th><s:text name="garuda.cbbdefaults.lable.cbbid" /></th>
								<th><s:text name="garuda.cbbdefaults.lable.cbbname" /></th>
								<th><s:text name="garuda.fundingNMDP.label.dateScheduled" /></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						   <s:iterator value="scheduledFundingLst" status="row">
														<tr id="<s:property	value="%{#row.index}"/>" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')">
														    <td><s:property	value="getSiteInfoBySiteId('SITE_ID',fkCbbId)" /></td>
											                <td><s:property	value="getSiteInfoBySiteId('SITE_NAME',fkCbbId)" /></td>
											                <td><s:date name="dateToGenerate"
																								id="datepicker21" format="MMM dd, yyyy" /> <s:property
																								value="%{datepicker21}" /></td>              
											           		<td><button type="button" id="editScheduledFundingRep" onclick="editScheduledFundingRepMeth('<s:property value="pkFundingSchedule"/>');"><s:text name="garuda.common.lable.edit"></s:text></button></td>
											           </tr>
							</s:iterator>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
												   <td>
												    <jsp:include page="paginationFooter.jsp">
													  	<jsp:param value="fundingReportScheduleds" name="divName"/>
													  	<jsp:param value="notrequired" name="formName"/>
													  	<jsp:param value="showsRow1" name="showsRecordId"/>
													  	<jsp:param value="getLstFundingReportSchedule" name="url"/>
													  	<jsp:param value="temp" name="cbuid"/>
													  	<jsp:param value="workflow" name="paginateSearch"/>
													  	<jsp:param value="fundingrptsPaginateTable" name="bodyOnloadFn"/>
													 </jsp:include>
									               </td>
									               <td colspan="3"></td>
						 	</tr>
						</tfoot>	
				 </table>
		          </div></s:else>
		          </s:form>