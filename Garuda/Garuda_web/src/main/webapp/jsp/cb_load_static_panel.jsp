<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<!-- <table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td>-->				
<script>
function displaydatesfornscbu(){
	var todate=new Date();
	var dat = "";
	if($j("#shippedonfornscbu").val()!=null && $j("#shippedonfornscbu").val()!=""){
		dat=$j.datepicker.parseDate("M dd, yy",$j("#shippedonfornscbu").val());
	}
		var cordStatus='<s:property value="cdrCbuPojo.fkCordCbuStatus"/>';
		var cordStatus_IN='<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)"/>';
		var orderType='<s:property value="getCodeListDescById(orderPojo.orderType)"/>';

		if(cordStatus==cordStatus_IN && orderType=='OR'){
			$j("#patientinfoheaderdiv").find("span").each(function(){
		  		$j(this).text('<s:text name="garuda.cbu.label.infused"></s:text>:'+$j("#patientId").val());
			 });
			$j("#shippedonlabel").find("span").each(function(){
		  		$j(this).text('<s:text	name="garuda.minimumcriteria.label.shippedon" />:');
			 });
			$j("#shippedonvalue").find("span").each(function(){
		  		$j(this).text($j("#shippedonfornscbu").val());
			 });
			$j("#infusedonlabel").find("span").each(function(){
		  		$j(this).text('<s:text	name="garuda.minimumcriteria.label.reportinfu" />:');
			 });
			$j("#infusedonvalue").find("span").each(function(){
		  		$j(this).text($j("#infusedonfornscbu").val());
			 });
		}
		else if(dat!=null && dat!='' && dat!='undefined' && dat<todate && orderType=='OR'){
			$j("#patientinfoheaderdiv").find("span").each(function(){
		  		$j(this).text('<s:text name="garuda.cbu.label.cordShipped"></s:text>:'+$j("#patientId").val());
			 });
			$j("#shippedonlabel").find("span").each(function(){
		  		$j(this).text('<s:text	name="garuda.minimumcriteria.label.shippedon" />:');
			 });
			$j("#shippedonvalue").find("span").each(function(){
		  		$j(this).text($j("#shippedonfornscbu").val());
			 });
			
		}else{
			$j("#patientinfoheaderdiv").find("span").each(function(){
		  		$j(this).text('<s:text name="garuda.cbu.label.patientActive"></s:text>:'+$j("#patientId").val());
			 });
		}
	
}
function flaggedItemsforPrint(){
	var divIdd="flaggeddivparent";
	var cbuRegID='<s:property value="cdrCbuPojo.registryId"/>';
	var divTitle='FlaggedItems';
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	showModal('<div id="notesPopup"><div style="float:left"><s:text name="garuda.cbufinalreview.label.flaggedItemsSummarty"></s:text>:<s:property value="cdrCbuPojo.registryId"/>'+patient_data+'</div><div style="float:right"><span class="ui-icon ui-icon-print" onclick="printQuicklinksWidget(\''+divIdd+'\',\''+cbuRegID+'\',\''+divTitle+'\');"></span></div></div>','getcbuFlaggedItems?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=flagged','400','750');
	$j('#ui-dialog-title-modelPopup1').width('98%');
}
function notesListforPrint(fcr){
	var divIdd="clinicalNoteFilterDiv";
	var cbuRegID='<s:property value="cdrCbuPojo.registryId"/>';
	var divTitle='Notes';
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	var orderType=$j('#orderType').val();
	var orderId='<s:property value="orderPojo.pk_OrderId"/>';
	orderId=$j.trim(orderId);
	orderType=$j.trim(orderType);
	if(orderType=="CT"){
	    
		if($j('#sampleAtLab').val()=='Y'){
			orderType='<s:text name="garuda.orOrderDetail.portletname.ctlaborder"/>';
		}
		else{
			orderType='<s:text name="garuda.orOrderDetail.portletname.ctshiporder"/>';
		}
	}
	if(fcr=='fcrcomplete'){
		showModal('<div id="notesPopup"><div style="float:left"><s:text name="garuda.header.label.clinicalNote" />:<s:property value="cdrCbuPojo.registryId"/>'+patient_data+'</div><div style="float:right"><span class="ui-icon ui-icon-print" onclick="printQuicklinksWidget(\''+divIdd+'\',\''+cbuRegID+'\',\''+divTitle+'\');"></span></div></div>','quickNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&finalReview=true'+'&orderId='+orderId+'&orderType='+orderType+'&requestDate='+$j('#requestedDate').val(),'500','850');
	}
	else{
	//showModal('<s:text name="garuda.header.label.clinicalNote"/> <s:property value="cdrCbuPojo.registryId"/>'+spacetext+'<div style="float:right"><span class="ui-icon ui-icon-print" onclick="clickheretoprint(\''+divIdd+'\',\''+cbuRegID+'\');"></span></div>','quickNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&siteName = <s:property value="cdrCbuPojo.site.siteName"/>','500','850');
		showModal('<div id="notesPopup"><div style="float:left"><s:text name="garuda.header.label.clinicalNote"/>:<s:property value="cdrCbuPojo.registryId"/>'+patient_data+'</div><div style="float:right"><span class="ui-icon ui-icon-print" onclick="printQuicklinksWidget(\''+divIdd+'\',\''+cbuRegID+'\',\''+divTitle+'\');"></span></div></div>','quickNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&siteName = <s:property value="cdrCbuPojo.site.siteName" escapeJavaScript="true"/>&orderId='+orderId+'&orderType='+orderType,'500','850');
	}
	$j('#ui-dialog-title-modelPopup1').width('98%');
}
$j(function() {
	displaydatesfornscbu();
});

</script>
<input type="hidden" id="orderType" value='<s:property value="getCodeListDescById(orderPojo.orderType)"/>'/>
<s:hidden name="cdrCbuPojo.numberOnCbuBag" id="numberOnCbuBag"></s:hidden>
<s:hidden id="cordCbuStatus" name="cdrCbuPojo.cordCbuStatuscode"></s:hidden>
<s:hidden id="executeAutoDeferFlagID" />
<div class="viewcbutoppanel">
<s:iterator value="patientInfoLst" var="rowVal">
	<s:if test="%{#rowVal[5]!=null}">
	<s:hidden name="patientId" id="patientId" value="%{#rowVal[5]}"></s:hidden>
	</s:if>
</s:iterator>
			<div class="colheading">			
				<table>
					<tr>
						<!--<td align="left">
							<s:if test="orderId==null || orderId==''">
							<span><s:text name="garuda.cdrcbuview.label.detailresult.cbu_information" /><s:property value="cdrCbuPojo.registryId"/></span>
							</s:if>
						</td>
						--><s:if test="cbuid!=null && cbuid!='' && orderId!=null && orderId!=''">
						<td align="right">
							<button type="button" onclick="getsearchResultsBack();">
							<s:text name="garuda.cdrcbuview.label.button.bactosearchres" /></button>
						</td>
						</s:if>
					</tr>
				</table>
			</div>
			<div class="columnDiv" style="width: 40%">
			<div class="outterDiv">
			<s:if test="hasViewPermission(#request.updateCBUInfoS)==true">  
			<div class="colheading">
				<span><s:text name="garuda.cdrcbuview.label.cbu_information" /></span>
			</div>
			
			<div class="innerDiv">
			<table cellpadding="0" cellspacing="0" border="0" align="center">
				<tr>
					<td align="left" width="31%"><s:text name="garuda.cdrcbuview.label.registry_id"/>:
						<!--<s:if test="#request.registryIdOnBag==true">
						   <span style="color: red;">(<s:text name="garuda.cdrcbuview.label.id_on_bag"/>)</span>
						</s:if>-->
						</td>
					<td align="left" width="26%">
					<s:hidden id="cburegIdHidden" value="%{cdrCbuPojo.registryId}"/>
					<s:if test="orderId!=null && orderId!=''"><a href="cordinfo?pkcordId=<s:property value="cdrCbuPojo.cordID"/>"><s:property value="cdrCbuPojo.registryId" /></a></s:if>
					<s:else><s:property value="cdrCbuPojo.registryId" /></s:else>
					</td>
					<td>&nbsp;</td>
					<td align="left" width="20%"><s:text
					name="garuda.cdrcbuview.label.collection_date" />:</td>
					<td align="left" width="30%">
					<s:if test="%{cdrCbuPojo.specimen.specCollDate != null }">
						<s:text name="collection.date">
							<s:param name="value" value="cdrCbuPojo.specimen.specCollDate" />
						</s:text>
					</s:if>
					</td>
				</tr>
				<tr>
					<%-- <td align="left"><s:text
						name="garuda.cdrcbuview.label.isbt_din" />:
					   <s:if test="#request.isbtDinOnBag==true">
						   <span style="color: red;">(ID On Bag)</span>
					   </s:if>		
					</td>
					<td align="left"><s:property
						value="cdrCbuPojo.cordIsbiDinCode" /></td>
					<td>&nbsp</td> --%>
				<s:if test="%{cdrCbuPojo.specimen.specCollDate == null }">
					<td align="left"><s:text name="garuda.cbuentry.label.birthdate" />:</td>
					<td align="left">
	             	<s:date name="CdrCbuPojo.birthDate" id="datepickerbbb"	format="MMM dd, yyyy" /> <s:property value="%{datepickerbbb}" />
					</td>
					<td>&nbsp;</td>
					</s:if>
					<td align="left"><s:text
						name="garuda.cbbdefaults.lable.cbb"></s:text>:</td>
					<td align="left"><a href="#" onclick="showCbbDetails('<s:property value='cdrCbuPojo.site.siteId' />');"  onmouseover="return overlib('<strong><s:property value="cdrCbuPojo.site.siteName" escapeJavaScript="true" /></strong>',CAPTION,'CBB Name');" onmouseout="return nd();"><s:property
						value="cdrCbuPojo.site.siteIdentifier"/></a></td>
				</tr>
				<tr>
					
					<td width="31%"><%--<s:text name="garuda.cbuentry.label.localcbuid"/>:--%>
					     <s:text name="garuda.cdrcbuview.label.id_on_bag"/>:
				     </td>
				  	<td width="26%">
				  		<s:hidden id="idonBagHidden" value="%{cdrCbuPojo.numberOnCbuBag}"/>
				  		<s:property value="cdrCbuPojo.numberOnCbuBag"/>
				  	</td>
				  	<!-- <td align="left"><s:text
						name="garuda.cdrcbuview.label.isbt_product_code"></s:text>:</td>
					<td align="left"><s:property
						value="cdrCbuPojo.cordIsitProductCode" /></td> -->
					<td>&nbsp;</td> 
					 <!-- <td align="left"><s:text
						name="garuda.cdrcbuview.label.tnc_frozen"></s:text>:</td>
					<td align="left"><s:property value="cdrCbuPojo.tncFrozen" /></td> -->
					<td align="left"><s:text name="garuda.cbuentry.label.tncfrozenpostproc"/>:</td>
				  	<td>
				  	<s:if test="#request.tncFrozenVal!=null && #request.tncFrozenVal!=''">
				  	   <s:property value="#request.tncFrozenVal"/> &nbsp;<s:text name="garuda.minimumcriteria.tncunit"></s:text>
				  	</s:if>
				  
				  	<%-- <s:if test="#request.tncFrozenVal==null && #request.tncFrozenVal=='' && #request.tncFrozenValNxt!=null && #request.tncFrozenValNxt!='' ">
				  	 <s:property value="#request.tncFrozenValNxt"/>/<s:property value="cdrCbuPojo.frozenPatWt"/>
				  	</s:if> --%>
				  	</td>
				</tr>
				<tr>
				   <td width="31%"><s:text name="garuda.cbuentry.label.localcbuid"/>:
				     </td>
				  	<td width="26%"><s:property value="cdrCbuPojo.localCbuId"/>
				  	</td>
					<td align="left" colspan="4"></td>
				</tr>
			</table>
			</div>
		  </s:if>
			</div>
		
			<div id="indshowdiv">
			<s:if test='cdrCbuPojo.cordnmdpstatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@AC) || cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)'>
			<s:if test="patientInfoLst!=null && patientInfoLst.size()>0">
			<s:iterator value="patientInfoLst" var="rowVal">
			<s:if test="%{(#rowVal[9]!=null && #rowVal[9]!='') || (#rowVal[8]!=null && #rowVal[8]!='')}">
			<div class="outterDiv">			
			<div class="colheading">
				<span><s:text name="garuda.cbuentry.label.ind" /></span>
			</div>
			<div class="innerDiv">
			<table>
				<tr>
					<td>
						<s:text name="garuda.cbuentry.label.ind_number" />:<s:property
							value="%{#rowVal[9]}" />
						
					</td>
					<!-- <td rowspan="2" valign="middle">
					<s:if test="hasEditPermission(#request.updateINDstat)==true">
					<a class="actionbutton" href="#" name="eligibleEdit" id="eligibleEdit" 
					onclick="return false;showModal('Update IND','updateINDstatus?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','370','450');">
					<span><s:text name="garuda.common.lable.edit"></s:text></span>
					</a>
					<input type="button" 
						value="" 
						onclick="">
						</input>
					</s:if>
					</td>
				</tr>
				<tr>-->
					<td>
						<s:text name="garuda.cbuentry.label.ind_sponsor" />:<s:property
							value="%{#rowVal[8]}" />
					</td>
				</tr>
			</table>
			</div>
			</div>
			</s:if>
			</s:iterator>
			</s:if>
			</s:if>
			</div>
			</div>
			
			<div class="columnDiv" style="width: 20%">
			<s:if test="hasViewPermission(#request.updateEligStat)==true">	
			
			<div id="eligibilitydiv" class="outterDiv">
			
			<div class="colheading">
				<span><s:text name="garuda.cdrcbuview.label.eligibility_determination" /></span>
			</div>
				<div id="eligibility" class="innerDiv">
				<table cellpadding="0" cellspacing="0" border="0" align="center">
					<tr>
					  <s:if test="CdrCbuPojo.fkCordCbuEligible!=null && CdrCbuPojo.fkCordCbuEligible!=''">
							<s:if
								test="CdrCbuPojo.fkCordCbuEligible==CdrCbuPojo.fkCordCbuEligibleReason">
								<td>
								<s:property value="CdrCbuPojo.eligibilitydes" /></td>
							</s:if>
							<s:else>
								<td onmouseover="return overlib('<strong><s:iterator value="getMultipleIneligibleReasons(CdrCbuPojo.fkCordCbuEligibleReason,null,0)" var="colVal" status="val"><s:property value="getCodeListDescById(#colVal)" /><br></s:iterator></strong>',CAPTION,'Reason For <s:property value="CdrCbuPojo.eligibilitydes" />');" onmouseout="return nd();">
								<s:property value="CdrCbuPojo.eligibilitydes"  />
								 </td>
							</s:else>
						</s:if>
						<s:if test="CdrCbuPojo.fkCordCbuEligible==null || CdrCbuPojo.fkCordCbuEligible==''">
						      <td>
						          <s:text name="garuda.common.lable.notyetdetermined" />
						      </td>						
						</s:if>
					<!-- </tr>
					<tr>-->
						<td align="right">
							<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
  							</s:if>
  							<s:else>
							<s:if test='cbuFinalReviewPojo.eligibleConfirmFlag == 1 && cbuFinalReviewPojo.licensureConfirmFlag == 1 && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y"'>
								<s:if test="hasEditPermission(#request.updateEligStat)==true">
									<a href="#" name="licenseEdit"  
										onclick="getShowModal('<s:text name="garuda.cbuentry.label.viewFinalEligible.header"/>:<s:property value="cdrCbuPojo.registryId"/>','viewFinalEligibility?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal','400','800');">
										<span><s:text name="garuda.cbuentry.label.viewFinalEligible"/></span>
									</a>
								</s:if>
								<!-- <input type="button" name="licenseEdit" value="<s:text name="garuda.cbuentry.label.viewFinalEligible"/>"
								onclick="showModal('<s:text name="garuda.cbuentry.label.viewFinalEligible"/>','viewFinalEligibility?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal','400','500');">
								-->		
											
							</s:if>
							<s:else>
								<s:if test="hasEditPermission(#request.updateEligStat)==true">
									<a class="actionbutton" href="#" name="eligibleEdit"  
										onclick="getShowModal('<s:text name="garuda.cdrcbuview.label.eligibility_determination"/>'+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','updateEligiblestatus?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_ELIGIBILITY" />&licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal','390','800');">
										<span><s:text name="garuda.common.lable.edit"/></span>
									</a>
								<!-- <input type="button" name="eligibleEdit"
									value="Edit"
									onclick="showModal('Eligibility Determination','updateEligiblestatus?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal','390','786');">
								-->
								</s:if>
							</s:else>
							</s:else>
						</td>
					</tr>
				</table>
				</div>
			</div>
				</s:if>
			
			<s:if test="hasViewPermission(#request.updateLicStat)==true">
			<div id="licensure_status" class="outterDiv">
			<input type="hidden" value="<s:property value='getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON)'/>" id="staticunLicensedPk"/>
			<s:hidden name="cdrCbuPojo.cbuLicStatus" id="staticlicenseid"></s:hidden>
			<div class="colheading">
				<span><s:text name="garuda.cdrcbuview.label.licensure_status" /></span>
			</div>
			<div id="licence" class="innerDiv">
			<table>
				<tr>
				<s:if
							test="cdrCbuPojo.cbuLicStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE) ||cdrCbuPojo.fkCordCbuUnlicenseReason==null || cdrCbuPojo.cbuLicStatus==null || cdrCbuPojo.cbuLicStatus<=0 || cdrCbuPojo.cbuLicStatus==''">
					<td >
								<s:property value="cdrCbuPojo.license" />
					</td>
							
				</s:if>
				<s:else>
					<s:if test="%{isTasksRequired(cdrCbuPojo.cordID)==false}">
					<td onmouseover="return overlib('<strong><s:iterator value="getMultipleIneligibleReasons(CdrCbuPojo.fkCordCbuUnlicenseReason,null,0)" var="colVal" status="val"><s:property value="getCodeListDescById(#colVal)" /><br></s:iterator></strong>',CAPTION,'Reason For <s:property value="cdrCbuPojo.license" />');" onmouseout="return nd();">
								<s:property value="cdrCbuPojo.license" />
					</td>
					</s:if>
					<s:else>
					<td>
								<s:property value="cdrCbuPojo.license" />
					</td>
					</s:else>
					
				</s:else>			
					<td>
					<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
					</s:if>
					<s:else>
					<s:if test='cbuFinalReviewPojo.eligibleConfirmFlag == 1 && cbuFinalReviewPojo.licensureConfirmFlag == 1 && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y"'>
						
  					
  						<s:if test="hasEditPermission(#request.updateLicStat)==true">
						<a href="#" name="licenseEdit"  
   							onclick="getShowModal('<s:text name="garuda.cbuentry.label.viewFinalLic"/>:<s:property value="cdrCbuPojo.registryId"/>','viewFinalLicense?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal','400','700');">
								<span><s:text name="garuda.cbuentry.label.viewFinalLicensure"/></span>
						</a>
						</s:if>
					
						<!-- <input type="button" name="licenseEdit" value="<s:text name="garuda.cbuentry.label.viewFinalLic"/>"
							onclick="showModal('<s:text name="garuda.cbuentry.label.viewFinalLic"/>','viewFinalLicense?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal','400','500');">					
						-->
					</s:if>
					<s:else>
					<s:if test="hasEditPermission(#request.updateLicStat)==true">
						<a class="actionbutton" href="#" name="licenseEdit"  
							onclick="getShowModal('<s:text name="garuda.message.modal.licenceupdatestatus"/> '+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','updateStatusLicense?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_LICENSE" />&licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal','400','700');">
								<span><s:text name="garuda.common.lable.edit"/></span>
						</a>
					<!-- <input type="button" name="licenseEdit" value="Edit"
						onclick="showModal('Update Licensure Status','updateStatusLicense?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal','400','500');">
					
					-->
					</s:if></s:else>
					</s:else>
					</td>
				</tr>
			</table>
			</div>
			</div>
			</s:if>			
			</div>	
			
			<div style="width: 40%;float: left;" id="cbustatdiv">
			<div class="columnDiv" style="width: 60%">
			<s:if test="hasViewPermission(#request.updateCBUstatus)==true">
			<div class="outterDiv">
			<div class="colheading">
				<span><s:text name="garuda.cbuentry.label.cbu_status" /></span>
			</div>
			<div class="innerDiv">
			<table>
				<tr>
						<td align="left" width="30%">
							<s:property value="cdrCbuPojo.cordnmdbStatusDesc"/>
						</td>
						<s:if test="isCdrUser==true">
						<td align="left" width="40%">
							<span><s:text name="garuda.pendingorderpage.label.reason"/>:</span>
							<span id="cbustatusdesc"><s:property value="cdrCbuPojo.cordCbuStatusDesc"/>
								<s:if test="cdrCbuPojo.cordCbuStatuscode!=null">
									(<s:property value="cdrCbuPojo.cordCbuStatuscode"/>)
								</s:if>
							</span>
						</td>
						<s:if test="#request.iscbbsameOrg==true && (( cdrCbuPojo.fkCordCbuStatus!=getCbuStatusPkByCode('DD') && cdrCbuPojo.fkCordCbuStatus!=getCbuStatusPkByCode('OT') ) || hasEditPermission(#request.overRideDeferred)==true) && cdrCbuPojo.fkCordCbuStatus!=getCbuStatusPkByCode('SH') && cdrCbuPojo.fkCordCbuStatus!=getCbuStatusPkByCode('IN')">
						<s:if test="hasEditPermission(#request.updateCBUstatus)==true && cdrCbuPojo.site.isCDRUser()==true">
							<td align="right" width="30%">
							  
							<a class="actionbutton" href="#" name="eligibleEdit"  id="cbustausEdit"
								onclick="showcbustatus(<s:property value="cdrCbuPojo.cordID" />,'','false')">
									<span><s:text name="garuda.common.lable.edit"/></span>
							</a>
							  
							<!-- 
							<input type="button" name="eligibleEdit" value="Edit"
							onclick="showModal('Update CBU Status','updateCbuStatus?licenceUpdate=False&cbustatusin=IS_LOCAL_REG&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','450','600');">
							-->
							</td>
						</s:if>
						</s:if>
						</s:if>
				</tr>
			</table>
			</div>
			</div>
			</s:if>
			<s:if test='cdrCbuPojo.cordnmdpstatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@AC) || cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)'>
			<s:if test="patientInfoLst!=null && patientInfoLst.size()>0">
			<div class="outterDiv">
			<div class="colheading">
				<span><s:text name="garuda.cdrcbuview.label.patient_information" /></span>
			</div>
			<div class="innerDiv">
			
				<s:iterator value="patientInfoLst" var="rowVal">
				<s:if test="cdrCbuPojo.site.isCDRUser()==true">
				<s:if test='%{#rowVal[0]!=null && getCodeListDescById(orderPojo.orderType)=="OR" && orderPojo.resolByTc==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN,@com.velos.ordercomponent.util.VelosGarudaConstants@RESOLUTION)}'>
						<strong><s:text name="garuda.cbu.label.infused"></s:text>:<s:if test="%{#rowVal[5]!=null}"><s:property value="%{#rowVal[5]}"/></s:if></strong>
					</s:if>
					<s:elseif test='%{#rowVal[7]!=null && getCodeListDescById(orderPojo.orderType)=="OR"}'>
						<strong><s:text name="garuda.cbu.label.cordShipped"></s:text>:<s:if test="%{#rowVal[5]!=null}"><s:property value="%{#rowVal[5]}"/></s:if></strong>
					</s:elseif>
					<s:else>
						<s:if test="cdrCbuPojo.cordnmdpstatus==pkCbuStatus_AC">
							<strong><s:text name="garuda.cbu.label.patientActive"></s:text>:<s:if test="%{#rowVal[5]!=null}"><s:property value="%{#rowVal[5]}"/></s:if></strong>
						</s:if>
					</s:else>
				</s:if>
				<s:else>
					<div id="patientinfoheaderdiv"><strong><span></span></strong></div>
				</s:else>
								
				<table cellpadding="0" cellspacing="0" border="0" align="center">
				<tr>
					<td><s:text name="garuda.cdrcbuview.label.patient_disease" />:</td>
					<td align="left">
						<s:if test="%{#rowVal[2]!=null}"><s:property value="%{#rowVal[2]}"/></s:if>
					</td>
				</tr>
				<tr>
					<td><s:text name="garuda.cdrcbuview.label.patient_disease_status" />:</td>
					<td align="left">
						<s:if test="%{#rowVal[3]!=null}"><s:property value="%{#rowVal[3]}"/></s:if>
					</td>
				</tr>
				<tr>
					<td><s:text name="garuda.cbuentry.label.tncfrozenpatwt" />:</td>
					<td align="left"><s:if test='%{getCodeListDescById(orderPojo.orderType)=="OR"}'><s:if test="%{#rowVal[4]!=null && #rowVal[4]!=0 && testCalVal1!=null}"><s:property value="%{testCalVal1}"/>&nbsp;<s:text name="garuda.minimumcriteria.tncunit"></s:text>&nbsp;/&nbsp;<s:text name="garuda.common.unit.kg"></s:text></s:if></s:if></td>
				</tr>
				<s:if test="cdrCbuPojo.site.isCDRUser()==true">
				<s:if test='%{#rowVal[7]!=null && getCodeListDescById(orderPojo.orderType)=="OR"}'>
					<tr>
						<td><s:text	name="garuda.minimumcriteria.label.shippedon" />:</td>
						<td align="left"><s:if test="%{#rowVal[6]!=null}"><s:property value="%{#rowVal[6]}"/></s:if></td>
					</tr>
				</s:if>
				<s:if test='%{#rowVal[0]!=null && getCodeListDescById(orderPojo.orderType)=="OR"}'>
				<s:if test="cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)">
					<tr>
					<td><s:text	name="garuda.minimumcriteria.label.reportinfu" />:</td>
					<td align="left"><s:if test="%{#rowVal[1]!=null}"><s:property value="%{#rowVal[1]}"/></s:if></td>
				</tr>
				</s:if>
				</s:if>
				</s:if>
				<s:else>
					<s:hidden id="shippedonfornscbu" value="%{#rowVal[6]}"/>
					<s:hidden id="infusedonfornscbu" value="%{#rowVal[1]}"/>
					<s:if test='%{#rowVal[6]!=null && getCodeListDescById(orderPojo.orderType)=="OR"}'>
						<tr>
							<td id="shippedonlabel"><span></span></td>
							<td align="left" id="shippedonvalue">
							<span></span></td>
						</tr>
					</s:if>
					<s:if test='%{#rowVal[1]!=null && getCodeListDescById(orderPojo.orderType)=="OR" && cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)}'>
						<tr>
							<td id="infusedonlabel"><span></span></td>
							<td align="left" id="infusedonvalue">
							<span></span></td>
						</tr>
					</s:if>
				</s:else>
			</table>
			</s:iterator>
			
			</div>
			</div>
			</s:if>
			</s:if>
			</div>
			<div class="columnDiv" style="width: 35%">
			<div class="outterDiv">
			<div class="colheading">
			<!-- 	<button type="button" onclick="alert('Functionality Not yet Provided!')">
				<s:text name="garuda.cdrcbuview.label.button.bactosearchres" /></button> -->
				<span>&nbsp;</span>
			</div>
			<s:if test="hasViewPermission(#request.cmpltReqInfo)==true">
			<div class="innerDiv" id="CRIDiv">
			<table>
			<tr>
						<s:if test="cbuCompleteReqInfoPojo.completeReqinfoFlag==1">
						<td><img src="images/tick_mark.gif" /></td>
						<td><a href="#" onclick="completeReqInf('<s:property value="cdrCbuPojo.cordID" />')" style="text-decoration: none;">
							<s:text name="garuda.completeReqInfo.label.widgetname" /></a></td>
						</s:if>
						
						<s:if test="cbuCompleteReqInfoPojo == null || cbuCompleteReqInfoPojo.completeReqinfoFlag==null ||  cbuCompleteReqInfoPojo.completeReqinfoFlag==0">
						<td><img src="images/exclamation.jpg" width="20" height="16"/></td>
						<td><a href="#" onclick="completeReqInf('<s:property value="cdrCbuPojo.cordID" />')">
							<s:text name="garuda.completeReqInfo.label.widgetname" /></a></td>
						</s:if>
					
			</tr>
			</table>
			</div>
			<div class="innerDiv">
			<table style="display: none;" id="CRICompleted">
			<tr>
						<td><img src="images/tick_mark.gif" /></td>
						<td><a href="#" onclick="completeReqInf('<s:property value="cdrCbuPojo.cordID" />')" style="text-decoration: none;">
							<s:text name="garuda.completeReqInfo.label.widgetname" /></a></td>
			</tr>
			</table>	
			<table style="display: none;" id="CRINotCompleted">
			<tr>			
						<td><img src="images/exclamation.jpg" width="20" height="16"/></td>
						<td><a href="#" style="text-decoration: none;" onclick="completeReqInf('<s:property value="cdrCbuPojo.cordID" />')">
							<s:text name="garuda.completeReqInfo.label.widgetname" /></a></td>
			</tr>
			</table>
			</div>
			</s:if>
			</div>
			<s:if test='cbuFinalReviewPojo.eligibleConfirmFlag == 1 && cbuFinalReviewPojo.licensureConfirmFlag == 1 && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y" && cbuCompleteReqInfoPojo.completeReqinfoFlag==1'>
			<div class="outterDiv">
			<div class="innerDiv">
			<table>
			<tr>
				
				<s:hidden name="cdrCbuPojo.reviewConfirmStatus" id="reviewConfirmStatus"/>
				<td>
				<img src="images/tick_mark.gif" />
				</td>
				<td>
				<a href="#"  style="text-decoration: none;" onclick="showFinalCbuReviewInfo()"><s:text name="garuda.cbuentry.label.cbufinalReviewFinish" /></a>
				</td>
				
			</tr>
			</table>
			</div>
			</div>
			</s:if>
			<s:elseif test='%{getCodeListDescById(orderPojo.orderType)=="OR"}'>
			<div class="outterDiv">
			<div class="innerDiv">
			<table>
			<tr>
				
				<s:hidden name="cdrCbuPojo.reviewConfirmStatus" id="reviewConfirmStatus"/>
				<td>
				<img src="images/exclamation.jpg" />
				</td>
				<td>
				<a href="#"  style="text-decoration: none;" onclick="showFinalCbuReviewInfo()"><s:text name="garuda.cbuentry.label.static.cbufinalReview" /></a>
				</td>
				
			</tr>
			</table>
			</div>
			</div>
			</s:elseif>
			<s:hidden name="cbuFinalReviewPojo.finalReviewConfirmStatus" id="fcrFlag"/>
			<s:if test='cbuFinalReviewPojo.eligibleConfirmFlag == 1 && cbuFinalReviewPojo.licensureConfirmFlag == 1 && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y" && cbuCompleteReqInfoPojo.completeReqinfoFlag==1'>
			<div class="outterDiv">
			<div class="colheading">
			<!-- 	<button type="button" onclick="alert('Functionality Not yet Provided!')">
				<s:text name="garuda.cdrcbuview.label.button.bactosearchres" /></button> -->
				<span>&nbsp;</span>
			</div>
			<s:if test='(cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.inEligiblePkid || cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId) && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y"'>
			<div class="innerDiv" id="DUMNDiv">
			<table>
			<tr>
						<s:property value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@URG_MED_NEED_CODESUBTYPE]"/>
						<s:if test="(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE].size > 0)">
						<s:set name="inFlag" value="0" scope="request"/>
						<s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]" var="rowVal">
						<s:set name="codeLstSubTyp" value="#rowVal[12]" scope="request"/>
						<s:set name="docId" value="%{#rowVal[5]}" scope="request"/>
						<s:set name="codeLstPkUrgMN" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@URG_MED_NEED_CODESUBTYPE)" scope="request"/>
						<s:if test="#request.inFlag==0">
						<s:if test="#request.codeLstSubTyp!=null && #request.codeLstSubTyp==#request.codeLstPkUrgMN">
						<td><img src="images/tick_mark.gif" /></td>
						<td><a href="#" onclick="window.open('getAttachmentFromDcms?docId=<s:property value="#request.docId"/>&signed=true','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');" style="text-decoration: none;">
							<s:text name="garuda.cdrcbuview.label.signeddumn" /></a></td>
							<s:set name="inFlag" value="1" scope="request"/>
						</s:if></s:if>
						</s:iterator>
						<s:if test="#request.inFlag==0">
						<td><img src="images/exclamation.jpg" width="20" height="16"/></td>
						<td><a href="#"  style="text-decoration: none;">
							<s:text name="garuda.cdrcbuview.label.dumnlbl" /></a></td>
						</s:if>
						</s:if>
						<s:else>
						<td><img src="images/exclamation.jpg" width="20" height="16"/></td>
						<td><a href="#"  style="text-decoration: none;">
							<s:text name="garuda.cdrcbuview.label.dumnlbl" /></a></td>
						</s:else>
					
			</tr>
			</table>
			</div>
			</s:if>
			</div>
			</s:if>
	</div>	
	</div>
</div>
<s:if test="hasViewPermission(#request.updateQuickLnkPanel)==true">
<div class="quicklinkpanel columnDiv" id="quicklinksDiv">
			<table style="padding-left: 2px;">
			<tr>
			<td width="5%" nowrap="nowrap">
			<s:text	name="garuda.cdrcbuview.label.quicklinks" />
			</td>
			<td width="95%">
			<s:if test="hasViewPermission(#request.updateUnitRepStat)==true && cdrCbuPojo.site.stausUnitReport()==true">
				<input type="button" onclick="getShowModal('<s:text name="garuda.cbuentry.label.unitreportracbi"/> <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','viewUnitReport?cordID=<s:property value="cdrCbuPojo.cordID" />&cbuid=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />','500','700');"
				value='<s:text name="garuda.cdrcbuview.label.button.unitreport" />'/>
			</s:if>
			<s:if test="hasViewPermission(#request.viewNote)==true">
			<s:if test='(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)) '>
				<input type="button" onclick="notesListforPrint('fcrcomplete')"
				value='<s:text name="garuda.cdrcbuview.label.button.addclinicalnotes" />'/>
			</s:if>
			<s:else>
				<input type="button" onclick="notesListforPrint('fcrnotcomplete')"
				value='<s:text name="garuda.cdrcbuview.label.button.addclinicalnotes" />'/>
			</s:else>
			</s:if>
			<s:if test="hasViewPermission(#request.updateUploadDoc)==true">
				<input type="button" onclick="getShowModal('<s:text name="garuda.cdrcbuview.label.button.uploaddocument"/> <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','uploaddocument?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CLINICAL_RECORD_UPLOAD_DOC"/>&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />','500','600');"
				value='<s:text name="garuda.cdrcbuview.label.button.uploaddocument" />'/>
			</s:if>
			<s:if test="hasViewPermission(#request.updateCBUReport)==true">
				<input type="button" onclick="getShowModal('<s:text name="garuda.report.label.report"/> <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','reports?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderPojo.pk_OrderId" />&orderType=<s:property value="getCodeListDescById(orderPojo.orderType)" />','400','650');" value="<s:text name="garuda.cdrcbuview.label.button.printcbureport"/>"></input>
			</s:if>	
			<s:if test="hasViewPermission(#request.updateAuditTrail)==true">
				<input type="button" onclick="fn_GetShowModals('<s:text name="garuda.cdrcbuview.label.button.audittrail"/> <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','revokedUnitReports?entityId=<s:property value="cdrCbuPojo.cordID" />&cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />','600','1000','auditModalDiv');"
				value="<s:text name="garuda.cdrcbuview.label.button.audittrail"/>"></input>
			</s:if>		
			<s:if test="hasViewPermission(#request.updateFinalCBURev)==true">
				<s:if test='FCRCompletedFlag==true || (cbuCompleteReqInfoPojo.completeReqinfoFlag == 1 && getCodeListDescById(orderPojo.orderType)=="OR")'>
					<input type="button" value="<s:text name="garuda.cdrcbuview.label.button.compfinaleligibility"/>" onclick="showFinalCbuReviewInfo();"></input>	
				</s:if>
		    </s:if>
			<s:if test="hasViewPermission(#request.updateMinCriteria)==true">
			<s:if test='(MCCompletedFlag==true || getCodeListDescById(orderPojo.orderType)=="OR")'>
				<input type="button" id="mincriteriaBtn" onclick="minimumcriteria('<s:property value="cdrCbuPojo.cordID" />');"
					value='<s:text name="garuda.minimumcriteria.label.widgetname" />'/>
			</s:if>
			</s:if>
			<s:if test='(CBUAssessmentFlag==true || getCodeListDescById(orderPojo.orderType)=="OR") && cdrCbuPojo.site.usingAssessment()==true'>
	            <input type="button" onclick="getShowModal('<s:text name="garuda.header.label.cbuAssessment"/>:<s:property value="cdrCbuPojo.registryId"/>','cbuAssessment?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','400','600');"
						value="<s:text name="garuda.unitreport.label.eligibility.cbu_assessment"/>"></input>
			</s:if> 
		<s:if test="hasViewPermission(#request.updateFlaggedItem)==true">
			 <input type="button" onclick="flaggedItemsforPrint()"
					value="<s:text name="garuda.unitreport.label.eligibility.flaggeditems"/>"></input>
		</s:if>
		<s:if test="hasViewPermission(#request.updateShipmentItern)==true">
				<s:hidden name="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPMENT_ITER_CODESUBTYPE].size" id="shpItern"></s:hidden>
				<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPMENT_ITER_CODESUBTYPE].size>0">
				 	<input type="button"  onclick="getShowModal('<s:text name="garuda.header.label.shipmentItinerary"/>:<s:property value="cdrCbuPojo.registryId"/>','getShipmentItern?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />','400','500');" 
						 value="<s:text name="garuda.unitreport.label.eligibility.shipmentiteneary"/>"></input>
				</s:if>
            </s:if>
		<s:if test="hasViewPermission(#request.crtePckngSlip)==true">
		<s:if test="tskShipmentInfocompl==1">
		<s:if test="getCodeListDescById(orderPojo.orderType)=='CT'">
		<s:if test='orderPojo.sampleAtLab=="N"'>
		<s:if test="orderPojo.resolByTc!=pkCanceled">
			 <s:if test="(orderPojo.iscordavailfornmdp!=@com.velos.ordercomponent.business.util.VelosMidConstants@FLAG_N && orderPojo.orderStatus!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)) && (orderPojo.orderStatus!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED))">
			 <input type="button" onclick="javascript:createPackageSlip()" 
			 value="<s:text name="garuda.ctShipmentInfo.button.label.createpackageslip" />"></input>
			 </s:if>
		</s:if>
		</s:if>
		</s:if>	 
		</s:if>
		</s:if>
		<s:hidden name="cbuCompleteReqInfoPojo.completeReqinfoFlag" id="compReqIdFlag"/>
		<s:hidden name="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@POST_SHPMNT_CODESUBTYPE].size" id="cbuReceipt"></s:hidden>
				<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@POST_SHPMNT_CODESUBTYPE].size>0">
				 	<input type="button"  onclick="getShowModal('<s:text name="garuda.header.label.cbureceipt"/>:<s:property value="cdrCbuPojo.registryId"/>','getCbuReceipt?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />','400','500');" 
						 value="<s:text name="garuda.cdrcbuview.label.button.cbureceipt"/>"></input>
				</s:if>
		</td>
		</tr>
	</table>
</div>
</s:if>
<script>
	showformsFromStatic();	

	function getShowModal(title,url,hight,width){
		var patientId = $j('#patientId').val();
		var patient_data = '';
		if(patientId!="" && patientId != null && patientId != "undefined"){
			patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
		}
		title = title + patient_data;
		showModal(title,url,hight,width);
	}

	function fn_GetShowModals(title,url,hight,width,divid){
		var patientId = $j('#patientId').val();
		var patient_data = '';
		if(patientId!="" && patientId != null && patientId != "undefined"){
			patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
		}
		title = title + patient_data;
		showModals(title,url,hight,width,divid);
	}
</script>