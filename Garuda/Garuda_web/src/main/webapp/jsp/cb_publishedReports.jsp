<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<script>

function getPdfReport(){
	
	if($j("#reportname").val() == "" || $j("#reportname").val().length == 0)
		alert("<s:text name="garuda.cbu.report.selectReport"/>");
	
	if($j("#reportname").val() == 162 || $j("#reportname").val() == 163){
		if( $j("#datepicker2").val().length == 0) {
			alert("<s:text name="garuda.cbu.report.reqDate"/>");
			return false;
		}
		window.open('pdfReports?repId='+$j('#reportname').val()+'&repName='+$j('#reportname option:selected').text()+'&selDate='+$j('#datepicker2').val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
	}
	if($j("#reportname").val() == 164 || $j("#reportname").val() == 166) {

		if( $j("#selYear").val().length < 4  || isNaN($j("#selYear").val())) {
			alert("<s:text name="garuda.cbu.report.reqYear"/>");
			return false;
		}
		window.open('pdfReports?repId='+$j('#reportname').val()+'&repName='+$j('#reportname option:selected').text()+'&selYear='+$j('#selYear').val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
	}
		
}

function divSelect(val){
if(val == 162 || val == 163){
	$j("#dateDiv").show();
	$j("#yearDiv").hide();
}
else if (val == 164 || val == 166){
    $j("#yearDiv").show();
    $j("#dateDiv").hide();
}	
else
{
	$j("#dateDiv").hide();
	$j("#yearDiv").hide();	
}
}

$j(document).ready(function(){	
/*	var today = new Date();
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();
	var h=today.getHours();
	var mn=today.getMinutes()+1;
	$j("#datepicker2").datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	*/
	getDatePic();
	$j("#dateDiv").hide();
	$j("#yearDiv").hide();	
	
});
	

</script>
<s:form id="publishrep">
<s:div class="portlet" id="ctorderctshipmentparentDiv">
							<div id="ctshipmentbarDiv"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
									<div
										class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
										style="text-align: center;"><span
										class="ui-icon ui-icon-minusthick"></span><s:text name="garuda.cbu.report.publishedReport"/> 
									</div>
									<table width="100%">
												<tr>
												<td width="25%">
													<strong><s:text name="garuda.report.label.repname"/></strong>:
												</td>
												<td width="25%">
												
												<s:select name="repDD" list="#request.reportList" id="reportname"
						listKey="pkReportId" listValue="reportName" headerKey=""
						headerValue="Select"  onchange = "divSelect(this.value)"/>
												</td>
												<td  width="10%">
					    						
					    						</td>
												<td  width="40%" nowrap> <span id= "dateDiv"> <s:text name="garuda.common.lable.date"/>:
												<s:date	name="cDate" id="datepicker2" format="MMM dd, yyyy" /> 
												<s:textfield cssStyle="width:50%" readonly="true" onkeydown="cancelBack();"  name="cDateStr"  id="datepicker2" value="%{datepicker2}" cssClass="datePicWMaxDate"/>
												</span>
												<span id ="yearDiv"> <s:text name="garuda.cbu.report.year"/>: <s:textfield  maxlength="4" cssStyle="width:50%" id="selYear"/> </span>
					    						</td>
												</tr>
									<tr> 
									<td width="25%"><input type="button" value="<s:text name="garuda.cbu.report.getReportButton"/>" onClick ="getPdfReport()" ></input> </td>
									<td width="25%"></td>
									</tr>
									</table>
								</div>
</s:div>
</s:form>