<%@ taglib prefix="s"  uri="/struts-tags"%>
 <%@page import="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"%>

 
<%
String requestObject = request.getParameter("paginateSearch");
PaginateSearch paginateSearch = null;
if(requestObject!=null){
	if(requestObject.equals("workflow")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch");		
	}
	if(requestObject.equals("orders")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginationSearch");
	}
	if(requestObject.equals("naAvailCBU")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginationSearch1");
	}
	if(requestObject.equals("alerts")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginationSearch2");
	}
	if(requestObject.equals("auditStatus")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch");
	}
	if(requestObject.equals("entityStatus")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch1");
	}
	if(requestObject.equals("entityStatusRsn")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch2");
	}
	if(requestObject.equals("multvalues")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch3");
	}
	
	if(requestObject.equals("docHistory")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch4");
	}
	
	if(requestObject.equals("cbureview")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch5");
	}
	
	if(requestObject.equals("hla")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch6");
	}
	
	if(requestObject.equals("pathla")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch16");
	}
	
	if(requestObject.equals("lab")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch7");
	}
	if(requestObject.equals("notes")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch8");
	}
	if(requestObject.equals("forms")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch9");
	}
	if(requestObject.equals("besthla")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch10");
	}
	if(requestObject.equals("pf")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch11");
	}
	if(requestObject.equals("notAvailableCbusOnCordEntry")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginationSearch");
		 request.setAttribute("cbuCount",paginateSearch.getiTotalRows());
	}
	if(requestObject.equals("formshhs")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch12");
	}
	if(requestObject.equals("id")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch13");
	}
	if(requestObject.equals("procInfo")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch14");
	}
	if(requestObject.equals("mc")){
		 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch15");
	}
}
else
	 paginateSearch = (PaginateSearch)request.getAttribute("paginateSearch");


 String divName = request.getParameter("divName");
 String formName = request.getParameter("formName");
 String showsRecordId = request.getParameter("showsRecordId");
 String url = request.getParameter("url");
 String bodyLoadFn = request.getParameter("bodyOnloadFn");
 String cbuid = request.getParameter("cbuid");
 String pkcord = request.getParameter("pkcord");
 String flag = request.getParameter("flag");
 String cbbftxt = request.getParameter("prof_search_txt");
 String cbbIdForFund = request.getParameter("cbbIdForFund");
 String cbbNameForFund = request.getParameter("cbbNameForFund");
 String pfilterflag = request.getParameter("filterflag");
 String pksite = request.getParameter("siteId"); 
 String fkordertype = request.getParameter("ordertype");
 String allparamvals = request.getParameter("dropdownfilter");
 String naCBUparamvals = request.getParameter("naCBUparams");
 String naCBUflag = request.getParameter("naCBUflag");
 String sipparamvals = request.getParameter("sipparams");
 String sipflag = request.getParameter("sipflag");
 String cordID = request.getParameter("cordID");
 String entityId = request.getParameter("entityId");
 String widget = request.getParameter("widget");
 String paramval = request.getParameter("idparam");
 String ordParamsval = request.getParameter("ordParams");
 String ordSortParam = request.getParameter("ord_sortParam");
 String allEntriesflag = request.getParameter("allentries");
 String ordParamsSortVal = request.getParameter("ordParamsSort");
 String procFilterValues = request.getParameter("procFilterVals");
 String cbuInfoParamIdVal = request.getParameter("cbuInfoParamId");
 String idInfoParamIdVal = request.getParameter("idInfoParamId");
 String procInfoParamIdVal = request.getParameter("procInfoParamId");
 String licInfoParamIdVal = request.getParameter("licInfoParamId");
 String eligInfoParamIdVal = request.getParameter("eligInfoParamId");
 String revInfoParamIdVal = request.getParameter("revInfoParamId");
 String labInfoParamIdVal = request.getParameter("labInfoParamId");
 String cnInfoParamIdVal = request.getParameter("cnInfoParamId");
 String hlaInfoParamIdVal = request.getParameter("hlaInfoParamId");
 String mcInfoParamIdVal = request.getParameter("mcInfoParamId");
 String orInfoParamIdVal = request.getParameter("orInfoParamId");
 String idmInfoParamIdVal = request.getParameter("idmInfoParamId");
 String hhsInfoParamIdVal = request.getParameter("hhsInfoParamId");
 String docInfoParamIdVal = request.getParameter("docInfoParamId");
 String pathlaInfoParamIdVal = request.getParameter("pathlaInfoParamId");
 
 String cbuCount = "";
 if(request.getAttribute("cbuCount")!=null)
 cbuCount = request.getAttribute("cbuCount").toString();
 String module = "";
 if(request.getAttribute("paginateModule")!=null)
 module = request.getAttribute("paginateModule").toString();
 
 String siteIdentifier = "";
 if(request.getAttribute("selSiteIdentifier")!=null)
 {
	 siteIdentifier = request.getAttribute("selSiteIdentifier").toString();
 }
 
 String cbusearchId = "";
 if(request.getAttribute("cbusearchId")!=null){
	 cbusearchId = request.getAttribute("cbusearchId").toString();	
	 if(cbusearchId.indexOf(",")!=-1){
		 String[] searhArr = cbusearchId.split(",");
		 /* cbusearchId = cbusearchId.replaceAll(",","_");*/
		 cbusearchId = "";
		 Integer count = 1;
		 for(String s : searhArr){
			 if(count==1){
				 cbusearchId = cbusearchId+s.trim(); 
			 }else{
				 cbusearchId += "_"+s.trim();
			 }
			 count++;
		 }
	 }	 
 }
 String searchId = "";
 if(request.getAttribute("searchId")!=null){
	 searchId = request.getAttribute("searchId").toString();	 
}
 String collectionDateFromStr = "";
 if(request.getAttribute("collectionDateFromStr")!=null){
	 collectionDateFromStr = request.getAttribute("collectionDateFromStr").toString();
	 if(collectionDateFromStr.indexOf(",")!=-1){
		 String[] searhArr = collectionDateFromStr.split(",");
		 /* cbusearchId = cbusearchId.replaceAll(",","_");*/
		 collectionDateFromStr = "";
		 Integer count = 1;
		 for(String s : searhArr){
			 if(count==1){
				 collectionDateFromStr = collectionDateFromStr+s; 
			 }else{
				 collectionDateFromStr += "_"+s;
			 }
			 count++;
		 }
	 }
 }
 String collectionDateToStr = "";
 if(request.getAttribute("collectionDateToStr")!=null){
	 collectionDateToStr = request.getAttribute("collectionDateToStr").toString();
	 if(collectionDateToStr.indexOf(",")!=-1){
		 String[] searhArr = collectionDateToStr.split(",");
		 /* cbusearchId = cbusearchId.replaceAll(",","_");*/
		 collectionDateToStr = "";
		 Integer count = 1;
		 for(String s : searhArr){
			 if(count==1){
				 collectionDateToStr = collectionDateToStr+s; 
			 }else{
				 collectionDateToStr += "_"+s;
			 }
			 count++;
		 }
	 }
 }
 String submitionDateFromStr = "";
 if(request.getAttribute("submitionDateFromStr")!=null){
	 submitionDateFromStr = request.getAttribute("submitionDateFromStr").toString();
	 if(submitionDateFromStr.indexOf(",")!=-1){
		 String[] searhArr = submitionDateFromStr.split(",");
		 /* cbusearchId = cbusearchId.replaceAll(",","_");*/
		 submitionDateFromStr = "";
		 Integer count = 1;
		 for(String s : searhArr){
			 if(count==1){
				 submitionDateFromStr = submitionDateFromStr+s; 
			 }else{
				 submitionDateFromStr += "_"+s;
			 }
			 count++;
		 }
	 }
 }
 String submitionDateToStr = "";
 if(request.getAttribute("submitionDateToStr")!=null){
	 submitionDateToStr = request.getAttribute("submitionDateToStr").toString();
	 if(submitionDateToStr.indexOf(",")!=-1){
		 String[] searhArr = submitionDateToStr.split(",");
		 /* cbusearchId = cbusearchId.replaceAll(",","_");*/
		 submitionDateToStr = "";
		 Integer count = 1;
		 for(String s : searhArr){
			 if(count==1){
				 submitionDateToStr = submitionDateToStr+s; 
			 }else{
				 submitionDateToStr += "_"+s;
			 }
			 count++;
		 }
	 }
 }
 String fkCordCbuEligible = "";
 if(request.getAttribute("fkCordCbuEligible")!=null){
	 fkCordCbuEligible = request.getAttribute("fkCordCbuEligible").toString();
 }
 String cbuLicStatus = "";
 if(request.getAttribute("cbuLicStatus")!=null){
	 cbuLicStatus = request.getAttribute("cbuLicStatus").toString();
 }
 if(bodyLoadFn == "" || bodyLoadFn == null){
	 bodyLoadFn = "";
 }
 if(cbuid == "" || cbuid == null){
	 cbuid = "";
 }
 if(pkcord == "" || pkcord == null){
	 pkcord = "";
 }
 if(flag == "" || flag == null){
	 flag = "";
 }
 if(pfilterflag == "" || pfilterflag == null){
	 pfilterflag = "";
 }
 if(pksite == "" || pksite == null){
	 pksite = "";
 }
 if(fkordertype == "" || fkordertype == null){
	 fkordertype = "";
 }
 if(cbbIdForFund == "" || cbbIdForFund == null){
	 cbbIdForFund = "";
 }
 if(cbbNameForFund == "" || cbbNameForFund == null){
	 cbbNameForFund = "";
 }
 if(naCBUparamvals == "" || naCBUparamvals == null){
	 naCBUparamvals = "";
 }
 if(naCBUflag == "" || naCBUflag == null){
	 naCBUflag = "";
 }
 if(sipparamvals == "" || sipparamvals == null){
	 sipparamvals = "";
 }
 if(cbbftxt == "" || cbbftxt == null){
	 cbbftxt = "";
 }
 if(sipflag == "" || sipflag == null){
	 sipflag = "";
 }
 if(cordID == "" || cordID == null){
	 cordID = "";
 }
 if(entityId == "" || entityId == null){
	 entityId = "";
 }
 if(widget == "" || widget == null){
	 widget = "";
 }
 if(paramval == "" || paramval == null){
	 paramval = "";
 }
 if(ordParamsval == "" || ordParamsval == null){
	 ordParamsval = "";
 }
 if(ordSortParam == "" || ordSortParam == null){
	 ordSortParam = "";
 }
 if(allEntriesflag == "" || allEntriesflag == null){
	 allEntriesflag = "";
 }
 if(ordParamsSortVal == "" || ordParamsSortVal == null){
	 ordParamsSortVal = "";
 }
 if(procFilterValues == "" || procFilterValues == null){
	 procFilterValues = "";
 }
 
 if(cbuInfoParamIdVal == "" || cbuInfoParamIdVal == null){
	 cbuInfoParamIdVal = "";
 }
 
 if(idInfoParamIdVal == "" || idInfoParamIdVal == null){
	 idInfoParamIdVal = "";
 }
 
 if(procInfoParamIdVal == "" || procInfoParamIdVal == null){
	 procInfoParamIdVal = "";
 }
 
 if(licInfoParamIdVal == "" || licInfoParamIdVal == null){
	 licInfoParamIdVal = "";
 }
 
 if(eligInfoParamIdVal == "" || eligInfoParamIdVal == null){
	 eligInfoParamIdVal = "";
 }
 
 if(revInfoParamIdVal == "" || revInfoParamIdVal == null){
	 revInfoParamIdVal = "";
 }
 
 if(labInfoParamIdVal == "" || labInfoParamIdVal == null){
	 labInfoParamIdVal = "";
 }
 if(hlaInfoParamIdVal == "" || hlaInfoParamIdVal == null){
	 hlaInfoParamIdVal = "";
 }
 if(cnInfoParamIdVal == "" || cnInfoParamIdVal == null){
	 cnInfoParamIdVal = "";
 }
 if(mcInfoParamIdVal == "" || mcInfoParamIdVal == null){
	 mcInfoParamIdVal = "";
 }
 if(orInfoParamIdVal == "" || orInfoParamIdVal == null){
	 orInfoParamIdVal = "";
 }
 if(idmInfoParamIdVal == "" || idmInfoParamIdVal == null){
	 idmInfoParamIdVal = "";
 }
 if(hhsInfoParamIdVal == "" || hhsInfoParamIdVal == null){
	 hhsInfoParamIdVal = "";
 }

 if(docInfoParamIdVal == "" || docInfoParamIdVal == null){
	 docInfoParamIdVal = "";
 }
 
 if(pathlaInfoParamIdVal == "" || pathlaInfoParamIdVal == null){
	 pathlaInfoParamIdVal = "";
 }
 
 String temp = "temp";
 String totalParameters = url+","+showsRecordId+","+cbuid+","+formName+","+divName+","+bodyLoadFn;
 if(pkcord.length()>0){
	 totalParameters = url+","+showsRecordId+","+cbuid+","+formName+","+divName+","+bodyLoadFn+","+pkcord;
	 if(flag.length()>0)
		 totalParameters = url+","+showsRecordId+","+cbuid+","+formName+","+divName+","+bodyLoadFn+","+pkcord+","+flag;
 }
 if(pfilterflag.length()>0){
	totalParameters = url+","+showsRecordId+","+cbuid+","+formName+","+divName+","+bodyLoadFn+","+pfilterflag+","+pksite+","+fkordertype+","+allparamvals+","+ordSortParam+","+allEntriesflag; 
 }
 if(naCBUparamvals.length()>0){
	totalParameters = url+","+showsRecordId+","+cbuid+","+formName+","+divName+","+bodyLoadFn+","+naCBUflag+","+naCBUparamvals+","+ordSortParam+","+allEntriesflag;
 }
 if(sipparamvals.length()>0){
		totalParameters = url+","+showsRecordId+","+cbuid+","+formName+","+divName+","+bodyLoadFn+","+sipflag+","+sipparamvals+","+ordSortParam+","+allEntriesflag;
 }
 if(flag == "parmsForFund" || flag.equals("parmsForFund")){
	 totalParameters = url+","+showsRecordId+","+cbuid+","+formName+","+divName+","+bodyLoadFn+","+cbbIdForFund+","+cbbNameForFund+","+flag;
 }
 if(cordID.equals("cordID")){
	 
	 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget;
	 
	 if(cbuInfoParamIdVal != null && cbuInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+cbuInfoParamIdVal;
	 
	 if(idInfoParamIdVal != null && idInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+idInfoParamIdVal;
	 
	 if(procInfoParamIdVal != null && procInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+procInfoParamIdVal;
	
	 if(licInfoParamIdVal != null && licInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+licInfoParamIdVal;
	
	 if(eligInfoParamIdVal != null && eligInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+eligInfoParamIdVal;
	 
	 if(revInfoParamIdVal != null && revInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+revInfoParamIdVal;
	 
	 if(labInfoParamIdVal != null && labInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+labInfoParamIdVal;
	 
	 if(cnInfoParamIdVal != null && cnInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+cnInfoParamIdVal;
	 
	 if(hlaInfoParamIdVal != null && hlaInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+hlaInfoParamIdVal;
	 
	 if(mcInfoParamIdVal != null && mcInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+mcInfoParamIdVal;
	
	 if(orInfoParamIdVal != null && orInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+orInfoParamIdVal;

	 if(idmInfoParamIdVal != null && idmInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+idmInfoParamIdVal;

	 if(hhsInfoParamIdVal != null && hhsInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+hhsInfoParamIdVal;

	 if(docInfoParamIdVal != null && docInfoParamIdVal.length()>0)
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+docInfoParamIdVal;
	 
	 if(pathlaInfoParamIdVal != null && pathlaInfoParamIdVal.length()>0) {
		 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cordID+","+entityId+","+widget+","+pathlaInfoParamIdVal;
	 }


 }
 /*if(module.equals("advanceLookUp")){
	 totalParameters = url+","+showsRecordId+","+cbuid+","+formName+","+divName+","+bodyLoadFn+","+cbusearchId +","+searchId +","+collectionDateFromStr +","+collectionDateToStr +","+submitionDateFromStr +","+submitionDateToStr +","+fkCordCbuEligible +","+cbuLicStatus;
	totalParameters = totalParameters +","+cbusearchId +","+searchId +","+collectionDateFromStr +","+collectionDateToStr +","+submitionDateFromStr +","+submitionDateToStr +","+fkCordCbuEligible +","+cbuLicStatus;
 }*/
 if(cbbftxt.length()>0){
	 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+cbbftxt;
 }
 if(ordParamsval.length()>0){
	 
	 totalParameters = url+","+showsRecordId+","+temp+","+formName+","+divName+","+bodyLoadFn+","+ordParamsval+","+ordSortParam+","+allEntriesflag;
 }
 if(procFilterValues.length()>0){
	 String orderBy = ordSortParam;
	 totalParameters = url+","+showsRecordId+","+cbuid+","+formName+","+divName+","+bodyLoadFn+","+procFilterValues;
 }
 //// index of pages 

 %>
 
<div id="paginateWrapper<%=paramval%>" style="display: inline-block;">
 <%       
        int i=0;
        int cPage=0;
        if(paginateSearch!=null && paginateSearch.getiTotalRows()!=null && paginateSearch.getiTotalRows()!=0){
        
        cPage = ((int)(Math.ceil((double)paginateSearch.getiEndResultNo()/(paginateSearch.getiTotalSearchRecords()*paginateSearch.getiShowRows()))));
        int prePageNo=(cPage*paginateSearch.getiTotalSearchRecords())-((paginateSearch.getiTotalSearchRecords()-1)+paginateSearch.getiTotalSearchRecords());
        
        if((cPage*paginateSearch.getiTotalSearchRecords())-(paginateSearch.getiTotalSearchRecords())>0)
        {       
        	%>
         <a href="#" onclick="paginationFooter(<%=prePageNo%>,'<%=totalParameters%>','<%=cbuCount%>','<%=module%>','<%=siteIdentifier%>');"> << <s:text name="garuda.pagination.label.previous"/></a>
         <%
        	}
        for(i=((cPage*paginateSearch.getiTotalSearchRecords())-(paginateSearch.getiTotalSearchRecords()-1));i<=(cPage*paginateSearch.getiTotalSearchRecords());i++)
        {
        	if(i==((paginateSearch.getiPageNo()/paginateSearch.getiShowRows())+1)){
        		%>
                <b><a href="#" onclick="paginationFooter(<%=i%>,'<%=totalParameters%>','<%=cbuCount%>','<%=module%>','<%=siteIdentifier%>');" style="cursor:pointer;color: red" class="currentPage"><%=i%></a></b>
               <%
        	
          }
          else if(i<=paginateSearch.getiTotalPages())
          {
          %>
           <a href="#" onclick="paginationFooter(<%=i%>,'<%=totalParameters%>','<%=cbuCount%>','<%=module%>','<%=siteIdentifier%>');"><%=i%></a>
          <% 
          }
        }
      
        if(paginateSearch.getiTotalPages()>paginateSearch.getiTotalSearchRecords() && i<paginateSearch.getiTotalPages())
        {
         %>
         <a href="#" onclick="paginationFooter(<%=i%>,'<%=totalParameters%>','<%=cbuCount%>','<%=module%>','<%=siteIdentifier%>');"> <s:text name="garuda.pagination.label.next"/> >></a> 
         <%
        }
       
        %>
        
        <input type="hidden" name="maxRec" id="maxRec<%=paramval%>" value=<%=paginateSearch.getiEndResultNo() %> />
        <input type="hidden" name="maxRec" id="minRec<%=paramval%>" value=<%=paginateSearch.getiStartResultNo() %> />
        
       <div id="paginationDiv<%=paramval%>" style="display: inline-block;">
       <b> (&nbsp;&nbsp;<s:text name="garuda.pagination.label.showing"/> <%=paginateSearch.getiStartResultNo() %> <s:text name="garuda.pagination.label.to"/> <%=paginateSearch.getiEndResultNo() %>  <s:text name="garuda.common.message.of"/>  <%=paginateSearch.getiTotalRows() %> <s:text name="garuda.pagination.label.entries"/> &nbsp;&nbsp;)</b>
  	   </div>
  		
		<%
        }
		%>
</div>

<div id="filterResult<%=paramval%>" style="display: none; display: inline-block;">

<div id="filterShow<%=paramval%>" ></div>
</div>