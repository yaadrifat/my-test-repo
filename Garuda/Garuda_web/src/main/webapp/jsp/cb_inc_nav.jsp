<%@ page import="com.velos.ordercomponent.business.domain.User" %>
<%	User userInfo = (User)session.getAttribute("user"); %>
<%@taglib prefix="s" uri="/struts-tags"%>
<script>
$j(document).ready(function(){
	$j("#nav-one").dropmenu();
});

function cbucall(){
	window.location="cbucall";
}
function openorders(){
	window.location="openorder";
}

function newCbucall(){
	window.location="newCbucall";
}

function getReview(){
	window.location="getReview";
}
function dataEntry(){
	window.location="dataEntry";
}
function getCbbDetails(){
	window.location="getCbbDetails";
}
function cordEntry(){
	window.location="cordEntryInProgress";
}
function cbbProcedure(){
	window.location="cbbProcedure";
}
function userProfile(){
	window.location="userProfile";
}
function productfullfillmenthome(){
	window.location="productfullfillmenthome";
}
function opentask(){
	window.location="opentask";
}
function pendingOrders(){
	window.location="pendingOrders";
}
function lookup(){
	
submitform("megamenu");
}
function advancelookup(){
window.location="advancedLookUp"
}

</script>




<div style="border-bottom: 1px solid #ccc; border-top: 1px solid #ccc;margin-top:10px;" >
<s:form id="megamenu" id="megamenu" action="getMainCBUDetails">
<ul id="nav-one" class="dropmenu"> 
			
			<li> <%if(userInfo.getRoleRights().indexOf("cdr")!=-1) {%>
				<a href="#"><s:text name="garuda.cbu.label.CBU"/></a> 
					<ul class="left"> 
						<li><a href="#" onclick="javaScript:cbucall();"><s:text name="garuda.cbu.nav.createCBU"/></a></li> 
						<li><a href="#" onclick="javaScript:openorders();"><s:text name="garuda.cbu.nav.searchOpenOrder"/></a></li> 
						<li><a href="#" onclick="javaScript:newCbucall();"><s:text name="garuda.cbu.nav.viewCBU"/></a></li>
						<li><a href="#" onclick="javaScript:cordEntry();"><s:text name="garuda.cordentry.label.cordentry"/></a></li>
					</ul>
			</li>
			<%} %>
			<%if(userInfo.getRoleRights().indexOf("pf")!=-1) {%>
			<li> 
				<a href="#"><s:text name="garuda.cbu.nav.pf"/></a> 
					<ul class="left"> 
						<li><a href="#" onclick="javaScript:getCbbDetails();"><s:text name="garuda.landingpage.label.CBB"/></a></li> 
						<li><a href="#" onclick="javaScript:cbbProcedure();"><s:text name="garuda.cbu.nav.cbbProc"/></a></li>
						<li><a href="#" onclick="javaScript:userProfile();"><s:text name="garuda.unitreport.label.user_name_profile"/></a></li>
						<li><a href="#" onclick="javaScript:productfullfillmenthome();"><s:text name="garuda.cbu.nav.pf"/></a></li>
						<li><a href="#" onclick="javaScript:opentask();"><s:text name="garuda.opentask.label.opentask"/></a></li>
						<li><a href="#" onclick="javaScript:pendingOrders();"><s:text name="garuda.cbu.nav.pendingOrder"/></a></li>
					</ul>
			</li>
			<%} %>
			<%if(userInfo.getRoleRights().indexOf("dataentry")!=-1) {%>
			<li> 
				<a href="#"><s:text name="garuda.cbu.nav.dataEntry"/></a> 
				<ul> 
					<li><a href="#" onclick="javaScript:dataEntry();"><s:text name="garuda.cbu.nav.dataEntrySpec"/></a></li> 
				</ul>	
			</li>
			<%} %>
			<%if(userInfo.getRoleRights().indexOf("review")!=-1) {%> 
			<li> 
				<a href="#"><s:text name="garuda.clinicalnote.label.button.review"/></a> 
					<ul class="left"> 
						<li><a href="#" onclick="javaScript:getReview();"><s:text name="garuda.cbu.nav.reviewUnitReport"/></a></li> 
					</ul>
			</li>
			<%} %>
			<li>
				<s:textfield name="cbuid" id="cbuid" placeholder="Enter Any ID"></s:textfield>
			</li>
			<li>
				<a href="#"  onclick="javascript:lookup();"><s:text name="garuda.advancelookup.label.lookup"/></a>
			</li>
       		<li>
       			<a href="#" onclick="javascript:advancelookup();"><s:text name="garuda.common.able.advancedlookup"/></a>
       		</li>		
		</ul></s:form>
	</div>