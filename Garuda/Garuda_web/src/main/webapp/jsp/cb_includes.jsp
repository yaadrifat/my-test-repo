<%
String contextpath=request.getContextPath();
%>
<%@ page import="java.util.List,com.velos.eres.service.util.*"%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/common_custom_methods.js" ></script>

	<%@ page contentType="text/html; charset=UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
	<%@ page import="com.velos.ordercomponent.util.VelosGarudaConstants" %>
		
	<%@ page import="java.util.List,java.util.ArrayList,java.util.Map,java.util.HashMap" %>

<%@page import="com.velos.eres.web.user.UserJB"%><script>
var cotextpath = "<%=contextpath%>";
</script>
<%

	String GARUDA_CBU = "CBU";
	String GARUDA_PRODUCTFULLFILLMENT = "PRODUCTFULLFILLMENT";
	String GARUDA_ADMIN = "ADMIN";
	UserJB userInfo = (UserJB)session.getAttribute("currentUser");
	String skin = "";
	
%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<%@ page language="java" import="java.util.*,com.velos.eres.web.user.UserJB, com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	int patientRights = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
	int studyRights = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	UserJB userB1 = (UserJB) tSession.getValue("currentUser");
	String accName = (String) tSession.getValue("accName");
	String accId = (String) tSession.getValue("accountId");
	accName=(accName==null)?"default":accName;
	String accSkinId = "";
	String usersSkinId = userB1.getUserSkin();
	String userSkin = "";
	String accSkin = (String) tSession.getValue("accSkin");
	skin = "default";
	userSkin= codeLst.getCodeCustomCol(StringUtil.stringToNum(usersSkinId) );
	accSkin= codeLst.getCodeCustomCol(StringUtil.stringToNum(accSkinId) );

	if (accSkin == null || accSkin.equals("") || accSkin.equals("default") ){
		accSkin = "accSkin";
	} else {
		skin = accSkin;
	}

	if (userSkin != null && !userSkin.equals("") ){
		skin = userSkin;
	}
}
%>
<%-- Commenting Un Used Css Reference for Performance improvement -- By Ganesavel 
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/jquery.time.slider.css" media="screen"/> 
--%> 

<%-- Commenting Un Used js Reference for Performance improvement -- By Ganesavel 
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/jsapi.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/general_message.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/jquery.time.slider.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/dropmenu.js"></script>--%> <%-- Commented for Bug#7616
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/modernizr-1.1.min.js"></script> 
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/mask.js"></script>
--%>  

<!-- Garuda Common CSS -->
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/velosCDR.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/<%=skin%>/Garuda_skin.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/general.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/portlet.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/jquery.alerts.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/pending-order.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/jquery.treeview.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/fullcalendar.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/jquery.tablescroll.css" media="screen"/>

<!-- Common CSS -->

<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/forms.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/clickmenu.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/styles.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/demo_table.css" media="screen" />

<%-- To Serve following four CSS files in one request
<link rel="StyleSheet" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/forms,clickmenu,styles,demo_table.css"/>
--%>

<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/tabletools/css/TableTools.css" media="screen" />

<!-- Garuda Common js -->

<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.dialogextend.1_0_1.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>


<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/dateFormat.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/scripts.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/jquery.printArea.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/fullcalendar.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/jquery.searchabledropdown-1.0.7.src.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/jquery.dTcolumnManager.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/jquery.tablescroll.js"></script>
 
<!-- DCMS File Upload -->
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/dcms-fileUpload.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/jquery.alerts.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/ui/i18n/jquery.ui.datepicker-en-US.js"></script>
<%--
<script language="JavaScript" src="<%=contextpath%>/jsp/js/jquery/jquery.dialogextend.1_0_1,jquery.dataTables,dataTables.scroller.min,./common/dateFormat,scripts,jquery.printArea,fullcalendar,jquery.searchabledropdown-1.0.7.src,jquery.dTcolumnManager,jquery.tablescroll,dcms-fileUpload,jquery.alerts.js"></script>
--%>
<script type="text/javascript" src="./FCKeditor/fckeditor.js?_skipcache_=1"></script>

<script type="text/javascript">
	var fck;
	var oFCKeditor;
      function init()
      {

      oFCKeditor = new FCKeditor("sectionContentsta") ;
      oFCKeditor.BasePath = './FCKeditor/';
      oFCKeditor.Width = 800 ;
      oFCKeditor.Height = editorHeight ; // 400 pixels
      oFCKeditor.ToolbarSet="garuda";
      //oFCKeditor.Config['SkinPath'] = '/FCKeditor/editor/skins/office2003/' ;
      oFCKeditor.ReplaceTextarea() ;

      }
      var editor;
  function FCKeditor_OnComplete(oFCKeditor)
{
	 fck= FCKeditorAPI.GetInstance('sectionContentsta');
	 oFCKeditor.Events.AttachEvent( 'OnSelectionChange',typeof enable != 'undefined'?enable:tempEnable);
}
  function tempEnable(){
		 
  }

</script>