<%@taglib prefix="s" uri="/struts-tags"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.ordercomponent.util.GetResourceBundleData" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="x-ua-compatible" content="IE=8" />
<jsp:include page="./cb_track_session_logging.jsp" />
<%
String contextpath=request.getContextPath();
%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<% GetResourceBundleData propData = new GetResourceBundleData();%>
<div id="dynaFormDiv">
<s:form id="dynamicForm" >
<s:hidden name="entityId" id="cordID" ></s:hidden>
<s:hidden name="fromViewClinicalFrm" id="fromViewClinicalFrm" ></s:hidden>
<s:hidden name="subentityType" id="subentityType"></s:hidden>
<s:hidden name="formId" id="formId" value="%{formId}"></s:hidden>
<s:hidden name="isCordEntry" id="isCordEntry"></s:hidden>
<s:hidden name="isCompInfo" id="isCompInfo"></s:hidden>
<s:hidden name="mrqlabel" value="%{@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_MRQ}" id="mrqlabel"></s:hidden>
<s:hidden name="fmhqlabel" value="%{@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ}" id="fmhqlabel"></s:hidden>
<s:hidden name="listsize" value="%{mrqFormList.size()}" id="listsize"/>
<s:hidden name="seqnumber" value="%{seqnumber+1}" id="seqnumber"></s:hidden>
<s:hidden name="iseditable" id="iseditable"/>
<s:hidden name="pkformversion" id="pkformversion" value="%{formversion.pkformversion}"/>
<s:hidden name='iscdrMember' id='iscdrMember'></s:hidden>
<s:hidden id="regid" value="%{cdrCbuPojo.registryId}"/>

<s:hidden name="isCordEntryPage" id="isCordEntryPage"/>
<s:hidden id="FMHQformversion" value="%{formvers}"/>
<s:hidden id="currentFMHQV" value="%{latestFormVersion}"/>
<s:hidden id="isFormavail" value="%{isFormavail}"/>
<s:hidden id="isFMHQNewForm" value="%{isFMHQNewForm}"/>
<s:hidden id="quoteField" name="quoteField"/>
<s:if test="%{isFormavail==true || editFlag==1}">
<div id="dynaUpdate" style="display: none;" >
	<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:property value="%{formId}"/>&nbsp;<%=propData.getPropertiesData("garuda.label.dynamicform.savemsg") %></strong></p>
			</div>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td>
			<input type="button" onclick="closeModals('<s:property value="%{formId}"/>ModalForm')" value='<%=propData.getPropertiesData("garuda.common.close") %>' />
		</td>
	</tr>
</table>		
</div>
<div class="dynamicForm">
	<div id="dynaInfo" >
	<s:hidden name ="assess_ncd" id="assess_ncd" value="%{#request.assess_ncd}"/>
	<div>
	<table width="100%" cellpadding="0" cellspacing="0" class="displaycdr" id="formsTable" >
		<thead>
			<tr>
				<th width="5%"><%=propData.getPropertiesData("garuda.label.dynamicform.question") %></th>
				<th width="65%"><%=propData.getPropertiesData("garuda.label.dynamicform.description") %></th>
				<th width="30%"><%=propData.getPropertiesData("garuda.label.dynamicform.response") %></th>
			</tr>
			<tr id="<s:property value='%{formId}'/>_dateTr">
			<td></td>
			<td>
				<s:if test="%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_MRQ}">
					<%=propData.getPropertiesData("garuda.label.dynamicform.datemomans") %>
				</s:if>
				<s:elseif test="%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ}">
					<%=propData.getPropertiesData("garuda.label.dynamicform.formcompdate") %>
				</s:elseif>
				<span class='error' id="dynaFormDateMan">
					*
				</span>
			</td>
			<td>
					<s:textfield onkeydown="cancelBack();"  name="dynaFormDate" id="%{formId}dynaFormDate" 
							cssClass="formdatePicWMaxDate iseditable IsMandatory isReqPriorToShip tabclass" onchange="fieldclear(this);isChangeDone(this,'%{formversion.dynaFormDateStr}','frmDatechangeCls');"
							value="%{formversion.dynaFormDateStr}" tabindex="1" /><br/>
				<span class='error fieldMan' style="display:none" id="errorMsg_<s:property value='%{formId}'/>dynaFormDate">
					<%=propData.getPropertiesData("garuda.label.dynamicform.errormsg") %>
				</span>
			</td>
			</tr>
		</thead>
		<tbody>
		<s:set name="indexval" value="0"></s:set>
		<s:set name="row" value="1"></s:set>
		<s:iterator value="%{quesGrp}" var="grpval">
		<s:if test="%{#grpval[1]!=null}">
			<tr bgcolor="#E6E6E5">
				<td colspan="3" style="padding-top: 5px;padding-bottom: "><s:property value="%{#grpval[1]}"/> </td>
			</tr>
		</s:if>
		<s:iterator value="%{mrqFormList}" var="rowval">
		<s:if test="%{#request.formpk==null}">
			<s:hidden name="fkform" id="fkform" value="%{#rowval[25]}"></s:hidden>
			<s:set name="formpk" scope="request" value="%{#rowval[25]}"></s:set>
		</s:if>
		<s:if test="%{#grpval[0]==#rowval[22]}">
		<s:if test="%{#rowval[3]==null}">
			<tr id="Question_<s:property value='%{#indexval}'/>" class="<s:if test='%{#rowval[11]!=null}'><s:if test='%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ}'><s:if test='%{#rowval[15]==null}'>hidechild</s:if></s:if></s:if>">
				<td width="5%" align="center" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;" id="Question_<s:property value='%{#indexval}'/>_td">
				<s:hidden id="%{formId}_formDate" value="%{#rowval[21]}"></s:hidden>
				<s:hidden name="%{formId}_Question[%{#indexval}].pkresponse" value="%{#rowval[13]}"></s:hidden>
				<s:hidden id="%{formId}_fkform" name="%{formId}_Question[%{#indexval}].fkform" value="%{#rowval[25]}"></s:hidden>
				<s:hidden name="%{formId}_Question[%{#indexval}].fkquestion" value="%{#rowval[0]}" id="%{formId}_Ques%{#row}_s_fktestid"></s:hidden>
				<s:hidden name="shrtName" value="%{formId}_Ques%{#row}_s" id="%{#indexval}_assessshname"/>
				<s:hidden name="subEntityId" value="%{#rowval[13]}" />
				<s:hidden name="formflag" value="true" />
				<s:hidden name="questionNo" value="%{formId} Q#%{#rowval[24]}" />
				<s:hidden name="fieldIndex" value="%{#rowval[0]}" id="%{formId}_index%{#indexval}" />
				<s:hidden name="assesCon" value="%{#rowval[7]}" id="%{formId}_assesCon%{#indexval}" />
				<s:hidden name="unreqFlag" value="%{#rowval[23]}" id="%{formId}_unreqFlag%{#rowval[0]}" />
				<s:hidden name="unreqPreFlag" value="%{#rowval[10]}" id="%{formId}_unreqPreFlag%{#rowval[0]}" />
				<s:hidden name="assessmentCount" value="%{#rowval[19]}" id="assessmentPk_%{#indexval}" />
				<s:hidden name="assessDivName1" id='assessDivName_%{#indexval}' value="%{formId}_Ques%{#row}_s_assessmentDiv"/>
				<s:hidden name="%{formId}_Question[%{#indexval}].commentbox" id="%{formId}_comment_%{#indexval}" value="%{#rowval[17]}"/>
				<s:hidden name="parentquesCodeH" id="%{formId}_parquesCodH_%{#rowval[0]}" value="%{#rowval[20]}"></s:hidden>
					<s:property value="%{#rowval[24]}"/>
				</td>
				<td width="70%" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;">
					<div>
						<s:if test="%{#rowval[4]!=null}">	
							<div style="width: 5%;float: left;">
								<img height="15px" src="./images/help_24x24px.png" onmouseout="return nd();"
									onmouseover="showHelpintooltip('<s:property value='%{#rowval[4]}'/>');">
							</div>
							<div style="width: 2%;float: left;">&nbsp;</div>
						</s:if>
					<div style="width: 93%;float: left;">
						<span style="vertical-align: top;" <s:if test='%{#rowval[5]!=null}'>onmouseout="return nd();" onmouseover="showHelpintooltip('<s:property value='%{#rowval[5]}'/>');"</s:if>>
							<s:property value="%{#rowval[1]}" escapeHtml="false"/>
						</span>
						<s:if test="%{#rowval[11]!=null}">
							<s:if test="%{#rowval[29]==1}">
								<span style="vertical-align: top;" id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#rowval[0]}'/>" class="error <s:if test="%{#rowval[11]!=null && #rowval[11]!=''}"><s:property value='%{formId}'/>_classMan<s:property value='%{#rowval[11]}'/></s:if>">
									*
								</span>
							</s:if>
							<s:else>
								<span style="vertical-align: top;display:none" id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#rowval[0]}'/>" class="error <s:if test="%{#rowval[11]!=null && #rowval[11]!=''}"><s:property value='%{formId}'/>_classMan<s:property value='%{#rowval[11]}'/></s:if>">
									*
								</span>
							</s:else>
						</s:if>
						<s:else>
							<span style="vertical-align: top;display:none" id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#rowval[0]}'/>" class="error <s:if test="%{#rowval[11]!=null && #rowval[11]!=''}"><s:property value='%{formId}'/>_classMan<s:property value='%{#rowval[11]}'/></s:if>">
								*
							</span>
						</s:else>
					<s:if test="%{#rowval[26]!=null}">
					<s:if test='%{#rowval[26]=="chart_vCJD"}'>
						<a onclick="showchat(this,'<%=propData.getPropertiesData("garuda.label.dynamicform.heading1") %>');" style="cursor: pointer;white-space: nowrap;"><%=propData.getPropertiesData("garuda.label.dynamicform.refchart") %></a>
						<div style="display: none;">
							<table width="400px" style="border: 1px solid #0073ae;border-collapse:collapse;font-size: 12px">
							<tr>
								<s:iterator value="%{#application.codeListValues[#rowval[26]]}" status="count">
									<s:if test="%{#count.index==0 || #count.index==7 || #count.index==14 || #count.index==21}">
										<td style="border: 1px solid #0073ae;border-collapse:collapse;padding: 5px"><s:property value="%{description}"/></td>
									</s:if>
									<s:if test="%{#count.index==28}">
										<td rowspan="4" valign="top" style="border: 1px solid #0073ae;border-collapse:collapse;padding: 5px"><s:property value="%{description}"/></td>
									</s:if>
								</s:iterator>
							</tr>
							<tr>
								<s:iterator value="%{#application.codeListValues[#rowval[26]]}" status="count">
									<s:if test="%{#count.index==1 || #count.index==8 || #count.index==15 || #count.index==22}">
										<td style="border: 1px solid #0073ae;border-collapse:collapse;padding: 5px"><s:property value="%{description}"/></td>
									</s:if>
								</s:iterator>
							</tr>
							<tr>
								<s:iterator value="%{#application.codeListValues[#rowval[26]]}" status="count">
									<s:if test="%{#count.index==2 || #count.index==9 || #count.index==16 || #count.index==23}">
										<td style="border: 1px solid #0073ae;border-collapse:collapse;padding: 5px"><s:property value="%{description}"/></td>
									</s:if>
								</s:iterator>
							</tr>
							<tr>
								<s:iterator value="%{#application.codeListValues[#rowval[26]]}" status="count">
									<s:if test="%{#count.index==3 || #count.index==10 || #count.index==17 || #count.index==24}">
										<td style="border: 1px solid #0073ae;border-collapse:collapse;padding: 5px"><s:property value="%{description}"/></td>
									</s:if>
								</s:iterator>
							</tr>
							<tr>
								<s:iterator value="%{#application.codeListValues[#rowval[26]]}" status="count">
									<s:if test="%{#count.index==4 || #count.index==11 || #count.index==18 || #count.index==25}">
										<td style="border: 1px solid #0073ae;border-collapse:collapse;padding: 5px"><s:property value="%{description}"/></td>
									</s:if>
									<s:if test="%{#count.index==29}">
										<td rowspan="3" valign="top" style="border: 1px solid #0073ae;border-collapse:collapse;padding: 5px"><s:property value="%{description}"/></td>
									</s:if>
								</s:iterator>
							</tr>
							<tr>
								<s:iterator value="%{#application.codeListValues[#rowval[26]]}" status="count">
									<s:if test="%{#count.index==5 || #count.index==12 || #count.index==19 || #count.index==26}">
										<td style="border: 1px solid #0073ae;border-collapse:collapse;padding: 5px"><s:property value="%{description}"/></td>
									</s:if>
								</s:iterator>
							</tr>
							<tr>
								<s:iterator value="%{#application.codeListValues[#rowval[26]]}" status="count">
									<s:if test="%{#count.index==6 || #count.index==13 || #count.index==20 || #count.index==27}">
										<td style="border: 1px solid #0073ae;border-collapse:collapse;padding: 5px"><s:property value="%{description}"/></td>
									</s:if>
								</s:iterator>
							</tr>
							</table>
						</div>
					</s:if>
					<s:elseif test='%{#rowval[26]=="risk_HIV1"}'>
						<a onclick="showchat(this,'<%=propData.getPropertiesData("garuda.label.dynamicform.heading2") %>');" style="cursor: pointer;white-space: nowrap;"><%=propData.getPropertiesData("garuda.label.dynamicform.refchart") %></a>
						<div style="display: none;">
							<table width="400px" style="border: 1px solid #0073ae;border-collapse:collapse;font-size: 12px">
								<s:iterator value="%{#application.codeListValues[#rowval[26]]}" status="count">
									<tr style="border: 1px solid #0073ae;">
										<s:if test="%{#count.index!=13}">
											<td style="border: 1px solid #0073ae;border-collapse:collapse;padding: 5px"><s:property value="%{description}"/></td>
										</s:if>
										<s:else>
											<td align="center" style="border: 1px solid #0073ae;border-collapse:collapse;padding: 5px"><s:property value="%{description}"/></td>
										</s:else>
									</tr>
								</s:iterator>
							</table>
						</div>
					</s:elseif>
					</s:if>
						<s:if test="%{#rowval[6]==1}">
							<a href="#" onclick="dynacomment(this);" style="text-decoration: none;white-space: nowrap;">
								<span id='spancomment<s:property value="%{#indexval}"/>' style="vertical-align: top;">
									<s:if test="%{#rowval[17]!=null}">
											<img height="15px" src="./images/addcomment.png">
											<%=propData.getPropertiesData("garuda.label.dynamicform.addorviewcomment") %>
									</s:if>
									<s:else>
										<s:if test="%{iseditable != 1}">
											<img height="15px" src="./images/addcomment.png">
											<%=propData.getPropertiesData("garuda.label.dynamicform.addcomment") %>
										</s:if>
									</s:else>
								</span>
							</a>
							<div class="commentbox" style="display: none;">
								<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
								<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
									<%=propData.getPropertiesData("garuda.label.dynamicform.commentbox") %>
								</div>
									<s:textarea name="%{formId}_Question[%{#indexval}].comment" cols="20" rows="5" value="%{#rowval[17]}" cssClass="iseditable" onkeypress="commentclear(this,%{#indexval});"
											style="width:95%" maxlength="3000" onchange="isChangeDone(this,'%{#rowval[17]}','frmDatechangeCls');"/>
									<br/><span class='error' style="display:none;" id='<s:property value="%{#indexval}"/>_Commenterror'><%=propData.getPropertiesData("garuda.label.dynamicform.errormsg") %></span>
									<br/><s:if test="%{iseditable!=1}">
									<input type="button" onclick="saveComment(this,'<s:property value="%{#indexval}"/>')" value='<%=propData.getPropertiesData("garuda.common.save") %>' tabindex="1"/>
									</s:if>
									<input type="button" onclick="hideDynaComment(this,'<s:property value="%{#indexval}"/>')" value='<%=propData.getPropertiesData("garuda.common.close") %>' tabindex="2"/>
								</div>
							</div>
						</s:if>
					</div>
					</div>
					<s:if test="%{#rowval[7]==1}">
						<div id="assess_<s:property value='%{#indexval}'/>" style="display: none;" class="assessMentDiv">
							
						</div>
					</s:if>
				</td>
				<td style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;" width="25%">
				<s:if test="%{#rowval[9]!=null}">
					<div style="display: none;">
						<s:select id="Ques%{#indexval}_UNRes" style="display:none" 
						list="%{#application.codeListValues[#rowval[9]]}" listKey="pkCodeId" listValue="description"/>
					</div>
				</s:if>
				<s:if test="%{#rowval[11]!=null}">
					<s:generator val="%{#rowval[12]}" separator=",">
					<select style="display:none" 
						id="<s:property value='%{formId}'/>_depentVal_<s:property value='%{#rowval[11]}'/><s:if test='%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ}'><s:property value='%{#rowval[12]}'/></s:if>">
						<s:iterator>
  							<option value='<s:property/>'><s:property/></option>
						</s:iterator>
					</select>
					</s:generator>
				</s:if>
				<s:if test="%{getCodeListDescById(#rowval[2])==@com.velos.ordercomponent.util.VelosGarudaConstants@DROPDOWN_FIELD}">
				<s:if test="%{#rowval[8]!=null}">
					<s:if test='%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ}'>
						<s:select name="%{formId}_Question[%{#indexval}].responsecode" tabindex="%{#indexval+2}" 
								id="%{formId}_QuestionRes%{#indexval}"  
								list="%{#application.codeListValues[#rowval[8]]}" 
								listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
								onchange="showDynaAssess('%{#indexval}',this.id,%{#rowval[7]},'1');checkMultiDependent(this.id,'%{#rowval[0]}','%{formId}');fieldclear(this);
								isChangeDone(this,'%{#rowval[15]}','frmDatechangeCls');"  
								cssClass="tabclass %{formId}_unclass%{#rowval[11]}%{#rowval[12]} findlen iseditable %{formId}_Ques%{#row}_s_res" value="%{#rowval[15]}"/>
					</s:if>
					<s:else>
					<s:if test="%{#rowval[11]!=null}">
						<s:if test="%{#rowval[29]==1}">
							<s:select name="%{formId}_Question[%{#indexval}].responsecode" tabindex="%{#indexval+2}"
									id="%{formId}_QuestionRes%{#indexval}" 
									list="%{#application.codeListValues[#rowval[8]]}" 
									listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
									onchange="showDynaAssess('%{#indexval}',this.id,%{#rowval[7]},'1');checkDependent(this.id,'%{#rowval[0]}','%{formId}');fieldclear(this);
									isChangeDone(this,'%{#rowval[15]}','frmDatechangeCls');"  
									cssClass="tabclass %{formId}_unclass%{#rowval[11]} findlen iseditable %{formId}_Ques%{#row}_s_res" value="%{#rowval[15]}"/>
						</s:if>
						<s:else>
							<s:select name="%{formId}_Question[%{#indexval}].responsecode" tabindex="%{#indexval+2}"
									id="%{formId}_QuestionRes%{#indexval}" disabled="true"
									list="%{#application.codeListValues[#rowval[8]]}" 
									listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
									onchange="showDynaAssess('%{#indexval}',this.id,%{#rowval[7]},'1');checkDependent(this.id,'%{#rowval[0]}','%{formId}');fieldclear(this);
									isChangeDone(this,'%{#rowval[15]}','frmDatechangeCls');"  
									cssClass="tabclass %{formId}_unclass%{#rowval[11]} findlen iseditable %{formId}_Ques%{#row}_s_res" value="%{#rowval[15]}"/>
						</s:else>
					</s:if>
					<s:else>
						<s:select name="%{formId}_Question[%{#indexval}].responsecode" tabindex="%{#indexval+2}"
									id="%{formId}_QuestionRes%{#indexval}" 
									list="%{#application.codeListValues[#rowval[8]]}" 
									listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
									onchange="showDynaAssess('%{#indexval}',this.id,%{#rowval[7]},'1');checkDependent(this.id,'%{#rowval[0]}','%{formId}');fieldclear(this);
									isChangeDone(this,'%{#rowval[15]}','frmDatechangeCls');"  
									cssClass="tabclass %{formId}_unclass%{#rowval[11]} findlen iseditable %{formId}_Ques%{#row}_s_res" value="%{#rowval[15]}"/>
					</s:else>
					</s:else>
					
				</s:if>
				</s:if>
				<s:if test="%{getCodeListDescById(#rowval[2])==@com.velos.ordercomponent.util.VelosGarudaConstants@TEXT_FIELD}">
					<s:if test="%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ}">
						<s:textfield name="%{formId}_Question[%{#indexval}].responsevalue"  onchange="fieldclear(this);isChangeDone(this,'%{#rowval[16]}','frmDatechangeCls');" tabindex="%{#indexval+2}"
							id="%{formId}_QuestionRes%{#indexval}"
							cssClass="tabclass %{#rowval[11]}_unclass%{#rowval[11]}%{#rowval[12]} findlen iseditable" value="%{#rowval[16]}"/>
						<s:if test="%{#rowval[20]=='can_leuk_ind' || #rowval[20]=='inher_other_dises_ind' }">
						<s:if test="%{iseditable!=1}">
							<div class="addType_<s:property value='%{#rowval[0]}'/>">
								<s:a href="#" onclick='addtype(this,"%{#rowval[25]}","%{#rowval[0]}","%{#rowval[1]}","%{#rowval[24]}");'>
									<%=propData.getPropertiesData("garuda.label.dynamicform.addtype") %>
								</s:a>
							</div>
						</s:if>
						</s:if>
					</s:if>
					<s:else>
					<s:if test="%{#rowval[11]!=null}">
						<s:if test="%{#rowval[29]==1}">
							<s:textfield name="%{formId}_Question[%{#indexval}].responsevalue"  onchange="fieldclear(this);isChangeDone(this,'%{#rowval[16]}','frmDatechangeCls');" tabindex="%{#indexval+2}"
								id="%{formId}_QuestionRes%{#indexval}"
								cssClass="tabclass %{#rowval[11]}_unclass%{#rowval[11]} findlen iseditable" value="%{#rowval[16]}"/>
						</s:if>
						<s:else>
							<s:textfield name="%{formId}_Question[%{#indexval}].responsevalue"  onchange="fieldclear(this);isChangeDone(this,'%{#rowval[16]}','frmDatechangeCls');" tabindex="%{#indexval+2}"
								id="%{formId}_QuestionRes%{#indexval}" disabled="disabled" 
								cssClass="tabclass %{#rowval[11]}_unclass%{#rowval[11]} findlen iseditable" value="%{#rowval[16]}"/>
						</s:else>
					</s:if>
					<s:else>
						<s:textfield name="%{formId}_Question[%{#indexval}].responsevalue"  onchange="fieldclear(this);isChangeDone(this,'%{#rowval[16]}','frmDatechangeCls');" tabindex="%{#indexval+2}"
								id="%{formId}_QuestionRes%{#indexval}"
								cssClass="tabclass %{#rowval[11]}_unclass%{#rowval[11]} findlen iseditable" value="%{#rowval[16]}"/>
					</s:else>
					</s:else>
				</s:if>
				<s:if test="%{getCodeListDescById(#rowval[2])==@com.velos.ordercomponent.util.VelosGarudaConstants@LABEL}">
					<span class="findlen" id="<s:property value='%{formId}'/>_QuestionRes<s:property value='%{#indexval}'/>"></span>
				</s:if>
				<!-- Section to Display the Multi Select -->
				<s:if test="%{getCodeListDescById(#rowval[2])==@com.velos.ordercomponent.util.VelosGarudaConstants@MULTISELECT_FIELD}">
				<s:if test="%{#rowval[8]!=null}">
				
					<s:select name="%{formId}_Question[%{#indexval}].responsevalue" tabindex="%{#indexval+2}"
							id="%{formId}_QuestionRes%{#indexval}"
							list="%{#application.codeListValues[#rowval[8]]}" 
							listKey="pkCodeId" listValue="description"
							onchange="showDynaAssess('%{#indexval}',this.id,%{#rowval[7]},'1');checkMultiDependent(this.id,'%{#rowval[0]}','%{formId}');fieldclear(this);
							isChangeDone(this,'%{getCommaSeparatedData(#rowval[16])}','frmDatechangeCls');"
							multiple="true" cssClass="tabclass %{formId}_unclass%{#rowval[11]}%{#rowval[12]} findlen iseditable %{formId}_Ques%{#row}_s_res"
							value="%{getCommaSeparatedData(#rowval[16])}" cssStyle="height:auto;"/>
				</s:if>
				</s:if>
					<span class='error fieldMan' style="display:none" id='errorMsg_<s:property value="%{formId}"/>_QuestionRes<s:property value="%{#indexval}"/>'>
						<%=propData.getPropertiesData("garuda.label.dynamicform.errormsg") %>
					</span>
					<span class='error <s:property value="%{formId}"/>_Ques<s:property value="%{#row}"/>_s_assessError' style="display:none" id="assesError_<s:property value='%{#indexval}'/>">
						<%=propData.getPropertiesData("garuda.label.dynamicform.assessmentmsg") %>
					</span>
					<div>
						<a id="showasses_<s:property value='%{#indexval}'/>" href="#" onclick="showDynaAssessLink(this,'<s:property value="%{#indexval}"/>','<s:property value="%{formId}"/>_QuestionRes<s:property value="%{#indexval}"/>',<s:property value='%{#rowval[7]}'/>,'1');"
							style="display: none;" class="<s:property value="%{formId}"/>_Ques<s:property value="%{#row}"/>_s_showassess">
							<%=propData.getPropertiesData("garuda.label.dynamicform.showassess") %>
						</a>
						<a id="hideasses_<s:property value='%{#indexval}'/>" href="#" onclick="hideDynaAssess(this,'<s:property value="%{#indexval}"/>');" style="display: none;"
							class="<s:property value="%{formId}"/>_Ques<s:property value="%{#row}"/>_s_hideassess">
							<%=propData.getPropertiesData("garuda.label.dynamicform.hideassess") %>
						</a>
					</div>
				</td>
			</tr>
			<s:set name="indexval" value="%{#indexval+1}"></s:set>
		</s:if>
		<s:set name="subrow" value="1"/>
		<s:iterator value="%{mrqFormList}" var="secrowval">
		<s:if test="%{#rowval[0]==#secrowval[3]}">
			<tr id="Question_<s:property value='%{#indexval}'/>" 
				class="<s:if test='%{#secrowval[11]!=null}'><s:if test='%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ}'><s:if test='%{#secrowval[15]!=null || #secrowval[16]!=null }'></s:if><s:elseif test="%{#secrowval[29]==1}"></s:elseif><s:else>hidechild</s:else></s:if></s:if> tr_<s:property value='%{formId}'/>_unclass<s:property value='%{#secrowval[11]}'/><s:property value='%{#secrowval[12]}'/>">
				<td width="5%" align="center" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;" id="Question_<s:property value='%{#indexval}'/>_td">
					<s:hidden name="%{formId}_Question[%{#indexval}].pkresponse" value="%{#secrowval[13]}"></s:hidden>
					<s:hidden name="%{formId}_Question[%{#indexval}].fkform" value="%{#secrowval[25]}"></s:hidden>
					<s:hidden name="%{formId}_Question[%{#indexval}].fkquestion" value="%{#secrowval[0]}"
					id="%{formId}_Ques%{#row}_%{#subrow}_s_fktestid"></s:hidden>
					<s:hidden name="shrtName" value="%{formId}_Ques%{#row}_%{#subrow}_s" id="%{#indexval}_assessshname"/>
					<s:hidden name="subEntityId" value="%{#secrowval[13]}" />
					<s:hidden name="formflag" value="true" />
					<s:hidden name="questionNo" value="%{formId} Q#%{#secrowval[24]}" />
					<s:hidden name="fieldIndex" value="%{#secrowval[0]}" id="%{formId}_index%{#indexval}" />
					<s:hidden name="assesCon" value="%{#secrowval[7]}" id="%{formId}_assesCon%{#indexval}" />
					<s:hidden name="unreqFlag" value="%{#secrowval[23]}" id="%{formId}_unreqFlag%{#secrowval[0]}" />
					<s:hidden name="unreqPreFlag" value="%{#secrowval[10]}" id="%{formId}_unreqPreFlag%{#secrowval[0]}" />
					<s:hidden name="assessmentPk" value="%{#secrowval[19]}" id="assessmentPk_%{#indexval}" />
					<s:hidden name="textVal" value="%{#secrowval[16]}" id="textval_%{#indexval}"/>
					<s:hidden name="assessDivName1" id="assessDivName_%{#indexval}" value="%{formId}_Ques%{#row}_%{#subrow}_s_assessmentDiv"/>
					<s:hidden name="%{formId}_Question[%{#indexval}].commentbox" id="%{formId}_comment_%{#indexval}" value="%{#secrowval[17]}"/>
					<s:hidden name="quesCodeH" id="%{formId}_quesCodH_%{#secrowval[0]}" value="%{#rowval[20]}"></s:hidden>
					<s:property value="%{#secrowval[24]}"/>
				</td>
				<td width="70%" style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;<s:if test='%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_MRQ}'>text-align: right;</s:if>">
					<div class="childquestions">
						<s:if test="%{#secrowval[4]!=null}">
							<span style="padding-right:5px">
								<img height="15px" src="./images/help_24x24px.png" onmouseout="return nd();"
									onmouseover="showHelpintooltip('<s:property value='%{#secrowval[4]}'/>');">
							</span>
						</s:if>
					<span style="vertical-align: top;" <s:if test='%{#secrowval[5]!=null}'>onmouseout="return nd();" onmouseover="showHelpintooltip('<s:property value='%{#secrowval[5]}'/>');"</s:if>>
						<s:property value="%{#secrowval[1]}" escapeHtml="false"/> 
					</span>
					<s:if test="%{#secrowval[11]!=null}">
						<s:if test="%{#secrowval[29]==1}">
							<span id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#secrowval[0]}'/>" class="error <s:if test="%{#secrowval[11]!=null && #secrowval[11]!=''}"><s:property value='%{formId}'/>_classMan<s:property value='%{#secrowval[11]}'/></s:if>" style="vertical-align: top;">
								*
							</span>
						</s:if>
						<s:else>
							<span id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#secrowval[0]}'/>" class="error <s:if test="%{#secrowval[11]!=null && #secrowval[11]!=''}"><s:property value='%{formId}'/>_classMan<s:property value='%{#secrowval[11]}'/></s:if>" style="display:none;vertical-align: top;">
								*
							</span>
						</s:else>
					</s:if>
					<s:else>
						<span id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#secrowval[0]}'/>" class="error <s:if test="%{#secrowval[11]!=null && #secrowval[11]!=''}"><s:property value='%{formId}'/>_classMan<s:property value='%{#secrowval[11]}'/></s:if>" style="display:none;vertical-align: top;">
							*
						</span>
					</s:else>
					<s:if test="%{#secrowval[26]!=null}">
					<s:if test='%{#secrowval[26]=="chart_vCJD"}'>
						<a onclick="showchat(this,'<%=propData.getPropertiesData("garuda.label.dynamicform.heading1") %>');" style="cursor: pointer;"><%=propData.getPropertiesData("garuda.label.dynamicform.refchart") %></a>
						<div style="display: none;">
							<table width="400px" style="border: 1px solid #0073ae;border-collapse:collapse;font-size: 12px">
								<tr>
									<s:iterator value="%{#application.codeListValues[#secrowval[26]]}" status="count">
										<s:if test="%{#count.index==0 || #count.index==7 || #count.index==14 || #count.index==21}">
											<td style="border: 1px solid #0073ae;border-collapse:collapse;"><s:property value="%{description}"/></td>
										</s:if>
										<s:if test="%{#count.index==28}">
											<td rowspan="4" style="border: 1px solid #0073ae;border-collapse:collapse;"><s:property value="%{description}"/></td>
										</s:if>
									</s:iterator>
								</tr>
								<tr>
									<s:iterator value="%{#application.codeListValues[#secrowval[26]]}" status="count">
										<s:if test="%{#count.index==1 || #count.index==8 || #count.index==15 || #count.index==22}">
											<td style="border: 1px solid #0073ae;border-collapse:collapse;"><s:property value="%{description}"/></td>
										</s:if>
									</s:iterator>
								</tr>
								<tr>
									<s:iterator value="%{#application.codeListValues[#secrowval[26]]}" status="count">
										<s:if test="%{#count.index==2 || #count.index==9 || #count.index==16 || #count.index==23}">
											<td style="border: 1px solid #0073ae;border-collapse:collapse;"><s:property value="%{description}"/></td>
										</s:if>
									</s:iterator>
								</tr>
								<tr>
									<s:iterator value="%{#application.codeListValues[#secrowval[26]]}" status="count">
										<s:if test="%{#count.index==3 || #count.index==10 || #count.index==17 || #count.index==24}">
											<td style="border: 1px solid #0073ae;border-collapse:collapse;"><s:property value="%{description}"/></td>
										</s:if>
									</s:iterator>
								</tr>
								<tr>
									<s:iterator value="%{#application.codeListValues[#secrowval[26]]}" status="count">
										<s:if test="%{#count.index==4 || #count.index==11 || #count.index==18 || #count.index==25}">
											<td style="border: 1px solid #0073ae;border-collapse:collapse;"><s:property value="%{description}"/></td>
										</s:if>
										<s:if test="%{#count.index==29}">
											<td rowspan="3" style="border: 1px solid #0073ae;border-collapse:collapse;"><s:property value="%{description}"/></td>
										</s:if>
									</s:iterator>
								</tr>
								<tr>
									<s:iterator value="%{#application.codeListValues[#secrowval[26]]}" status="count">
										<s:if test="%{#count.index==5 || #count.index==12 || #count.index==19 || #count.index==26}">
											<td style="border: 1px solid #0073ae;border-collapse:collapse;"><s:property value="%{description}"/></td>
										</s:if>
									</s:iterator>
								</tr>
								<tr>
									<s:iterator value="%{#application.codeListValues[#secrowval[26]]}" status="count">
										<s:if test="%{#count.index==6 || #count.index==13 || #count.index==20 || #count.index==27}">
											<td style="border: 1px solid #0073ae;border-collapse:collapse;"><s:property value="%{description}"/></td>
										</s:if>
									</s:iterator>
								</tr>
							</table>
						</div>
					</s:if>
					<s:elseif test='%{#secrowval[26]=="risk_HIV1"}'>
						<a onclick="showchat(this,'<%=propData.getPropertiesData("garuda.label.dynamicform.heading2") %>');" style="cursor: pointer;"><%=propData.getPropertiesData("garuda.label.dynamicform.refchart") %></a>
						<div style="display: none;">
							<table width="450px;" style="border: 1px solid #0073ae;border-collapse:collapse;font-size: 12px" >
								<s:iterator value="%{#application.codeListValues[#secrowval[26]]}" status="count">
									<tr style="border: 1px solid #0073ae;">
										<s:if test="%{#count.index!=13}">
											<td style="border: 1px solid #0073ae;border-collapse:collapse;"><s:property value="%{description}"/></td>
										</s:if>
										<s:else>
											<td style="border: 1px solid #0073ae;border-collapse:collapse;"><s:property value="%{description}"/></td>
										</s:else>
									</tr>
								</s:iterator>
							</table>
						</div>
					</s:elseif>
					</s:if>
						<s:if test="%{#secrowval[6]==1}">
							<a href="#" onclick="dynacomment(this);" style="text-decoration: none;white-space: nowrap;">
								<span id='spancomment<s:property value="%{#indexval}"/>' style="vertical-align: top;">
									<s:if test="%{#secrowval[17]!=null}">
										<img height="15px" src="./images/addcomment.png">
										<%=propData.getPropertiesData("garuda.label.dynamicform.addorviewcomment") %>
									</s:if>
									<s:else>
										<s:if test="%{iseditable != 1}">
											<img height="15px" src="./images/addcomment.png">
											<%=propData.getPropertiesData("garuda.label.dynamicform.addcomment") %>
										</s:if>
									</s:else>
								</span>
							</a>
							<div class="commentbox" style="display: none;" align="left">
								<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
									<%=propData.getPropertiesData("garuda.label.dynamicform.commentbox") %>
								</div>
									<s:textarea name="%{formId}_Question[%{#indexval}].comment" cols="20" rows="5" value="%{#secrowval[17]}" cssClass="iseditable" onkeypress="commentclear(this,%{#indexval});" 
										cssStyle="width:95%" maxlength="3000" onchange="isChangeDone(this,'%{#secrowval[17]}','frmDatechangeCls');"/>
									<br/><span class='error' style="display:none;text-align:left;width:100%;float:left;" id='<s:property value="%{#indexval}"/>_Commenterror'><%=propData.getPropertiesData("garuda.label.dynamicform.errormsg") %></span>
									<br/><s:if test="%{iseditable!=1}">
									<input type="button" onclick="saveComment(this,'<s:property value="%{#indexval}"/>')" value='<%=propData.getPropertiesData("garuda.common.save") %>' tabindex="1"/>
									</s:if>
									<input type="button" onclick="hideDynaComment(this,'<s:property value="%{#indexval}"/>')" value='<%=propData.getPropertiesData("garuda.common.close") %>' tabindex="2"/>
								</div>
							</div>
						</s:if>
					</div>
					<s:if test="%{#secrowval[7]==1}">
						<div id="assess_<s:property value='%{#indexval}'/>" style="display: none;" class="assessMentDiv">
							
						</div>
					</s:if>
				</td>
				<td style="vertical-align: top;padding-top: 5px;padding-bottom: 5px;" width="25%">
				<s:if test="%{#secrowval[9]!=null}">
					<div style="display: none;">
						<s:select id="Ques%{#indexval}_UNRes" 
								list="%{#application.codeListValues[#secrowval[9]]}" 
								listKey="pkCodeId" listValue="description" style="display:none"/>
					</div>
				</s:if>
				<s:if test="%{#secrowval[11]!=null}">
				<s:generator val="%{#secrowval[12]}" separator=",">
					<select style="display:none" 
						id="<s:property value='%{formId}'/>_depentVal_<s:property value='%{#secrowval[11]}'/><s:if test='%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ}'><s:property value='%{#secrowval[12]}'/></s:if>">
						<s:iterator>
  							<option value='<s:property/>'><s:property/></option>
						</s:iterator>
					</select>
					</s:generator>
				</s:if>
				<s:if test="%{getCodeListDescById(#secrowval[2])==@com.velos.ordercomponent.util.VelosGarudaConstants@DROPDOWN_FIELD}">
				<s:if test="%{#secrowval[8]!=null}">
					<s:if test='%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ}'>
						<s:select name="%{formId}_Question[%{#indexval}].responsecode" tabindex="%{#indexval+2}"
								id="%{formId}_QuestionRes%{#indexval}" 
								list="%{#application.codeListValues[#secrowval[8]]}"
								listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
								onchange="showDynaAssess('%{#indexval}',this.id,%{#secrowval[7]},'1');checkMultiDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
								isChangeDone(this,'%{#secrowval[15]}','frmDatechangeCls');" 
								cssClass="tabclass %{formId}_unclass%{#secrowval[11]}%{#secrowval[12]} %{formId}_childQues_%{#secrowval[3]} findlen iseditable %{formId}_Ques%{#row}_%{#subrow}_s_res" value="%{#secrowval[15]}"/>
					</s:if>
					<s:else>
					<s:if test="%{#secrowval[11]!=null}">
						<s:if test="%{#secrowval[29]==1}">
							<s:select name="%{formId}_Question[%{#indexval}].responsecode" tabindex="%{#indexval+2}"
									id="%{formId}_QuestionRes%{#indexval}" 
									list="%{#application.codeListValues[#secrowval[8]]}"  
									listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
									onchange="showDynaAssess('%{#indexval}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
									isChangeDone(this,'%{#secrowval[15]}','frmDatechangeCls');" 
									cssClass="tabclass %{formId}_unclass%{#secrowval[11]} findlen iseditable %{formId}_Ques%{#row}_%{#subrow}_s_res" value="%{#secrowval[15]}"/>
						</s:if>
						<s:else>
							<s:select name="%{formId}_Question[%{#indexval}].responsecode" tabindex="%{#indexval+2}"
									id="%{formId}_QuestionRes%{#indexval}" disabled="true" 
									list="%{#application.codeListValues[#secrowval[8]]}" 
									listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
									onchange="showDynaAssess('%{#indexval}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
									isChangeDone(this,'%{#secrowval[15]}','frmDatechangeCls');" 
									cssClass="tabclass %{formId}_unclass%{#secrowval[11]} findlen iseditable %{formId}_Ques%{#row}_%{#subrow}_s_res" value="%{#secrowval[15]}"/>
						</s:else>
					</s:if>
					<s:else>
						<s:select name="%{formId}_Question[%{#indexval}].responsecode" tabindex="%{#indexval+2}"
								id="%{formId}_QuestionRes%{#indexval}" 
								list="%{#application.codeListValues[#secrowval[8]]}" 
								listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
								onchange="showDynaAssess('%{#indexval}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
								isChangeDone(this,'%{#secrowval[15]}','frmDatechangeCls');" 
								cssClass="tabclass %{formId}_unclass%{#secrowval[11]} findlen iseditable %{formId}_Ques%{#row}_%{#subrow}_s_res" value="%{#secrowval[15]}"/>
					</s:else>
					</s:else>
					</s:if>
				</s:if>
				<s:if test="%{getCodeListDescById(#secrowval[2])==@com.velos.ordercomponent.util.VelosGarudaConstants@TEXT_FIELD}">
					<s:if test="%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ}">
						
						<s:textfield name="%{formId}_Question[%{#indexval}].responsevalue" onchange="fieldclear(this);showDynaAssess('%{#indexval}',this.id,%{#secrowval[7]},'1');isChangeDone(this,'%{#quoteField}','frmDatechangeCls');" onkeyup="fn_parseQuote(this);" tabindex="%{#indexval+2}"
						id="%{formId}_QuestionRes%{#indexval}"
						cssClass="tabclass %{formId}_unclass%{#secrowval[11]}%{#secrowval[12]} %{formId}_childQues_%{#secrowval[3]} findlen iseditable %{formId}_Ques%{#row}_%{#subrow}_s_res" 
						value="%{#secrowval[16]}"/>
						<s:if test="%{#rowval[20]=='can_leuk_ind' || #rowval[20]=='inher_other_dises_ind' }">
						<s:if test="%{iseditable!=1}">
							<div class="addType_<s:property value='%{#secrowval[0]}'/>" style="float: right" >
								<s:a href="#" onclick='addtype(this,"%{#secrowval[25]}","%{#secrowval[0]}","%{#secrowval[1]}","%{#secrowval[24]}","tabclass %{formId}_unclass%{#secrowval[11]}%{#secrowval[12]} %{formId}_childQues_%{#secrowval[3]} findlen iseditable IsMandatory isReqPriorToShip","%{#secrowval[11]}",%{#row},%{#subrow},"%{formId}_unclass%{#secrowval[11]}%{#secrowval[12]}");'
								tabindex="-1">
									<%=propData.getPropertiesData("garuda.label.dynamicform.addtype") %>
								</s:a>
							</div>
						</s:if>
						</s:if>
					</s:if>
					<s:else>
					<s:if test="%{#secrowval[11]!=null}">
						<s:if test="%{#secrowval[29]==1}">
							<s:textfield name="%{formId}_Question[%{#indexval}].responsevalue" onchange="fieldclear(this);isChangeDone(this,'%{#quoteField}','frmDatechangeCls');" onkeyup="fn_parseQuote(this);"  tabindex="%{#indexval+2}"
								id="%{formId}_QuestionRes%{#indexval}" 
								cssClass="tabclass %{formId}_unclass%{#secrowval[11]} findlen iseditable" value="%{#secrowval[16]}"/>
						</s:if>
						<s:else>
							<s:textfield name="%{formId}_Question[%{#indexval}].responsevalue" onchange="fieldclear(this);isChangeDone(this,'%{#quoteField}','frmDatechangeCls');" onkeyup="fn_parseQuote(this);"  tabindex="%{#indexval+2}"
								id="%{formId}_QuestionRes%{#indexval}" disabled="true" 
								cssClass="tabclass %{formId}_unclass%{#secrowval[11]} findlen iseditable" value="%{#secrowval[16]}"/>
						</s:else>
					</s:if>
					<s:else>
						<s:textfield name="%{formId}_Question[%{#indexval}].responsevalue" onchange="fieldclear(this);isChangeDone(this,'%{#quoteField}','frmDatechangeCls');" onkeyup="fn_parseQuote(this);" tabindex="%{#indexval+2}"
								id="%{formId}_QuestionRes%{#indexval}" 
								cssClass="tabclass %{formId}_unclass%{#secrowval[11]} findlen iseditable" value="%{#secrowval[16]}"/>
					</s:else>
					</s:else>
				</s:if>
				<s:if test="%{getCodeListDescById(#secrowval[2])==@com.velos.ordercomponent.util.VelosGarudaConstants@MULTISELECT_FIELD}">
					<s:if test="%{#secrowval[20]=='severe_aut_imm_sys_disordr_rel'}">
						<s:if test="%{#secrowval[8]!=null}">
							<s:iterator value="%{#application.codeListValues[#secrowval[8]]}">
								<s:if test="%{subType=='babymom'}">
									<s:set name="fmhqCodeListPk" value="%{pkCodeId}"></s:set>
									<s:hidden name="fmhqCodeListPkH" id="fmhqCodeListPkH" value="%{pkCodeId}"/>
								</s:if>
								<s:if test="%{subType=='babydad'}">
									<s:set name="fmhqCodeListDadPk" id="fmhqCodeListDadPk" value="%{pkCodeId}"/>
								</s:if>
								<s:if test="%{subType=='babysib'}">
									<s:set name="fmhqCodeListSibPk" id="fmhqCodeListSibPk" value="%{pkCodeId}"/>
								</s:if>
							</s:iterator>
							<s:set name='momSelected' value='0'/>
							<s:set name='dadSelected' value='0'/>
							<s:set name='sibSelected' value='0'/>
							<s:generator val="%{#secrowval[16]}" separator="," var="currentVal">
								<s:iterator value="%{currentVal}" var="resPonsetext">
									<s:if test="%{#resPonsetext==#fmhqCodeListPk}">
										<s:set name='momSelected' value='1'/>
									</s:if>
									<s:elseif test="%{#resPonsetext==#fmhqCodeListDadPk}">
										<s:set name='dadSelected' value='1'/>
									</s:elseif>
									<s:elseif test="%{#resPonsetext==#fmhqCodeListSibPk}">
										<s:set name='sibSelected' value='1'/>
									</s:elseif>
								</s:iterator>
							</s:generator>
							
							<s:if test="%{isFMHQNewForm==false}">
								<s:generator val="%{#secrowval[16]}" separator="," var="currentVal">
								<!--<s:select name="%{formId}_Question[%{#indexval}].responsevalue" tabindex="%{#indexval+2}"
										id="%{formId}_QuestionRes%{#indexval}" 
										list="%{#application.codeListValues[#secrowval[8]]}" 
										listKey="pkCodeId" listValue="description"
										onchange="showDynaAssess('%{#indexval}',this.id,%{#secrowval[7]},'1');checkMultiDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
										isChangeDone(this,'%{getCommaSeparatedData(#secrowval[16])}','frmDatechangeCls');"
										multiple="true" size="5" cssClass="tabclass %{formId}_unclass%{#secrowval[11]}%{#secrowval[12]} %{formId}_childQues_%{#secrowval[3]} findlen iseditable %{formId}_Ques%{#row}_%{#subrow}_s_res multiSelectBox"
										value="%{getCommaSeparatedData(#secrowval[16])}" cssStyle="height:auto;width:auto;min-width:50px;"/>-->
								<select multiple="multiple" name='<s:property value="%{formId}"/>_Question[<s:property value="%{#indexval}"/>].responsevalue' tabindex='<s:property value="%{#indexval+2}"/>'
										id='<s:property value="%{formId}"/>_QuestionRes<s:property value="%{#indexval}"/>'
										style="height:auto;width:auto;min-width:50px;"
										class="tabclass <s:property value='%{formId}'/>_unclass<s:property value='%{#secrowval[11]}'/><s:property value='%{#secrowval[12]}'/> <s:property value='%{formId}'/>_childQues_<s:property value='%{#secrowval[3]}'/> findlen iseditable <s:property value='%{formId}'/>_Ques<s:property value='%{#row}'/>_<s:property value='%{#subrow}'/>_s_res multiSelectBox fmhqQuestion17Child"
										onchange="showDynaAssess('<s:property value='%{#indexval}'/>',this.id,<s:property value='%{#secrowval[7]}'/>,'1');checkMultiDependent(this.id,'<s:property value='%{#secrowval[0]}'/>','<s:property value='%{formId}'/>');fieldclear(this);
										isChangeDone(this,'<s:property value='%{#secrowval[16]}'/>','frmDatechangeCls');">
								
									<s:iterator value="%{#application.codeListValues[#secrowval[8]]}">
										<s:if test="%{subType=='babymom'}">
											<s:if test="%{#momSelected==1}"> 
												<option value='<s:property value="%{pkCodeId}"/>' selected="selected"><s:property value="%{description}"/></option>
											</s:if>
											<s:else>
												<option value='<s:property value="%{pkCodeId}"/>'><s:property value="%{description}"/></option>
											</s:else>
										</s:if>
										<s:elseif test="%{subType=='babydad'}">
											<s:if test="%{#dadSelected==1}"> 
												<option value='<s:property value="%{pkCodeId}"/>' selected="selected" disabled="disabled"><s:property value="%{description}"/></option>
											</s:if>
											<s:else>
												<option value='<s:property value="%{pkCodeId}"/>'><s:property value="%{description}"/></option>
											</s:else>
										</s:elseif>
										<s:elseif test="%{subType=='babysib'}">
											<s:if test="%{#sibSelected==1}"> 
												<option value='<s:property value="%{pkCodeId}"/>' selected="selected" disabled="disabled"><s:property value="%{description}"/></option>
											</s:if>
											<s:else>
												<option value='<s:property value="%{pkCodeId}"/>'><s:property value="%{description}"/></option>
											</s:else>
										</s:elseif>
									</s:iterator>
								</select>
									</s:generator>
							</s:if>
							<s:else>
								<s:select name="%{formId}_Question[%{#indexval}].responsevalue" tabindex="%{#indexval+2}"
										id="%{formId}_QuestionRes%{#indexval}" 
										list="%{#application.codeListMapByIds[#fmhqCodeListPk]}" 
										listKey="pkCodeId" listValue="description"
										onchange="showDynaAssess('%{#indexval}',this.id,%{#secrowval[7]},'1');checkMultiDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
										isChangeDone(this,'%{getCommaSeparatedData(#secrowval[16])}','frmDatechangeCls');"
										multiple="true" size="5" cssClass="tabclass %{formId}_unclass%{#secrowval[11]}%{#secrowval[12]} %{formId}_childQues_%{#secrowval[3]} findlen iseditable %{formId}_Ques%{#row}_%{#subrow}_s_res multiSelectBox fmhqQuestion17Child"
										value="%{getCommaSeparatedData(#secrowval[16])}" cssStyle="height:auto;width:auto;min-width:50px;"/>
							</s:else>
						</s:if>
					</s:if>
					<s:else>
						<s:if test="%{#secrowval[8]!=null}">
							<s:select name="%{formId}_Question[%{#indexval}].responsevalue" tabindex="%{#indexval+2}"
									id="%{formId}_QuestionRes%{#indexval}" 
									list="%{#application.codeListValues[#secrowval[8]]}" 
									listKey="pkCodeId" listValue="description"
									onchange="showDynaAssess('%{#indexval}',this.id,%{#secrowval[7]},'1');checkMultiDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
									isChangeDone(this,'%{getCommaSeparatedData(#secrowval[16])}','frmDatechangeCls');"
									multiple="true" size="5" cssClass="tabclass %{formId}_unclass%{#secrowval[11]}%{#secrowval[12]} %{formId}_childQues_%{#secrowval[3]} findlen iseditable %{formId}_Ques%{#row}_%{#subrow}_s_res multiSelectBox"
									value="%{getCommaSeparatedData(#secrowval[16])}" cssStyle="height:auto;width:auto;min-width:50px;"/>
						</s:if>
					</s:else>
				</s:if>
					<span class='error fieldMan' style="display:none" id='errorMsg_<s:property value="%{formId}"/>_QuestionRes<s:property value="%{#indexval}"/>'>
						<%=propData.getPropertiesData("garuda.label.dynamicform.errormsg") %>
					</span>
					<span class='error <s:property value="%{formId}"/>_Ques<s:property value="%{#row}"/>_<s:property value="%{#subrow}"/>_s_assessError' style="display:none" id="assesError_<s:property value='%{#indexval}'/>">
						<%=propData.getPropertiesData("garuda.label.dynamicform.assessmentmsg") %>
					</span>
					<div>
						<a href="#" onclick="showDynaAssessLink(this,'<s:property value="%{#indexval}"/>','<s:property value="%{formId}"/>_QuestionRes<s:property value="%{#indexval}"/>',<s:property value='%{#secrowval[7]}'/>,'1');"
							id="showasses_<s:property value='%{#indexval}'/>" class="<s:property value="%{formId}"/>_Ques<s:property value="%{#row}"/>_<s:property value="%{#subrow}"/>_s_showassess" style="display: none;">
							<%=propData.getPropertiesData("garuda.label.dynamicform.showassess") %>
						</a>
						<a id="hideasses_<s:property value='%{#indexval}'/>" href="#" onclick="hideDynaAssess(this,'<s:property value="%{#indexval}"/>');" style="display: none;"
							class="<s:property value="%{formId}"/>_Ques<s:property value="%{#row}"/>_<s:property value="%{#subrow}"/>_s_hideassess">
							<%=propData.getPropertiesData("garuda.label.dynamicform.hideassess") %>
						</a>
					</div>
				</td>
			</tr>
			<s:set name="subrow" value="%{#subrow+1}"/>
			<s:set name="indexval" value="%{#indexval+1}"/>
		</s:if>
		</s:iterator>
		<s:if test="%{#rowval[3]==null}">
			<s:set name="row" value="%{#row+1}"></s:set>
		</s:if>
		</s:if>
		</s:iterator>
		</s:iterator>
		<s:hidden name="indexvalue" id="indexvalue" value="%{#indexval}"></s:hidden>
		</tbody>
	</table>
</div>
</div>
</div>
<br/>
<table width="100%"  bgcolor="#cccccc" id="eSignTable">
	<tr>
	<td style="float: right;">
		<table>
			<tr valign="baseline" >
				<td width="70%">
					<s:if test='%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ}'>
						<jsp:include page="./cb_esignature.jsp" flush="true">
				   			<jsp:param value='FMHQsaveForm' name="submitId"/>
							<jsp:param value='FMHQsaveForminvalid' name="invalid" />
							<jsp:param value='FMHQsaveFormminimum' name="minimum" />
							<jsp:param value='FMHQsaveFormpass' name="pass" />
							<jsp:param value='FMHQsaveFormesign' name="esign" />
		   				</jsp:include>
		   			</s:if>
		   			<s:if test='%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_MRQ}'>
						<jsp:include page="./cb_esignature.jsp" flush="true">
				   			<jsp:param value='MRQsaveForm' name="submitId"/>
							<jsp:param value='MRQsaveForminvalid' name="invalid" />
							<jsp:param value='MRQsaveFormminimum' name="minimum" />
							<jsp:param value='MRQsaveFormpass' name="pass" />
							<jsp:param value='MRQsaveFormesign' name="esign" />
		   				</jsp:include>
		   			</s:if>
	   			</td>
	    		<td width="15%" align="right">
	    			<button type="button" name="saveForm" id="<s:property value='%{formId}'/>saveForm" onclick="checkValid();" disabled="disabled" class="saveFrm">
	    				<%=propData.getPropertiesData("garuda.unitreport.label.button.submit") %>
	    			</button>
	    		</td>
	    		<td width="15%">
	    			<button type="button" name="closemodel" id="<s:property value='%{formId}'/>closemodel" onclick="closeModals('<s:property value="%{formId}"/>ModalForm')">
	    				<%=propData.getPropertiesData("garuda.unitreport.label.button.cancel") %>
	    			</button>
	    		</td>
	    	</tr>
		</table>
	</td>
</table>
</s:if>
<s:else>
<s:form><button name="addFMHQForm" type="button" id="addFMHQForm" onclick="hideSomeMoreDivs('fmhqTargetParent');getFormInModal('<s:text name="garuda.completeReqInfo.label.editFmhqForm"></s:text>'+' <s:text name="garuda.message.modal.headerregid"/> '+'<s:property value="cdrCbuPojo.registryId" />','getDynamicFrm?formId=FMHQ&isCompInfo=1&editFlag=1&entityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_EDIT_FMHQ" />','FMHQModalForm','fmhqTarget',true,true);">
						<s:text name="garuda.label.forms.addfmhq"></s:text>	
				</button></s:form>
</s:else>
</s:form>
</div>
<script>
var focusFlag = 0;
var mrqlabel = $j('#mrqlabel').val();
var fmhqlabel = $j('#fmhqlabel').val();
var keyPressFlag = false;
var elements = $j('.findlen').get(), i, len = elements.length;
var formId = $j('#formId').val();
var iseditable = $j('#iseditable').val();

function dynacomment(obj){
	var nextDiv = $j(obj).next('div');
//	$j(nextDiv).next('div').css(absolute:absolute);
	$j(nextDiv).next('div').css('padding','10px');
	$j(nextDiv).toggle();
	$j(nextDiv).find('textarea').focus();
}

function showDynaAssess(rowId,id,condition,code){
	if(condition == 1){
		var flag = false;
		var unResId = "Ques"+rowId+"_UNRes";
		var divId = 'assess_'+rowId;
		var showasses = "showasses_"+rowId;
		var hideasses = "hideasses_"+rowId;
		var assesError = "assesError_"+rowId;
		var assessmentPk = "assessmentPk_"+rowId;
		var shname = $j('#'+rowId+"_assessshname").val();
		var assessDivName = $j('#assessDivName_'+rowId).val();
		var rowId = $j('#'+id).parent().parent().attr('id');
		var selectCount = $j("#"+id+" option:selected").length;
			rowId = rowId+"_td";
		if(!$j("#"+id).is("input:text")){
			$j("#"+id+" option:selected").each(function(index,el){
				$j("#"+unResId+" option").each(function(index1,el1){
					if($j(el).text() == $j(el1).text()){
						flag = true;
					}
				});
			});
		}else{
			if($j("#"+id).val()!="" && $j("#"+id).val()!=null && $j("#"+id).val()!="undefined"){
				flag = true;
			}
		}

		if(flag){
			if(code == '1'){
				if(!$j("#"+id).is("input:text")){
					$j("#"+id+" option").each(function(ind,el){
						setTimeout(function(){
							var responsestr = $j(el).text();
							var responseval = $j(el).val();
							var assessCloseDiv = responseval+'_'+assessDivName;
							var url = 'getassessment?entityId='+$j('#cordID').val()+'&responsestr='+responsestr+'&assessDivName='+assessCloseDiv+'&responseval='+responseval;
							if($j(el).is(':selected')){
								if($j('#'+assessCloseDiv).html() != null && $j('#'+assessCloseDiv).html() != "" && $j('#'+assessCloseDiv).html() != 'undefined'){
									$j('#'+assessCloseDiv).show();
								}else{
									refreshassessmentDiv(url,rowId,divId,assessCloseDiv);
								}
							}else{
								$j('#'+assessCloseDiv).remove();
							}
						},0);
					});
				}else{
					setTimeout(function(){
						var responsestr = $j("#"+id).val();
						var responseval = $j("#"+id).val();
						var oldval=$j("#textval_"+rowId).val();
						responseval = $j.trim(responseval);
							var lastChar = responseval.substr(responseval.length - 1);
							if(lastChar.match(/[_\W]/))
		                      {
								 responseval=responseval+"QqQqQ";
			                  }
							responseval = responseval.replace(/[^a-zA-Z0-9]/g, "");
						var textFlag = true;
						var assessCloseDiv = responseval+'_'+assessDivName;
						
						var url='getassessment?entityId='+$j('#cordID').val();
						if( responseval!="" && responseval !=null ){
							if(oldval==responsestr){
							if($j('#'+assessCloseDiv).html()!=null && $j('#'+assessCloseDiv).html()!="" && $j('#'+assessCloseDiv).html()!='undefined'){
								$j('#'+assessCloseDiv).show();
							}
							}else{
								$j('#'+divId).html("");
								//$j('#'+id).val(responsestr.replace(/'/g, "''"));
								$j('#textflag').remove();
								var textflaginput = document.createElement("input");
								textflaginput.setAttribute("type", "hidden");
								textflaginput.setAttribute("name", "textflag");
								textflaginput.setAttribute("id","textflag");
								textflaginput.setAttribute("value", 'true');
								document.getElementById(rowId).appendChild(textflaginput);

								$j('#responsestr').remove();
								var responsestrinput = document.createElement("input");
								responsestrinput.setAttribute("type", "hidden");
								responsestrinput.setAttribute("name", "responsestr");
								responsestrinput.setAttribute("id","responsestr");
								responsestrinput.setAttribute("value", responsestr);
								document.getElementById(rowId).appendChild(responsestrinput);

								$j('#responseval').remove();
								var responsevalinput = document.createElement("input");
								responsevalinput.setAttribute("type", "hidden");
								responsevalinput.setAttribute("name", "responseval");
								responsevalinput.setAttribute("id","responseval");
								responsevalinput.setAttribute("value", responseval);
								document.getElementById(rowId).appendChild(responsevalinput);

								$j('#assessDivName').remove();
								var assessDivNameinput = document.createElement("input");
								assessDivNameinput.setAttribute("type", "hidden");
								assessDivNameinput.setAttribute("name", "assessDivName");
								assessDivNameinput.setAttribute("id","assessDivName");
								assessDivNameinput.setAttribute("value", assessCloseDiv);
								document.getElementById(rowId).appendChild(assessDivNameinput);

								
								refreshassessmentDiv(url,rowId,divId,assessCloseDiv);
							}
						}else{
							$j('#'+assessCloseDiv).hide();
						}
					},0);
				}
				$j('#'+divId).show();
				$j('#'+hideasses).show();
				$j('#'+showasses).hide();
				//if($j('#isCompInfo').val() != "1"){
				if(!$j("#"+id).is("input:text")){
					if($j('#'+assessmentPk).val() == "" || $j('#'+assessmentPk).val() == null || $j('#'+assessmentPk).val()!=selectCount){
						$j('#'+id).addClass('unexpectedRes');
					}else{
						$j('#'+id).removeClass('unexpectedRes');
					}
				}else{
					if($j('#'+assessmentPk).val() == "" || $j('#'+assessmentPk).val() == null){
						$j('#'+id).addClass('unexpectedRes');
					}
				}
				//}
				if($j('#isCompInfo').val() == "1"){
					if(!$j("#"+id).is("input:text")){
						if($j('#'+assessmentPk).val() == "" || $j('#'+assessmentPk).val() == null || $j('#'+assessmentPk).val()!=selectCount){
							$j('#'+id).addClass('assesReqPriorToShip');
						}else{
							$j('#'+id).removeClass('assesReqPriorToShip');
						}
					}else{
						if($j('#'+assessmentPk).val() == "" || $j('#'+assessmentPk).val() == null){
							$j('#'+id).addClass('assesReqPriorToShip');
						}
					}
				}
			}else{
				$j('#'+hideasses).hide();
				$j('#'+showasses).show();
				if($j('#isCompInfo').val() == "1"){
					if(!$j("#"+id).is("input:text")){
						if($j('#'+assessmentPk).val() == "" || $j('#'+assessmentPk).val() == null || $j('#'+assessmentPk).val()!=selectCount){
							$j('#'+assesError).show();
							$j('#'+id).addClass('assesReqPriorToShip');
						}
					}else{
						if($j('#'+assessmentPk).val() == "" || $j('#'+assessmentPk).val() == null){
							$j('#'+assesError).show();
							$j('#'+id).addClass('assesReqPriorToShip');
						}
					}
				}
				if(!$j("#"+id).is("input:text")){
					if($j('#'+assessmentPk).val() == "" || $j('#'+assessmentPk).val() == null || $j('#'+assessmentPk).val()!=selectCount){
						$j('#'+id).addClass('unexpectedRes');
					}
				}else{
					if($j('#'+assessmentPk).val() == "" || $j('#'+assessmentPk).val() == null){
						$j('#'+id).addClass('unexpectedRes');
					}
				}
			}
		}else{
			if(code == '1'){
				$j('#'+divId).empty();
				$j('#'+showasses).hide();
				$j('#'+hideasses).hide();
				if($j('#isCompInfo').val() == "1"){
					if(!$j("#"+id).is("input:text")){
						if($j("#"+id).hasClass("multiSelectBox")){
							if($j('#'+assessmentPk).val()==selectCount){
								$j('#'+assesError).hide();
								$j('#'+id).removeClass('assesReqPriorToShip');
							}
						}else{
							$j('#'+assesError).hide();
							$j('#'+id).removeClass('assesReqPriorToShip');
						}
					}else{
						$j('#'+assesError).hide();
						$j('#'+id).removeClass('assesReqPriorToShip');
					}
				}
				if(!$j("#"+id).is("input:text")){
					if($j("#"+id).hasClass("multiSelectBox")){
						if($j('#'+assessmentPk).val()==selectCount){
							$j('#'+id).removeClass('unexpectedRes');
						}
					}else{
						$j('#'+id).removeClass('unexpectedRes');
					}
				}else{
					$j('#'+id).removeClass('unexpectedRes');
				}
			}else{
				$j('#'+showasses).hide();
				$j('#'+hideasses).hide();
				if($j('#isCompInfo').val() == "1"){
					if(!$j("#"+id).is("input:text")){
						if($j("#"+id).hasClass("multiSelectBox")){
							if($j('#'+assessmentPk).val()==selectCount){
								$j('#'+assesError).hide();
								$j('#'+id).removeClass('assesReqPriorToShip');
							}
						}else{
							$j('#'+assesError).hide();
							$j('#'+id).removeClass('assesReqPriorToShip');
						}
					}else{
						$j('#'+assesError).hide();
						$j('#'+id).removeClass('assesReqPriorToShip');
					}
				}
				if(!$j("#"+id).is("input:text")){
					if($j("#"+id).hasClass("multiSelectBox")){
						if($j('#'+assessmentPk).val()==selectCount){
							$j('#'+id).removeClass('unexpectedRes');
						}
					}else{
						$j('#'+id).removeClass('unexpectedRes');
					}
				}else{
					$j('#'+id).removeClass('unexpectedRes');
				}
			}
		}
	}
}
function refreshassessmentDiv(url,rowId,divId,assessDivName){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data:$j('#'+rowId+' *').serialize(),
        success: function (result){
        	$j("#"+divId).append(result);
        	if(iseditable == "1"){
        		$j("#"+divId).find(".iseditable").attr("disabled","disabled");
			}
        }
	});
}
function checkDependent(id,index,formId){
	var depentValId = formId+'_depentVal_'+index;
	var depentClass = formId+'_unclass'+index;
	var unreqFlag = formId+'_unreqFlag'+index;
	var unreqPreFlag = formId+'_unreqPreFlag'+index;
	var mandatory = formId+'_classMan'+index;
	var flag = false;
	$j("#"+id+" option:selected").each(function(index,el){
			$j("#"+depentValId+" option").each(function(index1,el1){
				if($j(el).val() == $j(el1).val()){
					flag = true;
				}
			});
		});
	if(flag){
		if($j("#"+depentValId).html()){
			$j('.'+depentClass).removeAttr('disabled');
			if(focusFlag == 0){
				if(keyPressFlag){
					//$j('.'+depentClass).first().focus();
					keyPressFlag = false;
				}
			}
			$j('.'+depentClass).each(function(index,el){
				if($j('#'+unreqFlag).val() == "1"){
					$j(el).addClass('IsMandatory');
						$j('.'+mandatory).show();
					}
				if($j('#isCDRUser').val() == "true"){
					if($j('#isCompInfo').val() == "1"){
						if($j('#'+unreqPreFlag).val() == "1"){
							$j(el).addClass('isReqPriorToShip');
							$j('.'+mandatory).show();
							if($j(el).val()==""){
								$j(el).addClass('criHighlight');
							}
						}
					}
				}
			});	
		}
	}else{
		if(($j("#"+depentValId).html())){
			$j('.'+depentClass).attr('disabled','disabled');
			$j('.'+mandatory).hide();
			$j('.'+depentClass).each(function(index,el){
				$j(el).val("");
				$j('#errorMsg_'+this.id).hide();
				if($j('#'+unreqFlag).val() == "1"){
					$j(el).removeClass('IsMandatory');
				}
				if($j('#isCDRUser').val() == "true"){
					if($j('#isCompInfo').val() == "1"){
						if($j('#'+unreqPreFlag).val() == "1"){
							$j(el).removeClass('isReqPriorToShip');
						}
					}
				}
			});
			$j('.'+depentClass).removeClass('criHighlight');
			$j('.'+depentClass).each(function(index,el){
				$j(el).trigger('change');
			});
		}
	}
}
Array.prototype.exists = function(search){
	for (var e = 0; e < this.length; e++)
		if (this[e].value == search) return true;
	return false;
}
function checkMultiDependent(id,pkques,formId){
	var listValue = $j('#'+id+" option").get(), lSize = listValue.length, l;
	var childQues = formId+'_childQues_'+pkques;
	var quesCodH = formId+'_quesCodH_'+pkques;
		quesCodH = $j('#'+quesCodH).val();
	var parQuesCodH = formId+'_parquesCodH_'+pkques;
		parQuesCodH = $j('#'+parQuesCodH).val();
	var flag = false;
	for(l = 0; l < lSize; l++){
		var thisVal = $j(listValue[l]).val();
		var depentValId = formId+'_depentVal_'+pkques+thisVal;
		var depentClass = formId+'_unclass'+pkques+thisVal;
		var unreqFlag = formId+'_unreqFlag'+pkques;
		var unreqPreFlag = formId+'_unreqPreFlag'+pkques;
		var mandatory = formId+'_classMan'+pkques;
		var depenValue = $j("#"+depentValId+" option").get();
		var childSize;
		if(parQuesCodH=='severe_aut_imm_sys_disordr_ind'){
			$j('.fmhqQuestion17Child > option').each(function(index,el){
				if(!$j(el).is(':disabled'))
					childSize = index+1;
			});
		}
		if(depenValue.exists(thisVal) && $j(listValue[l]).is(":selected")){
			if(quesCodH == 'can_leuk_ind' || quesCodH == 'inher_other_dises_ind'){
				$j('.tr_'+depentClass+":first").removeClass('hidechild');
				$j('.'+depentClass+":first").next('div').show();
			}else{
				$j('.tr_'+depentClass).removeClass('hidechild');
				$j('.tr_'+depentClass).show();
			}
			
			if($j('#'+unreqFlag).val() == "1"){
				$j('.'+depentClass).addClass('IsMandatory');
				$j('.'+mandatory).show();
			}
			if($j('#isCDRUser').val() == "true"){
				if($j('#isCompInfo').val() == "1"){
					if($j('#'+unreqPreFlag).val() == "1"){
						$j('.'+depentClass).addClass('isReqPriorToShip');
						$j('.'+mandatory).show();
						$j('.'+depentClass).each(function(index,el){
							if($j(el).val()==""){
								$j(el).addClass('criHighlight');
							}
						});
					}
				}
			}
			if(parQuesCodH == 'severe_aut_imm_sys_disordr_ind' && ( $j("#isFMHQNewForm").val()=="true" || childSize == 1 )){
				flag = true;
				$j('.'+depentClass).each(function(index,el){
					//FMHQ_QuestionRes84
					 var elId = $j(el).attr("id");
					     elId = elId.replace("FMHQ_QuestionRes","");
					     elId = parseInt(elId)+1;
					     elId = "FMHQ_QuestionRes"+elId;
					 $j(el).val($j("#fmhqCodeListPkH").val());
			         $j(el).triggerHandler('change');
			         $j("#"+elId).focus();
				});
			}
		}else{
			if(parQuesCodH == 'severe_aut_imm_sys_disordr_ind' && ( $j("#isFMHQNewForm").val()=="true" || childSize == 1 ) && flag){
			
			}else{
				$j('.'+childQues).val("");
			}
			$j('.tr_'+depentClass).addClass('hidechild');
			if($j('#'+unreqFlag).val() == "1"){
				$j('.'+depentClass).val("").removeClass('unexpectedRes').removeClass('IsMandatory').trigger('change');
			}
			if($j('#isCDRUser').val() == "true"){
				if($j('#isCompInfo').val() == "1"){
					if($j('#'+unreqPreFlag).val() == "1"){
						$j('.'+depentClass).val("").removeClass('unexpectedRes').removeClass('isReqPriorToShip').removeClass('criHighlight').trigger('change');
					}
				}
			}
			if(quesCodH == 'can_leuk_ind' || quesCodH == 'inher_other_dises_ind'){
				$j('.'+depentClass).next('div').show();
			}
		}
	}
}

function refreshDynaForm(url,divname,formId1){
	showprogressMgs();
	var formId = $j('#formId').val();
	var changeFlag = divChanges("frmDatechangeCls",$j('#'+formId+'ModalForm').html());
	if(!changeFlag){
		changeFlag = divChanges("assessmentChngCls",$j('#'+formId+'ModalForm').html());
	}
	url = url+'?changeFlag='+changeFlag;
	$j('#dynaFormDiv textarea').removeAttr('disabled');
	$j('#dynaFormDiv select>option').removeAttr('disabled');
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data:$j('#'+formId1).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
            var $response = $j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            		$j("#"+divname).html(result);
            		$j('#dynaInfo').hide();
            		$j('#eSignTable').hide();
                	$j('#dynaUpdate').show();
                	if($j('#isCordEntry').val()=="1"){
                		    if(formId == mrqlabel){
							if(mrqcount == 0){
								mrqcount++;
								val = parseInt((mrqcount*100)/(noOfMandatoryMrqField));
								$j("#MRQbar").progressbar({
									value: val
								}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
								addTotalCount();
							}	
                    	}else if(formId == fmhqlabel){
                        	if(fmhqcount == 0){
								fmhqcount++;		  
								val = parseInt((fmhqcount*100)/(noOfMandatoryFmhqField));
								$j("#FMHQbar").progressbar({
									value: val
								}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
								addTotalCount();
                        	}
                        }
                	}else{
                		refreshMultipleDivsOnViewClinical('1024','<s:property value="cdrCbuPojo.cordID"/>');
                		if(changeFlag){
                			if($j('#fcrFlag').val() == "Y"){
                				refreshMultipleDivsOnViewClinical('2','<s:property value="cdrCbuPojo.cordID"/>');
                				}
                        	}
                   	}
                }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
	closeprogressMsg();
}
$j(function(){
	//alert($j('#fkCordCbuStatus').val());
	showprogressMgs();
	focusFlag = 1;
	var seqnumber = $j('#seqnumber').val();
	if(seqnumber == "" || seqnumber == null){
		$j('#seqnumber').val("1");
	}
	if(formId == mrqlabel){
		$j('#dynamicForm').find(':text').attr('maxlength','100');
	}else if(formId == fmhqlabel){
		$j('#dynamicForm').find(':text').attr('maxlength','30');
	}
	var licenseid = $j('#licenseid').val();
	var unLicensedPk = $j("#unLicensedPk").val();
	if($j("#currentFMHQV").val()==$j("#FMHQformversion").val()){
		for( i = 0; i < len; i++ ) {
			var id = elements[i].getAttribute('id');
			var pId = $j(elements[i]).parent().parent().attr('id');
			if( pId != null && pId != "" ){
				if ($j('#'+pId).css("display") != 'none' && !$j('#'+id).is(':disabled')){
					var fieldIndex = $j('#'+formId+'_index'+i).val();
					var unreqFlag = formId+'_unreqFlag'+fieldIndex;
					var unreqPreFlag = formId+'_unreqPreFlag'+fieldIndex;
					var condition = $j('#'+formId+'_assesCon'+i).val();
								if( $j('#'+unreqFlag).val() == "1" ){
									$j(elements[i]).addClass('IsMandatory');
									$j('#'+formId+'_mandatory'+fieldIndex).show();
								}
							if( $j('#isCompInfo').val() == "1" ){
								if( $j('#isCDRUser').val() == "true" ){
									if( $j('#'+unreqPreFlag).val() == "1" ){
										$j(elements[i]).addClass('isReqPriorToShip');
										$j('#'+formId+'_mandatory'+fieldIndex).show();
									}
								}
							}
					if(formId == fmhqlabel){
						hideaddType(fieldIndex);
					}
					if(condition == 1){
						showDynaAssess(i,id,condition,'0');
					}
				}
			}
		}
		if( $j('#isCompInfo').val() == "1" ){
			if( $j('#isCDRUser').val() == "true" ){
				$j('.isReqPriorToShip').each(function(){
					//$j(this).css("border", "");
					if($j(this).val() == null || $j(this).val() == "" || $j(this).val() == "-1"){
						$j(this).addClass('criHighlight');
					}
				});
				$j('.assesReqPriorToShip').each(function(){
					$j(this).addClass('unexpectedRes');
				});
			}
		}
	}
	if(iseditable == "1"){
		if(formId == mrqlabel){
			$j("#dynaFormDiv").find(".iseditable").attr("disabled","disabled");
		}
		if(formId == fmhqlabel){
			$j("#dynaFormDiv").find(".iseditable").attr("disabled","disabled");
		}
		$j("#eSignTable").hide();
	}
	focusFlag = 0;
	setMultiselectHeidht();
	var lastIndex = $j('#indexvalue').val(); 
	if(formId == mrqlabel){
		$j('#MRQsaveFormesign').addClass('tabclass');
	}
	if(formId == fmhqlabel){
		$j('#FMHQsaveFormesign').addClass('tabclass');
	}
	setTabIndex();
	formDatePick();
	var validator = $j("#dynamicForm").validate({
		invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    }
	});
	closeprogressMsg();	
});
$j(document).ready(function(){
	if($j('#iseditable').val()=="1"){
	$j('#dynaFormDiv .dpDate').each(function(){
		if($j(this).is(":disabled")){
			var imgElement = $j(this).parent().find("img");
			if(imgElement.hasClass('ui-datepicker-trigger')){
				imgElement.unbind("click");
			}
		}
	});
	}
});
function formDatePick(){
	getClearButton();
	$j( ".formdatePicWMaxDate" ).addClass("dpDate").each(function(){
		if($j(this).is(":disabled")){
			$j(this).datepicker("disable");
		}
		var s = this;
        var selectHolder = $j('<span>').css({ position: 'relative' });
	    $j(this).wrap(selectHolder);
        var titleSpan = $j('<label>').attr({'class': 'tooltipspan','id':'placeHolderOfid_'+$j(this).attr("id")}).html("mm/dd/yyyy")
	                				.css({'width':0,'height':0,'top':$j(s).position().top,'left':$j(s).position().left}).click(function(){
	                					var txtFldId = $j(this).attr("id"); 
	                					txtFldId = txtFldId.replace("placeHolderOfid_","");
	                					if(!$j("#"+txtFldId).is(":disabled")){
		                					$j("#"+txtFldId).focus();
			                			}
	                				});
		if($j(this).val()!=""){
			$j(titleSpan).css({'display':'none'});
		}else{
			$j(titleSpan).css({'display':'block'});
		}
        $j(this).parent().append(titleSpan);
	}).datepicker({
		dateFormat: 'M dd, yy',
		//maxDate: new Date(y, m, d),
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true, 
		prevText: '',
		nextText: '',
		showOn: "button",
		buttonImage: "./images/calendar-icon.png",
		buttonImageOnly: true,

        /* blur needed to correctly handle placeholder text */
        onSelect: function(dateText, inst) {
              this.fixFocusIE = true;
              $j(this).blur().change().focus();
        },
	    onChangeMonthYear:function(y, m, i){                                
	        var d = i.selectedDay;
	        $j(this).datepicker('setDate', new Date(y, m - 1, d));
	        $j(this).val(dateFormat(new Date(y, m - 1, d),'mm/dd/yyyy'));
	    }
	}).focus(function(){
		$j("#placeHolderOfid_"+$j(this).attr("id")).hide();
		if($j(this).val()!=""){
			var thisdate = new Date($j(this).val());
			if(!isNaN(thisdate)){
				var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
				$j(this).val(formattedDate);
			}
		}
	}).keydown(function(e) {
		  var val = $j(this).val();
		  if(e.keyCode==191 || e.keyCode==111){
			  return false;
		  }else if(e.keyCode!=8 && e.keyCode!=46 && e.keyCode!=191 && e.keyCode!=111){
			  if(val.length==2 || val.length==5){
				  $j(this).val(val+"/");
			  }
		  }else if(e.keyCode==8){
			if(val==""){
				return false;
			}
		  }
	}).focusout(function(){
		if($j(this).val()!=""){
			$j("#placeHolderOfid_"+$j(this).attr("id")).hide();
		}else{
			$j("#placeHolderOfid_"+$j(this).attr("id")).show();
		}
	});
}
function checkValid(){
	var flag = true;
	var url = "saveDynamicFrm";
	var noMandatory = true;
	var focusField = "1";
		if($j('#isCompInfo').val() == "1"){
			if($j('#isCDRUser').val() == "true"){
				var isReqPriorToShipEl = $j('#'+formId+'ModalForm .isReqPriorToShip').get(),s,len = isReqPriorToShipEl.length; 
				for(s = 0; s < len; s +=1){
					var pId = $j(isReqPriorToShipEl[s]).parents("tr").attr('id');
					if(pId !="" && pId !=null && pId != 'undefined'){
						var pObj = isReqPriorToShipEl[s].parentNode.parentNode;
						var Pflag = $j(pObj).hasClass('hidechild');
						if(!Pflag && isReqPriorToShipEl[s].disabled != 'true'){
							var id = isReqPriorToShipEl[s].getAttribute('id');
							if(isReqPriorToShipEl[s].value !="" && isReqPriorToShipEl[s].value != null && isReqPriorToShipEl[s].value != "undefined" && isReqPriorToShipEl[s].value !="mm/dd/yyyy"){
								$j('#errorMsg_'+id).hide();
							}else{
								 noMandatory = false;
								 $j('#errorMsg_'+id).show();
								flag = false;
								if(focusField=="1"){
									focusField = id;
								}
							}
						}
					}
				}	
				if(noMandatory){
					var len = $j('#'+formId+'ModalForm .assesReqPriorToShip').length;
					if(len != 0){
						noMandatory = false;
					}
					//url = url+'?noMandatory='+noMandatory;
				}
			}
		}else{
			var isMandatoryEl  = $j('.IsMandatory').get(),s,len = isMandatoryEl.length;
			for(s=0; s<len; s +=1){
				var pId = $j(isMandatoryEl[s]).parents("tr").attr('id');
				if(pId !="" && pId !=null && pId != 'undefined'){
					var pObj = isMandatoryEl[s].parentNode.parentNode; 
					var Pflag = $j(pObj).hasClass('hidechild');
					if(!Pflag && isMandatoryEl[s].disabled != 'true'){
						var id = isMandatoryEl[s].getAttribute('id');
						if(isMandatoryEl[s].value !="" && isMandatoryEl[s].value != null && isMandatoryEl[s].value != "undefined"  && isMandatoryEl[s].value !="mm/dd/yyyy"){
							$j('#errorMsg_'+id).hide();
						}else{
							$j('#errorMsg_'+id).show();
							flag = false;
							if(focusField=="1"){
								focusField = id;
							}
						}
					}
				}
			}
		}
		if(focusField !="" && focusField != null && focusField != "undefined" && focusField!="1"){
			$j('#'+focusField).focus();
		}
		if(flag==true && $j("#dynaFormDiv .hasDatepicker").valid()){
			var assessmentLen = $j('#'+formId+'ModalForm .warningFlag').length;
			var warningFlag = false;
			$j(".warningFlag").each(function(i,el){
				if($j(el).is(":visible")){
					warningFlag = true;
				}else{
				}
			});
			if(warningFlag){
				jConfirm('<s:text name="garuda.label.forms.assessmentwarn"/>', '<s:text name="garuda.common.lable.confirm"/>',
					function(r) {
						if (r == false) {
						}else if(r == true){
							if($j('#isCompInfo').val() == '1'){
								var div2;
								if(formId == mrqlabel){
									div2 = 'mrqTargetDiv';
								}else if(formId == fmhqlabel){
									div2 = 'fmhqTargetDiv';
								}
								loadMoredivsCRI(url,formId+'ModalForm,'+div2,'dynamicForm');
							}else{
								refreshDynaForm('saveDynamicFrm',formId+'_formVersion','dynamicForm');
							}
						}
				});
			}else{
				if($j('#isCompInfo').val() == '1'){
					var div2;
					if(formId == mrqlabel){
						div2 = 'mrqTargetDiv';
					}else if(formId == fmhqlabel){
						div2 = 'fmhqTargetDiv';
					}
					loadMoredivsCRI(url,formId+'ModalForm,'+div2,'dynamicForm');
				}else{
					refreshDynaForm('saveDynamicFrm',formId+'_formVersion','dynamicForm');
				}
			}
		}
		$j('#fromViewClinicalFrm').val('1');
}
function hideDynaAssess(obj,rowId){
	var divId='assess_'+rowId;
	$j('#'+divId).hide();
	$j(obj).hide();
	$j(obj).prev('a').show();
}
function showDynaAssessLink(obj,rowId,id,condition,code){
	showDynaAssess(rowId,id,condition,code);
	$j(obj).hide();
	$j(obj).next('a').show();
}

function fieldclear(obj){
	if(!$j(obj).is(":disabled")){
		if($j(obj).val()!=""){
			var id=$j(obj).attr('id');
			$j('#errorMsg_'+id).hide();
			$j(obj).removeClass("criHighlight");
		}else{
			if( $j('#isCompInfo').val() == "1" ){
				if( $j('#isCDRUser').val() == "true" ){
					if($j(obj).hasClass('isReqPriorToShip')){
						$j(obj).addClass("criHighlight");
					}
				}
			}
		}
	}else{
		var id=$j(obj).attr('id');
		$j('#errorMsg_'+id).hide();
	}
}
function loadMoredivsCRI(url,divname,formId1){ 
	showprogressMgs();
	var formId = $j('#formId').val();
	var changeFlag = divChanges("frmDatechangeCls",$j('#'+formId+'ModalForm').html());
	if(!changeFlag){
		changeFlag = divChanges("assessmentChngCls",$j('#'+formId+'ModalForm').html());
	}
	var criChFlag;
	if(changeFlag==false){
		criChFlag = $j("#dialogForCRI").find("#criChangeFlag").val();
	}else if(changeFlag==true){
		criChFlag = changeFlag;
	}
	url = url+'?changeFlag='+changeFlag+'&criChangeFlag='+criChFlag;
	var divnamearr = divname.split(",");
	var newdivname = "";
	$j('#dynaFormDiv textarea').removeAttr('disabled');
		$j('#dynaFormDiv select>option').removeAttr('disabled');
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        data:$j('#'+formId1).serialize(),
	        success: function (result){
	        	var $response = $j(result);
	        	var errorpage = $response.find('#errorForm').html();
	        	if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{ 
	        		newdivname = "finalreviewmodel";
	        		$j("#"+newdivname).html(result);

	        		$j('#'+divnamearr[0]+' #dynaInfo').hide();
            		$j('#'+divnamearr[0]+' #eSignTable').hide();
                	$j('#'+divnamearr[0]+' #dynaUpdate').show();
	            }
	        },
	        error: function (request, status, error) {
	        	closeprogressMsg();
	        	alert("Error " + error);
	            alert(request.responseText);
	        }
		});
	 closeprogressMsg();
}
function showchat(obj,header){
	var chatMsg = $j(obj).next('div').html();
	overlib(chatMsg,STICKY, CAPTION,header,CLOSECLICK,CLOSECOLOR,'white',WRAP,LEFT,ABOVE);
	$j('#overDiv').find('table:eq(1)').width('100%');
	$j('#overDiv').find('table').find('font').css({'font-size':'12px'});
}
function addtype(obj,fkform,fkques,quesDec,quesNo,cssclass,masterPk,row,subrow,trClass){
	var thisObj = $j('.addType_'+fkques);
	var oldTypeLen = thisObj.length;
	var finalFalg = true;
	var lastIndex = 0;
	$j(thisObj).each(function(index,el){
		var pObj = $j(this).parent().parent();
		if($j(pObj).is(":visible")){
			$j(this).hide();
			lastIndex = index+1;
		}else{
			lastIndex = index + 1;
			$j(pObj).removeClass('hidechild');
			if(index == 5){
				$j(this).hide();
			}else{
				$j(this).show().focus();
				finalFalg = false;
			}
			return false;
		}
	});
	
	if(oldTypeLen < 6 && finalFalg){
		quesDec = quesDec.replace(/'/g,"&#145;");
		subrow = subrow+1;
		var rowval = row+"_"+subrow;
		var indexval = $j('#indexvalue').val();
		var parentTd = $j(obj).parent().parent().parent();
		var newobj = "<tr id='Question_"+indexval+"' class='tr_"+trClass+"'><td align='center' width='5%' id='Question_"+indexval+"_td'>"+quesNo+"<input name='assesCon' id='"+formId+"_assesCon"+indexval+"' value='1' type='hidden'>";
			newobj = newobj+"<input name='subEntityId' value='' type='hidden'>";
			newobj = newobj+"<input name='formflag' value='true' type='hidden'>";
			newobj = newobj+"<input name='shrtName' value='"+formId+"_Ques"+rowval+"_s' id='"+indexval+"_assessshname' type='hidden'>";
			newobj = newobj+"<input name='assessDivName1' id='assessDivName_"+indexval+"' value='"+formId+"_Ques"+rowval+"_s_assessmentDiv' type='hidden'>";
			newobj = newobj+"<input name='questionNo' value='"+formId+" Q#"+quesNo+"' type='hidden'>";
			newobj = newobj+"<input name='"+formId+"_Question["+indexval+"].pkresponse' value='' type='hidden'>";
			newobj = newobj+"<input name='assessmentCount' id='assessmentPk_"+indexval+"' value='' type='hidden'>";
			newobj = newobj+"<input name='"+formId+"_Question["+indexval+"].fkform' value='"+fkform+"' type='hidden'>";
			newobj = newobj+"<input name='"+formId+"_Question["+indexval+"].fkquestion' value='"+fkques+"' type='hidden' id='"+formId+"_Ques"+row+"_"+subrow+"_s_fktestid'>";
			newobj = newobj+"</td><td width='70%'><div class='childquestions'>"+quesDec+"<span style='display: inline;' class='error FMHQ_classMan"+masterPk+"' id='FMHQ_mandatory"+fkques+"'>*</span></div>";
			newobj = newobj+"<div id='assess_"+indexval+"' style='display: none;' class='assessMentDiv'></div></td><td width='25%' style='vertical-align: top;'>";
			newobj = newobj+"<input maxlength='30' onchange=\"fieldclear(this);showDynaAssess('"+indexval+"',this.id,'1','1');\" id='"+formId+"_QuestionRes"+indexval+"' class='"+cssclass+" "+formId+"_Ques"+rowval+"_s_res frmDatechangeCls' name='"+formId+"_Question["+indexval+"].responsevalue' type='text'/><div class='addType_"+fkques+"' style='float:right'>";
			newobj = newobj+"<a href='#' tabindex='-1' onclick=\"addtype(this,'"+fkform+"','"+fkques+"','"+quesDec+"','"+quesNo+"','"+cssclass+"','"+masterPk+"',"+row+","+subrow+",'"+trClass+"');\"><%=propData.getPropertiesData("garuda.label.dynamicform.addtype") %></a></div>";
			newobj = newobj+"<div><a href='#' onclick=\"showDynaAssessLink(this,'"+indexval+"','"+formId+"_QuestionRes"+indexval+"','1','1');\" id='showasses_"+indexval+"' style='display: none;'>";
			newobj = newobj+"<%=propData.getPropertiesData("garuda.label.dynamicform.showassess") %></a> <a id='hideasses_"+indexval+"' href='#' onclick=\"hideDynaAssess(this,'"+indexval+"');\" style='display: none;'>";
			newobj = newobj+"<%=propData.getPropertiesData("garuda.label.dynamicform.hideassess") %></a></div>";
			newobj = newobj+"<span class='error fieldMan' style='display:none' id='errorMsg_"+formId+"_QuestionRes"+indexval+"'>";
			newobj = newobj+"<%=propData.getPropertiesData("garuda.label.dynamicform.errormsg") %></span></td></tr>";
		$j(parentTd).after(newobj);
		$j(obj).parent().hide();
		indexval = parseFloat(indexval)+parseFloat(1);
		$j('#indexvalue').val(indexval);
		hideaddType(fkques);
		setTabIndex();
		$j(newobj).find('td:last').find(':text').focus();
	}
	
}
function hideaddType(index){
	var id = 'addType_'+index;
	$j('.'+id).hide();
	if($j('.'+id).length<6){
		$j('.'+id+":last").show();
	}
}
function commentclear(obj,index){
	var errorComment = index+'_Commenterror';
	if($j(obj).val() != '' && $j(obj).val() != null){
		$j('#'+errorComment).hide();
	}else{
		$j('#'+errorComment).show();
	}
}
function saveComment(obj,index){
	var parentDiv = $j(obj).parent().parent();
	var textFld = $j(obj).parent().find('textarea').val();
	var commentID = formId+'_comment_'+index;
	var errorComment = index+'_Commenterror';
	$j('#'+commentID).val(textFld);
	if(textFld !='' && textFld != null){
		var spanText = "<img height='15px' src='./images/addcomment.png'>&nbsp;"+"<%=propData.getPropertiesData("garuda.label.dynamicform.addorviewcomment") %>";
		$j(parentDiv).prev('a').find('span').html(spanText);
		$j(parentDiv).hide();
		$j('#'+errorComment).hide();
		//$j(obj).hide();
		//$j(obj).parent().find('textarea').attr('disabled','disabled');
	}else{
		$j('#'+errorComment).show();
	}
}

function hideDynaComment(obj,index){
	var parentDiv = $j(obj).parent().parent();
	$j(parentDiv).hide();
	var textFld = $j(obj).parent().find('textarea').attr('id');
	var commentID = formId+'_comment_'+index;
	commentID = $j('#'+commentID).val();
	$j('#'+textFld).val(commentID);
}
function setMultiselectHeidht(){
	$j('.multiSelectBox').each(function(){
		var lstSize = $j(this).find('option').length;
		$j(this).attr('size',lstSize);
	});
}
function showHelpintooltip(msg){
	overlib(msg,CAPTION,'<%=propData.getPropertiesData("garuda.label.dynamicform.Help") %>',ABOVE,WIDTH,400);
}
function setTabIndex(){
	var tabElement = $j(".tabclass").get(), t, tLen = tabElement.length;
	var ind = 0;
	$j(".tabclass").each(function(ind){
		$j(this).attr('tabindex',++ind);
	});
}
function fn_parseQuote(el){
	
	var s= $j(el).val();
	
	if(s.indexOf("'")!=-1){
		s.replace(/'/g, "\'");
		$j('#quoteField').val(s);
	}
}
</script>