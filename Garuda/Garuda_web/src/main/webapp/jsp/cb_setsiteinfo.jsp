<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.eres.web.site.SiteJB"%>
<%@page import="com.velos.eres.web.address.AddressJB"%>
<script>
  var addId= '<s:property value="site.sitePerAdd"/>';
</script>
<%
	AddressJB add = new AddressJB();
    if(request.getParameter("addId")!=null && request.getParameter("addId")!=""
    		&& !request.getParameter("addId").equals("") && !request.getParameter("addId").equals("null")
    				&& request.getParameter("addId")!="null"){
    	String addId =request.getParameter("addId");
		add.setAddId(Integer.parseInt(addId));
		add.getAddressDetails();		
		request.setAttribute("city",add.getAddCity());
    } 
    if(request.getAttribute("sitePerAdd")!=null && request.getAttribute("sitePerAdd")!=""
    	&& !request.getAttribute("sitePerAdd").equals("")){
    	String sitePerAdd = request.getAttribute("sitePerAdd").toString();
		add.setAddId(Integer.parseInt(sitePerAdd));
		add.getAddressDetails();		
		request.setAttribute("city",add.getAddCity());
    }
%>
<table>
     <tr> 
        <td width="12%" style="padding-left: 5px;"><s:text name="garuda.cbuentry.label.cbbname" />:</td>
		<td width="27%" style="padding-left: 5px;"><s:hidden name="cdrCbuPojo.site.siteName" id="cbbname"/><s:textfield name="cdrCbuPojo.site.siteName" id="cbbname" readonly="true" disabled="true"/></td>
		<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cbudefaults.label.storage.location"></s:text>:</td>
		<td width="20%" style="padding-left: 5px;">
		    <s:if test="cbuStrLocationList!=null">
		        <s:select list="cbuStrLocationList"  headerKey="-1" headerValue="Select" name="cdrCbuPojo.fkCBUStorageLocation" listKey="siteId"  listValue="siteName"> </s:select>
		    </s:if>
		    <s:else>
		        <s:textfield name="cdrCbuPojo.fkCBUStorageLocation"></s:textfield>
		    </s:else>   
		</td>
		<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cbudefaults.label.collection.site"></s:text>:</td>
		<td width="20%" style="padding-left: 5px;">
		    <s:if test="cbuCollectionList!=null">
		        <s:select list="cbuCollectionList"  name="cdrCbuPojo.fkCBUCollectionSite"  headerKey="-1" headerValue="Select" listKey="siteId"  listValue="siteName"> </s:select>
		    </s:if>  
		    <s:else>
		        <s:textfield name="cdrCbuPojo.fkCBUCollectionSite"></s:textfield>
		    </s:else>
	    </td>
		<!--<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cordentry.label.cbblocation" /></td>
		<td width="30%" style="padding-left: 5px;"><s:textfield value="%{#request.city}" readonly="true" onkeydown="cancelBack();" /></td>
	    -->
	</tr>									   
</table>                                              