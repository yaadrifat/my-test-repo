<%@taglib prefix="s" uri="/struts-tags" %>
<s:if test="#request.linkRefCode=='SOP'" >
 <h1><s:text name="garuda.reference.label.sop"/></h1>
</s:if>
<s:if test="#request.linkRefCode=='MOP'" >
 <h1><s:text name="garuda.reference.label.mop"/></h1>
</s:if>
<s:if test="#request.linkRefCode=='GUIDE'" >
 <h1><s:text name="garuda.reference.label.guides"/></h1>	
</s:if>
<s:if test="#request.linkRefCode=='HELP'" >
 <h1><s:text name="garuda.reference.label.helpfulLinks"/></h1>	
</s:if>
<s:if test="#request.linkRefCode=='NETWORK'" >
 <h1><s:text name="garuda.reference.label.networkPartners"/></h1>	
</s:if>
  <div id="ref">
    
  </div>
<s:if test="#request.linkRefCode=='SOP'" >
	<script>
	sopReference();
	</script>
</s:if>
<s:if test="#request.linkRefCode=='MOP'" >
	<script>
	 mopReference();	
	</script>
</s:if>
<s:if test="#request.linkRefCode=='GUIDE'" >
	<script>
	 guideReference();	 
	</script>
</s:if>
<s:if test="#request.linkRefCode=='HELP'" >
	<script>
	 helpReference();	 
	</script>
</s:if>
<s:if test="#request.linkRefCode=='NETWORK'" >
	<script>
	 networkReference();	 
	</script>
</s:if>
