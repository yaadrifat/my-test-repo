<%@page import="com.velos.ordercomponent.business.util.VelosMidConstants"%>
<%@page import="com.velos.ordercomponent.business.pojoobjects.CdrCbuPojo"%>
<%@page contentType="text/html" import="java.util.*" %>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<%@page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<%
HttpSession tSession = request.getSession(true);
GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
String accId = null;
String logUsr = null;
if (sessionmaint.isValidSession(tSession)) {
	accId = (String) tSession.getValue("accountId");
	logUsr = (String) tSession.getValue("userId");
	request.setAttribute("username", logUsr);
}
%>
<script>

function chooseHistory(value,element){
	//$j("#modelPopup1").html("");
	var form = $j(element).parents('form:first');
	//alert($j(form).attr("id"));	
	if($j(form).attr("id")!=""){
	if(value=='CBU Request with History'){
		$j('#chistory').hide();
		$j('#cReqhistory').show();
	}
	if(value=='CBU History'){
		$j('#cReqhistory').hide();
		$j('#chistory').show();
	 }
	} 
	else{
	      if(value=='CBU Request with History'){
		   $j('#cbu-historyid').hide();
		   $j('#cbu-req-historyid').show();
	     }
	     if(value=='CBU History'){
		    $j('#cbu-req-historyid').hide();
		    $j('#cbu-historyid').show();
	     }
	}
}

function getHistory(pkorder,pkcord,id){

	if(!$j('#changedata'+id).hasClass('firstLoad')){
				
		if($j('#formidflag').val()!=null || ( $j('#orderType').val()!=null && $j('#orderType').val()!='') ){
			//refreshDiv('showcbuhistory?pkcordId='+pkcord+'&pkorderid='+pkorder,'reqCBUhistory','cbuCordInformationForm');
			loaddiv('showcbuhistory?pkcordId='+pkcord+'&pkorderid='+pkorder,'reqCBUhistory');
		}
		else{
			//refreshDiv('showcbuhistory?pkcordId='+pkcord+'&pkorderid='+pkorder,'reqCBUhistory','form1');
			loaddiv('showcbuhistory?pkcordId='+pkcord+'&pkorderid='+pkorder,'reqCBUhistory');
		}
		var tempval=$j('#reqCBUhistory').html();
		$j('#changedata'+id).html(tempval);
		$j('#changedata'+id).addClass('firstLoad');
		
	}
	
}

function displayHistory(id){
	var id1='detailedhistory'+id;
	var vicon='icondiv'+id;
	$j("#"+vicon).find(".ui-icon").each(function(){
		if($j(this).is(".ui-icon-minusthick") || $j(this).is(".ui-icon-plusthick"))
		{
			if($j(this).is(".ui-icon-minusthick")){
				$j('#changedata'+id).css('display','none');
			    $j(this).removeClass("ui-icon-minusthick").addClass("ui-icon-plusthick");
	    	}
	    	else if($j(this).is(".ui-icon-plusthick")){
	    		$j('#changedata'+id).css('display','block');
	    	    $j(this).removeClass("ui-icon-plusthick").addClass("ui-icon-minusthick");
	    	}
		}
	});    
}
function removecheck(){
	if($j("#cbuHistory1").is(':checked') || $j("#cbuRHistory1").is(':checked')){
		$j("#cbuRHistory1").removeAttr('checked');
		$j("#cbuHistory1").removeAttr('checked');
	}
}
function loadGroup(pkorder,ordertype1,pkcord,fkCbbId)
{
	var url="getGroupsByIdsForOrderType?fkCbbId="+fkCbbId+"&orderType="+ordertype1+"&orderId="+pkorder+"&pkcordId="+pkcord+"&userId="+<%=logUsr%>+'&regIdval='+'<s:property value="cdrCbuPojo.registryId" />';
	loadPageByGetRequset(url,'usersitegrpsdropdown');
}
function printPendingTab(printDiv,title){
	document.getElementById("printingdiv").value = printDiv;
	document.getElementById("printingdivTitle").value = title;
}
</script>
<input type="hidden" id="printingdiv" value="cReqhistory" >
<input type="hidden" id="printingdivTitle" value="CBU Request History">
<div id="cbuHistoryForm">
	<table cellpadding="0" cellspacing="0" border="0" class="col_100" width="100%">
		<tr>
			<td width="50%">
				<!--<s:radio list="{'CBU Request with History','CBU History'}" name="history" id="historyid" onchange="chooseHistory(this.value)" cssStyle="width:30%;"></s:radio>-->
				<input type="radio" value="CBU Request with History" name="history" id="cbuRHistory1" onclick="chooseHistory(this.value,this); printPendingTab('cReqhistory','CBU Request History');"/>
				<s:text name="garuda.cbuhistory.label.cbuRequestHistory"/>
			</td>
			<td width="50%">
				<input type="radio" value="CBU History" name="history" id="cbuHistory1" onclick="chooseHistory(this.value,this); printPendingTab('chistory','CBU History');"/>
				<s:text name="garuda.cbuhistory.label.cbuHistory"/>
			</td>
		</tr>
	</table>

<br/>

	<div class="historycls cbu-req-history" id="cReqhistory" style="display: none;">
				<table border="0" cellpadding="0" cellspacing="0" class="displaycdr historycls" id="cbuReqHistoryTbl">
							<thead>
								<tr>
									<th id="thSelectColumn2">
										<div class="cmDiv">
										<ul class="clickMenu" id="ulSelectColumn2">
											<li class="main hover">
												<img src="images/selectcol.png" alt="<s:text name="garuda.common.label.selectcolumns"/>" title="<s:text name="garuda.common.label.selectcolumns"/>" />
													<div style="position: absolute; display: block;" class="outerbox inner">
														<div class="shadowbox1"></div>
														<div class="shadowbox2"></div>
														<div class="shadowbox3"></div>
														<ul class="innerBox" id="targetall2"></ul>
													</div>
											</li>
										</ul>
										<div class="clear"></div>
										</div>
									</th>
									<th><s:text name="garuda.cbuhistory.label.requestdate"/></th>
									<th><s:text name="garuda.cbuhistory.label.requesttype"/></th>
									<th><s:text name="garuda.cbuhistory.label.resolution"/></th>
									<th><s:text name="garuda.cbuhistory.label.patientid"/></th>
									<th><s:text name="garuda.cbuhistory.label.transplantcenter"/></th>									
								</tr>
							</thead>
							<tbody  class="historycls">
								
														
								<s:if test="overallcbuReqHistry.size()>0 && overallcbuReqHistry!=null" >
									<s:iterator value="overallcbuReqHistry" var="rowIndex" status="row">
									
										<tr class="historycls" id="overallcbuReqHistryTR">
											<s:if test='%{#rowIndex[0]!=null && #rowIndex[0]!=""}' >
											<td>
											<div onclick="displayHistory('<s:property value="%{#row.index}"/>');getHistory('<s:property value="%{#rowIndex[6]}"/>','<s:property value="%{#rowIndex[7]}"/>','<s:property value="%{#row.index}"/>');" id="icondiv<s:property value="%{#row.index}"/>" title="<s:text name="garuda.common.label.title"/>" ><span class="ui-icon ui-icon-plusthick" ></span></div>
											</td>
											<td>
												 <s:property value="%{#rowIndex[0]}"/>
												 <s:hidden id="requestedDate" value="%{#rowIndex[0]}" />
											</td>
											<td>
											<s:if test="isCdrUser==true">
												<a href="#" onclick="loadGroup('<s:property value="%{#rowIndex[6]}"/>','<s:property value="%{#rowIndex[1]}" />','<s:property value="%{#rowIndex[7]}"/>','<s:property value="%{#rowIndex[8]}"/>');" ><s:property value="%{#rowIndex[1]}"  /></a>
											</s:if>
											<s:else>
											<s:property value="%{#rowIndex[1]}"  />
											</s:else>
											</td>
											<td><s:property value="%{#rowIndex[2]}"/></td>
											<td><s:property value="%{#rowIndex[3]}"/></td>
											<td><s:property value="%{#rowIndex[4]}"/></td>
											<s:hidden name="pkorder" id="pkorderid%{#row.index}" value="%{#rowIndex[6]}"></s:hidden>
											<s:hidden name="pkcord" id="pkcordid%{#row.index}" value="%{#rowIndex[7]}"/>
											</s:if>
											<s:else>
											<td colspan="6" width="100%" align="center"><s:text name="garuda.cbu.label.noHistoryFound"/></td>
											</s:else>
										</tr>
										<tr id="detailedhistory<s:property value="%{#row.index}"/>" >
											<td colspan="6" width="100%">
												<div id="changedata<s:property value="%{#row.index}"/>" style="display: none;" class="detailhstry" ></div>
											</td>
										</tr>
									</s:iterator>
								</s:if>
								<s:else>
									<tr><td colspan="6" align="center"><s:text name="garuda.cbu.label.noHistoryFound"/></td></tr>
								</s:else>
								</tbody>
							<tfoot>
					<tr><td colspan="6"></td></tr>
			</tfoot>
		</table>
	</div>
				
	<div style="display: none;" id="chistory" class="historycls cbu-histry">
					<input type="hidden" name="hentries" id="hentries" value=<%=paginationSearch.getiShowRows() %> />
					<input type="hidden" name="htentries" id="htentries" value=<%=paginationSearch.getiTotalRows() %> />
					<div align="right"><a href="#" onclick="fn_ResetHistorySort()"  id="resetSortLookup"><s:text name="garuda.pendingorderpage.label.resetsort"/></a></div><br>
					<table border="0" cellpadding="0" cellspacing="0" class="displaycdr historycls" id="cbuhistorytbl" >
							<thead>
								<tr id="historyTHeadTr">
									<th id="thSelectColumn">
										<div class="cmDiv">
										<ul class="clickMenu" id="ulSelectColumn1">
											<li class="main hover">
												<img src="images/selectcol.png" alt="<s:text name="garuda.common.label.selectcolumns"/>" title="<s:text name="garuda.common.label.selectcolumns"/>"/>
													<div style="position: absolute; display: block;" class="outerbox inner">
														<div class="shadowbox1"></div>
														<div class="shadowbox2"></div>
														<div class="shadowbox3"></div>
														<ul class="innerBox" id="targetall1"></ul>
													</div>
											</li>
										</ul>
										<div class="clear"></div>
										</div>
									</th>
									<th class="postionth"><s:text name="garuda.cbuhistory.label.date"/></th>
									<th class="postionth"><s:text name="garuda.cbuhistory.label.cordevent"/></th>
									<th class="postionth"><s:text name="garuda.cbuhistory.label.user"/></th>
									<th class="postionth"><s:text name="garuda.cbuhistory.label.request"/></th>
								</tr>
							</thead>
							<tbody id="historyTblTbody">
							<s:if test="cbuhistorylst!=null && cbuhistorylst.size()>0">
								<s:iterator value="cbuhistorylst" var="rowval">

							
								<s:if test="%{#rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@CORD_NMDP_STTS || #rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@CORD_STATUS || #rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS || #rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@FORMS_CODE || #rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@TASK_STATUS || #rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@TASK_RESET  || #rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@LICENSE  || #rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@ELIGIBLE}">
								<tr>
									<td>-</td>
									<td>
											<s:property value="%{#rowval[0]}"/>
									</td>
									<td>																			
													<s:if test="%{#rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@CORD_STATUS && #rowval[0]!='' && #rowval[0]!=null}">
												    
														    <s:if test="%{#rowval[6]==0 || #rowval[6]==null}">
														    <s:text name="garuda.cbuhistory.label.localCordStatus.entered"/> 
														    </s:if>
														     <s:if test="%{#rowval[6]==1}">
														    <s:text name="garuda.cbuhistory.label.localCordStatus.updated"/> 
														    </s:if>
														    <s:property value="%{#rowval[1]}"/>
												    
												    </s:if>
													<s:if test="%{#rowval[4] == @com.velos.ordercomponent.business.util.VelosMidConstants@LICENSE && #rowval[0]!='' && #rowval[0]!=null}">
													
															<s:if test="%{#rowval[6]==0 || #rowval[6]==null}">
														    <s:text name="garuda.cbuRhistory.label.license.entered"/> 
														    </s:if>
														     <s:if test="%{#rowval[6]==1}">
														    <s:text name="garuda.cbuRhistory.label.license.updated"/> 
														    </s:if>
														    <s:property value="%{#rowval[1]}"/>
													</s:if>
													<s:if test="%{#rowval[4] == @com.velos.ordercomponent.business.util.VelosMidConstants@ELIGIBLE && #rowval[0]!='' && #rowval[0]!=null}">
															<s:if test="%{#rowval[6]==0 || #rowval[6]==null}">
														    <s:text name="garuda.cbuRhistory.label.eligibility.entered"/> 
														    </s:if>
														     <s:if test="%{#rowval[6]==1}">
														    <s:text name="garuda.cbuRhistory.label.eligibility.updated"/> 
														    </s:if>
														    <s:property value="%{#rowval[1]}"/>
													</s:if>
													<s:if test="%{#rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@CORD_NMDP_STTS && #rowval[0]!='' && #rowval[0]!=null}">
															<s:if test="%{#rowval[6]==0 || #rowval[6]==null}">
														    <s:text name="garuda.cbuRhistory.label.nationRegStatus.entered"/> 
														    </s:if>
														     <s:if test="%{#rowval[6]==1}">
														    <s:text name="garuda.cbuRhistory.label.nationRegStatus.updated"/> 
														    </s:if>
														    <s:property value="%{#rowval[1]}"/>
													</s:if>
													<s:if test="%{#rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@FORMS_CODE}">
													
														<s:property value="%{#rowval[1]}"/>
														<s:if test="%{#rowval[6]==0}">
															 <s:text name="garuda.cbuhistory.label.formCreated"/> 
														</s:if>
														<s:if test="%{#rowval[6]==1}">
															 <s:text name="garuda.cbuhistory.label.formUpdated"/> 
														</s:if>
													 
													</s:if>
													<s:if test="%{#rowval[4] == @com.velos.ordercomponent.business.util.VelosMidConstants@TASK_STATUS && #rowval[6]!='' && #rowval[6]!=null}">
													<s:property value="%{#rowval[6]}"/> <s:text name="garuda.cbuhistory.label.taskcompleted"/>
													</s:if>
													<s:if test="%{#rowval[4] == @com.velos.ordercomponent.business.util.VelosMidConstants@TASK_RESET && #rowval[6]!='' && #rowval[6]!=null}">
													<s:property value="%{#rowval[6]}"/> <s:text name="garuda.cbuhistory.label.taskresetted"/>
													</s:if>
									
									
									</td>
									<td>
													<s:if test="%{#rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLUTION_TC}">
												    	  <s:text name="garuda.cbuhistory.label.caseManager"/>
												    </s:if>
												    <s:elseif test="%{#rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@CORD_NMDP_STTS}">
												    	  <s:text name="garuda.cbuhistory.label.system"/>
												    </s:elseif>
												    <s:else>
												    <s:if test="%{#rowval[2]!=null || #rowval[4]==@com.velos.ordercomponent.business.util.VelosMidConstants@FORMS_CODE}">
													<s:property value="%{#rowval[2]}"/>
													</s:if>
													</s:else>
									</td>
									<td><s:property value="%{#rowval[3]}"/></td>
								</tr>
								</s:if>
								</s:iterator>
							</s:if>
						
							</tbody>
							<tfoot>
									<tr>
											<td colspan="5" width="100%">
											<s:if test="orderType!=null && orderType!=''"> 
											<jsp:include page="paginationFooter.jsp">
													  	<jsp:param value="chistory" name="divName"/>
													  	<jsp:param value="cbuCordInformationForm" name="formName"/>
													  	<jsp:param value="showsRowcbuhistorytbl" name="showsRecordId"/>
													 	<jsp:param value="getCbuHistory" name="url"/>
													 	<jsp:param value="temp" name="cbuid" />
													 	<jsp:param value="pkcordId" name="pkcord" />
													 	<jsp:param value="orders" name="paginateSearch"/>
													 	<jsp:param value="5" name="idparam"/>
													  	<jsp:param value="historyTblOnLoad" name="bodyOnloadFn"/>
													</jsp:include> 
													
												</s:if>
												<s:else>
													<jsp:include page="paginationFooter.jsp">
													  	<jsp:param value="chistory" name="divName"/>
													  	<jsp:param value="cbuCordInformationForm" name="formName"/>
													  	<jsp:param value="showsRowcbuhistorytbl" name="showsRecordId"/>
													 	<jsp:param value="getCbuHistory" name="url"/>
													 	<jsp:param value="inputs" name="cbuid" />
													 	<jsp:param value="cbucordidval" name="pkcord" />
													 	<jsp:param value="orders" name="paginateSearch"/>
													 	<jsp:param value="5" name="idparam"/>
													  	<jsp:param value="historyTblOnLoad" name="bodyOnloadFn"/>
													</jsp:include>
												</s:else> 
										
											</td>
									</tr>
							</tfoot>
					</table>
				
	</div>
	<div id="reqCBUhistory" style="display: none;">
				<s:hidden name="historyOrdType" id="historyOrdType" />
				<s:hidden name="pkactivity_pakageSlip" id="pkactivity_pakageSlip" />
				<table id="cbutable" class="historycls">
						<s:if test="cbuRequestHistorylst.size>0 && cbuRequestHistorylst!=null" >
								<s:iterator value="cbuRequestHistorylst" var="rowIndex1">
											<tr>
												<td>
												<s:if test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLUTION_TC || #rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@CORD_NMDP_STTS || #rowIndex1[2]!=null}">
												<s:if test="%{#rowIndex1[0] == #rowIndex1[4]}">
												<span class="error"><s:property value="%{#rowIndex1[0]}"/></span>
												</s:if>
												<s:else>
												<s:property value="%{#rowIndex1[0]}"/>
												</s:else>
														
																		   
												    >
												</s:if>
													<s:if test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@CORD_STATUS && #rowIndex1[1]!=null && #rowIndex1[1]!=''}">
												    			
												    			<s:if test="%{#rowIndex1[10]==0 || #rowIndex1[10]==null}">
															    <s:text name="garuda.cbuhistory.label.localCordStatus.entered"/> 
															    </s:if>
															     <s:if test="%{#rowIndex1[10]==1}">
															    <s:text name="garuda.cbuhistory.label.localCordStatus.updated"/> 
															    </s:if>
															    <s:property value="%{#rowIndex1[1]}"/>
												    			
												    </s:if>
													<s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS && #rowIndex1[5]!=null && #rowIndex1[5]!='' && #rowIndex1[5]!=getCodeListDescByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED)}">
													
																<s:if test="%{#rowIndex1[5]==getCodeListDescByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@NEW)}">
																		<s:text name="garuda.cbuRhistory.label.neworderStatus"/>  <s:property value="%{#rowIndex1[5]}"/>
																</s:if>
																<s:else>
																	<s:text name="garuda.cbuRhistory.label.orderStatus"/>  <s:property value="%{#rowIndex1[5]}"/>
																</s:else>
													</s:elseif>
													<s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS && #rowIndex1[5]!=null && #rowIndex1[5]!='' && #rowIndex1[5]==getCodeListDescByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED)}">
													<s:text name="garuda.cbuRhistory.label.orderStatus"/>  <s:property value="%{#rowIndex1[5]}"/>  <s:if test="%{#rowIndex1[8]!=null}"> <s:text name="garuda.cbuhistory.label.as"/> <s:property value="%{#rowIndex1[8]}"/> </s:if>
													</s:elseif>
													<s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@TASK_STATUS && #rowIndex1[5]!=null && #rowIndex1[5]!='' && #rowIndex1[9]!=pkactivity_pakageSlip}">
													  <s:property value="%{#rowIndex1[5]}"/> <s:text name="garuda.cbuhistory.label.taskcompleted"/>
													</s:elseif>
													<s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@TASK_STATUS && #rowIndex1[5]!=null && #rowIndex1[5]!=''  && #rowIndex1[9]==pkactivity_pakageSlip}">
													
																<s:if test="%{historyOrdType==@com.velos.ordercomponent.business.util.VelosMidConstants@OR}">
															     <s:text name="garuda.cbuHistory.label.shipmentPacket"/> <s:text name="garuda.cbuhistory.label.taskcompleted"/>
															    </s:if>
															    <s:if test="%{historyOrdType==@com.velos.ordercomponent.business.util.VelosMidConstants@CT}">
															      <s:property value="%{#rowIndex1[5]}"/> <s:text name="garuda.cbuhistory.label.taskcompleted"/>
															    </s:if>
													    
													</s:elseif>
													<s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@TASK_RESET && #rowIndex1[5]!=null && #rowIndex1[5]!='' && #rowIndex1[9]!=pkactivity_pakageSlip}">
													  <s:property value="%{#rowIndex1[5]}"/> <s:text name="garuda.cbuhistory.label.taskresetted"/>
													</s:elseif>
													<s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@TASK_RESET && #rowIndex1[5]!=null && #rowIndex1[5]!=''  && #rowIndex1[9]==pkactivity_pakageSlip}">
													
																<s:if test="%{historyOrdType==@com.velos.ordercomponent.business.util.VelosMidConstants@OR}">
															     <s:text name="garuda.cbuHistory.label.shipmentPacket"/> <s:text name="garuda.cbuhistory.label.taskresetted"/>
															    </s:if>
															    <s:if test="%{historyOrdType==@com.velos.ordercomponent.business.util.VelosMidConstants@CT}">
															      <s:property value="%{#rowIndex1[5]}"/> <s:text name="garuda.cbuhistory.label.taskresetted"/>
															    </s:if>
													
													</s:elseif>
													<s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@SWITCH && #rowIndex1[5]!=null && #rowIndex1[5]!=''}">
													<s:text name="garuda.cbuRhistory.label.workflow"/> <s:property value="%{#rowIndex1[5]}"/>
													</s:elseif>
													<s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED_REASON && #rowIndex1[1]!=null && #rowIndex1[1]!=''}">
													<s:text name="garuda.cbuRhistory.label.closedreason"/> <s:property value="%{#rowIndex1[1]}"/>
													</s:elseif>
													<s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLUTION_TC  && #rowIndex1[1]!=null && #rowIndex1[1]!=''}">
													<s:text name="garuda.cbuRhistory.label.resolution"/> <s:property value="%{#rowIndex1[1]}"/>
													</s:elseif>
													<s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@LICENSE && #rowIndex1[5]!=null && #rowIndex1[5]!=''}">
													
																<s:if test="%{#rowIndex1[10]==0 || #rowIndex1[10]==null}">
															    <s:text name="garuda.cbuRhistory.label.license.entered"/> 
															    </s:if>
															     <s:if test="%{#rowIndex1[10]==1}">
															    <s:text name="garuda.cbuRhistory.label.license.updated"/> 
															    </s:if>
															    <s:property value="%{#rowIndex1[5]}"/>
														
													</s:elseif>
													<s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@ELIGIBLE  && #rowIndex1[5]!=null && #rowIndex1[5]!=''}">
																<s:if test="%{#rowIndex1[10]==0 || #rowIndex1[10]==null}">
															    <s:text name="garuda.cbuRhistory.label.eligibility.entered"/> 
															    </s:if>
															     <s:if test="%{#rowIndex1[10]==1}">
															    <s:text name="garuda.cbuRhistory.label.eligibility.updated"/> 
															    </s:if>
															    <s:property value="%{#rowIndex1[5]}"/>
													</s:elseif>
													<s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@CORD_NMDP_STTS && #rowIndex1[1]!=null && #rowIndex1[1]!=''}">
								
																<s:if test="%{#rowIndex1[10]==0 || #rowIndex1[10]==null}">
															    <s:text name="garuda.cbuRhistory.label.nationRegStatus.entered"/> 
															    </s:if>
															     <s:if test="%{#rowIndex1[10]==1}">
															    <s:text name="garuda.cbuRhistory.label.nationRegStatus.updated"/> 
															    </s:if>
															    <s:property value="%{#rowIndex1[1]}"/>
													
													</s:elseif>
													<s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@FORMS_CODE && #rowIndex1[1]!=null && #rowIndex1[1]!=''}">
													<s:property value="%{#rowIndex1[1]}"/> <s:text name="garuda.cbuhistory.label.formCreated"/>  
													</s:elseif>
													<s:if test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLUTION_TC || #rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@CORD_NMDP_STTS || #rowIndex1[2]!=null}">
													(
													<s:if test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLUTION_TC}">
												    	<s:text name="garuda.cbuhistory.label.caseManager"/>
												    </s:if>
												    <s:elseif test="%{#rowIndex1[3]==@com.velos.ordercomponent.business.util.VelosMidConstants@CORD_NMDP_STTS}">
												    	<s:text name="garuda.cbuhistory.label.system"/>
												    </s:elseif>
													<s:elseif test="%{#rowIndex1[2]!=null}">		
														<s:property value="%{#rowIndex1[2]}"/>
													</s:elseif>
												     )
												     </s:if>
											    </td>
													
											</tr>
								</s:iterator>
						</s:if>	
						<s:else>
											<tr>
												<td align="center"><s:text name="garuda.cbu.label.noHistoryFound"/></td>
											</tr>
	 					</s:else>
		 		</table>
	</div>
</div>
<div id="usersitegrpsdropdown">
			<s:hidden value="#request.grpList.size()" />
	        <s:if test="#request.grpList.size()>1">
				<jsp:include page="pf_userSiteGroups.jsp"></jsp:include>
			</s:if>
</div>