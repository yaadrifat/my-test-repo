	<%@page contentType="text/html; charset=UTF-8"%>
	<%@taglib prefix="s" uri="/struts-tags"%>
	
<html>
<script type="text/javascript">

function saveCBUUnitReport(){
	$j.ajax({
		type : "POST",
		async : false,
		url : 'saveCBUUnitReport',
		data : $j("#CBUUnitRptForm").serialize(),
		success : function(data) {
			//showUnitReport("close");
		}
		
	});
} 
$j(function() {
	$j('#userSelection').html("");
	$j( "#datepicker" ).datepicker({dateFormat: 'M dd, yy'});
	$j( "#datepicker1" ).datepicker({dateFormat: 'M dd, yy'});
	
	
});

</script>
<body>
  <s:if test="#request.submit=='cord'">
    <!--<table class="tabledisplay" align="center" border="0">
    <tbody>
      <tr>
        <td><h2><s:text name="garuda.unitreport.label.thankyoucord" /> </h2></td>
      </tr>
    </tbody>
    </table>-->
	 <script>
        window.location.href = "cordEntryInProgress.action?submit=cord"
     </script>
  </s:if>
  <s:if test="#request.submit=='autoDeferCord'">
    <table class="tabledisplay" align="center" border="0">
    <tbody>
      <tr>
        <td><h2><s:text name="garuda.unitreport.label.thankyoucordautodefer" /> </h2></td>
      </tr>
    </tbody>
    </table>
  </s:if>
  <s:if test="#request.submit=='cordinprogress'">
    <!--<table class="tabledisplay" align="center" border="0">
    <tbody>
      <tr>
        <td><h2><s:text name="garuda.unitreport.label.thankyousaveinprogresscord" /> </h2></td>
      </tr>
    </tbody>
    </table>
  -->
     <script>
        window.location.href = "cordEntryInProgress.action?submit=cordinprogress"
     </script>
  </s:if>
  <s:if test="#request.submit=='unitreport'">
    <table class="tabledisplay" align="center" border="0">
    <tbody>
      <tr>
        <td><h2><s:text name="garuda.unitreport.label.thankyou" /> </h2></td>
      </tr>
      <tr>
     	<td> <input type="button" onclick="closeModal()" value="<s:text name="garuda.common.close"/>" /></td>
      </tr>
    </tbody>
    </table>
  </s:if>
  <s:if test="#request.submit=='modifyunitrpt'">
    <table class="tabledisplay" align="center" border="0">
    <tbody>
      <tr>
      	<td>
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.unitreport.label.thankyoumodifyunitrpt" /> </strong></p>
			</div>
		</td>
      </tr>
      <tr>
     	<td> <input type="button" onclick="closeModal()" value="<s:text name="garuda.common.close"/>" /></td>
      </tr>
    </tbody>
    </table>
  </s:if>
  <s:if test="#request.submit=='revokeunitrpt'">
    <table class="tabledisplay" align="center" border="0">
    <tbody>
      <tr>
      	<td>
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.unitreport.label.thankyourevokeunitrpt" /> </strong></p>
			</div>
		</td>
      </tr>
      <tr>
     	<td> <input type="button" onclick="closeModal()" value="<s:text name="garuda.common.close"/>" /></td>
      </tr>
    </tbody>
    </table>
  </s:if>
  <s:if test="#request.submit=='addNewTest'">
    <table class="tabledisplay" align="center" border="0">
    <tbody>
      <tr>
      	<td>
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.unitreport.label.thankyouAddNewTest" /> </strong></p>
			</div>
		</td>
      </tr>
      <tr>
     	<td> <input type="button" onclick="closeModal()" value="<s:text name="garuda.common.close"/>" /></td>
      </tr>
    </tbody>
    </table>
  </s:if>
</body>
</html>