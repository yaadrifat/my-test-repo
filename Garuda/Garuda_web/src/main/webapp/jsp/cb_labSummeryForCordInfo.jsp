<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<style type="text/css">
 div#detaillabDivData {
     overflow-x:auto;  
     overflow-y:hidden;
     border-left: .1em solid #333333;
        
} 
.headcolBase {
     position: relative;
    width:25.4em;
    margin-left:.8em; 
    margin-right:.8em; 
    left:0;
    top:auto;
    text-align:center;
    vertical-align:top;
    border-left: .0em solid #333333;
    border-right: .0em solid #333333;
    border-top:.1em solid #333333;/* only relevant for first row*/
	border-bottom-width:0px;
    margin-top:0em; /*compensate for top border*/
}
</style>
<SCRIPT language="javascript">
function textFunctionBase(divId,textAreaId)
{
		$j("#"+divId).show();
		$j("#"+textAreaId).attr('readonly',true);
		$j('#comClose1Base').show();
		$j('#comClose2Base').show();
		textareaValue=document.getElementById(textAreaId).value;
}
function commentCloseBase(divId,textAreaId)
{
		document.getElementById(textAreaId).value = textareaValue;
		$j("#"+divId).hide();
}
$j(function(){

	 if($j("#pkViaOther").val()==$j("#viabSelectedView").val()){
		 $j("#viabilityView").show();
		}else{
			$j("#viabilityView").hide();
			}

		if($j("#pkOther").val()==$j("#cfuSelectedView").val()){
		 $j("#cfuCountView").show();
		}else{
			$j("#cfuCountView").hide();
		}
	
});

function showAssmentBase(Id,buttonTd,divId,mDivId,hideButtonTd){
	var responsestr=$j('#'+Id+' option:selected').text();

	var val = $j('#'+Id+' option:selected').val();
	var assesTest1=$j("#alphaThalismiaTraitPkVal").val();
	
	var assesTest2=$j("#hemozygousNoDiesasePkVal").val();
	
	var assesTest3=$j("#hemoAlphaThalassmiaPkVal").val();
	
	var assesTest4=$j("#hemoTraitPkVal").val();

	var assesTest5=$j("#hemozygousNoDonePkVal").val();

	var assesTest6=$j("#hemozygousUnknownPkVal").val();
	
	var assesTest7=$j("#hemoMultiTraitPkVal").val();
	
	var fungLabAssesPoes=$j("#fungalCulpostive").val();
	
	var fungLabAssesPoes=$j("#fungalCulpostive").val();

	var fungLabAssesPoes1=$j("#fungalCulnotdone").val();
	var data=null;
	if(val==fungLabAssesPoes1){
		data='entityId=<s:property value="cdrCbuPojo.cordID"/>&subEntityId='+val+'&shrtName=LAB_SUM_FungBase&subentitytype='+$j("#subentityType").val()+'&questionNo=FungalCulture&responsestr='+responsestr+'&assessDivName='+mDivId+'&responseval='+val;
		
	}
	else if(val==assesTest1 || val==assesTest2 || val==assesTest3 || val==assesTest4 || val==assesTest5 || val==assesTest6 || val==assesTest7){
		data='entityId=<s:property value="cdrCbuPojo.cordID"/>&subEntityId='+val+'&shrtName=LAB_SUM_HemoBase&subentitytype='+$j("#subentityType").val()+'&questionNo=Hemoglobinopathy Testing&responsestr='+responsestr+'&assessDivName='+mDivId+'&responseval='+val;
	}
	if(val!=null && val!= ""){
		$j('#'+mDivId).show();
		$j('#'+hideButtonTd).show();
		$j('#'+buttonTd).hide();
		refreshassessmentBase('getassessment?assesmentCheckFlag=true',divId,data);
		
	}else{
		$j('#'+mDivId).hide();
		$j('#'+buttonTd).hide();
	}
}
function hideAssmentBase(mDivId,shwbuttonTd,hideButtonTd){
	$j('#'+mDivId).hide();
	$j('#'+shwbuttonTd).show();
	$j('#'+hideButtonTd).hide();
}
function refreshassessmentBase(url,divname,data){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data:data,
        success: function (result){
            var $response=$j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).html(result);
            	$j("#"+divname+" button").hide();
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
}

</SCRIPT>

<jsp:include page="cb_user_rights.jsp"></jsp:include>
<s:set name="tempVal" value="cbuFinalReviewPojo.finalReviewConfirmStatus"></s:set>
<div  style="overflow: scroll;">
<table>
						<s:hidden value="%{#request.bacterialPositive}" id="bacterialPositive"></s:hidden>
						<s:hidden value="%{#request.bacterialNegative}" id="bacterialNegative"></s:hidden>
						<s:hidden value="%{#request.bacterialPositiveFung}" id="bacterialPositiveFung"></s:hidden>
						<s:hidden value="%{#request.bacterialNegativeFung}" id="bacterialNegativeFung"></s:hidden>
				     	<s:hidden name="pkCfuOther" id="pkOther"></s:hidden>	
	                   	<s:hidden name="pkViabilityOther" id="pkViaOther"></s:hidden>
				     	<tr>
							<td width="20%"  onmouseover="return overlib('<s:text
																	name="garuda.cordentry.label.procStartDateToolTip"></s:text>');" 
																	onmouseout="return nd();"><s:text name="garuda.cordentry.label.prostartdate"></s:text>:</td>
							<td width="20%">
								<s:date name="cdrCbuPojo.processingDate" id="datepicker4B" format="MMM dd, yyyy"/>
								<s:textfield disabled="true" name="prcsngStartDateStr" value="%{datepicker4B}" cssClass="datePicWMaxDate"></s:textfield>
							</td>
							<td width="20%"><s:text name="garuda.cordentry.label.proStartTime"></s:text>:</td>	
							<td width="20%"> <s:textfield name="cdrCbuPojo.processingTime" cssClass="timeValid" maxlength="5" disabled="true"></s:textfield><font size="1">(HH:MM 24 HR)</font></td>								  						
							<td width="20%"></td>
						</tr>
						<tr>
							<td width="20%" onmouseover="return overlib('<s:text
																	name="garuda.cordentry.label.freezeStartDateToolTip"></s:text>');" 
																	onmouseout="return nd();"><s:text name="garuda.cordentry.label.freezedate"></s:text>:</td>
							<td width="20%">
								<s:date name="cdrCbuPojo.frzDate" id="datepicker5" format="MMM dd, yyyy" />
								<s:textfield disabled="true" name="frzDateStr"  onchange="changeProgress(1,'datepicker5')" value="%{datepicker5}" cssClass="datePicWMaxDate"></s:textfield>
							</td>
							<td width="20%"><s:text name="garuda.cordentry.label.freezeTime"></s:text>:</td>	
							<td width="20%"><s:textfield name="cdrCbuPojo.freezeTime" cssClass="timeValid" maxlength="5" disabled="true"></s:textfield><font size="1">(HH:MM 24 HR)</font></td>					  							
							<td width="20%"></td>
					     </tr>
	
						<tr>
							<td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.bactcult"></s:text>:</td>
							<td width="20%"><s:if test="%{populatedData.bactCulList!=null && populatedData.bactCulList.size()>0}"><s:select name="cdrCbuPojo.bacterialResult" disabled="true"  id="bacterialResultt" onchange="bacterial(this.value);" list="populatedData.bactCulList" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select></s:if></td>
							
							<td width="20%" id="bactDateTextBase" <s:if test="cdrCbuPojo.bacterialResult!=#request.bacterialNegative && cdrCbuPojo.bacterialResult!=#request.bacterialPositive">style="display : none;"</s:if>><s:text name="garuda.cordimport.label.bactCultDate"></s:text>:</td>	
							<td width="20%" id="bactDateBase" <s:if test="cdrCbuPojo.bacterialResult!=#request.bacterialNegative && cdrCbuPojo.bacterialResult!=#request.bacterialPositive">style="display : none;"</s:if>>
								<s:date name="cdrCbuPojo.bactCultDate" id="datepicker6B" format="MMM dd, yyyy" />
								<s:textfield disabled="true" name="bactCultDateStr"  onchange="changeProgress(1,'datepicker6B')" value="%{datepicker6B}" cssClass="datePicWMaxDate"></s:textfield>
							</td>	
					  		<td width="20%"><s:set name="bactComment" value="cdrCbuPojo.bactComment" scope="request" />
																	   <!-- <s:property value="cdrCbuPojo.bactComment" default="value not fetched"/>-->
																		<s:if test ="(#request.bactComment != '' && #request.bactComment != null)">
																			<a href="#" onclick="textFunctionBase('textArea1Base','textAreaForBactBase')" id="com1Base" ><img height="15px" src="./images/addcomment.png"><span id="addBactCommentBase"><s:text name="garuda.cbufinalreview.label.flaggedItems.viewcomment"/></span></a>
																	    </s:if>
																		<%-- <s:else>
																			<a href="#" onclick="textFunctionBase('textArea1Base','textAreaForBactBase')" id="com3Base" ><img height="15px" src="./images/addcomment.png"><span id="addBactCommentBase"><s:text name="garuda.cbu.cordentry.addviewComment"/></span></a>
																		</s:else> --%>
																		<div id="textArea1Base" style="display: none;" >
																		<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
																		<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
																			<s:text name="garuda.label.dynamicform.commentbox" />
																		</div>
																		<table>
																			<tr>
																				<td colspan="2"> 
																					<s:textarea name="cdrCbuPojo.bactComment" readonly="true" cols="40" rows="5" id="textAreaForBactBase" /> 
																				</td>
																			</tr>
																			<tr>
																				<%-- <td>
																					<button type="button" onclick="commentSave('textArea1','textAreaForBact','addBactComment')" id="comSave1Base" ><s:text name="Save"/></button>
																				</td> --%>
																				<td colspan="2">
																				    <button type="button" onclick="commentCloseBase('textArea1Base','textAreaForBactBase')" id="comClose1Base" ><s:text name="Close"/></button>
																				</td>
																			</tr>
																		</table></div>
																	</div>
																	 </td>				  						
						</tr>
						<tr>
							<s:hidden name="fungalCulnotdone" id="fungalCulnotdone" value="%{#request.fungalCulnotdone}" />
							<s:set name="fungalCulnotdone1" value="%{#request.fungalCulnotdone}" scope="request"/>
							<td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.fungcult"></s:text>:</td>
							<td width="20%"><s:if test="%{populatedData.funCulList!=null && populatedData.funCulList.size()>0}"><s:select name="cdrCbuPojo.fungalResult" id="fungalResultt" onchange="fungal(this.value);" list="populatedData.funCulList" listKey="pkCodeId" listValue="description"  headerKey="-1" headerValue="Select" disabled="true"></s:select></s:if></td>
							<td width="20%" id="fungDateTextt" <s:if test="cdrCbuPojo.fungalResult!=#request.bacterialNegativeFung && cdrCbuPojo.fungalResult!=#request.bacterialPositiveFung">style="display : none;"</s:if>><s:text name="garuda.cordimport.label.fungCultDate"></s:text>:</td>
							<td width="20%" id="fungDatet" <s:if test="cdrCbuPojo.fungalResult!=#request.bacterialNegativeFung && cdrCbuPojo.fungalResult!=#request.bacterialPositiveFung">style="display : none;"</s:if>>
								<s:date name="cdrCbuPojo.fungCultDate" id="datepicker7B" format="MMM dd, yyyy" />
								<s:textfield name="fungCultDateStr" onchange="changeProgress(1,'datepicker7B')" value="%{datepicker7B}" cssClass="datePicWMaxDate" disabled="true"></s:textfield>
							</td>	
					 		
					       <td width="20%">
					                        <s:set name="fungComment" value="cdrCbuPojo.fungComment" scope="request" />
																	    <!--<s:property value="cdrCbuPojo.bactComment" default="default value"/>-->
												<s:if test = "(#request.fungComment != '' && #request.fungComment != null)" >
														<a href="#" onclick="textFunctionBase('textArea2Base','textAreaForFungBase')" id="com2Base" ><img height="15px" src="./images/addcomment.png"><span id="addFungCommentBase"><s:text name="garuda.cbufinalreview.label.flaggedItems.viewcomment"/></span></a>
												</s:if>
												<%-- <s:else>
														<a href="#" onclick="textFunctionBase('textArea2Base','textAreaForFungBase')" id="com4Base" ><img height="15px" src="./images/addcomment.png"><span id="addFungCommentBase"><s:text name="garuda.cbu.cordentry.addviewComment"/></span></a>
												</s:else> --%>
														<div id="textArea2Base" style="display: none;" >
														<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
														<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
															<s:text name="garuda.label.dynamicform.commentbox" />
														</div>
															<table>
																<tr>
																	<td colspan="2"> 
																		<s:textarea name="cdrCbuPojo.fungComment" readonly="true" cols="40" rows="5" id="textAreaForFungBase" /> 
																	</td>
																</tr>
																<tr>
																	<%-- <td>
																		<button type="button" onclick="commentSave('textArea2Base','textAreaForFungBase','addFungCommentBase')" id="comSave2Base" ><s:text name="Save"/></button>
																	</td> --%>
																	<td>
																		<button type="button" onclick="commentCloseBase('textArea2Base','textAreaForFungBase')" id="comClose2Base" ><s:text name="Close"/></button>
																	</td>
																</tr>
															</table></div>
														</div>
											</td>			  							
						 </tr>
						 <tr>
						               		<td>
						               			<span id="showassesmentFungBase"><s:if test="cdrCbuPojo.fungalResult==#request.fungalCulnotdone1 && #request.assesDoneFrFung==true"><a href="#"  id="showassesmentFungButtonBase" onclick="showAssmentBase('fungalResultt','showassesmentFungBase','Assessment_fungalCultureBase','Assessment_LabSummeryFungBase','hideassesmentFungBase');"><s:text name="garuda.label.dynamicform.showassess"/></a></s:if></span>
						               			<div id="hideassesmentFungBase" style="display: none;"><s:if test="(cdrCbuPojo.fungalResult==#request.fungalCulnotdone1) && #request.assesDoneFrFung==true"><a href="#"  id="hideassesmentFungButtonBase" onclick="hideAssmentBase('Assessment_LabSummeryFungBase','showassesmentFungBase','hideassesmentFungBase');"><s:text name="garuda.label.dynamicform.hideassess"/></a></s:if></div>
						               		</td>
						               </tr>
										<tr id="Assessment_LabSummeryFungBase"  style="display: none;"><td colspan="5">
							               	
							               	<fieldset><legend><s:text name="garuda.clinicalnote.label.assessment"></s:text>&nbsp;For&nbsp;<s:text name="garuda.cordentry.label.fungcult"/>
												</legend><table>
												<tr >
													<td  id="Assessment_fungalCultureBase">
													</td>
													
												</tr></table></fieldset></td>
										</tr>
						 <tr>
							<td width="20%"><s:text name="garuda.cordentry.label.bloodtype"></s:text>:</td>
							<td width="20%"><s:select name="cdrCbuPojo.aboBloodType" cssStyle="width: 180px;" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BLOOD_GROUP]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select Type" disabled="true"></s:select></td>
						    <td width="20%"><s:text name="garuda.cordentry.label.rhtype"></s:text>:</td>
							<td width="20%"><s:select disabled="true" cssStyle="width: 180px;" name="cdrCbuPojo.rhType" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@RH_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type"></s:select></td>
							<td colspan="3">
								<s:if test="cdrCbuPojo.specRhTypeOther!=null && cdrCbuPojo.specRhTypeOther!='' && cdrCbuPojo.rhType==cdrCbuPojo.rhTypeOther">
									<table>
										<tr>
											<td><s:text name="garuda.cbbprocedures.label.specifyother" ></s:text>:</td>
											<td>
												<s:textfield name="cdrCbuPojo.specRhTypeOther" maxLength="30" disabled="true" cssStyle="width: 190px;"></s:textfield>
											</td>
										</tr>
									</table>
								</s:if>
							</td>
					    </tr>     
				     
						 <tr>
						 	<s:hidden name="alphaThalismiaTraitPkVal" id="alphaThalismiaTraitPkVal" value="%{#request.alphaThalismiaTraitPkVal}" />
							<s:set name="alphaThalismiaTraitPkVal1" value="%{#request.alphaThalismiaTraitPkVal}" scope="request"/>
							<s:hidden name="hemozygousNoDiesasePkVal" id="hemozygousNoDiesasePkVal" value="%{#request.hemozygousNoDiesasePkVal}" />
							<s:set name="hemozygousNoDiesasePkVal1" value="%{#request.hemozygousNoDiesasePkVal}" scope="request"/>
							<s:hidden name="hemoAlphaThalassmiaPkVal" id="hemoAlphaThalassmiaPkVal" value="%{#request.hemoAlphaThalassmiaPkVal}" />
							<s:set name="hemoAlphaThalassmiaPkVal1" value="%{#request.hemoAlphaThalassmiaPkVal}" scope="request"/>
							<s:hidden name="hemoTraitPkVal" id="hemoTraitPkVal" value="%{#request.hemoTraitPkVal}" />
							<s:set name="hemoTraitPkVal1" value="%{#request.hemoTraitPkVal}" scope="request"/>
							<s:hidden name="hemozygousNoDonePkVal" id="hemozygousNoDonePkVal" value="%{#request.hemozygousNoDonePkVal}" />
							<s:set name="hemozygousNoDonePkVal1" value="%{#request.hemozygousNoDonePkVal}" scope="request"/>
							<s:hidden name="hemozygousUnknownPkVal" id="hemozygousUnknownPkVal" value="%{#request.hemozygousUnknownPkVal}" />
							<s:set name="hemozygousUnknownPkVal1" value="%{#request.hemozygousUnknownPkVal}" scope="request"/>	      
 							<s:hidden name="hemoMultiTraitPkVal" id="hemoMultiTraitPkVal" value="%{#request.hemoMultiTraitPkVal}" />
							<s:set name="hemoMultiTraitPkVal1" value="%{#request.hemoMultiTraitPkVal}" scope="request"/>
 							<td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.hemoglobinScrn"></s:text>:</td>
							<td width="20%"><s:select name="cdrCbuPojo.hemoglobinScrn" id="hemoglobinScrnTestBase" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" disabled="true"></s:select></td>
 		                    <td width="20%"></td>	
 		                    <td width="20%"></td>
 		                    <td width="20%"></td>
						</tr>
						<tr><td>
				             	<span id="showassessbuttonBase" ><s:if test="(cdrCbuPojo.hemoglobinScrn==#request.alphaThalismiaTraitPkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDonePkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousUnknownPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDiesasePkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoAlphaThalassmiaPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoTraitPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoMultiTraitPkVal1) && (#request.assesDoneFrHemo==true)"><br><a href="#"  id="showassesmentHemoButtonBase" onclick="showAssmentBase('hemoglobinScrnTestBase','showassessbuttonBase','Assessment_HemoglobinopathyBase','Assessment_LabSummeryHemoBase','hideassesmentHemoBase');"><s:text name="garuda.label.dynamicform.showassess"/></a></s:if></span>
				             	<div id="hideassesmentHemoBase" style="display: none;"><s:if test="(cdrCbuPojo.hemoglobinScrn==#request.alphaThalismiaTraitPkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDonePkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousUnknownPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDiesasePkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoAlphaThalassmiaPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoTraitPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoMultiTraitPkVal1) && (#request.assesDoneFrHemo==true)"><a href="#"  id="hideassesmentHemoButtonBase" onclick="hideAssmentBase('Assessment_LabSummeryHemoBase','showassessbuttonBase','hideassesmentHemoBase');"><s:text name="garuda.label.dynamicform.hideassess"/></a></s:if></div>
				             	</td>
				             </tr>
	             			<tr id="Assessment_LabSummeryHemoBase"  style="display: none;"><td colspan="5">
						          	<fieldset><legend><s:text name="garuda.clinicalnote.label.assessment"></s:text>&nbsp;For&nbsp;<s:text name="garuda.cordentry.label.hemoglobinScrn"/>
												</legend><table>
												<tr >
										<td colspan="5" id="Assessment_HemoglobinopathyBase">
											
										</td></tr></table></fieldset></td>
										<td></td>
									</tr>	
			</table></div>
			<table><tr align="right">
			      <td colspan="5">
			       <s:if test="hasEditPermission(#request.updateLabSmmry)==true && cdrCbuPojo.site.isCDRUser()==true">
			      <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
  				  </s:if>
   				 <s:else>
			        <input type="button" name="editLabSumm"
			        onclick="fn_showModalLabSumm('Edit Lab Summary For CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','updateLabSummary?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CLINICAL_RECORD_LAB_SUM" />&update=False&cordId=<s:property
			        value="cdrCbuPojo.cordID" />&entityType=<s:property 
			        value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_TYPE" />&entitySubtype=<s:property 
			        value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_SUB_TYPE" />&cordentry=true','500','1100','labSummaryModal');"
			        value="<s:text name="garuda.common.lable.edit"/>"></input>
			        </s:else>
			       </s:if>
			      </td>
			    </tr></table>
		<!-- <div id="added1Div" style="overflow: scroll;">	 -->
		<table width="100%" cellpadding="0" style="table-layout:fixed;" cellspacing="0" border="1">   
		      <tr>
			     <td> 
			     	<div id="detaillabDivData" style="overflow: auto;">
				    <table width="25%" height="25%" cellpadding="0" cellspacing="0" border="1" style="overflow: visible;" id="addedStact">
				    <% int headerCount=3; %>
					  <thead>
						    <tr>
							     <th class="headcolBase"><s:text name="garuda.common.lable.test"></s:text></th>
							     <th width="25%">
							       <table cellpadding="0" cellspacing="0">
							          <tr><td colspan="2"><s:text name="garuda.common.lable.preProcess"></s:text></td></tr> 
									   <tr><td><s:text name="garuda.common.lable.testingDate"></s:text></td>
									     <td>
										   <s:date name="prePatLabsPojo.testdate" id="datepicker52" format="MMM dd, yyyy" />
										   <s:textfield readonly="true" onkeydown="cancelBack();"  name="preTestDateStrn" id="preTestDateStrn" value="%{datepicker52}" cssStyle="width: 210px;"></s:textfield>
										  </td>
									  </tr>							    
							       </table>	
							     </th>
								    	
						    <th width="25%">
							 <table cellpadding="0" cellspacing="0">
							   <tr><td colspan="2"><s:text name="garuda.common.lable.postProcess"></s:text></td></tr> 
							    <tr><td><s:text name="garuda.common.lable.testingDate"></s:text></td>
							       <td>
							        <s:date name="postPatLabsPojo.testdate" id="datepicker53" format="MMM dd, yyyy" />
								      <s:textfield readonly="true" onkeydown="cancelBack();"  name="postTestDateStrn" id="postTestDateStrn" value="%{datepicker53}" cssStyle="width: 210px;"></s:textfield>
							       </td>
							     </tr>
							   </table>
							  </th>   
						<s:if test="otherPostTestList!=null && otherPostTestList.size()>0 ">
						 <%  headerCount=headerCount+1; %>
						  	<th width="25%"><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
						  	<s:date name="otherPatLabsPojo.testdate" id="datepicker92" format="MMM dd, yyyy" />
							<s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn1" id="otherTestDateStrn1"  value="%{datepicker92}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
						  	
						  	</th>
					   </s:if>
					   
					   <s:if test="otherPostTestListFirst!=null && otherPostTestListFirst.size()>0 ">
					    <%  headerCount=headerCount+1; %>
						    <th width="25%"><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
						     <s:date name="otherPatLabsPojoFirst.testdate" id="datepicker94" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn2" id="otherTestDateStrn2" onchange="changeProgress(1,'datepicker94')" value="%{datepicker94}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
						    
						    </th>
						  </s:if>
						  
						 <s:if test="otherPostTestListSecond!=null && otherPostTestListSecond.size()>0 ">
						  <%  headerCount=headerCount+1; %>
							    <th width="25%"><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
							  <s:date name="otherPatLabsPojoSecond.testdate" id="datepicker95" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn3" id="otherTestDateStrn3" onchange="changeProgress(1,'datepicker95')" value="%{datepicker95}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
							    
							    
							    </th>
						 </s:if>
						<s:if test="otherPostTestListThird!=null && otherPostTestListThird.size()>0 ">
						 <%  headerCount=headerCount+1; %>
							    <th width="25%"><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
							 <s:date name="otherPatLabsPojoThird.testdate" id="datepicker96" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn4" id="otherTestDateStrn4" onchange="changeProgress(1,'datepicker96')" value="%{datepicker96}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
							    
							    
							    </th>
						 </s:if>
					 
						<s:if test="otherPostTestListFourth!=null && otherPostTestListFourth.size()>0 ">
						 <%  headerCount=headerCount+1; %>
							    <th width="25%"><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
							  <s:date name="otherPatLabsPojoFourth.testdate" id="datepicker97" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn5" id="otherTestDateStrn5" onchange="changeProgress(1,'datepicker97')" value="%{datepicker97}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
							    
							    
							    </th>
						 </s:if>
					 
						 <s:if test="otherPostTestListFifth!=null && otherPostTestListFifth.size()>0 ">
						  <% headerCount=headerCount+1; %>
							  <th width="25%"><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>							  
 								<s:date name="otherPatLabsPojoFive.testdate" id="datepicker98" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn6" id="otherTestDateStrn6" onchange="changeProgress(1,'datepicker98')" value="%{datepicker98}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
 
							  </th>
						 </s:if>
					   
					   
							    
						   </tr>
<!--<th width="25%"><s:text name="garuda.common.lable.addTesting"></s:text></th>999-->
					 </thead>	
					   <tbody>
					   <% int testCount=1; %> 
					                    <!--<s:property value="labTestPojo.pkLabtest" /><s:property value="cdrCbuPojo.fkSpecimenId" />-->
						 <s:iterator value="processingTest" var="rowVal" status="row">
							<tr style="width: 100%;" id="<s:property	value="%{#row.index}"/>"
								    onMouseOver="highlight('<s:property	value="%{#row.index}"/>')"
								    onmouseout="removeHighlight('<s:property     value="%{#row.index}"/>')">
									    <% request.setAttribute("testCount",testCount); %>             
								
								<s:if test="labtestHower!=' '"> 
																		            	
								      <td class="headcolBase"><s:property value="labtestName"/></td>
								  </s:if>
								  <s:else>
									  <td class="headcolBase" onmouseover="return overlib('<s:property value="labtestHower"/>');"
											onmouseout="return nd();"><s:property value="labtestName"/></td>
								 </s:else>
								
								<%--<td width="30%" onmouseover="return overlib('<s:property value="labtestHower"/>');"
											onmouseout="return nd();"><s:property value="labtestName"/></td>
								 <% int precount=0; %> --%>
								<s:set name="checkEmpty" value="0" scope="request"/>
								     <td class="BasicText" >
								     <s:iterator value="preTestList">
									     <s:if test="pkLabtest==fktestid">
									      <s:if test="#request.valuePreFkTimingTest==fkTimingOfTest">
									      <s:set name="checkEmpty" value="1" scope="request"/>
									      <%-- <%precount++; %> --%>
									   		<s:set name="check" value="%{'check'}" />
										        
													 <s:hidden name="savePreTestList[%{#row.index}].fktestid" value="%{pkLabtest}"></s:hidden>  
													 <s:hidden name="savePreTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}"></s:hidden> 
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count' || labtestName=='CBU Nucleated Cell Count Concentration'">		 
							                         <s:text name="garuda.cbuentry.label.testresult"></s:text>
							                         <input type="text" style="width:50%" name="savePreTestList[%{#row.index}].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" readonly="true" onkeydown="cancelBack();"  />
					 							</s:if>
					 							<s:else>
													<s:set name="testresult" value="%{testresult}" scope="request"/>
													<s:if test="#request.testresult != null">
													<s:text name="garuda.cbuentry.label.testresult"></s:text>
													<s:textfield name="savePreTestList[%{#row.index}].testresult" value="%{testresult}" size="10" theme="simple" readonly="true" onkeydown="cancelBack();"  /> 
													<s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePreTestList[%{#row.index}].fktestspecimen" id="savePreTestListB%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" disabled="true"></s:select>
													</s:if> 
												</s:else>
					 								   <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">&nbsp;ml</s:if>
													   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x10<sup>7</sup></s:if>
													   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x10<sup>3</sup>/ml</s:if>
					 								
				            				
					            		  </s:if>
					            		  </s:if>
									  </s:iterator>
									  <s:if test="#request.checkEmpty==0 && preTestList.size()>0">
					               		 		&nbsp;
					               		</s:if>
									
								           <s:if test="%{#check!='check'}">	
									           
										             <s:hidden name="savePreTestList[%{#row.index}].fktestid" value="%{pkLabtest}"></s:hidden>  
										             <s:hidden name="savePreTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}"></s:hidden> 
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count' || labtestName=='CBU Nucleated Cell Count Concentration'">		 
							                         <s:text name="garuda.cbuentry.label.testresult"></s:text>
							                         <input type="text" style="width:50%" name="savePreTestList[%{#row.index}].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" disabled="disabled" />
					 							</s:if>
					 							<s:else>
					 								<s:set name="testresult" value="%{testresult}" scope="request"/>
													<s:if test="#request.testresult != null">
													<s:text name="garuda.cbuentry.label.testresult"></s:text>
													<s:textfield name="savePreTestList[%{#row.index}].testresult" value="%{testresult}" size="10" theme="simple" readonly="true" onkeydown="cancelBack();"  /> 
													<s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePreTestList[%{#row.index}].fktestspecimen" id="savePreTestListB%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" disabled="true"></s:select>
													</s:if> 
					 							</s:else>	
					 								   <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
													   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x10<sup>7</sup></s:if>
													   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x10<sup>3</sup><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
					 							
						                	   
	 	                                    </s:if> </td>
	 								<%-- <% int postcount=0;  %> --%>
	 								<s:set name="checkEmpty" value="0" scope="request"/>
	 								<s:set name="cellCountFlag" value="0" scope="request"/>
 							            <td class="BasicText">
 							           <s:iterator value="postTestList">
 									           	  <!--<s:property value="pkLabtest"/><s:property value="fktestid" /> -->
									        <s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     <s:if test="#request.valuePostFkTimingTest==fkTimingOfTest">
									     <s:if test="#request.cellCountFlag==0">
									        <s:set name="checkEmpty" value="1" scope="request"/>  
									        <s:set name="check1" value="%{'check'}"/>
									         		<s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTestB%{#row.index}"></s:hidden>
												   <s:text name="garuda.cbuentry.label.testresult"></s:text>
									           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
									           	    <s:iterator value="postTestList" status="innerrow">
									           	    <s:if test="fktestid==#request.TNCUncFkTestId">
									           	    <s:if test="#request.cellCountFlag==0">
									           	    <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>
											       	<input type="text" style="width:45%" disabled="disabled" id="testresultB<s:property value="%{#row.index}"/>"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" />
											       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
											       	<s:set name="cellCountFlag" value="1" scope="request"/>
											       	</s:if></s:if>
											       	</s:iterator>
											       </s:if>
												   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
												   <s:iterator value="postTestList" status="innerrow">
									           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
									           	    <s:if test="#request.cellCountFlag==0">
									           	    <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="testB%{#row.index}"></s:hidden>
											       	<input type="text" style="width:45%" disabled="disabled" id="testresultB<s:property value="%{#row.index}"/>"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" />
											       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
											       	<s:set name="cellCountFlag" value="1" scope="request"/>
											       	</s:if></s:if>
											       	</s:iterator>
											       	<s:if test="#request.cellCountFlag==0">
											       	<s:iterator value="postTestList" status="innerrow">
									           	    <s:if test="fktestid==#request.TNCUncFkTestId">
									           	    <s:if test="#request.cellCountFlag==0">
									           	    <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>
											       	<input type="text" style="width:45%" disabled="disabled" id="testresultB<s:property value="%{#row.index}"/>"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" />
											       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
											       	<s:set name="cellCountFlag" value="1" scope="request"/>
											       	</s:if></s:if>
											       	</s:iterator>
											       	</s:if>
											       	</s:elseif>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										      </s:if></s:if>
 									     </s:if>
									     <s:elseif test="pkLabtest==fktestid &&!(pkLabtest == #request.TNCFkTestId) && (labtestName!='NAT HIV' && labtestName!='NAT HCV') ">
									        <s:if test="#request.valuePostFkTimingTest==fkTimingOfTest">
									       <%--  <%postcount++;%> --%>
									       <s:set name="checkEmpty" value="1" scope="request"/>
									          <s:set name="check1" value="%{'check'}"/>
									           <%-- <%if(postcount>precount){%>
									         <td>&nbsp;</td> 
									         <%} %> --%>
									          
											       <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{pkLabtest}"></s:hidden>  
												   <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}"></s:hidden>
												   <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      	&nbsp;
											      </s:if>
											      <s:else>
											      <s:text name="garuda.cbuentry.label.testresult"></s:text>
												   <input type="text" style="width:50%" disabled="disabled" name="savePostTestList[%{#row.index}].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" />  
										           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
												   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='CBU nRBC Absolute Number' "></s:if>
												   <s:if test="labtestName=='CBU nRBC %' ">%</s:if>
												   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
												   <s:if test="labtestName=='Viability' ">%</s:if>
												   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
												   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
									               <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
									               <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
												   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												</s:else>  
												 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL">
												     <s:text name="garuda.common.lable.method"></s:text>
												     <s:select name="savePostTestList[%{#row.index}].fktestmethod" id="cfuSelectedView" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="cfuCount(this.value)" disabled="true"></s:select>
												   <div id="cfuCountView"> 
												   		<s:text name="garuda.common.lable.describe"></s:text>
													 	<s:textfield disabled="true" name="savePostTestList[%{#row.index}].testMthdDesc" value="%{testMthdDesc}"></s:textfield>
													</div>
												 </s:if>
												  
												   <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
													    <s:text name="garuda.common.lable.method"></s:text>
													    <s:select name="savePostTestList[%{#row.index}].fktestmethod" id="viabSelectedView" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)" disabled="true"></s:select>
												      <div id="viabilityView"> 
												      	<s:text name="garuda.common.lable.describe"></s:text>
														<s:textfield disabled="true" name="savePostTestList[%{#row.index}].testMthdDesc" value="%{testMthdDesc}"></s:textfield>
													  </div>													 
												   </s:if>
									            	<s:if test="#request.testCount>#request.labGroupTypTests">
												   	</br><s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePostTestList[%{#row.index}].fktestspecimen" id="savePostTestListB%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" disabled="true"></s:select>
												   </s:if>
									            
									        </s:if>
									        </s:elseif>
									   </s:iterator>
									           <s:if test="#request.checkEmpty==0 && postTestList.size()>=#request.labGroupTypTests">
					               		 			&nbsp;
					               				</s:if>
					               				<s:if test="#request.testCount<=#request.labGroupTypTests">
									       <s:if test="%{#check1!='check'}">
									           <s:if test="pkLabtest == #request.TNCFkTestId">
									         		<s:if test="cdrCbuPojo.isSystemCord=='yes'">
									         				<s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
													</s:if>
													<s:else>
												 			<s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
												  	</s:else>
												   <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTestB%{#row.index}"></s:hidden>
												   <s:text name="garuda.cbuentry.label.testresult"></s:text>
									           	   <input type="text" style="width:45%" disabled="disabled" id="testresultB<s:property value="%{#row.index}"/>"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" />
									           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
													<s:text name="garuda.labtest.label.uncrctd"></s:text>
											       </s:if>
												   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
											       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
											       	</s:elseif>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										 
 									     </s:if>
 									     <s:elseif test="!(pkLabtest == #request.TNCFkTestId) && (labtestName!='NAT HIV' && labtestName!='NAT HCV')">
									           	   <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{pkLabtest}"></s:hidden>  
												   <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}"></s:hidden>
												    <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      	&nbsp;
											      </s:if>
											      <s:else>
											      <s:text name="garuda.cbuentry.label.testresult"></s:text>
												   <input type="text" style="width:50%" disabled="disabled" name="savePostTestList[%{#row.index}].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" />  
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
												   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='CBU nRBC Absolute Number'"></s:if>
												   <s:if test="labtestName=='CBU nRBC %' ">%</s:if>
												   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
												   <s:if test="labtestName=='Viability' ">%</s:if>
												   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
												   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
												   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
												   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
												   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
											         </s:else>  	    
											    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL">
												     	<s:text name="garuda.common.lable.method"></s:text> 
												     	<s:select name="savePostTestList[%{#row.index}].fktestmethod" id="cfuSelectedView" value="%{fktestmethod}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="cfuCount(this.value)" disabled="true"></s:select>
												  <div id="cfuCountView">   
												  		<s:text name="garuda.common.lable.describe"></s:text>
													 	<s:textfield disabled="true" name="savePostTestList[%{#row.index}].testMthdDesc" id="xyz" value="%{testMthdDesc}"></s:textfield>
										          </div>		
										        </s:if>
												
												<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
										              <s:text name="garuda.common.lable.method"></s:text>
										              <s:select name="savePostTestList[%{#row.index}].fktestmethod" id="viabSelectedView" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)" disabled="true"></s:select>
												      
												   <div id="viabilityView"> 
												      <s:text name="garuda.common.lable.describe"></s:text>
												      <s:textfield disabled="true" name="savePostTestList[%{#row.index}].testMthdDesc" value="%{testMthdDesc}"></s:textfield>
												 	  
												  </div>
												  
												</s:if>
									         	<s:if test="#request.testCount>#request.labGroupTypTests">
												   	</br><s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePostTestList[%{#row.index}].fktestspecimen" id="savePostTestListB%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" disabled="true"></s:select>
												  </s:if></s:elseif>
									      </s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
									      
												<s:if test="otherPostTestList!=null && otherPostTestList.size()>0 ">	
												<s:set name="checkEmpty" value="0" scope="request"/>				  
													<td Class="BasicText">
													<s:iterator value="otherPostTestList">
													<s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherFkTimingTest==fkTimingOfTest">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check2" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTestB%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestListB1<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestList" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn1%{#row.index}')"> </s:textfield> 
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if> >
																		<s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTmB1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																		<s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																		</div>
																									 	
																	    <s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>	
																		 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		   <s:text name="garuda.common.label.testMthd"></s:text>	
									      								   <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB1%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		   <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		 </s:if>
																		 <s:else>						
																		  <s:text name="garuda.common.label.testMthd"></s:text>
									      								  <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB1%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		 <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		</s:else>
																	   <div id="testMethDescOthTmB1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   <s:text name="garuda.common.lable.describe"></s:text>
																	   <s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																	   </div></s:if>
																	</div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestList" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>
														       	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn1%{#row.index}')"> </s:textfield>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<div id="showHardDropDn1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if> >
																		<s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTmB1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																		<s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																		</div>
																									 	
																	    <s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>	
																		 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		   <s:text name="garuda.common.label.testMthd"></s:text>	
									      								   <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB1%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		   <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		 </s:if>
																		 <s:else>						
																		  <s:text name="garuda.common.label.testMthd"></s:text>
									      								  <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB1%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		 <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		</s:else>
																	   <div id="testMethDescOthTmB1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   <s:text name="garuda.common.lable.describe"></s:text>
																	   <s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																	   </div></s:if>
																	</div>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														       	</s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestList" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="testB%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn1%{#row.index}')"> </s:textfield>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if> >
																		<s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTmB1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																		<s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																		</div>
																									 	
																	    <s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>	
																		 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		   <s:text name="garuda.common.label.testMthd"></s:text>	
									      								   <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB1%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		   <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		 </s:if>
																		 <s:else>						
																		  <s:text name="garuda.common.label.testMthd"></s:text>
									      								  <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB1%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		 <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		</s:else>
																	   <div id="testMethDescOthTmB1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   <s:text name="garuda.common.lable.describe"></s:text>
																	   <s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																	   </div></s:if>
																	</div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>									   
											      			</s:if></s:if>
 									    				 </s:if>
									    			<s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
													<s:if test="#request.valueOtherFkTimingTest==fkTimingOfTest">
														<s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check2" value="%{'check'}"/>
																
																
																	 <s:set name="test" value="pkLabtest" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{pkLabtest}" id="testB%{#row.index}"></s:hidden>  
																	 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      								&nbsp;
											      					</s:if>
											      					<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestListB1<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestListB1%{#row.index}testresult" value="%{testresult}"  disabled="true" size="10" theme="simple"> </s:textfield> 
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestListB1%{#row.index}fktestspecimen" value="%{fktestspecimen}" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
											     					 </s:elseif>
											     					<s:else> 
																	 <div id="saveOtherPostTestListB1<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestListB1%{#row.index}testresult" value="%{testresult}" size="10" theme="simple" readonly="true" onkeydown="cancelBack();"  onchange="vidSaved('showHardDropDn1%{#row.index}')" ></s:textfield> 
																	 <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'"></s:if>
																	   <s:if test="labtestName=='CBU nRBC %' "></s:if>
																	   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='Viability' ">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count' ">%</s:if>
																	   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>	
																	
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>						
																		     
																		   <div id="showHardDropDn1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if> >  
																		      <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			<div id="reasonFrTestDescOthTmB1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestListB1%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" readonly="true" onkeydown="cancelBack();"  onkeypress="validate"></s:textfield>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestListB1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" disabled="true" headerValue="Select"></s:select>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>								
																		<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB1%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		   <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		 </s:if>
																		 <s:else>						
																		  <s:text name="garuda.common.label.testMthd"></s:text>
									      								  <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB1%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		 <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		</s:else>
																	   <div id="testMethDescOthTmB1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestListB1%{#row.index}testMthdDesc" value="%{testMthdDesc}" readonly="true" onkeydown="cancelBack();"  onkeypress="validate"></s:textfield>
																		</div></s:if>
																		</div></s:else>
																</s:if>
											 				</s:elseif>
										 				</s:iterator>
										 				<s:if test="#request.checkEmpty==0 && otherPostTestList.size()>#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>
										               		<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check2!='check'}">
										 		            <s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
															  		<s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTestB%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestListB1<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn1%{#row.index}')"> </s:textfield> 
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if> >
																		<s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTmB1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																		<s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																		</div>
																									 	
																	    <s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>	
																		 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		   <s:text name="garuda.common.label.testMthd"></s:text>	
									      								   <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB1%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		   <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		 </s:if>
																		 <s:else>						
																		  <s:text name="garuda.common.label.testMthd"></s:text>
									      								  <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB1%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		 <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		</s:else>
																	   <div id="testMethDescOthTmB1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   <s:text name="garuda.common.lable.describe"></s:text>
																	   <s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																	   </div></s:if>
																	</div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{pkLabtest}" id="testB%{#row.index}"></s:hidden>  
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      								&nbsp;
											      				</s:if>
											      				<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestListB1<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestListB1%{#row.index}testresult" value="%{testresult}"  disabled="true" size="10" theme="simple"> </s:textfield> 
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestListB1%{#row.index}fktestspecimen" value="%{fktestspecimen}" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
											     				</s:elseif>
											     				 <s:else> 
																 <div id="saveOtherPostTestListB1<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestListB1%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" disabled="true" onchange="vidSaved('showHardDropDn1%{#row.index}')" >  </s:textfield>
																	 <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
																		   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'"></s:if>
																		   <s:if test="labtestName=='CBU nRBC %' "></s:if>
																		   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		   <s:if test="labtestName=='Viability' ">%</s:if>
																		   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
																		   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count' ">%</s:if>
																		   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="test" value="pkLabtest" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
												
												             <div id="showHardDropDn1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if> >
																		<s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTmB1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																		<s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																		</div>
																									 	
																	    <s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>	
																		 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		   <s:text name="garuda.common.label.testMthd"></s:text>	
									      								   <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB1%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		   <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		 </s:if>
																		 <s:else>						
																		  <s:text name="garuda.common.label.testMthd"></s:text>
									      								  <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB1%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		 <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		</s:else>
																	   <div id="testMethDescOthTmB1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   <s:text name="garuda.common.lable.describe"></s:text>
																	   <s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																	   </div></s:if>
																	</div></s:else> 
																	   <!--
														   
													                <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																       <s:text name="garuda.common.lable.viability"></s:text>
																       <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																	   <s:text name="garuda.common.lable.describe"></s:text>
																	   <s:textfield name="saveOtherPostTestList1[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																 </s:if>
														     -->	</s:elseif> 
											        </s:if> </s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										     </s:if>
										     
											<s:if test="otherPostTestListFirst!=null && otherPostTestListFirst.size()>0 ">					  
												<s:set name="checkEmpty" value="0" scope="request"/>
												<td Class="BasicText">
												<s:iterator value="otherPostTestListFirst">
													<s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestFirst==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check3" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValueB%{#row.index}"></s:hidden>
														     <div id="saveOtherPostTestListB2<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListFirst" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn2%{#row.index}')"> </s:textfield> 
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>															     
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB2%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB2%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB2%{#row.index}testMthdDesc"  value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
																		</div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestListFirst" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>
													       	   	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn2%{#row.index}')"> </s:textfield>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<div id="showHardDropDn2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>															     
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB2%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB2%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB2%{#row.index}testMthdDesc"  value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
																		</div>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														    </s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListFirst" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn2%{#row.index}')"> </s:textfield>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>															     
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB2%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB2%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB2%{#row.index}testMthdDesc"  value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
																		</div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
														<s:if test="#request.valueOtherPostTestFirst==otherListValue">
														 	<s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check3" value="%{'check'}"/>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{pkLabtest}" id="testB%{#row.index}"></s:hidden>  
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      								&nbsp;
											      					</s:if>
											      					<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestListB2<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestListB2%{#row.index}testresult" value="%{testresult}"  disabled="true" size="10" theme="simple"> </s:textfield> 
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestListB2%{#row.index}fktestspecimen" value="%{fktestspecimen}" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
											     					</s:elseif>
											     					 <s:else> 
																	 <div id="saveOtherPostTestListB2<s:property value="%{#row.index}"/>div">
																	   <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB2%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onchange="vidSaved('showHardDropDn2%{#row.index}')"  ></s:textfield> 
																      <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
																			   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
																			   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
																			   <s:if test="labtestName=='CBU nRBC Absolute Number'"></s:if>
																			   <s:if test="labtestName=='CBU nRBC %' "></s:if>
																			   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																			   <s:if test="labtestName=='Viability' ">%</s:if>
																			   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
																			    <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																			   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count' ">%</s:if>
																			   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																			   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																       <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																       
																       <div id="showHardDropDn2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>															     
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB2%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB2%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB2%{#row.index}testMthdDesc"  value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
																		</div></s:else>
																
																
																	<!--<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																           <s:text name="garuda.common.lable.viability"></s:text>
																           <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		   <s:text name="garuda.common.lable.describe"></s:text>
																		   <s:textfield name="saveOtherPostTestList2[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	 </s:if>
																-->
											 				</s:if>
											 				</s:elseif>
										 				</s:iterator>
										 				<s:if test="#request.checkEmpty==0 && otherPostTestListFirst.size()>#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>
										               		<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		   			 <s:if test="%{#check3!='check'}">
										 		     			<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValueB%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestListB2<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn2%{#row.index}')"> </s:textfield> 
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																      
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB2%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB2%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB2%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																		 </div></s:if>
																		 </div>
													       
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{pkLabtest}" id="testB%{#row.index}"></s:hidden>  
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      								&nbsp;
											      				</s:if>
											      				<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestListB2<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestListB2%{#row.index}testresult" value="%{testresult}"  disabled="true" size="10" theme="simple"> </s:textfield> 
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestListB2%{#row.index}fktestspecimen" value="%{fktestspecimen}" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
											     				</s:elseif>
											     				<s:else> 
																 <div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB2%{#row.index}testresult" value="%{testresult}" size="10" theme="simple" onchange="vidSaved('showHardDropDn2%{#row.index}')"  >  </s:textfield>
																	<s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
																		   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'"></s:if>
																		   <s:if test="labtestName=='CBU nRBC %' "></s:if>
																		   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		   <s:if test="labtestName=='Viability' ">%</s:if>
																		   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
																		    <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count' ">%</s:if>
																		   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>			
																			
																	<div id="showHardDropDn2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																      
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB2%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB2%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB2%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																		 </div></s:if>
																		 </div></s:else>  
																		     <!--
																	
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList2[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if>
													          --></s:elseif></s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										                  </s:if>	     
										     
					                 <s:if test="otherPostTestListSecond!=null && otherPostTestListSecond.size()>0 ">
					                 				<s:set name="checkEmpty" value="0" scope="request"/>	  
												<td Class="BasicText">
												<s:iterator value="otherPostTestListSecond">
													<s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestSecond==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check4" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValueB%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestListB3<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListSecond" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	   <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn3%{#row.index}')"> </s:textfield> 
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>							 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB3%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB3%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
															  </div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestListSecond" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>
														       	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn3%{#row.index}')"> </s:textfield>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<div id="showHardDropDn3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>							 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB3%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB3%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
															  </div>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														       	</s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListSecond" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="testB%{#row.index}"></s:hidden> 
													       	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn3%{#row.index}')"> </s:textfield>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>							 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB3%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB3%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
															  </div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>												   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
													<s:if test="#request.valueOtherPostTestSecond==otherListValue">
												 		<s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check4" value="%{'check'}"/>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
											                         	
																	 <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{pkLabtest}" id="testB%{#row.index}"></s:hidden>  
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      								&nbsp;
											      					</s:if>
											      					<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestListB3<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestListB3%{#row.index}testresult" value="%{testresult}"  disabled="true" size="10" theme="simple"> </s:textfield> 
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestListB3%{#row.index}fktestspecimen" value="%{fktestspecimen}" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
											     					</s:elseif>
											     					<s:else> 
																	 <div id="saveOtherPostTestListB3<s:property value="%{#row.index}"/>div">
																	   <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB3%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onchange="vidSaved('showHardDropDn3%{#row.index}')"  ></s:textfield> 
																      <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
																			   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
																			   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
																			   <s:if test="labtestName=='CBU nRBC Absolute Number'"></s:if>
																			   <s:if test="labtestName=='CBU nRBC %' "></s:if>
																			   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																			   <s:if test="labtestName=='Viability' ">%</s:if>
																			   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
																			    <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																			   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count' ">%</s:if>
																			   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																			   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																      <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																      
																      <div id="showHardDropDn3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																														     
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB3%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB3%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
																	</div></s:else>
															
																		<!--<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList3[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if>
																 -->
											 				</s:if>
											 				</s:elseif>
										 				</s:iterator>
										 				<s:if test="#request.checkEmpty==0 && otherPostTestListSecond.size()>#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>
										               		<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check4!='check'}">
										 		     	<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValueB%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestListB3<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn3%{#row.index}')"> </s:textfield> 
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>							 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB3%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB3%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
															  </div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{pkLabtest}" id="testB%{#row.index}"></s:hidden>  
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      								&nbsp;
											      				</s:if>
											      				<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestListB3<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestListB3%{#row.index}testresult" value="%{testresult}"  disabled="true" size="10" theme="simple"> </s:textfield> 
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestListB3%{#row.index}fktestspecimen" value="%{fktestspecimen}" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
											     				</s:elseif>
											     				<s:else> 
																 <div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																    <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB3%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onchange="vidSaved('showHardDropDn3%{#row.index}')"  >  </s:textfield>
																	 <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
																		   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'"></s:if>
																		   <s:if test="labtestName=='CBU nRBC %' "></s:if>
																		   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		   <s:if test="labtestName=='Viability' ">%</s:if>
																		   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
																		    <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count' ">%</s:if>
																		   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
												
																		<div id="showHardDropDn3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>							 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB3%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB3%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
															  </div></s:else>
																		<!--<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList3[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	    </s:if>
											                  --></s:elseif></s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										               </s:if>	     

					<s:if test="otherPostTestListThird!=null && otherPostTestListThird.size()>0 ">	
					<s:set name="checkEmpty" value="0" scope="request"/>				  
												<td Class="BasicText">
												<s:iterator value="otherPostTestListThird">
													<s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestThird==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check5" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValueB%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestListB4<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListThird" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn4%{#row.index}')"> </s:textfield> 
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>     
																	        <s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB4%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB4%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
														              	</div></s:if>
														              	</div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestListThird" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>
														       	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn4%{#row.index}')"> </s:textfield>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<div id="showHardDropDn4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>     
																	        <s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB4%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB4%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
														              	</div></s:if>
														              	</div>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														    </s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListThird" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn4%{#row.index}')"> </s:textfield>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>     
																	        <s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB4%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB4%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
														              	</div></s:if>
														              	</div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>												   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
													<s:if test="#request.valueOtherPostTestThird==otherListValue">
														  <s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check5" value="%{'check'}"/>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{pkLabtest}" id="testB%{#row.index}"></s:hidden>  
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      								&nbsp;
											      					</s:if>
											      					<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestListB4<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestListB4%{#row.index}testresult" value="%{testresult}"  disabled="true" size="10" theme="simple"> </s:textfield> 
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestListB4%{#row.index}fktestspecimen" value="%{fktestspecimen}" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
											     					</s:elseif>
											     					<s:else> 
																	 <div id="saveOtherPostTestListB4<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB4%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onchange="vidSaved('showHardDropDn4%{#row.index}')" ></s:textfield> 
																      <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
																		   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'"></s:if>
																		   <s:if test="labtestName=='CBU nRBC %' "></s:if>
																		   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		   <s:if test="labtestName=='Viability' ">%</s:if>
																		   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
																		    <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count' ">%</s:if>
																		   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																      <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																			
																	<div id="showHardDropDn4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																															     
																			  <s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB4%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB4%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																		</div></s:if>
																		</div></s:else> 
																		     <!--
																	
																	  <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																           <s:text name="garuda.common.lable.viability"></s:text>
																           <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		   <s:text name="garuda.common.lable.describe"></s:text>
																		   <s:textfield name="saveOtherPostTestList4[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if>
																-->
											 				</s:if>
											 				</s:elseif>
										 				</s:iterator>
										 				<s:if test="#request.checkEmpty==0 && otherPostTestListThird.size()>#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>
										               		<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check5!='check'}">
										 		     		<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValueB%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestListB4<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn4%{#row.index}')"> </s:textfield> 
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>     
																	        <s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB4%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB4%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
														              	</div></s:if>
														              	</div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{pkLabtest}" id="testB%{#row.index}"></s:hidden>  
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      								&nbsp;
											      				</s:if>
											      				<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestListB4<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestListB4%{#row.index}testresult" value="%{testresult}"  disabled="true" size="10" theme="simple"> </s:textfield> 
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestListB4%{#row.index}fktestspecimen" value="%{fktestspecimen}" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
											     				</s:elseif>
											     				<s:else> 
																 <div id="saveOtherPostTestListB4<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB4%{#row.index}testresult" value="%{testresult}" size="10" theme="simple" onchange="vidSaved('showHardDropDn4%{#row.index}')"  >  </s:textfield>
																	 <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
																		   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'"></s:if>
																		   <s:if test="labtestName=='CBU nRBC %' "></s:if>
																		   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		   <s:if test="labtestName=='Viability' ">%</s:if>
																		   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
																		   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count' ">%</s:if>
																		   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	       
																	  <div id="showHardDropDn4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>     
																	        <s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB4%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB4%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
														              	</div></s:if>
														              	</div></s:else>
																		<!--<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList4[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if>
											              --></s:elseif></s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										         </s:if>	     
			
								<s:if test="otherPostTestListFourth!=null && otherPostTestListFourth.size()>0 ">					  
												<s:set name="checkEmpty" value="0" scope="request"/>
													<td Class="BasicText">
												<s:iterator value="otherPostTestListFourth">
													<s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestFourth==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check6" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValueB%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestListB5<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListFourth" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn5%{#row.index}')"> </s:textfield> 
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB5%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB5%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																		   </div></s:if>
																		   </div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestListFourth" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
														       	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn5%{#row.index}')"> </s:textfield>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<div id="showHardDropDn5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB5%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB5%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																		   </div></s:if>
																		   </div>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														       	</s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListFourth" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="testB%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn5%{#row.index}')"> </s:textfield>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB5%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB5%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																		   </div></s:if>
																		   </div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>											   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
													<s:if test="#request.valueOtherPostTestFourth==otherListValue">
														  <s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check6" value="%{'check'}"/>
															
																
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{pkLabtest}" id="testB%{#row.index}"></s:hidden>  
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      								&nbsp;
											      					</s:if>
											      					<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestListB5<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestListB5%{#row.index}testresult" value="%{testresult}"  disabled="true" size="10" theme="simple"> </s:textfield> 
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestListB5%{#row.index}fktestspecimen" value="%{fktestspecimen}" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
											     					</s:elseif>
											     					<s:else> 
																	 <div id="saveOtherPostTestListB5<s:property value="%{#row.index}"/>div">
																	  <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB5%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onchange="vidSaved('showHardDropDn5%{#row.index}')" ></s:textfield> 
																	 <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
																		   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'"></s:if>
																		   <s:if test="labtestName=='CBU nRBC %' "></s:if>
																		   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		   <s:if test="labtestName=='Viability' ">%</s:if>
																		   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
																		   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count' ">%</s:if>
																		   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	
																	<div id="showHardDropDn5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>	 
																			 <s:text name="garuda.common.label.reaForTest"></s:text>
																			 <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB5%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB5%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																		</div></s:if>
																		</div></s:else>
																	<!--<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																		 	 
																	   </s:if>
																
																-->
											 				</s:if>
											 				</s:elseif>
										 				</s:iterator>
										 				<s:if test="#request.checkEmpty==0 && otherPostTestListFourth.size()>#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>
										               		<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check6!='check'}">
										 		    		<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValueB%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestListB5<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn5%{#row.index}')"> </s:textfield> 
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB5%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB5%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																		   </div></s:if>
																		   </div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{pkLabtest}" id="testB%{#row.index}"></s:hidden>  
																<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      								&nbsp;
											      				</s:if>
											      				<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestListB5<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestListB5%{#row.index}testresult" value="%{testresult}"  disabled="true" size="10" theme="simple"> </s:textfield> 
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestListB5%{#row.index}fktestspecimen" value="%{fktestspecimen}" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
											     				</s:elseif>
											     				<s:else> 
																 <div id="saveOtherPostTestListB5<s:property value="%{#row.index}"/>div">
																 <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB5%{#row.index}testresult" value="%{testresult}" size="10" theme="simple" onchange="vidSaved('showHardDropDn5%{#row.index}')" >  </s:textfield>
																	  <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
																		   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'"></s:if>
																		   <s:if test="labtestName=='CBU nRBC %' "></s:if>
																		   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		   <s:if test="labtestName=='Viability' ">%</s:if>
																		   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
																		    <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count' ">%</s:if>
																		   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												                      <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
												
																		<div id="showHardDropDn5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB5%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB5%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																		   </div></s:if>
																		   </div> </s:else> 
																		     <!--
	
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																             <s:text name="garuda.common.lable.viability"></s:text>
																             <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if>
											         --></s:elseif></s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										     </s:if>	     
			
													     
								<s:if test="otherPostTestListFifth!=null && otherPostTestListFifth.size()>0 ">					  
												<s:set name="checkEmpty" value="0" scope="request"/>
												<td Class="BasicText">
												<s:iterator value="otherPostTestListFifth">
													<s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestFifth==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check7" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValueB%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestListB6<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListFifth" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn6%{#row.index}')"> </s:textfield> 
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>	
																			<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB6%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB6%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
																		</div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestListFifth" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
														       	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn6%{#row.index}')"> </s:textfield>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<div id="showHardDropDn6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>	
																			<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB6%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB6%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
																		</div>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														       	</s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListFifth" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn6%{#row.index}')"> </s:textfield>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>	
																			<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB6%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB6%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
																		</div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>												   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
													<s:if test="#request.valueOtherPostTestFifth==otherListValue">
														<s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check7" value="%{'check'}"/>
																
																
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{pkLabtest}" id="testB%{#row.index}"></s:hidden>  
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      								&nbsp;
											      					</s:if>
											      					<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestListB6<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestListB6%{#row.index}testresult" value="%{testresult}"  disabled="true" size="10" theme="simple"> </s:textfield> 
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestListB6%{#row.index}fktestspecimen" value="%{fktestspecimen}" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
											     					</s:elseif>
											     					<s:else> 
																	 <div id="saveOtherPostTestListB6<s:property value="%{#row.index}"/>div">
																	 <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB6%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onchange="vidSaved('showHardDropDn6%{#row.index}')"  ></s:textfield> 
																       <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
																			   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
																			   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
																			   <s:if test="labtestName=='CBU nRBC Absolute Number'"></s:if>
																			   <s:if test="labtestName=='CBU nRBC %' "></s:if>
																			   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																			   <s:if test="labtestName=='Viability' ">%</s:if>
																			   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
																			    <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																			   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count' ">%</s:if>
																			   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																			   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																       <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																       
																       <div id="showHardDropDn6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason"  disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>	
																			<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB6%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB6%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
																		</div></s:else>
																		<!--<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																             <s:text name="garuda.common.lable.viability"></s:text>
																             <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].notes" value="%{notes}" maxlength="50" onkeypress="validate"></s:textfield>
																	   </s:if>
																-->
											 				</s:if>
											 				</s:elseif>
										 				</s:iterator>
										 				<s:if test="#request.checkEmpty==0 && otherPostTestListFifth.size()>#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>
										               		<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check7!='check'}">
										 		    		<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="testB%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValueB%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestListB6<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDropDn6%{#row.index}')"> </s:textfield> 
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDropDn6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>	
																			<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB6%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB6%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
																		</div>
													       
													   		<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
											      			
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{pkLabtest}" id="testB%{#row.index}"></s:hidden>  
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      								&nbsp;
											      				</s:if>
											      				<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestListB6<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestListB6%{#row.index}testresult" value="%{testresult}"  disabled="true" size="10" theme="simple"> </s:textfield> 
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestListB6%{#row.index}fktestspecimen" value="%{fktestspecimen}" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
											     				</s:elseif>
											     				<s:else> 
																 <div id="saveOtherPostTestListB6<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabsB%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" disabled="true" id="saveOtherPostTestListB6%{#row.index}testresult" value="%{testresult}" size="10" theme="simple" onchange="vidSaved('showHardDropDn6%{#row.index}')"  >  </s:textfield>
																		   <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">&nbsp;x&nbsp;10<sup>7</sup></s:if>   
																		   <s:if test="labtestName=='Total CD34+ Cell Count'">&nbsp;x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'"></s:if>
																		   <s:if test="labtestName=='CBU nRBC %' "></s:if>
																		   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		   <s:if test="labtestName=='Viability' ">%</s:if>
																		   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
																		    <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count' ">%</s:if>
																		   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	
																	<div id="showHardDropDn6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTmB6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTmB6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>	
																			<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB6%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" disabled="true" id="saveOtherPostTestListB6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTmB6%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTmB6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" disabled="true" id="saveOtherPostTestListB6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																			</div></s:if>
																		</div></s:else>
																		     <!--
											         					
											         					 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																             <s:text name="garuda.common.lable.viability"></s:text>
																             <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].notes" value="%{notes}" maxlength="50" onkeypress="validate"></s:textfield>
																	    </s:if>
											            --></s:elseif>
											            </s:if> </s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										      </s:if>	     
 		      
									       </tr>
									       <s:if test="#request.testCount==#request.labGroupTypTests && processingTest.size()>#request.labGroupTypTests">
	                                        <tr><td class="headcolBase">
	                                        <div
												class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
												style="text-align: left;">
												 <s:text name="garuda.cordentry.label.otherLabTests"/>
											</div>
	                                       </td>
	                                       <td colspan="<%=headerCount%>-1">
	                                        <div
												class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
												style="text-align: left;">
												 <%-- <s:text name="garuda.cordentry.label.otherLabTests"/> --%>
											</div>
	                                       </td>
	                                        </tr></s:if>
	                                        <% testCount=testCount+1; %> 
	                                    <s:set name="check" value="%{'uncheck'}"/> 
                                        <s:set name="check1" value="%{'uncheck'}"/>
                                        <s:set name="check2" value="%{'uncheck'}"/>
                                        <s:set name="check3" value="%{'uncheck'}"/>
                                        <s:set name="check4" value="%{'uncheck'}"/>
                                        <s:set name="check5" value="%{'uncheck'}"/>
                                        <s:set name="check6" value="%{'uncheck'}"/>
                                        <s:set name="check7" value="%{'uncheck'}"/>
						   	 </s:iterator>
					   	</tbody>
					   	<tr>
                                        <td class="headcolBase">
                                
                                       </td><td colspan="<%=headerCount%>-1" style="border: 0px;">
                                       
                                       </td>
                                        </tr>
			      	</table>
			   	</div> 
			 </td>
		 </tr> 
		</table><!-- </div> -->
		<table>
		<tr>
					<%-- <td align="left">
						<s:if test="hasNewPermission(#request.updateLabSmmry)==true">
							<input type="button" id="addnewtest"
								value="<s:text name="garuda.cdrcbuview.label.button.addNewTest" />"
								onclick="javascript:addTest('<s:property value="cdrCbuPojo.cordID" />','True');" />
                         </s:if>
					</td> --%>		
					<td align="right">
								         <s:if test="hasEditPermission(#request.updateLabSmmry)==true && cdrCbuPojo.site.isCDRUser()==true">
								        <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
    </s:if>
    <s:else>
								      <input type="button" name="editLabSumm1"
								        onclick="fn_showModalLabSumm('Edit Lab Summary For CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','viewTestScreen?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CLINICAL_RECORD_LAB_SUM" />&cordentry=true&cdrCbuPojo.fkSpecimenId=<s:property
									        value="cdrCbuPojo.fkSpecimenId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId"/>&cdrCbuPojo.cordID=<s:property
									        value="cdrCbuPojo.cordID" />','550','900','labSummeryTests','','','','');"
								        value="<s:text name="garuda.common.lable.edit"/>">
								        </s:else>
				         					</s:if>		
					  
					</td>
			</tr></table>
		<div style="overflow:scroll;">
			<div id="labsummarynotecontent" onclick="toggleDiv('labsummarynote')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text name="garuda.cdrcbuview.label.button.labsummclinicalnotes" /></div>				
			<div id="labsummarynote" style=" width: 463px; height: 100%;">
			
						<div id="loadLabSummaryClinicalNoteDiv">
						<s:set name="cbuCateg" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CLINIC_CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_NOTE_CATEGORY_CODESUBTYPE) " />
						<jsp:include page="modal/cb_load_clinicalNote.jsp"></jsp:include>
				</div>
	
			  <s:if test="hasNewPermission(#request.vClnclNote)==true">
				<s:if test='(cbuFinalReviewPojo.eligibleConfirmFlag == 1 && cbuFinalReviewPojo.licensureConfirmFlag == 1 && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y" ) || (cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)) '>
				 	<button type="button" disabled="disabled"><s:text name="garuda.cdrcbuview.label.button.addnotes" /></button>
				 </s:if>
				   <s:else>
					<button type="button"
					onclick="fn_showModalLabSumm('<s:text name="garuda.cdrcbuview.label.clinical_notes"/> <s:text name="garuda.message.modal.headerregid"/> <s:property value="cdrCbuPojo.registryId"/>','clinicalNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cat=LAB_NOTE_CATEGORY_CODESUBTYPE','550','850','notesModalsDiv');"><s:text
					name="garuda.cdrcbuview.label.button.addnotes" /></button>
				 </s:else>
				 </s:if>
			</div>
			</div>
			<div id="loadLabSummaryDocInfoDiv">
			<s:if test="(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE].size>0 && hasViewPermission(#request.viewLabSumCat)==true)||(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size > 0 && hasViewPermission(#request.viewUnitRepCat)==true)">
           <div id="labSummaryDocContent" onclick="toggleDiv('loadLabSummaryDocInfo')"
		     class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
			 style="text-align: center;">
			<span class="ui-icon ui-icon-triangle-1-s"></span> 
			<s:text name="garuda.uploaddoc.label.button.labsummryuploaddocument" />
			</div>	
			<div id="loadLabSummaryDocInfo">
			<s:if test=" #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE].size>0 ||#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null ||#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size>0">		
			 <s:if test="hasViewPermission(#request.viewLabSumCat)==true">
			 <s:set name="cbuDocCateg" value="@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE"  scope="request" />
			 </s:if>
			 <jsp:include page="modal/cb_load_upload_document.jsp">
			 <jsp:param value="<%=request.getAttribute(\"cbuDocCateg\")%>" name="cbuDocCateg"/>
			 </jsp:include>	 
			</s:if>
			</div>
		</s:if>
		</div>
		<script>
		function fn_showModalLabSumm(title,url,hight,width,id){
			var patientId = $j('#patientId').val();
			var patient_data = '';
			if(patientId!="" && patientId != null && patientId != "undefined"){
				patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
			}
			title=title+patient_data;
			showModals(title,url,hight,width,id);
			}
			</script>