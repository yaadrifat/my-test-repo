<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="userLB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<script>
function mainonload(){    
	
	if($j('#completeReqInfoFlag').val()=="1"){
		$j('#finalReviewBtn').removeAttr('disabled');
	}
	else{
		$j('#finalReviewBtn').attr('disabled', true);
		}
	
	

	if($j('#completeReqInfoFlag').val()=="1"){
		$j('#finalReviewBtn').removeAttr('disabled');
	}
	else{
		$j('#finalReviewBtn').attr('disabled', true);
		}

	
	
	$j('#searchResults').dataTable(); 
	$j('#searchResults1').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
		},
		"bRetrieve": true,
		"bDestroy" :true
	} );
	$j('#searchResults1_length').empty();
	$j('#searchResults1_filter').empty();
	
$j('#searchResults2').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
		},
		"bRetrieve": true,
		"bDestroy" :true
	} );

$j('#searchResults2_length').empty();
$j('#searchResults2_filter').empty();

$j('#searchResults3').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
} );

$j('#searchResults3_length').empty();
$j('#searchResults3_filter').empty();

$j('#searchResults4').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
});
$j('#searchResults4_length').empty();
$j('#searchResults4_filter').empty();

$j('#searchResults5').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
});
$j('#searchResults5_length').empty();
$j('#searchResults5_filter').empty();

$j('#searchResults6').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
});
$j('#searchResults6_length').empty();
$j('#searchResults6_filter').empty();

$j('#searchResults7').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
});
$j('#searchResults7_length').empty();
$j('#searchResults7_filter').empty();

$j('#searchResults8').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
});
$j('#searchResults8_length').empty();
$j('#searchResults8_filter').empty();

$j('#searchResults9').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
});

$j('#searchResults9_length').empty();
$j('#searchResults9_filter').empty();
//$j('#searchResults10').dataTable();

$j('#maternalHlaResults').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	}
});
/*
$j('#idmtestResults').dataTable({
	"oLanguage": {
		"sEmptyTable": "Test Results not available"
	}
});*/

$j('button,input[type=submit],input[type=button]').button();
$j( "#datepicker" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	changeYear: true});
$j( "#datepicker1" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	changeYear: true});
$j( "#datepicker2" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
	changeYear: true});


//fn_events('cbuReqHistoryTbl','targetall2','ulSelectColumn2');
$j('#cbuReqHistoryTbl').css("width","100%");
$j("#cbuRHistory1").attr('checked',true);
$j('#cReqhistory').show();
historyTblOnLoad();
}
function settingwidth(){
	var staticpanelwidth=$j("#tablelookup1").width();
	var columnwidth=(staticpanelwidth/2);
	var datatablewidth=columnwidth-20;	
	var staticpanelheight=$j(".viewcbutoppanel").height();
	$j('.detailhstry').css('max-height',staticpanelheight);
	$j('.datatable').dataTable({
		"bSort": false,
		"sScrollX": datatablewidth,
        "bDeferRender": true
		});
	$j('.datatablewithoutsearch').dataTable({
		"bFilter": false,
		"bLengthChange": false,
	    "bSort": false,
	    "bInfo": false,
		"sScrollX": datatablewidth,
        "bDeferRender": true
		});	
}

$j(function() {
	mainonload();
	lookupDataTable();
	settingwidth();
});
function historyTblOnLoad(){
	
	var staticpanelwidth=$j(".viewcbutoppanel").width();
	var staticpanelheight=$j(".viewcbutoppanel").height();
	var columnwidth=staticpanelwidth-5;
	
	fn_events1('cbuhistorytbl','targetall1','ulSelectColumn1');
	
	
	var hstrytbl1 = $j('#cbuhistorytbl').dataTable({
			"iDisplayLength": 1000,
			"bRetrieve":true,
			"bDestroy":true,
			"sScrollY": staticpanelheight,
			"sScrollX":columnwidth,
			"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] },{"sType": "date", "aTargets": [ 1 ] }],
			"oLanguage": {
				"sSearch": "<s:text name="garuda.common.message.searchCol"/>",
				"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
			}
	});
	
	
	
	$j('#cbuhistorytbl_info').hide();
	$j('#cbuhistorytbl_paginate').hide();
	$j('#cbuhistorytbl_length').empty();
	$j('#cbuhistorytbl_length').replaceWith('Show <select name="showsRow" id="showsRowcbuhistorytbl" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="paginateSearch.getiTotalRows()"/>">All</option></select> Entries');
	
	
	if($j('#hentries').val()!=null || $j('#hentries').val()!=""){
		$j('#showsRowcbuhistorytbl').val($j('#hentries').val());
		}
	
	$j('#showsRowcbuhistorytbl').change(function(){
		paginationFooter(0,"getMain2CbuDetails,showsRowcbuhistorytbl,temp,Tableform,chistory,historyTblOnLoad,pkcordId,flag");
	});
	
	
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('position','static');
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('z-index','1000');
	
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').after('<div style="clear:both" class="clearDiv"></div>');

	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHeadInner table').width(columnwidth);
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollFootInner table').width(columnwidth);
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').find('.dataTables_scrollHeadInner table').width(columnwidth);
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollBody table').width(columnwidth);
	
	
	$j('#cbuhistorytbl_wrapper').css('overflow','hidden');
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollBody').scroll(function(){
		$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('overflow','hidden');
	});
	$j('#cbuhistorytbl_wrapper').find('#thSelectColumn div').click(function(){
		$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	});
	
	if(! $j("#cbuRHistory1").is(':checked')){
		$j("#cbuHistory1").attr('checked',true);
		$j('#chistory').show();
	}

	fn_events('cbuReqHistoryTbl','targetall2','ulSelectColumn2');
	$j('.detailhstry').css('max-height',staticpanelheight);
	
	$j('#cbuHistory1').bind('click',function(ui){
		var oTable = $j('div.dataTables_scrollBody> #cbuhistorytbl', ui.panel).dataTable();
		if ( oTable.length > 0 ) {
			oTable.fnAdjustColumnSizing();
		}
	});
	if ( hstrytbl1.length > 0 ) {
		hstrytbl1.fnAdjustColumnSizing();
	}
	
}

function showDiv(cordId,fkCbbId,userSiteId){
	$j('#recordDiv1').css('display','none');
	refreshDiv("getMain2CbuDetails?cordId="+cordId,"searchTbles1","form1");
	$j('#recordDiv').css('display','block');
	mainonload();
	settingwidth();
	
	if($j('#completeReqInfoFlag').val()=="1"){
		$j('#finalReviewBtn').removeAttr('disabled');
	}
	else{
		$j('#finalReviewBtn').attr('disabled', true);
		}
}

function loadGroup(cordId,fkCbbId,userSiteId,userId)
{
	var url="getGroupsByIds?fkCbbId="+fkCbbId+"&cordId="+cordId+"&userSiteId="+userSiteId+"&userId="+userId;
	//alert(url);
	loadPageByGetRequset(url,'usersitegrpdropdown');
	$j('#searchTbles1').css('display','none');
}

function lookupDataTable(){
	fn_events('tablelookup1','targetall','ulSelectColumn');

	$j('#tablelookup1').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
		},
		"iDisplayLength":1000	
	});
	 $j('#tablelookup1_info').hide();
		$j('#tablelookup1_paginate').hide();
		$j('#tablelookup1_length').empty();
		$j('#tablelookup1_length').replaceWith('Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;" ><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="paginateSearch.getiTotalRows()"/>">All</option></select> Entries');
		if($j('#entries').val()!=null || $j('#entries').val()!=""){
			$j('#showsRow').val($j('#entries').val());
			}
		$j('#showsRow').change(function(){
			paginationFooter(0,"getMainCBUDetails,showsRow,inputs,Tableform,searchTblediv,lookupDataTable");
		});
		var inputs = $j('#inputs').val();
		
		$j("#searchTblediv td:contains('"+inputs.toLowerCase()+"')").each(function (i, el) {
			 highlightContent(el, inputs.toLowerCase());
	    });
		$j("#searchTblediv td:contains('"+inputs.toUpperCase()+"')").each(function (i, el) {
			highlightContent(el, inputs.toUpperCase());
	    });
	    $j('.lookupmenu :text').removeAttr("disabled");
	       
}

function highlightContent(el, inputs){
	 var text = $j(el).text();
     text = text.replace(inputs ,"<span class='highlightCont'>"+inputs+"</span>");
     $j(el).html(text); 
}
</script>

<div class="col_100 maincontainer "><div class="col_100">
<div class="column">		
		<div class="portlet" id="searchresultparent"  ><div id="searchresult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
		<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
		<span class="ui-icon ui-icon-minusthick"></span><!--<span class="ui-icon ui-icon-close" ></span>--><s:text name="garuda.cbuentry.label.cdrcbusearchresult" />
		</div><div class="portlet-content">
<s:form name="Tableform" id="Tableform">
<s:hidden name="inputs" id="inputs" value="%{inputId}"></s:hidden>	
<s:hidden name="paginationflag" id="paginationflag" ></s:hidden>	
</s:form>

<div id="searchTblediv">
<input type="hidden" name="entries" id="entries" value=<%=paginateSearch.getiShowRows() %> />
<form>
<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="tablelookup1">
		<thead>
			<tr>
			<th id="thSelectColumn">
					<div class="cmDiv">
					<ul class="clickMenu" id="ulSelectColumn">
						<li class="main hover">
							<img src="images/selectcol.png" alt="select columns" title="select columns"/>
								<div style="position: absolute; display: block;" class="outerbox inner">
									<div class="shadowbox1"></div>
									<div class="shadowbox2"></div>
									<div class="shadowbox3"></div>
									<ul class="innerBox" id="targetall"></ul>
								</div>
						</li>
					</ul>
					<div class="clear"></div>
					</div>
				</th>
				<th><s:text name="garuda.cbuentry.label.registrycbuid" /></th>
				<th><s:text name="garuda.cbuentry.label.registrymaternalid" /></th>
				<th><s:text name="garuda.cbuentry.label.localcbuid" /></th>
				<th><s:text name="garuda.cbuentry.label.localmaternalid" /></th>
				<th><s:text name="garuda.cbuentry.label.isbtDin" /></th>
				<th><s:text name="garuda.cbuentry.label.CBB" /></th>
				<th><s:text name="garuda.recipient&tcInfo.label.recipientid" /></th>
				<!--
				<th><s:text name="garuda.cbuentry.label.requestType" /></th>
				<th><s:text name="garuda.cbuentry.label.requestDate" /></th>
				<th><s:text name="garuda.cbuentry.label.EligibilityDetermination" /></th>
				<th><s:text name="garuda.cbuentry.label.status" /></th>
				<th><s:text name="garuda.cbuentry.label.active4patiests" /></th>
				<th><s:text name="garuda.cbuentry.label.idbag" /></th>
				<th><s:text name="garuda.cbuentry.label.externalcbuid" /></th>
				<th><s:text name="garuda.cbuentry.label.cordtncfrozen" /></th>
			 	<th><s:text name="garuda.cbuentry.label.cordcd34cellcount" /></th>
				<th><s:text name="garuda.cbuentry.label.cordtncfrozenwgt" /></th>
				<th><s:text name="garuda.cbuentry.label.cordbabybirthdate" /></th>
				<th><s:text name="garuda.cbuentry.label.cordcbucollecdate" /></th>
				<th><s:text name="garuda.cbuentry.label.cordcreationdate" /></th>
				 -->
				
			</tr>

		</thead>
		<tbody>
			<s:iterator value="lstObject" var="rowVal" status="row">
					<tr id="<s:property	value="%{#row.index}"/>" class="hotspot" onmouseover="return overlib('<strong><s:property value="%{#rowVal[6]}"  /></strong>',CAPTION,'CBB Name');highlight('<s:property	value="%{#row.index}"/>');" onmouseout="return nd();removeHighlight('<s:property	value="%{#row.index}"/>');" onclick="javaScript:loadGroup('<s:property value="%{#rowVal[0]}" />','<s:property value="%{#rowVal[8]}" />','<s:property value="#request.userSiteId"/>','<s:property value="#request.userId"/>');">
						<td>&nbsp;-&nbsp;</td>
						<s:if test="%{#rowVal[1] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="%{#rowVal[1]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[1]!=null}"><s:property value="%{#rowVal[1]}"  /></s:if></td>
						</s:else>
										
						<s:if test="%{#rowVal[2] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="%{#rowVal[2]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[2]!=null}"><s:property value="%{#rowVal[2]}"  /></s:if></td>
						</s:else>
						
						<s:if test="%{#rowVal[3] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="%{#rowVal[3]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[3]!=null}"><s:property value="%{#rowVal[3]}"  /></s:if></td>
						</s:else>
						
						<s:if test="%{#rowVal[4] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="%{#rowVal[4]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[4]!=null}"><s:property value="%{#rowVal[4]}"  /></s:if></td>
						</s:else>
						
						<s:if test="%{#rowVal[5] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="%{#rowVal[5]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[5]!=null}"><s:property value="%{#rowVal[5]}"  /></s:if></td>
						</s:else>
						
						<s:if test="%{#rowVal[6] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="%{#rowVal[6]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[6]!=null}"><s:property value="%{#rowVal[6]}"  /></s:if></td>
						</s:else>
						
						<s:if test="%{#rowVal[7] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="%{#rowVal[7]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[7]!=null}"><s:property value="%{#rowVal[7]}"  /></s:if></td>
						</s:else>
						<!-- 
						<s:if test="%{#rowVal[7] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="%{#rowVal[7]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[7]!=null}"><s:property value="%{#rowVal[7]}"  /></s:if></td>
						</s:else>
						
						<s:if test="%{#rowVal[8] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="%{#rowVal[8]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[8]!=null}"><s:property value="%{#rowVal[8]}"  /></s:if></td>
						</s:else>
						 
						 
						<s:if test="%{#rowVal[8] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="%{#rowVal[8]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[8]!=null}"><s:property value="%{#rowVal[8]}"  /></s:if></td>
						</s:else>
					 	
						<s:if test="%{#rowVal[9] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="%{#rowVal[9]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[9]!=null}"><s:property value="%{#rowVal[9]}"  /></s:if></td>
						</s:else>
						
						<s:if test="%{#rowVal[10] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="%{#rowVal[10]}"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[10]!=null}"><s:property value="%{#rowVal[10]}"  /></s:if></td>
						</s:else>
						
						<s:if test="%{#rowVal[11] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="getText('{0,date,MMM dd,yyyy}',{#rowVal[11]})"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[11]!=null}"><s:property value="getText('collection.date',{#rowVal[11]})"  /></s:if></td>
						</s:else>
						
						<s:if test="%{#rowVal[12] == inputId && inputId!=null && inputId!=''}">
						<td class="highlight_match"><s:property value="getText('collection.date',{#rowVal[12]})"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[12]!=null}"><s:property value="getText('collection.date',{#rowVal[12]})"  /></s:if></td>
						</s:else>
						
						<s:if test="%{#rowVal[13] == inputId  && inputId!=null && inputId!=''} ">
						<td class="highlight_match"><s:property value="getText('collection.date',{#rowVal[13]})"  /></td>		
						</s:if>
						<s:else>
						<td><s:if test="%{#rowVal[13] !=null}"><s:property value="getText('collection.date',{#rowVal[13]})"  /></s:if></td>
						</s:else>  
						
						-->
					</tr>
				</s:iterator>
		</tbody>
		
		<tfoot>
			<tr><td colspan="8"></td></tr>
			
			<tr><td colspan="3">
		    <jsp:include page="paginationFooter.jsp">
		  	<jsp:param value="searchTblediv" name="divName"/>
		  	<jsp:param value="Tableform" name="formName"/>
		  	<jsp:param value="showsRow" name="showsRecordId"/>
		  	<jsp:param value="getMainCBUDetails" name="url"/>
		  	<jsp:param value="inputs" name="cbuid"/>
		  	<jsp:param value="workflow" name="paginateSearch"/>
		  	<jsp:param value="lookupDataTable" name="bodyOnloadFn"/>
		  </jsp:include>
    </td>
    <td colspan="5"></td>
    </tr>
		</tfoot>
		
	</table>
	</form>
	</div> 
</div></div>
   <div id="usersitegrpdropdown" >
			<s:hidden value="#request.grpList.size()" />
	        <s:if test="#request.grpList.size()>1">
				<jsp:include page="cb_userSiteGroup.jsp"></jsp:include>
			</s:if>
</div>
</div>
</div>
<form id="form1" name="form1">
<s:hidden name="cdrCbuPojo.fkCbbId" id="fkCbbId"></s:hidden>
<s:hidden name="#request.userSiteId" id="userSiteId"></s:hidden>
<s:if test="lstObject.size()==1 && (#request.grpList!=null || #request.grpList.size()==1)">
	<div id="recordDiv1">
	<div id="searchresult"><!--  class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
	<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
	<span class="ui-icon ui-icon-minusthick"></span><span class="ui-icon ui-icon-close" ></span>
	Details of <s:text name="garuda.cbuentry.label.cdrcbusearchresult" /> - <s:property value="inputId"/></div><div class="portlet-content">
	-->
	<div id="searchTbles1">
	<jsp:include page="cb_cbuCordInformation.jsp"></jsp:include>
	</div>
	</div></div>
</s:if>
	<div  id="recordDiv" style="display: none; ">
	<div id="searchresult"><!-- class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
	<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
	<span class="ui-icon ui-icon-minusthick"></span><span class="ui-icon ui-icon-close" ></span>
	Details of <s:text name="garuda.cbuentry.label.cdrcbusearchresult"/></div><div class="portlet-content">
	-->
	<div id="searchTbles2">
	<s:hidden name="pkcordId" id="pkcordId" value="%{pkcordId}"></s:hidden>	
	<jsp:include page="cb_cbuCordInformation.jsp"></jsp:include></div>
	</div>
	</div>
</form>
</div>
</div>

