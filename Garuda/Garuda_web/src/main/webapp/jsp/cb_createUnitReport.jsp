<%@ taglib prefix="s" uri="/struts-tags"%>
<% String contextpath=request.getContextPath();%>
<%@ page import="com.velos.ordercomponent.action.VelosBaseAction" %>

<script>
var cotextpath = cotextpath; 

var attid= $j("#attachmentid").val();
var pkcordextinfoid = $j("#pkcordextinfoid").val();
var pkhlaextinfo = $j("#pkhlaextinfo").val();

/* function saveCBUUnitReport(){
	
	//if($j("#CBUUnitRptForm").valid()){ 	
		
		//alert($j("#browseFlag").val());
		if($j("#browseFlag").val()==1){
		//alert("hio");	
		//document.getElementById("frame1").contentWindow.trackFile();
		saveCall();
		}
		else{
			saveCall();
			}
    //}
} */
function saveCBUUnitReport(){
	var iframe = document.getElementById('frame1');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;

		if($j("#CBUUnitRptForm").valid()){
	
		if($j("#browseFlag").val()==1){
	
			if(innerDoc.getElementById('fileUploadStatus').value == 1) {

				document.getElementById('frame1').contentWindow.getFileStatus();
		
				if(innerDoc.getElementById('FileId1').value!=null && innerDoc.getElementById('FileId1').value!=''){

					document.getElementById('frame1').contentWindow.document.attachdocument.submit(); 

					alert("<s:text name="garuda.cbu.fileattach.uploadSuccessAlert"/>");	

	     				saveCall();

	      			  } 
		 		else {
		 			alert('<s:text name="garuda.cbu.openorder.uploadFailed "/>');
				 }
		}
	else{
		alert('<s:text name="garuda.cbu.openorder.uploadFailed "/>');			
	}
	
}
else{
	saveCall();
}
}
}
function saveCall(){
	
	/* if($j('#completedby').val()==null || $j('#completedby').val()==""){
     alert("Please enter Completed By!");
     $j('#completedby').focus();
    }else { */
    	//if($j("#CBUUnitRptForm").valid()){ 	
    		
    		$j.ajax({
    			type : "POST",
    			async : false,
    			url : 'saveCBUUnitReport',
    			data : $j("#CBUUnitRptForm").serialize(),
    			success : function(result) {
    				//showUnitReport("close");
    				
    				$j('#CBUUnitRptForm').html(result);
    			}
			
		});
    //}
}

 $j(function() {
	var today = new Date();
	
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();

	var h=today.getHours();
	var mn=today.getMinutes()+1;


	
	$j( "#datepicker1" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker2" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker3" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker4" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	
	$j("#CBUUnitRptForm").validate({	
		rules:{			
				//"cbuUnitReporttempPojo.cordViabPostProcess" : {lessthanHundred : true },
				//"cbuUnitReportPojo.cordViabPostProcess" : {greaterThanZero : true },
				//"cbuUnitReporttempPojo.cordCfuPostProcessCount" : {lessthanHundred : true },
				//"cbuUnitReportPojo.cordCfuPostProcessCount" : {greaterThanZero : true },
				//"cbuUnitReporttempPojo.cordCbuPostProcessCount" : {lessthanHundred : true },
				//"cbuUnitReportPojo.cordCbuPostProcessCount" : {greaterThanZero : true },
				//"cbuUnitReportPojo.cordCompletedBy":"required",
				  "cbUploadInfoPojo.description":{required:true},
				  "cbUploadInfoPojo.strcompletionDate":{required:true},
				  "frame1":{required:true}
			},
			messages:{
				//"cbuUnitReportPojo.cordViabPostProcess":"Entered Viability Post Processing must be within 0 - 100.0%",
				//"cbuUnitReportPojo.cordCfuPostProcessCount":"Entered Cfu Post Processing must be within 0 - 100.0",
				//"cbuUnitReportPojo.cordCbuPostProcessCount":"Entered Cbu Post Processing must be within 0 - 100.0%",
				//"cbuUnitReportPojo.cordCompletedBy":"Please Enter Completed By",
				"cbUploadInfoPojo.description":"<s:text name="garuda.common.validation.description"/>",
				"cbUploadInfoPojo.strcompletionDate":"<s:text name="garuda.cbu.unitreport.completiondate"/>",
				"frame1":"<s:text name="garuda.common.validation.selectdocument"/>"
			}
		});
});
 /*function show(checkboxName,divName) {
	if(checkboxName.checked == true) {
		document.getElementById(divName).style.display='block';
	} else {
		document.getElementById(divName).style.display='none';
	}
}
function checkBx1(){
	
	var checkBox=document.getElementById("cfuStatusCheckbox");
	
	  if(checkBox.checked==true){
		
	   document.getElementById("isCfu").value=1;
	  }
	   else{
		   document.getElementById("isCfu").value=0;
		   }
	  show(checkBox,"viab2");
}
function checkBx2(){
	
	var checkBox1=document.getElementById("viabStatusCheckbox");
	
	  if(checkBox1.checked==true){
		 
	   document.getElementById("isViab").value=1;
	  }
	   else{
		   document.getElementById("isViab").value=0;
		   }
	  show(checkBox1,"viab");
}
function checkBx3(){
	
	var checkBox2=document.getElementById("cbuStatusCheckbox");
	
	  if(checkBox2.checked==true){
		//  alert(checkBox2.checked);
	   document.getElementById("isCbu").value=1;
	   
	  }
	   else{
		   document.getElementById("isCbu").value=0;
		   }
	  show(checkBox2,"viab3");
}

$j(document).ready(function(){
	
	//$j("#cfuStatusCheckbox").attr("checked",false);
	//$j("#viabStatusCheckbox").attr("checked",false);
//$j("#cbuStatusCheckbox").attr("checked",false);
	checkBx2();
	checkBx1();
	checkBx3();
	}); */
	
/* jQuery.validator.addMethod("lessthanHundred", function(value, element) {
    return this.optional(element) || (parseFloat(value) < 100);
}, "Percentage must be less than Hundred"); */

/* jQuery.validator.addMethod("greaterThanZero", function(value, element) {
    return this.optional(element) || (parseFloat(value) <= 100.00); */
//}, "Percentage must be greater than zero");


/* function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if ((charCode > 31 && (charCode < 46 || charCode > 57)) || charCode==45)
      return false;

   return true;
} */

function showAddWidget(url){
	 url = url + "?pageId=2" ; 
	 showAddWidgetModal(url);
}
</script>




<div class="col_100 maincontainer ">
<div class="col_100">



<s:form 
	id="CBUUnitRptForm" name="CBUUnitRptForm" >
	<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
	<s:hidden name="cdrCbuPojo.registryId" id="registryId"></s:hidden>
	<s:hidden name="attachmentid" id="attachmentid" value="%{attachmentid}"></s:hidden>
	
	<s:hidden name="pkcordextinfoid" id="pkcordextinfoid" value="%{pkcordextinfoid}"></s:hidden>
	<s:hidden name="pkhlaextinfo" id="pkhlaextinfo" value="%{pkhlaextinfo}"></s:hidden>
	<table  border="0" align="center" cellpadding="0" cellspacing="0"
		id="unitreportrequest" class="tabledisplay">
		<thead>
			<tr>
				<th align="center"><s:text
					name="garuda.unitreport.label.heading" /></th>

			</tr>
		</thead>
	</table>

	<div id="fileupload1" class="tabledisplay">
	<table class="tabledisplay" align="center" border="0">
		<tbody>
			<tr>
				<td><s:text name="garuda.unitreport.label.sublabel" /><s:property
					value="cdrCbuPojo.registryId" /></td>
			</tr>
		</tbody>
	</table>
	</div>
	 <div class="portlet" id="cbuinfodiv">
			  <div id="cbuinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			  <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			
			  <s:text name="garuda.unitreport.label.cbuinfo.subheading" /></div>
			  			
	<%-- <table width="100%" border="0" align="center" cellpadding="0"
		cellspacing="2" class="tabledisplay">


		<tr>
			<td><s:text name="garuda.unitreport.label.regunitid" /></td>
			<td><s:property
				  value="cdrCbuPojo.registryId" /></td>

			<td><s:text name="garuda.unitreport.label.idnocbubag" /></td>
			<td><s:property
				 value="cdrCbuPojo.numberOnCbuBag"  /></td>
		</tr>
		<tr>
			<td><s:text name="garuda.unitreport.label.localcbuid" /></td>
			<td><s:property
				 value="cdrCbuPojo.localCbuId"  /></td>

			<td><s:text name="garuda.unitreport.label.collectiondate" /></td>
			<td><s:text name="collection.date">
						<s:param name="value" value="cdrCbuPojo.specimen.specCollDate" />
					</s:text></td>
		</tr>

	</table> --%>
	<table>
	<tr>
	        
			     <td>
				 <s:text name="garuda.openorder.label.description" />
			    </td>
			    <td>
				 <s:textfield name="cbUploadInfoPojo.description" id="description"/>
				 </td>
		        
			       <td>
				    <s:text name="garuda.openorder.label.completiondate" />
			     </td>
			     <td>
				    <s:textfield name="cbUploadInfoPojo.strcompletionDate" id="datepicker2"  />
			    </td>
		        </tr></table>
	</div>
	</div>
	
	
	<%-- <div class="portlet" id="productinfodiv">
			<div id="productinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			
			<s:text name="garuda.unitreport.label.prodinfo.subheading" /></div>
			<div class="portlet-content">
			<div id="productinfo1">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody>
				<tr>
			      <td width="15%"><s:text name="garuda.unitreport.label.cellviability" /></td>
					<td width="10%"><s:textfield name="cbuUnitReportPojo.cordViabPostProcess" size="10" onKeyPress="return isNumberKey(event)" /></td>
					<td width="4%">%</td>
			      <td width="10%"><s:text name="garuda.unitreport.label.method" /></td>
			      <td width="15%">
			      
			       <s:select name="cbuUnitReportPojo.cordViabMethod" list="listViabMethod"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />
			      
			      </td>
			      <td  width="10%"><s:text name="garuda.unitreport.label.testdate" /></td>
			      <td width="10%"><s:textfield name="cbuUnitReportPojo.strcordViabPostTestDate"  id="datepicker4" /></td>
			      
			      <td width="10%"><s:text name="garuda.unitreport.label.or" /></td>
			      <td width="10%"><s:text name="garuda.unitreport.label.notdone" /></td>
			      
			      
			      <td width="6%">
			      <s:checkbox name="cordViabPostStatus" id="viabStatusCheckbox" onclick="checkBx2()" />
					<s:hidden name="cbuUnitReportPojo.cordViabPostStatus" id="isViab"  />
			      </td>
			      </tr>
			</tbody>
		</table>

	<div id="viab" style="display: none">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">

		 <tr>
			      <td width="40%"><s:text name="garuda.unitreport.question.priortest" /></td>
			      <td width="10%"><s:radio name="cbuUnitReportPojo.cordViabPostQues1" list="#{'1':'Yes','0':'No'}" /></td>
			      <td width="40%"><s:text name="garuda.unitreport.question2.priortest2" /></td>
			      <td width="10%"><s:radio name="cbuUnitReportPojo.cordViabPostQues2" list="#{'1':'Yes','0':'No'}" /></td>
			       
			    </tr>
	</table>
	</div>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
			      <td width="15%"><s:text name="garuda.unitreport.label.cfupostprocessingcount"  /></td>
			      <td width="10%"><s:textfield name="cbuUnitReportPojo.cordCfuPostProcessCount" size="10" onKeyPress="return isNumberKey(event)" /></td>
			      <td width="4%"></td>
			      <td width="10%"><s:text name="garuda.unitreport.label.method" /></td>
			      <td width="15%">
			      
			     <s:select name="cbuUnitReportPojo.cordCfuPostMethod" list="listCfuPostMethod"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />
			      </td>
			      <td width="10%"><s:text name="garuda.unitreport.label.testdate" /></td>
			      <td width="10%"><s:textfield name="cbuUnitReportPojo.strcordCfuPostTestDate"  id="datepicker2" /></td>
			     
			     <td width="10%"><s:text name="garuda.unitreport.label.or" /></td>
			      <td width="10%"><s:text name="garuda.unitreport.label.notdone" /></td>
			     
			      <td width="6%"><s:checkbox name="cordCfuPostStatus" id="cfuStatusCheckbox" onclick="checkBx1()" />
					  <s:hidden name="cbuUnitReportPojo.cordCfuPostStatus" id="isCfu" />
					  </td>
				</tr>
		</tbody>
	</table>

	<div id="viab2" style="display: none">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">

		<tr>	  
			      <td width="40%"><s:text name="garuda.unitreport.question.priortest" /></td>
			    
			      <td width="10%"><s:radio name="cbuUnitReportPojo.cordCfuPostQues1" list="#{'1':'Yes','0':'No'}" /></td>
			      
			      <td width="40%"><s:text name="garuda.unitreport.question2.priortest2" /></td>
			       <td width="10%"><s:radio name="cbuUnitReportPojo.cordCfuPostQues2" list="#{'1':'Yes','0':'No'}" /></td>
			        
			    </tr>
	</table>
	</div>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>    
			       <td width="15%"><s:text name="garuda.unitreport.label.cbupostprocessingnrbc" /></td>
			      <td width="10%"><s:textfield name="cbuUnitReportPojo.cordCbuPostProcessCount" size="10" onKeyPress="return isNumberKey(event)" /></td>
			      <td width="4%"></td>
			      
			      <td width="10%"><s:text name="garuda.unitreport.label.testdate" /></td>
			      <td width="15%"><s:textfield name="cbuUnitReportPojo.strcordCbuPostTestDate" id="datepicker3"  /></td>      
			      <td width="10%"><s:text name="garuda.unitreport.label.or" /></td>
			      <td width="10%"><s:text name="garuda.unitreport.label.notdone" /></td>
			      <td width="10%">
			      <s:checkbox name="cordCbuPostStatus" id="cbuStatusCheckbox" onclick="checkBx3()" />
					<s:hidden name="cbuUnitReportPojo.cordCbuPostStatus" id="isCbu"  />
			      
			      </td>
			      <td width="10%"></td>
			      <td width="6%"></td>
			      </tr>
		</tbody>
	</table>

	<div id="viab3" style="display: none">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		 <tr>
			      <td  width="40%"><s:text name="garuda.unitreport.question.priortest" /></td>
			      
			      <td width="10%"><s:radio name="cbuUnitReportPojo.cordCbuPostQues1" list="#{'1':'Yes','0':'No'}" /></td>
			      
			       <td  width="40%"><s:text name="garuda.unitreport.question2.priortest2" /></td>
			       <td  width="10%"><s:radio name="cbuUnitReportPojo.cordCbuPostQues2" list="#{'1':'Yes','0':'No'}" /></td>
			    </tr>
	</table>
	</div>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			        <td width="15%"><s:text name="garuda.unitreport.label.bacterialculture"/></td>
				  <td width="10%"><s:select name="cbuUnitReportPojo.cordBacterialCul" list="listBacterial"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />
			</td><td width="4%"></td>
			      <td width="10%"><s:text name="garuda.unitreport.label.fungalculture"/></td>
				  <td width="15%"><s:select name="cbuUnitReportPojo.cordFungalCul" list="listFungal"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />
			</td>
			      <td width="10%"></td>
			      <td width="10%"></td>
			      <td width="10%"></td>
			      <td width="10%"></td>
			      <td width="6%"></td>
			    </tr>
		<tr>
			<td ><s:text name="garuda.unitreport.label.hemoglobinopathyscr"/></td>
								<td><s:select name="cbuUnitReportPojo.cordHemogScreen" list="listHemoPathyScr"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />

		</tr>


	</table>
	</div>
	</div>
	</div>
	</div> 
	<div class="portlet" id="hlainfodiv">
	<div id="hlainfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
	
	<br/>
		</div>						 
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
					<div class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
				
				<s:text name="garuda.unitreport.label.hlainfo.subheading" /></div>
			<div class="portlet-content">
			<div id="hlainfo1">
					
					
				

					<table width="100%" border="0" cellpadding="0" cellspacing="0">
					    	<tr>
								<td width="75%" ><s:text name="garuda.unitreport.question.hlatype" /></td>
							    <td align="right" width="25%"><s:radio  name="cordHlaExtPojo.cordHlaTwice"  list="#{'1':'Yes','0':'No'}" />
								</td>
							</tr>
							
					        	
					        <tr ><td width="75%"><s:text name="garuda.unitreport.question.hlasamples"/></td>
								 <td align="right" width="25%"><s:radio name="cordHlaExtPojo.cordIndHlaSamples" list="#{'1':'Yes','0':'No'}" />
								</td>
							</tr>
							<tr>
								<tr><td width="75%"><s:text name="garuda.unitreport.question.hlalab"/></td>
								 <td align="right" width="25%"><s:radio name="cordHlaExtPojo.cordHlaSecondLab" list="#{'1':'Yes','0':'No'}" />
								</td>
							</tr>
							<tr>
								<td width="75%"><s:text name="garuda.unitreport.question.hlacontguoussegment"/></td>
								 <td align="right" width="25%"><s:radio name="cordHlaExtPojo.cordHlaContiSeg" list="#{'1':'Yes','0':'No'}" />
								</td>
								
								<tr>
								<td width="75%"><s:text name="garuda.unitreport.question.hlarelease"/></td>
								 <td align="right" width="25%"><s:radio name="cordHlaExtPojo.cordHlaContiSegBefRel" list="#{'1':'Yes','0':'No'}" />
								</td>
								
								
							</tr>		
							<tr>
								<td width="75%"><s:text name="garuda.unitreport.question.hlabeforerelease"/></td>
								 <td align="right" width="25%"><s:radio name="cordHlaExtPojo.cordHlaIndBefRel" list="#{'1':'Yes','0':'No'}" />
								</td>
								
								</tr>
								
								<tr>
								<td width="75%"><s:text name="garuda.unitreport.question.unitrequestedshipment"/></td>
								<td align="right" width="25%"><s:select name="cordHlaExtPojo.CordHlaContiSegNum" list="listNumOfSegments"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />
						</td>
					</tr>
					</table>
					
					<br/><br/><br/><br/><br/><br/>
					</div>
					</div>
					</div>
					
					</td>
				<td>
				
			<div class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
				
				<s:text name="garuda.unitreport.label.processinginfo.subheading" /></div>
			<div class="portlet-content">
			<div id="processinginfo1">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">


				<tr>
								<td><s:text name="garuda.unitreport.label.processingmethod" /></td>
							    <td><s:select name="cbuUnitReportPojo.cordProcMethod" list="listProcessMethod"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" /></td></tr>
						<tr>
							
					        	<td><s:text name="garuda.unitreport.label.typeofbagused"/></td>
								<td><s:select name="cbuUnitReportPojo.cordTypeBag" list="listBagType"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" /></td>
								
							</tr>
						
						<tr>
							
					        	<td><s:text name="garuda.unitreport.label.productmodification"/></td>
								<td>
								<s:select name="cbuUnitReportPojo.cordProdModification" list="listProductModify"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" />
								</td>
								
							</tr>
						
						<tr><td></td>
						<td></td>
						
						</tr>
						<tr><td><s:text name="garuda.unitreport.label.plsexplain" /></td>
						<td><textarea rows="3" cols="15" name="cbuUnitReportPojo.cordExplainNotes"></textarea> </td></tr>
						
						
						<tr>
						<td><s:text name="garuda.unitreport.label.additionalnotes"/></td>
						<td><textarea rows="4" cols="20" name="cbuUnitReportPojo.cordAddNotes"></textarea></td>
						</tr>


			</table>

			</div>
			</div>
			</div></td></tr></table></div>	</div>--%>
						<table>
						<tr>
						
						
						<td>	
						 <div id="fileupload" >
 
 							<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr"><tr><td>
 
							<%-- <input type=text value=<%=request.getParameter("cordID")%> /> --%>


							<s:hidden id="attachmentFlag" name="attachmentFlag" value=""></s:hidden>
							<s:hidden id="browseFlag" name="browseFlag" value="0"></s:hidden></td></tr>
							<tr><td><iframe id="frame1" name="frame1" width="100%"  height="80px" frameborder="0" style="background: #F7F7F7;"
										scrolling="no" src="cb_fileattach.jsp?attCodeType=attach_type&attCodeSubType=test_rprt&codeEntityType=entity_type&codeEntitySubType=CBU">
							</iframe><s:hidden name="timestamp" id="attachId" /></td></tr> </table>
							</div></td></tr>
						</table>
						<%-- <div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="col_100 ">
						<tr><td width="50%"></td>
							<td width="50%"><table><tr>
							<td width="25%"><s:text
								name="garuda.unitreport.label.completedby" /></td>
							<td width="25%"><s:textfield
								name="cbuUnitReportPojo.cordCompletedBy" id="completedby" /></td>
							<td width="25%"><s:text
								name="garuda.unitreport.label.completedstatus" /></td>
							<td width="25%" ><s:select
								name="cbuUnitReportPojo.cordCompletedStatus" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@COMPLETED_STATUS]" 
								listKey="pkCodeId" listValue="description"
								headerKey="" headerValue="Select" /></td>
								</tr></table></td>
						</tr>

					</table>
					</div> --%>
		
		<table>
   <tr bgcolor="#cccccc">
   <td><jsp:include page="cb_esignature.jsp" flush="true"></jsp:include></td>
   <td  align="center" colspan="2" >
           	<input type="button" name="createUnitReport" disabled="disabled" id="submit" value="<s:text name="garuda.unitreport.label.button.submit"/>" onclick="javascript:saveCBUUnitReport();" >
          	<input type="button" onclick="closeModal();" value="<s:text name="garuda.common.lable.cancel"/></input>" />
      </td>  
   </tr>
   </table>							
	<%-- <table width="100%" cellspacing="0" cellpadding="0" class="tabledisplay">
			      
			       <tr align="center">
			          <td>           
			          </td>
			          <td><button type="button" onclick="javascript:saveCBUUnitReport();" ><s:text name="garuda.unitreport.label.button.submit"/></button> 
			            <button type="button" ><s:text name="garuda.unitreport.label.button.export"/></button> 
			            <button type="button" onClick=" alert('If you dont want to submit this report, please close this window.');">
			            <s:text name="garuda.unitreport.label.button.cancel"/></button> 
			          </td>
			       </tr>
			    </table> --%>
	<br />
	<br />


</s:form>
</div>
</div>

