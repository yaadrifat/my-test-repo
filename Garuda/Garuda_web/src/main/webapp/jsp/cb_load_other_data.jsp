<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<div id="otherRecNotecontent" onclick="toggleDiv('otherRecNote')"
											class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
											style="text-align: center;"><span
											class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cdrcbuview.label.button.otherrecordclinicalnotes" /></div>				
			<div id="otherRecNote" style=" width: 463px; height: 100%;">
			<div id="loadOthersClinicalNoteDiv">
				        <s:set name="cbuCateg" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CLINIC_CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_NOTE_CATEGORY_CODESUBTYPE) " />
						<jsp:include page="modal/cb_load_clinicalNote.jsp"></jsp:include>
			</div>
			 <s:if test="hasNewPermission(#request.vClnclNote)==true">
				<s:if test='(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)) '>
					<button type="button" disabled="disabled"><s:text
				name="garuda.cdrcbuview.label.button.addnotes" /></button>
				</s:if>
				<s:else>
				<button type="button"
				onclick="fn_showModalOtherNotes('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','clinicalNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cat=OTHER_NOTE_CATEGORY_CODESUBTYPE','550','850','notesModalsDiv');"><s:text
				name="garuda.cdrcbuview.label.button.addnotes" /></button>
				</s:else>
				 </s:if>
				</div>
				<div id="loadOthrDocInfoDiv">
				<s:if test="(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_REC_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_REC_CODESUBTYPE].size > 0 && hasViewPermission(#request.viewOtherCat)==true)">
				<div id="othersDocContent" onclick="toggleDiv('loadOthrDocInfo')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;">
				<span class="ui-icon ui-icon-triangle-1-s"></span> 
				<s:text name="garuda.uploaddoc.label.button.otherrecorduploaddocument" />
				</div>
					<div id="loadOthrDocInfo">					
					      <s:set name="cbuDocCateg" value="@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_REC_CODESUBTYPE" scope="request" />
					      <div id="loadOthersRevokDoc">
							  <jsp:include page="modal/cb_load_upload_document.jsp">
								  <jsp:param value="<%=request.getAttribute(\"cbuDocCateg\")%>" name="cbuDocCateg"/>
								  <jsp:param value="loadOthersRevokDoc" name="replaceDivId"/>
							  </jsp:include>
						  </div>
                 
                   </div>
                 </s:if>
				</div>
				
				<script>
		 function fn_showModalOtherNotes(title,url,hight,width,id){
			 var patientId = $j('#patientId').val();
				var patient_data = '';
				if(patientId!="" && patientId != null && patientId != "undefined"){
					patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
				}
				title=title+patient_data;
				showModals(title,url,hight,width,id);
			}
		 </script>
				
				