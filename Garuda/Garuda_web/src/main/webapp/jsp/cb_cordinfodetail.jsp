<%@page import="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"%>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<%String contextpath = request.getContextPath()+"/jsp/"; %>
<script>

function showDocument(AttachmentId){
	var cotextpath = "<%=contextpath%>";
	var signed='true';
	var url='getAttachmentFromDcms?docId='+AttachmentId+'&signed='+signed;
	window.open( url);
	//showModal('View File',url,'300','500');
	
}

$j(function() {
	/*$j( "#datepicker" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		changeYear: true});
	$j( "#datepicker1" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		changeYear: true});
	$j( "#datepicker2" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		changeYear: true});*/
	getDatePic();
});

$j(document).ready(function(){
	var staticpanelwidth=$j(".viewcbutoppanel").width();
	var staticpanelheight=$j(".viewcbutoppanel").height();
	var columnwidth=(staticpanelwidth/2);
	var datatablewidth=columnwidth-20;
	if($j('#completeReqInfoFlag').val()=="1"){
		$j('#finalReviewBtn').removeAttr('disabled');
	}
	else{
		$j('#finalReviewBtn').attr('disabled', true);
		}
	$j('.datatable').dataTable({
		"bSort": false,
		"sScrollX": datatablewidth,
        "bDeferRender": true
		});
//$j('#searchResults').dataTable(); 
	$j("#cbuRHistory1").attr('checked',true);
	$j('#cReqhistory').show();
	historyTblOnLoad();

$j('#searchResults1').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
		}
	} );
$j('#searchResults1_length').empty();
$j('#searchResults1_filter').empty();

$j('#searchResults2').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
		}
	} );
$j('#searchResults2_length').empty();
$j('#searchResults2_filter').empty();

$j('#searchResults3').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	}
} );
$j('#searchResults3_length').empty();
$j('#searchResults3_filter').empty();

$j('#searchResults4').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	}
});
$j('#searchResults4_length').empty();
$j('#searchResults4_filter').empty();

$j('#searchResults5').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	}
});
$j('#searchResults5_length').empty();
$j('#searchResults5_filter').empty();

$j('#searchResults6').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	}
});
$j('#searchResults6_length').empty();
$j('#searchResults6_filter').empty();

$j('#searchResults7').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	},
	"bRetrieve": true,
	"bDestroy" :true
});
$j('#searchResults7_length').empty();
$j('#searchResults7_filter').empty();

$j('#searchResults8').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	}
});
$j('#searchResults8_length').empty();
$j('#searchResults8_filter').empty();

$j('#searchResults9').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	}
});
$j('#searchResults9_length').empty();
$j('#searchResults9_filter').empty();
//$j('#searchResults10').dataTable();

$j('#maternalHlaResults').dataTable({
	"oLanguage": {
		"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
	}
});
$j('#maternalHlaResults_length').empty();
$j('#maternalHlaResults_filter').empty();
/*
$j('#idmtestResults').dataTable({
	"oLanguage": {
		"sEmptyTable": "Test Results not available"
	}
});*/

$j('button,input[type=submit],input[type=button]').button();

});
function historyTblOnLoad(){
	
	var staticpanelwidth=$j(".viewcbutoppanel").width();
	var ordertype=$j('#orderType').val();
	if(ordertype=='CT' || ordertype=='HE' || ordertype=='OR'){
		staticpanelwidth=$j(".viewcbutoppanel").width();
		staticpanelwidth=(staticpanelwidth/2);
	}
	var staticpanelheight=$j(".viewcbutoppanel").height();
	var columnwidth=staticpanelwidth-5;
	
	fn_events1('cbuhistorytbl','targetall1','ulSelectColumn1');
	
	
	var hstrytbl1 = $j('#cbuhistorytbl').dataTable({
			"iDisplayLength": 1000,
			"bRetrieve":true,
			"bDestroy":true,
			"sScrollY": staticpanelheight,
			"sScrollX":columnwidth,
			"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] },{"sType": "date", "aTargets": [ 1 ] }],
			"oLanguage": {
				"sSearch": "<s:text name="garuda.common.message.searchCol"/>",
				"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
			}
	});
	
	
	
	$j('#cbuhistorytbl_info').hide();
	$j('#cbuhistorytbl_paginate').hide();
	$j('#cbuhistorytbl_length').empty();
	$j('#cbuhistorytbl_length').replaceWith('Show <select name="showsRow" id="showsRowcbuhistorytbl" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="paginateSearch.getiTotalRows()"/>">All</option></select> Entries');
	if($j('#hentries').val()!=null || $j('#hentries').val()!=""){
		$j('#showsRowcbuhistorytbl').val($j('#hentries').val());
		}
	$j('#showsRowcbuhistorytbl').change(function(){
		paginationFooter(0,"cordinfo,showsRowcbuhistorytbl,temp,cbuCordInformationForm,cbuHistoryForm,historyTblOnLoad,inputs");
	});
	
	
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('position','static');
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('z-index','1000');
	
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').after('<div style="clear:both" class="clearDiv"></div>');

	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHeadInner table').width(columnwidth);
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollFootInner table').width(columnwidth);
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').find('.dataTables_scrollHeadInner table').width(columnwidth);
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollBody table').width(columnwidth);
	
	
	$j('#cbuhistorytbl_wrapper').css('overflow','hidden');
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollBody').scroll(function(){
		$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('overflow','hidden');
	});
	$j('#cbuhistorytbl_wrapper').find('#thSelectColumn div').click(function(){
		$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	});
	
	if(! $j("#cbuRHistory1").is(':checked')){
		$j("#cbuHistory1").attr('checked',true);
		$j('#chistory').show();
	}

	fn_events('cbuReqHistoryTbl','targetall2','ulSelectColumn2');
	$j('.detailhstry').css('max-height',staticpanelheight);
	
	$j('#cbuHistory1').bind('click',function(ui){
		var oTable = $j('div.dataTables_scrollBody> #cbuhistorytbl', ui.panel).dataTable();
		if ( oTable.length > 0 ) {
			oTable.fnAdjustColumnSizing();
		}
	});
	if ( hstrytbl1.length > 0 ) {
		hstrytbl1.fnAdjustColumnSizing();
	}
	
}
function closePopUp(){
	 jQuery(".popupdialog").dialog("destroy");
}
/*
function paginationFooter(pageNo, allParams){
	//String totalParameters = url+","+showsRecordId+","+cbuid+","+formName+","+divName+","+bodyLoadFn;
	alert("inside cordinfor details paginationFooter");
	var totalparams = allParams.split(","); 
	url = totalparams[0]+"?iPageNo="+pageNo+"&iShowRows="+$j('#'+totalparams[1]).val();
	if(totalparams[2]!="" && totalparams[2]!="temp"){
		url=url+"&pkcordId="+$j('#'+totalparams[2]).val();
		alert("inside getting totalparams[2] "+$j('#'+totalparams[2]).val())
	}
	alert(url);
	refreshDiv(url,totalparams[4],totalparams[3]);
	if(totalparams[5]!=""){
		window[totalparams[5]]();
	}
 }
 */
</script>
<s:hidden name="formidflagval"  id="formidflag" value="cordform"/>
<div class="col_100 maincontainer "><div class="col_100">
<jsp:include page="cb_cbuCordInformation.jsp"></jsp:include>
</div>
</div>