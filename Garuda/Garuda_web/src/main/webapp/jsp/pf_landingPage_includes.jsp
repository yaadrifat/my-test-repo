<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.business.util.VelosMidConstants"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants"%>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch2" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<%
String contextpath=request.getContextPath();
%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<%
HttpSession tSession = request.getSession(true);
GrpRightsJB grpRights =null;
String accId = null;
String logUsr = null;
if (tSession.getAttribute("LocalGRights")!=null){
 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
}else
{
 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
}
if (sessionmaint.isValidSession(tSession))
{
    accId = (String) tSession.getValue("accountId");
    logUsr = (String) tSession.getValue("userId");
    request.setAttribute("username",logUsr);
}

String cellValue1="";
if(request.getAttribute("username")!=null && !request.getAttribute("username").equals("")){
	cellValue1 = request.getAttribute("username").toString();
}

int viewAlertNotiWidget = 0;
int viewWorkflowNotiWidget=0;
int viewOrdersWidget=0;
int viewCBBSummaryWidget=0;
int viewCordShippedWidget=0;

if(grpRights.getFtrRightsByValue("CB_ALERTNOTE")!=null && !grpRights.getFtrRightsByValue("CB_ALERTNOTE").equals(""))
{viewAlertNotiWidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ALERTNOTE"));}
else
	viewAlertNotiWidget = 4;
if(grpRights.getFtrRightsByValue("CB_WORKNOTE")!=null && !grpRights.getFtrRightsByValue("CB_WORKNOTE").equals(""))
{viewWorkflowNotiWidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_WORKNOTE"));}
else
	viewWorkflowNotiWidget = 4;
if(grpRights.getFtrRightsByValue("CB_ORDERS")!=null && !grpRights.getFtrRightsByValue("CB_ORDERS").equals(""))
{viewOrdersWidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ORDERS"));}
else
	viewOrdersWidget = 4;
if(grpRights.getFtrRightsByValue("CB_CBBSUM")!=null && !grpRights.getFtrRightsByValue("CB_CBBSUM").equals(""))
{viewCBBSummaryWidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBBSUM"));}
else
	viewCBBSummaryWidget = 4;
if(grpRights.getFtrRightsByValue("CB_CORDSHIP")!=null && !grpRights.getFtrRightsByValue("CB_CORDSHIP").equals(""))
{viewCordShippedWidget = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CORDSHIP"));}
else
	viewCordShippedWidget = 4;
request.setAttribute("viewAlertNotiWidget",viewAlertNotiWidget);
request.setAttribute("viewWorkflowNotiWidget",viewWorkflowNotiWidget);
request.setAttribute("viewOrdersWidget",viewOrdersWidget);
request.setAttribute("viewCBBSummaryWidget",viewCBBSummaryWidget);
request.setAttribute("viewCordShippedWidget",viewCordShippedWidget);
%>
<script type="text/javascript" >
$j(function(){
	getDatePic();
	jQuery(function() {
		  jQuery('#dateEnd, #dateStart').datepicker('option', {
		    beforeShow: customRange
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});
	jQuery(function() {
		  jQuery('#dateEnd1, #dateStart1').datepicker('option', {
		    beforeShow: customRange1
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange1(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});
	jQuery(function() {
		  jQuery('#dateEnd2, #dateStart2').datepicker('option', {
		    beforeShow: customRange2
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange2(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});
	jQuery(function() {
		  jQuery('#dateEnd3, #dateStart3').datepicker('option', {
		    beforeShow: customRange3
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange3(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});
	
	$j('#Searchbox').focus(function() {
		if($j(this).val()=='Enter Any CBB'){
	    	$j(this).val('');
		}
	});
	$j('#Searchbox').blur(function() {
	   if($j(this).val()==''){
	   		$j(this).val('Enter Any CBB');
	   }
	});
	
});

function ShowEntries_Sort_Data(){
	
	var sv_ord_se='<%=session.getAttribute("v_Ord_SE")%>';
	var sv_wrkflw_se='<%=session.getAttribute("v_wrkflw_SE")%>';
	var sv_alert_se='<%=session.getAttribute("v_alert_SE")%>';
	var sv_ord_se_txt='<%=session.getAttribute("v_Ord_SE_txt")%>';
	var sv_wrkflw_se_txt='<%=session.getAttribute("v_wrkflw_SE_txt")%>';
	var sv_alert_se_txt='<%=session.getAttribute("v_alert_SE_txt")%>';
	
	if(sv_ord_se!='null'  && sv_ord_se!=undefined && sv_ord_se!='' && sv_ord_se!='undefined' && sv_ord_se!='0'){
	    $j('#LP_OrdEntries').val(sv_ord_se);
	}else{
		$j('#LP_OrdEntries').val('10');
	}
	//alert("sv_wrkflw_se::::"+sv_wrkflw_se+":::")
	if(sv_wrkflw_se!='null' && sv_wrkflw_se!=undefined && sv_wrkflw_se!='' && sv_wrkflw_se!='undefined' && sv_wrkflw_se!='0'){
	    $j('#LP_WflwEntries').val(sv_wrkflw_se);
	}else{
		$j('#LP_WflwEntries').val('10');
	}
	//alert("sv_alert_se::::"+sv_alert_se+":::")
	if(sv_alert_se!='null'  && sv_alert_se!=undefined && sv_alert_se!='' && sv_alert_se!='undefined' && sv_alert_se!='0'){
	    $j('#LP_TaskEntries').val(sv_alert_se);
	}else{
		  $j('#LP_TaskEntries').val('10');
	}
	
	if(sv_ord_se_txt!='null'  && sv_ord_se_txt!=undefined && sv_ord_se_txt!='' && sv_ord_se_txt!='undefined' && sv_ord_se_txt!='0'){
	    $j('#LP_OrdEntries_txt').val(sv_ord_se_txt);
	}else{
		$j('#LP_OrdEntries_txt').val('10');
	}
	//alert("sv_wrkflw_se::::"+sv_wrkflw_se+":::")
	if(sv_wrkflw_se_txt!='null' && sv_wrkflw_se_txt!=undefined && sv_wrkflw_se_txt!='' && sv_wrkflw_se_txt!='undefined' && sv_wrkflw_se_txt!='0'){
	    $j('#LP_WflwEntries_txt').val(sv_wrkflw_se_txt);
	}else{
		$j('#LP_WflwEntries_txt').val('10');
	}
	//alert("sv_alert_se::::"+sv_alert_se+":::")
	if(sv_alert_se_txt!='null'  && sv_alert_se_txt!=undefined && sv_alert_se_txt!='' && sv_alert_se_txt!='undefined' && sv_alert_se_txt!='0'){
	    $j('#LP_TaskEntries_txt').val(sv_alert_se_txt);
	}else{
		  $j('#LP_TaskEntries_txt').val('10');
	}
	
	var sv_ord_sc='<%=session.getAttribute("v_Ord_SORT_COL")%>';
	var sv_wrkflw_sc='<%=session.getAttribute("v_WF_SORT_COL")%>';
	var sv_alert_sc='<%=session.getAttribute("v_ALT_SORT_COL")%>';
	var sv_ord_sc_typ='<%=session.getAttribute("v_Ord_SORT_TYPE")%>';
	var sv_wrkflw_sc_typ='<%=session.getAttribute("v_WF_SORT_TYPE")%>';
	var sv_alert_sc_typ='<%=session.getAttribute("v_ALT_SORT_TYPE")%>';
	
	if(sv_ord_sc!='null'  && sv_ord_sc!=undefined && sv_ord_sc!='' && sv_ord_sc!='undefined' && sv_ord_sc!='0'){
	    $j('#ord_sortParam').val(sv_ord_sc);
	    $j('#ord_sort_type').val(sv_ord_sc_typ);
	}
	if(sv_wrkflw_sc!='null'  && sv_wrkflw_sc!=undefined && sv_wrkflw_sc!='' && sv_wrkflw_sc!='undefined' && sv_wrkflw_sc!='0'){
		 $j('#wrkflow_sortParam').val(sv_wrkflw_sc);
		 $j('#wrkflw_sort_type').val(sv_wrkflw_sc_typ);
	}
	if(sv_alert_sc!='null'  && sv_alert_sc!=undefined && sv_alert_sc!='' && sv_alert_sc!='undefined' && sv_alert_sc!='0'){
		$j('#alert_sortParam').val(sv_alert_sc);
		$j('#alert_sort_type').val(sv_alert_sc_typ);
	}
}
function showContent(el,DivId,flag){

	if(!$j(el).hasClass('firstLoad')){
			
			var sv_wrkflw_se='<%=session.getAttribute("v_wrkflw_SE")%>';
			var sv_alert_se='<%=session.getAttribute("v_alert_SE")%>';
		
			var cbb=$j('#cbbdrpdwn').val();
			$j('#siteIdval').val(cbb);
			
			if(cbb!=null && cbb!=''){
				$j('#siteIdval').val(cbb);
				if(flag=='flag_cbbsummary'){
					mainOnLoad();
				}
				if(flag=='flag_shipment'){
					showShipmentData();
				}
				if(flag=='flag_orders'){
					var sv_ord_fval='<%=session.getAttribute("v_Ord_fval")%>';
					var param1="";
					var is_all_entries=$j('#LP_OrdEntries_txt').val();
					if(sv_ord_fval!='null' && sv_ord_fval!=''){
						$j('#ordStatFilterFlag').val('1');
						param1=sv_ord_fval.replace(/----/gi,'&');
						param1='&pksiteId='+$j('#siteIdval').val()+param1;
						$j('#ordFilterFlag').val('1');
					}else{
						param1='&pksiteId='+$j('#siteIdval').val();
						if($j('#orderStatus1').val()!=null && $j('#orderStatus1').val()!=""){
							$j('#ordStatFilterFlag').val('1');
							$j('#ordFilterFlag').val('1');
							$j('#resetOrdFilt').val('0');
							param1=param1+'&resetflag='+$j('#resetOrdFilt').val()+'&reqType=&sampleAtLab=&ordstatus='+$j('#orderStatus1').val()+'&assignto=&dateStart=&dateEnd=&RegId1=';
						}
					}
					$j('#ordParams').val(param1);
					//loaddiv('showLPOrders?pksiteId='+$j('#siteIdval').val()+'&iShowRows='+$j('#LP_OrdEntries').val()+param1+'&allEntries='+is_all_entries,'orderdiv');
					$j('#ord_allEntries_flag').val(is_all_entries);
					forOrdersBodyOnloads();
				}
				if(flag=='flag_workflow'){
					var sv_wrkflw_fval='<%=session.getAttribute("v_wrkflw_fval")%>';
					var param2="";
					//alert("sv_wrkflw_se"+sv_wrkflw_se)
					if(sv_wrkflw_se!='null' && sv_wrkflw_se!=undefined && sv_wrkflw_se!='' && sv_wrkflw_se!='undefined'){
					    $j('#LP_WflwEntries').val(sv_wrkflw_se);
					}else{
						$j('#LP_WflwEntries').val('10');
					}
					if(sv_wrkflw_fval!='null' && sv_wrkflw_fval!=''){
						$j('#wrkflowStatFilterFlag').val('1');
						param2=sv_wrkflw_fval.replace(/----/gi,'&');
						$j('#workflowFilterFlag').val('1');
						param2='&pksiteId='+$j('#siteIdval').val()+param2;
						$j('#workflowParam').val(param2);
					}
					//alert("param2"+param2+"showen"+$j('#LP_WflwEntries').val());
					var is_all_entries=$j('#LP_WflwEntries_txt').val();
					loaddiv('showLPWorkflow?pksiteId='+$j('#siteIdval').val()+'&iShowRows='+$j('#LP_WflwEntries').val()+param2+'&allEntries='+is_all_entries,'workflowdiv');
					$j('#wrkflw_allEntries_flag').val(is_all_entries);
					forWorkFlowBodyOnloads();
				}
				if(flag=='flag_alerts'){
					var sv_alert_fval='<%=session.getAttribute("v_alert_fval")%>';
					var param3="";
					if(sv_alert_se!='null'  && sv_alert_se!=undefined && sv_alert_se!='' && sv_alert_se!='undefined'){
					    $j('#LP_TaskEntries').val(sv_alert_se);
					}else{
						  $j('#LP_TaskEntries').val('10');
					}
					if(sv_alert_fval!='null' && sv_alert_fval!=''){
						$j('#alertStatFilterFlag').val('1');
						param3=sv_alert_fval.replace(/----/gi,'&');
						$j('#alertFilterFlag').val('1');
						param3='&pksiteId='+$j('#siteIdval').val()+param3;
						$j('#alertParam').val(param3);
						
					}
					var is_all_entries=$j('#LP_TaskEntries_txt').val();
					$j('#alrt_allEntries_flag').val(is_all_entries);
					//alert("param3"+param3+"showen"+$j('#LP_TaskEntries').val());
					loaddiv('showLPAlerts?pksiteId='+$j('#siteIdval').val()+'&iShowRows='+$j('#LP_TaskEntries').val()+param3+'&allEntries='+$j('#LP_TaskEntries_txt').val(),'tasksdiv');
					forAlertsOnloads();
				}
				$j(el).addClass('firstLoad');
				
			}
		
	}
	
	
}

function showShipmentData(){
	
	$j.ajax({
        type: "POST",
        url : 'getLPShipmentInfo?pksiteId='+$j('#cbbdrpdwn').val(),
        async:false,
        dataType: 'json',
        success: function (result){
        	$j('.shipmentCalen').attr('display','block');
        	
        	if(result.shedShipDtLst!=null){	
        		var resulobj=result.shedShipDtLst;
				//alert("inside message true"+resulobj);
				var count1=0;
				var count2=0;
				var count3=0;
				var count4=0;
				var shipDtArry=[];
				var cordRegIdArry=[];
				var pkOrdArry=[];
				var pkCordArry=[];
				
				$j(resulobj).each(function(i,v){
					var tempdt=v[0];
					shipDtArry[count1++]=tempdt;
					var tempReg=v[1];
					cordRegIdArry[count2++]=tempReg;
					var tempord=v[2];
					pkOrdArry[count3++]=tempord;
					var tempcordId=v[3];
					pkCordArry[count4++]=tempcordId;
				    //alert("elem:"+count);
				});
				
				$j('#shipDtArry').val(shipDtArry);
				$j('#shipRegIdArry').val(cordRegIdArry);
				$j('#shippkCordArry').val(pkCordArry);
				$j('#shippkOrdArry').val(pkOrdArry);
            
       		 }
        }
 	});

	
}
function resetValidForm(formId){
	$j("#"+formId).validate().resetForm();
}
function showDateDiv(el,id,id1,id2){
	var t=$j(el);
	var obj=t.offset();
	var xOffset=180;
	var yOffset=30;
	var val1="";
	var val2="";
	
	if(id=='dateDiv'){
		val1=$j('#lpdateStartord').val();
		val2=$j('#lpdateEndord').val();
	}
	if(id=='dateDiv1'){
		val1=$j('#lpdateStartwork').val();
		val2=$j('#lpdateEndwork').val();
	}
	if(id=='dateDiv2'){
		val1=$j('#lpdateStartalrt').val();
		val2=$j('#lpdateEndalrt').val();
	}
	if(id=='dateDiv3'){
		val1=$j('#lpdateStartalrtStr').val();
		val2=$j('#lpdateEndalrtStr').val();
	}
	
	//alert("----"+val1+"---"+val2);
	$j('#'+id1).val(val1);
	$j('#'+id2).val(val2);
	
	if(navigator.appName=="Microsoft Internet Explorer"){
			
		    yOffset=yOffset+15;
			xOffset=xOffset+50;
			if(id=="dateDiv2"){
					xOffset=xOffset+70;
			}
			if(id=="dateDiv3"){
				xOffset=xOffset+250;
			}
			
	}
	if(navigator.appName!="Microsoft Internet Explorer"){
		
	    	yOffset=yOffset+15;
			xOffset=xOffset+26;
			if(id=="dateDiv2"){
				xOffset=xOffset+70;
			}
			if(id=="dateDiv3"){
				xOffset=xOffset+250;
			}
		
	}
	$j("#"+id)
			.css({left:obj.left-xOffset,top:obj.top+yOffset})
			.fadeIn("fast").show()
			.addClass('dateDivClass')
	$j("#"+id).show();
	if(val1==null || val1==""){
		$j('#'+id1).focus();
	}
	
		
}
function parseDateValue(rawDate) {
	
	var year= rawDate.substring(7,12);
	var month= rawDate.substring(0,3);
	var dt= rawDate.substring(4,6);
	var parsedDate=dt+"-"+month+"-"+$j.trim(year);
	//alert("parsedDate"+parsedDate);
	return $j.trim(parsedDate);
}
function lastFunc(){
	
	  var ordersShowEntries=$j("#showsRoworder").val();
	  var workflowShowEntries=$j("#showsRowNotifications").val();
	  var tasksShowEntries=$j("#showsRowtasks").val();
	  var tbl1_site_filter=$j('#ordParams').val();
	  var tbl2_site_filter=$j('#workflowParam').val();
	  var tbl3_site_filter=$j('#alertParam').val();
	  var lastSelectCBB=$j('#cbbdrpdwn').val();
	  var ordersShowEntriestxt=$j("#showsRoworder :selected").text();
	  var workflowShowEntriestxt=$j("#showsRowNotifications :selected").text();
	  var tasksShowEntriestxt=$j("#showsRowtasks :selected").text();
	  var param1="";
	  var param2="";
	  var param3="";
	  var str="";
	  var str2="";
	  var str3="";
	  var ord_sort_col=$j('#ord_sortParam').val();
	  var wrkflow_sort_col=$j('#wrkflow_sortParam').val();
	  var alert_sort_col=$j('#alert_sortParam').val();
	  var ord_sort_type=$j('#ord_sort_type').val();
	  var wrkflow_sort_type=$j('#wrkflw_sort_type').val();
	  var alert_sort_type=$j('#alert_sort_type').val();
	  
	  //alert("tbl1_site_filter"+tbl1_site_filter+"tbl2_site_filter"+tbl2_site_filter+"tbl3_site_filter"+tbl3_site_filter)
	
	  if(tbl1_site_filter!='undefined' && tbl1_site_filter!=''){
		  
		 str=tbl1_site_filter.indexOf("&reqType");
		  
		   if(str!=-1){
			  var len=tbl1_site_filter.length;
			  var removedStr1=tbl1_site_filter.substring(str,len);
			  param1=removedStr1.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
			  if(param1==""){
				  param1=tbl1_site_filter.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
			  }
		   } 
		  
	  }
	 
	  if(tbl2_site_filter!='undefined' && tbl2_site_filter!=''){
		
		  str2=tbl2_site_filter.indexOf("&reqType");
		  
		  if(str2!=-1){
			  var len2=tbl2_site_filter.length;
			  var removedStr2=tbl2_site_filter.substring(str2,len2);
			  param2=removedStr2.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
			  if(param2==""){
				  param2=tbl2_site_filter.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
			  }
		  }
	  }
	  
	  
	  if(tbl3_site_filter!='undefined' && tbl3_site_filter!=''){
			
		  str3=tbl3_site_filter.indexOf("&reqType");
		  if(str3!=-1){
			  var len3=tbl3_site_filter.length;
			  var removedStr3=tbl3_site_filter.substring(str3,len3);
			  param3=removedStr3.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
			  if(param3==""){
				  param3=tbl3_site_filter.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
			  }
		  }
	  }
	  //alert("For orders TBL::::::::CBB:"+lastSelectCBB+"activeShowEntries:::::"+ordersShowEntries+"removedStr1:::"+param1);
	  //alert("For NACBU TBL::::::::CBB:"+lastSelectCBB+"naShowEntries:::::"+workflowShowEntries+"removedStr1:::"+param2);
	  //alert("For SIP TBL::::::::CBB:"+lastSelectCBB+"sipShowEntries:::::"+tasksShowEntries+"removedStr1:::"+param3);
	  
	  
	  var urltemp='setSessionDataLP?Ord_SE='+ordersShowEntries+'&wrkflw_SE='+workflowShowEntries+'&alert_SE='+tasksShowEntries+'&Ord_fval='+param1+'&wrkflw_fval='+param2+'&alert_fval='+param3+'&last_cbb1='+lastSelectCBB+'&Ord_SE_txt='+ordersShowEntriestxt+'&wrkflw_SE_txt='+workflowShowEntriestxt+'&alert_SE_txt='+tasksShowEntriestxt+'&ord_srt_col='+ord_sort_col+'&ord_srt_type='+ord_sort_type+'&workflow_srt_col='+wrkflow_sort_col+'&workflow_srt_type='+wrkflow_sort_type+'&alert_srt_col='+alert_sort_col+'&alert_srt_type='+alert_sort_type;
	  
	  //alert("urlTemp"+urltemp);
      setValueInSession(urltemp);
	  
}
function getSessionDataForOrders(){
	  var ordersShowEntries=$j("#showsRoworder").val();
	  var tbl1_site_filter=$j('#ordParams').val();
	  var param1="";
	  var str="";
	  var lastSelectCBB=$j('#cbbdrpdwn').val();
	  var ordersShowEntriestxt=$j("#showsRoworder :selected").text();
	  if(tbl1_site_filter!='undefined' && tbl1_site_filter!=''){
		  
			 str=tbl1_site_filter.indexOf("&reqType");
			  
			   if(str!=-1){
				  var len=tbl1_site_filter.length;
				  var removedStr1=tbl1_site_filter.substring(str,len);
				  param1=removedStr1.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
				  if(param1==""){
					  param1=tbl1_site_filter.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
				  }
			   } 
			  
      }
	  var ordSessionParam='&Ord_SE='+ordersShowEntries+'&Ord_fval='+param1+'&last_cbb1='+lastSelectCBB+'&Ord_SE_txt='+ordersShowEntriestxt;
	  return ordSessionParam;
		 
}
function getCEvents(){
	
	var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var shipDtArry=[];
    var cordRegIdArry=[];
    var pkOrdArry=[];
    var pkCordArry=[];
	var rowcount=0;
	var val1=0;
	var val2=0;
	var val3=0;
	var val4=0;
	
	var tempshipDtArry=$j('#shipDtArry').val();
	var tempcordRegIdArry=$j('#shipRegIdArry').val();
	var temppkCordArry=$j('#shippkCordArry').val();
	var temppkOrdArry=$j('#shippkOrdArry').val();
	
	
	shipDtArry=tempshipDtArry.split(",");
	cordRegIdArry=tempcordRegIdArry.split(",");
	pkCordArry=temppkCordArry.split(",");
	pkOrdArry=temppkOrdArry.split(",");

	var tregid=""; 
	var tshipdt="";
	var tpkcord="";
	var count=1;
	var ncount=0;
	var cordRegIdArry1=[];
	var pkCordArry1=[];
	for(var k=0;k<shipDtArry.length;k++){
		count=1;
		tregid="";
		tpkcord="";
		for(var l=k+1;l<shipDtArry.length;l++){
			//alert("iteration"+k+" comparing shipDtArry["+k+"] "+shipDtArry[k]+" shipDtArry["+l+"] "+shipDtArry[l]);
			if((shipDtArry[k]==shipDtArry[l]) && shipDtArry[k]!='0'){
				shipDtArry[l]='0';
				//alert("count"+count);
				if(count==1){
					tregid=cordRegIdArry[k]+",&nbsp;&nbsp;"+cordRegIdArry[l];
					tpkcord=pkOrdArry[k]+","+pkOrdArry[l];
				}
				else{
					tregid=tregid+",&nbsp;&nbsp;"+cordRegIdArry[l];
					tpkcord=tpkcord+","+pkOrdArry[l];
				}
				count++;
			}
		}
		//alert(shipDtArry[k]+"shipDtArry["+k+"]");
		if(tregid!=""){	cordRegIdArry1[k]=tregid;pkCordArry1[k]=tpkcord;}
		else if(shipDtArry[k]!='0'){cordRegIdArry1[k]=cordRegIdArry[k];pkCordArry1[k]=pkCordArry[k];}
		else if(shipDtArry[k]=='0'){cordRegIdArry1[k]='0';pkCordArry1[k]='0';}
	}
	/*for(var k=0;k<shipDtArry.length;k++){
		alert("shipDtArry["+k+"]: "+shipDtArry[k]+"cordRegIdArry["+k+"]:"+cordRegIdArry1[k]+"pkCordArry["+k+"]:"+pkCordArry1[k]);
	}*/
	
	
    $j('#calendar').fullCalendar({
        events: function(start, end, callback) {
               
               var events=[];
               for(var k=0;k<cordRegIdArry1.length;k++){
            	   events.push({
                       title:'',
                       start: new Date(shipDtArry[k]),
                       url:'shipmentDate?shipDt='+shipDtArry[k]+'&cordRegId='+cordRegIdArry1[k]+'&pkcordId='+pkCordArry1[k]+'&pksiteId='+$j('#cbbdrpdwn').val(),
                       description:cordRegIdArry[k],
            	   	   tooltip:'<p id="tooltip">'+cordRegIdArry1[k]+'</p>'	
                   });
               } 
               callback(events);
                
        },
        eventClick: function(event) {
            if (event.url) {
            	showModal('Shipment',event.url,'200','500');
                return false;
            }
        },
        eventMouseover: function(calEvent,jsEvent) {
        	xOffset = 20;
			yOffset = 17;
			var t=$j(this);
			var obj=t.offset();
			$j("#helpmsg").html('<div id="arrowDiv" style="display:inline-block;"></div><div class="fc-transparent" style="display:inline-block;">'+calEvent.tooltip+'</div>');
				$j("#helpmsg")
				.css({left:obj.left+xOffset,top:obj.top-yOffset})
				.fadeIn("fast").show()
				.css('z-index','9999');						
		},
		eventMouseout: function(calEvent,jsEvent) {
			$j("#helpmsg").hide();	
		},
		eventColor: 'red'
		
         
    }); 
        
}
function manageLegandHeight(){

	var scrhght=screen.height;
	var forDiv1=scrhght*0.30;
	maindivwidth=$j('.maincontainer').width();
	var orderwrapper=maindivwidth*0.8;
    orderwrapper=maindivwidth*0.2;
}

function mainOnLoad(){
	//loaddiv('showLPCBBSummary?pksiteId='+$j('#cbbdrpdwn').val(),'cbbprofiletbl');
	var o1Table = $j('#cbbprofile').dataTable({
							"oLanguage": {
								"sSearch": "<s:text name="garuda.common.message.searchCol"/>",
								"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
							},
							"bSort": false,
							"bRetrieve": true,
							"bDestroy" :true,
							"iDisplayLength":5
					});
	$j('#cbbprofile_length').hide();
	$j('#cbbprofile_filter').hide();
	$j('#cbbprofile_info').hide();
	$j('#cbbprofile_paginate').hide();
	
}
	 function constructTable() {
		
			}
	function showDocument(attachmentId){	
			var url = cotextpath+"/viewdocument1.action?attachmentId="+attachmentId;
		window.open( url);

	}

	function showDiv(cordId){

		$j('#recordDiv1').css('display','none');
		$j('#recordDiv').css('display','block');
		refreshDiv("getMain2CbuDetails?cordId="+cordId,"searchTbles1","form1");
	}	
	function getOrderParamval(){

		if($j('#ordParams').val()==null || $j('#ordParams').val()=="" || $j('#ordParams').val()==undefined){
			var is_all_entries=$j('#showsRoworder :selected').text();
			var temp=getSessionDataForOrders();
			ordParams='&pksiteId='+$j('#cbbdrpdwn').val()+'&resetflag='+$j('#resetOrdFilt').val()+'&last_cbb1'+$j('#cbbdrpdwn').val()+'&Ord_SE_txt='+is_all_entries+temp;
			$j('#ordParams').val(ordParams);
		}
	}

	function fnResetFilterOrders(){
		
		var reqType="";
		var startDate="";
		var startEnd="";
		var assignTo="";
		var ordStatus="";
		var sampleAtlab="";
		var regId="";
		var dontflag=0;
		var ordParams="";
		$j('#ordStatFilterFlag').val('0');
		reqType=$j('#orderType1').val();
		assignTo=$j('#AssignTo1').val();
		ordStatus=$j('#orderStatus1').val();
		regId=$j('#lpRegId1').val();
		resetValidForm('dummyform');
	    if($j('#dateStart').val()!="" && $j('#dateEnd').val()!=""){
			startDate=parseDateValue($j('#dateStart').val());
			startEnd=parseDateValue($j('#dateEnd').val());
		}
	    
	    var previousFilterVal=$j('#ordFilterFlag').val();
		
		if(previousFilterVal=='1'){
			var is_all_entries=$j('#showsRoworder :selected').text();
			var temp=getSessionDataForOrders();
			var ordParams='&pksiteId='+$j('#cbbdrpdwn').val();
			$j('#ordParams').val(ordParams);
			fnclear('dateStart','dateEnd');
			$j('#orderType1').val('');
			$j('#orderStatus1').val('');
			$j('#AssignTo1').val('');
			$j('#ordFilterFlag').val('');
			$j('#lpRegId1').val('');
			loaddiv('showLPOrders?iShowRows='+$j('#showsRoworder').val()+'&resetflag=1'+$j('#ordParams').val()+'&allEntries='+is_all_entries+temp,'orderdiv');
			$j('#ord_allEntries_flag').val(is_all_entries);
			$j('#resetOrdFilt').val('1');
			$j('#ord_filter_value').val("");
			forOrdersBodyOnloads();
			$j('#ordFilterFlag').val('');
		}
		fnclear('dateStart','dateEnd');
		$j('#orderType1').val('');
		$j('#orderStatus1').val('');
		$j('#AssignTo1').val('');
		$j('#ord_filter_value').val("");
		$j('#ordFilterFlag').val('');
		$j('#lpRegId1').val('');
		
		
	}
	function fn_FilterOrders(){
		
		var reqType="";
		var startDate="";
		var startEnd="";
		var assignTo="";
		var ordStatus="";
		var sampleAtlab="";
		var dontflag=0;
		var ordParams="";
		var regId="";
		var ordSortParam="";
		var ordParamTemp="";
		$j('#ordStatFilterFlag').val('1');
		reqType=$j('#orderType1 :selected').text();
		assignTo=$j('#AssignTo1').val();
		ordStatus=$j('#orderStatus1').val();
		regId=$j.trim($j('#lpRegId1').val());
		ordSortParam=$j('#ord_sortParam').val();
		
		if($j('#dateStart').val()!="" && $j('#dateEnd').val()!=""){
			var dateElements = $j('.datepic').get();
			  $j.each(dateElements, function(){
			customRange(this);
			  });
			if($j('#dummyform').valid()){
				startDate=parseDateValue($j('#dateStart').val());
				startEnd=parseDateValue($j('#dateEnd').val());
			}else{
				$j('#dateDiv').show();
				dontflag=1;
			}
			
		}else if( ($j.trim($j('#dateStart').val())!="" && $j.trim($j('#dateEnd').val())=="" ) || ($j.trim($j('#dateStart').val())=="" && $j.trim($j('#dateEnd').val())!="")){
			
			alert("Please enter correct Date Range");
			dontflag=1;
			   
		}
		if(reqType=='<%=VelosMidConstants.CT_LAB_ORDER%>'){ 
			sampleAtlab='Y';
		}
		if(reqType=='<%=VelosMidConstants.CT_SHIP_ORDER%>'){ 
			sampleAtlab='N';
		}
		if(reqType!='ALL'){ 
			reqType=$j('#orderType1').val();
		}
		var is_all_entries=$j('#showsRoworder :selected').text();
		
		
		if((regId!=null && regId!="")||(reqType!=null && reqType!="") || (startDate!=null && startDate!="" && startEnd!=null && startEnd!="") || (assignTo!=null && assignTo!="") || (ordStatus!='' && ordStatus!=null)){
			ordParams='&pksiteId='+$j('#cbbdrpdwn').val()+'&reqType='+reqType+'&sampleAtLab='+sampleAtlab+'&ordstatus='+ordStatus+'&assignto='+assignTo+'&dateStart='+startDate+'&dateEnd='+startEnd+'&RegId1='+regId;
		}
		ordParamTemp=ordParams;
	
		
		if(dontflag==0 && ordParams!=""){
			//console.log("Inside IF in fn_FilterOrders");
			$j('#ordParams').val(ordParams);
			var temp=getSessionDataForOrders();
			loaddiv('showLPOrders?iShowRows='+$j('#showsRoworder').val()+$j('#ordParams').val()+'&allEntries='+is_all_entries+temp,'orderdiv');
			$j('#ordParams').val(ordParams);
			$j('#ordFilterFlag').val('1');
			$j('#resetOrdFilt').val('0');
			$j('#ord_allEntries_flag').val(is_all_entries);
			forOrdersBodyOnloads();
		}
		
		
	}
	function fn_FilterAlerts(){
		
		var reqType="";
		var startDate="";
		var startEnd="";
		var assignTo="";
		var ordStatus="";
		var sampleAtlab="";
		var dontflag=0;
		var alertsparams="";
		var regId3="";
		var altstartDate="";
		var altstartEnd="";
		resetValidForm('dummyform');
		$j('#alertStatFilterFlag').val('1');
		reqType=$j('#orderType3 :selected').text();
		ordStatus=$j('#orderStatus3').val();
		regId3=$j.trim($j('#lpRegId3').val());
		
		if($j('#dateStart2').val()!="" && $j('#dateEnd2').val()!=""){
			var dateElements = $j('.datepic').get();
			  $j.each(dateElements, function(){
			customRange2(this);
			  });
			if($j('#dummyform').valid()){
				startDate=parseDateValue($j('#dateStart2').val());
				startEnd=parseDateValue($j('#dateEnd2').val());
			}else{
				$j('#dateDiv2').show();
				dontflag=1;
			}
			
		}else if( ($j('#dateStart2').val()!="" && $j('#dateEnd2').val()=="" ) || ($j('#dateStart2').val()=="" && $j('#dateEnd2').val()!="")){
			
			alert("Please enter correct Date Range");
			dontflag=1;
			   
		}
		
		if($j('#dateStart3').val()!="" && $j('#dateEnd3').val()!=""){
			var dateElements = $j('.datepic').get();
			  $j.each(dateElements, function(){
			customRange3(this);
			  });
			if($j('#dummyform').valid()){
			altstartDate=parseDateValue($j('#dateStart3').val());
			altstartEnd=parseDateValue($j('#dateEnd3').val());
			}
			else{
				$j('#dateDiv3').show();
				dontflag=1;
				}
		}else if( ($j('#dateStart3').val()!="" && $j('#dateEnd3').val()=="" ) || ($j('#dateStart3').val()=="" && $j('#dateEnd3').val()!="")){
			
			alert("Please enter correct Date Range");
			dontflag=1;
			   
		}
		
		
		if(reqType=='<%=VelosMidConstants.CT_LAB_ORDER%>'){ 
			sampleAtlab='Y';
		}
		if(reqType=='<%=VelosMidConstants.CT_SHIP_ORDER%>'){ 
			sampleAtlab='N';
		}
		if(reqType!=''){ 
			reqType=$j('#orderType3').val();
		}
		var is_all_entries=$j('#showsRowtasks :selected').text();
		
		if((regId3!=null && regId3!="") || (reqType!=null && reqType!="") || (startDate!=null && startDate!="" && startEnd!=null && startEnd!="") || (ordStatus!='' && ordStatus!=null) || (altstartDate!=null && altstartDate!="" && altstartEnd!=null && altstartEnd!="")){
			alertsparams='&pksiteId='+$j('#cbbdrpdwn').val()+'&reqType='+reqType+'&sampleAtLab='+sampleAtlab+'&ordstatus='+ordStatus+'&dateStart='+startDate+'&dateEnd='+startEnd+'&regId3='+regId3+'&altdateStart='+altstartDate+'&altdateEnd='+altstartEnd;
		}
		
		//alert("inside fn_Filteralerts::::"+alertsparams);
		
		if(dontflag==0 && alertsparams!=""){
			$j('#alertParam').val(alertsparams);
			$j('#alrt_allEntries_flag').val(is_all_entries);
			loaddiv('showLPAlerts?iShowRows='+$j('#showsRowtasks').val()+$j('#alertParam').val()+'&allEntries='+is_all_entries,'tasksdiv');
			forAlertsOnloads();
			$j('#alertParam').val(alertsparams);
			$j('#alertFilterFlag').val('1'); 
		}
	
	}

	function fnResetFilterWorkFlow(){
		var reqType="";
		var startDate="";
		var startEnd="";
		var ordStatus="";
		var sampleAtlab="";
		var regId2="";
		var dontflag=0;
		var workflowparams="";
		resetValidForm('dummyform');
		$j('#wrkflowStatFilterFlag').val('0');
		reqType=$j('#orderType2').val();
		ordStatus=$j('#orderStatus2').val();
		regId2=$j('#lpRegId2').val();
		
		
		if($j('#dateStart1').val()!="" && $j('#dateEnd1').val()!=""){
			startDate=parseDateValue($j('#dateStart1').val());
			startEnd=parseDateValue($j('#dateEnd1').val());
			
		}
		
		var previousFilterVal=$j('#workflowFilterFlag').val();
		
		if(previousFilterVal=='1'){
			var is_all_entries=$j('#showsRowNotifications :selected').text();
			workflowparams='&pksiteId='+$j('#cbbdrpdwn').val();
			$j('#workflowParam').val(workflowparams);
			fnclear('dateStart1','dateEnd1');
			$j('#orderType2').val('');
			$j('#orderStatus2').val('');
			$j('#lpRegId2').val('');
			$j('#wrkflw_allEntries_flag').val(is_all_entries);
			loaddiv('showLPWorkflow?iShowRows='+$j('#showsRowNotifications').val()+$j('#workflowParam').val()+'&allEntries='+is_all_entries,'workflowdiv');
			$j('#workflowFilterFlag').val('');
			$j('#workflowParam').val(workflowparams);
			$j('#workflow_filter_value').val('');
			forWorkFlowBodyOnloads();
		}

		fnclear('dateStart1','dateEnd1');
		$j('#orderType2').val('');
		$j('#orderStatus2').val('');
		$j('#lpRegId2').val('');
		$j('#workflow_filter_value').val('');

		
	}
	function fnResetFilterAlerts(){
		var reqType="";
		var startDate="";
		var startEnd="";
		var altstartDate="";
		var altstartEnd="";
		var ordStatus="";
		var sampleAtlab="";
		var alertsparams="";
		var regId3="";
		resetValidForm('dummyform');
		$j('#alertStatFilterFlag').val('0');
		reqType=$j('#orderType3').val();
		ordStatus=$j('#orderStatus3').val();
		regId3=$j('#lpRegId3').val();
		
		if($j('#dateStart2').val()!="" && $j('#dateEnd2').val()!=""){
			startDate=parseDateValue($j('#dateStart2').val());
			startEnd=parseDateValue($j('#dateEnd2').val());
			
		}
		if($j('#dateStart3').val()!="" && $j('#dateEnd3').val()!=""){
			altstartDate=parseDateValue($j('#dateStart3').val());
			altstartEnd=parseDateValue($j('#dateEnd3').val());
			
		}
		
		var is_all_entries=$j('#showsRowtasks :selected').text();
		
		if((regId3!=null && regId3!="") || (reqType!=null && reqType!="") || (startDate!=null && startDate!="" && startEnd!=null && startEnd!="") || (ordStatus!='' && ordStatus!=null) || (altstartDate!=null && altstartDate!="" && altstartEnd!=null && altstartEnd!="")){
			alertsparams='&pksiteId='+$j('#cbbdrpdwn').val();
		}
		var previousFilterVal=$j('#alertFilterFlag').val();
		
		if(alertsparams!="" || previousFilterVal=="1"){
			$j('#alertParam').val(alertsparams);
			//alert("alert param in resetalerts::::::::::::"+$j('#alert_sortParam').val())
			$j('#alrt_allEntries_flag').val(is_all_entries);
			loaddiv('showLPAlerts?iShowRows='+$j('#showsRowtasks').val()+$j('#alertParam').val()+'&allEntries='+is_all_entries,'tasksdiv');
			fnclear('dateStart2','dateEnd2');
			fnclear('dateStart3','dateEnd3');
			$j('#orderType3').val('');
			$j('#orderStatus3').val('');
			$j('#alertFilterFlag').val('');
			$j('#lpRegId3').val('');
			$j('#alrt_filter_value').val('');
			forAlertsOnloads();
			$j('#alertParam').val(alertsparams);
		}
		fnclear('dateStart2','dateEnd2');
		fnclear('dateStart3','dateEnd3');
		$j('#orderType3').val('');
		$j('#orderStatus3').val('');
		$j('#lpRegId3').val('');
		
	}
	function getAlertsParam(){

		if($j('#alertParam').val()==null || $j('#alertParam').val()=="" || $j('#alertParam').val()==undefined){
			var is_all_entries=$j('#showsRowtasks :selected').text();
			alertsparams='&pksiteId='+$j('#cbbdrpdwn').val();
			$j('#alertParam').val(alertsparams);
		}
		
	}

	function getOrderDetails(pkorder,pkcord,ordertype,fkCbbId){
		var flag="123";
		var subtp = '<s:text name="garuda.cbu.order.processConst"/>'
		//window.location="cdrcbuView?orderId="+pkorder+"&orderType="+ordertype+"&pkcordId="+pkcord+"&varflag="+flag+"&noteType="+subtp;
		submitpost('cdrcbuView',{'pkcordId':pkcord,'orderId':pkorder,'orderType':ordertype,'varflag':flag,'noteType':subtp});
	}
		
	function loadGroup(pkorder,pkcord,ordertype,fkCbbId,userId,regid,alertId)
	{
		
		var url="getGroupsByIdsForOrderType?fkCbbId="+fkCbbId+"&orderId="+pkorder+"&orderType="+ordertype+"&pkcordId="+pkcord+"&userId="+userId+"&alertInstanceId="+alertId+"&regIdval="+regid;
		loadPageByGetRequset(url,'usersitegrpsdropdown');
		
	}
	function saveQuickNotes(){
		refreshDivQuickNote("saveQuickNotes","quicknotes","quicknoteform");
		$j("#quicknotesta").val("");
		
	}
	function refreshDivQuickNote(url,divname,formId){
		$j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        data : $j("#"+formId).serialize(),
	        success: function (result){
	        	$j('.ui-datepicker').html("");
	        	//$('.tabledisplay').html("");
	            var $response=$j(result);
	            //query the jq object for the values
	            var errorpage = $response.find('#errorForm').html();
	           // alert(oneval);
	           // var subval = $response.find('#sub').text();
	            if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{
	            	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
	            	constructTable();
	            }
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }
		});
	  
	}
function forOrdersBodyOnloads(){
	
	//alert("Inside forOrdersBodyOnloads");
	
	fn_events1('orders','targetall2','ulSelectColumn2','showsRoworder','PENDING');
	var is_all_entries=$j('#ord_allEntries_flag').val();
	manageLegandHeight();			
	maindivwidth=$j('.maincontainer').width();
	var orderwrapper=maindivwidth*0.8;
	var temp1=orderwrapper*0.96;
	var scr_hgh=screen.height*0.20;
	var temp=scr_hgh;
	var o2Table ="";
	var seachTxt=$j('#ord_filter_value').val();
	var RecCount=$j('#tentriesOrder').val();
	var pageNo=$j('#paginateWrapper1').find('.currentPage').text();
	var sortVal=$j('#ord_sortParam').val();
	var sort=0;
	var sortDir="asc";
	var showEntriesTxt;
	if(sortVal!=undefined && sortVal!=null && sortVal!=""){
		sort=$j('#ord_sortParam').val();
		sortDir=$j('#ord_sort_type').val();
	}	
	//showEntriesTxt='Show <select name="showsRow" id="showsRoworder" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+$j('#tentriesOrder').val()+'" >ALL</option></select> Entries';
	showEntriesTxt='Show <select name="showsRow" id="showsRoworder" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries';
	
	//alert("is_all_entries:::::::::::::::::::::::::"+is_all_entries)
	
	if(is_all_entries=='ALL'){

		var is_all_entries=$j('#showsRoworder :selected').text();
		o2Table =	$j('#orders').dataTable({
			"sScrollY": temp,
			"sDom": "frtiS",
			"sAjaxSource": 'getJsonPagination.action?TblFind=lporders'+$j('#ordParams').val()+'&allEntries=ALL',
			"bServerSide": true,
			"bProcessing": true,
			"bRetrieve" : true,
			"bDestroy": true,
			"oSearch": {"sSearch": seachTxt},
			"aaSorting": [[ sort, sortDir ]],
			"bSortCellsTop": true,
			"bDeferRender": true,
			"bAutoWidth": false,
			"sAjaxDataProp": "aaData",
			"aoColumnDefs": [
							   {		
									"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
					                	   
			                	 		return "";
			                  }},
			                  {		
			                	  
			                	  "aTargets": [1], "mDataProp": function ( source, type, val ) {
			                	   
		                	 		return source[0];
		                	     }},
			                    { 	
		                	    	
		                	    	    "aTargets": [2], "mDataProp": function ( source, type, val ) { 
		                	    	    var ordType=source[1].replace("-","&#45;"); 
			                	 		return "<a href='#' onclick='loadGroup(\""+source[5]+"\",\""+source[6]+"\",\""+ordType+"\",\""+source[7]+"\",\""+ "<s:property value='#request.userId' />" +"\",\""+source[0]+"\");' >"+source[1]+"</a>";
			                	 }},
							  {
			                		 "aTargets": [3], "mDataProp": function ( source, type, val ) { 
									    return source[2];
									    }},
							  {
									    	"aTargets": [4], "mDataProp": function ( source, type, val ) {
								 	   return source[ 3];
							  		}},
							  {
							  			"aTargets": [5], "mDataProp": function ( source, type, val ) { 
								  		return source[4];
							  }}
							  
							  		
							],
		"fnInitComplete": function(){
			
							},
							"fnDrawCallback": function() { 
										     orderSort();
										     ColManForAll('orders','targetall2','false');
											 $j('#orders_wrapper').find('.dataTables_scrollFoot').hide();
										     $j('#orders_info').show();
											 $j("#showsRoworder option:contains('ALL')").attr('selected', 'selected');
											 showStatusFilterText('orderFiltStatusTxt','ordStatFilterFlag');
							}
		});  
		$j('#orders_filter').before(showEntriesTxt);
	
}else{
	
	o2Table =	$j('#orders').dataTable({
		"sScrollY": temp,
		"sAjaxSource": 'getJsonPagination.action?TblFind=lporders'+$j('#ordParams').val()+'&allEntries=ALL&iDisplayStart=0&iDisplayLength='+is_all_entries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+is_all_entries,
		"bServerSide": true,
		"bProcessing": true,
		"bRetrieve" : true,
		"bDestroy": true,
		"oSearch": {"sSearch": seachTxt},
		"aaSorting": [[ sort, sortDir ]],
		"bSortCellsTop": true,
		"bDeferRender": true,
		"bAutoWidth": false,
		"sAjaxDataProp": "aaData",
		"aoColumnDefs": [
						   {		
								"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
				                	   
		                	 		return "";
		                  }},
		                  {		
		                	  
		                	  "aTargets": [1], "mDataProp": function ( source, type, val ) {
		                	   
	                	 		return source[0];
	                	     }},
		                    { 	
	                	    	
	                	    	    "aTargets": [2], "mDataProp": function ( source, type, val ) { 
	                	    	    var ordType=source[1].replace("-","&#45;"); 
		                	 		return "<a href='#' onclick='loadGroup(\""+source[5]+"\",\""+source[6]+"\",\""+ordType+"\",\""+source[7]+"\",\""+ "<s:property value='#request.userId' />" +"\",\""+source[0]+"\");' >"+source[1]+"</a>";
		                	 }},
						  {
		                		 "aTargets": [3], "mDataProp": function ( source, type, val ) { 
								    return source[2];
								    }},
						  {
								    	"aTargets": [4], "mDataProp": function ( source, type, val ) {
							 	   return source[ 3];
						  		}},
						  {
						  			"aTargets": [5], "mDataProp": function ( source, type, val ) { 
							  		return source[4];
						  }}
						  
						  		
						],
	"fnInitComplete": function(){
		
						},
						"fnDrawCallback": function() { 
										 orderSort();
									     ColManForAll('orders','targetall2','false');
									     showStatusFilterText('orderFiltStatusTxt','ordStatFilterFlag');
						}
	});  
}
	
	$j('#orders_info').hide();
	$j('#orders_paginate').hide();
	$j('#orders_length').empty();
	$j('#orders_length').replaceWith(showEntriesTxt);
	if($j('#entriesOrder').val()!=null || $j('#entriesOrder').val()!=""){
		$j('#showsRoworder').val($j('#entriesOrder').val());
	}
	$j('#showsRoworder').change(function(){
		getOrderParamval();
		var is_all_ord=$j('#showsRoworder :selected').html();
		$j('#ord_allEntries_flag').val(is_all_ord);
		paginationFooter(0,"showLPOrders,showsRoworder,temp,quicknoteform,orderdiv,forOrdersBodyOnloads,ordParams,ord_sortParam,ord_allEntries_flag");
		//getHiddedValues();
	});
	
	getOrderParamval();
	
	$j('#resetSortOrd').click(function(){
		
		if($j('#ord_sortParam').val()!=undefined && $j('#ord_sortParam').val()!=null && $j('#ord_sortParam').val()!=""){
			
			$j('#ord_sortParam').val('');
			$j('#ord_sort_type').val('');
			var oTable = $j('#orders').dataTable();
			oTable.fnSort( [ [0,'asc'] ] );
			oTable.fnDraw();
		}
	});
		
	
	var empty='';
	$j('#targetall2').removeAttr('overflow');
	//$j('#orders').find('thead').html(empty);
	
	$j('#orders_wrapper').find('.dataTables_scrollHead').css('position','static');
	$j('#orders_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	$j('#orders_wrapper').find('.dataTables_scrollHead').css('z-index','1000');
	
	
		
	var scrollbody_html=$j('#orders_wrapper').find('.dataTables_scrollHead').html();
	var cleardiv='<div style="clear:both"></div>';
	var concatenated_html=scrollbody_html+cleardiv;
	$j('#orders_wrapper').find('.dataTables_scrollHead').after('<div style="clear:both"></div>');
	
	$j('#orders_wrapper').css('overflow','hidden');
	$j('#orders_wrapper').find('.dataTables_scrollBody').scroll(function(){
		if($j('#targetall2').is(':visible')){
			$j('#targetall2').fadeOut();
		}
		$j('#orders_wrapper').find('.dataTables_scrollHead').css('overflow','hidden');
	});
	$j('#orders_wrapper').find('#thSelectColumn2 div').click(function(){
		$j('#orders_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	});

	if ( o2Table.length > 0 ) {
		o2Table.fnAdjustColumnSizing(false);
	}
	
	
	if(is_all_entries=='ALL'){
	
		$j("#showsRoworder option:contains(" + is_all_entries + ")").attr('selected', 'selected'); 
	}
	
	getSearchableSelect('AssignTo1','id');
	
	$j('#orders_wrapper').find('#assignToTh div').click(function(){
		$j('#orders_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	});
	
	
	$j('#orders_filter').find(".searching1").keydown(function(e){
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#ord_filter_value').val(val);
		  if(e.keyCode==13)
		  {
			  if(val!=null && val!=""){
			  $j('#ordFilterFlag').val('1');
			  }
			  paginationFooter(0,"showLPOrders,showsRoworder,temp,quicknoteform,orderdiv,forOrdersBodyOnloads,ordParams,ord_sortParam,ord_allEntries_flag");
		  }
	}); 
	
	var validator = $j("#dummyform").validate({
		invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    }
	});
}
function orderSort(){
	var listValue = $j('#ordTheadTr').find('th').get(), lSize = listValue.length, l;
	for(l = 0; l < lSize; l++){
		var thisVal = $j(listValue[l]);
	   	if($j(thisVal).hasClass('sorting_asc')){
			$j('#ord_sortParam').val(l);
			$j('#ord_sort_type').val('asc');
		}
	   	if($j(thisVal).hasClass('sorting_desc')){
			$j('#ord_sortParam').val(l);
			$j('#ord_sort_type').val('desc');
		}
	}
}
function customRange(input) {
 
	if (input.id == 'dateEnd') {
	   
	    if($j("#dateStart").val()!="" && $j("#dateStart").val()!=null){
	    var minTestDate=new Date($j("#dateStart").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd').datepicker( "option", "minDate", null);
		}
	  } else if (input.id == 'dateStart') {
		  if($j("#dateEnd").val()!="" && $j("#dateEnd").val()!=null){
		    var minTestDate=new Date($j("#dateEnd").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		    $j('#dateStart').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart').datepicker( "option", "maxDate", null);
		  }
	  }
}

function forWorkFlowBodyOnloads(){

	fn_events1('workflow','targetall','ulSelectColumn','showsRowNotifications','PENDING');
	var is_all_entries=$j('#wrkflw_allEntries_flag').val();
	manageLegandHeight();			
	getWorkflowParams();
	maindivwidth=$j('.maincontainer').width();
	var orderwrapper=maindivwidth*0.8;
	var temp1=orderwrapper*0.96;
	var temp=screen.height*0.20;
	var o3Table ="";
	var RecCount=$j('#tentriesWorkflow').val();
	var pageNo=$j('#paginateWrapper2').find('.currentPage').text();
	var showEntriesTxt;
	var seachTxt=$j('#workflow_filter_value').val();
	var sort=0;
	var sortDir="asc";
	var lastFilter=$j('#wrkflow_sortParam').val();
	if(lastFilter!=undefined && lastFilter!=null && lastFilter!=""){
		sort=$j('#wrkflow_sortParam').val();
		sortDir=$j('#wrkflw_sort_type').val();
	}	
	
	//showEntriesTxt='Show <select name="showsRow" id="showsRowNotifications" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+$j('#tentriesWorkflow').val()+'" >ALL</option></select> Entries';
	showEntriesTxt='Show <select name="showsRow" id="showsRowNotifications" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries';

	if(is_all_entries=='ALL'){
		
		o3Table =	$j('#workflow').dataTable({
			"sScrollY": temp,
			"sDom": "frtiS",
			"sAjaxSource": 'getJsonPagination.action?TblFind=lpworkflow'+$j('#workflowParam').val()+'&allEntries=ALL',
			"bServerSide": true,
			"bProcessing": true,
			"bRetrieve" : true,
			"aaSorting": [[ sort, sortDir ]],
			"bDestroy": true,
			"bSortCellsTop": true,
			"bDeferRender": true,
			"oSearch": {"sSearch": seachTxt},
			"sAjaxDataProp": "workFlowNotiLst",
			"aoColumnDefs": [
							   {		
									"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
					                	   
			                	 		return "";
			                  }},
			                  {		
			                	  
			                	  "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                	   
			                		  var temp="";
			                		  if(source.orderNumber!='-'){
			                			  var ordType=String(source.orderType).replace("-","&#45;"); 
			                			  temp="<a href='#' onclick='loadGroup(\""+source.pkOrder+"\",\""+source.fkentityId+"\",\""+ordType+"\",\""+source.fksiteid+"\",\""+'<s:property value="#request.userId"/>'+"\",\""+source.regId1+"\")'>"+source.orderType+"</a>";
			                		  }else{
			                			  if(source.taskCompflag==1){
			                				  temp='&thinsp;<img src="images/tick_mark.gif"/>'+source.activityDesc;
			                			  }else if(source.taskCompflag==0){
			                				  temp="<a href='#"+source.activityName+"Div' onclick='loadGroup(\""+source.pkOrder+"\",\""+source.fkentityId+"\",\""+source.orderType+"\",\""+source.fksiteid+"\",\""+ '<s:property value="#request.userId"/>' +"\",\""+source.regId1+"\")'>"+source.activityDesc+"</a>";
			                			  }else{ temp='';}
			                		  }
			                		  
			                		  
		                	 		return temp;
		                	     }},
			                     { 	
		                	    	
		                	        "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
		                	    		 
		                	    	 var temp="";
		                	    	 if(source.orderNumber!='-'){
		                	    		 temp=source.cordRegistryId;
		                	    	 }
		                	    	 else{
		                	    		 temp='-';
		                	    	 }
		                	    	 return temp;
			                	 }},
							     {
			                		 "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			                			 
			                			 var temp="";
			                	    	 if(source.orderDate!='-'){
			                	    		 temp=source.orderDate;
			                	    	 }
			                	    	 else{
			                	    		 temp='-';
			                	    	 }
			                	    	 return temp;
							     }},
							     {
									    	"aTargets": [ 4], "mDataProp": function ( source, type, val ){
									    	
									    	var temp="";
					                	    if(source.orderDate!='-'){
					                	      temp=source.orderStatus;
					                	    }
					                	    else{
					                	     temp='-';
					                	    }
					                	    return temp;
							     }}
							  		
							],
		"fnDrawCallback": function(){
			
								 workflowSort();
								 ColManForAll('workflow','targetall','false');
							     $j('#workflow_wrapper').find('.dataTables_scrollFoot').hide();
							     $j('#workflow_info').hide();
								 $j("#showsRowNotifications option:contains('ALL')").attr('selected', 'selected'); 		 
								 showStatusFilterText('wrkflowFiltStatusTxt','wrkflowStatFilterFlag');
							},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
								var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
								if(iTotal==0){
									temptxt='Showing 0 to 0 of 0 entries';
								}
								if(iMax!=iTotal){
									temptxt=temptxt+'(filtered from '+iMax+' entries)';
								}
							    $j('#entriesvalues2').html(temptxt);
							    return temptxt;
							  }
		});  
		$j('#workflow_filter').before(showEntriesTxt);
		$j('#workflow_info').after('<div id="entriesvalues2"><div>');
       	var temptxt='Showing 1 to '+$j('#tentriesWorkflow').val()+' of '+$j('#tentriesWorkflow').val()+' entries';
       	$j('#entriesvalues2').html(temptxt);
		
	}else{
		
		o3Table =$j('#workflow').dataTable({
			"sScrollY": temp,
			"sAjaxSource": 'getJsonPagination.action?TblFind=lpworkflow'+$j('#workflowParam').val()+'&allEntries=ALL&iDisplayStart=0&iDisplayLength='+is_all_entries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+is_all_entries,
			"bServerSide": true,
			"bProcessing": true,
			"bRetrieve" : true,
			"aaSorting": [[ sort, sortDir ]],
			"bDestroy": true,
			"bSortCellsTop": true,
			"bDeferRender": true,
			"oSearch": {"sSearch": seachTxt},
			"sAjaxDataProp": "workFlowNotiLst",
			"aoColumnDefs": [
							   {		
									"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
					                	   
			                	 		return "";
			                  }},
			                  {		
			                	  
			                	  "aTargets": [ 1], "mDataProp": function ( source, type, val ) {
			                	   
			                		  var temp="";
			                		  if(source.orderNumber!='-'){
			                			  var ordType=String(source.orderType).replace("-","&#45;"); 
			                			  temp="<a href='#' onclick='loadGroup(\""+source.pkOrder+"\",\""+source.fkentityId+"\",\""+ordType+"\",\""+source.fksiteid+"\",\""+'<s:property value="#request.userId"/>'+"\",\""+source.regId1+"\")'>"+source.orderType+"</a>";
			                		  }else{
			                			  if(source.taskCompflag==1){
			                				  temp='&thinsp;<img src="images/tick_mark.gif"/>'+source.activityDesc;
			                			  }else if(source.taskCompflag==0){
			                				  temp="<a href='#"+source.activityName+"Div' onclick='loadGroup(\""+source.pkOrder+"\",\""+source.fkentityId+"\",\""+source.orderType+"\",\""+source.fksiteid+"\",\""+ '<s:property value="#request.userId"/>' +"\",\""+source.regId1+"\")'>"+source.activityDesc+"</a>";
			                			  }else{ temp='';}
			                		  }
			                		  
			                		  
		                	 		return temp;
		                	     }},
			                     { 	
		                	    	
		                	        "aTargets": [ 2], "mDataProp": function ( source, type, val ) {
		                	    		 
		                	    	 var temp="";
		                	    	 if(source.orderNumber!='-'){
		                	    		 temp=source.cordRegistryId;
		                	    	 }
		                	    	 else{
		                	    		 temp='-';
		                	    	 }
		                	    	 return temp;
			                	 }},
							     {
			                		 "aTargets": [ 3], "mDataProp": function ( source, type, val ) {
			                			 
			                			 var temp="";
			                	    	 if(source.orderDate!='-'){
			                	    		 temp=source.orderDate;
			                	    	 }
			                	    	 else{
			                	    		 temp='-';
			                	    	 }
			                	    	 return temp;
							     }},
							     {
									    	"aTargets": [ 4], "mDataProp": function ( source, type, val ){
									    	
									    	var temp="";
					                	    if(source.orderDate!='-'){
					                	      temp=source.orderStatus;
					                	    }
					                	    else{
					                	     temp='-';
					                	    }
					                	    return temp;
							     }}
							  		
							],
		"fnDrawCallback": function(){
								 
								 workflowSort();
								 ColManForAll('workflow','targetall','false');   
								 showStatusFilterText('wrkflowFiltStatusTxt','wrkflowStatFilterFlag');
							},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
								var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
								if(iTotal==0){
									temptxt='Showing 0 to 0 of 0 entries';
								}
								if(iMax!=iTotal){
									temptxt=temptxt+'(filtered from '+iMax+' entries)';
								}
							    $j('#entriesvalues2').html(temptxt);
							    return temptxt;
							  }
		});  
		
	}

	$j('#workflow_info').hide();
	$j('#workflow_paginate').hide();
	$j('#workflow_length').empty();
	$j('#workflow_length').replaceWith(showEntriesTxt);
	if($j('#entriesWorkflow').val()!=null || $j('#entriesWorkflow').val()!=""){
		$j('#showsRowNotifications').val($j('#entriesWorkflow').val());
	}
	$j('#showsRowNotifications').change(function(){
		var is_all_ord=$j('#showsRowNotifications :selected').html();
		$j('#wrkflw_allEntries_flag').val(is_all_ord);
		getWorkflowParams();
		paginationFooter(0,"showLPWorkflow,showsRowNotifications,temp,quicknoteform,workflowdiv,forWorkFlowBodyOnloads,workflowParam,wrkflow_sortParam,wrkflw_allEntries_flag");	
	});
	
	$j('#resetSortWorkflow').click(function(){
		
		if($j('#wrkflow_sortParam').val()!=undefined && $j('#wrkflow_sortParam').val()!=null && $j('#wrkflow_sortParam').val()!=""){
			$j('#wrkflow_sortParam').val('');
			$j('#wrkflw_sort_type').val('');
			var oTable = $j('#workflow').dataTable();
			oTable.fnSort( [ [0,'asc'] ] );
			oTable.fnDraw();
		}
	});
	
	getWorkflowParams();
	
	var empty='';
	$j('#targetall').removeAttr('overflow');
	
	$j('#workflow_wrapper').find('.dataTables_scrollHead').css('position','static');
	$j('#workflow_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	$j('#workflow_wrapper').find('.dataTables_scrollHead').css('z-index','1000');
	
		
	var scrollbody_html=$j('#workflow_wrapper').find('.dataTables_scrollHead').html();
	var cleardiv='<div style="clear:both"></div>';
	var concatenated_html=scrollbody_html+cleardiv;
	$j('#workflow_wrapper').find('.dataTables_scrollHead').after('<div style="clear:both"></div>');
	
	$j('#workflow_wrapper').css('overflow','hidden');
	$j('#workflow_wrapper').find('.dataTables_scrollBody').scroll(function(){
		if($j('#targetall').is(':visible')){
			$j('#targetall').fadeOut();
		}
		$j('#workflow_wrapper').find('.dataTables_scrollHead').css('overflow','hidden');
	});
	$j('#workflow_wrapper').find('#thSelectColumn div').click(function(){
		$j('#workflow_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	});
	
	if ( o3Table.length > 0 ) {
		o3Table.fnAdjustColumnSizing(false);
	} 
	
	if($j('#wrkflw_allEntries_flag').val()=='ALL'){
		is_all_entries=$j('#wrkflw_allEntries_flag').val();
		$j("#showsRowNotifications option:contains(" + is_all_entries + ")").attr('selected', 'selected'); 
	}
	
	$j('#workflow_filter').find(".searching1").keydown(function(e){
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#workflow_filter_value').val(val);
		  if(e.keyCode==13)
		  { 
			  if(val!=null && val!=""){
				  $j('#workflowFilterFlag').val('1');  
			  }
			  paginationFooter(0,"showLPWorkflow,showsRowNotifications,temp,quicknoteform,workflowdiv,forWorkFlowBodyOnloads,workflowParam,wrkflow_sortParam,wrkflw_allEntries_flag");
		  }
	});  
}
function workflowSort(){
	var listValue = $j('#wrkflowTheadTr').find('th').get(), lSize = listValue.length, l;
	for(l = 0; l < lSize; l++){
		var thisVal = $j(listValue[l]);
	   	if($j(thisVal).hasClass('sorting_asc')){
			$j('#wrkflow_sortParam').val(l);
			$j('#wrkflw_sort_type').val('asc');
		}
	   	if($j(thisVal).hasClass('sorting_desc')){
			$j('#wrkflow_sortParam').val(l);
			$j('#wrkflw_sort_type').val('desc');
		}
   }

}
function getWorkflowParams(){
	
	if($j('#workflowParam').val()==null || $j('#workflowParam').val()=="" || $j('#workflowParam').val()==undefined){
		var is_all_entries=$j('#showsRowNotifications :selected').text();
		workflowparams='&pksiteId='+$j('#cbbdrpdwn').val();
		$j('#workflowParam').val(workflowparams);
	}
	
}
function customRange1(input) {
/* if (input.id == 'dateEnd1') {
	    return {
	      minDate: jQuery('#dateStart1').datepicker("getDate")
	    };
	  } else if (input.id == 'dateStart1') {
	    return {
	      maxDate: jQuery('#dateEnd1').datepicker("getDate")
	    };
	  }*/
	if (input.id == 'dateEnd1') {
	    /*return {
	      minDate: jQuery('#dateStart').datepicker("getDate")
	    };*/
	    if($j("#dateStart1").val()!="" && $j("#dateStart1").val()!=null){
	    var minTestDate=new Date($j("#dateStart1").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd1').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd1').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart1') {
		  if($j("#dateEnd1").val()!="" && $j("#dateEnd1").val()!=null){
		  var minTestDate=new Date($j("#dateEnd1").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
	   /* return {
	      maxDate: jQuery('#dateEnd').datepicker("getDate")
	    };*/
		  $j('#dateStart1').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart1').datepicker( "option", "maxDate", "");
		}
	  }
}

function forAlertsOnloads(){
	
	  	fn_events1('tasks','targetUl','ulSelectCol','showsRowtasks','PENDING'); 
	  	var is_all_entries=$j('#alrt_allEntries_flag').val();
	   	manageLegandHeight();			
	   	getAlertsParam();
		maindivwidth=$j('.maincontainer').width();
		var orderwrapper=maindivwidth*0.8;
		var temp1=orderwrapper*0.96;
		var scr_hgh=screen.height*0.20;
		var temp=scr_hgh;
		var showEntriesTxt;
		var alertTable = "";
		var seachTxt=$j('#alrt_filter_value').val();
		var RecCount=$j('#tentriestasks').val();
		var pageNo=$j('#paginateWrapper3').find('.currentPage').text();
		var sort=0;
	    var sortDir="asc";
	    var lastSortval=$j('#alert_sortParam').val();
	    if(lastSortval!=undefined && lastSortval!=null && lastSortval!=""){
		    sort=$j('#alert_sortParam').val();
		    sortDir=$j('#alert_sort_type').val();
	    }
	    
	  //showEntriesTxt='Show <select name="showsRow" id="showsRowtasks" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+$j('#tentriestasks').val()+'" >ALL</option></select> Entries';
		showEntriesTxt='Show <select name="showsRow" id="showsRowtasks" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries';
	
		if(is_all_entries=='ALL'){
			o3Table =	$j('#tasks').dataTable({
				"sScrollY": temp,
				"sDom": "frtiS",
				"sAjaxSource": 'getJsonPagination.action?TblFind=lpalerts'+$j('#alertParam').val()+'&allEntries=ALL',
				"bServerSide": true,
				"bProcessing": true,
				"bRetrieve" : true,
				"bDestroy": true,
				"oSearch": {"sSearch": seachTxt},
				"bAutoWidth": false,
				"aaSorting": [[ sort, sortDir ]],
				"bDeferRender": true,
				"bSortCellsTop": true,
				"sAjaxDataProp": "alertNotificationsLst",
				"aoColumnDefs": [
								   {		
										"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ){
				                	 	return "";
				                  }},
				                  {		
				                	  "aTargets": [1], "mDataProp": function ( source, type, val ){
				                		  
				                		                 var temp;
				                		                 if(source.orderNum!='-'){
				                		                	 temp=source.orderType;
				                		                 }else{
				                		                	 if(source.alertCloseFlag==1){
				                		                		 temp='<div style="relative;float: left;width: 5%">&thinsp;<img src="images/tick_mark.gif"/>&thinsp;</div><div style="float: left;width: 35%;">'+source.alertTitle+'</div><div style="float: left;width: 1%;">-</div><div style="float: left;width: 59%;">'+source.alertWording+'</div>';
				                		                	 }
				                		                	 if(source.alertCloseFlag==0){
				                		                		 temp="<div style='relative;float: left;width: 5%;'>&emsp;&emsp;</div><div style='float: left;width: 35%;'><a href='#' onclick='loadGroup(\""+source.orderId+"\",\""+source.cordId+"\",\""+source.orderType+"\",\""+source.fksiteid+"\",\""+'<s:property value="#request.userId"/>'+"\",\""+source.regId1+"\",\""+source.alertInstanceId+"\")'>"+source.alertTitle+"</a></div><div style='float: left;width: 1%;'>-</div><div style='float: left;width: 59%;'>"+source.alertWording+"</div>";
				                		                	 }
				                		                 }
				                		  
			                	 	  					 return temp;
			                	   }},
				                   { 	
			                	    	 "aTargets": [2], "mDataProp": function ( source, type, val ){ 
			                	    		 
			                	    		 var temp="";
			                	    		 if(source.orderNum!='-'){
	                		                	 temp=source.cordRegistryId;
	                		                 }else{
	                		                	 temp='-';
	                		                 }
				                	 	     return temp;
				                   }},
								   {
				                		 "aTargets": [ 3], "mDataProp": function ( source, type, val ) { 
				                			 var temp="";
 											 if(source.orderNum!='-'){
	                		                	 temp=source.orderDate;
	                		                 }
										     return temp;
								   }},
								   {
										   "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
											     
											     var temp="";
											     if(source.orderNum!='-'){
		                		                	 temp=source.orderStatus;
		                		                 }else{
		                		                	 temp='';
		                		                 }
									 	         return temp;
								   }},
								   {
									   "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
										     
										      var temp="";
										      if(source.orderNum=='-'){
										    	   temp=source.alertCreatedOn;
										      }
								 	         return temp;
							       }}
								  		
								], 
			"fnDrawCallback": function(){
			  
									   alertSort();
									   ColManForAll('tasks','targetUl','false');
								       $j('#tasks_wrapper').find('.dataTables_scrollFoot').hide();
								       $j("#showsRowtasks option:contains('ALL')").attr('selected', 'selected');
								       $j('#tasks_info').hide();
								       showStatusFilterText('alertFiltStatusTxt','alertStatFilterFlag');
								},
			"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
									var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
									if(iMax!=iTotal){
										temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries (filtered from '+iMax+' entries)';
									}
									
									if(iTotal==0){
										temptxt='Showing 0 to 0 of 0 entries';
									}
								    $j('#entriesvalues').html(temptxt);
								    return temptxt;
								  }
			});  
			$j('#tasks_filter').before(showEntriesTxt);
			$j('#tasks_info').after('<div id="entriesvalues"><div>');
	       	var temptxt='Showing 1 to '+$j('#tentriestasks').val()+' of '+$j('#tentriestasks').val()+' entries';
	       	$j('#entriesvalues').html(temptxt);
		}else{
			
			o3Table =	$j('#tasks').dataTable({
				"sScrollY": temp,
				"sAjaxSource": 'getJsonPagination.action?TblFind=lpalerts'+$j('#alertParam').val()+'&allEntries=ALL&iDisplayStart=0&iDisplayLength='+is_all_entries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+is_all_entries,
				"bServerSide": true,
				"bProcessing": true,
				"bRetrieve" : true,
				"bDestroy": true,
				"oSearch": {"sSearch": seachTxt},
				"aaSorting": [[ sort, sortDir ]],
				"bAutoWidth": false,
				"bDeferRender": true,
				"bSortCellsTop": true,
				"sAjaxDataProp": "alertNotificationsLst",
				"aoColumnDefs": [
								   {		
										"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ){
				                	 	return "";
				                  }},
				                  {		
				                	  "aTargets": [1], "mDataProp": function ( source, type, val ){
				                		  
				                		                 var temp;
				                		                 if(source.orderNum!='-'){
				                		                	 temp=source.orderType;
				                		                 }else{
				                		                	 if(source.alertCloseFlag==1){
				                		                		 temp='<div style="relative;float: left;width: 5%">&thinsp;<img src="images/tick_mark.gif"/>&thinsp;</div><div style="float: left;width: 35%;">'+source.alertTitle+'</div><div style="float: left;width: 1%;">-</div><div style="float: left;width: 59%;">'+source.alertWording+'</div>';
				                		                	 }
				                		                	 if(source.alertCloseFlag==0){
				                		                		 temp="<div style='relative;float: left;width: 5%;'>&emsp;&emsp;</div><div style='float: left;width: 35%;'><a href='#' onclick='loadGroup(\""+source.orderId+"\",\""+source.cordId+"\",\""+source.orderType+"\",\""+source.fksiteid+"\",\""+'<s:property value="#request.userId"/>'+"\",\""+source.regId1+"\",\""+source.alertInstanceId+"\")'>"+source.alertTitle+"</a></div><div style='float: left;width: 1%;'>-</div><div style='float: left;width: 59%;'>"+source.alertWording+"</div>";
				                		                	 }
				                		                 }
				                		  
			                	 	  					 return temp;
			                	   }},
				                   { 	
			                	    	 "aTargets": [2], "mDataProp": function ( source, type, val ){ 
			                	    		 
			                	    		 var temp="";
			                	    		 if(source.orderNum!='-'){
	                		                	 temp=source.cordRegistryId;
	                		                 }else{
	                		                	 temp='-';
	                		                 }
				                	 	     return temp;
				                   }},
								   {
				                		 "aTargets": [ 3], "mDataProp": function ( source, type, val ) { 
				                			 var temp="";
 											 if(source.orderNum!='-'){
	                		                	 temp=source.orderDate;
	                		                 }
										     return temp;
								   }},
								   {
										   "aTargets": [ 4], "mDataProp": function ( source, type, val ) {
											     
											     var temp="";
											     if(source.orderNum!='-'){
		                		                	 temp=source.orderStatus;
		                		                 }else{
		                		                	 temp='';
		                		                 }
									 	         return temp;
								   }},
								   {
									   "aTargets": [ 5], "mDataProp": function ( source, type, val ) {
										     
										      var temp="";
										      if(source.orderNum=='-'){
										    	   temp=source.alertCreatedOn;
										      }
										      return temp;
							       }}
								   
								  		
								], 
			"fnDrawCallback": function(){
								      
									   alertSort();
									   ColManForAll('tasks','targetUl','false');
									   showStatusFilterText('alertFiltStatusTxt','alertStatFilterFlag');
								}
		});
			
		}
		if(is_all_entries!='ALL'){
			$j('#tasks_info').hide();
		}
		$j('#tasksdiv').addClass('tasksLoad');
		$j('#tasks_paginate').hide();
		$j('#tasks_length').empty();
		$j('#tasks_length').replaceWith(showEntriesTxt);
		if($j('#entriestasks').val()!=null || $j('#entriestasks').val()!=""){
			$j('#showsRowtasks').val($j('#entriestasks').val());
		}
		$j('#showsRowtasks').change(function(){
			getAlertsParam();
			var is_all_entries=$j('#showsRowtasks :selected').html();
			$j('#alrt_allEntries_flag').val(is_all_entries);
			//alert("is_all_entries in alert tab:::::::"+$j('#alrt_allEntries_flag').val())
			paginationFooter(0,"showLPAlerts,showsRowtasks,temp,quicknoteform,tasksdiv,forAlertsOnloads,alertParam,alert_sortParam,alrt_allEntries_flag");
			//getHiddedValues();
		});
		
		var empty='';
		$j('#targetUl').removeAttr('overflow');
		//$j('#tasks').find('thead').html(empty);
		
		$j('#tasks_wrapper').find('.dataTables_scrollHead').css('position','static');
		$j('#tasks_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
		$j('#tasks_wrapper').find('.dataTables_scrollHead').css('z-index','1000');
		
			
		var scrollbody_html=$j('#tasks_wrapper').find('.dataTables_scrollHead').html();
		var cleardiv='<div style="clear:both"></div>';
		var concatenated_html=scrollbody_html+cleardiv;
		$j('#tasks_wrapper').find('.dataTables_scrollHead').after('<div style="clear:both"></div>');
		
		$j('#tasks_wrapper').css('overflow','hidden');
		$j('#tasks_wrapper').find('.dataTables_scrollBody').scroll(function(){
			if($j('#targetUl').is(':visible')){
				$j('#targetUl').fadeOut();
			}
			$j('#tasks_wrapper').find('.dataTables_scrollHead').css('overflow','hidden');
		});
		$j('#tasks_wrapper').find('#thSelectCol div').click(function(){
			$j('#tasks_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
		});
		
		if ( alertTable.length > 0 ) {
			alertTable.fnAdjustColumnSizing(false);
		}
		
		$j('#resetSortAlert').click(function(){
			if($j('#alert_sortParam').val()!=undefined && $j('#alert_sortParam').val()!=null && $j('#alert_sortParam').val()!=""){
				$j('#alert_sortParam').val('');
				$j('#alert_sort_type').val('');
				var oTable = $j('#tasks').dataTable();
				oTable.fnSort( [ [0,'asc'] ] );
				oTable.fnDraw();
			}
		});
		
		
		getAlertsParam();

		if($j('#alrt_allEntries_flag').val()=='ALL'){
			is_all_entries=$j('#alrt_allEntries_flag').val();
			$j("#showsRowtasks option:contains(" + is_all_entries + ")").attr('selected', 'selected'); 
		}
		
		$j('#tasks_filter').find(".searching1").keydown(function(e){
			  var val = $j(this).val();
			  val=$j.trim(val);
			  $j('#alrt_filter_value').val(val);
			  if(e.keyCode==13 )
			  { 
				    if(val!=null && val!=""){
				    	$j('#alertFilterFlag').val('1'); 
				    }
				    paginationFooter(0,"showLPAlerts,showsRowtasks,temp,quicknoteform,tasksdiv,forAlertsOnloads,alertParam,alert_sortParam,alrt_allEntries_flag");
			  }
		});  
		
			
}
function alertSort(){

	
	var listValue = $j('#taskTheadTr').find('th').get(), lSize = listValue.length, l;
	for(l = 0; l < lSize; l++){
		var thisVal = $j(listValue[l]);
	   	if($j(thisVal).hasClass('sorting_asc')){
			$j('#alert_sortParam').val(l);
			$j('#alert_sort_type').val('asc');
		}
	   	if($j(thisVal).hasClass('sorting_desc')){
			$j('#alert_sortParam').val(l);
			$j('#alert_sort_type').val('desc');
		}
   }

}
function customRange2(input) {
	 /* if (input.id == 'dateEnd2') {
	    return {
	      minDate: jQuery('#dateStart2').datepicker("getDate")
	    };
	  } else if (input.id == 'dateStart2') {
	    return {
	      maxDate: jQuery('#dateEnd2').datepicker("getDate")
	    };
	  }*/
	if (input.id == 'dateEnd2') {
	    /*return {
	      minDate: jQuery('#dateStart').datepicker("getDate")
	    };*/
	    if($j("#dateStart2").val()!="" && $j("#dateStart2").val()!=null){
	    var minTestDate=new Date($j("#dateStart2").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd2').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd2').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart2') {
		  if($j("#dateEnd2").val()!="" && $j("#dateEnd2").val()!=null){
		  var minTestDate=new Date($j("#dateEnd2").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
	   /* return {
	      maxDate: jQuery('#dateEnd').datepicker("getDate")
	    };*/
		  $j('#dateStart2').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart2').datepicker( "option", "maxDate", "");
		}
	  }
}
function customRange3(input) {

	if (input.id == 'dateEnd3') {
	   
	    if($j("#dateStart3").val()!="" && $j("#dateStart3").val()!=null){
	    var minTestDate=new Date($j("#dateStart3").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd3').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd3').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart3') {
		  if($j("#dateEnd3").val()!="" && $j("#dateEnd3").val()!=null){
		  var minTestDate=new Date($j("#dateEnd3").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
	   
		  $j('#dateStart3').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart3').datepicker( "option", "maxDate", "");
		}
	  }
}
function fn_FilterWorkflow(){
	
	var reqType="";
	var startDate="";
	var startEnd="";
	var ordStatus="";
	var sampleAtlab="";
	var dontflag=0;
	var workflowparams="";
	var regId2="";
	$j('#wrkflowStatFilterFlag').val('1');
	reqType=$j('#orderType2 :selected').text();
	ordStatus=$j('#orderStatus2').val();
	regId2=$j.trim($j('#lpRegId2').val());
	
	if($j('#dateStart1').val()!="" && $j('#dateEnd1').val()!=""){
		var dateElements = $j('.datepic').get();
		  $j.each(dateElements, function(){
		customRange1(this);
		  });
		if($j("#dummyform").valid()){
			startDate=parseDateValue($j('#dateStart1').val());
			startEnd=parseDateValue($j('#dateEnd1').val());
		}else{
			$('#dateDiv1').show();
			dontflag=1;
		}
		
	}else if( ($j('#dateStart1').val()!="" && $j('#dateEnd1').val()=="" ) || ($j('#dateStart1').val()=="" && $j('#dateEnd1').val()!="")){
		
		alert("Please enter correct Date Range");
		dontflag=1;
		   
	}
	if(reqType=='<%=VelosMidConstants.CT_LAB_ORDER%>'){ 
		sampleAtlab='Y';
	}
	if(reqType=='<%=VelosMidConstants.CT_SHIP_ORDER%>'){ 
		sampleAtlab='N';
	}
	if(reqType!=''){ 
		reqType=$j('#orderType2').val();
	}
	
	var is_all_entries=$j('#showsRowNotifications :selected').text();
	
	if((regId2!=null && regId2!="") || (reqType!=null && reqType!="") || (startDate!=null && startDate!="" && startEnd!=null && startEnd!="") || (ordStatus!='' && ordStatus!=null)){
		workflowparams='&pksiteId='+$j('#cbbdrpdwn').val()+'&reqType='+reqType+'&sampleAtLab='+sampleAtlab+'&ordstatus='+ordStatus+'&dateStart='+startDate+'&dateEnd='+startEnd+'&regId2='+regId2;
	}
	
	//alert("workflowparams::::"+workflowparams);
	
	if(dontflag==0 && workflowparams!=""){
		
		$j('#workflowParam').val(workflowparams);
		$j('#wrkflw_allEntries_flag').val(is_all_entries);
		loaddiv('showLPWorkflow?iShowRows='+$j('#showsRowNotifications').val()+$j('#workflowParam').val()+'&allEntries='+is_all_entries,'workflowdiv');
		$j('#workflowParam').val(workflowparams);
		$j('#workflowFilterFlag').val('1');
		forWorkFlowBodyOnloads(); 
		
	}

		
	
		
}

function getCBBdetails(pksite){

			setTimeout(function(){
				showprogressMgs();
			},0);
			setTimeout(function(){
			var visibleCount=0;
			$j('.ui-icon-minusthick').each(function(i){
				visibleCount=i;
			});
			var cbb=$j('#cbbdrpdwn').val();
			$j('.ui-icon-plusthick').each(function(i){
				$j(this).removeClass('firstLoad');
				$j('#shipmentCalenderDiv').removeClass('shipmentLoad');
			});
			$j('#ordParams').val('');
			$j('#workflowParam').val('');
			$j('#alertParam').val('');
			fnclear('dateStart','dateEnd');
			fnclear('dateStart1','dateEnd1');
			fnclear('dateStart2','dateEnd2');
			fnclear('dateStart3','dateEnd3');
			//alert("order param value in getcbb:::"+$j('#ordParam').val())	
			var allCBB='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_SITE"/>';
			var allCBBFlag=0;
			var countCBB=0;
			$j('#cbbdrpdwn :selected').each(function(i){
				temp=$j(this).val();
				if(temp==allCBB){
					allCBBFlag=1;
				}
				countCBB++;
			});
			//alert("countCBB::"+countCBB+"allCBBFlag"+allCBBFlag)
			if(countCBB>1 && allCBBFlag==1){
				$j('#cbbdrpdwn').val(allCBB);
				pksite=allCBB;
			}
			
			if(countCBB==0){
				$j('#is_cbb_selected').val('0');
			}
			
					
			if(visibleCount==0 && cbb!=null && cbb!='null' && countCBB>0){
				$j('#siteIdval').val(pksite);
				var cbbval=$j('#is_cbb_selected').val();
				if(cbbval=='0'){
					//alert("inside expected.....");
					setTimeout(function(){
						loaddiv('showLPCBBSummary?pksiteId='+$j('#cbbdrpdwn').val(),'cbbprofiletbl');
						mainOnLoad();
						$j('.lporders').triggerHandler('click');
						loaddiv('showLPOrders?iShowRows=10'+$j('#ordParams').val(),'orderdiv');
						forOrdersBodyOnloads();
						$j('.cbbSummary').triggerHandler('click');
					},0);
					
				}else{
					$j('.cbbSummary').triggerHandler('click');
					$j('.lporders').triggerHandler('click');
				}
				/*$j('.shipmentPortlet').triggerHandler('click');
				getCEvents();
				$j('.lpworkflow').triggerHandler('click');
				$j('.lpalerts').triggerHandler('click');*/
				/* $j('.cbbSummary').addClass('firstLoad');
				$j('.lporders').addClass('firstLoad'); */
				/*$j('.lpworkflow').addClass('firstLoad');
				$j('.lpalerts').addClass('firstLoad');
				$j('.shipmentPortlet').addClass('firstLoad');*/
				
			}else{
				
				var previous_siteVal=$j('#siteIdval').val();
				if(previous_siteVal!=pksite || previous_siteVal==pksite ){
					//lert("inside calling action1");
					if($j('#orderdiv').is(':visible')){
						getOrderParamval();
						param1=$j('#ordParams').val();
						param1=param1+'&resetflag='+$j('#resetOrdFilt').val()+'&reqType=&sampleAtLab=&ordstatus='+$j('#orderStatus1').val()+'&assignto=&dateStart=&dateEnd=&RegId1=';
						$j('#ord_allEntries_flag').val('10');
						$j('#ordParams').val(param1);
						var is_all_entries=$j('#showsRoworder :selected').text();
						loaddiv('showLPOrders?iShowRows=10'+$j('#ordParams').val(),'orderdiv');
						forOrdersBodyOnloads(is_all_entries);
					}
					if($j('#workflowdiv').is(':visible')){
						getWorkflowParams();
						var is_all_entries=$j('#showsRowNotifications :selected').text();
						$j('#wrkflw_allEntries_flag').val('10');
						loaddiv('showLPWorkflow?iShowRows=10'+$j('#workflowParam').val(),'workflowdiv');
						forWorkFlowBodyOnloads();
						
					}
					if($j('#tasksdiv').is(':visible')){
						getAlertsParam();
						var is_all_entries=$j('#showsRowtasks :selected').text();
						$j('#alrt_allEntries_flag').val('10');
						loaddiv('showLPAlerts?iShowRows=10'+$j('#alertParam').val(),'tasksdiv');
						forAlertsOnloads();
					}
					if($j('#cbbprofiletbl').is(':visible')){
						loaddiv('showLPCBBSummary?pksiteId='+$j('#cbbdrpdwn').val(),'cbbprofiletbl');
						mainOnLoad();
					}
					if($j('#shipmentCalenderDiv').is(':visible')){
						$j('#calendar').html('');
						showShipmentData();
						getCEvents();
						$j('#shipmentCalenderDiv').addClass('shipmentLoad');
					}
					
				}
			}
			},100);
			setTimeout(function(){
				closeprogressMsg();
			},0);		
	}
	
	function clickHere(id,ordtype){
		var vicon='icondiv'+id;
		var sampleAtlab1="";
		var reqType=ordtype;
		if(reqType=='<%=VelosMidConstants.CT_LAB_ORDER%>'){ 
			sampleAtlab1='Y';
			reqType='CT';
		}
		if(reqType=='<%=VelosMidConstants.CT_SHIP_ORDER%>'){ 
			sampleAtlab1='N';
			reqType='CT';
		}
		showModal('CBB Details','detailedView?ordertype='+reqType+'&pksiteId='+$j('#cbbdrpdwn').val()+'&sampleAtLab='+sampleAtlab1,'200','300');
	}
    function setDropDownVal(){
    	if($j('#flag').val()!=null){
    		var temp=$j('#flag').val();
    		$j('#cbbdrpdwn').val(temp);
    	}
    }

    
    function sortSelect(selElem) {
        var tmpAry = new Array();
        for (var i=0;i<selElem.options.length-1;i++) {
        		tmpAry[i] = new Array();
            	tmpAry[i][0] = selElem.options[i+1].text;
                tmpAry[i][1] = selElem.options[i+1].value;
               
        }
        tmpAry.sort();
      
        for (var i=0;i<tmpAry.length;i++) {
            var op = new Option(tmpAry[i][0], tmpAry[i][1]);
            selElem.options[i+1] = op;
        }
        return;
   }
 
function fnclear(id1,id2){
		$j('#'+id1).val('');
		$j('#'+id2).val('');
}
function togglecbbIdDiv(obj){
	$j(obj).toggleClass("ui-icon-minusthick-lp").toggleClass("ui-icon-plusthick-lp");
	$j(obj).parent().next('div').toggle();
}    
function fn_setSort_ord(label,el){
	
	var sort_to="";
	var sorted=false;
	var pageno=$j('#ordTblFoot div:first').find('b').find('.currentPage').text(); 
	$j("#ord_header").val(label);
	if($j(el).hasClass('sorting') || $j(el).hasClass('sorting_desc')){
		sort_to='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SORT_ASC"/>';
		var temp_sort_Param='&'+label+'='+sort_to;
		$j('#ord_sortParam').val(temp_sort_Param);
		$j("#ord_sort_type").val('sorting_asc');
		paginationFooter(pageno,"showLPOrders,showsRoworder,temp,quicknoteform,orderdiv,forOrdersBodyOnloads,ordParams,ord_sortParam,ord_allEntries_flag");
		sorted=true;
	}
	if($j(el).hasClass('sorting_asc') && !sorted){
		sort_to='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SORT_DES"/>';
		var temp_sort_Param='&'+label+'='+sort_to;
		$j('#ord_sortParam').val(temp_sort_Param);
		$j("#ord_sort_type").val('sorting_desc');
		paginationFooter(pageno,"showLPOrders,showsRoworder,temp,quicknoteform,orderdiv,forOrdersBodyOnloads,ordParams,ord_sortParam,ord_allEntries_flag");
		sorted=true;
	}
}
function fn_setSort_wrkflw(label,el){
	var sort_to="";
	var sorted=false;
	$j("#wrkflw_header").val(label);
	var pageno=$j('#wrkFlwTblFoot div:first').find('b').find('.currentPage').text(); 
	if($j(el).hasClass('sorting') || $j(el).hasClass('sorting_desc')){
		sort_to='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SORT_ASC"/>';
		var temp_sort_Param='&'+label+'='+sort_to;
		$j('#wrkflow_sortParam').val(temp_sort_Param);
		$j("#wrkflw_sort_type").val('sorting_asc');
		paginationFooter(pageno,"showLPWorkflow,showsRowNotifications,temp,quicknoteform,workflowdiv,forWorkFlowBodyOnloads,workflowParam,wrkflow_sortParam,wrkflw_allEntries_flag");
		sorted=true;
	}
	if($j(el).hasClass('sorting_asc') && !sorted){
		sort_to='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SORT_DES"/>';
		var temp_sort_Param='&'+label+'='+sort_to;
		$j('#wrkflow_sortParam').val(temp_sort_Param);
		$j("#wrkflw_sort_type").val('sorting_desc');
		paginationFooter(pageno,"showLPWorkflow,showsRowNotifications,temp,quicknoteform,workflowdiv,forWorkFlowBodyOnloads,workflowParam,wrkflow_sortParam,wrkflw_allEntries_flag");
		sorted=true;
	}
}
function fn_setSort_alert(label,el){
	var sort_to="";
	var sorted=false;
	$j("#alert_header").val(label);
	var pageno=$j('#taskTblFoot div:first').find('b').find('.currentPage').text(); 
	if($j(el).hasClass('sorting') || $j(el).hasClass('sorting_desc')){
		sort_to='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SORT_ASC"/>';
		var temp_sort_Param='&'+label+'='+sort_to;
		$j('#alert_sortParam').val(temp_sort_Param);
		$j("#alert_sort_type").val('sorting_asc');
		paginationFooter(pageno,"showLPAlerts,showsRowtasks,temp,quicknoteform,tasksdiv,forAlertsOnloads,alertParam,alert_sortParam,alrt_allEntries_flag");
		sorted=true;
	}
	if($j(el).hasClass('sorting_asc') && !sorted){
		sort_to='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SORT_DES"/>';
		var temp_sort_Param='&'+label+'='+sort_to;
		$j('#alert_sortParam').val(temp_sort_Param);
		$j("#alert_sort_type").val('sorting_desc');
		paginationFooter(pageno,"showLPAlerts,showsRowtasks,temp,quicknoteform,tasksdiv,forAlertsOnloads,alertParam,alert_sortParam,alrt_allEntries_flag");
		sorted=true;
	}
}
function printWidgets(divData,printDiv){
	var shipDivFlag="0";
	if(printDiv=="printShipmentDiv" && !$j("#shipmentSpan").hasClass('ui-icon-minusthick')){
		$j("#shipmentSpan").trigger("click");
		shipDivFlag="1";
	}
	setTimeout(function(){
		$j('#'+printDiv).removeClass('hide-data');
		$j('#printTemp').html($j('#'+printDiv).html());
		if(divData == 'allOrdersDiv'){
			tableAlignmentInPrint('printTemp','orders');
		}
		else if(divData == 'workflowOrdDiv'){
			tableAlignmentInPrint('printTemp','workflow');
		}
		else if(divData == 'taskLstDiv'){
			tableAlignmentInPrint('printTemp','tasks');
		}
		clickheretoprint(divData,'');
		$j('#'+printDiv).addClass('hide-data');
		if(shipDivFlag=="1"){
			$j("#shipmentSpan").trigger("click");
		}
	},3000);
}
</script>