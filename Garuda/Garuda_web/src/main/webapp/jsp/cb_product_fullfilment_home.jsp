<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@ page import="com.velos.ordercomponent.util.VelosMailConstants" %>
<script type="text/javascript">
	$j(function() {
		$j( "#datepicker" ).datepicker({changeMonth: true,
			changeYear: true});
	});	
	
	function constructTable(){
			 $j('#searchResults').dataTable();		
			 var tdObj = $j('#searchResults').find('tbody').find("tr:eq(0)").find("td:eq(0)");
				if($j(tdObj).hasClass('dataTables_empty')){
					$j('#searchResults_info').hide();
					$j('#searchResults_paginate').hide();
				}
	}
	
	function showDiv(){
		$j('#pfsearchresultparent').css('display','block');
		refreshDiv('productfullfillmentsearch','studyDashDiv2','searchCordInfo')
	}
</script>

<div class="col_100 maincontainer "><div class="col_100"><div class="portlet" id="pfsearchparent">
<div id="pfsearch" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span><!--<span class="ui-icon ui-icon-close" ></span>--><s:text name="garuda.cbuentry.label.cdrcbusearch" /></div><div class="portlet-content">
   <div id="studyDashDiv1" >   
        <form id="searchCordInfo">
         <table cellpadding="0" cellspacing="0" border="0"
		style="width: 100%; height: auto;" class="col_100">         
         	<tr>
			   <td ><s:text name="garuda.cbuentry.label.cbuid" /></td>
			   <td ><s:textfield  name="cdrCbuPojo.cdrCbuId" placeholder="CBU ID" onkeypress="return enter2Refreshdiv(event);" /></td> 
			   <td><button type="button" onclick="showDiv();"><s:text name="garuda.opentask.label.search"/></button> </td>
			</tr>				
		</table>
        </form>	
	</div></div></div></div>	
	<div class="portlet" id="pfsearchresultparent" style="display: none;"><div id="pfsearchresult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span><!--<span class="ui-icon ui-icon-close" ></span>--><s:text name="garuda.cbuentry.label.cdrcbusearch" /></div><div class="portlet-content">
	<s:form>
	<div id="studyDashDiv2">
          <table border="0" align="center" cellpadding="0" cellspacing="0" class="display" id="searchResults">
                <thead>
                      <tr>
                        <th class="th3"  scope="col"><s:text name="garuda.entry.label.cbuid"/></th>
                        <th class="th4"  scope="col"><s:text name="garuda.cbuentry.label.recadditionalinfo"/></th>
                        <th class="th5"  scope="col"><s:text name="garuda.entryTask.label.sendmail"/></th>                                             
                      </tr>
			   </thead>  
			   <tbody>			  
			       <s:iterator value="cordInfoList" var="cdrcbu" status="row">			        
					    <tr id="<s:property	value="%{#row.index}"/>" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')">
					    <td><s:property value="registryId" /></td>
						<td>
						<s:if test="addInfoReq==true"><s:text name="garuda.clinicalnote.label.yes"/></s:if><s:if test="addInfoReq==false"><s:text name="garuda.clinicalnote.label.no"/></s:if></td>
						<td><s:if test="addInfoReq==true">
						<button type="button" onclick="showModal('Email To CBB for the CBU Registry ID <s:property value="registryId" />','sendMailHome?mailCode=<s:property value="@com.velos.ordercomponent.util.VelosMailConstants@UNIT_REPORT_CODE"/>&cbuID=<s:property value="registryId" />','350','450')"><s:text name="garuda.entryTask.label.sendmail"/></button>
						</s:if></td>
						</tr>
					</s:iterator>
			   </tbody>
			   <tfoot>
					<tr><td colspan="3"></td></tr>
				</tfoot>                
             </table>		  		
        </div></s:form>     
	</div></div></div>        
</div></div>