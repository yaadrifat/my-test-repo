<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="./cb_track_session_logging.jsp" />
<s:form><fieldset style="border: 1px solid #D2D2D2;">
		<div style="width: 100%;overflow: auto;"  class="rightcont">
			<div id="idsID">
			<table cellpadding="0" cellspacing="0" width="100%">
				<!--<tr align="left">
				
					<td width="20%;"><s:text name="garuda.cbuentry.label.externalcbuid" />:
					</td>
					<td width="30%;"><s:textfield disabled="true"
						name="CdrCbuPojo.externalCbuId" id="externalCbuId" readonly="true" onkeydown="cancelBack();"  />
					</td>
										
					<td width="50%;" colspan="2"></td>
				</tr >
				--><tr align="left">
				  <td width="20%;"><s:text name="garuda.cbuentry.label.registrycbuid"/>:
				      <!--  <s:if test="#request.registryIdOnBag==true">
						   <span style="color: red;">(<s:text name="garuda.cdrcbuview.label.id_on_bag"/>)</span>
						</s:if>-->					
				  </td>
				  <td width="30%;"><s:textfield name="cdrCbuPojo.registryId" id="registryId1" readonly="true" onkeydown="cancelBack();" cssClass="idsMandatory" ></s:textfield>
				  </td>
				  <td width="20%;"><s:text name="garuda.cbuentry.label.maternalregistryid"/>:</td>
				  <s:if test="cdrCbuPojo.site.isCDRUser()==true">				  
				  <td width="30%;"><s:textfield name="cdrCbuPojo.registryMaternalId" cssClass="idsMandatory" id="registryMaternalId" readonly="true" onkeydown="cancelBack();" ></s:textfield>
				  </td>
				  </s:if>
				  <s:else>
				  <td width="30%;"><s:textfield name="cdrCbuPojo.registryMaternalId" id="registryMaternalId" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
				  </s:else>
				</tr >
				<tr align="left">
				  <td width="20%;"><s:text name="garuda.cbuentry.label.localcbuid"/>:
				   <!--   <s:if test="#request.localIdOnBag==true">
						   <span style="color: red;">(<s:text name="garuda.cdrcbuview.label.id_on_bag"/>)</span>
					</s:if>-->
				  </td>
				  <s:if test="cdrCbuPojo.site.isCDRUser()==true">
				  <td width="30%;"><s:textfield name="cdrCbuPojo.localCbuId" cssClass="idsMandatory" id="cbuLocCBUId" readonly="true" onkeydown="cancelBack();" ></s:textfield>
				  </td>
				  </s:if>
				  <s:else>
				   <td width="30%;"><s:textfield name="cdrCbuPojo.localCbuId" id="cbuLocCBUId" readonly="true" onkeydown="cancelBack();" ></s:textfield>
				  </s:else>
				  <td width="20%;"><s:text name="garuda.cbuentry.label.maternallocalid"/>:</td>
				  <td width="30%;"><s:textfield name="cdrCbuPojo.localMaternalId"  id="localMaternalId" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
				  </td>
				</tr>
				<tr align="left"><td width="20%;"><s:text name="garuda.cordentry.label.isbtid"/>:
				      <!--<s:if test="#request.isbtDinOnBag==true">
						   <span style="color: red;">(<s:text name="garuda.cdrcbuview.label.id_on_bag"/>)</span>
					   </s:if>--></td> 
				    <td width="30%;"><s:textfield name="cdrCbuPojo.cordIsbiDinCode" id="IsbiDinCode" readonly="true" onkeydown="cancelBack();" ></s:textfield>
					</td>
					<!--<td width="20%;"><s:text name="garuda.cdrcbuview.label.isbt_product_code"/>:
				  </td><td width="30%;"><s:textfield name="cdrCbuPojo.cordIsitProductCode" readonly="true" onkeydown="cancelBack();"  ></s:textfield>					              
				   </td>
			   --><!--<td width="25%">
						<s:text name="garuda.cbuentry.label.idoncbubag" />:
					</td>
					<td width="25%">
						<s:select name="cbuOnBag" multiple="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_IDS]" listKey="pkCodeId" listValue="description" disabled="true" ></s:select>
					</td>
						   -->
				  <td width="20%"><s:text name="garuda.cdrcbuview.label.id_on_bag" />:</td>
			      <td width="30%"><s:textfield name="cdrCbuPojo.numberOnCbuBag" id="numberOnCbuBag" cssClass="idsMandatory" readonly="true"></s:textfield></td>
			      <td width="50%" colspan="2"></td>
			   </tr>
			   <tr>
			   <s:if test='cdrCbuPojo.historicLocalCbuId!=null && cdrCbuPojo.historicLocalCbuId!=""'>
				   <td width="20%"><s:text name="garuda.cdrcbuview.label.historic_cbu_lcl_id"/>:</td>
				   <td width="30%"><s:textfield name="cdrCbuPojo.historicLocalCbuId" readonly="true" onkeydown="cancelBack();" ></s:textfield>
				  </s:if>
				  <s:else>
				      <td width="50%" colspan="2"></td>
				  </s:else>
			   </tr>
			   <tr id="additionalsTR" style="display: none;">
			   <td colspan="4">
			   <fieldset><legend><s:text name="garuda.cordentry.label.additionalIds"/></legend>
			   <input type="hidden" id="sizess" value="<s:property value="#request.additionalIds.size"/>"></input>
		      <table>
		      <s:iterator value="#request.additionalIds" status="row" var="AdditionalIdsPojo">
		      	<tr align="left">
			           <td width="20%"><s:text name="garuda.cordentry.label.additionalIdDesc"></s:text>:</td>
					   <td width="25%"><input type="text" value="<s:property value="additionalIdDesc"/>" disabled="disabled"></td>
			           <td><s:text name="garuda.cordentry.label.additionalTypeId"></s:text>:</td>
					   <td>
					   <s:select list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES]" cssStyle="width:auto;" value="additionalIdType" listKey="pkCodeId" listValue="description" disabled="true"></s:select>
					   </td>
					   <td><s:text name="garuda.cordentry.label.additionalId"></s:text>:</td>
					   <td><input type="text" value="<s:property value="additionalId" />" disabled="disabled"></td>
			            <td></td>
				</tr>
				</s:iterator>	 
				</table>
				</fieldset>	 
			   </td>
			   </tr>			   	   
			</table>
			</div>	
		</div></fieldset>
		</s:form>