<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<script>
$j(document).ready(function(){
	callDate();
});
function callDate(){
	$j('#idmesign').show();
	getDatePic();
	checkMandatory();
	$j("#SYP_SUPP").show();
	//alert('<s:property value="esignFlag"/> ---for new');
	//if('<s:property value="esignFlag"/>'=="review"){
		//cbuIDM();
	//}
	for(i=6;i<19;i++){
		var id='testres_'+i;
			checkques(id);
		}
}
/*jQuery(function() {
	  jQuery('#bloodCDate').datepicker('option', {
	    beforeShow: customRange2
	  });
	});

	function customRange2(input) {
	  if (input.id == 'datepicker2') {
	    return {
	      minDate: jQuery('#datepicker1').datepicker("getDate")
	    };
	  } else if (input.id == 'datepicker1') {
	    return {
	      maxDate: jQuery('#datepicker2').datepicker("getDate")
	    };
	  }
	}*/
function showhelpmsg(id){
	msg=$j('#'+id+'_msg').html();
	overlib(msg,CAPTION,'Help');
	$j('#overDiv').css('z-index','99999');
}
function setidmQuesFlag(){
	if($j('#bloodCDate').val()=="" && $j("#subques1").val()=="" && $j("#subques2").val()==""){
		$j('#idmQuesFlag').val("0");
	}else{
		$j('#idmQuesFlag').val("1");
	}
}

function saveIdm(){
	var cordID=$j('#cordID').val();
	var data='cordID='+cordID;
	var flag=true;
	var noMandatory=true;
	$j('.idmMandatory').each(function(){
		var id=$j(this).parent().parent().attr('id');
		if($j('#'+id+':visible')){
			if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
				noMandatory=false;
				$j(this).parent().find('span').eq(0).show();
				flag=false;
			}else{
				$j(this).parent().find('span').eq(0).hide();
				$j(this).removeClass('IsMandatory');
			}
		}
	});	

	var url = "saveIdm?cdrCbuPojo.cordID="+cordID+"&noMandatory="+noMandatory;	
	var divname="idmtestform";
	if($j('#idmesignFlag').val()=="review"){
		divname="finalreviewmodel";
		}
	if(flag){
		refreshIdmDiv(url,divname,'idmForm');
	}else{
		$j('#esign').val("");
		$j('#cbuStatuspass').css("display","none");
	}
}
function checknull(){
/*	if($j('#subques1').val()=="" && $j('#subques2').val()==""){
		$j('.idmteststr').hide();
		$j('.idmtestdate').hide();
		$j('.idmtestres').each(function(){
			$j(this).val("");
		});
		$j("#SYP_SUPP").hide();
		$j(".assessDiv").hide();
	}else{
		$j('.idmteststr').show();
		$j("#SYP_SUPP").show();
		var i;
		for(i=6;i<19;i++){
		var id='testres_'+i;
			checkques(id);
		}
	}*/
}
function showassessment1(){
	var entityId=$j('#cordID').val();
	var subEntityId=$j('#pkforques1').val();
	var shname="subques1";
	var subEntitytype=$j('#subentityType').val();
	var responsestr=$j('#subques1 option:selected').text();
	var data='entityId='+entityId+'&subEntityId='+subEntityId+'&shrtName='+shname+'&subentitytype='+subEntitytype+'&questionNo=IDM Q#1&responsestr='+responsestr;
	refreshassessment('getassessment',"Assessment_Subques1",data);
	$j('#Assessment_Test1').show();
}
function showassessment2(){
	var entityId=$j('#cordID').val();
	var subEntityId=$j('#pkforques2').val();
	var shname="subques2";
	var subEntitytype=$j('#subentityType').val();
	var responsestr=$j('#subques2 option:selected').text();
	var data='entityId='+entityId+'&subEntityId='+subEntityId+'&shrtName='+shname+'&subentitytype='+subEntitytype+'&questionNo=IDM Q#2&responsestr='+responsestr;
	refreshassessment('getassessment',"Assessment_Subques2",data);
	$j('#Assessment_Test2').show();
}
function refreshIdmDiv(url,divname,formId){
	$j('#progress-indicator').css( 'display', 'block' );
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data:$j('#'+formId+' *').serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
            var $response=$j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).html(result);
            	$j('#IDMinformation').hide();
            	$j('#idmUpdate').show();
            	$j('#idmesign').hide();
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
	$j('#progress-indicator').css( 'display', 'none' );
}
function refreshassessment(url,divname,data){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data:data,
        success: function (result){
            var $response=$j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).html(result);
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
}
function checksubquestions1(){
	var nooftest=$j('#idmListSize').val();
	nooftest+=2;
	if($j('#subques1').val()!=null && $j('#subques1').val()!=""){
		if($j('#subques1').val()==$j('#idmsubquesnopk').val()){
			for(var i=3;i<=nooftest;i++){
				$j('#SubQus_'+i).hide();
				$j('#SubQusRes_'+i).val('0');
			}
			//alert("check --2");
			showassessment1();
		}else if($j('#subques1').val()==$j('#idmsubquesyespk').val()){
			for(var i=3;i<=nooftest;i++){
				$j('#SubQus_'+i).hide();
				$j('#SubQusRes_'+i).val('1');
			}
			$j('#Assessment_Test1').hide();
		}else{
			for(var i=3;i<=nooftest;i++){
				if($j('#testres_'+i).val()!=""){
					$j('#SubQus_'+i).show();
					$j('#SubQusRes_'+i).val("");
				}
			}
			$j('#Assessment_Test1').hide();
		}
		
	}else{
			for(var i=3;i<=nooftest;i++){
				$j('#SubQus_'+i).hide();
			}
			$j('#Assessment_Test1').hide();
		}
}
function checksubquestions2(){
	var nooftest=$j('#idmListSize').val();
	nooftest+=2;
	if($j('#subques2').val()!=null && $j('#subques2').val()!=""){
		if($j('#subques2').val()==$j('#idmsubquesnopk').val()){
			for(var i=3;i<=nooftest;i++){
				$j('#SubQus1_'+i).hide();
				$j('#SubQus1Res_'+i).val('0');
			}
			showassessment2();
		}else if($j('#subques2').val()==$j('#idmsubquesyespk').val()){
			for(var i=3;i<=nooftest;i++){
				$j('#SubQus1_'+i).hide();
				$j('#SubQus1Res_'+i).val('1');
			}
			$j('#Assessment_Test2').hide();
		}else{
			for(var i=3;i<=nooftest;i++){
				if($j('#testres_'+i).val()!=""){
					$j('#SubQus1_'+i).show();
					$j('#SubQus1Res_'+i).val('');
				}
			}
			$j('#Assessment_Test2').hide();
		}
	}else{
			for(var i=3;i<=nooftest;i++){
				$j('#SubQus1_'+i).hide();
			}
			$j('#Assessment_Test2').hide();
		}
}
function checktestresult(){
	$j('#IDMinformation select').each(function(index,el) {
		if($j(el).val()!=null && $j(el).val()!=""){
			if($j(el).val()==$j('#pkTestNotDone').val()){
				var fieldid=$j(el).attr('id');
				var idlen=fieldid.length;
				var idindex=fieldid.substr(8);
				$j('#Testdate_'+idindex).hide();
				$j('#SubQus_'+idindex).hide();
				$j('#SubQus1_'+idindex).hide();
			}
		}	
	});
}
function isNoteDone(notDoneVal,rowindex){
	var ind = rowindex;
	if(notDoneVal == $j('#pkTestNotDone').val() || notDoneVal==""){
		$j('#Testdate_'+ind).hide();
		$j('#testDate_'+ind).val("");
		$j('#testDate_'+ind).removeClass("idmMandatory");
		$j('#SubQusRes_'+ind).val("");
		$j('#SubQusRes_'+ind).removeClass("idmMandatory");
		$j('#SubQus1Res_'+ind).val("");
		$j('#SubQus1Res_'+ind).removeClass("idmMandatory");
		if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
			$j('#SubQus_'+ind).hide();
		}
		if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
			$j('#SubQus1_'+ind).hide();
		}
	}
	else{
		$j('#Testdate_'+ind).show();
		$j('#testDate_'+ind).addClass("idmMandatory");
		if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
			$j('#SubQus_'+ind).show();
			$j('#SubQusRes_'+ind).addClass("idmMandatory");
		}else{
			if($j('#subques1').val()==$j('#idmsubquesnopk').val()){
				$j('#SubQusRes_'+ind).val('0');
			}else{
				$j('#SubQusRes_'+ind).val('1');
			}
		}
		if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
			$j('#SubQus1_'+ind).show();
			$j('#SubQus1Res_'+ind).addClass("idmMandatory");
		}else{
			if($j('#subques2').val()==$j('#idmsubquesnopk').val()){
				$j('#SubQus1Res_'+ind).val('0');
			}else{
				$j('#SubQus1Res_'+ind).val('1');
			}
		}	
	}
}
function checkdate(obj,index){
	
	var parentObj=$j(obj).parent().parent();
	var nextRow=$j(parentObj).next();
	var val=$j(obj).val();
	if(val==0){
		$j(nextRow).hide();
		$j("#testDate_"+index).removeClass("idmMandatory");
	}else{
		$j(nextRow).show();
		$j("#testDate_"+index).addClass("idmMandatory");
	}
}

function checkques(id){
	if(id=='testres_6'){
		if($j('#testres_6').val()!=""){
		if($j('#testres_6').val()==$j("#pkTestNotDone").val()){
			$j('#Qus_7').show();
			$j('#testres_7').addClass("idmMandatory");
			$j('#testDate_7').addClass("idmMandatory");
			$j('#SubQusRes_7').addClass("idmMandatory");
			$j('#SubQus1Res_7').addClass("idmMandatory");
			$j('#Testdate_7').show();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus_7').show();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus1_7').show();
			}
		}else{
			$j('#testres_7').val("");
			$j('#testres_7').removeClass("idmMandatory");
			$j('#testDate_7').removeClass("idmMandatory");
			$j('#SubQusRes_7').removeClass("idmMandatory");
			$j('#SubQus1Res_7').removeClass("idmMandatory");
			$j('#Qus_7').hide();
			$j('#Testdate_7').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus_7').hide();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus1_7').hide();
			}

			}
		}else{
			$j('#Qus_7').hide();
			$j('#testres_7').val("");
			$j('#testres_7').removeClass("idmMandatory");
			$j('#testDate_7').removeClass("idmMandatory");
			$j('#SubQusRes_7').removeClass("idmMandatory");
			$j('#SubQus1Res_7').removeClass("idmMandatory");
			$j('#Testdate_7').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus_7').hide();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus1_7').hide();
			}
		}
	}
	if(id=='testres_10'){
		if($j('#testres_10').val()!=""){
		if($j('#testres_10').val()==$j("#pkTestNotDone").val()){
			//$j('#Qus_7').show();
		//	$j('#Testdate_7').show();
		//	$j('#testres_7').addClass("idmMandatory");
			//$j('#testDate_7').addClass("idmMandatory");
			//$j('#SubQusRes_7').addClass("idmMandatory");
			//$j('#SubQus1Res_7').addClass("idmMandatory");
			$j('#Qus_11').show();
			$j('#testres_11').addClass("idmMandatory");
			$j('#testDate_11').addClass("idmMandatory");
			$j('#SubQusRes_11').addClass("idmMandatory");
			$j('#SubQus1Res_11').addClass("idmMandatory");
			$j('#Testdate_11').show();
			$j('#Qus_13').show();
			$j('#Testdate_13').show();
			$j('#testres_13').addClass("idmMandatory");
			$j('#testDate_13').addClass("idmMandatory");
			$j('#SubQusRes_13').addClass("idmMandatory");
			$j('#SubQus1Res_13').addClass("idmMandatory");
			$j('#Qus_14').show();
			$j('#Testdate_14').show();
			$j('#testres_14').addClass("idmMandatory");
			$j('#testDate_14').addClass("idmMandatory");
			$j('#SubQusRes_14').addClass("idmMandatory");
			$j('#SubQus1Res_14').addClass("idmMandatory");
			
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
			//	$j('#SubQus_7').show();
				$j('#SubQus_11').show();
				$j('#SubQus_13').show();	
				$j('#SubQus_14').show();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
			//	$j('#SubQus1_7').show();
				$j('#SubQus1_11').show();
				$j('#SubQus1_13').show();	
				$j('#SubQus1_14').show();
			}
		}else if($j('#testres_10').val()==$j('#idmposiPk').val() || $j('#testres_10').val()==$j('#idmnegiPk').val()){
			$j('#testres_11').val("");
			$j('#testres_11').removeClass("idmMandatory");
			$j('#testDate_11').removeClass("idmMandatory");
			$j('#SubQusRes_11').removeClass("idmMandatory");
			$j('#SubQus1Res_11').removeClass("idmMandatory");
			$j('#testres_13').val("");
			$j('#testres_13').removeClass("idmMandatory");
			$j('#testDate_13').removeClass("idmMandatory");
			$j('#SubQusRes_13').removeClass("idmMandatory");
			$j('#SubQus1Res_13').removeClass("idmMandatory");
			$j('#testres_14').val("");
			$j('#testres_14').removeClass("idmMandatory");
			$j('#testDate_14').removeClass("idmMandatory");
			$j('#SubQusRes_14').removeClass("idmMandatory");
			$j('#SubQus1Res_14').removeClass("idmMandatory");
			
			$j('#Qus_11').hide();
			$j('#Testdate_11').hide();
			$j('#Qus_13').hide();
			$j('#Testdate_13').hide();
			$j('#Qus_14').hide();
			$j('#Testdate_14').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				//$j('#SubQus_7').hide();
				$j('#SubQus_11').hide();
				$j('#SubQus_13').hide();
				$j('#SubQus_14').hide();	
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				//$j('#SubQus1_7').hide();
				$j('#SubQus1_11').hide();
				$j('#SubQus1_13').hide();
				$j('#SubQus1_14').hide();	
			}			
		}
		}else{
			$j('#testres_11').val("");
			$j('#testres_11').removeClass("idmMandatory");
			$j('#testDate_11').removeClass("idmMandatory");
			$j('#SubQusRes_11').removeClass("idmMandatory");
			$j('#SubQus1Res_11').removeClass("idmMandatory");
			$j('#testres_13').val("");
			$j('#testres_13').removeClass("idmMandatory");
			$j('#testDate_13').removeClass("idmMandatory");
			$j('#SubQusRes_13').removeClass("idmMandatory");
			$j('#SubQus1Res_13').removeClass("idmMandatory");
			$j('#testres_14').val("");
			$j('#testres_14').removeClass("idmMandatory");
			$j('#testDate_14').removeClass("idmMandatory");
			$j('#SubQusRes_14').removeClass("idmMandatory");
			$j('#SubQus1Res_14').removeClass("idmMandatory");
			//$j('#Qus_7').hide();
			//$j('#Testdate_7').hide();
			$j('#Qus_11').hide();
			$j('#Testdate_11').hide();
			$j('#Qus_13').hide();
			$j('#Testdate_13').hide();
			$j('#Qus_14').hide();
			$j('#Testdate_14').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
			//	$j('#SubQus_7').hide();
				$j('#SubQus_11').hide();
				$j('#SubQus_13').hide();	
				$j('#SubQus_14').hide();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				//$j('#SubQus1_7').hide();
				$j('#SubQus1_11').hide();
				$j('#SubQus1_13').hide();	
				$j('#SubQus1_14').hide();
			}
		}
	}
	if(id=='testres_10' || id=='testres_11'){
		if($j('#testres_10').val()!="" || $j('#testres_11').val()!=""){
			if($j('#testres_10').val()==$j("#pkTestNotDone").val() && $j('#testres_11').val()==$j("#pkTestNotDone").val()){
				$j('#Qus_12').show();
				$j('#Testdate_12').show();
				$j('#testres_12').addClass("idmMandatory");
				$j('#testDate_12').addClass("idmMandatory");
				$j('#SubQusRes_12').addClass("idmMandatory");
				$j('#SubQus1Res_12').addClass("idmMandatory");
				if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
					$j('#SubQus_12').show();
				}
				if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
					$j('#SubQus1_12').show();
				}
			}else{
				$j('#testres_12').val("");
				$j('#testres_12').removeClass("idmMandatory");
				$j('#testDate_12').removeClass("idmMandatory");
				$j('#SubQusRes_12').removeClass("idmMandatory");
				$j('#SubQus1Res_12').removeClass("idmMandatory");
				$j('#Qus_12').hide();
				$j('#Testdate_12').hide();
				if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
					$j('#SubQus_12').hide();
				}
				if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
					$j('#SubQus1_12').hide();
				}
			}
		}else{
			$j('#Qus_12').hide();
			$j('#testres_12').val("");
			$j('#testres_12').removeClass("idmMandatory");
			$j('#testDate_12').removeClass("idmMandatory");
			$j('#SubQusRes_12').removeClass("idmMandatory");
			$j('#SubQus1Res_12').removeClass("idmMandatory");
			$j('#Testdate_12').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus_12').hide();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus1_12').hide();
			}
		}
	}
	if(id=='testres_15'){
		if($j('testres_15').val()!=""){
		if($j('#testres_15').val()==$j("#idmposiPk").val()){
			$j('#Qus_18').show();
			$j('#testres_18').addClass("idmMandatory");
			$j('#testDate_18').addClass("idmMandatory");
			$j('#SubQusRes_18').addClass("idmMandatory");
			$j('#SubQus1Res_18').addClass("idmMandatory");
			$j('#Testdate_18').show();
			$j('#Qus_19').show();
			$j('#Testdate_19').show();
			$j('#testres_19').addClass("idmMandatory");
			$j('#testDate_19').addClass("idmMandatory");
			$j('#SubQusRes_19').addClass("idmMandatory");
			$j('#SubQus1Res_19').addClass("idmMandatory");
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus_18').show();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus1_18').show();
			}
		}else{
			$j('#testres_18').val("");
			$j('#testres_18').removeClass("idmMandatory");
			$j('#testDate_18').removeClass("idmMandatory");
			$j('#SubQusRes_18').removeClass("idmMandatory");
			$j('#SubQus1Res_18').removeClass("idmMandatory");
			$j('#testres_19').val("");
			$j('#testres_19').removeClass("idmMandatory");
			$j('#testDate_19').removeClass("idmMandatory");
			$j('#SubQusRes_19').removeClass("idmMandatory");
			$j('#SubQus1Res_19').removeClass("idmMandatory");
			$j('#Qus_18').hide();
			$j('#Testdate_18').hide();
			$j('#Qus_19').hide();
			$j('#Testdate_19').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus_18').hide();
				$j('#SubQus_19').hide();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus1_18').hide();
				$j('#SubQus1_19').hide();
			}
		}
		}else{
			$j('#testres_18').val("");
			$j('#testres_18').removeClass("idmMandatory");
			$j('#testDate_18').removeClass("idmMandatory");
			$j('#SubQusRes_18').removeClass("idmMandatory");
			$j('#SubQus1Res_18').removeClass("idmMandatory");
			$j('#testres_19').val("");
			$j('#testres_19').removeClass("idmMandatory");
			$j('#testDate_19').removeClass("idmMandatory");
			$j('#SubQusRes_19').removeClass("idmMandatory");
			$j('#SubQus1Res_19').removeClass("idmMandatory");
			$j('#Qus_18').hide();
			$j('#Testdate_18').hide();
			$j('#Qus_19').hide();
			$j('#Testdate_19').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus_18').hide();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus1_18').hide();
			}
		}
	}
	if(id=='testres_18'){
		if($j('testres_18').val()!=""){
		if($j('#testres_18').val()==$j("#idmnegiPk").val()){
			$j('#Qus_19').show();
			$j('#Testdate_19').show();
			$j('#testres_19').addClass("idmMandatory");
			$j('#testDate_19').addClass("idmMandatory");
			$j('#SubQusRes_19').addClass("idmMandatory");
			$j('#SubQus1Res_19').addClass("idmMandatory");
			
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus_19').show();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus1_19').show();
			}
		}else{
			$j('#testres_19').val("");
			$j('#testres_19').removeClass("idmMandatory");
			$j('#testDate_19').removeClass("idmMandatory");
			$j('#SubQusRes_19').removeClass("idmMandatory");
			$j('#SubQus1Res_19').removeClass("idmMandatory");
			$j('#Qus_19').hide();
			$j('#Testdate_19').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus_19').hide();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus1_19').hide();
			}
		}
		}else{
			$j('#Qus_19').hide();
			$j('#testres_19').val("");
			$j('#testres_19').removeClass("idmMandatory");
			$j('#testDate_19').removeClass("idmMandatory");
			$j('#SubQusRes_19').removeClass("idmMandatory");
			$j('#SubQus1Res_19').removeClass("idmMandatory");
			$j('#Testdate_19').hide();
			
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus_19').hide();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#SubQus1_19').hide();
			}
		}
	}
//	checktestresult();
}
function commenttoggle(id,val){
	var pos=$j("#"+id).position()
	var left=pos.left;
	//var top=pos.top-50;
	$j('#content_'+val).css({left:left});
	$j('#content_'+val).show();
}
function hidecomment(val){
	$j('#content_'+val).hide();
}
function checkassessment(tname,id,val){
	//alert("check method");
	var questionNo ="IDM Q#"+tname;
	tname = $j('#shrtName'+tname).val();
	var responsestr=$j('#'+id+' option:selected').text();
	var data='shrtName='+tname+'&questionNo='+questionNo+'&responsestr='+responsestr;
	var divid=tname+"_assessmentTD";
	
	//alert(data);
	//alert("tname : "+tname);
		if(tname=='ANHB'){
		if(val==$j('#idmposiPk').val()||val==$j('#pkTestNotDone').val()){
			refreshassessment('getassessment',divid,data);
			$j('#'+tname+'_assessment').show();
		}else{
			$j('#'+tname+'_assessment').hide();
		}
	}

	//2nd Type Assessment
	
	if(tname=='ANHIV' ||tname=='ANHIV1' || tname=='ANHTLV' || tname=='NATH' ||tname=='HIVNAT' ||tname=='HIVP'||tname=='HCNAT'||tname=="HBNAT"||tname=="SYPH"||tname=="WNNAT"||tname=="CHA"||tname=='SYP_SUPP'){
		if(val==$j('#pkTestNotDone').val()){
			refreshassessment('getassessment',divid,data);
			$j('#'+tname+'_assessment').show();
		}else{
			$j('#'+tname+'_assessment').hide();
		}
	}

	//3rd Type Assessment
	if(tname=='FTA_ABS'){
		//alert("check");
		if(val==1){
			//alert("check - if");
			refreshassessment('getassessment',divid,data);
			$j('#'+tname+'_assessment').show();
		}else{
			//alert("check - else");
			$j('#'+tname+'_assessment').hide();
		}
	}
}

function addLabnewtest(){
	var fkSpecimenId=$j('#fkSpecimenId').val();
	//alert("fkSpecimenId : "+fkSpecimenId);
	if($j('#newTestSize').val()<6 || $j('#newTestSize').val()==6){
		showaddTest('Add LabTest','addLabnewtest?fkSpecimenId='+fkSpecimenId,'350','500','idmtestlist');
	}
}

function showaddTest(title,url,height,width,divId){
	var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/><img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	$j("#showaddtestmodal").html(progressMsg);
		$j("#showaddtestmodal").dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
		      		jQuery("#showaddtestmodal").dialog("destroy");
				    }
				   }
				  ); 
		$j("#showaddtestmodal").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
        data:$j('#'+divId+' *').serialize(),
        success: function (result, status, error){
	       	$j("#showaddtestmodal").html(result);
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
}

function showaddiasess(obj,shname,questionNo){
//	var nooftest=$j('#idmListSize').val();
	//nooftest+=2;
	//for(var i=3;i<nooftest;i++){
		id=$j(obj).attr('id');
		var val=$j(obj).val();
		var responsestr=$j('#'+id+' option:selected').text();
		var data='shrtName='+shname+'&questionNo=IDM Q#'+questionNo+"&responsestr="+responsestr;
		//alert(data);
		var divid=shname+"_assessmentTD";
	//	alert("Id : "+id);
		//alert("val : "+val);
			if(val==$j('#idmposiPk').val()||val==$j('#idmindeterPk').val()){
				refreshassessment('getassessment',divid,data);
				$j('#'+shname+'_assessment').show();
			}else{
				$j('#'+shname+'_assessment').hide();
			}
}
function setidmvalue(val){
	$j('#fttestsource').val(val);
}

function checkMandatory(){
	var i;
	if($j('#licenseid').val()==$j("#unLicensedPk").val()){
		$j('#fttestsource').addClass('idmMandatory');
		$j('#bloodCDate').addClass('idmMandatory');
		$j('#subques1').addClass('idmMandatory');
		$j('#subques2').addClass('idmMandatory');
		$j('.showmantry').show();
		for(i=3;i<20;i++){
			var testResponse='testres_'+i;
			var testDate='testDate_'+i;
			var subques1='SubQusRes_'+i;
			var subques2='SubQus1Res_'+i;
			var manditory='manditory_'+i;
			var testDateman='testDateman_'+i;
			var SubQus1man='SubQus1man_'+i;
			var SubQusman='SubQusman_'+i;
			$j('#'+manditory).show();
			if($j('#'+testResponse).val()==""){
				$j('#'+testResponse).addClass('idmMandatory');
			}else{
				$j('#'+testResponse).removeClass('idmMandatory');
				if($j('#'+testResponse).val()!=$j('#pkTestNotDone').val() || $j('#'+testResponse).val()!='0'){
					if($j('#'+testDate).val()==""){
						$j('#'+testDate).addClass('idmMandatory');
					}else{
						$j('#'+testDate).removeClass('idmMandatory');
					}
					if($j('#subques1').val()==$j('#idmsubquessomepk').val() || $j('#subques1').val()==""){
						if($j('#'+subques1).val()==""){
							$j('#'+subques1).addClass('idmMandatory');
						}else{
							$j('#'+subques1).removeClass('idmMandatory');
						}
					}else{
						$j('#'+subques1).removeClass('idmMandatory');
					}
					if($j('#subques2').val()==$j('#idmsubquessomepk').val() || $j('#subques2').val()==""){
						if($j('#'+subques2).val()==""){
							$j('#'+subques2).addClass('idmMandatory');
						}else{
							$j('#'+subques2).removeClass('idmMandatory');
						}
					}else{
						$j('#'+subques2).removeClass('idmMandatory');
					}
				}else{
					$j('#'+testDate).removeClass('idmMandatory');
					$j('#'+subques1).removeClass('idmMandatory');
					$j('#'+subques2).removeClass('idmMandatory');
				}
			}
		}
	}
}
function checkcommentnull(obj){
	var textareaObj=$j(obj).parent().find('textarea');
	if($j(textareaObj).val()!="" && $j(textareaObj).val()!=null){
		$j(obj).parent().find('span').hide();
		$j(obj).parent().parent().hide();
	}else{
		$j(obj).parent().find('span').show();
	}
}
</script>
<style>
#IDMinformation table{
	border: 1px solid #7dcfd5;
}
.IDMinformation table td{
	border-left: 1px solid #7dcfd5;
	border-right: 1px solid #7dcfd5;
}
.childquestions td{
	padding-left: 25px;
}
.commentbox{
	position: absolute;
	background: -moz-linear-gradient(center top , #FFFFFF, #EEEEEE) repeat scroll 0 0 #7dcfd5;
	z-index:9999;
	border: 1px solid #DBDAD9;
	padding: 2px;
}

</style>
<div id="idmForm">
		<s:hidden name="cdrCbuPojo.fkSpecimenId" id="fkSpecimenId"/>
		<s:hidden name="pkcordId" id="pkcordId" />
		<s:hidden name="max-min" id="max-min" value="1" />
		<s:hidden name="pkTestNotDone" id="pkTestNotDone" />
		<s:hidden name="esignFlag" id="idmesignFlag" />
		<s:hidden name="idmposiPk" id="idmposiPk" />
		<s:hidden name="idmnegiPk" id="idmnegiPk" />
		<s:hidden name="idmindeterPk" id="idmindeterPk" />
		<s:hidden name="idmsubquesnopk" id="idmsubquesnopk"/>
		<s:hidden name="idmsubquesyespk" id="idmsubquesyespk"/>
		<s:hidden name="idmsubquessomepk" id="idmsubquessomepk"/>
		<s:hidden name="cdrCbuPojo.idmcmsapprovedlab" id="idmcmsapprovedlab"/>
		<s:hidden name="cdrCbuPojo.idmfdalicensedlab" id="idmfdalicensedlab"/>
		<s:hidden name ="subentitytype" id="subentityType"></s:hidden>
		<s:hidden name ="idmTestcount" id="idmTestcount"></s:hidden>
		<s:hidden name="idmstatus" value="1" id="idmstatus"/>
		<s:hidden name="idmQuesFlag" id="idmQuesFlag" value="0"></s:hidden>
		<s:set name="subentityDsc" value="%{subentityType}" scope="request" ></s:set>
		<s:hidden name ="assess_ncd" id="assess_ncd" value="%{#request.assess_ncd}"/>
		<input type="hidden" name="idmListSize" id="idmListSize" value="<s:property value='idmTests1.size()'/>" />
		<div id="idmUpdate" style="display: none;" >
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr >
					<td colspan="2">
						<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
							<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
							<strong><s:text name='garuda.cordentry.label.idm'/>&nbsp;<s:text name="garuda.label.dynamicform.savemsg" /></strong></p>
						</div>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<input type="button" onclick="closeModals('dynaModalForm')" value="Close" />
					</td>
				</tr>
			</table>		
		</div>
		<div id="IDMinformation" class="IDMinformation">
			<table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" class="displaycdr" id="idmsearchResults" >
				<thead>
					<tr>
						<th scope="col"><s:text	name="Question #" /></th>
						<th scope="col"><s:text	name="Question" /></th>
						<th scope="col"><s:text name="garuda.label.dynamicform.response"/></th>
					</tr>
					<tr bgcolor="#E6E6E5">
					<td></td>
					<td>
						<s:text name="Source"></s:text>
						<span class="error showmantry" style="display:none">
							<s:text name="garuda.label.dynamicform.astrik"/>
						</span>
					</td>
					<td>
						<select id="fttestsource" name="idmpojo.fktestsource" onchange="setidmvalue(this.value);">
							<option value="">Select</option>
							<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES]">
							<s:if test="%{pkCodeId!=pkpatientID}">
								<option value='<s:property value='pkCodeId'/>'><s:text name="description"/></option>
							</s:if>
							</s:iterator>
						</select>
						<br/><span style="display:none" class="error"><s:text name="garuda.label.dynamicform.errormsg"></s:text> </span>
					</td>
					</tr>
					<tr bgcolor="#E6E6E5">
					<td></td>
					<td>
						<s:text name="Blood Collection Date"></s:text>
						<span class="error showmantry" style="display:none">
							<s:text name="garuda.label.dynamicform.astrik"/>
						</span>
					</td>
					<td>
						<s:date	name="idmpojo.bloodcdate" id="bloodCDate" format="MMM dd, yyyy" />
						<s:textfield readonly="true" onkeydown="cancelBack();"  name="idmpojo.bloodcdatestr" cssClass="datePicWMaxDate"
							value="%{getText('collection.date',{idmpojo.bloodcdate})}" id="bloodCDate" 
							onchange="setidmQuesFlag();"/>
						<br/><span style="display:none" class="error"><s:text name="garuda.label.dynamicform.errormsg"></s:text> </span>
					</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td align="center"><s:text name="1"></s:text></td>
						<td>
						<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IDMTEST_QUES]">
							<s:if test="%{subType=='subques1'}">
								<img id="cmsapprovedlab" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
								<s:hidden name="pkCodeId" id="pkforques1"></s:hidden>
								<s:property value="%{description}"/>
								<span class="error showmantry" style="display:none">
									<s:text name="garuda.label.dynamicform.astrik"/>
								</span>
							</s:if>
						</s:iterator>
						</td>
						<td>
							<s:select  name="idmpojo.cmsapprovedlab" id="subques1" onchange="checksubquestions1();setidmQuesFlag();" 
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IDMTEST_SUBQUES]" 
								listKey="pkCodeId" listValue="description"  value="idmpojo.cmsapprovedlab" headerKey="" headerValue="Select"/>
							<br/><span style="display:none" class="error"><s:text name="garuda.label.dynamicform.errormsg"></s:text> </span>
						</td>
					</tr>
					<tr id="Assessment_Test1" style="display: none;">
						<td></td>
						<td id="Assessment_Subques1">
							
						</td>
						<td></td>
					</tr>
					<tr>
						<td align="center"><s:text name="2"></s:text></td>
						<td>
						<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IDMTEST_QUES]">
							<s:if test="%{subType=='subques2'}">
								<img id="fdalicensedlab" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
								<s:hidden name="pkCodeId" id="pkforques2"></s:hidden>
								<s:property value="%{description}"/>
								<span class="error showmantry" style="display:none">
									<s:text name="garuda.label.dynamicform.astrik"/>
								</span>
							</s:if>
						</s:iterator>
						</td>
						<td>
							<s:select  name="idmpojo.fdalicensedlab" id="subques2" onchange="checksubquestions2();setidmQuesFlag();"
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IDMTEST_SUBQUES]" 
								listKey="pkCodeId" listValue="description"  value="idmpojo.fdalicensedlab" headerKey="" headerValue="Select"/>
							<br/><span style="display:none" class="error"><s:text name="garuda.label.dynamicform.errormsg"></s:text> </span>
						</td>
					</tr>
					<tr id="Assessment_Test2" style="display: none;">
						<td></td>
						<td id="Assessment_Subques2">
							
						</td>
						<td></td>
					</tr>
					<tr class="idmteststr" >
						<td colspan="3" style="background:#E6E6E5;font-weight: bold; padding: 10px;">
							<s:text name="garuda.idm.label.screeningtests"/>
						</td>
					</tr>
					<s:iterator value="idmTests1" var="idmtestpojo" status="row">
					<s:hidden name="fkidmgroup" value="%{fklabgroup}"></s:hidden>
					<s:set value="%{#row.index+3}" name="indexval" scope="request" ></s:set>
						<s:if test="%{fklabgroup!=pktestgrp}">
							<tr style="display: none;" id="<s:property value='%{shrtName}'/>">
								<td colspan="3" style="background:#E6E6E5;font-weight: bold; padding: 10px;">
									<s:text name="garuda.idm.label.confirmatorytest"/>
								</td>
							</tr>
							<tr id="Qus_<s:property value='%{#request.indexval}'/>" class="idmteststr"> 
								<td width="5%" align="center"><s:property value='%{#request.indexval}'/></td>
								<td width="75%">
									<s:hidden name="patLabs[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" />
									<s:hidden name="patLabs[%{#row.index}].fktestid" value="%{fktestid}" id="%{shrtName}_fktestid"/>
									<s:hidden name="patLabs[%{#row.index}].fktestgroup" value="%{fklabgroup}" />
									<s:hidden name="patLabs[%{#row.index}].testcount" value="%{idmTestcount+1}" />
									
									<img id="<s:property value='%{shrtName}'/>" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
									<span><b><s:property value="labtestname" /></b></span>
									<span class="error" id="manditory_<s:property value='%{#request.indexval}'/>" style="display:none">
										<s:text name="garuda.label.dynamicform.astrik"/>
									</span>
									&nbsp;&nbsp;
									<a href="#" id="testcomment_<s:property value='%{#request.indexval}'/>" onclick="commenttoggle(this.id,<s:property value='%{#request.indexval}'/>);" style="text-decoration: none;white-space: nowrap;">
									<img height="15px" src="./images/addcomment.png">
									<s:text name="garuda.label.dynamicform.comment"></s:text> </a>&nbsp;&nbsp;
									<div id="content_<s:property value='%{#request.indexval}'/>" class="commentbox" style="display: none;">
									<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
									<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
									<s:text name="Comment Box" />
									</div>									
										<s:textarea name="patLabs[%{#row.index}].testcomments" cols="20" rows="5" value="%{testcomments}"></s:textarea>
										<br/>
										<span class="error" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"></s:text> </span><br/>
										<input type="button" id="savebutton_<s:property value='%{#request.indexval}'/>" value="<s:text name="garuda.common.save"/>"
												onclick='checkcommentnull(this);'/>&nbsp;&nbsp;
										<input type="button" onclick="hidecomment(<s:property value='%{#request.indexval}'/>)" id="button_"+<s:property value='%{#request.indexval}'/> value="<s:text name="garuda.common.close"/>"/>
									</div>
									</div>
									<table id="<s:property value='%{shrtName}'/>_assessment" style="display: none;border: none;">
									<tr>
										<td id="<s:property value='%{shrtName}'/>_assessmentTD" style="border: none;" class="assessDiv"></td>
									</tr>
									</table>
								</td>
								
								<td width="20%" id="row_<s:property value='%{#request.indexval}'/>" valign="top">
								<s:hidden name="shrtName" id="shrtName%{#request.indexval}"></s:hidden>
								<s:if test="%{shrtName!='FTA_ABS' && shrtName!='SYP_SUPP'}">
									<select name="patLabs[<s:property value='%{#row.index}'/>].fktestoutcome" id="testres_<s:property value='%{#request.indexval}'/>"
										  onchange="checkques(this.id);isNoteDone(this.value,'<s:property value='%{#request.indexval}'/>');checkassessment('<s:property value='%{#request.indexval}'/>',this.id,this.value);" 
										  class="idmtestres">
										<option value="">Select</option>
										<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_OUTCOMES]">
											<s:if test="subType!='indeter'">
												<option value='<s:property value='%{pkCodeId}'/>'> <s:text name="%{description}"></s:text> </option>
											</s:if>
										</s:iterator>
									</select>
									<br/><span style="display:none" class="error"><s:text name="garuda.label.dynamicform.errormsg"></s:text> </span>
								</s:if>
								<s:if test="%{shrtName=='SYP_SUPP'}">
									<s:select  name="patLabs[%{#row.index}].fktestoutcome" id="testres_%{#request.indexval}"
										list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_OUTCOMES]"
										listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select" value="fktestoutcome" 
										onchange="checkques(this.id);isNoteDone(this.value,%{#request.indexval});checkassessment('%{#request.indexval}',this.id,this.value);"
										cssClass="idmtestres"/>
										<br/><span style="display:none" class="error"><s:text name="garuda.label.dynamicform.errormsg"></s:text> </span>
								
								</s:if>
								<s:if test="%{shrtName=='FTA_ABS'}">
									<s:select  name="patLabs[%{#row.index}].fktestoutcome" 
										list="#{'0':'No','1':'Yes'}" headerKey="" headerValue="Select" value="fktestoutcome" 
										id="testres_%{#request.indexval}" onchange="checkdate(this,'%{#request.indexval}');checkassessment('%{#request.indexval}',this.id,this.value)"
										cssClass="idmtestres"/>
										<br/><span style="display:none" class="error"><s:text name="garuda.label.dynamicform.errormsg"></s:text> </span>
								</s:if>
								</td>
							</tr>
							<tr id="Testdate_<s:property value='%{#request.indexval}'/>"  style="display: none;" class="idmtestdate">
								<td align="center">
									<s:property value='%{#request.indexval}'/>a
								</td>
								<td style="padding-left: 30px">
									<s:text name="garuda.idm.label.testdate"/>
									<span class="error" id="testDateman_<s:property value='%{#request.indexval}'/>" style="display:none">
										<s:text name="garuda.label.dynamicform.astrik"/>
									</span>
								</td>
								<td>
									<s:date	name="testDate" id="testDate%{#row.index}" 
										format="MMM dd, yyyy" /> <s:textfield readonly="true" onkeydown="cancelBack();" 
										name="patLabs[%{#row.index}].testDateStr" cssClass="datePicWMaxDate"
										value="%{getText('collection.date',{testDate})}" id="testDate_%{#request.indexval}"/>
										<br/><span style="display:none" class="error"><s:text name="garuda.label.dynamicform.errormsg"></s:text> </span>
								</td>
							</tr>
							<s:if test="%{shrtName!='FTA_ABS'}">
							<tr id="SubQus_<s:property value='%{#request.indexval}'/>" style="display: none;">
								<td align="center">
									<s:property value='%{#request.indexval}'/>b
								</td>
								<td style="padding-left: 30px">
									<s:text name="garuda.idm.label.cmsapprovedlab"/>
									<span class="error" id="SubQusman_<s:property value='%{#request.indexval}'/>" style="display:none">
										<s:text name="garuda.label.dynamicform.astrik"/>
									</span>
								</td>
								<td>
									<s:select  name="patLabs[%{#row.index}].cmsapprovedlab" 
										list="#{'0':'No','1':'Yes'}" value="cmsapprovedlab" headerKey="" 
										headerValue="Select" id="SubQusRes_%{#request.indexval}"/>
										<br/><span style="display:none" class="error"><s:text name="garuda.label.dynamicform.errormsg"></s:text> </span>
								</td>
							</tr> 
							<s:if test="%{shrtName!='SYP_SUPP'}">
							<tr id="SubQus1_<s:property value='%{#request.indexval}'/>" style="display: none;">
								<td align="center">
									<s:property value='%{#request.indexval}'/>c
								</td>
								<td style="padding-left: 30px">
									<s:text name="garuda.idm.label.fdalicensedlab"/>
									<span class="error" id="SubQus1man_<s:property value='%{#request.indexval}'/>" style="display:none">
										<s:text name="garuda.label.dynamicform.astrik"/>
									</span>
								</td>
								<td>
									<s:select  name="patLabs[%{#row.index}].fdalicensedlab" 
										list="#{'0':'No','1':'Yes'}" value="fdalicensedlab" headerKey=""
										headerValue="Select" id="SubQus1Res_%{#request.indexval}"/>
										<br/><span style="display:none" class="error"><s:text name="garuda.label.dynamicform.errormsg"></s:text> </span>
								</td>
							</tr>
							</s:if>
						</s:if>
					</s:if>
					</s:iterator>
					<tr class="idmteststr">
						<td colspan="3" style="background:#E6E6E5;font-weight: bold; padding: 10px;">
							<s:text name="Additional / Miscellaneous IDM Tests"/>
						</td>
					</tr>
					
					<tr class="idmteststr">
					<td colspan="3" style="padding: 0px" id="addittest">
					<table  style="border: none;">
					<s:hidden id="newTestSize" value="%{idmnewTest.size()}"/>
					<tr style="display: none;">
					<td>
					<div id="idmtestlist">
						<s:if test="%{idmnewTest!=null && idmnewTest.size()>0}">
							<s:iterator value="idmnewTest" var="newidm" >
								<s:hidden name="labtestlist1" value="%{newidm}"></s:hidden>
							</s:iterator>	
						</s:if>
					</div>
					</td>
					</tr>
						<s:iterator value="idmtestlist1" var="pojo" status="row">
							<s:set value="%{#row.index+20}" name="indexval1" scope="request" ></s:set>
							<s:set value="%{#row.index+17}" name="rowstatus" scope="request" ></s:set>
							<tr id="Qus_<s:property value='%{#request.indexval1}'/>"> 
								<td width="6.5%" align="center" style="border-left: none;">
									<s:property value="%{#request.indexval1}"/>
								</td>
								<td width="73.5%">
									<s:hidden name="patLabs[%{#request.rowstatus}].pkpatlabs" value="%{pkpatlabs}" />
									<s:hidden name="patLabs[%{#request.rowstatus}].fktestid" value="%{pkLabtest}" id="%{shrtName}_fktestid"/>
									<s:hidden name="patLabs[%{#request.rowstatus}].fktestgroup" value="%{fktestgroup}" />
									<s:hidden name="patLabs[%{#request.rowstatus}].testcount" value="%{idmTestcount+1}" />
									
									<span><b><s:property value="labtestName" /></b></span>
									<span class="error" id="manditory_<s:property value='%{#request.indexval1}'/>" style="display:none">
										<s:text name="garuda.label.dynamicform.astrik"/>
									</span>
									&nbsp;&nbsp;
									<a href="#" id="testcomment_<s:property value='%{#request.indexval1}'/>" onclick="commenttoggle(this.id,<s:property value='%{#request.indexval1}'/>);" style="text-decoration: none;white-space: nowrap;">
									<img height="15px" src="./images/addcomment.png">
									<s:text name="garuda.label.dynamicform.comment"></s:text> </a>&nbsp;&nbsp;
									<div id="content_<s:property value='%{#request.indexval1}'/>" class="commentbox"  style="display: none;">
									<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
									<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
									<s:text name="Comment Box" />
									</div>									
										<s:textarea name="patLabs[%{#request.rowstatus}].testcomments" cols="20" rows="5" ></s:textarea>
										<br/>
										<span class="error" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"></s:text> </span><br/>
										<input type="button" id="savebutton_<s:property value='%{#request.indexval1}'/>" value="<s:text name="garuda.common.save"/>"
												onclick='checkcommentnull(this);'/>&nbsp;&nbsp;
										<input type="button" onclick="hidecomment(<s:property value='%{#request.indexval1}'/>)" id="button_<s:property value='%{#request.indexval1}'/>" value="<s:text name="garuda.common.close"/>"/>
									</div>
									</div>
								</td>
								
								<td width="20%" id="row_<s:property value='%{#request.indexval1}'/>" style="border-right: none;">
								<s:hidden name="shrtName" id="shrtName%{#request.indexval1}"></s:hidden>
									<s:select  name="patLabs[%{#request.rowstatus}].fktestoutcome" id="testres_%{#request.indexval1}" 
										list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_OUTCOMES]"
										listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select" 
										onchange="isNoteDone(this.value,%{#request.indexval1});showaddiasess(this,'%{shrtName}','%{#request.indexval1}');" 
										cssClass="idmtestres"/>
								</td>
							</tr>
							<tr id="<s:property value='%{shrtName}'/>_assessment" style="display: none;">
								<td><s:set name="shrtName" value="%{shrtName}" ></s:set> </td>
								<td id="<s:property value='%{shrtName}'/>_assessmentTD" class="assessDiv">
									
								</td>
								<td></td>
							</tr>
							<tr id="Testdate_<s:property value='%{#request.indexval1}'/>" style="display: none;" class="idmtestdate">
								<td align="center" style="border-left: none;">
									<s:property value='%{#request.indexval1}'/>a
								</td>
								<td style="padding-left: 30px">
									<s:text name="garuda.idm.label.testdate"/>
									<span class="error" id="testDateman_<s:property value='%{#request.indexval1}'/>" style="display:none">
										<s:text name="garuda.label.dynamicform.astrik"/>
									</span>
								</td>
								<td style="border-right: none;">
									<s:textfield readonly="true" onkeydown="cancelBack();"  name="patLabs[%{#request.rowstatus}].testDateStr" cssClass="datePicWMaxDate"
										id="testDate_%{#request.indexval1}"/>
								</td>
							</tr>
					</s:iterator>
					</table>
					<br/>
					<s:if test="%{idmnewTest.size()<6 || idmnewTest.size()=6 }">
						<button type="button" onclick="addLabnewtest();"><s:text name="garuda.label.dynamicform.addtest"></s:text></button>
					</s:if>
					</td>
					</tr>
			</tbody>
		</table>
	</div>
<!--<table>
 <tr>
	<td>
		<button type="button" onclick="saveIdm();"><s:text name="Save"></s:text> </button>
	</td>
</tr>

</table>-->
<div id="helpmsg" style="display: none;">
	<div id=HB_msg>
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_react_positive_def"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_nodone_def"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_HB_msg"/></td></tr>
   		</table>
	</div>
   		<div id=ANHB_msg>
   			<table>
   				<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_ANHB_msg1"/></td></tr>  
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_ANHB_msg2"/></td></tr>
   				<tr></tr>
   				<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act_completedtest"/></td></tr>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_incom_eligibility_status"/></td></tr>
   			</table>
   		</div>
   		<div id=ANHCV_msg>
   			   		<table>
	   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
	   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_react_positive_def"/></td></tr>
	   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
	   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_nodone_def"/></td></tr>
	   			<tr></tr>
	   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
	   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_ANHCV_msg"/></td></tr>
	   		</table>
   		</div>
   		<div id="ANHIV_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_react_positive_def"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_ANHIV_msg1"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_ANHIV_msg2"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act_completedtest"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_ANHIV_msg3"/></td></tr>
   		</table>
   		</div>
   		<div id="ANHIV1_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_react_positive_def"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_ANHIV1_msg1"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act_completedtest"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_ANHIV1_msg2"/></td></tr>
   		</table>  
   		</div>
   		<div id="ANHTLV_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_react_positive_def"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_incom_collection_date"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act_completedtest"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_incom_eligibility_statusFDA"/></td></tr>
   		</table>
   		</div>
   		<div id="ANCMV_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_ANCMV_msg1"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act_completedtest"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_ANCMV_msg2"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_ANCMV_msg3"/></td></tr>
   		</table>
   		</div>
   		<div id="NATH_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_react_positive_def"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_NATH_msg1"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_NATH_msg2"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act_completedtest"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_NATH_msg3"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_testfor_HIV-1NAT_HIVp24"/></td></tr>
   		</table>
   		</div>
   		<div id="HIVNAT_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_react_positive_def"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_HIVNAT_msg1"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_HIVNAT_msg2"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act_completedtest"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_testfor_HIV-1NAT_HIVp24"/></td></tr>
   		</table>
   		</div>
   		<div id="HIVP_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_react_positive_def"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_HIVP_msg1"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_nodone_def"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_HIVP_msg2"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_react_positivetests"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_testfor_HIV-1NAT_HIVp24"/></td></tr>
   		</table>
   		</div>
   		<div id="HCNAT_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_react_positive_def"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_incom_collection_date"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_HCNAT_msg1"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_react_positivetests"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_HCNAT_msg2"/></td></tr>
   		</table>
   		</div>
   		<div id="HBNAT_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_react_positive_def"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_HBNAT_msg1"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_HBNAT_msg2"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_react_positivetests"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_HBNAT_msg3"/></td></tr>
   		</table>
   		</div>
   		<div id="SYPH_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_SYPH_msg1"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_incomplete_collectiondate"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_react_positivetests"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_SYPH_msg2"/></td></tr>
   		</table>
   		</div>
   		<div id="WNNAT_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_react_positive_def"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_incomplete_collectiondate"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_react_positivetests"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_incom_eligibility_statusFDA"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_WNNAT_msg"/></td></tr>
   		</table>
   		</div>
   		<div id="CHA_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_react_positive_def"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_CHA_msg1"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_react_positivetests"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_incom_eligibility_statusFDA"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_CHA_msg2"/></td></tr>
   		</table>
   		</div>
   		<div id="SYP_SUPP_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_react_positive_def"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_noreact_neg_noact"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_SYP_SUPP_msg1"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_nodone_def"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_react_positivetests"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_SYP_SUPP_msg2"/></td></tr>
   		</table>
   		</div>
   		<div id="FTA_ABS_msg">
   		<table>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_FTA_ABS_msg1"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_FTA_ABS_msg2"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_FTA_ABS_msg3"/></td></tr>
   			<tr></tr>
   			<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   			<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_FTA_ABS_msg4"/></td></tr>
   		</table>
   		</div>
   		<div id="fdalicensedlab_msg">
   			<table>
   				<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_yes_noaction"/></td></tr>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_incomplete_constraint"/></td></tr>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_collectiondate_constraint"/></td></tr>
   				<tr></tr>
   				<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act_completedtest"/></td>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_incom_eligibility_status"/></td></tr>
   			</table>
   		</div>
   		<div id="cmsapprovedlab_msg">
   			<table>
   				<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act"/></u></td></tr>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_yes_noaction"/></td></tr>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_incomplete_constraint"/></td></tr>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_collectiondate_constraint"/></td></tr>
   				<tr></tr>
   				<tr><td><u><s:text name="garuda.cbu.idmtest.helpmsg_hist_eval"/></u></td></tr>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_curr_eval_act_completedtest"/></td></tr>
   				<tr><td><s:text name="garuda.cbu.idmtest.helpmsg_incom_eligibility_status"/></td></tr>
   			</table>
   		</div>	
   	</div>
</div>
<div id="showaddtestmodal">
</div>