<%@ taglib prefix="s"  uri="/struts-tags"%>
<s:if test="#request.moduleEntityEventCode=='View CBU Information From Complete Required Informaion'">
<jsp:include page="./cb_track_session_logging.jsp" />
</s:if>
<s:set name="tempVal" value="cbuFinalReviewPojo.finalReviewConfirmStatus"></s:set>
<div>

			<!--<div id="cbusinfoscontent" onclick="toggleDiv('cbusinfos')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cordentry.label.cbuinfo" /></div>-->
			<div id="cbusinfos">
			    <table cellpadding="0" cellspacing="0" width="100%">
			      <tr align="left">
				       <td width="20%" style="padding-left: 5px;"><s:text name="garuda.cbuentry.label.CBB" />:</td>
				       <td width="30%" style="padding-left: 5px;">
				          <s:if test="#request.sites!=null">		
				          	 <s:textfield name="cdrCbuPojo.site.siteIdentifier" readonly="true" onkeydown="cancelBack();"  disabled="true"></s:textfield>			   
						  </s:if>
					   </td>
					   <td width="20%" style="padding-left: 5px;"><s:text name="garuda.cbudefaults.label.storage.location"></s:text>:</td>
					   <td width="30%" style="padding-left: 5px;">
						    <s:if test="cbuStrLocationList!=null">
						        <s:select list="cbuStrLocationList" disabled="true"  headerKey="-1" headerValue="Select" name="cdrCbuPojo.fkCBUStorageLocation" listKey="siteId"  listValue="siteName"> </s:select>
						    </s:if>
						    <s:else>
						        <s:textfield readonly="true" onkeydown="cancelBack();"  name="cdrCbuPojo.fkCBUStorageLocation"></s:textfield>
						    </s:else>   
					  </td>					   
				   </tr>
				   <tr>
				     <td width="20%" ><s:text name="garuda.cbuentry.label.cbbname" />:</td>
		             <td width="30%" style="padding-left: 5px;"><s:textfield name="cdrCbuPojo.site.siteName" readonly="true" onkeydown="cancelBack();"  disabled="true"/></td>
		             <td width="20%" style="padding-left: 5px;"><s:text name="garuda.cbudefaults.label.collection.site"></s:text>:</td>
					 <td width="30%" style="padding-left: 5px;">
					    <s:if test="cbuCollectionList!=null">
					        <s:select list="cbuCollectionList" disabled="true"  name="cdrCbuPojo.fkCBUCollectionSite"  headerKey="-1" headerValue="Select" listKey="siteId"  listValue="siteName"> </s:select>
					    </s:if>  
					    <s:else>
					        <s:textfield readonly="true" onkeydown="cancelBack();"  name="cdrCbuPojo.fkCBUCollectionSite"></s:textfield>
					    </s:else>
				     </td>
				   </tr>
			    </table>
			</div>
			<div id="<s:property value="esignFlag"/>cbufundinginfocontent" onclick="toggleDiv('<s:property value="esignFlag"/>cbufundinginfo');"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span><s:text
				name="garuda.cordentry.label.cbufunding" /></div>
			<div id="<s:property value="esignFlag"/>cbufundinginfo">
			    <table cellpadding="0" cellspacing="0" width="100%">
			       <tr align="left">
					  <td width="20%;" ><s:text name="garuda.cordentry.label.fundingrequested" />:</td>
					  <td width="30%;" style="padding-left: 5px;">
					  	<s:select id="fkFundCateg" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNDING_CATEGORY]" name="cdrCbuPojo.fkFundCateg" disabled="true" listKey="pkCodeId"  listValue="description" headerKey="-1" headerValue="Select"></s:select>
					  </td>
				      <td width="20%;" style="padding-left: 5px;"><s:text name="garuda.cordentry.label.fundedcbu" /></td>
					<td width="30%;" style="padding-left: 5px;"><strong><s:if test="cdrCbuPojo.fundedCBU==true"><s:text name="garuda.clinicalnote.label.yes"/></s:if><s:elseif test="cdrCbuPojo.fundedCBU==false"><s:text name="garuda.clinicalnote.label.no"/></s:elseif></strong></td>
				  </tr>				 
			    </table>
			</div>
			<div id="<s:property value="esignFlag"/>cbudemographicscontent" onclick="toggleDiv('<s:property value="esignFlag"/>cbudemographics');"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span><s:text
				name="garuda.cordentry.label.cbudemographics" /></div>
			<div id="<s:property value="esignFlag"/>cbudemographics">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr align="left">
					<td width="20%;"><s:text name="garuda.cbuentry.label.gender" />:</td>
					<td width="30%" style="padding-left: 5px;">
					   <%--<s:set name="babyId" value="%{CdrCbuPojo.babyGenderId}" scope="request" />--%>
						<input type="text" disabled="disabled" class="cbuInfoMandatory" name="bab" value="<s:property value="getCodeListDesc(populatedData.genderIdList,CdrCbuPojo.babyGenderId)"/>" />  
				   </td>
					<td width="20%;" style="padding-left: 5px;"><s:text name="garuda.cbuentry.label.birthdate" />:</td>
					<td width="30%;" style="padding-left: 5px;"><s:date name="CdrCbuPojo.birthDate" id="datepicker"
						format="MMM dd, yyyy" /> <s:textfield readonly="true" onkeydown="cancelBack();" 
						class="txtarea75" name="CdrCbuPojo.birthDate" id="datepicker" disabled="true" value="%{datepicker}" /></td>
				</tr>
				<tr align="left">
					<%--<td><s:text name="garuda.cbuentry.label.cbulicensure" />:</td>
					<td style="padding-left: 0px;"><s:radio disabled="true"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS]"
						name="cdrCbuPojo.cbuLicStatus" listKey="pkCodeId"
						listValue="description"></s:radio></td>--%>
						 <td><s:text name="garuda.cbuentry.label.babybirthtime" />:</td>
			      <td style="padding-left: 5px;">
			      <s:textfield name="cdrCbuPojo.babyBirthTime" readonly="true" onkeydown="cancelBack();"  disabled="true"></s:textfield>(HH:MM 24 HR)
			      </td>	
			     	<td style="padding-left: 5px;"><s:text name="garuda.cbuentry.label.collectiondate" />:
					</td>
					<td style="padding-left: 5px;"><s:date name="CdrCbuPojo.specimen.specCollDate"
						id="datepicker1" format="MMM dd, yyyy" /> <s:textfield
						readonly="true" onkeydown="cancelBack();"  class="txtarea75" cssClass="cbuInfoMandatory"
						name="CdrCbuPojo.specimen.specCollDate"
						id="datepicker1" disabled="true" value="%{datepicker1}" /></td>
				</tr>
				<tr>
			     <td><s:text name="garuda.cordentry.label.ethnicity"/>:</td> 
				    <td style="padding-left: 5px;">				    
				        <s:set name="ethId" value="%{cdrCbuPojo.ethnicity}" scope="request" />
				        <s:textfield disabled="true" cssClass="cbuInfoMandatory" name="eth" value="%{getCodeListDescByPk(#request.ethId)}"></s:textfield>
					</td> 
			      <td style="padding-left: 5px;"><s:text name="garuda.cbuentry.label.collectiontime" />:</td>
			      <td style="padding-left: 5px;"><s:textfield name="cdrCbuPojo.cbuCollectionTime" readonly="true" onkeydown="cancelBack();"  disabled="true"></s:textfield>(HH:MM 24 HR) </td>
			      	      
			   </tr>
				<!--<tr align="left">
					<td><s:text name="garuda.cbuentry.label.abo" />:</td>
					<td><s:textfield readonly="true" onkeydown="cancelBack();"  name="CdrCbuPojo.bloodGpDes"
						readonly="true" onkeydown="cancelBack();"  /></td>
					<td><s:text name="garuda.cbuentry.label.rhtype" />:</td>
					<td><s:textfield readonly="true" onkeydown="cancelBack();"  name="CdrCbuPojo.rhTypeDes"
						readonly="true" onkeydown="cancelBack();"  /></td>
				</tr>
				<tr align="left">
					<td><s:text name="garuda.cbuentry.label.bcresult" />:</td>
					<td><s:select readonly="true" onkeydown="cancelBack();" 
						name="CdrCbuPojo.bacterialResult"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS]"
						listKey="pkCodeId" listValue="description" headerKey=""
						headerValue="Select" disabled="true" /></td>
					<td><s:text name="garuda.cbuentry.label.fcresult" />:</td>
					<td><s:select readonly="true" onkeydown="cancelBack();"  name="CdrCbuPojo.fungalResult"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS]"
						listKey="pkCodeId" listValue="description" headerKey=""	headerValue="Select" disabled="true" /></td>
				</tr>
				--><tr align="left">
				   <td><s:text name="garuda.cordentry.label.cbucolltype" />:</td>
				 <td style="padding-left: 5px;">
					<s:select disabled="true" cssClass="cbuInfoMandatory" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_COLLECTION_TYPE]" name="cdrCbuPojo.fkCBUCollectionType" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" ></s:select>
				 </td>				   
					<td style="padding-left: 5px;"><s:text name="garuda.cbuentry.label.race"/>:</td>
					<td class="cbuProcMandatoryTD" style="padding-left: 5px;">
					 <s:set name="raceCount" value="%{race.length}" scope="request" />
						<s:property value="getRaceDescOrderBy(cdrCbuPojo.cordID)"/>
					  <!--  <s:iterator value="#request.race" var="racev" status="stat">
					        <s:set name="rPkCodeId" value="%{#racev}" scope="request" />
					        <s:property value="getCodeListDesc(populatedData.raceList,#request.rPkCodeId)"/>
					         <s:if test="#stat.index < #request.raceCount-1"> ,</s:if>
				   	  </s:iterator>	
				   	   -->              
				   </td>
			   </tr>	
			   <tr align="left">			   
			    <td><s:text name="garuda.cordentry.label.multiplebirth" />:</td>
				 <td style="padding-left: 5px;">
						<s:select disabled="true" list="#{'1':'Yes','0':'No'}" cssClass="cbuInfoMandatory" name="cdrCbuPojo.fkMultipleBirth" headerKey="-1" headerValue="Select"></s:select>
				 </td> 
				  <td style="padding-left: 5px;">
					<s:text name="garuda.cordentry.label.cbudelivtype" />:</td>
				 <td style="padding-left: 5px;">
				    <s:select disabled="true" cssClass="cbuInfoMandatory" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_DELIV_TYPE]" name="cdrCbuPojo.fkCBUDeliveryType" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" ></s:select>
				 </td>									     
			   </tr>
			   <tr align="left">
			   <%--   <td width="10%">
			        <s:text name="garuda.cordentry.label.hrsabroadrace" />:</td>
				 <td style="padding-left: 5px;">
					<s:if test="cdrCbuPojo.hrsaRaceRollUp!=null">
				        <s:select disabled="true" name="cdrCbuPojo.hrsaRaceRollUp" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@RACE_ROLLUP]" listKey="pkCodeId" listValue="description" ></s:select>
				    </s:if>
				    <s:else>
				        <s:textfield name="cdrCbuPojo.hrsaRaceRollUp" readonly="true" onkeydown="cancelBack();"  disabled="true"></s:textfield>					
				    </s:else>
				 </td> --%>
				 <td><s:text name="garuda.cordentry.label.nmdpbroadrace" />:</td>
				 <td style="padding-left: 5px;">
				    <s:if test="cdrCbuPojo.nmdpRaceId!=null">
				        <s:select disabled="true" name="cdrCbuPojo.nmdpRaceId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_BROAD_RACE]" listKey="pkCodeId" listValue="description" ></s:select>
				    </s:if>
				    <s:else>
				        <s:textfield name="cdrCbuPojo.nmdpRaceId" readonly="true" onkeydown="cancelBack();"  disabled="true"></s:textfield>
				    </s:else>
				 </td>
			   </tr><%-- 
			   <s:if test="#request.additionalIds!=null && #request.additionalIds.size>0">
			      <s:set name="additionalId1" value="%{#request.additionalIds[0].additionalId}" scope="request" />			     
			   </s:if>
			   <s:iterator value="#request.additionalIds">
			     <tr align="left">
				     <td width="10%">				    
				       <s:text name="garuda.cordentry.label.additionalIds" />:				           			      
				     </td>
				     <td style="padding-left: 5px;">
				       <s:textfield name="additionalId" readonly="true" onkeydown="cancelBack();"  disabled="true"></s:textfield>
				      </td>
				     <td colspan="2"></td>
			     </tr>
			   </s:iterator>--%>			   
			</table>
			<table>
				<tr align="right">
					<td colspan="4">
					   <s:if test="hasEditPermission(#request.updateCBUInfowidget)==true && cdrCbuPojo.site.isCDRUser()==true">
					   
			      <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
    </s:if>
    <s:else>
   				 	   <input type="button"
						name="characteristicsEdit" value="<s:text name="garuda.common.lable.edit"/>"
						onclick="fn_showModalCbu('<s:text name="garuda.completeReqInfo.label.editCbuInfo"/> <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','updateCharacteristics?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CLINICAL_RECORD_CBU_INFO" />&licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','500','850');">
						</s:else>
						</s:if>
					</td>
				</tr>
			</table>
			</div>	
			</div>	
			<script>
			function fn_showModalCbu(title,url,hight,width){
				var patientId = $j('#patientId').val();
				var patient_data = '';
				if(patientId!="" && patientId != null && patientId != "undefined"){
					patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
				}
				title=title+patient_data;
				showModal(title,url,hight,width)
				}
			</script>