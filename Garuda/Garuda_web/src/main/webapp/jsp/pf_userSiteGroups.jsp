<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.eres.web.site.SiteJB" %>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.address.AddressJB" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%
	HttpSession tSession = request.getSession(true); 
	GrpRightsJB grpRights =null;
	if (tSession.getAttribute("LocalGRights")!=null){
	 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
	}else
	{
	 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	}
	int viewCTShipSample = 0;
	int viewCTSampleAtLab = 0;
	int viewHE = 0;
	int viewOR = 0;
	
	if(grpRights.getFtrRightsByValue("CB_CTSHIPSMPL")!=null && !grpRights.getFtrRightsByValue("CB_CTSHIPSMPL").equals(""))
	{
		viewCTShipSample = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CTSHIPSMPL"));
	}
	
	if(grpRights.getFtrRightsByValue("CB_CTSMPLATLAB")!=null && !grpRights.getFtrRightsByValue("CB_CTSMPLATLAB").equals(""))
	{
		viewCTSampleAtLab = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CTSMPLATLAB"));
	}
	
	if(grpRights.getFtrRightsByValue("CB_HEORDRED")!=null && !grpRights.getFtrRightsByValue("CB_HEORDRED").equals(""))
	{
		viewHE = Integer.parseInt(grpRights.getFtrRightsByValue("CB_HEORDRED"));
	}
	
	if(grpRights.getFtrRightsByValue("CB_ORORDERD")!=null && !grpRights.getFtrRightsByValue("CB_ORORDERD").equals(""))
	{
		viewOR = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ORORDERD"));
	}
	request.setAttribute("viewCTShipSample",viewCTShipSample);
	request.setAttribute("viewCTSampleAtLab",viewCTSampleAtLab);
	request.setAttribute("viewHE",viewHE);
	request.setAttribute("viewOR",viewOR);
%>
<script>
var viewctshipsample='<s:property value="hasViewPermission(#request.viewCTShipSample)"/>';
var viewctsampleatlab='<s:property value="hasViewPermission(#request.viewCTSampleAtLab)"/>';
var viewhe='<s:property value="hasViewPermission(#request.viewHE)"/>';
var viewor='<s:property value="hasViewPermission(#request.viewOR)"/>';

function loadRightsByGrpId(grpsId){
	var flag="123";
	var subtp = 'PROCESS_NOTE_CATEGORY_CODESUBTYPE';
	//alert(grpsId);		
	//alert($j("#cordIdentifier").val());
	//alert($j("#orderIdentifier").val());
	//alert($j("#orderType").val());
	//window.location="getUserRightForGrpForOrder?grpId="+grpsId+"&orderId="+$j("#orderIdentifier").val()+"&orderType="+$j("#orderType").val()+"&pkcordId="+$j("#cordIdentifier").val()+"&varflag="+flag+"&noteType="+subtp+"&divId="+$j("#orderPageDivId").val();
	
	var ordType='<s:property value="#request.orderType"/>';
	if (ordType=='CT - Ship Sample' && viewctshipsample=='true'){
			ordType='CT';
		submitpost('getUserRightForGrpForOrder',{'grpId':grpsId,'orderId':$j("#orderIdentifier").val(),'orderType':ordType,'pkcordId':$j("#cordIdentifier").val(),'varflag':flag,'noteType':subtp,'divId':$j("#orderPageDivId").val(),'moduleEntityId':$j("#orderIdentifier").val(),'moduleEntityType':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />','moduleEntityIdentifier':$j('#cburegid').val(),'moduleEntityEventCode':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_ORDER" />'});
	}
	else if (ordType=='CT - Sample at Lab' && viewctsampleatlab=='true'){
			ordType='CT';
		submitpost('getUserRightForGrpForOrder',{'grpId':grpsId,'orderId':$j("#orderIdentifier").val(),'orderType':ordType,'pkcordId':$j("#cordIdentifier").val(),'varflag':flag,'noteType':subtp,'divId':$j("#orderPageDivId").val(),'moduleEntityId':$j("#orderIdentifier").val(),'moduleEntityType':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />','moduleEntityIdentifier':$j('#cburegid').val(),'moduleEntityEventCode':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_ORDER" />'});
	}
	else if (ordType=='HE' && viewhe=='true'){
		submitpost('getUserRightForGrpForOrder',{'grpId':grpsId,'orderId':$j("#orderIdentifier").val(),'orderType':ordType,'pkcordId':$j("#cordIdentifier").val(),'varflag':flag,'noteType':subtp,'divId':$j("#orderPageDivId").val(),'moduleEntityId':$j("#orderIdentifier").val(),'moduleEntityType':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />','moduleEntityIdentifier':$j('#cburegid').val(),'moduleEntityEventCode':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_ORDER" />'});
	}
	else if (ordType=='OR' && viewor=='true'){
		submitpost('getUserRightForGrpForOrder',{'grpId':grpsId,'orderId':$j("#orderIdentifier").val(),'orderType':ordType,'pkcordId':$j("#cordIdentifier").val(),'varflag':flag,'noteType':subtp,'divId':$j("#orderPageDivId").val(),'moduleEntityId':$j("#orderIdentifier").val(),'moduleEntityType':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />','moduleEntityIdentifier':$j('#cburegid').val(),'moduleEntityEventCode':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_ORDER" />'});
	}
	else{
		alert("<s:text name="garuda.order.detail.vieworderconfirm"/> "+ordType+" <s:text name="garuda.order.detail.vieworderconfirm1"/>");
	}

}
function loadRightsByGrpIdByParam(){
	var flag="123";
	var subtp = 'PROCESS_NOTE_CATEGORY_CODESUBTYPE';
	//window.location="getUserRightForGrpForOrderByParam?siteId="+$j("#siteIdentifier").val()+"&userId="+$j("#userIdentifier").val()+"&orderId="+$j("#orderIdentifier").val()+"&orderType="+$j("#orderType").val()+"&pkcordId="+$j("#cordIdentifier").val()+"&varflag="+flag+"&noteType="+subtp+"&divId="+$j("#orderPageDivId").val();
	
	var ordType="<s:property value="#request.orderType"/>";
	   if (ordType=='CT - Ship Sample' && viewctshipsample=='true'){
				ordType='CT';
				submitpost('getUserRightForGrpForOrderByParam',{'siteId':$j("#siteIdentifier").val(),'userId':$j("#userIdentifier").val(),'orderId':$j("#orderIdentifier").val(),'orderType':ordType,'pkcordId':$j("#cordIdentifier").val(),'varflag':flag,'noteType':subtp,'divId':$j("#orderPageDivId").val(),'moduleEntityId':$j("#orderIdentifier").val(),'moduleEntityType':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />','moduleEntityIdentifier':$j('#cburegid').val(),'moduleEntityEventCode':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_ORDER" />'});
		}
		else if (ordType=='CT - Sample at Lab' && viewctsampleatlab=='true'){
				ordType='CT';
				submitpost('getUserRightForGrpForOrderByParam',{'siteId':$j("#siteIdentifier").val(),'userId':$j("#userIdentifier").val(),'orderId':$j("#orderIdentifier").val(),'orderType':ordType,'pkcordId':$j("#cordIdentifier").val(),'varflag':flag,'noteType':subtp,'divId':$j("#orderPageDivId").val(),'moduleEntityId':$j("#orderIdentifier").val(),'moduleEntityType':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />','moduleEntityIdentifier':$j('#cburegid').val(),'moduleEntityEventCode':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_ORDER" />'});
		}
		else if (ordType=='HE' && viewhe=='true'){
			submitpost('getUserRightForGrpForOrderByParam',{'siteId':$j("#siteIdentifier").val(),'userId':$j("#userIdentifier").val(),'orderId':$j("#orderIdentifier").val(),'orderType':ordType,'pkcordId':$j("#cordIdentifier").val(),'varflag':flag,'noteType':subtp,'divId':$j("#orderPageDivId").val(),'moduleEntityId':$j("#orderIdentifier").val(),'moduleEntityType':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />','moduleEntityIdentifier':$j('#cburegid').val(),'moduleEntityEventCode':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_ORDER" />'});
		}
		else if (ordType=='OR' && viewor=='true'){
			submitpost('getUserRightForGrpForOrderByParam',{'siteId':$j("#siteIdentifier").val(),'userId':$j("#userIdentifier").val(),'orderId':$j("#orderIdentifier").val(),'orderType':ordType,'pkcordId':$j("#cordIdentifier").val(),'varflag':flag,'noteType':subtp,'divId':$j("#orderPageDivId").val(),'moduleEntityId':$j("#orderIdentifier").val(),'moduleEntityType':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />','moduleEntityIdentifier':$j('#cburegid').val(),'moduleEntityEventCode':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_ORDER" />'});
		}
		else{
			alert("<s:text name="garuda.order.detail.vieworderconfirm"/> "+ordType+" <s:text name="garuda.order.detail.vieworderconfirm1"/>");
		}
}
$j(function(){
	var grpLstSize= document.getElementById("grpLst").value;
	
 	if(grpLstSize==0)
	{
		confirm("<s:text name="garuda.cbu.detail.groupConfirm"/> ");
	}             
});

</script>
<div id="usersitegrpsdropdown" >
	<input type="hidden" name="cordIdentifier" id="cordIdentifier" value="<s:property value="#request.pkcordId"/>" />
	<input type="hidden" name="orderIdentifier" id="orderIdentifier" value="<s:property value="#request.orderId"/>" />
	<input type="hidden" name="siteIdentifier" id="siteIdentifier" value="<s:property value="#request.siteId"/>" />
	<input type="hidden" name="userIdentifier" id="userIdentifier" value="<s:property value="#request.userId"/>" />
	<input type="hidden" name="orderType" id="orderType" value="<s:property value="#request.orderType"/>" />
	<input type="hidden" name="cburegid" id="cburegid" value="<s:property value="#request.regIdval"/>" />
	<s:hidden name="orderPageDivId" id="orderPageDivId"/>
	<s:hidden name="#request.grpList.size()" id="grpLst"></s:hidden>
	<s:hidden value="#request.CbbList" />
	<table>
		<tr>
			<td width="50%">
				<s:if test="#request.grpList!=null && #request.grpList.size()>1">
				<script>
					loadRightsByGrpIdByParam();
				</script>
				</s:if>
				<s:elseif test="#request.grpList!=null && #request.grpList.size()==1">
				<script>
					loadRightsByGrpId('<s:property value="#request.grpList[0].pkGrp" />');
				</script>
				</s:elseif>
				<s:elseif test="#request.grpList!=null && #request.grpList.size()==0">
				</s:elseif>
			</td> 
		</tr>
	</table>
</div>

