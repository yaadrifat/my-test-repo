<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction"%>
<%@page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.ordercomponent.business.util.VelosMidConstants"%>
<%@page import="com.velos.ordercomponent.business.util.Utilities"%>
<jsp:useBean id="modCtlDao" scope="request"
	class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<%
	HttpSession tSession = request.getSession(true);
	GrpRightsJB grpRights = (GrpRightsJB) tSession
			.getAttribute("GRights");
	String accId = null;
	String logUsr = null;
	if (sessionmaint.isValidSession(tSession)) {
		accId = (String) tSession.getValue("accountId");
		logUsr = (String) tSession.getValue("userId");
		request.setAttribute("username", logUsr);
	}
%>
<script>
function addPrintContent(divId){
	var sourceFile ="";
	var patientStr=$j.trim($j("#patientId").val());
	var regidStr=$j.trim($j("#registryId").val());
	$j("#printDiv").html(sourceFile);
	sourceFile +="<table><thead><tr><th><center><h3>";
	 if(regidStr!="" && regidStr!="undefined"){
	 sourceFile +="<u>CBU Registry ID:"+regidStr+"</u>";
	}
	 if(patientStr!="" && patientStr!="undefined"){
	 	 sourceFile +="&nbsp;&nbsp;&nbsp;<u>Patient ID:"+patientStr+"</u>";
	 }
	 if(divId=="ctShipmentInfoContentDiv"){
		    	sourceFile +="</h3></center><br/></th></tr></thead><tbody><tr><td>"+$j("#ctshipmentbarDiv").html()+"</td></tr></tbody></table>";
		 }
	 else if(divId=="ctresolsubDiv"){
	    	sourceFile +="</h3></center><br/></th></tr></thead><tbody><tr><td>"+$j("#ctresolutionbarDiv").html()+"</td></tr></tbody></table>";
	 }
	 else{
		 	    sourceFile +="</h3></center><br/></th></tr></thead><tbody><tr><td>"+$j('#'+divId).html()+"</td></tr></tbody></table>";
		 }
	 var source = $j("#printDiv").html(sourceFile);
	 $j("#printDiv").hide();
	 if(divId=="openOrdercbuhis"){
		printHistoryWidget('openOrdercbuhis',regidStr);
	 }
	 else{
	    clickheretoprint('printDiv','','1');
	 }
	 }
$j(function() {
$j(".positive-integer").numeric({ decimal: false, negative: false }, function() { alert("<s:text name="garuda.common.cbu.positiveIntAlert"/>"); this.value = ""; this.focus(); });		
$j("#errorShipmentDateDiv").hide();
$j("#errorShipmentCmpyDiv").hide();
$j("#errorShipmentTrackNoDiv").hide();
$j("#errorAddiTrackNoDiv").hide();
$j("#errorShipmentSamTypDiv").hide();
$j("#errorAliquotsDiv").hide();
$j("#errorcordAvailConfirmDiv").hide();
$j("#errorunavailReasonDiv").hide();

$j('.toggleHla').live('click',function(){
		currenthlatbl_onload();	
		hlatblOther_onload();
		$j('#currenthlaTbl_info').empty();
		$j('#hlaTblOther_info').empty();
});

});

function currenthlatbl_onload(){
	var t_width=$j('.viewcbutoppanel').width();
	t_width=(t_width/2)-30;
	var t_height=$j('.viewcbutoppanel').height();
	var hlatbl=$j('#currenthlaTbl').dataTable({
	    "oLanguage": {
			"sSearch": '<s:text name="garuda.common.message.searchCol"/>',
			"sEmptyTable": '<s:text name="garuda.common.message.noMatchingRecords"/>'
		},
	 	"sScrollY": t_height, 
	 	"sScrollX": t_width,
	 	"bSort":false,
	 	"bDestory":true,
	 	"bRetrieve":true
	});
	$j('#currenthlaTbl_length').empty();
	$j('#currenthlaTbl_filter').empty();
	$j('#currenthlaTbl_info').empty();
	$j('#currenthlaTbl_paginate').empty();
	
	if ( hlatbl.length > 0 ) {
		hlatbl.fnAdjustColumnSizing();
	}
}
function hlatblOther_onload(){
	var t_width=$j('.viewcbutoppanel').width();
	t_width=(t_width/2)-30;
	var t_height=$j('.viewcbutoppanel').height();
	var hlatbl=$j('#hlaTblOther').dataTable({
	    "oLanguage": {
			"sSearch": '<s:text name="garuda.common.message.searchCol"/>',
			"sEmptyTable": '<s:text name="garuda.common.message.noMatchingRecords"/>'
		},
	 	"sScrollY": t_height,
	 	"sScrollX": t_width,
	 	"bSort":false,
	 	"bDestory":true,
	 	"bRetrieve":true
	});
	$j('#hlaTblOther_length').empty();
	$j('#hlaTblOther_filter').empty();
	$j('#hlaTblOther_info').empty();
	$j('#hlaTblOther_paginate').empty();
	
	if (hlatbl.length > 0 ) {
		hlatbl.fnAdjustColumnSizing();
	}
}


 function disableVal()
 {
	 alert("<s:text name="garuda.cbu.order.userEditAlert"/>");
 }
 
 function savecordAvailNmdp(){
		var orderId = $j("#orderId").val();
		 var type = $j('#orderType').val();
		 fn_showModalCTdetails('<s:text name="garuda.pf.modalwindowheader.cbuavailconfirm"/><s:property value="cdrCbuPojo.registryId" />','savecordAvailNmdp?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&cbuid=<s:property value="cdrCbuPojo.cordID" />'+'&moduleEntityId='+$j("#orderId").val()+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_CORD_AVAIL_CONFIRM" />','300','800');
		
		//window.open( 'savecordAvailNmdp', "savecordAvailNmdp", "status = 1, height = 600, width = 800, scrollbars = yes , resizable = yes");
	}
 function shwMdal(pfcat,resistryId,divId)
 {
	 var progresspermission = $j("#progressNotePermiossion").val(); 
	 var clinicalNotePermiossion = $j("#clinicalNotePermiossion").val();
 	 var type = $j('#orderType').val();
 	 var orderId = $j("#orderId").val();
 	 var requestedDate = $j("#requestedDate").val();
 	
 		     	    	var subtp = '<s:text name="garuda.cbu.order.processConst"/>';
 		     	    			 jConfirm('<s:text name="garuda.clinicalnote.label.confirmationmsg"/>', '<s:text name="garuda.common.lable.confirm"/>',
 		     	    					 function(r) {
 		     	   		     	 if (r == true) {
 		     	   		     	    	var subtp = '<s:text name="garuda.cbu.order.noteConst"/>';
 		     	   		     	   		if(clinicalNotePermiossion=="true")
 		     	   		     	   			fn_showModalCTNotes('<s:text name="garuda.header.label.clinicalNote" /> <s:property value="cdrCbuPojo.registryId" />','clinicalNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&subtype='+subtp,'550','850','notesModalsDiv');
 		     	   		     	   		else
 		     	   		     				alert('<s:text name ="garuda.clinicalNote.accessRights"/>');
 		     	   		     	   return result;
 		     	   		     	    }
 		     	    	 		    else {
 		     	    	 		     	    	var subtp = '<s:text name="garuda.cbu.order.processConst"/>';
 		     	    	 		     	    	if(progresspermission=="true"){
 		     	    	 		     	    		if(requestedDate!=null)
 		    	 		     	    					{
 		     	    	 		     	    				fn_showModalCTNotes('<s:text name="garuda.header.label.progressNote"/> <s:property value="cdrCbuPojo.registryId" />','pfNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&pfcat='+pfcat+'&pkcordId='+$j("#pkcordId").val()+'&divId='+divId+'&requestedDate='+requestedDate,'600','850','notesModalDiv');
 		    	 		     	    					}
 		     	    	 		     				else
 		 	 		     	    						{
 		     	    	 		     						fn_showModalCTNotes('<s:text name="garuda.header.label.progressNote"/> <s:property value="cdrCbuPojo.registryId" />','pfNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&pfcat='+pfcat+'&pkcordId='+$j("#pkcordId").val()+'&divId='+divId,'600','850','notesModalDiv');
 		 	 		     	    						}
 		     	    	 		     	    	}
 		     	    	 		     	    	else
 		     	    	 		     	    		{
 		     	    	 		     	    				alert('<s:text name ="garuda.progressNote.accessRights"/>');
 		     	    	 		     	    		}
 		     	    	 		     	    }
 		     	    	 		     });
 		     
 	  
 }
 function shwMdal1(resistryId,divId)
 {
 	 var type = $j('#orderType').val();
 	 var orderId = $j("#orderId").val();
 	 var requestedDate = $j("#requestedDate").val();
 	 var progresspermission = $j("#progressNotePermiossion").val();
 	var sampleAtLab = '<s:property value="orderPojo.sampleAtLab"/>';
 	if(type=="CT"){
 	 	if(sampleAtLab=="Y"){
 	 		type='<s:text name="garuda.orOrderDetail.portletname.ctlaborder"/>';
 	 	 }
 	 	else if(sampleAtLab=="N"){
 	 		type='<s:text name="garuda.orOrderDetail.portletname.ctshiporder"/>';
 	 	 }
 	 }
	var clinicalNotePermiossion = $j("#clinicalNotePermiossion").val();
 	 jConfirm('<s:text name="garuda.clinicalnote.label.confirmationmsg"/>', '<s:text name="garuda.common.lable.confirm"/>',
 		       function(r) {
 		     	 if (r == true) {
 		     	    	var subtp = '<s:text name="garuda.cbu.order.noteConst"/>';
 		     	    	if(clinicalNotePermiossion=="true")
 		     	    		fn_showModalCTNotes('<s:text name="garuda.header.label.clinicalNote" /> <s:property value="cdrCbuPojo.registryId" />','clinicalNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&subtype='+subtp,'550','850','notesModalsDiv');
 		     	    	else
 		     	    		alert('<s:text name ="garuda.clinicalNote.accessRights"/>');
 		     	    	return result;
 		     	    }
 		     	    else {
 		     	    	var subtp = '<s:text name="garuda.cbu.order.processConst"/>';
 		     	    	if(progresspermission=="true")
    	    			{
 		     	    	if(requestedDate!=null)
 		     	    		{
 		     	    		fn_showModalCTNotes('<s:text name="garuda.header.label.progressNote"/> <s:property value="cdrCbuPojo.registryId" />','pfNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&pkcordId='+$j("#pkcordId").val()+'&divId='+divId+'&requestedDate='+requestedDate,'650','850','notesModalDiv');
 		     	    		}
 		     	    		
 		     	    	else
 		     	    		{
 		     	    		fn_showModalCTNotes('<s:text name="garuda.header.label.progressNote"/> <s:property value="cdrCbuPojo.registryId" />','pfNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&pkcordId='+$j("#pkcordId").val()+'&divId='+divId,'650','850','notesModalDiv');
 		     	    		}
    	    			}
 		     	    	else
    	    			{
    	    			alert('<s:text name ="garuda.progressNote.accessRights"/>');
    	    			}
 		     	    }
 		     });

 }
 function fn_showModalCTNotes(title,url,hight,width,id){
	 var patientId = $j('#patientId').val();
		var patient_data = '';
		if(patientId!="" && patientId != null && patientId != "undefined"){
			patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
		}
		title=title+patient_data;
		showModals(title,url,hight,width,id);
		}
 function fn_showModalCTdetails(title,url,hight,width){
	 var patientId = $j('#patientId').val();
		var patient_data = '';
		if(patientId!="" && patientId != null && patientId != "undefined"){
			patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
		}
		title=title+patient_data;
		showModal(title,url,hight,width);
		}
function submitCTResolution(){
	if($j("#availdatepicker").val()==null){var availableDt="";}
	else{var availableDt=$j("#availdatepicker").val();}
	if($j("#ResolutionCBB").val()==null){var resolbycbb="";}
	else{var resolbycbb=$j("#ResolutionCBB").val();}
	if($j("#ResolutionTC").val()==null){var resolbytc="";}
	else{var resolbytc=$j("#ResolutionTC").val();}
	if($j("#ResoluAck").attr('checked')){var resolutioncheck="Y";}
	else{var resolutioncheck="N";}
	if($j("#currentuserid").val()==null){var userid="";}
	else{var userid=$j("#currentuserid").val();}
	loadmultiplediv('submitCtResolution?pkcordId='+$j('#pkcordId').val()+'&availableDt='+availableDt+'&resolbycbb='+resolbycbb+'&resolbytc='+resolbytc+'&ackResolution='+resolutioncheck+'&userID='+userid+'&orderType='+$j('#orderType').val()+'&orderId='+$j("#orderId").val(),'ctresolutionbarDiv,openOrdercbuhis,openOrdercbuInfo');
	historyTblOnLoad();
	Fnresol();
	if($j('#canackorders').val()!=""){
    	alert("<s:text name="garuda.cbu.order.orderCancelAlert"/> ");
    }
	getprogressbarcolor();
	
}

function createPackageSlip(){
		var pkPakageslip=$j("#pkPackingSlipId").val();
		var flag=$j("#hlaTypingAvailFlag").val();
		if($j("#tskAdditiTypingcompl").val()==null || $j("#tskAdditiTypingcompl").val()==0){
			alert("<s:text name="garuda.cbu.order.updatedHlaTyping"/>");
		}
		else
		{
		if(flag=='Y' && $j("#recHlaEnteredFlag").val()!=null && $j("#recHlaEnteredFlag").val()=='Y'){
			if($j("#dataModifiedFlag").val()!=null && $j("#dataModifiedFlag").val()!='' && $j("#dataModifiedFlag").val()=='Y'){
				jConfirm('<s:text name="garuda.cbu.order.generatePakageSlipAlert"/>', '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
				    if(r){
				    	loadMoredivs('updatePackingSlipDet?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+
								'&schshipdate='+$j("#ctShipDateId").val()+'&shipcompany='+$j("#shipCompany").val()+'&trackNo='+$j("#trackNo").val()+'&samtype='+$j("#SamTypeAvailable").val()+'&aliqtype='+$j("#AliquotsType").val()+'&pkShipmentId='+$j("#pkShipmentId").val()+'&cbbId='+$j("#cbbId").val()+'&pkHlaId='+$j("#recentHlaId").val()+'&sampleAtLab='+$j("#sampleAtLabVal").val(),'ctShipmentInfoContentDiv,currentRequestProgressMainDiv','cbuCordInformationForm');
						pkPakageslip=$j("#pkPackingSlipId").val();
						
				    }
				    loadMoredivs('createPackageSlip.action?orderId='+$j("#orderId").val()+'&pkcordId='+$j("#pkcordId").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&orderType='+$j("#orderType").val()+'&cdrCbuPojo.cordID='+$j("#pkcordId").val(),'currentRequestProgress,taskCompleteFlagsDiv,chistory,cReqhistory','cbuCordInformationForm');
				    setpfprogressbar();
					getprogressbarcolor();
					historyTblOnLoad();
				    window.open('cbuPdfReports?repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+$j("#pkcordId").val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
				    //window.open('createPackageSlip.action?orderId='+$j("#orderId").val()+'&pkcordId='+$j("#pkcordId").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+$j("#pkcordId").val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
				});

			}
			else if($j("#dataModifiedFlag").val()!=null && $j("#dataModifiedFlag").val()!='' && $j("#dataModifiedFlag").val()=='N'){
				loadMoredivs('updatePackingSlipDet?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+
						'&schshipdate='+$j("#ctShipDateId").val()+'&shipcompany='+$j("#shipCompany").val()+'&trackNo='+$j("#trackNo").val()+'&samtype='+$j("#SamTypeAvailable").val()+'&aliqtype='+$j("#AliquotsType").val()+'&pkShipmentId='+$j("#pkShipmentId").val()+'&cbbId='+$j("#cbbId").val()+'&pkHlaId='+$j("#recentHlaId").val()+'&sampleAtLab='+$j("#sampleAtLabVal").val(),'ctShipmentInfoContentDiv,currentRequestProgressMainDiv','cbuCordInformationForm');
				pkPakageslip=$j("#pkPackingSlipId").val();
				//window.open('createPackageSlip.action?orderId='+$j("#orderId").val()+'&pkcordId='+$j("#pkcordId").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+$j("#pkcordId").val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
				loadMoredivs('createPackageSlip.action?orderId='+$j("#orderId").val()+'&pkcordId='+$j("#pkcordId").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&orderType='+$j("#orderType").val()+'&cdrCbuPojo.cordID='+$j("#pkcordId").val(),'currentRequestProgress,taskCompleteFlagsDiv,chistory,cReqhistory','cbuCordInformationForm');
				setpfprogressbar();
				getprogressbarcolor();
				historyTblOnLoad();
				window.open('cbuPdfReports?repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+$j("#pkcordId").val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
			}
			else if($j("#dataModifiedFlag").val()!=null && $j("#dataModifiedFlag").val()=='' && $j("#dataModifiedFlag").val()!="undefined"){
				//window.open('createPackageSlip.action?orderId='+$j("#orderId").val()+'&pkcordId='+$j("#pkcordId").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+$j("#pkcordId").val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
				loadMoredivs('createPackageSlip.action?orderId='+$j("#orderId").val()+'&pkcordId='+$j("#pkcordId").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&orderType='+$j("#orderType").val()+'&cdrCbuPojo.cordID='+$j("#pkcordId").val(),'currentRequestProgress,taskCompleteFlagsDiv,chistory,cReqhistory','cbuCordInformationForm');
				setpfprogressbar();
				getprogressbarcolor();
				historyTblOnLoad();
				window.open('cbuPdfReports?repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+$j("#pkcordId").val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
			}
		}
		else if(flag=='N'){
			if($j("#dataModifiedFlag").val()!=null && $j("#dataModifiedFlag").val()!='' && $j("#dataModifiedFlag").val()=='Y'){
				jConfirm('<s:text name="garuda.cbu.order.generatePakageSlipAlert"/>', '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
				    if(r){
				    	loadMoredivs('updatePackingSlipDet?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+
								'&schshipdate='+$j("#ctShipDateId").val()+'&shipcompany='+$j("#shipCompany").val()+'&trackNo='+$j("#trackNo").val()+'&samtype='+$j("#SamTypeAvailable").val()+
								'&aliqtype='+$j("#AliquotsType").val()+'&pkShipmentId='+$j("#pkShipmentId").val()+'&cbbId='+$j("#cbbId").val()+'&pkHlaId='+$j("#recentHlaId").val()+
								'&sampleAtLab='+$j("#sampleAtLabVal").val(),'ctShipmentInfoContentDiv,currentRequestProgressMainDiv','cbuCordInformationForm');
						pkPakageslip=$j("#pkPackingSlipId").val();
				    }
				    //window.open('createPackageSlip.action?orderId='+$j("#orderId").val()+'&pkcordId='+$j("#pkcordId").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+$j("#pkcordId").val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
				    loadMoredivs('createPackageSlip.action?orderId='+$j("#orderId").val()+'&pkcordId='+$j("#pkcordId").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&orderType='+$j("#orderType").val()+'&cdrCbuPojo.cordID='+$j("#pkcordId").val(),'currentRequestProgress,taskCompleteFlagsDiv,chistory,cReqhistory','cbuCordInformationForm');
				    setpfprogressbar();
					getprogressbarcolor();
					historyTblOnLoad();
				    window.open('cbuPdfReports?repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+$j("#pkcordId").val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
				});
			}
			else if($j("#dataModifiedFlag").val()!=null && $j("#dataModifiedFlag").val()!='' && $j("#dataModifiedFlag").val()=='N'){
				loadMoredivs('updatePackingSlipDet?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+
						'&schshipdate='+$j("#ctShipDateId").val()+'&shipcompany='+$j("#shipCompany").val()+'&trackNo='+$j("#trackNo").val()+'&samtype='+$j("#SamTypeAvailable").val()+'&aliqtype='+$j("#AliquotsType").val()+'&pkShipmentId='+$j("#pkShipmentId").val()+'&cbbId='+$j("#cbbId").val()+'&pkHlaId='+$j("#recentHlaId").val()+'&sampleAtLab='+$j("#sampleAtLabVal").val(),'ctShipmentInfoContentDiv,currentRequestProgressMainDiv','cbuCordInformationForm');
				pkPakageslip=$j("#pkPackingSlipId").val();
				//window.open('createPackageSlip.action?orderId='+$j("#orderId").val()+'&pkcordId='+$j("#pkcordId").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+$j("#pkcordId").val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
				loadMoredivs('createPackageSlip?orderId='+$j("#orderId").val()+'&pkcordId='+$j("#pkcordId").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&orderType='+$j("#orderType").val()+'&cdrCbuPojo.cordID='+$j("#pkcordId").val(),'currentRequestProgress,taskCompleteFlagsDiv,chistory,cReqhistory','cbuCordInformationForm');
				setpfprogressbar();
				getprogressbarcolor();
				historyTblOnLoad();
				window.open('cbuPdfReports?repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+$j("#pkcordId").val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
			}
			else if($j("#dataModifiedFlag").val()!=null && $j("#dataModifiedFlag").val()=='' && $j("#dataModifiedFlag").val()!="undefined"){
				//window.open('createPackageSlip.action?orderId='+$j("#orderId").val()+'&pkcordId='+$j("#pkcordId").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+$j("#pkcordId").val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
				loadMoredivs('createPackageSlip.action?orderId='+$j("#orderId").val()+'&pkcordId='+$j("#pkcordId").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&orderType='+$j("#orderType").val()+'&cdrCbuPojo.cordID='+$j("#pkcordId").val(),'currentRequestProgress,taskCompleteFlagsDiv,chistory,cReqhistory','cbuCordInformationForm');
				setpfprogressbar();
				getprogressbarcolor();
				historyTblOnLoad();
				window.open('cbuPdfReports?repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+$j("#pkcordId").val(),'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
			}
			
		}
		else{
			alert('<s:text name="garuda.cbu.order.updatedHlaTypingalert"/>');
			}
		}
}

function openModalForLabSummary(){
	fn_showModalCTNotes('View Lab Summary for CBU Registry ID <s:property value="cdrCbuPojo.registryId" />','getLabSummary?cdrCbuPojo.cordID='+$j("#pkcordId").val()+'&cordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&esignFlag=modal','500','800','labSummaryForComplete');
	//showModals('View Lab Summary','getLabSummary?cdrCbuPojo.cordID='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val(),'500','800','dynaModalForm');
}

function openModalForSampleInventory(){
	fn_showModalCTdetails('View Sample Inventory for CBU Registry ID <s:property value="cdrCbuPojo.registryId" />','updateProcessInfo?entityId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val());
}

function showSwitchWorkflowHelpMsg(id){
	msg=$j('#'+id).html();
	overlib(msg,CAPTION,'Workflow Switched details');
}
var cttcorderdetflag=0;
function getcttcorderdetails(){
	if($j('#cttcorderdetspan').is(".ui-icon-plusthick")){
		if(cttcorderdetflag==0){		
			loadMoredivs('getcttcorderdetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val(),'cttcorderdetcontentdiv','ctOrderDetailsForm');
			cttcorderdetflag=1;
		}
	}
	else if($j('#cttcorderdetspan').is(".ui-icon-minusthick")) {
		$j('#cttcorderdetcontentdiv').show();
	}
}
var addititypingflag=0;
function getAdditiTypingDet(){
	if($j('#additiTypingspan').is(".ui-icon-plusthick")){
		if(addititypingflag==0){		
			loadMoredivs('getAdditiTypingDetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&cdrCbuPojo.cordID='+$j("#pkcordId").val(),'additityingandtestcontentDiv','ctOrderDetailsForm');
			addititypingflag=1;
		}
	}
	else if($j('#additiTypingspan').is(".ui-icon-minusthick")) {
		$j('#additityingandtestcontentDiv').show();
	}
	
	
	var flag=$j('#hlaLoadedflag').val();
	
	if(flag==null || flag=="" || flag=="0"){
		currenthlatbl_onload();
		hlatblOther_onload();
	}
}
function openmodalforAdditiTyping(){
	if($j("#tskConfirmCbuAvailcompl").val()=="1"){
		setEmptyOrders();
		fn_showModalCTdetails('<s:text name="garuda.pf.modalwindowheader.additihlatyping"/><s:property value="cdrCbuPojo.registryId" />','getModalAdditiTypingDetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&cdrCbuPojo.cordID='+$j("#pkcordId").val()+'&moduleEntityId='+$j("#orderId").val()+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_ADDIT_HLA_TYPING" />','300','800');
	}else{
		alert('<s:text name="garuda.common.validation.cordavailconfirm"/>');
	}
}

function getContentToPrint(divToPrint){
	$j('#printTemp').html($j('#additityingandtestDiv').html());
	tableAlignmentInPrint('printTemp','currenthlaTbl,hlaTblOther');
	addPrintContent('printTemp');
}
</script>
<s:form id="ctOrderDetailsForm">
	<s:hidden name="resolbyCbbDynamic"></s:hidden>
	<s:hidden name="clinicalNotePojo.pkNotes" />
	<s:hidden name="orderType" id="orderType"></s:hidden>
	<s:set name="orderType" value="orderType" scope="request" />
	<s:hidden name="clinicalNotePojo.requestType" />
	<s:hidden name="cdrCbuPojo.registryId" id="registryId"/>
	<input type="hidden" id="clinicalNotePermiossion" value="<s:property value="hasNewPermission(#request.vClnclNote)"/>"/>
    <input type="hidden" id="progressNotePermiossion" value="<s:property value="hasNewPermission(#request.vPrgrssNote)"/>"/>

	<table>
		<tr>
			<td width="100%">
			<div id="Middiv">
			<div id="ctordertaskListProgressBarDiv">
			<div class="portlet" id="openOrdercbuInfoparent">
		      <s:if test="hasViewPermission(#request.viewCBUShipmnt)==true">
			<div id="openOrdercbuInfo"
				class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			<span class="ui-icon ui-icon-print" onclick="getCurrentRequestProgressDetails('currentRequestProgressMainDiv,caseManagerPatientandTCdetailsDiv,cttcorderdetcontentdiv,ctShipmentInfoContentDiv,switchWorkflowDetailsDiv,ctReqClinInfoContentDiv,quicklinksDiv,additityingandtestcontentDiv');addPrintContent('openOrdercbuInfo')"></span>
			<span class="ui-icon ui-icon-plusthick" id="currentReqProSpan" onclick="getCurrentRequestProgressDetails('currentRequestProgressMainDiv,caseManagerPatientandTCdetailsDiv,cttcorderdetcontentdiv,ctShipmentInfoContentDiv,switchWorkflowDetailsDiv,ctReqClinInfoContentDiv,quicklinksDiv,additityingandtestcontentDiv');"></span>
			<!--<span class="ui-icon ui-icon-minusthick"></span>-->
			 <s:text
				name="garuda.ctOrderDetail.portletname.currentReqProgress"></s:text>

			</div>
			<div class="portlet-content" style="display: none;">
			<div id="currentRequestProgressMainDiv">
			<s:if
				test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
				<table>
					<tr>
						<td colspan="9" rowspan="5" align="left">
					         <s:if test="hasViewPermission(#request.switchFlow)==true">
								<div <s:if test="(orderPojo.iscordavailfornmdp==@com.velos.ordercomponent.business.util.VelosMidConstants@FLAG_N || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)) || (orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED))">style="display:none"</s:if>>
								<button type="button" onclick="changeWorkflow()"><s:text
							name="garuda.currentReqProgress.label.switchworkflow"></s:text></button>
							</div>
					         </s:if>		
						</td>
					
						<td colspan="1" rowspan="5" align="right">
					        
					        <s:iterator value="ctOrderDetailsList" var="rowVal">
					        <s:if test='%{#rowVal[2]!=null && #rowVal[2]=="N"}'>
					        <s:if test="hasViewPermission(#request.viewCTShipReport)==true">
					        	<button type="button" onclick="window.open('cbuPdfReports?cdrCbuPojo.CordID=<s:property value="cdrCbuPojo.CordID"/>&repId=180&repName=NMDP CT-Ship Sample Detail Report&reqDate=<s:property value="%{#rowVal[45]}" />&selDate=&params=<s:property value="orderId"/>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><s:text
								name="garuda.ordersreport.button.label"></s:text></button>
							</s:if>
							</s:if>
							
							<s:if test='%{#rowVal[2]!=null && #rowVal[2]=="Y"}'>
							<s:if test="hasViewPermission(#request.viewCTSampReport)==true">
							<button type="button" onclick="window.open('cbuPdfReports?cdrCbuPojo.CordID=<s:property value="cdrCbuPojo.CordID"/>&repId=181&repName=NMDP CT-Sample at Lab Detail Report&reqDate=<s:property value="%{#rowVal[45]}" />&selDate=&params=<s:property value="orderId"/>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><s:text
							name="garuda.ordersreport.button.label"></s:text></button>
							</s:if>
							</s:if>
							</s:iterator>
								
						</td>
					</tr>
				</table>
			</s:if> <s:if test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
				<s:iterator value="ctOrderDetailsList" var="rowVal">
					<s:if test='%{#rowVal[2]!=null && #rowVal[2]=="N"}'>
						<table>
							<tr style="font-style: italic; color: blue; font-size: 10px">
								<td width="9%" height="20px"></td>
								<td width="9%" align="center"><a
									href="#cbuavailconfbarDiv" onclick="togglePFDivs('cordAvailspan','ctcbuavailconfbarDiv','getCordAvailDetails')" id="cbuavailtd"><s:property
									value="tskConfirmCbuAvail" /></a></td>
								<td width="9%" align="center"><a
									href="#reqclincinfobarDiv" onclick="togglePFDivs('reqclinspanid','ctReqClinInfoContentDiv','')"><s:property
									value="tskReqClincInfo" /></a></td>
								<td width="9%" align="center"><a
									href="#additityingandtestDiv" onclick="togglePFDivs('additiTypingspan','additityingandtestcontentDiv','getAdditiTypingDet')"><s:property
									value="tskAdditiTyping" /></a></td>
								<td width="9%" align="center"><a 
									href="#ctshipmentbarDiv" onclick="togglePFDivs('ctshipmentspan','ctShipmentInfoContentDiv','getctshipmentDetails')"><s:property
									value="tskShipmentInfo" /></a></td>
								<td width="9%" align="center"><a 
									href="#ctshipmentbarDiv" onclick="togglePFDivs('ctshipmentspan','ctShipmentInfoContentDiv','getctshipmentDetails')"><s:property
									value="tskGenPackageSlip" /></a></td>
								<td width="9%" align="center"><a 
									href="#ctshipmentbarDiv" onclick="togglePFDivs('ctshipmentspan','ctShipmentInfoContentDiv','getctshipmentDetails')"><s:text
									name="garuda.ctOrderDetail.progressbar.sampleshipped" /></a></td>
								<td width="9%" align="center"></td>
								<td width="9%" align="center"></td>
								<td width="9%" align="center"><a
									href="#ctresolutionbarDiv" id="resoltd" onclick="togglePFDivs('ctResoultionspan','ctresolsubDiv','getResolutionDetails','ctresolsubDiv')"><s:property
									value="tskAcknowledgeResol" /></a></td>
								<td width="10%" style="font-size: 12px; font: bold;"
									align="center" rowspan="4"><s:if test="%{#rowVal[48]>2}">
									<span style="color: red; font-size: 30px"><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:if> <s:else>
									<span style="font-size: 30px""><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:else></td>
							</tr>
							<tr>
								<td width="9%" height="20px"></td>
								<td width="9%" id="cbuavailconfbar"></td>
								<td width="9%" id="completeReqInfobar"></td>
								<td width="9%" id="additityingandtest"></td>
								<td width="9%" id="ctshipmentbar"></td>
								<td width="9%" id="genpackageslipbar"></td>
								<td width="9%" id="ctshipdatebar"></td>
								<td width="9%" id="resRecbar"></td>
								<td width="9%" id="Resolutionbar"></td>
								<td width="9%" id="Acknowledgebar"></td>

							</tr>
							<tr>
								<td width="9%"></td>
								<td width="9%" align="left"><img src="images/sort_asc.png" /></td>
								<td width="9%" align="left"><img src="images/sort_asc.png" /></td>
								<td width="9%" align="right"></td>
								<td width="9%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="9%"></td>
								<td width="9%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="9%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="9%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="9%" align="right"><img
									src="images/sort_asc.png" /></td>

							</tr>
							<tr style="font-style: italic; color: green; font-size: 10px">
								<td width="9%"></td>
								<td width="9%" align="left" style=""><s:property
									value="msNew" /></td>
								<td width="9%" align="left"><s:property value="msReserved" /></td>
								<td width="9%" align="right"></td>
								<td width="9%" align="right"><s:property
									value="msShipmentsch" /></td>
								<td width="9%"></td>
								<td width="9%" align="right"><s:property
									value="msAwaitHlaRes" /></td>
								<td width="9%" align="right"><s:property
									value="msHlaResRece" /></td>
								<td width="9%" align="right"><s:property
									value="msResolved" /></td>
								<td width="9%" align="right"><s:property
									value="msComplete" /></td>

							</tr>
						</table>
					</s:if>
					<s:elseif test='%{#rowVal[2]!=null && #rowVal[2]=="Y"}'>
						<table>
							<tr style="font-style: italic; color: blue; font-size: 10px">
								<td width="10%" height="20px"></td>
								<td width="10%" align="center"><a
									href="#cbuavailconfbarDiv" onclick="togglePFDivs('cordAvailspan','ctcbuavailconfbarDiv','getCordAvailDetails')"><s:property
									value="tskConfirmCbuAvail" /></a></td>
								<td width="10%" align="center"><a
									href="#reqclincinfobarDiv" onclick="togglePFDivs('reqclinspanid','ctReqClinInfoContentDiv','')"><s:property
									value="tskReqClincInfo" /></a></td>
								<td width="10%" align="center"><a
									href="#additityingandtestDiv" onclick="togglePFDivs('additiTypingspan','additityingandtestcontentDiv','getAdditiTypingDet')"><s:property
									value="tskAdditiTyping" /></a></td>
								<td width="10%" align="center"></td>
								<td width="10%" align="center"></td>
								<td width="10%" align="center"><a
									href="#ctresolutionbarDiv" id="resoltd" onclick="togglePFDivs('ctResoultionspan','ctresolsubDiv','getResolutionDetails','ctresolsubDiv')"><s:property
									value="tskAcknowledgeResol" /></a></td>
								<td width="10%" style="font-size: 12px; font: bold;"
									align="center" rowspan="4"><s:if test="%{#rowVal[48]>2}">
									<span style="color: red; font-size: 30px"><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:if> <s:else>
									<span style="font-size: 30px""><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:else></td>
							</tr>
							<tr>
								<td width="10%"></td>
								<td width="10%" id="cbuavailconfbar"></td>
								<td width="10%" id="completeReqInfobar"></td>
								<td width="10%" id="additityingandtest"></td>
								<td width="10%" id="resRecbar"></td>
								<td width="10%" id="Resolutionbar"></td>
								<td width="10%" id="Acknowledgebar"></td>

							</tr>
							<tr>
								<td width="10%"></td>
								<td width="10%" align="left"><img src="images/sort_asc.png" /></td>
								<td width="10%" align="left"><img src="images/sort_asc.png" /></td>
								<td width="10%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="10%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="10%" align="right"><img
									src="images/sort_asc.png" /></td>
								<td width="10%" align="right"><img
									src="images/sort_asc.png" /></td>

							</tr>
							<tr style="font-style: italic; color: green; font-size: 10px">
								<td width="10%"></td>
								<td width="10%" align="left" style=""><s:property
									value="msNew" /></td>
								<td width="10%" align="left"><s:property value="msReserved" /></td>
								<td width="10%" align="right"><s:property
									value="msAwaitHlaRes" /></td>
								<td width="10%" align="right"><s:property
									value="msHlaResRece" /></td>
								<td width="10%" align="right"><s:property
									value="msResolved" /></td>
								<td width="10%" align="right"><s:property
									value="msComplete" /></td>

							</tr>
						</table>
					</s:elseif>
				</s:iterator>
			</s:if> <s:if test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
				<table>
					<tr>
						<td width="100%" colspan="3">
						<fieldset>
						<div id="currentRequestProgress">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="10%" align="left"><strong><s:text
									name="garuda.cbuHistory.label.requesttype" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:hidden name="sampleAtLab" id="sampleAtLab"
											value="%{#rowVal[2]}"></s:hidden>
										<s:if test='%{#rowVal[2]!=null && #rowVal[2]=="Y"}'>
											<s:text name="garuda.orOrderDetail.portletname.ctlaborder"></s:text>
											<s:hidden id="labOrder" value="Y" />
										</s:if>
										<s:if test='%{#rowVal[2]!=null && #rowVal[2]=="N"}'>
											<s:text name="garuda.orOrderDetail.portletname.ctshiporder"></s:text>
											<s:hidden id="labOrder" value="N" />
										</s:if>
										<s:if
											test='%{(#rowVal[71]!=null && #rowVal[71]!="") || (#rowVal[72]!=null && #rowVal[72]!="")}'>
											<img id="switchworkflowdetId" height="15px"
												src="./images/help_24x24px.png"
												onmouseover="showSwitchWorkflowHelpMsg('switchWorkflowDetailsDiv')"
												onmouseout="return nd();">
										</s:if>
									</s:iterator>
								</s:if></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.requeststatus"></s:text>:</strong>
								</td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<span style="color: red;"> <s:property
											value="%{#rowVal[0]}" /></span>
									</s:iterator>
								</s:if> <s:else>
									<s:text name="garuda.currentReqProgress.label.notprovided"></s:text>
								</s:else></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.reqStatusDate"></s:text>:</strong>
								</td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test='#rowVal[1]!=null && #rowVal[1]!=""'>
											<s:property value="%{#rowVal[1]}" />
										</s:if>
									</s:iterator>
								</s:if></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.resolbytc" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test='#rowVal[57]!=null && #rowVal[57]!=""'>
											<s:property value="getCbuStatusDescByPk(#rowVal[57])" />
											<s:hidden name="resolByTCFlag" id="resolByTCFlag" value="%{#rowVal[57]}"></s:hidden>
											<s:hidden name="resolByCBBFlag" id="resolByCBBFlag"	value="%{#rowVal[58]}"></s:hidden>
										</s:if>
										<s:elseif test='#rowVal[58]!=null && #rowVal[58]!=""'>
									<s:property value="getCbuStatusDescByPk(#rowVal[58])" />
										<s:hidden name="resolByTCFlag" id="resolByTCFlag" value="%{#rowVal[57]}"></s:hidden>
										<s:hidden name="resolByCBBFlag" id="resolByCBBFlag"	value="%{#rowVal[58]}"></s:hidden>
									</s:elseif>
									</s:iterator>
								</s:if></td>
							</tr>
							<tr>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.ctrequestdate" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test="#rowVal[45]!=null && #rowVal[45]!=''">
											<s:property value="%{#rowVal[45]}" />
										</s:if>
									</s:iterator>
								</s:if></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.assignedto" /></strong>:</td>
								<td width="15%" align="left">
								<s:if test="lstUsers!=null && lstUsers.size()>0">
									<s:hidden name="assignLstSize" id="assignLstSize" value="%{lstUsers.size()}"/>
								</s:if>
								<s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
									<s:hidden name="hAssignTo" id="hAssignTo" value="%{#rowVal[3]}"/>
										<s:if test='#rowVal[3]!=null && #rowVal[3]!=""'>
											<s:if test="lstUsers!=null && lstUsers.size()>0">
												<s:if test="hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true">
													<s:if test="hasEditPermission(#request.assignOrders)!=true">
																	<select id="userIdval" name="userIdval"  style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test='%{#rowVal[3]==#tempVal}' >selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
																	 </select>
															
													</s:if>
													<s:else>
														<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">-Select-</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test='%{#rowVal[3]==#tempVal}' >selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
													    </select>
													</s:else>
												</s:if>
											</s:if>

										</s:if>
										<s:elseif
											test="(hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true) && (#rowVal[3]==null || #rowVal[3]=='')">
											<s:if test="lstUsers!=null && lstUsers.size()>0">
												<s:if test="hasEditPermission(#request.assignOrders)!=true">
													<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
													</select>
												</s:if>
												<s:else>
													<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
													</select>
												</s:else>
											</s:if>
										</s:elseif>
									</s:iterator>
								</s:if> <s:else>

									<s:if
										test="hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true">
										<s:if test="lstUsers!=null && lstUsers.size()>0">
											<s:if test="hasEditPermission(#request.assignOrders)!=true">
												<select id="userIdval" name="userIdval"  style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
													</select>
											</s:if>
											<s:else>
												<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[1]}"/></option>
																			</s:iterator>
												</select>
											</s:else>
										</s:if>
									</s:if>
								</s:else>
								</td>
								<s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if
											test='#rowVal[3]!=null && #rowVal[3]!="" && #rowVal[4]!="Not Provided"'>
											<td width="10%" align="left"><strong><s:text
												name="garuda.currentReqProgress.label.assigneddate" />:</strong></td>
											<td width="15%" align="left"><s:property
												value="%{#rowVal[4]}" /></td>
										</s:if>
										<s:else>
											<td width="10%" align="left"></td>
											<td width="15%" align="left"></td>
										</s:else>
									</s:iterator>
								</s:if>
								<s:else>
									<td width="10%" align="left"></td>
									<td width="15%" align="left"></td>
								</s:else>
								<td width="10%" align="left"><strong><s:text
									name="garuda.heResolution.label.resolutiondate" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="orderResolLst!=null && orderResolLst.size>0">
									<s:iterator value="orderResolLst" var="rowVal">
										<s:if test="#rowVal[2]!=null && #rowVal[2]!=''">
											<s:property value="%{#rowVal[2]}" /> 
										</s:if>
									</s:iterator>
								</s:if></td>
							</tr>
							<tr>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.priority"></s:text>:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test="#rowVal[64]!=null && #rowVal[64]!=''">
											<s:property value="%{#rowVal[64]}" />
										</s:if>
										<s:else>
											<s:text name="garuda.currentReqProgress.label.notprovided" />
										</s:else>
									</s:iterator>
								</s:if><s:else>
									<s:text name="garuda.currentReqProgress.label.notprovided" />
								</s:else></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.lastrequestreviewedby" />:</strong>
								</td>

								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test="#rowVal[5]!=null && #rowVal[5]!=''">
											<s:property value="%{#rowVal[5]}" />
										</s:if>
									</s:iterator>
								</s:if></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.lastrevieweddate" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:hidden name="recRecDateFlag" id="recRecDateFlag"
											value="%{#rowVal[65]}"></s:hidden>
										<s:if test='#rowVal[6]!=null && #rowVal[6]!=""'>
											<s:property value="%{#rowVal[6]}" />
										</s:if>
									</s:iterator>
								</s:if></td>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.closereason" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test="%{#rowVal[75]!=null && #rowVal[75]!=''}">
											<s:property value="getCbuStatusDescByPk(#rowVal[75])" />
										</s:if>
									</s:iterator>
								</s:if></td>
							</tr>
							<tr>
								<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.prevctd" />:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test="%{#rowVal[67]!=null && #rowVal[67]!=''}">
											<s:if test='%{#rowVal[67]=="Y"}'>
												<s:text name="garuda.clinicalnote.label.yes"></s:text>
											</s:if>
											<s:elseif test='%{#rowVal[67]=="N"}'>
												<s:text name="garuda.clinicalnote.label.no"></s:text>
											</s:elseif>
										</s:if>
									</s:iterator>
								</s:if> <s:else>
									<s:text name="garuda.currentReqProgress.label.notprovided" />
								</s:else></td>
								<td width="10%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test='%{#rowVal[2]!=null && #rowVal[2]=="N"}'>
											<strong><s:text
												name="garuda.ctShipmentInfo.label.ctshipdate" />:</strong>
										</s:if>
									</s:iterator>
								</s:if></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test='%{#rowVal[2]!=null && #rowVal[2]=="N"}'>
											<s:if
												test="shipmentInfoLst!=null && shipmentInfoLst.size()>0">
												<s:iterator value="shipmentInfoLst" var="rowVal">
													<s:property value="%{#rowVal[0]}" /><s:hidden name="schshipdate" id="schshipdate" value="%{#rowVal[0]}"></s:hidden>
													<input type="hidden" name="todate" id="todate" value="<%=Utilities.getFormattedDate(new Date(),"MMM dd, yyyy")%>" >
												</s:iterator>
											</s:if>
										</s:if>
									</s:iterator>
								</s:if></td>
								<td width="10%" align="left"></td>
								<td width="15%" align="left"><%--  <button type="button" onclick="window.open('cbuPdfReports?repId=180&repName=NMDP OR Detail Report&selDate=&params=<s:property value="orderId"/>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');" value="Print Order Detail Report"> Print Order Detail Report </button> --%> </td>
								<td width="10%" align="left"><a
									href="#cttcorderdetcontentdiv" onclick="togglePFDivs('cttcorderdetspan','cttcorderdetcontentdiv','getcttcorderdetails')"><strong><s:text
									name="garuda.ctShipmentInfo.label.resultsrecdate" />:</strong></a></td>
								<td width="15%" align="left">
								<s:set name="orderServicesLstflag" value="0" scope="request"/>
								<s:if test="resRecDatesCnt==0">
									<s:if test="orderServicesLst!=null && orderServicesLst.size()>0">
										<s:iterator value="orderServicesLst" var="orderServicesPojo">
											<s:if test='resRecDateStr!=null && !resRecDateStr.equals("") && !resRecDateStr.equals("-") && #request.orderServicesLstflag=="0"'>
												<s:property value="resRecDateStr" />
												<s:set name="orderServicesLstflag" value="1" scope="request"/>
												<br>
											</s:if>
										</s:iterator>
									</s:if>
								</s:if></td>
							</tr>
						</table>
						</div>
						</fieldset>
						</td>
					</tr>
				</table>
			</s:if></div>
			</div>
			</div>
			</s:if>
			</div>
			</div>
			</div>
			<div id="ctassignto" style="display: none;">
								<table bgcolor="#cccccc" class="tabledisplay disable_esign"
									id="ctassigntoTbl">
									<tr bgcolor="#cccccc" valign="baseline">
										<td width="70%"><jsp:include page="cb_esignature.jsp"
											flush="true">
											<jsp:param value="submitAssignto_CT" name="submitId" />
											<jsp:param value="invalidAssignto" name="invalid" />
											<jsp:param value="minimumAssignto" name="minimum" />
											<jsp:param value="passAssignto" name="pass" />
										</jsp:include></td>
										<td align="left" width="30%"><input type="button"
											id="submitAssignto_CT" onclick="submitAssignto();"
											value="<s:text name="garuda.unitreport.label.button.submit"/>"
											disabled="disabled" /> <input type="button" id="closediv"
											onclick="$j('#ctassignto').hide();"
											value="<s:text name="garuda.common.close"/>" /></td>
									</tr>
								</table>
			</div>
 
			</td>
		</tr>
		<tr>
			<td>
			<div id="ctorderdiv">
			<div id="pendingOrdersSearchparent">
			<div id="openOrderSearch">
			<table>
				<tr>
					<td width="49.5%" style="vertical-align: top">
					  <s:if test="hasViewPermission(#request.viewPatienttc)==true">
						<div class="portlet" id="openOrderrecTcInfoparent">
						<div id="openOrderrecTcInfo"
							class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
						<div
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-print"
							onclick="getCaseManagerPatientDetails();addPrintContent('openOrderrecTcInfo')"></span><span
							class="ui-icon ui-icon-newwin"></span> <span id='openOrderrecTcInfospan' class="ui-icon ui-icon-plusthick"  onclick="getCaseManagerPatientDetails();"></span>
						<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
							class="ui-icon ui-icon-minusthick"></span>--> <s:text
							name="garuda.ctOrderDetail.portletname.recipient&tcInfo" /></div>
						<div class="portlet-content"
							id="caseManagerPatientandTCdetailsDiv" style="display: none;"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<table>
									<tr>
										<td colspan="2">
										<table width="100%">
											<tr>
												<td width="25%" align="left"></td>
												<td width="25%" align="left"></td>
												<td width="50%" align="right" colspan="2">
													<input type="button"
														value='<s:text name="garuda.recipient&tcInfo.label.button.viewantigens"/>'
														onclick="showAntigensModal('<s:property value="%{#rowVal[13]}"/>','<s:property value="%{#rowVal[87]}"/>');" />
												</td>

											</tr>
											<tr>
												<td width="25%" align="left"><strong><s:text
													name="garuda.recipient&tcInfo.label.recipientid" /></strong>:</td>
												<td width="25%" align="left"><s:property
													value="%{#rowVal[13]}" /></td>
												<td width="50%" align="right" colspan="2"></td>
											</tr>
											<tr>
												<td width="25%" align="left"><strong><s:text
													name="garuda.recipient&tcInfo.label.age" /></strong>:</td>
												<td width="25%" align="left"><s:property
													value="%{#rowVal[11]}" /></td>
												<td width="25%" align="left"><strong><s:text
													name="garuda.recipient&tcInfo.label.tcname" /></strong>:</td>
												<td width="25%" align="left"><s:property
													value="%{#rowVal[7]}" /></td>
											</tr>
											<tr>
												<td width="25%" align="left"><strong><s:text
													name="garuda.recipient&tcInfo.label.sex" /></strong>:</td>
												<td width="25%" align="left"><s:property
													value="%{#rowVal[12]}" /></td>
												<td width="25%" align="left"><strong><s:text
													name="garuda.recipient&tcInfo.label.tcno" /></strong>:</td>
												<td width="25%" align="left"><s:property
													value="%{#rowVal[8]}" /></td>
											</tr>
											<tr>
												<td width="25%" align="left"><strong><s:text
													name="garuda.tcorderdetails.label.currentdiagnosis" /></strong>:</td>
												<td width="25%" align="left"><s:property
													value="%{#rowVal[86]}" /></td>
												<td width="25%" align="left"><strong><s:text
													name="garuda.recipient&tcInfo.label.sectcno" /></strong>:</td>
												<td width="25%" align="left"><s:property
													value="%{#rowVal[9]}" /></td>
											</tr>
										</table>
										</td>
									</tr>
								</table>
								
							</s:iterator>
						</s:if> <s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<table>
									<tr>
										<td width="25%"><strong><s:text
											name="garuda.caseMangerInfo.label.cmid" /></strong>:</td>
										<td width="25%"><s:property value="%{#rowVal[15]}" /></td>
										<td width="25%"><strong><s:text
											name="garuda.caseMangerInfo.label.cmemail" /></strong>:</td>
										<td width="25%"><a
											href='mailto:<s:property value="%{#rowVal[17]}"/>'><s:property
											value="%{#rowVal[17]}" /></a></td>
									</tr>
									<tr>
										<td width="25%"><strong><s:text
											name="garuda.caseMangerInfo.label.cmname" /></strong>:</td>
										<td width="25%"><s:property value="%{#rowVal[16]}" /></td>
										<td width="25%"><strong><s:text
											name="garuda.caseMangerInfo.label.cmphno" /></strong>:</td>
										<td width="25%"><s:property value="%{#rowVal[18]}" /></td>
									</tr>
									<tr>
										<td width="100%" colspan="4" align="right">
											<input type="button"
												value='<s:text name="garuda.recipient&tcInfo.label.button.cmdirectory"/>'
												onclick="readNMDPXML('1');" />
										</td>
									</tr>
								</table>
							</s:iterator>
						</s:if></div>
						</div>
						</div>
						</s:if>
						
					   <s:if test="hasViewPermission(#request.viewCbuAvailCon)==true">
						<div class="portlet" id="cbuavailconfbarDivparent">
						<div id="cbuavailconfbarDiv"
							class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
						<div
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-print"
							onclick="getCordAvailDetails();addPrintContent('cbuavailconfbarDiv')"></span><span
							class="ui-icon ui-icon-newwin"></span> 
							<span class="ui-icon ui-icon-plusthick" id="cordAvailspan" onclick="getCordAvailDetails();"></span>
						<!--<span class="ui-icon ui-icon-close"></span>--><!-- <span
							class="ui-icon ui-icon-minusthick"></span>--> <s:text
							name="garuda.ctOrderDetail.portletname.cbuAvailconformation" /></div>
						<div class="portlet-content" id="ctcbuavailconfbarDiv" style="display: none;"><s:if
							test="cordAvailConformLst!=null && cordAvailConformLst.size()>0">
							<s:iterator value="cordAvailConformLst" var="rowVal">
								<table>
									<tr>
										<td><strong><s:text
											name="garuda.cbuAvailconformation.label.cbuavailfornmdp" /></strong>:<span
											style="color: red;">*</span></td>
										<td><s:if test="#rowVal[0]!=null && #rowVal[0]!=''">
											<s:if test='%{#rowVal[0]=="Y"}'>
												<s:text name="garuda.cbushipmetInfo.label.yes"></s:text>
											</s:if>
											<s:if test='%{#rowVal[0]=="N"}'>
												<s:text name="garuda.cbushipmetInfo.label.no"></s:text>
											</s:if>
										</s:if><s:hidden id="cordavailflag" value="%{#rowVal[4]}"></s:hidden></td>
									</tr>
									<s:if test='%{#rowVal[0]!=null && #rowVal[0]=="N"}'>
										<tr>
											<td width="50%"><strong><s:text
												name="garuda.cbuAvailconformation.label.unavailreason" />:</strong></td>
											<td width="50%"><s:property value="%{#rowVal[1]}" /></td>
										</tr>
									</s:if>
									<s:if test="#rowVal[4]!=null">
										<tr>
											<td width="50%"><strong><s:text
												name="garuda.cbuAvailconformation.label.cbuavailconfirmdays" />:</strong>
											</td>
											<td width="50%"><s:property value="%{#rowVal[5]}" /></td>
										</tr>
									</s:if>
									<s:if test="%{(#rowVal[2]!=null && #rowVal[2]!='') && (#rowVal[3]!=null && #rowVal[3]!='')}">
									<tr>
										<td width="100%" colspan="2">
										<fieldset><legend> </legend>
										<table>
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.cbuAvailconformation.label.submittedby" /></strong>:</td>
												<td width="25%"><s:property value="%{#rowVal[2]}" /></td>
												<td width="25%"><strong><s:text
													name="garuda.cbuAvailconformation.label.submitteddate" /></strong>:</td>
												<td width="25%"><s:property value="%{#rowVal[3]}" /></td>
											</tr>
										</table>
										</fieldset>
										</td>
									</tr>
									</s:if>
								</table>
								</s:iterator>
								</s:if>
								<table id="ctAvailNotesDiv">
									<tr>
										<td colspan="6"><s:set name="cbuCateg"
											value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT,@com.velos.ordercomponent.util.VelosGarudaConstants@AVAIL_CNFRM_CODESUBTYPE) " />
										<s:iterator value="#request.categoryPfNotes[#cbuCateg]"
											var="rowVal" status="row">
											<s:set name="amendFlag1" value="%{#rowVal[6]}"
												scope="request" />
											<s:set name="tagNote" value="%{#rowVal[7]}" scope="request" />
											<s:set name="notes" value="%{#rowVal[8]}" scope="request" />
											<s:set name="cellValue6" value="cellValue" scope="request" />
											<tr>
												<td><s:if test="#request.tagNote==true">
													<img width="20" height="16" src="images/exclamation.jpg">
												</s:if></td>
												<s:iterator value="rowVal" var="cellValue" status="col">
													<s:if test="%{#col.index == 1}">
														<s:set name="cellValue1" value="cellValue" scope="request" />
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
														<%
														String creatorName="";
														if (request.getAttribute("cellValue1") != null
							 									&& !request
							 											.getAttribute("cellValue1")
							 											.equals("")) {
															creatorName = request.getAttribute(
							 										"cellValue1").toString();
							 							}
															
																						UserJB userB = new UserJB();
																						userB.setUserId(EJBUtil
																								.stringToNum(creatorName));
																						userB.getUserDetails();
														
														%> <%=userB.getUserLastName() + " "
												  + userB.getUserFirstName()%></td>
													</s:if>
													<s:if test="%{#col.index == 2}">
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
															name="garuda.cbu.order.progressNote" /><s:property /></td>
													</s:if>
													<s:if test="%{#col.index == 0}">
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
															name="garuda.clinicalnote.label.posted_on"></s:text> <s:set
															name="cellValue15" value="cellValue" /> <s:date
															name="cellValue15" id="cellValue15"
															format="MMM dd, yyyy / hh:mm:ss a" /> <s:property
															value="cellValue15" /></td>
													</s:if>
													<s:if test="%{#col.index == 3}">

														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<s:text name="garuda.clinicalnote.label.subject"></s:text>:
														<s:property /></td>
													</s:if>
													<s:if test="%{#col.index == 4}">
														
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:set name="cellValue3" value="cellValue"
																	scope="request" /> <s:iterator
																	value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]"
																	var="subType">


																	<%-- <s:if test="subType==#cellValue">
																		<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
																		<s:property value="description" />
																	</s:if> --%>
																</s:iterator>
															
														</td>
													</s:if>

													<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
													<s:if test="%{#col.index == 5}">
														<td><a href="#"
															onClick="loadMoredivs('revokePfCategoryNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&pkcordId=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&orderType=<s:property value="orderType"/>&orderId=<s:property value="orderId"/>&category=<s:property value="#request.cbuCateg"/>','ctAvailNotesDiv,ctProgressNotesTable','ctOrderDetailsForm')">
														<s:text name="garuda.clinicalnote.label.revoke" /></a></td>
													</s:if>
													</s:if>

													<s:if test="%{#col.index == 5}">
														<td><a href="#"
															onClick="fn_showModalCTdetails('Progress Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfViewNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property />','500','850');">
														<s:text name="garuda.clinicalnote.label.viewnotes" /></a></td>
													</s:if>

												</s:iterator>
											</tr>
											<tr>
											<td></td>
													<td colspan = "6" <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
													<div style="overflow: auto;">
															<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
													</div>
													</td>
											<tr>
											
										</s:iterator></td>
										
									</tr>
									
									<tfoot>
										<tr>
											<td colspan="7"></td>
										</tr>
									</tfoot>

								</table>

								
									<a href="#"
										onclick="shwMdal('AVAIL_CNFRM_CODESUBTYPE','<s:property value="cdrCbuPojo.registryId"/>','ctAvailNotesDiv');"><s:text
										name="garuda.cdrcbuview.label.button.addnewnotes" /></a>
							
						<div <s:if test="orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED) || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)">style="display:none"</s:if>>
						<s:if test="hasEditPermission(#request.viewCbuAvailCon)==true">
						<table>
							<tr>
								<s:if test="cordAvailConformLst!=null && cordAvailConformLst.size()>0">
									<s:iterator value="cordAvailConformLst" var="rowVal">
								<s:if test="#rowVal[0]==null || #rowVal[0]==''">
									<td align="right"><input type="button" id="saveCbuavail"
										onclick="savecordAvailNmdp()"
										value="<s:text name="garuda.cbbprocedures.label.edit" />" /></td>
								</s:if>
								</s:iterator>
								</s:if>
							</tr>
						</table>
						</s:if>
						</div>
						</div>
						</div>
						</div>
						</s:if>
						
				 		<s:if test="hasViewPermission(#request.viewreqclincinfo)==true">
						<div class="portlet" id="reqclincinfobarDivparent">
						  
						<div id="reqclincinfobarDiv"
							class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
						<div
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-print"
							onclick="addPrintContent('reqclincinfobarDiv')"></span><span
							class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id="reqclinspanid"></span>
						<!--<span class="ui-icon ui-icon-close"></span>--><!-- <span
							class="ui-icon ui-icon-minusthick"></span>--> <s:text
							name="garuda.ctOrderDetail.portletname.requiredClinicalInfo" /></div>
						<div class="portlet-content" id="ctReqClinInfoContentDiv" style="display: none;">
						<table>
							<tr>
								<td colspan="2"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:hidden name="completeReqInfoTaskFlag"
											id="completeReqInfoTaskFlag" value="%{#rowVal[63]}"></s:hidden>
									</s:iterator>
								</s:if>
								<table width="100%">
									<tr>
										<td width="25%"><strong><s:text
											name="garuda.requiredClinicalInfo.label.checkliststatus" /></strong>:</td>
										<td width="25%"><s:hidden name="clinchkstat" /> <s:if
											test="clinchkstat==null">
											<label style="color: red;"><s:text
												name="garuda.requiredClinicalInfo.label.notstarted" /></label>
										</s:if> <s:elseif test="clinchkstat==0">
											<label style="color: blue;"><s:text
												name="garuda.requiredClinicalInfo.label.pending" /></label>
										</s:elseif> <s:elseif test="clinchkstat==1">
											<label style="color: green;"><s:text
												name="garuda.requiredClinicalInfo.label.completed" /></label>
										</s:elseif></td>
										  <s:if test="hasViewPermission(#request.cmpltReqInfo)==true">
											<td width="50%" colspan="2"><strong><a href="#"
												onclick="f_callCompleReqInfo('<s:property value="cdrCbuPojo.registryId"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="orderId"/>','<s:property value="orderType"/>');"><s:text
												name="garuda.requiredClinicalInfo.label.clinicalinfostatus" /></a></strong></td>
								         </s:if>
									</tr>
									<tr>
										<td width="25%"></td>
										<td width="25%"></td>
										<td width="25%"></td>
										<td width="25%"></td>
									</tr>
								</table>
								</td>

							</tr>
						</table>
						</div>
						</div>
						</div>
						</s:if>
					 <s:if test="hasViewPermission(#request.viewCTshipmnt)==true">
					 <s:if test='sampleAtLab!=null && sampleAtLab=="N"'>			
						<div class="portlet" id="ctshipmentbarDivparent">
						<div id="ctshipmentbarDiv"
							class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
						<div
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-print"
							onclick="getctshipmentDetails();addPrintContent('ctShipmentInfoContentDiv')"></span><span
							class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id="ctshipmentspan" onclick="getctshipmentDetails();"></span>
						<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
							class="ui-icon ui-icon-minusthick"></span>--> <s:text
							name="garuda.ctOrderDetail.portletname.ctShipmentInfo" /></div>
						<div class="portlet-content shipmentInfoTbl" id="ctShipmentInfoContentDiv" style="display: none;"><s:hidden
							name="pkCbuShipment" id="pkCbuShipment"></s:hidden> <s:if
							test="shipmentInfoLst!=null && shipmentInfoLst.size()>0">
							<s:iterator value="shipmentInfoLst" var="rowVal">
								<s:hidden name="prevSampleType" id="prevSampleType"
									value="%{#rowVal[5]}"></s:hidden>
								<s:hidden name="prevAliqType" id="prevAliqType"
									value="%{#rowVal[6]}"></s:hidden>
								<s:hidden name="dataModifiedFlag" id="dataModifiedFlag"
									value="%{#rowVal[24]}"></s:hidden>
								<s:hidden name="pkShipmentId" id="pkShipmentId"
									value="%{#rowVal[25]}"></s:hidden>
								<s:hidden name="pkPackingSlip" id="pkPackingSlipId"></s:hidden>
								<s:hidden name="timediff" id="timediff" value="%{#rowVal[31]}"></s:hidden>
								<table>
									<tr>
										<td>
										<table width="100%">
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.ctShipmentInfo.label.ctshipdate" /></strong>:<span
													style="color: red;">*</span></td>
												<td width="25%"><s:date name="ctshipDate"
													id="ctShipDateId" format="MMM dd, yyyy" /> <s:hidden
													name="ctShipDateId" id="ctShipDateId" value="%{#rowVal[0]}" />
												<s:property value="%{#rowVal[0]}" /></td>
												<td width="25%"><s:if
													test="orderPojo.resolByTc!=null && orderPojo.resolByTc!='' && pkCanceled!=null && pkCanceled!='' && orderPojo.resolByTc==pkCanceled">
													<span
														style="font-style: italic; color: red; font-size: 10px"><s:text
														name="garuda.ctshipmetInfo.label.shipmentcancelled"></s:text></span>
												</s:if> <s:else>
													<strong><s:text
														name="garuda.cbushipmetInfo.label.day"></s:text></strong>:
													</s:else></td>
												<td width="25%" id="ctshimentdayDiv"><s:if
													test="orderPojo.resolByTc!=null && orderPojo.resolByTc!='' && pkCanceled!=null && pkCanceled!='' && orderPojo.resolByTc==pkCanceled">
													<strong><s:text
														name="garuda.cbushipmetInfo.label.day"></s:text></strong>:
					    						</s:if> <span style="font-style: italic; color: blue"></span></td>
											</tr>
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.ctShipmentInfo.label.shippingcompany" /></strong>:</td>
												<td width="25%"><s:hidden
													name="shipmentPojo.fkshipingCompanyId" id="shipCompany"
													value="%{#rowVal[2]}"></s:hidden> <s:property
													value="getCodeListDescById(#rowVal[2])" /></td>
												<td width="25%"><strong><s:text
													name="garuda.ctShipmentInfo.label.trackingno" /></strong>:</td>
												<td width="25%"><s:hidden
													name="shipmentPojo.shipmentTrackingNo" id="trackNo"
													value="%{#rowVal[3]}"></s:hidden><a href="#"
													onclick='connectToShipperWebsite("<s:property value='%{#rowVal[2]}'/>","<s:property value='%{#rowVal[3]}'/>");'><s:property
													value="%{#rowVal[3]}" /></a></td>
											</tr>
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.ctShipmentInfo.label.sampletypeavail" /></strong>:<img id="sampleInventoryhelp"
													height="15px" src="./images/help_24x24px.png"
													onclick="readNMDPXML('4')"></td>
												<td width="25%"><s:hidden name="SamTypeAvailable"
													id="SamTypeAvailable" value="%{#rowVal[5]}"></s:hidden> <s:property
													value="getCodeListDescById(#rowVal[5])" /></td>
												<td width="50%" colspan="2">
											 <s:if test="hasEditPermission(#request.viewCTshipmnt)==true">
												<strong><a
													href="#" onclick="openModalForSampleInventory();"><s:text
													name="garuda.ctShipmentInfo.label.updatesampleinventory"/></a></strong>
											</s:if>
											<s:else>
												<strong><a
													href="#" onclick=""><s:text
													name="garuda.ctShipmentInfo.label.updatesampleinventory" /></a></strong>
											</s:else>		
												</td>
											</tr>
											<tr>
												<td width="100%" colspan="4"><s:if
													test="%{#rowVal[5]==pkaliquots}">
													<div id="aliquotstypeDiv">
													<table width="100%">
														<tr>
															<td width="25%"><strong><s:text
																name="garuda.ctShipmentInfo.label.aliquotstype" /></strong>:<span
																style="color: red;">*</span></td>
															<td width="25%"><s:hidden name="AliquotsType"
																id="AliquotsType" value="%{#rowVal[6]}"></s:hidden> <s:property
																value="getCodeListDescById(#rowVal[6])" /></td>
															<td width="25%"></td>
															<td width="25%"></td>
														</tr>
													</table>
													</div>
												</s:if></td>
											</tr>
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.ctResolution.label.labcode"></s:text>:</strong><img id="labCodehelp"
													height="15px" src="./images/help_24x24px.png"
													onclick="readNMDPXML('5')"></td>
												<td width="25%"><s:property
													value="getCodeListDescById(#rowVal[21])" />
												</td>
												<td width="25%"><strong><s:text
													name="garuda.ctResolution.label.noofdaystoshipct"></s:text></strong>:
												</td>
												<td width="25%"><s:property value="%{#rowVal[22]}" /></td>
											</tr>
											<tr>
												<td width="50%" colspan="2" align="left">
													<input type="button"
														value='<s:text name="garuda.ctResolution.label.viewctinstructions"/>'
														onclick="readNMDPXML('3')" />
												</td>
												<td width="50%" colspan="2" id="genpackageslipbarDiv" align="right">
														<s:if test="%{#rowVal[0]!=null && orderPojo.resolByTc!=pkCanceled}">
														<div <s:if test="(orderPojo.iscordavailfornmdp==@com.velos.ordercomponent.business.util.VelosMidConstants@FLAG_N || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)) || (orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED))">style="display:none"</s:if>>
															<input type="button" value=<s:text name="garuda.ctShipmentInfo.button.label.createpackageslip"/> onclick="createPackageSlip()" />
														</div>
													</s:if>
												</td>
											</tr>
										</table>
										</td>
									</tr>
									<s:hidden id="shipschFlag" name="shipschFlag" value="%{#rowVal[11]}"></s:hidden>
									<s:hidden id="packageslipFlag" value="%{#rowVal[13]}"></s:hidden>
								</table>
							</s:iterator>
						</s:if> <s:else>
							<table>
								<tr>
									<td>
									<table width="100%">
										<tr>
											<td width="25%"><strong><s:text
												name="garuda.ctShipmentInfo.label.ctshipdate" /></strong>:<span
												style="color: red;">*</span></td>
											<td width="25%"></td>
											<td width="25%"><strong><s:text
												name="garuda.cbushipmetInfo.label.day"></s:text></strong>:</td>
											<td width="25%"></td>
										</tr>
										<tr>
											<td width="25%"><strong><s:text
												name="garuda.ctShipmentInfo.label.shippingcompany" /></strong>:</td>
											<td width="25%"></td>
											<td width="25%"><strong><s:text
												name="garuda.ctShipmentInfo.label.trackingno" /></strong>:</td>
											<td width="25%"></td>
										</tr>
										<tr>
											<td width="25%"><strong><s:text
												name="garuda.ctShipmentInfo.label.sampletypeavail" /></strong>:<img id="sampleInventoryhelp"
												height="15px" src="./images/help_24x24px.png"
												onclick="readNMDPXML('4')"></td>
											<td width="25%"></td>
											<td width="25%"><strong><s:text
												name="garuda.ctResolution.label.labcode"></s:text></strong>:<img id="labCodehelp"
												height="15px" src="./images/help_24x24px.png"
												onclick="readNMDPXML('5')"></td>
											<td width="25%"></td>
										</tr>

										<tr>
											<td width="50%" colspan="2" align="left"></td>
											<td width="50%" colspan="2" align="right"></td>
										</tr>
									</table>
									</td>
								</tr>

							</table>
							
							
						</s:else>
						<table id="ctShipmentNotesTable">
									<tbody>
										<s:set name="cbuCateg"
											value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT,@com.velos.ordercomponent.util.VelosGarudaConstants@CT_SHHIPMENT_CODESUBTYPE) " />
										<s:iterator value="#request.categoryPfNotes[#cbuCateg]"
											var="rowVal" status="row">
											<s:set name="amendFlag1" value="%{#rowVal[6]}"
												scope="request" />
											<s:set name="tagNote" value="%{#rowVal[7]}" scope="request" />
											<s:set name="notes" value="%{#rowVal[8]}" scope="request" />
											<s:set name="cellValue6" value="cellValue" scope="request" />
											<tr>
												<td><s:if test="#request.tagNote==true">
													<img width="20" height="16" src="images/exclamation.jpg">
												</s:if></td>
												<s:iterator value="rowVal" var="cellValue" status="col">
													<s:if test="%{#col.index == 1}">
														<s:set name="cellValue1" value="cellValue" scope="request" />
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
														String creatorName="";
														if (request.getAttribute("cellValue1") != null
							 									&& !request
							 											.getAttribute("cellValue1")
							 											.equals("")) {
															creatorName = request.getAttribute(
							 										"cellValue1").toString();
							 							}
															
																						UserJB userB = new UserJB();
																						userB.setUserId(EJBUtil
																								.stringToNum(creatorName));
																						userB.getUserDetails();
														
														%> <%=userB.getUserLastName() + " "
												  + userB.getUserFirstName()%></td>
													</s:if>
													<s:if test="%{#col.index == 2}">
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
															name="garuda.cbu.order.progressNote" /><s:property /></td>
													</s:if>
													<s:if test="%{#col.index == 0}">
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
															name="garuda.clinicalnote.label.posted_on"></s:text> <s:set
															name="cellValue15" value="cellValue" /> <s:date
															name="cellValue15" id="cellValue15"
															format="MMM dd, yyyy / hh:mm:ss a" /> <s:property
															value="cellValue15" /></td>
													</s:if>
													<s:if test="%{#col.index == 3}">

														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<s:text name="garuda.clinicalnote.label.subject"></s:text>:
														<s:property /></td>
													</s:if>
													<s:if test="%{#col.index == 4}">
														
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:set name="cellValue3" value="cellValue"
																	scope="request" /> <s:iterator
																	value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]"
																	var="subType">


																	<%-- <s:if test="subType==#cellValue">
																		<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
																		<s:property value="description" />
																	</s:if> --%>
																</s:iterator>
															
														</td>
													</s:if>

													<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
													<s:if test="%{#col.index == 5}">
														<td><a href="#"
															onClick="loadMoredivs('revokePfCategoryNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&pkcordId=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&orderType=<s:property value="orderType"/>&orderId=<s:property value="orderId"/>&category=<s:property value="#request.cbuCateg"/>','ctShipmentNotesTable,ctProgressNotesTable','ctOrderDetailsForm')">
														<s:text name="garuda.clinicalnote.label.revoke" /></a></td>
													</s:if>
													</s:if>

													<s:if test="%{#col.index == 5}">
														<td><a href="#"
															onClick="fn_showModalCTdetails('Progress Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfViewNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property />','500','850');">
														<s:text name="garuda.clinicalnote.label.viewnotes" /></a></td>
													</s:if>

												</s:iterator>
											</tr>
											<tr>
											<td></td>
													<td colspan = "6" <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														 <div style="overflow: auto;">
															<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
													       </div>
													</td>
											<tr>
										</s:iterator>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="6"><a href="#"
												onclick="shwMdal('CT_SHHIPMENT_CODESUBTYPE','<s:property value="cdrCbuPojo.registryId"/>','ctShipmentNotesTable');">
											<s:text name="garuda.cdrcbuview.label.button.addnewnotes" /></a>
											</td>
										</tr>
									</tfoot>

								</table>
								<s:if
								test="(recRecDateFlag==null || recRecDateFlag=='Not Available' || recRecDateFlag=='') && orderPojo.resolByTc!=pkCanceled && hasEditPermission(#request.viewCTshipmnt)==true">
								<table>
									<tr>			  
										<td align="right"><div <s:if test="(orderPojo.iscordavailfornmdp==@com.velos.ordercomponent.business.util.VelosMidConstants@FLAG_N || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)) || (orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED))">style="display:none"</s:if>><input type="button" id="editCtshipment"
											onclick="openmodaltoeditshipment();"
											value="<s:text name="garuda.common.lable.edit"/>" /></div></td>		
									</tr>
								</table>
							</s:if>
						</div>
						</div>
						</div>
			             </s:if>
						</s:if>
				 <s:if test="hasViewPermission(#request.viewTCOrdrDetail)==true">
					<div class="portlet" id="cttcorderdetailsDivparent">
					<div id="cttcorderdetailsDiv"
						class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
					<div
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-print"
						onclick="getcttcorderdetails();addPrintContent('cttcorderdetailsDiv')"></span><span
						class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id="cttcorderdetspan" onclick="getcttcorderdetails();"></span>
					<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
						class="ui-icon ui-icon-minusthick"></span> --><s:text
						name="garuda.orOrderDetail.portletname.tcorderdetails" /></div>
					<div class="portlet-content" id="cttcorderdetcontentdiv" style="display: none;">
					<table border="0" align="left" cellpadding="0" cellspacing="0"
						class="displaycdr" id="searchResults">
						<thead>
							<tr>
								<th><s:text name="garuda.tcorderdetails.label.servicesreq" /></th>
								<th><s:text name="garuda.tcorderdetails.label.reqdate" /></th>
								<th><s:text name="garuda.tcorderdetails.label.recrecdate" /></th>
								<th><s:text name="garuda.tcorderdetails.label.canceldate" /></th>
								<th><s:text name="" /></th>
							</tr>
						</thead>
						<tbody>
							<s:if test="orderServicesLst!=null && orderServicesLst.size()>0">
								<s:iterator value="orderServicesLst" var="orderServicesPojo">
									<tr>
										<td><s:property value="serviceCode" /></td>
										<td><s:property value="requestDateStr" /></td>
										<td><s:property value="resRecDateStr" /></td>
										<td><s:property value="cancelledDateStr" /></td>
										<s:if test='resRecFlag!=null && resRecFlag!=""'>
											<td><img alt="Results Received Flag"
												src="images/tick_mark.gif"></td>
										</s:if>
										<s:elseif test='cancelledFlag!=null && cancelledFlag!=""'>
											<td><img alt="Service Cancellation Flag"
												src="images/delete.gif"></td>
										</s:elseif>
										<s:else>
											<td></td>
										</s:else>

									</tr>
								</s:iterator>
							</s:if>
							<s:else>
								<tr>
									<td colspan="5" align="center"><s:text
										name="garuda.tcorderdetails.label.noservices"></s:text></td>

								</tr>
							</s:else>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5"></td>
							</tr>
						</tfoot>
					</table>
					</div>
					</div>
					</div>
					</s:if>
					</td>
					<td width="1%"></td>
					<td width="49.5%" style="vertical-align: top">
					    <s:if test="hasViewPermission(#request.viewAddTypbyCBB)==true">
						<div class="portlet" id="additityingandtestDivparent">
						<div id="additityingandtestDiv"
							class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
						<div
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-print"
							onclick="getAdditiTypingDet();getContentToPrint('additityingandtestDiv');"></span><span
							class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id="additiTypingspan" onclick="getAdditiTypingDet();"></span>
						<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
							class="ui-icon ui-icon-minusthick"></span> --><s:text
							name="garuda.ctOrderDetail.portletname.addTyping&Testing" /></div>
						<div class="portlet-content" id="additityingandtestcontentDiv" style="display: none;">
						<s:hidden name="hlaLoadedflag" id="hlaLoadedflag" value="0"></s:hidden>
						<table>
							<tr>
								<td>
								<table width="100%">
									<tr>
										<td width="50%" colspan="2"><strong><s:text
											name="garuda.addTyping&Testing.label.recenthlatypavail" /></strong><span
											style="color: red;">*</span></td>
										<td width="50%" colspan="2">
										   <s:if test="hasEditPermission(#request.viewAddTypbyCBB)==true">
											<s:if test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
											<s:iterator value="ctOrderDetailsList" var="rowVal">
												<s:hidden name="recHlaEnteredFlag" id="recHlaEnteredFlag"
													value="%{#rowVal[60]}"></s:hidden>
												<s:hidden name="hlaTypingAvailFlag" id="hlaTypingAvailFlag"
													value="%{#rowVal[59]}"></s:hidden>
												<s:hidden name="AdditiTypingFlagValue"
													id="AdditiTypingFlagValue" value="%{#rowVal[54]}"></s:hidden>
												<s:hidden name="recentHlaId" id="recentHlaId"
													value="%{#rowVal[66]}"></s:hidden>
											   
												<s:if test="%{#rowVal[59]!=null && #rowVal[59]!=''}">
													<s:if test='%{#rowVal[59]=="Y"}'>
														<s:text name="garuda.clinicalnote.label.yes"></s:text>
													</s:if>
													<s:if test='%{#rowVal[59]=="N"}'>
														<s:text name="garuda.clinicalnote.label.no"></s:text>
													</s:if>
												</s:if>
											</s:iterator>
										</s:if> 
										</s:if>
										</td>
									</tr>
									<s:if test="%{#rowVal[47]!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED) && #rowVal[47]!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED) && #rowVal[76]!=@com.velos.ordercomponent.business.util.VelosMidConstants@FLAG_N}">
										<s:if test="hasEditPermission(#request.viewAddTypbyCBB)==true">
										<tr id="ctAdditiTypingsubmitDiv">
											<td width="100%" colspan="4" align="right">
												<input type="button" id="saveAdditiTyping"
													onclick="openmodalforAdditiTyping();"
													value="<s:text name="garuda.cbbprocedures.label.edit" />"/>
											</td>
										</tr>
									   </s:if>	
									</s:if>
									<tr>
										<td width="100%" colspan="4">
										<div class="portlet" id="currentHlaTypingparent">
										<div id="currentHlaTypingstaus"
											class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
										<div onclick="toggleDiv('currentHlaTyping');" id="currentHlaTypingcontent"
											class="portlet-header ui-widget-header ui-widget-content ui-corner-all toggleHla"
											style="text-align: center;"><span
											class="ui-icon ui-icon-triangle-1-e"></span><!--<span
																	class="ui-icon ui-icon-close"></span>--><s:text
											name="garuda.addTyping&Testing.label.currenthlatyping"></s:text>
										</div>
										<div id="currentHlaTyping" class="portlet-content" style="display: none;">
										<div id="searchTble">
										<table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="currenthlaTbl">
						 <thead>
						   <tr>
						      <th></th>			   			   
							   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							       <th>
							          <s:property value="description.replace('HLA-','')"/>
							       </th>				     
							   </s:iterator>
						   </tr>
						 </thead>			 
						 <tbody>
						    <tr>
							    <td>
									 <s:text name="garuda.cbu.label.besthla"/>								            
								</td>								    
								  <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							            <s:iterator value="#request.bestHlas" var="bhla">
									             <s:if test="pkCodeId==fkHlaCodeId">
									                 <s:set name="bhlaCheck" value="%{'true'}" />
										             <td>
											            <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
										                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>	  
												     </td>	
											      </s:if>				      										      	        	
									        </s:iterator> 
									        <s:if test="%{#bhlaCheck!='true'}">
											         <td></td>
											</s:if>	
											<s:set name="bhlaCheck" value="%{'false'}" />
								  </s:iterator>
							</tr>
						   </tbody>
						   <tfoot>
							    <tr>
									<td colspan="13"></td>
								</tr>
							</tfoot>
						</table>
<!-- 						<table width="100%" style="padding-top: 50px;">
						      <tr>
						         <td></td>
						      </tr>
						</table> -->
						<table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="hlaTblOther">
							 <thead>
							   <tr>
							      <!--<th><s:text name="garuda.cbuentry.label.typingdate"/></th>	
							      --><th><s:text name="garuda.cbuentry.label.recieveddate"/></th>
							      <th><s:text name="garuda.cbuentry.label.source"/></th>		   			   
								   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
								       <th>
								          <s:property value="description.replace('HLA-','')"/>
								       </th>				     
								   </s:iterator>
							   </tr>
							 </thead>			 
							 <tbody>	
							    <s:iterator value="#request.cbuHlaMap">
							        <s:set name="cbuHlaMapKey" value="key" />
							        <tr>				
							        <!--<td>
							            <s:if test="#request.cbuHlaMap[#cbuHlaMapKey][0].hlaTypingDate !=null">
							                <s:set name="cbuhlatypedate" value="#request.cbuHlaMap[#cbuHlaMapKey][0].hlaTypingDate" />
									        <s:date name="cbuhlatypedate" id="cbuhlatypedate" format="MMM dd, yyyy" />
									        <s:property value="%{#request.cbuhlatypedate}"/>									       
							            </s:if>									        
							        </td>-->
							        <td>
							            <s:if test="#request.cbuHlaMap[#cbuHlaMapKey][0].hlaRecievedDate !=null">
									        <s:set name="cbuhlarecievedate" value="#request.cbuHlaMap[#cbuHlaMapKey][0].hlaRecievedDate" />
									        <s:date name="cbuhlarecievedate" id="cbuhlarecievedate" format="MMM dd, yyyy" />
									        <s:property value="%{#request.cbuhlarecievedate}"/>									        
									    </s:if>
							        </td>
							        <td>
							           <!--<s:if test="#request.cbuHlaMap[#cbuHlaMapKey][0].fkSource !=null && #request.cbuHlaMap[#cbuHlaMapKey][0].fkSource!=''">
							               <s:set name="cbuhlasource" value="#request.cbuHlaMap[#cbuHlaMapKey][0].fkSource" />
								           <s:property value="getCodeListDescById(#cbuhlasource)"/>
								       </s:if>
								       --><s:if test="#request.cbuHlaMap[#cbuHlaMapKey][0].createdBy !=null && #request.cbuHlaMap[#cbuHlaMapKey][0].createdBy!=''">
							               <s:set name="cbuhlacreator" value="#request.cbuHlaMap[#cbuHlaMapKey][0].createdBy" />
								           <s:property value="getUserNameById(#cbuhlacreator)"/>
								       </s:if>
							        </td>
							        <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">					       
								            <s:iterator value="#request.cbuHlaMap[#cbuHlaMapKey]" var="bhla">
									             <s:if test="pkCodeId==fkHlaCodeId">
									                 <s:set name="hlaCheck" value="%{'true'}" />
										             <td>
											            <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
										                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>	  
												     </td>	
											      </s:if>				      										      	        	
									        </s:iterator> 
									        <s:if test="%{#hlaCheck!='true'}">
											         <td></td>
											</s:if>	
											<s:set name="hlaCheck" value="%{'false'}" />	    								          
									 </s:iterator> 
									 </tr>
								 </s:iterator>
							   </tbody>
							   <tfoot>
								    <tr>
										<td colspan="13"></td>
									</tr>
								</tfoot>
			              </table>
										
										</div>
										</div>
										</div>
										</div>
										</td>
									</tr>
									
										<tr>
										  <s:if test="hasEditPermission(#request.viewAddTypbyCBB)==true">
											<td width="50%" colspan="2" align="left"><strong><a
												href="#" onclick="openModalForLabSummary();"><s:text
												name="garuda.addTyping&Testing.label.updatecbutesting" /></a></strong></td>
											<td width="50%" colspan="2" align="right"><strong><a
												href="#" onclick="openModalforHlaTyping('link');"><s:text
												name="garuda.addTyping&Testing.label.updatecbuhlatyping" /></a></strong></td>
										 </s:if>
										</tr>
									
								</table>
								</td>

							</tr>
						</table>
						</div>
						</div>
						</div>
						</s:if>
						
					       <s:if test="hasViewPermission(#request.viewCTResolution)==true">
						<div class="portlet" id="ctresolutionbarDivparent">
						<div id="ctresolutionbarDiv"
							class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
						<div
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-print"
							onclick="getResolutionDetails('ctresolsubDiv');addPrintContent('ctresolsubDiv')"></span><span
							class="ui-icon ui-icon-newwin"></span> <span
						class="ui-icon ui-icon-plusthick" id="ctResoultionspan" onclick="getResolutionDetails('ctresolsubDiv');"></span>
						<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
							class="ui-icon ui-icon-minusthick"></span> --><s:text
							name="garuda.ctOrderDetail.portletname.ctResolution" /></div>
						<div id="ctresolsubDiv" class="portlet-content" style="display: none;"><s:hidden
							name="cancelled_ack_orders" id="canackorders"></s:hidden><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
							</s:iterator>
							
						</s:if> <s:if test="orderResolLst!=null && orderResolLst.size>0">
							<s:iterator value="orderResolLst" var="rowVal">
								<s:hidden name="acknowledgeFlag" id="acknowledgeFlag"
									value="%{#rowVal[9]}"></s:hidden>
								<table width="100%">
									<tr>
										<td width="40%"><strong><s:text
											name="garuda.currentReqProgress.label.closereason" /></strong>:</td>
										<td width="40%"><s:if
											test="%{#rowVal[10]!=null && #rowVal[10]!=''}">
											<s:property value="getCbuStatusDescByPk(#rowVal[10])" />
											<s:hidden name="orderCloseRsn" id="orderCloseRsn"
												value="%{#rowVal[10]}"></s:hidden>
										</s:if></td>
										<td width="20%"></td>
									</tr>
									<tr>
										<td colspan="3">
										<table cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="40%"><strong><s:text
													name="garuda.heResolution.label.resolutiondate" /></strong>:</td>
												<td width="40%"><s:if
													test="%{#rowVal[2]!=null && #rowVal[2]!=''}">
													<s:property value="%{#rowVal[2]}" />
												</s:if></td>
												<td width="20%"></td>
											</tr>
										</table>
										</td>
									</tr>

									<tr>
										<td colspan="3"><s:if
											test="%{#rowVal[1]!=null && #rowVal[1]!=''}">
											<table id="resolBytc">
												<tr>
													<td width="40%"><strong><s:text
														name="garuda.ctResolution.label.resolutionbytc" /></strong>:</td>
													<td width="40%"><s:if
														test="%{#rowVal[1]!=null && #rowVal[1]!=''}">
														<s:hidden name="tcResolVal" id="tcResolVal"
															value="%{#rowVal[1]}" />
														<s:hidden name="ResolutionTC" id="ResolutionTC"
															value="%{#rowVal[1]}"></s:hidden>
														<s:property value="getCbuStatusDescByPk(#rowVal[1])" />
													</s:if></td>
													<td width="20%"></td>
												</tr>
												<tr>
													<td width="40%"><strong><s:text
														name="garuda.ctResolution.label.acknowledgetcresolution" /></strong>:<span
														style="color: red;">*</span></td>
													<td width="40%"><s:if test='%{#rowVal[9]!=null && #rowVal[9]=="Y"}'>
															<s:checkbox name="ResoluAck"
															id="ResoluAck" disabled="true" checked="true"/><s:text
															name="garuda.ctResolution.label.resolutionbytc" /> <s:hidden
															name="acknowledgeFlag" id="acknowledgeFlag"
															value="%{#rowVal[9]}"></s:hidden>
														</s:if>
														<s:else>
															<s:checkbox name="ResoluAck"
															id="ResoluAck" disabled="true" /><s:text
															name="garuda.ctResolution.label.resolutionbytc" />
														</s:else></td>
													<td width="20%"></td>
												</tr>


												<s:if test="%{#rowVal[9]!=null && #rowVal[9]!=''}">
													<tr>
														<td width="40%"><strong><s:text
															name="garuda.cbu.order.ackDateTime" /></strong>:</td>
														<td width="40%"><s:if
															test="%{#rowVal[6]!=null && #rowVal[6]!=''}">
															<s:property value="%{#rowVal[6]}" />
														</s:if></td>
														<td width="20%"></td>
													</tr>
													<tr>
														<td width="40%"><strong><s:text
															name="garuda.cbu.order.ackBy" /></strong>:</td>
														<td width="40%">
														<s:if test="%{#rowVal[7]!=null && #rowVal[7]!='' && #rowVal[7]!=0 }">
														<s:set name="username"
															value="%{#rowVal[7]}" scope="request" /> <%
 	String cellValue1 = "";
 							if (request.getAttribute("username") != null
 									&& !request
 											.getAttribute("username")
 											.equals("")) {
 								cellValue1 = request.getAttribute(
 										"username").toString();
 							}
 							UserJB userB = new UserJB();
 							userB.setUserId(EJBUtil
 									.stringToNum(cellValue1));
 							userB.getUserDetails();
 %> <%=userB.getUserLastName() + " "
											+ userB.getUserFirstName()%>
														</s:if>
														<s:if test="%{#rowVal[7]==null || #rowVal[7]==0}">
														  <s:text name="garuda.cbuhistory.label.system" />
														</s:if>
														</td>
														<td width="20%"></td>
													</tr>
												</s:if>
												<s:else>
													<%
														String cellValue1 = "";
																				if (request.getAttribute("username") != null
																						&& !request
																								.getAttribute("username")
																								.equals("")) {
																					cellValue1 = request.getAttribute(
																							"username").toString();
																				}
																				UserJB userB = new UserJB();
																				userB.setUserId(EJBUtil
																						.stringToNum(cellValue1));
																				userB.getUserDetails();
													%>
													<input type="hidden" name="currentuser" id="currentuserid"
														value=<%=logUsr%>>
												</s:else>



											</table>

										</s:if> <s:elseif
											test="%{ {{#rowVal[1]==null || #rowVal[1]==''} && {#rowVal[0]==null || #rowVal[0]==''}} || {#rowVal[0]!=null && #rowVal[0]!=''} }">
											<table cellpadding="0" cellspacing="0" border="0"
												id="resolByCbb">
												<tr>
													<td width="40%"><strong><s:text
														name="garuda.ctResolution.label.resolutionbycbb" /></strong>:</td>
													<td width="40%"><s:if
														test="%{#rowVal[0]!=null && #rowVal[0]!=''}">
														<s:property value="getCbuStatusDescByPk(#rowVal[0])" />
													</s:if></td>
													<td width="20%"></td>
													<s:hidden name="hresolcbb" id="hresolcbb"
														value="%{#rowVal[0]}" />
												</tr>
												<s:if test="%{#rowVal[0]!=null && #rowVal[0]!=''}">
													<tr>
														<td width="40%"><strong><s:text
															name="garuda.ctResolution.label.acknowledgetcresolution" /></strong>:<span
															style="color: red;">*</span></td>
														<td width="40%">
														<s:if test='%{#rowVal[9]!=null && #rowVal[9]=="Y"}'>
															<s:checkbox name="ResoluAck"
															id="ResoluAck" disabled="true" checked="true"/><s:text
															name="garuda.ctResolution.label.resolutionbytc" /> <s:hidden
															name="acknowledgeFlag" id="acknowledgeFlag"
															value="%{#rowVal[9]}"></s:hidden>
														</s:if>
														<s:else>
															<s:checkbox name="ResoluAck"
															id="ResoluAck" disabled="true" /><s:text
															name="garuda.ctResolution.label.resolutionbytc" />
														</s:else>
														</td>
														<td width="20%"></td>
													</tr>
												</s:if>
												<s:if test="%{#rowVal[9]!=null && #rowVal[9]!=''}">
													<tr>
														<td width="40%"><strong><s:text
															name="garuda.cbu.order.ackDateTime" /></strong>:</td>
														<td width="40%"><s:if
															test="%{#rowVal[6]!=null && #rowVal[6]!=''}">
															<s:property value="%{#rowVal[6]}" />
														</s:if></td>
														<td width="20%"></td>
													</tr>
													<tr>
														<td width="40%"><strong><s:text
															name="garuda.cbu.order.ackBy" /></strong>:</td>
														<td width="40%">
														<s:if test="%{#rowVal[7]!=null && #rowVal[7]!='' && #rowVal[7]!=0 }">
														<s:set name="username"
															value="%{#rowVal[7]}" scope="request" /> <%
 															String cellValue1 = "";
 															if (request.getAttribute("username") != null
 									&& !request
 											.getAttribute("username")
 											.equals("")) {
 								cellValue1 = request.getAttribute(
 										"username").toString();
 							}
 							UserJB userB = new UserJB();
 							userB.setUserId(EJBUtil
 									.stringToNum(cellValue1));
 							userB.getUserDetails();
 %> <%=userB.getUserLastName() + " "
											+ userB.getUserFirstName()%>
											</s:if>
											<s:if test="%{#rowVal[7]==null || #rowVal[7]=='' || #rowVal[7]==0 }">
											<s:text name="garuda.cbuhistory.label.system" />
											</s:if>
														</td>
														<td width="20%"></td>
													</tr>
												</s:if>
												<s:else>
													<%
														String cellValue1 = "";
																				if (request.getAttribute("username") != null
																						&& !request
																								.getAttribute("username")
																								.equals("")) {
																					cellValue1 = request.getAttribute(
																							"username").toString();
																				}
																				UserJB userB = new UserJB();
																				userB.setUserId(EJBUtil
																						.stringToNum(cellValue1));
																				userB.getUserDetails();
													%>
													<input type="hidden" name="currentuser" id="currentuserid"
														value=<%=logUsr%>>
												</s:else>
											</table>
										</s:elseif></td>
									</tr>
								</table>
							</s:iterator>
						</s:if>
						<table id="ctResolutionTable">
								<tbody>
									<s:set name="cbuCateg"
										value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT,@com.velos.ordercomponent.util.VelosGarudaConstants@RESOLUTION_CODESUBTYPE) " />
									<s:iterator value="#request.categoryPfNotes[#cbuCateg]"
										var="rowVal" status="row">
										<s:set name="amendFlag1" value="%{#rowVal[6]}"
											scope="request" />
										<s:set name="tagNote" value="%{#rowVal[7]}" scope="request" />
										<s:set name="notes" value="%{#rowVal[8]}" scope="request" />
										<s:set name="cellValue6" value="cellValue" scope="request" />
										<tr>
											<td><s:if test="#request.tagNote==true">
												<img width="20" height="16" src="images/exclamation.jpg">
											</s:if></td>
											<s:iterator value="rowVal" var="cellValue" status="col">
												<s:if test="%{#col.index == 1}">
													<s:set name="cellValue1" value="cellValue"
														scope="request" />
													<td 
														<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
											<%
														String creatorName="";
														if (request.getAttribute("cellValue1") != null
							 									&& !request
							 											.getAttribute("cellValue1")
							 											.equals("")) {
															creatorName = request.getAttribute(
							 										"cellValue1").toString();
							 							}
															
																						UserJB userB = new UserJB();
																						userB.setUserId(EJBUtil
																								.stringToNum(creatorName));
																						userB.getUserDetails();
														
														%> <%=userB.getUserLastName() + " "
												  + userB.getUserFirstName()%></td>
												</s:if>
												<s:if test="%{#col.index == 2}">
													<td 
														<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
														name="garuda.cbu.order.progressNote" /><s:property /></td>
												</s:if>
												<s:if test="%{#col.index == 0}">
													<td 
														<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
														name="garuda.clinicalnote.label.posted_on"></s:text> <s:set
														name="cellValue15" value="cellValue" /> <s:date
														name="cellValue15" id="cellValue15"
														format="MMM dd, yyyy / hh:mm:ss a" /> <s:property
														value="cellValue15" /></td>
												</s:if>
												<s:if test="%{#col.index == 3}">

													<td 
														<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.clinicalnote.label.subject"></s:text>:
													<s:property /></td>
												</s:if>
														<s:if test="%{#col.index == 4}">
														
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:set name="cellValue3" value="cellValue"
																	scope="request" /> <s:iterator
																	value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]"
																	var="subType">


																	<%-- <s:if test="subType==#cellValue">
																		<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
																		<s:property value="description" />
																	</s:if> --%>
																</s:iterator>
															
														</td>
													</s:if>

												<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
												<s:if test="%{#col.index == 5}">
												
													<td style="padding:5px"><a href="#"
															onClick="loadMoredivs('revokePfCategoryNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&pkcordId=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&orderType=<s:property value="orderType"/>&orderId=<s:property value="orderId"/>&category=<s:property value="#request.cbuCateg"/>','ctResolutionTable,ctProgressNotesTable','ctOrderDetailsForm')">
															<s:text name="garuda.clinicalnote.label.revoke" /></a></td>
												</s:if>
												</s:if>

												<s:if test="%{#col.index == 5}">
													<td style="padding:5px"><a href="#"
														onClick="fn_showModalCTdetails('Progress Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfViewNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property />','500','850');">
													    <s:text name="garuda.clinicalnote.label.viewnotes" /></a></td>
												</s:if>

											</s:iterator>
										</tr>
										<tr>
										<td></td>
													<td colspan="6" <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
															<div style="overflow: auto;">
															<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
													        </div>
													</td>
									<tr>
									</s:iterator>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="6">
											<a href="#"
												onclick="shwMdal('RESOLUTION_CODESUBTYPE','<s:property value="cdrCbuPojo.registryId"/>','ctResolutionTable');" class="notesClass"><s:text
												name="garuda.cdrcbuview.label.button.addnewnotes" /></a>
										</td>
									</tr>
								</tfoot>

							</table>
						<s:if test="hasEditPermission(#request.viewCTResolution)==true">
						<s:if test="orderResolLst!=null && orderResolLst.size()>0">
							<s:iterator value="orderResolLst" var="rowVal">
								<s:if
									test='((orderPojo.resolByCbb!= null && orderPojo.resolByCbb!="") || (orderPojo.resolByTc!=null && orderPojo.resolByTc!=""))'>
									<s:if test='%{#rowVal[9]==null || #rowVal[9]=="" || #rowVal[9]=="N"}'>
										<table>
										<tr>
											<td align="right"><input type="button"
												onclick="checkResolution();"
												value="<s:text name="garuda.common.lable.edit"/>"
												id="submitCtResolution" /></td>
										</tr>
									</table>
									</s:if>
									
								</s:if>
							</s:iterator>
						</s:if>
						</s:if>
						</div>
						</div>
						</div>
						</s:if>
						
					 <s:if test="hasViewPermission(#request.viewPrgrssNotes)==true">
						<div class="portlet" id="openOrdernotesparent">
						<div id="openOrdernotes"
							class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
						<div
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-print"
							onclick="addPrintContent('openOrdernotes')"></span><span
							class="ui-icon ui-icon-newwin"></span> <span id="progress-notes" class="ui-icon ui-icon-plusthick"></span>
						<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
							class="ui-icon ui-icon-minusthick"></span>--><s:text
							name="garuda.ctOrderDetail.portletname.notes" /></div>
						<div class="portlet-content" id= "openOrdernotesContent" style="display: none;">
						<table id="ctProgressNotesTable">
							
										<s:iterator  value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT]">
												<s:set name="cbuCateg1" value="pkCodeId" scope="request"/>
										<s:iterator value="#request.categoryPfNotes[#request.cbuCateg1]" var="rowVal" status="row">
											<s:set name="amendFlag1" value="%{#rowVal[6]}"
												scope="request" />
											<s:set name="tagNote" value="%{#rowVal[7]}" scope="request" />
											<s:set name="notes" value="%{#rowVal[8]}" scope="request" />
											<s:set name="cellValue6" value="cellValue" scope="request" />
											<tr>
												<td><s:if test="#request.tagNote==true">
													<img width="20" height="16" src="images/exclamation.jpg">
												</s:if></td>
												<s:iterator value="rowVal" var="cellValue" status="col">
													<s:if test="%{#col.index == 1}">
														<s:set name="cellValue1" value="cellValue" scope="request" />
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
														<%
														String creatorName="";
														if (request.getAttribute("cellValue1") != null
							 									&& !request
							 											.getAttribute("cellValue1")
							 											.equals("")) {
															creatorName = request.getAttribute(
							 										"cellValue1").toString();
							 							}
															
																						UserJB userB = new UserJB();
																						userB.setUserId(EJBUtil
																								.stringToNum(creatorName));
																						userB.getUserDetails();
														
														%> <%=userB.getUserLastName() + " "
												  + userB.getUserFirstName()%></td>
													</s:if>
													<s:if test="%{#col.index == 2}">
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
															name="garuda.cbu.order.progressNote" /><s:property /></td>
													</s:if>
													<s:if test="%{#col.index == 0}">
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
															name="garuda.clinicalnote.label.posted_on"></s:text> <s:set
															name="cellValue15" value="cellValue" /> <s:date
															name="cellValue15" id="cellValue15"
															format="MMM dd, yyyy / hh:mm:ss a" /> <s:property
															value="cellValue15" /></td>
													</s:if>
													<s:if test="%{#col.index == 3}">

														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<s:text name="garuda.clinicalnote.label.subject"></s:text>:
														<s:property /></td>
													</s:if>
													<s:if test="%{#col.index == 4}">
														
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:set name="cellValue3" value="cellValue"
																	scope="request" /> <s:iterator
																	value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]"
																	var="subType">


																	<%-- <s:if test="subType==#cellValue">
																		<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
																		<s:property value="description" />
																	</s:if> --%>
																</s:iterator>
															
														</td>
													</s:if>

													<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
													<s:if test="%{#col.index == 5}">
														<td><a href="#"
															onClick="loadMoredivs('revokePfCategoryNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&pkcordId=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&orderType=<s:property value="orderType"/>&orderId=<s:property value="orderId"/>&category=<s:property value="#request.cbuCateg"/>','ctAvailNotesDiv,ctProgressNotesTable,ctShipmentNotesTable,ctResolutionTable','ctOrderDetailsForm')">
														<s:text name="garuda.clinicalnote.label.revoke" /></a></td>
													</s:if>
													</s:if>

													<s:if test="%{#col.index == 5}">
														<td><a href="#"
															onClick="fn_showModalCTdetails('Progress Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfViewNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property />','500','850');">
														<s:text name="garuda.clinicalnote.label.viewnotes" /></a></td>
													</s:if>

												</s:iterator>
											</tr>
											<tr>
											<td></td>
													<td colspan="6" <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
													      <div style="overflow: auto;">
															<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
													     </div>
													</td>
											<tr>
											</s:iterator>
										</s:iterator>
						</table>
						
							<a href="#"
								onclick="shwMdal1('<s:property value="cdrCbuPojo.registryId"/>','ctAvailNotesDiv,ctShipmentNotesTable,ctResolutionTable');"><s:text
								name="garuda.cdrcbuview.label.button.addnewnotes" /></a>
						</div>
						</div>
						</div>
						</s:if>
						
			             <s:if test="hasViewPermission(#request.viewRqstCBUhis)==true">
						<div class="portlet" id="openOrdercbuhisparent">
						<div id="openOrdercbuhis"
							class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
						<div
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-print"
							onclick="printHistoryWidget('openOrdercbuhis','<s:property value="cdrCbuPojo.registryId"/>');"></span><span
							class="ui-icon ui-icon-newwin"></span> <span id="cbu-req-history" class="ui-icon ui-icon-plusthick orderHistorydata"></span>
						<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
							class="ui-icon ui-icon-minusthick"></span>--><s:text
							name="garuda.ctOrderDetail.portletname.cbunrequesthistory" /></div>
						<div class="portlet-content" style="display: none;">
						<table>
							<tr>
								<td colspan="2"><jsp:include page="cb_cbuHistory.jsp"></jsp:include>
								</td>
							</tr>
						</table>
						</div>
						</div>
						</div>
						</s:if>
					<!-- <br>
						<br>
						<div class="portlet" id="pendingOrdertsklstparent">
							<div id="openOrdertsklst"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
									<div
										class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
										style="text-align: center;"><span class="ui-icon ui-icon-print" onclick="clickheretoprint('openOrdertsklst','<s:property value="cdrCbuPojo.registryId" />')"></span><span
										class="ui-icon ui-icon-newwin"></span>
										<span class="ui-icon ui-icon-plusthick"></span><span
										class="ui-icon ui-icon-close"></span><s:text name="garuda.ctOrderDetail.portletname.tasks"/>
									</div>
									<table>
										<tr>
											<td >
												
											</td>
											
										</tr>
									</table>
							</div>
						</div> --></td>
				</tr>


			</table>
			</div>
			</div>
			</div>
			</td>
		</tr>
	</table>

	<div id="dummyDiv"></div>
	<div id="switchWorkflowDetailsDiv" style="display: none;"><s:if
		test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
		<s:iterator value="ctOrderDetailsList" var="rowVal">
			<table>
				<tr>
					<td><b><s:text
						name="garuda.switchworkflow.label.switchreason" /></b></td>
					<td><s:if test="%{#rowVal[71]!=null && #rowVal[71]!=''}">
						<s:if test="%{#rowVal[71]==getCodeListDescById(switchRsnOtherPk)}">
							<s:property value="%{#rowVal[72]}" />
							<br>
						</s:if>
						<s:else>
							<s:property value="%{#rowVal[71]}" />
						</s:else>
					</s:if> <s:else>
						<s:property value="%{#rowVal[72]}" />
						<br>
					</s:else></td>
				</tr>
				<tr>
					<td><b><s:text
						name="garuda.switchworkflow.label.switchdate" /></b></td>
					<td><s:property value="%{#rowVal[73]}" /></td>
				</tr>
				<tr>
					<td><b><s:text name="garuda.switchworkflow.label.switchby" /></b>
					</td>
					<td><s:set name="workflowSwitchedby" value="%{#rowVal[74]}"
						scope="request" /> <%
 	String workflowswitchuser = "0";
 				if (request.getAttribute("workflowSwitchedby") != null
 						&& !request.getAttribute("workflowSwitchedby")
 								.equals("")) {
 					workflowswitchuser = request.getAttribute(
 							"workflowSwitchedby").toString();
 				}
 				UserJB userB = new UserJB();
 				userB.setUserId(EJBUtil.stringToNum(workflowswitchuser));
 				userB.getUserDetails();
 %> <%=userB.getUserLastName() + " "
								+ userB.getUserFirstName()%></td>
				</tr>
			</table>
		</s:iterator>
	</s:if></div>
	<div id="editAvailConfDiv"></div>
<div id="printDiv" style="hidden: true"></div>
</s:form>
