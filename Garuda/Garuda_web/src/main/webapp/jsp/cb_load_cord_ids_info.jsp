<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>

<s:set name="tempVal" value="cbuFinalReviewPojo.finalReviewConfirmStatus"></s:set>
<div id="idinfo"><!--toggleDiv  -->			
			
			<!--<div id="idscontent" onclick="toggleDiv('idsID')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cdrcbuview.label.ids" /></div>-->
			<div id="idsID">
			<table cellpadding="0" cellspacing="0" width="100%">
				<!--<tr align="left">
				
					<td width="20%;"><s:text name="garuda.cbuentry.label.externalcbuid" />:
					</td>
					<td width="30%;"><s:textfield disabled="true"
						name="CdrCbuPojo.externalCbuId" id="externalCbuId" readonly="true" onkeydown="cancelBack();"  />
					</td>
										
					<td width="50%;" colspan="2"></td>
				</tr >
				--><tr align="left">
				  <td width="20%;" onmouseover="return overlib('<s:text
			name="garuda.cbuentry.label.cbuRegToolTip"></s:text>');" 
			onmouseout="return nd();"><s:text name="garuda.cbuentry.label.registrycbuid"/>:
				        <!--<s:if test="#request.registryIdOnBag==true">
						   <span style="color: red;">(<s:text name="garuda.cdrcbuview.label.id_on_bag"/>)</span>
						</s:if>-->					
				  </td>
				  <td width="30%;"><s:textfield name="cdrCbuPojo.registryId" readonly="true" onkeydown="cancelBack();" disabled="true" ></s:textfield>
				  </td>
				  <td width="20%;" onmouseover="return overlib('<s:text
			name="garuda.cbuentry.label.matRegToolTip"></s:text>');" 
			onmouseout="return nd();"><s:text name="garuda.cbuentry.label.maternalregistryid"/>:</td>				  
				  <td width="30%;"><s:textfield name="cdrCbuPojo.registryMaternalId" readonly="true" onkeydown="cancelBack();" disabled="true" ></s:textfield>
				  </td>
				</tr >
				<tr align="left">
				  <td width="20%;" onmouseover="return overlib('<s:text
				name="garuda.cbuentry.label.cbuLocalToolTip"></s:text>');" 
				onmouseout="return nd();"><s:text name="garuda.cbuentry.label.localcbuid"/>:
				   <!-- <s:if test="#request.localIdOnBag==true">
						   <span style="color: red;">(<s:text name="garuda.cdrcbuview.label.id_on_bag"/>)</span>
					</s:if>-->
				  </td>
				  <td width="30%;"><s:textfield name="cdrCbuPojo.localCbuId" readonly="true" onkeydown="cancelBack();" disabled="true" ></s:textfield>
				  </td>
				  <td width="20%;" onmouseover="return overlib('<s:text
				name="garuda.cbuentry.label.matLocalToolTip"></s:text>');" 
				onmouseout="return nd();"><s:text name="garuda.cbuentry.label.maternallocalid"/>:</td>
				  <td width="30%;"><s:textfield name="cdrCbuPojo.localMaternalId" readonly="true" onkeydown="cancelBack();" disabled="true" ></s:textfield>
				  </td>
				</tr>
				<tr align="left"><td width="20%;" onmouseover="return overlib('<s:text
				name="garuda.cbuentry.label.isbtDinToolTip"></s:text>');" 
				onmouseout="return nd();"><s:text name="garuda.cordentry.label.isbtid"/>:
				       <!--<s:if test="#request.isbtDinOnBag==true">
						   <span style="color: red;">(<s:text name="garuda.cdrcbuview.label.id_on_bag"/>)</span>
					   </s:if>--></td> 
				    <td width="30%;"><s:textfield id="isbidin" name="cdrCbuPojo.cordIsbiDinCode" readonly="true" onkeydown="cancelBack();" disabled="true" ></s:textfield>
					</td>
					<!--<td width="20%;"><s:text name="garuda.cdrcbuview.label.isbt_product_code"/>:
				  </td><td width="30%;"><s:textfield name="cdrCbuPojo.cordIsitProductCode" readonly="true" onkeydown="cancelBack();"  ></s:textfield>					              
				   </td>
			   --><!--<td width="25%">
						<s:text name="garuda.cbuentry.label.idoncbubag" />:
					</td>
					<td width="25%">
						<s:select name="cbuOnBag" multiple="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_IDS]" listKey="pkCodeId" listValue="description" disabled="true" ></s:select>
					</td>
						   -->
					<td width="20%" onmouseover="return overlib('<s:text
										name="garuda.cbuentry.label.uniqueIdToolTip"></s:text>');" 
										onmouseout="return nd();"><s:text name="garuda.cbuentry.label.idbag" />:</td>
			      <td width="30%"><s:textfield name="cdrCbuPojo.numberOnCbuBag" id="idonbagvalresult" disabled="true"></s:textfield></td>
			   </tr>
			   <tr> 
			      <s:if test='cdrCbuPojo.historicLocalCbuId!=null && cdrCbuPojo.historicLocalCbuId!=""'>
				   <td width="20%"><s:text name="garuda.cdrcbuview.label.historic_cbu_lcl_id"/>:</td>
				   <td width="30%"><s:textfield name="cdrCbuPojo.historicLocalCbuId" readonly="true" onkeydown="cancelBack();" disabled="true" ></s:textfield>
				  </s:if>
				  <s:else>
				      <td width="50%" colspan="2"></td>
				  </s:else>
			      <td width="50%" colspan="2"></td>
			   </tr>
	<s:if
		test="#request.additionalIds!=null && #request.additionalIds.size>0">
		<tr>
		<td colspan="4">
		<fieldset><legend><s:text name="garuda.cordentry.label.additionalIds"/></legend>
		<table>
			<s:iterator value="#request.additionalIds" status="row" var="AdditionalIdsPojo">
		      	<tr align="left">
			           <td width="20%"><s:text name="garuda.cordentry.label.additionalIdDesc"></s:text>:</td>
					   <td width="25%"><input type="text" value="<s:property value="additionalIdDesc"/>" disabled="disabled"></td>
			           <td><s:text name="garuda.cordentry.label.additionalTypeId"></s:text>:</td>
					   <td>
					   <s:select list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES]" cssStyle="width:auto;" value="additionalIdType" listKey="pkCodeId" listValue="description" disabled="true"></s:select>
					   </td>
					   <td><s:text name="garuda.cordentry.label.additionalId"></s:text>:</td>
					   <td><input type="text" value="<s:property value="additionalId" />" disabled="disabled"></td>
			            <td></td>
				</tr>
				</s:iterator>
		</table></fieldset>		
		<td>
		</tr>		
	</s:if>
</table>
			
			<table>
				<tr align="right">
					<td colspan="4">
					    <s:if test="hasEditPermission(#request.updateIDwidget)==true">
					    <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
    				</s:if>
  				  <s:else>
					    	<input type="button"
							name="characteristicsEdit" value="<s:text name="garuda.common.lable.edit"/>"
							onclick="fn_showModalIds('<s:text name="garuda.cdrcbuview.label.edit_id"/> <s:text name="garuda.message.modal.headerregid"/> <s:property value="cdrCbuPojo.registryId" />','updateIdInfo?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CLINICAL_RECORD_ID" />&licenceUpdate=False&cordIdStr=<s:property value="cdrCbuPojo.cordID" />','500','850');">
							</s:else>
						</s:if>
					</td>
				</tr>
			</table>
			
			</div><!-- toggleDiv end Here-->
	
				<div id="idnotecontent" onclick="toggleDiv('idnote')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cdrcbuview.label.button.idcordclinicalnotes" /></div>
				<table><tr><td>				
			<div id="idnote" style=" width: 463px; height: 100%;">
			
			<div id="loadIdClinicalNoteDiv">
				        <s:set name="cbuCateg" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CLINIC_CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@ID_CATEGORY_CODESUBTYPE) " />
						<jsp:include page="modal/cb_load_clinicalNote.jsp"></jsp:include>
			</div>
			  <s:if test="hasNewPermission(#request.vClnclNote)==true">
			<s:if test='(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)) '>
				<button type="button" disabled="disabled"><s:text
				name="garuda.cdrcbuview.label.button.addnotes" /></button>
			</s:if>
			 <s:else>
			 <button type="button"
				onclick="fn_showModalIdsForNotes('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','clinicalNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cat=ID_CATEGORY_CODESUBTYPE','550','850','notesModalsDiv');"><s:text
				name="garuda.cdrcbuview.label.button.addnotes" /></button>
			</s:else>
				  </s:if>
			</div>
			</td></tr></table>
</div>
<script>
function fn_showModalIds(title,url,hight,width){
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title=title+patient_data;
	showModal(title,url,hight,width)
	}
function fn_showModalIdsForNotes(title,url,hight,width,id){
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title=title+patient_data;
	showModals(title,url,hight,width,id);
	}
</script>