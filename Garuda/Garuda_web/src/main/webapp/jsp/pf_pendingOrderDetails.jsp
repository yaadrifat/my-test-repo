<%@ taglib prefix="s"  uri="/struts-tags"%>
<script>
function showModelWindow(){
	var var_orderId=$j("#pkcord").val();
	showModal('Update Cord Status','updateCordStatus?orderId='+var_orderId,'300','400');
}


function saveCordAvailForNmdp(){
	var var_cordId=$j("#pkcord").val();
	var var_cbuAvail=$j('input:radio[name=iscordavailfornmdp]:checked').val();
	var var_unavailres=$j("#unavailReason").val();
	//alert(var_cordId+"***********"+var_cbuAvail+"**********"+var_unavailres);
	refreshDiv('updateCordAvailForNmdp?cordId='+var_cordId+'&cbuavail='+var_cbuAvail+'&unavailres='+var_unavailres,'openOrdersamInv','pendingOrderDetailsForm');
}
function showAntigensModal(receipantId){
	showModal('Antigens Details for Receipant '+receipantId,'viewAntigens?receipantId='+receipantId,'300','400');
}
function submitCTResolution(){
	var orderId=$j("#orderId").val();
	var availableDt=$j("#datepicker5").val();
	//alert("orderId"+orderId+"AvailDate"+availableDt);
	refreshDiv('submitCtResolution?orderId='+orderId+'&availableDt='+availableDt,'openOrderctresol','ctResolutionForm');
}
function submitHeResolution(){
	var orderId=$j("#orderId").val();
	var availableDt=$j("#datepicker5").val();
	//alert("orderId"+orderId+"AvailDate"+availableDt);
	refreshDiv('submitHeResolution?orderId='+orderId+'&availableDt='+availableDt,'openOrderheresol','heResolutionForm');
}
$j(function() {
/*	var today = new Date();
	
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();

	var h=today.getHours();
	var mn=today.getMinutes()+1;

	$j( "#datepicker5" ).datepicker({dateFormat: 'dd M yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	*/
	getDatePic();
		
});
$j(document).ready(function(){
	
	$j("#acknowledgeDiv").attr('style','display:none');
	$j("#canceldiv").attr('style','display:none');
	//alert($j('#pkCanceledId').val());
	
});

function enableAcknowledgeDate(value){
//alert(value +"-----------"+$j('#pkTempUnavailable').val());
if(value==$j('#pkTempUnavailable').val()){
$j("#acknowledgeDiv").attr('style','display:block');
}
else{
$j("#acknowledgeDiv").attr('style','display:none');
}
}
function enableCancel(){
	if($j('#cancelled').attr('checked')){
		$j("#canceldiv").attr('style','display:block');
		}
	else{
		$j("#canceldiv").attr('style','display:none');
		}
}
</script>
<s:form name="pendingOrderDetailsForm">
<s:hidden name="pkTempUnavail" id="pkTempUnavailable"></s:hidden>
<s:hidden name="pkCanceled" id="pkCanceledId"></s:hidden>
	<s:hidden name="pkcord" id="pkcord"></s:hidden>
	<s:hidden name="orderId" id="orderId"></s:hidden>
	<table width="100%" cellpadding="0" cellspacing="0">
		<s:if test="orderDetailsList!=null && orderDetailsList.size()>0">
			<s:iterator value="orderDetailsList" var="rowVal">
		<tr>
			<td width="50%">
			<fieldset>
			 <legend><s:text
				name="garuda.cdrcbuview.label.cbu_information" /></legend>
			<table cellpadding="0" cellspacing="0" border="0" align="center">
			
				<tr>
					<td align="left"><b><s:text
						name="garuda.cdrcbuview.label.registry_id" /> :</b></td>
					<td align="left"><s:property value="%{#rowVal[0]}" /></td>
					<td align="left"><b><s:text
						name="garuda.cdrcbuview.label.collection_date" /> :</b></td>

					<td align="left"><s:text name="collection.date">
						<s:param name="value" value="%{#rowVal[2]}" />
					</s:text></td>
				</tr>

				<tr>
					<td align="left"><b><s:text
						name="garuda.cdrcbuview.label.isbt_din" /> :</b></td>
					<td align="left"><s:property
						value="%{#rowVal[3]}" /></td>
					<td align="left"><b><s:text
						name="garuda.cdrcbuview.label.cbb"></s:text> :</b></td>
					<td align="left"><s:property
						value="%{#rowVal[4]}" /></td>
				</tr>
				<tr>
					<td align="left"><b><s:text
						name="garuda.cdrcbuview.label.isbt_product_code"></s:text> :</b></td>
					<td align="left"><s:property
						value="%{#rowVal[5]}" /></td>
					<td align="left"><b><s:text
						name="garuda.cdrcbuview.label.tnc_frozen"></s:text> :</b></td>
					<td align="left"><s:property value="%{#rowVal[1]}" /></td>
				</tr>
				<tr>
					<td align="left"><b><s:text
						name="garuda.cdrcbuview.label.id_on_bag"></s:text> :</b></td>
					<td align="left"><s:property value="%{#rowVal[6]}" /></td>
					<td></td>
					<td></td>
				</tr>
			</table>
			</fieldset>
			<div id="licensure_status">
			<fieldset><legend><b><s:text
				name="garuda.cdrcbuview.label.licensure_status" /></b></legend>
			<div id="licence">
			<table>
				<tr>
					<td><b><s:property value="%{#rowVal[7]}" /></b></td>

					
						<td>&nbsp;&nbsp;&nbsp;</td>
						<td><b><s:text
							name="garuda.cdrcbuview.label.licencure_reason" /></b> : <s:property
							value="%{#rowVal[8]}" /></td>
					
					<td><input type="button" name="licenseEdit" value="<s:text name="garuda.cbbdefaults.lable.edit"/>"
						onclick="showModal('Update License Status','updateStatusLicense?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','400','500');">
					</td>
				</tr>
			</table>
			</div>
			</fieldset>
			</div>
			</td>

			<td width="25%">
			<div id="eligibilitydiv">
				<fieldset><legend><s:text
					name="garuda.cdrcbuview.label.eligibility_determination" /></legend>
				<table cellpadding="0" cellspacing="0" border="0" align="center">
					
					<tr>
						
							<td><b><s:property value="%{#rowVal[9]}" /></b><br />
							<b><s:text name="garuda.cdrcbuview.label.eligibility_reason" /></b>
							: <s:property value="%{#rowVal[10]}" /></td>
						
						<td align="right"><input type="button" name="eligibleEdit"
							value="<s:text name="garuda.cbbdefaults.lable.edit"/>"
							onclick="showModal('Update Eligible Status','updateEligiblestatus?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','300','470');"></td>
					</tr>
				</table>
				</fieldset>
			</div>
			<div id="indshowdiv">
			<fieldset><legend><s:text
				name="garuda.cbuentry.label.ind" /></legend>
			<table>
				<tr>
					<td><b><s:text name="garuda.cbuentry.label.ind_sponsor" /></b>:<s:property
						value="%{#rowVal[15]}" /><s:property
						value="%{#rowVal[13]}" /></td>
				</tr>
				<tr>
					<td><b><s:text name="garuda.cbuentry.label.ind_number" /></b>:<s:property
						value="%{#rowVal[16]}" /><s:property
						value="%{#rowVal[14]}" /></td>

					<td align="right"><input type="button" name="eligibleEdit"
						value="<s:text name="garuda.cbbdefaults.lable.edit"/>"
						onclick="showModal('Update IND','updateINDstatus?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','370','450');"></td>
				</tr>
			</table>
			</fieldset>
			</div>


			</td>
			<td width="25%">
			<fieldset><legend><s:text
				name="garuda.cbuentry.label.cbu_status" /></legend>
			<table>
				<tr>
					<td><b><s:property value="%{#rowVal[17]}" /></b>
					</td>
					<td align="right"><input type="button" name="eligibleEdit"
						value="<s:text name="garuda.cbbdefaults.lable.edit"/>"
						onclick="showModal('Update CBU Status','updateCbuStatus?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','300','400');"></td>
				</tr>
			</table>

			<fieldset><legend><span><s:text
				name="garuda.cdrcbuview.label.patient_information" /></span> </legend>
			<table cellpadding="0" cellspacing="0" border="0" align="center">

				
				
						<tr>
							<td><b><s:text
								name="garuda.cdrcbuview.label.patient_disease" /></b>:</td>
							<td align="left"></td>
						</tr>
						<tr>
							<td><b><s:text
								name="garuda.cdrcbuview.label.patient_disease_status" /></b> :</td>
							<td align="left"></td>
						</tr>
						<tr>
							<td><b><s:text
								name="garuda.cbuentry.label.tncfrozenpatwt" /></b>:</td>
							<td align="left"><s:property value="%{#rowVal[12]}" /></td>
						</tr>

					
				
			</table>
			</fieldset>
			</fieldset>
			</td>
		</tr>
		</s:iterator>
		</s:if>
		
		<tr>			
			<td colspan="3">
				<table width="100%" cellpadding="0" cellspacing="0"
		style="padding-top: 5px; padding-left: 5px;">
		<tr bordercolor="black">
			<th Style="text-align: left;"><s:text
				name="garuda.cdrcbuview.label.quicklinks" />
			<button type="button"
				onclick="showModal('View Unit Report','viewUnitReport?cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />','500','550');"><s:text
				name="garuda.cdrcbuview.label.button.unitreport" /></button>
			<!--<button type="button"
				onclick="showModal('Clinical Notes','clinicalNotes?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','700','750');"><s:text
				name="garuda.cdrcbuview.label.button.addclinicalnotes" /></button>
			-->
			<button type="button"
				onclick="alert('Page Not Found');"><s:text
				name="garuda.cdrcbuview.label.button.addclinicalnotes" /></button>
			<button type="button" onclick="showModal('Upload Document','uploaddocument?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />','500','550');"><s:text
				name="garuda.cdrcbuview.label.button.uploaddocument" /></button>

			<button type="button"
				onclick="showModal('Update Clinical Status','updateClinicalStatus?licenceUpdate=False&cdrCbuPojo.clinicalStatus=<s:property value="cdrCbuPojo.clinicalStatus" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />','400','500');"><s:text
				name="garuda.cdrcbuview.label.button.updateclinicalstatus" /></button>
			<input type="submit" disabled
				value="<s:text
										name="garuda.cdrcbuview.label.button.compfinaleligibility" />"></input>
			<input type="submit" disabled
				value="<s:text
										name="garuda.cdrcbuview.label.button.printcbureport" />"></input>
			<input type="submit" disabled
				value="<s:text
										name="garuda.cdrcbuview.label.button.audittrail" />"></input>


			</th>
		</tr>
	</table>
			</td>
		</tr>
		
		<tr>
			<td colspan="3">
			<s:if test="orderType=='CT'">
			<div class="col_100 maincontainer "><div class="col_100">
			<jsp:include page="pf_CT_OrderDetails.jsp"></jsp:include>
			</div>
			</div>
			</s:if>
			<s:if test="orderType=='HE'">
			<div class="col_100 maincontainer "><div class="col_100">
			<jsp:include page="pf_HE_OrderDetails.jsp"></jsp:include>
			</div>
			</div>
			</s:if>
			<s:if test="orderType=='OR'">
			<div class="col_100 maincontainer "><div class="col_100">
			<jsp:include page="pf_OR_OrderDetails.jsp"></jsp:include>
			</div>
			</div>
			</s:if> 
			</td>
		</tr>
	</table>
</s:form>