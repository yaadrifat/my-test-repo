<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
		<div>
			<div id="idmtestinfoforcbu">
				<jsp:include page="./cb_IDMformVersion.jsp"></jsp:include>	
			</div>
		</div>
		<div>
			<div id="idmnotecontent" onclick="toggleDiv('idmnote')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cdrcbuview.label.button.idmclinicalnotes" /></div>	
			 <div id="idmnote" style=" width: 463px; height: 100%;">
			 <div id="loadIdmClinicalNoteDiv">
				        <s:set name="cbuCateg" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CLINIC_CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@INFCT_NOTE_CATEGORY_CODESUBTYPE) " />
						<jsp:include page="modal/cb_load_clinicalNote.jsp"></jsp:include>	
			 </div>	
			 <s:if test="hasNewPermission(#request.vClnclNote)==true">
				<s:if test='(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)) '>
					<button type="button" disabled="disabled"><s:text
				name="garuda.cdrcbuview.label.button.addnotes" /></button>
				</s:if>
				<s:else>
				<button type="button"
				onclick="fn_showModalIdmForNotes('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','clinicalNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&loadDivName=loadIdmClinicalNoteDiv&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cat=INFCT_NOTE_CATEGORY_CODESUBTYPE','550','850','notesModalsDiv');"><s:text
				name="garuda.cdrcbuview.label.button.addnotes" /></button>
				</s:else>
				 </s:if>
			</div>
			</div>
			<div id="loadIdmDocInfoDiv">
			<s:if test="(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@INFECT_DIS_MARK_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@INFECT_DIS_MARK_CODESUBTYPE].size>0 && hasViewPermission(#request.viewIDMCat)==true)||(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size > 0 && hasViewPermission(#request.viewUnitRepCat)==true)">
			<div id="loadIdmDocInfocontent" onclick="toggleDiv('loadIdmDocInfo')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;">
				<span class="ui-icon ui-icon-triangle-1-s"></span> 
				<s:text name="garuda.uploaddoc.label.button.idmuploaddocument" />
			</div>
			<div id="loadIdmDocInfo">
			<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@INFECT_DIS_MARK_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@INFECT_DIS_MARK_CODESUBTYPE].size>0 ||#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null ||#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size>0">
			<s:if test="hasViewPermission(#request.viewIDMCat)==true">
			<s:set name="cbuDocCateg" value="@com.velos.ordercomponent.util.VelosGarudaConstants@INFECT_DIS_MARK_CODESUBTYPE" scope="request" />
			</s:if>			
						<jsp:include page="modal/cb_load_upload_document.jsp">
						<jsp:param value="<%=request.getAttribute(\"cbuDocCateg\")%>" name="cbuDocCateg"/>
						</jsp:include>						
			</s:if>		
			</div>
		</s:if>	
		</div>
		<script>
		function fn_showModalIdmForNotes(title,url,hight,width,id){
			var patientId = $j('#patientId').val();
			var patient_data = '';
			if(patientId!="" && patientId != null && patientId != "undefined"){
				patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
			}
			title=title+patient_data;
			showModals(title,url,hight,width,id);
			}
		</script>