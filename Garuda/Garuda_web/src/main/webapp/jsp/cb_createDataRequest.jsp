<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%
HttpSession tSession = request.getSession(true);
GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
int cbuCrtDataRqstMnuRght = 0;
if(grpRights.getFtrRightsByValue("CB_DATAREQUEST")!=null && !grpRights.getFtrRightsByValue("CB_DATAREQUEST").equals(""))
{cbuCrtDataRqstMnuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_DATAREQUEST"));}
else
	cbuCrtDataRqstMnuRght = 4;
request.setAttribute("cbuCrtDataRqstMnuRght",cbuCrtDataRqstMnuRght);
%>
<script type="text/javascript">
$j(document).ready(function(){
	/*var today = new Date();
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();
	var h=today.getHours();
	var mn=today.getMinutes()+1;
	$j('#requestdatepicker').datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true, changeYear: true});
*/
getDatePic();
	$j('#createDataRequestTbl').css('padding-left','25%');
	if(navigator.appName=="Microsoft Internet Explorer"){
		$j('#createDataRequestDiv').css('padding-left','15%');
		$j('#esignTbl').width("100%");
	}
	
});

$j(function(){
	$j("#createDataRequestForm").validate({
		rules:{	
				"datarequestpojo.title":{required:true},
				"datarequestpojo.requestDuedate":{required:true,dpDate:true},
				"datarequestpojo.requestReason":{required:true},
				"datarequestpojo.dataModiDescription":{required:true},
				"datarequestpojo.affectedFields":{required:true},
				"datarequestpojo.cbbProcessChange":{required:true}
		},
		messages:{	
			"datarequestpojo.title":"<s:text name="garuda.cbu.datarequest.title"/>",
			"datarequestpojo.requestDuedate":{required:"<s:text name="garuda.cbu.datarequest.requestduedate"/>"},
			"datarequestpojo.requestReason":"<s:text name="garuda.cbu.datarequest.requestreason"/>",
			"datarequestpojo.dataModiDescription":"<s:text name="garuda.cbu.datarequest.datamodidescription"/>",
			"datarequestpojo.affectedFields":"<s:text name="garuda.cbu.datarequest.affectedFields"/>",
			"datarequestpojo.cbbProcessChange":"<s:text name="garuda.cbu.datarequest.cbbprocesschange"/>"
		}
	});
});

function fnCreateRequest(){
	
	if($j("#createDataRequestForm").valid()){
		alert("<s:text name="garuda.cbu.datarequest.alert"/>");
		document.getElementById('createDataRequestForm').submit();
    }
	
}
function fnCancel(){
	$j('#title').val("");
	$j('#requestdatepicker').val("");
	$j('#reason').val("");
	$j('#description').val("");
	$j('#affectedField').val("");
	$j('#processChange').val("");
}
</script>
<div class="maincontainer">



<s:form id="createDataRequestForm" action="createDataReq" method="POST">

<div class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" id="historydiv" style="height: 100%;">
						<div style="text-align: center;" class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
								<span onclick="clickheretoprint()" class="ui-icon ui-icon-print"></span>
								<span class="ui-icon ui-icon-newwin"></span>
								<span class="ui-icon ui-icon-minusthick"></span>
								<s:text name="Create Data Modification Request" />
						</div>
		<div id="wrapper_createDataRequest" style="width: 100%">
		
		<div id="createDataRequestDiv" style="width: 80%;">
						<br/><br/><br/>
						<table id="createDataRequestTbl" width="100%">
								<tbody>
									<tr>
										<td width="60%"><s:text name="garuda.dmrequest.label.requestttile"/><span class="error">*</span></td>
										<td width="40%"><s:textfield id="title" name="datarequestpojo.title"/></td>
									</tr>
									<tr>
										<td width="60%"><s:text name="garuda.dmrequest.label.requestdue"/><span class="error">*</span></td>
										<td width="40%">
										<s:date name="datarequestpojo.requestDuedate" id="requestdatepicker" format="MMM dd, yyyy" />
        								<s:textfield readonly="true" onkeydown="cancelBack();"  name="datarequestpojo.requestDuedate" class="datepic" id="requestdatepicker" value="%{requestdatepicker}" requiredposition="true" cssClass="datePicWMinDate" onfocus="onFocusCall(this);"/>
										</td>
									</tr>
									<tr>
										<td width="60%"><s:text name="garuda.dmrequest.label.datamodificationdescri"/><span class="error">*</span></td>
										<td width="40%"><s:textarea id="description" name="datarequestpojo.dataModiDescription" cols="50" rows="4"/></td>
									</tr>
									<tr>
										<td width="60%"><s:text name="garuda.dmrequest.label.requestreason"/><span class="error">*</span></td>
										<td width="40%"><s:textarea id="reason" name="datarequestpojo.requestReason" cols="50" rows="4"/></td>
									</tr>
									<tr>
										<td width="60%"><s:text name="garuda.dmrequest.label.affectedfields"/><span class="error">*</span></td>
										<td width="40%"><s:textarea id="affectedField" name="datarequestpojo.affectedFields" cols="50" rows="2"/></td>
									</tr>
									<tr>
										<td width="60%"><s:text name="garuda.dmrequest.label.cbbprocesschange"/><span class="error">*</span></td>
										<td width="40%"><s:textarea id="processChange" name="datarequestpojo.cbbProcessChange" cols="50" rows="2"/></td>
									</tr>
									<tr>
										<td width="100%" colspan="2">
										<br/>
										<br/>
										      <s:if test="hasEditPermission(#request.cbuCrtDataRqstMnuRght)==true">
										     <table bgcolor="#cccccc" style="padding-top: 5px;" id="esignTbl" style="width: 80%;">
												<tr bgcolor="#cccccc" valign="baseline">								        
													<td width="70%">
															<jsp:include page="cb_esignature.jsp" flush="true"></jsp:include>
													</td>
	    											<td width="15%" align="right">
	    												<input type="submit"" id="submit" disabled= "disabled" onclick="fnCreateRequest()" value='<s:text name="garuda.unitreport.label.button.submit"/>'/>
	    											</td>
	    											<td width="15%">
	    												<input type="button"  onclick="fnCancel()" value='<s:text name="garuda.advancelookup.label.reset"/>'/>
	    											</td>	    											    
    											</tr>	
  										</table>
  										        </s:if>
										</td>
									</tr>
								</tbody>
						</table>
						
						<br/><br/><br/>
		</div>	
		<div id="emptyDiv2" style="width: 20%;float: right;">&nbsp; &nbsp;</div>		
		</div>
						
</div>
</s:form>



</div>