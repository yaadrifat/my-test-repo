<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.helper.UserAlertsHelper" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:include page="cb_track_session_logging.jsp" />
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", 0); 
%>
<%@page import="com.velos.ordercomponent.business.pojoobjects.CBBPojo"%><script>
   $j( "input:submit, button", ".column" ).button();

   $j('#cbbdefaultinfo').find(".portlet-header .ui-icon").bind("click",function() {
	   if($j(this).is(".ui-icon-minusthick") || $j(this).is(".ui-icon-plusthick"))
		{
			$j(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
			
			$j(this).parents(".portlet:first").find(".portlet-content").toggle();
			
			var divid = $j(this).parent().parent().attr("id");
			if(flag)
			divHeight=document.getElementById(divid).style.height;
			if(divHeight=='100%')
			{
				divHeight = 'auto';
				}	
		    $j(this).parents(".portlet:first").css('height',divHeight);
			
		}
	});
  
   </script>

<s:iterator value="#request.CbbDefaultList" >
  <s:form>
  
<div class="column">
				<div class="portlet" id="cbbdefaultinfoparent">
				<div id="cbbdefaultinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span><!--<span class="ui-icon ui-icon-close" ></span>--><s:text name="garuda.cbbdefaults.lable.cbb" />:&nbsp;<s:property value="siteName" /></div>
				<div class="portlet-content">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
				
				
				
				<tr>
				         <td>
				           <s:if test="hasViewPermission(#request.viewCbbSetup)==true">
				             <div id="cbbsetupcontent" onclick="toggleDiv('cbbsetup')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
							 <span class="ui-icon ui-icon-triangle-1-s"></span>
						     <s:text name="garuda.cbbdefaults.lable.cbbsetup" /></div>
								<div id="cbbsetup">  
									  <table cellpadding="0" cellspacing="5" width="100%" align="left">
										<tr>
											<td>
												<s:text name="garuda.pendingorderpage.label.cddid" />:<span style="color: red;">*</span>      				
								   			</td>
											<td >
												<s:textfield name="siteIdentifier" readonly="true" onkeydown="cancelBack();"  />
											</td>			
											<td>
												<s:text name="garuda.cbbdefaults.lable.cbbregistryname" />:<span style="color: red;">*</span>     				
								   			</td>
											<td >
												<s:textfield name="siteName" readonly="true" onkeydown="cancelBack();"  />
											</td>
								        </tr>
								        <tr>
										    <td >
												<s:text name="garuda.cbbdefaults.lable.cbbsystemuser" />:<span style="color: red;">*</span>																			
											</td>
											<td >
												<strong><s:if test="cbb.cdrUser==true"><s:text name="garuda.opentask.label.yes"/></s:if><s:elseif test="cbb.cdrUser==false"><s:text name="garuda.clinicalnote.label.no"/></s:elseif></strong>
											</td>								
											<td>
												<s:text name="garuda.cbbdefaults.lable.bmdwcode" />:     				
								   			</td>
											<td >
												<s:textfield name="cbb.bmdw" readonly="true" onkeydown="cancelBack();"  />
											</td>		
								        	
								        </tr>
								        <tr>
										    <td>
												<s:text name="garuda.cbbdefaults.lable.emdis" />:     				
								   			</td>
											<td >
												<s:textfield name="cbb.emdis" readonly="true" onkeydown="cancelBack();"  />
											</td>
								        
								        	<td >
												<s:text name="garuda.cbbdefaults.lable.membershiptype"></s:text>:
											</td>
						          			<td >
						          				<!--<strong><s:if test="cbb.allowMember==true">Yes</s:if><s:elseif test="cbb.allowMember==false">No</s:elseif></strong>
						          				--><s:select headerKey="9" headerValue=" " disabled="true"
														list="#{'1':'Member', '0':'Non Member', '2':'Participating', '3':'Other'}" 
														name="cbb.allowMember" 
														 />
						          			</td>	
						        		</tr>
								        <tr>
								           <td>
								   				<s:text name="garuda.cbbdefaults.lable.cbuassessmentbutton" />:<span style="color: red;">*</span>
											</td>
								        	<td>
								        		<strong><s:if test="cbb.cbuAssessment==true"><s:text name="garuda.opentask.label.yes"/></s:if><s:elseif test="cbb.cbuAssessment==false"><s:text name="garuda.clinicalnote.label.no"/></s:elseif></strong>
								        		
								        	</td>
								            <td>
								   				<s:text name="garuda.cbbdefaults.lable.unitreportbutton" />:<span style="color: red;">*</span>
											</td>
								        	<td>
								        		<strong><s:if test="cbb.unitReport==true"><s:text name="garuda.opentask.label.yes"/></s:if><s:elseif test="cbb.unitReport==false"><s:text name="garuda.clinicalnote.label.no"/></s:elseif></strong>
								           	</td>
								        </tr>
										<tr>
								         	<td>
								   				<s:text name="garuda.cbbdefaults.lable.researchsample" />:
											</td>
								        	<td>
								        		<strong><s:if test="cbb.nmdpResearch==true"><s:text name="garuda.opentask.label.yes"/></s:if><s:elseif test="cbb.nmdpResearch==false"><s:text name="garuda.clinicalnote.label.no"/></s:elseif></strong>
								        	</td>
								          	 <td><s:text name="garuda.cbbdefaults.lable.domestic"></s:text>:</td>
								          	<td><strong><s:if test="cbb.isDomesticCBB==true">Yes</s:if><s:elseif test="cbb.isDomesticCBB==false">No</s:elseif></strong></td>
								          
								        					        	
								        </tr>
								       <tr>
								         	<td>
								   				<s:text name="garuda.cbbdefaults.lable.CRIReqElgQuest" />:
											</td>
								        	<td><s:checkbox name="cbb.criReqElgQuest" disabled="true" value="%{cbb.criReqElgQuest}"  /> 
								        		<!-- <strong><s:if test="%{cbb.criReqElgQuest==true}"><s:text name="garuda.opentask.label.yes"/></s:if><s:else><s:text name="garuda.clinicalnote.label.no"/></s:else></strong> -->
								        	</td>
								        	<td colspan="2"></td>
								        </tr>
								          
								        </table>
								</div>
								</s:if>		
				         </td>
				      </tr>
				
				       <tr>
				         <td>
				           <s:if test="hasViewPermission(#request.veiwCbbInfo)==true">
				             <div id="cbbinformationcontent" onclick="toggleDiv('cbbinformation')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
							 <span class="ui-icon ui-icon-triangle-1-s"></span>
						     <s:text name="garuda.cbbdefaults.lable.cbbinformation" /></div>
								<div id="cbbinformation">  
									  <table cellpadding="0" cellspacing="5" width="100%" align="left">
										<tr>		
											<td>
												<s:text name="garuda.cbbdefaults.lable.cbbname" />:     				
								   			</td>
											<td >
												<s:textfield name="siteName" readonly="true" onkeydown="cancelBack();"  />
											</td>
											<!--<td>
										<s:text name="garuda.cbbdefaults.lable.OrgType" />:     				
						   			</td>
									<td >
										<s:textfield name="cbbType"  readonly="true" onkeydown="cancelBack();"  />
									</td>
								        -->
								        <td colspan="2">
								        </td>
								        
								        </tr>
								        <tr>
								        	<td>
								   				<s:text name="garuda.cbbdefaults.lable.coordinatorlastname" />:
											</td>
								        	<td>
								        		<s:textfield name="cbb.person.personLname" readonly="true" onkeydown="cancelBack();"  />
								        	</td>
								            <td>
								   				<s:text name="garuda.cbbdefaults.lable.coordinatorfirstname" />:
											</td>
								        	<td>
								        		<s:textfield name="cbb.person.personFname" readonly="true" onkeydown="cancelBack();"  />
								        	</td>
								        	
								        </tr>
								        <tr>
								        <td>
						   				<s:text name="garuda.cbbdefaults.lable.phone" />:
									</td>
						        	<td>
						        		<s:textfield name="address.phone" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
						        	</td>
						            <td>
						   				<s:text name="garuda.cbbdefaults.lable.phoneext" />:
									</td>
						        	<td>
						        		<s:textfield name="address.ext" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
						        	</td>
						        </tr>
						        <tr>
						            <td>
						   				<s:text name="garuda.cbbdefaults.lable.fax" />:
									</td>
						        	<td>
						        		<s:textfield name="address.fax" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
						        	</td>
						        	<td>
						   				<s:text name="garuda.cbbdefaults.lable.email" />:
									</td>
						        	<td>
						        		<s:textfield name="address.email" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
						        	</td>
						        </tr>
								        
								        <jsp:include page="cb_address.jsp" flush="true">
								           <jsp:param value="address" name="prefix"/>
								        </jsp:include>
								          
								        </table>
								</div>
								</s:if>		
				         </td>
				      </tr>
				      <tr>
				         <td>
				         <s:if test="hasViewPermission(#request.viewCbbpick)==true">
				             <div id="cbuPickupAddresscontent" onclick="toggleDiv('cbuPickupAddress')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
				             <span class="ui-icon ui-icon-triangle-1-s"></span>
					         <s:text name="garuda.cbbdefaults.lable.cbuPickupAddress" /></div>
								<div id="cbuPickupAddress"> 
								    <table cellpadding="0" cellspacing="0" width="100%" align="left"><!--
								       <tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.address.contact"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbb.attention" readonly="true" onkeydown="cancelBack();" /></td>
								      	  <td colspan="2"></td>
							</tr>
							
							
								--><tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.address.contact.lastName"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbb.pickUpAddLastName" readonly="true" onkeydown="cancelBack();"  /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.address.contact.firstName"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbb.pickUpAddFirstName" readonly="true" onkeydown="cancelBack();"  /></td>
							</tr>	
							
							
							<tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.contact.phone"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbb.address.phone" readonly="true" onkeydown="cancelBack();"  /></td>
								      	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.contact.email"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbb.address.email" readonly="true" onkeydown="cancelBack();"  /></td>
							</tr>	      
								<jsp:include page="cb_address.jsp" flush="true">
						           <jsp:param value="cbb.address" name="prefix"/>
						           <jsp:param value="Yes" name="hideFields"/>
						        </jsp:include>
						       <tr>
						       			<td width="30%"><s:text name="garuda.common.label.lstmodifiedon"></s:text>:</td>
								          <td width="20%">
								         <s:date name="cbb.address.lastModifiedOn" id="pickUpAddLstModifdDT" format="MMM dd, yyyy / HH:mm" />
												<s:textfield name="pickUpAddLstModifdDT" readonly="true" onkeydown="cancelBack();"  />
								          </td>
								        	  <td width="30%"><s:text name="garuda.cbbdefaults.lable.lastmodifiedby"></s:text>:</td>
								        	 <td width="20%">
												<s:if test="cbb.address.lastModifiedBy!=null">
												<s:set name="cellValuePickup" value="cbb.address.lastModifiedBy" scope="request"/>	
													<% 
														
													if(request.getAttribute("cellValuePickup")!=null)
													{
														String cellValuePickup = request.getAttribute("cellValuePickup").toString();
														UserJB userB = new UserJB();
														UserAlertsHelper userHelp = new UserAlertsHelper();
														userB.setUserId(EJBUtil.stringToNum(cellValuePickup));
														userB.getUserDetails();													
															//String userNamePickup = userB.getUserLastName()+" "+userB.getUserFirstName();
															String userNamePickup = userHelp.getLogNameByUserId(new Long (EJBUtil.stringToNum(cellValuePickup)));
																request.setAttribute("userNamePickup",userNamePickup);
													}			
															%>									
													</s:if>
													<s:else>
														  <s:set name="cellValuePickup" value="cbb.address.createdBy" scope="request"/>	
													<% 
														
													if(request.getAttribute("cellValuePickup")!=null)
													{
														String cellValuePickup = request.getAttribute("cellValuePickup").toString();
														UserJB userB = new UserJB();
														UserAlertsHelper userHelp = new UserAlertsHelper();
														userB.setUserId(EJBUtil.stringToNum(cellValuePickup));
														userB.getUserDetails();													
															//String userNamePickup = userB.getUserLastName()+" "+userB.getUserFirstName();
															String userNamePickup = userHelp.getLogNameByUserId(new Long (EJBUtil.stringToNum(cellValuePickup)));
																request.setAttribute("userNamePickup",userNamePickup);
													}			
															%>		
														  
													</s:else>
													
												<s:textfield name="#request.userNamePickup" readonly="true" onkeydown="cancelBack();"  />
											</td>
								         
							</tr><!--	
							
							 <tr>
								          <td width="30%"><s:text name="garuda.cbuentry.label.cdrcbucreby"></s:text>:</td>
								         
								          <td width="20%">
												<s:if test="cbb.address.createdBy!=null">
												<s:set name="cellValuePickAddCreatedBy" value="cbb.address.createdBy" scope="request"/>	
													<% 
														String cellValuePickAddCreatedBy = request.getAttribute("cellValuePickAddCreatedBy").toString();
														UserJB userBB = new UserJB();
														userBB.setUserId(EJBUtil.stringToNum(cellValuePickAddCreatedBy));
														userBB.getUserDetails();													
															%>	
															<%String userNamePickAddCreatedBy = userBB.getUserLastName()+" "+userBB.getUserFirstName();
																request.setAttribute("userNamePickAddCreatedBy",userNamePickAddCreatedBy);
															%>									
													</s:if>
												<s:textfield name="#request.userNamePickAddCreatedBy" readonly="true" onkeydown="cancelBack();"  />
											</td>	          
								          
								          
								          
								      	  <td width="30%"><s:text name="garuda.cbuentry.label.cdrcbucreon"></s:text>:</td>
								         					          
								            <td width="20%">
								          <s:date name="cbb.address.createdOn" id="PickUpCreatedDT" format="MMM dd, yyyy / HH:MM" />
												<s:textfield name="PickUpCreatedDT" readonly="true" onkeydown="cancelBack();"  />
								          </td>
							</tr>			
									
									--><tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.pickup.address.special.instruction"></s:text>:</td>
								          <td width="20%"><s:textarea name="cbb.pickUpAddSplInstuction" cols="45" rows="3" readonly="true" onkeydown="cancelBack();"  /></td>
								      	  <td colspan="2"></td>
								      	  
								      	  
							</tr>	 
						
									</table>			
								</div>	
								</s:if>			            
				         </td>
				      </tr>
				       <tr>
				         <td>
				           <s:if test="hasViewPermission(#request.viewDryShip)==true">
				             <div id="cbudryShipperAddresscontent" onclick="toggleDiv('cbudryShipperAddress')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
				            <span class="ui-icon ui-icon-triangle-1-s"></span>
					         <s:text name="garuda.cbbdefaults.lable.dryShipperReturnAddtess" /></div>
								<div id="cbudryShipperAddress"> 
								    <table cellpadding="0" cellspacing="5" width="100%" align="left">
								       <tr>
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.attentionorg"></s:text>:</td>
								          <td width="20%"><s:textfield name="cbb.attentiondDryShipper" readonly="true" onkeydown="cancelBack();" /></td>
								      	  <td colspan="2"></td>
							</tr>							
						      
								<jsp:include page="cb_address.jsp" flush="true">
						           <jsp:param value="cbb.addressDryShipper" name="prefix"/>
						           <jsp:param value="Yes" name="hideFields"/>
						        </jsp:include>
						  
						        <tr>
								         
								            <td width="30%"><s:text name="garuda.common.label.lstmodifiedon"></s:text>:</td>
								          <td width="20%">
								          <s:date name="cbb.addressDryShipper.lastModifiedOn" id="dryShipperLstModifdDT" format="MMM dd, yyyy / HH:mm" />
												<s:textfield name="dryShipperLstModifdDT" readonly="true" onkeydown="cancelBack();"  />
								          </td>
								           <td width="30%"><s:text name="garuda.cbbdefaults.lable.lastmodifiedby"></s:text>:</td>
								          <td width="20%">
												<s:if test="cbb.addressDryShipper.lastModifiedBy!=null">
												<s:set name="cellValueDryShipper" value="cbb.addressDryShipper.lastModifiedBy" scope="request"/>	
													<% 
													if(request.getAttribute("cellValueDryShipper")!=null)
													{
														String cellValueDryShipper = request.getAttribute("cellValueDryShipper").toString();
														UserJB userB = new UserJB();
														UserAlertsHelper userHelp = new UserAlertsHelper();
														userB.setUserId(EJBUtil.stringToNum(cellValueDryShipper));
														userB.getUserDetails();													
														//String userNameDryshipper = userB.getUserLastName()+" "+userB.getUserFirstName();
															String userNameDryshipper = userHelp.getLogNameByUserId(new Long (EJBUtil.stringToNum(cellValueDryShipper)));
																request.setAttribute("userNameDryshipper",userNameDryshipper);
													}			
															%>									
													</s:if>
													<s:else>
														  <s:set name="cellValueDryShipper" value="cbb.addressDryShipper.createdBy" scope="request"/>	
													<%
													if(request.getAttribute("cellValueDryShipper")!=null)
													{
														String cellValueDryShipper = request.getAttribute("cellValueDryShipper").toString();
														UserJB userB = new UserJB();
														UserAlertsHelper userHelp = new UserAlertsHelper();
														userB.setUserId(EJBUtil.stringToNum(cellValueDryShipper));
														userB.getUserDetails();													
															
															//String userNameDryshipper = userB.getUserLastName()+" "+userB.getUserFirstName();
															String userNameDryshipper = userHelp.getLogNameByUserId(new Long (EJBUtil.stringToNum(cellValueDryShipper)));
																request.setAttribute("userNameDryshipper",userNameDryshipper);
													}			
															%>		
														  
													</s:else>
													
												<s:textfield name="#request.userNameDryshipper" readonly="true" onkeydown="cancelBack();"  />
											</td>	          
							</tr>	
							
							<!--<tr>
								          <td width="30%"><s:text name="garuda.cbuentry.label.cdrcbucreby"></s:text>:</td>
								         
								          <td width="20%">
												<s:if test="cbb.addressDryShipper.createdBy!=null">
												<s:set name="cellValueDryShipperCreatedBy" value="cbb.addressDryShipper.createdBy" scope="request"/>	
													<% 
														String cellValueDryShipperCreatedBy = request.getAttribute("cellValueDryShipperCreatedBy").toString();
														UserJB userBB = new UserJB();
														userBB.setUserId(EJBUtil.stringToNum(cellValueDryShipperCreatedBy));
														userBB.getUserDetails();													
															%>	
															<%String userNameDryshipperCreatedBy = userBB.getUserLastName()+" "+userBB.getUserFirstName();
																request.setAttribute("userNameDryshipperCreatedBy",userNameDryshipperCreatedBy);
															%>									
													</s:if>
												<s:textfield name="#request.userNameDryshipperCreatedBy" readonly="true" onkeydown="cancelBack();"  />
											</td>	          
								          
								          
								          
								      	  <td width="30%"><s:text name="garuda.cbuentry.label.cdrcbucreon"></s:text>:</td>
								         					          
								            <td width="20%">
								          <s:date name="cbb.addressDryShipper.createdOn" id="dryShipperCreatedDT" format="MMM dd, yyyy / HH:MM" />
												<s:textfield name="dryShipperCreatedDT" readonly="true" onkeydown="cancelBack();"  />
								          </td>
							</tr>			
							
								
						
									--></table>			
								</div>
								</s:if>				            
				         </td>
				      </tr>				     
				      <tr>
				         <td>
				             <s:if test="hasViewPermission(#request.ViewCbbDeflt)==true">
				             <div id="cbbDefaultDivcontent" onclick="toggleDiv('cbbDefaultDiv')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
				             <span class="ui-icon ui-icon-triangle-1-s"></span>
					         <s:text name="garuda.cbbdefaults.lable.cbbdefaults" /></div>
								<div id="cbbDefaultDiv"> 
								    <table cellpadding="0" cellspacing="5" width="100%" align="left">
								    
								    	<tr>
								        	<td><s:text name="garuda.cbudefaults.label.storage.location"></s:text>:</td>
								          	  <td ><s:select name="fkCbuStorage" disabled="true" list="cbuStrLocationList" headerKey="-1" headerValue="" listKey="siteId"  listValue="siteName"></s:select>
								         	 </td>
								         	 <td ><s:text name="garuda.cbudefaults.label.collection.site"></s:text>:</td>
								         	 <td ><s:select name="fkCbuCollection" disabled="true" list="cbuCollectionList" headerKey="-1" headerValue="" listKey="siteId"  listValue="siteName"></s:select>
								          </td>
								       </tr>
								       <tr><!--								     
											 <td width="30%"><s:text name="garuda.cbuentry.label.idbag"></s:text>:</td>
											 <td width="20%"> <s:select name="cbuOnBag" multiple="true"  disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_IDS]" listKey="pkCodeId" listValue="description" ></s:select>
											 </td>
											-->
											<td ><s:text name="garuda.cbbdefaults.lable.defaultCTShipper"></s:text>:</td>
								         	<td ><s:select name="cbb.defaultCTShipper" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPER]" listKey="pkCodeId" listValue="description" disabled="true" headerKey="" headerValue=""></s:select> </td>
								         	<td ><s:text name="garuda.cbbdefaults.lable.defaultORShipper"></s:text>:</td>
								          	<td ><s:select name="cbb.defaultORShipper" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPER]" listKey="pkCodeId" listValue="description" disabled="true" headerKey="" headerValue=""></s:select></td>
										</tr>
								       <!--<tr>
								          <td><s:text name="garuda.cbbdefaults.lable.enableeditanitgens"></s:text>:</td>
								          <td><strong><s:if test="cbb.enableEditAntigens==true">Yes</s:if><s:elseif test="cbb.enableEditAntigens==false">No</s:elseif></strong>
								          </td>
								          <td><s:text name="garuda.cbbdefaults.lable.displayMaternalAntigens"></s:text>:</td>
								          <td><strong><s:if test="cbb.displayMaternalAntigens==true">Yes</s:if><s:elseif test="cbb.displayMaternalAntigens==false">No</s:elseif></strong>
								          </td>
								       </tr>
								       <tr>
				          					<td width="30%"><s:text name="garuda.cbbdefaults.lable.enableautocompletecordentry"></s:text>:</td>
				          					<td width="20%"><strong><s:if test="cbb.enableAutoCompleteCordEntry==true">Yes</s:if><s:elseif test="cbb.enableAutoCompleteCordEntry==false">No</s:elseif></strong>
				          					</td>
				          					<td width="20%"><s:text name="garuda.cbbdefaults.lable.enableautocompleteackrestask"></s:text>:</td>
				          					<td width="30%"><strong><s:if test="cbb.enableAutoCompleteAckResTask==true">Yes</s:if><s:elseif test="cbb.enableAutoCompleteAckResTask==false">No</s:elseif></strong>
				          					</td>
				          					
				       					</tr>
								       <tr>
								          <td><s:text name="garuda.cbbdefaults.lable.enableCbuInventFeature"></s:text>:</td>
								          <td><strong><s:if test="cbb.enableCbuInventFeature==true">Yes</s:if><s:elseif test="cbb.enableCbuInventFeature==false">No</s:elseif></strong>
								          </td>							          
								       </tr>
								       --><!--<tr>
								          <td><s:text name="garuda.cbbdefaults.lable.defaultMicroContamDateToProcDate"></s:text>:</td>
								          <td><strong><s:if test="cbb.defaultMicroContamDateToProcDate==true">Yes</s:if><s:elseif test="cbb.defaultMicroContamDateToProcDate==false">No</s:elseif></strong>
								          </td>
								          
								         
								         <td width="50%" colspan="2"></td>
								          
								          
								       </tr>
								       <tr>
								          <td><s:text name="garuda.cbbdefaults.lable.defaultCfuTestDateToProcDate"></s:text>:</td>
								          <td><strong><s:if test="cbb.defaultCfuTestDateToProcDate==true">Yes</s:if><s:elseif test="cbb.defaultCfuTestDateToProcDate==false">No</s:elseif></strong>
								          </td>
								          <td><s:text name="garuda.cbbdefaults.lable.defaultViabTestDateToProcDate"></s:text>:</td>
								          <td><strong><s:if test="cbb.defaultViabTestDateToProcDate==true">Yes</s:if><s:elseif test="cbb.defaultViabTestDateToProcDate==false">No</s:elseif></strong>
								          </td>
								       </tr>
								       --><tr><!--
								          <td width="30%"><s:text name="garuda.cbbdefaults.lable.EnableCordInfoLock"></s:text>:</td>
								          <td width="20%"><strong><s:if test="cbb.cordInfoLock==true">Yes</s:if><s:elseif test="cbb.cordInfoLock==false">No</s:elseif></strong>
								          </td>  
								          --><td width="20%"><s:text name="garuda.cbbdefaults.lable.defaultShippingPaperWorkLoc"></s:text>:</td>
				       	 				  <td width="30%"><s:select name="cbb.fkDefaultShippingPaperWorkLocation" headerValue="" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DEFAULT_SHIPPING_PAPER_WRK_LOC]" disabled="true" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue=""></s:select></td>
								          
								          <td width="20%"><s:text name="garuda.cbbdefaults.lable.accreditation"></s:text>:</td>
						 					<td width="30%"><s:select name="fkAccreditation" multiple="true" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ACCREDITITAON]" listKey="pkCodeId" listValue="description"></s:select></td>
						 				</tr><!--
								       <tr>
								          <td><s:text name="garuda.cbbdefaults.lable.allowmember"></s:text>:</td>
								          <td><strong><s:if test="cbb.allowMember==true">Yes</s:if><s:elseif test="cbb.allowMember==false">No</s:elseif></strong></td>
								          <td><s:text name="garuda.cbbdefaults.lable.usingcdr"></s:text>:</td>
								          <td><strong><s:if test="cbb.cdrUser==true">Yes</s:if><s:elseif test="cbb.cdrUser==false">No</s:elseif></strong></td>
								       </tr>
								       <tr>
								          <td><s:text name="garuda.cbbdefaults.lable.domestic"></s:text>:</td>
								          <td><strong><s:if test="cbb.isDomesticCBB==true">Yes</s:if><s:elseif test="cbb.isDomesticCBB==false">No</s:elseif></strong></td>
								          <td><s:text name="garuda.cbbdefaults.lable.useowndumn"></s:text>:</td>
								          <td><strong><s:if test="cbb.useOwnDumn==true">Yes</s:if><s:elseif test="cbb.useOwnDumn==false">No</s:elseif></strong></td>
								       </tr>
								        <tr>
								          <td><s:text name="garuda.cbbdefaults.lable.fdoe"></s:text>:</td>
								          <td><strong><s:if test="cbb.useFdoe==true">Yes</s:if><s:elseif test="cbb.useFdoe==false">No</s:elseif></strong></td>
								          <td colspan="2"></td>
								        </tr>
								         
								          <tr>
								          <td>&nbsp;</td>
								          <td>&nbsp;</td>
								          <td width="20%"><s:text name="garuda.cbbdefaults.lable.defaultShippingPaperWorkLoc"></s:text>:</td>
				       	 				  <td width="30%"><s:select name="cbb.fkDefaultShippingPaperWorkLocation" headerValue="" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DEFAULT_SHIPPING_PAPER_WRK_LOC]" disabled="true" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue=""></s:select></td>
								       </tr>
								       <tr>
								        <td width="30%"><s:text name="garuda.cbbdefaults.lable.dateFormat"></s:text>:</td>
				       	 				<td width="20%"><s:select name="cbb.fkDateFormat"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DATE_FORMAT]" listKey="pkCodeId" listValue="description" disabled="true" ></s:select></td>
						 				<td width="20%"><s:text name="garuda.cbbdefaults.lable.timeFormat"></s:text>:</td>
						 				<td width="30%"><s:select name="cbb.fkTimeFormat" headerKey="" headerValue="" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TIME_FORMAT]" listKey="pkCodeId" disabled="true" listValue="description" headerKey="-1" headerValue=""></s:select></td>
						 				
					 
									</tr>
									--></table>			
								</div>	
								</s:if>			            
				         </td>
				      </tr>
				      <!--<tr>
				         <td>
				              <div id="<s:property value="siteIdentifier" />content" onclick="toggleDiv('<s:property value="siteIdentifier" />')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
							  <span class="ui-icon ui-icon-triangle-1-s"></span>
							  <s:text name="garuda.cbbdefaults.lable.localidassignment" /></div>
								<div id="<s:property value='siteIdentifier' />"> 
								    <table cellpadding="0" cellspacing="0" width="100%" align="left">
								      <tr>
								          <td width="25%"><s:text name="garuda.cbbdefaults.lable.localcbuidassignmentassgnusing"></s:text> </td>
								          <td width="25%"><s:select name="cbb.localcbuassgnmentid" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LOCAL_ASSIGNMENT_ID]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="" ></s:select></td>
								      	  <td colspan="2"></td>
								      </tr>
								      <tr>
								      	<td colspan="4">
								      		<div id="localcbuassignid1"  <s:if test="#request.autoMaticSequencingLocalAssgnId!=cbb.localcbuassgnmentid">style="display: none;"</s:if>>
								       		<table width="100%">
								       		<tr>
								      		<td width="25%">
								   				<s:text name="garuda.cbbdefaults.lable.localcbuidprefix" />:
											</td>
								        	<td width="25%">
								        		<s:textfield name="cbb.localCbuIdPrefix" readonly="true" onkeydown="cancelBack();" />
								        	</td>
								        	<td width="25%">
								   				<s:text name="garuda.cbbdefaults.lable.localcbuidstrtnum" />:
											</td>
								        	<td width="25%">
								        		<s:textfield name="cbb.localCbuIdStrtNum" readonly="true" onkeydown="cancelBack();" />
								        	</td>
								        	</tr>
								        	</table></div>
								        </td>
								      		
								      </tr>
								      <tr>
								          <td width="25%"><s:text name="garuda.cbbdefaults.lable.localmaternalidassignmentassgnusing"></s:text> </td>
								          <td width="25%"><s:select name="cbb.localmaternalassgnmentid"  headerKey="-1" headerValue="" disabled="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LOCAL_ASSIGNMENT_ID]" listKey="pkCodeId" listValue="description" ></s:select></td>
								       	  <td colspan="2"></td>
								       </tr>
								     
								       <tr>
								       	<td colspan="4">
								      		<div id="localmaternalassignid1" <s:if test="#request.autoMaticSequencingLocalAssgnId!=cbb.localmaternalassgnmentid">style="display: none;"</s:if>>
								       		<table width="100%">
								       		<tr>
								      		<td width="25%">
								   				<s:text name="garuda.cbbdefaults.lable.localmaternalidprefix" />:
											</td>
								        	<td width="25%">
								        		<s:textfield name="cbb.localMaternalIdPrefix" readonly="true" onkeydown="cancelBack();" />
								        	</td>
								        	<td width="25%">
								   				<s:text name="garuda.cbbdefaults.lable.localmaternalidstrtnum" />:
											</td>
								        	<td width="25%">
								        		<s:textfield name="cbb.localMaternalIdStrtNum" readonly="true" onkeydown="cancelBack();" />
								        	</td>
								        	</tr>
								        	</table></div>
								        </td>
								      </tr>
								      
								       
									</table>
								</div>		
				         </td>
				      </tr>
				     
				      --><tr>
				      	<td>
				      	    <s:if test="hasViewPermission(#request.ViewLastmodby)==true">
				      		<div id="lastModifiedByMaincontent" onclick="toggleDiv('lastModifiedByMain')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
							 <span class="ui-icon ui-icon-triangle-1-s"></span>
						     <s:text name="garuda.cbbdefaults.lable.lastmodifiedby" /></div>
						     <div id="lastModifiedByMain">  
									  <table cellpadding="0" cellspacing="5" width="100%" align="left">
										<tr>		
											<td width="25%">
												<s:text name="garuda.unitreport.label.User_id" />:     				
								   			</td>
											<td width="25%">
												<s:if test="cbb.lastModifiedBy!=null">
												<s:set name="cellValue1" value="cbb.lastModifiedBy" scope="request"/>	
													<% 
													if(request.getAttribute("cellValue1")!=null)
													{
														String cellValue1 = request.getAttribute("cellValue1").toString();
														UserJB userB = new UserJB();
														UserAlertsHelper userHelp = new UserAlertsHelper();
														userB.setUserId(EJBUtil.stringToNum(cellValue1));
														userB.getUserDetails();													
															//String userName = userB.getUserLastName()+" "+userB.getUserFirstName();
															String userName = userHelp.getLogNameByUserId(new Long (EJBUtil.stringToNum(cellValue1)));
																request.setAttribute("userName",userName);
													}			
															%>									
													</s:if>
												<s:textfield name="#request.userName" readonly="true" onkeydown="cancelBack();"  />
											</td>
											<td width="25%">
												<s:text name="garuda.clinicalnote.label.dateTime" />:     				
								   			</td>
											<td width="25%">
												<s:date name="cbb.lastModifiedOn" id="createdOn1" format="MMM dd, yyyy / HH:mm" />
												<s:textfield name="createdOn1" readonly="true" onkeydown="cancelBack();"  />
											</td>
								        </tr>
								        </table>
								</div>
								</s:if>
				      	</td>
				      </tr>
				      <tr>
				         <td align="right">
				         <s:if test="diableCBBEditAndPrintFlag!=1">
				         	<s:if test="hasEditPermission(#request.editCbbPrfle)==true">
				              <button type="button" onclick="showModal('Edit CBB Profile <s:property value="siteName" escapeJavaScript="true"/>','editCBBDefaults?moduleEntityId=<s:property value="siteId" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBB" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_CBB" />&site.siteId=<s:property value="siteId" />&moduleEntityIdentifier=<s:property value="siteName" escapeJavaScript="true"/>',435,850)"><s:text name="garuda.cbbdefaults.lable.edit"/></button>
				            </s:if>
				              <button type="button" onclick="javaScript:return printCbbDefaults('<s:property value="siteId" />');"><s:text name="garuda.common.lable.print"/></button>
				         </s:if>
				            					          
					     </td>
				      </tr>
				      <tr><td></td></tr>
				</table></div></div></div>
				</div></s:form></s:iterator>