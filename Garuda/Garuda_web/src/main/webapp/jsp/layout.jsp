<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">
	
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="x-ua-compatible" content="IE=8" />
	<title>
		<tiles:insertAttribute name="title" />
	</title>
</head>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", 0); 
%>
<script>
function showprogressMgs(){
	$j('#progressindicator').dialog({
		autoOpen: false,
		resizable: false, width:400, height:90,closeOnEscape: false,
		modal: true}).siblings('.ui-dialog-titlebar').remove();	
	$j('#progressindicator').dialog("open");
}
function closeprogressMsg(){
	setTimeout(function(){
		$j('#progressindicator').dialog("close");
		$j('#progressindicator').dialog("destroy");
	},500);
}
</script>
<style>
<!--[if IE]>
#wrapper{
	overflow:inherit !important; 
}
<![endif]-->
</style>
<body style="position: fixed;">
	<div id="wrapper" style="height: 100%;width:100%;overflow-y: auto;">		
	<div id='progressindicator' style="display:none;">
		<table width='100%' height='100%'>
			<tr>
				<td width='100%' align='center'>
					<table>
						<tr>
							<td align='center'><s:text name="garuda.common.message.wait"/><img src="../images/jpg/loading_pg.gif" /></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
		<div id="navc" style="width:99%;">
		    <tiles:insertAttribute name="localization" />
		    <tiles:insertAttribute name="sessionlogin" />
	    	<tiles:insertAttribute name="navc" />
	    	<script>toggleNotification('off',"");</script>
		</div>
		
		<div id="commonheader">
			<tiles:insertAttribute	name="widget_setting" />
			<tiles:insertAttribute	name="cb_includes" />	
			<tiles:insertAttribute	name="includejs" />
				
			<!--<script>showprogressMgs();</script>			
		--></div>

		<div id="addwidget" style="width:100%;">
			<tiles:insertAttribute	name="addwidget" />
		</div>

		<div id="userbody">
			<div id="main" class="divmain">
				<tiles:insertAttribute name="userbody" />
			</div>
			<div id='progress-indicator' style="display:none;"><br><p class="displaycdr" align="center"><s:text name="garuda.layout.msg.processingRequest"/> <img src="../images/jpg/loading_pg.gif" /></p></div>
		</div>		

		<div id="footer" >
			<tiles:insertAttribute	name="footer" />
		</div>
		<!--<script>closeprogressMsg();</script>
	--></div>
</body>
<div id="modelPopup1"></div>
<div id="modelPopup2"></div>
<div id="dynaModalForm"></div>
<div id="widgetPrintDiv" style="display:none;"> 
<!-- This Used For Print -->
</div>
<div id="noNameDiv" style="display: none;"></div>
<div id="printTemp" style="display:none;"></div>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:9999;"></div>
</html>