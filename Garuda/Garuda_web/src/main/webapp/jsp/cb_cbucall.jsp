<%@ taglib prefix="s"  uri="/struts-tags"%>

<script>

 function showsearchTble(){
	 	document.getElementById('searchTble').setAttribute("style","display:true");
 }
 
 function beforecdrcbuscreen(localCbuId, registryId, numberOnCbuBag,cdrcbuId){

		updateBreadcrumb('CDR CBU View Screen','loadpage(\'jsp/cb_cdrcbuViewScreen.jsp\')');
		showCdrCbuScreen(localCbuId, registryId, numberOnCbuBag,cdrcbuId);
		
	}
	
 function showCdrCbuScreen(formname,localCbuId, registryId, numberOnCbuBag,cdrcbuId){
	  	  $j('#localCbuId').val(localCbuId);
		  $j('#registryId').val(registryId);
		  $j('#numberOnCbuBag').val(numberOnCbuBag);
		  $j('#cdrcbuId').val(cdrcbuId);
		  submitform(formname);
 }
 
 function constructTable() {
			 $j('#searchResults').dataTable();
		}

 function showDiv()
 {
			$j('#searchresultparent').css('display','block');
			refreshDiv('getCbuDetails','searchTble','cbuentry');
 }

</script> 


<div class="col_100 maincontainer"><div class="col_100">
<div class="column">
	<div class="portlet" id="cdrsearchparent">
	<div id="cdrsearch" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span><span class="ui-icon ui-icon-close" ></span><s:text name="garuda.cbuentry.label.cdrcbusearch" /></div><div class="portlet-content">
	<div id="studyDashDiv1" >   
	        <form id="cbuentry" >
	         <table cellpadding="0" cellspacing="0" border="0"
			 class="col_100">         
	         	<tr>
				   <td ><s:text name="garuda.cbuentry.label.cbuid" />:</td>
				   <td ><s:textfield  name="cdrCbuPojo.cdrCbuId" placeholder="CBU ID" onkeypress="return enter2Refreshdiv(event);"  /></td> 
				    <td><button type="button" onclick="showDiv();"><s:text name="garuda.opentask.label.search"/></button> </td>
				</tr>				
			</table>
	        </form>	
		</div></div></div></div></div>
        


<div class="column">
<div class="portlet" id="searchresultparent" style="display: none;"><div id="searchresult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span><span class="ui-icon ui-icon-close" ></span><s:text name="garuda.cbuentry.label.cdrcbusearchresult" /></div><div class="portlet-content">
<s:form action="cdrcbuViewScreen" id="search" method="post">
<s:hidden name="registryId" id="registryId"></s:hidden>
<s:hidden name="localCbuId" id="localCbuId"></s:hidden>
<s:hidden name="numberOnCbuBag" id="numberOnCbuBag"></s:hidden>
<s:hidden name="cdrcbuId" id="cdrcbuId"></s:hidden>
<div id="searchTble">
<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults" >
	<thead>
		<tr >
			<th><s:text name="garuda.cbuentry.label.localcbuid" /></th>
			<th><s:text name="garuda.cbuentry.label.registrycbuid" /></th>
			<th><s:text name="garuda.cbuentry.label.cbubag" /></th>			
		</tr>
	</thead>
	<tbody>
		<s:iterator value="lstObject" var="cdrcbu" status="row">
			<tr id="<s:property	value="%{#row.index}"/>" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')" onclick="javaScript:return showCdrCbuScreen('search','<s:property value="localCbuId" />','<s:property value="registryId" />','<s:property value="numberOnCbuBag" />','<s:property value="cdrCbuId" />');" >
			
				<td><s:property value="localCbuId" /></td>
				<td><s:property value="registryId" /></td>
				<td><s:property value="numberOnCbuBag" /></td>				
			</tr>
		</s:iterator>
	</tbody>

	<tfoot>
		<tr>
			<td colspan="3"></td>
		</tr>
	</tfoot>
</table>
</div>
</s:form>
</div></div></div></div>
</div></div>