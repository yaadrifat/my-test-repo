<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<%@page import="com.velos.ordercomponent.business.util.VelosMidConstants"%>
<jsp:useBean id="modCtlDao" scope="request"
	class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<%
	HttpSession tSession = request.getSession(true);
	GrpRightsJB grpRights = (GrpRightsJB) tSession
			.getAttribute("GRights");
	String accId = null;
	String logUsr = null;
	if (sessionmaint.isValidSession(tSession)) {
		accId = (String) tSession.getValue("accountId");
		logUsr = (String) tSession.getValue("userId");
		request.setAttribute("username", logUsr);
	}
%>
<script>
function addPrintContent(divId){
	var sourceFile ="";
	var patientStr=$j.trim($j("#patientId").val());
	var regidStr=$j.trim($j("#registryId").val());
	$j("#printDiv").html(sourceFile);
	sourceFile +="<table><thead><tr><th><center><h3>";
	 if(regidStr!="" && regidStr!="undefined"){
	 sourceFile +="<u>CBU Registry ID:"+regidStr+"</u>";
	}
	 if(patientStr!="" && patientStr!="undefined"){
	 	 sourceFile +="&nbsp;&nbsp;&nbsp;<u>Patient ID:"+patientStr+"</u>";
	 }
	 if(divId=="ctcbuavailconfbarDiv"){
		    	sourceFile +="</h3></center><br/></th></tr></thead><tbody><tr><td>"+$j("#cbuavailconfbarDiv").html()+"</td></tr></tbody></table>";
		 }
	 else if(divId=="heresolsubDiv"){
	    	sourceFile +="</h3></center><br/></th></tr></thead><tbody><tr><td>"+$j("#ctresolutionbarDiv").html()+"</td></tr></tbody></table>";
	 }
	 else{
		 	    sourceFile +="</h3></center><br/></th></tr></thead><tbody><tr><td>"+$j('#'+divId).html()+"</td></tr></tbody></table>";
		 }
	 var source = $j("#printDiv").html(sourceFile);
	 $j("#printDiv").hide();
	 if(divId=="openOrdercbuhis"){
		printHistoryWidget('openOrdercbuhis',regidStr);
	 }
	 else{
		 clickheretoprint('printDiv','','1');
	 }
	 }
function submitHeResolution(){
	if($j("#availdatepicker").val()==null){var availableDt="";}
	else{var availableDt=$j("#availdatepicker").val();}
	if($j("#ResolutionCBB").val()==null){var resolbycbb="";}
	else{var resolbycbb=$j("#ResolutionCBB").val();}
	if($j("#ResolutionTC").val()==null){var resolbytc="";}
	else{var resolbytc=$j("#ResolutionTC").val();}
	if($j("#ResoluAck").attr('checked')){var resolutioncheck="Y";}
	else{var resolutioncheck="N";}
	if($j("#currentuserid").val()==null){var userid="";}
	else{var userid=$j("#currentuserid").val();}

	loadmultiplediv('submitCtResolution?pkcordId='+$j('#pkcordId').val()+'&availableDt='+availableDt+'&resolbycbb='+resolbycbb+'&resolbytc='+resolbytc+'&ackResolution='+resolutioncheck+'&userID='+userid+'&orderType='+$j('#orderType').val()+'&orderId='+$j("#orderId").val(),'heresolsubDiv,openOrdercbuhis,openOrdercbuInfo');
	historyTblOnLoad();
	Fnresol();
	if($j('#canackorders').val()!=""){
		alert("<s:text name="garuda.cbu.order.orderCancelAlert"/> ");
    }
	getprogressbarcolor();
}
function shwMdal(pfcat,resistryId,divId)
{
	 var type = $j('#orderType').val();
	 var orderId = $j("#orderId").val();
	 var requestedDate = $j("#requestedDate").val();
	 var progresspermission = $j("#progressNotePermiossion").val(); 
	 var clinicalNotePermiossion = $j("#clinicalNotePermiossion").val();
	
		     	    	var subtp = '<s:text name="garuda.cbu.order.processConst"/>';
		     	    			 jConfirm('<s:text name="garuda.clinicalnote.label.confirmationmsg"/>', '<s:text name="garuda.common.lable.confirm"/>',
		     	    					 function(r) {
		     	   		     	 if (r == true) {
		     	   		     	    	var subtp = '<s:text name="garuda.cbu.order.noteConst"/>';
		     	   		     	   		if(clinicalNotePermiossion=="true")
		     	   		     	    			fn_showModalHENotes('<s:text name="garuda.header.label.clinicalNote" /> <s:property value="cdrCbuPojo.registryId" />','clinicalNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&subtype='+subtp,'550','850','notesModalsDiv');
		     	   		     	   		else
		     	   		     				alert('<s:text name ="garuda.clinicalNote.accessRights"/>');
		     	   		     	   return result;
		     	   		     	    }
		     	    	 		    else {
		     	    	 		     	    	var subtp = '<s:text name="garuda.cbu.order.processConst"/>';
		     	    	 		     	    	if(progresspermission=="true"){
		     	    	 		     	    		if(requestedDate!=null)
		    	 		     	    					{
		    	 		     	    						fn_showModalHENotes('<s:text name="garuda.header.label.progressNote"/> <s:property value="cdrCbuPojo.registryId" />','pfNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&pfcat='+pfcat+'&pkcordId='+$j("#pkcordId").val()+'&divId='+divId+'&requestedDate='+requestedDate,'600','850','notesModalDiv');
		    	 		     	    					}
		     	    	 		     				else
		 	 		     	    						{
		 	 		     	    							fn_showModalHENotes('<s:text name="garuda.header.label.progressNote"/> <s:property value="cdrCbuPojo.registryId" />','pfNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&pfcat='+pfcat+'&pkcordId='+$j("#pkcordId").val()+'&divId='+divId,'600','850','notesModalDiv');
		 	 		     	    						}
		     	    	 		     	    	}
		     	    	 		     	    	else
		     	    	 		     	    		{
		     	    	 		     	    				alert('<s:text name ="garuda.progressNote.accessRights"/>');
		     	    	 		     	    		}
		     	    	 		     	    }
		     	    	 		     });
		     
	  
}
function shwMdal1(resistryId,divId)
{
	 var type = $j('#orderType').val();
	 var orderId = $j("#orderId").val();
	 var requestedDate = $j("#requestedDate").val();
	var progresspermission = $j("#progressNotePermiossion").val(); 
	var clinicalNotePermiossion = $j("#clinicalNotePermiossion").val();
	 jConfirm('<s:text name="garuda.clinicalnote.label.confirmationmsg"/>', '<s:text name="garuda.common.lable.confirm"/>',
		       function(r) {
		     	 if (r == true) {
		     	    	var subtp = '<s:text name="garuda.cbu.order.noteConst"/>';
		     	    	if(clinicalNotePermiossion=="true")
		     	    		fn_showModalHENotes('<s:text name="garuda.header.label.clinicalNote" /> <s:property value="cdrCbuPojo.registryId" />','clinicalNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&subtype='+subtp,'500','850','notesModalsDiv');
		     	    	else
		     	    		alert('<s:text name ="garuda.clinicalNote.accessRights"/>');
		     	    	return result;
		     	    }
		     	    else {
		     	    	var subtp = '<s:text name="garuda.cbu.order.processConst"/>';
		     	    	if(progresspermission=="true")
   	    			{
		     	    	if(requestedDate!=null)
		     	    		{
		     	    		fn_showModalHENotes('<s:text name="garuda.header.label.progressNote"/> <s:property value="cdrCbuPojo.registryId" />','pfNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&pkcordId='+$j("#pkcordId").val()+'&divId='+divId+'&requestedDate='+requestedDate,'500','850','notesModalDiv');
		     	    		}
		     	    		
		     	    	else
		     	    		{
		     	    		fn_showModalHENotes('<s:text name="garuda.header.label.progressNote"/> <s:property value="cdrCbuPojo.registryId" />','pfNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&pkcordId='+$j("#pkcordId").val()+'&divId='+divId,'500','850','notesModalDiv');
		     	    		}
   	    			}
		     	    	else
   	    			{
   	    			alert('<s:text name ="garuda.progressNote.accessRights"/>');
   	    			}
		     	    }
		     });

}
function fn_showModalHENotes(title,url,hight,width,id){
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title=title+patient_data;
	showModals(title,url,hight,width,id);
	}
function fn_showModalHEdetails(title,url,hight,width){
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title=title+patient_data;
	showModal(title,url,hight,width);
	}
function savecordAvailNmdp(){
	var orderId = $j("#orderId").val();
	 var type = $j('#orderType').val();
	fn_showModalHEdetails('<s:text name="garuda.pf.modalwindowheader.cbuavailconfirm"/><s:property value="cdrCbuPojo.registryId" />','savecordAvailNmdp?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&cbuid=<s:property value="cdrCbuPojo.cordID" />'+'&moduleEntityId='+$j("#orderId").val()+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_CORD_AVAIL_CONFIRM" />','300','700');
}


</script>

<s:form id="heorderdetailsForm">
<table>
	<s:set name="orderType" value="orderType" scope="request" />
	<s:hidden name="clinicalNotePojo.requestType" />
	<s:hidden name="clinicalNotePojo.requestType" />
	<s:hidden name="cdrCbuPojo.registryId" id="registryId"/>
	<tr>
		<td width="100%">
		<div id="heordertaskListProgressBarDiv">
		<div class="portlet" id="openOrdercbuInfoparent">
		<s:if test="hasViewPermission(#request.currntReqst)==true">
		<div id="openOrdercbuInfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
		<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
		<span class="ui-icon ui-icon-print" onclick="getCurrentRequestProgressDetails('currentRequestProgressMainDiv,caseManagerPatientandTCdetailsDiv,ctReqClinInfoContentDiv,quicklinksDiv');addPrintContent('openOrdercbuInfo')"></span>
		<span class="ui-icon ui-icon-plusthick" id="currentReqProSpan" onclick="getCurrentRequestProgressDetails('currentRequestProgressMainDiv,caseManagerPatientandTCdetailsDiv,ctReqClinInfoContentDiv,quicklinksDiv');"></span>
		<!--<span class="ui-icon ui-icon-minusthick"></span>--> <s:text
			name="garuda.ctOrderDetail.portletname.currentReqProgress"></s:text>
		</div>
		<div class="portlet-content" id="currentRequestProgressMainDiv" style="display: none;">
		<s:hidden name="tskReqClincInfocompl" id="tskReqClincInfocompl"></s:hidden>
		<s:hidden name="tskFinalCbuReviewcompl" id="tskFinalCbuReviewcompl"></s:hidden>
		<s:hidden name="tskShipmentInfocompl" id="tskShipmentInfocompl"></s:hidden>
		<s:hidden name="tskGenPackageSlipcompl" id="tskGenPackageSlipcompl"></s:hidden>
		<s:hidden name="tskAdditiTypingcompl" id="tskAdditiTypingcompl"></s:hidden>
		<s:hidden name="tskShipmentConfirmcompl" id="tskShipmentConfirmcompl"></s:hidden>
		<s:hidden name="tskSampleShipmentcompl" id="tskSampleShipmentcompl"></s:hidden>
		<s:hidden name="tskAcknowledgeResolcompl"
			id="tskAcknowledgeResolcompl"></s:hidden>
			<input type="hidden" id="clinicalNotePermiossion" value="<s:property value="hasNewPermission(#request.vClnclNote)"/>"/>
    		<input type="hidden" id="progressNotePermiossion" value="<s:property value="hasNewPermission(#request.vPrgrssNote)"/>"/>
		<table>
					<tr>
						<td colspan="10" rowspan="5" align="right">
							<s:if test="hasViewPermission(#request.viewHEReport)==true">
							  <s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
							<button type="button" onclick="window.open('cbuPdfReports?cdrCbuPojo.CordID=<s:property value="cdrCbuPojo.CordID"/>&repId=182&repName=NMDP HE Detail Report&reqDate=<s:property value="%{#rowVal[45]}" />&selDate=&params=<s:property value="orderId"/>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><s:text
							name="garuda.ordersreport.button.label"></s:text></button>
							</s:iterator>
							</s:if>
							</s:if>
						</td>
					</tr>
				</table>
		<table>
			<tr style="font-style: italic; color: blue; font-size: 10px">
				<td width="10%" height="20px"></td>
				<td width="20%" align="center"><a href="#cbuavailconfbarDiv"
					id="cbuavailtd" onclick="togglePFDivs('cordAvailspan','ctcbuavailconfbarDiv','getCordAvailDetails')"><s:property value="tskConfirmCbuAvail" /></a></td>
				<td width="20%" align="center"><a href="#reqclincinfobarDiv" onclick="togglePFDivs('reqclinspanid','ctReqClinInfoContentDiv','')"><s:property
					value="tskReqClincInfo" /></a></td>
				<td width="20%" align="center"></td>
				<td width="20%" align="center"><a href="#ctresolutionbarDiv"
					id="resoltd" onclick="togglePFDivs('ctResoultionspan','heresolsubDiv','getResolutionDetails','heresolsubDiv')"><s:property value="tskAcknowledgeResol" /></a></td>
				<td width="10%" style="font-size: 12px; font: bold;" align="center"
					rowspan="4"><s:if
					test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
					<s:iterator value="ctOrderDetailsList" var="rowVal">
						<s:if test="%{#rowVal[48]>2}">
							<span style="color: red; font-size: 30px"><s:property
								value="%{#rowVal[48]}" /></span>
							<br />
							<s:text name="garuda.currentReqProgress.label.noofdays" />
						</s:if>
						<s:else>
							<span style="font-size: 30px""><s:property
								value="%{#rowVal[48]}" /></span>
							<br />
							<s:text name="garuda.currentReqProgress.label.noofdays" />
						</s:else>
					</s:iterator>
				</s:if></td>
			</tr>
			<tr>
				<td width="10%" height="20px"></td>
				<td width="20%" id="cbuavailconfbar"></td>
				<td width="20%" id="completeReqInfobar"></td>
				<td width="20%" id="Resolutionbar"></td>
				<td width="20%" id="Acknowledgebar"></td>

			</tr>
			<tr>
				<td width="10%"></td>
				<td width="20%">
				<table>
					<tr>
						<td width="10%" align="left"><img src="images/sort_asc.png"
							align="left" /></td>
						<td width="10%" align="right"><img src="images/sort_asc.png"
							align="right" /></td>
					</tr>
				</table>

				</td>
				<td width="20%" align="right"></td>
				<td width="20%" align="right"><img src="images/sort_asc.png" /></td>
				<td width="20%" align="right"><img src="images/sort_asc.png" /></td>

			</tr>
			<tr style="font-style: italic; color: green; font-size: 10px">
				<td width="10%"></td>
				<td width="20%">
				<table>
					<tr style="font-style: italic; color: green; font-size: 10px">
						<td width="10%" align="left"><s:property value="msNew" /></td>
						<td width="10%" align="right"><s:property value="msReserved" />
						</td>
					</tr>
				</table>
				</td>
				<td width="20%" align="right"></td>
				<td width="20%" align="right"><s:property value="msResolved" /></td>
				<td width="20%" align="right"><s:property value="msComplete" /></td>

			</tr>
		</table>
		<div id="currentRequestProgress">
		<table>
			<tr>
				<td width="100%" colspan="3">
				<fieldset>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="10%" align="left"><strong><s:text
							name="garuda.cbuHistory.label.requesttype" />:</strong></td>
						<td width="15%" align="left"><s:text
							name="garuda.heOrderDetail.portletname.heOrder" /></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.requeststatus" />:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="%{#rowVal[0]!=null && #rowVal[0]!=''}">
									<span style="color: red;"><s:property
										value="%{#rowVal[0]}" /></span>
								</s:if>
							</s:iterator>
						</s:if></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.reqStatusDate"></s:text>:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test='#rowVal[1]!=null && #rowVal[1]!=""'>
									<s:property value="%{#rowVal[1]}" />
								</s:if>
							</s:iterator>
						</s:if></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.resolbytc" />:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test='#rowVal[57]!=null && #rowVal[57]!=""'>
									<s:property value="getCbuStatusDescByPk(#rowVal[57])" />
									<s:hidden name="resolByTCFlag" id="resolByTCFlag" value="%{#rowVal[57]}"></s:hidden>
									<s:hidden name="resolByCBBFlag" id="resolByCBBFlag"	value="%{#rowVal[58]}"></s:hidden>
								</s:if>
								<s:elseif test='#rowVal[58]!=null && #rowVal[58]!=""'>
									<s:property value="getCbuStatusDescByPk(#rowVal[58])" />
										<s:hidden name="resolByTCFlag" id="resolByTCFlag" value="%{#rowVal[57]}"></s:hidden>
										<s:hidden name="resolByCBBFlag" id="resolByCBBFlag"	value="%{#rowVal[58]}"></s:hidden>
									</s:elseif>
							</s:iterator>
						</s:if></td>
					</tr>
					<tr>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.herequestdate" />:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="%{#rowVal[45]!=null && #rowVal[45]!=''}">
									<s:property value="%{#rowVal[45]}" />
									<s:hidden id="requestedDate" value="%{#rowVal[45]}" />
								</s:if>
							</s:iterator>
						</s:if></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.assignedto" /></strong>:</td>
						<td width="15%" align="left" class="testData">
						<s:if test="lstUsers!=null && lstUsers.size()>0">
									<s:hidden name="assignLstSize" id="assignLstSize" value="%{lstUsers.size()}"/>
						</s:if>
						<s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
							<s:hidden name="hAssignTo" id="hAssignTo" value="%{#rowVal[3]}"/>
								<s:if test='#rowVal[3]!=null && #rowVal[3]!=""'>
									<s:if test="lstUsers!=null && lstUsers.size()>0">
										<s:if
											test="hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true">
											<s:if test="hasEditPermission(#request.assignOrders)!=true">
												<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled" >
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test='%{#rowVal[3]==#tempVal}' >selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
												</select>
											</s:if>
											<s:else>
												<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test='%{#rowVal[3]==#tempVal}' >selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
												</select>
											</s:else>
										</s:if>
									</s:if>

								</s:if>
								<s:elseif
									test="(hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true) && (#rowVal[3]==null || #rowVal[3]=='')">
									<s:if test="lstUsers!=null && lstUsers.size()>0">
										<s:if test="hasEditPermission(#request.assignOrders)!=true">
											<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' ><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
											</select>
										</s:if>
										<s:else>
											<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" >
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' ><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
											</select>
										</s:else>
									</s:if>
								</s:elseif>
							</s:iterator>
						</s:if> <s:else>

							<s:if
								test="hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true">
								<s:if test="lstUsers!=null && lstUsers.size()>0">
									<s:if test="hasEditPermission(#request.assignOrders)!=true">
										<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
										</select>
									</s:if>
									<s:else>
										<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>'><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
																			
										</select>
									</s:else>
								</s:if>
							</s:if>
						</s:else>
						</td>
						<s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if
									test='#rowVal[3]!=null && #rowVal[3]!="" && #rowVal[4]!="Not Provided"'>
									<td width="10%" align="left"><strong><s:text
										name="garuda.currentReqProgress.label.assigneddate" />:</strong></td>
									<td width="15%" align="left"><s:if
										test="#rowVal[4]!=null && #rowVal[4]!=''">
										<s:property value="%{#rowVal[4]}" />
									</s:if>
								</s:if>
								<s:else>
									<td width="10%" align="left"></td>
									<td width="15%" align="left"></td>
								</s:else>
							</s:iterator>
						</s:if>
						<s:else>
							<td width="10%" align="left"></td>
							<td width="15%" align="left"></td>
						</s:else>
						<td width="10%" align="left"><strong><s:text
							name="garuda.heResolution.label.resolutiondate" />:</strong></td>
						<td width="15%" align="left"><s:if
							test="orderResolLst!=null && orderResolLst.size>0">
							<s:iterator value="orderResolLst" var="rowVal">
								<s:if test="#rowVal[2]!=null && #rowVal[2]!=''">
									<s:property value="%{#rowVal[2]}" />
								</s:if>
							</s:iterator>
						</s:if></td>
						
					</tr>
					<tr>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.priority"></s:text>:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="%{#rowVal[64]!=null && #rowVal[64]!=''}">
									<s:property value="%{#rowVal[64]}" />
								</s:if>
								<s:else>
									<s:text name="garuda.currentReqProgress.label.notprovided" />
								</s:else>
							</s:iterator>
						</s:if> <s:else>
							<s:text name="garuda.currentReqProgress.label.notprovided" />
						</s:else></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.lastrequestreviewedby" />:</strong>
						</td>

						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test='#rowVal[5]!=null && #rowVal[5]!=""'>
									<s:property value="%{#rowVal[5]}" />
								</s:if>
							</s:iterator>
						</s:if></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.lastrevieweddate" />:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:hidden name="recRecDateFlag" id="recRecDateFlag"
									value="%{#rowVal[65]}"></s:hidden>
								<s:if test="#rowVal[6]!=null && #rowVal[6]!=''">
									<s:property value="%{#rowVal[6]}" />
								</s:if>
							</s:iterator>
						</s:if></td>
						<td width="10%" align="left"><strong><s:text
							name="garuda.currentReqProgress.label.closereason" />:</strong></td>
						<td width="15%" align="left"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="%{#rowVal[75]!=null && #rowVal[75]!=''}">
									<s:property value="getCbuStatusDescByPk(#rowVal[75])" />
								</s:if>
							</s:iterator>
						</s:if></td>
					</tr>
				</table>
				</fieldset>
				</td>
			</tr>
		</table>
		</div>
		</div>
		</div>
		</s:if>
		</div>
		</div>
		<div id="ctassignto" style="display: none;">
						<table bgcolor="#cccccc" class="tabledisplay disable_esign" id="">
							<tr bgcolor="#cccccc" valign="baseline">
								<td width="70%"><jsp:include page="cb_esignature.jsp"
									flush="true">
									<jsp:param value="submitAssignto_HE" name="submitId" />
									<jsp:param value="invalidAssignto" name="invalid" />
									<jsp:param value="minimumAssignto" name="minimum" />
									<jsp:param value="passAssignto" name="pass" />
								</jsp:include></td>
								<td align="left" width="30%"><input type="button"
									id="submitAssignto_HE" onclick="submitAssignto();"
									value="<s:text name="garuda.unitreport.label.button.submit"/>"
									disabled="disabled" /> <input type="button" id="closediv"
									onclick="$j('#ctassignto').hide();"
									value="<s:text name="garuda.common.close"/>" /></td>
							</tr>
						</table>
		</div>
		</td>
	</tr>
	<tr>
		<td>
		<div id="heorderdiv">
		<div id="pendingOrdersSearchparent">
		<div id="openOrderSearch">
		<table>
			<tr>
				<td width="49.5%" style="vertical-align: top">
				   <s:if test="hasViewPermission(#request.viewPatienttc)==true">
					<div class="portlet" id="openOrderrecTcInfoparent">
					<div id="openOrderrecTcInfo"
						class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
					<div
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-print"
						onclick="getCaseManagerPatientDetails();addPrintContent('openOrderrecTcInfo')"></span><span
						class="ui-icon ui-icon-newwin"></span> <span id='openOrderrecTcInfospan' class="ui-icon ui-icon-plusthick" onclick="getCaseManagerPatientDetails();"></span>
					<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
						class="ui-icon ui-icon-minusthick"></span>--> <s:text
						name="garuda.ctOrderDetail.portletname.recipient&tcInfo" /></div>
					<div class="portlet-content" id="caseManagerPatientandTCdetailsDiv" style="display: none;">
					<table>
						<s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<tr>
									<td colspan="2">
									<table width="100%">
										<tr>
											<td width="25%" align="left"></td>
											<td width="25%" align="left"></td>
											<td width="50%" align="right" colspan="2">
												<input type="button"
													value='<s:text name="garuda.recipient&tcInfo.label.button.viewantigens"/>'
													onclick="showAntigensModal('<s:property value="%{#rowVal[13]}"/>','<s:property value="%{#rowVal[87]}"/>');" />
											</td>
										</tr>
										<tr>
											<td width="25%" align="left"><strong><s:text
												name="garuda.recipient&tcInfo.label.recipientid" /></strong>:</td>
											<td width="25%" align="left"><s:property
												value="%{#rowVal[13]}" /></td>
											<td width="50%" align="right" colspan="2"></td>
										</tr>
										<tr>
											<td width="25%" align="left"><strong><s:text
												name="garuda.recipient&tcInfo.label.age" /></strong>:</td>
											<td width="25%" align="left"><s:property
												value="%{#rowVal[11]}" /></td>
											<td width="25%" align="left"><strong><s:text
												name="garuda.recipient&tcInfo.label.tcname" /></strong>:</td>
											<td width="25%" align="left"><s:property
												value="%{#rowVal[7]}" /></td>
										</tr>
										<tr>
											<td width="25%" align="left"><strong><s:text
												name="garuda.recipient&tcInfo.label.sex" /></strong>:</td>
											<td width="25%" align="left"><s:property
												value="%{#rowVal[12]}" /></td>
											<td width="25%" align="left"><strong><s:text
												name="garuda.recipient&tcInfo.label.tcno" /></strong>:</td>
											<td width="25%" align="left"><s:property
												value="%{#rowVal[8]}" /></td>
										</tr>
										<tr>
											<td width="25%" align="left"><strong><s:text
												name="garuda.tcorderdetails.label.currentdiagnosis" /></strong>:</td>
											<td width="25%" align="left"><s:property
												value="%{#rowVal[86]}" /></td>
											<td width="25%" align="left"><strong><s:text
												name="garuda.recipient&tcInfo.label.sectcno" /></strong>:</td>
											<td width="25%" align="left"><s:property
												value="%{#rowVal[9]}" /></td>
										</tr>
									</table>
									</td>
								</tr>
							</s:iterator>
						</s:if>
					</table>
					<br>
					<br>
					<table>
						<s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<tr>
									<td width="25%"><strong><s:text
										name="garuda.caseMangerInfo.label.cmid" /></strong>:</td>
									<td width="25%"><s:property value="%{#rowVal[15]}" /></td>
									<td width="25%"><strong><s:text
										name="garuda.caseMangerInfo.label.cmemail" /></strong>:</td>
									<td width="25%"><a
										href='mailto:<s:property value="%{#rowVal[17]}"/>'><s:property
										value="%{#rowVal[17]}" /></a></td>
								</tr>
								<tr>

									<td width="25%"><strong><s:text
										name="garuda.caseMangerInfo.label.cmname" /></strong>:</td>
									<td width="25%"><s:property value="%{#rowVal[16]}" /></td>
									<td width="25%"><strong><s:text
										name="garuda.caseMangerInfo.label.cmphno" /></strong>:</td>
									<td width="25%"><s:property value="%{#rowVal[18]}" /></td>
								</tr>
								<tr>
									<td width="100%" colspan="4" align="right">
										<input type="button"
											value='<s:text name="garuda.recipient&tcInfo.label.button.cmdirectory"/>'
											onclick="readNMDPXML('1');" />
									</td>
								</tr>
							</s:iterator>
						</s:if>
						<!--<s:else>
											<tr>
					    						<td width="25%">
					    							<strong><s:text name="garuda.caseMangerInfo.label.cmid"/></strong>:
					    						</td>
					    						<td width="25%">
					    							
					    						</td>
					    						<td width="25%">
													<strong><s:text name="garuda.caseMangerInfo.label.cmemail"/></strong>:
												</td>
												<td width="25%">
													
												</td>
					    						</tr>
												<tr>
												
												<td width="25%">
					    							<strong><s:text name="garuda.caseMangerInfo.label.cmname"/></strong>:
					    						</td>
					    						<td width="25%">
					    							
					    						</td>
												<td width="25%">
													<strong><s:text name="garuda.caseMangerInfo.label.cmphno"/></strong>:
												</td>
													<td width="25%">
													
												</td>
												</tr>
										</s:else>-->
					</table>
					</div>
					</div>
					</div>
					</s:if>
					<br>
					<br>
				<s:if test="hasViewPermission(#request.viewCbuAvailCon)==true">
					<div class="portlet" id="cbuavailconfbarDivparent">
					<div id="cbuavailconfbarDiv"
						class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
					<div
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-print"
						onclick="getCordAvailDetails();addPrintContent('ctcbuavailconfbarDiv')"></span><span
						class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick"  id="cordAvailspan" onclick="getCordAvailDetails();"></span>
					<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
						class="ui-icon ui-icon-minusthick"></span>--> <s:text
						name="garuda.ctOrderDetail.portletname.cbuAvailconformation" /></div>
					<div class="portlet-content" id="ctcbuavailconfbarDiv" style="display: none;"><s:if
						test="cordAvailConformLst!=null && cordAvailConformLst.size()>0">
						<s:iterator value="cordAvailConformLst" var="rowVal">
							<table>
								<tr>
									<td><strong><s:text
										name="garuda.cbuAvailconformation.label.cbuavailfornmdp" /></strong>:<span
										style="color: red;">*</span></td>
									<td><s:if test="#rowVal[0]!=null && #rowVal[0]!=''">
										<s:if test='%{#rowVal[0]=="Y"}'>
											<s:text name="garuda.cbushipmetInfo.label.yes"></s:text>
										</s:if>
										<s:if test='%{#rowVal[0]=="N"}'>
											<s:text name="garuda.cbushipmetInfo.label.no"></s:text>
										</s:if>
									</s:if><s:hidden id="cordavailflag" value="%{#rowVal[4]}"></s:hidden></td>
								</tr>
								<s:if test='%{#rowVal[0]!=null && #rowVal[0]=="N"}'>
									<tr>
										<td width="50%"><strong><s:text
											name="garuda.cbuAvailconformation.label.unavailreason" />:</strong></td>
										<td width="50%"><s:property value="%{#rowVal[1]}" /></td>
									</tr>
								</s:if>
								<s:if test="#rowVal[4]!=null">
									<tr>
										<td width="50%"><strong><s:text
											name="garuda.cbuAvailconformation.label.cbuavailconfirmdays" />:</strong>
										</td>
										<td width="50%"><s:property value="%{#rowVal[5]}" /></td>
									</tr>
								</s:if>
								<s:if test="%{(#rowVal[2]!=null && #rowVal[2]!='') && (#rowVal[3]!=null && #rowVal[3]!='')}">
								<tr>
									<td width="100%" colspan="2">
									<fieldset><legend> </legend>
									<table>
										<tr>
											<td width="25%"><strong><s:text
												name="garuda.cbuAvailconformation.label.submittedby" /></strong>:</td>
											<td width="25%"><s:property value="%{#rowVal[2]}" /></td>
											<td width="25%"><strong><s:text
												name="garuda.cbuAvailconformation.label.submitteddate" /></strong>:</td>
											<td width="25%"><s:property value="%{#rowVal[3]}" /></td>
										</tr>
									</table>
									</fieldset>
									</td>
								</tr>
								</s:if>
							</table>
						</s:iterator>
					</s:if>
					<table id="heAvailNotesTable">
										<tbody>
											<s:set name="cbuCateg"
												value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT,@com.velos.ordercomponent.util.VelosGarudaConstants@AVAIL_CNFRM_CODESUBTYPE) " />
											<s:iterator value="#request.categoryPfNotes[#cbuCateg]"
												var="rowVal" status="row">
												<s:set name="amendFlag1" value="%{#rowVal[6]}"
													scope="request" />
												<s:set name="tagNote" value="%{#rowVal[7]}" scope="request" />
												<s:set name="notes" value="%{#rowVal[8]}" scope="request" />
												<s:set name="cellValue6" value="cellValue" scope="request" />
												<tr>
													<td><s:if test="#request.tagNote==true">
														<img width="20" height="16" src="images/exclamation.jpg">
													</s:if></td>
													<s:iterator value="rowVal" var="cellValue" status="col">
														<s:if test="%{#col.index == 1}">
															<s:set name="cellValue1" value="cellValue"
																scope="request" />
															<td
																<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
															<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
															<%
														String creatorName="";
														if (request.getAttribute("cellValue1") != null
							 									&& !request
							 											.getAttribute("cellValue1")
							 											.equals("")) {
															creatorName = request.getAttribute(
							 										"cellValue1").toString();
							 							}
															
																						UserJB userB = new UserJB();
																						userB.setUserId(EJBUtil
																								.stringToNum(creatorName));
																						userB.getUserDetails();
														
														%> <%=userB.getUserLastName() + " "
												  + userB.getUserFirstName()%>
															</td>
														</s:if>
														<s:if test="%{#col.index == 2}">
															<td
																<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
																name="garuda.cbu.order.progressNote" /><s:property /></td>
														</s:if>
														<s:if test="%{#col.index == 0}">
															<td
																<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
																name="garuda.clinicalnote.label.posted_on"></s:text> <s:set
																name="cellValue15" value="cellValue" /> <s:date
																name="cellValue15" id="cellValue15"
																format="MMM dd, yyyy / hh:mm:ss a" /> <s:property
																value="cellValue15" /></td>
														</s:if>
														<s:if test="%{#col.index == 3}">

															<td
																<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
															<s:text name="garuda.clinicalnote.label.subject"></s:text>:
															<s:property /></td>
														</s:if>
															<s:if test="%{#col.index == 4}">
														
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:set name="cellValue3" value="cellValue"
																	scope="request" /> <s:iterator
																	value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]"
																	var="subType">


																	<%-- <s:if test="subType==#cellValue">
																		<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
																		<s:property value="description" />
																	</s:if> --%>
																</s:iterator>
															
														</td>
													</s:if>

														<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
														<s:if test="%{#col.index == 5}">
															<td><a href="#" onClick="loadMoredivs('revokePfCategoryNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&pkcordId=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&orderType=<s:property value="orderType"/>&orderId=<s:property value="orderId"/>&category=<s:property value="#request.cbuCateg"/>','heAvailNotesTable,heProgressNotesTable','heorderdetailsForm')">
															<s:text name="garuda.clinicalnote.label.revoke" /></a></td>
														</s:if>
														</s:if>

														<s:if test="%{#col.index == 5}">
															<td><a href="#"
																onClick="fn_showModalHEdetails('Progress Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfViewNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property />','500','850');">
															<s:text name="garuda.clinicalnote.label.viewnotes" /></a></td>
														</s:if>

													</s:iterator>
												</tr>
												<tr>
												<td></td>
													<td colspan = "6" <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
															<div style="overflow: auto;">
															<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
													        </div>
													</td>
											<tr>
											</s:iterator>
										</tbody>

									</table>
									<table>
										<tr>
											<td colspan="6">
												<a href="#"
													onclick="shwMdal('AVAIL_CNFRM_CODESUBTYPE','<s:property value="cdrCbuPojo.registryId"/>','heAvailNotesTable');"><s:text
													name="garuda.cdrcbuview.label.button.addnewnotes" /></a>
										</td>
										</tr>
									</table>
					<div <s:if test="orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED) || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)">style="display:none"</s:if> >
					 <s:if test="hasEditPermission(#request.viewCbuAvailCon)==true">
					<table>
						<tr>
							<s:if test="cordAvailConformLst!=null && cordAvailConformLst.size()>0">
									<s:iterator value="cordAvailConformLst" var="rowVal">
							<s:if test="#rowVal[0]==null || #rowVal[0]==''">
								<td align="right"><input type="button" id="saveCbuavail"
									onclick="savecordAvailNmdp()"
									value="<s:text name="garuda.cbbprocedures.label.edit" />" /></td>
							</s:if>
							</s:iterator>
							</s:if>
						</tr>
					</table>
					  </s:if>
					</div>
					</div>
					</div>
					</div>
					</s:if>
					<br>
					<br>
					 <s:if test="hasViewPermission(#request.viewreqclincinfo)==true">
					<div class="portlet" id="reqclincinfobarDivparent">
					<div id="reqclincinfobarDiv"
						class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
					<div
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-print"
						onclick="addPrintContent('reqclincinfobarDiv')"></span><span
						class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id="reqclinspanid"></span>
					<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
						class="ui-icon ui-icon-minusthick"></span>--> <s:text
						name="garuda.ctOrderDetail.portletname.requiredClinicalInfo" /></div>
					<div class="portlet-content" style="display: none;" id="ctReqClinInfoContentDiv">
					<table>
						<tr>
							<td colspan="2"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<s:hidden name="completeReqInfoTaskFlag"
										id="completeReqInfoTaskFlag" value="%{#rowVal[63]}"></s:hidden>
								</s:iterator>
							</s:if>
							<table width="100%">
								<tr>
									<td width="25%"><strong><s:text
										name="garuda.requiredClinicalInfo.label.checkliststatus" /></strong>:</td>
									<td width="25%"><s:hidden name="clinchkstat" /> <s:if
										test="clinchkstat==null">
										<label style="color: red;"><s:text
											name="garuda.requiredClinicalInfo.label.notstarted" /></label>

									</s:if> <s:elseif test="clinchkstat==0">
										<label style="color: blue;"><s:text
											name="garuda.requiredClinicalInfo.label.pending" /></label>
									</s:elseif> <s:elseif test="clinchkstat==1">
										<label style="color: green;"><s:text
											name="garuda.requiredClinicalInfo.label.completed" /></label>
									</s:elseif></td>
									 <s:if test="hasViewPermission(#request.cmpltReqInfo)==true">
									<td width="50%" colspan="2"><strong><a href="#"
										onclick="f_callCompleReqInfo('<s:property value="cdrCbuPojo.registryId"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="orderId"/>','<s:property value="orderType"/>');"><s:text
										name="garuda.requiredClinicalInfo.label.clinicalinfostatus" /></a></strong></td>
									</s:if>
								</tr>
								<tr>
									<td width="25%"></td>
									<td width="25%"></td>
									<td width="25%"></td>
									<td width="25%"></td>
								</tr>
							</table>
							</td>

						</tr>
					</table>
					</div>
					</div>
					</div>
					<br>
					<br>
					</s:if>
				</td>
				<td width="1%"></td>
				<td width="49.5%" style="vertical-align: top">
				<s:if test="hasViewPermission(#request.viewHEResol)==true">
					<div class="portlet" id="ctresolutionbarDivparent">
					<div id="ctresolutionbarDiv"
						class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
					<div
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-print"
						onclick="getResolutionDetails('heresolsubDiv');addPrintContent('heresolsubDiv')"></span><span
						class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id="ctResoultionspan" onclick="getResolutionDetails('heresolsubDiv');"></span>
						<!--<span class="ui-icon ui-icon-close"></span> <span
						class="ui-icon ui-icon-minusthick"></span>--> <s:text
						name="garuda.heOrderDetail.portletname.heResolution" /></div>
					<div id="heresolsubDiv" class="portlet-content" style="display: none;"><s:hidden
						name="cancelled_ack_orders" id="canackorders"></s:hidden> <s:if
						test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
						<s:iterator value="ctOrderDetailsList" var="rowVal">
						</s:iterator>
					</s:if> <s:if test="orderResolLst!=null && orderResolLst.size>0">
						<s:iterator value="orderResolLst" var="rowVal">
							<table width="100%">
								<tr>
									<td width="40%"><strong> <s:text
										name="garuda.currentReqProgress.label.closereason" /></strong>:</td>
									<td width="40%"><s:if
										test="%{#rowVal[10]!=null && #rowVal[10]!=''}">
										<s:property value="getCbuStatusDescByPk(#rowVal[10])" />
										<s:hidden name="orderCloseRsn" id="orderCloseRsn"
											value="%{#rowVal[10]}"></s:hidden>
									</s:if></td>
									<td width="20%"></td>
								</tr>
								<tr>
									<td colspan="3">
									<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td width="40%"><strong><s:text
											name="garuda.heResolution.label.resolutiondate" /></strong>:</td>
										<td width="40%"><s:if
											test="%{#rowVal[2]!=null && #rowVal[2]!=''}">
											<s:property value="%{#rowVal[2]}" />
										</s:if></td>
										<td width="20%"></td>
									</tr>
									</table>
									</td>
								</tr>

								<tr>
									<td colspan="3"><s:if
										test="%{#rowVal[1]!=null && #rowVal[1]!=''}">
										<table id="resolBytc">
											<tr>
												<td width="40%"><strong><s:text
													name="garuda.ctResolution.label.resolutionbytc" /></strong>:</td>
												<td width="40%"><s:if
													test="%{#rowVal[1]!=null && #rowVal[1]!=''}">
													<s:hidden name="tcResolVal" id="tcResolVal"
														value="%{#rowVal[1]}" />
													<s:hidden name="ResolutionTC" id="ResolutionTC"
														value="%{#rowVal[1]}"></s:hidden>
													<s:property value="getCbuStatusDescByPk(#rowVal[1])" />
												</s:if></td>
												<td width="20%"></td>
											</tr>
											<tr>
												<td width="40%"><strong><s:text
													name="garuda.ctResolution.label.acknowledgetcresolution" /></strong>:<span
													style="color: red;">*</span></td>
												<td width="40%">
												<s:if test='%{#rowVal[9]!=null && #rowVal[9]=="Y"}'>
															<s:checkbox name="ResoluAck"
															id="ResoluAck" disabled="true" checked="true"/><s:text
															name="garuda.ctResolution.label.resolutionbytc" /> <s:hidden
															name="acknowledgeFlag" id="acknowledgeFlag"
															value="%{#rowVal[9]}"></s:hidden>
														</s:if>
														<s:else>
															<s:checkbox name="ResoluAck"
															id="ResoluAck" disabled="true" /><s:text
															name="garuda.ctResolution.label.resolutionbytc" />
														</s:else>
													</td>
												<td width="20%"></td>
											</tr>


											<s:if test="%{#rowVal[9]!=null && #rowVal[9]!=''}">
												<tr>
													<td width="40%"><strong><s:text
														name="garuda.cbu.order.ackDateTime" /></strong>:</td>
													<td width="40%"><s:if
														test="%{#rowVal[6]!=null && #rowVal[6]!=''}">
														<s:property value="%{#rowVal[6]}" />
													</s:if></td>
													<td width="20%"></td>
												</tr>
												<tr>
													<td width="40%"><strong><s:text
														name="garuda.cbu.order.ackBy" /></strong>:</td>
													<td width="40%">
													<s:if test="%{#rowVal[7]!=null && #rowVal[7]!='' && #rowVal[7]!=0}">
														<s:set name="username"
														value="%{#rowVal[7]}" scope="request" /> 
														<%
														String cellValue1 = "";
							 							if (request.getAttribute("username") != null
							 									&& !request
							 											.getAttribute("username")
							 											.equals("")) {
							 								cellValue1 = request.getAttribute(
							 										"username").toString();
							 							}
													 	
								 						UserJB userB = new UserJB();
								 						userB.setUserId(EJBUtil.stringToNum(cellValue1));
								 						userB.getUserDetails();%>
								 						<%=userB.getUserLastName() + " "+ userB.getUserFirstName()%>
														</s:if>
								 						<s:if test="%{#rowVal[7]==null || #rowVal[7]==0}">
														  <s:text name="garuda.cbuhistory.label.system" />
														</s:if>
								 					</td>
													<td width="20%"></td>
												</tr>
											</s:if>
											<s:else>
												<%
												String cellValue1 = "";
					 							if (request.getAttribute("username") != null
					 									&& !request
					 											.getAttribute("username")
					 											.equals("")) {
					 								cellValue1 = request.getAttribute(
					 										"username").toString();
					 							}
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();
												%>
												<input type="hidden" name="currentuser" id="currentuserid" value=<%=logUsr%>>
											</s:else>



										</table>

									</s:if> <s:elseif
										test="%{ {{#rowVal[1]==null || #rowVal[1]==''} && {#rowVal[0]==null || #rowVal[0]==''}} || {#rowVal[0]!=null && #rowVal[0]!=''} }">
										<table cellpadding="0" cellspacing="0" border="0"
											id="resolByCbb">
											<tr>
												<td width="40%"><strong><s:text
													name="garuda.ctResolution.label.resolutionbycbb" /></strong>:</td>
												<td width="40%"><s:if
													test="%{#rowVal[0]!=null && #rowVal[0]!=''}">
													<s:property value="getCbuStatusDescByPk(#rowVal[0])" />
												</s:if></td>
												<td width="20%"></td>
												<s:hidden name="hresolcbb" id="hresolcbb"
													value="%{#rowVal[0]}" />
											</tr>
											<s:if test="%{#rowVal[0]!=null && #rowVal[0]!=''}">
												<tr>
													<td width="40%"><strong><s:text
														name="garuda.ctResolution.label.acknowledgetcresolution" /></strong>:<span
														style="color: red;">*</span></td>
													<td width="40%"><s:if test='%{#rowVal[9]!=null && #rowVal[9]=="Y"}'>
															<s:checkbox name="ResoluAck"
															id="ResoluAck" disabled="true" checked="true"/><s:text
															name="garuda.ctResolution.label.resolutionbytc" /> <s:hidden
															name="acknowledgeFlag" id="acknowledgeFlag"
															value="%{#rowVal[9]}"></s:hidden>
														</s:if>
														<s:else>
															<s:checkbox name="ResoluAck"
															id="ResoluAck" disabled="true" /><s:text
															name="garuda.ctResolution.label.resolutionbytc" />
														</s:else></td>
													<td width="20%"></td>
												</tr>
											</s:if>
											<s:if test="%{#rowVal[9]!=null && #rowVal[9]!=''}">
												<tr>
													<td width="40%"><strong><s:text
														name="garuda.cbu.order.ackDateTime" /></strong>:</td>
													<td width="40%"><s:if
														test="%{#rowVal[6]!=null && #rowVal[6]!=''}">
														<s:property value="%{#rowVal[6]}" />
													</s:if></td>
													<td width="20%"></td>
												</tr>
												<tr>
													<td width="40%"><strong><s:text
														name="garuda.cbu.order.ackBy" /></strong>:</td>
													<td width="40%">
													<s:if test="%{#rowVal[7]!=null && #rowVal[7]!='' && #rowVal[7]!=0}">
													<s:set name="username"
														value="%{#rowVal[7]}" scope="request" /> 
														<%
														String cellValue1 = "";
							 							if (request.getAttribute("username") != null
							 									&& !request
							 											.getAttribute("username")
							 											.equals("")) {
							 								cellValue1 = request.getAttribute(
							 										"username").toString();
							 							}
									 						UserJB userB = new UserJB();
									 						userB.setUserId(EJBUtil.stringToNum(cellValue1));
									 						userB.getUserDetails();
														 %>
														 <%=userB.getUserLastName() + " "+ userB.getUserFirstName()%>
													</s:if>	 
													<s:if test="%{#rowVal[7]==null || #rowVal[7]==0}">
														  <s:text name="garuda.cbuhistory.label.system" />
														</s:if>
														 </td>
												<td width="20%"></td>
												</tr>
											</s:if>
											<s:else>
												<%
												String cellValue1 = "";
					 							if (request.getAttribute("username") != null
					 									&& !request
					 											.getAttribute("username")
					 											.equals("")) {
					 								cellValue1 = request.getAttribute(
					 										"username").toString();
					 							}
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();
												%>
												<input type="hidden" name="currentuser" id="currentuserid" value=<%=logUsr%>>
											</s:else>
										</table>
									</s:elseif></td>
								</tr>
							</table>
						</s:iterator>
					</s:if> 
					<table id = "heresolution">
										<tbody>
											<s:set name="cbuCateg"
												value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT,@com.velos.ordercomponent.util.VelosGarudaConstants@RESOLUTION_CODESUBTYPE) " />
											<s:iterator value="#request.categoryPfNotes[#cbuCateg]"
												var="rowVal" status="row">
												<s:set name="amendFlag1" value="%{#rowVal[6]}"
													scope="request" />
												<s:set name="tagNote" value="%{#rowVal[7]}" scope="request" />
												<s:set name="notes" value="%{#rowVal[8]}" scope="request" />
												<s:set name="cellValue6" value="cellValue" scope="request" />
												<tr>
													<td><s:if test="#request.tagNote==true">
														<img width="20" height="16" src="images/exclamation.jpg">
													</s:if></td>
													<s:iterator value="rowVal" var="cellValue" status="col">
														<s:if test="%{#col.index == 1}">
															<s:set name="cellValue1" value="cellValue"
																scope="request" />
															<td
																<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
															<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
															<%
														String creatorName="";
														if (request.getAttribute("cellValue1") != null
							 									&& !request
							 											.getAttribute("cellValue1")
							 											.equals("")) {
															creatorName = request.getAttribute(
							 										"cellValue1").toString();
							 							}
															
																						UserJB userB = new UserJB();
																						userB.setUserId(EJBUtil
																								.stringToNum(creatorName));
																						userB.getUserDetails();
														
														%> <%=userB.getUserLastName() + " "
												  + userB.getUserFirstName()%></td>
														</s:if>
														<s:if test="%{#col.index == 2}">
															<td
																<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
																name="garuda.cbu.order.progressNote" /><s:property /></td>
														</s:if>
														<s:if test="%{#col.index == 0}">
															<td
																<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
																name="garuda.clinicalnote.label.posted_on"></s:text> <s:set
																name="cellValue15" value="cellValue" /> <s:date
																name="cellValue15" id="cellValue15"
																format="MMM dd, yyyy / hh:mm:ss a" /> <s:property
																value="cellValue15" /></td>
														</s:if>
														<s:if test="%{#col.index == 3}">

															<td
																<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
															<s:text name="garuda.clinicalnote.label.subject"></s:text>:
															<s:property /></td>
														</s:if>
														<s:if test="%{#col.index == 4}">
														
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:set name="cellValue3" value="cellValue"
																	scope="request" /> <s:iterator
																	value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]"
																	var="subType">


																	<%-- <s:if test="subType==#cellValue">
																		<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
																		<s:property value="description" />
																	</s:if> --%>
																</s:iterator>
															
														</td>
													</s:if>

														<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
														<s:if test="%{#col.index == 5}">
															<td><a href="#" onClick="loadMoredivs('revokePfCategoryNote?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&pkcordId=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&orderType=<s:property value="orderType"/>&orderId=<s:property value="orderId"/>&category=<s:property value="#request.cbuCateg"/>','heresolution,openOrdernotesContent','heorderdetailsForm')">
															<s:text name="garuda.clinicalnote.label.revoke" /></a></td>
														</s:if>
														</s:if>

														<s:if test="%{#col.index == 5}">
															<td><a href="#"
																onClick="fn_showModalHEdetails('Progress Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfViewNote?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property />','500','850');">
															<s:text name="garuda.clinicalnote.label.viewnotes" /></a></td>
														</s:if>

													</s:iterator>
												</tr>
												<tr>
												<td></td>
													<td colspan = "6" <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
															<div style="overflow: auto;">
															<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
													</div></td>
											<tr>
											</s:iterator>
										</tbody>
									</table>
									<a href="#"
											onclick="shwMdal('RESOLUTION_CODESUBTYPE','<s:property value="cdrCbuPojo.registryId"/>','heresolution');"><s:text
											name="garuda.cdrcbuview.label.button.addnewnotes" /></a>
					<s:if test="hasEditPermission(#request.viewHEResol)==true">
					<s:if test="orderResolLst!=null && orderResolLst.size()>0">
						<s:iterator value="orderResolLst" var="rowVal">
							<s:if
									test='((orderPojo.resolByCbb!= null && orderPojo.resolByCbb!="") || (orderPojo.resolByTc!=null && orderPojo.resolByTc!=""))'>
									<s:if test='%{#rowVal[9]==null || #rowVal[9]=="" || #rowVal[9]=="N"}'>
								<table>
									<tr>
										<td align="right"><input type="button"
											onclick="checkResolution();"
											value="<s:text name="garuda.common.lable.edit"/>"
											id="submitHeResol" /></td>
									</tr>
								</table>
								</s:if>
							</s:if>
						</s:iterator>
					</s:if>
					</s:if>
					</div>
					</div>
					</div>
				</s:if>
					<s:if test="hasViewPermission(#request.viewPrgrssNotes)==true">
					<div class="portlet" id="openOrdernotesparent">
					<div id="openOrdernotes"
						class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
					<div
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-print"
						onclick="addPrintContent('openOrdernotes')"></span><span
						class="ui-icon ui-icon-newwin"></span> <span id="progress-notes" class="ui-icon ui-icon-plusthick"></span>
					<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
						class="ui-icon ui-icon-minusthick"></span>--> <s:text
						name="garuda.ctOrderDetail.portletname.notes" /></div>
					<div class="portlet-content" id="openOrdernotesContent" style="display: none;">
					<table id = "heProgressNotesTable">
					
						<s:iterator  value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT]">
												<s:set name="cbuCateg1" value="pkCodeId" scope="request"/>
										<s:iterator value="#request.categoryPfNotes[#request.cbuCateg1]" var="rowVal" status="row">
											<s:set name="amendFlag1" value="%{#rowVal[6]}"
												scope="request" />
											<s:set name="tagNote" value="%{#rowVal[7]}" scope="request" />
											<s:set name="notes" value="%{#rowVal[8]}" scope="request" />
											<s:set name="cellValue6" value="cellValue" scope="request" />
											<tr>
												<td><s:if test="#request.tagNote==true">
													<img width="20" height="16" src="images/exclamation.jpg">
												</s:if></td>
												<s:iterator value="rowVal" var="cellValue" status="col">
													<s:if test="%{#col.index == 1}">
														<s:set name="cellValue1" value="cellValue" scope="request" />
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
														<%
														String creatorName="";
														if (request.getAttribute("cellValue1") != null
							 									&& !request
							 											.getAttribute("cellValue1")
							 											.equals("")) {
															creatorName = request.getAttribute(
							 										"cellValue1").toString();
							 							}
															
																						UserJB userB = new UserJB();
																						userB.setUserId(EJBUtil
																								.stringToNum(creatorName));
																						userB.getUserDetails();
														
														%> <%=userB.getUserLastName() + " "
												  + userB.getUserFirstName()%></td>
													</s:if>
													<s:if test="%{#col.index == 2}">
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
															name="garuda.cbu.order.progressNote" /><s:property /></td>
													</s:if>
													<s:if test="%{#col.index == 0}">
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
															name="garuda.clinicalnote.label.posted_on"></s:text> <s:set
															name="cellValue15" value="cellValue" /> <s:date
															name="cellValue15" id="cellValue15"
															format="MMM dd, yyyy / hh:mm:ss a" /> <s:property
															value="cellValue15" /></td>
													</s:if>
													<s:if test="%{#col.index == 3}">

														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<s:text name="garuda.clinicalnote.label.subject"></s:text>:
														<s:property /></td>
													</s:if>
													<s:if test="%{#col.index == 4}">
														
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:set name="cellValue3" value="cellValue"
																	scope="request" /> <s:iterator
																	value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]"
																	var="subType">


																	<%-- <s:if test="subType==#cellValue">
																		<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
																		<s:property value="description" />
																	</s:if> --%>
																</s:iterator>
															
														</td>
													</s:if>

													<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
													<s:if test="%{#col.index == 5}">
														<td><a href="#"
															onClick="loadMoredivs('revokePfCategoryNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&pkcordId=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&orderType=<s:property value="orderType"/>&orderId=<s:property value="orderId"/>&category=<s:property value="#request.cbuCateg"/>','heAvailNotesTable,heresolution,openOrdernotesContent','heorderdetailsForm')">
														<s:text name="garuda.clinicalnote.label.revoke" /></a></td>
													</s:if>
													</s:if>

													<s:if test="%{#col.index == 5}">
														<td><a href="#"
															onClick="fn_showModalHEdetails('Progress Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfViewNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property />','500','850');">
														<s:text name="garuda.clinicalnote.label.viewnotes" /></a></td>
													</s:if>

												</s:iterator>
											</tr>
											<tr>
											<td></td>
													<td colspan = "6" <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<div style="overflow: auto;">	<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
													</div></td>
											<tr>
											</s:iterator>
										</s:iterator>
					</table>

					
						<a href="#"
							onclick="shwMdal1('<s:property value="cdrCbuPojo.registryId"/>','heAvailNotesTable,heresolution');"><s:text
							name="garuda.cdrcbuview.label.button.addnewnotes" /></a>
					</div>
					</div>
					</div>
					<br>
					<br>
					<br>
					</s:if>
		          <s:if test="hasViewPermission(#request.viewRqstCBUhis)==true">
					<div class="portlet" id="openOrdercbuhisparent">
					<div id="openOrdercbuhis"
						class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
					<div
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-print"
						onclick="addPrintContent('openOrdercbuhis');"></span><span
						class="ui-icon ui-icon-newwin"></span> <span id="cbu-req-history" class="ui-icon ui-icon-plusthick orderHistorydata"></span>
					<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
						class="ui-icon ui-icon-minusthick"></span>--> <s:text
						name="garuda.ctOrderDetail.portletname.cbunrequesthistory" /></div>
					<div class="portlet-content" style="display: none;">
					<table>
						<tr>
							<td colspan="2"><jsp:include page="cb_cbuHistory.jsp"></jsp:include>
							</td>
						</tr>
					</table>
					</div>
					</div>
					</div>
					</s:if>
				</td>
			</tr>
		</table>
		</div>
		</div>
		</div>
		</td>
	</tr>
</table>
</s:form>
<div id="printDiv" style="hidden: true"></div>
<div id="dummyDiv"></div>