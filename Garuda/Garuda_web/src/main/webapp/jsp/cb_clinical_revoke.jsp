<%@ include file="cb_includes.jsp"%>
<div id ="revoked">
						<tbody>
												<s:if test ="#request.category>0">													
														 <s:set name="cbuCateg" value="#request.categoryPfNotes.keySet().toArray()[0]" scope="request"/> 
													</s:if>
													<s:iterator value="#request.categoryPfNotes[#cbuCateg]" var="rowVal" status="row">
								 					<s:set name="amendFlag1" value="%{#rowVal[6]}" scope="request"/>
														<s:set name="cellValue6" value="cellValue" scope="request"/>
														<tr>		
													<s:iterator value="rowVal" var="cellValue" status="col">
													<s:if test="%{#col.index == 1}">
												 	<s:set name="cellValue1" value="cellValue" scope="request"/>
													<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
														String cellValue1 = request.getAttribute("cellValue1").toString();
														UserJB userB = new UserJB();
														userB.setUserId(EJBUtil.stringToNum(cellValue1));
														userB.getUserDetails();													
													%>		
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>										
													</td>
								</s:if>
								<s:if test="%{#col.index == 2}">
										<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text name="garuda.cbu.order.progressNote"/><s:property /></td>
								</s:if>
										<s:if test="%{#col.index == 0}">
												<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text name="garuda.clinicalnote.label.posted_on"></s:text>
												<s:property  /></td>
											</s:if>
												<s:if test ="%{#col.index == 3}" >
			
												<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<s:text name="garuda.clinicalnote.label.subject"></s:text>
														<s:property />
												</td>
											</s:if>
											<s:if test ="%{#col.index == 4}" >
											<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<fieldset style="display:inline-block" >
						   									<legend> 
						   										<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>
															</legend>
																<table width="100%" cellpadding="0" cellspacing="0">
											    					<tr>
											        					<td <s:if test="#request.cellValue10==true">style="text-decoration:line-through;"</s:if>>
											        						<s:set name="cellValue3" value="cellValue" scope="request"/> 
														         			 <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]" var="subType" >
														    				 
														    				
														    				<s:if test="subType==#cellValue">
														         			<input type="radio" checked="checked" disabled="disabled"  ><s:property value="description"/>
														    				</s:if> 
																		</s:iterator>
											        					</td>
											    					</tr>								
																</table>
														</fieldset>
											</td>
											</s:if>
												<td>
											
											<s:if test ="%{#col.index == 5}" >
												<a href ="#" onClick="loadPageByGetRequset('revokePfCategoryNote?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&orderType=<s:property value="orderType"/>&orderId=<s:property value="orderId"/>&category=<s:property value="#request.cbuCateg"/>','revoked')"> <s:text name="garuda.clinicalnote.label.revoke" /></a>
											</s:if>
											</td>
											<td>
												<s:if test ="%{#col.index == 5}" >
												<a href ="#" onClick="showModal('Progress Notes for Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfViewNote?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property />','500','850');"> <s:text name="garuda.clinicalnote.label.viewnotes" /></a>
												
												</s:if>
											</td>
							</s:iterator>
							</tr>
							</s:iterator>	
							
							</tbody>
	</div>