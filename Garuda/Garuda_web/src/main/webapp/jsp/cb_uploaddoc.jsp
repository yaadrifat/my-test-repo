<%@ taglib prefix="s"  uri="/struts-tags"%>

<!-- 
<jsp:include page="cb_widget-setting.jsp" flush="true">
<jsp:param value="1" name="pageId"/>
</jsp:include> -->



<script>
function showDiv()
{
	//document.getElementById('searchresultparent').style.display = 'block';
			$j('#searchresultparent').css('display','block');
			refreshDiv('getCbuDetail','searchTble','searchCbuInfo');
}		
function showDocument(attachmentId){
	
	//alert( " docId " + docId);
	var url = cotextpath+"/viewdocument1.action?attachmentId="+attachmentId;
	//window.open( url, "Attachment", "status = 1, height = 500, width = 800, resizable = 0" );
	window.open( url);
}
function constructTable() {
	  // alert("ready Volunteer")
		 $j('#searchResults').dataTable();
		 var tdObj = $j('#searchResults').find('tbody').find("tr:eq(0)").find("td:eq(0)");
			if($j(tdObj).hasClass('dataTables_empty')){
				$j('#searchResults_info').hide();
				$j('#searchResults_paginate').hide();
			}
		}
		
		
function showAddWidget(url){
	 url = url + "?pageId=1" ; 
	 showAddWidgetModal(url);
}
</script>

<div class="col_100 maincontainer ">
<div class="col_100">
		<table width="100%" id="container1" >
		<tr><td>
			<div class="column">
			<div class="portlet" id="cdrsearchparent">
			<div id="cdrsearch" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			<span class="ui-icon ui-icon-minusthick"></span>
			<!--<span class="ui-icon ui-icon-close" ></span>-->
			<s:text name="garuda.cbuentry.label.cdrcbusearch" />
			</div>
			<div class="portlet-content">
			<div id="studyDashDiv1" > 
  
				
	        	<s:form id="searchCbuInfo" onsubmit="showDiv(); return false;" method="post">
			         <table cellpadding="0" cellspacing="0" border="0"
					style="width: 100%; height: auto;" class="col_100 font11em">         
			         	<tr>
						   <td ><s:text name="garuda.cbuentry.label.cbuid" /></td>
						   <td ><s:textfield  name="cdrCbuPojo.cdrCbuId" placeholder="CBU ID" /></td> 
						
						
						    <td><button type="button" onclick="javascript:showDiv();"><s:text name="garuda.uploaddoc.label.search" /></button> </td>
						</tr>				
					</table>
	        	</s:form>
	        	
			</div></div></div></div></div>
		</td></tr>
		</table>
       
 <table width="100%" id="container2" ><tr><td>
<div class="column">

<div class="portlet" id="searchresultparent" style="display: none;"><div id="searchresult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span><!--<span class="ui-icon ui-icon-close" ></span>--><s:text name="garuda.cbuentry.label.cdrcbusearchresult" /></div><div class="portlet-content">

<div id="searchTble">

<table style="height:100%"  border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults" >
       
	
                <thead>
                      <tr>
                        
                        <th class="th2"  scope="col"><s:text name="garuda.uploaddoc.label.date" /></th>
                        <th class="th3"  scope="col"><s:text name="FileName" /></th>
                        <th class="th5"  scope="col"><s:text name="garuda.uploaddoc.label.view" /></th>
                                             
                      </tr>
			         </thead>  
			  <tbody>     
			  
			<s:iterator value="cordInfoList" var="rowVal" status="row">
			        
				<tr>
				<td><s:date name="%{#rowVal[4]}" id="uploadDate" format="MMM dd, yyyy" /><s:property value="%{#uploadDate}" /></td>
				<td><s:property value="%{#rowVal[3]}" /></td>	
				<td><a href="#"><img src="images/attachment-icon.png" onclick="javascript: return showDocument('<s:property value="%{#rowVal[0]}" />')" /></a></td>
				<%-- <a href="#" onclick="javascript: return showDocument('<s:property value="%{#rowVal[5]}" />')" ><s:property value="%{#rowVal[8]}" /></a></td> --%>	
					
				</tr>
			</s:iterator>
						
				      
              </tbody>  
               
                
				  <tfoot>
					<tr><td colspan="6"></td></tr>
				  </tfoot>
                
                </table>
	
        </div>
		</div>
       
         </div>

</div>
</div>
</td></tr>     
       
 </table>             
       
       </div>
</div> 

