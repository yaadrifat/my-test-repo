<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants" %>
<jsp:include page="../cb_track_session_logging.jsp" />
<script>

$j("#elistshistry").dataTable();
var elgelgiChangesDone = false;
var elgelgiFcrChangesDone = false;
$j(function(){
	$j("#eligible1").validate({
		ignore: ":hidden",
		invalidHandler: function(form, validator) {
        var errors = validator.numberOfInvalids();
        if (errors) {
            validator.errorList[0].element.focus();
        }
    },
    rules:{			
				"fkCordCbuEligible":{required:true},
				"inCompReasons":{
									required:{
          									depends: function(element) {
												 return ($j("#fkCordCbuEligible").val()==$j("#incompleteReasonId").val());
												 }
											}
									},
				"inEligibleReasons":{
									required:{
										depends: function(element) {
												 return ($j("#fkCordCbuEligible").val()==$j("#inEligiblePk").val());
												 }
											}
									},							
				"cordAdditionalInfo":{
									required:{
          									depends: function(element) {
											 return (($j("#fkCordCbuEligible").val()==$j("#notCollectedToPriorReasonId").val()));
											 }
										},
									maxlength:300
										}
				
				
			},
			messages:{

				"fkCordCbuEligible":"<s:text name="garuda.cbu.cordentry.cordeligible"/>",
				"inEligibleReasons":"<s:text name="garuda.cbu.clinicalnote.reason"/>",
				"inCompReasons":"<s:text name="garuda.cbu.clinicalnote.reason"/>",
				"cordAdditionalInfo":{
	                 		required : "<s:text name="garuda.cbu.cordentry.relevantinfo"/>",
	                 		maxlength : "<s:text name="garuda.cbu.cordentry.maxchar"/>"
					}
			},
			errorPlacement: function(error, element) {
				if (element.is(":radio")) {
	            error.appendTo(element.parent());
				}
				else{
					 error.insertAfter(element);
				}
	        }
		});
	//$j("#finalDeclQstns").html('');
	});

</script>
<jsp:include page="../cb_eligibility_questionnaire_validation.jsp"></jsp:include>
<s:if test = "#request.shipMentFlag!=null && #request.shipMentFlag==true">
   <script> 
   
   addRules(new_cord_entry_elig_quest_validtaion_rules);
   
   </script>
</s:if>
<s:else>
  <script>
  addRules(old_elig_quest_validtaion_rules);
  </script>
</s:else>
<script>
$j(document).ready(function(){
	
	getDatePic();
	var eligibleTextArea = $j('#validate_count').val();
	if(eligibleTextArea!=null){
		eligibleTextArea = $j.trim(eligibleTextArea);
	}
	if('<s:property value="esignFlag"/>'=="modalFCRElig"){
		$j('#finalDeclQstnsOnUpdtElgblMdl').hide();
	}
	if($j('#licensureConfirmFlag').val()=="1" && eligibleTextArea=="" && $j('#eligibleConfirmFlag').val()=="1" && $j('#finalReviewConfirmStatus').val()=="Y"){
		$j('#fkCordCbuEligible').attr('disabled',true);
		$j('#fkCordCbuEligibleReason').attr('disabled',true);
		$j('#fkCordCbuIncompleteReason').attr('disabled',true);
		$j('#word_count').attr('disabled',true);
		
		if($j("#finalDeclNewReadOnly").length!=0){
			disableFormFldsinChng('finalDeclNewReadOnly');
		}
		if($j("#finalDeclReadOnly").length!=0){
			disableFormFldsinChng('finalDeclReadOnly');
		}
	}

		$j('#fkCordCbuEligible').change(function(){
			resetingProcess(this.value);
			
		});
		
	 /**
	  * Character Counter for inputs and text areas showing characters left.
	  */
	 $j('#word_count').each(function(){
	     //maximum limit of characters allowed.
	     var maxlimit = 300;
	     // get current number of characters
	     var length = $j(this).val().length;
	     if(length >= maxlimit) {
	   $j(this).val($j(this).val().substring(0, maxlimit));
	   length = maxlimit;
	  }
	     // update count on page load
	     $j(this).parent().find('#counter').html( (maxlimit - length) + ' <s:text name="garuda.cbu.cordentry.charleft"/>');
	     // bind on key up event
	     $j(this).keyup(function(){
	  // get new length of characters
	  var new_length = $j(this).val().length;
	  if(new_length >= maxlimit) {
	    $j(this).val($j(this).val().substring(0, maxlimit));
	    //update the new length
	    new_length = maxlimit;
	   }
	  // update count
	  $j(this).parent().find('#counter').html( (maxlimit - new_length) + ' <s:text name="garuda.cbu.cordentry.charleft"/>');
	     });
	 });
});
function limitText(txtField, maxLength) {
	if (txtField.value.length > maxLength) {
		txtField.value = txtField.value.substring(0, maxLength)
		
	} 
}
function notEligibilityApplicable(val){
	var noMandatory=$j("#eligible1").valid();
	if(noMandatory){
	if(val==$j("#notCollectedToPriorReasonId").val()){
		$j( "#dialog-confirm" ).dialog({
			resizable: false,
			modal: true,
			closeText: '',
			closeOnEscape: false ,
			buttons: {
				"Yes": function() {
					$j( this ).dialog( "close" );
					$j('#NAEligibleReasonFlag').val("true");
					var url = 'submitEligiblestatus?&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId=<s:property value="cdrCbuPojo.cordID"/>&noMandatory='+noMandatory+'&esignFlag=submitNAEligibleFcr';
					if('<s:property value="#request.module"/>'!='CBU'){
						modalFormSubmitRefreshDiv('eligible1',url,'finalreviewmodel','update','status');
					}
					else{
						modalFormSubmitRefreshDiv('eligible1',url,'finalreviewmodel','update1','status1');
					}
					$j('#blckaction').val("0"); 
					var historyArr=new Array();
					var visibleHistoryPart="";
					var notvisibleHistoryPart="";
					var i=0; 
					
				
					setTimeout(function(){
						$j('.detailhstry').each(function(){
							if($j(this).is(':visible')){
								var idval=$j(this).attr("id");
								historyArr[i]=idval;
								i++;
							}
						});
					},0);
					
					
					if($j('#cReqhistory').is(":visible")){
						visibleHistoryPart="cReqhistory";
						notvisibleHistoryPart="chistory";
					}
					if($j('#chistory').is(":visible")){
						visibleHistoryPart="chistory";
						notvisibleHistoryPart="cReqhistory";
					}
					
				
					var status = 'status1';
					var update = 'update1';
					if('<s:property value="#request.module"/>'=='CBU'){
												
						var url = 'submitEligiblestatus?esignFlag=reviewCRI&pkcordId='+$j('#cordId').val();
						if('<s:property value="#request.resetFun"/>'=='reset'){
							url = url+'&resetOnce=once';
						}
						setTimeout(function(){
						//loadMoredivs(url,'eligibiltyDetailsDiv,test','eligible1',update,status);
						},0);
						setTimeout(function(){
							loadMoredivs('getCbuHistory?pkcordId=<s:property value="cdrCbuPojo.cordID" />','cReqhistory,chistory','cbuCordInformationForm');
							if(visibleHistoryPart!=""){
								$j('#'+visibleHistoryPart).show();
								$j('#'+notvisibleHistoryPart).hide();
							}
							$j('#cbuHistoryForm').removeClass('historyLoaded')
							//historyTblOnLoad();
						},0);
						
						$j('input[name=acceptance]').attr({'disabled': true});
						setTimeout(function(){
							RefreshPrevious('<s:property value="cdrCbuPojo.cordID"/>');
							if('<s:property value="#request.resetFun"/>'=='reset'){
								$j('#divRef').html($j('#eligibiltyDetailsDiv').html());	
								var cbuid = document.getElementById('cbuLicStatus');
								var criflag = $j('#divRef #criCompFlg').val();
								criflag=$j.trim(criflag);
								var crimrqflg = $j('#divRef #criMrqFlg').val();
								crimrqflg=$j.trim(crimrqflg);
								$j('#divRef').html("");
								if(cbuid!=null && criflag!=1 && crimrqflg!=1){
									alert('<s:text name="garuda.cbufinalreview.label.fcr.CRI.alert"/>');
									window.close();
								}
							}
							},0);
						}
					else{
						setTimeout(function(){
							modalFormSubmitRefreshDiv('eligible1','submitEligiblestatus?esignFlag=modal&pkcordId='+$j('#cordId').val(),'');
						},0);
						if('<s:property value="cdrCbuPojo.cordnmdpstatus"/>==<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@AC)"/> || <s:property value="cdrCbuPojo.fkCordCbuStatus"/>==<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH)"/> || <s:property value="cdrCbuPojo.fkCordCbuStatus"/>==<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)"/>'){
							if('<s:property value="isCdrUser"/>'==true){
								setTimeout(function(){
									//refreshMultipleDivsOnViewClinical('1026','<s:property value="cdrCbuPojo.cordID"/>');
									
											loadMoredivs('getCurrReqDetailsFromViewClin?orderId=<s:property value="orderPojo.pk_OrderId"/>&orderType=<s:property value="getCodeListDescById(orderPojo.orderType)"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />','currentRequestProgressMainDiv,quicklinksDiv','cbuCordInformationForm');
											currReqflag=1;
											$j("#userIdval").width("100px");
											getSearchableSelect('userIdval','id');
											setpfprogressbar();
											getprogressbarcolor();
										
								},2000);
							}
						}
						setTimeout(function(){
							loadMoredivs('getCbuHistory?pkcordId=<s:property value="cdrCbuPojo.cordID" />','cReqhistory,chistory','cbuCordInformationForm');
							if(visibleHistoryPart!=""){
								$j('#'+visibleHistoryPart).show();
								$j('#'+notvisibleHistoryPart).hide();
							}
							$j('#cbuHistoryForm').removeClass('historyLoaded')
							historyTblOnLoad();
						},0);
						setTimeout(function(){
							//alert("historyArr::length"+historyArr.length)
							$j.each(historyArr,function(i,val){
								var Index=val.charAt(val.length-1);
								$j("#icondiv"+Index).triggerHandler('click');
							});
						},0);
						
						}
					setTimeout(function(){
						if('<s:property value="#request.refreashFinalCBUScr"/>' !=null && '<s:property value="#request.refreashFinalCBUScr"/>'=='true'){
						showrightdiv1('cbuFinalEligibilityStatus?esignFlag=reviewFCR&status=eligble&licenceUpdate=false&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR_VIEW_ELIGIBLE" />','eligibleDiv');
						}
						},0);
					refreshMultipleDivsOnViewClinical('1026','<s:property value="cdrCbuPojo.cordID"/>');
					getJQButton();
					
				},
				"No": function() {
					$j( this ).dialog( "close" );
					$j('#blckaction').val("0");
					if('<s:property value="fkCordCbuEligible"/>'!=""){
						$j('#fkCordCbuEligible').val('<s:property value="fkCordCbuEligible"/>');
						inEligibleinCompleteReason('<s:property value="fkCordCbuEligible"/>');
					}
					else{
						$j('#fkCordCbuEligible').val('-1');
						inEligibleinCompleteReason('-1');
					}
				}
			}
	
		});
	}
}
}

function resetingProcess(val){
	if('<s:property value="cdrCbuPojo.cbuLicStatus"/>'=='<s:property value="#request.licensedPK"/>' && (val=='<s:property value="cdrCbuPojo.inEligiblePkid"/>'||val=='<s:property value="cdrCbuPojo.incompleteReasonId"/>')){


		$j( "#dialog-resetElig-1" ).dialog({
			resizable: false,
			modal: true,
			closeText: '',
			closeOnEscape: false ,
			buttons: {
				"Yes": function() {
					$j( this ).dialog( "close" );
					
					 //var url = 'updateFinalEligibleStatus?module=CBU&esignFlag=modal&resetFun=reset&licenceUpdate=True&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&fkCordCbuEligible='+val;
				    	//if('<s:property value="resetFun"/>'=="reset"){
					    	//url=url+'&resetAll=resetOnce';
				    	//}
				    	//else{
				    		//url=url+'&resetAll=resetAll';
					    //}
				    	var input = document.createElement("input");
				    	input.setAttribute("type", "hidden");
				    	input.setAttribute("name", "cdrCbuPojo.cbuLicStatus");
				    	input.setAttribute("id","cbuLicStatus");
				    	input.setAttribute("value", '<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON)"/>');
				    	document.getElementById("licDyn").appendChild(input);
				    	input = document.createElement("input");
				    	input.setAttribute("type", "hidden");
				    	input.setAttribute("name", "licReasons");
				    	input.setAttribute("id","fkCordCbuUnlicenseReason");
				    	input.setAttribute("value", '<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON_INTERNATIONAL)"/>');
				    	document.getElementById("licDyn").appendChild(input);
				    	//showModal('Eligibility Determination',url,'390','700');
				    	
				},
				"No": function() {
					$j( this ).dialog( "close" );
					$j('#fkCordCbuEligible').val('<s:property value="fkCordCbuEligible"/>');
				    inEligibleinCompleteReason('<s:property value="fkCordCbuEligible"/>');
				}
			}
		});
	}
}

function enableFields(value){
	value = $j.trim(value);
	if(value==""){
		document.getElementById("errormsg").innerHTML="<s:text name="garuda.update.status.resonToChange"/>";
		$j('#fkCordCbuEligible').attr('disabled',true);
		$j('#fkCordCbuEligibleReason').attr('disabled',true);
		$j('#fkCordCbuIncompleteReason').attr('disabled',true);
		$j('#word_count').attr('disabled',true);
		if($j("#finalDeclNewReadOnly").length!=0){
			disableFormFldsinChng('finalDeclNewReadOnly');
		}
		if($j("#finalDeclReadOnly").length!=0){
			disableFormFldsinChng('finalDeclReadOnly');
		}
		}
	if(value!=""){
		document.getElementById("errormsg").innerHTML="";
		$j('#fkCordCbuEligible').attr('disabled',false);
		$j('#fkCordCbuEligibleReason').attr('disabled',false);
		$j('#fkCordCbuIncompleteReason').attr('disabled',false);
		$j('#word_count').attr('disabled',false);
		if((('<s:property value="hasEditPermission(#request.eligbleSumQues)"/>')=='true')){
			$j("#finalDeclNewReadOnly input[type=radio]").removeAttr('disabled');
			$j("#finalDeclNewReadOnly textarea").removeAttr('disabled');
			$j("#finalDeclReadOnly input[type=radio]").removeAttr('disabled');
			$j("#finalDeclReadOnly textarea").removeAttr('disabled');
			}
		}
}

function eligibleSubmit(){
	refreshDiv1('submitEligibleFinalReview','isEligibleDiv','eligible1');
}
function disableFormFldsinChng(formId){
	$j("#"+formId+" :input:not(:button)").each(function(){
	  	if($j(this).attr('type')!="radio"){
	 		$j(this).attr('disabled', 'disabled');
	  	}else{
		 	 	 if(!$j(this).is(":checked")){
				 $j(this).attr('disabled', 'disabled');
			}
	  	}	
  });
}
</script>
<script>
function beforesubmitEligible(){
	var criResetFlag = false;
	if(('<s:property value="fkCordCbuEligible"/>' != $j("#notCollectedToPriorReasonId").val()) && ('<s:property value="#request.shipMentFlag"/>'=="true")){
   		if(($j("#fkCordCbuEligible").val()==$j("#notCollectedToPriorReasonId").val()) && formValueChk("finalDeclQstnsOnUpdtElgblMdl")){
   			$j('#blckaction').val("1");
   			notEligibilityApplicable($j("#fkCordCbuEligible").val());
   			
   		}
   		else{
   				$j('#blckaction').val("0");
   				
   			}
   	}
	
   if($j('#blckaction').val()!="1"){
	elgelgiChangesDone = divChanges('elgChanges',$j('#ineligibleDiv').html());
	elgelgiFcrChangesDone = divChanges('elgFcrChanges',$j('#ineligibleDiv').html());
	if($j("#eligible1").valid()){
	var orderId = $j("#orderId").val();
	var orderType = $j("#orderType").val();
	clearOrders(orderId,orderType);
	var eligTextArea = $j('#validate_count').val();
	if(eligTextArea!=null){
		eligTextArea = $j.trim(eligTextArea);
	}
	if($j('#licensureConfirmFlag').val()=="1" && eligTextArea=="" && $j('#eligibleConfirmFlag').val()=="1" && $j('#finalReviewConfirmStatus').val()=="Y"){
		 document.getElementById("errormsg").innerHTML="<s:text name="garuda.update.status.resonToChange"/>";
	}
	else{
		if(elgelgiChangesDone || elgelgiFcrChangesDone){
		
		if('<s:property value="esignFlag"/>'=="reviewCRIview"){
			var noMandatory=true;
			if($j('#fkCordCbuEligible').val()==""){
				noMandatory=false;
			}else if($j('#fkCordCbuEligible').val()=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON)"/>' && $j('#fkCordCbuEligibleReason').val() == null){
				noMandatory=false;
			}else if($j('#fkCordCbuEligible').val()=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INCOMPLETE_REASON)"/>' && $j('#fkCordCbuIncompleteReason').val() == null){	
				noMandatory=false;
			}
			var url='submitEligiblestatus?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val()+'&noMandatory='+noMandatory+'&esignFlag=reviewCRI';
			modalFormSubmitRefreshDiv('eligible1',url,'finalreviewmodel');
		}
		if('<s:property value="esignFlag"/>'=="modalFCRElig"){
			var noMandatory=$j("#eligible1").valid();
			var url;
			if(('<s:property value="fkCordCbuEligible"/>'==$j("#notCollectedToPriorReasonId").val()) && ($j('#fkCordCbuEligible').val()!='<s:property value="fkCordCbuEligible"/>')){
				url='submitEligiblestatus?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val()+'&noMandatory='+noMandatory+'&esignFlag=modalFCRElig'+'&resetCri=true';
				criResetFlag= true;
			}
			else{
				url='submitEligiblestatus?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val()+'&noMandatory='+noMandatory+'&esignFlag=modalFCRElig';
			}
			modalFormSubmitRefreshDiv('eligible1',url,'finalreviewmodel','update1','status1');
			
			
		}
		
		if('<s:property value="esignFlag"/>'=="modalFCR"){
			var noMandatory=$j("#eligible1").valid();
			if(noMandatory){
			var url='submitEligiblestatus?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val()+'&noMandatory='+noMandatory+'&esignFlag=modalFCRSubmit';
			modalFormSubmitRefreshDiv('eligible1',url,'finalreview','update1','status1');
			//loadMoredivs(url,'test,finalDeclNewQuestRO','eligible1','update1','status1');
			}
		}
		else{
				var historyArr=new Array();
				var visibleHistoryPart="";
				var notvisibleHistoryPart="";
				var i=0; 
				
			
				setTimeout(function(){
					$j('.detailhstry').each(function(){
						if($j(this).is(':visible')){
							var idval=$j(this).attr("id");
							historyArr[i]=idval;
							i++;
						}
					});
				},0);
				
				
				if($j('#cReqhistory').is(":visible")){
					visibleHistoryPart="cReqhistory";
					notvisibleHistoryPart="chistory";
				}
				if($j('#chistory').is(":visible")){
					visibleHistoryPart="chistory";
					notvisibleHistoryPart="cReqhistory";
				}
				
			
				var status = 'status';
				var update = 'update';
				if($j("#orderId").val()!=null && $j("#orderId").val()!='' && $j("#orderId").val()!='undefined' && $j("#orderType").val()!=null && $j("#orderType").val()!='' && $j("#orderType").val()!='undefined'){
					setTimeout(function(){
						if('<s:property value="#request.module"/>'=='CBU'){
							status = "status1";
							update = "update1";
							var url = 'submitEligiblestatusFromPF?esignFlag=reviewCRI&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val();
							if('<s:property value="#request.resetFun"/>'=='reset'){
								url = url+'&resetOnce=once';
							}
							loadMoredivs(url,'eligibiltyDetailsDiv,test','eligible1',update,status);
							//loadMoredivs('submitEligiblestatusFromPF?esignFlag=modal&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val(),'eligiLiceDiv','cbuCordInformationForm',update,status);
							RefreshPrevious($j('#cordId').val());
							
							$j('input[name=acceptance]').attr({'disabled': true});
							setTimeout(function(){
								if('<s:property value="#request.resetFun"/>'=='reset'){
									$j('#divRef').html($j('#eligibiltyDetailsDiv').html());	
									var cbuid = document.getElementById('cbuLicStatus');
									var criflag = $j('#divRef #criCompFlg').val();
									criflag=$j.trim(criflag);
									var crimrqflg = $j('#divRef #criMrqFlg').val();
									crimrqflg=$j.trim(crimrqflg);
									$j('#divRef').html("");
									if(cbuid!=null && criflag!=1 && crimrqflg!=1){
										alert('<s:text name="garuda.cbufinalreview.label.fcr.CRI.alert"/>');
										window.close();
									}
								}
								},0);
							setTimeout(function(){
							
							},0);
							}else{
								loadMoredivs('submitEligiblestatusFromPF?esignFlag=modal&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val(),'indshowdiv,eligiLiceDiv,reqInfoFinalReviewDiv,ctReqClinInfoContentDiv,finalreviewcontentDiv,taskCompleteFlagsDiv,chistory,cReqhistory,quicklinksDiv','eligible1',update,status);
								setpfprogressbar();
						 		getprogressbarcolor();
						 		historyTblOnLoad();
						 		loadMoredivs('getctshipdetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val(),'cbuShipmentInfoContentDiv,quicklinksDiv','cbuCordInformationForm');
								cbushipflag=1;
								//getDayfromshipdate($j("#datepicker15").val(),'cbushimentdayDiv');
								}
						
					},0);
					setTimeout(function(){
						//alert("historyArr::length"+historyArr.length)
						$j.each(historyArr,function(i,val){
							var Index=val.charAt(val.length-1);
							$j("#icondiv"+Index).triggerHandler('click');
						});
					},0);
				}else{
					
					
					
					if('<s:property value="#request.module"/>'=='CBU'){
						status = "status1";
						update = "update1";
						
						var url = 'submitEligiblestatus?esignFlag=reviewCRI&pkcordId='+$j('#cordId').val();
						if('<s:property value="#request.resetFun"/>'=='reset'){
							url = url+'&resetOnce=once';
						}
						setTimeout(function(){
						loadMoredivs(url,'eligibiltyDetailsDiv,test','eligible1',update,status);
						},0);
						setTimeout(function(){
							loadMoredivs('getCbuHistory?pkcordId=<s:property value="cdrCbuPojo.cordID" />','cReqhistory,chistory','cbuCordInformationForm');
							if(visibleHistoryPart!=""){
								$j('#'+visibleHistoryPart).show();
								$j('#'+notvisibleHistoryPart).hide();
							}
							$j('#cbuHistoryForm').removeClass('historyLoaded')
							//historyTblOnLoad();
						},0);
						//alert('Prop Val:::'+'<s:property value="#request.criReset"/>');
						$j('input[name=acceptance]').attr({'disabled': true});
						setTimeout(function(){
							RefreshPrevious('<s:property value="cdrCbuPojo.cordID"/>');
							if('<s:property value="#request.resetFun"/>'=='reset'){
								$j('#divRef').html($j('#eligibiltyDetailsDiv').html());	
								var cbuid = document.getElementById('cbuLicStatus');
								var criflag = $j('#divRef #criCompFlg').val();
								criflag=$j.trim(criflag);
								var crimrqflg = $j('#divRef #criMrqFlg').val();
								crimrqflg=$j.trim(crimrqflg);
								$j('#divRef').html("");
								if(cbuid!=null && criflag!=1 && crimrqflg!=1){
									alert('<s:text name="garuda.cbufinalreview.label.fcr.CRI.alert"/>');
									window.close();
								}
							}
							},0);
							
						}else{
						setTimeout(function(){
							modalFormSubmitRefreshDiv('eligible1','submitEligiblestatus?esignFlag=modal&pkcordId='+$j('#cordId').val(),'');
						},0);
						
						if('<s:property value="cdrCbuPojo.cordnmdpstatus"/>==<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@AC)"/> || <s:property value="cdrCbuPojo.fkCordCbuStatus"/>==<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH)"/> || <s:property value="cdrCbuPojo.fkCordCbuStatus"/>==<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)"/>'){
							if('<s:property value="isCdrUser"/>'==true){
								setTimeout(function(){
									//refreshMultipleDivsOnViewClinical('1026','<s:property value="cdrCbuPojo.cordID"/>');
									
											loadMoredivs('getCurrReqDetailsFromViewClin?orderId=<s:property value="orderPojo.pk_OrderId"/>&orderType=<s:property value="getCodeListDescById(orderPojo.orderType)"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />','currentRequestProgressMainDiv,quicklinksDiv','cbuCordInformationForm');
											currReqflag=1;
											$j("#userIdval").width("100px");
											getSearchableSelect('userIdval','id');
											setpfprogressbar();
											getprogressbarcolor();
										
								},2000);
							}
						}
						setTimeout(function(){
							loadMoredivs('getCbuHistory?pkcordId=<s:property value="cdrCbuPojo.cordID" />','cReqhistory,chistory','cbuCordInformationForm');
							if(visibleHistoryPart!=""){
								$j('#'+visibleHistoryPart).show();
								$j('#'+notvisibleHistoryPart).hide();
							}
							$j('#cbuHistoryForm').removeClass('historyLoaded')
							historyTblOnLoad();
						},0);
						setTimeout(function(){
							//alert("historyArr::length"+historyArr.length)
							$j.each(historyArr,function(i,val){
								var Index=val.charAt(val.length-1);
								$j("#icondiv"+Index).triggerHandler('click');
							});
						},0);
						
						}
				
				}
		}
		setTimeout(function(){
			if('<s:property value="#request.refreashFinalCBUScr"/>' !=null && '<s:property value="#request.refreashFinalCBUScr"/>'=='true'){
			showrightdiv1('cbuFinalEligibilityStatus?esignFlag=review&status=eligble&licenceUpdate=false&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR_VIEW_ELIGIBLE" />','eligibleDiv');
			}
			},0);
		refreshMultipleDivsOnViewClinical('1026','<s:property value="cdrCbuPojo.cordID"/>');
		getJQButton();
		setTimeout(function(){
			if(criResetFlag && ('<s:property value="#request.isTasksRequiredFlag"/>'=="true") && ('<s:property value="cdrCbuPojo.site.isReqElgQuest()"/>'=="true")){
				alert('<s:text name="garuda.cbufinalreview.label.fcr.CRI.alert"/>');
				window.close();
			}
		},0);
		}else{
			/* if('<s:property value="#request.refreashFinalCBUScr"/>' !=null && '<s:property value="#request.refreashFinalCBUScr"/>'=='true'){
				//showrightdiv1('cbuFinalEligibilityStatus?esignFlag=review&status=eligble&licenceUpdate=false&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR_VIEW_ELIGIBLE" />','eligibleDiv');
				$j("#finalDeclQstns").html($j("#finalDeclQstnsOnUpdtElgblMdl").html());
				$j("#finalDeclNewReadOnly input[type=radio]").attr('disabled','disabled');
			} 
			$j("#finalDeclQstns").html($j("#finalDeclQstnsOnUpdtElgblMdl").html());
			//$j("#finalDeclNewReadOnly input[type=radio]").attr('disabled','disabled');
			$j("#finalDeclNewReadOnly").find("input, select, textarea").attr('disabled', 'disabled');
			$j("#finalDeclReadOnly").find("input, select, textarea").attr('disabled', 'disabled');*/
			//$j('#finalDeclNewReadOnly input[type=textarea]').attr('readonly',true);
			//$j("#finalDeclReadOnly input[type=radio]").attr('disabled','disabled');
			//refreshMultipleDivsOnViewClinical('1026','<s:property value="cdrCbuPojo.cordID"/>');
			showSuccessdiv("update","status");
			showSuccessdiv("update1","status1");
			}
		}
		
	}
   }
}


// Function to remove unused action calls


function beforesubmitEligiblility(){
	var criResetFlag = false;
	if(('<s:property value="fkCordCbuEligible"/>' != $j("#notCollectedToPriorReasonId").val()) && ('<s:property value="#request.shipMentFlag"/>'=="true")){
   		if(($j("#fkCordCbuEligible").val()==$j("#notCollectedToPriorReasonId").val()) && formValueChk("finalDeclQstnsOnUpdtElgblMdl")){
   			$j('#blckaction').val("1");
   			notEligibilityApplicable($j("#fkCordCbuEligible").val());
   			
   		}
   		else{
   				$j('#blckaction').val("0");
   				
   			}
   	}
	
   if($j('#blckaction').val()!="1"){
	elgelgiChangesDone = divChanges('elgChanges',$j('#ineligibleDiv').html());
	elgelgiFcrChangesDone = divChanges('elgFcrChanges',$j('#ineligibleDiv').html());
	if($j("#eligible1").valid()){
	var orderId = $j("#orderId").val();
	var orderType = $j("#orderType").val();
	clearOrders(orderId,orderType);
	var eligTextArea = $j('#validate_count').val();
	if(eligTextArea!=null){
		eligTextArea = $j.trim(eligTextArea);
	}
	if($j('#licensureConfirmFlag').val()=="1" && eligTextArea=="" && $j('#eligibleConfirmFlag').val()=="1" && $j('#finalReviewConfirmStatus').val()=="Y"){
		 document.getElementById("errormsg").innerHTML="<s:text name="garuda.update.status.resonToChange"/>";
	}
	else{
		if(elgelgiChangesDone || elgelgiFcrChangesDone){
		
		if('<s:property value="esignFlag"/>'=="reviewCRIview"){
			var noMandatory=true;
			if($j('#fkCordCbuEligible').val()==""){
				noMandatory=false;
			}else if($j('#fkCordCbuEligible').val()=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON)"/>' && $j('#fkCordCbuEligibleReason').val() == null){
				noMandatory=false;
			}else if($j('#fkCordCbuEligible').val()=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INCOMPLETE_REASON)"/>' && $j('#fkCordCbuIncompleteReason').val() == null){	
				noMandatory=false;
			}
			var url='submitEligiblestatus?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val()+'&noMandatory='+noMandatory+'&esignFlag=reviewCRI';
			modalFormSubmitRefreshDiv('eligible1',url,'finalreviewmodel');
		}
		if('<s:property value="esignFlag"/>'=="modalFCRElig"){
			var noMandatory=$j("#eligible1").valid();
			var url;
			if(('<s:property value="fkCordCbuEligible"/>'==$j("#notCollectedToPriorReasonId").val()) && ($j('#fkCordCbuEligible').val()!='<s:property value="fkCordCbuEligible"/>')){
				url='submitEligiblestatus?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val()+'&noMandatory='+noMandatory+'&esignFlag=modalFCRElig'+'&resetCri=true';
				criResetFlag= true;
			}
			else{
				url='submitEligiblestatus?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val()+'&noMandatory='+noMandatory+'&esignFlag=modalFCRElig';
			}
			modalFormSubmitRefreshDiv('eligible1',url,'finalreviewmodel','update1','status1');
			
			
		}
		
		if('<s:property value="esignFlag"/>'=="modalFCR"){
			var noMandatory=$j("#eligible1").valid();
			if(noMandatory){
			var url='submitEligiblestatus?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val()+'&noMandatory='+noMandatory+'&esignFlag=modalFCRSubmit';
			modalFormSubmitRefreshDiv('eligible1',url,'finalreview','update1','status1');
			//loadMoredivs(url,'test,finalDeclNewQuestRO','eligible1','update1','status1');
			}
		}
		else{
				var historyArr=new Array();
				var visibleHistoryPart="";
				var notvisibleHistoryPart="";
				var i=0; 
				
			
				setTimeout(function(){
					$j('.detailhstry').each(function(){
						if($j(this).is(':visible')){
							var idval=$j(this).attr("id");
							historyArr[i]=idval;
							i++;
						}
					});
				},0);
				
				
				if($j('#cReqhistory').is(":visible")){
					visibleHistoryPart="cReqhistory";
					notvisibleHistoryPart="chistory";
				}
				if($j('#chistory').is(":visible")){
					visibleHistoryPart="chistory";
					notvisibleHistoryPart="cReqhistory";
				}
				
			
				var status = 'status';
				var update = 'update';
				if($j("#orderId").val()!=null && $j("#orderId").val()!='' && $j("#orderId").val()!='undefined' && $j("#orderType").val()!=null && $j("#orderType").val()!='' && $j("#orderType").val()!='undefined'){
					setTimeout(function(){
						if('<s:property value="#request.module"/>'=='CBU'){
							status = "status1";
							update = "update1";
							var url = 'submitEligiblestatusFromPF?esignFlag=reviewCRI&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val();
							if('<s:property value="#request.resetFun"/>'=='reset'){
								url = url+'&resetOnce=once';
							}
							loadMoredivs(url,'eligibiltyDetailsDiv,test','eligible1',update,status);
							//loadMoredivs('submitEligiblestatusFromPF?esignFlag=modal&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val(),'eligiLiceDiv','cbuCordInformationForm',update,status);
							RefreshPrevious($j('#cordId').val());
							
							$j('input[name=acceptance]').attr({'disabled': true});
							setTimeout(function(){
								if('<s:property value="#request.resetFun"/>'=='reset'){
									$j('#divRef').html($j('#eligibiltyDetailsDiv').html());	
									var cbuid = document.getElementById('cbuLicStatus');
									var criflag = $j('#divRef #criCompFlg').val();
									criflag=$j.trim(criflag);
									var crimrqflg = $j('#divRef #criMrqFlg').val();
									crimrqflg=$j.trim(crimrqflg);
									$j('#divRef').html("");
									if(cbuid!=null && criflag!=1 && crimrqflg!=1){
										alert('<s:text name="garuda.cbufinalreview.label.fcr.CRI.alert"/>');
										window.close();
									}
								}
								},0);
							setTimeout(function(){
							
							},0);
							}else{
								loadMoredivs('submitEligiblestatusFromPF?esignFlag=modal&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val(),'indshowdiv,eligiLiceDiv,reqInfoFinalReviewDiv,ctReqClinInfoContentDiv,finalreviewcontentDiv,taskCompleteFlagsDiv,chistory,cReqhistory,quicklinksDiv','eligible1',update,status);
								setpfprogressbar();
						 		getprogressbarcolor();
						 		historyTblOnLoad();
						 		loadMoredivs('getctshipdetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val(),'cbuShipmentInfoContentDiv,quicklinksDiv','cbuCordInformationForm');
								cbushipflag=1;
								//getDayfromshipdate($j("#datepicker15").val(),'cbushimentdayDiv');
								}
						
					},0);
					setTimeout(function(){
						//alert("historyArr::length"+historyArr.length)
						$j.each(historyArr,function(i,val){
							var Index=val.charAt(val.length-1);
							$j("#icondiv"+Index).triggerHandler('click');
						});
					},0);
				}else{
					
					
					
					if('<s:property value="#request.module"/>'=='CBU'){
						status = "status1";
						update = "update1";
						
						var url = 'submitEligiblestatus?esignFlag=reviewCRI&pkcordId='+$j('#cordId').val();
						if('<s:property value="#request.resetFun"/>'=='reset'){
							url = url+'&resetOnce=once';
						}
						setTimeout(function(){
						loadMoredivs(url,'eligibiltyDetailsDiv,test','eligible1',update,status);
						},0);
						setTimeout(function(){
							loadMoredivs('getCbuHistory?pkcordId=<s:property value="cdrCbuPojo.cordID" />','cReqhistory,chistory','cbuCordInformationForm');
							if(visibleHistoryPart!=""){
								$j('#'+visibleHistoryPart).show();
								$j('#'+notvisibleHistoryPart).hide();
							}
							$j('#cbuHistoryForm').removeClass('historyLoaded')
							//historyTblOnLoad();
						},0);
						//alert('Prop Val:::'+'<s:property value="#request.criReset"/>');
						$j('input[name=acceptance]').attr({'disabled': true});
						setTimeout(function(){
							RefreshPrevious('<s:property value="cdrCbuPojo.cordID"/>');
							if('<s:property value="#request.resetFun"/>'=='reset'){
								$j('#divRef').html($j('#eligibiltyDetailsDiv').html());	
								var cbuid = document.getElementById('cbuLicStatus');
								var criflag = $j('#divRef #criCompFlg').val();
								criflag=$j.trim(criflag);
								var crimrqflg = $j('#divRef #criMrqFlg').val();
								crimrqflg=$j.trim(crimrqflg);
								$j('#divRef').html("");
								if(cbuid!=null && criflag!=1 && crimrqflg!=1){
									alert('<s:text name="garuda.cbufinalreview.label.fcr.CRI.alert"/>');
									window.close();
								}
							}
							},0);
							
						}else{
						setTimeout(function(){
							modalFormSubmitRefreshDiv('eligible1','submitEligiblestatus?esignFlag=modal&pkcordId='+$j('#cordId').val(),'');
						},0);
						
						if('<s:property value="cdrCbuPojo.cordnmdpstatus"/>==<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@AC)"/> || <s:property value="cdrCbuPojo.fkCordCbuStatus"/>==<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH)"/> || <s:property value="cdrCbuPojo.fkCordCbuStatus"/>==<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)"/>'){
							if('<s:property value="isCdrUser"/>'==true){
								setTimeout(function(){
									//refreshMultipleDivsOnViewClinical('1026','<s:property value="cdrCbuPojo.cordID"/>');
									
											loadMoredivs('getCurrReqDetailsFromViewClin?orderId=<s:property value="orderPojo.pk_OrderId"/>&orderType=<s:property value="getCodeListDescById(orderPojo.orderType)"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />','currentRequestProgressMainDiv,quicklinksDiv','cbuCordInformationForm');
											currReqflag=1;
											$j("#userIdval").width("100px");
											getSearchableSelect('userIdval','id');
											setpfprogressbar();
											getprogressbarcolor();
										
								},2000);
							}
						}
						setTimeout(function(){
							//loadMoredivs('getCbuHistory?pkcordId=<s:property value="cdrCbuPojo.cordID" />','cReqhistory,chistory','cbuCordInformationForm');
							if(visibleHistoryPart!=""){
								$j('#'+visibleHistoryPart).show();
								$j('#'+notvisibleHistoryPart).hide();
							}
							$j('#cbuHistoryForm').removeClass('historyLoaded')
							//historyTblOnLoad();
						},0);
						setTimeout(function(){
							//alert("historyArr::length"+historyArr.length)
							$j.each(historyArr,function(i,val){
								var Index=val.charAt(val.length-1);
								$j("#icondiv"+Index).triggerHandler('click');
							});
						},0);
						
						}
				
				}
		}
		setTimeout(function(){
			if('<s:property value="#request.refreashFinalCBUScr"/>' !=null && '<s:property value="#request.refreashFinalCBUScr"/>'=='true'){
			showrightdiv1('cbuFinalEligibilityStatus?esignFlag=review&status=eligble&licenceUpdate=false&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR_VIEW_ELIGIBLE" />','eligibleDiv');
			}
			},0);
		refreshMultipleDivsOnViewClinical('1026','<s:property value="cdrCbuPojo.cordID"/>');
		getJQButton();
		setTimeout(function(){
			if(criResetFlag && ('<s:property value="#request.isTasksRequiredFlag"/>'=="true") && ('<s:property value="cdrCbuPojo.site.isReqElgQuest()"/>'=="true")){
				alert('<s:text name="garuda.cbufinalreview.label.fcr.CRI.alert"/>');
				window.close();
			}
		},0);
		}else{
			/* if('<s:property value="#request.refreashFinalCBUScr"/>' !=null && '<s:property value="#request.refreashFinalCBUScr"/>'=='true'){
				//showrightdiv1('cbuFinalEligibilityStatus?esignFlag=review&status=eligble&licenceUpdate=false&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR_VIEW_ELIGIBLE" />','eligibleDiv');
				$j("#finalDeclQstns").html($j("#finalDeclQstnsOnUpdtElgblMdl").html());
				$j("#finalDeclNewReadOnly input[type=radio]").attr('disabled','disabled');
			} 
			$j("#finalDeclQstns").html($j("#finalDeclQstnsOnUpdtElgblMdl").html());
			//$j("#finalDeclNewReadOnly input[type=radio]").attr('disabled','disabled');
			$j("#finalDeclNewReadOnly").find("input, select, textarea").attr('disabled', 'disabled');
			$j("#finalDeclReadOnly").find("input, select, textarea").attr('disabled', 'disabled');*/
			//$j('#finalDeclNewReadOnly input[type=textarea]').attr('readonly',true);
			//$j("#finalDeclReadOnly input[type=radio]").attr('disabled','disabled');
			//refreshMultipleDivsOnViewClinical('1026','<s:property value="cdrCbuPojo.cordID"/>');
			showSuccessdiv("update","status");
			showSuccessdiv("update1","status1");
			}
		}
		
	}
   }
}





function clearOrders(orderId,orderType){
	if(typeof(orderId)=='undefined'){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")!=-1){
		$j("#orderId").val("");
		orderId="";
	}
	
	if(typeof(orderType)=='undefined'){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")!=-1){
		$j("#orderType").val("");
		orderType="";
	}
}

function reviewEdit(){
	elgelgiChangesDone = divChanges('elgChanges',$j('#ineligibleDiv').html());
	elgelgiFcrChangesDone = divChanges('elgFcrChanges',$j('#ineligibleDiv').html());
	if($j("#eligible1").valid()){
		if(elgelgiChangesDone || elgelgiFcrChangesDone){
		setTimeout(function(){
		//modalFormSubmitRefreshDiv('eligible1','submitFinalEligibleStatus','main');
			loadMoredivs('submitFinalEligibleStatus?esignFlag=modal&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val(),'eligibiltyDetailsDiv,test,flaggedItems','eligible1','update1','status1');
			},0);
		}else{
			showSuccessdiv("update","status");
			showSuccessdiv("update1","status1");
			}
	}
}

function closeModalUpdateEligibility(){
	/*if('<s:property value="#request.refreashFinalCBUScr"/>' !=null && '<s:property value="#request.refreashFinalCBUScr"/>'=='true'){
		showrightdiv1('cbuFinalEligibilityStatus?esignFlag=review&status=eligble&licenceUpdate=false&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR_VIEW_ELIGIBLE" />','eligibleDiv');
	}*/ 
	//refreshMultipleDivsOnViewClinical('1026','<s:property value="cdrCbuPojo.cordID"/>');
	 //$j("#finalDeclQstns").html($j("#finalDeclQstnsOnUpdtElgblMdl").html());
	// $j("#finalDeclNewReadOnly").find("input, select, textarea").attr('disabled', 'disabled');
	 //$j("#finalDeclReadOnly").find("input, select, textarea").attr('disabled', 'disabled');
	 /* $j("#finalDeclNewReadOnly input[type=radio]").attr('disabled','disabled');
	 $j("#finalDeclReadOnly input[type=radio]").attr('disabled','disabled'); */
	 $j("#modelPopup1").dialog("close");
	 $j("#modelPopup").dialog("close");
	 $j("#modelPopup1").dialog("destroy");
	 $j("#modelPopup").dialog("destroy");
}
</script>
<div id="dialog-resetElig-1" style="display: none;">
<table><tr><td><img src="images/warning.png"/></td><td><s:text name="garuda.update.license.resetConfirm"/></td></tr></table>
</div>
<div id="divRef" style="display: none;"></div>
<div id="ineligibleDiv" >
<s:form id="eligible1">
<s:hidden name="cdrCbuPojo.inEligiblePkid" id="inEligiblePk"></s:hidden>
<s:hidden name="cdrCbuPojo.incompleteReasonId" id="incompleteReasonId" ></s:hidden>
<s:hidden name="cdrCbuPojo.notCollectedToPriorReasonId" id="notCollectedToPriorReasonId" ></s:hidden>
<s:hidden name="licenceUpdate" value="True"></s:hidden>
<s:hidden name="cdrCbuPojo.cordID" id="cordId"/>
<s:hidden name="cdrCbuPojo.fkSpecimenId" />
<s:hidden name="cdrCbuPojo.specimen.pkSpecimen" />
<s:hidden name="cbuFinalReviewPojo.licensureConfirmFlag" id="licensureConfirmFlag" />
<s:hidden name="cbuFinalReviewPojo.eligibleConfirmFlag" id="eligibleConfirmFlag" />
<s:hidden name="cbuFinalReviewPojo.finalReviewConfirmStatus" id="finalReviewConfirmStatus" />
<s:hidden name="NAEligibleReasonFlag" id ="NAEligibleReasonFlag" value="false"/>
<s:hidden name="orderId" id="orderId" />
<s:hidden name="orderType" id="orderType"></s:hidden>
<s:hidden name="blckaction" id="blckaction" value="0"></s:hidden>
<div id="licDyn"></div>
	<div <s:if test="#request.module=='CBU'">id="update1"</s:if><s:else>id="update"</s:else> style="display: none;" >
	  <jsp:include page="../cb_update.jsp">
	    <jsp:param value="garuda.message.modal.eligibility_deter" name="message"/>
	  </jsp:include>	   
	</div>	
	 
	<table width="100%" <s:if test="#request.module=='CBU'">id="status1"</s:if><s:else>id="status"</s:else>>
	<s:hidden name="criResetFlg" id="criResetFlg"/>
	<!-- <tr><td colspan="2">
					<div id="eligiblehistory" >
						<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
						<s:text name="garuda.cdrcbuview.label.eligibility_edit_histry" /></div>
						<div class="portlet-content">
							<div id="searchTble">
								<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="elistshistry" >
									<thead>
										<tr>
											<th><s:text name="garuda.common.lable.date" /></th>
											<th><s:text name="garuda.cdrcbuview.label.user_name" /></th>
											<th><s:text name="garuda.cdrcbuview.label.eligible_determin" /></th>
											<th><s:text name="garuda.cdrcbuview.label.eligible_reason" /></th>
										</tr>
									</thead>
									<tbody>
										<s:iterator value="lstEligibility" var="rowVal" status="row" >
										<tr>
											<s:iterator value="rowVal" var="cellValue" status="col"  >
												
												
												<s:if test="%{#col.index == 0}">
													<s:date name="%{#cellValue}"
													id="statusDate" format="MMM dd, yyyy" />
													<td><s:property value="%{statusDate}"/></td>
												</s:if>
												<s:if test="%{#col.index == 1}">
												 <s:set name="cellValue1" value="cellValue" scope="request"/>
													<td>										
													</td>
												</s:if>
												<s:if test="%{#col.index == 2}">																							
												<td style="valign : top">
												  <s:property value="getCodeListDescById(#cellValue)" />
												</td>
												</s:if>
												<s:if test="%{#col.index == 3}">
													<td><s:iterator value="getMultipleIneligibleReasons(#cellValue)" var="colVal" status="val"><s:property value="getCodeListDescById(#colVal)" /><br></s:iterator></td>
												</s:if>
											</s:iterator>
										</tr>
										</s:iterator>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="3"></td>
										</tr>
									</tfoot>
								</table> 								
							</div>
							</div>
								<s:if test="#request.module=='CBU'">	
								<s:set name="cordId" value="cdrCbuPojo.cordID" />								
									<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
									<s:text name="garuda.cdrcbuview.label.summary_identified_factor" /></div>
									<div class="portlet-content">
										<div id="searchTble">
										   <table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="elistshistry" >
												<thead>
													<tr>
													    <th><s:text name="garuda.cdrcbuview.label.type" /></th>
														<th><s:text name="garuda.cdrcbuview.label.summary" /></th>
														<th><s:text name="garuda.cdrcbuview.label.eligible_determin" /></th>
													</tr>
												</thead>
												<tbody>
												     <s:iterator value="#request.assessment">
													     <tr>
													        <td><s:property value="fkNotesType" /></td>
													        <td><s:property value="notes" /></td>
													        <td><s:property value="getCodeListDescByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_ASSESS,@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON)"/></td>
													     </tr>
												     </s:iterator>
												</tbody>
												<tfoot>
												<tr>
													<td colspan="3"></td>
												</tr>
											</tfoot>
											</table>									
										</div></div>
								</s:if>															
							</div>
	    </td>
	</tr>-->
	<tr>
		<td colspan="2">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<s:if test='cbuFinalReviewPojo.licensureConfirmFlag == 1 && cbuFinalReviewPojo.eligibleConfirmFlag == 1 && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y" '>
			    <tr>
					<td width="35%">
							<s:text name="garuda.common.message.reasonForChange" />:<span style="color: red;">*</span></td>
						<td width="65%">
							<textarea name="eliChangeReason" cols="73" rows="5" style="width:390px;" id="validate_count" onkeyup="enableFields(this.value); limitText(this,'200');" onchange="isChangeDone(this,'<s:property value="%{eliChangeReason}" escapeJavaScript="true"/>','elgChanges');" ><s:property value="%{eliChangeReason}"/></textarea>
							<table cellpadding="0" cellspacing="0" align="left">
							 <tr>
							 <td >
							  <label id="errormsg" style="color:#FF0000;font-size:12px;"></label>
							 </td>
							 </tr>
							</table>
						</td>
				</tr>
			</s:if>
			</table>
			</td>
			</tr>
			
	<tr><td>
	
	<table id="finalDeclQstnsOnUpdtElgblMdl">
	<s:if test='#request.isTasksRequiredFlag == false '>
	 <tr>
        <td colspan="2">
			<strong><s:text name="garuda.common.label.summarize"/></strong>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <s:hidden name="declPojo.pkEligDecl"></s:hidden>
            <jsp:include page="../cord-entry/cb_final_decl_quest.jsp">
             <jsp:param value="false" name="readonly"/>
            </jsp:include>
        </td>
    </tr>
    </s:if>
    <s:elseif test='#request.isTasksRequiredFlag == true '>
    
     <tr>
        <td colspan="2">
			<strong><s:text id="summTxt1" name="garuda.common.label.summarize"/></strong>
        </td>
    </tr>
    <tr>
        <td colspan="2">
        <div id="newQnaireModal">
            <s:hidden name="declPojo.pkEligDecl"></s:hidden>
            <jsp:include page="../cb_new_final_decl_quest.jsp">
            <jsp:param value="false" name="readonly"/>
            </jsp:include>
            </div>
        </td>
    </tr>
    
    </s:elseif>
    </table></td></tr>
    <s:if test="esignFlag!='modalFCR'">
	<tr>
		<td colspan="2">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			 <tr>
  				<td width="35%" style="align:left"><s:text name="garuda.cdrcbuview.label.eligible_status" />:<span style="color: red;">*</span></td>
				<td width="65%" style="align:left"><s:select name="fkCordCbuEligible" cssStyle="width:390px;" id="fkCordCbuEligible" list="%{getListOfCodeLstObj(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS,cdrCbuPojo.cordID)}"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select" onchange="inEligibleinCompleteReason(this.value);isChangeDone(this,'%{fkCordCbuEligible}','elgChanges');" /></td>
			 </tr>
		</table>
		</td>
	</tr>
	<tr>
	<td>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr id="inEliReason"><input type="hidden" id="inElgDbReasons" value='<s:if test="inEligibleReasons!=null"><s:property value="%{inEligibleReasons[0]}"/><s:iterator status="row1" begin="1" end="%{inEligibleReasons.length-1}">,<s:property value="%{inEligibleReasons[#row1.index+1]}"/></s:iterator></s:if>' />
			<td width="35%" style="vertical-align: centre;align:left;"><s:text name="garuda.cdrcbuview.label.ineligible_reason" />:<span style="color: red;">*</span></td>
			<td width="65%" style="align:left">
			<s:select cssStyle="height:100px;width:390px;" name="inEligibleReasons" id="fkCordCbuEligibleReason" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON]"
							listKey="pkCodeId" listValue="description"  value="inEligibleReasons" onchange="getDbinElgReasons(this,'elgChanges');"
							  multiple="true"/>
			</td>
		</tr>
		<tr id="inComReason"><input type="hidden" id="inCompDbReasons" value='<s:if test="inCompReasons!=null"><s:property value="%{inCompReasons[0]}"/><s:iterator status="row2" begin="1" end="%{inCompReasons.length-1}">,<s:property value="%{inCompReasons[#row2.index+1]}"/></s:iterator></s:if>' />
			<td width="35%" style="vertical-align: centre;align:left;"><s:text name="garuda.cdrcbuview.label.incomplete_reason" />:<span style="color: red;">*</span></td>
			<td width="65%" style="align:left">
			<s:select cssStyle="height:100px;width:390px;" name="inCompReasons" id="fkCordCbuIncompleteReason" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@INCOMPLETE_REASON]"
							listKey="pkCodeId" listValue="description"  value="inCompReasons" onchange="getDbinCompReasons(this,'elgChanges');"
							  multiple="true"/>
			</td>
		</tr>
		<tr id="additonalinfo_1">
			<td width="35%" style="vertical-align: centre;align:left;"><s:text name="garuda.cdrcbuview.label.ineligible_reason_notes" />:<span style="color: red;">*</span></td>
			<td width="65%" style="align:left">
			<textarea name="cordAdditionalInfo" maxlength="300" cols="73" rows="5" id="word_count" cssStyle="width:390px;" onchange="isChangeDone(this,'<s:property value="%{cordAdditionalInfo}" escapeJavaScript="true"/>','elgChanges');"><s:property value="%{cordAdditionalInfo}"/></textarea> 
			<br/>
			<span id="counter"></span>
			</td>
		</tr>
	</table>
	</td>
	</tr>
	</s:if>	
	<!-- <tr>			
   	<td colspan="2">
   		<div id="inEligibleReason" <s:if test="#request.unlicensed!=true">style="display: none;"</s:if>>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
	  		 <tr id="ineligiblereason">
				<td width="35%" style="vertical-align: centre;align:left;">
				 <div id="inelreason" style="display: none"><s:text name="garuda.cdrcbuview.label.ineligible_reason" />:<span style="color: red;">*</span></div>
				 <div id="increason" style="display: none"><s:text name="garuda.cdrcbuview.label.incomplete_reason" />:<span style="color: red;">*</span></div>
				 <div id="priorreason" style="display: none"><s:text name="garuda.cdrcbuview.label.prior_reason" />:<span style="color: red;">*</span></div></td>
				<td width="65%" style="align:left"><s:select cssStyle="height:100px;width:390px;" name="reasons" id="fkCordCbuEligibleReason" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON]"
					listKey="pkCodeId" listValue="description"  value="reasons"
					  multiple="true"/></td>
	  		</tr>
	 		<tr id="additonalinfo">
			<td width="35%" style="vertical-align: middle; align:left;" ><s:text name="garuda.cdrcbuview.label.ineligible_reason_notes" />:<span style="color: red;">*</span></td>
			<td width="65%" style="align:left"><s:textarea name="cordAdditionalInfo" maxlength="300" cols="73" rows="5" id="word_count" cssStyle="width:390px;"  /> 
			<br/>
			<span id="counter"></span>
		  </td>
	   </tr>			   
	</table>
	</div>
   </td> 
   </tr>-->
   <tr><td colspan="2">
   <table bgcolor="#cccccc" width="100%">
   <s:if test="esignFlag == 'modal' || esignFlag == 'modalFCRElig'">
   <tr bgcolor="#cccccc" valign="baseline">
   <td width="70%"><jsp:include page="../cb_esignature.jsp" flush="true">
                        <jsp:param value="eleReviewsubmit" name="submitId"/>
						<jsp:param value="eleReviewinvalid" name="invalid" />
						<jsp:param value="eleReviewminimum" name="minimum" />
						<jsp:param value="eleReviewpass" name="pass" />
                   </jsp:include>
   </td>
   <td align="right" width="15%" valign="top">
   <s:if test="#request.module=='CBU' && #request.resetAll=='resetOnce'">
   <input type="button" disabled="disabled" id="eleReviewsubmit" onclick="reviewEdit();" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
   </s:if>
   <s:elseif test="#request.module=='CBU' && #request.resetAll=='resetAll'">
   <input type="button" disabled="disabled" id="eleReviewsubmit" onclick="beforesubmitEligible();" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
   </s:elseif>
   <s:else>
   <input type="button" disabled="disabled" id="eleReviewsubmit" onclick="beforesubmitEligiblility();" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
   </s:else>
   </td>
   <td width="15%" valign="top"><input type="button"  onclick="closeModalUpdateEligibility()" value="<s:text name="garuda.unitreport.label.button.cancel"/>" /></td>
   </tr>
   </s:if>
    <s:if test="esignFlag == 'reviewCRIview' || esignFlag == 'modalFCR'">
   <tr bgcolor="#cccccc" valign="baseline">
   <td width="70%"><jsp:include page="../cb_esignature.jsp" flush="true">
                        <jsp:param value="eleReviewsubmit" name="submitId"/>
						<jsp:param value="eleReviewinvalid" name="invalid" />
						<jsp:param value="eleReviewminimum" name="minimum" />
						<jsp:param value="eleReviewpass" name="pass" />
                   </jsp:include>
   </td>
   <td align="right" width="15%" valign="top">
   <input type="button" disabled="disabled" id="eleReviewsubmit" onclick="beforesubmitEligible();" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
   </td>
   <td width="15%" valign="top"><input type="button"  onclick="closeModalUpdateEligibility()" value="<s:text name="garuda.unitreport.label.button.cancel"/>" /></td>
   </tr>
   </s:if>
   </table> 
   </td>
   </tr>
   </table>
   <div id="dialog-confirm" style="display: none;">
	<s:text name="garuda.common.validation.notapplicableelig"/> 
</div>
</s:form>
<script type="text/javascript">
$j(function(){
	$j("#esign").bind("keydown", function(event) {
		// track enter key
	      var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
	      if (keycode == 13) { // keycode for enter key
	          // force the 'Enter Key' to implicitly click the Update button
	          document.getElementById('eleReviewsubmit').click();
	          return false;
	       } else  {
	          return true;
	       }
	    }); // end of function	
});
function inEligibleinCompleteReason(val){

	if(val=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON)"/>'){
		$j('#inEliReason').show();
		$j('#inComReason').hide();
		$j('#additonalinfo_1').hide();
		$j("#word_count").val("");
		if($j("#newQnaireModal").length>0){
			$j("#newQnaireModal").show();
			$j("#summTxt1").show();
		}
		}
	else if(val=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INCOMPLETE_REASON)"/>'){
		$j('#inEliReason').hide();
		$j('#inComReason').show();
		$j('#additonalinfo_1').hide();
		$j("#word_count").val("");
		if($j("#newQnaireModal").length>0){
			$j("#newQnaireModal").show();
			$j("#summTxt1").show();
		}
		
		}
	else if(val=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBLE)"/>'){
		$j('#inEliReason').hide();
		$j('#inComReason').hide();
		$j('#additonalinfo_1').hide();
		$j("#word_count").val("");
		if($j("#newQnaireModal").length>0){
			$j("#newQnaireModal").show();
			$j("#summTxt1").show();
		}
		}
	else if(val==$j("#notCollectedToPriorReasonId").val()){
		$j('#inEliReason').hide();
		$j('#inComReason').hide();
		$j('#additonalinfo_1').show();
		if($j("#newQnaireModal").length>0){
			$j("#newQnaireModal").hide();
			$j("#summTxt1").hide();
		}
		}
	else{
		$j('#inEliReason').hide();
		$j('#inComReason').hide();
		$j("#word_count").val("");
		$j('#additonalinfo_1').hide();
		if($j("#newQnaireModal").length>0){
			$j("#newQnaireModal").show();
			$j("#summTxt1").show();
		}
		}
}
$j(function(){
	inEligibleinCompleteReason($j("#fkCordCbuEligible").val());
});
function getDbinElgReasons(thisVal,classname){
	isChangeDone(thisVal,$j('#inElgDbReasons').val(),classname);
}
function getDbinCompReasons(thisVal,classname){
	isChangeDone(thisVal,$j('#inCompDbReasons').val(),classname);
}
</script>
</div>
