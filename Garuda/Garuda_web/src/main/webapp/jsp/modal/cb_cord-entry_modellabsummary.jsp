<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<jsp:include page="../cb_track_session_logging.jsp" />
<%
String contextpath=request.getContextPath();
%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<SCRIPT language="javascript">
var textareaValue = "";
var autoDefer = false;
var autoFilled= false;
var autoFillee= false;

function autoDeferModalFormSubmitRefreshDiv(formid,url,divname,updateDivId,statusDivId){
	
	//if($j("#"+formid).valid()){	
		showprogressMgs();
			$j.ajax({
		        type: "POST",
		        url: url,
		        async:false,
		        data : $j("#"+formid).serialize(),
		        success: function (result){
		        	$j('.ui-datepicker').html("");		        
		            var $response=$j(result);		           
		            var errorpage = $response.find('#errorForm').html();		           
		            if(errorpage != null &&  errorpage != "" ){
		            	$j('#main').html(result);
		            }else{
		            	if(divname!=""){ 
			            	 $j("#"+divname).html(result);
			            }
		            	if(updateDivId!=undefined && updateDivId!=""){
		            		$j("#"+updateDivId).css('display','block');			            	
		            	}else{
		            		$j("#update").css('display','block');			            	
		            	}
		            	if(statusDivId!=undefined && statusDivId!=""){
		            		$j("#"+statusDivId).css('display','none');			            	
		            	}else{
		            		$j("#status").css('display','none');			            	
		            	}		            	
		            }
		        },
		        error: function (request, status, error) {
		        	closeprogressMsg();
		        	alert("Error " + error);
		            alert(request.responseText);
		        }

			});	
			closeprogressMsg();
    //}	
}

function addTestDate(value){
	//alert($j("#datepicker4").val());
	if($j("#preTestDateStrnM").val()=="" || autoFilled==true){
		autoFilled= true;
		$j("#preTestDateStrnM").val(value);
	  }
	if($j("#preTestDateStrnM").val()!=""){
		$j(".placeHolderOfid_preTestDateStrnM").hide();
	}else{
		$j(".placeHolderOfid_preTestDateStrnM").show();
	}
	 }
	function addTestDateScnd(value){
	if($j("#postTestDateStrnM").val()=="" || autoFillee==true){
		autoFillee= true;
		$j("#postTestDateStrnM").val(value);
	}
	if($j("#postTestDateStrnM").val()!=""){
		$j(".placeHolderOfid_postTestDateStrnM").hide();
	}else{
		$j(".placeHolderOfid_postTestDateStrnM").show();
	}
	}

function loadDivWithFormSubmitDefer(formid,url,divname,updateDivId,statusDivId){
	
	/*var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	var processingSaveDialog = document.createElement('div');
	processingSaveDialog.innerHTML=progressMsg;		
	$j(processingSaveDialog).dialog({autoOpen: false,
		resizable: false, width:400, height:90,closeOnEscape: false,
		modal: true}).siblings('.ui-dialog-titlebar').remove();		
	$j(processingSaveDialog).dialog("open");		
	*/
	showprogressMgs();
		$j.ajax({
	        type: "POST",
	        url: url,
	       // async:false,
	        data : $j("#"+formid).serialize(),
	        success: function (result){
	        	$j('.ui-datepicker').html("");
	        	//$('.tabledisplay').html("");
	            var $response=$j(result);
	            //query the jq object for the values
	            var errorpage = $response.find('#errorForm').html();
	           // alert(oneval);
	           // var subval = $response.find('#sub').text();
	            if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{
	            	if(divname!=""){ 
		            	 $j("#"+divname).html(result);
		            }
	            	if(updateDivId!=undefined && updateDivId!=""){
	            		$j("#"+updateDivId).css('display','block');			            	
	            	}else{
	            		$j("#update").css('display','block');			            	
	            	}
	            	if(statusDivId!=undefined && statusDivId!=""){
	            		$j("#"+statusDivId).css('display','none');			            	
	            	}else{
	            		$j("#status").css('display','none');			            	
	            	}		            	
	            }
	        },
	        error: function (request, status, error) {
	        	closeprogressMsg();
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});	
		closeprogressMsg();


}
function autoDeferHEMO(Val,Id){
	elementValue = autoDeferField_3;
	
	var cordId =<s:property value ="cdrCbuPojo.cordID"/>;
	var url="saveLabSummary?cordId="+cordId+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val();	
    if(Val==$j("#hemoBeatThal").val() || Val==$j("#hemoSickBeta").val() || Val==$j("#hemoSickCell").val() || Val==$j("#hemoAlphaSev").val()){
      jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
      function(r) {
			if (r == false) {			   
			   $j("#"+Id).val("-1");
			   $j("#"+Id+" option[value="+elementValue+"]").attr("selected", "selected");
			   parent.document.getElementById(Id).focus();	
			   $j('#'+Id).removeClass('labChanges');			   
			   if(elementValue==$j("#alphaThalismiaTraitPkVal").val() || elementValue==$j("#hemozygousNoDiesasePkVal").val() || elementValue==$j("#hemoAlphaThalassmiaPkVal").val() || elementValue==$j("#hemoTraitPkVal").val() || elementValue==$j("#hemozygousNoDonePkVal").val() || elementValue==$j("#hemozygousUnknownPkVal").val() || elementValue==$j("#hemoMultiTraitPkVal").val()){
				   $j("#showassessbutton").show();
				   $j("#hideassesmentHemo").hide();
			   }
			   $j("[name='submitButtonAutoDefer']:last").val("false"); 
			  }else if(r == true){
				  $j("[name='submitButtonAutoDefer']:last").val("true");
				  autoDefer = true;
				  mutex_Lock = true;
				  element_Id = Id;
				  element_Name='cdrCbuPojo.hemoglobinScrn';
				  autoDeferFormSubmitParam[0]= 'formprocandcount';
				  autoDeferFormSubmitParam[1]= url;
				  autoDeferFormSubmitParam[2]='';
				  autoDeferFormSubmitParam[3]= 'update1';
				  autoDeferFormSubmitParam[4]= 'status2';
				  refresh_DivNo = 1536;
				  refresh_CordID = '<s:property value="cdrCbuPojo.cordID"/>';
				  refreshDiv_Id[0]='defer';
				  refreshDiv_Id[1]=	'status3'
				  commonMethodForSaveAutoDefer();
				  if(elementValue==$j("#alphaThalismiaTraitPkVal").val() || elementValue==$j("#hemozygousNoDiesasePkVal").val() || elementValue==$j("#hemoAlphaThalassmiaPkVal").val() || elementValue==$j("#hemoTraitPkVal").val() || elementValue==$j("#hemozygousNoDonePkVal").val() || elementValue==$j("#hemozygousUnknownPkVal").val() || elementValue==$j("#hemoMultiTraitPkVal").val()){
					  assessLinknFlag_Id[0]= 'true';
					  assessLinknFlag_Id[1]= 'LAB_SUM_Hemo_showassess';
					  assessLinknFlag_Id[2]= 'LAB_SUM_Hemo_hideassess';
				   }
			  }
			});
	   }
}

function autoDeferAssessmentHEMO(Val,Id){
	elementValue = autoDeferField_3;
	
	var cordId =<s:property value ="cdrCbuPojo.cordID"/>;
	var url="saveLabSummary?cordId="+cordId+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val();	
    if(Val==$j("#alphaThalismiaTraitPkVal").val() || Val==$j("#hemozygousNoDiesasePkVal").val() || Val==$j("#hemoAlphaThalassmiaPkVal").val() || Val==$j("#hemoTraitPkVal").val() || Val==$j("#hemozygousNoDonePkVal").val() || Val==$j("#hemozygousUnknownPkVal").val() || Val==$j("#hemoMultiTraitPkVal").val()){
      jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
      function(r) {
			if (r == false) {			   
			   $j("#"+Id).val("-1");
			   $j("#"+Id+" option[value="+elementValue+"]").attr("selected", "selected");
			   parent.document.getElementById(Id).focus();	
			   $j('#'+Id).removeClass('labChanges');			   
			   if(elementValue==$j("#alphaThalismiaTraitPkVal").val() || elementValue==$j("#hemozygousNoDiesasePkVal").val() || elementValue==$j("#hemoAlphaThalassmiaPkVal").val() || elementValue==$j("#hemoTraitPkVal").val() || elementValue==$j("#hemozygousNoDonePkVal").val() || elementValue==$j("#hemozygousUnknownPkVal").val() || elementValue==$j("#hemoMultiTraitPkVal").val()){
				   $j("#showassessbutton").show();
				   $j("#hideassesmentHemo").hide();
			   }
			   $j("[name='submitButtonAutoDefer']:last").val("false"); 
			  }else if(r == true){
				  $j("[name='submitButtonAutoDefer']:last").val("true");
				  autoDefer = true;
				  mutex_Lock = true;
				  element_Id = Id;
				  element_Name='cdrCbuPojo.hemoglobinScrn';
				  autoDeferFormSubmitParam[0]= 'formprocandcount';
				  autoDeferFormSubmitParam[1]= url;
				  autoDeferFormSubmitParam[2]='';
				  autoDeferFormSubmitParam[3]= 'update1';
				  autoDeferFormSubmitParam[4]= 'status2';
				  refresh_DivNo = 1536;
				  refresh_CordID = '<s:property value="cdrCbuPojo.cordID"/>';
				  refreshDiv_Id[0]='defer';
				  refreshDiv_Id[1]=	'status3'
				  commonMethodForSaveAutoDefer();
				  if(elementValue==$j("#alphaThalismiaTraitPkVal").val() || elementValue==$j("#hemozygousNoDiesasePkVal").val() || elementValue==$j("#hemoAlphaThalassmiaPkVal").val() || elementValue==$j("#hemoTraitPkVal").val() || elementValue==$j("#hemozygousNoDonePkVal").val() || elementValue==$j("#hemozygousUnknownPkVal").val() || elementValue==$j("#hemoMultiTraitPkVal").val()){
					  assessLinknFlag_Id[0]= 'true';
					  assessLinknFlag_Id[1]= 'LAB_SUM_Hemo_showassess';
					  assessLinknFlag_Id[2]= 'LAB_SUM_Hemo_hideassess';
				   }
			  }
			});
	   }
}

function textFunction(divId,textAreaId,saveButtonId,savedComment){
		$j("#"+divId).show();
		textareaValue=document.getElementById(textAreaId).value;
		if(savedComment!=null && savedComment!=''){
			$j("#"+textAreaId).attr('readonly',true);
			$j("#"+saveButtonId).hide();
		}
}

function commentSave(divId,textAreaId,addComment)
{
		textareaValue=document.getElementById(textAreaId).value;
		if(textareaValue==''){
			$j("#"+addComment).text("Add Comment");
			//$j("#"+textId).name("Add Comment");
		}
		else{
			$j("#"+addComment).text("Add/View Comment");
		}
		$j("#"+divId).hide();
}

function commentClose(divId,textAreaId)
{
		document.getElementById(textAreaId).value = textareaValue;
		$j("#"+divId).hide();
}

function bactAutoDefer(Val,Id,datetextId,dateId){ 
	elementValue = autoDeferField_1;
	var cordId =<s:property value ="cdrCbuPojo.cordID"/>;
	var url="saveLabSummary?cordId="+cordId+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val();
	if (Val==$j('input[name="bacterialPositive"]').val() || Val ==$j("#bacterialNotDone").val()){
		jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
				function(r) {
					if (r == false) {
					   //$j("#"+Id).val("");
					   $j("#"+Id).focus();
					   $j('#'+Id).removeClass('labChanges');
					   $j("#"+Id+" option[value="+elementValue+"]").attr("selected", "selected");
					   bacterial(elementValue);
					   $j("[name='submitButtonAutoDefer']:last").val("false"); 
					   //$j("#"+datetextId).hide();
					   //$j("#"+dateId).hide();
					  
					  }else if(r == true){
						  $j("[name='submitButtonAutoDefer']:last").val("true");
						  autoDefer = true;
						  mutex_Lock = true;
						  element_Id = Id;
						  element_Name = 'cdrCbuPojo.bacterialResult';
						  autoDeferFormSubmitParam[0]= 'formprocandcount';
						  autoDeferFormSubmitParam[1]= url;
						  autoDeferFormSubmitParam[2]='';
						  autoDeferFormSubmitParam[3]= 'update1';
						  autoDeferFormSubmitParam[4]= 'status2';
						  refresh_DivNo = 1536;	
						  refresh_CordID = '<s:property value="cdrCbuPojo.cordID"/>';
						  refreshDiv_Id[0]='defer';
						  refreshDiv_Id[1]=	'status3';			  
						  commonMethodForSaveAutoDefer();
						  //autoDeferModalFormSubmitRefreshDiv('formprocandcount',url,'','update1','status2');
				    	  //refreshMultipleDivsOnViewClinical('1536','<s:property value="cdrCbuPojo.cordID"/>');
						 // $j("#defer").css('display','block');
						 // $j("#status3").css('display','none');	
						  }
					});
		}
		}

function fungAutoDefer(Id,datetextId,dateId,assessId){
	elementValue = autoDeferField_2;
	var cordId =<s:property value ="cdrCbuPojo.cordID"/>;
	var url="saveLabSummary?cordId="+cordId+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val();
	  if ($j("#fungalResultModal").val()==$j("#bacterialPositiveFung").val())
	       jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
			function(r) {
				if (r == false) {
				   //$j("#"+Id).val("-1");
				   try{
				   $j('#'+Id).removeClass('labChanges');
				   $j("#"+Id).focus();
				   $j("#"+Id+" option[value="+elementValue+"]").attr("selected", "selected");
				   $j("#"+datetextId).hide();
				  // $j("#"+assessId).hide();
				   fungal(elementValue);
				   if(elementValue==$j("#fungalCulnotdone").val()){
					   $j("#showassesmentFung").show();
					   $j("#hideassesmentFung").hide();
				   }
					   $j("[name='submitButtonAutoDefer']:last").val("false"); 
				   }catch(e){alert(e);}
				  }else if(r == true){
					 
					  $j("[name='submitButtonAutoDefer']:last").val("true");
					  autoDefer = true;
					  mutex_Lock = true;
					  element_Id = Id;
					  element_Name ='cdrCbuPojo.fungalResult';
					  autoDeferFormSubmitParam[0]= 'formprocandcount';
					  autoDeferFormSubmitParam[1]= url;
					  autoDeferFormSubmitParam[2]='';
					  autoDeferFormSubmitParam[3]= 'update1';
					  autoDeferFormSubmitParam[4]= 'status2';
					  refresh_DivNo = 1536;
					  refresh_CordID = '<s:property value="cdrCbuPojo.cordID"/>';
					  refreshDiv_Id[0]='defer';
					  refreshDiv_Id[1]=	'status3';
					  commonMethodForSaveAutoDefer();					    					  
					  if(elementValue==$j("#fungalCulnotdone").val()){
						  assessLinknFlag_Id[0]= 'true';
						  assessLinknFlag_Id[1]= 'LAB_SUM_Fung_showassess';
						  assessLinknFlag_Id[2]= 'LAB_SUM_Fung_hideassess';
					   }
					  //autoDeferModalFormSubmitRefreshDiv('formprocandcount',url,'','update1','status2');
					  //refreshMultipleDivsOnViewClinical('1536','<s:property value="cdrCbuPojo.cordID"/>');
					  //$j("#defer").css('display','block');
					  //$j("#status3").css('display','none');	
					  }
				});
      }

function closeModal4(){
	 $j("#dynaModalForm").dialog("close");
	 $j("#dynaModalForm").dialog("close");
	 $j("#dynaModalForm").dialog("destroy");
	 $j("#dynaModalForm").dialog("destroy");
	 $j("#labSummaryModal").dialog("close");
	 $j("#labSummaryModal").dialog("close");
	 $j("#labSummaryModal").dialog("destroy");
	 $j("#labSummaryModal").dialog("destroy");
}

$j(".timeValid").keypress(function(e) {
	  var val = $j(this).val();
	  //console.log("Val::"+val);
	  if(val !="" && val!=null ){
		  if(e.keyCode!=8 && e.keyCode!=46 )
		  {
			  if(val.length==2){						  			  
				  if(e.which!=58){
					   $j(this).val(val+":");
				  }
			  }
		  }
	  }
});		

function closeModal5(){
	 $j("#update").dialog("close");
	 $j("#update").dialog("close");
	 $j("#update").dialog("destroy");
	 $j("#update").dialog("destroy");
}
function showAssment(Id,buttonTd,divId,mDivId,hideButtonTd){
	var assesmentCheckFlag="";
	var responsestr=$j('#'+Id+' option:selected').text();

	var val = $j('#'+Id+' option:selected').val();

	var assesTest1=$j("#alphaThalismiaTraitPkVal").val();
	
	var assesTest2=$j("#hemozygousNoDiesasePkVal").val();
	
	var assesTest3=$j("#hemoAlphaThalassmiaPkVal").val();
	
	var assesTest4=$j("#hemoTraitPkVal").val();

	var assesTest5=$j("#hemozygousNoDonePkVal").val();

	var assesTest6=$j("#hemozygousUnknownPkVal").val();
	
	var assesTest7=$j("#hemoMultiTraitPkVal").val();

	var fungLabAssesPoes=$j("#fungalCulpostive").val();
	
	var fungLabAssesPoes1=$j("#fungalCulnotdone").val();
	var data=null;
	if(val==fungLabAssesPoes1){
		if(val==$j("#lb_fungresult").val()){
			assesmentCheckFlag="true";
		}
		else{
			assesmentCheckFlag="flase";
		}
		data='entityId=<s:property value="cdrCbuPojo.cordID"/>&subEntityId='+val+'&shrtName=LAB_SUM_Fung&subentitytype='+$j("#subentityType").val()+'&questionNo=FungalCulture&responsestr='+responsestr+'&assessDivName='+mDivId+'&responseval='+val;
		
	}
	else if(val==assesTest1 || val==assesTest2 || val==assesTest3 || val==assesTest4 || val==assesTest5 || val==assesTest6 || val==assesTest7){
		if(val==$j("#lb_hemoglobinscrn").val()){
			assesmentCheckFlag="true";
		}
		else{
			assesmentCheckFlag="false";
		}
		data='entityId=<s:property value="cdrCbuPojo.cordID"/>&subEntityId='+val+'&shrtName=LAB_SUM_Hemo&subentitytype='+$j("#subentityType").val()+'&questionNo=Hemoglobinopathy Testing&responsestr='+responsestr+'&assessDivName='+mDivId+'&responseval='+val;
	}
	if(val!=null && val!= ""){
		$j('#'+mDivId).show();
		$j('#'+hideButtonTd).show();
		$j('#'+buttonTd).hide();
		if($j('#'+divId).html() != null && $j('#'+divId).html() != "" && $j('#'+divId).html() != 'undefined'){
			//$j('#'+divId).show();
		}else{
		refreshassessment1('getassessmentHemo?assesmentCheckFlag='+assesmentCheckFlag,divId,data);
		}
	}else{
		$j('#'+mDivId).hide();
		$j('#'+buttonTd).hide();
	}
}
function hideAssment(mDivId,shwbuttonTd,hideButtonTd){
	$j('#'+mDivId).hide();
	$j('#'+shwbuttonTd).show();
	$j('#'+hideButtonTd).hide();
}

function checkassessment1(tname,masterDiv,buttonId,hideButtonTd,id,val){
	
	var divid=tname;
	var mDiv=masterDiv;
	var assesmentCheckFlag="";
	var assesTest1=$j("#alphaThalismiaTraitPkVal").val();
	
	var assesTest2=$j("#hemozygousNoDiesasePkVal").val();
	
	var assesTest3=$j("#hemoAlphaThalassmiaPkVal").val();
	
	var assesTest4=$j("#hemoTraitPkVal").val();

	var assesTest5=$j("#hemozygousNoDonePkVal").val();

	var assesTest6=$j("#hemozygousUnknownPkVal").val();
	
	var assesTest7=$j("#hemoMultiTraitPkVal").val();

	var fungLabAssesPoes=$j("#fungalCulpostive").val();

	var fungLabAssesPoes1=$j("#fungalCulnotdone").val();
	
	//alert(data);
	//alert("tname : "+tname);
	var responsestr=$j('#'+id+' option:selected').text();
	
	//var data='entityId='+$j("#cordID").val()+'&subEntityId='+$j("#cordID").val()+'&shrtName=LAB_SUM&subentitytype='+$j("#subentityType").val()+'&&questionNo=Hemoglobinopathy&responsestr='+responsestr;
	$j('#'+hideButtonTd).hide();
	if(val!=null && val!= "" && (val==assesTest1 || val==assesTest2 || val==assesTest3 || val==assesTest4 || val==assesTest5 || val==assesTest6 || val==assesTest7)){
		if(val==$j("#lb_hemoglobinscrn").val()){
			assesmentCheckFlag="true";
		}
		else{
			assesmentCheckFlag="false";
		}
		var data='entityId=<s:property value="cdrCbuPojo.cordID"/>&subEntityId='+val+'&shrtName=LAB_SUM_Hemo&subentitytype='+$j("#subentityType").val()+'&questionNo=Hemoglobinopathy Testing&responsestr='+responsestr+'&assessDivName='+mDiv+'&responseval='+val;	
		$j('#'+mDiv).show();
		$j('#'+buttonId).hide();
		$j('#'+hideButtonTd).show();
		refreshassessment1('getassessmentHemo?assesmentCheckFlag='+assesmentCheckFlag,divid,data);
	}
	else if(val==fungLabAssesPoes1){
		if(val==$j("#lb_fungresult").val()){
			assesmentCheckFlag="true";
		}
		else{
			assesmentCheckFlag="flase";
		}
		var data='entityId=<s:property value="cdrCbuPojo.cordID"/>&subEntityId='+val+'&shrtName=LAB_SUM_Fung&subentitytype='+$j("#subentityType").val()+'&questionNo=FungalCulture&responsestr='+responsestr+'&assessDivName='+mDiv+'&responseval='+val;
		$j('#'+mDiv).show();
		//$j('#'+buttonId).show();
		$j('#'+buttonId).hide();
		$j('#'+hideButtonTd).show();
		refreshassessment1('getassessmentHemo?assesmentCheckFlag='+assesmentCheckFlag,divid,data);
	}
	else{
			$j('#'+mDiv).hide();
			$j('#'+buttonId).hide();
			//$j('#showassessbutton').hide();
		}
	//2nd Type Assessment
}



function refreshassessment1(url,divname,data){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data:data,
        success: function (result){
            var $response=$j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).html(result);
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
}

function bacterial(value)
{
	//alert("Value"+value)
	if(value==$j("#bacterialPositive").val() || value==$j("#bacterialNegative").val())
	{
		$j("#bactDateText1").show();
		$j("#bactDate1").show();
	}else{
		$j("#bactDateText1").hide();
		$j("#bactDate1").hide();
		$j("#datepicker6").val(null);
	}
	if(value!="<s:property value='cdrCbuPojo.bacterialResult'/>" && (value!=$j("#bacterialPositive").val() || value!=$j("#bacterialNegative").val())){
		$j("#datepicker6").val(null);
	}else{
		$j("#datepicker6").val($j("#bactCultDateHiddenId").val());
	}
	$j("#dropValue").val(value);
	//alert($j("#dropValue").val());
	if($j("#datepicker6").val()!=""){
		$j(".placeHolderOfid_datepicker6").hide();
	}else{
		$j(".placeHolderOfid_datepicker6").show();
	}
}

function fungal(value){
	if(value==$j("#bacterialPositiveFung").val() || value==$j("#bacterialNegativeFung").val()){
		$j("#fungDateText1").show();
		$j("#fungDate1").show();
	}
	else{
		$j("#fungDateText1").hide();
		$j("#fungDate1").hide();
		$j("#datepicker7").val(null);
	}
	if(value!="<s:property value='cdrCbuPojo.fungalResult'/>" && (value!=$j("#bacterialPositiveFung").val() || value!=$j("#bacterialNegativeFung").val())){
		$j("#datepicker7").val(null);
	}else{
		$j("#datepicker7").val($j("#fungCultDateHiddenId").val());
	}
	
	$j("#dropValueFung").val(value);
	if($j("#datepicker7").val()!=""){
		$j(".placeHolderOfid_datepicker7").hide();
	}else{
		$j(".placeHolderOfid_datepicker7").show();
	}
}
var assesmmentDeferFlag = true;
			 function savefForCBUSearching(cordId,autoDeferSubmit){
				
				 var labSummChange = divChanges('labChanges',$j('#labSumm_modelbodyDiv').html());
				 
					if(!labSummChange){
						
						labSummChange = divChanges('assessmentChngCls',$j('#labSumm_modelbodyDiv').html());
						
					}				
				 if(autoDeferStatus(autoDeferSubmit, labSummChange)){
					 try{
				 		  setOrdersClear();
				 		  var prcessDate = setMinDateWidRespCollDat();
				 		  var hourFlag = validateHours('collectionDateId','collectionTimeId','datepicker5','cordFreezeTime','frzTimeRangeErrorMsg');
				 		  var prcDate = validateDateData('datepicker4','cordProcessingTime','datepicker5','cordFreezeTime','frzdateAftprcStart','frzTimeRangebfProcssTime');
				 		  var prcdateRange = validateCordDateRange('collectionDateId','datepicker4','processdateErrorMsg1',true);
				 		  var frzdateRange = validateCordDateRange('collectionDateId','datepicker5','frzdateRangeErrorMsg',true);
				 		  var url="saveLabSummary?cordId="+cordId+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val();
				 		  var dateElements = $j('.hasDatepicker').get();
				 		  $j.each(dateElements, function(){
				 			  if(this.id=='datepicker4' || this.id=='datepicker5' || this.id=='datepicker6' || this.id=='datepicker7')
				 				  customCordDateRanges(this);
				 		  });
				 		  }catch(e){alert(e);}
				 		  
				          if($j("#formprocandcount").valid() && prcessDate == true && hourFlag==true && prcDate == true && frzdateRange == true && prcdateRange == true) 
					      {
					        try{
						      if('<s:property value="esignFlag"/>'=="review"){
						    	  var noMandatory=true;
						    		$j('#divprocandcountviewlabsum .cbuLabSummMandatory').each(function(){
						    			 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
						    				 noMandatory=false;
						    				}
						    			  });
							      url = url+"&esignFlag=review&noMandatory="+noMandatory;
							      }
						      else{
						      
						    	  var labSummChanges = divChanges('labChanges',$j('#labSumm_modelbodyDiv').html());
									if(!labSummChanges){
										labSummChanges = divChanges('assessmentChngCls',$j('#labSumm_modelbodyDiv').html());
									}
									if(labSummChanges==true){
						    	  	modalFormSubmitRefreshDiv('formprocandcount',url,'','update1','status2','1','<s:property value="cdrCbuPojo.cordID"/>');
						    	  	refreshMultipleDivsOnViewClinical('1536','<s:property value="cdrCbuPojo.cordID"/>');
						    	  	if($j('#fcrFlag').val() == "Y"){
										refreshMultipleDivsOnViewClinical('2','<s:property value="cdrCbuPojo.cordID"/>');
										}
						          $j("#status3").css('display','none');
									}
									else  if(updateStatusDiv){
										 showSuccessdiv('update1','labSumm_modelbodyDiv');
										}
							      }				
						      $j('#fromViewClinicalLS1').val('1');
				        	  }catch(e){alert(e);}
			    }
			  }
          }
          
          var updateStatusDiv = true;
						 function autoDeferStatus(param, labSummChange) {

			 	var returnval =true;
			 	
			 	if(labSummChange){
			 	     
			 	 	    	  var fungFlag = false;
			 	 	    	  var bactFlag = false;
			 	 	    	  var hemoFlag = false;
			 	 	    	  
			 				  if($j("#hemoglobinScrnTest").val()==$j("#alphaThalismiaTraitPkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDiesasePkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoAlphaThalassmiaPkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoTraitPkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDonePkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemozygousUnknownPkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoMultiTraitPkVal").val() || $j("#hemoglobinScrnTest").val() == $j("#multTrait").val()
			 		 		    	      ){
			 		    		updateStatusDiv=true;
			 		    		returnval = true;
			 		    		 $j("[name='submitButtonAutoDefer']:last").val("true");
			 		    	 }
			 				 if(('<s:property value="cdrCbuPojo.bacterialResult"/>' != $j("#bacterialResult").val()) && ($j("#bacterialResult").val()== $j('#formprocandcount').find("#bacterialPositive").val() || $j("#bacterialResult").val()== $j('#formprocandcount').find("#bacterialNotDone").val()/*$j("#bacterialNotDone").val()*/)){
			 		    		updateStatusDiv=false;
			 		    		returnval = false;
			 		    		bactAutoDefer($j('#bacterialResult').val(),$j('#bacterialResult').attr('id'),'bactDateText1','bactDate1');
			 		    		 $j("[name='submitButtonAutoDefer']:last").val("true");
			 		    		
			 		    	 }
			 		    	  if(('<s:property value="cdrCbuPojo.fungalResult"/>' != $j("#fungalResultModal").val()) && ($j("#fungalResultModal").val() == /*$j("#bacterialPositiveFung").val()*/$j('#formprocandcount').find("#bacterialPositiveFung").val())){
			 		    		 
			 	 		    	 updateStatusDiv = false;
			 	 		    	 returnval = false;
			 	 	   		   	 fungAutoDefer($j('#fungalResultModal').attr('id'),'fungDateText1','fungDate1','');
			 	 	   		     $j("[name='submitButtonAutoDefer']:last").val("true");
			 	 	   		     
			 			     }
			 		    	 if(('<s:property value="cdrCbuPojo.hemoglobinScrn"/>' != $j("#hemoglobinScrnTest").val()) && ($j("#hemoglobinScrnTest").val() == $j("#hemoBeatThal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoSickBeta").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoSickCell").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoAlphaSev").val())){
			 		    		
			 		    		updateStatusDiv=false;
			 		    		returnval = false;
			 		    		autoDeferHEMO($j('select[name="cdrCbuPojo.hemoglobinScrn"].labclass').val(),$j('select[name="cdrCbuPojo.hemoglobinScrn"].labclass').attr('id'));
			 		    		$j("[name='submitButtonAutoDefer']:last").val("true");
			 		    		 
			 		    	 }else
			 		    		 $j("[name='submitButtonAutoDefer']:last").val("false"); 	    	
			 				}
			 				
			 			 	return returnval; 			  
			 	}
			 
			 
			 function rhSpecify(val){
		           if($j("#rhtypeother").val()==val){
		               $j("#rhothspec").show();
		           }else{
		        	   $j("#rhothspec").hide();
		        	  //$j("#cdrCbuPojo.specRhTypeOther").val('');
		        	   $j('input[name="cdrCbuPojo.specRhTypeOther"]').val('');
		           }
		     }

		$j(".timeValid").keypress(function(e) 
				{
					  var val = $j(this).val();
					  if(e.keyCode!=8 && e.keyCode!=46 )
					  {
						  if(val.length==2){						  			  
							  if(e.which!=58){
								   $j(this).val(val+":");
							  }
						  }
					  }
		        });	

	$j(function(){
		              /*	       
		                alert("yyyyy"+$j("#bacterialPositiveFung").val());
			            alert($j("#bacterialPositive").val());
		                alert($j("#bacterialNegativeFung").val());
						var today = new Date();
						var d = today.getDate();
						var m = today.getMonth();
						var y = today.getFullYear();
						var h = today.getHours();
						var mn=today.getMinutes()+1;
										 $j( "#datepicker4" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
												changeYear: true });	
										
										 $j( "#datepicker5" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
												changeYear: true });
										 $j( "#datepicker6" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
												changeYear: true });	
										
										 $j( "#datepicker7" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
												changeYear: true });
					 */
					 getDatePic();		
					 assignDatePicker('datepicker4');
					 assignDatePicker('datepicker5');
					 assignDatePicker('datepicker6');
					 assignDatePicker('datepicker7');
		
		
							 var validator = $j("#formprocandcount").validate({
		
												   rules:{	
												     			    "prcsngStartDateStr":{required:{
																		depends: function(element){
																			if($j(this).val()!=""){
																				var formattedDate = dateFormat($j(this).val(),'mmm dd, yyyy');
																				$j(this).val(formattedDate);
																			}
																		return ($j("#lb_prcsngstartdate").val()!=$j(this).val());
																	}
																   },dpDate: true},
																	"frzDateStr":{required:{
																		depends: function(element){
																			if($j(this).val()!=""){
																				var formattedDate = dateFormat($j(this).val(),'mmm dd, yyyy');
																				$j(this).val(formattedDate);
																			}
																		return ($j("#lb_frzdate").val()!=$j(this).val());
																	}
																   },dpDate: true},
																	"cdrCbuPojo.bacterialResult":{validateSelect :["lb_bactresult"]},
																	"cdrCbuPojo.fungalResult":{validateSelect :["lb_fungresult"]},
																	"cdrCbuPojo.aboBloodType":{validateSelect :["lb_abobloodtype"]},
																	"cdrCbuPojo.hemoglobinScrn":{validateSelect : ["lb_hemoglobinscrn"]},
																	"cdrCbuPojo.rhType":{validateSelect : ["lb_rhtype"]},
																	"cdrCbuPojo.processingTime":{required:{
																		depends: function(element){
																		return ($j("#lb_prcsngstarttime").val()!=$j(this).val());
																	}
																   },validateTimeFormat:["lb_prcsngstarttime"]},
																	"cdrCbuPojo.freezeTime":{validateTimeFormat:["lb_frztime"]},
																	"cdrCbuPojo.specRhTypeOther":{required:
																	{	  depends: function(element){
																		        return ($j("#rhtypeother").val()==$j("#cdrcburhtype").val());
																			}
																		}			
																     },
																     "LAB_SUM_Hemo_assessmentRemarks":{ required:{
																			depends: function(element) {
																				if($j("#hemoglobinScrnTest").val()==$j("#alphaThalismiaTraitPkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDiesasePkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemoAlphaThalassmiaPkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemoTraitPkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDonePkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemozygousUnknownPkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoMultiTraitPkVal").val()){
																					return true;
																				}
																				else{return false;}
																				}
																			},
																				maxlength:200
																		},
																		"LAB_SUM_Fung_assessmentRemarks":{ required:{
																														depends: function(element) {
																															if($j("#fungalResultModal").val()==$j("#fungalCulnotdone").val()){
																																return true;
																															}
																															else{return false;}
																															}
																														},
																															maxlength:200
																													},
																		"LAB_SUM_Hemo_assessmentresponse":{ required :{
																														depends: function(element) {
																															if($j("#hemoglobinScrnTest").val()==$j("#alphaThalismiaTraitPkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDiesasePkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemoAlphaThalassmiaPkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemoTraitPkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDonePkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemozygousUnknownPkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoMultiTraitPkVal").val()){
																																return true;
																															}
																															else{return false;}
																															}
																														}
																											},
																		"LAB_SUM_Fung_assessmentresponse":{ required :{
																												depends: function(element) {
																													if($j("#fungalResultModal").val()==$j("#fungalCulnotdone").val()){
																														return true;
																													}
																													else{return false;}
																													}
																												}
																									},
															"bactCultDateStr":{required:
														            {	  depends: function(element){
																			        return ($j("#dropValue").val()==$j("#bacterialPositive").val() || $j("#dropValue").val()==$j("#bacterialNegative").val() || (($j("#bacterialResult").val()==$j("#bacterialPositive").val() || $j("#bacterialResult").val()==$j("#bacterialNegative").val()) && (document.getElementById("datepicker6").defaultValue != '')));
																				}
																			}			
																	     },	

															"fungCultDateStr":{required:
																 {	  depends: function(element){
																	       return ($j("#dropValueFung").val()==$j("#bacterialPositiveFung").val() || $j("#dropValueFung").val()==$j("#bacterialNegativeFung").val() || (($j("#fungalResultModal").val()==$j("#bacterialPositiveFung").val() || $j("#fungalResultModal").val()==$j("#bacterialNegativeFung").val())&& (document.getElementById("datepicker7").defaultValue != '')));
																			}
																		}			
																	 },				     
											             },
												 messages:{	
									            	        "fungCultDateStr":{required:"<s:text name="garuda.cbu.cordentry.fungalstartdate"/>"},
									            	        "bactCultDateStr":{required:"<s:text name="garuda.cbu.cordentry.bactculstartdate"/>"},
															"prcsngStartDateStr":{required:"<s:text name="garuda.cbu.cordentry.procstartdate"/>"},
															"frzDateStr":{required:"<s:text name="garuda.cbu.cordentry.freezedate"/>"},
															"cdrCbuPojo.aboBloodType":"<s:text name="garuda.cbu.cordentry.abobloodtype"/>",
															"LAB_SUM_Hemo_assessmentRemarks":{
												         		required : "<s:text name="garuda.label.dynamicform.errormsg"/>",
												         		maxlength : "<s:text name="garuda.cbu.cordentry.cordadditionalinfomaxlength"/>"
																},
															"LAB_SUM_Fung_assessmentRemarks":{
													         	required : "<s:text name="garuda.label.dynamicform.errormsg"/>",
													         	maxlength : "<s:text name="garuda.cbu.cordentry.cordadditionalinfomaxlength"/>"
																},
															"LAB_SUM_Hemo_assessmentresponse":"<s:text name="garuda.label.dynamicform.errormsg"/>",
															"LAB_SUM_Fung_assessmentresponse":"<s:text name="garuda.label.dynamicform.errormsg"/>",
															"cdrCbuPojo.rhType":"<s:text name="garuda.cbu.cordentry.rhtype"/>",
															"cdrCbuPojo.bacterialResult":"<s:text name="garuda.cbu.cordentry.bactcul"/>",
															"cdrCbuPojo.fungalResult":"<s:text name="garuda.cbu.cordentry.fungcul"/>",
															"cdrCbuPojo.hemoglobinScrn":"<s:text name="garuda.cbu.cordentry.hemotesting"/>",
															"cdrCbuPojo.processingTime":{required:"<s:text name="garuda.cbu.cordentry.proctime"/>",validateTimeFormat:"<s:text name="garuda.common.validation.timeformat"/>"},
															"cdrCbuPojo.freezeTime":{validateTimeFormat:"<s:text name="garuda.common.validation.timeformat"/>"},
															"cdrCbuPojo.specRhTypeOther":{required:"<s:text name="garuda.common.validation.reason"/>"}	
														   }
									  
								 });
		
		                 });

	function setOrdersClear(){
		var orderId = $j("#orderId").val();
		var orderType = $j("#orderType").val();

		if(typeof(orderId)=='undefined'){
			$j("#orderId").val("");
			orderId="";
		}else if(orderId!=null && orderId.indexOf(",")!=-1){
			$j("#orderId").val("");
			orderId="";
		}
		
		if(typeof(orderType)=='undefined'){
			$j("#orderType").val("");
			orderType="";
		}else if(orderType!=null && orderType.indexOf(",")!=-1){
			$j("#orderType").val("");
			orderType="";
		}
	}
	function setDefBactFungDate(procDateStrId,bactDateStrId,fungDateStrId,emptyChngFlag){
		if($j("#"+procDateStrId).val()!=null){
			if(document.getElementById(bactDateStrId)!=null && document.getElementById(bactDateStrId)!='undefined' && $j("#"+bactDateStrId).is(':visible')==true){
				if($j("#"+bactDateStrId).val()=='' && emptyChngFlag=="procChng"){
				$j("#"+bactDateStrId).val($j("#"+procDateStrId).val());
				}else if($j("#"+bactDateStrId).val()=='' && emptyChngFlag=="bactChng"){
					$j("#"+bactDateStrId).val($j("#"+procDateStrId).val());
				}
			}
			if(document.getElementById(fungDateStrId)!=null && document.getElementById(fungDateStrId)!='undefined' && $j("#"+fungDateStrId).is(':visible')==true){
				if($j("#"+fungDateStrId).val()=='' && emptyChngFlag=="procChng"){
				$j("#"+fungDateStrId).val($j("#"+procDateStrId).val());
				}else if($j("#"+fungDateStrId).val()=='' && emptyChngFlag=="fungChng"){
					$j("#"+fungDateStrId).val($j("#"+procDateStrId).val());
				}
			}
		}
		if($j("#"+bactDateStrId).val()!=""){
			$j(".placeHolderOfid_"+bactDateStrId).hide();
		}else{
			$j(".placeHolderOfid_"+bactDateStrId).show();
		}
		if($j("#"+fungDateStrId).val()!=""){
			$j(".placeHolderOfid_"+fungDateStrId).hide();
		}else{
			$j(".placeHolderOfid_"+fungDateStrId).show();
		}
	}
	$j(function(){
		autoDeferField_1 = $j('select[name="cdrCbuPojo.bacterialResult"] option:selected').val();
		autoDeferField_2 = $j('select[name="cdrCbuPojo.fungalResult"] option:selected').val();
		autoDeferField_3 = $j('select[name="cdrCbuPojo.hemoglobinScrn"] option:selected').val();
		});
/**** Business validattion for mandatory fields ****/
   $j(function(){
	   jQuery.validator.addMethod("validateSelect", function(value, element, params) {
		    if(params.length > 0 && ($j('#'+params[0]).val()==value || $j('#'+params[0]).val()=="")){
                return true;
			}else{
				return (true && (parseInt(value) != -1));
			}			
		}, "<s:text name='garuda.cordentry.PlsSel'/>");

		jQuery.validator.addMethod("validateTimeFormat", function(value, element, params) {
			if(params.length > 0 && ($j('#'+params[0]).val()==value || $j('#'+params[0]).val()=="")){
				return true;
			}else{
			 return (true && (dateTimeFormat(value)));		 
			}
		}, "");

	   });


   function setMinDateWidRespCollDat(){
		  var FreezeDate=new Date($j("#datepicker5").val());
		  var ProcessingDate=new Date($j("#datepicker4").val());
		  var flag = true;
		  var procStartDt = new Date($j("#procStartDtinLabSum").val());
		  var procTermiDt = new Date($j("#procTermiDtinLabSum").val());
		  
		  if (ProcessingDate < procStartDt || ProcessingDate > procTermiDt) {
			  $j("#dynaErrorPrcngDt").show();
			  flag = false;
		  }
		  else
		  {
			  $j("#dynaErrorPrcngDt").hide();
		  }

		  if (FreezeDate < procStartDt || FreezeDate > procTermiDt) {
			  $j("#dynaErrorFreezeDt").show();
			  flag = false;
		  }
		  else
		  {
			  $j("#dynaErrorFreezeDt").hide();
		  }
		  return flag;
	  }
		

   </SCRIPT>
 
  <div id="divprocandcountviewlabsum"> 
  <s:form id="formprocandcount">
    <s:hidden name="cordCbuStatus"></s:hidden>
    <s:hidden name="autoDeferExecFlag" id="labSumaryAutoDeferExecFlag"/>
    <s:hidden value="%{#request.bacterialNotDone}" id="bacterialNotDone"></s:hidden>
	<s:hidden value="%{#request.bacterialPositive}" id="bacterialPositive" name="bacterialPositive"></s:hidden>
	<s:hidden value="%{#request.bacterialNegative}" id="bacterialNegative"></s:hidden>
	<s:hidden name="fromViewClinicalLS1" id="fromViewClinicalLS1"></s:hidden>
	<s:hidden name="dropValue" id="dropValue"></s:hidden>
	<s:hidden name="subentityType" id="subentityType" value="%{#request.subentitytype}"/>
	<s:set name="tempCordId" value="cdrCbuPojo.cordID" scope="request"/>
	<s:hidden name="cordID" id="cordID" value="%{#request.tempCordId}"></s:hidden>
	<s:hidden value="%{#request.bacterialPositiveFung}" id="bacterialPositiveFung"></s:hidden>
	<s:hidden value="%{#request.bacterialNegativeFung}" id="bacterialNegativeFung"></s:hidden>
	<s:hidden value="%{#request.bacterialNotDoneFung}" id="bacterialNotDoneFung"></s:hidden>
	<s:hidden name ="assess_ncd" id="assess_ncd" value="%{#request.assess_ncd}"/>
	<s:hidden id='iscdrMember' value="%{cdrCbuPojo.site.isCDRUser()}"></s:hidden>
	<s:hidden value="%{#request.hemoBeatThal}" id="hemoBeatThal"></s:hidden>
	<s:hidden value="%{#request.hemoSickBeta}" id="hemoSickBeta"></s:hidden>
	<s:hidden value="%{#request.hemoSickCell}" id="hemoSickCell"></s:hidden>
	<s:hidden value="%{#request.hemoAlphaSev}" id="hemoAlphaSev"></s:hidden>
	<s:hidden value="%{#request.multTrait}" id="multTrait"></s:hidden>
	<s:hidden value="%{cdrCbuPojo.registryId}" id="regid" />	
	<s:hidden name="dropValueFung" id="dropValueFung"></s:hidden>
	<s:hidden value="%{getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN,@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_NORMAL)}" id="normal_id"></s:hidden>

	<%-- hidden field is taken to store the initial values of manadatory field --%>	
	<s:hidden value="%{cdrCbuPojo.processingTime}" id="lb_prcsngstarttime"></s:hidden>
	<s:hidden value="%{cdrCbuPojo.freezeTime}" id="lb_frztime"></s:hidden>
	<s:hidden value="%{cdrCbuPojo.bacterialResult}" id="lb_bactresult"></s:hidden>
	<s:hidden value="%{cdrCbuPojo.fungalResult}" id="lb_fungresult"></s:hidden>
	<s:hidden value="%{cdrCbuPojo.aboBloodType}" id="lb_abobloodtype"></s:hidden>
	<s:hidden value="%{cdrCbuPojo.rhType}" id="lb_rhtype"></s:hidden>
	<s:hidden value="%{cdrCbuPojo.hemoglobinScrn}" id="lb_hemoglobinscrn"></s:hidden>	
	<s:hidden value="%{cdrCbuPojo.fkCbbProcedure}" id="fkCbbProcedure"></s:hidden>
	<s:date name="cdrCbuPojo.getSpecimen().getSpecCollDate()" id="collectionDateId" format="MMM dd, yyyy" />
	<s:hidden value="%{collectionDateId}" id="collectionDateId" cssClass="collectDate" name="collectiondateandtime"/>
	<s:date name="cdrCbuPojo.getSpecimen().getSpecCollDate()" id="collectionTimeId" format="HH:mm" />
	<s:hidden value="%{collectionTimeId}" id="collectionTimeId" name="collectionTime"/>
	
	<s:hidden value="%{#request.procStartDt}" id="procStartDtinLabSum"></s:hidden>
	<s:hidden value="%{#request.procTermiDt}" id="procTermiDtinLabSum"></s:hidden>
	<s:hidden name="submitButtonAutoDefer" id="autodeferfieldflag" value="false"/>
	<input type="hidden" id="orderId" value=<s:property value="orderId"/> >
	<input type="hidden" id="orderType" value=<s:property value="orderType"/> >
	  <s:hidden name="cdrCbuPojo.rhTypeOther" id="rhtypeother" ></s:hidden>
		<div id="update1" style="display: none;" >
			
			 <!--<jsp:include page="../cb_update.jsp">
			    <jsp:param value="garuda.common.lable.processMessage"  name="message" />
			  </jsp:include>
			 -->
			 <table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr >
					<td colspan="2">
						<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
							<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
							<strong><s:text name="garuda.completeReqInfo.label.labsummaryupdateSucc" /></strong></p>
						</div>
					</td>
				</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="button" onclick="closeModal4()" value="<s:text name="garuda.pendingorderpage.label.close"/>" />
							</td>
						</tr>
			</table>	   
		</div>	
		<div id="labSumm_modelbodyDiv">		
			      <table width="100%" cellpadding="0" cellspacing="0" border="0" id ="status2">
						         <tr>
							               <td width="20%" style="padding-left:10px;" align="left" onmouseover="return overlib('<s:text
												name="garuda.cordentry.label.procStartDateToolTip"></s:text>');" 
												onmouseout="return nd();"><s:text name="garuda.cordentry.label.prostartdate"></s:text>:<br/><font size="1">&nbsp;</font></td>
										   <td width="20%">
													 <s:date name="cdrCbuPojo.processingDate" id="datepicker4" format="MMM dd, yyyy"/>
													 <s:textfield readonly="true" name="prcsngStartDateStr" id="datepicker4"  value="%{datepicker4}" cssClass="procStartDate" onchange="addTestDate(this.value),addTestDateScnd(this.value);setDefBactFungDate('datepicker4','datepicker6','datepicker7','procChng');isChangeDone(this,'%{datepicker4}','labChanges');"></s:textfield><br/>
													 <span style="display:none" id="dynaErrorPrcngDt" class="error"><br/><s:text name="garuda.cbu.cordentry.prcssngdtwrtproc"/></span><font size="1">&nbsp;</font>
													 <span style="display:none" id="processdateErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.collectionmust"></s:text></span>
													 <span style="display:none" id="processdateErrorMsg1" class="error"><br/><s:text name="garuda.cbu.cordentry.procdateaftercoldate"></s:text></span>
													 <span style="display:none" id="precessDatebffrz" class="error prcessError"><br/><s:text name="garuda.cbu.cordentry.procbeforefreezdate"/></span>
													 <s:hidden value="%{datepicker4}" id="lb_prcsngstartdate"></s:hidden>													 
											</td>
											
								       <td width="20%"><s:text name="garuda.cordentry.label.proStartTime"></s:text>:<br/><font size="1">&nbsp;</font></td>	
									   <td width="20%"><s:textfield name="cdrCbuPojo.processingTime" cssClass="timeValid cbuclass" maxlength="5" id="cordProcessingTime" onchange="validateDateData('datepicker4','cordProcessingTime','datepicker5','cordFreezeTime','precessDatebffrz','precessDatebffrztime');isChangeDone(this,'%{cdrCbuPojo.processingTime}','labChanges');"></s:textfield><font size="1">(HH:MM 24 HR)</font>
									   <span style="display:none" id="precessDatebffrztime" class="error prcessError"><br/><s:text name="garuda.cbu.cordentry.procbeforefreeztime"/></span>
									   </td>								  						
									   <td width="20%"></td>
						       </tr>
										
								 <tr>
	 								    <td width="20%" style="padding-left:10px;" onmouseover="return overlib('<s:text
										name="garuda.cordentry.label.freezeStartDateToolTip"></s:text>');" 
										onmouseout="return nd();"><s:text name="garuda.cordentry.label.freezedate"></s:text>:<br/><font size="1">&nbsp;</font></td>
									    <td width="20%"  >
										   <s:date name="cdrCbuPojo.frzDate" id="datepicker5" format="MMM dd, yyyy" />
										   <s:textfield readonly="true" name="frzDateStr" id="datepicker5" value="%{datepicker5}" onchange="isChangeDone(this,'%{datepicker5}','labChanges');" cssClass="freezDate"></s:textfield><br/>
										   <span style="display:none" id="frzdateErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.procdatebeforefrzdate"></s:text></span>
										   <span style="display:none" id="frzdateRangeErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.frzdateTimehrs"></s:text> </span>
										   <span style="display:none" id="dynaErrorFreezeDt" class="error"><br/><s:text name="garuda.cbu.cordentry.freezedtwrtproc"/></span><font size="1">&nbsp;</font>
										   <span style="display:none" id="frzdateAftprcStart" class="error prcessError"><br/><s:text name="garuda.cbu.cordentry.frzdateafterproc"></s:text></span>
										   <s:hidden value="%{datepicker5}" id="lb_frzdate"></s:hidden>										   
						               </td>
									   <td width="20%"><s:text name="garuda.cordentry.label.freezeTime"></s:text>:<br/><font size="1">&nbsp;</font></td>	
									   <td width="20%"><s:textfield name="cdrCbuPojo.freezeTime" cssClass="timeValid cbuclass" maxlength="5" id="cordFreezeTime"
									   				onchange="validateDateData('datepicker4','cordProcessingTime','datepicker5','cordFreezeTime','frzdateAftprcStart','frzTimeRangebfProcssTime');validateHours('collectionDateId','collectionTimeId','datepicker5','cordFreezeTime','frzTimeRangeErrorMsg');isChangeDone(this,'%{cdrCbuPojo.freezeTime}','labChanges');"></s:textfield><font size="1">(HH:MM 24 HR)</font><br/>
									   				<span style="display:none" id="frzTimeRangeErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.frzTimehrs"></s:text> </span>
									   				<span style="display:none" id="frzTimeRangebfProcssTime" class="error prcessError"><br/><s:text name="garuda.cbu.cordentry.frztimeafterproctime"></s:text> </span>
									   				</td>					  							
									   <td width="20%"></td>
						         </tr>
											
										<tr>
											<td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.bactcult"></s:text>:</td>
									        <td width="20%"><s:select name="cdrCbuPojo.bacterialResult" cssClass="cbuLabSummMandatory" id="bacterialResult" onchange="bacterial(this.value),bactAutoDefer(this.value,this.id,'bactDateText1','bactDate1');setDefBactFungDate('datepicker4','datepicker6','datepicker7','bactChng');isChangeDone(this,'%{cdrCbuPojo.bacterialResult}','labChanges');" list="populatedData.bactCulList" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select></td>
											<td width="20%" id="bactDateText1" <s:if test="cdrCbuPojo.bacterialResult!=#request.bacterialNegative && cdrCbuPojo.bacterialResult!=#request.bacterialPositive">style="display : none;"</s:if>><s:text name="garuda.cordimport.label.bactCultDate"></s:text>:</td>	
											<td width="20%" id="bactDate1" <s:if test="cdrCbuPojo.bacterialResult!=#request.bacterialNegative && cdrCbuPojo.bacterialResult!=#request.bacterialPositive">style="display : none;"</s:if>>
												<s:date name="cdrCbuPojo.bactCultDate" id="datepicker6" format="MMM dd, yyyy" />
												<s:textfield readonly="true" name="bactCultDateStr" id="datepicker6" value="%{datepicker6}" onchange="isChangeDone(this,'%{datepicker6}','labChanges');" cssClass="bactFungDate"></s:textfield>
										 		<s:hidden value="%{datepicker6}" id="bactCultDateHiddenId"/>
										 	</td>	
					  					   
									       <td width="20%"><s:set name="bactComment" value="cdrCbuPojo.bactComment" scope="request" />
																	   <!-- <s:property value="cdrCbuPojo.bactComment" default="value not fetched"/>-->
																		<s:if test ="(#request.bactComment == '' || #request.bactComment == null)">
																			<a href="#" onclick="textFunction('textArea1','textAreaForBact','comSave1','<s:property value="cdrCbuPojo.bactComment"/>')" id="com1" ><img height="15px" src="./images/addcomment.png"><span id="addBactComment"><s:text name="garuda.cbu.cordentry.addcomment"/></span></a>
																	    </s:if>
																		<s:else>
																			<a href="#" onclick="textFunction('textArea1','textAreaForBact','comSave1','<s:property value="cdrCbuPojo.bactComment"/>')" id="com3" ><img height="15px" src="./images/addcomment.png"><span id="addBactComment"><s:text name="garuda.cbufinalreview.label.flaggedItems.viewcomment"/></span></a>
																		</s:else>
																		<div id="textArea1" style="display: none;" >
																		<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
																		<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
																			<s:text name="garuda.label.dynamicform.commentbox" />
																		</div>
																		<table>
																			<tr>
																				<td colspan="2"> 
																					<s:textarea name="cdrCbuPojo.bactComment" cols="40" rows="5" id="textAreaForBact" onchange="isChangeDoneBactComment(this,'labChanges');"/> 
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<button type="button" onclick="commentSave('textArea1','textAreaForBact','addBactComment')" id="comSave1" ><s:text name="Save"/></button>
																				</td>
																				<td>
																				    <button type="button" onclick="commentClose('textArea1','textAreaForBact')" id="comClose1" ><s:text name="Close"/></button>
																				</td>
																			</tr>
																		</table></div>
																	</div>
																	 </td>			  						
							     	 							</tr><%-- <s:textfield name="cdrCbuPojo.bactComment"></s:textfield> --%>			  						
							     	 	
							     	 	
							     	 	<tr>
							     	 		<td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.fungcult"></s:text>:</td>
								          <s:hidden name="fungalCulpostive" id="fungalCulpostive" value="%{#request.fungalCulpostive}" />
								           <s:set name="fungalCulpostive1" value="%{#request.fungalCulpostive}" scope="request"/>
								           <s:hidden name="fungalCulnotdone" id="fungalCulnotdone" value="%{#request.fungalCulnotdone}" />
											<s:set name="fungalCulnotdone1" value="%{#request.fungalCulnotdone}" scope="request"/>
								           <td width="20%"><s:select name="cdrCbuPojo.fungalResult" cssClass="labclass" id="fungalResultModal" onchange="fungal(this.value);checkassessment1('Assessment_fungalCulture','Assessment_LabSummeryFung','showassesmentFung','hideassesmentFung',this.id,this.value),fungAutoDefer('fungalResultModal','fungDateText1','fungDate1','Assessment_LabSummeryFung');setDefBactFungDate('datepicker4','datepicker6','datepicker7','fungChng');isChangeDone(this,'%{cdrCbuPojo.fungalResult}','labChanges');" list="populatedData.funCulList" listKey="pkCodeId" listValue="description"  headerKey="-1" headerValue="Select"></s:select></td>
							     	 	  <td width="20%" id="fungDateText1" <s:if test="cdrCbuPojo.fungalResult!=#request.bacterialNegativeFung && cdrCbuPojo.fungalResult!=#request.bacterialPositiveFung">style="display : none;"</s:if>><s:text name="garuda.cordimport.label.fungCultDate"></s:text>:</td>	
									      <td width="20%" id="fungDate1" <s:if test="cdrCbuPojo.fungalResult!=#request.bacterialNegativeFung && cdrCbuPojo.fungalResult!=#request.bacterialPositiveFung">style="display : none;"</s:if>>
											 <s:date name="cdrCbuPojo.fungCultDate" id="datepicker7" format="MMM dd, yyyy" />
											 <s:textfield readonly="true" name="fungCultDateStr" id="datepicker7" value="%{datepicker7}" cssClass="bactFungDate" onchange="isChangeDone(this,'%{datepicker7}','labChanges');"></s:textfield>
									      	<s:hidden value="%{datepicker7}" id="fungCultDateHiddenId"/>
									      </td>	
					 					  
					                       <td width="20%">
					                        <s:set name="fungComment" value="cdrCbuPojo.fungComment" scope="request" />
																	    <!--<s:property value="cdrCbuPojo.bactComment" default="default value"/>-->
												<s:if test = "(#request.fungComment == '' || #request.fungComment == null)" >
														<a href="#" onclick="textFunction('textArea2','textAreaForFung','comSave2','<s:property value="cdrCbuPojo.fungComment"/>')" id="com2" ><img height="15px" src="./images/addcomment.png"><span id="addFungComment"><s:text name="garuda.cbu.cordentry.addcomment"/></span></a>
												</s:if>
												<s:else>
														<a href="#" onclick="textFunction('textArea2','textAreaForFung','comSave2','<s:property value="cdrCbuPojo.fungComment"/>')" id="com4" ><img height="15px" src="./images/addcomment.png"><span id="addFungComment"><s:text name="garuda.cbufinalreview.label.flaggedItems.viewcomment"/></span></a>
												</s:else>
														<div id="textArea2" style="display: none;" >
														<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
														<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
															<s:text name="garuda.label.dynamicform.commentbox" />
														</div>
															<table>
																<tr>
																	<td colspan="2"> 
																		<s:textarea name="cdrCbuPojo.fungComment" cols="40" rows="5" id="textAreaForFung" onchange="isChangeDoneFungComment(this,'labChanges');"/> 
																	</td>
																</tr>
																<tr>
																	<td>
																		<button type="button" onclick="commentSave('textArea2','textAreaForFung','addFungComment')" id="comSave2" ><s:text name="Save"/></button>
																	</td>
																	<td>
																		<button type="button" onclick="commentClose('textArea2','textAreaForFung')" id="comClose2" ><s:text name="Close"/></button>
																	</td>
																</tr>
															</table></div>
														</div>
											</td>
											
					                       <%-- <s:textfield name="cdrCbuPojo.fungComment"></s:textfield> --%>		  										  							
						               </tr>
						               <tr>
						               		<td>
						               			<span id="showassesmentFung" class="LAB_SUM_Fung_showassess"><s:if test="cdrCbuPojo.fungalResult==#request.fungalCulnotdone1"><a href="#"  id="showassesmentFungButton" onclick="showAssment('fungalResultModal','showassesmentFung','Assessment_fungalCulture','Assessment_LabSummeryFung','hideassesmentFung');"><s:text name="garuda.label.dynamicform.showassess"/></a></s:if></span>
						               			<div id="hideassesmentFung" style="display: none;" class="LAB_SUM_Fung_hideassess"><s:if test="(cdrCbuPojo.fungalResult==#request.fungalCulnotdone1)"><a href="#"  id="hideassesmentFungButton" onclick="hideAssment('Assessment_LabSummeryFung','showassesmentFung','hideassesmentFung');"><s:text name="garuda.label.dynamicform.hideassess"/></a></s:if></div>
						               		</td>
						               </tr>
										<tr id="Assessment_LabSummeryFung"  style="display: none;"><td colspan="5">
							               	
							               	<fieldset><legend><s:text name="garuda.clinicalnote.label.assessment"></s:text>&nbsp;For&nbsp;<s:text name="garuda.cordentry.label.fungcult"/>
												</legend><table>
												<tr >
													<td id="Assessment_fungalCulture"></td>
													
												</tr></table></fieldset></td>
										</tr>
									  <tr>
						   			     
						   			     <td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.bloodtype"></s:text>:</td>
									     <td width="20%"><s:select name="cdrCbuPojo.aboBloodType" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BLOOD_GROUP]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select Type" onchange="isChangeDoneABO(this,'labChanges');"  ></s:select></td>
						   			     
						   			      <td width="20%"><s:text name="garuda.cordentry.label.rhtype"></s:text>:</td>
					                      <td width="20%"><s:select name="cdrCbuPojo.rhType" id="cdrcburhtype" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@RH_TYPE]" listKey="pkCodeId" onchange="rhSpecify(this.value); isChangeDone(this,'%{cdrCbuPojo.rhType}','labChanges');" listValue="description" headerKey="-1"  headerValue="Select Type"></s:select></td>
						   			   
						   			    <td width="60%" colspan="3">
												    <div id="rhothspec" <s:if test="cdrCbuPojo.rhType!=cdrCbuPojo.rhTypeOther">style="display: none;"</s:if> >
												       <table>
												           <tr>
												               <td><s:text name="garuda.cbbprocedures.label.specifyother"></s:text>:</td>
												               <td>
												                   <s:textfield name="cdrCbuPojo.specRhTypeOther" maxLength="30" cssStyle="width: 200px;" onchange="isChangeDone(this,'%{cdrCbuPojo.specRhTypeOther}','labChanges');"></s:textfield>
												               </td>
												           </tr>
												       </table>
												    </div>
					     				</td> 
						          	 </tr>     

	
	             					<tr>	 
	             
	             					 <td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.hemoglobinScrn"></s:text>:</td>
								         <s:hidden name="alphaThalismiaTraitPkVal" id="alphaThalismiaTraitPkVal" value="%{#request.alphaThalismiaTraitPkVal}" />
										<s:set name="alphaThalismiaTraitPkVal1" value="%{#request.alphaThalismiaTraitPkVal}" scope="request"/>
										<s:hidden name="hemozygousNoDiesasePkVal" id="hemozygousNoDiesasePkVal" value="%{#request.hemozygousNoDiesasePkVal}" />
										<s:set name="hemozygousNoDiesasePkVal1" value="%{#request.hemozygousNoDiesasePkVal}" scope="request"/>
										<s:hidden name="hemoAlphaThalassmiaPkVal" id="hemoAlphaThalassmiaPkVal" value="%{#request.hemoAlphaThalassmiaPkVal}" />
										<s:set name="hemoAlphaThalassmiaPkVal1" value="%{#request.hemoAlphaThalassmiaPkVal}" scope="request"/>
										<s:hidden name="hemoTraitPkVal" id="hemoTraitPkVal" value="%{#request.hemoTraitPkVal}" />
										<s:set name="hemoTraitPkVal1" value="%{#request.hemoTraitPkVal}" scope="request"/>
										<s:hidden name="hemozygousNoDonePkVal" id="hemozygousNoDonePkVal" value="%{#request.hemozygousNoDonePkVal}" />
										<s:set name="hemozygousNoDonePkVal1" value="%{#request.hemozygousNoDonePkVal}" scope="request"/>
										<s:hidden name="hemozygousUnknownPkVal" id="hemozygousUnknownPkVal" value="%{#request.hemozygousUnknownPkVal}" />
										<s:set name="hemozygousUnknownPkVal1" value="%{#request.hemozygousUnknownPkVal}" scope="request"/>
										<s:hidden name="hemoMultiTraitPkVal" id="hemoMultiTraitPkVal" value="%{#request.hemoMultiTraitPkVal}" />
										<s:set name="hemoMultiTraitPkVal1" value="%{#request.hemoMultiTraitPkVal}" scope="request"/>										
								        <td width="20%"><s:select name="cdrCbuPojo.hemoglobinScrn" id="hemoglobinScrnTest" style="width:250px" cssClass="labclass" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onChange="checkassessment1('Assessment_Hemoglobinopathy','Assessment_LabSummeryHemo','showassessbutton','hideassesmentHemo',this.id,this.value),autoDeferHEMO(this.value,this.id); isChangeDoneHemo(this,'labChanges');"></s:select></td>
						   			    <td width="20%"></td>
	                  
						
	             </tr>
	             <tr><td>
	             	<span id="showassessbutton" class="LAB_SUM_Hemo_showassess"><s:if test="cdrCbuPojo.hemoglobinScrn==#request.alphaThalismiaTraitPkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDonePkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousUnknownPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDiesasePkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoAlphaThalassmiaPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoTraitPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoMultiTraitPkVal1"><br><a href="#"  id="showassesmentHemoButton" onclick="showAssment('hemoglobinScrnTest','showassessbutton','Assessment_Hemoglobinopathy','Assessment_LabSummeryHemo','hideassesmentHemo');"><s:text name="garuda.label.dynamicform.showassess"/></a></s:if></span>
	             	<div id="hideassesmentHemo" style="display: none;" class="LAB_SUM_Hemo_hideassess"><s:if test="cdrCbuPojo.hemoglobinScrn==#request.alphaThalismiaTraitPkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDonePkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousUnknownPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDiesasePkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoAlphaThalassmiaPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoTraitPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoMultiTraitPkVal1"><a href="#"  id="hideassesmentHemoButton" onclick="hideAssment('Assessment_LabSummeryHemo','showassessbutton','hideassesmentHemo');"><s:text name="garuda.label.dynamicform.hideassess"/></a></s:if></div>
	             	</td>
	             </tr>
	             			<tr id="Assessment_LabSummeryHemo"  style="display: none;"><td colspan="5">
						          	<fieldset><legend><s:text name="garuda.clinicalnote.label.assessment"></s:text>&nbsp;For&nbsp;<s:text name="garuda.cordentry.label.hemoglobinScrn"/>
												</legend><table>
												<tr>
										<td colspan="5" id="Assessment_Hemoglobinopathy"></td>
										</tr></table></fieldset></td>
										<td></td>
									</tr>	
	              
				</table>
				<div id="defer" style="display: none;" >
						     
				</div>
		            
		       <table id="status3" cellpadding="0" cellspacing="0" width="100%">
				 <tr bgcolor="#cccccc">
       				<td colspan="2" align="right"><jsp:include page="../cb_esignature.jsp" flush="true">
		       				<jsp:param value="labSummarysubmit" name="submitId"/>
							<jsp:param value="labSummaryinvalid" name="invalid" />
							<jsp:param value="labSummaryminimum" name="minimum" />
							<jsp:param value="labSummarypass" name="pass" />
       				</jsp:include></td>
		  			<td colspan="2" align="left">
		  				<button type="button"  disabled="disabled"  id="labSummarysubmit" onclick="savefForCBUSearching(<s:property value ="cdrCbuPojo.cordID"/>,'submitButtonAutoDefer')"><s:text name="garuda.unitreport.label.button.submit"/></button>
		 	  			<button type="button"  onclick="closeModal4();"><s:text name="garuda.unitreport.label.button.cancel"></s:text></button>		
		  			</td>  	   
               </tr>         
			  </table>
			  </div>
	      </s:form>
	 </div> 					
 <script>
 function isChangeDoneABO(thisVal,classname){
	  isChangeDone(thisVal,'<s:property value="cdrCbuPojo.aboBloodType" />',classname);
	  }
	  function isChangeDoneHemo(thisVal,classname){
	  isChangeDone(thisVal,'<s:property value="cdrCbuPojo.hemoglobinScrn"/>',classname);
	  }
	  function isChangeDoneBactComment(thisVal,classname){
	  isChangeDone(thisVal,'<s:property value="cdrCbuPojo.bactComment"/>',classname);
	  }
	  function isChangeDoneFungComment(thisVal,classname){
	  isChangeDone(thisVal,'<s:property value="cdrCbuPojo.fungComment"/>',classname);
	  }
	  
	  $j('[name="cordCbuStatus"]').val($j('#cordCbuStatus').val());
	  $j('#executeAutoDeferFlagID').val('labSumaryAutoDeferExecFlag');
 </script>