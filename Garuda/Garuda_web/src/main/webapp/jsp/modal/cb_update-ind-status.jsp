<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script>

$j(function(){
	
	$j("#indNmdpStatus").validate({
			
		rules:{	
				"fkNmdpSpNum":{required:
								{depends:
									function(element){
										return $j("input[id=nmdp]:radio:checked").val()=="on";
													}
								}
							},
				"fkNmdpSpName":{required:
									{depends:
										function(element){
										return $j("input[id=nmdp]:radio:checked").val()=="on";
														}
									}
								}
			},
			messages:{	
				"fkNmdpSpNum":"<s:text name="garuda.cbu.nmdp.sponsernum"/>",
				"fkNmdpSpName":"<s:text name="garuda.cbu.nmdp.sponsername"/>"							
			}
		});

	$j("#indOtherStatus").validate({
		
		rules:{										
				"sponsorNum":{required:
								{depends:
									function(element){
										return $j("input[id=other]:radio:checked").val()=="on";
													}
								}
							},
				"sponsorName":{required:
								{depends:
									function(element){
										return $j("input[id=other]:radio:checked").val()=="on";
													}
								}
							}
			},
			messages:{				
				"sponsorNum":"<s:text name="garuda.cbu.nmdp.sponsernum"/>",
				"sponsorName":"<s:text name="garuda.cbu.nmdp.sponsername"/>"	
			}
		});
	});
function showNmdpDiv(){
	$j('#nmdpDiv').attr('style','display:block');
	$j('#otherDiv').attr('style','display:none');
	$j('#esign').val("");
	$j("#submit").attr("disabled", true);
	$j("#pass").css('display','none'); 
	$j("#minimum").css('display','none');	
	$j("#invalid").css('display','none'); 
}
function showOtherDiv(){
	$j('#nmdpDiv').attr('style','display:none');
	$j('#otherDiv').attr('style','display:block');
	$j('#otheresign').val("");
	$j("#othersubmit").attr("disabled", true);
	$j("#otherpass").css('display','none'); 
	$j("#otherminimum").css('display','none');	
	$j("#otherinvalid").css('display','none');
}

function submitNMDPInfo(formname, sponsorSid){
	
	modalFormSubmitRefreshDiv(formname,'submitINDStatus?sponsorid='+sponsorSid,'main');
	
}

</script>

<div id="indDiv" class='tabledisplay '>
<s:hidden name="sponsorName" id="sponsorName"></s:hidden>
<s:hidden name="sponsorNum" id="sponsorNum"></s:hidden>
<s:hidden name="fkNmdpSpName" id="fkNmdpSpName"></s:hidden>
<s:hidden name="fkNmdpSpNum" id="fkNmdpSpNum"></s:hidden>
<s:hidden name="cdrCbuPojo.fkSpecimenId" />
<s:hidden name="cdrCbuPojo.specimen.pkSpecimen" />
<s:hidden name="showEsign" id="showEsign" />
	<s:form>
	<div id="update" style="display: none;" >
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="garuda.message.modal.IND" name="message"/>
		  </jsp:include>	   
	</div>
	</s:form>	
  <table width="100%" cellspacing="0" cellpadding="0" border="0" id="status">
  
    <s:if test="#request.indupdate==true" ></s:if>
	<tr>
  <td colspan="2"/>
  </tr>
  <tr>
  <td>
  </td>
  <td>
	</td>					
  </tr>
  
   <tr>
  <td width="25%"><s:text	name="garuda.unitreport.label.nmdp_holder" />:</td>
  
	<td width="25%" ><input type="radio" name="indsponsor" id="nmdp" onclick="showNmdpDiv();"   /><s:text	name="garuda.unitreport.label.nmdp" /></td>
	<td width="25%"> <input type="radio" name="indsponsor" id="other" onclick="showOtherDiv();" /><s:text	name="garuda.unitreport.label.other" /></td>
	<td width="25%"> &nbsp;</td>
	</tr>

	<tr>			
   <td colspan="4">
   <s:form id="indNmdpStatus">
	<s:hidden name="cdrCbuPojo.pkNMDPSponsorId" id="pkNMDPSponsorId"></s:hidden>
	<s:hidden name="cdrCbuPojo.pkOtherSponsorId" id="pkOtherSponsorId"></s:hidden>
	
	<s:hidden name="cdrCbuPojo.cordID" />
	<s:hidden name="licenceUpdate" value="True"></s:hidden>
   <div id="nmdpDiv" style="display: none;" >
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	
				<tr>
					<td><s:text	name="garuda.unitreport.label.sponsor_num" />:</td>
			      <td align="left">
			      <s:select name="fkNmdpSpNum" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IND_SPONSOR_NUMBER]"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select"  />
			      </td>
			    </tr>
			    <tr>
			    
			  	  <td><s:text	name="garuda.unitreport.label.sponsor_name" />:</td>
			   	 <td>
			   	  <s:select name="fkNmdpSpName" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IND_SPONSOR_NAME]"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select"  /> 
			    </tr>
		</table>
		<table  bgcolor="#cccccc">
			    <tr valign="baseline" bgcolor="#cccccc">
				<td><jsp:include page="../cb_esignature.jsp" flush="true"></jsp:include></td>
			    <td align="right">
			    <input type="button" id="submit" disabled="disabled" onclick="submitNMDPInfo('indNmdpStatus','<s:property value="cdrCbuPojo.pkNMDPSponsorId" />')" value="<s:text name="garuda.unitreport.label.button.submit"/>" /> 
			    </td>
			    <td>
			    	<input type="button" onclick="closeModal();" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />
			    </td>
			    </tr>
		</table>
	</div>
</s:form>
<s:form id="indOtherStatus">	
<s:hidden name="cdrCbuPojo.pkNMDPSponsorId" id="pkNMDPSponsorId"></s:hidden>
<s:hidden name="cdrCbuPojo.pkOtherSponsorId" id="pkOtherSponsorId"></s:hidden>
<s:hidden name="cdrCbuPojo.cordID" />
<s:hidden name="licenceUpdate" value="True"></s:hidden>
	   <div id="otherDiv" style="display: none;" >
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
			    
			    <tr>
			       <td><s:text	name="garuda.unitreport.label.sponsor_num" />:</td>
			      <td align="left"><s:textfield name="sponsorNum"  onKeyDown="limitText(this.form.limitedtextfield,this.form.countdown,12);" 
onKeyUp="limitText(this.form.limitedtextfield,this.form.countdown,12);" maxlength="12" />
			       </td>
			    </tr>
		 		<tr>
			      <td><s:text	name="garuda.unitreport.label.sponsor_name"  />:</td>
			      <td align="left"><s:textfield name="sponsorName" onKeyDown="limitText(this.form.limitedtextfield,this.form.countdown,12);" 
onKeyUp="limitText(this.form.limitedtextfield,this.form.countdown,12);" maxlength="12" />
			        </td>
			    </tr>
	</table>
	<table bgcolor="#cccccc">
			
			<tr valign="baseline" bgcolor="#cccccc">
			   <td><jsp:include page="../cb_esignature.jsp" flush="true">
	   					<jsp:param value="othersubmit" name="submitId"/>
						<jsp:param value="otherinvalid" name="invalid" />
						<jsp:param value="otherminimum" name="minimum" />
						<jsp:param value="otherpass" name="pass" />
						<jsp:param value="otheresign" name="esign" />
   					</jsp:include>
   				</td>
			<td align="right"><input type="button" id="othersubmit" disabled="disabled" onclick="submitNMDPInfo('indOtherStatus','<s:property value="cdrCbuPojo.pkOtherSponsorId" />');" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
    		</td><td><input type="button" onclick="closeModal();" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />
			</td>
			</tr>
	</table>
	</div>
	</s:form>
   </td>
   </tr>
	
  </table>


</div>