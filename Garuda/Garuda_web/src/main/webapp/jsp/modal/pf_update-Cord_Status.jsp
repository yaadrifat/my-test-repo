<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script>

</script>
<s:form id="cordStatus">
<s:hidden name="licenceUpdate" value="True"></s:hidden>
<s:hidden name="orderId"></s:hidden>
<div id="update" style="display: none;" >
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="garuda.message.modal.cbuStatus" name="message"/>
		  </jsp:include>	   
</div>
	 <table width="100%" cellspacing="0" cellpadding="0" border="0" id="status">
		<tr>
			<td>
				<s:text name="garuda.advancelookup.label.crdstus"/>
			</td>
			<td>
				<s:select name="orderStatus" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_STATUS]" 
						listKey="pkCodeId" listValue="description" headerKey=""	headerValue="Select"/>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:middle">
				<s:text name="garuda.clinicalnote.label.reason"/>
			</td>
			<td>
				<s:textarea cols="35" rows="5" name="statReason"></s:textarea>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
				<input type="button" value="<s:text name="garuda.cbbprocedures.labelsave"/>" onclick="modalFormSubmitRefreshDiv('cordStatus','submitCordStatus','main');"/><input type="button" value="<s:text name="garuda.cbbprocedures.label.cancel"/>" onclick="closeModal();"/>
			</td>
		</tr>
	</table>
</s:form>