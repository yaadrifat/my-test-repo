<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script>
jQuery(function(){
    
    	 $j("#updateMailIdForm").validate({
    		 event: "keyup",

    		 rules: {
 				
 				emailId: {
 					required: true,
 					email: true
 				}
 			},
   			
 			messages:{
 				"emailId":"<s:text name="garuda.common.validation.emailid"/>"
 			}
             
         });

});


</script>
<div id="MailIdDiv" class='tabledisplay'>
<s:form id="updateMailIdForm" name="updateMailIdForm">
<s:hidden name="mailUpdate" value="True"></s:hidden>
<s:hidden name="userPojo.userId" />
	<div id="update" style="display: none;" >
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="garuda.message.modal.mail.id" name="message"/>
		  </jsp:include>	   
	</div>
  <table width="100%" cellspacing="0" cellpadding="0" border="0" id="status">
   <tr>
		<td width="50%">
  			<s:text name="garuda.common.label.emailId"/>
  		</td>
		<td width="50%">
			<s:textfield name="emailId" id="emailId" />
		</td>
	</tr>
	
	<tr bgcolor="#cccccc">
	 <td ><jsp:include page="../cb_esignature.jsp" flush="true"></jsp:include></td>
		
    <td  align="center" colspan="2">
    <input type="button" id="submit" disabled= "disabled" onclick="modalFormSubmitRefreshDiv('updateMailIdForm','submitUserMailId','main');" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
    <input type="button"  onclick="closeModal()" value="<s:text name="garuda.unitreport.label.button.cancel"/>" /></td>
    </tr>	
  </table>
</s:form>

</div>