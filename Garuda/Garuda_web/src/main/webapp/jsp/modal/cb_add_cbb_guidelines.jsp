<%@ taglib prefix="s"  uri="/struts-tags"%>
<%
String contextpath=request.getContextPath();
%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include> 
<script>

$j(function(){
	/*	var today = new Date();
		var d = today.getDate();
		var m = today.getMonth();
		var y = today.getFullYear();

		
		
$j("#datepicker1").datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
	changeYear: true});
*/
getDatePic();
});

$j(function(){
	$j("#addcbbGuidelinesform").validate({
		
		rules:{	
			"fundingGuidelinespojo.fundingId":"required",
			"fundingGuidelinespojo.fkFundCateg":"required",
			"fundingGuidelinespojo.description":"required",
			"fundingGuidelinespojo.fundingStatus":"required",
			"fundingGuidelinespojo.fundingBdate":{required:true,checkfundingguidelinesdate:["FundingGuidelines","fundingBdate","fkFundCateg","fkCbbId"]}
		},
		messages:{
			"fundingGuidelinespojo.fundingId":"<s:text name="garuda.cbu.funding.fundingid"/>",
			"fundingGuidelinespojo.fkFundCateg":"<s:text name="garuda.cbu.funding.fundingcat"/>",
			"fundingGuidelinespojo.description":"<s:text name="garuda.common.validation.description"/>",
			"fundingGuidelinespojo.fundingStatus":"<s:text name="garuda.cbu.funding.fundingstatus"/>",
			"fundingGuidelinespojo.fundingBdate":{required : "<s:text name="garuda.common.validation.date"/>",checkfundingguidelinesdate:"<s:text name="garuda.cbu.funding.guidelines"/>"}
		}
		});

});

/* $j(function(){
 $j("#addnewtestform").validate({
	
	rules:{	
			"timingOfTest":{required : true},
			"patLabsPojo.testDescription":{required : true,maxlength:50},
			"patLabsPojo.fktestspecimen":{checkselect : true},
			"patLabsPojo.testresult":{required : true,maxlength:30},
			"patLabsPojo.testdate":"required"
			
		},
		messages:{
			"timingOfTest":"Please Select Timing of Test",
			"patLabsPojo.testDescription":{
         		required : "Please Enter Test Description",
         		maxlength : "Maximum 50 characters allowed"
				},
			"patLabsPojo.fktestspecimen":"Please Select Sample Type",
			"patLabsPojo.testresult":{
         		required : "Please Enter Test Result",
         		maxlength : "Maximum 30 characters allowed"
				},
			"patLabsPojo.testdate":"Please Enter Test Start Date"
				}
						
		});
}); */
 function addNewCBBGuidlines(){
		 modalFormSubmitRefreshDiv('addcbbGuidelinesform','addcbbGuidelines','addFundingGuidlines'); 
		 /* submitform('addcbbGuidelinesform');
		 $j("#update").css('display','block');
     	$j("#status").css('display','none'); */
		 /* refreshDiv('addcbbGuidelines','addFundingGuidlines','addcbbGuidelinesform');  */
		   /* $j.ajax({
				type: "POST",
				url : "addcbbGuidelines",
				data : $j("#addcbbGuidelinesform").serialize(),
				success : function(result) {
					$j("#addFundingGuidlines").html(result);
					$j("#update").css('display','block');
	            	$j("#status").css('display','none');
	            	
				},
				error:function() { 
					alert("Error ");
				}
				
				});	     
		   */
	
 }
 
</script>
<div id="update" style="display: none;" >
		<jsp:include page="../cb_update.jsp">
	    <jsp:param value="garuda.common.lable.addcbbGuidelines"  name="message" />
	    </jsp:include>
</div>
						      
<div class="col_100 maincontainer" id="status"><div class="col_100">
	<s:form id="addcbbGuidelinesform" name="addcbbGuidelinesform" action="addcbbGuidelines">
	 <div class="column">
	      <div class="portlet" id="addcbbGuidelinesdiv" >
				<table width="100%" cellpadding="0" cellspacing="0" border="0" >
									  <tr>
									      <td>
									         <s:text name="garuda.fundingNMDP.label.fundingGuidlineId" /><span style="color: red;">*</span>
									      </td>
									      <td>
									      	<s:textfield name="fundingGuidelinespojo.fundingId" readonly="true" onkeydown="cancelBack();" />
									      </td>
									   </tr>
									  <tr>
									      <td>
									         <s:text name="garuda.fundingNMDP.label.fundingCategory" /><span style="color: red;">*</span>
									      </td>
									      <td>
									      	<s:select id="fkFundCateg" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNDING_CATEGORY]" name="fundingGuidelinespojo.fkFundCateg"  listKey="pkCodeId"  listValue="description" headerKey=""	headerValue="Select"></s:select>
									      </td>
									   </tr>
									   <tr>
									   		<td>
									         <s:text name="garuda.fundingNMDP.label.birthDate" /><span style="color: red;">*</span>
									      </td>
									       <td>
									         <s:date name="fundingGuidelinespojo.fundingBdate" id="datepicker12" format="MMM dd, yyyy" />
											<s:textfield readonly="true" onkeydown="cancelBack();"  name="fundingGuidelinespojo.fundingBdate" id="datepicker1"  value="%{datepicker12}" cssClass="datePicWMaxDate"></s:textfield>
										</td>								      
									  </tr>
									  <tr>
										<td><s:text name="garuda.openorder.label.description" /><span style="color: red;">*</span>:</td>
										<td><s:textarea name="fundingGuidelinespojo.description" cols="73" rows="5" cssStyle="width:470px;"  /></td>
	   									</tr>
									  <tr>
									      <td>
									         <s:text name="garuda.fundingNMDP.label.fundingStatus" /><span style="color: red;">*</span>
									      </td>
									      <td>
									      	<s:select id="fundingGuidelinespojo.fundingStatus" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNDING_STATUS]" name="fundingGuidelinespojo.fundingStatus"  listKey="pkCodeId"  listValue="description" headerKey=""	headerValue="Select"></s:select>
									      </td>
									   </tr>
									  
									   <s:hidden name="fundingGuidelinespojo.fkCbbId" id="fkCbbId"/>
									</table>
									<table>
										 <tr>
									   <td><jsp:include page="../cb_esignature.jsp" flush="true"></jsp:include></td>
									   	<td>
									   		<button type="button" 
												onclick="javascript:addNewCBBGuidlines()" disabled="disabled" id="submit"><s:text name="garuda.cdrcbuview.label.button.addNewCBBGuidelines" /></button>
											<input type="button"  onclick="closeModal()" value="Cancel" />
										</td>
									   </tr>
									</table>		
			</div></div>
		</s:form>
	</div></div>