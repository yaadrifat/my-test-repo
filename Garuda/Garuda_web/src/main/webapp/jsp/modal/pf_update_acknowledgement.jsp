<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script>
$j(document).ready(function(){

	$j("#cancelOrdersForm").validate({
		rules:{			
			"cancelResult":{required:true}
		},
		messages:{
			"cancelResult":"<s:text name="garuda.pf.update.cancelResult"/>"
		}
	});
	
});

function submitCancelOrd(){
	

	if($j("#cancelOrdersForm").valid()){
		var vcbuid=$j("#cbuIdval").val();
		var vorderid=$j("#orderIdval").val();
		var vcancelres=$j("#cancelResult").val();
		//alert("vcbuid"+vcbuid+"vorderid"+vorderid+"vcancelres"+vcancelres)
		modalFormSubmitRefreshDiv('cancelOrdersForm','submitCancelOrder?cancelRes='+vcancelres+'&cbuid='+vcbuid+'&orderid='+vorderid,'modelPopup1');
    }	
}


</script>
<div class="maincontainer">
<div class="column">
<s:hidden name="cbuId" id="cbuIdval"/>
<s:hidden name="msgShipDt" id="msgShipDt1"/>
<s:hidden name="orderId" id="orderIdval"/>
<div id="update" style="display: none;" >
		  <s:if test="msgShipDt==null">
				<jsp:include page="../cb_update.jsp">
		  	 	<jsp:param name="message" value="Order cancelled successfully" />
		  	 	</jsp:include>
		  </s:if>
		  <s:else>
		  		<jsp:include page="../cb_update.jsp">
		    	<jsp:param  name="message" value="msgShipDt" />
		  		</jsp:include>		 
		  </s:else>
		   
</div>
<div id="RejectMsgdiv" style="display: none;">
	<jsp:include page="../cb_update.jsp">
    <jsp:param name="message" value="Cancellation request rejected by Cbb co-ordinator" />
	</jsp:include>
</div>
<div id="status">
<form id="cancelOrdersForm">
<table>
<tr>
<td width="25%"></td>
<td width="50%" valign="top">
</br>
</br>
</br>
<table cellpadding="0" cellspacing="0" border="0" align="center" style="vertical-align: middle;" >
	<tr>
		<td width="50%">
			<s:text name="garuda.orders.update_ack.orderCancelCOnfirm"/> 
		</td>
		<td width="50%"><s:select list='#{"Y":"Yes","N":"No"}' name="cancelResult" id="cancelResult" headerValue="-select-" headerKey=""/></td>
	</tr>
</table>
</br>
</br>
</br>
</br>
</br>
</br>

<table style="background-color:#cccccc;">
	<tr valign="baseline">
		<td width="70%">
			<jsp:include page="../cb_esignature.jsp" flush="true">
			<jsp:param value="cancelOrdsubmit" name="submitId"/>
			<jsp:param value="iCancelOrd" name="invalid" />
			<jsp:param value="mCancelOrd" name="minimum" />
			<jsp:param value="pCancelOrd" name="pass" />
			</jsp:include>
		</td>
		<td align="left" width="30%">
			<input type="button" onclick="submitCancelOrd();" value="<s:text name="garuda.unitreport.label.button.submit"/>" id="cancelOrdsubmit" disabled="disabled"/>
		</td>
	</tr>
</table>
</br>
</br>
</br>
</br>
</br>
</br>

</td>
<td width="25%"></td>
</table>

</form>
</div>
</div>
</div>