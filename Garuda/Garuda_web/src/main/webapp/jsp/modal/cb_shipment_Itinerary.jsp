<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants" %>
<script>
$j(document).ready(function(){
	getShipmentTbl();
});
function getShipmentTbl(){

	$j('#shipmentItern').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
		},
		"bRetrieve": true,
		"bDestroy" :true
	});
	
	
	$j('#shipmentItern_length').hide();
	$j('#shipmentItern_filter').hide();
	$j('#shipmentItern_info').hide();
}

function revokeDocumentModal(cdrCbuId,attachmentId,dcmsAttachId,title,url1,lenght,breadth){
	
 	var orderId=$j("#orderId").val();
	var orderType=$j("#orderType").val();	
	//alert(orderId);
	//alert(orderId);
	//alert(url1);
	var answer = confirm('Please confirm if you want to revoke the document.');
	if (answer){	
				if(orderId!=null && orderId!="" && orderId!="undefined" && orderType!=null && orderType!="" && orderType!="undefined"){
						var url='revokeDocFromPF?orderId='+orderId+'&orderType='+orderType+'&cbuCordId='+cdrCbuId+'&cbuid='+cdrCbuId+'&cordId='+cdrCbuId+'&pkcordId='+cdrCbuId+'&cordID='+cdrCbuId+'&attachmentId='+attachmentId;
						loadMoredivs(url,'DUMNDiv,quicklinksDiv,shipmentItinerarychkIdDiv','shipmentIternForm','','');
						modalFormSubmitRefreshDiv('shipmentIternForm',url1,'shipment');
						alert("Document revoked successfully");
					}
				else{
				
						var url='revokeDoc?cbuCordId='+cdrCbuId+'&cbuid='+cdrCbuId+'&cordId='+cdrCbuId+'&pkcordId='+cdrCbuId+'&cordID='+cdrCbuId+'&attachmentId='+attachmentId;
						loadMoredivs(url,'DUMNDiv,quicklinksDiv','shipmentIternForm','','');
						modalFormSubmitRefreshDiv('shipmentIternForm',url1,'shipment');
						alert("Document revoked successfully");
				}
	}
}
</script>
<s:form id="shipmentIternForm">
	<s:hidden name="cdrCbuPojo.cdrCbuId" id="cdrCbuId"></s:hidden> 
	<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
	<s:hidden name="cdrCbuPojo.registryId"  id="cburegid"></s:hidden>
		<div id="shipment">
			<div id="searchTble1">
			<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="shipmentItern">
				<thead>
					<tr>
						<th class="th2" scope="col"><s:text name="garuda.uploaddoc.label.date" /></th>
							<th><s:text name="garuda.uploaddoc.label.documentuploadedby" /></th>
					       <th><s:text name="garuda.uploaddoc.label.documentcategory" /></th>
				          <th><s:text name="garuda.uploaddoc.label.documentsubcategory" /></th>
						<th class="th3" scope="col"><s:text name="garuda.openorder.label.description" /></th>
						<th class="th5" scope="col"><s:text name="garuda.uploaddoc.label.view" /></th>
						<th><s:text name="garuda.common.lable.modify" /></th>
						<th><s:text name="garuda.common.lable.revoke" /></th>
					</tr>
				</thead>
				<tbody>
						<s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPMENT_ITER_CODESUBTYPE]" var="rowVal" status="row">
							<s:set name="cellValue1" value="%{#rowVal[14]}" scope="request"/>
							<tr>
							<td><s:date name="%{#rowVal[10]}" id="uploadDate"format="MMM dd, yyyy /hh:mm:ss a" /><s:property value="%{#uploadDate}" /></td>
					        <td>
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>	
					       </td>	
					        <td><s:property value="getCodeListDescByPk(#rowVal[15])" /></td>
					       <td><s:property value="getCodeListDescByPk(#rowVal[12])" /></td>
							<td><s:property value="%{#rowVal[13]}" /></td>
							<td><a href="#"><img src="images/attachment-icon.png"
								onclick="javascript: return showDocument('<s:property value="%{#rowVal[5]}" />')" /></a></td>
							<s:if test="hasEditPermission(#request.viewShipmentCat)==true">	
							<td><button type="button" onclick="javascript:modifyDocument('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="%{#rowVal[3]}"/>','<s:property value="%{#rowVal[5]}" />');"><s:text name="garuda.common.lable.modify" /></button></td>
							<td><button type="button" onclick="javascript: revokeDocumentModal('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="%{#rowVal[5]}" />','Shipment Itinerary','getShipmentItern?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />','400','500')" ><s:text name="garuda.common.lable.revoke" /></button></td>
							</s:if>
								<s:else>
									<td><button type="button" disabled="disabled"><s:text name="garuda.common.lable.modify" /></button></td>
									<td><button type="button" disabled="disabled"><s:text name="garuda.common.lable.revoke" /></button></td>
				               </s:else>
							</tr>
						</s:iterator>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="6"></td>
					</tr>
				</tfoot>
			</table>
			</div>
			</div>
</s:form>