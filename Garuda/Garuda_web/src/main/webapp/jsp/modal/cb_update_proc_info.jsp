<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<jsp:include page="../cb_track_session_logging.jsp" />
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
		<div id="procinfo">
		<s:form name="cbuProcedureMain" id="cbuProcedureMain">
		<s:hidden name="cdrCbuPojo.cordID" value="%{cdrCbuPojo.cordID}"></s:hidden>
		<s:date name="CdrCbuPojo.specimen.specCollDate"	id="specCollDate" format="MMM dd, yyyy" />
		<s:date name="cdrCbuPojo.processingDate"	id="procDate" format="MMM dd, yyyy" />
		<s:hidden name="cdrCbuPojo.processingDate" value="%{procDate}" id="prcgDate"></s:hidden>
		<s:hidden name="cdrCbuPojo.specimen.specCollDate" value="%{specCollDate}" id="collDate"></s:hidden>
		
		<s:hidden name="entitySamplesPojo.pkEntitySamples"></s:hidden>
		<s:hidden value="%{cdrCbuPojo.fkCbbProcedure}" id="currProc"></s:hidden>
		<div id="proc_update" style="display: none;" >  
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="garuda.message.modal.processingProcedure" name="message"/>
		    <jsp:param value="ProcedureModalForm" name="divName"/>
		  </jsp:include>
   		</div>
   		<div id="bodyProcessing_Div">
			<table cellpadding="0" cellspacing="0" width="100%" id="proc_status">
			    <tr align="center">
			       <td width="20%">
			          <s:text name="garuda.cbbprocedures.label.procname" />:
			       </td>
			       <td colspan="3">
			          <s:if test="#request.cbbProceduresList!=null">
			            <s:select name="cdrCbuPojo.fkCbbProcedure" id="cbbProcessProc" cssClass="cbuProcMandatory" listKey="pkProcId" listValue="procName" list="#request.cbbProceduresList" onchange="isChangeDone(this,'%{cdrCbuPojo.fkCbbProcedure}','procChanges'); getProcedureInfo(this.value,this.id,'submitButtonAutoDefer=false');" headerKey="-1" headerValue="Select"></s:select>
			           </s:if>     
					</td>
			    </tr>
				<tr align="right">
					<td colspan="4">
						<div id="cbbprocedurerefresh">
					     <jsp:include page="../cord-entry/cb_procedure_refresh.jsp" />
					     </div>
					</td>
				</tr>
				 <tr bgcolor="#cccccc">
				 <td colspan="4">
				 <table width="100%">
				 <tr>
				   <td colspan="2" align="right" width="50%">
				   <jsp:include page="../cb_esignature.jsp" flush="true">
				   		<jsp:param value="cbuProcesubmit" name="submitId"/>
						<jsp:param value="cbuProceinvalid" name="invalid" />
						<jsp:param value="cbuProceminimum" name="minimum" />
						<jsp:param value="cbuProcepass" name="pass" />
				   </jsp:include></td>
				   <td colspan="2" align="left" width="50%">
				           	<input type="button" name="procedureEdit" disabled="disabled" id="cbuProcesubmit" value="<s:text name="garuda.unitreport.label.button.submit"/>" onclick="submitProcessinfoForm('submitButtonAutoDefer=false');checkProcHaveGap();">
				          	<input type="button" onclick="closeModals('ProcedureModalForm');" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />
				   </td>
				   </tr>
				   </table>
				   </td>  
			   </tr>
			</table>
			</div>
		</s:form>
		
		<script>
		$j(function() {
			var validator = $j("#cbuProcedureMain").validate({			
				rules:{	
				        "cdrCbuPojo.fkCbbProcedure":{checkselect : true},
						"entitySamplesPojo.filtPap":{required :{
							depends: function(element){
								if($j("#cri_entitySamplesPojo_filtPap").val()=="" || $j("#cri_entitySamplesPojo_filtPap").val()==null){ 
									  if($j("#filtPap").val()!=null && $j("#filtPap").val()!=""){
										return true;}}}
							},},
						"entitySamplesPojo.rbcPel":{required :{
							depends: function(element){
								if($j("#cri_entitySamplesPojo_rbcPel").val()=="" || $j("#cri_entitySamplesPojo_rbcPel").val()==null){ 
									  if($j("#rbcPel").val()!=null && $j("#rbcPel").val()!=""){
										return true;}}}
							},},
						"entitySamplesPojo.extDnaAli":{required :{
							depends: function(element){
								if($j("#cri_entitySamplesPojo_extDnaAli").val()=="" || $j("#cri_entitySamplesPojo_extDnaAli").val()==null){ 
									  if($j("#extDnaAli").val()!=null && $j("#extDnaAli").val()!=""){
										return true;}}}
							},},
						"entitySamplesPojo.noSerAli":{required :{
							depends: function(element){
								if($j("#cri_entitySamplesPojo_noSerAli").val()=="" || $j("#cri_entitySamplesPojo_noSerAli").val()==null){ 
									  if($j("#noSerAli").val()!=null && $j("#noSerAli").val()!=""){
										return true;}}}
							},},
						"entitySamplesPojo.noPlasAli":{required :{
							depends: function(element){
								if($j("#cri_entitySamplesPojo_noPlasAli").val()=="" || $j("#cri_entitySamplesPojo_noPlasAli").val()==null){ 
									  if($j("#noPlasAli").val()!=null && $j("#noPlasAli").val()!=""){
										return true;}}}
							},},
						"entitySamplesPojo.nonViaAli":{required :{
							depends: function(element){
								if($j("#cri_entitySamplesPojo_nonViaAli").val()=="" || $j("#cri_entitySamplesPojo_nonViaAli").val()==null){ 
									  if($j("#nonViaAli").val()!=null && $j("#nonViaAli").val()!=""){
										return true;}}}
							},},
						"entitySamplesPojo.viaCelAli":{required :{
							depends: function(element){
								if($j("#cri_entitySamplesPojo_viaCelAli").val()=="" || $j("#cri_entitySamplesPojo_viaCelAli").val()==null){ 
									  if($j("#viaCelAli").val()!=null && $j("#viaCelAli").val()!=""){
										return true;}}}
							},},
						"entitySamplesPojo.noSegAvail":{required :{
							depends: function(element){
							
								if($j("#cri_entitySamplesPojo_noSegAvail").val()=="" || $j("#cri_entitySamplesPojo_noSegAvail").val()==null){ 
									  if($j("#noSegAvail").val()!=null && $j("#noSegAvail").val()!=""){
											
										return true;}}}
							},},
						"entitySamplesPojo.cbuOthRepConFin":{required :{
							depends: function(element){
								if($j("#cri_entitySamplesPojo_cbuOthRepConFin").val()=="" || $j("#cri_entitySamplesPojo_cbuOthRepConFin").val()==null){ 
									  if($j("#cbuOthRepConFin").val()!=null && $j("#cbuOthRepConFin").val()!=""){
										return true;}}}
							},},
						"entitySamplesPojo.cbuRepAltCon":{required :{
							depends: function(element){
								if($j("#cri_entitySamplesPojo_cbuRepAltCon").val()=="" || $j("#cri_entitySamplesPojo_cbuRepAltCon").val()==null){ 
									  if($j("#cbuRepAltCon").val()!=null && $j("#cbuRepAltCon").val()!=""){
										return true;}}}
							},},
						"entitySamplesPojo.serMatAli":{required :{
							depends: function(element){
								if($j("#cri_entitySamplesPojo_serMatAli").val()=="" || $j("#cri_entitySamplesPojo_serMatAli").val()==null){ 
									  if($j("#serMatAli").val()!=null && $j("#serMatAli").val()!=""){
										return true;}}}
							},},
						"entitySamplesPojo.plasMatAli":{required :{
							
							depends: function(element){
								
								if($j("#cri_entitySamplesPojo_plasMatAli").val()=="" || $j("#cri_entitySamplesPojo_plasMatAli").val()==null){
									
									  if($j("#plasMatAli").val()!=null && $j("#plasMatAli").val()!=""){
										return true;}}}
							},},
						"entitySamplesPojo.extDnaMat":{required :{
							depends: function(element){
								if($j("#cri_entitySamplesPojo_extDnaMat").val()=="" || $j("#cri_entitySamplesPojo_extDnaMat").val()==null){ 
									  if($j("#extDnaMat").val()!=null && $j("#extDnaMat").val()!=""){
										return true;}}}
							},},
						"entitySamplesPojo.noMiscMat":{required :{
							depends: function(element){
								if($j("#cri_entitySamplesPojo_noMiscMat").val()=="" || $j("#cri_entitySamplesPojo_noMiscMat").val()==null){ 
									  if($j("#noMiscMat").val()!=null && $j("#noMiscMat").val()!=""){
										return true;}}}
							},}
			          },
			   messages:{
			        	  "cdrCbuPojo.fkCbbProcedure":{checkselect : "<s:text name="garuda.cbu.cordentry.cbbprocedure"/>"},
			        	  "entitySamplesPojo.filtPap":{required :"<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
			        	  "entitySamplesPojo.rbcPel":{required : "<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
			              "entitySamplesPojo.extDnaAli":{required : "<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
			              "entitySamplesPojo.noSerAli":{required : "<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
			              "entitySamplesPojo.noPlasAli":{required : "<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
			              "entitySamplesPojo.nonViaAli":{required : "<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
			              "entitySamplesPojo.viaCelAli":{required : "<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
			              "entitySamplesPojo.noSegAvail":{required : "<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
			              "entitySamplesPojo.cbuOthRepConFin":{required : "<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
			              "entitySamplesPojo.cbuRepAltCon":{required : "<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
			              "entitySamplesPojo.serMatAli":{required : "<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
			              "entitySamplesPojo.plasMatAli":{required : "<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
			              "entitySamplesPojo.extDnaMat":{required : "<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
			              "entitySamplesPojo.noMiscMat":{required : "<s:text name="garuda.cbu.cordentry.mandatoryField"/>"}, 
			          }
			   });
		});
		function submitProcessinfoForm(autoDeferSubmit){
			var noMandatory=true;
			 $j('#ProcedureModalForm .cbuProcMandatory').each(function(){
				 //$j(this).css("border", "");
				 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
					noMandatory=false;
					}
				  });
			url = 'submitCRIProcedure?noMandatory='+noMandatory+'&esignFlag=review&entityId=<s:property value="cdrCbuPojo.cordID"/>&cdrCbuPojoModify.fkCbbProcedure='+$j("#cbbProcessProc").val()+'&autoDifferFlag='+$j("#autoDifferFlag").val()+"&"+autoDeferSubmit+'&criChangeFlag='+true;
			var proceChange = divChanges('procChanges',$j('#bodyProcessing_Div').html());
			if($j('#cbuProcedureMain').valid()){
			if(proceChange){
				modalFormPageRequest('cbuProcedureMain',url,'finalreviewmodel',$j("#cordId").val(),$j("#orderId").val(),$j("#orderType").val(),'proc_update','proc_status');
				if($j('#viewcbutoppanel').length>0){
					
					updateStatic.proc();
				}
				}
				else{
				
				showSuccessdiv('proc_update','bodyProcessing_Div');
				}
			}
			}
		function modalFormPageRequest(formid,url,divname,cordid,orderid,ordertype,updateDivId,statusDivId){
			showprogressMgs();
			$j.ajax({
		        type: "POST",
		        url: url,
		        //async:false,
		        data : $j("#"+formid).serialize(),
		        success: function (result){
		        	$j('.ui-datepicker').html("");
		        	//$('.tabledisplay').html("");
		            var $response=$j(result);
		            //query the jq object for the values
		            var errorpage = $response.find('#errorForm').html();
		           // alert(oneval);
		           // var subval = $response.find('#sub').text();
		            if(errorpage != null &&  errorpage != "" ){
		            	$j('#main').html(result);
		            }else{
		            	if(divname!=""){ 
			            	 $j("#"+divname).html(result);
			            }
		            	if(updateDivId!=undefined && updateDivId!=""){
		            		$j("#"+updateDivId).css('display','block');			            	
		            	}else{
		            		$j("#update").css('display','block');			            	
		            	}
		            	if(statusDivId!=undefined && statusDivId!=""){
		            		$j("#"+statusDivId).css('display','none');			            	
		            	}else{
		            		$j("#status").css('display','none');			            	
		            	}
		            	/*if(orderid!=null && orderid!="" && typeof(orderid)!="undefined" && ordertype!=null && ordertype!="" && typeof(ordertype)!="undefined"){
		                	var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID='+cordid+'&orderId='+orderid+'&orderType='+ordertype+'&pkcordId='+cordid+'&currentReqFlag=true';
		            		loadMoredivs(urlb,'viewcbutoppanel,quicklinksDiv,taskCompleteFlagsDiv,ctReqClinInfoContentDiv,finalreviewcontentDiv,currentRequestProgressMainDiv','cbuProcedureMain','update','status');//url,divname,formId,updateDiv,statusDiv
		            		setpfprogressbar();
		            		getprogressbarcolor();
		                }
		            	else{
		            	var urla = "loadClinicalData?cdrCbuPojo.cordID="+cordid;
		            	loadPageByGetRequset(urla,'cordSelectedData');
		            	}*/		            	
		            }
		        },
		        error: function (request, status, error) {
		        	closeprogressMsg();
		        	alert("Error " + error);
		            alert(request.responseText);
		        }

			});	
			closeprogressMsg();
			}

		function getProcedureInfo(procedureId){
			$j("#cbbprocedurerefresh").show();
			loadPageByGetRequset('refreshCBBProcedures?esignFlag=review&cbbProcedures.pkProcId='+procedureId,'cbbprocedurerefresh');
		}

		function getProcedureInfo(procedureId,id,autoDeferSubmit){
			var cordId =<s:property value ="cdrCbuPojo.cordID"/>;
			url = 'submitCRIProcedure?noMandatory='+noMandatory+'&esignFlag=review&entityId=<s:property value="cdrCbuPojo.cordID"/>&cdrCbuPojoModify.fkCbbProcedure='+$j("#cbbProcessProc").val()+'&autoDifferFlag='+$j("#autoDifferFlag").val()+"&"+autoDeferSubmit+'&criChangeFlag='+true;
			var noMandatory=true;
			var currProc = $j("#currProc").val();


			 $j('#ProcedureModalForm .cbuProcMandatory').each(function(){
				 //$j(this).css("border", "");
				 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
					noMandatory=false;
					}
				  });
			$j.ajax({
			        type: "POST",
			        url: 'deferCheckProcedure?procPk='+procedureId,
			        async:false,
			        success: function (result){

				  var collectionDate=new Date($j("#collDate").val());
				  var procStartDt = new Date(result.procStartDateStrAdd);			 
				  var prcgStDate = new Date($j("#prcgDate").val());	
				  var procTermiDt = new Date(result.procTermiDateStrAdd);

				  if(collectionDate < procStartDt) {
					  alert("<s:text name="garuda.cbu.cordentry.prcdrDateComp"/>");
					  $j("#"+id).val(currProc);	
					  $j("#"+id).removeClass('procChanges');
					  return;
				  }

				  if (prcgStDate < procStartDt ||  prcgStDate > procTermiDt){
						 alert("<s:text name="garuda.cbu.cordentry.prcssngDateComp"/>");
						 $j("#"+id).val(currProc);
						 $j("#"+id).removeClass('procChanges');
						 return;
				  }
			     if(procedureId != -1 &&  procedureId != '') {
				  if(result.checkDefer== "true"){
					  jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
								function(r) {
									if (r == false) {
										 $j("#"+id).removeClass('procChanges');
										$j("#"+id).focus();
										 $j("#"+id).val(currProc);
										
									  }else if(r == true){
										  
										  autoDefer = true;
										  url +="&submitButtonAutoDefer="+true;
										  modalFormPageRequest('cbuProcedureMain',url,'finalreviewmodel',$j("#cordId").val(),$j("#orderId").val(),$j("#orderType").val(),'proc_update','proc_status');									  }
									
					  });
				  }
				  else{
						  jConfirm("<s:text name="garuda.common.cbu.procProcedureAlert"/>", '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
							    if(r==true){
							    	loadPageByGetRequset('refreshInternalCBBProcedures?cbbProcedures.pkProcId='+procedureId,'refCbbProcOnly'); 
								}else{
									$j("#"+id).focus();
									 $j("#"+id).val(currProc);
									 $j("#"+id).removeClass('procChanges');
								}			    
						  });
					  }
			     }
			        else {
			        	$j("#"+id).focus();
						 $j("#"+id).val("");
				
				        }    
			  }
				});
		  }
		
		$j(function(){
			checkProcHaveGap();
			var val = $j('#cbbProcessProc').val();
			if(val!=null && val=="-1"){
				$j('#cbbprocedurerefresh').hide();
			}
			});
		function checkProcHaveGap(){
			var procidd = $j('#cbbProcessProc').val();
			if(procidd=='-1' && '<s:property value="esignFlag"/>' == 'modal'){
				$j('#cbbProcessProc').css("border", "1px solid red");	
			}
		}
		</script>
		</div>	 
  	