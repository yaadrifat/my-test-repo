<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script>

	/*function getProcedureInfo(procedureId){
		  $j("#cbbprocedurerefresh").show();
			if('<s:property value="esignFlag"/>'=="review"){
				loadPageByGetRequset('refreshCBBProcedures?esignFlag=review&cbbProcedures.pkProcId='+procedureId,'cbbprocedurerefresh');
				}		  
			else
			jConfirm("<s:text name="garuda.common.cbu.procProcedureAlert"/>", '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
			    if(r==true){
			    	loadPageByGetRequset('refreshCBBProcedures?cbbProcedures.pkProcId='+procedureId,'cbbprocedurerefresh'); 
				}else{
                   $j("#selfkCbbProcedure").val(onFocusProcVal);    
				}			    
		  });			
		  //loadPageByGetRequset('refreshCBBProcedures?cbbProcedures.pkProcId='+procedureId,'cbbprocedurerefresh');  
	}*/
	 var onFocusProcVal = -1;
	 function setProcedurePrevVal(value){
		 onFocusProcVal = value;
	 }
	function getProcedureInfo(procedureId,id){
		var cordId =<s:property value ="cdrCbuPojo.cordID"/>;
		var currProc = $j("#currProc").val();
		  $j.ajax({
		        type: "POST",
		        url: 'deferCheckProcedure?procPk='+procedureId,
		        async:false,
		        success: function (result){

				var collectionDate=new Date($j("#collDt").val());
				var prcgStDate=new Date($j("#prcgStDt").val());
				var frzDt=new Date($j("#frzDt").val());
				var procStartDt = new Date(result.procStartDateStrAdd);
				var procTermiDt = new Date(result.procTermiDateStrAdd);

				//alert("procStartDt::"+procStartDt);
				//alert("procTermiDt::"+procTermiDt);
				//alert("prcgStDate:::"+prcgStDate);
				if(collectionDate < procStartDt) {
					  alert("<s:text name="garuda.cbu.cordentry.prcdrDateComp"/>");
					  $j("#selfkCbbProcedure").val(currProc);
					  $j("#"+id).removeClass('procChanges');
					  return;
				}

				if (prcgStDate < procStartDt ||  prcgStDate > procTermiDt){
					 alert("<s:text name="garuda.cbu.cordentry.prcssngDateComp"/>");
					 $j("#selfkCbbProcedure").val(currProc);
					 $j("#"+id).removeClass('procChanges');
					 return;
				}

				if (frzDt < procStartDt ||  frzDt > procTermiDt){
					 alert("<s:text name="garuda.cbu.cordentry.freezedtComp"/>");
					 $j("#selfkCbbProcedure").val(currProc);
					 $j("#"+id).removeClass('procChanges');
					 return;
			    }
				  
		     if(procedureId != -1 && procedureId!='') {
			  if(result.checkDefer== "true"){
				  jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
							function(r) {
								if (r == false) {
									$j("#selfkCbbProcedure").focus();
									$j("#selfkCbbProcedure").val(currProc);
									$j("#"+id).removeClass('procChanges');
									
								  }else if(r == true){
									  autoDefer = true;
										modalFormSubmitRefreshDiv('cbuProcedureMain','submitProcedure','');
										refreshMultipleDivsOnViewClinical('32','<s:property value="cdrCbuPojo.cordID"/>');
										refreshMultipleDivsOnViewClinical('1024','<s:property value="cdrCbuPojo.cordID"/>');

										/*function submitProcessInfoEdit(cordId){
											modalFormSubmitRefreshDiv('cbuProcedureMain','submitProcedure','');
											refreshMultipleDivsOnViewClinical('32','<s:property value="cdrCbuPojo.cordID"/>');
										}*/



									      //  var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
									 //	var value = field.GetHTML(true);
									  //	$j('#sectionContentsta').val(value);
									  //  $j("#cordSearchable").val("1");	  
									    //loadDivWithFormSubmitDefer('saveCordEntry','maincontainerdiv','cordentryform1');	
										/*	
										 if (	
													  ($j("#filtPap").val()==null) || ($j("#filtPap").val()=="") &&
													 ($j("#rbcPel").val()==null) || ($j("#rbcPel").val()=="") &&
													 ($j("#extDnaAli").val()==null) || ($j("#extDnaAli").val()=="") &&
													 ($j("#noSerAli").val()==null) || ($j("#noSerAli").val()=="") ||
													 ($j("#noPlasAli").val()==null) || ($j("#noPlasAli").val()=="") &&
													 ($j("#nonViaAli").val()==null) || ($j("#nonViaAli").val()=="") &&
													 ($j("#viaCelAli").val()==null) || ($j("#viaCelAli").val()=="") &&
													 ($j("#noSegAvail").val()==null) || ($j("#noSegAvail").val()=="") &&
													 ($j("#cbuOthRepConFin").val()==null) || ($j("#cbuOthRepConFin").val()=="") &&
													 ($j("#cbuRepAltCon").val()==null) || ($j("#cbuRepAltCon").val()=="") &&
													 ($j("#celMatAli").val()==null) || ($j("#celMatAli").val()=="") &&
													 ($j("#plasMatAli").val()==null) || ($j("#plasMatAli").val()=="") &&
													 ($j("#extDnaMat").val()==null) || ($j("#extDnaMat").val()=="") &&
													 ($j("#noMiscMat").val()==null) || ($j("#noMiscMat").val()=="") ){
											 
									      		loadPageByGetRequset('refreshCBBProcedures?cbbProcedures.pkProcId='+procedureId,'cbbprocedurerefresh');  
											  }else{
										  jConfirm("<s:text name="garuda.common.cbu.procProcedureAlert"/>", '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
											    if(r==true){
											    	loadPageByGetRequset('refreshInternalCBBProcedures?cbbProcedures.pkProcId='+procedureId,'refCbbProcOnly'); 
												}else{
													$j("#selfkCbbProcedure").val(onFocusProcVal);    
												}			    
										  });
									  } */
								  }
								});
			  }
			  else{
				/*  if (	
						  ($j("#filtPap").val()==null) || ($j("#filtPap").val()=="") &&
						 ($j("#rbcPel").val()==null) || ($j("#rbcPel").val()=="") &&
						 ($j("#extDnaAli").val()==null) || ($j("#extDnaAli").val()=="") &&
						 ($j("#noSerAli").val()==null) || ($j("#noSerAli").val()=="") ||
						 ($j("#noPlasAli").val()==null) || ($j("#noPlasAli").val()=="") &&
						 ($j("#nonViaAli").val()==null) || ($j("#nonViaAli").val()=="") &&
						 ($j("#viaCelAli").val()==null) || ($j("#viaCelAli").val()=="") &&
						 ($j("#noSegAvail").val()==null) || ($j("#noSegAvail").val()=="") &&
						 ($j("#cbuOthRepConFin").val()==null) || ($j("#cbuOthRepConFin").val()=="") &&
						 ($j("#cbuRepAltCon").val()==null) || ($j("#cbuRepAltCon").val()=="") &&
						 ($j("#celMatAli").val()==null) || ($j("#celMatAli").val()=="") &&
						 ($j("#plasMatAli").val()==null) || ($j("#plasMatAli").val()=="") &&
						 ($j("#extDnaMat").val()==null) || ($j("#extDnaMat").val()=="") &&
						 ($j("#noMiscMat").val()==null) || ($j("#noMiscMat").val()=="") ){				 
				      loadPageByGetRequset('refreshCBBProcedures?cbbProcedures.pkProcId='+procedureId,'cbbprocedurerefresh');  
				  }else{*/
					  jConfirm("<s:text name="garuda.common.cbu.procProcedureAlert"/>", '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
						    if(r==true){
						    	loadPageByGetRequset('refreshInternalCBBProcedures?cbbProcedures.pkProcId='+procedureId,'refCbbProcOnly'); 
							}else{
								$j("#"+id).removeClass('procChanges');
								$j("#selfkCbbProcedure").val(currProc);
								$j("#selfkCbbProcedure").focus();
							}			    
					  });
				 /* }*/
			  }
		     }
		        else {
		        	$j("#selfkCbbProcedure").val(-1);
		        	$j("#selfkCbbProcedure").focus();
			
			        }    
		  }
			});
	  }

	
	$j(function() {
		var validator = $j("#cbuProcedureMain").validate({			
			rules:{	
			        "cdrCbuPojoModify.fkCbbProcedure":{checkselect : true}
		          },
		   messages:{
		        	  "cdrCbuPojoModify.fkCbbProcedure":{checkselect : "<s:text name="garuda.cbu.cordentry.cbbprocedure"/>"}
		          }
		   });
		
	});
	function submitProcessinfoForm(){
		var noMandatory=true;
		if('<s:property value="cdrCbuPojoModify.fkCbbProcedure"/>'=="-1"){
			 noMandatory=false;
			}
		url = 'submitProcedure?noMandatory='+noMandatory;
		var procreviewchanges = divChanges('procChanges',$j('#proce_bodyDiv').html());
		if(procreviewchanges){
		modalFormSubmitRefreshDiv('cbuProcedureMain',url,'finalreviewmodel');
		}
		else{
			showSuccessdiv('update','proce_bodyDiv');
		}
		}
	function submitProcessInfoEdit(cordId,autoDeferSubmit){
		var procclinchanges = divChanges('procChanges',$j('#proce_bodyDiv').html());
		if(procclinchanges){
		modalFormSubmitRefreshDiv('cbuProcedureMain','submitProcedure?'+autoDeferSubmit,'','','','1','<s:property value="cdrCbuPojo.cordID"/>');
		refreshMultipleDivsOnViewClinical('32','<s:property value="cdrCbuPojo.cordID"/>');
		refreshMultipleDivsOnViewClinical('1024','<s:property value="cdrCbuPojo.cordID"/>');
		if($j('#fcrFlag').val() == "Y"){
			refreshMultipleDivsOnViewClinical('2','<s:property value="cdrCbuPojo.cordID"/>');
			}
		$j('#fromViewClinicalproc').val('1');
		}
		else{
			showSuccessdiv('update','proce_bodyDiv');
		}
	}
	
	
	$j(function(){
		
		  $j('[name="cordCbuStatus"]').val($j('#cordCbuStatus').val());
		  $j('#executeAutoDeferFlagID').val('labSumaryAutoDeferExecFlag');
		
		
		
	});
	
</script>
<s:form id="cbuProcedureMain">

 <s:hidden name="cordCbuStatus"></s:hidden>
 <s:hidden name="autoDeferExecFlag" id="labSumaryAutoDeferExecFlag"/>

<s:hidden name="licenceUpdate" value="True"></s:hidden>
<s:hidden name="fromViewClinicalproc" id="fromViewClinicalproc"></s:hidden>
<s:hidden name="cdrCbuPojo.cordID" value="%{cdrCbuPojoModify.cordID}"/>
<s:hidden name="cdrCbuPojo.specimen.specCollDate" id="collDt" value="%{getText('collection.date',{cdrCbuPojo.specimen.specCollDate})}"/>
<s:hidden name="cdrCbuPojo.processingDate" id="prcgStDt" value="%{getText('collection.date',{cdrCbuPojo.processingDate})}"/>
<s:hidden name="cdrCbuPojo.frzDate" id="frzDt" value="%{getText('collection.date',{cdrCbuPojo.frzDate})}"/>
<s:hidden value="%{cdrCbuPojoModify.fkCbbProcedure}"  id="currProc"></s:hidden>


   <div id="update" style="display: none;" >  
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="CBB Procedures are updated successfully" name="message"/>
		  </jsp:include>
   </div>
   <div id="proce_bodyDiv">
   <table width="100%" cellpadding="0" cellspacing="0" border="0" id="status">
      <tr>
          <td colspan="3">
              <fieldset>
					     <legend><s:text name="garuda.unitreport.label.processinginfo.subheading" /></legend>
					          <table>
									<tr>
										<td ><s:text name="garuda.cbbprocedures.label.procname" />:</td>
										<td >
												<s:select name="cdrCbuPojoModify.fkCbbProcedure" id="selfkCbbProcedure" listKey="pkProcId" listValue="procName" list="#request.cbbProceduresList" onchange="getProcedureInfo(this.value,this.id); isChangeDone(this,'%{cdrCbuPojoModify.fkCbbProcedure}','procChanges');"  onfocus="setProcedurePrevVal(this.value)" headerKey="-1" headerValue="Select"></s:select>
										</td>
										<s:if test="cdrCbuPojoModify.fkCbbProcedure != null && cdrCbuPojoModify.fkCbbProcedure != -1">
												<td>
													<s:if test="esignFlag != 'review'"> <a href="javascript:void(0);" onclick="showModal('View CBB Procedure','refreshCBBProcedures?cbbProcedures.pkProcId=<s:property value='cdrCbuPojoModify.fkCbbProcedure'/>','500','850');"><s:text name="garuda.common.lable.view"></s:text> </a></s:if>
												</td><td colspan="3"></td>
										 </s:if>
										<s:else>
												<td colspan="4"></td>
										</s:else>									     
									</tr>
									<tr>
										<td colspan="6">
											<div id="cbbprocedurerefresh" style="display: none;">
												<jsp:include page="../cord-entry/cb_procedure_refresh.jsp"></jsp:include>
											</div>
										</td>
									</tr>
								</table>
			</fieldset>
          </td>
      </tr>      
	   <tr bgcolor="#cccccc">
		   <td colspan="2" align="right"><jsp:include page="../cb_esignature.jsp" flush="true">
		   		<jsp:param value="cbuProcesubmit" name="submitId"/>
				<jsp:param value="cbuProceinvalid" name="invalid" />
				<jsp:param value="cbuProceminimum" name="minimum" />
				<jsp:param value="cbuProcepass" name="pass" />
		   </jsp:include></td>
		   <td colspan="2" align="left">
		   <s:if test="esignFlag == 'review'">
		           	<input type="button" name="procedureEdit" disabled="disabled" id="cbuProcesubmit" value="<s:text name="garuda.unitreport.label.button.submit"/>" onclick="submitProcessinfoForm();">
		   </s:if>
		   <s:else>
		           	<input type="button" name="procedureEdit" disabled="disabled" id="cbuProcesubmit" value="<s:text name="garuda.unitreport.label.button.submit"/>" onclick="submitProcessInfoEdit('<s:property value="cdrCbuPojo.cordID"/>','submitButtonAutoDefer=false');">
		   </s:else>        	
		          	<input type="button" onclick="closeModal();" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />
		   </td>  
	   </tr>   
   </table>   
   </div>
</s:form>