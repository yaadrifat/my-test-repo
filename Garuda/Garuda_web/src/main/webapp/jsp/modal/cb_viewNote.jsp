<%@ taglib prefix="s"  uri="/struts-tags"%>

<jsp:include page="../cb_track_session_logging.jsp" />
<script>
$j(function(){
	var flagForLater = $j('#flagForLater:checked').val();
if(flagForLater=='true')
{
$j("#viewComment").show();
$j("#viewCommentText").show();
}
else
{
	$j("#viewComment").hide();
	$j("#viewCommentText").hide();
}
});
</script>


<div id="clinicalNoteViewDiv" class='popupdialog tabledisplay ' >
<s:form id="clinicalView" name="clinicalView">
<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
<s:hidden name="clinicalNotePojo.fkNotesType"/>
<s:hidden name ="clinicalNotePojo.keyword" id="keyword1"/>
<s:hidden name="clinicalNotePojo.amended"/>
  <table width="100%" cellspacing="0" cellpadding="0" border="1" id="status">
  <tr><td><span style="color: red;">
  <s:if test="clinicalNotePojo.amended==true">
  <s:text name="garuda.clinicalnote.label.attention_msg"/>
  </s:if></span></td></tr>
   <tr>
   <td >
	
				<s:text name="garuda.common.message.note" />
				
	
	</td>
	</tr>
	<tr>
   <td>  
   <div style="overflow: auto;">
    <s:property escapeHtml="false" value="%{clinicalNotePojo.notes}" />
    </div>
    </td>
   </tr>
   <tr>
  
  <td  valign="middle">
  <fieldset style="display:inline-block"  ><legend> <s:text  name="garuda.clinicalnote.label.assessment"></s:text>
				</legend>
		<table>
			<tr>
				<td valign="middle" ><s:radio name="clinicalNotePojo.noteAssessment" id="assess" disabled = "true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_ASSESS]" listKey="subType" listValue="description" id="noteAssessment" onclick="javascript:showDiv(this.value);"  />
				</td>	
				<td>
								<s:checkbox name="clinicalNotePojo.flagForLater" id="flagForLater" disabled = "true" value="clinicalNotePojo.flagForLater"  ></s:checkbox><s:text
								name="garuda.clinicalnote.label.flagForLater"></s:text>
							</td>	
							
				<td id = "viewCommentText" style="display:none">
						<s:text name ="garuda.clinicalnote.label.comment"></s:text>
				</td>
				<td id = "viewComment" style="display:none">
						<s:textfield name="clinicalNotePojo.comments" disabled = "true" id="comment" />
				</td>
				</tr>
				<tr>				
				
							
				
								
		</tr>
							
	</table>
</fieldset>
						
   </td>
   </tr>
   <tr>
   <td>
   <table cellspacing="0" class="displaycdr">
   <tr>
   
    <td >
					<s:checkbox disabled = "true" name="clinicalNotePojo.visibility" id="checkbox" value="clinicalNotePojo.visibility"></s:checkbox><s:text
					name="garuda.clinicalnote.label.visibleToTc"></s:text>
	</td>
	<td >
				<s:text name="garuda.clinicalnote.label.keyword" />
				
	</td>
	<td>
		<s:textfield disabled = "true" name="clinicalNotePojo.keyword" id="keyword7" style="width: 200px"  />
	</td>
	
    <td ><s:text name="garuda.clinicalnote.label.category" /></td>
    <td>
  		<s:select name="clinicalNotePojo.fkNotesCategory" style="width:150px"	list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CLINIC_CATEGORY]"
		listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select" disabled="true" />
   </td>
   
  <%--  <td >
				<s:text name="garuda.clinicalnote.label.button.send_to" />	
				
	</td>
	<td>
		<s:textfield disabled = "true" name="clinicalNotePojo.sentTo" id="send_to" style="width: 200px" />
	</td> --%>
	</tr>
	</table>
	</td>
   </tr>
 
   

  </table>

  </s:form>

</div>