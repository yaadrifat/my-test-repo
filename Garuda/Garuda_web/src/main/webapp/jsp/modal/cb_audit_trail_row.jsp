<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script>
      $j(function(){
              $j("#audittrail").dataTable();
       });
</script>
<div class="col_100 maincontainer ">
   <div class="col_100">
	 <div class="column">
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="audittrail" class="displaycdr">
	        <thead>
	           <tr>
	              <th><s:text name="garuda.cbuentry.label.registrycbuid"/></th>
	              <th><s:text name="garuda.cbuentry.label.localcbuid"/></th>
	              <th><s:text name="garuda.cbuentry.label.maternalregistryid"/></th>
	              <th><s:text name="garuda.cbuentry.label.maternallocalid"/></th>
	              <th><s:text name="garuda.cbuentry.label.cdrcbucreon"/></th>
	              <th><s:text name="garuda.cbuentry.label.cdrcbucreby"/></th>
	              <th><s:text name="garuda.cbbdefaults.lable.lastmodifiedby"/></th>
	              <th><s:text name="garuda.applMessge.label.message.lastModOn"/></th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailRowData" var="row" status="status">
	                <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property value="getText('{0,date,MMM dd,yyyy}',{#row[5]})"  />
	                    </td>
	                    <td>
	                        <s:property value="%{#row[6]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[7]}"/>
	                    </td>
	                    <td>
	                        <s:property value="getText('{0,date,MMM dd,yyyy}',{#row[8]})"  />
	                    </td>	                   
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	            <tr>
	               <td colspan="9"></td>
	            </tr>
	        </tfoot>
	     </table>	
	 </div>
   </div>
</div>