<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.eres.web.site.SiteJB" %>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@ page import="java.util.List,com.velos.eres.service.util.*"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<jsp:include page="../cb_track_session_logging.jsp" />
<%
UserJB userB = (UserJB)session.getAttribute("currentUser");
request.setAttribute("userid",Integer.parseInt(userB.getUserSiteId()));	
%>
<%
	HttpSession tSession = request.getSession(true); 
	GrpRightsJB grpRights =null;
	if (tSession.getAttribute("LocalGRights")!=null){
	 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
	}else
	{
	 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	}
	
	int updateIds=0;
	if(grpRights.getFtrRightsByValue("CB_IDW")!=null && !grpRights.getFtrRightsByValue("CB_IDW").equals(""))
	{updateIds = Integer.parseInt(grpRights.getFtrRightsByValue("CB_IDW")); }
	else
		updateIds = 4;
	request.setAttribute("updateIds",updateIds);
	
%>
<style>
#cbuIdInfo td{
padding-left: 5px;
}
</style>
<script>
   var additionalIdCount = 0;   
   var maxadditionalidcount=0;
   var additionalIdDes = Array();
   var additionalTypeId = Array();
   var additionalId = Array();
   var changesDone = false;
   jQuery.fn.extend({
	    initVal: function () { 
	     var returnVal;
		 var elementName=$j(this).attr('name');		    
		     try{
	    	   returnVal=document.getElementsByName(elementName)[0].defaultValue;
		    }catch(e){
		     window.console.warn('Some Exception Occur');
		     window.console.error(e);
		    }		     
	        return returnVal;
	    }
	});
	
$j(function() {
	/*var today = new Date();
	
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();

	var h=today.getHours();
	var mn=today.getMinutes()+1;

	
	$j( "#datepicker2" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker3" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
*/
getDatePic();
	jQuery.validator.addMethod("checkdbdata01", function(value, element, params) {
		 if($j("#"+params[2]).val()==value){
            return true;
		 }else{
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
		 return (true && (validateDbData(url)));
		 }
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/	

	jQuery.validator.addMethod("checkdbdata11", function(value, element, params) {
		if($j("#"+params[2]).val()==value){
            return true;
		 }else{
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
		 return (true && (validateDbData(url)));
		 }
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/	

	jQuery.validator.addMethod("checkcbbdbdata11", function(value, element, params) {
		if($j("#"+params[2]).val()==value){
            return true;
		 }else{
			    var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[3]+"&entityValue1="+$j("#fkCbbId").val();
		        if(true && (validateDbAdditionalData(url))){
			       return true; 
			   }
		        else{
			        var url1 = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
					if(true && (validateDbAdditionalData(url1))){
					    return false;
					}
					else{  return true;
					}			   
			   }
		 }
	}, "");/*}, "Please Enter Valid Data");*****/ 

	jQuery.validator.addMethod("checkisbtdata1", function(value, element,params) {
		if($j("#"+params[1]).val()==value){
            return true;
		 }else{
		 var url = "getCodeValidate?refnum="+params[0]+"&"+$j.param({'codeval':value});
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}		 
		 }
	}, "<%=MC.M_PlsEtr_IsbtData%>");/*}, "Please Enter Valid ISBT Data");*****/

	jQuery.validator.addMethod("checkdbadditionaldata1", function(value, element, params) {
		if($j("#"+params[3]).val()==value){
            return true;
		 }else{
			 if($j(element).attr("id")=='matlocalid'){
		           value=value.replace(/&/g,"%26");}
			 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField2="+params[2]+"&entityValue2="+$j("#regMaternalId").val()+"&entityField1="+params[4]+"&entityValue1="+$j("#fkCbbId").val();
			 var url1 = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[4]+"&entityValue1="+$j("#fkCbbId").val();
			 if(value==""){return true;}else{
				if(validateDbAdditionalData(url)){
					return true;
				}else{				
					return validateDbData(url1);
				} 		  
			 }		
		 }
	}, "");/*}, "Please Enter Valid Data");*****/	

	jQuery.validator.addMethod("checklocalcbuiddbdata1", function(value, element, params) {
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}	
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/

	jQuery.validator.addMethod("checklocalcbuidadditionaldbdata01", function(value, element, params) {
		if($j("#"+params[3]).val()==value){
            return true;
		 }else{
			 if($j(element).attr("id")=='loccbuid'){
		           value=value.replace(/&/g,"%26");}
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[2]+"&entityValue1="+$j("#fkCbbId").val();
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}	
		 }
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/	

	jQuery.validator.addMethod("checklocalcbuidadditionaldbdata11", function(value, element, params) {
		if($j("#"+params[3]).val()==value){
            return true;
		 }else{
			 if($j(element).attr("id")=='loccbuid' || $j(element).attr("id")=='matlocalid'){
		           value=value.replace(/&/g,"%26");}
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[2]+"&entityValue1="+$j("#fkCbbId").val();
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}	
		 }
	}, "<%=MC.M_Etr_ValidData%>");/*}, "Please Enter Valid Data");*****/	

	jQuery.validator.addMethod("checkisbtdbdata1", function(value, element,params) {
		if($j("#"+params[2]).val()==value){
            return true;
		 }else{
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&"+$j.param({'entityValue':value})+"&entityField1=fkCbbId&entityValue1="+$j("#fkCbbId").val();
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}		
		 } 
	}, "<%=MC.M_PlsEtr_IsbtData%>");/*}, "Please Enter Valid ISBT Data");*****/

	jQuery.validator.addMethod("validateValues1", function(value, element,params) {
		if($j("#"+params[2]).val()==value){
            return true;
		 }else if($j("#"+params[0]).val()=="" && $j("#"+params[1]).val()==""){
			 return true;
		 }
		 else{
			return (true && (validateValues(params[0],params[1])));
		 }		 
	}, "<%=MC.M_PlsEtr_IsbtData%>");/*}, "Please Enter Valid ISBT Data");*****/

	jQuery.validator.addMethod("checkformat1", function(value, element,params) {
		 if($j("#"+params[0]).val()==value){
            return true;
		 }else{ 
			return (true && (checkFormat(value)));	
		 }	 
	}, "<%=MC.M_EtrVal_XxxxFrmt%>");/*}, "Please Enter Value in xxxx-xxxx-x format");*****/	

	jQuery.validator.addMethod("checksum1", function(value, element,params) {
		 if($j("#"+params[0]).val()==value){
            return true;
		 }else{
			return (true && (CheckSum(value)));
		 }
	}, "<%=MC.M_PlsEtr_ValidVal%>");/*}, "Please Enter Valid Value");*****/	

	jQuery.validator.addMethod("checkregcbbdbdata", function(value, element, params) {
		 if($j("#"+params[2]).val()==value){
          return true;
		 }else{		
			 if(nonSystemCordStatus() && $j(element).attr("id")=='cburegid' || $j(element).attr("id")=='regMaternalId'){
		              value=value.replace(/&/g,"%26");}	
			 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[3]+"&entityValue1="+$j("#fkCbbId").val();
		 return (true && (validateDbData(url)));
		 }
	}, "");/*}, "Please Enter Valid Data");*****/
	
	jQuery.validator.addMethod("checkdbmatregcbbdbdata", function(value, element, params) {
		 if($j("#"+params[2]).val()==value){
         return true;
		 }else{
			 if(nonSystemCordStatus() && $j(element).attr("id")=='cburegid' || $j(element).attr("id")=='regMaternalId'){
	              value=value.replace(/&/g,"%26");}
			 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[3]+"&entityValue1="+$j("#fkCbbId").val();
		 return (true && (validateDbData(url)));
		 }
	}, "");/*}, "Please Enter Valid Data");*****/

	var validator = $j("#cbuIdInfo").validate({
		
		rules:{	
				"cdrCbuPojoModify.registryId":{required:true,checkspecialchar :{depends: function(element){ return !(nonSystemCordStatus());}},checklength:{depends: function(element){ return !(nonSystemCordStatus());}},checkformat1:{param:["cbucordregid"],depends:function(element){return !(nonSystemCordStatus());}},validateValues1:{param:["cburegid","regMaternalId","cbucordregid"],depends:function(element){return !(nonSystemCordStatus());}},checksum1:{param:["cbucordregid"],depends:function(element){return !(nonSystemCordStatus());}},checkdbdata11:{param:["CdrCbu","registryId","cbucordregid"],depends:function(element){return !(nonSystemCordStatus());}},checkdbdata01:{param:["CdrCbu","registryMaternalId","cbucordregid"],depends:function(element){return !(nonSystemCordStatus());}},checkregcbbdbdata:{param:["CdrCbu","registryId","cbucordregid","fkCbbId"],depends:function(element){return nonSystemCordStatus();}},checkdbmatregcbbdbdata:{param:["CdrCbu","registryMaternalId","cbucordregid","fkCbbId"],depends:function(element){return nonSystemCordStatus();}}},					
				"cdrCbuPojoModify.registryMaternalId":{required:{
					depends: function(element){
					                   if(($j(element).initVal()==$j(element).val())){
						                   return false;
					                   }
					                       return !(nonSystemCordStatus());
					 }
				   },checkspecialchar : {
					   depends: function(element){ 
					                   if(($j(element).initVal()==$j(element).val())){
                                          return false;
					                   }
					                      return !(nonSystemCordStatus());
					   }
				   },checklength:{
					   depends: function(element){
					                  if(($j(element).initVal()==$j(element).val())){
						                  return false;
					                  }
					                      return !(nonSystemCordStatus());
					    }
				    },checkformat1:{param:["regcordmatid"],depends:function(element){return !(nonSystemCordStatus());}},validateValues1:{param:["cburegid","regMaternalId","regcordmatid"],depends:function(element){return !(nonSystemCordStatus());}},checksum1:{param:["regcordmatid"],depends:function(element){return !(nonSystemCordStatus());}},checkcbbdbdata11:{param:["CdrCbu","registryMaternalId","regcordmatid","fkCbbId"],depends:function(element){return !(nonSystemCordStatus());}},checkdbdata01:{param:["CdrCbu","registryId","regcordmatid"],depends:function(element){return !(nonSystemCordStatus());}},checkregcbbdbdata:{param:["CdrCbu","registryId","regcordmatid","fkCbbId"],depends:function(element){return nonSystemCordStatus();}},checkdbmatregcbbdbdata:{param:["CdrCbu","registryMaternalId","regcordmatid","fkCbbId"],depends:function(element){return nonSystemCordStatus();}}},
				"cdrCbuPojoModify.localMaternalId":{validateValues1:["matlocalid","loccbuid","localcordmatid"],checkdbadditionaldata1:["CdrCbu","localMaternalId","registryMaternalId","localcordmatid"],checklocalcbuidadditionaldbdata11:["CdrCbu","localCbuId","fkCbbId","localcordmatid"]},
				"cbuOnBag":{checkselect:
					{
						depends: function(element){
					                        if($j('#numberoncbubag').val()==$j("#cbuOnBag :selected").text() || $j('#numberoncbubag').val()=="" && $j(element).val()==-1){
						                        return false;
					                        }
							                   return ($j("#cbuOnBag :selected").val()=="-1" && $j("#numberoncbubag").val()!=null);
						}
					}, },
					
				"cdrCbuPojoModify.cordIsbiDinCode":{required:
								{
									depends: function(element){
						                             if(($j(element).initVal()==$j(element).val())){
							                             return false;
						                             }
											            return ($j("#loccbuid").val()=="" && !(nonSystemCordStatus()));
										}
									},checkisbtdbdata1:["CdrCbu","cordIsbiDinCode","isbidincordcode"],checkisbtdata1:["001","isbidincordcode"]},
				"cdrCbuPojoModify.localCbuId":{required:
				{
										depends: function(element){
										                if(($j(element).initVal()==$j(element).val())){
											                  return false;
										                }
												              return !($j("#loccbuid").val()=="" && (nonSystemCordStatus()));
											}
										},validateValues1:["matlocalid","loccbuid","localcordcbuid"],checklocalcbuidadditionaldbdata01:["CdrCbu","localMaternalId","fkCbbId","localcordcbuid"],checklocalcbuidadditionaldbdata11:["CdrCbu","localCbuId","fkCbbId","localcordcbuid"]},
				"cdrCbuPojoModify.fkCBUDeliveryType":{checkselect : true},
				"cdrCbuPojoModify.fkCBUCollectionType":{checkselect : true},
				"cdrCbuPojoModify.fkMultipleBirth":{checkselect : true},	
				"cdrCbuPojoModify.cordIdsChangeReason":{required:true},			
				"birthDateStr":"required",
				"babyGenderId":{checkselect : true},
				"cdrCbuPojoModify.ethnicity":{checkselect : true},
				"cbuCollectionDateStr":"required",
				"tncFrozen":{number:true},
				"frozenPatWt":{number:true},
				"cellCount":{number:true}			
			},
			messages:{
				"cdrCbuPojoModify.registryId":{checkspecialchar : "<s:text name="garuda.common.validation.alphanumeric"/>",checklength:"<s:text name="garuda.common.validation.alphalength"/>",checkformat:"<s:text name="garuda.cbu.cordentry.checkformat"/>",validateValues1:"<s:text name="garuda.cbu.cordentry.cbumatsameregid"/>",checksum1:"<s:text name="garuda.common.validation.checksumrid"/>",checkdbdata11:"<s:text name="garuda.common.id.exist"/>",checkdbdata01:"<s:text name="garuda.common.id.exist"/>",checkregcbbdbdata:"<s:text name="garuda.cbu.cordentry.cburegid"/>",checkdbmatregcbbdbdata:"<s:text name="garuda.cbu.cordentry.cburegidasmatregid"/>"},
				"cdrCbuPojoModify.registryMaternalId":{checkspecialchar : "<s:text name="garuda.common.validation.alphanumeric"/>",checklength:"<s:text name="garuda.common.validation.alphalength"/>",checkformat:"<s:text name="garuda.cbu.cordentry.checkformat"/>",validateValues1:"<s:text name="garuda.cbu.cordentry.cbumatsameregid"/>",checksum1:"<s:text name="garuda.common.validation.checksummid"/>",checkcbbdbdata11:"<s:text name="garuda.cbu.cordentry.samematregid"/>",checkdbdata01:"<s:text name="garuda.cbu.cordentry.samematregidcbuid"/>",checkregcbbdbdata:"<s:text name="garuda.cbu.cordentry.matregid"/>",checkdbmatregcbbdbdata:"<s:text name="garuda.cbu.cordentry.matregidascburegid"/>"},
				"cdrCbuPojoModify.localMaternalId":{validateValues1:"<s:text name="garuda.cbu.cordentry.cbumatlocsamecbulocid"/>",checkdbadditionaldata1:"<s:text name="garuda.cbu.cordentry.cbumatlocexistdiffmatid"/>",checklocalcbuidadditionaldbdata11:"<s:text name="garuda.cbu.cordentry.locmatidexistascblocid"/>"},
				"cbuOnBag":{checkselect:"<s:text name="garuda.common.validation.value"/>"},
				"cdrCbuPojoModify.cordIsbiDinCode":{required:"<s:text name="garuda.cbu.cordentry.reqisbtdin"/>",checkisbtdbdata1:"<s:text name="garuda.common.id.exist"/>",checkisbtdata1:"<s:text name="garuda.cbu.cordentry.reqisbtdin"/>"},	
				//"cdrCbuPojo.cordDonorIdNumber":{checkisbtdata1:"Please Enter Valid Donor Identification Number"},	
				"cdrCbuPojoModify.localCbuId":{required:"<s:text name="garuda.cbu.cordentry.reqcbulocid"/>",validateValues1:"<s:text name="garuda.cbu.cordentry.cbulocsamematlocid"/>",checklocalcbuiddbdata1:"<s:text name="garuda.common.id.exist"/>",checklocalcbuidadditionaldbdata01:"<s:text name="garuda.cbu.cordentry.localCbuIdexistasMatId"/>",checklocalcbuidadditionaldbdata11:"<s:text name="garuda.cbu.cordentry.localCbuIdexist"/>"},
				"cdrCbuPojoModify.fkCBUDeliveryType":"<s:text name="garuda.cbu.cordentry.deliverytype"/>",
				"cdrCbuPojoModify.fkCBUCollectionType":"<s:text name="garuda.cbu.cordentry.collectiontype"/>",
				"cdrCbuPojoModify.fkMultipleBirth":"<s:text name="garuda.cbu.cordentry.MultipleBirth"/>",
				"cdrCbuPojoModify.cordIdsChangeReason":{required:"<s:text name="garuda.cbu.cordentry.reasonchangeId"/>"},
				"birthDateStr":"<s:text name="garuda.cbu.cordentry.birthdate"/>",
				"babyGenderId":"<s:text name="garuda.cbu.cordentry.babygenderid"/>",
				"cdrCbuPojoModify.ethnicity":"<s:text name="garuda.cbu.cordentry.birthethnicity"/>",
				"cbuCollectionDateStr":"<s:text name="garuda.cbu.cordentry.collectiondate"/>",
				"tncFrozen":"<s:text name="garuda.cbu.cordentry.checknumbers"/>",
				"frozenPatWt":"<s:text name="garuda.cbu.cordentry.checknumbers"/>",
				"cellCount":"<s:text name="garuda.cbu.cordentry.checknumbers"/>"						
			}
		});

	$j("#cburegid").keydown(function(e) {
		if($j('#non_system_cord').val()!="false"){
		  var val = $j("#cburegid").val();
		  if(e.keyCode!=8 && e.keyCode!=46 )
		  {
			  if(val.length==4 || val.length==9){
				  if(e.which!=109){
					   $j("#cburegid").val(val+"-");
				  }
			  }
		  }
		}
	});

	$j(function() {	
	  <s:property value="#request.additionalIds.size"/> > 0 ? maxadditionalidcount=additionalIdCount = <s:property value="#request.additionalIds.size"/> : additionalIdCount = 0;

		  for(var i=0; i< maxadditionalidcount ; i++)
	     { 		
		   additionalIdDes.push($j("#additionalIds"+i+"additionalIdDescid").val());
		   additionalTypeId.push($j("#additionalIds"+i+"additionalIdValueTypeid option:selected").text());
		   additionalId.push($j("#additionalIds"+i+"additionalId").val());
	     }
});
	
	$j("#regMaternalId").keydown(function(e) {
		if($j('#non_system_cord').val()!="false"){
		  var val = $j("#regMaternalId").val();
		  if(e.keyCode!=8 && e.keyCode!=46 )
		  {
			  if(val.length==4 || val.length==9){
				  if(e.which!=109){
				   $j("#regMaternalId").val(val+"-");
				  }
			  }
		  }
		}
	});	
	
});

jQuery(function() {
	  jQuery('#datepicker3, #datepicker2').datepicker('option', {
	    beforeShow: customRange
	  });
	});

	function customRange(input) {
	  /*if (input.id == 'datepicker3') {
	    return {
	      minDate: jQuery('#datepicker2').datepicker("getDate")
	    };
	  } else if (input.id == 'datepicker2') {
	    return {
	      maxDate: jQuery('#datepicker3').datepicker("getDate")
	    };
	  }*/
		 if (input.id == 'datepicker3') {
			 if($j("#datepicker2").val()!="" && $j("#datepicker2").val()!=null){
			    var minTestDate=new Date($j("#datepicker2").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
				  $j('#datepicker3').datepicker( "option", "minDate", new Date(y1,m1,d1));
			 }else{
				 $j('#datepicker3').datepicker( "option", "minDate", "");
			 }
		} else if (input.id == 'datepicker2') {
			if($j("#datepicker3").val()!="" && $j("#datepicker3").val()!=null){
				var minTestDate=new Date($j("#datepicker3").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
				$j('#datepicker2').datepicker( "option", "maxDate", new Date(y1,m1,d1));
			}else{
				$j('#datepicker2').datepicker( "option", "maxDate", "");
			}
		}
			  var result = $j.browser.msie ? !this.fixFocusIE : true;
			  this.fixFocusIE = false;
			  return result;
	}

    function addAdditionalId(){	            
		 if(maxadditionalidcount < 10){
			$j('#additionalidtable').each(function(){			
		       var n = 0;              
			       n = (additionalIdCount==0) ? 0  : additionalIdCount ; 
					   additionalIdCount++;
					   maxadditionalidcount++;			       
                 			  
		    	   var $table = $j(this);    	   
		    	   var tds = '';		    	  
		    	       tds += '<tr id="additionalid_row'+n+'"><td width="20%"><s:text name="garuda.cordentry.label.additionalIdDesc"></s:text>:</td><td><s:textfield cssClass="required" id="additionalIds'+n+'additionalIdDescid"  name="additionalIds['+n+'].additionalIdDesc" maxlength="30" onchange="additonalIdscall('+n+')" ></s:textfield></td>'
		        	   tds +='<td><s:text name="garuda.cordentry.label.additionalTypeId"></s:text>:</td><td><select id="additionalIds'+n+'additionalIdValueTypeid" name="additionalIds['+n+'].additionalIdValueType" class="required" onchange="additonalIdscall('+n+')" ><option value="">Select</option><option value="cbu">CBU</option><option value="maternal">Maternal</option></select></td>';
		       	       tds += '<td><s:text name="garuda.cordentry.label.additionalId"></s:text>:</td><td><s:textfield cssClass="required" id="additionalIds'+n+'additionalId" name="additionalIds['+n+'].additionalId" maxlength="30"  onchange="additonalIdscall('+n+')"></s:textfield></td><td><img style="cursor: pointer;vertical-align:middle;"  src="images/cross.png" border="0"  onclick="javascript:deleteRow('+n+');" /></td></tr>';
	          	       $j(this).append(tds);   
		           	if(maxadditionalidcount==10){
		   		      $j("#addbutton").css("cursor","default");
		   	        } 	   
		       });			   
		 }				 
				    else{
				      $j("#addbutton").css("cursor","default");
				  }
	 }	
	
	function deleteRow(rowid) {            	 
		$j('#additionalid_row'+rowid).remove();	
        maxadditionalidcount--;	
        if(maxadditionalidcount < 10){            			    
		   $j("#addbutton").css("cursor","pointer");
        }			
     /* removing the additional ID Unique Product identity on bag drop down.*/           	
		htmlUniqueIdOnBag();	 
    }	
   
	function uniqueIdOnBagVal(val){
		$j("#idonbagvalresultmod").val($j("#cbuOnBag option:selected").text());	
		isChangeDone($j("#idonbagvalresultmod"), '<s:property value="cdrCbuPojoModify.numberOnCbuBag"/>','IDsChanges');
	 }

	function htmlUniqueIdOnBag(val){
		    var paramObj={
		    		data: $j('#idonbagvalresultmod').val(),
		    		regId:$j('#cburegid').val(),
		    		isbt: $j('#isbidin1').val(),
		    		locId:$j('#loccbuid').val()
		    }
	     var  url = "getUniqIdOnBagHtml?"+$j.param(paramObj);
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
			data : $j("#cbuIdInfo").serialize(),
	        success: function (result, status, error){		        	
	        	     	$j("#idonbag").html(result.htmlStr);
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }
		});	 
	
	 if($j("#esignFlag").val()=="review")
		 {
		 $j("#cbuOnBag").addClass('idsMandatory');
		 if($j("#cbuOnBag").val()=="" || $j("#cbuOnBag").val()==null || $j("#cbuOnBag").val()=="-1")
			 {
			
			 $j("#cbuOnBag").css('border','1px solid red');
			 }
		 }
   }
	
function submitCRIforIds(){
	
	if('<s:property value="#request.additionalIds.size()"/>'!=maxadditionalidcount){
		changesDone = true;
	}else{
		changesDone = divChanges('IDsChanges',$j('#bodyDiv').html());
	}
	
	if(ckeckValidateStatus()){
		 $j("#cordidschangereason").rules("add", {
			    required: true,
			    messages: {
			        required: "<s:text name="garuda.cbu.cordentry.reasonchangeId"/>"
			    }
			});
	}	
	else{
		$j("#cordidschangereason").rules("remove", "required");
		$j("#cordidschangereason").rules("remove", "messages");
	}	
	var noMandatory=true;
	var localId = $j('#loccbuid').val();
	$j('#indDiv .idsMandatory').each(function(){
		 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
			 noMandatory=false;
			}
		});
	var url = 'submitIdInfo?cordIdStr=<s:property value="cdrCbuPojo.cordID" />&noMandatory='+noMandatory;	
	if($j('#cbuIdInfo').valid()){
		if(changesDone){
			url=url+'&criChangeFlag='+true;
			modalFormPageRequest('cbuIdInfo',url,'finalreviewmodel',$j("#cordId").val(),$j("#orderId").val(),$j("#orderType").val(),'','');
		}
		else{
			showSuccessdiv("update","status");
		}
	}

}

function modalFormPageRequest(formid,url,divname,cordid,orderid,ordertype,updateDivId,statusDivId){
	showprogressMgs();
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        data : $j("#"+formid).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	if(divname!=""){ 
	            	 $j("#"+divname).html(result);
	            }
            	if(updateDivId!=undefined && updateDivId!=""){
            		$j("#"+updateDivId).css('display','block');			            	
            	}else{
            		$j("#update").css('display','block');			            	
            	}
            	if(statusDivId!=undefined && statusDivId!=""){
            		$j("#"+statusDivId).css('display','none');			            	
            	}else{
            		$j("#status").css('display','none');			            	
            	}
            	/*if(orderid!=null && orderid!="" && typeof(orderid)!="undefined" && ordertype!=null && ordertype!="" && typeof(ordertype)!="undefined"){
                	var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID='+cordid+'&orderId='+orderid+'&orderType='+ordertype+'&pkcordId='+cordid+'&currentReqFlag=true';
            		loadMoredivs(urlb,'viewcbutoppanel,quicklinksDiv,taskCompleteFlagsDiv,ctReqClinInfoContentDiv,finalreviewcontentDiv,currentRequestProgressMainDiv','cbuIdInfo','update','status');//url,divname,formId,updateDiv,statusDiv
            		setpfprogressbar();
            		getprogressbarcolor();
                }
            	else{
            	var urla = "loadClinicalData?cdrCbuPojo.cordID="+cordid;
            	loadPageByGetRequset(urla,'cordSelectedData');
            	}	*/	            	
            }
        },
        error: function (request, status, error) {
        	closeprogressMsg();
        	alert("Error " + error);
            alert(request.responseText);
        }

	});	
	closeprogressMsg();
	/*setTimeout(function(){
		var divIdd="completeTd";
		var cbuRegID=$j('#cburegid').val();
		var upiob = $j('#idonbagvalresultmod').val();
		var upiob_data = '';
		if(upiob!="" && upiob != null && upiob != "undefined"){
			upiob_data = ', <s:text name="garuda.cbuentry.label.idbag"/>:'+upiob;
		}
		var divTitle='Complete Required Information';
		$j("#dialogForCRI").prev("div").find('span.ui-dialog-title').html('<div id="notesPopup"><div style="float:left"><s:text name="garuda.completeReqInfo.label.modal.title"/> '+$j('#cburegid').val()+upiob_data+'</div><div style="float:right"><span class="ui-icon ui-icon-print" onclick="printQuicklinksWidget(\''+divIdd+'\',\''+cbuRegID+'\',\''+divTitle+'\');"></span></div></div>');
		$j("#modelPopup1").prev("div").find('span.ui-dialog-title').text('<s:text name="garuda.message.modal.edit.cbuids"/> <s:text name="garuda.message.modal.headerregid"/> '+$j('#cburegid').val()+upiob_data);
	},0);*/
	
		setTimeout(function(){
		var divIdd="completeTd";
		var cbuRegID=$j('#cburegid').val();
		var divTitle='Complete Required Information';
		$j("#dialogForCRI").prev("div").find('span.ui-dialog-title').html('<div id="notesPopup"><div style="float:left"><s:text name="garuda.completeReqInfo.label.modal.title"/> '+$j('#cburegid').val()+'</div><div style="float:right"><span class="ui-icon ui-icon-print" onclick="printQuicklinksWidget(\''+divIdd+'\',\''+cbuRegID+'\',\''+divTitle+'\');"></span></div></div>');
		$j("#modelPopup1").prev("div").find('span.ui-dialog-title').text('<s:text name="garuda.message.modal.edit.cbuids"/> <s:text name="garuda.message.modal.headerregid"/> '+$j('#cburegid').val());
	},0);
}
	function submitID(value){
		
		if('<s:property value="#request.additionalIds.size()"/>'!=maxadditionalidcount){
			changesDone = true;
		}else{
			changesDone = divChanges('IDsChanges',$j('#bodyDiv').html());
		}
		
		if(ckeckValidateStatus()){
			 $j("#cordidschangereason").rules("add", {
				    required: true,
				    messages: {
				        required: "<s:text name="garuda.cbu.cordentry.reasonchangeId"/>"
				    }
				});
		}	
		else{
			$j("#cordidschangereason").rules("remove", "required");
			$j("#cordidschangereason").rules("remove", "messages");
		}	
		if($j("#cbuIdInfo").valid()){
			if(changesDone){
				modalFormSubmitRefreshDiv('cbuIdInfo','submitIdInfo?cordIdStr=<s:property value="cdrCbuPojo.cordID" />','','','');
				//refreshMultipleDivsOnViewClinical('17','<s:property value="cdrCbuPojo.cordID"/>');
				refreshMultipleDivsOnViewClinical('1040','<s:property value="cdrCbuPojo.cordID"/>');
				if($j('#fcrFlag').val() == "Y"){
				refreshMultipleDivsOnViewClinical('2','<s:property value="cdrCbuPojo.cordID"/>');
				}
				setTimeout(function(){
					$j("#modelPopup1").prev("div").find('span.ui-dialog-title').text('<s:text name="garuda.message.modal.edit.cbuids"/> <s:text name="garuda.message.modal.headerregid"/> '+$j('#cburegid').val());
				},0);
			}else{
				showSuccessdiv("update","status");
			}
		}
	}
	
function additonalIdscall(rownumber){
        var description   = $j("#additionalIds"+rownumber+"additionalIdDescid").val();
		  var typeofid      = $j("#additionalIds"+rownumber+"additionalIdValueTypeid").val();
		  var additionalid  = $j("#additionalIds"+rownumber+"additionalId").val();
		  if(description !='' && typeofid !='Select' && additionalid !=''){
		     htmlUniqueIdOnBag();				 
		 }
   }  

function ckeckValidateStatus()
 {  
	 if(maxadditionalidcount < <s:property value="#request.additionalIds.size"/>)
		 return true;
	 if($j('#cbucordregid').val()!="" && $j('#cbucordregid').val()!= $j('#cburegid').val())
		 return true;
	 if($j('#regcordmatid').val()!="" && $j('#regcordmatid').val()!= $j('#regMaternalId').val())
		 return true;
	 if($j('#localcordcbuid').val()!="" && $j('#localcordcbuid').val()!= $j('#loccbuid').val())
		 return true;
	 if($j('#localcordmatid').val()!="" && $j('#localcordmatid').val()!= $j('#matlocalid').val())
		 return true;
	 if($j('#isbidincordcode').val()!="" && $j('#isbidincordcode').val()!= $j('#isbidin1').val())
		 return true;
	 if($j('#numberoncbubag').val()!="" && $j('#numberoncbubag').val()!= $j('#idonbagvalresultmod').val())
		 return true;
	
	 for(var i=0; i< <s:property value="#request.additionalIds.size"/>; i++)
	  { 
		  if(additionalIdDes[i] !="" && additionalIdDes[i] != $j("#additionalIds"+i+"additionalIdDescid").val())
			  return true;
		  if(additionalTypeId[i] !="" && additionalTypeId[i] != $j("#additionalIds"+i+"additionalIdValueTypeid option:selected").text())
			  return true;
		  if(additionalId[i] !="" && additionalId[i] != $j("#additionalIds"+i+"additionalId").val())
			  return true;
	  }
	return false;
 }
$j(function(){
	 if('<s:property value="esignFlag"/>' == 'review'){
		 $j('#indDiv .idsMandatory').each(function(){
			 $j(this).css("border", "1px solid #DBDAD9");
			 var fieldVal = $j(this).val();
			 fieldVal = $j.trim(fieldVal);
			
			 if(fieldVal==null || fieldVal=="" || fieldVal=="-1"){
				 $j(this).css("border", "1px solid red");
				
				}
			  });
		  }
	 
	 });
 function nonSystemCordStatus()
 {
 	if($j('#non_system_cord').val()=="true"){
 		return false;
 	}
 	else{
 		return true;
 	}
 }
</script>

<s:form id="cbuIdInfo">
<div id="indDiv">
<s:hidden id="userid" value="%{#request.userid}" name="userid"></s:hidden>
<s:hidden id="userlogname" value="%{#request.userLogName}" name="userlogname"></s:hidden>
<s:hidden name="cdrCbuPojo.fkSponsorId" id="fkSponsorId"></s:hidden>
<s:hidden name="cdrCbuPojo.pkNMDPSponsorId" id="pkNMDPSponsorId"></s:hidden>
<s:hidden name="cdrCbuPojo.pkOtherSponsorId" id="pkOtherSponsorId"></s:hidden>
<s:hidden name="cdrCbuPojo.birthDate" id="bdate" ></s:hidden>
<s:hidden name="licenceUpdate" value="True"></s:hidden>
<s:hidden id="uRegId" name="cdrCbuPojo.uniqueRegId"></s:hidden>
<s:hidden id="uLocalId" name="cdrCbuPojo.uniqueLocaId"></s:hidden>
<s:hidden id="uIsbtId" name="cdrCbuPojo.uniqueIsbtId"></s:hidden>
<s:hidden id="uAddId" name="cdrCbuPojo.uniqueAddId"></s:hidden>
<!--<s:hidden name="cdrCbuPojo.cordID" />
--><s:hidden name="cdrCbuPojo.fkSpecimenId" />
<s:hidden name="cdrCbuPojo.specimen.pkSpecimen" />
<s:hidden name="cdrCbuPojo.registryId" id="cbucordregid"/>
<s:hidden name="cdrCbuPojo.registryMaternalId" id="regcordmatid"/>
<s:hidden name="cdrCbuPojo.localCbuId" id="localcordcbuid"/>
<s:hidden name="cdrCbuPojo.localMaternalId" id="localcordmatid"/>
<s:hidden name="cdrCbuPojo.cordIsbiDinCode" id="isbidincordcode" />
<s:hidden name="cdrCbuPojo.numberOnCbuBag" id="numberoncbubag" />
<s:hidden name="cdrCbuPojo.historicLocalCbuId" id="histcbulocalid"></s:hidden>	
<s:hidden name="cdrCbuPojo.fkCbbId" id="fkCbbId" />
<s:hidden id="regmatidstatus" name="registrymaternalidstaus" />
<s:hidden id="orderId" name="orderId" />
<s:hidden id="orderType" name="orderType" />
<s:hidden id="non_system_cord" value="%{cdrCbuPojo.site.isCDRUser()}" />

<s:hidden name="esignFlag" value="%{esignFlag}" id="esignFlag"></s:hidden>
<div id="update" style="display: none;" >
  
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="garuda.message.modal.ids" name="message"/>
		  </jsp:include>	   
	
</div>
<div id="bodyDiv">
<input type="hidden" name="divforClass" id="divforClass">
<table width="100%" cellspacing="0" cellpadding="0" border="0" id="status">
   <s:if test="#request.update==true" ></s:if>
	<tr>
  <td colspan="2"/>
  </tr>
  <tr>
  <td>
   <fieldset>
					<legend><s:text name="garuda.cdrcbuview.label.ids" /></legend>
  <table cellpadding="0" cellspacing="0" border="0" >
	<!--<tr>			
		<td width="25%">
			<s:text name="garuda.cbuentry.label.externalcbuid" />:
		</td>
		<td width="25%">
			<s:textfield name="externalCbuId" value="%{externalCbuId}" ></s:textfield>
		</td >
				
		<td width="25%">
			<s:text name="garuda.cbuentry.label.idoncbubag" />:
		</td>
		<td width="25%">
			<s:select name="cbuOnBag" multiple="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_IDS]" listKey="pkCodeId" listValue="description" ></s:select>
		</td>
	</tr>
	--><tr align="left">
		<td width="20%" onmouseover="return overlib('<s:text
			name="garuda.cbuentry.label.cbuRegToolTip"></s:text>');" 
			onmouseout="return nd();"><s:text name="garuda.cbuentry.label.registrycbuid"/>:
		</td>
		<td width="25%"><s:if test="hasEditPermission(#request.updateCbuRegid)==false">
		    <s:textfield maxlength="30" id="cburegid" name="cdrCbuPojoModify.registryId" readonly="true"></s:textfield>
		</s:if>
		<s:else>
           <s:if test="cdrCbuPojo.site.isCDRUser()==true">
			   <s:textfield maxlength="11" cssClass="idsMandatory" name="cdrCbuPojoModify.registryId" id="cburegid" readonly="false" onchange='htmlUniqueIdOnBag();isChangeDone(this,"%{cdrCbuPojoModify.registryId}","IDsChanges");'></s:textfield>
		   </s:if>
			<s:else>
				<s:textfield maxlength="30" cssClass="idsMandatory" name="cdrCbuPojoModify.registryId" id="cburegid" readonly="false" onchange='htmlUniqueIdOnBag();isChangeDone(this,"%{cdrCbuPojoModify.registryId}","IDsChanges")'></s:textfield>
			</s:else>		
			</s:else>
		</td>
		<td width="20%"onmouseover="return overlib('<s:text
			name="garuda.cbuentry.label.matRegToolTip"></s:text>');" 
			onmouseout="return nd();"><s:text name="garuda.cbuentry.label.maternalregistryid"/>:
			</td>		  
		<td width="25%"><s:if test="hasEditPermission(#request.updateCbuMaternalid)==false">
		<s:textfield id="regMaternalId" name="cdrCbuPojoModify.registryMaternalId"  readonly="true"></s:textfield></s:if>
		<s:else>
		<s:if test="cdrCbuPojo.site.isCDRUser()==true">
		<s:textfield maxlength="11" id="regMaternalId" name="cdrCbuPojoModify.registryMaternalId" cssClass="idsMandatory" onchange='isChangeDone(this,"%{cdrCbuPojoModify.registryMaternalId}","IDsChanges");'></s:textfield>
		</s:if>
		<s:else>
		<s:textfield maxlength="30" id="regMaternalId" name="cdrCbuPojoModify.registryMaternalId" onchange='isChangeDone(this,"%{cdrCbuPojoModify.registryMaternalId}","IDsChanges");'></s:textfield>
		</s:else>
		</s:else>
		</td>
	</tr>
	<tr align="left">
		<td width="20%" onmouseover="return overlib('<s:text
				name="garuda.cbuentry.label.cbuLocalToolTip"></s:text>');" 
				onmouseout="return nd();"><s:text name="garuda.cbuentry.label.localcbuid"/>:
			</td>	
		<td width="25%">
		<%--<s:if test="%{#request.userLogName==@com.velos.ordercomponent.util.VelosGarudaConstants@EMAVEN_USER_LOGNAME}">
		<s:textfield id="loccbuid" name="cdrCbuPojoModify.localCbuId" maxlength="30" disabled="true"></s:textfield>
		</s:if>--%>
		<%--<s:else>--%>
		<s:if test="cdrCbuPojo.site.isCDRUser()==true">
		<s:textfield id="loccbuid" name="cdrCbuPojoModify.localCbuId" maxlength="30" onchange='htmlUniqueIdOnBag();isChangeDone(this,"%{cdrCbuPojoModify.localCbuId}","IDsChanges");' cssClass="idsMandatory" ></s:textfield>
		</s:if>
		<s:else>
		<s:textfield id="loccbuid" name="cdrCbuPojoModify.localCbuId" maxlength="30" onchange='htmlUniqueIdOnBag();isChangeDone(this,"%{cdrCbuPojoModify.localCbuId}","IDsChanges");'></s:textfield>
		</s:else>
		<%--</s:else>--%>
		</td>
		<td width="20%" onmouseover="return overlib('<s:text
				name="garuda.cbuentry.label.matLocalToolTip"></s:text>');" 
				onmouseout="return nd();"><s:text name="garuda.cbuentry.label.maternallocalid"/>:</td>
		<td width="25%"><%--<s:if test="%{#request.userLogName==@com.velos.ordercomponent.util.VelosGarudaConstants@EMAVEN_USER_LOGNAME}"><s:textfield id="matlocalid" name="cdrCbuPojoModify.localMaternalId"  disabled="true" maxlength="30"></s:textfield></s:if>--%>
		<%--<s:else>--%><s:textfield id="matlocalid" name="cdrCbuPojoModify.localMaternalId" maxlength="30" onchange='isChangeDone(this,"%{cdrCbuPojoModify.localMaternalId}","IDsChanges");'></s:textfield><%--</s:else>--%>
		</td>
	</tr>
	<tr align="left">
		<td width="20%" onmouseover="return overlib('<s:text
				name="garuda.cbuentry.label.isbtDinToolTip"></s:text>');" 
				onmouseout="return nd();"><s:text name="garuda.cordentry.label.isbtid"/>:</td>
	   <td width="25%"><%--<s:if test="%{#request.userLogName==@com.velos.ordercomponent.util.VelosGarudaConstants@EMAVEN_USER_LOGNAME}"><s:textfield id="isbidin" name="cdrCbuPojoModify.cordIsbiDinCode" disabled="true" onchange="htmlUniqueIdOnBag()"></s:textfield></s:if>--%>
	  <%--<s:else>--%><s:textfield id="isbidin1" name="cdrCbuPojoModify.cordIsbiDinCode" onchange='htmlUniqueIdOnBag(this.value);isChangeDone(this,"%{cdrCbuPojoModify.cordIsbiDinCode}","IDsChanges");'></s:textfield><%--</s:else>--%>
	  </td>	 
	
	    <td width="20%" onmouseover="return overlib('<s:text
						name="garuda.cbuentry.label.uniqueIdToolTip"></s:text>');" 
						onmouseout="return nd();">
			<s:text name="garuda.cbuentry.label.idbag" />:
		</td>
		<td width="25%">
			<!--<s:select name="cbuOnBag" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_IDS]" listKey="pkCodeId" listValue="description" onchange="uniqueIdOnBagVal(this.value)"></s:select>
		-->
		   <input type="text" id="idonbagvalresultmod"  name="cdrCbuPojoModify.numberOnCbuBag" value="<s:property value="cdrCbuPojoModify.numberOnCbuBag"/>" style="display:none">
		   <div id="idonbag">
						<select name="cbuOnBag" class="idsMandatory" id="cbuOnBag" >
						     <option value="-1">Select</option>
						 </select>
		  </div>
		</td>
		<!--<td width="25%"><s:text name="garuda.cdrcbuview.label.isbt_product_code"/>:
		</td><td width="25%"><s:textfield name="cdrCbuPojoModify.cordIsitProductCode" ></s:textfield>					              
		</td>
	-->
	    <%-- <td><s:text name="garuda.cbuentry.label.idbag" />:
	   </td>
	   <td><s:if test="%{#request.userLogName==@com.velos.ordercomponent.util.VelosGarudaConstants@EMAVEN_USER_LOGNAME}"><s:textfield name="cdrCbuPojoModify.numberOnCbuBag" disabled="true"></s:textfield></s:if>
	   <s:else>--%><%--</s:else>--%>
	   <%--</td>--%>
	</tr>
	<tr>
	<s:if test='cdrCbuPojo.historicLocalCbuId!=null && cdrCbuPojo.historicLocalCbuId!=""'>
				   <td width="20%"><s:text name="garuda.cdrcbuview.label.historic_cbu_lcl_id"/>:</td>
				   <td width="30%"><s:textfield name="cdrCbuPojo.historicLocalCbuId" readonly="true" onkeydown="cancelBack();" ></s:textfield>
				  </s:if>
				  <td colspan="2">&nbsp;</td>
	</tr>	
		<!--<tr >
				<td width="25%" >
				       <s:text name="garuda.cordentry.label.additionalIds" />:
				 </td>
				 <td width="25%">
				    <s:textfield name="additionalId" id="additionalId%{#row.index}" ></s:textfield>
				 </td>
				<td colspan="2" width="50%"></td>
		</tr>
		-->		
		<tr>
		   <td colspan="4">
		   <input type="hidden" id="size" value="<s:property value="#request.additionalIds.size"/>"></input>
		      <table width="100%" cellpadding="0" cellspacing="0" border="0" id="additionalidtable">
		          <s:iterator value="#request.additionalIds" status="row">
		            
		             <input type="hidden" name="additionalIds[<s:property value="%{#row.index}"/>].pkAdditionalIds" value="<s:property value="pkAdditionalIds"/>" />
			         <tr align="left" id="additionalid_row<s:property value="%{#row.index}"/>">
			           <td width="20%"><s:text name="garuda.cordentry.label.additionalIdDesc"></s:text>:</td>
					   <td width="25%"><input type="text" class="required" maxlength="30" onchange='additonalIdscall(<s:property value="%{#row.index}"/>);isChangeDone(this,"<s:property value="additionalIdDesc"/>","IDsChanges");'  name="additionalIds[<s:property value="%{#row.index}"/>].additionalIdDesc" id="additionalIds<s:property value="%{#row.index}"/>additionalIdDescid" value="<s:property value="additionalIdDesc"/>" <%--<s:if test="%{#request.userLogName==@com.velos.ordercomponent.util.VelosGarudaConstants@EMAVEN_USER_LOGNAME}">disabled=disabled</s:if>--%>/></td>
			           <td><s:text name="garuda.cordentry.label.additionalTypeId"></s:text>:</td>
					   <td><select name="additionalIds[<s:property value="%{#row.index}"/>].additionalIdValueType" class="required" id="additionalIds<s:property value="%{#row.index}"/>additionalIdValueTypeid" onchange='additonalIdscall(<s:property value="%{#row.index}"/>);isChangeDone(this,"<s:property value="additionalIdValueType"/>","IDsChanges");' id="addID<s:property value="%{#row.index}"/>" >
					   <option value="">Select</option>
					   <option <s:if test="additionalIdValueType=='cbu'">selected</s:if> value="cbu">CBU</option>
					   <option <s:if test="additionalIdValueType=='maternal'">selected</s:if> value="maternal">Maternal</option>
					   </select>
					   </td>
					   <td><s:text name="garuda.cordentry.label.additionalId"></s:text>:</td>
					   <td><input type="text" class="required"  maxlength="30" onchange='additonalIdscall(<s:property value="%{#row.index}"/>);isChangeDone(this,"<s:property value="additionalId" />","IDsChanges");' name="additionalIds[<s:property value="%{#row.index}"/>].additionalId" id="additionalIds<s:property value="%{#row.index}"/>additionalId"  value="<s:property value="additionalId" />" <%--<s:if test="%{#request.userLogName==@com.velos.ordercomponent.util.VelosGarudaConstants@EMAVEN_USER_LOGNAME}">disabled=disabled"</s:if>--%>></td>
			            <td><img style="cursor: pointer;vertical-align:middle;"  src="images/cross.png" border="0"  onclick='deleteRow(<s:property value="%{#row.index}"/>);deleteAddId();' /></td>
					 </tr>					 
		         </s:iterator>				
		      </table>
		   </td>			   
		</tr>
  	    <tr>
  	        <td width="20%">
	        <s:text name="garuda.cbbprocedures.label.specifychangereason"/>:
	       </td>
  	        <td width="25%">
	  	   <%--<s:if test="%{#request.userLogName==@com.velos.ordercomponent.util.VelosGarudaConstants@EMAVEN_USER_LOGNAME}">
	       <s:textarea name="cdrCbuPojoModify.cordIdsChangeReason" cols="15" rows="5" disabled="true"></s:textarea>
	       </s:if>--%>
	       <%--<s:else>--%><textarea id="cordidschangereason" name="cdrCbuPojoModify.cordIdsChangeReason" cols="24" rows="4"  maxlength="200" onchange='isChangeDone(this,"<s:property value='%{cdrCbuPojoModify.cordIdsChangeReason}' escapeJavaScript="true"/>","IDsChanges");'><s:property value='%{cdrCbuPojoModify.cordIdsChangeReason}'/></textarea><%--</s:else>--%>
	  </td>	  
  	    </tr>
	<!--<tr align="left">
	    <td width="25%">val111</td> 
		<td width="25%"><s:property value="cordIdStr" />
		</td>
		<td width="25%">
			val22222
		</td>
		<td width="25%">
			<s:property value="cdrCbuPojo.cordID" />
		</td>
	</tr>-->
	<tr >
	        <td align="left" colspan="5" width="30%">
			    <span style="cursor: pointer;" onclick="addAdditionalId()" id="addbutton">
			           <img style="vertical-align: middle;" src="images/addcomment.png" border="0" id="plus"/>
							<s:text name="garuda.cordentry.label.addAdditionalId"></s:text></span>
			</td>						         		    
		</tr>
</table>

  
</fieldset>
</td>
</tr>






<tr>
   <td colspan="2" align="center">
   <div>
   <table>
   <tr bgcolor="#cccccc">
   <td><jsp:include page="../cb_esignature.jsp" flush="true">
   			<jsp:param value="idsSubmit" name="submitId"/>
			<jsp:param value="idsInvalid" name="invalid" />
			<jsp:param value="idsMinimum" name="minimum" />
			<jsp:param value="idsPass" name="pass" />
   </jsp:include></td>
   <td  align="center" colspan="2" >
   			<s:if test="esignFlag == 'review'">
           		<input type="button" name="idsEdit" disabled="disabled" id="idsSubmit" value="<s:text name="garuda.unitreport.label.button.submit"/>" onclick="submitCRIforIds();">
           	</s:if>
           	<s:else>
           	<input type="button" name="characteristicsEdit" disabled="disabled" id="idsSubmit" value="<s:text name="garuda.unitreport.label.button.submit"/>" onclick="submitID('<s:property value="cdrCbuPojo.cordID" />');">
           	</s:else>
          	<input type="button" onclick="closeModal();" value="<s:text name="garuda.openorder.label.button.cancel"/>" />
      </td>  
   </tr>
   </table>
   </div>
  </table>
  
  </div>
</s:form>
<script>
htmlUniqueIdOnBag();
function deleteAddId(){
	$j('#divforClass').addClass("IDsChanges");
}
</script>
<%--<s:if test="%{#request.userLogName==@com.velos.ordercomponent.util.VelosGarudaConstants@EMAVEN_USER_LOGNAME}">
  <script>
    $j("#cbuOnBag").attr("disabled","disabled");
    var listSize=$j("#size").val();
    for( i=0;i<listSize;i++)
	 {
		$j("#addID"+i).attr("disabled","disabled");
	 }
   
  </script>  
</s:if>--%>