<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<script>
$j("#clinicalhistory").dataTable();
var tdObj = $j('#clinicalhistory').find('tbody').find("tr:eq(0)").find("td:eq(0)");
if($j(tdObj).hasClass('dataTables_empty')){
	$j('#clinicalhistory_info').hide();
	$j('#clinicalhistory_paginate').hide();
}
function reasonsFor(value){
	if(value != $j('#tempUnavail').val()){
		document.getElementById('tempUnavailableReason').style.display='none';
	}
	if(value == $j('#tempUnavail').val()){
		document.getElementById('tempUnavailableReason').style.display='block';
	}
		
}

$j(document).ready(function(){

	 /**
	  * Character Counter for inputs and text areas showing characters left.
	  */
	 $j('#word_count').each(function(){
	     //maximum limit of characters allowed.
	     var maxlimit = 200;
	     // get current number of characters
	     var length = $j(this).val().length;
	     if(length >= maxlimit) {
	   $j(this).val($j(this).val().substring(0, maxlimit));
	   length = maxlimit;
	  }
	     // update count on page load
	     $j(this).parent().find('#counter').html( (maxlimit - length) + ' <s:text name="garuda.cbu.cordentry.charleft"/>');
	     // bind on key up event
	     $j(this).keyup(function(){
	  // get new length of characters
	  var new_length = $j(this).val().length;
	  if(new_length >= maxlimit) {
	    $j(this).val($j(this).val().substring(0, maxlimit));
	    //update the new length
	    new_length = maxlimit;
	   }
	  // update count
	  $j(this).parent().find('#counter').html( (maxlimit - new_length) + ' <s:text name="garuda.cbu.cordentry.charleft"/>');
	     });
	 });

	 //---
	if($j('#clinicalStatus').val() == $j('#tempUnavail').val()){
		
		document.getElementById('tempUnavailableReason').style.display='block';
		}
	else{
		document.getElementById('tempUnavailableReason').style.display='none';
		}
});

$j(function() {
/*	var today = new Date();
	
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();

	var h=today.getHours();
	var mn=today.getMinutes()+1;

	$j( "#datepicker2" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
*/
getDatePic();
	
});


$j(function(){
	
	$j("#clinicalStat").validate({
			
		rules:{	
				"clinicalStatus":{required:true},
				"clinicalStatReason":{required:true},
				"clinicalStatDateStr":{required:{
											depends: function(element) {
												 						return $j("#clinicalStatus").val()==$j("#tempUnavail").val();
												 						}
												}
										}
				},
			messages:{	
				"clinicalStatus":"<s:text name="garuda.cbu.clinicalnote.clinicalstatus"/>",
				"clinicalStatReason":"<s:text name="garuda.cbu.clinicalnote.reasonmaxlimit"/>",
				"clinicalStatDateStr":"<s:text name="garuda.common.validation.selectdate"/>"					
					}
								});
});




</script>
<div id="clinicalDiv" class='popupdialog tabledisplay '>

<s:form id="clinicalStat">
<s:hidden name="licenceUpdate" value="True"></s:hidden>
<s:hidden name="cdrCbuPojo.defferedPkCode" id="differedCode"></s:hidden>
<s:hidden name="cdrCbuPojo.tempUnavail" id="tempUnavail"></s:hidden>
<s:hidden name="cdrCbuPojo.cordID" />
<s:hidden name="cdrCbuPojo.fkSpecimenId" />
<s:hidden name="cdrCbuPojo.specimen.pkSpecimen" />
	<div id="update" style="display: none;" >
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="garuda.message.modal.clinical" name="message"/>
		  </jsp:include>	   
	</div>
<div id="status">
<table width="100%">
	<s:if test="#request.update==true" ></s:if>
<tr>
	<td>
		<table class="displaycdr" id="clinicalhistory" >
			<thead>
				<tr>
					<th><s:text name="garuda.common.lable.date" /></th>
					<th><s:text name="garuda.cdrcbuview.label.user_name" /></th>
					<th><s:text name="garuda.cdrcbuview.label.clinical_status"/></th>
					<th><s:text name="garuda.cdrcbuview.label.eligible_reason" /></th>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="entityStatusHistoryList" var="rowVal" status="row">
				<tr>
					<s:iterator value="rowVal" var="cellValue" status="col" >
						<s:if test="%{#col.index == 0}">
							<s:date name="%{#cellValue}"
									id="statusDate" format="MMM dd, yyyy" />
							<td><s:property value="%{statusDate}"/></td>
						</s:if>
						<s:if test="%{#col.index == 1}">
							<s:set name="cellValue1" value="cellValue" scope="request"/>
							<td>
								<%
									String cellValue1 = request.getAttribute("cellValue1").toString();
									UserJB userB = new UserJB();
									userB.setUserId(EJBUtil.stringToNum(cellValue1));
									userB.getUserDetails();													
								%>	
								<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>										
							</td>
						</s:if>
						<s:if test="%{#col.index == 2}">																							
							<td>
								<s:property value="getCodeListDescById(#cellValue)" />
							</td>
						</s:if>
						<s:if test="%{#col.index == 4}">
						<td><s:property value="%{#rowVal[4]}" /></td>
						</s:if>
					</s:iterator>
				</tr>
				</s:iterator>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3"></td>
				</tr>
			</tfoot>
		</table>
	</td>	
</tr>
<tr>			
	<td colspan="2">
		<div id="licenseReason"  <s:if test="#request.unlicensed!=true">style="display: none;"</s:if>>
			<table width="100%">
				<tr>
					<td width="35%">
						<s:text name="garuda.cdrcbuview.label.unlicense_reason" />:
					</td>
					<td width="65%">
						<s:select name="reasons" id="fkCordCbuUnlicenseReason"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CLINICAL_STATUS]"
									listKey="pkCodeId" listValue="description" multiple="true" value="reasons" cssStyle="height:100px;"/>
					</td>
				</tr>
			</table>
		</div>
   </td>
</tr>
</table>
   <table width="100%" cellspacing="0" cellpadding="0" border="0">
   <tr><td><table><tr>
		<td width="25%">
  			<s:text name="garuda.cbuentry.label.clinicalstatus" />:
  		</td>
		<td width="75%">
			<s:select name="clinicalStatus" id="clinicalStatus" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CLINICAL_STATUS]"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select" onchange="reasonsFor(this.value)" />
		</td></tr></table></td>
	</tr>
	<tr>
		<td>
			 <div id="tempUnavailableReason" <s:if test="#request.unlicensed!=true">style="display: none;"</s:if>>
				<table>
					<tr>
						<td width="25%"><s:text name="garuda.cdrcbuview.label.available_date" />:</td>
						<td width="75%"><s:date name="clinicalStatDate" id="datepicker2" format="MMM dd, yyyy" />
        					<s:textfield readonly="true" onkeydown="cancelBack();"  name="clinicalStatDateStr" class="datepic" id="datepicker2" value="%{datepicker2}" cssClass="datePicWMinDate"/>
       					 </td>
					</tr>
				</table>
			</div>
   		</td>
   </tr>
   <tr><td><table><tr>
		<td width="25%" style="vertical-align: top;"><s:text name="garuda.cdrcbuview.label.clinical_Reason" />:</td>
		<td width="75%"><s:textarea name="clinicalStatReason" cols="35" rows="5" id="word_count"   ></s:textarea> 
			<br/>
			<span id="counter"></span>
		</td></tr></table></td>
	</tr></table>
	<table bgcolor="#cccccc">
	<tr bgcolor="#cccccc" valign="baseline">
		<td width="70%"><jsp:include page="../cb_esignature.jsp" flush="true"></jsp:include></td>
    	<td align="right" width="15%">
    		<input type="button" id="submit" onclick="modalFormSubmitRefreshDiv('clinicalStat','submitClinicalStatus','main');" disabled="disabled" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
    		</td><td width="15%"><input type="button"  onclick="closeModal()" value="<s:text name="garuda.unitreport.label.button.cancel"/>" /></td>
    </tr>	
  </table>
  </div>
</s:form>

</div>