<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script type="text/javascript">
function submitHlavalues(){
	modalFormSubmitRefreshDiv('addCurrentHlaTypingForm','submitCurrentHlaTyping?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#cordId").val()+'&HlaTypingAvail=Y&sampleAtLab='+$j("#sampleAtLab").val(),'main');
	updateHistoryDiv();

}
</script>
<div class="col_100 maincontainer"><div class="col_100">
<div class="column">
<s:form id="addCurrentHlaTypingForm">
	<div id="update" style="display: none;">
		<jsp:include page="../cb_update.jsp">
			<jsp:param value="garuda.message.modal.edithla"	name="message" />
		</jsp:include>
	</div>
	<s:hidden name="hlaUpdate" value="True"></s:hidden>
	<s:hidden name="hlaPojo.fkentityCordId"></s:hidden>
	<s:hidden name="hlaPojo.fkEntityCodeType"></s:hidden>
	<s:hidden name="hlaPojo.fkOrderId" id="orderId"></s:hidden>
	<s:hidden name="hlaPojo.pkcordId" id="cordId"></s:hidden>
	<s:hidden name="hlaPojo.orderType" id="orderType"></s:hidden>
	<s:hidden name="hlaPojo.sampleAtLab" id="sampleAtLab"></s:hidden>
<div id="status">
<table cellpadding="0" cellspacing="0" border="0" class="col_100 ">
<tr>
<td>
	<s:div class="portlet" id="currentHlaTypingparent">
	<div id="currentHlaTyping"
			class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-minusthick"></span><!--<span
				class="ui-icon ui-icon-close"></span>--><s:text name="garuda.addTyping&Testing.label.currenthlatyping"></s:text>
			</div>
			<table>
				<tr>
					<td>
				        <table width="100%">
						<tr>
						<td width="25%">
							<strong><s:text name="garuda.cbuentry.label.hlaLocus"/></strong>:
						</td>
						<td width="25%">
							<s:select name="hlaPojo.fkHlaCodeId" id="hlalucosId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" listKey="pkCodeId" listValue="description"></s:select>
						</td>
						<td width="25%">
							<strong><s:text name="garuda.cbuentry.label.method"/></strong>:
						</td>
						<td width="25%">
							<s:select name="hlaPojo.fkHlaMethodId" id="hlaMethodId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_METHOD]" listKey="pkCodeId" listValue="description"></s:select>
						</td>
						</tr>
						<tr>
						<td width="25%">
							<strong><s:text name="garuda.cbuentry.label.type1"/></strong>:
						</td>
							<td width="25%"><s:textfield name="hlaPojo.hlaType1" id="hlaTypeId1" size="6" onchange="getAntigen();"/>
						</td>
						<td width="25%">
							<strong><s:text name="garuda.cbuentry.label.type2"/></strong>:
						</td>
						<td width="25%">
							<s:textfield name="hlaPojo.hlaType2" id="hlaTypeId2" size="6"/>
						</td>
						</tr>
						<!--<tr>
							<td width="25%">
								<strong><s:text name="Send Lab Report to"/></strong>:
							</td>
							<td width="75%" colspan="3">
								<s:textfield name="paperLabReport"></s:textfield>
							</td>
						</tr>
      					--></table>
				     </td>
					
				</tr>
			</table>
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
			    <tr bgcolor="#cccccc">
					<td >
						<jsp:include page="../cb_esignature.jsp" flush="true">
							<jsp:param value="submitCurrHla" name="submitId"/>
							<jsp:param value="invalidCurrHla" name="invalid" />
							<jsp:param value="minimumCurrHla" name="minimum" />
							<jsp:param value="passCurrHla" name="pass" />
						</jsp:include>
					</td>
						
				    <td  align="center" colspan="2">
				    <input type="button" id="submitCurrHla" onclick="submitHlavalues();" value="<s:text name="garuda.unitreport.label.button.submit"/>" disabled="disabled"/>
				    <input type="button"  onclick="closeModal()" value="<s:text name="garuda.unitreport.label.button.cancel"/>" /></td>
			   </tr>
			</table>
	</div>
</s:div>
</td>
</tr>
</table>
</div>
</s:form>
</div>
</div>
</div>
