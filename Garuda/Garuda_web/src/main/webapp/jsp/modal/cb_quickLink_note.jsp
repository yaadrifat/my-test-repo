<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<script>
function showReplyModals(registryId,cordId,fkNotesCategory,noteSeq,module)
{
	closeModal();
	$j("#modelPopup1").html("");
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	if(module=='pf')
	showModals('Reply Notes for CBU Registry ID:'+registryId+patient_data,'pfReplyNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REPLY_NOTE" />&cdrCbuPojo.cordID='+cordId +'&clinicalNotePojo.fkNotesCategory='+fkNotesCategory+'&clinicalNotePojo.noteSeq='+noteSeq,'500','850','notesModalDiv')
	else
		showModals('Reply Notes for CBU Registry ID:'+registryId+patient_data,'replyNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REPLY_NOTE" />&cdrCbuPojo.cordID='+cordId +'&clinicalNotePojo.fkNotesCategory='+fkNotesCategory+'&clinicalNotePojo.noteSeq='+noteSeq,'500','850','notesModalsDiv')
	}
function revokeNotes(cordId,fkNotesCategory,pkNotes)
{
	loadPageByGetRequset('quickRevokeNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID='+cordId+'&clinicalNotePojo.fkNotesCategory='+fkNotesCategory+'&clinicalNotePojo.pkNotes='+pkNotes,'clinicalNoteFilterDiv')
	refreshMultipleDivsOnViewClinical('256','<s:property value="cdrCbuPojo.cordID"/>');
	}
function showDataModal(title,url,height,width,regisId)
{

	var regisId=$j("#registryId").val();
	var title1 = title +':'+regisId;

	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title1 = title1+patient_data;
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
	if($j('#viewNote').html()==null){
		var obj="<div id='viewNote'></div>";
		$j('body').append(obj);
	}
	var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/> <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	$j("#viewNote").html(progressMsg);
		$j("#viewNote").dialog(
				   {autoOpen: false,
					title: title1  ,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#viewNote").dialog("destroy");
				    }
				   }
				  ); 
		$j("#viewNote").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#viewNote").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
}

function showDataModal1(title,url,height,width,regisId)
{
	var keyword  =  $j("#modelPopup1").find("#keyword").val();
	var keyword1 = $j("#modelPopup1").find("#keyword").val();
 	var pattern1='!@*!';
 	var pattern2='*@!*';
 	var pattern3='@!*@';
 	var index1 = keyword.indexOf("&");
 	var index2 = keyword.indexOf("#");
 	var index3 = keyword.indexOf("%");
 	if(index1>=0)
 	keyword = keyword.replace('&', pattern1);
 	if(index2>=0)
 		{
 		keyword = keyword.replace('#', pattern2);
 		}
 	if(index3>=0)
		{
		keyword = keyword.replace('%', pattern3);
		}
	var type = $j("#modelPopup1").find("#reason").val();
	var cordId = $j("#modelPopup1").find("#cordId").val();
	var title1 = " " + title + keyword1 + " " + 'for CBU Registry ID:' + regisId;
	
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title1 = title1+patient_data;
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
	var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/> <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	$j("#modelPopup2").html(progressMsg);
		$j("#modelPopup2").dialog(
				   {autoOpen: false,
					title: title1  ,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
						jQuery("#modelPopup2").html("");
		      		jQuery("#modelPopup2").dialog("destroy");
				    }
				   }
				  ); 
		$j("#modelPopup2").dialog("open");
	$j.ajax({
        type: "POST",
        url: 'filterNotes?keyword='+ keyword +'&type='+type+'&cordId='+cordId,
       // async:false,
        success: function (result, status, error){
	       	$j("#modelPopup2").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
}
function showMdal(cordId)
{
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	
	var requestedDate = $j("#requestedDate").val();
	var orType=$j("#orderType").val();
	orType=$j.trim(orType);
	if(orType=="CT"){
		if($j('#sampleAtLab').val()=='Y'){
			orType='<s:text name="garuda.orOrderDetail.portletname.ctlaborder"/>';
		}
		else{
			orType='<s:text name="garuda.orOrderDetail.portletname.ctshiporder"/>';
		}
	}
	var progresspermission = $j("#progressNotePermiossion").val(); 
	var clinicalNotePermiossion = $j("#clinicalNotePermiossion").val(); 
	var noteCatVal = $j("#noteCat").val();
    jConfirm('<s:text name="garuda.clinicalnote.label.confirmationmsg"/>', '<s:text name="garuda.common.lable.confirm"/>',
      function(r) {
    	 if (r == true) {
    	    	var subtp = 'NOTE_TYPE_CODE_SUB_TYPE_CLINIC'
    	    	if(clinicalNotePermiossion=="true"){
    	    		closeModal();
    	    		$j("#modelPopup1").html("");
    	    		if(noteCatVal.length==0)
    	    			{
    	    			showModals('Clinical Notes for CBU Registry ID:' +cordId+patient_data,'clinicalNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&subtype='+subtp,'550','850','notesModalsDiv');
    	    			}
    	    		else
    	    			{
    	    			showModals('Clinical Notes for CBU Registry ID:' +cordId+patient_data,'clinicalNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&finalReview=true'+'&subtype='+subtp,'550','850','notesModalsDiv');
    	    			}
    	    	}
    	    	else
    	    		alert('<s:text name ="garuda.clinicalNote.accessRights"/>');
    	        return r;
    	    }
    	    else {
    	    		var subtp = 'PROCESS_NOTE_CATEGORY_CODESUBTYPE'
    	    		var divId='usersitegrpsdropdown';
    	    		if(progresspermission=="true")
    	    			{
    	    			closeModal();
        	    		$j("#modelPopup1").html("");
        	    		if(requestedDate!=null && requestedDate!="undefined" && requestedDate!="")
            	    	   	showModals('Progress Notes for CBU Registry ID:' +cordId+patient_data,'pfNotes?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&subtype='+subtp+'&orderType='+orType+'&divId='+divId+'&requestedDate='+requestedDate,'600','850','notesModalDiv');
        	    		else
                	    	showModals('Progress Notes for CBU Registry ID:' +cordId+patient_data,'pfNotes?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&subtype='+subtp+'&orderType='+orType+'&divId='+divId,'600','850','notesModalDiv');
            	    	}
    	    		else
    	    			{
    	    			alert('<s:text name ="garuda.progressNote.accessRights"/>');
    	    			}
    	    }
    });
 

}

function filter(e)
{  
    evt = e || window.event;
    var keyPressed = evt.which || evt.keyCode;
	if (keyPressed == 13){
	showDataModal1('Notes having keyword ','filterNotes','500','800','<s:property value="cdrCbuPojo.registryId"/>');
	
	return false;
	}
	return true;
}

</script>
<div id="clinicalNoteFilterDiv" class='popupdialog tabledisplay ' >
<s:form id="clinical" name="clinical">
<s:hidden name="cdrCbuPojo.cordID" id="cordId"></s:hidden>
<s:hidden name="clinicalNotePojo.pkNotes" />
<s:hidden name="cdrCbuPojo.registryId" id = "registryId"/>
<s:hidden name="clinicalNotePojo.fkNotesCategory" id="noteCat" />
<s:hidden name="clinicalNotePojo.viewFlag" />
<s:hidden name="orderId" id="orderId"></s:hidden>
<s:hidden name="orderType" id="orderType"></s:hidden>
<input type="hidden" id="sampleAtLab" value='<s:property value="orderPojo.sampleAtLab"/>'/>
<s:hidden name="clinicalNotePojo.requestDate" id="chk"/>
<input type="hidden" id="clinicalNotePermiossion" value="<s:property value="hasNewPermission(#request.vClnclNote)"/>"/>
<input type="hidden" id="progressNotePermiossion" value="<s:property value="hasNewPermission(#request.vPrgrssNote)"/>"/>
<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test='#rowVal[45]!=null && #rowVal[45]!=""'>
										<s:hidden name="requestedDate" id="requestedDate" value="%{#rowVal[45]}"></s:hidden>
										</s:if>
									</s:iterator>
  <table width="100%" cellspacing="0" cellpadding="0" border="1" id="notessearchTable">
  <s:if test ="#request.filter!=true">
  <tr>
   <td align="left">
  				<button type="button" 
				onclick="showMdal('<s:property value="cdrCbuPojo.registryId"/>');"><s:text
				name="garuda.cdrcbuview.label.button.addnewnotes" /></button>
   </td>
   </tr>
     </s:if>
   <tr>
  		<td >
				<s:text name="garuda.clinicalnote.label.keyword" />
				
		</td>
		<td>
				<s:textfield name="clinicalNotePojo.keyword" id="keyword" style="width: 200px" onkeypress="return filter(event);" />
		</td>
		<td>
				<s:text name="garuda.clinicalnote.label.type" />
				
		</td>
		<td>
				<s:select name="clinicalNotePojo.fkNotesType" id = "noteType" style="width:200px" id="reason" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_TYPE]"
											listKey="pkCodeId" listValue="description" headerKey=""
											headerValue="Select"  />
		</td>
		<s:if test ="#request.filter!=true">
		<td>
				<button type="button"  onclick="showDataModal1('Notes having keyword ','filterNotes','500','800','<s:property value="cdrCbuPojo.registryId"/>');"><s:text name="garuda.cbu.quickNote.filter" /></button>
		</td>
			</s:if>			
   </tr>

   </table>
   <table>
   	<tr>
   	<td>
   			
   				<s:if test="clinicalNotelist.size()>0">
  		<div id="history1" style=" width: 800px; height: 100px;" >  		
  		<table>
  		 	<tr>
  		 		<td>   				
					<table>		
						<s:iterator value="#request.clinicalNotelist">
						<s:if test ="reply==true">
						<s:if test = "fkNotesType==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_TYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_TYPE_CODE_SUB_TYPE_PROGRESS)">
						<tr>
						
										    <td></td>
											<s:set name="cellValue1" value="createdBy" scope="request"/>
													<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>	
												</td>
											<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
												<s:text name="garuda.clinicalnote.label.posted_on"></s:text>
												<s:set name="cellValue15" value="createdOn" />
							        			<s:date name="cellValue15" id="cellValue15" format="MMM dd, yyyy / hh:mm:ss a"  />
							        			<s:property value="cellValue15"/>	
											</td>
										
										<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
											<s:text name="garuda.cdrcbuview.label.progress_notes"></s:text>
											<s:text name="garuda.cbu.label.reProductFul"/><s:property value="noteSeq"/>											
										</td>
										
									<td>
									<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
												<a href ="#" onClick="loadPageByGetRequset('quickRevokeNote?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes='+<s:property value="pkNotes"/>,'clinicalNoteFilterDiv')"> <s:text name="garuda.clinicalnote.label.revoke" /></a>
									</s:if>
									</td>
									
									
									<td>
												<a href ="#" onClick="showDataModal('Progress Note for CBU RegistryId ','pfViewNote?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property value="pkNotes"/>','500','700','<s:property value="cdrCbuPojo.registryId"/>')"> <s:text name="garuda.clinicalnote.label.view" /></a>
												
									</td>
									 <s:if test="hasEditPermission(#request.vPrgrssNote)==true">
									<td>
												<a href ="#" onClick="showReplyModals('<s:property value="cdrCbuPojo.registryId"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="fkNotesCategory"/>','<s:property value="#request.noteSeq"/>','pf');"> <s:text name="garuda.clinicalnote.label.reply" /></a>
									</td> 
									</s:if>
							</tr>
							 <tr>
							 <td></td>
  		 							<td colspan="9" <s:if test="#request.amended==true">style="text-decoration:line-through;"</s:if>>
									<div style="overflow: auto;">
									<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="notes"/></strong>
									</div>
									</td>
  		 					</tr>
							</s:if>
							<s:else>
							<tr>	
							<td></td>
											<s:set name="cellValue1" value="createdBy" scope="request"/>
													<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>	
												</td>
									<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
										<s:text name="garuda.clinicalnote.label.posted_on"></s:text>
										<s:set name="cellValue15" value="createdOn" />
							        	<s:date name="cellValue15" id="cellValue15" format="MMM dd, yyyy / hh:mm:ss a"  />
							        	<s:property value="cellValue15"/>	
									</td>
									<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
											<s:text name="garuda.cdrcbuview.label.clinical_notes"></s:text>
											<s:text name="garuda.cbu.label.reClinical"/><s:property value="noteSeq"/>											
									</td>
									
									<td>
									 <s:if test="hasEditPermission(#request.vClnclNote)==true">
												<a href ="#" onClick="loadPageByGetRequset('quickRevokeNote?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes='+<s:property value="pkNotes"/>,'clinicalNoteFilterDiv')"> <s:text name="garuda.clinicalnote.label.revoke" /></a>
									</s:if>
									</td>
									<td>
											<a href ="#" onClick="showDataModal('Clinical Note for CBU RegistryId ','viewNotes?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property value="pkNotes"/>','500','700','<s:property value="cdrCbuPojo.registryId"/>')"> <s:text name="garuda.clinicalnote.label.view" /></a>
									</td>
								 	<s:if test="hasEditPermission(#request.vClnclNote)==true">
									<td>
											<a href ="#" onClick="showReplyModals('<s:property value="cdrCbuPojo.registryId"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="fkNotesCategory"/>','<s:property value="#request.noteSeq"/>','cn');"> <s:text name="garuda.clinicalnote.label.reply" /></a>
									</td> 
									</s:if>
							</tr>
							 <tr>
							 <td></td>
  		 							<td colspan="9" <s:if test="#request.amended==true">style="text-decoration:line-through;"</s:if>>
  		 							<div style="overflow: auto;">
									<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="notes"/></strong>
									</div>
									</td>
  		 					</tr>
							</s:else>
						</s:if>
						<s:else>
						<s:if test = "fkNotesType==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_TYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_TYPE_CODE_SUB_TYPE_PROGRESS)">
								<tr>
								<s:set name="cellValue1" value="createdBy" scope="request"/>
													<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
													<s:if test="tagNote==true">
													<img width="20" height="16" src="images/exclamation.jpg">
													</s:if>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>										
													</td>
							<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
								<s:text name="garuda.clinicalnote.label.posted_on"></s:text>
									<s:set name="cellValue15" value="createdOn" />
							        <s:date name="cellValue15" id="cellValue15" format="MMM dd, yyyy / hh:mm:ss a"  />
							        <s:property value="cellValue15"/>	
								</td>
								<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
										<s:text name="garuda.cdrcbuview.label.progress_notes"></s:text>
										<s:text name="garuda.cbu.order.pf"/><s:property value="noteSeq"/> <s:property value="getCodeListDescById(fkNotesCategory)" />
								<input type="hidden" name="pkNotes"/>
								</td>				
								<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
												<s:text name="garuda.clinicalnote.label.subject"></s:text>:
												<s:property value="subject"/>
								</td>
						<%-- 		<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
										<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
										   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]">
														    <s:if test="subType==noteVisible">
														         <s:property value="description"/>
														    </s:if> 
														</s:iterator>
								</td> --%>
								<td>
								<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
												<a href ="#" onClick="loadPageByGetRequset('quickRevokeNote?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.fkNotesCategory=<s:property value="fkNotesCategory"/>&clinicalNotePojo.pkNotes='+<s:property value="pkNotes"/>,'clinicalNoteFilterDiv')"> <s:text name="garuda.clinicalnote.label.revoke" /></a>
								</s:if>
								</td>
								
								<td>
								        		<a href ="#" onClick="showDataModal('Progress Note for CBU Registry ID ','pfViewNote?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property value="pkNotes"/>','500','700','<s:property value="cdrCbuPojo.registryId"/>')"> <s:text name="garuda.clinicalnote.label.view" /></a>
								
								</td>
									 <s:if test="hasEditPermission(#request.vPrgrssNote)==true">		
								<td>
												<a href ="#" onClick="showReplyModals('<s:property value="cdrCbuPojo.registryId"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="fkNotesCategory"/>','<s:property value="#request.noteSeq"/>','pf');"> <s:text name="garuda.clinicalnote.label.reply" /></a>
								</td> 
							</s:if>
						    </tr>	
						     <tr>
  		 							<td colspan="9" <s:if test="#request.amended==true">style="text-decoration:line-through;"</s:if>>
  		 							<div style="overflow: auto;">
									<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="notes"/></strong>
									</div>
									</td>
  		 					</tr>
						</s:if> 
						<s:else>
							<tr>
								<s:set name="cellValue1" value="createdBy" scope="request"/>
													<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>										
													</td>
							<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
								<s:text name="garuda.clinicalnote.label.posted_on"></s:text>
									<s:set name="cellValue15" value="createdOn" />
							        <s:date name="cellValue15" id="cellValue15" format="MMM dd, yyyy / hh:mm:ss a"  />
							        <s:property value="cellValue15"/>	
								</td>
								<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
								<s:text name="garuda.cdrcbuview.label.clinical_notes"></s:text>
									:<s:text name="garuda.cbu.label.clinical_note"/><s:property value="noteSeq"/> <s:property value="getCodeListDescById(fkNotesCategory)" />
								<input type="hidden" name="pkNotes"/>
								</td>
						
					
								
								<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
								     
						   						<s:text name="garuda.clinicalnote.label.assessment"></s:text>
										
											
											             <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_ASSESS]">
														    <s:if test="subType==noteAssessment">
														       	<s:property value="description"/>
														    </s:if> 
														</s:iterator>
											        
								</td>
								<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
											          <s:text name="garuda.clinicalnote.label.visibility"></s:text>
											           <s:if test="visibility==true"><s:text name="garuda.clinicalnote.label.visibleToTc" /></s:if>
											           <s:else><s:text name="garuda.clinicalnote.label.notvisibleToTc" /></s:else>
								
								</td>
								<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
								    
											       <s:text name="garuda.clinicalnote.label.flagForLater"></s:text>:
											           <s:if test="flagForLater==true"><s:text name="garuda.clinicalnote.label.yes" /></s:if>
											           <s:else><s:text name="garuda.clinicalnote.label.no" /></s:else>
											       
								</td>
							
									<td>
									 <s:if test="hasEditPermission(#request.vClnclNote)==true">
										<a href ="#" onClick="revokeNotes('<s:property value="cdrCbuPojo.cordID" />','<s:property value="fkNotesCategory"/>','<s:property value="pkNotes"/>')"> <s:text name="garuda.clinicalnote.label.revoke" /></a>
									</s:if>
									</td>
								
									<td>
									  
												<a href ="#" onClick="showDataModal('Clinical Note for CBU Registry ID ','viewNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property value="pkNotes"/>','500','700','<s:property value="cdrCbuPojo.registryId"/>')"> <s:text name="garuda.clinicalnote.label.view" /></a>
									</td>
									 <s:if test="hasEditPermission(#request.vClnclNote)==true">
									<td>
											<a href ="#" onClick="showReplyModals('<s:property value="cdrCbuPojo.registryId"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="fkNotesCategory"/>','<s:property value="#request.noteSeq"/>','cn');"> <s:text name="garuda.clinicalnote.label.reply" /></a>
									</td>
									</s:if>
						    </tr>	
						    <tr>
  		 							<td colspan="9" <s:if test="#request.amended==true">style="text-decoration:line-through;"</s:if>>
  		 							<div style="overflow: auto;">
									<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="notes"/></strong>
									</div>
									</td>
  		 					</tr>
						   
						   </s:else>
						   </s:else>
						</s:iterator>
				   </table>
				
  		 				</td>
  		 	</tr>
  		 	
  		</table></div>
  	
  		</s:if>
  		<s:else>
  				<s:if test ="#request.filter==true">
  					
  					 <div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
						<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
						<strong><s:text name="garuda.clinicalnote.label.filter" /> </strong></p>
					</div>
  				</s:if>
  		</s:else>
    </td>
    </tr>
	
  </table>

  </s:form>

</div>
<div id="modelPopup4"></div>
