<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script>
//$j("#elistshistry").dataTable();
function inEligibleReason(val){
	if(val=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON)"/>'){
		$j('#inEliReason').show();
		if($j("#newQnaire").length>0){
			$j("#newQnaire").show();
			$j("#summTxt").show();
		}
		//get_selected('fkCordCbuEligibleReason');
		$j('#inComReason').hide();
		$j('#additonalinfo').hide();
		}
	else if(val=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INCOMPLETE_REASON)"/>'){
		$j('#inEliReason').hide();
		//get_selected('fkCordCbuIncompleteReason');
		$j('#inComReason').show();
		$j('#additonalinfo').hide();
		if($j("#newQnaire").length>0){
			$j("#newQnaire").show();
			$j("#summTxt").show();
		}
		}	
	else if(val=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBLE)"/>'){
		$j('#inEliReason').hide();
		$j('#inComReason').hide();
		$j('#additonalinfo').show();
		if($j("#newQnaire").length>0){
			$j("#newQnaire").show();
			$j("#summTxt").show();
		}
		}
	else if(val==$j("#notCollectedToPriorReasonId").val()){
		$j('#inEliReason').hide();
		$j('#inComReason').hide();
		$j('#additonalinfo').show();
		if($j("#newQnaire").length>0){
			$j("#newQnaire").hide();
			$j("#summTxt").hide();
		}
		}
	else{
		$j('#inEliReason').hide();
		$j('#inComReason').hide();
		$j('#additonalinfo').hide();
		if($j("#newQnaire").length>0){
			$j("#newQnaire").show();
			$j("#summTxt").show();
		}
		}
	if(('<s:property value="cdrCbuPojo.fkCordCbuStatus"/>'=='<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH)"/>' || '<s:property value="cdrCbuPojo.fkCordCbuStatus"/>'=='<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)"/>')){
				$j("input[name=eligibleEdit]").hide();
	}
}
/*function get_selected(id){
$j("#"+id).each(function (i, el) {
    var isMultiSelect=$j(el).attr("multiple");
    if(isMultiSelect){
       var id =el.id;
       var text ="";
       $j("#"+id+" option:selected").each(function (i, el) {
           if(text!=""){
          	 text+="<br/>";
              }
      	 text+=$j(this).text();
         });
       $j(el).replaceWith(text);
    }
  });
}
*/

$j(function(){
	inEligibleReason($j("#fkCordCbuEligible").val());
	disableFormFlds("viewFinaleligible");
	/*$j("#fkCordCbuEligible option:selected").each(function (i, el) {
        var  text = $j(this).text();
        if(text == "Select"){
      	  text = "";
           }
        var parent = $j(el).closest("select");
        $j(parent).replaceWith(text);
      });*/
});
</script>
<div id="ineligibleDiv" >
<s:form id="viewFinaleligible">
<s:hidden name="cdrCbuPojo.inEligiblePkid" id="inEligiblePk"></s:hidden>
<s:hidden name="cdrCbuPojo.notCollectedToPriorReasonId" id="notCollectedToPriorReasonId" ></s:hidden>
<s:hidden name="licenceUpdate" value="True"></s:hidden>
<s:hidden name="cdrCbuPojo.cordID" />
<s:hidden name="cdrCbuPojo.fkSpecimenId" />
<s:hidden name="cdrCbuPojo.specimen.pkSpecimen" />
<!--<s:hidden name="orderId" id="orderId" />-->
	<table width="100%">
	<!-- <tr><td colspan="2">
					<div id="eligiblehistory" >
						<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
						<s:text name="garuda.cdrcbuview.label.eligibility_edit_histry" /></div>
						<div class="portlet-content">
							<div id="searchTble">
								<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="elistshistry" >
									<thead>
										<tr>
											<th><s:text name="garuda.common.lable.date" /></th>
											<th><s:text name="garuda.cdrcbuview.label.user_name" /></th>
											<th><s:text name="garuda.cdrcbuview.label.eligible_determin" /></th>
											<th><s:text name="garuda.cdrcbuview.label.eligible_reason" /></th>
										</tr>
									</thead>
									<tbody>
										<s:iterator value="lstEligibility" var="rowVal" status="row" >
										<tr>
											<s:iterator value="rowVal" var="cellValue" status="col"  >
												
												
												<s:if test="%{#col.index == 0}">
													<s:date name="%{#cellValue}"
													id="statusDate" format="MMM dd, yyyy" />
													<td><s:property value="%{statusDate}"/></td>
												</s:if>
												<s:if test="%{#col.index == 1}">
												 <s:set name="cellValue1" value="cellValue" scope="request"/>
													<td>
													<%
														//String cellValue1 = request.getAttribute("cellValue1").toString();
														//UserJB userB = new UserJB();
														//userB.setUserId(EJBUtil.stringToNum(cellValue1));
														//userB.getUserDetails();													
													%>		
													<%//userB.getUserLastName()+" "+userB.getUserFirstName() %>										
													</td>
												</s:if>
												<s:if test="%{#col.index == 2}">																							
												<td style="valign : top">
												  <s:property value="getCodeListDescById(#cellValue)" />
												</td>
												</s:if>
												<s:if test="%{#col.index == 3}">
													<td><s:iterator value="getMultipleIneligibleReasons(#cellValue)" var="colVal" status="val"><s:property value="getCodeListDescById(#colVal)" /><br></s:iterator></td>
												</s:if>
											</s:iterator>
										</tr>
										</s:iterator>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="3"></td>
										</tr>
									</tfoot>
								</table>
							</div>
							</div></div>
	</td></tr> -->
	<s:if test='#request.isTasksRequiredFlag == false '>
	 <tr>
        <td colspan="2">
			<strong><s:text name="garuda.common.label.summarize"/></strong>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <s:hidden name="declPojo.pkEligDecl"></s:hidden>
            <jsp:include page="../cord-entry/cb_final_decl_quest.jsp">
            <jsp:param value="true" name="readonly"/>
            </jsp:include>
        </td>
    </tr>
    </s:if>
    <s:elseif test='#request.isTasksRequiredFlag == true '>
     <tr>
        <td colspan="2" id="summTxt">
			<strong><s:text name="garuda.common.label.summarize"/></strong>
        </td>
    </tr>
    <tr>
        <td colspan="2" id="newQnaire">
            <s:hidden name="declPojo.pkEligDecl"></s:hidden>
            <jsp:include page="../cb_new_final_decl_quest.jsp">
            <jsp:param value="true" name="readonly"/>
			</jsp:include>
        </td>
    </tr>
    </s:elseif>
	<tr>
		<td colspan="2">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
	  		 <tr>
  				<td width="35%" style="align:left"><s:text name="garuda.cdrcbuview.label.eligible_status" />:</td>
				<td width="65%" style="align:left"><s:select name="fkCordCbuEligible" cssStyle="width:290px;" id="fkCordCbuEligible" list="%{getListOfCodeLstObj(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS,cdrCbuPojo.cordID)}"
				listKey="pkCodeId" listValue="description" headerKey="" disabled="true"
				headerValue="Select" /></td>
			 </tr>
		</table>
		</td>
	</tr>	
	
	<tr>			
   	<td>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr id="inEliReason">
			<td width="35%" style="vertical-align: centre;align:left;"><s:text name="garuda.cdrcbuview.label.ineligible_reason" />:</td>
			<td width="65%" style="align:left">
			<s:select cssStyle="height:100px;width:390px;" name="inEligibleReasons" id="fkCordCbuEligibleReason" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON]"
							listKey="pkCodeId" listValue="description"  value="inEligibleReasons" disabled="true"
							  multiple="true"/>
			</td>
		</tr>
		<tr id="inComReason">
			<td width="35%" style="vertical-align: centre;align:left;"><s:text name="garuda.cdrcbuview.label.incomplete_reason" />:</td>
			<td width="65%" style="align:left">
			<s:select cssStyle="height:100px;width:390px;" name="inCompReasons" id="fkCordCbuIncompleteReason" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@INCOMPLETE_REASON]"
							listKey="pkCodeId" listValue="description"  value="inCompReasons" disabled="true"
							  multiple="true" />
			</td>
		</tr>
		<tr id="additonalinfo">
			<td width="35%" style="vertical-align: centre;align:left;"><s:text name="garuda.cdrcbuview.label.ineligible_reason_notes" />:</td>
			<td width="65%" style="align:left">
			<s:property value="cordAdditionalInfo"/> 
			<br/>
			<span id="counter"></span>
			</td>
		</tr>
	</table>
	</td>
   <tr><td colspan="2">
   <table bgcolor="#cccccc" width="100%" id="eligibleTable">
  		<tr>
  			<td align="center">
	  			<input type="button" name="eligibleEdit"
							value="<s:text name="garuda.cbuentry.label.change"/>"
							onclick="showModal('Edit Eligibility Determination','updateEligiblestatus?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&pkcordId=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&esignFlag=modal&pkcordId=<s:property value="cdrCbuPojo.cordID" />','390','700');">
  			</td>
  		</tr>
   </table> 
   </td>
   </tr>
   </table>
</s:form>
</div>
