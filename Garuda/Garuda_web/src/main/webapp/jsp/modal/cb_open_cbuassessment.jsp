<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>

<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants" %>
<script>
$j(function(){
	$j('#cbuassment').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
		},
		"bRetrieve": true,
		"bDestroy" :true
	});
	var tdObj = $j('#cbuassment').find('tbody').find("tr:eq(0)").find("td:eq(0)");
	if($j(tdObj).hasClass('dataTables_empty')){
		$j('#cbuassment_info').hide();
		$j('#cbuassment_paginate').hide();
	}
});

function showPdfForms(cordID,doc){
	var url='getPdfObject?cdrCbuPojo.CordID='+cordID+'&pdfDocument='+doc+'&repId=184&repName=&params='+cordID+":MRQ";
	window.open(url);
}
function revokeDocumentModal(cdrCbuId,attachmentId,dcmsAttachId,title,url1,lenght,breadth){
	
 	var orderId=$j("#orderId").val();
	var orderType=$j("#orderType").val();	
	//alert(orderId);
	//alert(orderId);
	//alert(url1);
	var answer = confirm('Please confirm if you want to revoke the document.');
	if (answer){	
				if(orderId!=null && orderId!="" && orderId!="undefined" && orderType!=null && orderType!="" && orderType!="undefined"){
						var url='revokeDocFromPF?orderId='+orderId+'&orderType='+orderType+'&cbuCordId='+cdrCbuId+'&cbuid='+cdrCbuId+'&cordId='+cdrCbuId+'&pkcordId='+cdrCbuId+'&cordID='+cdrCbuId+'&attachmentId='+attachmentId;
						loadMoredivs(url,'quicklinksDiv','cbuassmentForm','','');
						modalFormSubmitRefreshDiv('cbuassmentForm',url1,'cbuassmnt');
						$j("#viewCbuass").hide();
						alert("Document revoked successfully");
					}
				else{
				
						var url='revokeDoc?cbuCordId='+cdrCbuId+'&cbuid='+cdrCbuId+'&cordId='+cdrCbuId+'&pkcordId='+cdrCbuId+'&cordID='+cdrCbuId+'&attachmentId='+attachmentId;
						loadMoredivs(url,'quicklinksDiv','cbuassmentForm','','');
						modalFormSubmitRefreshDiv('cbuassmentForm',url1,'cbuassmnt');
						$j("#viewCbuass").hide();
						alert("Document revoked successfully");
				}
	}
}
</script>
<s:form id="cbuassmentForm">
	<s:hidden name="cdrCbuPojo.cdrCbuId" id="cdrCbuId"></s:hidden> 
	<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
	<s:hidden name="cdrCbuPojo.registryId"  id="cburegid"></s:hidden>
	<s:if test="#request.sentMail==true">
	  <jsp:include page="../cb_update.jsp">
			    <jsp:param value="Message Sent Successfully" name="message"/>
	 </jsp:include>
	</s:if>
	<s:else>
		<table width="100%" cellpadding="0" cellspacing="0" align="center">
		  <!-- <tr>
		      <td align="left">
		        <button type="button" onclick="loadPageByGetRequset('sentCbuAssessmentMail?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID"/>&mailCode=<s:property value="@com.velos.ordercomponent.util.VelosMailConstants@TRANSPLANT_CENTER_CODE"/>&cbuID=<s:property value="cdrCbuPojo.registryId" />','modelPopup1')"><s:text name="garuda.cbuentry.label.send_email" /></button>
		      
		      <button type="button" onclick="loadPageByGetRequset('sendMailHome?module=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_CBU_ASSESSMENT"/>&repId=165&filterKeys=cordId&filterValues=<s:property value="cdrCbuPojo.cordID" />&repName=CBU Assessment&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID"/>&mailCode=<s:property value="@com.velos.ordercomponent.util.VelosMailConstants@CBU_ASSESSMENT_CODE"/>&cbuID=<s:property value="cdrCbuPojo.registryId" />','modelPopup1')"><s:text name="garuda.cbuentry.label.send_email" /></button>
		      </td>
		  </tr>
		  <tr><td>&nbsp;</td></tr>-->
		  <tr>
		      <td align="left"><!--
		         <s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBUASSESSMENT_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBUASSESSMENT_CODESUBTYPE].size > 0">
			         <s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBUASSESSMENT_CODESUBTYPE]" var="rowVal" status="row">
							<s:set name="docId" value="%{#rowVal[5]}" scope="request"/>																							 	
					</s:iterator>	
				     <button type="button" onclick="window.open('getAttachmentFromDcms?docId=<s:property value="#request.docId"/>&signed=true','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><s:text name="garuda.common.lable.viewcbuassandmrq" /></button>
			    </s:if>
			    <s:else> </s:else>
			       --><button type="button" id="viewCbuass" onclick="showPdfForms('<s:property value="cdrCbuPojo.CordID"/>','<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@PDF_CBU_ASSESSMENT"/>')"><s:text name="garuda.common.lable.viewcbuassandmrq" /></button>
		       
		      </td>
		  </tr>
		</table>
	</s:else>
		<div id="cbuassmnt" class="portlet-content">
			<div id="searchTble1">
			<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="cbuassment">
				<thead>
					<tr>
						<th class="th2" scope="col"><s:text name="garuda.uploaddoc.label.date" /></th>
						<th><s:text name="garuda.uploaddoc.label.documentuploadedby" /></th>
					    <th><s:text name="garuda.uploaddoc.label.documentcategory" /></th>
				        <th><s:text name="garuda.openorder.label.completiondate" /></th>
				        <th class="th3" scope="col"><s:text name="garuda.openorder.label.description" /></th>
						<th class="th5" scope="col"><s:text name="garuda.uploaddoc.label.view" /></th>
						<th><s:text name="garuda.common.lable.modify" /></th>
						<th><s:text name="garuda.common.lable.revoke" /></th>						
					</tr>
				</thead>
				<tbody>
				 
				 	<s:if test="(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBUASSESSMENT_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBUASSESSMENT_CODESUBTYPE].size > 0)">
						<s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBUASSESSMENT_CODESUBTYPE] " var="rowVal" status="row">
							<s:set name="cellValue1" value="%{#rowVal[14]}" scope="request"/>
							<tr>
							<td><s:date name="%{#rowVal[10]}" id="uploadDate"format="MMM dd, yyyy / hh:mm:ss a" /><s:property value="%{#uploadDate}" /></td>
						    <td>
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>	
					        </td>	
					        <td><s:property value="getCodeListDescByPk(#rowVal[15])" /></td>
					       <td><s:date name="%{#rowVal[20]}" id="completionDate2" format="MMM dd, yyyy"/><s:property value="%{#completionDate2}" /></td>
					       	<td><s:property value="%{#rowVal[13]}" /></td>
							<td><a href="#"><img src="images/attachment-icon.png"
								onclick="javascript: return showDocument('<s:property value="%{#rowVal[5]}" />')" /></a></td>
							<s:if test="hasEditPermission(#request.viewAssmentCat)==true">
							<td><button type="button" onclick="javascript:modifyDocument('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="%{#rowVal[3]}"/>','<s:property value="%{#rowVal[5]}" />');"><s:text name="garuda.common.lable.modify" /></button></td>
							<td><button type="button" onclick="javascript: revokeDocumentModal('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="%{#rowVal[5]}" />','<s:text name="garuda.header.label.cbuAssessment"/>:<s:property value="cdrCbuPojo.registryId"/>','cbuAssessment?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />','400','500')" ><s:text name="garuda.common.lable.revoke" /></button></td>
							</s:if>	
								<s:else>
									<td><button type="button" disabled="disabled"><s:text name="garuda.common.lable.modify" /></button></td>
									<td><button type="button" disabled="disabled"><s:text name="garuda.common.lable.revoke" /></button></td>
				               </s:else>
							<s:if test ="#rowVal[11]!=null"><s:set name="cellVal" value="%{#rowVal[11]}" scope="request"/></s:if>
							
							</tr>
							</s:iterator>
					
				</s:if>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="6"></td>
					</tr>
					
				</tfoot>
			</table>
			</div>
		</div>
	
</s:form>