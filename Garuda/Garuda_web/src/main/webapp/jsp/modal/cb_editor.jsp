<%@ include file="../cb_includes.jsp"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<%@page import="com.velos.eres.web.user.UserJB"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<script>
/*function amend(value)
{
	alert(value);
	$.ajax({
		type : "POST",
		async : false,
		url : 'amendNotes?pkNotes='+ value,
		success : function(result) {
			//showUnitReport("close");
			alert("Sucess");
			$('#clinical').html(result);
			if()
				{
			$('#cbustat').
				}
		}
		
	});
}*/
init();
<%
Long categoryTypeID = null;
if(request.getParameter("categoryTypeID")!=null){
	categoryTypeID = Long.parseLong(request.getParameter("categoryTypeID"));
}else if(request.getAttribute("categoryTypeID")!=null){
	categoryTypeID = (Long)request.getAttribute("categoryTypeID");
}
request.setAttribute("categoryTypeID",categoryTypeID);

%>

function closeModalNoteDiv(){
	
	 $j("#notesModalsDiv").dialog("close");
	 $j("#modelPopup").dialog("close");
	 $j("#notesModalsDiv").dialog("destroy");
	 $j("#modelPopup").dialog("destroy");
}

function clearError(id,val)
{
	var userId = $j("#userList").val();
	if(userId>0)
		$j("#inform_error").hide();
	else
		$j("#inform_error").show();
		
}

function showDataModal(title,url,height,width,regisId,id)
{
	var id1 = "#" + id;
	var title1 = title + regisId;
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}

	$j(id1).dialog(
				   {autoOpen: false,
					title: title1  ,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery(id1).dialog("destroy");
				    }
				   }
				  ); 
		$j(id1).dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j(id1).html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	
}

function showEmail(cordId,regisId,code,id){
	var flag = true;
	var url;
	if(id=="inform")
		{
		url ='saveClincalNotes?informTo='+flag
		}
	else if(id=="consult")
		{
		url = 'saveClincalNotes?consultTo='+flag
		}
	var userId = $j("#userList").val();
	if($j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val()==null)
	{
		$j("#assess_error").show();
	}
	else
		{
	if(userId>0)
		{
	var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
 	var value = field.GetHTML(true);
  	$j('#sectionContentsta').val(value);
  
  		$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#clinical").serialize(),
        success: function (result){
        }
        
	}); 
  	showModal('Email','sendMailHome?cdrCbuPojo.cordID='+cordId+'&mailCode='+code+'&cbuID='+regisId+'&userId='+userId,'350','450');
		}
	else
		{
		$j("#inform_error").show();
		}
		}
}
function insertData(msg)
{
	var visibleToTc="";
	var checked = $j('#visibility1:checked').val();
	if($j("#replyVal").val()!="true")
		{
	if(checked=='true')
		{
	var answer = confirm(msg);
	if(!answer)
		{
		
		$j("#visibility1").attr('checked', false);
		visibleToTc="false";
		}
	else
		{
		$j("#visibility1").attr('checked', true);
		visibleToTc="true";
		}
		}
	var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
 	var value = field.GetHTML(true);
 	$j('#sectionContentsta').val(value);
 	if($j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val()==null)
	{
		$j("#assess_error").show();
	
 	 if(value=='<br>'||value.length==0)
	{
		$j('#note_error').show();
		$j("#clinical").valid();
	}
	}
 	else if(value=='<br>'||value.length==0){
 		$j('#note_error').show();
		$j("#clinical").valid();
 	}

else
	{
	  if($j("#clinical").valid()){
		$j("#note_error").hide();
		$j("#assess_error").hide();
		var word = $j("#keyword").val();
		var pattern='!@*!';
		var pattern2='@!*@';
		word = word.replace('&', pattern);
		word = word.replace('%', pattern);
		$j("#keyword1").val(word);
		if(visibleToTc==""||visibleToTc=="true")
		{
		loadParticularDiv('saveClincalNotes','main','clinical','updateNotes','statusNotes');
		refreshMultipleDivsOnViewClinical('256','<s:property value="cdrCbuPojo.cordID"/>');
		//loadClinicalSubModuleData('<s:property value="cdrCbuPojo.cordID"/>','9','loadWholeNotesDiv','clinicalNotesparent','2','')
		}
	  }
	}
		}
	else
	{
	//console.log('inside elese block');
	var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
 	var value = field.GetHTML(true);
 	$j('#sectionContentsta').val(value);
 	 if(value=='<br>'||value.length==0)
 	{
 		$j('#note_error').show();
 		
 	}
 	 else{
 		$j('#note_error').hide();
			loadParticularDiv('saveClincalNotes','main','clinical','updateNotes','statusNotes');
			refreshMultipleDivsOnViewClinical('256','<s:property value="cdrCbuPojo.cordID"/>');
 	 		}
	}
}

function showfield()
{
	if($j('#checkbox').attr('checked'))
	{
	$j("#comment").show();
	$j("#commentText").show();
	}
	else
	{
		$j("#comment").hide();
		$j("#commentText").hide();
	}
}

function getKeyword()
{
	var word = $j("#keyword").val();
	while(word.indexOf(",")>0)
	{
		 word = word.replace(",", " ");
	}
	$j("#keyword").val(word);
}

function showFlag()
{
	$j("#assess_error").hide();
	if($j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val()=='no_cause')
	{
	$j("#checkbox").attr('disabled',false);	
	}
else
	{
	$j("#checkbox").attr('disabled',true);
	$j("#comment").hide();
	$j("#commentText").hide();
	$j('#checkbox').attr('checked',false);
	}	
}

function showCbuStatus(val){
	$j("#assess_error").hide();
	var regisId = $j("#regisId").val();
	var deferId = 'noteAssessment'+val;
	 if(val=='na' || val=='tu'){
		var url='updateCbuStatus?module=notes&clinicalNotePojo.noteAssessment='+val+'&licenceUpdate=False&cbustatusin=IS_LOCAL_REG&cdrCbuPojo.cordID='+$j("#cordNoteId").val()+'&orderId=&orderType=&applyResol=&iscordavailfornmdp=&deferId='+deferId;
		showCbuStatusModal('Update CBU Status For CBU Registry ID '+regisId,url,'450','600',val);
	} 
	
}
function showCbuStatusModal(title,url,height,width,val)
{
  
	$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/> <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	$j("#modelPopup2").html(progressMsg);
		$j("#modelPopup2").dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: true,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#modelPopup2").dialog("destroy");
		      		jQuery("#modelPopup2").html("");
				    }
				   }
				  ); 
		$j("#modelPopup2").dialog("open");
	$j.ajax({
        type: "POST",
        cache : false,		
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#modelPopup2").html(result);
	       	$j("#noteAssessment"+val).attr("checked","checked");      	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
}


$j(function(){
	$j("#clinical").validate({
		rules:{	
			"clinicalNotePojo.notes":{maxlength:4000},
			"clinicalNotePojo.reasonTu":{required:
			{
			  depends: function(element){
				  return ($j("#reasonTu").is(':visible'));
				}
			}	
		},
		"clinicalNotePojo.fkReason":{required:
		{
		  depends: function(element){
			  return ($j("#reasonNa").is(':visible'));
			}
		}	
	},

		"clinicalNotePojo.availableDate":{required:
		{	  depends: function(element){
				  return ($j("#datepicker6").is(':visible'));
				}
			}			
		}
			
		},
		messages:{	
			"clinicalNotePojo.notes": "<s:text name="garuda.cbu.clinicalnote.notes"/>",
			"clinicalNotePojo.fkReason": "<s:text name="garuda.cbu.clinicalnote.reason"/>",
			"clinicalNotePojo.availableDate": "<s:text name="garuda.common.validation.availabledate"/>",
			"clinicalNotePojo.reasonTu": "<s:text name="garuda.cbu.clinicalnote.reason"/>"
			
			
		}
			
		
		});
	});

/*
var today = new Date();

var d = today.getDate();
var m = today.getMonth();
var y = today.getFullYear();

var h=today.getHours();
var mn=today.getMinutes()+1;

$j( "#datepicker6" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
	changeYear: true});
*/
getDatePic();

</script>
<div id="clinicalNoteDiv" class='popupdialog tabledisplay ' >
<s:form id="clinical" name="clinical">
<s:hidden name="cdrCbuPojo.cordID" id="cordNoteId"></s:hidden>
<s:hidden name="clinicalNotePojo.pkNotes" />
<s:hidden name="clinicalNotePojo.fkNotesCategory" id="category" value="%{#request.categoryTypeID}" />
<s:hidden name ="clinicalNotePojo.fkNotesType" value="%{#request.noteCategory}"/>
<s:hidden name="cdrCbuPojo.registryId" id="regisId"></s:hidden>
<s:hidden name="loadDivName" ></s:hidden>
<s:hidden name="clinicalNotePojo.viewFlag" />
<s:hidden name="clinicalNotePojo.keyword" id="keyword1"/>
<s:hidden name="clinicalNotePojo.amended" />
	<div id="updateNotes" style="display: none;" >
	  <jsp:include page="../cb_update.jsp">
	    <jsp:param value="garuda.common.lable.note"  name="message"/>
	    <jsp:param value="notesModalsDiv"  name="divName"/>
	  </jsp:include>	   
	</div>

  <table width="100%" cellspacing="0" cellpadding="0" border="1" id="statusNotes">
  <tr>
   <td><span style="color: red;">
   <s:text name="garuda.clinicalnote.label.warning_msg"/>
	</span>	</td>				
   </tr>
  <tr>
   <td>
    <s:textarea id="sectionContentsta" name="clinicalNotePojo.notes"  rows="5" cols="50" wrap="hard" style="width:800;height:190;" onKeyDown="javascript:limitText(this,20);" 
onKeyUp="javascript:limitText();" ></s:textarea><span class="error">*</span>
	<div id='note_error' style="display: none;">
					<span style="color: red;"><s:text name="garuda.cbu.cordentry.reqnotes"/></span>
			</div>
    </td>
   </tr>
   <tr>
     <s:if test="clinicalNotePojo.reply!=true">
   <tr>
  <td  valign="middle">
  <fieldset style="display:inline-block" ><legend> <s:text  name="garuda.clinicalnote.label.assessment"></s:text>
				</legend>
		<table>
			<tr>
				<s:if test="cdrCbuPojo.site.isCDRUser()==true">
				<td valign="middle" ><s:radio name="clinicalNotePojo.noteAssessment" id="assess"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_ASSESS]" listKey="subType" listValue="description" id="noteAssessment" onclick="showCbuStatus(this.value)"  />
			<div id='assess_error' style="display: none;">
					<span style="color: red;"><s:text name="garuda.cbu.assessment.selectAssess"/></span>
			</div>
				</td>
				</s:if>
				<s:else>
				<td valign="middle" ><s:radio name="clinicalNotePojo.noteAssessment" id="assess"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_ASSESS]" listKey="subType" listValue="description" id="noteAssessment" onclick="showFlag(this.value)"  />
			<div id='assess_error' style="display: none;">
					<span style="color: red;"><s:text name="garuda.cbu.assessment.selectAssess"/></span>
			</div>
				</td>
				</s:else>
				<td>
								<s:checkbox name="clinicalNotePojo.flagForLater" id="checkbox"  value="clinicalNotePojo.flagForLater"  onclick="javascript:showfield();"></s:checkbox><s:text
								name="garuda.clinicalnote.label.flagForLater"></s:text>
							</td>	
				<td id = "commentText" style="display:none">
						<s:text name ="garuda.cbufinalreview.label.flaggedItems.comment"></s:text>
				</td>
				<td id = "comment" style="display:none">
						<s:textfield name="clinicalNotePojo.comments" maxlength="25" id="comment" />
				</td>
				 </tr>
					<%-- <tr>
								
				<td valign="middle">
					<div id="cbustat" style="display:none">
							<table>
								<tr>
										<td>
											<s:text name="garuda.clinicalnote.label.reason" />
											<span style="color: red;">*</span>
										</td>
										<td>
							 				<s:select name="clinicalNotePojo.fkReason" style="width:100px" id="reasonNa" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_REASON]"
											listKey="pkCodeId" listValue="description" headerKey=""
											headerValue="Select"  />
										</td>
										<td>
											<s:text name="garuda.clinicalnote.label.explain" />
										</td>
										<td>
							 				<s:textarea rows="1" cols="50" wrap="hard"  id="explain"  name="clinicalNotePojo.explaination" />
										</td>
								</tr>
							</table>
					</div>
				</td>
						</tr>
						<tr>	
				<td>
					<div id="tustat" style="display:none">
						<table >
							<tr>
									<td><s:text name="garuda.clinicalnote.label.avail_date" />
										<span style="color: red;">*</span>
									</td>
									<td>
								 		<s:textfield  id="datepicker6" maxlength="50" name="clinicalNotePojo.availableDate"  cssClass="datePicWMinDate"/>
									</td>
									<td>
										<s:text name="garuda.clinicalnote.label.reason" />
										<span style="color: red;">*</span>
									</td>
									<td>
							 			<s:select name="clinicalNotePojo.reasonTu" style="width:100px" id="reasonTu" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_REASON]"
										listKey="pkCodeId" listValue="description" headerKey=""
										headerValue="Select"  />
									</td>
									<td>
										<s:text name="garuda.clinicalnote.label.explain" />
									</td>
									<td>
							 				<s:textarea rows="1" cols="20" wrap="hard" name="clinicalNotePojo.explaination" />
									</td>
							
							</tr>
						</table>
					</div>
				</td>
								
		</tr> --%>
							
	</table>
</fieldset><span class="error">*</span>
						
   </td>
   </tr>
   </s:if>
   
    <s:if test="clinicalNotePojo.reply!=true">
   <tr>
   <td>
   <table cellspacing="0" class="displaycdr">
   <tr><td></td></tr><tr><td></td></tr>
   <tr>
   
    <td >
					<s:checkbox name="clinicalNotePojo.visibility" id="visibility1"   value="clinicalNotePojo.visibility"></s:checkbox><s:text
					name="garuda.clinicalnote.label.visibleToTc"></s:text>
	</td>
	<td valign="middle">
				<s:text name="garuda.clinicalnote.label.keyword"  />
	</td>
	<td>
				<s:textfield name="clinicalkeyword" maxlength="25" id="keyword" onblur="javascript:getKeyword();" style="width: 200px"  />
	</td>
	
	
<%-- 	 <td >
				<s:text name="garuda.clinicalnote.label.button.send_to" />	
				
	</td>
	 
   <td >
				<s:if test="userNameList!=null && userNameList.size()>0">
		<s:select list="UserNameList" name ="clinicalNotePojo.sentTo" style="width:200px" id="userList" headerKey="" headerValue="-Select-"
					value="" listKey="userId" listValue="firstName" onchange="clearError(this.id,this.value);"/>
		</s:if>
		<div id='inform_error' style="display: none;">
					<label class="error"><s:text name="garuda.cbu.assessment.user"/></label>
			</div>
				
	</td> --%>
	</tr>
		
	<tr >
	</table>
	</td>
   </tr>
   </s:if>
       <tr><td>
   <table>
      	<tr  <s:if test="#request.viewFlag==true">style="display: none;"</s:if>>
			<td><jsp:include page="../cb_esignature.jsp" flush="true"></jsp:include></td>
			    <td align="center">
						<button type="button" id="submit" disabled="disabled" onclick="insertData('<s:text name="garuda.clinicalnote.label.visibility_warning_msg"/>');" ><s:text
						name="garuda.common.save" ></s:text></button>
						<%-- <button type="button" id="inform" onclick="showEmail('<s:property value="cdrCbuPojo.cordID"/>','<s:property value="cdrCbuPojo.registryId" />','<s:property value="@com.velos.ordercomponent.util.VelosMailConstants@CLINICAL_NOTES"/>',this.id);" ><s:text
						name="garuda.clinicalnote.label.button.inform" ></s:text></button>	
						<button type="button" id="consult" onclick="showEmail('<s:property value="cdrCbuPojo.cordID"/>','<s:property value="cdrCbuPojo.registryId" />','<s:property value="@com.velos.ordercomponent.util.VelosMailConstants@CLINICAL_NOTES"/>',this.id);"><s:text
						name="garuda.clinicalnote.label.button.sendToConsult"></s:text></button>		 --%>	
    			<button type="button" onclick="closeModalNoteDiv();"><s:text name="garuda.common.close"></s:text></button>
    			</td>
		</tr>
		</table>
</td></tr> 
   <tr>
  <td>
  		
  			<div id="history" style=" width: 800px; height: 100px; overflow: auto;" > 
				<table>
				<s:set name="cbuCateg1" value="#request.categoryTypeID"/>
				<tbody>
						<s:iterator value="#request.categoryNotes[#cbuCateg1]" var="rowVal" status="row">
						<s:set name="amendFlag1" value="%{#rowVal[5]}" scope="request"/>
						<s:set name="noteSeq" value="%{#rowVal[2]}" scope="request"/>
						<s:set name="replyFlag" value="%{#rowVal[8]}" scope="request"/>
						<s:set name="notes" value="%{#rowVal[9]}" scope="request"/>
						<s:set name="flagForLater" value="%{#rowVal[10]}" scope="request"/>
							<tr>
								<s:iterator value="rowVal" var="cellValue" status="col">
								<s:if test="#request.replyFlag==true">
										<s:if test="%{#col.index == 0}">
												 <s:set name="cellValue1" value="cellValue" scope="request"/>
												 <td></td>
													<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
														String cellValue1 = request.getAttribute("cellValue1").toString();
														UserJB userB = new UserJB();
														userB.setUserId(EJBUtil.stringToNum(cellValue1));
														userB.getUserDetails();													
													%>		
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>										
													</td>
										</s:if>
										<s:if test="%{#col.index == 1}">
										<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text name="garuda.clinicalnote.label.posted_on"></s:text>
									  		<s:set name="cellValue15" value="cellValue" />
							       	 			<s:date name="cellValue15" id="cellValue15" format="MMM dd, yyyy / hh:mm:ss a"  />
												<s:property value="cellValue15"/>
										</td>
									</s:if>
											<s:if test="%{#col.index == 2}">
												<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.cdrcbuview.label.clinical_notes"></s:text>
											<s:text name="garuda.cbu.label.reClinical"/><s:property />
												</td>
										</s:if>								
								<s:if test="hasEditPermission(#request.vClnclNote)==true">
										<s:if test ="%{#col.index == 6}" ><td>
											<a href ="#" onClick="loadPageByGetRequset('revokeEditorNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.fkNotesCategory=<s:property value="#cbuCateg1"/>&clinicalNotePojo.pkNotes='+<s:property />,'clinicalNoteDiv')"> <s:text name="garuda.clinicalnote.label.revoke" /></a>
											
											</td>
											</s:if>
											</s:if>
											<s:if test ="%{#col.index == 6}" >
												<td>
												<a href ="#" onClick="showModal('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','viewNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>','500','850');"> <s:text name="garuda.label.dynamicform.view"/></a>
												
											</td>
											</s:if>
											<s:if test ="%{#col.index == 6}" >
											<td>
												
												<a href ="#" onClick="showModals('Reply Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','replyCategoryNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REPLY_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&fkNoteCategory=<s:property value="#request.cbuCateg1"/>&noteSeq=<s:property value="#request.noteSeq"/>','500','850','notesModalsDiv');"> <s:text name="garuda.clinicalnote.label.reply" /></a>
												
											</td>
											</s:if>	
										
								</s:if>
								<s:else>
								<s:if test="%{#col.index == 0}">
												 <s:set name="cellValue1" value="cellValue" scope="request"/>
													<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
														String cellValue1 = request.getAttribute("cellValue1").toString();
														UserJB userB = new UserJB();
														userB.setUserId(EJBUtil.stringToNum(cellValue1));
														userB.getUserDetails();													
													%>		
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>										
													</td>
								</s:if>
									<s:if test="%{#col.index == 1}">
									<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text name="garuda.clinicalnote.label.posted_on"></s:text>
									  <s:set name="cellValue15" value="cellValue" />
							       	 			<s:date name="cellValue15" id="cellValue15" format="MMM dd, yyyy / hh:mm:ss a"   />
												<s:property value="cellValue15"/>
									</td>
								</s:if>
								<s:if test="%{#col.index == 2}">
									<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.cbu.label.clinical"/><s:property />
									</td>
								</s:if>								
							
												<s:if test ="%{#col.index == 3}" >
												 <s:set name="cellValue3" value="cellValue" scope="request"/>
												<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if> style="padding:0">
														
						   										<s:text name="garuda.clinicalnote.label.assessment"></s:text>
														
											        						<s:set name="cellValue3" value="cellValue" scope="request"/> 
														         			 <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_ASSESS]" var="subType" >
														    				 
														    				
														    				<s:if test="subType==#cellValue">
														         			<s:property value="description"/>
														    				</s:if> 
																		</s:iterator>
											        					
												</td>
											</s:if>
											<s:if test ="%{#col.index == 4}" >
											<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if> style="padding:0"><s:set name="cellValue2" value="cellValue" scope="request"/> 
												
						   								<s:text name="garuda.clinicalnote.label.visibility"></s:text>
													
											    				<s:if test ="#request.cellValue2==true" >
											        			<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
											           			<s:text name="garuda.clinicalnote.label.visibleToTc" />
											        			</td>
											        			</s:if>
											        			<s:else>
											        				<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
											           			<s:text name="garuda.clinicalnote.label.notvisibleToTc" />
											        			</td>
											        			</s:else>
											   	 			
											</td>
											
													<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if> style="padding:0">
											      		 <s:text name="garuda.clinicalnote.label.flagForLater"></s:text>:
											           <s:if test="#request.flagForLater==true"><s:text name="garuda.clinicalnote.label.yes" /></s:if>
											           <s:else><s:text name="garuda.clinicalnote.label.no" /></s:else>
											       
													</td>
											</s:if>
											 <s:if test="hasEditPermission(#request.vClnclNote)==true">
											<s:if test ="%{#col.index == 6}" ><td>
											<a href ="#" onClick="loadPageByGetRequset('revokeEditorNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.fkNotesCategory=<s:property value="#cbuCateg1"/>&clinicalNotePojo.pkNotes='+<s:property />,'clinicalNoteDiv')"> <s:text name="garuda.clinicalnote.label.revoke" /></a>
											
											</td>
											</s:if></s:if>
											<s:if test ="%{#col.index == 6}" >
												<td>
												<a href ="#" onClick="showModal('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','viewNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>','500','850');"> <s:text name="garuda.clinicalnote.label.view" /></a>
												
											</td>
											</s:if>
											<s:if test="hasEditPermission(#request.vClnclNote)==true">
											<s:if test ="%{#col.index == 6}" >
											<td>
												
												<a href ="#" onClick="showModals('Reply Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','replyCategoryNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REPLY_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&fkNoteCategory=<s:property value="#request.cbuCateg1"/>&noteSeq=<s:property value="#request.noteSeq"/>','500','850','notesModalsDiv');"> <s:text name="garuda.clinicalnote.label.reply" /></a>
												
											</td>
											</s:if>	
											</s:if>
								</s:else>			
							</s:iterator>
							</tr>	
							<tr>
							<s:if test="#request.replyFlag==true">
							<td></td>
							</s:if>
								<td colspan="7" <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
									<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
								</td>
							</tr>
							</s:iterator>					
				</tbody>
				<tfoot>
					<tr>
						<td colspan="6"></td>
					</tr>
				</tfoot>
			</table>
			</div>
  	
  		
  </td>
  </tr>
  
  </table>

  </s:form>

</div>
<div id="modelPopup5"></div>
