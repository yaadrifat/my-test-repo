<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<jsp:include page="../cb_track_session_logging.jsp" />
<%String contextpath = request.getContextPath()+"/jsp/modal/"; %>
<script>
 $j(function(){
	 $j('#uploadtab').dataTable({
			"oLanguage": {
				"sEmptyTable": "<s:text name="garuda.cbu.uploaddocument.norecord"/>"
			},
	   "bRetrieve": true
		});
	 var tdObj = $j('#uploadtab').find('tbody').find("tr:eq(0)").find("td:eq(0)");
		if($j(tdObj).hasClass('dataTables_empty')){
			$j('#uploadtab_info').hide();
			$j('#uploadtab_paginate').hide();
		}
	      
	 });
	 
	 $j(function(){

		   $j('#uploadedDocuments').find(".portlet-header .ui-icon").bind("click",function() {
			   if($j(this).is(".ui-icon-minusthick") || $j(this).is(".ui-icon-plusthick"))
				{
					$j(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
					
					$j(this).parents(".portlet:first").find(".portlet-content").toggle();
					
					var divid = $j(this).parent().parent().attr("id");
					if(divid !="undefined" || divid !=null)
					divHeight=document.getElementById(divid).style.height;
					if(divHeight=='100%')
					{
						divHeight = 'auto';
						}	
				    $j(this).parents(".portlet:first").css('height',divHeight);
					
				}
			});
		 });
	 
</script>
<div class="column">
		<s:form id="uploadDocs" name="uploadDocs" method="post">
		<s:hidden name="cdrCbuPojo.cordID" id="cdrCbucordID"></s:hidden>	
		<s:hidden name="cdrCbuPojo.registryId" id="registryId"></s:hidden>
		<div id="link">
		 <table>
		 	<tr><td>
				<button type="button" onclick="UploadedShowModal('Upload Document for CBU Registry ID:<s:property value="cdrCbuPojo.registryId" />','uploaddocument?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>','500','600'); closeDialog('modelPopup2');">
				<s:text name="garuda.cdrcbuview.label.button.uploaddocument" /></button>
				<input type="button" style="border:3px solid  #dcdcdc;" onclick="fn_UploadedDocShowModals('Uploaded Documents','uploadedDocuments?cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />','400','900','modelPopup2');" 
					 value="<s:text name="garuda.uploaddoc.label.viewuploadeddocument" />"></input>
			</td>
			</tr>
		</table>
		</div>
		<div class="portlet" id="searchresultparent" >
			<div id="uploadedDocuments" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			<s:text name="garuda.uploaddoc.label.uploadeddocument" /> <span class="ui-icon ui-icon-minusthick"></span></div>			
			<div class="portlet-content">			
			<div id="searchTble">
			<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="uploadtab" >
				<thead>
					<tr>
						<!--<th><s:text name="garuda.cdrcbuview.label.registry_id" /></th>-->
						<th width="0%"></th>
						<th><s:text name="garuda.uploaddoc.label.date" /></th>
						<th><s:text name="garuda.uploaddoc.label.documentuploadedby" /></th>
						<th><s:text name="garuda.uploaddoc.label.documentcategory" /></th>
						<th><s:text name="garuda.uploaddoc.label.documentsubcategory" /></th>
						<th><s:text name="garuda.common.description" /></th>
						<th><s:text name="garuda.openorder.label.completiondate" /></th>
						<th><s:text name="garuda.uploaddoc.label.diffrntDates" /></th>
						<th><s:text name="garuda.uploaddoc.label.view" /></th>	
						 <th><s:text name="garuda.common.lable.modify" /></th>
						<th><s:text name="garuda.common.lable.revoke" /></th>			
					</tr>
				</thead>
				<tbody>
					<s:iterator value="uploadedDocumentLst" var="uploadedDoc" status="row">
						<tr id="<s:property	value="%{#row.index}"/>" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')" >
								<td width="0%"></td>
								<td ><s:date name="%{#uploadedDoc[0]}" id="lastModifiedOn1" format="MMM dd, yyyy / hh:mm:ss a" />
									 <s:property  value="%{lastModifiedOn1}" /></td>
									  <s:set name="cellValue1" value="%{#uploadedDoc[1]}" scope="request"/>
									 				<td>												
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>	
							    </td>		
								<s:if test ="#uploadedDoc[4]!=null"><s:set name="cellValue4" value="%{#uploadedDoc[4]}" scope="request"/>
								<td><s:property  value="getCodeListDescById(#request.cellValue4)" /></td>
								</s:if><s:else><td></td> </s:else>
								<s:if test="#uploadedDoc[5]!=null"><s:set name="cellValue5" value="%{#uploadedDoc[5]}" scope="request"/>
								<td><s:property  value="getCodeListDescById(#request.cellValue5)" /></td>
								</s:if><s:else><td></td> </s:else>
								<td><s:property value="%{#uploadedDoc[3]}" /></td>
								<td ><s:if test="#uploadedDoc[7]!=null "><s:date name="%{#uploadedDoc[7]}" id="CompletionDate1" format="MMM dd, yyyy" /><s:property  value="%{CompletionDate1}" /></s:if></td>
								<td><s:if test="#uploadedDoc[8]!=null "><s:date name="%{#uploadedDoc[8]}" id="ReportDate" format="MMM dd, yyyy" /><s:property  value="%{ReportDate}"/></s:if>
								<s:if test="#uploadedDoc[9]!=null"><s:date name="%{#uploadedDoc[9]}" id="ProcessDate" format="MMM dd, yyyy" /><s:property  value="%{ProcessDate}"/></s:if>
									<s:if test="#uploadedDoc[10]!=null"><s:date name="%{#uploadedDoc[10]}" id="TestDate" format="MMM dd, yyyy" /><s:property  value="%{TestDate}"/></s:if>
									<s:if test="#uploadedDoc[11]!=null"><s:date name="%{#uploadedDoc[11]}" id="RecievedDate" format="MMM dd, yyyy" /><s:property  value="%{RecievedDate}"/></s:if></td>
								
								<td><a href="#"><img src="images/attachment-icon.png"
								onclick="javascript: return showDocument('<s:property value="%{#uploadedDoc[2]}" />')" /></a></td>								
								<s:if test="(getCodeListSubTypeById(#request.cellValue4)=='lab_sum_cat'&& hasEditPermission(#request.viewLabSumCat)==true)||(getCodeListSubTypeById(#request.cellValue4)=='post_shpmnt'&& hasEditPermission(#request.viewPostShipCat)==true)||(getCodeListSubTypeById(#request.cellValue4)=='shpmnt_iter_cat'&& hasEditPermission(#request.viewShipmentCat)==true)
								||(getCodeListSubTypeById(#request.cellValue4)=='cbu_asmnt_cat'&& hasEditPermission(#request.viewAssmentCat)==true)||(getCodeListSubTypeById(#request.cellValue4)=='other_rec_cat'&& hasEditPermission(#request.viewOtherCat)==true)||(getCodeListSubTypeById(#request.cellValue4)=='unit_rprt_cat'&& hasEditPermission(#request.viewUnitRepCat)==true)||(getCodeListSubTypeById(#request.cellValue4)=='hlth_scren'&& hasEditPermission(#request.viewHealthHisCat)==true)||(getCodeListSubTypeById(#request.cellValue4)=='hla'&& hasEditPermission(#request.viewHlaCat)==true)
								||(getCodeListSubTypeById(#request.cellValue4)=='infect_mark'&& hasEditPermission(#request.viewIDMCat)==true)||(getCodeListSubTypeById(#request.cellValue4)=='eligibile_cat'&& hasEditPermission(#request.viewEligbCat)==true)||(getCodeListSubTypeById(#request.cellValue4)=='process_info'&& hasEditPermission(#request.viewProcesInfoCat)==true)||(getCodeListSubTypeById(#request.cellValue4)=='cbu_info_cat'&& hasEditPermission(#request.viewCbuInfocat)==true)||(getCodeListSubTypeById(#request.cellValue4)=='other_rec_cat'&& hasEditPermission(#request.viewOtherCat)==true)">
								<td><button type="button" onclick="javascript:modifyDocument('<s:property value="cdrCbuPojo.cordID"/>','<s:property value="%{#uploadedDoc[6]}"/>','<s:property value="cdrCbuPojo.registryId"/>','<s:property value="%{#uploadedDoc[2]}" />');"><s:text name="garuda.common.lable.modify" /></button></td>
								<td><button type="button" onclick="javascript: revokeDocumentForViewUpload('<s:property value="cdrCbuPojo.cordID"/>','<s:property value="%{#uploadedDoc[6]}"/>','<s:property value="%{#uploadedDoc[2]}" />','400','800')" ><s:text name="garuda.common.lable.revoke" /></button></td>						
						        </s:if>
						        <s:else>
						        <td><button type="button" disabled="disabled"><s:text name="garuda.common.lable.modify" /></button></td>
								<td><button type="button" disabled="disabled"><s:text name="garuda.common.lable.revoke" /></button></td>
						        </s:else>
						</tr>
					</s:iterator>
				</tbody>	
				<tfoot>
					<tr>
						<td colspan="6"></td>
					</tr>
				</tfoot>
			</table>
			</div>
			</div>
			</div>
			<button type="reset" onclick="closeDialog('modelPopup2')"><s:text
				name="garuda.common.close" /></button>
		</div>
	</s:form>
</div>
<script>
function fn_UploadedDocShowModals(title,url,hight,width,divid){
	var upiob = $j('#numberOnCbuBag').val();
	var upiob_data = '';
	if(upiob!="" && upiob != null && upiob != "undefined"){
		upiob_data = ', <s:text name="garuda.cbuentry.label.idbag"/>:'+upiob;
	}
	title = title + ' <s:text name="gaurda.common.cburegtxt"/><s:property value="cdrCbuPojo.registryId"/>'
	title = title + upiob_data;
	viewUploadedDocuments(title,url,hight,width,divid);
}
function UploadedShowModal(title,url,hight,width){
	var upiob = $j('#numberOnCbuBag').val();
	var upiob_data = '';
	if(upiob!="" && upiob != null && upiob != "undefined"){
		upiob_data = ', <s:text name="garuda.cbuentry.label.idbag"/>:'+upiob;
	}
	title = title + upiob_data;
	showModal(title,url,hight,width);
}
</script>