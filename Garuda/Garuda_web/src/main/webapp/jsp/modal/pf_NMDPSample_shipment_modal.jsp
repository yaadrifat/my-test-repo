<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<% String contextpath=request.getContextPath();%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<script type="text/javascript">
$j(function() {
	$j("#modalNMDPShipDate").removeAttr("readonly");
	getDatePic();
	getHiddedValues();
	disableShipmentDetEdit();
	disableExcuseFlagEdit();
	var validator = $j("#nmdpshipmentmodalform").validate({
		invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    }
	});
});
function disableShipmentDetEdit(){
	
	var checkedflag=$j("#modalNmdpShipExcuse").attr('checked');
	if(checkedflag){
		$j("#fkModalNmdpshipingCompanyId").attr('disabled','disabled');
		$j("#fkModalNmdpshipingCompanyId").val("");
		$j("#modalResearchSampleshipmentTrackingNo").attr('disabled','disabled');
		$j("#modalResearchSampleshipmentTrackingNo").val("");
		$j("#modalNMDPShipDate").attr('disabled','disabled');
		$j("#modalNMDPShipDate").val("");
		$j('#enterExcuseDt').show();
		$j("#shipcompmandEditspan").hide();
		$j("#excusedatemandEditspan").show();
	}else{
		$j("#fkModalNmdpshipingCompanyId").removeAttr('disabled');
		$j("#modalResearchSampleshipmentTrackingNo").removeAttr('disabled');
		$j("#modalNMDPShipDate").removeAttr('disabled');
		$j('#enterExcuseDt').hide();
		if($j("#modalNMDPShipDate").val()!=null && $j("#modalNMDPShipDate").val()!='' && $j("#modalNMDPShipDate").val()!='undefined'){
			$j("#shipcompmandEditspan").show();
		}
		$j("#excusedatemandEditspan").hide();
	}
	
}

function disableExcuseFlagEdit(){
	if($j("#modalNMDPShipDate").val()==null || $j("#modalNMDPShipDate").val()==''){
		$j("#modalNmdpShipExcuse").removeAttr('disabled');
		$j("#modalResearchExcuseDiv").show();
		$j('#enterExcuseDt').hide();
		$j("#shipcompmandEditspan").hide();
	}else{
		$j("#modalNmdpShipExcuse").attr('disabled','disabled');
		$j("#modalNmdpShipExcuse").removeAttr('checked');
		$j('#enterExcuseDt').hide();
		$j("#modalResearchExcuseDiv").hide();
		$j("#shipcompmandEditspan").show();
	}
	
}

function updateNmdpShipment(){
	if(validateNMDPShipDet() && $j("#nmdpshipmentmodalform").valid()){
	var checkedflag=$j("#modalNmdpShipExcuse").attr('checked');
	if(checkedflag){
		checkedflag='Y';
	}else{
		checkedflag='N';
	
	}
	var historyArr=new Array();
	var i=0; 
	var excDt=$j('#excuseDate').val();
		$j('.detailhstry').each(function(){
		if($j(this).is(':visible')){
			var idval=$j(this).attr("id");
			historyArr[i]=idval;
			i++;
		}
	}); 
	setTimeout(function(){
			loadMoredivs('updateNmdpshipment?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&shipcompany='+$j("#fkModalNmdpshipingCompanyId").val()+'&trackNo='+encodeURIComponent($j("#modalResearchSampleshipmentTrackingNo").val())+'&shipmentType='+$j("#pkNmdpShipment").val()+'&shipmentDate='+$j("#modalNMDPShipDate").val()+'&nmdpShipExcuse='+checkedflag+'&excuseDate='+excDt,'currentRequestProgress,taskCompleteFlagsDiv,chistory,cReqhistory,nmdpResearchSampleContentDiv','ororderdetailsForm','updatenmdpshipmentmodal','statusnmdpshipmentmodal');
			setpfprogressbar();
			getprogressbarcolor();
			historyTblOnLoad();
			disableExcuseFlag();
			disableShipmentDet();
		},0);
		setTimeout(function(){
		$j.each(historyArr,function(i,val){
			var Index=val.charAt(val.length-1);
			$j("#icondiv"+Index).triggerHandler('click');
		});
	},0);
	}
	
}

function validateNMDPShipDet(){
	var checkedflag=$j("#modalNmdpShipExcuse").attr('checked');
	if(($j("#modalNMDPShipDate").val()==null || $j("#modalNMDPShipDate").val()=='') && (checkedflag==null || checkedflag=='')){
		$j("#errornmdpShipmentDateDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.cbu.order.nmdpshipexcuse"/>");
		 });
		$j("#errornmdpShipmentDateDiv").show();
		return false;
		
	}
	else{
		$j("#errornmdpShipmentDateDiv").find("span").each(function(){
      		$j(this).text("");
	 });
		$j("#errornmdpShipmentDateDiv").hide();
	}
	if(($j("#modalNMDPShipDate").val()!=null && $j("#modalNMDPShipDate").val()!='') && 
		($j("#fkModalNmdpshipingCompanyId").val()==null || $j("#fkModalNmdpshipingCompanyId").val()=='')){
		$j("#errornmdpShipCompanyDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.cbu.order.selectShippingCompany"/>");
		 });
		$j("#errornmdpShipCompanyDiv").show();
		return false;
		
	}
	else{
		$j("#errornmdpShipCompanyDiv").find("span").each(function(){
      		$j(this).text("");
	 });
		$j("#errornmdpShipCompanyDiv").hide();
	}
	var excDt=$j('#excuseDate').val();
	
	if((checkedflag!=null && checkedflag!='') && (excDt!='' && excDt!=null)){
		$j("#errornmdpShipmentDateDiv").hide();
		$j("#errornmdpShipCompanyDiv").hide();
		$j("#excuseDataErrorDiv").hide();
		return true;
	}else if((checkedflag!=null && checkedflag!='') && (excDt=='' || excDt==null)){
		$j("#errornmdpShipmentDateDiv").hide();
		$j("#errornmdpShipCompanyDiv").hide();
		$j("#excuseDataErrorDiv").show();
		return false;
	}
	if((checkedflag!=null && checkedflag!='') ||
		 (($j("#modalNMDPShipDate").val()!=null && $j("#modalNMDPShipDate").val()!='') &&
		 ($j("#fkModalNmdpshipingCompanyId").val()!=null && $j("#fkModalNmdpshipingCompanyId").val()!=''))){
			$j("#errornmdpShipmentDateDiv").hide();
			$j("#errornmdpShipCompanyDiv").hide();
			$j("#excuseDataErrorDiv").hide();
			return true;
	}
	
}

</script>
<s:form name="nmdpshipmentmodalform" id="nmdpshipmentmodalform">
	<div id="updatenmdpshipmentmodal" style="display: none;">
	<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.message.modal.nmdpshipment" /></strong></p>
			</div>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td>
			<input type="button" onclick="closeModals('nmdpshipmodaldiv')" value="<s:text name="garuda.common.close"/>" />
		</td>
	</tr>
	</table>
	</div>
	<div id="errornmdpShipmentDateDiv"><span id="cbushipmentDatelbl"
		class="error"></span></div>
	<table width="100%" cellspacing="0" cellpadding="0" border="0"
		id="statusnmdpshipmentmodal">
		<tr>
			<td>
			<div class="portlet" id="nmdpresearchSampleDivparent">
			<div id="nmdpresearchSampleDiv"
				class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-print"
				onclick="clickheretoprint('nmdpresearchSampleDiv','<s:property value="cdrCbuPojo.registryId" />')"></span><span
				class="ui-icon ui-icon-newwin"></span> <!--<span class="ui-icon ui-icon-plusthick"></span>-->
			<!--<span class="ui-icon ui-icon-close"></span>--> <span
				class="ui-icon ui-icon-minusthick"></span> <s:text
				name="garuda.orOrderDetail.portletname.nmdpresearchsample" /></div>
			<div class="portlet-content" id="nmdpResearchSampleContentDiv">
			<s:hidden name="pkNmdpShipment" id="pkNmdpShipment"></s:hidden> <s:if
				test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
				<s:iterator value="ctOrderDetailsList" var="rowVal">
					<s:hidden name="nmdpSampleShipped" id="nmdpSampleShipped"
						value="%{#rowVal[61]}"></s:hidden>
				</s:iterator>
			</s:if>
			<table>

				<s:if
					test="nmdpShipmentInfoLst!=null && nmdpShipmentInfoLst.size()>0">
					<s:iterator value="nmdpShipmentInfoLst" var="rowVal">
						<tr>
							<td colspan="2">
							<table width="100%">
								<tr>
									<td width="25%"><strong><s:text
										name="garuda.cbuShipment.label.researchsampleshipdate" /></strong>:</td>
									<td width="25%"><s:if test="%{#rowVal[0]=='Jan 01, 1900'}">
										<s:date name="researchSampleShipDate" id="modalNMDPShipDate"
											format="MMM dd, yyyy" />
										<s:textfield name="shipmentPojo.researchSampleShipDate" readonly="true" onkeydown="cancelBack();" 
											class="datepic" id="modalNMDPShipDate"
											cssClass="datePicWOMinDate" value="" maxlength="12"
											onchange="disableExcuseFlagEdit();" onfocus="onFocusCall(this);"/>
											<span class='error fieldMan' style="display:none" id='dateMsg'>
											<br/><s:text name="garuda.label.dynamicform.testdatemsg"/>
											</span>
									</s:if> <s:else>
										<s:date name="researchSampleShipDate" id="modalNMDPShipDate"
											format="MMM dd, yyyy" />
										<s:textfield name="shipmentPojo.researchSampleShipDate" readonly="true" onkeydown="cancelBack();" 
											class="datepic" id="modalNMDPShipDate" disabled="false"
											cssClass="datePicWMinDate findlen" value="%{#rowVal[0]}"
											maxlength="12" onchange="disableExcuseFlagEdit();" onfocus="onFocusCall(this);"/>
											<span class='error fieldMan' style="display:none" id='dateMsg'>
											<br/><s:text name="garuda.label.dynamicform.testdatemsg"/>
											</span>
									</s:else></td>
									<td width="25%"><strong><s:text
										name="garuda.cbuShipment.label.shipcompany" /></strong>:<span id="shipcompmandEditspan" 
										style="color: red;">*</span></td>
									<td width="25%"><s:select
										name="shipmentPojo.fkshipingCompanyId"
										id="fkModalNmdpshipingCompanyId"
										list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPER]"
										listKey="pkCodeId" listValue="description" headerKey=""
										headerValue="Select" value="%{#rowVal[2]}"></s:select>
									<div id="errornmdpShipCompanyDiv"><span
										id="cbushipCompanylbl" class="error"></span></div>
									</td>
								</tr>
								<tr>
									<td width="25%"><s:if
										test="%{#rowVal[3]!=null && #rowVal[3]!=''}">
										<strong><a href="#"
											onclick='connectToShipperWebsite($j("#fkModalNmdpshipingCompanyId").val(),$j("#modalResearchSampleshipmentTrackingNo").val());'><s:text
											name="garuda.ctShipmentInfo.label.trackingno" /></a></strong>:
    						</s:if> <s:else>
										<strong><s:text
											name="garuda.ctShipmentInfo.label.trackingno" /></strong>:
    						</s:else></td>
									<td width="25%"><s:textfield
										name="shipmentPojo.researchSampleshipmentTrackingNo"
										id="modalResearchSampleshipmentTrackingNo"
										value="%{#rowVal[3]}" maxlength="30" /></td>
									<td width="50%" colspan="2"><s:if
										test='%{#rowVal[23]!=null && #rowVal[23]=="Y"}'>
										<s:checkbox name="modalNmdpShipExcuse"
											id="modalNmdpShipExcuse" checked="true"
											onclick="disableShipmentDetEdit();">
											<strong><s:text
												name="garuda.cbuShipment.label.samplenotshippedexcuse"></s:text></strong>
										</s:checkbox>
									</s:if> <s:else>
										<s:checkbox name="modalNmdpShipExcuse"
											id="modalNmdpShipExcuse" onclick="disableShipmentDetEdit();">
											<strong><s:text
												name="garuda.cbuShipment.label.samplenotshippedexcuse"></s:text></strong>
										</s:checkbox>
									</s:else></td>
								</tr>
								<tr id="enterExcuseDt">
								<td width="25%"><strong><s:text name="garuda.cbuShipment.label.excuseformsubmitdatetime"/></strong>:<span id="excusedatemandEditspan" 
										style="color: red;">*</span></td>
								<td width="25%"><s:date name="excuseDate"
									id="excuseDate" format="MMM dd, yyyy" /> 
									<s:if test='%{#rowVal[33]==null || #rowVal[33]=="" || #rowVal[33]=="Jan 01, 1900"}'>
										<s:textfield readonly="true" onkeydown="cancelBack();"  name="excuseDate"
									class="datepic" id="excuseDate"	cssClass="datePicWOMinDate" value=""/>
									</s:if>
									<s:else>
										<s:textfield readonly="true" onkeydown="cancelBack();"  name="excuseDate"
									class="datepic" id="excuseDate"	cssClass="datePicWOMinDate" value="%{#rowVal[33]}"/>
									</s:else>
								</td>
								<td width="25%" colspan="2"><span id="excuseDataErrorDiv" style="display:none;" class="error"><s:text name="garuda.cbuShipment.label.errorexcuseformsubmitdatetime"/></span></td>
								</tr>
								<tr>
									<td width="100%" colspan="4"><a href="#"
										onclick="readNMDPXML('2')"><strong><s:text
										name="garuda.cbuShipment.label.researchexcuseform"></s:text></strong></a></td>
								</tr>
							</table>
							<table bgcolor="#cccccc" class="tabledisplay disable_esign"
								width="100%">
								<tr bgcolor="#cccccc" valign="baseline">
									<td width="70%"><jsp:include page="../cb_esignature.jsp"
										flush="true">
										<jsp:param value="editNMDPSampleShipment" name="submitId" />
										<jsp:param value="invalidSampleShipment" name="invalid" />
										<jsp:param value="minimumSampleShipment" name="minimum" />
										<jsp:param value="passSampleShipment" name="pass" />
									</jsp:include></td>
									<td align="left" width="30%"><input type="button"
										id="editNMDPSampleShipment" onclick="updateNmdpShipment();"
										value="Edit" disabled="disabled" />
									<input type="button"  onclick="closeModals('nmdpshipmodaldiv')" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />
									</td>
								</tr>
							</table>
							</td>
						</tr>
					</s:iterator>
				</s:if>
				<s:else>
					<tr>
						<td colspan="2">
						<table width="100%">
							<tr>
								<td width="25%"><strong><s:text
									name="garuda.cbuShipment.label.researchsampleshipdate" /></strong>:</td>
								<td width="25%"><s:date name="researchSampleShipDate"
									id="modalNMDPShipDate" format="MMM dd, yyyy" /> <s:textfield
									readonly="true" onkeydown="cancelBack();"  name="shipmentPojo.researchSampleShipDate"
									class="datepic" id="modalNMDPShipDate" onfocus="onFocusCall(this);"
									cssClass="datePicWOMinDate" value=""
									onchange="disableExcuseFlagEdit();"/></td>
								<td width="25%"><strong><s:text
									name="garuda.cbuShipment.label.shipcompany" /></strong>:<span id="shipcompmandEditspan" 
										style="color: red;">*</span></td>
								<td width="25%"><s:select
									name="shipmentPojo.fkshipingCompanyId"
									id="fkModalNmdpshipingCompanyId"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPER]"
									listKey="pkCodeId" listValue="description" headerKey=""
									headerValue="Select"
									value="<s:property value='defaultshipper'/>"></s:select>
								<div id="errornmdpShipCompanyDiv"><span
									id="cbushipCompanylbl" class="error"></span></div>
								</td>
							</tr>
							<tr>
								<td width="25%"><strong><s:text
									name="garuda.ctShipmentInfo.label.trackingno" /></strong>:</td>
								<td width="25%"><s:textfield
									name="shipmentPojo.researchSampleshipmentTrackingNo"
									id="modalResearchSampleshipmentTrackingNo" value=""
									maxlength="30" /></td>
								<td width="50%" colspan="2"><s:checkbox
									name="modalNmdpShipExcuse" id="modalNmdpShipExcuse"
									onclick="disableShipmentDetEdit();">
									<strong><s:text
										name="garuda.cbuShipment.label.samplenotshippedexcuse"></s:text></strong>
								</s:checkbox></td>

							</tr>
							<tr id="enterExcuseDt">
								<td width="25%"><strong><s:text name="garuda.cbuShipment.label.excuseformsubmitdatetime"/></strong>:<span id="excusedatemandEditspan" 
										style="color: red;">*</span></td>
								<td width="25%"><s:date name="excuseDate"
									id="excuseDate" format="MMM dd, yyyy" /> <s:textfield
									readonly="true" onkeydown="cancelBack();"  name="excuseDate"
									class="datepic" id="excuseDate"
									cssClass="datePicWOMinDate" value=""
									/></td>
								<td width="25%" colspan="2"><span id="excuseDataErrorDiv" style="display:none;" class="error"><s:text name="garuda.cbuShipment.label.errorexcuseformsubmitdatetime"/></span></td>
								
							</tr>
							<tr>
								<td width="100%" colspan="4" id="modalResearchExcuseDiv"><a
									href="#" onclick="readNMDPXML('2')"><strong><s:text
									name="garuda.cbuShipment.label.researchexcuseform"></s:text></strong></a></td>
							</tr>
							
							
						</table>
						<table bgcolor="#cccccc" class="tabledisplay disable_esign"
							width="100%">
							<tr bgcolor="#cccccc" valign="baseline">
								<td width="70%"><jsp:include page="../cb_esignature.jsp"
									flush="true">
									<jsp:param value="submitNMDPSampleShipment" name="submitId" />
									<jsp:param value="invalisNMDPSampleShipment" name="invalid" />
									<jsp:param value="minimumNMDPSampleShipment" name="minimum" />
									<jsp:param value="passNMDPSampleShipment" name="pass" />
								</jsp:include></td>
								<td align="left" width="30%"><input type="button"
									id="submitNMDPSampleShipment" onclick="updateNmdpShipment();"
									value="Submit" disabled="disabled" />
								<input type="button"  onclick="closeModals('nmdpshipmodaldiv')" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />	
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</s:else>
			</table>
			</div>
			</div>
			</div>
			</td>
		</tr>
	</table>
</s:form>