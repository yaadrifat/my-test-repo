<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<%@ page import="com.velos.ordercomponent.action.VelosBaseAction"%>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<jsp:include page="../cb_track_session_logging.jsp" />
<%
	String contextpath = request.getContextPath();
%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<%@ page import="com.velos.eres.web.grpRights.GrpRightsJB" %>

<%
String message = request.getParameter("message");
request.setAttribute("message",message);
%><!-- 
$j(function(){
				$j('#searchOpenOrders2').validate({
				rules:{	
					"revokeToId":{required:true,checkdbdata2:["CdrCbu","registryId"]}
				}
			messages:{
				"revokeToId":{required:"Please enter To Id",checkdbdata2:"Not a valid registery Id"}
			}
				});
			
			});  -->
<s:if test="#request.subcatid!=null">
	<script>
	     $j('#testname').css('display','block');
	     $j('#descript').css('display','block');
	     $j('#com_date').css('display','block');	 
	     $j('#subcatid').val('<s:property value="#request.subcatid" />');  
	     $j('#categoryId').val('<s:property value="#request.categoryId" />');   
	     //getDatePic();
	   </script>
</s:if>
<s:if test="#request.modifyUnitrpt=='True'">

			<script>
				$j(function(){
				$j('#searchOpenOrders2').validate({
				rules:{	
					"categoryName":{checkselect:true},
					"labTestName":{checkselect:
					{
					  depends: function(element){
						  return ($j("#labTestName").is(':visible'));
							}
						}
					},
					"cbUploadInfoPojo.strcompletionDate":{required:
					{
					  depends: function(element){
						  return ($j("#com_date").is(':visible'));
							}
						},dpDate:true
					},
					"cbUploadInfoPojo.strprocessDate":{required:
					{
					  depends: function(element){
						  return ($j("#pro_date").is(':visible'));
							}
						},dpDate:true
					},
					"cbUploadInfoPojo.strreceivedDate":{required:
					{
					  depends: function(element){
						  return ($j("#rec_date").is(':visible'));
							}
						},dpDate:true
					},
					/*"cbUploadInfoPojo.veficationTyping":{checkselect:
					{
					  depends: function(element){
						  return ($j("#ver_type").is(':visible'));
							}
						}
					},*/
					"cbUploadInfoPojo.description":"required",
					"cbUploadInfoPojo.strtestDate":{required:
					{
					  depends: function(element){
						  return ($j("#test").is(':visible'));
							}
						},dpDate:true
					},
					"cbUploadInfoPojo.strreportDate":{required:
					{
					  depends: function(element){
						  return ($j("#repo_date").is(':visible'));
							}
						},dpDate:true
					},
					"revokeToId":{required:true,checkdbdata2:["CdrCbu","registryId"]}
				},
			messages:{
					"categoryName":" <s:text name="garuda.common.validation.categoryname"/>",
					"labTestName":"<s:text name="garuda.common.validation.labtestname"/>",
					"cbUploadInfoPojo.description":"<s:text name="garuda.common.validation.description"/>",
					"cbUploadInfoPojo.strcompletionDate":{required:"<s:text name="garuda.common.validation.completionDate"/>"},
					"cbUploadInfoPojo.strtestDate":{required:"<s:text name="garuda.common.validation.testdate"/>"},
					"cbUploadInfoPojo.strprocessDate":{required:"<s:text name="garuda.common.validation.processdate"/>"},
					"cbUploadInfoPojo.strreceivedDate":{required:"<s:text name="garuda.common.validation.receiveddate"/>"},
					"cbUploadInfoPojo.reportDate":{required:"<s:text name="garuda.common.validation.reportdate"/>"},
				"revokeToId":{required:"<s:text name="garuda.cbu.upload.toId"/>",checkdbdata2:"<s:text name="garuda.cbu.upload.validCbuRegId"/>"}
			}
				});
			
			});  

				$j("#revokeToId").blur(function(e) {
						  var val = $j("#revokeToId").val();
						  val = val.replace(/-/g,"");
						  var sub1 = val.substring(0,4);
						  var sub2 = val.substring(4,8);
						  var sub3 = val.substring(8);
				  		 $j("#revokeToId").val(sub1+"-"+sub2+"-"+sub3);
				});
				 </script>
			</s:if>
<script>
 
function constructTable() {
}
 $j(document).ready(function(){

	 /**
	  * Character Counter for inputs and text areas showing characters left.
	  */
	 $j('#word_count').each(function(){
	     //maximum limit of characters allowed.
	     var maxlimit = 200;
	     // get current number of characters
	     var length = $j(this).val().length;
	     if(length >= maxlimit) {
	   $j(this).val($j(this).val().substring(0, maxlimit));
	   length = maxlimit;
	  }
	     // update count on page load
	     $j(this).parent().find('#counter').html( (maxlimit - length) + ' <s:text name="garuda.cbu.cordentry.charleft"/>');
	     // bind on key up event
	     $j(this).keyup(function(){
	  // get new length of characters
	  var new_length = $j(this).val().length;
	  if(new_length >= maxlimit) {
	    $j(this).val($j(this).val().substring(0, maxlimit));
	    //update the new length
	    new_length = maxlimit;
	   }
	  // update count
	  $j(this).parent().find('#counter').html( (maxlimit - new_length) + ' <s:text name="garuda.cbu.cordentry.charleft"/>');
	     });
	 });
	 getDatePic();
	 //---
});
  var subtype="";

  
  function getCodelistValues(value){
		 var contextpath = "<%=contextpath%>";
		 var subtype = "";
		 
		 $j.ajax({
				type : "GET",
				async : false,
				url : 'getSubtype?catPkCodeId='+ value,
				success : function(result) {
				//alert(result.subtype);
				subtype = result.subtype;	
				}
			
			
			
			});
		  return subtype;
	} 

  function showFields(){


			var catPkCodeId=document.getElementById("categoryName").value;
			var categoryName=$j("select[id$='categoryName'] :selected").text();
			subtype= getCodelistValues(catPkCodeId);
			//to populate testname dropdown

			if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE" />' || subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_INFORMATION_CODESUBTYPE" />' 
		     || subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE" />'|| subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_INFO_CODESUBTYPE" />'
		    	 ||subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_ASMNT_CODESUBTYPE"/>' || subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPMENT_ITER_CODESUBTYPE"/>')
		    {		
			  	$j('#testname').css('display','none');
			  	
			}
			else{
				if(catPkCodeId!=-1){
				
					//$j('#testname').css('display','block');
					loaddiv('populateDropDown?opt='+catPkCodeId,'testname');	
					$j('#testname').css('display','block');
						
				
				}else{
					
					$j('#testname').css('display','none');
						
					}

			 }
			
		//to show other fields
		if((categoryName!='Select'))
		{
		  document.getElementById("descript").style.display = 'block';
		  if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE" />'
	             || subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_ASMNT_CODESUBTYPE"/>'){
		 		  
				  document.getElementById("com_date").style.display = 'block';  
		 		 }
		 	 	else{
				  document.getElementById("com_date").style.display = 'none';  
		  			}
		 	  if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE" />'|| subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@INFECT_DIS_MARK_CODESUBTYPE" />' ||subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE" />'|| 
		 			  subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_CODESUBTYPE" />'){
				  document.getElementById("test").style.display = 'block'; 
			 	 }
			  else{
				  document.getElementById("test").style.display = 'none';  
			  	}
		 	 if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_INFO_CODESUBTYPE" />'){
				  document.getElementById("pro_date").style.display = 'block'; 
			 	 }
			  else{
				  document.getElementById("pro_date").style.display = 'none';  
			  	}
		 	if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_INFORMATION_CODESUBTYPE"/>'){
				  
				  document.getElementById("repo_date").style.display = 'block';
			 	 }
			  else{
				  document.getElementById("repo_date").style.display = 'none'; 
			  	}
		 	if(subtype!='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_REC_CODESUBTYPE" />'){
				  document.getElementById("rec_date").style.display = 'none'; 
			 	 }
		 	
		}
		else{
		
			 document.getElementById("descript").style.display = 'none';
			 document.getElementById("com_date").style.display = 'none';
			 document.getElementById("test").style.display = 'none';  
			 document.getElementById("pro_date").style.display = 'none'; 
			 document.getElementById("rec_date").style.display = 'none';
			 document.getElementById("repo_date").style.display = 'none'; 
			
			 
		}
	
	}
	
  function showFieldsSubCat(){
		
		var subcatPkCodeId=document.getElementById("labTestName").value;
		
		var subtype1=getCodelistValues(subcatPkCodeId);
		
			if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_CODESUBTYPE" />' && (subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE" />' || subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@PATIENT_CODESUBTYPE" />'))
				{
				  document.getElementById("ver_type").style.display = 'block';  			
					  
				}
			 else{
					  document.getElementById("ver_type").style.display = 'none';
					 
					 
				  	}	 	
			if(subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_REC_CODESUBTYPE" />' &&(subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@OR_RELE_MED_REC_CODESUBTYPE" />' ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_RECORD_CODESUBTYPE" />'))
					{
					  document.getElementById("rec_date").style.display = 'block';  
					}
				 else{
						  document.getElementById("rec_date").style.display = 'none';  
						
					  	}
			if((subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@HEALTH_HISt_SCR_CODESUBTYPE" />' &&(subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@MRQ_CODESUBTYPE" />' ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FMHQ_CODESUBTYPE" />'
				 ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_CODESUBTYPE_HLTH" />'))
				 ||(( subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE"/>' &&(subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_RISK_QUESTIONAAIRE" />' ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@URG_MED_NEED_CODESUBTYPE" />'
				 ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FINAL_DEC_OF_ELIG_CODESUBTYPE" />' ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@OTHER_ELIG_CODESUBTYPE" />'))) 
				 || (subtype=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@POST_SHPMNT_CODESUBTYPE" />' 
				 &&(subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_RECEIPT_CODESUBTYPE" />' ||subtype1=='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@POST_SHPMNT_OTHER_CODESUBTYPE" />')))
			{
			  document.getElementById("com_date").style.display = 'block';  
			}
			else{
				  document.getElementById("com_date").style.display = 'none';  
				
			 }
	}



  function saveCategoryName(obj){
		var categoryName=document.forms["searchOpenOrders2"].elements["categoryName"].value;
	 	document.forms["searchOpenOrders2"].elements["categoryId"].value=categoryName;	 	
	}
	function checkLabTestName(comp) {
		
		
		var subCategoryName=document.forms["searchOpenOrders2"].elements["labTestName"].value;
		
		 //alert(subCategoryName);
			document.forms["searchOpenOrders2"].elements["subcatid"].value=subCategoryName;	
	}	


	function saveOrderAttachment(){
		var iframe = document.getElementById('frame1');
		var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
			
		
		if($j("#searchOpenOrders2").valid()){
			
			if($j("#browseFlag").val()==1){
				if(innerDoc.getElementById('fileUploadStatus').value == 1) {

		    		document.getElementById('frame1').contentWindow.getFileStatus();

					if(innerDoc.getElementById('FileId1').value!=null && innerDoc.getElementById('FileId1').value!=''){
						
						document.getElementById('frame1').contentWindow.document.attachdocument.submit(); 
						
							//alert("File Uploaded Successfully!");
				     		//saveCall();
			
				        } 
					 else {
						 alert('<s:text name="garuda.cbu.openorder.uploadFailed"/>');
					 }
				}
				else{
					alert('<s:text name="garuda.cbu.openorder.uploadFailed"/>');					
				}
			
	    }
			else{
				saveCall();
			}
	}
	}

	function saveFinalOrderAttachment(){
		var iframe = document.getElementById('frame1');
		var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
		if($j("#searchOpenOrders2").valid()){
					
					if($j("#browseFlag").val()==1){
						if(innerDoc.getElementById('fileUploadStatus').value == 1) {

				    		document.getElementById('frame1').contentWindow.getFileStatus();
							
							if(innerDoc.getElementById('FileId1').value!=null && innerDoc.getElementById('FileId1').value!=''){
								$j("#saveFinalOrderAttachFlagId").val(1);
								document.getElementById('frame1').contentWindow.document.attachdocument.submit(); 

								//alert("File Uploaded Successfully!");	

								//loadDivWithFormSubmit('saveFinalEligibileAttachment','eligibleDiv','searchOpenOrders2');
								//document.getElementById("document").style.display = 'none';
					
						        } 
							 else {
								 alert('<s:text name="garuda.cbu.openorder.uploadFailed"/>');
							 }
						}
						else{
							alert('<s:text name="garuda.cbu.openorder.uploadFailed"/>');				
						}
					
			    }
					else{
						loadDivWithFormSubmit('saveFinalEligibileAttachment','modelPopup1','searchOpenOrders2');
						document.getElementById("document").style.display = 'none';
					}
			}
	}
	
	function modifyUnitrptDoc(cbuid){
		if($j("#searchOpenOrders2").valid()){
		 loadDivWithFormSubmit('modifyUnitrptDoc?cbuid='+cbuid,'unitReportDiv','searchOpenOrders2');
		 modalFormSubmitRefreshDiv('viewreport','refreshMain?cordId='+cbuid,'main');
		}
		 /* $j.ajax({
   			type : "POST",
   			async : false,
   			url : 'modifyUnitrptDoc',
   			data : $j("#searchOpenOrders2").serialize(),
   			success : function(result) {
   				//showUnitReport("close");
   				
   				$j('#searchOpenOrders2').html(result);
   			}
			
		}); */
		
		
	}

function revokeDocumentForViewUpload(cdrCbuId,attachmentId,dcmsAttachId,lenght,breadth){
	
	 	var orderId=$j("#orderId").val();
		var orderType=$j("#orderType").val();	
		var answer = confirm('Please confirm if you want to revoke the document.');
		if (answer){	
					if(orderId!=null && orderId!="" && orderId!="undefined" && orderType!=null && orderType!="" && orderType!="undefined"){
							var url='revokeDocFromPF?orderId='+orderId+'&orderType='+orderType+'&cbuCordId='+cdrCbuId+'&cbuid='+cdrCbuId+'&cordId='+cdrCbuId+'&pkcordId='+cdrCbuId+'&cordID='+cdrCbuId+'&attachmentId='+attachmentId;
							
							loadMoredivs(url,'DUMNDiv,quicklinksDiv,shipmentItinerarychkIdDiv','searchOpenOrders2','','');
							if ($j("#modelPopup2").dialog("isOpen")===true) {
								 url1='uploadedDocuments?cordID='+$j("#pkcordId").val()+'&cdrCbuPojo.cdrCbuId='+cdrCbuId;
								 loaddiv(url1,'uploadedDocuments');
							 }
							alert("Document revoked successfully");
						}
					else{
					
							var url='revokeDoc?cbuCordId='+cdrCbuId+'&cbuid='+cdrCbuId+'&cordId='+cdrCbuId+'&pkcordId='+cdrCbuId+'&cordID='+cdrCbuId+'&attachmentId='+attachmentId;
							
							loadMoredivs(url,'DUMNDiv,quicklinksDiv,loadCbuDocInfoDiv,loadEligibilityDocInfoDiv,loadLabSummaryDocInfoDiv,loadProcessingDocInfoDiv,loadHlaDocInfoDiv,loadIdmDocInfoDiv,loadHealthHistroyDocInfoDiv,loadOthrDocInfoDiv,cbuAssmentEligibleDiv','searchOpenOrders2','','');
							if ($j("#modelPopup2").dialog("isOpen")===true) {
								 url1='uploadedDocuments?cordID='+$j("#pkcordId").val()+'&cdrCbuPojo.cdrCbuId='+cdrCbuId;
								
								 loaddiv(url1,'uploadedDocuments');
							 }
							alert("Document revoked successfully");
					}
		}
		$j(function(){
			 $j('#uploadtab').dataTable({
					"oLanguage": {
						"sEmptyTable": "<s:text name="garuda.cbu.uploaddocument.norecord"/>"
					},
			   "bRetrieve": true
				});
			 var tdObj = $j('#uploadtab').find('tbody').find("tr:eq(0)").find("td:eq(0)");
				if($j(tdObj).hasClass('dataTables_empty')){
					$j('#uploadtab_info').hide();
					$j('#uploadtab_paginate').hide();
				}
			      
			 });
}

	function modifyDoc(cbuid){
		if($j("#searchOpenOrders2").valid()){
		 //loadDivWithFormSubmit('modifyDoc?cbuid='+cbuid,'document','searchOpenOrders2');
	//added byb IDrush
			 var orderId=$j("#orderId").val();
			var orderType=$j("#orderType").val();
				if(orderId!=null && orderId!="" && orderId!="undefined" && orderType!=null && orderType!="" && orderType!="undefined"){
					loadMoredivs('modifyDocFormPF?orderId='+orderId+'&orderType='+orderType+'&pkcordId='+$j("#pkcordId").val()+'&cordID='+$j("#pkcordId").val()+'cbuid='+cbuid,'DUMNDiv,quicklinksDiv,shipmentItinerarychkIdDiv','searchOpenOrders2','','');
				}else{
					loadMoredivs('modifyDoc?pkcordId='+$j("#pkcordId").val()+'&cordID='+$j("#pkcordId").val()+'&cbuid='+cbuid,'DUMNDiv,quicklinksDiv,loadCbuDocInfoDiv,loadEligibilityDocInfoDiv,loadLabSummaryDocInfoDiv,loadProcessingDocInfoDiv,loadHlaDocInfoDiv,loadIdmDocInfoDiv,loadHealthHistroyDocInfoDiv,loadOthrDocInfoDiv,cbuAssmentEligibleDiv','searchOpenOrders2','','');
				}
	
	//added by Idrush
		 
		 if ($j("#modelPopup2").dialog("isOpen")===true) {
			 url="uploadedDocuments?cordID="+cbuid+"&cdrCbuPojo.cdrCbuId="+cbuid;
		 	 modalFormSubmitRefreshDiv('searchOpenOrders2',url,'modelPopup2');
		 }
		// refreshMultipleDivsOnViewClinical('1775',cbuid);
			$j('#modifyunitrptMsg').show();
			document.getElementById("document").style.display = 'none';
		}
	}
		
	function saveCall(){
	
	var orderId=$j("#orderId").val();
	var orderType=$j("#orderType").val();
		//modalFormSubmitRefreshDiv('searchOpenOrders2','saveAttachment1','main');	
		//loadDivWithFormSubmit('saveAttachment1?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val(),'searchOpenOrders2','searchOpenOrders2');
		//loadParticularDiv('saveAttachment1?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val(),'searchOpenOrders2','searchOpenOrders2','','');
		if(orderId!=null && orderId!="" && orderId!="undefined" && orderType!=null && orderType!="" && orderType!="undefined"){
			loadMoredivs('saveAttachmentFromPF?orderId='+orderId+'&orderType='+orderType+'&pkcordId='+$j("#pkcordId").val()+'&cordID='+$j("#pkcordId").val(),'DUMNDiv,quicklinksDiv,shipmentItinerarychkIdDiv','searchOpenOrders2','','');
		}else{
			loadMoredivs('saveAttachment1?pkcordId='+$j("#pkcordId").val()+'&cordID='+$j("#pkcordId").val(),'DUMNDiv,quicklinksDiv,loadCbuDocInfoDiv,loadEligibilityDocInfoDiv,loadLabSummaryDocInfoDiv,loadProcessingDocInfoDiv,loadHlaDocInfoDiv,loadIdmDocInfoDiv,loadHealthHistroyDocInfoDiv,loadOthrDocInfoDiv,cbuAssmentEligibleDiv','searchOpenOrders2','','');
			}
		
		//refreshMultipleDivsOnViewClinical('1775','<s:property value="cdrCbuPojo.cordID"/>');
		
		$j('#update1').show();
		document.getElementById("document").style.display = 'none';
      
	}

	function saveCall1(){
		loadDivWithFormSubmit('saveFinalEligibileAttachment?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val(),'eligibleDiv','searchOpenOrders2');
		document.getElementById("document").style.display = 'none';
	}


	function resetField(){
		//$j("#datepicker2").reset();
		document.getElementById('datepicker2').value="";
		document.getElementById('datepicker3').value="";
		document.getElementById('datepicker4').value="";
		document.getElementById('datepicker5').value="";
	}


	$j(function(){

	/*	jQuery.validator.addMethod("checkselect", function(value, element) {
			return (true && (parseInt(value) != -1));
		}, "Please Select Value");*/
		
		$j("#searchOpenOrders2").validate({
			
			rules:{			
					
					"categoryName":{checkselect:true},
					"labTestName":{checkselect:
					{
					  depends: function(element){
						  return ($j("#labTestName").is(':visible'));
							}
						}
					},
					"cbUploadInfoPojo.strcompletionDate":{required:
					{
					  depends: function(element){
						  return ($j("#com_date").is(':visible'));
							}
						},dpDate:true
					},
					"cbUploadInfoPojo.strprocessDate":{required:
					{
					  depends: function(element){
						  return ($j("#pro_date").is(':visible'));
							}
						},dpDate:true
					},
					"cbUploadInfoPojo.strreceivedDate":{required:
					{
					  depends: function(element){
						  return ($j("#rec_date").is(':visible'));
							}
						},dpDate:true
					},
					/*"cbUploadInfoPojo.veficationTyping":{checkselect:
					{
					  depends: function(element){
						  return ($j("#ver_type").is(':visible'));
							}
						}
					},*/
					"cbUploadInfoPojo.description":"required",
					"cbUploadInfoPojo.strtestDate":{required:
					{
					  depends: function(element){
						  return ($j("#test").is(':visible'));
							}
						},dpDate:true
					},
					"cbUploadInfoPojo.strreportDate":{required:
					{
					  depends: function(element){
						  return ($j("#repo_date").is(':visible'));
							}
						},dpDate:true
					},
					//"timestamp":"required"
					"attachmentFlag":{required:true},
					/*"attachmentFlag":{required:{
												depends: function(element){
															return $j("#browseFlag").val()==1;
														}
												}
									}*/
					"uploadingFlag":{required:true}	
				},
				messages:{
					"categoryName":" <s:text name="garuda.common.validation.categoryname"/>",
					"labTestName":"<s:text name="garuda.common.validation.labtestname"/>",
					"cbUploadInfoPojo.description":"<s:text name="garuda.common.validation.description"/>",
					"cbUploadInfoPojo.strcompletionDate":{required:"<s:text name="garuda.common.validation.completionDate"/>"},
					"cbUploadInfoPojo.strtestDate":{required:"<s:text name="garuda.common.validation.testdate"/>"},
					"cbUploadInfoPojo.strprocessDate":{required:"<s:text name="garuda.common.validation.processdate"/>"},
					"cbUploadInfoPojo.strreceivedDate":{required:"<s:text name="garuda.common.validation.receiveddate"/>"},
					"cbUploadInfoPojo.strreportDate":{required:"<s:text name="garuda.common.validation.reportdate"/>"},
					//"cbUploadInfoPojo.veficationTyping":"<s:text name="garuda.common.validation.veficationtyping"/>",
				    //"timestamp":"Please Select a file to upload"
				   "attachmentFlag":"<s:text name="garuda.common.validation.attachment"/>",
				   "uploadingFlag":"<s:text name="garuda.common.validation.uploading"/>"
						
				}
			});
		
		});


	function validateForms(){
		if($j("#searchOpenOrders2").valid()){
			showMessage($j("#cburegid").val(),$j("select[id$='categoryName'] :selected").text());
		}
	}

	function showMessage(value1,value2)
	{
		   var str1 ="<s:text name="garuda.cbu.openorder.continueConfirm"/> ";
	       var str2="<br> <s:text name="garuda.uploaddoc.label.documentcategory"/>&nbsp: ";
		   jConfirm(str1+value1+str2+value2, '<s:text name="garuda.common.dialog.confirm"/>', function(answer) {
		   if(answer){
			           saveOrderAttachment();	
				}			    
			});	
		
	}

	 function showCdrCbuScreen(formname){
		 
		 if($j("#orderId").val()!=null && $j("#orderId").val()!=''){
			 loadMoredivs('getctshipdetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val(),'cbuShipmentInfoContentDiv,quicklinksDiv','cbuCordInformationForm');
		 }
		 else{
			 submitform(formname);
			}
	 }

	 function closeModalsLocal(divname){
		 //$j("#"+divname).dialog("close");
		 //$j("#"+divname).dialog("destroy");
		 $j("#"+divname).html("");
		 }

	 $j(function() {
	        if($j('#tabspecifier').val()=="true")
	        {
		      $j('#upload_document').css({border: '3px solid  #dcdcdc'});	 
	        }  
});		 
  </script>





<div class="col_100 maincontainer ">
<div class="col_100">
<s:form action="cdrcbuView"
	id="refreshUpload" >
	<s:hidden name="cdrCbuPojo.cordID" id="pkcordId"></s:hidden>
	<s:hidden name="cdrCbuPojo.registryId" ></s:hidden>
	<s:hidden name="orderId" id="orderId"></s:hidden>
	<s:hidden name="orderType" id="orderType"></s:hidden>
	<input type="hidden" name="modifyUnitrpt" value="<s:property value="#request.modifyUnitrpt" />" />
	<input type="hidden" name="saveFinalOrderAttachFlagId" id="saveFinalOrderAttachFlagId" >
	<s:if test="#request.update==true"><table  border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" >
           <tr><td><div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
						<p style="color:#1589FF;"><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
						<strong><s:text name="garuda.upload.regId.docUploadedMsg"/>  <s:property value="cdrCbuPojo.registryId"/>  </strong></p>
						</div></td>
						</tr>
						<tr>
						<td width="15%;"> 
						<button type="button" onclick="javaScript:return closeModal();">
						<s:text	name="garuda.common.close" /></button>
					</td>
				</tr></table>
       </s:if>
	   

</s:form> 
<s:form id="searchOpenOrders2" name="searchOpenOrders2">
	<s:hidden name="cdrCbuPojo.cdrCbuId" id="cdrCbuId"></s:hidden>
	<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
	<s:hidden name="cdrCbuPojo.registryId"  id="cburegid"></s:hidden>
		
	<input type="hidden" name="cbuCheck" id="cordId"
		value="<s:property value="cdrCbuPojo.cordID" />" />
	<s:hidden id="subcatid" name="subcatid" />
	
	<s:hidden id="categoryId" name="categoryId" />
	<table  border="0" align="left" id="update1" cellpadding="0" cellspacing="0" style ="display:none;" class="displaycdr" >
           <tr><td><div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
						<p style="color:#1589FF;"><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
						<strong><s:text name="garuda.upload.regId.docUploadedMsg"/>  <s:property value="cdrCbuPojo.registryId"/>  </strong></p>
						</div></td>
						</tr>
						<tr>
						<td width="15%;"> 
						<button type="button" onclick="javaScript:return closeModal();">
						<s:text	name="garuda.common.close" /></button>
					</td>
				</tr></table>
	<!-- -----------------------BY Ganesavel-------------------------------->
	<table  border="0" align="left" id="unitreportMsg" cellpadding="0" cellspacing="0" style ="display:none;" class="displaycdr" >
           <tr><td><div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
						<p style="color:#1589FF;"><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
						<strong><s:text name="garuda.unitreport.label.thankyou" /></strong></p>
						</div></td>
						</tr>
						<tr>
						<td width="15%;"> 
						<button type="button" onclick="javaScript:return closeModal();">
						<s:text	name="garuda.common.close" /></button>
					</td>
				</tr>
	</table>
	<table  border="0" align="left" id="modifyunitrptMsg" cellpadding="0" cellspacing="0" style ="display:none;" class="displaycdr" >
           <tr><td><div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
						<p style="color:#1589FF;"><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
						<strong><s:text name="garuda.unitreport.label.thankyoumodifyunitrpt" /></strong></p>
						</div></td>
						</tr>
						<tr>
						<td width="15%;"> 
						<button type="button" onclick="javaScript:return closeModal();">
						<s:text	name="garuda.common.close" /></button>
					</td>
				</tr>
	</table>
	<table  border="0" align="left" id="revokeunitrptMsg" cellpadding="0" cellspacing="0" style ="display:none;" class="displaycdr" >
           <tr><td><div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
						<p style="color:#1589FF;"><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
						<strong><s:text name="garuda.unitreport.label.thankyourevokeunitrpt" /></strong></p>
						</div></td>
						</tr>
						<tr>
						<td width="15%;"> 
						<button type="button" onclick="javaScript:return closeModal();">
						<s:text	name="garuda.common.close" /></button>
					</td>
				</tr>
	</table>
	<!-- ---------------------------------------------------------------->
	<div id="document">
	<div>
		<table>
			<tbody>
			<tr><td>
				<button type="button" id="upload_document" onclick="UploadShowModal('Upload Document for CBU Registry ID:<s:property value="cdrCbuPojo.registryId" />','uploaddocument?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>','500','800'); closeDialog('modelPopup2');">
				<s:text name="garuda.cdrcbuview.label.button.uploaddocument" /></button>
				<input type="button" onclick="fn_UploadDocShowModals('Uploaded Documents for CBU Registry ID:<s:property value="cdrCbuPojo.registryId" />','uploadedDocuments?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CLINICAL_RECORD_VIE_UPLOADED_DOC" />&cordID=<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />','400','800','modelPopup2');" 
					 value="<s:text name="garuda.uploaddoc.label.viewuploadeddocument" />"></input>
			</td>
			</tr>
		</tbody></table>	
	</div>
	<input type="hidden" name="dcmsIpAddress" id="dcmsIpAddress" value="<s:property value="%{getDCMSIPAddress()}" />" />
	<br></br>
	<!--        	
<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
	<tr>
	<td>		    -->
	<div>
	<table width="100%" border="0" align="left" cellpadding="0"
		cellspacing="0">
		<%-- <tr>
			<td><strong><s:text
				name="garuda.openorder.label.otherclinicaltest" /></strong></td>
		</tr>--%>
		
		<tr>
			<td>
			<div>
			
			<div id="modifyrpt1" <s:if test="#request.modifyUnitrpt!='True'">style="display: none;"</s:if>>
					<input type="hidden" name="cbuCordId" value="<s:property value="#request.cbuCordId" />" />
					<input type="hidden" name="attachmentId" value="<s:property value="#request.attachmentId" />" />
					<input type="hidden" id ="regid" name="cbuRegisteryId" value="<s:property value="#request.cbuRegisteryId" />" />
					<input type="hidden" name="modifyUnitrpt" value="<s:property value="#request.modifyUnitrpt" />" />
					
					<%-- <s:hidden name="cbuCordId" value="#request.cbuCordId"/>
					<s:hidden name="attachmentId" value="#request.attachmentId"/>
					<s:hidden name="revokeToId" value="<s:property value="#request.cbuRegisteryId" />"/>
					<s:hidden name="cbuRegisteryId" value="#request.cbuRegisteryId"/>
					<s:hidden name="modifyUnitrpt" value="#request.modifyUnitrpt"/>
					 --%><table border="0" align="left" cellpadding="0" cellspacing="0"  width="100%">	
   						<tr>
			    			 <td align="left">
				 				<s:text name="garuda.unitreport.label.revoke_frm_id" />
			   				 </td>
			   				 <td align="left">
								 <s:textfield name="cbuRegisteryId" id="revokeFromId" disabled="true" readonly="true" onkeydown="cancelBack();" />
				 				</td>
		        
			      			 <td align="left">
				 				   <s:text name="garuda.unitreport.label.revoke_to_id" />
			   				  </td>
			    			 <td align="left">
								    <s:textfield name="revokeToId" id="revokeToId" value="%{cbuRegisteryId}" />
			    				</td>
		        		</tr>
		        		</table>
		        		</div>
		        		
			<table border="0" align="left" cellpadding="0" cellspacing="0"
				width="100%">
				<tr>
					<td width="30%;"><s:text
						name="garuda.openorder.label.clinicalcat" />:<span style="color: red;">*</span></td>
					<td width="70%;"><s:if test="#request.docCategWithRights!=null && #request.docCategWithRights.size>0">
								<s:select name="categoryName" id="categoryName" headerKey="-1" headerValue="Select"	list="#request.docCategWithRights"
									listKey="pkCodeId" listValue="description"	onChange="javascript:saveCategoryName(this);showFields();resetField();">
								</s:select> <!--  <input type="hidden" id="catId" value=<s:property value="#application.codeListMapByIds[24142]" /> /> -->
                              </s:if>
                              <s:else>
                                <select name="categoryName" id="categoryName"  >
                                  <option value="-1"><s:text name="garuda.openorder.label.select" /></option>
                                </select>								
                              </s:else>
                     </td>
				</tr>
			</table>
			</div>
			</td>
		</tr>
		<tr>
			<td>
			<div id="testname" style="display: none;">
			<table border="0" align="left" cellpadding="0" cellspacing="0"
				width="100%">
				<tr>
					<td width="30%;"><s:text name="garuda.openorder.label.subcategory" />:<span
						style="color: red;">*</span></td>
					<td width="70%;"><s:if test="testNameList==null">
						<s:select headerKey="0" headerValue="Select" name="namelist"
							list="#{'1':'','2':''}" value="1" />
					</s:if> <s:if test="testNameList!=null">
						<s:select id="labTestName" headerKey="-1" headerValue="Select"
							name="labTestName" list="testNameList" listKey="pkCodeId"
							listValue="description"
							onchange="javascript:checkLabTestName(this);showFieldsSubCat();" />
					</s:if></td>
				</tr>
			</table>
			</div>
			</td>
		</tr>
		<tr>
			<td>
			<div id="com_date" style="display: none;">
			<table border="0" align="left" cellpadding="0" cellspacing="0"
				width="100%">
				<tr>
					<td width="30%;"><s:text
						name="garuda.openorder.label.completiondate" />:<span style="color: red;">*</span></td>
					<td width="70%;"><s:textfield name="cbUploadInfoPojo.strcompletionDate" id="datepicker2" readonly="true" onkeydown="cancelBack();"  cssClass="datePicWOMinDate" onfocus="onFocusCall(this);"/></td>
				</tr>
			</table>
			</div>
			<div id="test" style="display: none;">
			<table border="0" align="left" cellpadding="0" cellspacing="0"
				width="100%">
				<tr>
					<td width="30%;"><s:text
						name="garuda.openorder.label.testdate" />:<span style="color: red;">*</span></td>
					<td width="70%;"><s:textfield name="cbUploadInfoPojo.strtestDate" id="datepicker5" readonly="true" onkeydown="cancelBack();"  cssClass="datePicWOMinDate" onfocus="onFocusCall(this);"/></td>
				</tr>
			</table>
			</div>
			<div id="pro_date" style="display: none;">
			<table border="0" align="left" cellpadding="0" cellspacing="0"
				width="100%">
				<tr>
					<td width="30%;"><s:text
						name="garuda.openorder.label.processingdate" />:<span style="color: red;">*</span></td>
					<td width="70%;"><s:textfield name="cbUploadInfoPojo.strprocessDate" id="datepicker3" readonly="true" onkeydown="cancelBack();"  cssClass="datePicWOMinDate" onfocus="onFocusCall(this);"/></td>
				</tr>
			</table>
			</div>
			<div id="rec_date" style="display: none;">
			<table border="0" align="left" cellpadding="0" cellspacing="0"
				width="100%">
				<tr>
					<td width="30%;"><s:text
						name="garuda.openorder.label.receiveddate" />:<span style="color: red;">*</span></td>
					<td width="70%;">
						<s:textfield name="cbUploadInfoPojo.strreceivedDate" id="datepicker4" readonly="true" onkeydown="cancelBack();"  cssClass="datePicWMaxDate" onfocus="onFocusCall(this);"/></td>
				</tr>
			</table>
			</div>
			
			</td>
		</tr>
		<tr>
			<td>
			<div id="descript" style="display: none;">
			<table border="0" align="left" cellpadding="0" cellspacing="0"
				width="100%">
				<tr>
					<td width="30%;" valign="top"><s:text
						name="garuda.openorder.label.description" />:<span style="color: red;">*</span></td>
					<td width="70%;"><s:textarea
						name="cbUploadInfoPojo.description" cols="49" rows="5"
						id="word_count" /> <br />
					<span id="counter"></span></td>
				</tr>
			</table>
			</div>
			</td>
		</tr>
		<tr>
			<td>
			<div id="ver_type" style="display: none;">
			<table border="0" align="left" cellpadding="0" cellspacing="0"
				width="100%">
				<tr>
					<td width="30%;">
					<!--
					<s:text
						name="garuda.openorder.label.verificationtyping" />:
						-->
						</td>
					<td width="70%;">
					<!-- <s:select headerKey="-1" headerValue="Select"
						name="cbUploadInfoPojo.veficationTyping" id="verificationTyping"
						list="#{'1':'YES','0':'NO'}" value="0">
					</s:select> -->
					</td>
				</tr>
			</table>
			</div>
			<div id="repo_date" style="display: none;">
					<table border="0" align="left" cellpadding="0" cellspacing="0"
						class="displaycdr" width="100%">
						<tr>
							<td width="28%;"><s:text
								name="garuda.openorder.label.reportdate" />:<span style="color: red;">*</span></td>
							<td>
				    			<s:textfield name="cbUploadInfoPojo.strreportDate" id="datepicker6" readonly="true" onkeydown="cancelBack();"  cssClass="datePicWOMinDate" onfocus="onFocusCall(this);"/>
			    		</td>
						</tr>
					</table>
					</div>
			</td>
		</tr>
	</table>
	</div>
	<!-- 
	</td>
</tr>
<tr>
	<td> -->
	<s:if test="#request.modifyUnitrpt!='True'"><s:hidden id="tabspecifier" value="true"/></s:if>
	<div id="fileupload" style="margin-left: -0px;<s:if test="#request.modifyUnitrpt=='True'">display: none;</s:if>" >
			<!-- <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0" >
			<tr>
				<td>
				
 					<input type=text value=<%=request.getParameter("cordID")%> /> -->
					<iframe id="frame1" width="100%"  height="80px" frameborder="0" style="background: #F7F7F7;"
										scrolling="no" src="cb_fileattach.jsp?attCodeType=attach_type&attCodeSubType=test_rprt&codeEntityType=entity_type&codeEntitySubType=CBU">
					</iframe>
					<s:hidden id="attachmentFlag" name="attachmentFlag" value=""></s:hidden>
						<s:hidden id="uploadingFlag" name="uploadingFlag" value="true"></s:hidden>
					<s:hidden id="browseFlag" name="browseFlag" value="0"></s:hidden>
				<!-- </td>
			</tr>
			<tr>
				<td>-->
					<s:hidden name="attachmentPojo.timestamp" id="attachId" />
			<!-- </td>
			</tr>
			</table>-->
		</div>
	<!-- 
	</td>
</tr>
</table> --> 
	<table border="0" align="left" cellpadding="0" cellspacing="0"
		bgcolor="#cccccc">
		<tr bgcolor="#cccccc" valign="baseline">
			<td width="70%;"><div id ="esign">
				<s:if test="#request.module=='CBU'">
				   <jsp:include page="../cb_esignature.jsp" flush="true">
				        <jsp:param value="eleReviewsubmit" name="submitId"/>
						<jsp:param value="eleReviewinvalid" name="invalid" />
						<jsp:param value="eleReviewminimum" name="minimum" />
						<jsp:param value="eleReviewpass" name="pass" />
				   </jsp:include>
				</s:if>
				<s:else>
				   <jsp:include page="../cb_esignature.jsp" flush="true">
				   		<jsp:param value="nonCBUsubmit" name="submitId"/>
						<jsp:param value="nonCBUinvalid" name="invalid" />
						<jsp:param value="nonCBUminimum" name="minimum" />
						<jsp:param value="nonCBUpass" name="pass" />
				   </jsp:include>
				</s:else>
			</div></td>
			<td width="15%;" align="center"><s:hidden
				name="orderAttachmentPojo.attachmentId" id="attachmentId" /> <s:if
				test="#request.module=='CBU'">
				<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
				<s:hidden name="module"></s:hidden>
				<s:hidden name="reasonPojo.pkEntityStatusReason"></s:hidden>
				<s:hidden name="licenceUpdate"></s:hidden>
				<button type="button" onclick="saveFinalOrderAttachment();"	id="eleReviewsubmit">
				  <s:text name="garuda.openorder.label.button.upload" /></button>
			</s:if> <s:else>
				<button type="button"  onclick="<s:if test="#request.modifyUnitrpt=='True' && #request.modifyDoc=='True'">javascript:modifyDoc('<s:property value="cdrCbuPojo.cordID" />');</s:if><s:elseif test="#request.modifyUnitrpt=='True'">javascript:modifyUnitrptDoc('<s:property value="cdrCbuPojo.cordID" />');</s:elseif><s:else>javascript:validateForms();</s:else>"id="nonCBUsubmit" disabled="disabled"><s:text
					name="garuda.openorder.label.button.upload" /></button>
			</s:else></td>
			<td width="15%;">
			<button type="reset" onclick="closeModal()"><s:text
				name="garuda.openorder.label.button.cancel" /></button>
			</td>
		</tr>
	</table>
	</div>
</s:form></div>
</div>
<script>

function fn_UploadDocShowModals(title,url,hight,width,divid){
	var upiob = $j('#numberOnCbuBag').val();
	var upiob_data = '';
	if(upiob!="" && upiob != null && upiob != "undefined"){
		upiob_data = ', <s:text name="garuda.cbuentry.label.idbag"/>:'+upiob;
	}
	title + ' <s:text name="gaurda.common.cburegtxt"/><s:property value="cdrCbuPojo.registryId"/>'
	title = title + upiob_data;
	viewUploadedDocuments(title,url,hight,width,divid);
}
function UploadShowModal(title,url,hight,width){
	var upiob = $j('#numberOnCbuBag').val();
	var upiob_data = '';
	if(upiob!="" && upiob != null && upiob != "undefined"){
		upiob_data = ', <s:text name="garuda.cbuentry.label.idbag"/>:'+upiob;
	}
	title = title + upiob_data;
	showModal(title,url,hight,width);
}
</script>