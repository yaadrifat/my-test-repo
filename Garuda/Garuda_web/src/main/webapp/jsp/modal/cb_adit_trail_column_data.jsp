<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%String contextpath = request.getContextPath()+"/jsp/modal/"; %>
<script>
   $j("#columnaudittrailtab").dataTable();
</script>
<div class="column">
  <div class="portlet" id="columnaudittrailparent" >
    <div id="columnaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
       <s:text name="Audit Trail" />
      </div>
      <div class="portlet-content">
<div id="searchTble1">
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="columnaudittrailtab" class="displaycdr">
	        <thead>
	           <tr>
	              <th><s:text name="garuda.cbuentry.label.registrycbuid"/></th>
	              <th><s:text name="garuda.cbuentry.label.localcbuid"/></th>
	              <th><s:text name="garuda.cbuentry.label.maternalregistryid"/></th>
	              <th><s:text name="garuda.cbuentry.label.maternallocalid"/></th>
	              <th><s:text name="garuda.cbuentry.label.cdrcbucreon"/></th>
	              <th><s:text name="garuda.cbuentry.label.cdrcbucreby"/></th>
	              <th><s:text name="garuda.cbbdefaults.lable.lastmodifiedby"/></th>
	              <th><s:text name="garuda.applMessge.label.message.lastModOn"/></th>
	              <th><s:text name="garuda.audittrail.label.column"/></th>
	              <th><s:text name="garuda.audittrail.label.old"/></th>
	              <th><s:text name="garuda.audittrail.label.new"/></th>
	              <th><s:text name="garuda.audittrail.label.statusreason"/></th>
	              <th><s:text name="garuda.audittrail.label.remarks"/></th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailColumnData" var="row" status="status">
	                <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property value="getText('{0,date,MMM dd,yyyy}',{#row[5]})"  />
	                    </td>
	                    <td>
	                        <s:property value="%{#row[6]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[7]}"/>
	                    </td>
	                    <td>
	                        <s:property value="getText('{0,date,MMM dd,yyyy}',{#row[8]})"  />
	                    </td>	
	                    <td>
	                        <s:if test="%{#row[14]}!=null && %{#row[14]}!=''">
	                           <s:property value="%{#row[14]}"/>
	                        </s:if>
	                        <s:else>
	                          <s:property value="%{#row[9]}"/>
	                         </s:else>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[10]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[11]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[12]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[13]}"/>
	                    </td>                   
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	            <tr>
	               <td colspan="13"></td>
	            </tr>
	        </tfoot>
	     </table>	
</div></div></div></div></div>