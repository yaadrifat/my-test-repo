<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:include page="../cb_track_session_logging.jsp" />
<%
	HttpSession tSession = request.getSession(true);
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	String accId = null;
	String logUsr = null;
	if (sessionmaint.isValidSession(tSession))
	{
    	accId = (String) tSession.getValue("accountId");
    	logUsr = (String) tSession.getValue("userId");
    	request.setAttribute("username",logUsr);
	}
%>
<script type="text/javascript">
$j(function() {
	getDatePic();
});

function validateResol(){
	var flag=0;
	if(!$j('#ResoluAckModal').is(':checked')){
		$j('#showerrormsg').attr('style','display:block');
		var msg="<s:text name="garuda.cbu.resolution.resoluAck"/>";
		$j('#errorcheck').html(msg);
		flag=1;
	}
		if(flag==0){	
			if($j('#orderType').val()=='CT')submitCTResolution();
			if($j('#orderType').val()=='HE')submitHeResolution();
			if($j('#orderType').val()=='OR')submitcbuOrdResolution();
		}
		
	}

function submitCTResolution(){
	if($j("#availdatepicker").val()==null){var availableDt="";}
	else{var availableDt=$j("#availdatepicker").val();}
	if($j("#ResolutionCBB").val()==null){var resolbycbb="";}
	else{var resolbycbb=$j("#ResolutionCBB").val();}
	if($j("#ResolutionTC").val()==null){var resolbytc="";}
	else{var resolbytc=$j("#ResolutionTC").val();}
	if($j("#ResoluAckModal").attr('checked')){var resolutioncheck="Y";}
	else{var resolutioncheck="N";}
	if($j("#currentuserid").val()==null){var userid="";}
	else{var userid=$j("#currentuserid").val();}
	loadMoredivs('submitCtResolution?pkcordId='+$j('#pkcordId').val()+'&availableDt='+availableDt+'&resolbycbb='+resolbycbb+'&resolbytc='+resolbytc+'&ackResolution='+resolutioncheck+'&userID='+userid+'&orderType='+$j('#orderType').val()+'&orderId='+$j("#orderId").val(),'ctresolsubDiv,currentRequestProgressMainDiv,cbuHistoryForm,taskCompleteFlagsDiv','orderResolutionModalForm','updateresolutionmodal','statusresolutionmodal');
	historyTblOnLoad();
	Fnresol();
	setpfprogressbar();
	getprogressbarcolor();
	
}

function submitHeResolution(){
	if($j("#availdatepicker").val()==null){var availableDt="";}
	else{var availableDt=$j("#availdatepicker").val();}
	if($j("#ResolutionCBB").val()==null){var resolbycbb="";}
	else{var resolbycbb=$j("#ResolutionCBB").val();}
	if($j("#ResolutionTC").val()==null){var resolbytc="";}
	else{var resolbytc=$j("#ResolutionTC").val();}
	if($j("#ResoluAckModal").attr('checked')){var resolutioncheck="Y";}
	else{var resolutioncheck="N";}
	if($j("#currentuserid").val()==null){var userid="";}
	else{var userid=$j("#currentuserid").val();}
	/*
	if($j("#acceptToCancel").val()==null){var acceptToCancel="";}
	else{var acceptToCancel=$j("#acceptToCancel").val();}
	if($j("#datepicker6").val()==null){var rejectDt="";}
	else{var rejectDt=$j("#datepicker6").val();}
	if($j("#rejectby").val()==null){var rejectBy="";}
	else{var rejectBy=$j("#rejectby").val();}
	*/
	//alert("pkcord"+$j('#pkcordId').val()+"---------------ResolutionCBB"+resolbycbb+"---------------Resolutiontc"+resolbytc+"---------------availableDt"+availableDt+"---------------resolutioncheck-"+resolutioncheck+"----userid"+userid);
	loadMoredivs('submitCtResolution?pkcordId='+$j('#pkcordId').val()+'&availableDt='+availableDt+'&resolbycbb='+resolbycbb+'&resolbytc='+resolbytc+'&ackResolution='+resolutioncheck+'&userID='+userid+'&orderType='+$j('#orderType').val()+'&orderId='+$j("#orderId").val(),'heresolsubDiv,currentRequestProgressMainDiv,cbuHistoryForm,taskCompleteFlagsDiv','orderResolutionModalForm','updateresolutionmodal','statusresolutionmodal');
	historyTblOnLoad();
	Fnresol();
	setpfprogressbar();
	getprogressbarcolor();
	
}

function submitcbuOrdResolution(){
	var availableDt="";
	var resolbycbb="";
	var resolbytc="";
	var resolutioncheck="";
	var userid="";
	if($j("#availdatepicker").val()==null){
		availableDt="";
	}
	else{
		availableDt=$j("#availdatepicker").val();
	}
	if($j("#ResolutionCBB").val()==null){
		resolbycbb="";
	}
	else{

		resolbycbb=$j("#ResolutionCBB").val();
	}
	if($j("#ResolutionTC").val()==null){
		resolbytc="";
	}
	else{
		resolbytc=$j("#ResolutionTC").val();
	}
	if($j("#ResoluAckModal").attr('checked')){
		resolutioncheck="Y";
	}
	else{
		resolutioncheck="N";
	}
	if($j("#currentuserid").val()==null){
		userid="";
	}
	else{
		userid=$j("#currentuserid").val();
	}
	loadMoredivs('submitCtResolution?pkcordId='+$j('#pkcordId').val()+'&availableDt='+availableDt+'&resolbycbb='+resolbycbb+'&resolbytc='+resolbytc+'&ackResolution='+resolutioncheck+'&userID='+userid+'&orderType='+$j('#orderType').val()+'&orderId='+$j("#orderId").val(),'orresolsubDiv,currentRequestProgressMainDiv,cbuHistoryForm,taskCompleteFlagsDiv','orderResolutionModalForm','updateresolutionmodal','statusresolutionmodal');
	historyTblOnLoad();
	Fnresol();
	setpfprogressbar();
	getprogressbarcolor();
	
}

</script>
<s:form name="orderResolutionModalForm">
<div id="updateresolutionmodal" style="display: none;">
	
	<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.message.modal.orderressolack" /></strong></p>
			</div>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td>
			<input type="button" onclick="closeModals('resolmodaldiv')" value="<s:text name="garuda.common.close"/>" />
		</td>
	</tr>
	</table>
	
</div>
<table width="100%" cellspacing="0" cellpadding="0" border="0" id="statusresolutionmodal">
<tr>
<td>
<div class="portlet" id="ctresolutionbarDivparent">
							<div id="ctresolutionbarDiv"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
									<div
										class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
										style="text-align: center;"><span class="ui-icon ui-icon-print" onclick="clickheretoprint('ctresolutionbarDiv','<s:property value="cdrCbuPojo.registryId" />')"></span><span
										class="ui-icon ui-icon-newwin"></span>
										<!--<span class="ui-icon ui-icon-plusthick"></span>-->
										<!--<span class="ui-icon ui-icon-close"></span>-->
										<span class="ui-icon ui-icon-minusthick"></span>
										<s:if test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
										<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test='orderType=="CT"'>
											<s:text name="garuda.ctOrderDetail.portletname.ctResolution"/>
										</s:if>
										<s:if test='orderType=="HE"'>
											<s:text name="garuda.orOrderDetail.portletname.heResolution"/>
										</s:if>	
										<s:if test='orderType=="OR"'>
											<s:text name="garuda.orOrderDetail.portletname.orResolution"/>
										</s:if>
										</s:iterator>
										</s:if>
									</div>
									<div id="ctresolsubDiv" class="portlet-content">
												<s:hidden name="cancelled_ack_orders" id="canackorders"></s:hidden>
												<s:if test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
												<s:iterator value="ctOrderDetailsList" var="rowVal">
													<s:hidden name="resolByTCFlag" id="resolByTCFlag" value="%{#rowVal[57]}"></s:hidden>
													<s:hidden name="resolByCBBFlag" id="resolByCBBFlag" value="%{#rowVal[58]}"></s:hidden>
												</s:iterator>
												</s:if>
										       <s:if test="orderResolLst!=null && orderResolLst.size>0">
							 					<s:iterator value="orderResolLst" var="rowVal">
							 					<s:hidden name="acknowledgeFlag" id="acknowledgeFlag" value="%{#rowVal[9]}"></s:hidden>
										        <table width="100%">
										       		
											        <tr>
											        	<td colspan="3">
														 <table cellpadding="0" cellspacing="0" border="0" width="100%">
															<tr>
													        	<td width="40%"><strong><s:text name="garuda.heResolution.label.resolutiondate"/></strong>:</td>
																<td width="40%"><s:property value="%{#rowVal[2]}"/></td>
																<td width="20%"></td>
															</tr>
														</table>
														</td>
													</tr>	
													
													<tr>
														<td colspan="3">
															<s:if test="%{#rowVal[1]!=null && #rowVal[1]!=''}">
																<table id="resolBytc" width="100%">
																	<tr>
																		<td width="40%"><strong><s:text name="garuda.ctResolution.label.resolutionbytc"/></strong></td>
										    							<td width="40%">

										    							<s:property value="getCbuStatusDescByPk(#rowVal[1])" />
										    							<s:hidden name="tcResolVal" id="tcResolVal" value="%{#rowVal[1]}"/>
										    							
										    							<span id="errormsg" class="error"></span>
										    							</td>
										    							<td width="20%"></td>
									    							</tr>
									    							<tr>
																		<td width="40%"><strong><s:text name="garuda.currentReqProgress.label.closereason"/></strong></td>
										    							<td width="40%">
										    							<s:property value="getCbuStatusDescByPk(#rowVal[10])" />
										    							<span id="errormsg" class="error"></span>
										    							</td>
										    							<td width="20%"></td>
									    							</tr>
									    							<tr>
																		<td width="40%"><strong><s:text name="garuda.ctResolution.label.acknowledgetcresolution"/></strong>:<span style="color: red;">*</span></td>
																		<td width="40%">
																			
																			<s:checkbox name="ResoluAckModal" id="ResoluAckModal" fieldValue="%{#rowVal[9]}" value="%{#rowVal[9]}" /><s:text name="garuda.ctResolution.label.resolutionbytc" />
																			<span id="errorcheck" class="error"></span>
																			
																		</td>
																		<td width="20%"></td>
																	</tr>
																	
																	
																	<s:if test="%{#rowVal[7]!=null && #rowVal[7]!=''}">
								    								<tr>
																		<td width="40%"><strong><s:text name="garuda.cbu.order.ackDateTime"/></strong>:</td>
																		<td width="40%"><s:textfield name="AckDate" required="true" disabled="true" value="%{#rowVal[6]}"/></td>
																		<td width="20%"></td>
																	</tr>
																	<tr>
																		<td width="40%"><strong><s:text name="garuda.cbu.order.ackBy"/></strong>:</td>
																		<td width="40%">
																	
																		<s:set name="username" value="%{#rowVal[7]}" scope="request"/>
																		<%
																			String cellValue1="";
																			if(request.getAttribute("username")!=null && !request.getAttribute("username").equals("")){
																				cellValue1 = request.getAttribute("username").toString();
																			}
																			UserJB userB = new UserJB();
																			userB.setUserId(EJBUtil.stringToNum(cellValue1));
																			userB.getUserDetails();													
																		%>
																		<%=userB.getUserLastName()+" "+userB.getUserFirstName()%>
																		</td>
																		<td width="20%"></td>
																	</tr>
																	</s:if>
																	<s:else>
																	<%
																			String cellValue1="";
																			if(request.getAttribute("username")!=null && !request.getAttribute("username").equals("")){
																				cellValue1 = request.getAttribute("username").toString();
																			}
																			UserJB userB = new UserJB();
																			userB.setUserId(EJBUtil.stringToNum(cellValue1));
																			userB.getUserDetails();													
																	%>	
																	<input type="hidden" name="currentuser" id="currentuserid" value=<%=logUsr%> >
																	</s:else>
																	
																	
																	
																</table>
																
														</s:if>
								    					<s:elseif test="%{ {{#rowVal[1]==null || #rowVal[1]==''} && {#rowVal[0]==null || #rowVal[0]==''}} || {#rowVal[0]!=null && #rowVal[0]!=''} }">
																<table cellpadding="0" cellspacing="0" border="0" id="resolByCbb">
																	<tr>
																		<td width="40%"><strong><s:text name="garuda.ctResolution.label.resolutionbycbb"/></strong>:</td>
										    							<td width="40%">
										    							<s:property value="getCbuStatusDescByPk(#rowVal[0])" />
										    							<span id="errormsg" class="error"></span>
										    							</td>
										    							<td width="20%"></td><s:hidden name="hresolcbb" id="hresolcbb" value="%{#rowVal[0]}"/>
									    							</tr>
									    							<tr>
																		<td width="40%"><strong><s:text name="garuda.currentReqProgress.label.closereason"/>:</strong></td>
										    							<td width="40%">
										    							<s:property value="getCbuStatusDescByPk(#rowVal[10])" />
										    							<span id="errormsg" class="error"></span>
										    							</td>
										    							<td width="20%"></td>
									    							</tr>
		        													<s:if test="%{#rowVal[0]!=null && #rowVal[0]!=''}">
		        													<tr>
																		<td width="40%"><strong><s:text name="garuda.ctResolution.label.acknowledgetcresolution"/></strong>:<span style="color: red;">*</span></td>
																		<td width="40%">
																			
																			<s:checkbox name="ResoluAckModal" id="ResoluAckModal" fieldValue="%{#rowVal[9]}" value="%{#rowVal[9]}" /><s:text name="garuda.ctResolution.label.resolutionbytc" />
																			<span id="errorcheck" class="error"></span>
																			
																		</td>
																		<td width="20%"></td>
																	</tr>		
																	</s:if>
																	<s:if test="%{#rowVal[7]!=null && #rowVal[7]!=''}">
								    								<tr>
																		<td width="40%"><strong><s:text name="garuda.cbu.order.ackDateTime"/></strong>:</td>
																		<td width="40%"><s:textfield name="AckDate" required="true" disabled="true" value="%{#rowVal[6]}"/></td>
																		<td width="20%"></td>
																	</tr>
																	<tr>
																		<td width="40%"><strong><s:text name="garuda.cbu.order.ackBy"/></strong>:</td>
																		<td width="40%">
																	
																		<s:set name="username" value="%{#rowVal[7]}" scope="request"/>
																		<%
																			String cellValue1="";
																			if(request.getAttribute("username")!=null && !request.getAttribute("username").equals("")){
																				cellValue1 = request.getAttribute("username").toString();
																			}
																			UserJB userB = new UserJB();
																			userB.setUserId(EJBUtil.stringToNum(cellValue1));
																			userB.getUserDetails();													
																		%>
																		<%=userB.getUserLastName()+" "+userB.getUserFirstName()%>
																		</td>
																		<td width="20%"></td>
																	</tr>
																	</s:if>
																	<s:else>
																	<%
																			String cellValue1="";
																			if(request.getAttribute("username")!=null && !request.getAttribute("username").equals("")){
																				cellValue1 = request.getAttribute("username").toString();
																			}
																			UserJB userB = new UserJB();
																			userB.setUserId(EJBUtil.stringToNum(cellValue1));
																			userB.getUserDetails();													
																	%>	
																	<input type="hidden" name="currentuser" id="currentuserid" value=<%=logUsr%> >
																	</s:else>											
								    							</table>
								    					</s:elseif>
								    						
							    						</td>
							    					</tr>
												</table>
												</s:iterator>
												</s:if>
									<s:if test="orderResolLst!=null && orderResolLst.size()>0">
									<s:iterator value="orderResolLst" var="rowVal">
									<s:if test='%{((#rowVal[0]!=null && #rowVal[0]!="") || (#rowVal[1]!=null && #rowVal[1]!="")) && (#rowVal[9]==null || #rowVal[9]=="" || #rowVal[9]=="N")}'>
									<table style="background-color: #cccccc;" id="esignTbl" class="disable_esign" width="100%">
										<tr valign="baseline">
											<td width="70%">
												<jsp:include page="../cb_esignature.jsp" flush="true">
												<jsp:param value="modalsubmitCtResolution" name="submitId"/>
												<jsp:param value="modalinvalidsubmitctRes" name="invalid" />
												<jsp:param value="modalminimumsubmitctRes" name="minimum" />
												<jsp:param value="modalpasssubmitctRes" name="pass" />
												</jsp:include>
											</td>
												<td align="left" width="30%">
												<input type="button" onclick="validateResol();" value="<s:text name="garuda.unitreport.label.button.submit"/>" id="modalsubmitCtResolution" disabled="disabled"/>
												<input type="button"  onclick="closeModals('resolmodaldiv')" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />
											</td>
										</tr>
									</table>
									</s:if>
									</s:iterator>
									</s:if>
							</div>
							</div>
							
						</div>
						</td>
						</tr>
						</table>
</s:form>