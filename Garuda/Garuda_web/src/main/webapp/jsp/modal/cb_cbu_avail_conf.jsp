
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction"%>
<%@ page
	import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id="modCtlDao" scope="request"
	class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<jsp:include page="../cb_track_session_logging.jsp" />
<%
	HttpSession tSession = request.getSession(true);
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	int viewCbuAvailCon = 0;
	
	if(grpRights.getFtrRightsByValue("CB_CBUAVAILCONW")!=null && !grpRights.getFtrRightsByValue("CB_CBUAVAILCONW").equals(""))
	{	viewCbuAvailCon = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUAVAILCONW"));}
	else
		viewCbuAvailCon = 4;
	
	request.setAttribute("viewCbuAvailCon",viewCbuAvailCon);
	String accId = null;
	String logUsr = null;
	if (sessionmaint.isValidSession(tSession))
	{
    	accId = (String) tSession.getValue("accountId");
    	logUsr = (String) tSession.getValue("userId");
    	request.setAttribute("username",logUsr);
	}
%>
<script>

function saveCordAvailForNmdp1(){
	var var_cordId=$j("#pkcordId").val();
	var var_cbuAvail=$j('input:radio[name=iscordavailfornmdp]:checked').val();	
	if(validateCordAvailConfirm1()){
		var historyArr=new Array();
		var i=0; 
 		$j('.detailhstry').each(function(){
			if($j(this).is(':visible')){
				var idval=$j(this).attr("id");
				historyArr[i]=idval;
				i++;
			}
		}); 
 		setTimeout(function(){
		loadMoredivs('updateCordAvailForNmdp?cordId='+var_cordId+'&cbuavail='+var_cbuAvail+'&pkcordId='+var_cordId+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val(),'ctcbuavailconfbarDiv,currentRequestProgress,cordlocalstatusdiv,taskCompleteFlagsDiv,chistory,cReqhistory','modalcordavailconfirmform','updatecordavailmodal','statuscordavailmodal');
		setpfprogressbar();
 		getprogressbarcolor();
 		historyTblOnLoad();
 		},0);
 		setTimeout(function(){
			//alert("historyArr::length"+historyArr.length)
			$j.each(historyArr,function(i,val){
				var Index=val.charAt(val.length-1);
				$j("#icondiv"+Index).triggerHandler('click');
			});
		},0);
	}
}
function validateCordAvailConfirm1(){
	var iscordavail=$j('input:radio[name=iscordavailfornmdp]:checked').val();
	if(iscordavail==null || iscordavail==''){
		$j("#errorcordAvailConfirmDiv1").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.cbu.cord.nmdpAvailable"/>");
		 });
		$j("#errorcordAvailConfirmDiv1").show();
		return false;
	}
	
	else{
		$j("#errorcordAvailConfirmDiv1").find("span").each(function(){
      		$j(this).text("");
	 });
		$j("#errorcordAvailConfirmDiv1").hide();
	
		return true;
	}
}

	 

</script>
<s:form id="modalcordavailconfirmform">
<div class="portlet" id="cbuavailconfbarDivparent1"><s:form
	id="cbuAvailCnf">
	<div id="updatecordavailmodal" style="display: none;">
	<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.common.lable.cbuavail" /></strong></p>
			</div>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td>
			<input type="button" onclick="closeModal()" value="<s:text name="garuda.common.close"/>" />
		</td>
	</tr>
	</table>
	</div>
	<div id="statuscordavailmodal"
		class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">

	<div class="portlet-content">
	<div id="ctcbuavailconfbarDiv"><s:property
		value="#request.rowVal[0]" /> <s:iterator value="cordAvailConformLst"
		var="rowVal">
		<table>
			<tr>
				<td><strong><s:text
					name="garuda.cbuAvailconformation.label.cbuavailfornmdp" /></strong>:<span
					style="color: red;">*</span></td>
				<td><s:if test="#rowVal[0]!=null && #rowVal[0]!=''">
					<s:radio name="iscordavailfornmdp" id="iscordavailfornmdp"
						list="#{'Y':'Yes - Reserve for NMDP','N':'No'}"
						value="%{#rowVal[0]}" onclick="getUnavailReason(this.value);" />
				</s:if> <s:else>
					<s:if test="hasEditPermission(#request.viewCbuAvailCon)==true">
						<s:radio name="iscordavailfornmdp" id="iscordavailfornmdp"
							list="#{'Y':'Yes - Reserve for NMDP','N':'No'}"
							onclick="getUnavailReason(this.value);" />
					</s:if>
					<s:else>
						<s:radio name="iscordavailfornmdp" id="iscordavailfornmdp"
							list="#{'Y':'Yes - Reserve for NMDP','N':'No'}"
							onchange="disableVal();" />
					</s:else>
				</s:else>

				<div id="errorcordAvailConfirmDiv1"><span
					id="cordAvailConfirmlbl" class="error"></span></div>
				</td>
			</tr>
		</table>
	</s:iterator> <s:if test="cordAvailConformLst!=null && cordAvailConformLst.size()>0">
		<s:iterator value="cordAvailConformLst" var="rowVal">
			<div id="esignForCordAvail">
			<table bgcolor="#cccccc" class="tabledisplay disable_esign" width="100%">
				<tr bgcolor="#cccccc" valign="baseline">
					<td width="70%"><jsp:include page="../cb_esignature.jsp"
						flush="true">
						<jsp:param value="modalsubmitOrCbuavail" name="submitId" />
						<jsp:param value="modalinvalidOrCbuavail" name="invalid" />
						<jsp:param value="modalminimumOrCbuavail" name="minimum" />
						<jsp:param value="modalpassOrCbuavail" name="pass" />
					</jsp:include></td>
					<td align="left" width="30%"><input type="button"
						id="modalsubmitOrCbuavail" disabled="disabled"
						onclick="saveCordAvailForNmdp1()"
						value="<s:text name="garuda.unitreport.label.button.submit" />" />
					<input type="button"  onclick="closeModal()" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />
					</td>
				</tr>

			</table>
			</div>

		</s:iterator>
	</s:if></div>
	</div>
	</div>

</s:form></div>
<div id="modelPopup3"></div>
</s:form>