<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script>
</script>
<s:form id="viewAntigensForm">
		
			  <div>						         
				<div id="patbesthlainfocontent" onclick="toggleDiv('patbesthlainfo')"
					class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
					style="text-align: center;"><span
					class="ui-icon ui-icon-triangle-1-s"></span> <s:text
					name="garuda.cbuentry.label.patient"></s:text></div>
				<div id="patbesthlainfo">
				   <table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults10">
						 <thead>
						   <tr>
						      <th></th>   			   
							   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							       <th>
							          <s:property value="description.replace('HLA-','')"/>
							       </th>				     
							   </s:iterator>
						   </tr>
						 </thead>			 
						 <tbody>
						    <tr>
							  <td><s:text name="garuda.cbu.label.besthla"/></td>
						      <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							            <s:iterator value="#request.patientBestHlas" var="bhla">
									             <s:if test="pkCodeId==fkHlaCodeId">
									                 <s:set name="phlaCheck" value="%{'true'}" />
										             <td>
											            <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
    									                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>	  
												     </td>	
											      </s:if>				      										      	        	
									        </s:iterator> 
									        <s:if test="%{#phlaCheck!='true'}">
											         <td></td>
											</s:if>	
											<s:set name="phlaCheck" value="%{'false'}" />
								  </s:iterator>
							 </tr> 
						   </tbody>
						   <tfoot>
							    <tr>
									<td colspan="13"></td>
								</tr>
							</tfoot>
						</table>
						<table width="100%" style="padding-top: 50px;">
						      <tr>
						         <td></td>
						      </tr>
						</table>
						<table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults10">
								 <thead>
								   <tr>
								      <th><s:text name="garuda.cbuentry.label.typingdate"/></th>	
							          <th><s:text name="garuda.cbuentry.label.recieveddate"/></th>
							          <th><s:text name="garuda.cbuentry.label.source"/></th>							   		   			   
									   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
									       <th>
									          <s:property value="description.replace('HLA-','')"/>
									       </th>				     
									   </s:iterator>
								   </tr>
								 </thead>			 
								 <tbody>	
								    <s:iterator value="#request.patHlaMap">
								        <s:set name="patHlaMapKey" value="key" />
								        <tr>				
								        <td>
								            <s:if test="#request.patHlaMap[#patHlaMapKey][0].hlaTypingDate !=null">
								                <s:set name="pathlatypedate" value="#request.patHlaMap[#patHlaMapKey][0].hlaTypingDate" />
										        <s:date name="pathlatypedate" id="pathlatypedate" format="MMM dd, yyyy" />
										        <s:property value="%{pathlatypedate}"/>
								            </s:if>									        
								        </td>
								        <td>
								            <s:if test="#request.patHlaMap[#patHlaMapKey][0].hlaRecievedDate !=null">
										        <s:set name="pathlarecievedate" value="#request.patHlaMap[#patHlaMapKey][0].hlaRecievedDate" />
										        <s:date name="pathlarecievedate" id="pathlarecievedate" format="MMM dd, yyyy" />
										        <s:property value="%{pathlarecievedate}"/>
										    </s:if>
								        </td>
								        <td>
								           <!--<s:if test="#request.patHlaMap[#patHlaMapKey][0].fkSource !=null && #request.patHlaMap[#patHlaMapKey][0].fkSource!=''">
								               <s:set name="pathlasource" value="#request.patHlaMap[#patHlaMap][0].fkSource" />
									           <s:property value="getCodeListDescById(#pathlasource)"/>
									       </s:if>
									       --><s:if test="#request.patHlaMap[#patHlaMapKey][0].createdBy !=null && #request.patHlaMap[#patHlaMapKey][0].createdBy!=''">
							               <s:set name="pathlacreator" value="#request.patHlaMap[#patHlaMapKey][0].createdBy" />
								           <s:property value="getUserNameById(#pathlacreator)"/>
								       </s:if>
								        </td>							        					       
									    <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
										     	<s:iterator value="#request.patHlaMap[#patHlaMapKey]" var="bhla">
										             <s:if test="pkCodeId==fkHlaCodeId">
										             <s:set name="patCheck" value="%{'true'}" />
											             <td>
												            <table>
											                    <tr>
											                        <td>
											                            <s:property value="hlaType1" />
											                        </td>
											                    </tr>
											                     <tr>
											                        <td>
											                            <s:property value="hlaType2" />
											                        </td>
											                    </tr>
											                </table>	  
													     </td>	
												      </s:if>										      		        	
										        </s:iterator>    
										        <s:if test="%{#patCheck!='true'}">
												         <td></td>
												</s:if>	
												<s:set name="patCheck" value="%{'false'}" />    
										 </s:iterator> 
										 </tr>
									 </s:iterator>
								   </tbody>
								   <tfoot>
									    <tr>
											<td colspan="13"></td>
										</tr>
									</tfoot>
					     </table>
						 <table><tr><td></td></tr></table>
				</div>	
			  </div>		
</s:form>