<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants"%>
<html>
<head>
<title><s:text name="garuda.common.label.velos"/></title>

<script>


function getdata(){
	
     window.print(); 
     window.close(); 
	
}

</script>
<style>
@media print
{
tbody {page-break-after:always}
}
.overflowcls{
overflow:scroll;
}
</style>
</head>

<body class="maincontainer overflowcls">
<div align="center" style='width:100%'>
		<s:form	action="" id="packageSlipForm" name="packageSlipForm">
		<s:hidden value="cbuEntityType"></s:hidden>
		<s:if test="packingSlipDetLst!=null && packingSlipDetLst.size()>0">
				<s:iterator value="packingSlipDetLst" var="rowVal">
				<s:set name="pkpakageslip" id="pkpakageslip" value="%{#rowVal[19]}"/>
				</s:iterator>
		</s:if>
		<p align="right"><a href="#" onclick="window.open('cbuPdfReports?repId=174&repName=Packaging Slip&selDate=&params=<s:property value="%{#pkpakageslip}"/>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><s:text name="garuda.shipment.packaging.generatePdf"/></a></p>
			<table width="100%">
			<s:if test="packingSlipDetLst!=null && packingSlipDetLst.size()>0">
				<s:iterator value="packingSlipDetLst" var="rowVal">
				<tr>
					<td width="20%" align="left" rowspan="2">
						<s:text name="garuda.ctShipmentInfo.label.rundate"></s:text>:<strong><s:property value="%{#rowVal[12]}"/></strong><br>
						<s:text name="garuda.ctShipmentInfo.label.runtime"></s:text>:<strong><s:property value="%{#rowVal[13]}"/></strong>
					</td>
					<td width="60%" align="center">
						<h2><s:text name="garuda.ctShipmentInfo.label.nmdpcbu"></s:text></h2>
					</td>
					<td width="20%" align="left">
						<s:text name="garuda.ctShipmentInfo.label.reportR005"></s:text>
					</td>
				</tr>
				
				<tr>
					<td width="60%" align="center">
						<h3><s:text name="garuda.ctShipmentInfo.label.ctreqform"></s:text></h3>
					</td>
					<td width="20%" align="left">
						
					</td>
				</tr>
				</s:iterator>
				</s:if>
				</table>
				<br>
				<table width="100%">
					<tr>
						<td width="25%" align="right" style="vertical-align:top">
							<h4><s:text name="garuda.shipment.packaging.instructions"/>:</h4>
						</td>
						<td width="75%" align="left" style="vertical-align:top">
							<h4><s:text name="garuda.shipping.packaging.info_by_CBB"/></h4>
						</td>
					</tr>
				</table>
			
				<s:if test="packingSlipDetLst!=null && packingSlipDetLst.size()>0">
				<s:iterator value="packingSlipDetLst" var="rowVal">
				<table width="100%">
				<tr>
					<td align="left" style="vertical-align:top">
						<s:text name="garuda.ctShipmentInfo.label.cbbname"></s:text>:
					</td>
					<td align="left" style="vertical-align:top">
						<strong><s:property value="%{#rowVal[3]}"/></strong>
					</td>
					<td align="left" style="vertical-align:top">
						<s:text name="garuda.ctShipmentInfo.label.cbbaddr"></s:text>:
					</td>
					<td align="left" style="vertical-align:top">
						<strong><s:property value="%{#rowVal[4]}"/><br>
						<s:property value="%{#rowVal[5]}"/><br>
						<s:property value="%{#rowVal[6]}"/><br>
						<s:property value="%{#rowVal[7]}"/><br>
						<s:property value="%{#rowVal[8]}"/></strong>
					</td>
				</tr>
				<tr>
					<td align="left" style="vertical-align:top">
						<s:text name="garuda.productinsert.label.nmdpcbuid"></s:text>:
					</td>
					<td align="left" style="vertical-align:top">
						<strong><s:property value="%{#rowVal[0]}"/></strong>
					</td>
					<td align="left" style="vertical-align:top">
						<s:text name="garuda.unitreport.label.localcbuid"></s:text>:
					</td>
					<td align="left" style="vertical-align:top">
						<strong><s:property value="%{#rowVal[1]}"/></strong>
					</td>
				</tr>
				<tr>
					<td align="left" style="vertical-align:top">
						<s:text name="garuda.ctShipmentInfo.label.isbtid"></s:text>:
					</td>
					<td align="left" style="vertical-align:top">
						<strong><s:property value="%{#rowVal[2]}"/></strong>
					</td>
					<td align="left" style="vertical-align:top">
						
					</td>
					<td align="left" style="vertical-align:top">
						
					</td>
				</tr>
				<tr>
					<td align="left" style="vertical-align:top">
						<s:text name="garuda.ctShipmentInfo.label.ctshipdate"></s:text>:
					</td>
					<td align="left" style="vertical-align:top">
						<strong><s:property value="%{#rowVal[9]}"/></strong>
					</td>
					<td align="left" style="vertical-align:top">
						<s:text name="garuda.ctShipmentInfo.label.sampletypeshipped"></s:text>:
					</td>
					<td align="left" style="vertical-align:top">
						<strong><s:property value="%{#rowVal[10]}"/></strong>
					</td>
				</tr>
			</table>
			</s:iterator>
			</s:if>
			<table width="100%">
				<tr>
					<td width="100%" align="left">
						<h4><s:text name="garuda.shipping.packaging.pleaseNote"/></h4>
					</td>
				</tr>
				<tr>
					<td align="left">
						<ul>
							<li>
								<h4><s:text name="garuda.shipping.packaging.non_viable_cell_includes"/></h4> 
							</li>
							<li>
								<h4><s:text name="garuda.shipping.packaging.viable_cell_includes"/></h4>
							</li>
						</ul>
					</td>
				</tr>
			</table>
			<h4>
			<u><s:text name="garuda.shipping.packaging.currentHlaTyping"/></u></h4>
			<table width="100%">
				<tr>
					<th align="left">
						<u><s:text name="garuda.addTyping&Testing.label.locus"></s:text></u>
					</th>
					<th align="left">
						<u><s:text name="garuda.addTyping&Testing.label.method"></s:text></u>
					</th>
					<th align="left">
						<u><s:text name="garuda.addTyping&Testing.label.type1"></s:text></u>
					</th>
					<th align="left">
						<u><s:text name="garuda.addTyping&Testing.label.type2"></s:text></u>
					</th>
					<th align="left">
						<u><s:text name="garuda.addTyping&Testing.label.entrydate"></s:text></u>
					</th>
				</tr>
				<s:if test="packingSlipHladet!=null && packingSlipHladet.size()>0">
					<s:iterator value="packingSlipHladet" var="hlapojo">
					<tr>
					<td align="left"><s:property value="getCodeListDescById(fkHlaCodeId)"/></td>
					<td align="left"><s:property value="getCodeListDescById(fkHlaMethodId)"/></td>
					<td align="left"><s:property value="hlaType1"/></td>
					<td align="left"><s:property value="hlaType2"/></td>
					<td align="left">
				        <s:property value="%{getText('collection.date',{hlaRecievedDate})}"/>
					</td>
					</tr>
					</s:iterator>
				</s:if>
			</table>
			<br>
			<strong><s:text name="garuda.shipping.packaging.nmdpContactInfo"/></strong>
			<br>
				<p><s:text name="garuda.ctShipmentInfo.label.nmdpaddr"></s:text></p>
			<table width="100%">
				<tr>
					<td width="25%" align="left">
						<strong><s:text name="garuda.esignature.label.sign"></s:text></strong>
					</td>
					<td width="25%">
					</td>
					<td width="25%" align="right">
						<strong><s:text name="garuda.esignature.label.date"></s:text></strong>
					</td>
					<td width="25%">
					</td>
				</tr>
				<tr>
					<td width="25%">
						
					</td>
					<td width="25%">
						<strong><s:text name="garuda.esignature.label.formcompletedby"></s:text></strong>
					</td>
					<td width="25%">
						
					</td>
					<td width="25%">
					</td>
				</tr>
				<tr>
					<td width="50%" colspan="2">
						<p><strong><s:text name="garuda.shipping.packaging.phoneNumberForQuery"/></strong></p>
					</td>
					<td width="25%">
						
					</td>
					<td width="25%">
					</td>
				</tr>
			</table>
			</s:form>
		</div>
</body>
</html>