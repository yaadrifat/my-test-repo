<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<jsp:include page="../cb_track_session_logging.jsp" />
<%
String contextpath=request.getContextPath();
%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<script>
$j(".numeric").numeric();
$j(".integer").numeric(false, function() { alert("<s:text name="garuda.cbu.procedure.integerAlert"/>"); this.value = ""; this.focus(); });
$j(".positive").numeric({ negative: false }, function() { alert("<s:text name="garuda.common.cbu.negativeValueAlert"/>"); this.value = ""; this.focus(); });
$j(".positivecheck").numeric();
$j(".positive-integer").numeric({ decimal: false, negative: false }, function() { alert("<s:text name="garuda.common.cbu.positiveIntAlert "/>"); this.value = ""; this.focus(); });
/*$j("#thouUnitPerMlHeparinMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#maxValueMod").decimalMask({separator: ".",decSize: 1,intSize: 4});
$j("#maxValue2Mod").decimalMask({separator: ".",decSize: 1,intSize: 4});
$j("#thouUnitPerMlHeparinModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#fiveUnitPerMlHeparinModper").decimalMask({separator: ".",decSize: 1,intSize: 1});
$j("#fiveUnitPerMlHeparinMod").decimalMask({separator: ".",decSize: 1,intSize: 1});
$j("#tenUnitPerMlHeparinMod").decimalMask({separator: ".",decSize: 1,intSize: 1});
$j("#tenUnitPerMlHeparinModper").decimalMask({separator: ".",decSize: 1,intSize: 1});
$j("#sixPerHydroxyethylStarchMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#sixPerHydroxyethylStarchModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#cpdaMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#cpdaperMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#cpdMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#cpdModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#acdMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#acdModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#otherAnticoagulantMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#otherAnticoagulantModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#hunPerDmsoMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#hunPerDmsoModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#hunPerGlycerolMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#hunPerGlycerolModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#tenPerDextran_40Mod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#tenPerDextran_40Modper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#fivePerHumanAlbuMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#fivePerHumanAlbuModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#twenFiveHumAlbuMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#twenFiveHumAlbuModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#plasmalyteMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#plasmalyteModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#othCryoprotectantMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#othCryoprotectantModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#fivePerDextroseMod").decimalMask({separator: ".",decSize: 1,intSize: 3});
$j("#fivePerDextroseModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#pointNinePerNaclMod").decimalMask({separator: ".",decSize: 1,intSize: 3});
$j("#pointNinePerNaclModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#othDiluentsMod").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#othDiluentsModper").decimalMask({separator: ".",decSize: 1,intSize: 2});
*/

$j(function(){
	$j(".decimalPoint").keydown(function(e){
		var val = $j(this).val();
		var maxLen = $j(this).attr("maxlength");
		if(val.length == 0) {
		    if(e.keyCode == 190 || e.keyCode == 110)
			return false;
		}

		 /* if(e.keyCode!=8 && e.keyCode!=46)
		  {
			  if(val.length == maxLen -1){
				  if(e.keyCode != null)
				  {}
				  else 	
				  	$j(this).val(val+"0");
				  
			  } 	 	
			  if(val.length == maxLen-2){
				  if(e.which!=190 || e.which != 110){
					   $j(this).val(val+".");
				  }
			  }
			  
		  }*/
	});
});

function checkFormat(val,id)
{
	if(val == null || val == "")
		return false;
	if(val.indexOf('.') != -1)
	{}
	else
	{
		$j("#"+id).val(val+".0");
	}
	if(val.charAt(val.length -1) == '.'){
		$j("#"+id).val(val+"0");
	}
	if(val.charAt(val.length -3) == '.'){
		$j("#"+id).val(val.slice(0,-1)); 
	}
	if(val.charAt(val.length -4) == '.'){
		$j("#"+id).val(val.slice(0,-2)); 
	}
	if(val.charAt(val.length -5) == '.'){
		$j("#"+id).val(val.slice(0,-3)); 
	}

	getOtherAnticoagulantMod();
	getOtherCyroprotectantMod();
	getothDiluents();
}

function eitherOne(id){
	if($j("#"+id).val() != '') {
		$j("#"+id+"per").attr('disabled','disabled');
	}
	else
	{
	   $j("#"+id+"per").removeAttr('disabled');
	}
}

function eitherOneOther(id){
	var id1 = id.substring(0, id.length - 3)
	if($j("#"+id).val() != '') {
		$j("#"+id1).attr('disabled','disabled');
	}
	else
	{
		$j("#"+id1).removeAttr('disabled');
	}
}

function submitCBBProcedureInfo(formname, procID,val){

	modalFormSubmitRefreshDiv(formname,'submitCbbProcInfo?pkProcId='+procID+'&loginIds='+$j('#loginIds').val()+'&assignedToCordFlag='+$j("#assignedToCordFlag").val(),'main');
	refreshCbbProcDiv('getCBBProcedures?loginIds='+$j("#loginIds").val(),'cbbProcedureMaindiv','cbbProcedureForm');
}

	function customRangeChk(input) {
	/*  if (input.id == 'datepicker6') {
	    return {
	      minDate: jQuery('#datepicker1').datepicker("getDate")
	    };
	  } else if (input.id == 'datepicker1') {
	    return {
	      maxDate: jQuery('#datepicker6').datepicker("getDate")
	    };
	  }*/
	  	 val= $j("#datepicker6").val();
		 if (input.id == 'datepicker6') {
			 if($j("#datepicker1").val()!="" && $j("#datepicker1").val()!=null){
			    var minTestDate=new Date($j("#datepicker1").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
				  $j('#datepicker6').datepicker( "option", "minDate", new Date(y1,m1,d1));
			 }else{
				 $j('#datepicker6').datepicker( "option", "minDate", "");
			}
		} else if (input.id == 'datepicker1') {
			if($j("#datepicker6").val()!="" && $j("#datepicker6").val()!=null){
				  var minTestDate=new Date($j("#datepicker6").val()); 	 
					var d1 = minTestDate.getDate();
					var m1 = minTestDate.getMonth();
					var y1 = minTestDate.getFullYear();
				  $j('#datepicker1').datepicker( "option", "maxDate", new Date(y1,m1,d1));
			  }else{
				  $j('#datepicker1').datepicker( "option", "maxDate", "");
			}
		}

		 	 $j("#datepicker6").val(val);	
	}


	function customRangeCollChk(input) {
		val= $j("#datepicker6").val();
		  /*if (input.id == 'datepicker6') {
		    return {
		      minDate: jQuery('#procCordsMaxCollDt').datepicker("getDate")
		    };
		  } else if (input.id == 'procCordsMaxCollDt') {
		    return {
		      maxDate: jQuery('#datepicker6').datepicker("getDate")
		    };
		  }*/
		 if (input.id == 'datepicker6') {
			 if($j("#procCordsMaxCollDt").val()!="" && $j("#procCordsMaxCollDt").val()!=null){
			    var minTestDate=new Date($j("#procCordsMaxCollDt").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
				  $j('#datepicker6').datepicker( "option", "minDate", new Date(y1,m1,d1));
			 }else{
				 $j('#datepicker6').datepicker( "option", "minDate", "");
				}
			  } else if (input.id == 'procCordsMaxCollDt') {
				  if($j("#datepicker6").val()!="" && $j("#datepicker6").val()!=null){
				  var minTestDate=new Date($j("#datepicker6").val()); 	 
					var d1 = minTestDate.getDate();
					var m1 = minTestDate.getMonth();
					var y1 = minTestDate.getFullYear();
				  $j('#procCordsMaxCollDt').datepicker( "option", "maxDate", new Date(y1,m1,d1));
				  }else{
					  $j('#procCordsMaxCollDt').datepicker( "option", "maxDate", "");
				}
			  }
		  	  $j("#datepicker6").val(val);		
		}
		
	/*jQuery(function() {
		  jQuery('#datepicker8, #datepicker7').datepicker('option', {
		    beforeShow: customRange3
		  });
		});

		function customRange3(input) {
		  if (input.id == 'datepicker8') {
		    return {
		      minDate: jQuery('#datepicker7').datepicker("getDate")
		    };
		  } else if (input.id == 'datepicker7') {
		    return {
		      maxDate: jQuery('#datepicker8').datepicker("getDate")
		    };
		  }
		}*/

function getIfAutomated(){
	if($j("#fkProcMethIdMod").val()==$j("#processingMethod").val()){
		$j("#ifautomatedspan").show();
		$j("#fkIfAutomatedMod").removeAttr("disabled");
		$j("#fkIfAutomatedMod").removeAttr("readonly");
		}
	else{
		$j("#ifautomatedspan").hide();
		$j("#fkIfAutomatedMod").val("");
		$j("#fkIfAutomatedMod").attr("disabled","enabled");
		$j("#fkIfAutomatedMod").attr("readonly","false");
		$j("#otherProcessing").val("");
		$j("#otherAutomatedMod").hide();
		}
}
function getOtherAutomated(){
   
	if($j("#fkIfAutomatedMod").val()==$j("#IfAutomatedOther").val()){
		$j("#otherAutomatedMod").show();
		}
	else{
		$j("#otherProcessing").val("");		
		$j("#otherAutomatedMod").hide();
		}
}
function getOtherProdModi(){
	if($j("#fkProductModificationMod").val()==$j("#ProdModiOther").val()){
		$j("#otherProdModiMod").show();
		}
	else{
		$j("#otherProdModi").val("");
		$j("#otherProdModiMod").hide();
		}
}
function getOtherFreezerManufac(){
	if($j("#fkFreezManufacMod").val()==$j("#FreezManuOther").val()){
		$j("#otherFreezerManufacMod").show();
		}
	else{
		$j("#otherFreezManufac").val("");
		$j("#otherFreezerManufacMod").hide();
		}
}

function cbuAutoDefer(val,Id){
	if (val == '<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FROZEN_IN,@com.velos.ordercomponent.util.VelosGarudaConstants@FROZEN_IN_VIALS)"/>'	 || val == '<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FROZEN_IN,@com.velos.ordercomponent.util.VelosGarudaConstants@FROZEN_IN_TUBES)"/>')
	{
		jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
				function(r) {
					if (r == false) {
					   $j("#"+Id).val("");
					  }else if(r == true){
						  autoDefer = true;
					  }
					});
	}
	
}

function tempAutoDefer(val,Id){
	if (val == '<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@STORAGE_TEMPERATURE,@com.velos.ordercomponent.util.VelosGarudaConstants@STORAGE_TEMPERATURE_GTMINUSONETHIRTYFIVE)"/>')
	{
		jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
				function(r) {
					if (r == false) {
					   $j("#"+Id).val("");
					  }else if(r == true){
						  autoDefer = true;
					  }
					});
	}
}

function segAutoDefer(val,Id){
	if (val !='' && (val == 0 || val == 1))
	{
		jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
				function(r) {
					if (r == false) {
					   $j("#"+Id).val("");
					  }else if(r == true){
						  autoDefer = true;
					  }
					});
	}
}

function getIfBagSelected(){
	if($j("#fkFrozenInMod").val()==$j("#pkFrozenInBagMod").val()){
		$j("#ifBagSelectedMod").show();
		$j("#otherFrozenInDivMod").hide();
		$j("#otherBagTypeModDiv").hide();
		$j("#bag12TypeDivMod").hide();
		$j("#bagTypeDivMod").hide();
		$j("#otherFrozenContMod").val("");
		}
	else if($j("#fkFrozenInMod").val()==$j("#FrozenInOtherMod").val()){
		$j("#ifBagSelectedMod").hide();
		$j("#otherFrozenInDivMod").show();
		$j("#otherBagTypeModDiv").hide();
		$j("#bag12TypeDivMod").hide();
		$j("#bagTypeDivMod").hide();
		$j("#fkNoOfBagsMod").val("");
	}
	else{
		$j("#ifBagSelectedMod").hide();
		$j("#otherFrozenInDivMod").hide();
		$j("#otherBagTypeModDiv").hide();
		$j("#bag12TypeDivMod").hide();
		$j("#bagTypeDivMod").hide();
		$j("#fkNoOfBagsMod").val("");
		$j("#otherFrozenContMod").val("");
	}
	getNoOfBagsSelected();
}


function getNoOfBagsSelected() {

	if($j("#fkNoOfBagsMod").val() == $j("#pkBag1Type").val()){
		$j("#otherBagTypeModDiv").hide();
		$j("#bagTypeDivMod").show();
		$j("#bagTypeLabel1").hide();
		$j("#cryoLabel1").hide();
		$j("#maxVolLabel1").hide();
		$j("#bagTypeLabel").show();
		$j("#cryoLabel").show();
		$j("#maxVolLabel").show();
		$j("#bag12TypeDivMod").hide();
		$j("#fkBag2TypeMod").val("");
		$j("#cryoBag2ManufacMod").val("");
		$j("#maxValue2Mod").val("");
		$j("#otherBagTypeMod").val("");
		
		}
	else if($j("#fkNoOfBagsMod").val() == $j("#pkBag2Type").val()){
		
		$j("#otherBagTypeModDiv").hide();
		$j("#bagTypeDivMod").show();
		$j("#bagTypeLabel").hide();
		$j("#cryoLabel").hide();
		$j("#maxVolLabel").hide();
		$j("#bagTypeLabel1").show();
		$j("#cryoLabel1").show();
		$j("#maxVolLabel1").show();
		$j("#bag12TypeDivMod").show();
		$j("#otherBagTypeMod").val("");
	}
	else if($j("#fkNoOfBagsMod").val() == $j("#pkNoOfBagsOthers").val()){
		$j("#otherBagTypeModDiv").show();
		$j("#bagTypeDivMod").hide();
		$j("#bag12TypeDivMod").hide();
		$j("#fkBag1TypeMod").val("");
		$j("#cryoBagManufacMod").val("");
		$j("#maxValueMod").val("");
		$j("#fkBag2TypeMod").val("");
		$j("#cryoBag2ManufacMod").val("");
		$j("#maxValue2Mod").val("");
	}
	else
	{
		$j("#otherBagTypeModDiv").hide();
		$j("#bagTypeDivMod").hide();
		$j("#bag12TypeDivMod").hide();
		$j("#fkBag1TypeMod").val("");
		$j("#cryoBagManufacMod").val("");
		$j("#maxValueMod").val("");
		$j("#fkBag2TypeMod").val("");
		$j("#cryoBag2ManufacMod").val("");
		$j("#maxValue2Mod").val("");
		$j("#otherBagTypeMod").val("");
	}



	
}



$j(function(){
	
	$j("#cbbProcedureFormUpdate").validate({
			
		rules:{	
				"cbbProcedurePojo.procName":{required:true},
				"cbbProcedurePojo.procStartDateStr":{required:true,dpDate:true},
				"cbbProcedurePojo.fkSite":{required:true},
				"cbbProcedureInfoPojo.fkProcMethId":{required:true},
				"cbbProcedureInfoPojo.fkIfAutomated":{required:
														{
															depends: function(element){
																	return $j("#fkProcMethIdMod").val()==$j("#processingMethod").val();
																}
														}
													},
				"cbbProcedureInfoPojo.otherProcessing":{required:
														{
														depends: function(element){
																return $j("#fkIfAutomatedMod").val()==$j("#IfAutomatedOther").val();
															}
														}
														},
				"cbbProcedureInfoPojo.fkProductModification":{required:true},
				"cbbProcedureInfoPojo.otherProdModi":{required:
														{
															depends: function(element){
																	return $j("#fkProductModificationMod").val()==$j("#ProdModiOther").val();
																}
															}
														},
				//"cbbProcedureInfoPojo.fkStorMethod":{required:true},
				//"cbbProcedureInfoPojo.fkFreezManufac":{required:true},
				"cbbProcedureInfoPojo.otherFreezManufac":{required:
															{
																depends: function(element){
																		return $j("#fkFreezManufacMod").val()==$j("#FreezManuOther").val();
																	}
																}
															},
				"cbbProcedureInfoPojo.fkFrozenIn":{required:true},

				"cbbProcedureInfoPojo.noOfBags":{required:
												{
												depends: function(element){
														return $j("#fkFrozenInMod").val()==$j("#pkFrozenInBagMod").val();
													}
												}
											},
				
				"cbbProcedureInfoPojo.fkBag1Type":{required:
											{
												depends: function(element){

													if($j("#fkNoOfBagsMod").val()==$j("#pkBag1Type").val()){
														return true;
													}
													if($j("#fkNoOfBagsMod").val()==$j("#pkBag2Type").val()){
														return true;
													}
													}
												}
											},
				"cbbProcedureInfoPojo.fkBag2Type":{required:
											{
												depends: function(element){
														return $j("#fkNoOfBagsMod").val()==$j("#pkBag2Type").val();
													}
												}
											},	

				"cbbProcedureInfoPojo.otherBagType":{required:
									{
										depends: function(element){
												return $j("#fkNoOfBagsMod").val()==$j("#pkNoOfBagsOthers").val();
											}
										}
									},
				"cbbProcedureInfoPojo.otherBagType1":{required:
									{
										depends: function(element){
												return $j("#fkBag1TypeMod").val()==$j("#pkOtherBagTypeMod").val();
											}
										}
									},
				"cbbProcedureInfoPojo.otherBagType2":{required:
									{
										depends: function(element){
												return $j("#fkBag2TypeMod").val()==$j("#pkOtherBagTypeMod").val();
											}
										}
									},
				"cbbProcedureInfoPojo.cryoBagManufac":{required:
														{
															depends: function(element){

																if($j("#fkNoOfBagsMod").val()==$j("#pkBag1Type").val()){
																	return true;
																}
																if($j("#fkNoOfBagsMod").val()==$j("#pkBag2Type").val()){
																	return true;
																}
																}
															}

														},
				"cbbProcedureInfoPojo.fkStorTemp":{required:true},																								
				"cbbProcedureInfoPojo.maxValue":{required:
													{
														depends: function(element){
															if($j("#fkNoOfBagsMod").val()==$j("#pkBag1Type").val()){
																return true;
															}
															if($j("#fkNoOfBagsMod").val()==$j("#pkBag2Type").val()){
																return true;
															}
															}
														}, range:[0,9999]

													},
				"cbbProcedureInfoPojo.cryoBagManufac2":{required:
													{
														depends: function(element){
																return $j("#fkNoOfBagsMod").val()==$j("#pkBag2Type").val();
															}
														}

													},
				"cbbProcedureInfoPojo.maxValue2":{required:
													{
														depends: function(element){
																return $j("#fkNoOfBagsMod").val()==$j("#pkBag2Type").val();
															}
														}, range:[0,9999]

													},
				"cbbProcedureInfoPojo.otherFrozenCont":{required:
															{
																depends: function(element){
																		return $j("#fkFrozenInMod").val()==$j("#FrozenInOtherMod").val();
																	}
																}
											
															},
				//"cbbProcedureInfoPojo.fkContrlRateFreezing":{required:true},
				"cbbProcedureInfoPojo.noOfIndiFrac":{required:true,range: [1, 99]},
				"cbbProcedureInfoPojo.thouUnitPerMlHeparin":{range: [0, 20]},
				"cbbProcedureInfoPojo.thouUnitPerMlHeparinper":{range: [0, 25]},
				"cbbProcedureInfoPojo.fiveUnitPerMlHeparin":{range:[0.4,4]},
				"cbbProcedureInfoPojo.tenUnitPerMlHeparin":{range:[0,2]},
				"cbbProcedureInfoPojo.sixPerHydroxyethylStarch":{range:[0,50]},
				"cbbProcedureInfoPojo.cpda":{range:[0,50]},
				"cbbProcedureInfoPojo.cpd":{range:[0,50]},
				"cbbProcedureInfoPojo.acd":{range:[0,50]},
				"cbbProcedureInfoPojo.otherAnticoagulant":{range:[0,50]},
				
				"cbbProcedureInfoPojo.hunPerDmso":{range:[0,10]},
				"cbbProcedureInfoPojo.hunPerGlycerol":{range:[0,10]},
				"cbbProcedureInfoPojo.tenPerDextran_40":{range:[0,10]},
				"cbbProcedureInfoPojo.fivePerHumanAlbu":{range:[0,10]},
				"cbbProcedureInfoPojo.twenFiveHumAlbu":{range:[0,10]},
				"cbbProcedureInfoPojo.plasmalyte":{range:[0,10]},
				"cbbProcedureInfoPojo.othCryoprotectant":{range:[0,50]},
				"cbbProcedureInfoPojo.fivePerDextrose":{range:[0,100]},
				"cbbProcedureInfoPojo.pointNinePerNacl":{range:[0,100]},
				"cbbProcedureInfoPojo.othDiluents":{range:[0,100]},
				"cbbProcedureInfoPojo.noOfSegments":{required:true},
				"cbbProcedureInfoPojo.filterPaper":{required:true},
				"cbbProcedureInfoPojo.rbcPallets":{required:true},
				"cbbProcedureInfoPojo.noOfExtrDnaAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfNonviableAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfSerumAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfPlasmaAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfViableCellAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfExtrDnaMaterAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfCellMaterAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfSerumMaterAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfPlasmaMaterAliquots":{required:true},
				"cbbProcedureInfoPojo.fiveUnitPerMlHeparinper":{range:[0,5]},
				"cbbProcedureInfoPojo.tenUnitPerMlHeparinper":{range:[0,3]},
				"cbbProcedureInfoPojo.sixPerHydroxyethylStarchper":{range:[0,25]},
				"cbbProcedureInfoPojo.cpdaper":{range:[0,60]},
				"cbbProcedureInfoPojo.cpdper":{range:[0,60]},
				"cbbProcedureInfoPojo.acdper":{range:[0,60]},
				"cbbProcedureInfoPojo.otherAnticoagulant":{range:[0,50]},
				"cbbProcedureInfoPojo.otherAnticoagulantper":{range:[0,60]},
				"cbbProcedureInfoPojo.hunPerDmsoper":{range:[0,20]},
				"cbbProcedureInfoPojo.hunPerGlycerolper":{range:[0,20]},
				"cbbProcedureInfoPojo.tenPerDextran_40per":{range:[0,20]},
				"cbbProcedureInfoPojo.fivePerHumanAlbuper":{range:[0,20]},
				"cbbProcedureInfoPojo.twenFiveHumAlbuper":{range:[0,20]},
				"cbbProcedureInfoPojo.plasmalyteper":{range:[0,40]},
				"cbbProcedureInfoPojo.othCryoprotectantper":{range:[0,60]},
				"cbbProcedureInfoPojo.fivePerDextroseper":{range:[0,60]},
				"cbbProcedureInfoPojo.pointNinePerNaclper":{range:[0,60]},
				"cbbProcedureInfoPojo.othDiluentsper":{range:[0,60]},
				"cbbProcedureInfoPojo.noOfViableSampleNotRep":{required:true},
				"cbbProcedureInfoPojo.noOfOthRepAliqProd":{required:true},
				"cbbProcedureInfoPojo.noOfOthRepAliqAltCond":{required:true},
				"cbbProcedureInfoPojo.specifyOthAnti":{required:
				{
					depends: function(element){
							return ($j("#otherAnticoagulantMod").val()!= '' || $j("#otherAnticoagulantModper").val() != '');
						}
					}

				},
				"cbbProcedureInfoPojo.specOthCryopro":{required:
				{
					depends: function(element){
							return ($j("#othCryoprotectantMod").val()!= '' || $j("#othCryoprotectantModper").val() != '');
						}
					}

				},
				"cbbProcedureInfoPojo.specOthDiluents":{required:
				{
					depends: function(element){
							return ($j("#othDiluentsMod").val()!= '' || $j("#othDiluentsModper").val() != '');
						}
					}

				},
				},
			messages:{	
					"cbbProcedurePojo.procName":"<s:text name="garuda.pf.procedure.procnum"/>",
					"cbbProcedurePojo.procStartDateStr":{required:"<s:text name="garuda.pf.procedure.startdate"/>"},
					"cbbProcedurePojo.fkSite":"<s:text name="garuda.pf.procedure.site"/>",
					"cbbProcedureInfoPojo.fkProcMethId":"<s:text name="garuda.pf.procedure.procmethod"/>",
					"cbbProcedureInfoPojo.fkIfAutomated":"<s:text name="garuda.pf.procedure.ifauto"/>",
					"cbbProcedureInfoPojo.otherProcessing":"<s:text name="garuda.pf.procedure.otherautometh"/>",
					"cbbProcedureInfoPojo.fkProductModification":"<s:text name="garuda.pf.procedure.prodmodif"/>",
					"cbbProcedureInfoPojo.otherProdModi":"<s:text name="garuda.pf.procedure.otherprodmod"/>",
					//"cbbProcedureInfoPojo.fkStorMethod":"<s:text name="garuda.pf.procedure.storagemeth"/>",
					//"cbbProcedureInfoPojo.fkFreezManufac":"<s:text name="garuda.pf.procedure.freezmanuf"/>",
					"cbbProcedureInfoPojo.otherFreezManufac":"<s:text name="garuda.pf.proceudre.othfreezmanuf"/>",
					"cbbProcedureInfoPojo.fkFrozenIn":"<s:text name="garuda.pf.procedure.frozenin"/>",
					"cbbProcedureInfoPojo.noOfBags":"<s:text name="garuda.pf.procedure.numbags"/>",
					"cbbProcedureInfoPojo.fkBag1Type":"<s:text name="garuda.pf.procedure.bag1type"/>",
					"cbbProcedureInfoPojo.fkBag2Type":"<s:text name="garuda.pf.procedure.bag2type"/>",
  					"cbbProcedureInfoPojo.otherBagType":"<s:text name="garuda.pf.procedure.othbagtype"/>",
					"cbbProcedureInfoPojo.otherBagType1":"<s:text name="garuda.pf.procedure.othbagtype"/>",
					"cbbProcedureInfoPojo.otherBagType2":"<s:text name="garuda.pf.procedure.othbagtype"/>",
					"cbbProcedureInfoPojo.cryoBagManufac":"<s:text name="garuda.pf.procedure.cryobagmanu"/>",
					"cbbProcedureInfoPojo.cryoBagManufac2":"<s:text name="garuda.pf.procedure.cryobagmanu"/>",
					"cbbProcedureInfoPojo.fkStorTemp":"<s:text name="garuda.pf.procedure.storagetemp"/>",
					"cbbProcedureInfoPojo.maxValue":"<s:text name="garuda.pf.procedure.maxval"/>",
					"cbbProcedureInfoPojo.maxValue2":"<s:text name="garuda.pf.procedure.maxval"/>",
					//"cbbProcedureInfoPojo.fkContrlRateFreezing":"<s:text name="garuda.pf.procedure.controlratefreez"/>",
					"cbbProcedureInfoPojo.noOfIndiFrac":"<s:text name="garuda.common.range.decbetween1to99"/>",
					"cbbProcedureInfoPojo.thouUnitPerMlHeparin":"<s:text name="garuda.common.range.decbetween0to20"/>",
				"cbbProcedureInfoPojo.fiveUnitPerMlHeparin":"<s:text name="garuda.common.range.decbetween0p4to4"/>",
				"cbbProcedureInfoPojo.tenUnitPerMlHeparin":"<s:text name="garuda.common.range.decbetween0to2"/>",
				"cbbProcedureInfoPojo.sixPerHydroxyethylStarch":"<s:text name="garuda.common.range.decbetween0to50"/>",
				"cbbProcedureInfoPojo.cpda":"<s:text name="garuda.common.range.decbetween0to50"/>",
				"cbbProcedureInfoPojo.cpd":"<s:text name="garuda.common.range.decbetween0to50"/>",
				"cbbProcedureInfoPojo.acd":"<s:text name="garuda.common.range.decbetween0to50"/>",
				"cbbProcedureInfoPojo.otherAnticoagulant":"<s:text name="garuda.common.range.decbetween0to50"/>",
				"cbbProcedureInfoPojo.hunPerDmso":"<s:text name="garuda.common.range.decbetween0to10"/>",
				"cbbProcedureInfoPojo.hunPerGlycerol":"<s:text name="garuda.common.range.decbetween0to10"/>",
				"cbbProcedureInfoPojo.tenPerDextran_40":"<s:text name="garuda.common.range.decbetween0to10"/>",
				"cbbProcedureInfoPojo.fivePerHumanAlbu":"<s:text name="garuda.common.range.decbetween0to10"/>",
				"cbbProcedureInfoPojo.twenFiveHumAlbu":"<s:text name="garuda.common.range.decbetween0to10"/>",
				"cbbProcedureInfoPojo.plasmalyte":"<s:text name="garuda.common.range.decbetween0to10"/>",
				"cbbProcedureInfoPojo.othCryoprotectant":"<s:text name="garuda.common.range.decbetween0to50"/>",
				"cbbProcedureInfoPojo.fivePerDextrose":"<s:text name="garuda.common.range.decbetween0to100"/>",
				"cbbProcedureInfoPojo.pointNinePerNacl":"<s:text name="garuda.common.range.decbetween0to100"/>",
				"cbbProcedureInfoPojo.othDiluents":"<s:text name="garuda.common.range.decbetween0to100"/>",
				"cbbProcedureInfoPojo.noOfSegments":"<s:text name="garuda.pf.procedure.noofsegments"/>",
				"cbbProcedureInfoPojo.filterPaper":"<s:text name="garuda.cbu.samples.filterpaper"/>",
				"cbbProcedureInfoPojo.rbcPallets":"<s:text name="garuda.cbu.samples.rbcpallets"/>",
				"cbbProcedureInfoPojo.noOfExtrDnaAliquots":"<s:text name="garuda.cbu.samples.numextracteddna"/>",
				"cbbProcedureInfoPojo.noOfNonviableAliquots":"<s:text name="garuda.cbu.samples.numnonviable"/>",
				"cbbProcedureInfoPojo.noOfSerumAliquots":"<s:text name="garuda.cbu.samples.numserum"/>",
				"cbbProcedureInfoPojo.noOfPlasmaAliquots":"<s:text name="garuda.cbu.samples.numplasma"/>",
				"cbbProcedureInfoPojo.noOfViableCellAliquots":"<s:text name="garuda.cbu.samples.numviable"/>",
				"cbbProcedureInfoPojo.noOfExtrDnaMaterAliquots":"<s:text name="garuda.cbu.samples.matextracteddna"/>",
				"cbbProcedureInfoPojo.noOfCellMaterAliquots":"<s:text name="garuda.cbu.samples.nummiscmat"/>",
				"cbbProcedureInfoPojo.noOfSerumMaterAliquots":"<s:text name="garuda.cbu.samples.nummatserum"/>",
				"cbbProcedureInfoPojo.noOfPlasmaMaterAliquots":"<s:text name="garuda.cbu.samples.nummatplasma"/>",
				"cbbProcedureInfoPojo.otherFrozenCont":"<s:text name="garuda.pf.procedure.frozencontainer"/>",
				"cbbProcedureInfoPojo.thouUnitPerMlHeparinper":"<s:text name="garuda.common.range.decbetween0to25"/>",
				"cbbProcedureInfoPojo.fiveUnitPerMlHeparinper":"<s:text name="garuda.common.range.decbetween0to5"/>",
				"cbbProcedureInfoPojo.tenUnitPerMlHeparinper":"<s:text name="garuda.common.range.decbetween0to3"/>",
				"cbbProcedureInfoPojo.sixPerHydroxyethylStarchper":"<s:text name="garuda.common.range.decbetween0to25"/>",
				"cbbProcedureInfoPojo.cpdaper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.cpdper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.acdper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.otherAnticoagulantper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.hunPerDmsoper":"<s:text name="garuda.common.range.decbetween0to20"/>",
				"cbbProcedureInfoPojo.hunPerGlycerolper":"<s:text name="garuda.common.range.decbetween0to20"/>",
				"cbbProcedureInfoPojo.tenPerDextran_40per":"<s:text name="garuda.common.range.decbetween0to20"/>",
				"cbbProcedureInfoPojo.fivePerHumanAlbuper":"<s:text name="garuda.common.range.decbetween0to20"/>",
				"cbbProcedureInfoPojo.twenFiveHumAlbuper":"<s:text name="garuda.common.range.decbetween0to20"/>",
				"cbbProcedureInfoPojo.plasmalyteper":"<s:text name="garuda.common.range.decbetween0to40"/>",
				"cbbProcedureInfoPojo.othCryoprotectantper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.fivePerDextroseper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.pointNinePerNaclper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.othDiluentsper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.noOfViableSampleNotRep":"<s:text name="garuda.cbu.samples.numviablesamplenotrep"/>",
				"cbbProcedureInfoPojo.noOfOthRepAliqProd":"<s:text name="garuda.cbu.samples.numothrepaliqprod"/>",
				"cbbProcedureInfoPojo.noOfOthRepAliqAltCond":"<s:text name="garuda.cbu.samples.numothrepaliqaltcond"/>"
					}
								});
});
$j(function() {
	var frozenProd = new Array("thouUnitPerMlHeparinMod","cpda","fiveUnitPerMlHeparinMod","cpdMod","tenUnitPerMlHeparinMod",
			"acdMod","sixPerHydroxyethylStarchMod","otherAnticoagulantMod","hunPerDmsoMod","twenFiveHumAlbuMod","hunPerGlycerolMod","plasmalyteMod",
			"tenPerDextran_40Mod","othCryoprotectantMod","fivePerHumanAlbuMod","fivePerDextroseMod","pointNinePerNaclMod","othDiluentsMod");
	for(i=0;i<frozenProd.length;i++){
		eitherOneEdit(frozenProd[i]);
		eitherOneEditOther(frozenProd[i]+"per");
	}
	getIfAutomated();
	getOtherAutomated();
	getOtherProdModi();
	getOtherFreezerManufac();
	getIfBagSelected();
	getOtherAnticoagulantMod();
	getOtherCyroprotectantMod();
	getothDiluents();
	//validateDate('datepicker6','datepicker1','invalidDate5','invalidDate4');
	//validateDate('datepicker8','datepicker7','invalidDate7','invalidDate6');
	$j("#ifautomatedspan").hide();
	getOtherBagTypeSelected($j("#fkBag1TypeMod").val());
	getOtherBagType2Selected($j("#fkBag2TypeMod").val());
	
	//alert("assival::"+$j("#assignedToCordFlag").val());
	getDatePic();
	if($j("#assignedToCordFlag").val()!='0' && $j("#assignedToCordFlag").val()!=''){
		$j('#cbbDiv input[type=text],#cbbDiv select').each(function(index,el) {
			$j(el).attr('disabled',true);
		});
		$j('#datepicker1').datepicker("disable");
		$j('#datepicker6').removeAttr('disabled');
		
		var maxCollDt = new Date($j("#procCordsMaxCollDt").val());
		var procStartDt = new Date($j("#datepicker1").val());
		if(maxCollDt < procStartDt) {
			val= $j("#datepicker6").val();
			jQuery('#datepicker6, #datepicker1').datepicker('option', {
			    beforeShow: customRangeChk
			  }).focus(function(){
					if(!$j("#ui-datepicker-div").is(":visible"))
						customRangeChk(this);
					if($j(this).val()!=""){
						var thisdate = new Date($j(this).val());
						if(!isNaN(thisdate)){
							var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
							$j(this).val(formattedDate);
						}
					}
				});
			$j("#datepicker6").val(val);
		}
		else
		{
			val= $j("#datepicker6").val();
			jQuery('#datepicker6, #procCordsMaxCollDt').datepicker('option', {
			    beforeShow: customRangeCollChk
			  }).focus(function(){
					if(!$j("#ui-datepicker-div").is(":visible"))
						customRangeCollChk(this);
					if($j(this).val()!=""){
						var thisdate = new Date($j(this).val());
						if(!isNaN(thisdate)){
							var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
							$j(this).val(formattedDate);
						}
					}
				});
			$j("#datepicker6").val(val);
		}
	} else {
		val= $j("#datepicker6").val();
		jQuery('#datepicker6, #datepicker1').datepicker('option', {
	    beforeShow: customRangeChk
	  }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRangeChk(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
		$j("#datepicker6").val(val);
	}
	$j(".hasDatepicker").each(function(){
		if(!$j(this).is(':visible'))
			$j(this).next('img').hide();
	});
});

function getTotaliquots(){
	var var_noOfExtrDnaAliquotsMod=$j("#noOfExtrDnaAliquotsMod").val();
	var var_noOfNonviableAliquotsMod=$j("#noOfNonviableAliquotsMod").val();
	var var_noOfPlasmaAliquotsMod=$j("#noOfPlasmaAliquotsMod").val();
	var var_noOfSerumAliquotsMod=$j("#noOfSerumAliquotsMod").val();
	var var_noOfViableCellAliquotsMod=$j("#noOfViableCellAliquotsMod").val();
	
	if(var_noOfExtrDnaAliquotsMod==""||var_noOfExtrDnaAliquotsMod==null){
		var_noOfExtrDnaAliquotsMod=0;
	}
	if(var_noOfNonviableAliquotsMod==""||var_noOfNonviableAliquotsMod==null){
		var_noOfNonviableAliquotsMod=0;
	}
	if(var_noOfPlasmaAliquotsMod==""||var_noOfPlasmaAliquotsMod==null){
		var_noOfPlasmaAliquotsMod=0;
	}
	if(var_noOfSerumAliquotsMod==""||var_noOfSerumAliquotsMod==null){
		var_noOfSerumAliquotsMod=0;
	}
	if(var_noOfViableCellAliquotsMod==""||var_noOfViableCellAliquotsMod==null){
		var_noOfViableCellAliquotsMod=0;
	}
	
	$j("#totCbuAliquotsMod").val(parseInt(var_noOfExtrDnaAliquotsMod)+parseInt(var_noOfNonviableAliquotsMod)+parseInt(var_noOfPlasmaAliquotsMod)+parseInt(var_noOfSerumAliquotsMod)+parseInt(var_noOfViableCellAliquotsMod));
}

function getTotaliquots1(){
	
	var var_noOfExtrDnaMaterAliquotsMod=$j("#noOfExtrDnaMaterAliquotsMod").val();
	var var_noOfCellMaterAliquotsMod=$j("#noOfCellMaterAliquotsMod").val();
	var var_noOfSerumMaterAliquotsMod=$j("#noOfSerumMaterAliquotsMod").val();
	var var_noOfPlasmaMaterAliquotsMod=$j("#noOfPlasmaMaterAliquotsMod").val();
	
	if(var_noOfExtrDnaMaterAliquotsMod==""||var_noOfExtrDnaMaterAliquotsMod==null){
		var_noOfExtrDnaMaterAliquotsMod=0;
	}
	if(var_noOfCellMaterAliquotsMod==""||var_noOfCellMaterAliquotsMod==null){
		var_noOfCellMaterAliquotsMod=0;
	}
	if(var_noOfSerumMaterAliquotsMod==""||var_noOfSerumMaterAliquotsMod==null){
		var_noOfSerumMaterAliquotsMod=0;
	}
	if(var_noOfPlasmaMaterAliquotsMod==""||var_noOfPlasmaMaterAliquotsMod==null){
		var_noOfPlasmaMaterAliquotsMod=0;
	}
	$j("#totMaterAliquotsMod").val(parseInt(var_noOfExtrDnaMaterAliquotsMod)+parseInt(var_noOfCellMaterAliquotsMod)+parseInt(var_noOfSerumMaterAliquotsMod)+parseInt(var_noOfPlasmaMaterAliquotsMod));
}

function getOtherAnticoagulantMod(){
	if( ($j("#otherAnticoagulantMod").val() != '') ||  ($j("#otherAnticoagulantModper").val() != '') ){	
		
		$j("#otherAnticoagulantDivMod").show();
	}
	else{
		$j("#specifyOthAntiMod").val("");
		$j("#otherAnticoagulantDivMod").hide();
		}
}
function getOtherCyroprotectantMod(){
	if( ($j("#othCryoprotectantMod").val() != '') ||  ($j("#othCryoprotectantModper").val() != '') ){	
		$j("#otherCryoprotectantDivMod").show();
	}
	else{
		$j("#specOthCryoproMod").val("");
		$j("#otherCryoprotectantDivMod").hide();
		}
}


function getothDiluents(){
		if( ($j("#othDiluentsMod").val() != '') ||  ($j("#othDiluentsModper").val() != '') ){		
		$j("#specOthDiluentsDiv").show();
	}
	else{
		$j("#specOthDiluentsMod").val("");
		$j("#specOthDiluentsDiv").hide();
		}	
}

function validateDate(enddate,strtdate,strtdatelabl,enddatelabl){
	var date =$j("#"+enddate).val(); 
	var date1 =$j("#"+strtdate).val();
	
	if(date1!=""){
		document.getElementById(enddatelabl).innerHTML="";
		date = Date.parse(date);
		date1 = Date.parse(date1);
	if(date < date1){
		document.getElementById(strtdatelabl).innerHTML="<s:text name="garuda.cbu.procedure.chckTermStartDate"/>";
		document.getElementById(enddatelabl).innerHTML="";
//		$j("#"+enddate).val("");
//		$j("#"+strtdate).val("");
	}
	else{
		document.getElementById(strtdatelabl).innerHTML="";
		document.getElementById(enddatelabl).innerHTML="";
	}
	}
	else{
		$j("#"+enddate).val("");
		document.getElementById(enddatelabl).innerHTML="<s:text name="garuda.cbu.procedure.startDate"/>";

	}
}

function getOtherBagTypeSelected(val){
	if(val == $j("#pkOtherBagTypeMod").val()){
		$j("#otherBagType1DivMod").show();
	}
	else{
		$j("#otherBagType1DivMod").hide();
		$j("#otherBagType1Mod").val("");
	}
	
}

function getOtherBagType2Selected(val){
	if(val == $j("#pkOtherBagTypeMod").val()){
		$j("#otherBagType2DivMod").show();
	}
	else{
		$j("#otherBagType2DivMod").hide();
		$j("#otherBagType2Mod").val("");
	}
	
}

function eitherOneEdit(id){
	 if($j("#"+id).val() != '') {
		 $j("#"+id+"per").attr('disabled','disabled');
	 }
}
function eitherOneEditOther(id){
	var id1 = id.substring(0, id.length - 3)
	 if($j("#"+id).val() != '') {
		 $j("#"+id1).attr('disabled','disabled');
	 }
}

</script>
<div id="cbbDiv" class='popupdialog tabledisplay '><s:form
	id="cbbProcedureFormUpdate">
	<s:hidden name="statusUpdate" value="True"></s:hidden>
	<s:hidden name="cbbProcedurePojo.pkProcId"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkProcInfoId"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.fkProcessingId"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkProcMethodId"
		id="processingMethod"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkIfAutomatedOther"
		id="IfAutomatedOther"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkProdModiOther"
		id="ProdModiOther"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkFreezManuOther"
		id="FreezManuOther"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkFrozenInOther"
		id="FrozenInOtherMod"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkFrozenInBag"
		id="pkFrozenInBagMod"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkOtherBagType"
		 id="pkOtherBagTypeMod"></s:hidden>
	<input type="hidden" name="fkProcId" id="loginIds" />
	
	<s:hidden name="cbbProcedureInfoPojo.pkBag1Type"
	 id="pkBag1Type"></s:hidden>
	 <s:hidden name="cbbProcedureInfoPojo.pkBag2Type"
	 id="pkBag2Type"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkNoOfBagsOthers"
	 id="pkNoOfBagsOthers"></s:hidden>
	 <s:hidden name="assignedToCordFlag" id="assignedToCordFlag"></s:hidden>
	
	<div id="update" style="display: none;"><jsp:include
		page="../cb_update.jsp">
		<jsp:param value="garuda.message.modal.cbbProcedureInfo"
			name="message" />
	</jsp:include></div>
	<table width="100%" cellspacing="0" cellpadding="0" border="0"
		id="status">

		<tr>
			<td colspan="2">
			<div class="portlet" id="cbbProcedureparent">
			<div id="cbbProcedure"
				class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-minusthick"></span><s:text
				name="garuda.cbbprocedures.label.processingprocedure"></s:text> <b><u><s:property
				value="cbbProcedurePojo.procName"></s:property></u></b></div>
			<div>
			<div id="<s:property value="updateProcNo" /><s:property value="cbbProcedurePojo.procName" />content" onclick="toggleDiv('<s:property value="updateProcNo" /><s:property value="cbbProcedurePojo.procName" />');"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cbbprocedures.label.cbbprocessingprocedures"></s:text>
			</div>
			<div id="<s:property value="updateProcNo" /><s:property value="cbbProcedurePojo.procName" />">

			<table>
				<tr>
					<!-- <td width="20%" align="left"><s:text
						name="garuda.cbbprocedures.label.procno"></s:text>:<span style="color: red;">*</span></td> -->
						
					<td width="20%" align="left" onmouseover="return overlib('<s:text
									name="garuda.cbbprocedures.label.procnametooltip"></s:text>');" 
							onmouseout="return nd();"><s:text
						name="garuda.cbbprocedures.label.procname"></s:text>:<span style="color: red;">*</span></td>
					<!-- <td width="30%" align="left"><s:textfield
						name="cbbProcedurePojo.procNo" id="updateProcNo" /> <br /> -->
					<td width="30%" align="left"><s:textfield
						name="cbbProcedurePojo.procName" id="updateProcNo" maxlength="50" readonly="true" cssStyle="background-color:#ECECEC;"  onkeydown="cancelBack();" /> <br />	
					<s:label id="ProcNolbl" style="color:#FF0000;"></s:label></td>
					
						<s:date name="cbbProcedures.procStartDate" id="datepicker9" format="dd-MMM-yyyy" />	
						<s:date  name="cbbProcedures.createdOn"  format="dd-MMM-yyyy" id="datepicker10"/>						
						<s:set name="procDate"  value="datepicker9"/>                                  
                         <%--   <s:property value="%{checkContantDate(#datepicker9)}"/> --%>
                        <s:if test="%{checkContantDate(#datepicker10)}">
       						 <td width="20%" align="left"><s:text name="garuda.cbuentry.label.productcode" /></td> 
       						 <td><s:textfield width="auto" name="cbbProcedures.productCode" id="productCode" disabled="true"></s:textfield> </td>        								 
        					       </s:if>
        							<s:else>
        								<!--   <s:text name="garuda.cbbprocedures.label.procversion"></s:text>:</td>
										<td width="30%" align="left">
										    <s:textfield width="auto" name="cbbProcedures.productCode" id="productCode" disabled="true"></s:textfield>
										    <s:textfield name="cbbProcedures.procVersion" disabled="true" />
										</td>	-->
        							</s:else>		
									
					<!-- <td width="20%" align="left"><s:text name="garuda.cbuentry.label.productcode" /><s:text
						name="garuda.cbbprocedures.label.procversion"></s:text>:</td>
					<td width="30%" align="left"><s:textfield name="cbbProcedurePojo.productCode" id="productCode" maxlength="50"></s:textfield><s:textfield
						name="cbbProcedurePojo.procVersion" readonly="true" onkeydown="cancelBack();" 
						disabled="disabled" value="PV2" /></td> -->
				</tr>
				<tr>
					<td width="20%" align="left"><s:text
						name="garuda.cbbprocedures.label.procstartdate"></s:text>:<span style="color: red;">*</span></td>
					<td width="30%" align="left"><s:date
						name="cbbProcedurePojo.procStartDate" id="datepicker1"
						format="MMM dd, yyyy"/> <s:textfield
						name="cbbProcedurePojo.procStartDateStr" class="datepic"
						id="datepicker1" value="%{datepicker1}"  cssClass="datePicWOMinDate" readonly="true" onkeydown="cancelBack();"/>
						<div id="validDateDiv4"><s:label id="invalidDate4"
						style="color:#FF0000;" /></div>
					</td>
					<td width="20%" align="left"><s:text
						name="garuda.cbbprocedures.label.proctermidate"></s:text>:</td>
					<td width="30%" align="left"><s:date
						name="cbbProcedurePojo.procTermiDate" id="datepicker6"
						format="MMM dd, yyyy" /> <s:textfield
						name="cbbProcedurePojo.procTermiDateStr" class="datepic"
						id="datepicker6" value="%{datepicker6}"
						cssClass="datePicWMinDate" readonly="true" onkeydown="cancelBack();"/>
						<div id="validDateDiv5"><s:label id="invalidDate5"
						style="color:#FF0000;" /></div>
					</td>
				</tr>
				
				 <tr>
					 <td width="20%" align="left">
					 	<s:text name="garuda.cbuentry.label.CBB" /><span class="error"><s:text name="garuda.label.dynamicform.astrik"></s:text> </span>
					 </td>
				     <td width="30%" align="left">
				       <s:select name="cbbProcedurePojo.fkSite" listKey="siteId" listValue="siteIdentifier" list="sites" headerKey="" headerValue="Select"></s:select>
				     </td>
				     <td width="20%" align="left">
				     </td>
				     <td width="30%" align="left">
				     </td>
				</tr> 
			</table>
			</div>
			</div>
			<div>
			<div id="<s:property value="updateProcNo" /><s:property value="cbbProcedureInfoPojo.fkProcMethId" />content" onclick="toggleDiv('<s:property value="updateProcNo" /><s:property value="cbbProcedureInfoPojo.fkProcMethId" />')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cbbprocedures.label.productmodification"></s:text></div> 
			<div id="<s:property value="updateProcNo" /><s:property value="cbbProcedureInfoPojo.fkProcMethId" />">
			<table>
				<tr>
					<td width="20%" align="left"><s:text
						name="garuda.cbbprocedures.label.processing"></s:text>:<span style="color: red;">*</span></td>
					<td width="30%" align="left"><s:select
						name="cbbProcedureInfoPojo.fkProcMethId" id="fkProcMethIdMod"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING]"
						listKey="pkCodeId" listValue="description" headerKey=""
						headerValue="Select" onchange="getIfAutomated();" /></td>
					<td width="20%" align="left"><s:text
						name="garuda.cbbprocedures.label.ifautomated"></s:text>:<span id="ifautomatedspan" style="color: red;">*</span></td>
					<td width="30%" align="left"><s:select
						name="cbbProcedureInfoPojo.fkIfAutomated" id="fkIfAutomatedMod"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IF_AUTOMATED]"
						listKey="pkCodeId" listValue="description" headerKey=""
						headerValue="Select" onchange="getOtherAutomated();" /></td>
				</tr>
				<tr id="otherAutomatedMod">
							<td width="20%" align="left"></td>
							<td width="30%" align="left"></td>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.specifyoth"></s:text>:<span id="ifautomatedotherspan" style="color: red;">*</span></td>
							<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.otherProcessing" id="otherProcessing" />
							</td>
						
				</tr>
				<tr>
					<td width="20%" align="left"><s:text
						name="garuda.cbbprocedures.label.productmodification"></s:text>:<span style="color: red;">*</span></td>
					<td width="30%" align="left"><s:select
						name="cbbProcedureInfoPojo.fkProductModification"
						id="fkProductModificationMod"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PRODUCT_MODI]"
						listKey="pkCodeId" listValue="description" headerKey=""
						headerValue="Select" onchange="getOtherProdModi();" /></td>
					<td width="20%" align="left"></td>
					<td width="30%" align="left"></td>
				</tr>
				<tr id="otherProdModiMod">
					<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyoth"></s:text>:
					<span id="prodmodiotherspan" style="color: red;">*</span></td>
					<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.otherProdModi" id="otherProdModi" />
					</td>
					<td width="20%" align="left"></td>
					<td width="30%" align="left"></td>
				</tr>
			</table>
			</div>
			</div>
			<div>
			<div id="<s:property value="cbbProcedureInfoPojo.fkFreezManufac" /><s:property value="cbbProcedureInfoPojo.fkProcMethId" />content" onclick="toggleDiv('<s:property value="cbbProcedureInfoPojo.fkFreezManufac" /><s:property value="cbbProcedureInfoPojo.fkProcMethId" />')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cbbprocedures.label.storageconditions"></s:text></div>
			<div id="<s:property value="cbbProcedureInfoPojo.fkFreezManufac" /><s:property value="cbbProcedureInfoPojo.fkProcMethId" />">
			<table>
				<tr>
					<td width="20%" align="left"><s:text
						name="garuda.cbbprocedures.label.stormethod"></s:text>:</td>
					<td width="30%" align="left"><s:select
						name="cbbProcedureInfoPojo.fkStorMethod"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@STORAGE_METHOD]"
						listKey="pkCodeId" listValue="description" headerKey=""
						headerValue="Select" /></td>
					<td width="20%" align="left"><s:text
						name="garuda.cbbprocedures.label.freezmanufac"></s:text>:</td>
					<td width="30%" align="left"><s:select
						name="cbbProcedureInfoPojo.fkFreezManufac" id="fkFreezManufacMod"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FREEZER_MANUFACTURER]"
						listKey="pkCodeId" listValue="description" headerKey=""
						headerValue="Select" onchange="getOtherFreezerManufac();" /></td>
				</tr>
				<tr id="otherFreezerManufacMod">
							<td width="20%" align="left"></td>
							<td width="30%" align="left"></td>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.freezmanufac"></s:text> - <s:text
								name="garuda.cbbprocedures.label.specifyoth"></s:text>:<span id="freezmanuotherspan" style="color: red;">*</span></td>
							<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.otherFreezManufac"
								id="otherFreezManufac" /></td>
				</tr>
				<tr>
					<td width="20%" align="left"><s:text
						name="garuda.cbbprocedures.label.frozenin"></s:text>:<span style="color: red;">*</span></td>
					<td width="30%" align="left"><s:select
						name="cbbProcedureInfoPojo.fkFrozenIn" id="fkFrozenInMod"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FROZEN_IN]"
						listKey="pkCodeId" listValue="description" headerKey=""
						headerValue="Select" onchange="cbuAutoDefer(this.value,this.id);getIfBagSelected();" /></td>
						<td width="20%" align="left"></td>
						<td width="30%" align="left"></td>
				</tr>
				<tr id="otherFrozenInDivMod">
					<td  width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother"></s:text>:<span id="frozeninotherspan" style="color: red;">*</span>
					</td>
					<td  width="30%" align="left"><s:textfield name="cbbProcedureInfoPojo.otherFrozenCont"
						id="otherFrozenContMod" maxlength="30"/></td>
					<td width="20%" align="left"></td>
				   <td width="30%" align="left"></td>
				</tr>
				<tr id="ifBagSelectedMod">
					<td width="20%" align="left"><s:text
						name="garuda.cbbprocedures.label.noofbags"></s:text>:<span id="bagtypespan" style="color: red;">*</span></td>
					<td width="30%" align="left"><s:select
						name="cbbProcedureInfoPojo.noOfBags" id="fkNoOfBagsMod"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NUM_OF_BAGS]"
						listKey="pkCodeId" listValue="description" headerKey=""
						headerValue="Select" onchange="getNoOfBagsSelected();"/></td>
					<td width="20%" align="left"></td>
				    <td width="30%" align="left"></td>
							
				</tr>
				<tr id="otherBagTypeModDiv">
			
					<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
					</s:text><span id="otherbagtypespan" style="color: red;">*</span>
					</td>
					<td width="30%" align="left"><s:textfield name="cbbProcedureInfoPojo.otherBagType" id="otherBagTypeMod" maxlength="30"></s:textfield>
					</td>
					<td width="20%" align="left"></td>
    				<td width="30%" align="left"></td>
				</tr>
				<tr id="bagTypeDivMod">
						<td align="left" colspan="4">
						<table>
						<tr>
						<td id ="bagTypeLabel" width="20%" align="left"><s:text
							name="garuda.cbbprocedures.label.bagtype"></s:text>:<span id="bag1typespan" style="color: red;">*</span></td>
						<td id ="bagTypeLabel1" width="20%" align="left"><s:text
							name="garuda.cbbprocedures.label.bag1type"></s:text>:<span id="bag1typespan" style="color: red;">*</span></td>
							
						<td width="30%" align="left"><s:select
							name="cbbProcedureInfoPojo.fkBag1Type" id="fkBag1TypeMod"
							list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BAG_TYPE]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select"  onchange="getOtherBagTypeSelected(this.value);"/></td>
						</tr>
						<tr>
							<td id="cryoLabel" width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.cryobagmanufac"></s:text>:<span id="cyrobagspan" style="color: red;">*</span></td>
							<td id="cryoLabel1" width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.cryobagmanufac.bagtype1"></s:text>:<span id="cyrobagspan" style="color: red;">*</span></td>
							<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.cryoBagManufac"
								id="cryoBagManufacMod" maxlength="50"/></td>
							<td id="maxVolLabel" width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.maxvalue"></s:text>(<s:text
								name="garuda.cbbprocedures.label.ml"></s:text>):<span id="maxvaluespan" style="color: red;">*</span></td>
							<td id="maxVolLabel1" width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.maxvalue.bagtype1"></s:text>(<s:text
								name="garuda.cbbprocedures.label.ml"></s:text>):<span id="maxvaluespan" style="color: red;">*</span></td>
							<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.maxValue" id="maxValueMod" maxlength="4"
								cssClass="positive-integer" /></td>
						</tr>
						<tr id="otherBagType1DivMod">
						<td width="50%" align="left" colspan="2">
								<table>
									<tr>
										<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
										</s:text><span id="otherbagtypespan" style="color: red;">*</span>
										</td>
										<td width="30%" align="left"><s:textfield name="cbbProcedureInfoPojo.otherBagType1" id="otherBagType1Mod" maxlength="30"></s:textfield>
										</td>
									</tr>
								</table>
						</td>
						</tr>
						</table>
						</td>
						</tr>
						<tr id="bag12TypeDivMod">
						<td align="left" colspan="4">
						<table>
						<tr>
						<td width="20%" align="left"><s:text
							name="garuda.cbbprocedures.label.bag2type"></s:text>:<span id="bag1typespan" style="color: red;">*</span></td>
						<td width="30%" align="left">
						<s:select
							name="cbbProcedureInfoPojo.fkBag2Type" id="fkBag2TypeMod"
							list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BAG_TYPE]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select"  onchange="getOtherBagType2Selected(this.value);"/></td>
						</tr>
						<tr>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.cryobagmanufac.bagtype2"></s:text>:<span id="cyrobag2span" style="color: red;">*</span></td>
							<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.cryoBagManufac2"
								id="cryoBag2ManufacMod" maxlength="50"/></td>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.maxvalue.bagtype2"></s:text>(<s:text
								name="garuda.cbbprocedures.label.ml"></s:text>):<span id="maxvaluespan" style="color: red;">*</span></td>
							<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.maxValue2" id="maxValue2Mod" maxlength="4"
								cssClass="positive-integer"/></td>
						</tr>
						<tr id="otherBagType2DivMod">
						<td width="50%" align="left" colspan="2">
								<table>
									<tr>
										<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
										</s:text><span id="otherbagtypespan" style="color: red;">*</span>
										</td>
										<td width="30%" align="left"><s:textfield name="cbbProcedureInfoPojo.otherBagType2" id="otherBagType2Mod" maxlength="30"></s:textfield>
										</td>
									</tr>
								</table>
						</td>
						</tr>
						</table>
						</td>
						</tr>
						<tr>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.stortemp"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%" align="left">
								<select name="cbbProcedureInfoPojo.fkStorTemp" id="fkStorTempAdd" onchange="tempAutoDefer(this.value,this.id);">
								<option value="">Select</option>
								<s:iterator value = "#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@STORAGE_TEMPERATURE]">
									<s:if test="%{pkCodeId==cbbProcedureInfoPojo.fkStorTemp}">
										<option value ='<s:property value='%{pkCodeId}'/>' selected="selected"><s:property value="%{description}" escapeHtml="false"/></option>
									</s:if><s:else>
										<option value ='<s:property value='%{pkCodeId}'/>'> <s:property value="%{description}" escapeHtml="false"/></option>
									</s:else>
								</s:iterator>
								</select> </td>
							<td width="20%" align="left"></td>
							<td width="30%" align="left"></td>
						</tr>
						<tr>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.controlledratefreezing"></s:text>:
							</td>
							<td width="30%" align="left"><s:select
								name="cbbProcedureInfoPojo.fkContrlRateFreezing"
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CONTR_RATE_FREEZ]"
								listKey="pkCodeId" listValue="description" headerKey=""
								headerValue="Select" /></td>
							<td width="20%" align="left" onmouseover="return overlib('<s:text name='garuda.cbbprocedure.label.numberThawedtooltip'/>')" onmouseout="return nd();">
								<s:text name="garuda.cbbprocedures.label.noofindifrac"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.noOfIndiFrac" maxlength="2"
								cssClass="positive-integer" /></td>
						</tr>
			</table>
			</div>
			</div>

			<div>
			<div id="<s:property value="cbbProcedureInfoPojo.fkFreezManufac" /><s:property value="cbbProcedureInfoPojo.thouUnitPerMlHeparin" />content" onclick="toggleDiv('<s:property value="cbbProcedureInfoPojo.fkFreezManufac" /><s:property value="cbbProcedureInfoPojo.thouUnitPerMlHeparin" />')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cbbprocedures.label.frozenproductcomposition"></s:text>
			</div>
			<div id="<s:property value="cbbProcedureInfoPojo.fkFreezManufac" /><s:property value="cbbProcedureInfoPojo.thouUnitPerMlHeparin" />">
			<fieldset><legend> <s:text
				name="garuda.cbbprocedures.label.additivesbeforevolumereductions"></s:text>
			</legend>
			<table>
						<tr valign="top">
							
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
									
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.thouunitpermlheparin"></s:text>:
							</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.thouUnitPerMlHeparin" id="thouUnitPerMlHeparinMod" maxlength="4" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);">
								</s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.thouUnitPerMlHeparinper" id="thouUnitPerMlHeparinModper" maxlength="4" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);">
								</s:textfield></td>
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.cpda"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.cpda" id="cpda" maxlength="4" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.cpdaper" id="cpdaper" maxlength="4" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
						
						</tr>
						<tr valign="top">
							
								
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.fiveunitpermlheparin"></s:text>:
							</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.fiveUnitPerMlHeparin" id="fiveUnitPerMlHeparinMod" maxlength="3" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);">
								</s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.fiveUnitPerMlHeparinper" id="fiveUnitPerMlHeparinModper" maxlength="3" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);">
								</s:textfield></td>
							
								<td width="20%"><s:text
								name="garuda.cbbprocedures.label.cpd"></s:text>:</td>
							<td width="15%"><s:textfield name="cbbProcedureInfoPojo.cpd" maxlength="4" id="cpdMod" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield name="cbbProcedureInfoPojo.cpdper" maxlength="4" id="cpdModper" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
						
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.tenunitpermlheparin"></s:text>:
							</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.tenUnitPerMlHeparin" maxlength="3" id="tenUnitPerMlHeparinMod" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.tenUnitPerMlHeparinper" maxlength="3" id="tenUnitPerMlHeparinModper" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.acd"></s:text>:</td>
							<td width="15%"><s:textfield name="cbbProcedureInfoPojo.acd" maxlength="4" id="acdMod" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield name="cbbProcedureInfoPojo.acdper" maxlength="4" id="acdModper" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
						</tr>
						<tr valign="top">
								
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.sixperhydroxyethylstarch"></s:text>:
							</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.sixPerHydroxyethylStarch" maxlength="4" id="sixPerHydroxyethylStarchMod" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.sixPerHydroxyethylStarchper" maxlength="4" id="sixPerHydroxyethylStarchModper" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.otheranticoagulant"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.otherAnticoagulant"
								id="otherAnticoagulantMod" maxlength="4" onChange="getOtherAnticoagulantMod();" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.otherAnticoagulantper"
								id="otherAnticoagulantModper" maxlength="4" onChange="getOtherAnticoagulantMod();"   cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td> 
						</tr>

						<tr id="otherAnticoagulantDivMod" valign="top">
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.specifyothanti"></s:text>:<span style="color: red;">*</span></td>
									<td width="30%" align="left" colspan="2"><s:textfield
										name="cbbProcedureInfoPojo.specifyOthAnti"
										id="specifyOthAntiMod" maxlength="30"/></td>
									<td width="20%" align="left"></td>
									<td width="30%" align="left" colspan="2"></td>
						</tr>
					</table>
			</fieldset>
			<fieldset><legend> <s:text
				name="garuda.cbbprocedures.label.cryoprotectantsandotheradditives"></s:text>
			</legend>
			<table>
						<tr valign="top">
							
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
									
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.hunperdmso"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.hunPerDmso" maxlength="4" id="hunPerDmsoMod" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.hunPerDmsoper" id="hunPerDmsoModper" maxlength="4" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.twenfivehumalbu"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.twenFiveHumAlbu" maxlength="4" id="twenFiveHumAlbuMod" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.twenFiveHumAlbuper" maxlength="4" id="twenFiveHumAlbuModper" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
						
						</tr>
						<tr valign="top">
									
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.hunperglycerol"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.hunPerGlycerol" maxlength="4" id="hunPerGlycerolMod" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.hunPerGlycerolper" maxlength="4" id="hunPerGlycerolModper" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.plasmalyte"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.plasmalyte" maxlength="4" id="plasmalyteMod" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>	
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.plasmalyteper" maxlength="4" id="plasmalyteModper" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
				
						</tr>
						<tr valign="top">
								<td width="20%"><s:text
								name="garuda.cbbprocedures.label.tenperdextran40"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.tenPerDextran_40" maxlength="4" id="tenPerDextran_40Mod" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.tenPerDextran_40per" maxlength="4" id="tenPerDextran_40Modper" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.othcryoprotectant"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.othCryoprotectant"
								id="othCryoprotectantMod" maxlength="4" onchange="getOtherCyroprotectantMod();" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>	
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.othCryoprotectantper"
								id="othCryoprotectantModper" maxlength="4" onchange="getOtherCyroprotectantMod();" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
						</tr>
						<tr valign="top">
										
						
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.fiveperhumanalbu"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.fivePerHumanAlbu" maxlength="4" id="fivePerHumanAlbuMod" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.fivePerHumanAlbuper" maxlength="4" id="fivePerHumanAlbuModper"  cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%" colspan="2"></td>
							
						</tr>
						<tr id="otherCryoprotectantDivMod" valign="top">
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.specothcryopro"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%" align="left" colspan="2"><s:textfield
								name="cbbProcedureInfoPojo.specOthCryopro"
								id="specOthCryoproMod" maxlength="30"/></td>
							
							<td width="20%" align="left"></td>
							<td width="30%" align="left" colspan="2"></td>						
						</tr>
					</table>
			</fieldset>
			<fieldset><legend> <s:text
				name="garuda.cbbprocedures.label.diluents"></s:text> </legend>
			<table width="100%">
						<tr valign="top">
							
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
							<td width="20%">
							</td>
							<td width="15" align="center">
							</td>
							<td width="15" align="center">
							</td>
									
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.fiveperdextrose"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.fivePerDextrose" maxlength="5" id="fivePerDextroseMod" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.fivePerDextroseper" maxlength="4" id="fivePerDextroseModper" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%"colspan="2"></td>
						</tr>	
						<tr valign="top">	
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.pointninepernacl"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.pointNinePerNacl" maxlength="5" id="pointNinePerNaclMod" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.pointNinePerNaclper" maxlength="4" id="pointNinePerNaclModper" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%"colspan="2"></td>	
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.otherdiluents"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.othDiluents" id="othDiluentsMod" maxlength="4"
								onchange="getothDiluents();" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.othDiluentsper" id="othDiluentsModper" maxlength="4"
								onchange="getothDiluents();" cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%"colspan="2"></td>
						</tr>
						<tr id="specOthDiluentsDiv" valign="top">
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.specothdiluents"></s:text>:<span style="color: red;">*</span></td>
									<td width="30%" align="left" colspan="2"><s:textfield
										name="cbbProcedureInfoPojo.specOthDiluents"
										id="specOthDiluentsMod" maxlength="30"/></td>
									<td width="20%" align="left"></td>
									<td width="30%" align="left" colspan="2"></td>
									
									
						</tr>
					</table>
			</fieldset>
			</div>
			</div>

			<div>
			<div id="<s:property value="cbbProcedureInfoPojo.noOfSerumAliquots" /><s:property value="cbbProcedureInfoPojo.noOfSegments" />_cbusmplcontent" onclick="toggleDiv('<s:property value="cbbProcedureInfoPojo.noOfSerumAliquots" /><s:property value="cbbProcedureInfoPojo.noOfSegments" />_cbusmpl')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cbbprocedures.label.cbusamples"></s:text></div>
			<div id="<s:property value="cbbProcedureInfoPojo.noOfSerumAliquots" /><s:property value="cbbProcedureInfoPojo.noOfSegments"/>_cbusmpl">
			<table>
				<tr valign="top">
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.filterpaper"></s:text>:<span style="color: red;">*</span></td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.filterPaper"
						cssClass="positive-integer" maxlength="2"></s:textfield></td>
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.rbcpellets"></s:text>:<span style="color: red;">*</span></td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.rbcPallets" cssClass="positive-integer"
						maxlength="2"></s:textfield></td>
				
				</tr>
				<tr valign="top">
					
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.noofextrdnaaliquots"></s:text>:<span style="color: red;">*</span></td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.noOfExtrDnaAliquots"
						id="noOfExtrDnaAliquotsMod" onchange="getTotaliquots();"
						cssClass="positive-integer" maxlength="2"></s:textfield></td>
						
					<td width="20%" onmouseover="return overlib('<s:text name='garuda.cbbprocedure.label.numberSerumToolTip'/>');" 
							onmouseout="return nd();"><s:text
						name="garuda.cbbprocedures.label.noofserumaliquots"></s:text>:<span style="color: red;">*</span></td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.noOfSerumAliquots"
						id="noOfSerumAliquotsMod" onfocus="getTotaliquots();"
						cssClass="positive-integer" maxlength="2"></s:textfield></td>	
				</tr>
				<tr valign="top">
					
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.noofplasmaaliquots"></s:text>:<span style="color: red;">*</span></td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.noOfPlasmaAliquots"
						id="noOfPlasmaAliquotsMod" onfocus="getTotaliquots();"
						cssClass="positive-integer" maxlength="2"></s:textfield></td>
					
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.noofnonviablealiquots"></s:text>:<span style="color: red;">*</span>
					</td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.noOfNonviableAliquots"
						id="noOfNonviableAliquotsMod" onfocus="getTotaliquots();"
						cssClass="positive-integer" maxlength="2"></s:textfield></td>
					
				</tr>
				<tr>
					
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.noofviablesampnotrep"></s:text>:<span style="color: red;">*</span>
					</td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.noOfViableSampleNotRep"
						id="noOfViableCellAliquotsMod" onfocus="getTotaliquots();"
						cssClass="positive-integer" maxlength="2"></s:textfield></td>
						
					<td width="20%"></td>
					<td width="30%"></td>
				</tr>	
				</table>
				
				
				<fieldset>
				<legend>
					<s:text name="garuda.cbbprocedures.label.aliqrepoffinalprod"></s:text>
				</legend>
				<table>
				<tr valign="top">
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.noofsegments"></s:text>:<span style="color: red;">*</span></td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.noOfSegments"
						cssClass="positive-integer" maxlength="2" onblur = "segAutoDefer(this.value,this.id);"></s:textfield></td>
					<td width="20%"></td>
					<td width="30%"></td>		
				</tr>
				
				<tr>
						<td width="20%"><s:text
								name="garuda.cbbprocedures.label.noofothrepaliqprod"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.noOfOthRepAliqProd" cssClass="positive-integer" maxlength="2"></s:textfield>
							</td>
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.noofothrepaliqaltcond"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.noOfOthRepAliqAltCond" cssClass="positive-integer" maxlength="2"></s:textfield>
							</td>	
				</tr>
				</table>
				</fieldset>
				</div>
				</div>
				
				
				
				<!-- tr>
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.totcbualiquots"></s:text>:</td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.totCbuAliquots" id="totCbuAliquotsMod"
						readonly="true" onkeydown="cancelBack();"  onfocus="getTotaliquots();"></s:textfield></td>
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.noofextrdnamateraliquots"></s:text>:<span style="color: red;">*</span>
					</td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.noOfExtrDnaMaterAliquots"
						id="noOfExtrDnaMaterAliquotsMod" onchange="getTotaliquots1();"
						cssClass="positive-integer" maxlength="2"></s:textfield></td>
				</tr -->
				
				<div>
				<div id="<s:property value="cbbProcedureInfoPojo.noOfSerumMaterAliquots" /><s:property value="cbbProcedureInfoPojo.noOfPlasmaMaterAliquots" />_matsmplcontent" onclick="toggleDiv('<s:property value="cbbProcedureInfoPojo.noOfSerumMaterAliquots" /><s:property value="cbbProcedureInfoPojo.noOfPlasmaMaterAliquots" />_matsmpl')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cbbprocedures.label.maternalsamples"></s:text></div>
				<div id="<s:property value="cbbProcedureInfoPojo.noOfSerumMaterAliquots" /><s:property value="cbbProcedureInfoPojo.noOfPlasmaMaterAliquots"/>_matsmpl">
				<table>
				
				<tr valign="top">
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.noofserummateraliquots"></s:text>:<span style="color: red;">*</span>
					</td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.noOfSerumMaterAliquots"
						id="noOfSerumMaterAliquotsMod"
						cssClass="positive-integer" maxlength="2"></s:textfield></td>
					
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.noofplasmamateraliquots"></s:text>:<span style="color: red;">*</span>
					</td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.noOfPlasmaMaterAliquots"
						id="noOfPlasmaMaterAliquotsMod" 
						cssClass="positive-integer" maxlength="2"></s:textfield></td>	
					
				</tr>
				
				<tr valign="top">
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.noofextrdnamateraliquots"></s:text>:<span style="color: red;">*</span>
					</td>	
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.noOfExtrDnaMaterAliquots"
						id="totMaterAliquotsMod" cssClass="positive-integer"
								maxlength="2"></s:textfield>
					</td>
					
					<td width="20%" onmouseover="return overlib('<s:text name='garuda.cbbprocedure.label.numberMisMatToolTip'/>');" 
							onmouseout="return nd();"><s:text
						name="garuda.cbbprocedures.label.noofcellmateraliquots"></s:text>:<span style="color: red;">*</span>
					</td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.noOfCellMaterAliquots"
						id="noOfCellMaterAliquotsMod"
						cssClass="positive-integer" maxlength="2"></s:textfield></td>
						
				</tr>
			</table>
			</div>
			</div>

			<!-- div>
			<div id="<s:property value="cbbProcedureInfoPojo.sopRefNo" /><s:property value="cbbProcedureInfoPojo.sopStrtDateStr" />content" onclick="toggleDiv('<s:property value="cbbProcedureInfoPojo.sopRefNo" /><s:property value="cbbProcedureInfoPojo.sopStrtDateStr" />')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cbbprocedures.label.sop"></s:text></div>
			<div id="<s:property value="cbbProcedureInfoPojo.sopRefNo" /><s:property value="cbbProcedureInfoPojo.sopStrtDateStr" />">
			<table>
				<tr>
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.soprefno"></s:text>:</td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.sopRefNo"></s:textfield></td>
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.soptitle"></s:text>:</td>
					<td width="30%"><s:textfield
						name="cbbProcedureInfoPojo.sopTitle"></s:textfield></td>
				</tr>
				<tr>
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.sopstrtdate"></s:text>:</td>
					<td width="30%"><s:date
						name="cbbProcedureInfoPojo.sopStrtDate" id="datepicker7"
						format="MMM dd, yyyy" /> <s:textfield
						name="cbbProcedureInfoPojo.sopStrtDateStr" class="datepic"
						id="datepicker7" value="%{datepicker7}" />
					<div id="validDateDiv6"><s:label id="invalidDate6"
						style="color:#FF0000;" /></div>
					</td>
					<td width="20%"><s:text
						name="garuda.cbbprocedures.label.soptermidate"></s:text>:</td>
					<td width="30%"><s:date
						name="cbbProcedureInfoPojo.sopTermiDate" id="datepicker8"
						format="MMM dd, yyyy" /> <s:textfield
						name="cbbProcedureInfoPojo.sopTermiDateStr" class="datepic"
						id="datepicker8" value="%{datepicker8}"
						onchange="validateDate('datepicker8','datepicker7','invalidDate7','invalidDate6');" />
					<div id="validDateDiv7"><s:label id="invalidDate7"
						style="color:#FF0000;" /></div>
					</td>
				</tr>

			</table>
			</div>
			</div-->

			</div>
			</div>
			</td>
		</tr>
		<tr>
		<td>
		<table bgcolor="#cccccc">
		<tr>
			<td width="70%"><jsp:include page="../cb_esignature.jsp" flush="true"></jsp:include></td>
			<td><input type="button"
				id="submit" disabled="disabled"
				onclick="submitCBBProcedureInfo('cbbProcedureFormUpdate','<s:property value="cbbProcedurePojo.pkProcId" />','<s:property value="cbbProcedureInfoPojo.pkProcInfoId" />');"
				value="<s:text name="garuda.unitreport.label.button.submit"/>" /></td>
			<td><input type="button" onclick="closeModal();"
				value="<s:text name="garuda.unitreport.label.button.cancel"/>" /></td>
		</tr>
		</table>
		</td>
		</tr>
	</table>
	
</s:form></div>