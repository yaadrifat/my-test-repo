<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<% String contextpath=request.getContextPath();%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<script type="text/javascript">

$j(function() {
	getHiddedValues();
	getDatePic();
	validateTime();
	var shipmentdate=$j('#ModalcbuSchShipmentDate').val();
	if(shipmentdate!=null && shipmentdate!='' && shipmentdate!='undefined'){
		getDayfromshipdate(shipmentdate,'cbushimentdayModalDiv');
	}
	var validator = $j("#cbushipmentmodalform").validate({
		invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    }
	});
	
});

function limitText(txtField, maxLength) {
	if (txtField.value.length > maxLength) {
		txtField.value = txtField.value.substring(0, maxLength)
	} 
}

function validateTime(){
	$j(".timeValid").keyup(function(e) {
		  var val = $j(this).val();
		  if(e.keyCode!=8 && e.keyCode!=46 )
		  {
			  	 if(val.length==1){
			  		 if( ! IsNumeric(val)){
			  			alert("<s:text name="garuda.common.timeFormat.validHour"/>");
						$j(this).val('');
			  			 
			  		 }
			  	  }
				  if(val.length==2){

						  if( IsNumeric(val) && val<24){
							  if(e.which!=58){
								   $j('#cbuShedShipmentTime').val(val+":");
						  		}
						  }else{
							  alert("<s:text name="garuda.common.timeFormat.validHour"/>");
							  $j(this).val('');
						  }

			  	  }
				  
				  if(val.length==4 || val.length==5){
					  //alert("val lenght 4 && 3:::"+val.length+":::val:::"+val)
					  
					  if(val.length==4){
						  temp=val.substring(3);
					  }
					  if(val.length==5){
						  temp=val.substring(3,5);
					  }
					  
					  
					  //alert("temp leng 4 3::"+temp+"result of isnumberic"+IsNumeric(temp))
					  if( IsNumeric(temp)==false || (Number(temp)>59)){
						  alert("<s:text name="garuda.common.timeFormat.validMin"/>");
						  var str=$j(this).val();
						  var index=str.indexOf(":");
						  var modistr=str.substring(0,index+1);
						  $j('#cbuShedShipmentTime').val(modistr);
					  }
					  
				  }
		
		  }
	});	
}

function updateCbuShipment(){
	if(validateCBUShipmentDet() && $j("#cbushipmentmodalform").valid()){

		var historyArr=new Array();
		var i=0; 
 		$j('.detailhstry').each(function(){
			if($j(this).is(':visible')){
				var idval=$j(this).attr("id");
				historyArr[i]=idval;
				i++;
			}
		}); 
 		setTimeout(function(){
		loadMoredivs('updateCtshipment?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&shipcompany='+$j("#cbuFkshipingCompanyId").val()+'&trackNo='+encodeURIComponent($j("#cbuShipmentTrackingNo").val())+'&shipCont='+$j("#cbuFkShippingContainer").val()+'&tempmoniter='+$j("#cbuFkTempMoniter").val()+'&schshiptime='+$j("#cbuShedShipmentTime").val()+'&padlockCombi='+$j("#cbuPadlockCombination").val()+'&paperworkLoc='+$j("#cbuPaperworkLoc").val()+'&additiShiiperDet='+$j("#cbuAdditiShiiperDet").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&schshipdate='+$j("#ModalcbuSchShipmentDate").val(),'currentRequestProgress,taskCompleteFlagsDiv,chistory,cReqhistory,cbuShipmentInfoContentDiv','cbushipmentmodalform','updatecbushipmentmodal','statuscbushipmentmodal');
		setpfprogressbar();
		getprogressbarcolor();
		historyTblOnLoad();
		validateTime();		
		if($j("#datepicker15").val()!=null && $j("#datepicker15").val()!='' && $j("#datepicker15").val()!='undefined'){
			getDayfromshipdate($j("#datepicker15").val(),'cbushimentdayDiv');
		}
 		},0);
		setTimeout(function(){
			//alert("historyArr::length"+historyArr.length)
			$j.each(historyArr,function(i,val){
				var Index=val.charAt(val.length-1);
				$j("#icondiv"+Index).triggerHandler('click');
			});
		},0);
	}
}

function updateCbuShipmentEdit(){
	var historyArr=new Array();
	var i=0;
	if($j("#cbushipmentmodalform").valid()) {
		$j('.detailhstry').each(function(){
		if($j(this).is(':visible')){
			var idval=$j(this).attr("id");
			historyArr[i]=idval;
			i++;
		}
	}); 
	setTimeout(function(){
	loadMoredivs('updateCtshipment?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&shipcompany='+$j("#cbuFkshipingCompanyId").val()+'&trackNo='+encodeURIComponent($j("#cbuShipmentTrackingNo").val())+'&shipCont='+$j("#cbuFkShippingContainer").val()+'&tempmoniter='+$j("#cbuFkTempMoniter").val()+'&schshiptime='+$j("#cbuShedShipmentTime").val()+'&padlockCombi='+$j("#cbuPadlockCombination").val()+'&paperworkLoc='+$j("#cbuPaperworkLoc").val()+'&additiShiiperDet='+$j("#cbuAdditiShiiperDet").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&schshipdate='+$j("#ModalcbuSchShipmentDate").val()+'&novalidate=true','currentRequestProgress,taskCompleteFlagsDiv,chistory,cReqhistory,cbuShipmentInfoContentDiv','cbushipmentmodalform','updatecbushipmentmodal','statuscbushipmentmodal');
	setpfprogressbar();
	getprogressbarcolor();
	historyTblOnLoad();
	validateTime();
	if($j("#datepicker15").val()!=null && $j("#datepicker15").val()!='' && $j("#datepicker15").val()!='undefined'){
		getDayfromshipdate($j("#datepicker15").val(),'cbushimentdayDiv');
	}
	},0);
	setTimeout(function(){
		//alert("historyArr::length"+historyArr.length)
		$j.each(historyArr,function(i,val){
			var Index=val.charAt(val.length-1);
			$j("#icondiv"+Index).triggerHandler('click');
		});
	},0);
	}
}

function validateCBUShipmentDet(){
	if($j("#ModalcbuSchShipmentDate").val()==null || $j("#ModalcbuSchShipmentDate").val()==''){
		$j("#errorcbuSchShipmentDateDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.cbu.order.selectShipmentDate"/>");
		 });
		$j("#errorcbuSchShipmentDateDiv").show();
		return false;
		
	}
	else{
		$j("#errorcbuSchShipmentDateDiv").find("span").each(function(){
      		$j(this).text("");
	 });
		$j("#errorcbuSchShipmentDateDiv").hide();
	}
	/*if($j("#cbuShedShipmentTime").val()==null || $j("#cbuShedShipmentTime").val()==''){
		$j("#errorcbuSchShipmenttimeDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.cbu.order.selectShipmentTime"/>");
		 });
		$j("#errorcbuSchShipmenttimeDiv").show();
		return false;
		
	}
	else{
		$j("#errorcbuSchShipmenttimeDiv").find("span").each(function(){
			
      		$j(this).text("");
	 });
		$j("#errorcbuSchShipmenttimeDiv").hide();
	}
	if($j("#cbuFkshipingCompanyId").val()==null || $j("#cbuFkshipingCompanyId").val()==''){
		$j("#errorcbuShippingCompanyDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.cbu.order.selectShippingCompany"/>");
		 });
		$j("#errorcbuShippingCompanyDiv").show();
		return false;
		
	}
	else{
		$j("#errorcbuShippingCompanyDiv").find("span").each(function(){
			
      		$j(this).text("");
	 });
		$j("#errorcbuShippingCompanyDiv").hide();
	}
	if($j("#cbuFkShippingContainer").val()==null || $j("#cbuFkShippingContainer").val()==''){
		$j("#errorcbuShippingContainerDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.cbu.order.selectShippingContainer"/>");
		 });
		$j("#errorcbuShippingContainerDiv").show();
		return false;
		
	}
	else{
		$j("#errorcbuShippingContainerDiv").find("span").each(function(){
			
      		$j(this).text("");
	 });
		$j("#errorcbuShippingContainerDiv").hide();
	}
	if($j("#cbuFkTempMoniter").val()==null || $j("#cbuFkTempMoniter").val()==''){
		$j("#errorcbuTempMonitorDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.cbu.order.selectTempmonitor"/>");
		 });
		$j("#errorcbuTempMonitorDiv").show();
		return false;
		
	}
	else{
		$j("#errorcbuTempMonitorDiv").find("span").each(function(){
			
      		$j(this).text("");
	 });
		$j("#errorcbuTempMonitorDiv").hide();
	}*/
	if(($j("#ModalcbuSchShipmentDate").val()!=null || $j("#ModalcbuSchShipmentDate").val()!='') 
			//&& ($j("#cbuShedShipmentTime").val()!=null || $j("#cbuShedShipmentTime").val()!='')
			//&& ($j("#cbuFkshipingCompanyId").val()!=null || $j("#cbuFkshipingCompanyId").val()!='')
			//&& ($j("#cbuFkShippingContainer").val()!=null || $j("#cbuFkShippingContainer").val()!='') 
			//&& ($j("#cbuFkTempMoniter").val()!=null || $j("#cbuFkTempMoniter").val()!='')
			){
				$j("#errorcbuSchShipmentDateDiv").hide();
				//$j("#errorcbuSchShipmenttimeDiv").hide();
				//$j("#errorcbuShippingCompanyDiv").hide();
				//$j("#errorcbuShippingContainerDiv").hide();
				//$j("#errorcbuTempMonitorDiv").hide();
				return true;

		}
}


</script>
<s:form name="cbushipmentmodalform" id="cbushipmentmodalform">
	<div id="updatecbushipmentmodal" style="display: none;">
	<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.message.modal.cbushipment" /></strong></p>
			</div>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td>
			<input type="button" onclick="closeModals('cbushipmodaldiv')" value="<s:text name="garuda.common.close"/>" />
		</td>
	</tr>
</table>
	
	</div>
	<table width="100%" cellspacing="0" cellpadding="0" border="0"
		id="statuscbushipmentmodal">
		<tr>
			<td>
			<div class="portlet" id="ctshipmentbarDivparent">
			<div id="ctshipmentbarDiv"
				class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-print"
				onclick="clickheretoprint('ctshipmentbarDiv','<s:property value="cdrCbuPojo.registryId" />')"></span><span
				class="ui-icon ui-icon-newwin"></span> <!--<span class="ui-icon ui-icon-plusthick"></span>-->
			<!--<span class="ui-icon ui-icon-close"></span>--> <span
				class="ui-icon ui-icon-minusthick"></span> <s:text
				name="garuda.ctOrderDetail.portletname.cbuShipment" /></div>
			<div class="portlet-content" id="cbuShipmentInfoContentDiv"><s:hidden
				name="pkCbuShipment" id="pkCbuShipment"></s:hidden>
			<table width="100%">
				<s:if test="shipmentInfoLst!=null && shipmentInfoLst.size()>0">
					<s:iterator value="shipmentInfoLst" var="rowVal">
						<tr>
							<td width="25%"><strong><s:text
								name="garuda.cbuShipment.label.cbusheduledshipdate" /></strong>:<span
								style="color: red;">*</span></td>
							<td width="25%"><s:date name="schShipmentDate"
								id="ModalcbuSchShipmentDate" format="MMM dd, yyyy"></s:date> <s:textfield
								readonly="true" onkeydown="cancelBack();"  name="shipmentPojo.schShipmentDate"
								class="datepic" cssClass="datePicWMinDate" onfocus="onFocusCall(this);"
								id="ModalcbuSchShipmentDate" value="%{#rowVal[0]}"
								onchange="getDayfromshipdate(this.value,'cbushimentdayModalDiv');validateCBUShipmentDet()" />
							<div id="errorcbuSchShipmentDateDiv"><span
								id="cbushipmentDatelbl" class="error"></span></div>
							</td>
							<td width="25%"><strong><s:text
								name="garuda.cbushipmetInfo.label.day"></s:text></strong>:</td>
							<td width="25%" id="cbushimentdayModalDiv"><span
								style="font-style: italic; color: blue"></span></td>
						</tr>
						<tr>
							<td width="25%"><strong><s:text
								name="garuda.cbuShipment.label.cbusheduledshiptime" /></strong>:</td>
							<td width="25%"><s:textfield
								name="shipmentPojo.shipmentTime" value="%{#rowVal[16]}"
								id="cbuShedShipmentTime" cssClass="timeValid" maxlength="5"
								onchange="validateCBUShipmentDet();" />
							<div id="errorcbuSchShipmenttimeDiv"><span
								id="cbushipmentTimelbl" class="error"></span></div>
							</td>
							<s:if test="#rowVal[17]!=null">
								<td width="25%"><strong><s:text
									name="garuda.cbuShipment.label.noofdaystoshipcbu" /></strong>:<span
									style="color: red;">*</span></td>
								<td width="25%"><s:textfield name="noofdaystoshipcbu"
									id="noofdaystoshipcbu" value="%{#rowVal[17]}" disabled="true"></s:textfield>
								</td>
							</s:if>
							<s:else>
								<td width="25%"></td>
								<td width="25%"></td>
							</s:else>
						</tr>
						<tr>
							<td width="25%"><strong><s:text
								name="garuda.cbuShipment.label.shipcompany" /></strong>:</td>
							<td width="25%"><s:select
								name="shipmentPojo.fkshipingCompanyId"
								id="cbuFkshipingCompanyId"
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPER]"
								listKey="pkCodeId" listValue="description" headerKey=""
								headerValue="Select" value="%{#rowVal[2]}"
								onchange="validateCBUShipmentDet();"></s:select>
							<div id="errorcbuShippingCompanyDiv"><span
								id="cbuShippingCompanylbl" class="error"></span></div>
							</td>
							<td width="25%"><strong><s:text
								name="garuda.ctShipmentInfo.label.trackingno" /></strong>:</td>
							<td width="25%"><s:textfield
								name="shipmentPojo.shipmentTrackingNo"
								id="cbuShipmentTrackingNo" value="%{#rowVal[3]}"  maxlength="100" /></td>
						</tr>
						<tr>
							<td width="25%"><strong><s:text
								name="garuda.cbuShipment.label.shippingcontainer" /></strong>:</td>
							<td width="25%"><s:select
								name="shipmentPojo.fkShippingContainer"
								id="cbuFkShippingContainer"
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPING_CONTAINER]"
								listKey="pkCodeId" listValue="description" headerKey=""
								headerValue="Select" value="%{#rowVal[7]}"
								onchange="validateCBUShipmentDet();" />
							<div id="errorcbuShippingContainerDiv"><span
								id="cbuShippingContainerlbl" class="error"></span></div>
							</td>
							<td width="25%"><strong><s:text
								name="garuda.cbuShipment.label.tempmonitor" /></strong>:</td>
							<td width="25%"><s:select name="shipmentPojo.fkTempMoniter"
								id="cbuFkTempMoniter"
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEMP_MONITOR]"
								listKey="pkCodeId" listValue="description" headerKey=""
								headerValue="Select" value="%{#rowVal[8]}"
								onchange="validateCBUShipmentDet();" />
							<div id="errorcbuTempMonitorDiv"><span
								id="cbuTempMonitorlbl" class="error"></span></div>
							</td>
						</tr>
						<tr>
							<td width="25%"><strong><s:text
								name="garuda.cbuShipment.label.actualshipdate" /></strong>:</td>
							<td width="25%">
							<s:if test='%{#rowVal[29]!=null && #rowVal[29]!=""}'>
							<s:textfield
								name="shipmentPojo.shipmentDate" id="actshipmentDate"
								disabled="true" value="%{#rowVal[9]}" />
							</s:if>
							<s:else>
								<input type="text" name="shipmentPojo.shipmentDate" 
								id="actshipmentDate" disabled="disabled" 
								value="<s:text name='garuda.currentReqProgress.label.notprovided'/>"/>
							</s:else>	
							</td>
							<td width="25%"><strong><s:text
								name="garuda.cbuShipment.label.anticipatedarrivaldate" /></strong>:</td>
							<td width="25%"><s:textfield
								name="shipmentPojo.expectingArriveDate" disabled="true"
								value="%{#rowVal[10]}" /></td>
						</tr>
						<tr>
							<td width="25%"><strong><s:text
								name="garuda.cbuShipment.label.shipperpadlockcombination" /></strong>:</td>
							<td width="25%"><s:textarea
								name="shipmentPojo.padlockCombination"
								id="cbuPadlockCombination" value="%{#rowVal[20]}" 
								style="width: 100px; height: 70px;" onkeyup="limitText(this,200);"></s:textarea></td>
							<td width="25%"><strong><s:text
								name="garuda.cbuShipment.label.additishipperdet" /></strong>:</td>
							<td width="25%"><s:textarea
								name="shipmentPojo.additiShiiperDet" id="cbuAdditiShiiperDet"
								value="%{#rowVal[19]}" 
								style="width: 100px; height: 70px;" onkeyup="limitText(this,200);"></s:textarea></td>
						</tr>
						<tr>
							<td width="25%"><strong><s:text
								name="garuda.cbuShipment.label.paerworkloc" /></strong>:</td>
							<td width="50%" colspan="2"><s:select
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DEFAULT_SHIPPING_PAPER_WRK_LOC]"
								name="shipmentPojo.paperworkLoc" headerValue="select"
								id="cbuPaperworkLoc" listKey="pkCodeId" listValue="description"
								headerKey="" value="%{#rowVal[18]}"></s:select></td>
							<td width="25%">
							<s:if
								test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPMENT_ITER_CODESUBTYPE].size>0">
								<s:checkbox name="shipmentItinerarychkname"
									id="shipmentItinerarychkId" checked="checked" disabled="true">
									<a href="#"
										onclick="showModal('Shipment Itinerary','getShipmentItern?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />','400','500');"><strong><s:text
										name="garuda.cbuShipment.label.itineraryrec"></s:text></strong></a>
								</s:checkbox>
							</s:if><s:else>
								<s:checkbox name="shipmentItinerarychkname"
									id="shipmentItinerarychkId" disabled="true">
									<strong><s:text
										name="garuda.cbuShipment.label.itineraryrec"></s:text></strong>
								</s:checkbox>
							</s:else></td>
						</tr>
						<s:if test="%{#rowVal[15]!=null && #rowVal[15]!=''}">
							<tr>
								<td colspan="4" width="100%">
								<fieldset><legend> </legend>
								<table>
									<tr>
										<td width="25%"><strong><s:text
											name="garuda.cbuShipment.label.shipmentconformedby" /></strong>:</td>
										<td width="25%"><s:textfield name="sctodConfby"
											disabled="true" value="%{#rowVal[14]}" /></td>
										<td width="25%"><strong><s:text
											name="garuda.cbuShipment.label.shipmentconformeddt" /></strong>:</td>
										<td width="25%"><s:date name="ShipConfDt"
											id="datepicker14" format="MMM dd, yyyy hh:mm aaa"></s:date> <s:textfield
											readonly="true" onkeydown="cancelBack();"  name="ShipConfDt" class="datepic"
											id="datepicker14" value="%{#rowVal[15]}" /></td>
									</tr>
								</table>
								</fieldset>
								</td>
							</tr>
						</s:if>
						<s:hidden id="shipschFlag" value="%{#rowVal[11]}"></s:hidden>
						<s:hidden id="packageslipFlag" value="%{#rowVal[13]}"></s:hidden>
						<s:hidden id="cordShipedFlag" value="%{#rowVal[12]}"></s:hidden>
						<tr>
							<td width="100%" colspan="4">
							<table bgcolor="#cccccc" class="tabledisplay disable_esign"
								width="100%">
								<tr bgcolor="#cccccc" valign="baseline">
									<td width="70%"><jsp:include page="../cb_esignature.jsp"
										flush="true">
										<jsp:param value="editModalOrCbuShipment" name="submitId" />
										<jsp:param value="invalidModalOrCbuShipment" name="invalid" />
										<jsp:param value="minimumModalOrCbuShipment" name="minimum" />
										<jsp:param value="passCbuModalOrShipment" name="pass" />
									</jsp:include></td>
									<td align="left" width="30%"><input type="button"
										id="editModalOrCbuShipment" onclick="updateCbuShipmentEdit();"
										value="<s:text name="garuda.unitreport.label.button.submit"/>"
										disabled="disabled" />
									<input type="button"  onclick="closeModals('cbushipmodaldiv')" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />	
									</td>
								</tr>
							</table>
							</td>
						</tr>
					</s:iterator>
				</s:if>
				<s:else>
					<tr>
						<td width="25%"><strong><s:text
							name="garuda.cbuShipment.label.cbusheduledshipdate" /></strong>:<span
							style="color: red;">*</span></td>
						<td width="25%"><s:date name="schShipmentDate"
							id="ModalcbuSchShipmentDate" format="MMM dd, yyyy"></s:date> <s:textfield
							readonly="true" onkeydown="cancelBack();"  name="shipmentPojo.schShipmentDate"
							class="datepic" cssClass="datePicWMinDate"
							id="ModalcbuSchShipmentDate" value=""
							onchange="getDayfromshipdate(this.value,'cbushimentdayModalDiv');validateCBUShipmentDet();" onfocus="onFocusCall(this);"/>
						<div id="errorcbuSchShipmentDateDiv"><span
							id="cbushipmentDatelbl" class="error"></span></div>
						</td>

						<td width="25%"><strong><s:text
							name="garuda.cbushipmetInfo.label.day"></s:text></strong>:</td>
						<td width="25%" id="cbushimentdayModalDiv"><span
							style="font-style: italic; color: blue"></span></td>
					</tr>
					<tr>
						<td width="25%"><strong><s:text
							name="garuda.cbuShipment.label.cbusheduledshiptime" /></strong>:</td>
						<td width="25%"><s:textfield name="shipmentPojo.shipmentTime"
							id="cbuShedShipmentTime" cssClass="timeValid" maxlength="5"
							onchange="validateCBUShipmentDet();" />
						<div id="errorcbuSchShipmenttimeDiv"><span
							id="cbushipmentTimelbl" class="error"></span></div>
						</td>
						<s:if test="#rowVal[17]!=null">
							<td width="25%"><strong><s:text
								name="garuda.cbuShipment.label.noofdaystoshipcbu" /></strong>:</td>
							<td width="25%"><s:textfield name="noofdaystoshipcbu"
								id="noofdaystoshipcbu" value="" disabled="true"></s:textfield></td>
						</s:if>
						<s:else>
							<td width="25%"></td>
							<td width="25%"></td>
						</s:else>
					</tr>

					<tr>
						<td width="25%"><strong><s:text
							name="garuda.cbuShipment.label.shipcompany" /></strong>:</td>
						<td width="25%"><s:select
							name="shipmentPojo.fkshipingCompanyId" id="cbuFkshipingCompanyId"
							list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPER]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" value="%{defaultshipper}"
							onchange="validateCBUShipmentDet();"></s:select>
						<div id="errorcbuShippingCompanyDiv"><span
							id="cbuShippingCompanylbl" class="error"></span></div>
						</td>
						<td width="25%"><strong><s:text
							name="garuda.ctShipmentInfo.label.trackingno" /></strong>:</td>
						<td width="25%"><s:textfield
							name="shipmentPojo.shipmentTrackingNo" id="cbuShipmentTrackingNo"
							value="" maxlength="30" /></td>
					</tr>
					<tr>
						<td width="25%"><strong><s:text
							name="garuda.cbuShipment.label.shippingcontainer" /></strong>:</td>
						<td width="25%"><s:select
							name="shipmentPojo.fkShippingContainer"
							id="cbuFkShippingContainer"
							list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPING_CONTAINER]"
							listKey="pkCodeId" listValue="description" value=""
							onchange="validateCBUShipmentDet();" />
						<div id="errorcbuShippingContainerDiv"><span
							id="cbuShippingContainerlbl" class="error"></span></div>
						</td>
						<td width="25%"><strong><s:text
							name="garuda.cbuShipment.label.tempmonitor" /></strong>:</td>
						<td width="25%"><s:select name="shipmentPojo.fkTempMoniter"
							id="cbuFkTempMoniter"
							list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEMP_MONITOR]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" value=""
							onchange="validateCBUShipmentDet();" />
						<div id="errorcbuTempMonitorDiv"><span
							id="cbuTempMonitorlbl" class="error"></span></div>
						</td>
					</tr>
					<tr>
						<td width="25%"><strong><s:text
							name="garuda.cbuShipment.label.actualshipdate" /></strong>:</td>
						<td width="25%"><s:textfield name="shipmentPojo.shipmentDate"
							id="actshipmentDate" disabled="true" value="" /></td>
						<td width="25%"><strong><s:text
							name="garuda.cbuShipment.label.anticipatedarrivaldate" /></strong>:</td>
						<td width="25%"><s:textfield
							name="shipmentPojo.expectingArriveDate" disabled="true" value="" />
						</td>
					</tr>
					<tr>
						<td width="25%"><strong><s:text
							name="garuda.cbuShipment.label.shipperpadlockcombination" /></strong>:</td>
						<td width="25%"><s:textarea
							name="shipmentPojo.padlockCombination" id="cbuPadlockCombination"
							value="" onkeyup="limitText(this,200);" style="width: 100px; height: 70px;"></s:textarea>
						</td>
						<td width="25%"><strong><s:text
							name="garuda.cbuShipment.label.additishipperdet" /></strong>:</td>
						<td width="25%"><s:textarea
							name="shipmentPojo.additiShiiperDet" id="cbuAdditiShiiperDet"
							value="" onkeyup="limitText(this,200);" style="width: 100px; height: 70px;"></s:textarea>
						</td>
					</tr>
					<tr>
						<td width="25%"><strong><s:text
							name="garuda.cbuShipment.label.paerworkloc" /></strong>:</td>
						<td width="50%" colspan="2"><s:select
							list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DEFAULT_SHIPPING_PAPER_WRK_LOC]"
							name="shipmentPojo.paperworkLoc" headerValue="select"
							id="cbuPaperworkLoc" listKey="pkCodeId" listValue="description"
							headerKey="" value="%{defaultPaperLoc}"></s:select></td>
						<td width="25%">
							<s:if
								test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPMENT_ITER_CODESUBTYPE].size>0">
								<s:checkbox name="shipmentItinerarychkname"
									id="shipmentItinerarychkId" checked="checked" disabled="true">
									<a href="#"
										onclick="showModal('Shipment Itinerary','getShipmentItern?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />','400','500');"><strong><s:text
										name="garuda.cbuShipment.label.itineraryrec"></s:text></strong></a>
								</s:checkbox>
							</s:if><s:else>
								<s:checkbox name="shipmentItinerarychkname"
									id="shipmentItinerarychkId" disabled="true">
									<strong><s:text
										name="garuda.cbuShipment.label.itineraryrec"></s:text></strong>
								</s:checkbox>
							</s:else></td>
					</tr>
					<s:if test='orderPojo.resolByTc!=pkCanceled'>
						<tr>
							<td width="100%" colspan="4">
							<table bgcolor="#cccccc" class="tabledisplay disable_esign"
								width="100%">
								<tr bgcolor="#cccccc" valign="baseline">
									<td width="70%"><jsp:include page="../cb_esignature.jsp"
										flush="true">
										<jsp:param value="submitModalOrCbuShipment" name="submitId" />
										<jsp:param value="invalidModalOrCbuShipment" name="invalid" />
										<jsp:param value="minimumModalOrCbuShipment" name="minimum" />
										<jsp:param value="passCbuModalOrShipment" name="pass" />
									</jsp:include></td>
									<td align="left" width="30%"><input type="button"
										id="submitModalOrCbuShipment" onclick="updateCbuShipment();"
										value="<s:text name="garuda.unitreport.label.button.submit"/>"
										disabled="disabled" />
									<input type="button"  onclick="closeModals('cbushipmodaldiv')" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />	
									</td>
								</tr>
							</table>
							</td>
						</tr>
					</s:if>
				</s:else>
			</table>
			</div>
			</div>
			</div>
			</td>
		</tr>
	</table>
</s:form>