<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
String contextpath=request.getContextPath();
%>
<%@ page import="java.util.List,com.velos.eres.service.util.*"%>

	<%@ page contentType="text/html; charset=UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
	<%@ page import="com.velos.ordercomponent.util.VelosGarudaConstants" %>
		
	<%@ page import="java.util.List,java.util.ArrayList,java.util.Map,java.util.HashMap" %>

<%@page import="com.velos.eres.web.user.UserJB"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<script>
var cotextpath = "<%=contextpath%>";
</script>
<%

	String GARUDA_CBU = "CBU";
	String GARUDA_PRODUCTFULLFILLMENT = "PRODUCTFULLFILLMENT";
	String GARUDA_ADMIN = "ADMIN";
	UserJB userInfo = (UserJB)session.getAttribute("currentUser");
	String skin = "";
	
%>
<jsp:useBean id="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id="codeLst" scope="session" class="com.velos.eres.web.codelst.CodelstJB"/>
<%@ page language="java" import="java.util.*,com.velos.eres.web.user.UserJB, com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>

<%
HttpSession tSession = request.getSession(true);
if (sessionmaint.isValidSession(tSession)) {
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	int patientRights = Integer.parseInt(grpRights.getFtrRightsByValue("MPATIENTS"));
	int studyRights = Integer.parseInt(grpRights.getFtrRightsByValue("NPROTOCOL"));
	UserJB userB1 = (UserJB) tSession.getValue("currentUser");
	String accName = (String) tSession.getValue("accName");
	String accId = (String) tSession.getValue("accountId");
	accName=(accName==null)?"default":accName;
	String accSkinId = "";
	String usersSkinId = userB1.getUserSkin();
	String userSkin = "";
	String accSkin = (String) tSession.getValue("accSkin");
	skin = "default";
	userSkin= codeLst.getCodeCustomCol(StringUtil.stringToNum(usersSkinId) );
	accSkin= codeLst.getCodeCustomCol(StringUtil.stringToNum(accSkinId) );

	if (accSkin == null || accSkin.equals("") || accSkin.equals("default") ){
		accSkin = "accSkin";
	} else {
		skin = accSkin;
	}

	if (userSkin != null && !userSkin.equals("") ){
		skin = userSkin;
	}
}
%>
 

<!-- Garuda Common CSS -->
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/velosCDR.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/<%=skin%>/Garuda_skin.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/general.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/portlet.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/jquery.alerts.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/pending-order.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/styles/jquery.time.slider.css" media="screen"/>

<!-- Common CSS -->
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/forms.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/clickmenu.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/styles.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/common/demo_table.css" media="screen" />

<html>
<head>
<title></title>

<script>


function getdata(){
	
     window.print(); 
     window.close(); 
	
}
</script>

</head>

<body onload="getdata();" class="maincontainer">
<table width="100%">
	<tr>
		<td><div class="headerStyle row" id="header"> </div> <s:form
			action="" id="CBBProcedureForm" name="CBBProcedureForm">
			<table width="100%" cellspacing="0" cellpadding="0" border="0"
				id="status">
			<tbody>
				<tr>
				<td colspan="2">
					<table width="100%" class="tabledisplay" align="center" border="0">
					
						<tr>
							<th><s:text
								name="garuda.cbbprocedures.label.processingprocedure"></s:text>: <b><u><s:property
								value="cbbProcedurePojo.procName"></s:property></u></b></th>
						</tr>
						<tr>
							<td>
							<hr align="center" width="100%"></hr>
							</td>
						</tr>
					</table>
				</td>
				</tr>
				<tr>
					<td colspan="2">
					<fieldset><legend><s:text
						name="garuda.cbbprocedures.label.cbbprocessingprocedures"></s:text></legend>

					<table>
						<tr>
							<!-- <td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.procno"></s:text>:</td>
							<td width="25%" align="left"><s:property
								value="cbbProcedurePojo.procNo" /></td> -->
								
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.procname"></s:text>:</td>
							<td width="25%" align="left"><s:property
								value="cbbProcedurePojo.procName" /></td>
								<!-- 
							<td width="25%" align="left"><s:text
								name="garuda.cbuentry.label.productcode"></s:text>:</td>
							<td width="25%" align="left"><s:property
								value="cbbProcedurePojo.productCode" /></td> -->
						</tr>
						<tr>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.procstartdate"></s:text>:</td>
							<td width="25%" align="left"><s:date
								name="cbbProcedurePojo.procStartDate" id="datepicker1"
								format="MMM dd, yyyy" /> <s:hidden
								name="cbbProcedurePojo.procStartDateStr" class="datepic"
								id="datepicker1" value="%{datepicker1}" /> <s:label
								value="%{datepicker1}"></s:label></td>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.proctermidate"></s:text>:</td>
							<td width="25%" align="left"><s:date
								name="cbbProcedurePojo.procTermiDate" id="datepicker6"
								format="MMM dd, yyyy" /> <s:hidden
								name="cbbProcedurePojo.procTermiDateStr" class="datepic"
								id="datepicker6" value="%{datepicker6}" /> <s:label
								value="%{datepicker6}"></s:label></td>
						</tr>
						<tr>
					     <td width="25%" align="left"><s:text name="garuda.cbuentry.label.CBB" /></td>
					     <td width="25%" align="left">
					       <s:iterator value="sites" >
					       <s:if test="cbbProcedurePojo.fkSite == siteId">
					       <s:property value="siteIdentifier"/>
					       </s:if>
					       </s:iterator>
					    </td>
						</tr>
						
					</table>
					</fieldset>
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<fieldset><legend><s:text
						name="garuda.cbbprocedures.label.productmodification"></s:text></legend>
					<table>
						<tr>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.processing"></s:text>:</td>
							<td width="25%" align="left"><s:property
								value="getCodeListDescById(cbbProcedureInfoPojo.fkProcMethId)" />
							</td>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.ifautomated"></s:text>:</td>
							<td width="25%" align="left"><s:property
								value="getCodeListDescById(cbbProcedureInfoPojo.fkIfAutomated)" />
							</td>
						</tr>
						<s:if test="cbbProcedureInfoPojo.fkIfAutomated==cbbProcedureInfoPojo.pkIfAutomatedOther">
							<tr>
							<td width="25%" align="left"></td>
							<td width="25%" align="left"></td>

							<td width="25%" align="left"><s:text
										name="garuda.cbbprocedures.label.specifyoth"></s:text>:</td>
							<td width="25%" align="left"><s:property
									value="cbbProcedureInfoPojo.otherProcessing" /></td>
						</tr>
						</s:if>
						<tr>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.productmodification"></s:text>:
							</td>
							<td width="25%" align="left"><s:property
								value="getCodeListDescById(cbbProcedureInfoPojo.fkProductModification)" />
							</td>
							<td width="25%" align="left"></td>
							<td width="25%" align="left"></td>
						</tr>
						<s:if test="cbbProcedureInfoPojo.fkProductModification==cbbProcedureInfoPojo.pkProdModiOther">
						<tr>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.specifyoth"></s:text>:</td>
							<td width="25%" align="left"><s:property
									value="cbbProcedureInfoPojo.otherProdModi" /></td>
							<td width="25%" align="left"></td>
							<td width="25%" align="left"></td>
						</tr>
						</s:if>
					</table>
					</fieldset>
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<fieldset><legend><s:text
						name="garuda.cbbprocedures.label.storageconditions"></s:text></legend>
					<table>
						<tr>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.stormethod"></s:text>:</td>
							<td width="25%" align="left"><s:property
								value="getCodeListDescById(cbbProcedureInfoPojo.fkStorMethod)" />
							</td>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.freezmanufac"></s:text>:</td>
							<td width="25%" align="left"><s:property
								value="getCodeListDescById(cbbProcedureInfoPojo.fkFreezManufac)" />
							</td>
						</tr>
						<s:if test="cbbProcedureInfoPojo.fkFreezManufac==cbbProcedureInfoPojo.pkFreezManuOther">
						<tr>
							<td width="25%" align="left"></td>
							<td width="25%" align="left"></td>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.freezmanufac"></s:text> - <s:text
								name="garuda.cbbprocedures.label.specifyoth"></s:text>:</td>
							<td width="25%" align="left"><s:property
								value="cbbProcedureInfoPojo.otherFreezManufac" /></td>
						</tr>
						</s:if>
						<tr>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.frozenin"></s:text>:</td>
							<td width="25%" align="left"><s:property
								value="getCodeListDescById(cbbProcedureInfoPojo.fkFrozenIn)" />
							</td>
							<td width="25%" align="left"></td>
							<td width="25%" align="left"></td>

						</tr>
						<s:if test="cbbProcedureInfoPojo.fkFrozenIn==cbbProcedureInfoPojo.pkFrozenInOther">
							<tr>
								<td width="25%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
								</s:text>
								</td>
								<td width="25%" align="left">
								<s:property value="cbbProcedureInfoPojo.otherFrozenCont"/>
								</td>
							</tr>
						</s:if>
						<s:if test="cbbProcedureInfoPojo.fkFrozenIn==cbbProcedureInfoPojo.pkFrozenInBag">
						<tr>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.noofbags"></s:text>:</td>
							<td width="25%" align="left"><s:property
								value="getCodeListDescById(cbbProcedureInfoPojo.noOfBags)" />
							</td>
							<td width="25%" align="left"></td>
							<td width="25%" align="left"></td>
						</tr>
						</s:if>
						<s:if test="cbbProcedureInfoPojo.noOfBags==cbbProcedureInfoPojo.pkBag1Type">
						<tr>
							<td align="left" colspan="4">
							<table>
							<tr>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.bagtype"></s:text>:</td>
							<td width="25%" align="left"><s:property value="getCodeListDescById(cbbProcedureInfoPojo.fkBag1Type)"/>
							</td>
							</tr>
							<tr>
								<td width="25%" align="left"><s:text
									name="garuda.cbbprocedures.label.cryobagmanufac"></s:text>:</td>
								<td width="25%" align="left"><s:property value="cbbProcedureInfoPojo.cryoBagManufac"/></td>
								<td width="25%" align="left"><s:text
									name="garuda.cbbprocedures.label.maxvalue"></s:text>(<s:text
									name="garuda.cbbprocedures.label.ml"></s:text>):</td>
								<td width="25%" align="left">
								<s:property value="cbbProcedureInfoPojo.maxValue"/>
								</td>
							</tr>
							<s:if test="cbbProcedureInfoPojo.fkBag1Type==cbbProcedureInfoPojo.pkOtherBagType">
							<tr>
							<td width="50%" align="left" colspan="2">
									<table>
										<tr>
											<td width="25%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
											</s:text>
											</td>
											<td width="25%" align="left">
											<s:property value="cbbProcedureInfoPojo.otherBagType1"/>
											</td>
										</tr>
									</table>
							</td>
							</tr>
							</s:if>
							</table>
							</td>
							</tr>
						</s:if>
						<s:if test="cbbProcedureInfoPojo.noOfBags==cbbProcedureInfoPojo.pkBag2Type">
							<tr>
							<td align="left" colspan="4">
							<table>
							<tr>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.bag1type"></s:text>:</td>
							<td width="25%" align="left">
							<s:property value="getCodeListDescById(cbbProcedureInfoPojo.fkBag1Type)"/>
							</td>
							</tr>
							<tr>
								<td width="25%" align="left"><s:text
									name="garuda.cbbprocedures.label.cryobagmanufac.bagtype1"></s:text>:</td>
								<td width="25%" align="left"><s:property value="cbbProcedureInfoPojo.cryoBagManufac"/></td>
								<td width="25%" align="left"><s:text
									name="garuda.cbbprocedures.label.maxvalue.bagtype1"></s:text>(<s:text
									name="garuda.cbbprocedures.label.ml"></s:text>):</td>
								<td width="25%" align="left">
								<s:property value="cbbProcedureInfoPojo.maxValue"/>
								</td>
							</tr>
							<s:if test="cbbProcedureInfoPojo.fkBag1Type==cbbProcedureInfoPojo.pkOtherBagType">
							<tr>
							<td width="50%" align="left" colspan="2">
									<table>
										<tr>
											<td width="25%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
											</s:text>
											</td>
											<td width="25%" align="left">
											<s:property value="cbbProcedureInfoPojo.otherBagType1"/>
											</td>
										</tr>
									</table>
							</td>
							</tr>
							</s:if>
							</table>
							</td>
							</tr>
							<tr>
							<td align="left" colspan="4">
							<table>
							<tr>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.bag2type"></s:text>:</td>
							<td width="25%" align="left">
							<s:property value="getCodeListDescById(cbbProcedureInfoPojo.fkBag2Type)"/>
							</td>
							</tr>
							<tr>
								<td width="25%" align="left"><s:text
									name="garuda.cbbprocedures.label.cryobagmanufac.bagtype2"></s:text>:</td>
								<td width="25%" align="left">
								<s:property value="cbbProcedureInfoPojo.cryoBagManufac2"/>
								</td>
								<td width="25%" align="left"><s:text
									name="garuda.cbbprocedures.label.maxvalue.bagtype2"></s:text>(<s:text
									name="garuda.cbbprocedures.label.ml"></s:text>):</td>
								<td width="25%" align="left">
								<s:property value="cbbProcedureInfoPojo.maxValue2"/>
								</td>
							</tr>
							<s:if test="cbbProcedureInfoPojo.fkBag2Type==cbbProcedureInfoPojo.pkOtherBagType">
							<tr>
							<td width="50%" align="left" colspan="2">
									<table>
										<tr>
											<td width="25%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
											</s:text>
											</td>
											<td width="25%" align="left">
											<s:property value="cbbProcedureInfoPojo.otherBagType2"/>
											</td>
										</tr>
									</table>
							</td>
							</tr>
							</s:if>
							</table>
							</td>
							</tr>
						</s:if>
						<s:if test="cbbProcedureInfoPojo.noOfBags==cbbProcedureInfoPojo.pkNoOfBagsOthers">
						<tr>
							<td width="50%" align="left" colspan="2">
									<table>
										<tr>
											<td width="25%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
											</s:text>
											</td>
											<td width="25%" align="left">
											<s:property value="cbbProcedureInfoPojo.otherBagType"/>
											</td>
										</tr>
									</table>
							</td>
						</tr>
						</s:if>
						<tr>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.stortemp"></s:text>:</td>
							<td width="25%" align="left"><s:property
								value="getCodeListDescById(cbbProcedureInfoPojo.fkStorTemp)"  escapeHtml="false"/>
							</td>
							<td width="25%" align="left"></td>
							<td width="25%" align="left"></td>
						</tr>
						<tr>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.controlledratefreezing"></s:text>:
							</td>
							<td width="25%" align="left"><s:property
								value="getCodeListDescById(cbbProcedureInfoPojo.fkContrlRateFreezing)" />
							</td>
							<td width="25%" align="left"><s:text
								name="garuda.cbbprocedures.label.noofindifrac"></s:text>:</td>
							<td width="25%" align="left"><s:property
								value="cbbProcedureInfoPojo.noOfIndiFrac" /></td>
						</tr>
					</table>
					</fieldset>
					</td>
				</tr>
			</tbody>
			<tbody>
				
				<tr>
					<!--
                   <th><s:text name="garuda.cbbprocedures.label.frozenproductcomposition"></s:text></th>
                   -->
					<td colspan="2">

					<fieldset><legend> <s:text
						name="garuda.cbbprocedures.label.additivesbeforevolumereductions"></s:text>
						
					</legend>
					<table>
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.thouunitpermlheparin"></s:text>:
							</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.thouUnitPerMlHeparin)=='' || !(cbbProcedureInfoPojo.thouUnitPerMlHeparin)==null">
							<s:property
								value="cbbProcedureInfoPojo.thouUnitPerMlHeparin" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.thouUnitPerMlHeparinper)=='' || !(cbbProcedureInfoPojo.thouUnitPerMlHeparinper)==null">
								<s:property
								value="cbbProcedureInfoPojo.thouUnitPerMlHeparinper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if>
								</td>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.cpda"></s:text>:</td>
							<td width="25%">
								<s:if test="!(cbbProcedureInfoPojo.cpda)=='' || !(cbbProcedureInfoPojo.cpda)==null">
								<s:property
								value="cbbProcedureInfoPojo.cpda" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.cpdaper)=='' || !(cbbProcedureInfoPojo.cpdaper)==null"><s:property
								value="cbbProcedureInfoPojo.cpdaper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
								
						
						</tr>
						<tr>
								
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.fiveunitpermlheparin"></s:text>:
							</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.fiveUnitPerMlHeparin)=='' || !(cbbProcedureInfoPojo.fiveUnitPerMlHeparin)==null"><s:property
								value="cbbProcedureInfoPojo.fiveUnitPerMlHeparin" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.fiveUnitPerMlHeparinper)=='' || !(cbbProcedureInfoPojo.fiveUnitPerMlHeparinper)==null"><s:property
								value="cbbProcedureInfoPojo.fiveUnitPerMlHeparinper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.cpd"></s:text>:</td>						
							<td width="25%">
							<s:if test="!(cbbProcedureInfoPojo.cpd)=='' || !(cbbProcedureInfoPojo.cpd)==null"><s:property
								value="cbbProcedureInfoPojo.cpd" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.cpdper)=='' || !(cbbProcedureInfoPojo.cpdper)==null"><s:property
								value="cbbProcedureInfoPojo.cpdper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
						</tr>
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.tenunitpermlheparin"></s:text>:
							</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.tenUnitPerMlHeparin)=='' || !(cbbProcedureInfoPojo.tenUnitPerMlHeparin)==null"><s:property
								value="cbbProcedureInfoPojo.tenUnitPerMlHeparin" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							  </s:if>
								<s:if test="!(cbbProcedureInfoPojo.tenUnitPerMlHeparinper)=='' || !(cbbProcedureInfoPojo.tenUnitPerMlHeparinper)==null"> <s:property
								value="cbbProcedureInfoPojo.tenUnitPerMlHeparinper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text></s:if></td>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.acd"></s:text>:</td>
							<td width="25%">
							<s:if test="!(cbbProcedureInfoPojo.acd)=='' || !(cbbProcedureInfoPojo.acd)==null"><s:property
								value="cbbProcedureInfoPojo.acd" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.acdper)=='' || !(cbbProcedureInfoPojo.acdper)==null"><s:property
								value="cbbProcedureInfoPojo.acdper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if>
							</td>	
						</tr>
					
						<tr>
										<td width="25%"><s:text
								name="garuda.cbbprocedures.label.sixperhydroxyethylstarch"></s:text>:
							</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.sixPerHydroxyethylStarch)=='' || !(cbbProcedureInfoPojo.sixPerHydroxyethylStarch)==null"><s:property
								value="cbbProcedureInfoPojo.sixPerHydroxyethylStarch" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.sixPerHydroxyethylStarchper)=='' || !(cbbProcedureInfoPojo.sixPerHydroxyethylStarchper)==null"><s:property
								value="cbbProcedureInfoPojo.sixPerHydroxyethylStarchper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
							
							
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.otheranticoagulant"></s:text>:
							</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.otherAnticoagulant)=='' || !(cbbProcedureInfoPojo.otherAnticoagulant)==null"><s:property
								value="cbbProcedureInfoPojo.otherAnticoagulant" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.otherAnticoagulantper)=='' || !(cbbProcedureInfoPojo.otherAnticoagulantper)==null"><s:property
								value="cbbProcedureInfoPojo.otherAnticoagulantper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
						</tr>
						<s:if test="(!(cbbProcedureInfoPojo.otherAnticoagulant)=='' || !(cbbProcedureInfoPojo.otherAnticoagulant)==null) || (!(cbbProcedureInfoPojo.otherAnticoagulantper)=='' || !(cbbProcedureInfoPojo.otherAnticoagulantper)==null)">
						<tr>
							<td colspan="4">
							<div id="otherAnticoagulantDivMod">
							<table>
								<tr>
									<td width="25%" align="left"><s:text name="garuda.processingrep.specifyotheranticoag"></s:text>:
									</td>
									<td width="25%" align="left"><s:property
										value="cbbProcedureInfoPojo.specifyOthAnti" /></td>
									<td width="25%" align="left"></td>
									<td width="25%" align="left"></td>
								</tr>
							</table>
							</div>
							</td>
						</tr>
						</s:if>
					</table>
					</fieldset>
					<fieldset><legend> <s:text
						name="garuda.cbbprocedures.label.cryoprotectantsandotheradditives"></s:text>
					</legend>
					<table>
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.hunperdmso"></s:text>:</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.hunPerDmso)=='' || !(cbbProcedureInfoPojo.hunPerDmso)==null"><s:property
								value="cbbProcedureInfoPojo.hunPerDmso" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.hunPerDmsoper)=='' || !(cbbProcedureInfoPojo.hunPerDmsoper)==null"><s:property
								value="cbbProcedureInfoPojo.hunPerDmsoper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.twenfivehumalbu"></s:text>:</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.twenFiveHumAlbu)=='' || !(cbbProcedureInfoPojo.twenFiveHumAlbu)==null"><s:property
								value="cbbProcedureInfoPojo.twenFiveHumAlbu" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.twenFiveHumAlbuper)=='' || !(cbbProcedureInfoPojo.twenFiveHumAlbuper)==null"><s:property
								value="cbbProcedureInfoPojo.twenFiveHumAlbuper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
						</tr>
						<tr>
						
														
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.hunperglycerol"></s:text>:</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.hunPerGlycerol)=='' || !(cbbProcedureInfoPojo.hunPerGlycerol)==null"><s:property
								value="cbbProcedureInfoPojo.hunPerGlycerol" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.hunPerGlycerolper)=='' || !(cbbProcedureInfoPojo.hunPerGlycerolper)==null"><s:property
								value="cbbProcedureInfoPojo.hunPerGlycerolper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
								
							
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.plasmalyte"></s:text>:</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.plasmalyte)=='' || !(cbbProcedureInfoPojo.plasmalyte)==null"><s:property
								value="cbbProcedureInfoPojo.plasmalyte" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.plasmalyteper)=='' || !(cbbProcedureInfoPojo.plasmalyteper)==null"><s:property
								value="cbbProcedureInfoPojo.plasmalyteper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
							
				
						</tr>
						<tr>
								<td width="25%"><s:text
								name="garuda.cbbprocedures.label.tenperdextran40"></s:text>:</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.tenPerDextran_40)=='' || !(cbbProcedureInfoPojo.tenPerDextran_40)==null"><s:property
								value="cbbProcedureInfoPojo.tenPerDextran_40" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.tenPerDextran_40per)=='' || !(cbbProcedureInfoPojo.tenPerDextran_40per)==null"><s:property
								value="cbbProcedureInfoPojo.tenPerDextran_40per"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.othcryoprotectant"></s:text>:</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.othCryoprotectant)=='' || !(cbbProcedureInfoPojo.othCryoprotectant)==null"><s:property
								value="cbbProcedureInfoPojo.othCryoprotectant" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.othCryoprotectantper)=='' || !(cbbProcedureInfoPojo.othCryoprotectantper)==null"><s:property
								value="cbbProcedureInfoPojo.othCryoprotectantper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
						</tr>
						<tr>
									
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.fiveperhumanalbu"></s:text>:</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.fivePerHumanAlbu)=='' || !(cbbProcedureInfoPojo.fivePerHumanAlbu)==null"><s:property
								value="cbbProcedureInfoPojo.fivePerHumanAlbu" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.fivePerHumanAlbuper)=='' || !(cbbProcedureInfoPojo.fivePerHumanAlbuper)==null"><s:property
								value="cbbProcedureInfoPojo.fivePerHumanAlbuper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
							<td width="25%"></td>
							<td width="25%"></td>
						</tr>
						
						<s:if test="(!(cbbProcedureInfoPojo.othCryoprotectant)=='' || !(cbbProcedureInfoPojo.othCryoprotectant)==null) || (!(cbbProcedureInfoPojo.othCryoprotectantper)=='' || !(cbbProcedureInfoPojo.othCryoprotectantper)==null)">
						<tr>
							<td colspan="4">
							<div id="otherCryoprotectantDivMod">
							<table>
								<tr>
									<td width="25%" align="left"><s:text
										name="garuda.cbbprocedures.label.specothcryopro"></s:text>:</td>
									<td width="25%" align="left"><s:property
										value="cbbProcedureInfoPojo.specOthCryopro" /></td>
									<td width="25%" align="left"></td>
									<td width="25%" align="left"></td>
								</tr>
							</table>
							</div>
							</td>
						</tr>
						</s:if>
					</table>
					</fieldset>
					<fieldset><legend> <s:text
						name="garuda.cbbprocedures.label.diluents"></s:text> </legend>
					<table>
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.fiveperdextrose"></s:text>:</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.fivePerDextrose)=='' || !(cbbProcedureInfoPojo.fivePerDextrose)==null"><s:property
								value="cbbProcedureInfoPojo.fivePerDextrose" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.fivePerDextroseper)=='' || !(cbbProcedureInfoPojo.fivePerDextroseper)==null"><s:property
								value="cbbProcedureInfoPojo.fivePerDextroseper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
							<td width="25%" align="left"></td>
							<td width="25%" align="left"></td>	
						</tr>
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.pointninepernacl"></s:text>:</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.pointNinePerNacl)=='' || !(cbbProcedureInfoPojo.pointNinePerNacl)==null"><s:property
								value="cbbProcedureInfoPojo.pointNinePerNacl" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.pointNinePerNaclper)=='' || !(cbbProcedureInfoPojo.pointNinePerNaclper)==null"><s:property
								value="cbbProcedureInfoPojo.pointNinePerNaclper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
							<td width="25%" align="left"></td>
							<td width="25%" align="left"></td>	
						</tr>
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.otherdiluents"></s:text>:</td>
							<td width="25%"><s:if test="!(cbbProcedureInfoPojo.othDiluents)=='' || !(cbbProcedureInfoPojo.othDiluents)==null"><s:property
								value="cbbProcedureInfoPojo.othDiluents" /><s:text name="garuda.cbbprocedures.label.ml"></s:text>
							   </s:if>
								<s:if test="!(cbbProcedureInfoPojo.othDiluentsper)=='' || !(cbbProcedureInfoPojo.othDiluentsper)==null"><s:property
								value="cbbProcedureInfoPojo.othDiluentsper"/><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
								</s:if></td>
							<td width="25%" align="left"></td>
							<td width="25%" align="left"></td>
						</tr>
						
						<s:if test="(!(cbbProcedureInfoPojo.othDiluents)=='' || !(cbbProcedureInfoPojo.othDiluents)==null) || (!(cbbProcedureInfoPojo.othDiluentsper)=='' || !(cbbProcedureInfoPojo.othDiluentsper)==null)">
						<tr>	
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.specothdiluents"></s:text>:</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.specOthDiluents" /></td>
							<td width="25%" align="left"></td>
							<td width="25%" align="left"></td>	
						</tr>
						</s:if>
					</table>
					</fieldset>
					</td>
				</tr>
				</tbody>
				<tbody>
				<tr>
					<td colspan="2">
					<fieldset><legend><s:text
						name="garuda.cbbprocedures.label.cbusamples"></s:text></legend>
					<table>
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.filterpaper"></s:text>:</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.filterPaper" /></td>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.rbcpellets"></s:text>:</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.rbcPallets" /></td>	
						</tr>
						<tr>
							
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.noofextrdnaaliquots"></s:text>:
							</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.noOfExtrDnaAliquots" /></td>
								
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.noofserumaliquots"></s:text>:</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.noOfSerumAliquots" /></td>	
						</tr>
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.noofplasmaaliquots"></s:text>:</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.noOfPlasmaAliquots" /></td>
							
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.noofnonviablealiquots"></s:text>:
							</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.noOfNonviableAliquots" /></td>
							
						</tr>
						
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.noofviablesampnotrep"></s:text>:
							</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.noOfViableSampleNotRep" /></td>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.noofsegments"></s:text>:</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.noOfSegments" /></td>
						</tr>
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.noofothrepaliqprod"></s:text>:</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.noOfOthRepAliqProd" /></td>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.noofothrepaliqaltcond"></s:text>:
							</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.noOfOthRepAliqAltCond" /></td>
						</tr>
					</table>
					</fieldset>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
					<fieldset><legend><s:text
						name="garuda.cbbprocedures.label.maternalsamples"></s:text></legend>
					<table>
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.noofserummateraliquots"></s:text>:
							</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.noOfSerumMaterAliquots" /></td>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.noofplasmamateraliquots"></s:text>:
							</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.noOfPlasmaMaterAliquots" /></td>
						</tr>
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.noofextrdnamateraliquots"></s:text>:
							</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.noOfExtrDnaMaterAliquots" /></td>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.noofcellmateraliquots"></s:text>:</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.noOfCellMaterAliquots" /></td>
						</tr>
					</table>
					</fieldset>
					</td>
				</tr>
				
				<!-- <tr>
					<td colspan="2">
					<fieldset><legend><s:text
						name="garuda.cbbprocedures.label.sop"></s:text></legend>
					<table>
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.soprefno"></s:text>:</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.sopRefNo" /></td>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.soptitle"></s:text>:</td>
							<td width="25%"><s:property
								value="cbbProcedureInfoPojo.sopTitle" /></td>
						</tr>
						<tr>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.sopstrtdate"></s:text>:</td>
							<td width="25%"><s:date
								name="cbbProcedureInfoPojo.sopStrtDate" id="datepicker7"
								format="MMM dd, yyyy" /> <s:hidden
								name="cbbProcedureInfoPojo.sopStrtDateStr" class="datepic"
								id="datepicker7" /> <s:label value="%{datepicker7}"></s:label>
							</td>
							<td width="25%"><s:text
								name="garuda.cbbprocedures.label.soptermidate"></s:text>:</td>
							<td width="25%"><s:date
								name="cbbProcedureInfoPojo.sopTermiDate" id="datepicker8"
								format="MMM dd, yyyy" /> <s:hidden
								name="cbbProcedureInfoPojo.sopTermiDateStr" class="datepic"
								id="datepicker8" /> <s:label value="%{datepicker8}"></s:label>
							</td>
						</tr>
					</table>
					</fieldset>
					</td>
				</tr>-->
			</tbody>
			</table>
		</s:form> 
		<div class="footerStyle" id="footer"></div> 
		 </td>
	</tr>
</table>
</body>
</html>