<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<s:if test="clinicalNotePojo!=null && clinicalNotePojo.noteAssessment!=null">
   <s:if test="clinicalNotePojo.noteAssessment=='na'">
     <script> $j("#availableCbuStatus").find('td:first').hide();</script>
     <script> $j("#tempAvailableCbuStatus").hide();</script>
     <script> $j("#notAvailableCbuStatus").show();</script>
   </s:if>
   <s:if test="clinicalNotePojo.noteAssessment=='tu'">
     <script> $j("#availableCbuStatus").find('td:first').hide();</script>
     <script> $j("#tempAvailableCbuStatus").show();</script>
     <script> $j("#notAvailableCbuStatus").hide();</script>
   </s:if>   
</s:if>
<s:if test="%{assessmentstatus=='true'}">
	 <script> $j("#availableCbuStatus").find('td:first').hide();</script>
     <script> $j("#tempAvailableCbuStatus").hide();</script>
     <script> $j("#notAvailableCbuStatus").show();</script>
</s:if>
<script>
$j(function(){
	/*var today = new Date();
	var d = today.getDate()+1;
	var m = today.getMonth();
	var y = today.getFullYear();
	var h=today.getHours();
	var mn=today.getMinutes()+1;
	$j('#cordstatusdatepicker1').datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true, changeYear: true});
	*/
	getDatePic();

	$j('#dialog-confirm-1 span').attr('style','font-weight: bold');
	$j("#cbuStatus").validate({
		rules:{	
				"cbuStatus":{required:true}
				},
			messages:{	
				"cbuStatus":"<s:text name="garuda.cbu.cordentry.status"/>"					
					}
								});
	$j('#notavail input[type=radio]+label').each(function(index,el) {
			$j(el).append('<br>');
	});
	if($j('#cbustatus').val()!='null' && $j('#cbustatus').val()!=""){
		$j('#'+$j('#cbustatus').val()).attr('checked','checked');
	}	
});

function cleardate(){
$j("#cordstatusdatepicker1").val("");
$j("#cordstatusdatepicker1").attr('disabled','disabled');
$j('#errormsg').html("");
$j('#errordate').html("");
}
function checkdate(){
	$j("#cordstatusdatepicker1").removeAttr('disabled');
}
function validateStatus(){
	var flag=0;
	var indexval=0;
	var codestatus;
	$j('#status input[type=radio]').each(function(index,el){
		var s=$j(el).attr('checked');
		if(s==true){
				flag=1;
				codestatus=$j(el).closest('fieldset').find('#cordstatus').text();
		}
	});

	if(flag==1){
		
		/*if(codestatus=='RO'){
			if($j('#cordstatusdatepicker1').val()!='null' && $j('#cordstatusdatepicker1').val()!=""){
				conformsubmitmodel();
			}else{
					var msg='<s:text name="garuda.cbu.status.enteravaildate"/>';
					
					$j('#errordate').html(msg);
			}
		}else{
				
			}*/
			conformsubmitmodel();
		
	}else{
			$j('#showerrormsg').attr('style','display:block');
			var msg='<s:text name="garuda.cbu.cordentry.status"/>';
			$j('#errormsg').html(msg);
		 }
}
function modalFormSubmitRefreshAssessmentDiv(formid,url){
	var divname=$j("#shrtName").val();
	if($j("#"+formid).valid()){
		$j.ajax({
			type: "POST",
		    url: url,
		    async:false,
		    data : $j("#"+formid).serialize(),
		    success: function (result){
		    	$j('.ui-datepicker').html("");
			    $j("#"+divname+"_availabledate").val($j("#cordstatusdatepicker1").val());
		        $j("#update").show();
		        $j(".status").hide();
		    },
			error:function() { 
				alert("Error :" + error);
	            alert(request.responseText);
			}
			
		});
	}
}

function conformsubmitmodel(){
	setOrdersEmpty();
	$j('#status input[type=radio]').each(function(index,el) {
		if($j(el).attr('checked')==true){
			var labelid=$j(el).attr('id')+"_label";
			var labeltext=$j('#'+labelid).text();
			var statusid=$j(el).closest('fieldset').find("legend").text();
			$j('#cnfmstatus1').text(statusid);
			$j('#statusreason').text(labeltext);
		}
	});
	$j( "#dialog-confirm-1" ).dialog({
		resizable: false,
		modal: true,
		closeText: '',
		closeOnEscape: false ,
		buttons: {
			"Yes": function() {
				$j( this ).dialog( "close" );
				if($j("#assessmentstatus").val()=="true"){
					
						modalFormSubmitRefreshAssessmentDiv('cbuStatus','submitCbuStatus');
						
						/*if($j('#cordCbuStatus').val() !== $j('#cbuStatusId').val()){
							
									$j('#cordCbuStatus').val($j('#cbuStatusId').val());
									$j('[name="cordCbuStatus"]:last').val($j('#cbuStatusId').val());
									
						}*/
						
						/*if($j('#cbuStatusId').val()!== undefined && ($j('#cbuStatusId').val()==='OT' || $j('#cbuStatusId').val()=== 'DD')){
							
								getRequestedPage('rollbackAutoDefer?statusCode='+$j('#cbuStatusId').val()+'&cdrCbuPojo.cordID='+$j('#pkcordId').val());
								
								$j('#'+$j('#executeAutoDeferFlagID').val()).val('false');
							
							}
						

						$j.get('getCbuStus', { cordId: $j('#pkcordId').val()} ,function( cbuStatus ) {
											
								if(cbuStatus){
							
									$j('#cordCbuStatus').val(cbuStatus);
									$j('[name="cordCbuStatus"]:last').val(cbuStatus);							
							   }											
						});*/
						
						AutoDefer.rollback();
				     	AutoDefer.resetCBUStatus();
						
				}else{
					if($j('#resetFun').val()=="reset"){
					var url = 'submitCbuStatus?pkcordId='+$j("#pkcordId").val()+'&licenceUpdate=True&cordAcceptId='+$j('#cordAcceptId').val()
					if($j("#orderId").val()!=null && $j("#orderId").val()!="" && $j("#orderId").val()!='undefined'){
						url = url +'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val();	
					}
					loadMoredivs(url,'finalreview','cbuStatus','update','status');
					flaggedItemsTable();
					//modalCordStatus('cbuStatus','submitCbuStatus?pkcordId='+$j("#pkcordId").val()+'&licenceUpdate=True&cordAcceptId='+$j('#cordAcceptId').val()+'&resetFun=reset','main');
					$j('.status').hide();
					$j('#cordUpdate').hide();
	            	$j("#cordUpdate").show();
					}
					else{
						var historyArr=new Array();
						var i=0; 
				 		$j('.detailhstry').each(function(){
							if($j(this).is(':visible')){
								var idval=$j(this).attr("id");
								historyArr[i]=idval;
								i++;
							}
						}); 
				 		
				 		
						if($j("#orderId").val()!=null && $j("#orderId").val()!="" && $j("#orderId").val()!='undefined'){
							loadMoredivs('submitCbuStatusFromPF?cdrCbuPojo.cordID='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val()+'&iShowRows='+$j('#showsRowcbuhistorytbl').val()+'&iscordavailfornmdp='+$j("#iscordavailfornmdpId").val(),'cordlocalstatusdiv,ctcbuavailconfbarDiv,currentRequestProgressMainDiv,chistory,cReqhistory,taskCompleteFlagsDiv,ctShipmentInfoContentDiv,cbuShipmentInfoContentDiv,nmdpResearchSampleContentDiv','cbuStatus','update','status');
							if(!('<s:property value="#request.autoDeferRollbackStatus"/>'== 'false')){
								AutoDefer.rollback();
								AutoDefer.resetCBUStatus();
								updateStatic.proc();
							}
							historyTblOnLoad();
							setpfprogressbar();
							getprogressbarcolor();
						}else{
							//setTimeout(function(){
								modalFormSubmitRefreshDiv('cbuStatus','submitCbuStatus?cdrCbuPojo.cordID='+$j("#pkcordId").val()+'&pkcordId='+$j("#pkcordId").val(),'','','');
							//},0);
							
							if(!('<s:property value="#request.autoDeferRollbackStatus"/>'== 'false')){
								AutoDefer.rollback();
								AutoDefer.resetCBUStatus();
							}
							
							setTimeout(function(){
								refreshMultipleDivsOnViewClinical('1024','<s:property value="cdrCbuPojo.cordID"/>');
							},0);
							setTimeout(function(){
								loadMoredivs('getCbuHistory?pkcordId='+'<s:property value="cdrCbuPojo.cordID"/>'+'&iShowRows='+$j('#showsRowcbuhistorytbl').val(),'chistory,cReqhistory');
								historyTblOnLoad();
							},0);
						}
						
						$j.each(historyArr,function(i,val){
							var Index=val.charAt(val.length-1);
							$j("#icondiv"+Index).triggerHandler('click');
						});
						
					}
				}
			},
			"No": function() {
				$j( this ).dialog( "close" );
			}
		}
	});
}



var AutoDefer ={
		rollback:function(){
			
			if($j('#cbuStatusId').val()!== undefined && ($j('#cbuStatusId').val()==='OT' || $j('#cbuStatusId').val()=== 'DD')){
				
				getRequestedPage('rollbackAutoDefer?statusCode='+$j('#cbuStatusId').val()+'&cdrCbuPojo.cordID='+$j('#pkcordId').val());
				
				if($j('#executeAutoDeferFlagID').val()!='' && $j('#executeAutoDeferFlagID').val()!= undefined && $j('#executeAutoDeferFlagID').val()!=null)
				    $j('#'+$j('#executeAutoDeferFlagID').val()).val('false');							
			
			}
			
		},
		resetCBUStatus:function(){
			
			$j.get('getCbuStus', { cordId: $j('#pkcordId').val()} ,function( cbuStatus ) {
				
				if(cbuStatus){
			
					$j('#cordCbuStatus').val(cbuStatus);
					$j('[name="cordCbuStatus"]:last').val(cbuStatus);							
			   }											
		});
			
		}
};




function closeModal1(){
	 $j("#modelPopup2").dialog("close");
	 $j("#modelPopup").dialog("close");
	 $j("#modelPopup1").dialog("destroy");
	 $j("#modelPopup1").dialog("close");
	 $j("#modelPopup2").dialog("destroy");
	 $j("#modelPopup").dialog("destroy");
}
function conformcloseModal(){
	$j( "#dialog-confirm" ).dialog({
		resizable: false,
		modal: true,
		closeText: '',
		closeOnEscape: false ,
		buttons: {
			"Yes": function() {
				$j( "#dialog-confirm" ).dialog( "close" );
				var deferid = $j("#assessdeferId").val();
				if(deferid!="" && deferid!=null && deferid!="undefined"){
					$j('#'+deferid).removeAttr('checked');
				}
				 $j("#modelPopup1").dialog("destroy");				
			},
			"No": function() {
				$j("#dialog-confirm" ).dialog( "close" );
			}
		}
	});
}
function conformcloseModal1(){
	$j( "#dialog-confirm" ).dialog({
		resizable: false,
		modal: true,
		closeText: '',
		closeOnEscape: false ,
		buttons: {
			"Yes": function() {
				$j( "#dialog-confirm" ).dialog( "close" );
				var deferid = $j("#assessdeferId").val();
				if(deferid!="" && deferid!=null && deferid!="undefined"){
					$j('#'+deferid).removeAttr('checked');
				}
				$j( "#dialog-confirm" ).dialog( "close" );
				  closeModal1();
			},
			"No": function() {
				$j("#dialog-confirm" ).dialog( "close" );
			}
		}
	});
}

function closeModal2(){

	var value = $j("#noteAssessment").val();
	if(value.length==0)
		{
		$j("#modelPopup").dialog("close");
		 $j("#modelPopup1").dialog("close");
		 $j("#modelPopup2").dialog("close");
		 $j("#modelPopup").dialog("destroy");
		 $j("#modelPopup1").dialog("destroy");
		 $j("#modelPopup2").dialog("destroy");
		}
	else
		{
	 		$j("#modelPopup1").dialog("close");
			$j("#modelPopup1").dialog("destroy");
			$j("#modelPopup2").dialog("close");
			$j("#modelPopup2").dialog("destroy");
		}
}
function modalCordStatus(formid,url,divname){
	if($j("#"+formid).valid()){
		/*var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/><img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
		var processingSaveDialog = document.createElement('div');
		processingSaveDialog.innerHTML=progressMsg;		
		$j(processingSaveDialog).dialog({autoOpen: false,
			resizable: false, width:400, height:90,closeOnEscape: false,
			modal: true}).siblings('.ui-dialog-titlebar').remove();		
		$j(processingSaveDialog).dialog("open");	*/
		showprogressMgs();	
			$j.ajax({
		        type: "POST",
		        url: url,
		       // async:false,
		        data : $j("#"+formid).serialize(),
		        success: function (result){
		        	$j('.ui-datepicker').html("");
		        	//$('.tabledisplay').html("");
		            var $response=$j(result);
		            //query the jq object for the values
		            var errorpage = $response.find('#errorForm').html();
		           // alert(oneval);
		           // var subval = $response.find('#sub').text();
		            if(errorpage != null &&  errorpage != "" ){
		            	$j('#main').html(result);
		            }else{
		            	$j("#"+divname).html(result);
		            	$j("#update").css('display','block');
		            	//$j("#status").css('display','none');
		            }
		        },
		        error: function (request, status, error) {
		        	closeprogressMsg();
		        	alert("Error " + error);
		            alert(request.responseText);
		        }

			});	
    }
	closeprogressMsg();
}

function setOrdersEmpty(){
	var orderId = $j("#orderId").val();
	var orderType = $j("#orderType").val();

	if(typeof(orderId)=='undefined'){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")!=-1){
		$j("#orderId").val("");
		orderId="";
	}
	
	if(typeof(orderType)=='undefined'){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")!=-1){
		$j("#orderType").val("");
		orderType="";
	}
}
</script>
<div id="cbuinfoDiv" class='tabledisplay' >
<s:form id="cbuStatus">

<s:hidden id="cbuStatusId" name="cdrCbuPojo.cordCbuStatuscode"></s:hidden>
<s:hidden id="autoDeferRollbackStatus" name="#request.autoDeferRollbackStatus"></s:hidden>

<s:hidden name="licenceUpdate" value="True"></s:hidden>
<s:hidden name="cdrCbuPojo.cordID" id="pkcordId"/>
<s:hidden name="cdrCbuPojo.fkSpecimenId" />
<s:hidden name="cdrCbuPojo.specimen.pkSpecimen" />
<s:hidden name="cdrCbuPojo.fkCordCbuStatus" id="cbustatus"/>
<s:hidden name="orderPojo.pk_OrderId" id="orderId"></s:hidden>
<s:hidden name="orderType" id="orderType"></s:hidden>
<s:hidden name="applyResolFlag" id="applyResolFlag"></s:hidden>
<s:hidden name="assessmentstatus" id="assessmentstatus"></s:hidden>
<s:hidden name="shrtName" id="shrtName"/>
<s:hidden name="resetFun" id="resetFun"/>
<s:hidden name="iscordavailfornmdp" id="iscordavailfornmdpId"></s:hidden>
<s:hidden name="cordAcceptanceId" id="cordAcceptId" value="%{#request.cordAcceptanceId}"></s:hidden>
<s:hidden name="clinicalNotePojo.noteAssessment" id="noteAssessment"/>
<s:hidden name="deferId" id="assessdeferId"></s:hidden>
<div id="update" style="display: none;" >
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.message.modal.cbuStatus"/></strong></p>
			</div>
		</td>
	</tr>
	<tr>
		<td>
				<input type="button" onclick="closeModal2()" value="<s:text name="garuda.common.close"/>" />
		</td>
	
	</tr>
</table>	   
	</div>
	<div id="cordUpdate" style="display: none;" >
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="garuda.message.modal.cbuStatus" name="message"/>
		  </jsp:include>	   
	</div>
	
<div id="status" class="status">
  <table width="100%" cellspacing="0" cellpadding="0" border="0" >
	<tr>
		<td colspan="2">
			<s:text name="garuda.cbustatus.label.plsChooseapp" /><span class="error"><s:text name="garuda.label.dynamicform.astrik"></s:text> </span>
		</td>
	</tr>
	
	<tr id="availableCbuStatus">
		<s:if test="%{getpkOrders(cdrCbuPojo.cordID)==null || getpkOrders(cdrCbuPojo.cordID)=='' || getpkOrders(cdrCbuPojo.cordID)==0 ||cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode('RO')}">
		<s:if test="cbustatuslist!=null && cbustatuslist.size()>0">
			<s:iterator value="cbustatuslist" var="rowVal">
				<s:if test="%{#rowVal[2]=='AV'}">
					<td width="50%">
						<fieldset>
							<legend><s:text name="garuda.cbustatus.label.avail"/></legend>
								<input type="radio" name="cbuStatus" id="<s:property value='%{#rowVal[0]}'/>" value="<s:property value='%{#rowVal[0]}'/>" onclick="cleardate();">
								<label id="<s:property value='%{#rowVal[0]}'/>_label"><s:property value="%{#rowVal[1]}"/> (<s:property value='%{#rowVal[2]}'/>)</label>
						</fieldset>
					</td>
				</s:if>
			</s:iterator>
		</s:if>
		</s:if>
		<td width="50%">
			<fieldset style="background: #ffffff;">
				<s:text name="garuda.cbustatus.label.note.bysellocstatus" />
			</fieldset>
		</td>
	</tr>
	
	<tr id="tempAvailableCbuStatus">
		<td colspan="2">
	<s:if test="cbustatuslist!=null && cbustatuslist.size()>0">
		<s:iterator value="cbustatuslist" var="rowVal">
			<s:if test="%{#rowVal[2]=='RO'}">
			<fieldset>
				<legend><s:text name="garuda.cbustatus.label.active" /></legend>
				<table>
					<tr>
						<td><span id="cordstatus" style="display:none"><s:property value='%{#rowVal[2]}'/></span>
							<input type="radio" name="cbuStatus" id="<s:property value='%{#rowVal[0]}'/>" value="<s:property value='%{#rowVal[0]}'/>" onclick="checkdate();">
							<label id="<s:property value='%{#rowVal[0]}'/>_label"><s:property value="%{#rowVal[1]}"/> (<s:property value='%{#rowVal[2]}'/>)</label>
						</td>
						<td style="display: none;"><s:text name="garuda.cbustatus.label.availDate"/>:<span class="error">*</span><br/>
							<s:date name="cdrCbuPojo.cordavaildate" id="cordstatusdatepicker1" format="MMM dd, yyyy" />
        					<s:textfield readonly="true" onkeydown="cancelBack();"  name="cdrCbuPojo.cordavaildatestr" class="datepic" id="cordstatusdatepicker1" value="%{cordstatusdatepicker1}" cssClass="datePicWMinDate"/>
							<span id="errordate" class="error"></span>
						</td>
					</tr>
				</table>
			</fieldset>
			</s:if>
		</s:iterator>
	</s:if>
		</td>
	</tr>
	<tr id="notAvailableCbuStatus">
		<td colspan="2">
			<fieldset>
				<legend><s:text name="garuda.cbustatus.label.notavailable" /></legend>
				<table>
					<tr>
						<td id="notavail">
						<s:if test="orderType=='CT'">
						<s:if test="cbustatuslist!=null && cbustatuslist.size()>0">
								<s:iterator value="cbustatuslist" var="rowVal">
								<s:if test="%{#rowVal[2]!='AV'}">
								<s:if test="%{#rowVal[2]!='RSO'}">
								<s:if test="%{#rowVal[2]!='RO'}">
								<s:if test="%{#rowVal[2]!='RSN'}">
								<s:if test="%{#rowVal[2]!='RCT'}">
								<s:if test="%{#rowVal[2]!='RHE'}">
								<s:if test="%{#rowVal[2]!='SH'}">
								<s:if test="%{#rowVal[2]!='CB'}">
									<input type="radio" name="cbuStatus" id="<s:property value='%{#rowVal[0]}'/>" value="<s:property value='%{#rowVal[0]}'/>" onclick="cleardate();">
									<label id="<s:property value='%{#rowVal[0]}'/>_label"><s:property value="%{#rowVal[1]}"/> (<s:property value='%{#rowVal[2]}'/>)</label>
								</s:if>
								</s:if>
								</s:if>
								</s:if>
								</s:if>
								</s:if>
								</s:if>
								</s:if>
							</s:iterator>
						</s:if>
						</s:if>
						<s:else>
						<s:if test="cbustatuslist!=null && cbustatuslist.size()>0">
							<s:iterator value="cbustatuslist" var="rowVal">
								<s:if test="%{#rowVal[2]!='AV'}">
								<s:if test="%{#rowVal[2]!='RSO'}">
								<s:if test="%{#rowVal[2]!='RO'}">
								<s:if test="%{#rowVal[2]!='RSN'}">
								<s:if test="%{#rowVal[2]!='AG'}">
								<s:if test="%{#rowVal[2]!='RCT'}">
								<s:if test="%{#rowVal[2]!='RHE'}">
								<s:if test="%{#rowVal[2]!='SH'}">
								<s:if test="%{#rowVal[2]!='CB'}">
									<input type="radio" name="cbuStatus" id="<s:property value='%{#rowVal[0]}'/>" value="<s:property value='%{#rowVal[0]}'/>" onclick="cleardate();">
									<label id="<s:property value='%{#rowVal[0]}'/>_label"><s:property value="%{#rowVal[1]}"/> (<s:property value='%{#rowVal[2]}'/>)</label>
								</s:if>
								</s:if>
								</s:if>
								</s:if>
								</s:if>
								</s:if>
								</s:if>
								</s:if>
								</s:if>
							</s:iterator>
						</s:if>
						</s:else>
						</td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
</table>
<div id="showerrormsg" style="display: none;">
<span id="errormsg" class="error"></span>
</div>
<table bgcolor="#cccccc" style="padding-top: 5px;">
	<tr bgcolor="#cccccc" valign="baseline">
	<td width="70%">
          <s:if test="#request.module!=null && #request.module=='notes'">
			   <jsp:include page="../cb_esignature1.jsp" flush="true">
	   					<jsp:param value="cbuStatussubmit" name="submitId"/>
						<jsp:param value="cbuNotesStatusinvalid" name="invalid" />
						<jsp:param value="cbuNotesStatusminimum" name="minimum" />
						<jsp:param value="cbuNotesStatuspass" name="pass" />
   			 </jsp:include>
          </s:if>
          <s:else>
              <jsp:include page="../cb_esignature.jsp" flush="true">
 					<jsp:param value="cbuStatussubmit" name="submitId"/>
					<jsp:param value="cbuStatusinvalid" name="invalid" />
					<jsp:param value="cbuStatusminimum" name="minimum" />
					<jsp:param value="cbuStatuspass" name="pass" />
			 </jsp:include>
          </s:else>
   	</td>
    <td width="15%" align="right">
    <input type="button" id="cbuStatussubmit" disabled= "disabled" onclick="validateStatus()" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
    </td>
    <s:if test="clinicalNotePojo!=null && clinicalNotePojo.noteAssessment!=null">
    <td width="15%"><input type="button"  onclick="conformcloseModal1()" value="<s:text name="garuda.unitreport.label.button.cancel"/>" /></td>
    </s:if>
    <s:else>
    <td width="15%"><input type="button"  onclick="conformcloseModal()" value="<s:text name="garuda.unitreport.label.button.cancel"/>" /></td>
    </s:else>
    </tr>	
  </table></div>
</s:form>

<div id="dialog-confirm" style="display: none;">
	<s:text name="garuda.cbu.cbustatus.confirmmessagecbu1"/>
		<s:property value="cdrCbuPojo.registryId"/>
		<s:text name="garuda.cbu.cbustatus.cancelmessagecbu2"/>
	
</div>
<div id="dialog-confirm-1" style="display: none;">
	<s:text name="garuda.cbu.cbustatus.confirmmessage1"/> <b><span id="statusreason"></span></b>. <s:text name="garuda.cbu.cbustatus.confirmmessage2"/> <b><span id="cnfmstatus1"></span></b>. <s:text name="garuda.cbu.cbustatus.confirmmessage3"/>
</div>

</div>