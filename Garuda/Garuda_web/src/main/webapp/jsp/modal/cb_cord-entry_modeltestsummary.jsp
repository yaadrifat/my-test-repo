<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.eres.web.site.SiteJB" %>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@ page import="java.util.List,com.velos.eres.service.util.*"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<style type="text/css">
div#added1Div {
     overflow-x:auto;  
     margin-left:15em;
     overflow-y:hidden;
     border-left: .1em solid #333333;
        
}

.headcol {
    position:absolute;
    width:15em;
    margin-left:.2em;
    left:0;
    top:auto;
    text-align:center;
    vertical-align:top;
    border-left: 0em solid #333333;
    border-right: 0em solid #333333;
    border-top:.1em solid #333333;/* only relevant for first row*/
	border-bottom-width:0px;
    margin-top:0em; /*compensate for top border*/
}
</style>
<SCRIPT language="javascript">
var calcCbuNbrcFlag=true;
function validate(){
	$j("#labtestupdateform").valid();
}

function autoDeferModalFormSubmitRefreshDiv(formid,url,divname,updateDivId,statusDivId){
	//if($j("#"+formid).valid()){	
		showprogressMgs();
			$j.ajax({
		        type: "POST",
		        url: url,
		        async:false,
		        data : $j("#"+formid).serialize(),
		        success: function (result){
		        	$j('.ui-datepicker').html("");		        
		            var $response=$j(result);		           
		            var errorpage = $response.find('#errorForm').html();		           
		            if(errorpage != null &&  errorpage != "" ){
		            	$j('#main').html(result);
		            }else{
		            	if(divname!=""){ 
			            	 $j("#"+divname).html(result);
			            }
		            	if(updateDivId!=undefined && updateDivId!=""){
		            		$j("#"+updateDivId).css('display','block');			            	
		            	}else{
		            		$j("#update").css('display','block');			            	
		            	}
		            	if(statusDivId!=undefined && statusDivId!=""){
		            		$j("#"+statusDivId).css('display','none');			            	
		            	}else{
		            		$j("#status").css('display','none');			            	
		            	}		            	
		            }
		        },
		        error: function (request, status, error) {
		        	closeprogressMsg();
		        	alert("Error " + error);
		            alert(request.responseText);
		        }

			});	
			closeprogressMsg();
    //}	
}


function showDescField(divId,compValue,fieldval,fieldId){
	if(compValue==fieldval){
		$j("#"+divId).show();
}else{
	$j("#"+divId).hide();
	$j("#"+divId).children().each(function(n, i) {
		  var id = this.id;
		  if(id.indexOf('reasonTestDesc') != -1 && id.indexOf('reasonTestDesc_error') == -1){
				$j("#"+id).val('');
		  }
		  if(id.indexOf('testMthdDesc') != -1 && id.indexOf('testMthdDesc_error') == -1){
				$j("#"+id).val('');
		  }
		});

}
	if(fieldval==-1 || fieldval==''){
		$j("#"+fieldId+"_error").show();
		$j("#"+fieldId+"_error").text("<s:text name="garuda.common.validation.value"></s:text>");
	}
	else{
		$j("#"+fieldId+"_error").hide();
	}
}
function chekDepandtFldPrs(fieldId,fieldval){
	if(fieldval==null || fieldval==''){
		$j("#"+fieldId+"_error").show();
	}
	else{
		$j("#"+fieldId+"_error").hide();
	}
}
function addTestDate(value){
	//alert($j("#datepicker4").val());
	if($j("#preTestDateStrnM").val()=="" || autoFilled==true){
		autoFilled= true;
		$j("#preTestDateStrnM").val(value);
	  }
	if($j("#preTestDateStrnM").val()!=""){
		$j(".placeHolderOfid_preTestDateStrnM").hide();
	}else{
		$j(".placeHolderOfid_preTestDateStrnM").hide();
	}
	 }
	function addTestDateScnd(value){
	if($j("#postTestDateStrnM").val()=="" || autoFillee==true){
		autoFillee= true;
		$j("#postTestDateStrnM").val(value);
	}
	if($j("#postTestDateStrnM").val()!=""){
		$j(".placeHolderOfid_postTestDateStrnM").hide();
	}else{
		$j(".placeHolderOfid_postTestDateStrnM").hide();
	}
	}

function loadDivWithFormSubmitDefer(url,divname,formId){
	$j('#progress-indicator').css( 'display', 'block' );  
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).html(result);
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('#progress-indicator').css( 'display', 'none' );	

}

function closeModalTest(divId){ 
	 $j("#"+divId).dialog("close");
	 $j("#"+divId).dialog("close");
	 $j("#"+divId).dialog("destroy");
	 $j("#"+divId).dialog("destroy");
	 $j("#modelPopup1").dialog("close");
	 $j("#modelPopup").dialog("close");
	 $j("#modelPopup1").dialog("destroy");
	 $j("#modelPopup").dialog("destroy");
}

function descValidation(Id){
	 //alert($j("#"+Id).val());
	 }

 $j(function(){
	   jQuery.validator.addMethod("checkSelectCordEntry", function(value, element,params) {
		   if($j("#"+params[0]).val()==value || ($j("#"+params[0]).val()=="" && (parseInt(value) == -1))){
          return true;
			}else{
				return (true && (parseInt(value) != -1));
			}			
		}, "<%=LC.L_PlsSel_Val%>");/*}, "Please Select Value");*****/
		
		jQuery.validator.addMethod("validateRequiredCordEntry", function(value, element,params) {
	        var flag = true;
	        if($j("#"+params[0]).val()==value){
	        	return true;
	        }else{
	        	if(value==""){
					flag =false;
				}
			}
			return (true && flag);
		}, "<%=MC.M_PlsEtr_IsbtData%>");/*}, "Please Enter Valid ISBT Data");*****/			
		
		<%--
		jQuery.validator.addMethod("checkDatesCordEntry", function(value, element, params) {
			if($j("#"+params[2]).val()==value){
				return true;				
			}else{
				return (true && (checkDates(params[0],params[1])));
			}			 		 
		}, "<%=MC.M_EtrVal_XxxxFrmt%>");/*}, "Please Check The Date You Entered");*****/

		jQuery.validator.addMethod("checkTimeFormatCordEntry", function(value, element,params) {
			if($j("#"+params[0]).val()==value){
				return true;				
			}else{
				if(value==""){
					return true;
				}else{
				 return (true && (dateTimeFormat(value)));		 
				}
			}
		}, "<%=MC.M_EtrVal_XxxxFrmt%>");/*}, "Please Enter Time format hh:mm ");*****/

		jQuery.validator.addMethod("rangeLabsummary", function(value, element,params) {
			if($j("#"+params[2]).val()==value){
				return true;				
			}else{
				if(value==""){
					return true;
				}else{
				 return (true && value >= params[0] && value <= params[1] );		 
				}
			}
		}, "Enter Correct Range Data");--%> 
		$j("input").bind({keydown:function(e) {
			 var key = e.charCode || e.keyCode;
			 if(key==9){
				 calcCbuNbrcFlag=false;
			 }
			 else{
				 calcCbuNbrcFlag=true;
			 }
		}});
	});  

 function vidSaved(id,value){
	if(value!=null && value!="" && value!="undefined"){
	   $j("#"+id).show();
	}
	else{
		$j("#"+id).hide();
		$j("#"+id).children().each(function(n, i) {
			  var idParent = this.id;
			 if((idParent.indexOf('fktestreason') != -1 || idParent.indexOf('fktestspecimen') != -1 || idParent.indexOf('fktestmethod') != -1)&& 
					  (idParent.indexOf('fktestreason_error') == -1 || idParent.indexOf('fktestspecimen_error') == -1 || idParent.indexOf('fktestmethod_error') == -1)){
					$j("#"+idParent+" option[value='-1']").attr("selected", "selected");
			  }
			 if(idParent.indexOf('reasonFrTestDescOthTm')!= -1 || idParent.indexOf('testMethDescOthTm')!= -1 ){
					$j("#"+idParent).children().each(function(n, i) {
						var idChild = this.id;
					  	if((idChild.indexOf('reasonTestDesc') != -1 || idChild.indexOf('testMthdDesc') != -1 )&& 
								  (idChild.indexOf('reasonTestDesc_error') == -1  || idChild.indexOf('testMthdDesc_error') == -1)){
								$j("#"+idChild).val('');
						  }
					});
					$j("#"+idParent).hide();
			 }
	 });
	}
 }

function vid(num,value){
if(value!=null && value!="" && value!="undefined"){
$j("#showDropDown"+num).show();}
else
	{
	$j("#showDropDown"+num).hide();

	$j("#showDropDown"+num).children().each(function(n, i) {
		  var id = this.id;
		 if((id.indexOf('fktestreason') != -1 || id.indexOf('fktestspecimen') != -1 || id.indexOf('fktestmethod') != -1)&& 
				  (id.indexOf('fktestreason_error') == -1 || id.indexOf('fktestspecimen_error') == -1 || id.indexOf('fktestmethod_error') == -1)){
				$j("#"+id+" option[value='-1']").attr("selected", "selected");
		  }
		 if(id.indexOf('reasonFrTestDescOthTm')!= -1 || id.indexOf('testMethDescOthTm')!= -1 ){
				$j("#"+id).children().each(function(n, i) {
					var idChild = this.id;
				  	if((idChild.indexOf('reasonTestDesc') != -1 || idChild.indexOf('testMthdDesc') != -1 )&& 
							  (idChild.indexOf('reasonTestDesc_error') == -1  || idChild.indexOf('testMthdDesc_error') == -1)){
							$j("#"+idChild).val('');
					  }
				});
				$j("#"+id).hide();
		 }
 });
	   




	
	}
	//alert("Still Wating ! Is That you ");
}


function showDataModal(title,url,height,width)
{
	$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/> <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#modelPopup2").html(progressMsg);
		$j("#modelPopup2").dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#modelPopup2").dialog("destroy");
				    }
				   }
				  ); 
		$j("#modelPopup2").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#modelPopup2").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
}
function autoDeferViab(Val,Id){

	 elementValue = autoDeferField_2;
	 var url="saveTestScreen?cordId="+'<s:property value="cdrCbuPojo.cordID"/>';
	 if(Val !='' && Val>=0 && Val<85 ){
	//alert("ID"+Id);alert($j("#"+Id).val());
		jConfirm('Response you entered would cause CBU to be automatically status as permanently medically deferred. Please confirm if you would like to proceed', '<s:text name="garuda.common.lable.confirm"/>',
			function(r) {
			 if (r == false) { 
				  // $j("#"+Id).val("");
				  $j('input[name="savePostTestList[13].testresult"]').val(elementValue);
				  $j('input[name="savePostTestList[13].testresult"]').focus();
				  $j('#'+Id).removeClass('labChanges');
				  // $j('#'+Id).focus();
			   }else if(r == true){
				      autoDefer = true;
					  mutex_Lock = true;
					  element_Id ="";
					  element_Name = "savePostTestList[13].testresult";
					  autoDeferFormSubmitParam[0]= 'labtestupdateform';
					  autoDeferFormSubmitParam[1]= url;
					  autoDeferFormSubmitParam[2]= '';
					  autoDeferFormSubmitParam[3]= 'updateModalLab';
					  autoDeferFormSubmitParam[4]= 'statusModalLab';
					  refresh_DivNo = 1536;
					  refresh_CordID = '<s:property value="cdrCbuPojo.cordID"/>';
					  commonMethodForSaveAutoDefer();
					  refreshDiv_Id[0]= 'status1';
				    // autoDeferModalFormSubmitRefreshDiv('labtestupdateform',url,'','updateModalLab','statusModalLab');
			        // refreshMultipleDivsOnViewClinical('1536','<s:property value="cdrCbuPojo.cordID"/>');
			         // $j("#status1").css('display','none');
				 }
			});
		  }
	  }

function autoDeferCFU(Val,Id){
	elementValue = autoDeferField_1;
	 var url="saveTestScreen?cordId="+'<s:property value="cdrCbuPojo.cordID"/>';
	 if( Val !='' && Val==0){
		//alert("ID"+Id);alert($j("#"+Id).val());
		jConfirm('Response you entered would cause CBU to be automatically status as permanently medically deferred. Please confirm if you would like to proceed','<s:text name="garuda.common.lable.confirm"/>',
			function(r) {
				if (r == false) {
				   //$j("#"+Id).val("");
				   $j('input[name="savePostTestList[12].testresult"]').val(elementValue);
				   $j('input[name="savePostTestList[12].testresult"]').focus();
				   $j('#'+Id).focus();
				   $j('#'+Id).removeClass('labChanges');
				   
				  }else if(r == true){
					  autoDefer = true;
					  mutex_Lock = true;
					  element_Id = "";
					  element_Name = "savePostTestList[12].testresult";
					  autoDeferFormSubmitParam[0]= 'labtestupdateform';
					  autoDeferFormSubmitParam[1]= url;
					  autoDeferFormSubmitParam[2]= 'loadLabSummaryDiv';
					  autoDeferFormSubmitParam[3]= 'updateModalLab';
					  autoDeferFormSubmitParam[4]= 'statusModalLab';
					  refresh_DivNo = 1536;
					  refresh_CordID = '<s:property value="cdrCbuPojo.cordID"/>';
					  refreshDiv_Id[0]= 'status1';
					  commonMethodForSaveAutoDefer();					  
					  //autoDeferModalFormSubmitRefreshDiv('labtestupdateform',url,'loadLabSummaryDiv','updateModalLab','statusModalLab');
				      //refreshMultipleDivsOnViewClinical('1536','<s:property value="cdrCbuPojo.cordID"/>');
				     // $j("#status1").css('display','none');
				  }
			});
		  }
	  }
	function addTest(cordId,cbuOtherTestFlag){
		var id="datepickerbirthlabModal1";
		var msg="<s:text name='garuda.message.modal.colldatecantbecurdate'/>";
		/* var compCurNCordDate=checkCurrentDateCordDate(id,msg);
		if(compCurNCordDate){ */
				var headerLnght = $j("#added1 thead th").length;
		  		//alert(headerLnght);
		  		var preTstDate =document.getElementById("preTestDateStrnM").value;
		  		var postTstDate =document.getElementById("postTestDateStrnM").value;
		  		var postTstThawDate = new Array();
		  		//postTstThawDate=null;
		  		var counter=0;
		  		for(counter=0; counter<headerLnght-3 ; counter++){
		  			 var value1 = counter+1;
		  			 //var temp = document.getElementById("otherTestDateStrn"+value1).value;
		  		var val1 =$j("#otherTestDateStrnM"+value1).val();
		  		postTstThawDate[counter] = val1.replace(",",":");
		  		//otherLabTstsLstId=$j("#otherLabTstsLstId").val();
		  		//alert(otherLabTstsLstId);
		  		 /* postTstThawDate[1] =document.getElementById("otherTestDateStrn2").value;
		  		alert(postTstThawDate[1]);
		  		 postTstThawDate[2] =document.getElementById("otherTestDateStrn3").value;
		  		alert(postTstThawDate[2]);
		  		 postTstThawDate[3] =document.getElementById("otherTestDateStrn4").value;
		  		alert(postTstThawDate[3]);
		  		 postTstThawDate[4] =document.getElementById("otherTestDateStrn5").value;
		  		alert(postTstThawDate[4]);
		  		 postTstThawDate[5] =document.getElementById("otherTestDateStrn6").value;
		  		alert(postTstThawDate[5]); */
		  		//counter++;
		  		}
		  		var specCollDate = $j("#datepickerbirthlabModal2").val();
	  			showModalWithData('<s:text name="garuda.cdrcbuview.label.title.addNewTest" /> <s:property value="cdrCbuPojo.registryId" />, <s:text name="garuda.cbuentry.label.idbag"/>:<s:property value="cdrCbuPojo.numberOnCbuBag" />','addNewTest?cordId='+cordId+'&specCollDate='+specCollDate+'&specimenId=<s:property value="cdrCbuPojo.fkSpecimenId"/>&cbuOtherTestFlag='+cbuOtherTestFlag+'&headerLnght='+headerLnght+'&preTstDate='+preTstDate+'&postTstDate='+postTstDate+'&postTstThawDate='+postTstThawDate,'500','800','labtestupdateform','modelPopup2');
		/* } */
	}

	function showTest(fkSpeciId){
		var id="datepickerbirthlabModal1";
		var msg="<s:text name='garuda.message.modal.colldatecantbecurdate'/>";
		/* var compCurNCordDate=checkCurrentDateCordDate(id,msg);
		if(compCurNCordDate){ */
		document.getElementById('fkSpecimenId').value=fkSpeciId;
		//document.getElementById('fktestid').value=fktestid[2];
	    // alert("hiiii :"+fkSpeciId);
	    //  alert("hello >>>>>>>>>>>>>>>>>>>: "+fktestid[2]);
	        $j("#dialog").dialog({closeText: ''});
	        //$jmyWindow.show();
	       // $jmyWindow.dialog("open");
	       	$jmyWindow=jQuery("#dialog");
	         $jmyWindow.show();
	         $j('.removeFocus').datepicker( "hide" );
	         $j('#datepicker91').blur().change().focusout();
	         $j('#addtestButn').focus();
	        // $j("#dialog").css('display','none');
	         //$jmyWindow.hide();

	        // $j('#dialog').dialog('close');
		/* } */
	}
function checkRangeMthd(fieldId,fkTestId,fieldVal,prePostOthFlag){
	var preFlag="pre";
	var postFlag="post";
	var othThawFlag="otherThaw";
	var txtFldClsValueMand="onChngMand";
	var txtFldClsValRangChk="onChngRangeChk";
	var txtFldCls="";
	var chkMinVal=null;
	var chkMaxVal=null;
	var dispMsg1="";
	var dbStoredFieldVal=$j("#"+fieldId+"_dbStoredVal").val();
	var cbuAbsFldId="";
	var cbuPrcntFldId="";
	var tbncCntFldId="";
	var calcValucNbrcAbs="";
	if(prePostOthFlag==postFlag){
		cbuAbsFldId="savePostTestList10testresult";
		cbuPrcntFldId="savePostTestList11testresult";
		tbncCntFldId="savePostTestList1testresult";
	}
	var CBU_volume_test = document.getElementById('CBU_VOL').value;
	var Ttl_CBU_Ncltd_cnt = document.getElementById('TCNCC').value;
	var Ttl_CBU_Ncltd_cnt_uncrtd = document.getElementById('UNCRCT').value;
	var Ttl_CBU_Ncltd_cnt_unkif_uncrtd = document.getElementById('TCBUUN').value;
	if(fkTestId==Ttl_CBU_Ncltd_cnt || fkTestId==Ttl_CBU_Ncltd_cnt_uncrtd || fkTestId==Ttl_CBU_Ncltd_cnt_unkif_uncrtd){
		chkMinVal=12;
		chkMaxVal=999;
		dispMsg1="<s:text name="garuda.common.range.testval12to999"/>";
	}
	var CBU_Ncltd_Cell_Cncton = document.getElementById('CBUNCCC').value;
	if(fkTestId==CBU_Ncltd_Cell_Cncton){
		chkMinVal=5000;
		chkMaxVal=200000;
		dispMsg1="<s:text name="garuda.common.range.testval5000to200000"/>";
	}
	if(fkTestId==CBU_Ncltd_Cell_Cncton && prePostOthFlag==preFlag){
		chkMinVal=1000;
		chkMaxVal=50000;
		dispMsg1="<s:text name="garuda.common.range.testval1000to50000"/>";
	}
	var Final_Pdct_Vol = document.getElementById('FNPV').value;
	if(fkTestId==Final_Pdct_Vol){
		chkMinVal=10;
		chkMaxVal=600;
		dispMsg1="<s:text name="garuda.common.range.testval10to600"/>";
	}
	var Ttl_CD34_Cel_Cnt = document.getElementById('TCDAD').value;
	if(fkTestId==Ttl_CD34_Cel_Cnt){
		chkMinVal=0.0;
		chkMaxVal=999.9;
		dispMsg1="<s:text name="garuda.common.range.val0to999"/>";
	}
	var prcnt_of_CD34_Vab = document.getElementById('PERCD').value;
	if(fkTestId==prcnt_of_CD34_Vab){
		chkMinVal=0.0;
		chkMaxVal=100;
		dispMsg1="<s:text name="garuda.common.range.val0to100"/>";
	}
	var prcnt_of_CD34_ttl_Monclr_Cl_Cnt = document.getElementById('PERTMON').value;
	if(fkTestId==prcnt_of_CD34_ttl_Monclr_Cl_Cnt){
		chkMinVal=0.0;
		chkMaxVal=10.0;
		dispMsg1="<s:text name="garuda.common.range.testrange0to10"/>";
	}
	var prcnt_of_Monclr_Cls_Ttl_Ncltd_Cnt = document.getElementById('PERTNUC').value;
	if(fkTestId==prcnt_of_Monclr_Cls_Ttl_Ncltd_Cnt){
		chkMinVal=20.0;
		chkMaxVal=99.9;
		dispMsg1="<s:text name="garuda.common.range.val20to99"/>";
	}
	var Ttl_CD3_Cel_Cnt = document.getElementById('TOTCD').value;
	if(fkTestId==Ttl_CD3_Cel_Cnt){
		chkMinVal=0;
		chkMaxVal=999.9;
		dispMsg1="<s:text name="garuda.common.range.val0to999"/>";
	}
	var prcnt_CD3_Cls_Ttl_Monclr_Cl_Cnt = document.getElementById('PERCD3').value;
	if(fkTestId==prcnt_CD3_Cls_Ttl_Monclr_Cl_Cnt){
		chkMinVal=0;
		chkMaxVal=99.9;
		dispMsg1="<s:text name="garuda.common.range.val0to99"/>";
	}
	var CBU_nRBC_Abslt_No = document.getElementById('CNRBC').value;
	if(fkTestId==CBU_nRBC_Abslt_No){
		chkMinVal=0;
		chkMaxVal=9999;
		dispMsg1="<s:text name="garuda.common.range.val0to9999"/>";
	}
	var CBU_nRBC_prcnt = document.getElementById('PERNRBC').value;
	if(fkTestId==CBU_nRBC_prcnt){
		chkMinVal=0;
		chkMaxVal=100;
		dispMsg1="<s:text name="garuda.common.range.val0to100"/>";
	}
	var CFU_Cnt_test = document.getElementById('CFUCNT').value;
	if(fkTestId==CFU_Cnt_test){
		chkMinVal=0;
		chkMaxVal=999;
		dispMsg1="<s:text name="garuda.common.range.testrange0to999"/>";
	}
	var Viability_test = document.getElementById('VIAB').value;
	if(fkTestId==Viability_test){
		chkMinVal=0;
		chkMaxVal=100;
		dispMsg1="<s:text name="garuda.common.range.val0to100"/>";
	}
	/* var NAT_HIV_test = document.getElementById('NATHIV').value;
	var NAT_HCV_test = document.getElementById('NATHCV').value;
	if(fkTestId==NAT_HIV_test || fkTestId==NAT_HCV_test){
		chkMinVal=0;
		chkMaxVal=0;
		dispMsg1="";
	} */
	switch(fkTestId){
	case CBU_volume_test: 
			if((fieldVal>=40 && fieldVal<=500) || (fieldVal==dbStoredFieldVal)){
				$j("#"+fieldId+"_error").hide();
				$j("#"+fieldId).removeClass(txtFldClsValueMand);
				$j("#"+fieldId).removeClass(txtFldClsValRangChk);
			}
			else{
				$j("#"+fieldId+"_error").show();
				if(prePostOthFlag==preFlag)
					{
						if(fieldVal==null || fieldVal==''){
							txtFldCls=txtFldClsValueMand;
							$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
						}
						else{
							txtFldCls=txtFldClsValRangChk;
							$j("#"+fieldId+"_error").text("<s:text name="garuda.common.range.testval40to500"/>");
						}
				}
				else if(prePostOthFlag==postFlag){
					if(fieldVal==null || fieldVal==''){
						$j("#"+fieldId).removeClass(txtFldClsValueMand);
						$j("#"+fieldId).removeClass(txtFldClsValRangChk);
					}
					else{
						txtFldCls=txtFldClsValRangChk;
						$j("#"+fieldId+"_error").text("<s:text name="garuda.common.range.testval40to500"/>");
					}
				}
				else if(prePostOthFlag==othThawFlag){
					if(fieldVal==null || fieldVal==''){
						$j("#"+fieldId).removeClass(txtFldClsValueMand);
						$j("#"+fieldId).removeClass(txtFldClsValRangChk);
					}
					else{
						txtFldCls=txtFldClsValRangChk;
						$j("#"+fieldId+"_error").text("<s:text name="garuda.common.range.testval40to500"/>");
					}
				}
				$j("#"+fieldId).addClass(txtFldCls);
			}
			break;
	case Ttl_CBU_Ncltd_cnt:
	case Ttl_CBU_Ncltd_cnt_uncrtd: 
	case Ttl_CBU_Ncltd_cnt_unkif_uncrtd:
	case Ttl_CD34_Cel_Cnt: 
	case CBU_nRBC_Abslt_No: 
	case CBU_nRBC_prcnt: 
	case CFU_Cnt_test: 
	case Viability_test: 
		if((fieldVal!="" && fieldVal>=chkMinVal && fieldVal<=chkMaxVal) || (fieldVal==dbStoredFieldVal)){
			$j("#"+fieldId+"_error").hide();
			$j("#"+fieldId).removeClass(txtFldClsValueMand);
			$j("#"+fieldId).removeClass(txtFldClsValRangChk);
		}
		else{
			$j("#"+fieldId+"_error").show();
			if(prePostOthFlag==preFlag)
				{
					if(fieldVal==null || fieldVal==''){
						$j("#"+fieldId+"_error").hide();
						$j("#"+fieldId).removeClass(txtFldClsValueMand);
						$j("#"+fieldId).removeClass(txtFldClsValRangChk);
					}
					else{
						txtFldCls=txtFldClsValRangChk;
						$j("#"+fieldId+"_error").text(dispMsg1);
					}
			}
			else if(prePostOthFlag==postFlag){
				if(fieldVal==null || fieldVal==''){
					txtFldCls=txtFldClsValueMand;
					$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
				}
				else{
					txtFldCls=txtFldClsValRangChk;
					$j("#"+fieldId+"_error").text(dispMsg1);
				}
				
			}
			else if(prePostOthFlag==othThawFlag){
				if(fieldVal==null || fieldVal==''){
					$j("#"+fieldId+"_error").hide();
					$j("#"+fieldId).removeClass(txtFldClsValueMand);
					$j("#"+fieldId).removeClass(txtFldClsValRangChk);
				}
				else{
					txtFldCls=txtFldClsValRangChk;
					$j("#"+fieldId+"_error").text(dispMsg1);
				}
			}
			$j("#"+fieldId).addClass(txtFldCls);
		}
		break;
	case CBU_Ncltd_Cell_Cncton:
	case Final_Pdct_Vol:
	case prcnt_of_CD34_Vab: 
	case prcnt_of_CD34_ttl_Monclr_Cl_Cnt:
	case prcnt_of_Monclr_Cls_Ttl_Ncltd_Cnt:
	case Ttl_CD3_Cel_Cnt: 
	case prcnt_CD3_Cls_Ttl_Monclr_Cl_Cnt: 
		if((fieldVal>=chkMinVal && fieldVal<=chkMaxVal) || (fieldVal==dbStoredFieldVal)){
			$j("#"+fieldId+"_error").hide();
			$j("#"+fieldId).removeClass(txtFldClsValueMand);
			$j("#"+fieldId).removeClass(txtFldClsValRangChk);
		}
		else{
			$j("#"+fieldId+"_error").show();
				if(fieldVal==null || fieldVal==''){
					$j("#"+fieldId+"_error").hide();
					$j("#"+fieldId).removeClass(txtFldClsValueMand);
					$j("#"+fieldId).removeClass(txtFldClsValRangChk);
				}
				else{
					txtFldCls=txtFldClsValRangChk;
					$j("#"+fieldId+"_error").text(dispMsg1);
				}
				$j("#"+fieldId).addClass(txtFldCls);
		}
		break;
	/* case NAT_HIV_test: 
	case NAT_HCV_test: 
		$j("#"+fieldId+"_error").hide();
		break; */
	default:
		$j("#"+fieldId+"_error").hide();
		$j("#"+fieldId).removeClass(txtFldClsValueMand);
		$j("#"+fieldId).removeClass(txtFldClsValRangChk);
	}	
	if(prePostOthFlag==postFlag && calcCbuNbrcFlag==true){
	if( fkTestId==Ttl_CBU_Ncltd_cnt || fkTestId==Ttl_CBU_Ncltd_cnt_uncrtd || fkTestId==Ttl_CBU_Ncltd_cnt_unkif_uncrtd || fkTestId==CBU_nRBC_prcnt || fkTestId==CBU_nRBC_Abslt_No){
		if(fkTestId==Ttl_CBU_Ncltd_cnt || fkTestId==Ttl_CBU_Ncltd_cnt_uncrtd || fkTestId==Ttl_CBU_Ncltd_cnt_unkif_uncrtd ){
			if(fieldVal==null || fieldVal==''){
					$j("#"+cbuAbsFldId).val("");
					$j("#"+cbuPrcntFldId).val("");
			}
			else{
				if(($j("#"+cbuAbsFldId).val()!=null && $j("#"+cbuAbsFldId).val()!='') && ($j("#"+cbuPrcntFldId).val()==null || $j("#"+cbuPrcntFldId).val()=='')){
					calcValucNbrcAbs=($j("#"+cbuAbsFldId).val()/($j("#"+tbncCntFldId).val()*10))*100;
					$j("#"+cbuPrcntFldId).val(Math.round(calcValucNbrcAbs));
				}
				else if(($j("#"+cbuAbsFldId).val()==null && $j("#"+cbuAbsFldId).val()=='') && ($j("#"+cbuPrcntFldId).val()!=null || $j("#"+cbuPrcntFldId).val()!='')){
					calcValucNbrcAbs=($j("#"+cbuPrcntFldId).val()/100)*($j("#"+tbncCntFldId).val()*10);
					$j("#"+cbuAbsFldId).val(calcValucNbrcAbs);
				}
				else if(($j("#"+cbuAbsFldId).val()!=null && $j("#"+cbuAbsFldId).val()!='') && ($j("#"+cbuPrcntFldId).val()!=null || $j("#"+cbuPrcntFldId).val()!='')){
					$j("#"+cbuAbsFldId).val("");
					$j("#"+cbuPrcntFldId).val("");
				}
			}
		}
		else if(fkTestId==CBU_nRBC_prcnt){
				if(fieldVal==null || fieldVal==''){
					$j("#"+cbuAbsFldId).val("");
				}
				else if($j("#"+tbncCntFldId).val()!=null && $j("#"+tbncCntFldId).val()!=''){
					calcValucNbrcAbs=($j("#"+cbuPrcntFldId).val()/100)*($j("#"+tbncCntFldId).val()*10);
					if(calcValucNbrcAbs<0 || calcValucNbrcAbs>9999){
						$j("#"+cbuAbsFldId).val("");
						$j("#"+cbuPrcntFldId).val("");
						$j("#"+fieldId+"_error").show();
						$j("#"+fieldId+"_error").text("Please enter valid value");
					}
					else{
						$j("#"+cbuAbsFldId+"_error").hide();
						$j("#"+cbuAbsFldId).val(calcValucNbrcAbs);
					}
				}
				else if($j("#"+tbncCntFldId).val()==null || $j("#"+tbncCntFldId).val()==''){
					$j("#"+cbuPrcntFldId).val("");
					alert("Please Enter value for Total CBU Nucleated Cell Count");
				}
		}
		else if(fkTestId==CBU_nRBC_Abslt_No){
			if(fieldVal==null || fieldVal==''){
				$j("#"+cbuPrcntFldId).val("");
			}
			else if($j("#"+tbncCntFldId).val()!=null && $j("#"+tbncCntFldId).val()!=''){
				calcValucNbrcAbs=($j("#"+cbuAbsFldId).val()/($j("#"+tbncCntFldId).val()*10))*100;
				if(Math.round(calcValucNbrcAbs)<0 || Math.round(calcValucNbrcAbs)>100){
					$j("#"+cbuAbsFldId).val("");
					$j("#"+cbuPrcntFldId).val("");
					$j("#"+fieldId+"_error").show();
					$j("#"+fieldId+"_error").text("Please enter valid value");
				}
				else{
					$j("#"+cbuPrcntFldId+"_error").hide();
				$j("#"+cbuPrcntFldId).val(Math.round(calcValucNbrcAbs));
				}
			}
			else if($j("#"+tbncCntFldId).val()==null || $j("#"+tbncCntFldId).val()==''){
				$j("#"+cbuAbsFldId).val("");
				alert("Please Enter value for Total CBU Nucleated Cell Count");
			}
	}
	}}
}	  

function validateThawOtherRow(formId){
	var flag=true;
	var rangeFlag=true;
	var headerLnght = $j("#added1 thead th").length;
	var thawRow=headerLnght-3;
	var thawOtherRow=1;
	for(thawRow=3;thawRow<headerLnght;thawRow++){
		var n=0;
		$j("#added1 tr:gt(0)").each(function(){
			if(n>=$j("#noOfLabGrpTypTests").val()){
				var fieldId = 'savePreTestList'+n+'testresult';
				if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
				var tempVarbl=document.getElementById(fieldId).value;
				if(tempVarbl != null && tempVarbl !=''){
					$j("#"+fieldId+"_error").hide();
					var fieldfktestspecimenId = 'savePreTestList'+n+'fktestspecimen';
					var tempfktestspecimenVarbl=document.getElementById(fieldfktestspecimenId).value;
					if(tempfktestspecimenVarbl != null && tempfktestspecimenVarbl >0){
						$j("#"+fieldfktestspecimenId+"_error").hide();
					}
					else{
						$j("#"+fieldfktestspecimenId+"_error").show();
						$j("#"+fieldfktestspecimenId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
						flag=false;
					}
				}
				else{
					$j("#"+fieldId+"_error").show();
					$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
					flag=false;
				}
			}
				fieldId = 'savePostTestList'+n+'testresult';
				if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
				var tempVarbl=document.getElementById(fieldId).value;
				if(tempVarbl != null && tempVarbl !=''){
					$j("#"+fieldId+"_error").hide();
					var fieldfktestspecimenId = 'savePostTestList'+n+'fktestspecimen';
					var tempfktestspecimenVarbl=document.getElementById(fieldfktestspecimenId).value;
					if(tempfktestspecimenVarbl != null && tempfktestspecimenVarbl >0){
						$j("#"+fieldfktestspecimenId+"_error").hide();
					}
					else{
						$j("#"+fieldfktestspecimenId+"_error").show();
						$j("#"+fieldfktestspecimenId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
						flag=false;
					}
				}
				else{
					$j("#"+fieldId+"_error").show();
					$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
					flag=false;
				}
			}
				fieldId = 'saveOtherPostTestList'+thawOtherRow+n+'testresult';
				if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
				var tempVarbl=document.getElementById(fieldId).value;
				if(tempVarbl != null && tempVarbl !=''){
					$j("#"+fieldId+"_error").hide();
					var fieldfktestspecimenId = 'saveOtherPostTestList'+thawOtherRow+n+'fktestspecimen';
					var tempfktestspecimenVarbl=document.getElementById(fieldfktestspecimenId).value;
					if(tempfktestspecimenVarbl != null && tempfktestspecimenVarbl >0){
						$j("#"+fieldfktestspecimenId+"_error").hide();
					}
					else{
						$j("#"+fieldfktestspecimenId+"_error").show();
						$j("#"+fieldfktestspecimenId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
						flag=false;
					}
				}
				else{
					$j("#"+fieldId+"_error").show();
					$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
					flag=false;
				}
			}
				n++;
			}
			else{
			var fieldId = 'saveOtherPostTestList'+thawOtherRow+n+'testresult';
			if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
			var tempVarbl=document.getElementById(fieldId).value;
			if(tempVarbl != null && tempVarbl !=''){
				//var fieldName='saveOtherPostTestList'+thawOtherRow+'['+n+']'+'.fktestid';
				//var fkTestId=document.getElementsByName(fieldName);
				//rangeFlag = checkRangeMthd(fieldId,fkTestId,tempVarbl);
				var fieldfktestreasonId = 'saveOtherPostTestList'+thawOtherRow+n+'fktestreason';
				var tempfktestreasonVarbl=document.getElementById(fieldfktestreasonId).value;
				if(tempfktestreasonVarbl != null && tempfktestreasonVarbl >0){
					var tempvalueTestReasonOtherPkVal=document.getElementById('valueTestReasonOtherPkVal').value;
					if(tempfktestreasonVarbl==tempvalueTestReasonOtherPkVal){
						var fieldreasonTestDescId = 'saveOtherPostTestList'+thawOtherRow+n+'reasonTestDesc';
						var tempreasonTestDescVarbl=document.getElementById(fieldreasonTestDescId).value;
						if(tempreasonTestDescVarbl != null && tempreasonTestDescVarbl != ''){
							$j("#"+fieldreasonTestDescId+"_error").hide();
						}
						else{
							$j("#"+fieldreasonTestDescId+"_error").show();
							flag=false;
						}
					}
					$j("#"+fieldfktestreasonId+"_error").hide();
				}
				else{
					$j("#"+fieldfktestreasonId+"_error").show();
					flag=false;
				}
				var fieldfktestspecimenId = 'saveOtherPostTestList'+thawOtherRow+n+'fktestspecimen';
				var tempfktestspecimenVarbl=document.getElementById(fieldfktestspecimenId).value;
				if(tempfktestspecimenVarbl != null && tempfktestspecimenVarbl >0){
					$j("#"+fieldfktestspecimenId+"_error").hide();
				}
				else{
					$j("#"+fieldfktestspecimenId+"_error").show();
					flag=false;
				}
				var fieldfktestmethodId = 'saveOtherPostTestList'+thawOtherRow+n+'fktestmethod';
				if(document.getElementById(fieldfktestmethodId)!=null && document.getElementById(fieldfktestmethodId)!='undefined'){
				var tempfktestmethodVarbl=document.getElementById(fieldfktestmethodId).value;
				if(tempfktestmethodVarbl != null && tempfktestmethodVarbl >0){
					var tempValueCfuOtherPkVal=document.getElementById('valueCfuOtherPkVal').value;
					var tempValueViablityOtherPkVal=document.getElementById('valueViablityOtherPkVal').value;
					if(tempfktestmethodVarbl==tempValueCfuOtherPkVal || tempfktestmethodVarbl==tempValueViablityOtherPkVal){
						var fieldtestMthdDescId = 'saveOtherPostTestList'+thawOtherRow+n+'testMthdDesc';
						var temptestMthdDescVarbl=document.getElementById(fieldtestMthdDescId).value;
						if(temptestMthdDescVarbl != null && temptestMthdDescVarbl != ''){
							$j("#"+fieldtestMthdDescId+"_error").hide();
						}
						else{
							$j("#"+fieldtestMthdDescId+"_error").show();
							flag=false;
						}
					}
					$j("#"+fieldfktestmethodId+"_error").hide();
				}
				else{
					$j("#"+fieldfktestmethodId+"_error").show();
					flag=false;
				}
				}
			}
			}
			n++;
			}
		});
		thawOtherRow++;
	}
	return flag;
}
	
function saveForTestResult(autoDeferSubmit){
	
	 			var labSummChanges = divChanges('labChanges',$j('#labSumm_modeltestDiv').html());
	 			
				 if( autoDeferStatus(autoDeferSubmit, labSummChanges)){
	             
				  var url="saveTestScreen?cordId="+'<s:property value="cdrCbuPojo.cordID"/>';
				  url=url+"&"+autoDeferSubmit;
				  var flag=validateThawOtherRow('labtestupdateform');
				  var rangeFlag=true;
				 $j(".onChngMand").each(function() {
					  rangeFlag=false;
					});
				  $j(".onChngRangeChk").each(function() {
					  rangeFlag=false;
					}); 
				  if(flag==true && rangeFlag==true){
					  if($j("#labtestupdateform").valid()){ 
						 // var labSummChanges = divChanges('labChanges',$j('#labSumm_modeltestDiv').html());
						  
					if(labSummChanges==true){
			         modalFormSubmitRefreshDiv('labtestupdateform',url,'loadLabSummaryDiv','updateModalLab','statusModalLab','1','<s:property value="cdrCbuPojo.cordID"/>');
					   $j("#status1").css('display','none');
					 refreshMultipleDivsOnViewClinical('1025','<s:property value="cdrCbuPojo.cordID"/>');
					 if($j('#fcrFlag').val() == "Y"){
							refreshMultipleDivsOnViewClinical('2','<s:property value="cdrCbuPojo.cordID"/>');
							} 
					}
					else if (labModelStatusDiv){
						showSuccessdiv('updateModalLab','labSumm_modeltestDiv');
					   }
					  } 
					 }
				   }
                 }
		function cfuCount(cfuVal){
			if(cfuVal ==$j("#pkOther").val()){
			document.getElementById("cfuCount").style.display="block";
				}
			else{
				document.getElementById("cfuCount").style.display="none";
				$j("#cfuDescOther").val('');
				}
		}
		function test(viabilityVal){
			if(viabilityVal ==$j("#pkViaOther").val()){
 			 document.getElementById("viability").style.display="block";
				}
			else{
				document.getElementById("viability").style.display="none";
				$j("#viaDescOther").val('');
				}
 		     }
 		 
           var labModelStatusDiv = true; 
          function autoDeferStatus(autoDeferSubmit, labSummChanges) {
        	  var returnType = true;
		       if(labSummChanges){
						if((document.getElementById('savePostTestList12testresult').defaultValue != $j('input[name="savePostTestList[12].testresult"]').val()) && (($j('input[name="savePostTestList[12].testresult"]').val())!='' && ($j('input[name="savePostTestList[12].testresult"]').val())==0)){
							
							returnType = false;
							labModelStatusDiv = false;	
							autoDeferCFU($j('input[name="savePostTestList[12].testresult"]').val(),$j('input[name="savePostTestList[12].testresult"]').attr('id'));	
							$j("#autodeferfieldflag").val("true");
							
						 }
			    	  if((document.getElementById('savePostTestList13testresult').defaultValue != $j('input[name="savePostTestList[13].testresult"]').val()) && (($j('input[name="savePostTestList[13].testresult"]').val())!='' && ($j('input[name="savePostTestList[13].testresult"]').val())>=0 && ($j('input[name="savePostTestList[13].testresult"]').val())<85)){
			    		  
			    		    returnType = false;  
			    		 	labModelStatusDiv = false;	
			    		    autoDeferViab($j('input[name="savePostTestList[13].testresult"]').val(),$j('input[name="savePostTestList[13].testresult"]').attr('id'));
						    $j("#autodeferfieldflag").val("true");			    			
					   
			    	    }
			    	  else{
			    		$j("#autodeferfieldflag").val("false");
				      }
			        }
		      		 return returnType;
				}
		  

</SCRIPT>
	
	
<script>
function customRangeM(input) {
	var today = new Date();
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();
	var h=today.getHours();
	var mn=today.getMinutes()+1;
	var minTestDate=$j("#datepickerbirthlabModal1").val();
	var minTestDate1=minTestDate.split("-");
	if(input.id == 'preTestDateStrnM' ||input.id == 'postTestDateStrnM' ||input.id == 'otherTestDateStrnM1' ||input.id == 'otherTestDateStrnM2' ||input.id == 'otherTestDateStrnM3' ||input.id == 'otherTestDateStrnM4' ||input.id == 'otherTestDateStrnM5' ||input.id == 'otherTestDateStrnM6'){
		//return {
	    	// minDate: new Date(minTestDate1[0],minTestDate1[1]-1,++minTestDate1[2])
	    //};
	    $j('#'+input.id).datepicker( "option", "minDate", new Date(minTestDate1[0],minTestDate1[1]-1,minTestDate1[2]));
	    if(input.id == 'preTestDateStrnM'){
	    	var maxTestDate=new Date($j("#processingDateId").val());// processing start date
			var d1 = maxTestDate.getDate();
			var m1 = maxTestDate.getMonth();
			var y1 = maxTestDate.getFullYear();
			if(today.getTime() > maxTestDate.getTime()){
				$j('#'+input.id).datepicker( "option", "maxDate", new Date(y1,m1,d1));
			}else{
				$j('#'+input.id).datepicker( "option", "maxDate", new Date(y,m,d));
			}
	    }else if(input.id !== 'otherTestDateStrnM5' && input.id !== 'otherTestDateStrnM6'){
	    	LabSumPostCustomDate(input,'processingDateId');
		 }		
	}
	var result = $j.browser.msie ? !this.fixFocusIE : true;
	this.fixFocusIE = false;
	return result;
}
var LabSumPostCustomDate=function(input,id){
						    var today = new Date();
							var d = today.getDate();
							var m = today.getMonth();
							var y = today.getFullYear();
							
							var maxTestDate=new Date($j("#"+id).val());// processing start date
							var d1 = maxTestDate.getDate();
							var m1 = maxTestDate.getMonth();
							var y1 = maxTestDate.getFullYear();
							
							$j('#'+input.id).datepicker( "option", "minDate", new Date(y1,m1,d1));
							$j('#'+input.id).datepicker( "option", "maxDate", new Date(y,m,d));
}
      $j(function(){
    	//alert($j("#savePreTestList0testresult").val());
    	        
    		 if($j("#pkViaOther").val()==$j("#viabSelected").val()){
    			 $j("#viability").show();
    			}else{
    				$j("#viability").hide();
    				}

     		if($j("#pkOther").val()==$j("#cfuSelected").val()){
    			 $j("#cfuCount").show();
    			}else{
    				$j("#cfuCount").hide();
    			}
     		
				getDatePic();
				jQuery('#preTestDateStrnM').datepicker('option', { beforeShow: customRangeM }).focus(function(){
					if(!$j("#ui-datepicker-div").is(":visible"))
						customRangeM(this);
					if($j(this).val()!=""){
						var thisdate = new Date($j(this).val());
						if(!isNaN(thisdate)){
							var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
							$j(this).val(formattedDate);
						}
					}
				});
				jQuery('#postTestDateStrnM').datepicker('option', { beforeShow: customRangeM }).focus(function(){
					if(!$j("#ui-datepicker-div").is(":visible"))
						customRangeM(this);
					if($j(this).val()!=""){
						var thisdate = new Date($j(this).val());
						if(!isNaN(thisdate)){
							var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
							$j(this).val(formattedDate);
						}
					}
				});
				jQuery('#otherTestDateStrnM1').datepicker('option', { beforeShow: customRangeM }).focus(function(){
					if(!$j("#ui-datepicker-div").is(":visible"))
						customRangeM(this);
					if($j(this).val()!=""){
						var thisdate = new Date($j(this).val());
						if(!isNaN(thisdate)){
							var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
							$j(this).val(formattedDate);
						}
					}
				});
				jQuery('#otherTestDateStrnM2').datepicker('option', { beforeShow: customRangeM }).focus(function(){
					if(!$j("#ui-datepicker-div").is(":visible"))
						customRangeM(this);
					if($j(this).val()!=""){
						var thisdate = new Date($j(this).val());
						if(!isNaN(thisdate)){
							var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
							$j(this).val(formattedDate);
						}
					}
				});
				jQuery('#otherTestDateStrnM3').datepicker('option', { beforeShow: customRangeM }).focus(function(){
					if(!$j("#ui-datepicker-div").is(":visible"))
						customRangeM(this);
					if($j(this).val()!=""){
						var thisdate = new Date($j(this).val());
						if(!isNaN(thisdate)){
							var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
							$j(this).val(formattedDate);
						}
					}
				});
				jQuery('#otherTestDateStrnM4').datepicker('option', { beforeShow: customRangeM }).focus(function(){
					if(!$j("#ui-datepicker-div").is(":visible"))
						customRangeM(this);
					if($j(this).val()!=""){
						var thisdate = new Date($j(this).val());
						if(!isNaN(thisdate)){
							var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
							$j(this).val(formattedDate);
						}
					}
				});
				jQuery('#otherTestDateStrnM5').datepicker('option', { beforeShow: customRangeM }).focus(function(){
					if(!$j("#ui-datepicker-div").is(":visible"))
						customRangeM(this);
					if($j(this).val()!=""){
						var thisdate = new Date($j(this).val());
						if(!isNaN(thisdate)){
							var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
							$j(this).val(formattedDate);
						}
					}
				});
				jQuery('#otherTestDateStrnM6').datepicker('option', { beforeShow: customRangeM }).focus(function(){
					if(!$j("#ui-datepicker-div").is(":visible"))
						customRangeM(this);
					if($j(this).val()!=""){
						var thisdate = new Date($j(this).val());
						if(!isNaN(thisdate)){
							var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
							$j(this).val(formattedDate);
						}
					}
				});
			    var validator = $j("#labtestupdateform").validate({
						 rules:{	
							 "savePostTestList[12].testMthdDesc":{required:
								{
								  depends: function(element){
								    return ($j("#cfuDescOther").is(':visible') && $j("#cordSearchable").val()!='0');
										}
							        }	
							     },

								 "savePostTestList[13].testMthdDesc":{required:
									{
									  depends: function(element){
										  return ($j("#viaDescOther").is(':visible')  && $j("#cordSearchable").val()!='0');
										}
									}	
								},
								"preTestDateStrn":{required:
					            {	  depends: function(element){
										        return (document.getElementById("preTestDateStrnM").defaultValue != '');
											}
										}			
								},	
								"postTestDateStrn":{required:
							            {	  depends: function(element){
									        return (document.getElementById("postTestDateStrnM").defaultValue != '');
										}
									}			
							     },	
							     "otherTestDateStrn1":{required:
						            		{	  depends: function(element){
								     	  	 return (document.getElementById("otherTestDateStrnM1").defaultValue != '');
											}
										}			
						    		 },
						    	 "otherTestDateStrn2":{required:
					            			{	  depends: function(element){
							     	  	 		return (document.getElementById("otherTestDateStrnM2").defaultValue != '');
											}
										}			
					    		 	},
					    		 "otherTestDateStrn3":{required:
			            		 			{	  depends: function(element){
					     	  	 				return (document.getElementById("otherTestDateStrnM3").defaultValue != '');
											}
										}			
			    		 			},
			    		 		"otherTestDateStrn4":{required:
	            		 					{	  depends: function(element){
			     	  	 							return (document.getElementById("otherTestDateStrnM4").defaultValue != '');
											}
										}			
	    		 					},
	    		 				"otherTestDateStrn5":{required:
        		 							{	  depends: function(element){
	     	  	 									return (document.getElementById("otherTestDateStrnM5").defaultValue != '');
											}
										}			
		 							},
		 						"otherTestDateStrn6":{required:
		 									{	  depends: function(element){
 	  	 											return (document.getElementById("otherTestDateStrnM6").defaultValue != '');
											}
										}			
 									},
							    //"savePostTestList[12].fktestmethod":{validateRequiredCordEntry:["savePostTestList12fktestmethod"],checkSelectCordEntry:["savePostTestList12fktestmethod"]},
			   					 // "savePostTestList[13].fktestmethod":{validateRequiredCordEntry:["savePostTestList13fktestmethod"],checkSelectCordEntry:["savePostTestList13fktestmethod"]},
								
				   					  /* "savePreTestList[0].testresult":{validateRequiredCordEntry:["savePreTestList0testresult"],rangeLabsummary:[40,500,"savePreTestList0testresult"]},
				   					  "savePostTestList[1].testresult":{validateRequiredCordEntry:["savePostTestList4testresult"],rangeLabsummary:[12,999,"savePostTestList4testresult"]},
				   					  "savePostTestList[4].testresult":{validateRequiredCordEntry:["savePostTestList5testresult"],rangeLabsummary:[0,999,"savePostTestList5testresult"]},
				   					  //"savePostTestList[5].testresult":{validateRequiredCordEntry:["savePostTestList6testresult"],rangeLabsummary:[0,999.9,"savePostTestList6testresult"]},
				   					  "savePostTestList[10].testresult":{validateRequiredCordEntry:["savePostTestList12testresult"],rangeLabsummary:[0,9999,"savePostTestList12testresult"]},
				   					  "savePostTestList[11].testresult":{validateRequiredCordEntry:["savePostTestList13testresult"],rangeLabsummary:[0,100,"savePostTestList13testresult"]},
				   					  "savePostTestList[12].testresult":{validateRequiredCordEntry:["savePostTestList14testresult"],rangeLabsummary:[0,999,"savePostTestList14testresult"]},
				   					  "savePostTestList[13].testresult":{validateRequiredCordEntry:["savePostTestList14testresult"],rangeLabsummary:[0,100,"savePostTestList15testresult"]},
				   					  "savePostTestList[12].fktestmethod":{validateRequiredCordEntry:["savePostTestList14fktestmethod"],checkSelectCordEntry:["savePostTestList14fktestmethod"]},
				   					  "savePostTestList[13].fktestmethod":{validateRequiredCordEntry:["savePostTestList15fktestmethod"],checkSelectCordEntry:["savePostTestList15fktestmethod"]},

				   					    "savePreTestList[0].testresult":{required:true,range:[40,500]},
					   					"savePreTestList[1].testresult":{range:[12,999]},
										"savePreTestList[2].testresult":{range:[1000,50000]},
										"savePostTestList[2].testresult":{range:[5000,200000]},
										"savePostTestList[3].testresult":{range:[10,600]},
										"savePostTestList[4].testresult":{required:true,range:[0.0,999.9]},
										//"savePostTestList[5].testresult":{required:true,range:[12,999]},
										//"savePostTestList[6].testresult":{required:true,range:[0,999.9]},
										"savePostTestList[5].testresult":{range:[0.0,100]},
										"savePostTestList[6].testresult":{range:[0.0,10.0]},
										"savePostTestList[7].testresult":{range:[20.0,99.9]},
										"savePostTestList[8].testresult":{range:[0,999.9]},
										"savePostTestList[9].testresult":{range:[0,99.9]},
										"savePostTestList[11].testresult":{required:true,range:[0,9999]},
										"savePostTestList[12].testresult":{required:true,range:[0,999]},
										"savePostTestList[13].testresult":{required:true,range:[0,100]}, */
										 
										//"savePostTestList[15].testresult":{required:true,range:[0,100]},
										//"savePostTestList[14].fktestmethod":{checkselect:true},
										//"savePostTestList[15].fktestmethod":{checkselect:true},
										  

								},
						 messages:{	
							 			"savePostTestList[12].testMthdDesc":"<s:text name="garuda.cbu.cordentry.cfuOther"/>",
										"savePostTestList[13].testMthdDesc":"<s:text name="garuda.cbu.cordentry.viaOther"/>",
										"preTestDateStrn":{required :"<s:text name="garuda.cbu.cordentry.inputId"/>"},
										"postTestDateStrn":{required :"<s:text name="garuda.cbu.cordentry.inputId"/>"},
										"otherTestDateStrn1":{required :"<s:text name="garuda.cbu.cordentry.inputId"/>"},
										"otherTestDateStrn2":{required :"<s:text name="garuda.cbu.cordentry.inputId"/>"},
										"otherTestDateStrn3":{required :"<s:text name="garuda.cbu.cordentry.inputId"/>"},
										"otherTestDateStrn4":{required :"<s:text name="garuda.cbu.cordentry.inputId"/>"},
										"otherTestDateStrn5":{required :"<s:text name="garuda.cbu.cordentry.inputId"/>"},
										"otherTestDateStrn6":{required :"<s:text name="garuda.cbu.cordentry.inputId"/>"}
										//"savePostTestList[12].fktestmethod":"<s:text name="garuda.cbu.cordentry.cfucount"/>",
										//"savePostTestList[13].fktestmethod":"<s:text name="garuda.cbu.cordentry.viability"/>"
										/* "savePostTestList[14].notes":"<s:text name="garuda.cbu.cordentry.cfuOther"/>",
									    "savePostTestList[15].notes":"<s:text name="garuda.cbu.cordentry.viaOther"/>",
										"savePreTestList[0].testresult":"<s:text name="garuda.common.range.testval40to500"/>",
										"savePreTestList[1].testresult":"<s:text name="garuda.common.range.testval12to999"/>",
										"savePostTestList[1].testresult":"<s:text name="garuda.common.range.testval12to999"/>",
										"savePreTestList[2].testresult":"<s:text name="garuda.common.range.testval1000to50000"/>",
										"savePostTestList[2].testresult":"<s:text name="garuda.common.range.testval5000to200000"/>",
										"savePostTestList[3].testresult":"<s:text name="garuda.common.range.testval10to600"/>",
										"savePostTestList[4].testresult":"<s:text name="garuda.common.range.val0to999"/>",
										"savePostTestList[5].testresult":"<s:text name="garuda.common.range.testrange0to100"/>",
										"savePostTestList[6].testresult":"<s:text name="garuda.common.range.testrange0to10"/>",
										"savePostTestList[7].testresult":"<s:text name="garuda.common.range.val20to99"/>",
										"savePostTestList[8].testresult":"<s:text name="garuda.common.range.val0to999"/>",
										"savePostTestList[9].testresult":"<s:text name="garuda.common.range.val0to99"/>",
										"savePostTestList[10].testresult":"<s:text name="garuda.common.range.val0to9999"/>",
										"savePostTestList[11].testresult":"<s:text name="garuda.common.range.val0to100"/>",
										"savePostTestList[12].testresult":"<s:text name="garuda.common.range.testrange0to999"/>",
										"savePostTestList[13].testresult":"<s:text name="garuda.common.range.decbetween0to100"/>",
										//"savePostTestList[14].testresult":"<s:text name="garuda.common.range.val0to999"/>",
										//"savePostTestList[15].testresult":"<s:text name="garuda.common.range.decbetween0to100"/>",
										"savePostTestList[12].fktestmethod":"<s:text name="garuda.cbu.cordentry.cfucount"/>",
										"savePostTestList[13].fktestmethod":"<s:text name="garuda.cbu.cordentry.viability"/>" */
										 
															
								 }
					   }); 
		 });
     function restrict(mytextarea) 
     {  
         var maxlength = 15;
         mytextarea.value = mytextarea.value.substring(0,maxlength);
     }    
	  
    $j(function(){
    	autoDeferField_1= $j('input[name="savePostTestList[12].testresult"]').val();
    	autoDeferField_2= $j('input[name="savePostTestList[13].testresult"]').val();
        });
</script>

<div id="divlabtestupdateform">	
<s:form id="labtestupdateform">
<s:hidden name="cordCbuStatus"/>
<s:hidden name="submitButtonAutoDefer" id="autodeferfieldflag" value="false"/>
 <s:hidden name="otherLabTstsLstId" id="otherLabTstsLstId"></s:hidden>
<s:hidden name="pkCfuOther" id="pkOther"></s:hidden>	
<s:hidden name="pkViabilityOther" id="pkViaOther"></s:hidden>	
<s:hidden name="cdrCbuPojo.fkSpecimenId"></s:hidden>			
<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
<s:hidden name="esignFlag" id="esignFlag"></s:hidden>

<s:hidden value="%{#request.valuePreFkTimingTest}" id="valuePreFkTimingTest"></s:hidden>
<s:hidden value="%{#request.valuePostFkTimingTest}" id="valuePostFkTimingTest"></s:hidden>
<s:hidden value="%{#request.valueOtherFkTimingTest}" id="valueOtherFkTimingTest"></s:hidden>
					   
<s:hidden value="%{#request.valueOtherPostTestFirst}" id="valueOtherPostTestFirst"></s:hidden>
<s:hidden value="%{#request.valueOtherPostTestSecond}" id="valueOtherPostTestSecond"></s:hidden>
<s:hidden value="%{#request.valueOtherPostTestThird}" id="valueOtherPostTestThird"></s:hidden>
<s:hidden value="%{#request.valueOtherPostTestFourth}" id="valueOtherPostTestFourth"></s:hidden>
<s:hidden value="%{#request.valueOtherPostTestFifth}" id="valueOtherPostTestFifth"></s:hidden>

<s:hidden name="fromViewClinicalLS2" id="fromViewClinicalLS2"></s:hidden>
<!-- <input type="hidden" id="savePreTestList0testresult" value="<s:property value="preTestList[0].testresult"/>"/>
<input type="hidden" id="savePostTestList4testresult" value="<s:property value="postTestList[3].testresult"/>"/>
<input type="hidden" id="savePostTestList5testresult" value="<s:property value="postTestList[4].testresult"/>"/>
<input type="hidden" id="savePostTestList6testresult" value="<s:property value="postTestList[5].testresult"/>"/>
<input type="hidden" id="savePostTestList12testresult" value="<s:property value="postTestList[12].testresult"/>"/>
<input type="hidden" id="savePostTestList13testresult" value="<s:property value="postTestList[13].testresult"/>"/>
<input type="hidden" id="savePostTestList14testresult" value="<s:property value="postTestList[14].testresult"/>"/>
<input type="hidden" id="savePostTestList15testresult" value="<s:property value="postTestList[15].testresult"/>"/>

<input type="hidden" id="savePostTestList14fktestmethod" value="<s:property value="postTestList[14].fktestmethod"/>"/>
<input type="hidden" id="savePostTestList15fktestmethod" value="<s:property value="postTestList[15].fktestmethod"/>"/>
-->
<s:hidden value="%{#request.cfuOtherPkVal}" id="valueCfuOtherPkVal"></s:hidden>
<s:hidden value="%{#request.testReasonOtherPkVal}" id="valueTestReasonOtherPkVal"></s:hidden>
<s:hidden value="%{#request.viablityOtherPkVal}" id="valueViablityOtherPkVal"></s:hidden>
<s:iterator value="processingTest" var="rowVal" status="row">
<s:hidden value="%{pkLabtest}" id="%{shrtName}" name="%{labtestName}"></s:hidden>
</s:iterator>
<s:hidden value="%{#request.labGroupTypTests}" id="noOfLabGrpTypTests" name="noOfLabGrpTypTests"/>
<s:hidden value="#request.TNCUncFkTestId" id="UNCRCT" name="Total CBU Nucleated Cell Count (uncorrected)"/>
<s:hidden value="#request.TNCUnkIfUncFkTestId" id="TCBUUN" name="Total CBU Nucleated Cell Count (unknown if uncorrected)"/>
<div style="display: none;">
	<s:date name="CdrCbuPojo.birthDate" id="datepickerbirthlabModal" format="MMM dd, yyyy" />
	<s:date name="CdrCbuPojo.processingDate" id="processingDateId" format="MMM dd, yyyy" />	
 	<s:textfield readonly="true" onkeydown="cancelBack();"  name="datepickerbirthlabModal" id="datepickerbirthlabModal"  value="%{datepickerbirthlabModal}" ></s:textfield>
 	<s:hidden name="processingDateId" id="processingDateId" value="%{processingDateId}"/>											      
						
</div>	
<div style="display: none;">
	<s:date name="CdrCbuPojo.specimen.specCollDate" id="datepickerbirthlabModal1" format="yyyy-MM-dd" />	
 	<s:textfield readonly="true" name="datepickerbirthlabModal1" id="datepickerbirthlabModal1"  value="%{datepickerbirthlabModal1}" ></s:textfield>
 	<s:date name="CdrCbuPojo.specimen.specCollDate" id="datepickerbirthlabModal2" format="MMM dd, yyyy"  />	
 	<s:textfield readonly="true" name="datepickerbirthlabModal2" id="datepickerbirthlabModal2"  value="%{datepickerbirthlabModal2}" ></s:textfield>
</div>	

<div class="portlet" id="procandcountparent" >
	<div id="editlabsummarytest" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
							<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" >
										<span class="ui-icon ui-icon-minusthick"></span>
										<span class="ui-icon ui-icon-close" ></span>
										<s:text name="garuda.cordentry.label.labsummaryTest" />
							</div> 
								
		<div id="updateModalLab" style="display: none;" >
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr >
							<td colspan="2">
								<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
									<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
									<strong><s:text name="garuda.common.lable.testMessage" /> </strong></p>
								</div>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="button" onclick="closeModalTest('labSummeryTests')" value="<s:text name="garuda.common.close"/>" />
							</td>
						</tr>
					</table>	   
		</div>	
		<div id="labSumm_modeltestDiv">
		<table width="100%" cellpadding="0" style="table-layout : fixed;" cellspacing="0" border="1" >   
		<tr>
			<td>		
		<div id="added1Div" style="overflow-x: scroll;">			
		<table id="statusModalLab" width="100%" cellpadding="0" cellspacing="0" border="0">   
		   <tr>
						<!--<td class="headcol"></td>
						--><td align="right">
							<button type="button"  id="addTestDate" onclick="showTest('<s:property  value="cdrCbuPojo.fkSpecimenId" />'),closeModal();"><s:text name="garuda.cordentry.label.addTestTime"/></button>
						</td>
			
					</tr>
		   <tr>
			   <td> 
				  <table width="100%" cellpadding="0" cellspacing="0" border="1" id="added1">
					<% int headerCount=3; %>
					<thead>
					
					  <tr>
						  <th  class="headcol" bgcolor="white"><s:text name="garuda.common.lable.test"></s:text></th>
						  <th >
						      <s:text name="garuda.common.lable.preProcess"></s:text><br></br>
						      <s:text name="garuda.common.lable.testingDate"></s:text>
						      <s:if test="prePatLabsPojo.testdate!=null">
							  <s:date name="prePatLabsPojo.testdate" id="datepicker52" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="preTestDateStrn" id="preTestDateStrnM" onchange="changeProgress(1,'datepicker52'); isChangeDone(this,'%{datepicker52}','labChanges');" value="%{datepicker52}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
						    </s:if>
						    <s:else>
						      <s:date name="patLabsPojo.testdate" id="datepicker52" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="preTestDateStrn" id="preTestDateStrnM" onchange="changeProgress(1,'datepicker52'); isChangeDone(this,'%{datepicker52}','labChanges');" value="%{datepicker52}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
						    
						    </s:else>
						    
							  </th>
							        	
						  <th >
					        <s:text name="garuda.common.lable.postProcess"></s:text><br></br>
					        <s:text name="garuda.common.lable.testingDate"></s:text>
					        <s:if test="postPatLabsPojo.testdate!=null">
							    <s:date name="postPatLabsPojo.testdate" id="datepicker53" format="MMM dd, yyyy" />
							    <s:textfield readonly="true" onkeydown="cancelBack();"  name="postTestDateStrn" id="postTestDateStrnM" onchange="changeProgress(1,'datepicker53'); isChangeDone(this,'%{datepicker53}','labChanges');" value="%{datepicker53}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
							</s:if>
							<s:else>
							    <s:date name="patLabsPojo.testdate" id="datepicker53" format="MMM dd, yyyy" />
							    <s:textfield readonly="true" onkeydown="cancelBack();"  name="postTestDateStrn" id="postTestDateStrnM" onchange="changeProgress(1,'datepicker53'); isChangeDone(this,'%{datepicker53}','labChanges');" value="%{datepicker53}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
							</s:else>
					
					  </th>
		                 <s:if test="otherPostTestList!=null && otherPostTestList.size()>0 ">
		                 	 <%  headerCount=headerCount+1; %>
						    <th ><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
						      <s:date name="otherPatLabsPojo.testdate" id="datepicker92" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn1" id="otherTestDateStrnM1" onchange="changeProgress(1,'datepicker92'); isChangeDone(this,'%{datepicker92}','labChanges');" value="%{datepicker92}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
						    </th>
						  </s:if>
						  
						<s:if test="otherPostTestListFirst!=null && otherPostTestListFirst.size()>0 ">
							 <%  headerCount=headerCount+1; %>
						    <th ><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
						      <s:date name="otherPatLabsPojoFirst.testdate" id="datepicker94" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn2" id="otherTestDateStrnM2" onchange="changeProgress(1,'datepicker94'); isChangeDone(this,'%{datepicker94}','labChanges');" value="%{datepicker94}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
						    </th>
						</s:if>
						  
						 <s:if test="otherPostTestListSecond!=null && otherPostTestListSecond.size()>0 ">
						 	 <%  headerCount=headerCount+1; %>
							    <th ><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
							   <s:date name="otherPatLabsPojoSecond.testdate" id="datepicker95" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn3" id="otherTestDateStrnM3" onchange="changeProgress(1,'datepicker95'); isChangeDone(this,'%{datepicker95}','labChanges');" value="%{datepicker95}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
							    
							    </th>
						 </s:if>
						<s:if test="otherPostTestListThird!=null && otherPostTestListThird.size()>0 ">
							 <%  headerCount=headerCount+1; %>
							    <th ><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
							    <s:date name="otherPatLabsPojoThird.testdate" id="datepicker96" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn4" id="otherTestDateStrnM4" onchange="changeProgress(1,'datepicker96'); isChangeDone(this,'%{datepicker96}','labChanges');" value="%{datepicker96}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
							    
							    
							    </th>
						 </s:if>
					 
						<s:if test="otherPostTestListFourth!=null && otherPostTestListFourth.size()>0 ">
							 <%  headerCount=headerCount+1; %>
							    <th ><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
							    <s:date name="otherPatLabsPojoFourth.testdate" id="datepicker97" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn5" id="otherTestDateStrnM5" onchange="changeProgress(1,'datepicker97'); isChangeDone(this,'%{datepicker97}','labChanges');" value="%{datepicker97}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
							    
							    </th>
						 </s:if>
					 
						 <s:if test="otherPostTestListFifth!=null && otherPostTestListFifth.size()>0 ">
						 	 <%  headerCount=headerCount+1; %>
							  <th ><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
								<s:date name="otherPatLabsPojoFive.testdate" id="datepicker98" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn6" id="otherTestDateStrnM6" onchange="changeProgress(1,'datepicker98'); isChangeDone(this,'%{datepicker98}','labChanges');" value="%{datepicker98}" cssClass="datePicWMaxDate" cssStyle="width: 210px;"></s:textfield>
 
							  
							  </th>
						 </s:if>
						  
					  </tr>	                       <%--  <th width="25%"><s:text name="garuda.common.lable.addTesting"></s:text></th> --%>
					
				</thead>	
					<tbody>
						<% int testCount=1; %> 
						<s:iterator value="processingTest" var="rowVal" status="row">
								<tr style="width: 100%;" id="<s:property	value="%{#row.index}"/>">
									<% request.setAttribute("testCount",testCount); %>            	
								 
								 <s:if test="labtestHower!=' '"> 										            	
								      <td width="30%" class="headcol" bgcolor="white"><s:property value="labtestName"/></td>
								  </s:if>
								  <s:else>
									  <td width="30%" class="headcol" bgcolor="white" onmouseover="return overlib('<s:property value="labtestHower"/>');"
											onmouseout="return nd();"> <s:property value="labtestName"/>
									  </td>
								  </s:else>	
		  
								<%-- 	<% int precount=0; %> --%>
								<s:set name="checkEmpty" value="0" scope="request"/>
									<td class="BasicText" align="left">
									 <s:iterator value="preTestList">
						            <%-- ondblclick="addInfo('<s:property value="cdrCbuPojo.fkSpecimenId" />','<s:property value="fktestid" />','<s:property value="pkpatlabs" />');	 
								          <s:property value="cdrCbuPojo.fkSpecimenId" /><s:property value="pkLabtest" /> <s:property value="fktestid" /><s:property value="pkpatlabs" /> 
									 <s:property value="pkLabtest" /> <s:property value="fktestid" /> <s:property value="pkpatlabs" />  --%>
							
									   	  <s:if test="pkLabtest==fktestid">
									   	   <s:if test="#request.valuePreFkTimingTest==fkTimingOfTest">
									   	   <s:set name="checkEmpty" value="1" scope="request"/>
									   	<%--   <%precount++; %> --%>
									   	  <%--  preeee<s:property value="fkTimingOfTest"/> --%>
									   		<s:set name="check" value="%{'check'}" />
										      
													 <s:hidden name="savePreTestList[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
													 <s:hidden name="savePreTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden> 
													<s:hidden name="savePreTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePreFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"></s:if>
												<s:else></s:else>
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count' || labtestName=='CBU Nucleated Cell Count Concentration'">
														<s:text name="garuda.cbuentry.label.testresult"></s:text>
														<input style="width:40%" type="text" name="savePreTestList[<s:property value="%{#row.index}"/>].testresult" id="savePreTestList<s:property value="%{#row.index}"/>testresult" value="<s:property value="%{testresult}"/>" onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" size="10" <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' ">class="labclass"</s:if> onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'pre')"/>
					 									<s:hidden value="%{testresult}" id="savePreTestList%{#row.index}testresult_dbStoredVal"/>
					 									<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
					 							</s:if>	
												<s:else>
													 <s:set name="testresult" value="%{testresult}" scope="request"/>
													<s:if test="#request.testresult != null">
													<s:text name="garuda.cbuentry.label.testresult"></s:text>
													<s:textfield name="savePreTestList[%{#row.index}].testresult" id="savePreTestList%{#row.index}testresult" cssClass="width:45%" value="%{testresult}" size="10" theme="simple" onkeydown="cancelBack();" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'pre')" onchange="isChangeDone(this,'%{testresult}','labChanges');"/> 
													<s:hidden  value="%{testresult}" id="savePreTestList%{#row.index}testresult_dbStoredVal"/>
													<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													<s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePreTestList[%{#row.index}].fktestspecimen" id="savePreTestList%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
													<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
													</s:if> 
												</s:else>
													   <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
													   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;10<sup>7</sup></s:if>
													   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;10<sup>3</sup>/<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
						            </s:if>
					            	</s:if>	
					              </s:iterator>
								    <s:if test="#request.checkEmpty==0 && (preTestList.size()>0)">
					               		 		&nbsp;
					               	</s:if> 
					               <%--<%=precount%> --%>
							           <s:if test="%{#check!='check'}">	
								           		<s:hidden name="savePreTestList[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
									             <s:hidden name="savePreTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden> 
									              <s:hidden name="savePreTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePreFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"></s:if>
												<s:else></s:else>
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count' || labtestName=='CBU Nucleated Cell Count Concentration'">		 
					 								 <s:text name="garuda.cbuentry.label.testresult"></s:text>
					 								 <input type="text" style="width:40%" name="savePreTestList[<s:property value="%{#row.index}"/>].testresult" id="savePreTestList<s:property value="%{#row.index}"/>testresult" value="<s:property value="%{testresult}"/>" onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" size="10" <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count' || labtestName=='CBU Nucleated Cell Count Concentration'">class="labclass"</s:if> onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'pre')"/>
				 							    	<s:hidden  value="%{testresult}" id="savePreTestList%{#row.index}testresult_dbStoredVal"/>
				 							    	<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
				 							    </s:if>	
											    <s:else>
											    <s:set name="testresult" value="%{testresult}" scope="request"/>
													<s:if test="#request.testresult != null">
													<s:text name="garuda.cbuentry.label.testresult"></s:text>
													<s:textfield name="savePreTestList[%{#row.index}].testresult" id="savePreTestList%{#row.index}testresult" cssClass="width:45%" value="%{testresult}" size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'pre')"/> 
													<s:hidden  value="%{testresult}" id="savePreTestList%{#row.index}testresult_dbStoredVal"/>
													<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													<s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePreTestList[%{#row.index}].fktestspecimen" id="savePreTestList%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{testresult}','labChanges');"></s:select>
													<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
													</s:if> 
											  	</s:else>
													<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												    <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;10<sup>7</sup></s:if>
												    <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;10<sup>3</sup>/<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
					                	</s:if></td>
 	                           			<%-- <% int postcount=0;  %> --%>
										<s:set name="checkEmpty" value="0" scope="request"/>
										<s:set name="cellCountFlag" value="0" scope="request"/>
 							           <td class="BasicText" align="left">
 							           <s:iterator value="postTestList">
 									     <s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     <s:if test="#request.valuePostFkTimingTest==fkTimingOfTest">
									     <s:if test="#request.cellCountFlag==0">
									        <s:set name="checkEmpty" value="1" scope="request"/>  
									        <s:set name="check1" value="%{'check'}"/>
									         	   <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												   <s:text name="garuda.cbuentry.label.testresult"></s:text>
									           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
									           	    <s:iterator value="postTestList" status="innerrow">
									           	    <s:if test="fktestid==#request.TNCUncFkTestId">
									           	    <s:if test="#request.cellCountFlag==0">
									           	    <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
											       	<input type="text" style="width:45%" id="savePostTestList<s:property value="%{#row.index}"/>testresult"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')"/>
											       	<s:hidden  value="%{testresult}" id="savePostTestList%{#row.index}testresult_dbStoredVal"/>
											       	<br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
											       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
											       	<s:set name="cellCountFlag" value="1" scope="request"/>
											       	</s:if></s:if>
											       	</s:iterator>
											       </s:if>
												   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
												   	<s:iterator value="postTestList" status="innerrow">
									           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
									           	    <s:if test="#request.cellCountFlag==0">
									           	    <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>
											       	<input type="text" style="width:45%" id="savePostTestList<s:property value="%{#row.index}"/>testresult"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')"/>
											       	<s:hidden  value="%{testresult}" id="savePostTestList%{#row.index}testresult_dbStoredVal"/>
											       	<br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
											       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
											       	<s:set name="cellCountFlag" value="1" scope="request"/>
											       	</s:if></s:if>
											       	</s:iterator>
												   	<s:if test="#request.cellCountFlag==0">
												   	<s:iterator value="postTestList" status="innerrow">
									           	    <s:if test="fktestid==#request.TNCUncFkTestId">
									           	    <s:if test="#request.cellCountFlag==0">
									           	    <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
											       	<input type="text" style="width:45%" id="savePostTestList<s:property value="%{#row.index}"/>testresult"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')"/>
											       	<s:hidden  value="%{testresult}" id="savePostTestList%{#row.index}testresult_dbStoredVal"/>
											       	<br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
											       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
											       	<s:set name="cellCountFlag" value="1" scope="request"/>
											       	</s:if></s:if>
											       	</s:iterator>
												   	</s:if>
											       	</s:elseif>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										      </s:if></s:if>
 									     </s:if>
									     <s:elseif test="pkLabtest==fktestid &&!(pkLabtest == #request.TNCFkTestId) && (labtestName!='NAT HIV' && labtestName!='NAT HCV')">
									        <s:if test="#request.valuePostFkTimingTest==fkTimingOfTest">
									        	<s:set name="checkEmpty" value="1" scope="request"/>  
									       <%--  <%postcount++;%> --%>
									     <%--   <%=postcount %>--%>
									         <%-- post<s:property value="fkTimingOfTest" /> --%>
									          
									          <s:set name="check1" value="%{'check'}"/>
									         <%-- <%if(postcount>precount){%>
									         <td>&nbsp;</td> 
									         <%} %> --%>
									         		
												 <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>
												 <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Viability' || labtestName=='Total CD34+ Cell Count' || labtestName=='CBU nRBC Absolute Number' || labtestName=='CBU nRBC %' || labtestName=='CFU Count' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'"></s:if>
												   <%-- <s:if test=""></s:if>
												   <s:if test=" "></s:if>
												   <s:if test=""></s:if>
												   <s:if test=""></s:if>
												  <s:if test=""></s:if> --%>
												  <s:else></s:else>
												<%-- 
												   <s:textfield name="savePostTestList[%{#row.index}].testresult" value="%{testresult}" size="10" theme="simple" />
												
											       --%>
											      <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      	&nbsp;
											      </s:if>
											      <s:else>
											      <s:text name="garuda.cbuentry.label.testresult"></s:text>
												  <input type="text" style="width:45%" id="savePostTestList<s:property value="%{#row.index}"/>testresult"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" <s:if test="cdrCbuPojo.site.isCDRUser()==true && labtestName=='Viability'">onchange="autoDeferViab(this.value,this.id); isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');"</s:if><s:if test="labtestName=='CFU Count'">onchange="autoDeferCFU(this.value,this.id); isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');"</s:if> onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')" />
										           <s:hidden  value="%{testresult}" id="savePostTestList%{#row.index}testresult_dbStoredVal"/>
										           <br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
										           <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
												   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
												   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
												   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if> 
												   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
												   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
												   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
												   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Viability'">%</s:if>
												   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												  </s:else>
												 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL">
												   <div id="savePostTestListcfuCount"> 
												    <br><s:text name="garuda.common.lable.method"></s:text>
												    <s:select name="savePostTestList[%{#row.index}].fktestmethod" id="cfuSelected"  value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="cfuCount(this.value);descValidation(this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
												    </div>	 
												   <div id="cfuCount"> 
													   <br><s:text name="garuda.common.lable.describe"></s:text><span style="color: red;">*</span>
													  <span>&nbsp;&nbsp;</span><s:textfield name="savePostTestList[%{#row.index}].testMthdDesc" maxlength="50" value="%{testMthdDesc}" id="cfuDescOther" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');"  onkeypress="validate();"></s:textfield>
													  
													</div>
												 </s:if>
												 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
													   <div id="savePostTestListcfuviability"> 
													    <br><s:text name="garuda.common.lable.method"></s:text>
													    <s:select name="savePostTestList[%{#row.index}].fktestmethod" id="viabSelected" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value); isChangeDone(this,'%{fktestmethod}','labChanges');"><br></s:select>
												        </div>
												      <div id="viability"> 
														<br><s:text name="garuda.common.lable.describe"></s:text><span style="color: red;">*</span>
														 <span>&nbsp;&nbsp;</span><s:textfield name="savePostTestList[%{#row.index}].testMthdDesc" maxlength="50" id="viaDescOther" onchange="isChangeDone(this,'%{fktestmethod}','labChanges');" value="%{testMthdDesc}" onkeypress="validate();"></s:textfield>
														
													  </div>													 
												   </s:if>
												   <s:if test="#request.testCount>#request.labGroupTypTests">
												   	<s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePostTestList[%{#row.index}].fktestspecimen" id="savePostTestList%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
												   <br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
												   </s:if>
									            
									        </s:if>
									        
									       </s:elseif>
									       
									   </s:iterator>
									  
									     <s:if test="#request.checkEmpty==0 && postTestList.size()>=#request.labGroupTypTests">
					               		 		&nbsp;
					               		</s:if>  
					               		<s:if test="#request.testCount<=#request.labGroupTypTests"> 
									     <s:if test="%{#check1!='check'}">
									    <s:if test="pkLabtest == #request.TNCFkTestId">
									         		<s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
									         				<s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													</s:if>
													<s:else>
												 			<s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>  
												  	</s:else>
												   <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												   <s:text name="garuda.cbuentry.label.testresult"></s:text>
									           	   <input type="text" style="width:45%" id="savePostTestList<s:property value="%{#row.index}"/>testresult"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')"/>
									           	   <s:hidden  value="%{testresult}" id="savePostTestList%{#row.index}testresult_dbStoredVal"/>
									           	   <br/><span style="display:none" id="savePostTestList%{#row.index}testresult_error" class="error"><s:text name=""></s:text> </span>
									           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
													<s:text name="garuda.labtest.label.uncrctd"></s:text>
											       </s:if>
												   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
											       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
											       	</s:elseif>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										 
 									     </s:if>
 									     <s:elseif test="!(pkLabtest == #request.TNCFkTestId) && (labtestName!='NAT HIV' && labtestName!='NAT HCV')">
									           	   <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
												   <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Viability' || labtestName=='Total CD34+ Cell Count' || labtestName=='CBU nRBC Absolute Number' || labtestName=='CBU nRBC %' || labtestName=='CFU Count' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'"></s:if>
												   <%-- <s:if test=""></s:if>
												   <s:if test=" "></s:if>
												   <s:if test=""></s:if>
												   <s:if test=""></s:if>
												  <s:if test=""></s:if> --%>
												  <s:else><span>&nbsp;&nbsp;</span></s:else>
												   <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      	&nbsp;
											      </s:if>
											      <s:else>
											      <s:text name="garuda.cbuentry.label.testresult"></s:text>
												   <input type="text" style="width:45%" id="savePostTestList<s:property value="%{#row.index}"/>testresult"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" <s:if test="cdrCbuPojo.site.isCDRUser()==true && labtestName=='Viability'">onchange="autoDeferViab(this.value,this.id); isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');"</s:if><s:if test="labtestName=='CFU Count'">onchange="autoDeferCFU(this.value,this.id); isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');"</s:if> onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')" />
										           <s:hidden  value="%{testresult}" id="savePostTestList%{#row.index}testresult_dbStoredVal"/>
										           <br/><span style="display:none" id="savePostTestList%{#row.index}testresult_error" class="error"><s:text name=""></s:text> </span>
										           <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										 		   <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;10^7</s:if>   
												   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
												   
												   <s:if test="labtestName=='CBU nRBC %' ">%</s:if>
												   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
												   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count' ">%</s:if>
												   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count' ">%</s:if>
												   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
												   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												    <s:if test="labtestName=='Viability'">%</s:if>
												    <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												  </s:else>
											    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL">
												   <div id="savePostTestListcfuCount"> 
												  <br><s:text name="garuda.common.lable.method"></s:text>
												     <span>&nbsp;&nbsp;</span><s:select name="savePostTestList[%{#row.index}].fktestmethod" id="cfuSelected" value="%{fktestmethod}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="cfuCount(this.value); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
												      </div>	
												  <div id="cfuCount">   
													 <br><s:text name="garuda.common.lable.describe"></s:text><span style="color: red;">*</span>
													 <span>&nbsp;&nbsp;</span> <s:textfield name="savePostTestList[%{#row.index}].testMthdDesc" maxlength="50" value="%{testMthdDesc}" id="cfuDescOther" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');"  onkeypress="validate();"></s:textfield>
										             
										          </div>		
										        </s:if>
												
												<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
										               <div id="savePostTestListviability"> 
										              <br><s:text name="garuda.common.lable.method"></s:text>
										               <br><s:select name="savePostTestList[%{#row.index}].fktestmethod" id="viabSelected" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value); isChangeDone(this,'%{fktestmethod}','labChanges');"><br></s:select>
												   	</div>
												   <div id="viability"> 
												       <s:text name="garuda.common.lable.describe"></s:text><span style="color: red;">*</span>
												       <span>&nbsp;&nbsp;</span><s:textfield name="savePostTestList[%{#row.index}].testMthdDesc" maxlength="50" id="viaDescOther" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeypress="validate();"></s:textfield>
												 	 
												  </div>
												</s:if>
									        	<s:if test="#request.testCount>#request.labGroupTypTests">
												   	<s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePostTestList[%{#row.index}].fktestspecimen" id="savePostTestList%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
												  	<div id="notReq"></div>
												  	<br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
												  </s:if></s:elseif>
									      </s:if>
									      </s:if>
									     </td>
											<s:if test="otherPostTestList!=null && otherPostTestList.size()>0 ">					  
													<s:set name="checkEmpty" value="0" scope="request"/>
													<s:set name="cellCountFlag" value="0" scope="request"/>
													<td Class="BasicText">
													<s:iterator value="otherPostTestList">
													  <s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherFkTimingTest==fkTimingOfTest">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check2" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestList" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestList" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
														       	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
														       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
														       	 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
														       	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														     </s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestList" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
													       	  <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       		
													       	</s:if>
													       	</s:elseif>		
													       		<s:if test="#request.cellCountFlag==1">
													       			<div id="showHardDrop1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																		<s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason"  id="saveOtherPostTestList1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																		 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																		<s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate();"></s:textfield>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																		</div>
																									 	
																	    <s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   		<s:text name="garuda.common.lable.describe"></s:text>
																	   		<s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate();"></s:textfield>
																   			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																   			</div></s:if>
																		</div>
													       		</s:if>							   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
														<s:if test="#request.valueOtherFkTimingTest==fkTimingOfTest">
														 <s:set name="checkEmpty" value="1" scope="request"/>
														 <s:set name="check2" value="%{'check'}"/>
																	<s:set name="test" value="pkLabtest" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:set name="abc" value="%{#row.index}" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
																	 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" cssClass="width:45%" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" onchange="isChangeDone(this,'%{testresult}','labChanges');"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
																	 	 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
																	 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
														           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
																    <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																   <s:if test="labtestName=='Viability'">%</s:if>						 
																   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																																 
																		<div id="showHardDrop1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if> >											
																		      <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason"  id="saveOtherPostTestList1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			<div id="reasonFrTestDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeypress="validate();"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>						 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   		<s:text name="garuda.common.lable.describe"></s:text>
																	   		<s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeypress="validate();"></s:textfield>
																   			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																   			</div></s:if>
																   		</div></s:else>
																		     
																        <%--   
																							  
																	  <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																          <s:text name="garuda.common.lable.viability"></s:text>
																          <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		  <s:text name="garuda.common.lable.describe"></s:text>
																		  <s:textfield name="saveOtherPostTestList1[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate();"></s:textfield>
																	 </s:if>
																 --%>
											 				 </s:if>
												 				 
											 				</s:elseif>
											 				
										 				</s:iterator>
										 				  <s:if test="#request.checkEmpty==0 && otherPostTestList.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if> 
										               	<s:if test="#request.testCount<=#request.labGroupTypTests"> 
										 		    	<s:if test="%{#check2!='check'}">
										 		        <s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		<s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/> 
													       	  <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																		<s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason"  id="saveOtherPostTestList1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																		<s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeypress="validate();"></s:textfield>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																		</div>
																									 	
																	    <s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   		<s:text name="garuda.common.lable.describe"></s:text>
																	   		<s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeypress="validate();"></s:textfield>
																   			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																   			</div></s:if>
																   		</div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" cssClass="width:45%" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" onchange="isChangeDone(this,'%{testresult}','labChanges');"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
																	 	 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     						<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     				</s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" cssClass="width:45%" value="%{testresult}" size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
																		 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
																		 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
																	    <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>								 
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="test" value="pkLabtest" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 
												                    <div id="showHardDrop1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																		<s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason"  id="saveOtherPostTestList1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																		<s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate();"></s:textfield>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																		</div>
																									 	
																	    <s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   		<s:text name="garuda.common.lable.describe"></s:text>
																	   		<s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeypress="validate();"></s:textfield>
																   			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																   			</div></s:if>
																   		</div></s:else>
													               <%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																       <s:text name="garuda.common.lable.viability"></s:text>
																       <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																	   <s:text name="garuda.common.lable.describe"></s:text>
																	   <s:textfield name="saveOtherPostTestList1[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																 </s:if> --%>
														</s:elseif>     	 
											        </s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										     </s:if>
										     
										     
											<s:if test="otherPostTestListFirst!=null && otherPostTestListFirst.size()>0">
												<s:set name="checkEmpty" value="0" scope="request"/>					  
												<td Class="BasicText">
												<s:iterator value="otherPostTestListFirst">
												<s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestFirst==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check3" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValue%{#row.index}"></s:hidden>
														     <div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListFirst" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
													       	  <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestListFirst" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	   	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
														       	   <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
														       	  <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														    </s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListFirst" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
													       	   <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
													       	  <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>
													       		<s:if test="#request.cellCountFlag==1">
													       			<div id="showHardDrop2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		 
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																      
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason"  id="saveOtherPostTestList2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate();" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																			<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">					 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								    <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeypress="validate();"></s:textfield>
																	   		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																	   		</div></s:if>
																	   </div>
													       		</s:if>
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
														 <s:if test="#request.valueOtherPostTestFirst==otherListValue">
														 	<s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check3" value="%{'check'}"/>
																
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValue%{#row.index}"></s:hidden>
																	 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" cssClass="width:45%" value="%{testresult}"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	   <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" cssClass="width:45%" value="%{testresult}"  size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"></s:textfield> 
																		 <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
																	    <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>						      
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																      <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																			
																	 <div id="showHardDrop2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		
																			  <s:text name="garuda.common.label.reaForTest"></s:text>															     
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason"  id="saveOtherPostTestList2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate();"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">					 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								    <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}testMthdDesc"  value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate();"></s:textfield>
																  			<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																  			</div></s:if>
																		</div></s:else>
																
																	<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																           <s:text name="garuda.common.lable.viability"></s:text>
																           <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		   <s:text name="garuda.common.lable.describe"></s:text>
																		   <s:textfield name="saveOtherPostTestList2[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	 </s:if>
																 --%>
																
											 				 </s:if>
											 				 
											 				</s:elseif>
											 					
										 				</s:iterator>
										 				 <s:if test="#request.checkEmpty==0 && otherPostTestListFirst.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>  
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check3!='check'}">
										 		    	<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		 
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																      
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason"  id="saveOtherPostTestList2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeypress="validate"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">					 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								    <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																	   		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																	   		</div></s:if>
																	   </div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" cssClass="width:45%" value="%{testresult}"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" cssClass="width:45%" value="%{testresult}" size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"  >  </s:textfield>
																		 <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
																	    <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>										 .
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>			
																		
																	 <div id="showHardDrop2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		 
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																      
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason"  id="saveOtherPostTestList2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">					 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								    <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																	   		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																	   		</div></s:if>
																	   </div></s:else>    
																		     
																	 <%-- 
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList2[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if> --%>
													          </s:elseif>
													          </s:if></s:if></td> <s:set name="cellCountFlag" value="0" scope="request"/>
										                  </s:if>	     
										     
										     
		                         <s:if test="otherPostTestListSecond!=null && otherPostTestListSecond.size()>0 ">
		                         	<s:set name="checkEmpty" value="0" scope="request"/>					  
												<td Class="BasicText">
												<s:iterator value="otherPostTestListSecond">
												 <s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestSecond==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check4" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListSecond" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   		<s:iterator value="otherPostTestListSecond" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
														       	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
														       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
														       	 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														       	</s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListSecond" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden> 
													       	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>
													       		<s:if test="#request.cellCountFlag==1">
													       			<div id="showHardDrop3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason"  id="saveOtherPostTestList3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
															           		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
															           		</div></s:if>
															           </div>
													       		</s:if>												   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
												 	<s:if test="#request.valueOtherPostTestSecond==otherListValue">
												 		<s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check4" value="%{'check'}"/>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
											                         	
																	 <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValue%{#row.index}"></s:hidden>
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	   <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" ></s:textfield> 
																		 <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
																	    <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>									       
																       <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																		
																		 <div id="showHardDrop3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>	
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																														     
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason"  id="saveOtherPostTestList3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
															           		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
															           		</div></s:if>
															           	</div></s:else>
															
																		<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList3[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if> --%>
																 
											 				</s:if>
											 			 </s:elseif>	
										 				</s:iterator>
										 				 <s:if test="#request.checkEmpty==0 && otherPostTestListSecond.size()>=#request.labGroupTypTests">
										               		 		&nbsp; 
										               	</s:if>  
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		   		 <s:if test="%{#check4!='check'}">
										 		    		<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason"  id="saveOtherPostTestList3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
															           		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
															           		</div></s:if>
															           </div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw');" onchange="isChangeDone(this,'%{testresult}','labChanges');"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																    <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%" size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw'); isChangeDone(this,'%{testresult}','labChanges');" >  </s:textfield>
																		 <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
																	    <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>							 
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												                     <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
												                      
												                      <div id="showHardDrop3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason"  id="saveOtherPostTestList3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
															           		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
															           		</div></s:if>
															           </div></s:else>
																		<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList3[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	    </s:if> --%>
											                 </s:elseif>
											                  </s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										               </s:if>	     

					<s:if test="otherPostTestListThird!=null && otherPostTestListThird.size()>0 ">	
					<s:set name="checkEmpty" value="0" scope="request"/>				  
												<td Class="BasicText">
												<s:iterator value="otherPostTestListThird">
												  <s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestThird==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check5" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValue%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListThird" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   		<s:iterator value="otherPostTestListThird" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
														       	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
														       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
														       	 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       <s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														       	</s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListThird" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
															<s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
															<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
															<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>
													       		<s:if test="#request.cellCountFlag==1">
													       			<div id="showHardDrop4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>      
																	        <s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason"  cssClass="dependantOnFldRequired" id="saveOtherPostTestList4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" cssClass="dependantOnFldRespRequired" maxlength="50" id="saveOtherPostTestList4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" cssClass="dependantOnFldRequired" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" cssClass="dependantOnFldRequired" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" cssClass="dependantOnFldRequired" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc"  maxlength="50" cssClass="dependantOnFldRespRequired" id="saveOtherPostTestList4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
														                	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
														                	</div></s:if>
														                </div>
													       		</s:if>												   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
												  		<s:if test="#request.valueOtherPostTestThird==otherListValue">
														  <s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check5" value="%{'check'}"/>
																 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValue%{#row.index}"></s:hidden>
																	 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%" onchange="isChangeDone(this,'%{testresult}','labChanges');"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" ></s:textfield> 
																		<s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																		<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
																        <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																		<s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		<s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		<s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		<s:if test="labtestName=='CBU nRBC %'">%</s:if>
																		<s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																		<s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		<s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
																		 <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																		<s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		<s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		<s:if test="labtestName=='Viability'">%</s:if>
																		<s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>				     
																     <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																			
																	<div id="showHardDrop4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																															     
																			  <s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason"  id="saveOtherPostTestList4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
														                	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
														                	</div></s:if>
														                	</div></s:else>
																	  <%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																           <s:text name="garuda.common.lable.viability"></s:text>
																           <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		   <s:text name="garuda.common.lable.describe"></s:text>
																		   <s:textfield name="saveOtherPostTestList4[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if> --%>
																
											 				  </s:if>
											 				 </s:elseif>			
										 				</s:iterator>
										 				  <s:if test="#request.checkEmpty==0 && otherPostTestListThird.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>  
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    	<s:if test="%{#check5!='check'}">
										 		    	<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>      
																	        <s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason"  id="saveOtherPostTestList4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
														                	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
														                	</div></s:if>
														                </div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" onchange="isChangeDone(this,'%{testresult}','labChanges');" cssClass="width:45%" size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     				</s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%" size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');"  onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw'); "></s:textfield>
																		<s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
																	    <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>								 
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												                     <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	        
																	  <div id="showHardDrop4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>      
																	        <s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason"  id="saveOtherPostTestList4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
														                	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
														                	</div></s:if>
														                </div></s:else>
																	<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList4[%{#row.index}].notes" value="%{notes}" maxlength="50" onkeypress="validate"></s:textfield>
																	   </s:if> --%>
											              </s:elseif>
											              </s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										         </s:if>	     
			
								<s:if test="otherPostTestListFourth!=null && otherPostTestListFourth.size()>0 ">					  
												<s:set name="checkEmpty" value="0" scope="request"/>
												<td Class="BasicText">
												<s:iterator value="otherPostTestListFourth">
												  <s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestFourth==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check6" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValue%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListFourth" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop5%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestListFourth" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
														       	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop5%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
														       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
														       	 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														       	</s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListFourth" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop5%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>
													       		<s:if test="#request.cellCountFlag==1">
													       			<div id="showHardDrop5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>	
																			  <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason"  id="saveOtherPostTestList5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" id="saveOtherPostTestList5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																</div>
															</s:if>											   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
														<s:if test="#request.valueOtherPostTestFourth==otherListValue">
														  <s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check6" value="%{'check'}"/>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValue%{#row.index}"></s:hidden>
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	  <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop5%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"></s:textfield> 
																		 <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
																	    <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>									
																		<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																		
																		<div id="showHardDrop5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			 <s:text name="garuda.common.label.reaForTest"></s:text>
																			 <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason"  id="saveOtherPostTestList5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																		</div></s:else>
																	<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].notes" value="%{notes}" maxlength="50" onkeypress="validate"></s:textfield>
																		 	 
																	   </s:if>
																 --%>
											 				</s:if>
											 			  </s:elseif>
										 				</s:iterator>
										 				  <s:if test="#request.checkEmpty==0 && otherPostTestListFourth.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>  
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check6!='check'}">
										 		   		<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop5%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       <div id="showHardDrop5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>	
																			  <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason"  id="saveOtherPostTestList5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																		</div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																 <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}"  cssClass="width:45%" size="10" theme="simple"  onchange="vidSaved('showHardDrop5%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')">  </s:textfield>
																	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
																	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
														           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
																    <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																   <s:if test="labtestName=='Viability'">%</s:if>								 
																   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												                     <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
												
																	<div id="showHardDrop5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>	
																			  <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason"  id="saveOtherPostTestList5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																		</div></s:else>
																		   
	
																	 <%--    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																             <s:text name="garuda.common.lable.viability"></s:text>
																             <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if> --%></s:elseif>
											         </s:if> </s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										     </s:if>	     
			
													     
								<s:if test="otherPostTestListFifth!=null && otherPostTestListFifth.size()>0 ">					  
										<s:set name="checkEmpty" value="0" scope="request"/>
										<td Class="BasicText">
										<s:iterator value="otherPostTestListFifth">
											<s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestFifth==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check7" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValue%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListFifth" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestListFifth" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
														       	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
														       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
														       	 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														     </s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListFifth" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>
													       		 <s:if test="#request.cellCountFlag==1">
													       		 	<div id="showHardDrop6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason"  id="saveOtherPostTestList6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																		</div>
													       		 </s:if>												   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
												 		<s:if test="#request.valueOtherPostTestFifth==otherListValue">
														<s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check7" value="%{'check'}"/>
																
																
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValue%{#row.index}"></s:hidden>
																	 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	 <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" ></s:textfield> 
																	<s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
																	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
														           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
																    <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																   <s:if test="labtestName=='Viability'">%</s:if>						    
																   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																    <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																		
																	<div id="showHardDrop6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason"  id="saveOtherPostTestList6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																			</div></s:else>
																		<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																             <s:text name="garuda.common.lable.viability"></s:text>
																             <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	    </s:if>
																 --%>
											 			 </s:if>
											 	    </s:elseif>
										 	</s:iterator>
										 	  <s:if test="#request.checkEmpty==0 && otherPostTestListFifth.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if> 
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check7!='check'}">
										 		    	<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason"  id="saveOtherPostTestList6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																	</div>
													       
													   		<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
											      			
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     				</s:if>
											     				<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     				<s:else>
																 <div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" size="10" cssClass="width:45%"  theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" >  </s:textfield>
																	<s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
																	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>	
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
																	    <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>											
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												                      <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
												
												                        <div id="showHardDrop6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason"  id="saveOtherPostTestList6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																			</div></s:else>
																		    
											         					
											         					 <%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																             <s:text name="garuda.common.lable.viability"></s:text>
																             <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	    </s:if> --%></s:elseif>
											            </s:if></s:if>  </td><s:set name="cellCountFlag" value="0" scope="request"/>
										      </s:if>	     
                                        </tr>
                                       <s:if test="#request.testCount==#request.labGroupTypTests && processingTest.size()>#request.labGroupTypTests">
                                        <tr>
                                        <td class="headcol">
                                        <div
											class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
											style="text-align: left;">
											 <s:text name="garuda.cordentry.label.otherLabTests"/>
										</div>
                                       </td>
                                       <td colspan="<%=headerCount%>">
                                        <div
											class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
											style="text-align: left;">
											 <%-- <s:text name="garuda.cordentry.label.otherLabTests"/> --%> 
										</div>
                                       </td>
                                        </tr></s:if>
                                        <% testCount=testCount+1; %> 
                                       <s:set name="check" value="%{'uncheck'}"/> 
                                        <s:set name="check1" value="%{'uncheck'}"/>
                                        <s:set name="check2" value="%{'uncheck'}"/>
                                        <s:set name="check3" value="%{'uncheck'}"/>
                                        <s:set name="check4" value="%{'uncheck'}"/>
                                        <s:set name="check5" value="%{'uncheck'}"/>
                                        <s:set name="check6" value="%{'uncheck'}"/>
                                        <s:set name="check7" value="%{'uncheck'}"/> 
								  </s:iterator>
						     </tbody></table>
						     	<tr>
                                        <td>
                                
                                       </td><td colspan="<%=headerCount%>-1" style="border: 0px;">
                                       
                                       </td>
                                        </tr>
					      </table></div>      
						</td>
					</tr>
				</table>
			<table id="status1" width="100%" cellpadding="0" cellspacing="0">
					  <tr>
						  		<td align="left">
									<input type="button" id="addnewtest"
										value="<s:text name="garuda.cdrcbuview.label.button.addNewTest" />"
										onclick="javascript:addTest('<s:property value="cdrCbuPojo.cordID" />','True');" />
								</td>	      		   	
			       			   <td width="70%"><jsp:include page="../cb_esignature.jsp" flush="true"></jsp:include></td>
					  		   <td align="right">
					  			  <button type="button" disabled="disabled" id="submit" onclick="saveForTestResult('submitButtonAutoDefer=false')" class="labSummarysubmit"><s:text name="garuda.unitreport.label.button.submit"/></button>
					 	  		  <button type="button" onclick="closeModalTest('labSummeryTests');"><s:text name="garuda.unitreport.label.button.cancel"></s:text></button>		
					  		   </td>
					  	   	  	   
			            </tr>         
				</table>
				</div>
			</div>
		</div>
	</s:form>	
	<div id="dialog" style="display: none;" title="Add Test Date">
		<jsp:include page="cb_dialog_modeltestdate.jsp" flush="true">
	   	    <jsp:param value=""  name="fkSpeciId" />
		</jsp:include>
	</div>
	</div>
<script type="text/javascript">

$j('[name="cordCbuStatus"]').val($j('#cordCbuStatus').val());

</script>
 <!-- <div id="modelPopup2"></div>  -->
   