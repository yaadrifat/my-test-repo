<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<div class="portlet" id="modifyDocDiv" >
<s:hidden name="cdrCbuPojo.registryId" id="registryId"></s:hidden>
<div id="update" style="display: none;" >
	  <jsp:include page="../cb_update.jsp">
	    <jsp:param value="garuda.message.modal.clinical"  name="message" />
	  </jsp:include>	   
	</div>
		      
</div>