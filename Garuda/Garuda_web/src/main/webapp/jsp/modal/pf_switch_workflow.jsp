<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script type="text/javascript">
$j(function(){
	hideSwitchReasons();
});
function submitSwitchWorkflow(){
	var switchto=$j('input:radio[name=sampleatlab]:checked').val();
	if(validateSwitchWorkflow()){
		if(switchto=='Y'){

			jConfirm('<s:text name="garuda.workflow.message.changeWorkflowConfirm"/>', '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
			    if(r){
			    	//window.location='submitSwitchWorkflow?switchflag='+switchto+'&pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&switchRsnOther=<s:text name="garuda.workflow.message.switchrsnother"/>';
			    	submitpost('submitSwitchWorkflow',{'switchflag':switchto,'pkcordId':$j("#pkcordId").val(),'orderId':$j("#orderId").val(),'orderType':$j("#orderType").val(),'switchRsnOther':'<s:text name="garuda.workflow.message.switchrsnother"/>'});
				}
			    
			});
			
		}
		if(switchto=='N' && $j("#switchwfreasons").val()!=null && $j("#switchwfreasons").val()!=$j("#switchRsnOtherPk").val()){

			jConfirm('<s:text name="garuda.workflow.message.changeWorkflowConfirm"/>\n\n<s:text name="garuda.workflow.message.reason_for_workflow"/> '+$j("#switchwfreasons option:selected").text(), '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
			    if(r){
			    	//window.location='submitSwitchWorkflow?switchflag='+switchto+'&pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&switchReason='+$j("#switchwfreasons").val();
			    	submitpost('submitSwitchWorkflow',{'switchflag':switchto,'pkcordId':$j("#pkcordId").val(),'orderId':$j("#orderId").val(),'orderType':$j("#orderType").val(),'switchReason':$j("#switchwfreasons").val()});
				}
			    
			});
		}
		if(switchto=='N' && $j("#switchwfreasons").val()!=null && $j("#switchwfreasons").val()==$j("#switchRsnOtherPk").val()){

			jConfirm('<s:text name="garuda.workflow.message.changeWorkflowConfirm"/>\n\n<s:text name="garuda.workflow.message.reason_for_workflow"/> '+$j("#reasonOther").val(), '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
			    if(r){
			    	//window.location='submitSwitchWorkflow?switchflag='+switchto+'&pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&switchReason='+$j("#switchwfreasons").val()+'&switchRsnOther='+$j("#reasonOther").val();
			    	submitpost('submitSwitchWorkflow',{'switchflag':switchto,'pkcordId':$j("#pkcordId").val(),'orderId':$j("#orderId").val(),'orderType':$j("#orderType").val(),'switchReason':$j("#switchwfreasons").val(),'switchRsnOther':$j("#reasonOther").val()});
				}
			    
			});
		}
	}
	
}

function displaySwitchReasons(){
	$j("#switchReasonsDiv").show();
}
function hideSwitchReasons(){
	
	$j("#switchReasonsDiv").hide();
	$j("#switchReasonOtherDiv").hide();
	$j("#switchwfreasons").val("");
	$j("#reasonOther").val("");
}

function showOtherReason(){
	if($j("#switchwfreasons").val()==$j("#switchRsnOtherPk").val()){
		$j("#switchReasonOtherDiv").show();
	}else{
		$j("#switchReasonOtherDiv").hide();
		$j("#reasonOther").val("");
	}
}
function validateSwitchWorkflow(){
	var switchto=$j('input:radio[name=sampleatlab]:checked').val();
	if(switchto==null || switchto==''){
		$j("#errorswitchtoDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.workflow.message.select_switch_to"/>");
		 });
		$j("#errorswitchtoDiv").show();
		return false;
		
	}
	else{
		$j("#errorswitchtoDiv").find("span").each(function(){
      		$j(this).text("");
	 });
	}
	if(switchto=='N' && ($j("#switchwfreasons").val()==null || $j("#switchwfreasons").val()=='')){
		$j("#errorreasonDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.workflow.message.select_switch_to_reason"/>");
		 });
		$j("#errorreasonDiv").show();
		return false;
		
	}
	else{
		$j("#errorreasonDiv").find("span").each(function(){
      		$j(this).text("");
	 });
	}
	if($j("#switchwfreasons").val()==$j("#switchRsnOtherPk").val() && ($j("#reasonOther").val()==null || $j("#reasonOther").val()=='')){
		$j("#errorreasonOtherDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.common.message.otherReason"/>");
		 });
		$j("#errorreasonOtherDiv").show();
		return false;
	}
	else{
		$j("#errorreasonOtherDiv").find("span").each(function(){
      		$j(this).text("");
	 });
	}
	if((switchto!=null && switchto=='Y') ||	(switchto=='N' && $j("#switchwfreasons").val()!=null && $j("#switchwfreasons").val()!='' && $j("#switchwfreasons").val()!=$j("#switchRsnOtherPk").val()) ||
			(switchto=='N' && ($j("#switchwfreasons").val()!=null && $j("#switchwfreasons").val()!='') && $j("#switchwfreasons").val()==$j("#switchRsnOtherPk").val() && $j("#switchwfreasons").val()!=null && $j("#switchwfreasons").val()!='')){
				$j("#errorswitchtoDiv").hide();
				$j("#errorreasonDiv").hide();
				$j("#errorreasonOtherDiv").hide();
				return true;
	}
}
</script>
<s:form name="switchWorkflow">
<s:hidden name="orderId" id="orderId"></s:hidden>
<s:hidden name="orderType" id="orderType"></s:hidden>
<s:hidden name="pkcordId" id="pkcordId"></s:hidden>
<s:hidden name="sampleAtLab" id="sampleAtLabId"></s:hidden>
<s:hidden name="switchRsnOtherPk" id="switchRsnOtherPk"></s:hidden>
<div id="updateWorkflow" style="display: none;" >
  <table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.message.modal.switchwf" /></strong></p>
			</div>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td>
			<input type="button" onclick="closeModals('switchwfmodaldiv')" value="<s:text name="garuda.common.close"/>" />
		</td>
	</tr>
</table>
	  
</div>
<div id="statusWorkflow">
<table>
	<tr>
		<td rowspan="2">
			<s:text name="garuda.switchworkflow.label.switchto"></s:text>:<span style="color: red;">*</span>
		</td>
		<td>
			<input type="radio" name="sampleatlab" id="sampleatlaby" value="Y" onchange="validateSwitchWorkflow();hideSwitchReasons();"><label for="sampleatlaby"><s:text name="garuda.workflow.message.CTsample"/></label>
		</td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="sampleatlab" id="sampleatlabn" value="N" onchange="validateSwitchWorkflow();displaySwitchReasons();"><label for="sampleatlabn"><s:text name="garuda.workflow.message.CTship"/></label>
			<s:div id="errorswitchtoDiv"><span id="switchtolbl" class="error" ></span></s:div>
		</td>
	</tr>
	<tr id="switchReasonsDiv">
		<td>
			<s:text name="garuda.switchworkflow.label.switchreason"></s:text> :<span style="color: red;">*</span> 
		</td>
		<td>
			<s:select name="switchwfreasons" id="switchwfreasons" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SWITCH_WORFLOW_RSN]" listKey="pkCodeId" listValue="description" headerKey=""
								headerValue="Select" onchange="validateSwitchWorkflow();showOtherReason()"></s:select>
			<s:div id="errorreasonDiv"><span id="reasonlbl" class="error" ></span></s:div>	
		</td>
	</tr>
	<tr id="switchReasonOtherDiv">
		<td>
			<s:text name="garuda.switchworkflow.label.switchrsnother"></s:text> :<span style="color: red;">*</span> 
		</td>
		<td>
			<s:textarea name="reasonOther" id="reasonOther" style="width:300px" onchange="validateSwitchWorkflow();"></s:textarea>
			<s:div id="errorreasonOtherDiv"><span id="reasonOtherlbl" class="error" ></span></s:div>
		</td>
	</tr>
</table>
<br>
<br>
<!-- <table>
	<tr>
		<td>
			<img alt="Warning" src="images/advertencia.png">
		</td>
		<td>
		<p>
			The CT Workflow switch is only allowed when requested by the NMDP. By proceeding, you are confirming that you have been contacted by the NMDP to change the workflow.
		</p> 
		</td>
	</tr>
</table>-->
<table bgcolor="#cccccc" class="tabledisplay disable_esign">
	<tr bgcolor="#cccccc" valign="baseline">
		<td width="70%">
			<jsp:include page="../cb_esignature.jsp" flush="true">
				<jsp:param value="submitswitchwf" name="submitId"/>
				<jsp:param value="invalidswitchwf" name="invalid" />
				<jsp:param value="minimumswitchwf" name="minimum" />
				<jsp:param value="passswitchwf" name="pass" />
			</jsp:include>
		</td>
		<td align="left" width="30%">
			<input type="button" id="submitswitchwf" disabled="disabled" onclick="submitSwitchWorkflow()" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
			<input type="button"  onclick="closeModals('switchwfmodaldiv')" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />
		</td>	
	</tr>
</table>
</div>
</s:form>
