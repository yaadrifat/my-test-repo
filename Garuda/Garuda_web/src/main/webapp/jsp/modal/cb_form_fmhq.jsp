<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.eres.service.util.EJBUtil"%>
<%@page import="com.velos.ordercomponent.util.EresForms"%>
<script>
$j(function(){
	$j("#formsListTable1").dataTable({"bSort": false,"bRetrieve": true,
		"bDestroy" :true});
});
</script>
<div id="fmhqcontent">
	 <s:if test="formsList != null">
					  	 <table border="0" cellpadding="0" cellspacing="0" class="displaycdr" id="formsListTable1" width="100%">
						<thead>
							<tr>
		
								<th class="th3" scope="col" width="50%"><s:text
									name="Date" /></th>
								<th class="th3" scope="col" width="50%"><s:text
									name="User" /></th>
							</tr>
						</thead>
						<tbody>
								<s:set name="formName" value="@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ" scope="request"/>
								<%
								String fmhqName = request.getAttribute("formName").toString();
								EresForms eres = new EresForms();
								int formId = eres.getFormId(fmhqName);
								request.setAttribute("formId",formId);
								%>
								
								<s:iterator value="formsList" var="rowVal" >
								<s:if test ="%{fkFormLib == #request.formId}">
								<tr>
									<td width="50%"><a href ="#" onClick="showModal('Upload FMHQ Form','openEresForm?module=CORDENTRY&formCode=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ"/>&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID"/>&pk_acctforms=<s:property value="pkAccoutForms"/>','600','750');"> 
									<s:date name="%{formFillDate}" id="formFillDt" format="MMM dd, yyyy" /><s:property value="%{formFillDt}"/></a> </td>
									<td width="50%">
									<s:set name="userName" value="%{createdBy}" scope="request"/>
									<% 
									String userName = request.getAttribute("userName").toString();
									UserJB user = new UserJB();
									user.setUserId(EJBUtil.stringToNum(userName));
									user.getUserDetails();	
									%>
									<%=user.getUserLastName()+" "+user.getUserFirstName() %> </td>
								
								</tr>
								</s:if>
							</s:iterator>
						</tbody>
						<tfoot>
							<tr>
							
								<td colspan="6"></td>
							</tr>
						</tfoot>
		
					</table><br/>
					</s:if>
</div>