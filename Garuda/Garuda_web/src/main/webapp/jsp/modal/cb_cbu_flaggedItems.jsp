<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<script>
function commenttoggle(id,val){
	var pos=$j("#"+id).position()
	var left=pos.left;
	//var top=pos.top-50;
	$j('#content_'+val).css({left:left});
	$j('#content_'+val).toggle();
}
function hidecomment(val){
	$j('#content_'+val).hide();
}
function flaggedItemsTable(){
	$j('#finaltablelookup').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.noFlagItems"/>"
		},
		"aaSorting": [[ 1, "asc" ]],
		"bRetrieve" : true,
        "bDestroy": true
	});
	$j('#finaltablelookup_length').hide();
	$j('#finaltablelookup_filter').hide();
	if($j('#flaggedItemListSize').val()!="" && $j('#flaggedItemListSize').val()!=null && $j('#flaggedItemListSize').val()>0){
		$j('#finaltablelookup_info').show();
		$j('#finaltablelookup_paginate').show();
	}else{
		$j('#finaltablelookup_info').hide();
		$j('#finaltablelookup_paginate').hide();
	}
	/*if('<s:property value="esignFlag"/>'=="review"){
		$j('#finaltablelookup_info').hide();
		$j('#finaltablelookup_paginate').hide();
	}*/
}
$j(function(){
	flaggedItemsTable();
});

function getFlaggedDetails(pkAssessment, entityid){
	showprogressMgs();
	if('<s:property value="esignFlag"/>'=="review"){
		showrightdiv('getFlaggedItemDetails?esignFlag=review&assessmentPojo.pkAssessment='+pkAssessment+'&cdrCbuPojo.cordID='+entityid,'flaggedItemsDiv');
		}
	else
		showModals('<s:text name="garuda.cbufinalreview.label.detailsflaggedItems"/> <s:property value="cdrCbuPojo.registryId"/>','getFlaggedItemDetails?assessmentPojo.pkAssessment='+pkAssessment+'&cdrCbuPojo.cordID='+entityid,'400','550','dialogForCRI');

	closeprogressMsg();
}
</script>
<style>
.commentbox{
	position: absolute;
	background: -moz-linear-gradient(center top , #FFFFFF, #EEEEEE) repeat scroll 0 0 #7dcfd5;
	z-index:1;
	border: 1px solid #DBDAD9;
	padding: 2px;
}
</style>
<div id="flaggeddivparent">
<s:form>
<s:hidden id="flaggedItemListSize" value="%{lstFlaggedItems.size()}"></s:hidden>
<table border="0" align="left" width="100%" cellpadding="0" cellspacing="0" class="displaycdr" id="finaltablelookup">
						<thead>
						<tr>
							<th><s:text name="garuda.cbufinalreview.label.flaggedItems.type"></s:text></th>
							<th><s:text name="garuda.cbufinalreview.label.flaggedItems.reference"></s:text></th>
							<th><s:text name="garuda.cbufinalreview.label.flaggedItems.comment"></s:text></th>
						</tr>
						</thead>
						<tbody>
						<s:iterator value="lstFlaggedItems" var="assessmentPojo" status="row">
						<tr>
							<td><s:property value="getCodeListDescById(subEntityType)"/></td>
							<td>
								<a href="#" onclick="getFlaggedDetails('<s:property value="pkAssessment"/>','<s:property value="entityId"/>');">
									<s:property value="columnReference"/><s:if test="getCodeListSubTypeById(subEntityType)!='NOTES' && getCodeListSubTypeById(subEntityType)!='LAB_SUM'"> - <s:if test="assessmentReason!=null"><s:property value="%{getCodeListDescById(assessmentReason)}"/></s:if><s:else><s:property value="reasonRemarks"/></s:else></s:if>
								</a>
							</td>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].pkAssessment" value="%{pkAssessment}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].entityId" value="%{entityId}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].entityType" value="%{entityType}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].subEntityId" value="%{subEntityId}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].subEntityType" value="%{subEntityType}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].flagforLater" value="%{flagforLater}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].assessmentResp" value="%{assessmentResp}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].visibleTCFlag" value="%{visibleTCFlag}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].assessmentReason" value="%{assessmentReason}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].reasonRemarks" value="%{reasonRemarks}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].availableDate" value="%{availableDate}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].sendReviewFlag" value="%{sendReviewFlag}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].consultFlag" value="%{consultFlag}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].assessmentReviewed" value="%{assessmentReviewed}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].columnReference" value="%{columnReference}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].deletedFlag" value="%{deletedFlag}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].assessmentpostedby" value="%{assessmentpostedby}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].assessmentpostedon" value="%{assessmentpostedon}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].assessmentconsultby" value="%{assessmentconsultby}"></s:hidden>
							<s:hidden name="flaggedAssessmentList[%{#row.index}].assessmentconsulton" value="%{assessmentconsulton}"></s:hidden>
							<td id="commentTd"><img height="15px" src="./images/addcomment.png">
									<a href="#" id="testcomment_<s:property value='%{#row.index}'/>" onclick="commenttoggle(this.id,<s:property value='%{#row.index}'/>);" style="text-decoration: none;">
									<s:if test='%{esignFlag=="flagged"}'>
										<s:text name="garuda.cbufinalreview.label.flaggedItems.viewcomment"></s:text>
									</s:if>
									<s:else>
										<s:if test='%{(assessmentFlagComment==null || assessmentFlagComment=="") && (cbuFinalReviewPojo.finalReviewConfirmStatus!="Y")}'>
											<s:text name="garuda.cbufinalreview.label.flaggedItems.addcomment"></s:text>
										</s:if>
										<s:else>
											<s:text name="garuda.cbufinalreview.label.flaggedItems.viewcomment"></s:text>
										</s:else>
									</s:else>		
									</a>&nbsp;&nbsp;
									<div id="content_<s:property value='%{#row.index}'/>" class="commentbox" style="display: none;width: 35%">
									<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
									<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
									<s:text name="garuda.cbufinalreview.label.flaggedItems.commentbox" />
									</div>			
										<s:if test='%{esignFlag=="flagged"}'>
										<s:textarea id="comments" class="displayTrue" name="flaggedAssessmentList[%{#row.index}].assessmentFlagComment" cols="20" rows="5" value="%{assessmentFlagComment}" disabled="true"></s:textarea>
										<br/>
										</s:if>
										<s:else>
										<s:if test='%{(cbuFinalReviewPojo.finalReviewConfirmStatus!="Y")}'>
											<s:textarea id="comments" class="displayTrue" name="flaggedAssessmentList[%{#row.index}].assessmentFlagComment" cols="20" rows="5" value="%{assessmentFlagComment}" ></s:textarea>
											<br/>
										</s:if>
										<s:else>
											<s:textarea id="comments" class="displayTrue" name="flaggedAssessmentList[%{#row.index}].assessmentFlagComment" cols="20" rows="5" value="%{assessmentFlagComment}" disabled="true"></s:textarea>
										<br/>
										</s:else>
										</s:else>							
										<input type="button" onclick="hidecomment(<s:property value='%{#row.index}'/>)" id="button_"+<s:property value='%{#row.index}'/> value="<s:text name="garuda.pendingorderpage.label.close"/>"/>
									</div>
									</div>
							</td>
						</tr>
						</s:iterator>
						</tbody>
						<tfoot>
							<tr><td colspan="3"></td></tr>
						</tfoot>
					</table>
					<s:if test='%{esignFlag=="flagged"}'>
					<div>&nbsp;</div>
					<table align="center">
							<tr><td align="center"><input type="button" onclick="closeModal();" value="<s:text name="garuda.common.close"/>"/></td></tr>
					</table>
					</s:if>		
</s:form>
</div>