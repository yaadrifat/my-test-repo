<%@taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants" %>
<%@page import="com.velos.ordercomponent.util.VelosSystemServices" %>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<jsp:include page="../cb_track_session_logging.jsp" />
<%String contextpath = request.getContextPath()+"/jsp/modal/"; %>
<s:set name="eligDocAvail" value="false" scope="request"></s:set>
<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]!=null">
    <s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]" var="rowVal" status="row">
           <s:if test="%{#rowVal[11]}==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@FINAL_DEC_OF_ELIG_CODESUBTYPE)">
              <s:set name="eligDocAvail" value="true" scope="request"></s:set>              
           </s:if>
     </s:iterator>											                
</s:if>
<s:if test="cdrCbuPojo.site.usingFdoe()==false || #eligDocAvail == false">
	<script> 
	
	
	function validatesReasons(){
		
		jQuery.validator.addMethod("validateRadioRequired", function(value, element,params) {
			var flag = false;
			$j('[name='+params[0]+']').each(function(){
			   if($j(this).attr('checked'))
	            {   
	                flag = true;                    
	            }                        
	        });
			
			return (true && flag);		 
		}, "<s:text name="garuda.cbu.cordentry.question"/>");
		if($j('#fkCordCbuEligible_1').val()!=$j('#notCollectedToPriorReasonId').val()){
		<s:if test = "#request.shipMentFlag==true">
		$j("#eligible").validate({
			invalidHandler: function(form, validator) {
		        var errors = validator.numberOfInvalids();
		        if (errors) {
		            validator.errorList[0].element.focus();
		        }
		    },
		    errorPlacement: function(error, element) {
			   if ( element.is(":radio") )
				   error.appendTo(element.parent());
			  else 
	            error.insertAfter( element);	
		    },
		    rules:{			
		    		//"cbuFinalReviewPojo.finalEligibleCreator":{required : true},
					"declNewPojo.mrqNewQues1":{validateRadioRequired:["declNewPojo.mrqNewQues1"]},
					"declNewPojo.mrqNewQues1a":{validateRadioRequired:{
						                param:["declNewPojo.mrqNewQues1a"],
										depends: function(element) {
	                                        var flag = false;
											$j('[name=declNewPojo.mrqNewQues1]').each(function(){
								                if($j(this).attr('checked'))
								                {
	                                                if($j(this).val()=='false')
	                                                flag = true;     
								                                
								                }                        
								            });
										    return flag;
										 }
									   }
					                  },
				  "declNewPojo.mrqNewQues1AddDetail":{required:{
					 						depends: function(element) {
					                        var flag = false;
											$j('[name=declNewPojo.mrqNewQues1]').each(function(){
								                if($j(this).attr('checked'))
								                {
								                	if($j(this).val()=='false')
										            flag = true;     
								                                
								                }                        
								            });
										    return flag;
										 }
									   }
					                 },									                  
					"declNewPojo.mrqNewQues2":{validateRadioRequired:["declNewPojo.mrqNewQues2"]},
					"declNewPojo.mrqNewQues2AddDetail":{required:{
								                depends: function(element) {
			                                        var flag = false;
													$j('[name=declNewPojo.mrqNewQues2]').each(function(){
										                if($j(this).attr('checked'))
										                {
										                	if($j(this).val()=='false')
												            flag = true;     
										                                
										                }                        
										            });
												    return flag;
												 }
											   }
							                  },
					"declNewPojo.mrqNewQues3":{validateRadioRequired:["declNewPojo.mrqNewQues3"]},
					"declNewPojo.mrqNewQues3AddDetail":{required:{
															     depends: function(element) {
										                          var flag = false;
																  $j('[name=declNewPojo.mrqNewQues3]').each(function(){
																	                if($j(this).attr('checked'))
																	                {
																	                	if($j(this).val()=='true')
																			            flag = true;     
																	                                
																	                }                        
																	            });
																			    return flag;
																			 }
																		   }
														                  },
				
					"declNewPojo.phyNewFindQues4":{validateRadioRequired:["declNewPojo.phyNewFindQues4"]},
					"declNewPojo.phyNewFindQues4A":{validateRadioRequired:{
																			param:["declNewPojo.phyNewFindQues4A"],
																			depends: function(element) {
															                 var flag = false;
																			 $j('[name=declNewPojo.phyNewFindQues4]').each(function(){
																					if($j(this).attr('checked'))
																						  {
															                                    if($j(this).val()=='false')
															                                                flag = true;     
																						                                
																						                }                        
																						            });
																								    return flag;
																								 }
																							   }
																			                  },
					"declNewPojo.phyNewFindQues4AddDetail":{required:{
																	depends: function(element) {
																	var flag = false;
																	$j('[name=declNewPojo.phyNewFindQues4]').each(function(){
																		if($j(this).attr('checked'))
																						   {
																						      if($j(this).val()=='false')
																								flag = true;     
																						                                
																						                }                        
																						            });
																								    return flag;
																								 }
																							   }
																			                 },			
		      "declNewPojo.phyNewFindQues5":{validateRadioRequired:["declNewPojo.phyNewFindQues5"]},
			  "declNewPojo.phyNewFindQues5AddDetail":{required:{
										                depends: function(element) {
									                      var flag = false;
															$j('[name=declNewPojo.phyNewFindQues5]').each(function(){
												                if($j(this).attr('checked'))
												                {
												                	if($j(this).val()=='true')
														            flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
									                },
			"declNewPojo.phyNewFindQues6":{validateRadioRequired:["declNewPojo.phyNewFindQues6"]},
			"declNewPojo.phyNewFindQues6a":{validateRadioRequired:{
										                param:["declNewPojo.phyNewFindQues6a"],
														depends: function(element) {
										                   var flag = false;
															$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
												                if($j(this).attr('checked'))
												                {
										                           if($j(this).val()=='true')
										                           flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
										              },
			"declNewPojo.phyNewFindQues6b":{validateRadioRequired:{
											                param:["declNewPojo.phyNewFindQues6b"],
															depends: function(element) {
											                   var flag = false;
																$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
													                if($j(this).attr('checked'))
													                {
											                           if($j(this).val()=='true')
											                           flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
											              },
			"declNewPojo.phyNewFindQues6c":{validateRadioRequired:{
												                param:["declNewPojo.phyNewFindQues6c"],
																depends: function(element) {
												                   var flag = false;
																	$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
														                if($j(this).attr('checked'))
														                {
												                           if($j(this).val()=='true')
												                           flag = true;     
														                                
														                }                        
														            });
																    return flag;
																 }
															   }
												              },
			"declNewPojo.phyNewFindQues6d":{validateRadioRequired:{
													                param:["declNewPojo.phyNewFindQues6d"],
																	depends: function(element) {
													                   var flag = false;
																		$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
															                if($j(this).attr('checked'))
															                {
													                           if($j(this).val()=='true')
													                           flag = true;     
															                                
															                }                        
															            });
																	    return flag;
																	 }
																   }
													              },
			"declNewPojo.phyNewFindQues6e":{validateRadioRequired:{
														                param:["declNewPojo.phyNewFindQues6e"],
																		depends: function(element) {
														                   var flag = false;
																			$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
																                if($j(this).attr('checked'))
																                {
														                           if($j(this).val()=='true')
														                           flag = true;     
																                                
																                }                        
																            });
																		    return flag;
																		 }
																	   }
														              },
			"declNewPojo.phyNewFindQues6f":{validateRadioRequired:{
															                param:["declNewPojo.phyNewFindQues6f"],
																			depends: function(element) {
															                   var flag = false;
																				$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
																	                if($j(this).attr('checked'))
																	                {
															                           if($j(this).val()=='true')
															                           flag = true;     
																	                                
																	                }                        
																	            });
																			    return flag;
																			 }
																		   }
															              },
			"declNewPojo.phyNewFindQues6g":{validateRadioRequired:{
																                param:["declNewPojo.phyNewFindQues6g"],
																				depends: function(element) {
																                   var flag = false;
																					$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
																		                if($j(this).attr('checked'))
																		                {
																                           if($j(this).val()=='true')
																                           flag = true;     
																		                                
																		                }                        
																		            });
																				    return flag;
																				 }
																			   }
																              },
			"declNewPojo.phyNewFindQues6h":{validateRadioRequired:{
													                param:["declNewPojo.phyNewFindQues6h"],
																	depends: function(element) {
													                   var flag = false;
																		$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
															                if($j(this).attr('checked'))
															                {
													                           if($j(this).val()=='true')
													                           flag = true;     
															                                
															                }                        
															            });
																	    return flag;
																	 }
																   }
													              },
			"declNewPojo.phyNewFindQues6i":{validateRadioRequired:{
														                param:["declNewPojo.phyNewFindQues6i"],
																		depends: function(element) {
														                   var flag = false;
																			$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
																                if($j(this).attr('checked'))
																                {
														                           if($j(this).val()=='true')
														                           flag = true;     
																                                
																                }                        
																            });
																		    return flag;
																		 }
																	   }
														              },
			"declNewPojo.phyNewFindQues6AddDetail":{required:{
											                depends: function(element) {
										                      var flag = false;
																$j('[name=declNewPojo.phyNewFindQues6]').each(function(){
													                if($j(this).attr('checked'))
													                {
													                	if($j(this).val()=='true')
															            flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
										                },
	      
	 		
			"declNewPojo.idmNewQues7":{validateRadioRequired:["declNewPojo.idmNewQues7"]},
			"declNewPojo.idmNewQues7AddDetail":{required:{
												depends: function(element) {
						                      var flag = false;
												$j('[name=declNewPojo.idmNewQues7]').each(function(){
									                if($j(this).attr('checked'))
									                {
									                	if($j(this).val()=='false')
											            flag = true;
									                }                        
									            });
											    return flag;
											 }
										   }
							                										             						                													                
		    },									             
			"declNewPojo.idmNewQues7a":{validateRadioRequired:{
													               param:["declNewPojo.idmNewQues7a"],
																	depends: function(element) {
													                  var flag = false;
																		$j('[name=declNewPojo.idmNewQues7]').each(function(){
															                if($j(this).attr('checked'))
															                {
													                          if($j(this).val()=='false')
													                          flag = true;     
															                                
															                }                        
															            });
																	    return flag;
																	 }
																   }
													             },
				"declNewPojo.idmNewQues8":{validateRadioRequired:["declNewPojo.idmNewQues8"]},		
				"declNewPojo.idmNewQues8a":{validateRadioRequired:{
		               param:["declNewPojo.idmNewQues8a"],
						depends: function(element) {
		                  var flag = false;
							$j('[name=declNewPojo.idmNewQues8]').each(function(){
				                if($j(this).attr('checked'))
				                {
		                          if($j(this).val()=='false')
		                          flag = true;     
				                                
				                }                        
				            });
						    return flag;
						 }
					   }
		             },
				"declNewPojo.idmNewQues8AddDetail":{required:{
														                depends: function(element) {
													                      var flag = false;
																			$j('[name=declNewPojo.idmNewQues8]').each(function(){
																                if($j(this).attr('checked'))
																                {
																                	if($j(this).val()=='false')
																		            flag = true;     
																                                
																                }                        
																            });
																		    return flag;
																		 }
																	   }
													                }		,
	           "declNewPojo.idmNewQues9":{validateRadioRequired:["declNewPojo.idmNewQues9"]},		
			   "declNewPojo.idmNewQues9a":{validateRadioRequired:{
				        param:["declNewPojo.idmNewQues9a"],
						depends: function(element) {
				        var flag = false;
						$j('[name=declNewPojo.idmNewQues9]').each(function(){
				        if($j(this).attr('checked'))
				        {
				             if($j(this).val()=='false')
				              flag = true;     
				        }                        
				});
				return flag;
				}
				}
				},									                
				"declNewPojo.idmNewQues9AddDetail":{required:{
	                depends: function(element) {
                      var flag = false;
						$j('[name=declNewPojo.idmNewQues9]').each(function(){
			                if($j(this).attr('checked'))
			                {
			                	if($j(this).val()=='true')
					            flag = true;     
			                                
			                }                        
			            });
					    return flag;
					 }
				   }
                }	
				},
				messages:{
					
					//"cbuFinalReviewPojo.finalEligibleCreator":{required:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
					"declNewPojo.mrqNewQues1":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},

					"declNewPojo.mrqNewQues1a":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.mrqNewQues1AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					"declNewPojo.mrqNewQues2":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.mrqNewQues2AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					"declNewPojo.mrqNewQues3":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.mrqNewQues3AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					"declNewPojo.phyNewFindQues4":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.phyNewFindQues4A":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.phyNewFindQues4AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					"declNewPojo.phyNewFindQues5":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.phyNewFindQues5AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					"declNewPojo.phyNewFindQues6":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.phyNewFindQues6a":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.phyNewFindQues6b":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.phyNewFindQues6c":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.phyNewFindQues6d":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.phyNewFindQues6e":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.phyNewFindQues6f":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.phyNewFindQues6g":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.phyNewFindQues6h":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.phyNewFindQues6i":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.phyNewFindQues6AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					
					"declNewPojo.idmNewQues7":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.idmNewQues7a":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.idmNewQues7AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					"declNewPojo.idmNewQues8":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.idmNewQues8a":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.idmNewQues8AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					"declNewPojo.idmNewQues9":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.idmNewQues9a":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declNewPojo.idmNewQues9AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"}
					}
				
			});
		

		</s:if>
		}
		<s:else>
		$j("#eligible").validate({
			
			invalidHandler: function(form, validator) {
		        var errors = validator.numberOfInvalids();
		        if (errors) {
		            validator.errorList[0].element.focus();
		        }
		    },
		    errorPlacement: function(error, element) {
			   if ( element.is(":radio") )
				   error.appendTo(element.parent());
			  else 
	            error.insertAfter( element);	
		    },
		    rules:{			
		    		//"cbuFinalReviewPojo.finalEligibleCreator":{required : true},
					"declPojo.mrqQues1":{validateRadioRequired:["declPojo.mrqQues1"]},
					"declPojo.mrqQues1a":{validateRadioRequired:{
						                param:["declPojo.mrqQues1a"],
										depends: function(element) {
	                                        var flag = false;
											$j('[name=declPojo.mrqQues1]').each(function(){
								                if($j(this).attr('checked'))
								                {
	                                                if($j(this).val()=='false')
	                                                flag = true;     
								                                
								                }                        
								            });
										    return flag;
										 }
									   }
					                  },
				  "declPojo.mrqQues1AddDetail":{required:{
							                depends: function(element) {
					                        var flag = false;
											$j('[name=declPojo.mrqQues1]').each(function(){
								                if($j(this).attr('checked'))
								                {
								                	if($j(this).val()=='false')
										            flag = true;     
								                                
								                }                        
								            });
										    return flag;
										 }
									   }
					                 },									                  
					"declPojo.mrqQues1b":{validateRadioRequired:{
							                param:["declPojo.mrqQues1b"],
											depends: function(element) {
		                                        var flag = false;
												$j('[name=declPojo.mrqQues1a]').each(function(){
									                if($j(this).attr('checked'))
									                {
									                	if($j(this).val()=='false')
											            flag = true;     
									                                
									                }                        
									            });
											    return flag;
											 }
										   }
						                  },
					"declPojo.mrqQues2AddDetail":{required:{
								                depends: function(element) {
			                                        var flag = false;
													$j('[name=declPojo.mrqQues2]').each(function(){
										                if($j(this).attr('checked'))
										                {
										                	if($j(this).val()=='true')
												            flag = true;     
										                                
										                }                        
										            });
												    return flag;
												 }
											   }
							                  },
				   "declPojo.phyFindQues3":{validateRadioRequired:["declPojo.mrqQues1"]},
				   "declPojo.phyFindQues3a":{validateRadioRequired:{
								                param:["declPojo.phyFindQues3a"],
												depends: function(element) {
						                           var flag = false;
													$j('[name=declPojo.phyFindQues3]').each(function(){
										                if($j(this).attr('checked'))
										                {
						                                   if($j(this).val()=='false')
						                                   flag = true;     
										                                
										                }                        
										            });
												    return flag;
												 }
											   }
							                  },
				"declPojo.phyFindQues3c":{validateRadioRequired:{
									                param:["declPojo.phyFindQues3c"],
													depends: function(element) {
							                           var flag = false;
														$j('[name=declPojo.phyFindQues3]').each(function(){
											                if($j(this).attr('checked'))
											                {
							                                   if($j(this).val()=='false')
							                                   flag = true;     
											                                
											                }                        
											            });
													    return flag;
													 }
												   }
								                  },
				"declPojo.phyFindQues3AddDetail":{required:{
										                depends: function(element) {
					                                        var flag = false;
															$j('[name=declPojo.phyFindQues3]').each(function(){
												                if($j(this).attr('checked'))
												                {
												                	if($j(this).val()=='false')
														            flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
									                  },
				"declPojo.phyFindQues3b":{validateRadioRequired:{
											                param:["declPojo.phyFindQues3b"],
															depends: function(element) {
									                           var flag = false;
																$j('[name=declPojo.phyFindQues3c]').each(function(){
													                if($j(this).attr('checked'))
													                {
									                                   if($j(this).val()=='false')
									                                   flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
										                 },
				"declPojo.phyFindQues3d":{validateRadioRequired:{
												                param:["declPojo.phyFindQues3d"],
																depends: function(element) {
										                           var flag = false;
																	$j('[name=declPojo.phyFindQues3c]').each(function(){
														                if($j(this).attr('checked'))
														                {
										                                   if($j(this).val()=='false')
										                                   flag = true;     
														                                
														                }                        
														            });
																    return flag;
																 }
															   }
											                  },
		      "declPojo.phyFindQues4":{validateRadioRequired:["declPojo.phyFindQues4"]},
			  "declPojo.phyFindQues4AddDetail":{required:{
										                depends: function(element) {
									                      var flag = false;
															$j('[name=declPojo.phyFindQues4]').each(function(){
												                if($j(this).attr('checked'))
												                {
												                	if($j(this).val()=='true')
														            flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
									                },
			"declPojo.phyFindQues5":{validateRadioRequired:["declPojo.phyFindQues5"]},
			"declPojo.phyFindQues5a":{validateRadioRequired:{
										                param:["declPojo.phyFindQues5a"],
														depends: function(element) {
										                   var flag = false;
															$j('[name=declPojo.phyFindQues5]').each(function(){
												                if($j(this).attr('checked'))
												                {
										                           if($j(this).val()=='true')
										                           flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
										              },
			"declPojo.phyFindQues5b":{validateRadioRequired:{
											                param:["declPojo.phyFindQues5b"],
															depends: function(element) {
											                   var flag = false;
																$j('[name=declPojo.phyFindQues5]').each(function(){
													                if($j(this).attr('checked'))
													                {
											                           if($j(this).val()=='true')
											                           flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
											              },
			"declPojo.phyFindQues5c":{validateRadioRequired:{
												                param:["declPojo.phyFindQues5c"],
																depends: function(element) {
												                   var flag = false;
																	$j('[name=declPojo.phyFindQues5]').each(function(){
														                if($j(this).attr('checked'))
														                {
												                           if($j(this).val()=='true')
												                           flag = true;     
														                                
														                }                        
														            });
																    return flag;
																 }
															   }
												              },
			"declPojo.phyFindQues5d":{validateRadioRequired:{
													                param:["declPojo.phyFindQues5d"],
																	depends: function(element) {
													                   var flag = false;
																		$j('[name=declPojo.phyFindQues5]').each(function(){
															                if($j(this).attr('checked'))
															                {
													                           if($j(this).val()=='true')
													                           flag = true;     
															                                
															                }                        
															            });
																	    return flag;
																	 }
																   }
													              },
			"declPojo.phyFindQues5e":{validateRadioRequired:{
														                param:["declPojo.phyFindQues5e"],
																		depends: function(element) {
														                   var flag = false;
																			$j('[name=declPojo.phyFindQues5]').each(function(){
																                if($j(this).attr('checked'))
																                {
														                           if($j(this).val()=='true')
														                           flag = true;     
																                                
																                }                        
																            });
																		    return flag;
																		 }
																	   }
														              },
			"declPojo.phyFindQues5f":{validateRadioRequired:{
															                param:["declPojo.phyFindQues5f"],
																			depends: function(element) {
															                   var flag = false;
																				$j('[name=declPojo.phyFindQues5]').each(function(){
																	                if($j(this).attr('checked'))
																	                {
															                           if($j(this).val()=='true')
															                           flag = true;     
																	                                
																	                }                        
																	            });
																			    return flag;
																			 }
																		   }
															              },
			"declPojo.phyFindQues5g":{validateRadioRequired:{
																                param:["declPojo.phyFindQues5g"],
																				depends: function(element) {
																                   var flag = false;
																					$j('[name=declPojo.phyFindQues5]').each(function(){
																		                if($j(this).attr('checked'))
																		                {
																                           if($j(this).val()=='true')
																                           flag = true;     
																		                                
																		                }                        
																		            });
																				    return flag;
																				 }
																			   }
																              },
			"declPojo.phyFindQues5h":{validateRadioRequired:{
													                param:["declPojo.phyFindQues5h"],
																	depends: function(element) {
													                   var flag = false;
																		$j('[name=declPojo.phyFindQues5]').each(function(){
															                if($j(this).attr('checked'))
															                {
													                           if($j(this).val()=='true')
													                           flag = true;     
															                                
															                }                        
															            });
																	    return flag;
																	 }
																   }
													              },
			"declPojo.phyFindQues5i":{validateRadioRequired:{
														                param:["declPojo.phyFindQues5i"],
																		depends: function(element) {
														                   var flag = false;
																			$j('[name=declPojo.phyFindQues5]').each(function(){
																                if($j(this).attr('checked'))
																                {
														                           if($j(this).val()=='true')
														                           flag = true;     
																                                
																                }                        
																            });
																		    return flag;
																		 }
																	   }
														              },
			"declPojo.phyFindQues5AddDetail":{required:{
											                depends: function(element) {
										                      var flag = false;
																$j('[name=declPojo.phyFindQues5]').each(function(){
													                if($j(this).attr('checked'))
													                {
													                	if($j(this).val()=='true')
															            flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
										                },
	       "declPojo.idmQues6":{validateRadioRequired:["declPojo.idmQues6"]},
			"declPojo.idmQues6a":{validateRadioRequired:{
										               param:["declPojo.idmQues6a"],
														depends: function(element) {
										                  var flag = false;
															$j('[name=declPojo.idmQues6]').each(function(){
												                if($j(this).attr('checked'))
												                {
										                          if($j(this).val()=='false')
										                          flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
										             },
	 			"declPojo.idmQues6AddDetail":{required:{
										                depends: function(element) {
									                      var flag = false;
															$j('[name=declPojo.idmQues6]').each(function(){
												                if($j(this).attr('checked'))
												                {
												                	if($j(this).val()=='false')
														            flag = true;     
												                                
												                }                        
												            });
														    return flag;
														 }
													   }
									                },
				"declPojo.idmQues6b":{validateRadioRequired:{
											               param:["declPojo.idmQues6b"],
															depends: function(element) {
											                  var flag = false;
																$j('[name=declPojo.idmQues6a]').each(function(){
													                if($j(this).attr('checked'))
													                {
											                          if($j(this).val()=='false')
											                          flag = true;     
													                                
													                }                        
													            });
															    return flag;
															 }
														   }
											             },
			"declPojo.idmQues7a":{validateRadioRequired:{
												               param:["declPojo.idmQues7a"],
																depends: function(element) {
												                  var flag = false;
																	$j('[name=declPojo.idmQues7]').each(function(){
														                if($j(this).attr('checked'))
														                {
												                          if($j(this).val()=='false')
												                          flag = true;     
														                                
														                }                        
														            });
																    return flag;
																 }
															   }
												             },
			"declPojo.idmQues7AddDetail":{required:{
								                depends: function(element) {
						                      var flag = false;
												$j('[name=declPojo.idmQues7]').each(function(){
									                if($j(this).attr('checked'))
									                {
									                	if($j(this).val()=='false')
											            flag = true;
									                }                        
									            });
											    return flag;
											 }
										   }
							                										             						                													                
		    },									             
			"declPojo.idmQues7b":{validateRadioRequired:{
													               param:["declPojo.idmQues7b"],
																	depends: function(element) {
													                  var flag = false;
																		$j('[name=declPojo.idmQues7a]').each(function(){
															                if($j(this).attr('checked'))
															                {
													                          if($j(this).val()=='false')
													                          flag = true;     
															                                
															                }                        
															            });
																	    return flag;
																	 }
																   }
													             },
				"declPojo.idmQues8AddDetail":{required:{
														                depends: function(element) {
													                      var flag = false;
																			$j('[name=declPojo.idmQues8]').each(function(){
																                if($j(this).attr('checked'))
																                {
																                	if($j(this).val()=='true')
																		            flag = true;     
																                                
																                }                        
																            });
																		    return flag;
																		 }
																	   }
													                }
													               
													                								             						                													                
				},
				messages:{
					
					//"cbuFinalReviewPojo.finalEligibleCreator":{required:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.mandatoryField"/>"},
					"declPojo.mrqQues1":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},

					"declPojo.mrqQues1a":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.mrqQues1AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					"declPojo.mrqQues1b":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.mrqQues2AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					"declPojo.phyFindQues3":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues3a":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues3c":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues3AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					"declPojo.phyFindQues3b":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues3b":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues4":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues5":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues5a":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues5b":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues5c":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues5d":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues5e":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues5f":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues5g":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues5h":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues5i":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.phyFindQues5AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					"declPojo.idmQues6":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.idmQues6a":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.idmQues6AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					"declPojo.idmQues6b":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.idmQues7a":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					"declPojo.idmQues7AddDetail":{required:"<br/><s:text name="garuda.common.validation.value"/>"},
					"declPojo.idmQues7b":{validateRadioRequired:"<br/>&nbsp;&nbsp;<s:text name="garuda.cbu.cordentry.question"/>"},
					 "declPojo.idmQues8AddDetail":{required:"<br/><s:text name="garuda.cbu.cordentry.question"/>"},
					}
				
			
		});
		</s:else>
		
	}
	function validateEligibility(){
		var validate=true;
		if($j("#fkCordCbuEligible_1").val()==null || $j("#fkCordCbuEligible_1").val()==""){
				$j("#cordEligible_error").css('display','block');
				validate=false;
		}
		if($j("#fkCordCbuEligible_1").val()==$j("#incompleteReasonId").val()){
			if($j("#fkCordCbuIncompleteReason > option:selected").length == 0) {
				$j('#inCompReasons_error').css('display','block');
				validate=false;
			}
			
		}
		if($j("#fkCordCbuEligible_1").val()==$j("#inEligiblePk").val()){
			
			if($j("#fkCordCbuEligibleReason > option:selected").length == 0) {
				
				$j('#inEligibleReasons_error').css('display','block');
				validate=false;
			}
		}
		if(($j("#cordAdditionalInfo").val()==null || $j("#cordAdditionalInfo").val()=="") && ($j("#fkCordCbuEligible_1").val()==$j("#notCollectedToPriorReasonId").val())){
			$j('#cordAdditionalInfo_error').css('display','block');
			validate=false;
		}
		return validate;
	}
	function fn_eligibleError(id){
		$j('#'+id).css('display','none');
	}
	
	</script>
</s:if>
<script>
   //$j('#searchResults').dataTable();  

   $j('.prior').click(function(){
	   var checked;
	   $j('input:checkbox').each(function(){
		   if($j(this).attr('checked')){
		    	checked = true;    	   
		    }
	   });
	   if(checked==true){
		   $j('#finalelig').css('display','block');	 
	        
	   }else{
		   $j('#finalelig').css('display','none');
	   }
   }); 
	$j(function(){
		
		if('<s:property value="CdrCbuPojo.eligibilitydes" />'=='Not Applicable'){
			if($j("#newQnaire").length>0){
				$j("#newQnaire").hide();
				$j("#summTxt").hide();
			}
		}
		if(('<s:property value="esignFlag"/>'=='reviewCriView') || ('<s:property value="esignFlag"/>'=='reviewCriEdit')){
			
			inEligibleinCompleteReason($j("#fkCordCbuEligible_1").val());
		}
		if(('<s:property value="esignFlag"/>'=='review')){
			$j('#inEliReason_1').hide();
			$j('#inComReason_1').hide();
			$j('#additonalinfo').hide();
			$j('#inEliReason_1Txt').hide();
			$j('#inComReason_1Txt').hide();
			$j('#additonalinfoTxt').hide();
			}
		});
   function eligibleSubmitFrom(){
	   
	   if(('<s:property value="fkCordCbuEligible"/>' != $j("#notCollectedToPriorReasonId").val()) && ('<s:property value="#request.shipMentFlag"/>'=="true")){
	   		if(($j("#fkCordCbuEligible_1").val()==$j("#notCollectedToPriorReasonId").val()) && formValueChk("eligInclude")){
	   			$j('#blckaction').val("1");
	   			notEligibilityApplicable($j("#fkCordCbuEligible_1").val());
	   		}
	   		else{
	   				$j('#blckaction').val("0");
	   			}
	   	}
	   if($j('#blckaction').val()!="1"){
	    var elgForChangesDone = divChanges('elgFcrChanges',$j('#cbuFinalEligDiv').html());
	   
	   if(('<s:property value="esignFlag"/>'!='reviewCriEdit')){
	  
	    $j('#eligibleerror').html('');
	   var inEli = $j('#reasonsInEliCom').val();
	   inEli = $j.trim(inEli);
	   if($j('#eligibleSta').val()==null || $j('#eligibleSta').val()==""){
		   $j('#eligibleerror').html('<s:text name="garuda.cbu.fcr.eligible.req"/>');
		   }
	   else if($j('#eligibleSta').val()==$j('#ineligibleStaPK').val() && (inEli=="0" || inEli=="")){
		   $j('#eligibleerror').html('<s:text name="garuda.cbu.fcr.ineligible.req"/>');
		   }
	   else if($j('#eligibleSta').val()==$j('#incompleteStaPK').val() && (inEli=="0" || inEli=="")){
		   $j('#eligibleerror').html('<s:text name="garuda.cbu.fcr.incomplete.req"/>');
		   }
	   }
	   
		   if(('<s:property value="esignFlag"/>'=='reviewCriEdit')){
			   $j('#esignFlag').val('criEditModal');
			   var noMandatory=validateEligibility();
			   if(('<s:property value="cdrCbuPojo.site.isReqElgQuest()"/>'=='true') && noMandatory==true){
				   noMandatory=$j("#eligible").valid(validatesReasons());
				   
				}
				if(noMandatory){
					var url;
					if($j("#licResetChangeFlag").val()!=null && $j("#licResetChangeFlag").val()!=""){
			   			url = 'submitEligibleCri?&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId=<s:property value="cdrCbuPojo.cordID"/>'+'&noMandatory='+noMandatory+'&criChangeFlag='+true+'&licResetChangeFlag='+$j('#licResetChangeFlag').val();
					}
					else{
			   			url = 'submitEligibleCri?&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId=<s:property value="cdrCbuPojo.cordID"/>'+'&noMandatory='+noMandatory+'&criChangeFlag='+true;
					}
			  
			   if(elgForChangesDone){
			   modalFormPageRequestCRI('eligible',url,'finalreviewmodel','<s:property value="cdrCbuPojo.cordID"/>',$j("#orderId").val(),$j("#orderType").val(),'updateEligible1','eligibleContent');
			   }
			   else{
				   showSuccessdiv('updateEligible1','eligibleContent');
				   }
				}
		   }
		   else{
			   
			   var noMandFlag;
			   if($j("#flagValForElig").val()=='1'){
				   noMandFlag=true;
			   }
			   else{
				   noMandFlag=false;
			   }
			   
			   if(noMandFlag){
					if(elgForChangesDone || $j('#FCRreviewConfirmFlg').val()!=1){
					
						refreshDiv1('submitEligibleFinalReview?esignFlag=review&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId=<s:property value="cdrCbuPojo.cordID"/>','first_finalreivew','eligible');
					}else{
						hideDiv();
					}
			   	}
			    else{
				   $j('#EligibleQues_error').css('display','block');
			    }
			   
		   }
	   
	   if(('<s:property value="esignFlag"/>'!='reviewCriEdit')){
	   if(elgForChangesDone || $j('#FCRreviewConfirmFlg').val()!=1){
		   if($j("#flagValForElig").val()!='1'){
				RefreshPrevious('<s:property value="cdrCbuPojo.cordID"/>');
		   }
	   }else{
		   hideDiv();
		   }
	   }
	   
	}
   }
   function modalFormPageRequestCRI(formid,url,divname,cordid,orderid,ordertype,updateDivId,statusDivId){
		showprogressMgs();
		$j.ajax({
	        type: "POST",
	        url: url,
	       // async:false,
	        data : $j("#"+formid).serialize(),
	        success: function (result){
	        	$j('.ui-datepicker').html("");
	        	//$('.tabledisplay').html("");
	            var $response=$j(result);
	            //query the jq object for the values
	            var errorpage = $response.find('#errorForm').html();
	           // alert(oneval);
	           // var subval = $response.find('#sub').text();
	            if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{
	            	if(divname!=""){ 
		            	 $j("#"+divname).html(result);
		            }
	            	if(updateDivId!=undefined && updateDivId!=""){
	            		$j("#"+updateDivId).css('display','block');			            	
	            	}else{
	            		$j("#update").css('display','block');			            	
	            	}
	            	if(statusDivId!=undefined && statusDivId!=""){
	            		$j("#"+statusDivId).css('display','none');			            	
	            	}else{
	            		$j("#status").css('display','none');			            	
	            	}
	            	/*if(orderid!=null && orderid!="" && typeof(orderid)!="undefined" && ordertype!=null && ordertype!="" && typeof(ordertype)!="undefined"){
	                	var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID='+cordid+'&orderId='+orderid+'&orderType='+ordertype+'&pkcordId='+cordid+'&currentReqFlag=true';
	            		loadMoredivs(urlb,'viewcbutoppanel,quicklinksDiv,taskCompleteFlagsDiv,ctReqClinInfoContentDiv,chistory,cReqhistory,finalreviewcontentDiv,currentRequestProgressMainDiv','cbuProcedureMain','update','status');//url,divname,formId,updateDiv,statusDiv
	            		setpfprogressbar();
				 		getprogressbarcolor();
				 		historyTblOnLoad();
	                }
	            	else{
	            	var urla = "loadClinicalData?cdrCbuPojo.cordID="+cordid;
	            	loadPageByGetRequset(urla,'cordSelectedData');
	            	}*/		            	
	            }
	        },
	        error: function (request, status, error) {
	        	closeprogressMsg();
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});	
		closeprogressMsg();
		}
	function checkPrior(){
		var checked;
		$j('#cbuFinalEligDiv').find('input:checkbox').each(function(){
			if($j(this).attr('checked')){
		    	checked = true;    	   
		    }
		});
		 if(checked==true){
			   $j('#finalelig').css('display','block');	 
		        
		   }else{
			   $j('#finalelig').css('display','none');
		   }
	}
	function getDbinElgReasons(thisVal,classname){
		isChangeDone(thisVal,$j('#inElgDbReasons').val(),classname);
	}
	function getDbinCompReasons(thisVal,classname){
		isChangeDone(thisVal,$j('#inCompDbReasons').val(),classname);
	}
	function notEligibilityApplicable(val){
		var noMandatory=validateEligibility();
		if(noMandatory){
		if(val==$j("#notCollectedToPriorReasonId").val()){
			$j( "#dialog-confirm-1" ).dialog({
				resizable: false,
				modal: true,
				closeText: '',
				closeOnEscape: false ,
				buttons: {
					"Yes": function() {
						$j( this ).dialog( "close" );
						$j('#esignFlag').val('submitNAEligibleCri');
						/* $j("#newQnaire :input:not(:button)").each(function(){
							  	if($j(this).attr('type')!="radio"){
							 		$j(this).val('');
							  	}else{
								 	 $j(this).removeAttr('checked');
									}
							  	}); */
						var url = 'submitEligibleCri?&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId=<s:property value="cdrCbuPojo.cordID"/>'+'&criChangeFlag='+true+'&noMandatory'+noMandatory;
						modalFormPageRequestCRI('eligible',url,'finalreviewmodel','<s:property value="cdrCbuPojo.cordID"/>',$j("#orderId").val(),$j("#orderType").val(),'updateEligible1','eligibleContent');
					},
					"No": function() {
						$j( this ).dialog( "close" );
						$j('#blckaction').val("0");
						if('<s:property value="fkCordCbuEligible"/>'!=""){
							$j('#fkCordCbuEligible_1').val('<s:property value="fkCordCbuEligible"/>');
							inEligibleinCompleteReason('<s:property value="fkCordCbuEligible"/>');
						}
						else{
							$j('#fkCordCbuEligible_1').val('-1');
							inEligibleinCompleteReason('-1');
						}
					}
				}
		
			});
		}
	}
	}
	function inEligibleinCompleteReason(val){
		
		if(val=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON)"/>'){
			$j('#inEliReason_1').show();
			$j('#inEliReason_1Txt').show();
			$j('#inComReason_1').hide();
			$j('#additonalinfo').hide();
			$j("#word_count").val("");
			$j('#additonalinfoTxt').hide();
			$j('#inComReason_1Txt').hide();
			if($j("#newQnaire").length>0){
				$j("#newQnaire").show();
				$j("#summTxt").show();
			}
			}
		else if(val=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@INCOMPLETE_REASON)"/>'){
			$j('#inEliReason_1').hide();
			$j('#inComReason_1').show();
			$j('#inComReason_1Txt').show();
			$j('#additonalinfo').hide();
			$j("#word_count").val("");
			$j('#additonalinfoTxt').hide();
			$j('#inEliReason_1Txt').hide();
			if($j("#newQnaire").length>0){
				$j("#newQnaire").show();
				$j("#summTxt").show();
			}
			}
		else if(val=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBLE)"/>'){
			$j('#inEliReason_1').hide();
			$j('#inComReason_1').hide();
			$j('#additonalinfo').hide();
			$j("#word_count").val("");
			$j('#additonalinfoTxt').hide();
			$j('#inComReason_1Txt').hide();
			$j('#inEliReason_1Txt').hide();
			if($j("#newQnaire").length>0){
				$j("#newQnaire").show();
				$j("#summTxt").show();
			}
			}
		else if(val==$j("#notCollectedToPriorReasonId").val()){
			
			$j('#inEliReason_1').hide();
			$j('#inComReason_1').hide();
			$j('#additonalinfo').show();
			$j('#additonalinfoTxt').show();
			$j('#inComReason_1Txt').hide();
			$j('#inEliReason_1Txt').hide();
			if($j("#newQnaire").length>0){
				$j("#newQnaire").hide();
				$j("#summTxt").hide();
			}
			}
		else{
			$j('#inEliReason_1').hide();
			$j('#inComReason_1').hide();
			$j('#additonalinfo').hide();
			$j("#word_count").val("");
			$j('#additonalinfoTxt').hide();
			$j('#inComReason_1Txt').hide();
			$j('#inEliReason_1Txt').hide();
			if($j("#newQnaire").length>0){
				$j("#newQnaire").show();
				$j("#summTxt").show();
			}
		}
	}
	
	function updateElgStas(title,url,bredth,length){
		//$j('#finalDeclQstns').html('');
		var patientId = $j('#patientId').val();
		var patient_data = '';
		if(patientId!="" && patientId != null && patientId != "undefined"){
			patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
		}
		title=title+patient_data;
		showModal(title,url,bredth,length);
	}
	
	function fn_closeDiv(){
		
		if(('<s:property value="esignFlag"/>'=='review')){
		$j("#cbuFinalEligDiv").css('display','none');
		}
		else{
			closeModal();
			}
	}
function resetingProcess(val){
		if('<s:property value="cdrCbuPojo.cbuLicStatus"/>'=='<s:property value="#request.licensedPK"/>' && (val=='<s:property value="cdrCbuPojo.inEligiblePkid"/>'||val=='<s:property value="cdrCbuPojo.incompleteReasonId"/>')){


			$j( "#dialog-resetElig-11" ).dialog({
				resizable: false,
				modal: true,
				closeText: '',
				closeOnEscape: false ,
				buttons: {
					"Yes": function() {
						$j( this ).dialog( "close" );
						
						 //var url = 'updateFinalEligibleStatus?module=CBU&esignFlag=modal&resetFun=reset&licenceUpdate=True&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&fkCordCbuEligible='+val;
					    	//if('<s:property value="resetFun"/>'=="reset"){
						    	//url=url+'&resetAll=resetOnce';
					    	//}
					    	//else{
					    		//url=url+'&resetAll=resetAll';
						    //}
						    $j("#licResetChangeFlag").val("true");
					    	var input = document.createElement("input");
					    	input.setAttribute("type", "hidden");
					    	input.setAttribute("name", "cdrCbuPojo.cbuLicStatus");
					    	input.setAttribute("id","cbuLicStatus");
					    	input.setAttribute("value", '<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON)"/>');
					    	document.getElementById("licDyn1").appendChild(input);
					    	input = document.createElement("input");
					    	input.setAttribute("type", "hidden");
					    	input.setAttribute("name", "licReasons");
					    	input.setAttribute("id","fkCordCbuUnlicenseReason");
					    	input.setAttribute("value", '<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON_INTERNATIONAL)"/>');
					    	document.getElementById("licDyn1").appendChild(input);
					    	//showModal('Eligibility Determination',url,'390','700');
					    	
					},
					"No": function() {
						$j( this ).dialog( "close" );
						$j('#fkCordCbuEligible_1').val('<s:property value="fkCordCbuEligible"/>');
					    inEligibleinCompleteReason('<s:property value="fkCordCbuEligible"/>');
					}
				}
			});
		}
	}
	$j(document).ready(function(){
		if(($j("#esignFlag").val()=="reviewCriView") || ($j("#esignFlag").val()=="reviewCriEdit")){
			$j('#eligibiltyDetailsDiv .criHighlightFld').each(function(){
				 $j(this).css("border", "1px solid #DBDAD9");
				 var fieldVal = $j(this).val();
				 fieldVal = $j.trim(fieldVal);
				 if($j(this).is(':visible')){
				 	if(fieldVal==null || fieldVal=="" || fieldVal=="-1"){
					 	$j(this).css("border", "1px solid red");
					}
				 }
				 });
		}
		if('<s:property value="resetFun"/>'=="reset"){
			$j('#fkCordCbuEligible_1').change(function(){
				resetingProcess(this.value);
			});
		}else{
			$j('#fkCordCbuEligible_1').change(function(){
				resetingProcess(this.value);
			});
			}
		});
</script>
<div id="dialog-resetElig-11" style="display: none;">
<table><tr><td><img src="images/warning.png"/></td><td><s:text name="garuda.update.license.resetConfirm"/></td></tr></table>
</div>
<s:set name="tempVal" value="cbuFinalReviewPojo.finalReviewConfirmStatus"></s:set>
<div id="cbuFinalEligDiv">
<s:form id="eligible">
<s:hidden name="cdrCbuPojo.inEligiblePkid" id="inEligiblePk"></s:hidden>
<s:hidden name="cdrCbuPojo.incompleteReasonId" id="incompleteReasonId" ></s:hidden>
<s:hidden name="flagValForElig" id="flagValForElig"></s:hidden>
<s:hidden name="cdrCbuPojo.notCollectedToPriorReasonId" id="notCollectedToPriorReasonId" ></s:hidden>
<s:hidden name="licenceUpdate" id="licenceUpdate" value="True"></s:hidden>
<s:hidden name="cdrCbuPojo.cordID" />
<s:hidden name="esignFlag" id="esignFlag" />
<s:hidden name="orderId" id="orderId" />
<s:hidden name="orderType" id="orderType"></s:hidden>
<s:hidden name="licResetChangeFlag" id="licResetChangeFlag"></s:hidden>
<s:hidden name="blckaction" id="blckaction" value="0"></s:hidden>
<div id="licDyn1"></div>
<div id="updateEligible1" style="display: none;" >
	  <jsp:include page="../cb_update.jsp">
	    <jsp:param value="garuda.message.modal.eligibilityCRI_deter" name="message"/>
	  </jsp:include>	   
	</div>	
<table id="eligibleContent" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
		 <s:if test="(esignFlag != 'reviewCriView') && (esignFlag != 'reviewCriEdit')">
			<div id="eligiblecontent" 
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><s:text
				name="garuda.cdrcbuview.label.eligibility_determination"></s:text></div>
				</s:if>
				
		</td>
	</tr>
			
	   <tr><td colspan="2" width="100%"><table width="100%" id="finalDeclQstns">
	   <tr>
	        <td colspan="2">
				<strong><s:text id="summTxt" name="garuda.common.label.summarize"/></strong>
	        </td>
	    </tr>
	    <tr id="eligInclude">
	        <td colspan="2">
	        <s:if test = "#request.shipMentFlag==true">
	        <div id="newQnaire">
	           		<s:if test="esignFlag == 'review'">
	           		<s:hidden name="declNewPojo.pkNewEligDecl"></s:hidden>
	            	<jsp:include page="../cb_view_new_final_decl_quest.jsp"/>
	       </s:if>
	       <s:else>
	       			
	       			<s:hidden name="declNewPojo.pkNewEligDecl"></s:hidden>
	       			
	            	<jsp:include page="../cb_new_final_decl_quest.jsp">
	            	
	            	<jsp:param value="true" name="readonly"/>
	            	</jsp:include>
	            	
	       </s:else>
	       </div>
	        </s:if>
	        <s:else>
	        <div id="oldQnaire">
	        <s:if test="esignFlag == 'review'">
	        
	        		<s:hidden name="declNewPojo.pkNewEligDecl"></s:hidden>
	            	<jsp:include page="../cord-entry/cb_view_final_decl_quest.jsp"/>
	       </s:if>
	       <s:else>
	       			<s:hidden name="declPojo.pkEligDecl"></s:hidden>
	            	<jsp:include page="../cord-entry/cb_final_decl_quest.jsp"/>
	            	</s:else>
	        </div>
	        </s:else>
	        <span style="display:none; color:red;" id="EligibleQues_error"><s:text name="garuda.cbu.fcr.eligques.req"/></span>
	        </td>
	    </tr>
	    </table></td></tr>
	    <br></br>
	    <br></br>
    
    <tr>
    	<td colspan="2">
    	<div id="eligibiltyDetailsDiv">
    	<input type="hidden" id="eligibleSta" value='<s:property value="cdrCbuPojo.fkCordCbuEligible"/>'/>
		<input type="hidden" id="ineligibleStaPK" value='<s:property value="cdrCbuPojo.inEligiblePkid"/>'/>
		<input type="hidden" id="incompleteStaPK" value='<s:property value="cdrCbuPojo.incompleteReasonId"/>'/>
		<input type="hidden" id="reasonsInEliCom" value='<s:property value="#request.reasonPojos.size()"/> '/>
		<input type="hidden" id="criCompFlg" value='<s:property value="cbuCompleteReqInfoPojo.completeReqinfoFlag"/> ' />
		<input type="hidden" id="criMrqFlg" value='<s:property value="cbuCompleteReqInfoPojo.mrqFlag"/> ' />
		<input type="hidden" id="FCRreviewConfirmFlg" value='<s:property value="cbuFinalReviewPojo.eligibleConfirmFlag"/> ' />
     	<table>
    		<tr>
    		
        <td width="35%">
             <s:text name="garuda.cdrcbuview.label.eligibility_determination" />:
        </td>
        <td width="65%" align="left">
         <s:if test="(esignFlag != 'reviewCriView') && (esignFlag != 'reviewCriEdit')">
         
           <s:property value="CdrCbuPojo.eligibilitydes" />
           </s:if>
           <s:else><s:select name="fkCordCbuEligible" cssClass="criHighlightFld" cssStyle="width:390px;" id="fkCordCbuEligible_1" list="%{getListOfCodeLstObj(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS,cdrCbuPojo.cordID)}"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select" onchange="inEligibleinCompleteReason(this.value);isChangeDone(this,'%{fkCordCbuEligible}','elgFcrChanges'); fn_eligibleError('cordEligible_error');" />
				<span style="display:none; color:red;" id="cordEligible_error"><s:text name="garuda.cbu.cordentry.cordeligible"/></span>
				</s:else>
        </td>
		 <s:if test="hasEditPermission(#request.conElgble)==true">  
		 <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
  		  </s:if>
   			 <s:else>
   			 <s:if test="(esignFlag != 'reviewCriView') && (esignFlag != 'reviewCriEdit')">
       <td><button type="button" onclick="updateElgStas('<s:text name="garuda.cdrcbuview.label.eligibility_determination"/>'+' <s:text name="garuda.message.modal.headerregid"/>:<s:property value="cdrCbuPojo.registryId" />','updateFinalEligibleStatus?module=CBU&esignFlag=modalFCRElig&resetFun=reset&licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR_EDIT_ELIGIBLE" />','390','900');"><s:text name="garuda.common.lable.edit" /></button></td>
       </s:if>
       </s:else>
       </s:if>
    </tr>
    <tr>
    	<td colspan="2" align="center"><span class="error"><label id="eligibleerror"></label> </span></td>
    </tr>
   
        <tr>
        <s:if test="(esignFlag == 'reviewCriView') || (esignFlag == 'reviewCriEdit')">
	         <td valign="top" id="inEliReason_1Txt">
	             <s:text name="garuda.cdrcbuview.label.ineligible_reason" /> :
	        </td>
	        </s:if>
        	 <s:elseif test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.inEligiblePkid">
	        <td valign="top">
	             <s:text name="garuda.cdrcbuview.label.ineligible_reason" /> :
	        </td>
	        </s:elseif>
	       
	        <td align="left">
	        <s:if test="(esignFlag == 'reviewCriView') || (esignFlag == 'reviewCriEdit')">
	           <table id="inEliReason_1" width="100%" cellpadding="0" cellspacing="0">
	           <tr>
	           <td>
	           <s:select cssStyle="height:100px;width:390px;" cssClass="criHighlightFld" name="inEligibleReasons" id="fkCordCbuEligibleReason" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON]"
							listKey="pkCodeId" listValue="description"  value="inEligibleReasons" onchange="getDbinElgReasons(this,'elgFcrChanges'); fn_eligibleError('inEligibleReasons_error');"
							  multiple="true"/>
							  <span style="display:none; color:red;" id="inEligibleReasons_error"><s:text name="garuda.cbu.clinicalnote.reason"/></span>
							  </td>
							  </tr>
							  </table>
							  </s:if>
	        <s:elseif test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.inEligiblePkid">
	        
	           <table width="100%" cellpadding="0" cellspacing="0">
		             <s:iterator value="#request.reasonPojos" >
		             <s:set name="reasonId" value="fkReasonId" />
		              <tr>
		                  <td>
		                     <strong><s:property value="getCodeListDescById(#reasonId)" /></strong>
		                  </td>
		              </tr>
			        </s:iterator>
	           </table>	
	           
	           </s:elseif>
	           
			           
	        </td>
	   </tr>
	
	
	
	 <tr>
	 		 <s:if test="(esignFlag == 'reviewCriView') || (esignFlag == 'reviewCriEdit')">
	        <td valign="top" id="inComReason_1Txt">
	            <s:text name="garuda.cdrcbuview.label.incomplete_reason" /> :
	        </td>
	        </s:if>
	 		 <s:elseif test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId">
	        <td valign="top">
	            <s:text name="garuda.cdrcbuview.label.incomplete_reason" /> :
	        </td>
	        </s:elseif>
	        
	        <td align="left">
	        <s:if test="(esignFlag == 'reviewCriView') || (esignFlag == 'reviewCriEdit')">
	           
	            <table id="inComReason_1" width="100%"  cellpadding="0" cellspacing="0">
	           
	            <tr>
	            <td>
	           <s:select cssStyle="height:100px;width:390px;" cssClass="criHighlightFld" name="inCompReasons" id="fkCordCbuIncompleteReason" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@INCOMPLETE_REASON]"
							listKey="pkCodeId" listValue="description"  value="inCompReasons" onchange="getDbinCompReasons(this,'elgFcrChanges'); fn_eligibleError('inCompReasons_error');"
							  multiple="true"/>
							  <span style="display:none; color:red;" id="inCompReasons_error"><s:text name="garuda.cbu.clinicalnote.reason"/></span>
				</td>
				</tr>
				</table>
	           </s:if>
	        <s:elseif test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId">
	           <table width="100%"  cellpadding="0" cellspacing="0">
		             <s:iterator value="#request.reasonPojos" >
		             <s:set name="reasonId" value="fkReasonId" />
		              <tr>
		                  <td>
		                     <strong><s:property value="getCodeListDescById(#reasonId)" /></strong>
		                  </td>
		              </tr>
			        </s:iterator>
	           </table>	           
	           </s:elseif>
	           
	        </td>
	   </tr>
	     <tr>
	     		<s:if test="(esignFlag == 'reviewCriView') || (esignFlag == 'reviewCriEdit')">
				<td valign="top" id="additonalinfoTxt">
				<s:text name="garuda.cdrcbuview.label.ineligible_reason_notes" />:
				</td>
				</s:if>
	     		<s:elseif test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.NotCollectedToPriorReasonId">
				<td valign="top" >
				<s:text name="garuda.cdrcbuview.label.ineligible_reason_notes" />:
				</td>
				</s:elseif>
				
				<s:if test="(esignFlag == 'reviewCriView') || (esignFlag == 'reviewCriEdit')">
				<td id="additonalinfo"><textarea name="cordAdditionalInfo" class="criHighlightFld" id="cordAdditionalInfo" maxlength="300" cols="73" rows="5" id="word_count" cssStyle="width:390px;" onchange="isChangeDone(this,'<s:property value="%{cordAdditionalInfo}" escapeJavaScript="true"/>','elgFcrChanges'); fn_eligibleError('cordAdditionalInfo_error');"><s:property value="%{cordAdditionalInfo}"/></textarea>
				 <span style="display:none; color:red;" id="cordAdditionalInfo_error"><s:text name="garuda.cbu.cordentry.relevantinfo"/></span>
				</td>
				</s:if>
				 <s:elseif test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.NotCollectedToPriorReasonId">
				<td style="align:left"><s:property value="cordAdditionalInfo" escapeHtml="true" /></td>
				</s:elseif>
				
		   </tr>
	
    	</table>
    	</div>
    	</td>
    </tr>
        
    <tr>
       <td>&nbsp;</td>
         <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
    </s:if>
    <s:else>
          <!--<s:if test="hasEditPermission(#request.conElgble)==true">  
       <td><button type="button" onclick="showModal('Eligibility Determination','updateFinalEligibleStatus?module=CBU&esignFlag=modal&resetFun=reset&licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />','390','700');"><s:text name="garuda.common.lable.edit" /></button></td>
       </s:if>-->
       </s:else>
    </tr>  
   <s:if test="(esignFlag != 'reviewCriView') && (esignFlag != 'reviewCriEdit')">
    
				<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
				<tr>
   <td width="50%"><s:text name="garuda.finalreview.label.confirmcompletedby" /></td>
   <td width="50%" align="left"><s:textfield id="completedbydiffuser1" name="cbuFinalReviewPojo.finalEligibleCreator" disabled="true"> </s:textfield></td>
   </tr>
			    </s:if>
			
			<s:else>
   <tr>
   <td width="50%"><s:text name="garuda.finalreview.label.confirmcompletedby" /></td>
   <td width="50%" align="left"><s:textfield id="completedbydiffuser" maxlength="30" name="cbuFinalReviewPojo.finalEligibleCreator" onchange="isChangeDone(this,'%{cbuFinalReviewPojo.finalEligibleCreator}','elgFcrChanges');"> </s:textfield></td>
   </tr>
  </s:else>
  </s:if>
 
    <!--<s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.inEligiblePkid || cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId || cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.notCollectedToPriorReasonId">
	    <tr>
	        <td colspan="2">
		          <table width="100%" cellpadding="0" cellspacing="0" id="searchResults" class="displaycdr">
		                 <thead>
		                     <tr>
		                        <th><s:text name="garuda.finalcbu.eligibility.category" /></th>
		                        <th><s:text name="garuda.finalcbu.eligibility.documentation" /></th>
		                     </tr>
		                 </thead>
		                 <tbody>
			                 <s:iterator value="#request.reasonPojos" >
			                  <s:set name="reasonId" value="fkReasonId" />
		                      <s:set name="attachId" value="fkAttachmentId" />		             
				              <tr>				                   
				                  <td>
				                     <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.notCollectedToPriorReasonId && ((fkFormId!='' && fkFormId !=null) || (fkAttachmentId!='' && fkAttachmentId!=null))">
				                     	   <input type="checkbox" name="priorReasons" id="<s:property value="pkEntityStatusReason"/>" class="prior"	value="<s:property value="pkEntityStatusReason"/>" onclick="checkPrior()"/>	                     
				                     </s:if>
				                     <strong><s:property value="getCodeListDescById(#reasonId)" /></strong>
				                  </td>
				                  <td>
				                     <s:if test="getCodeListSubTypeById(#reasonId)==@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_MATERNAL_REASON">
				                        <button type="button"
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId =='' || fkFormId ==null)">onclick="showModal('Upload Form','openEresForm?formCode=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_MRQ"/>&entityStatusReasonPojo.pkEntityStatusReason=<s:property value="pkEntityStatusReason"/>&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID"/>','500','550');"</s:if> 
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId !='' && fkFormId !=null)">onclick="showModal('Upload Form','openEresForm?formCode=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_MRQ"/>&formId=<s:property value="fkFormId" />&entityStatusReasonPojo.pkEntityStatusReason=<s:property value="pkEntityStatusReason"/>&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID"/>','500','550');"</s:if> 
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId =='' || fkAttachmentId ==null)"> onclick="showModal('Upload Document','uploadFinalDocument?licenceUpdate=false&reasonPojo.pkEntityStatusReason=<s:property value="pkEntityStatusReason"/>&module=CBU&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID"/>&opt=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE)"/>&categoryName=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE)"/>&labTestName=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@AC_ELIGIBILITY_CODESUBTYPE)"/>','500','550');"</s:if>
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId !='' && fkAttachmentId !=null)"> onclick="showDocument('<s:property value="getDCMSAttachId(#attachId)" />')" </s:if>>
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId =='' || fkAttachmentId ==null)">
				                            <script>$j('#finalelig').css('display','none');</script> 				                            
				                            <s:text name="garuda.finalcbu.eligibility.maternalattachment" />
				                          </s:if>
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId !='' && fkAttachmentId !=null)">
				                            <script>$j('#finalelig').css('display','block');</script> 
				                            <s:text name="garuda.finalcbu.eligibility.viewmaternalattachment" /> 
				                          </s:if>
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId =='' || fkFormId ==null)">
				                            <script>$j('#finalelig').css('display','none');</script> 				                            
				                            <s:text name="garuda.finalcbu.eligibility.maternalattachment" />
				                          </s:if> 
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId !='' && fkFormId !=null)">
				                            <script>$j('#finalelig').css('display','block');</script> 
				                            <s:text name="garuda.finalcbu.eligibility.viewmaternalattachment" />
				                          </s:if> 
				                        </button>
				                     </s:if>	
				                     <s:if test="getCodeListSubTypeById(#reasonId)==@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_PHYSICAL_REASON">
				                        <button type="button"
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId =='' || fkFormId ==null)">onclick="showModal('Upload Form','openEresForm?formCode=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_PHYS_FIND_ASSESS"/>&entityStatusReasonPojo.pkEntityStatusReason=<s:property value="pkEntityStatusReason"/>&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID"/>','500','550');"</s:if> 
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId !='' && fkFormId !=null)">onclick="showModal('Upload Form','openEresForm?formCode=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_PHYS_FIND_ASSESS"/>&formId=<s:property value="fkFormId" />&entityStatusReasonPojo.pkEntityStatusReason=<s:property value="pkEntityStatusReason"/>&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID"/>','500','550');"</s:if> 
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId =='' || fkAttachmentId ==null)">onclick="showModal('Upload Document','uploadFinalDocument?licenceUpdate=false&reasonPojo.pkEntityStatusReason=<s:property value="pkEntityStatusReason"/>&module=CBU&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID"/>&opt=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE)"/>&categoryName=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE)"/>&labTestName=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@BC_ELIGIBILITY_CODESUBTYPE)"/>','500','550');"</s:if>
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId !='' && fkAttachmentId !=null)">onclick="showDocument('<s:property value="getDCMSAttachId(#attachId)" />')"</s:if>>
				                         <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId =='' || fkAttachmentId ==null)">
				                            <script>$j('#finalelig').css('display','none');</script>
				                            <s:text name="garuda.finalcbu.eligibility.physiattachment" />
				                          </s:if>
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId !='' && fkAttachmentId !=null)">
				                            <script>$j('#finalelig').css('display','block');</script> 
				                            <s:text name="garuda.finalcbu.eligibility.viewphysiattachment" /> 
				                          </s:if>
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId =='' || fkFormId ==null)">
				                            <script>$j('#finalelig').css('display','none');</script>
				                            <s:text name="garuda.finalcbu.eligibility.physiattachment" />
				                          </s:if>
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId !='' && fkFormId !=null)">
				                            <script>$j('#finalelig').css('display','block');</script> 
				                            <s:text name="garuda.finalcbu.eligibility.viewphysiattachment" /> 
				                          </s:if>
				                        </button>
				                     </s:if>
				                     <s:if test="getCodeListSubTypeById(#reasonId)==@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_INFECTIOUS_REASON">
				                        <button type="button"
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId =='' || fkFormId ==null)">onclick="showModal('Upload Form','openEresForm?formCode=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_INF_DIS_MARK"/>&entityStatusReasonPojo.pkEntityStatusReason=<s:property value="pkEntityStatusReason"/>&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID"/>','500','550');"</s:if> 
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId !='' && fkFormId !=null)">onclick="showModal('Upload Form','openEresForm?formCode=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_INF_DIS_MARK"/>&formId=<s:property value="fkFormId" />&entityStatusReasonPojo.pkEntityStatusReason=<s:property value="pkEntityStatusReason"/>&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID"/>','500','550');"</s:if> 				                         
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId =='' || fkAttachmentId ==null)">onclick="showModal('Upload Document','uploadFinalDocument?licenceUpdate=false&reasonPojo.pkEntityStatusReason=<s:property value="pkEntityStatusReason"/>&module=CBU&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID"/>&opt=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE)"/>&categoryName=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE)"/>&labTestName=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@DC_ELIGIBILITY_CODESUBTYPE)"/>','500','550');"</s:if>
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId !='' && fkAttachmentId !=null)">onclick="showDocument('<s:property value="getDCMSAttachId(#attachId)" />')"</s:if>>
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId =='' || fkAttachmentId ==null)">
				                            <script>$j('#finalelig').css('display','none');</script>
				                            <s:text name="garuda.finalcbu.eligibility.idmattachment" />
				                          </s:if>
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId !='' && fkAttachmentId !=null)">
				                            <script>$j('#finalelig').css('display','block');</script> 
				                            <s:text name="garuda.finalcbu.eligibility.viewidmattachment" /> 
				                          </s:if>	
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId =='' || fkFormId ==null)">
				                            <script>$j('#finalelig').css('display','none');</script>
				                            <s:text name="garuda.finalcbu.eligibility.idmattachment" />
				                          </s:if>
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId !='' && fkFormId !=null)">
				                            <script>$j('#finalelig').css('display','block');</script> 
				                            <s:text name="garuda.finalcbu.eligibility.viewidmattachment" /> 
				                          </s:if>				                        
				                        </button>
				                     </s:if>
				                     <s:if test="getCodeListSubTypeById(#reasonId)==@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_OTHER_REASON">
				                        <button type="button"
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId =='' || fkFormId ==null)">onclick="showModal('Upload Form','openEresForm?formCode=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_OTH_MED_REC"/>&entityStatusReasonPojo.pkEntityStatusReason=<s:property value="pkEntityStatusReason"/>&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID"/>','500','550');"</s:if> 
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId !='' && fkFormId !=null)">onclick="showModal('Upload Form','openEresForm?formCode=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_OTH_MED_REC"/>&formId=<s:property value="fkFormId" />&entityStatusReasonPojo.pkEntityStatusReason=<s:property value="pkEntityStatusReason"/>&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID"/>','500','550');"</s:if> 				                        
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId =='' || fkAttachmentId ==null)">onclick="showModal('Upload Document','uploadFinalDocument?uploadFinalDocument?licenceUpdate=false&reasonPojo.pkEntityStatusReason=<s:property value="pkEntityStatusReason"/>&module=CBU&cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID"/>&opt=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE)"/>&categoryName=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE)"/>&labTestName=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@CC_ELIGIBILITY_CODESUBTYPE)"/>','500','550');"</s:if>
				                        <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId !='' && fkAttachmentId !=null)">onclick="showDocument('<s:property value="getDCMSAttachId(#attachId)" />')"</s:if>>
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId =='' || fkAttachmentId ==null)">
				                            <script>$j('#finalelig').css('display','none');</script>
				                            <s:text name="garuda.finalcbu.eligibility.otherattachment" />
				                          </s:if>
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==false && (fkAttachmentId !='' && fkAttachmentId !=null)">
				                            <script>$j('#finalelig').css('display','block');</script> 
				                            <s:text name="garuda.finalcbu.eligibility.viewotherattachment" /> 
				                          </s:if>
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId =='' || fkFormId ==null)">
				                            <script>$j('#finalelig').css('display','none');</script>
				                            <s:text name="garuda.finalcbu.eligibility.otherattachment" />
				                          </s:if>
				                          <s:if test="cdrCbuPojo.site.isCDRUser()==true && (fkFormId !='' && fkFormId !=null)">
				                            <script>$j('#finalelig').css('display','block');</script> 
				                            <s:text name="garuda.finalcbu.eligibility.viewotherattachment" /> 
				                          </s:if>				                        
				                        </button>
				                     </s:if>			                     
				                   </td>
				              </tr>
					        </s:iterator>
				        </tbody>
				        <tfoot></tfoot>
		          </table>
		      </td>
		</tr>  
	</s:if>  
    -->
     <s:if test="(esignFlag != 'reviewCriView')">
    <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
    </s:if>
    <s:else>
        <s:if test="hasEditPermission(#request.conElgble)==true">	
    <tr bgcolor="#cccccc" id="finalelig" >
	    <td colspan="2">
		    <table>
			   <tr bgcolor="#cccccc">
				    <td ><jsp:include page="../cb_esignature.jsp" flush="true"></jsp:include></td>
				   
					 <td>
					  <input type="button" id="submit" disabled="disabled" onclick="eligibleSubmitFrom();" value="Confirm"/>
					 </td>
					
					 <td>
					  <input type="button"  onclick="fn_closeDiv();" value="Cancel" />
				     </td> 		 
				</tr>
		    </table>
	    </td>
    </tr>
  	</s:if>
  	</s:else>
  	</s:if>
</table></s:form>
<div id="dialog-confirm-1" style="display: none;">
	<s:text name="garuda.common.validation.notapplicableelig"/> 
</div>
</div>   	