<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<jsp:include page="../cb_track_session_logging.jsp" />
<%@page import="com.velos.eres.web.user.UserJB"%>

<script>
init();
function showDataModal(title,url,height,width,regisId)
{
	var title1 = title + regisId;
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}

		$j("#modelPopup7").dialog(
				   {autoOpen: false,
					title: title1  ,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#modelPopup7").dialog("destroy");
				    }
				   }
				  ); 
		$j("#modelPopup7").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#modelPopup7").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	
}


function insertData(msg,cordId)
{
	var orderId=$j("#orderId").val();
	var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
 	var value = field.GetHTML(true);
 	var requestDate = $j("#chkDate").val();
 	$j('#sectionContentsta').val(value);
 	if($j("#replyVal").val()!="true")
 		{
 	/* if($j('input[name=clinicalNotePojo.noteVisible]:radio:checked').val()==null)
	{
 		$j("#visible_error").show();
	
  	
  	if($j("#sectionContentsta").val()=='<br>'||value.length==0)
		{
  		$j('#note_error').show();
  		$j("#clinical").valid();
		}
	}
 	else  */if($j("#sectionContentsta").val()=='<br>'||value.length==0)
 		{
 			$j('#note_error').show();
  			$j("#clinical").valid();
 		}
	else
		{
		$j("#note_error").hide();
		
		var word = $j("#keyword").val();
		
		var pattern='!@*!';
		var pattern2='@!*@';
		word = word.replace('&', pattern);
		word = word.replace('%', pattern2);;
		
		$j("#keyword1").val(word);
		if(orderId.length==0)
			{
			
			 if($j("#clinical").valid())
			  modalFormSubmitRefreshDiv('clinical','saveClincalNotes','usersitegrpsdropdown');
			}
		else
			{
		
			if($j("#clinical").valid())
			 loadMoredivs('saveClincalNotes?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+cordId+'&cbuid='+cordId,$j("#divIdForPf").val()+',openOrdernotesContent,heAvailNotesTable,heresolution,ctAvailNotesDiv,ctShipmentNotesTable,ctResolutionTable,orAvailNotesTable,cbuShipmentNotesTable,orResolutionNotesTable,nmdpResearchSampleTable','clinical','update','status');
			}
		
		}
 		}
 	else
 		{
 		var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
	 	var value = field.GetHTML(true);
	 	$j('#sectionContentsta').val(value);
	 	 if(value=='<br>'||value.length==0)
	 	{
	 		$j('#note_error').show();
	 		
	 	}
	 	 else{
		 		$j('#note_error').hide();
		  		modalFormSubmitRefreshDiv('clinical','saveClincalNotes','usersitegrpsdropdown');
	 	 }
 		}
		/* }
	else
		{
		alert("insertdata2");
		if($j("#clinical").valid())
		 loadMoredivs('saveClincalNotes?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+cordId+'&cbuid='+cordId,$j("#divIdForPf").val()+',openOrdernotesContent,heAvailNotesTable,heresolution,ctAvailNotesDiv,ctShipmentNotesTable,ctResolutionTable,orAvailNotesTable,cbuShipmentNotesTable,orResolutionNotesTable,nmdpResearchSampleTable','clinical','update','status');
		} */
 		
}



function getKeyword()
{
	var word = $j("#keyword").val();
	while(word.indexOf(",")>0)
	{
		 word = word.replace(",", " ");
	}
	$j("#keyword").val(word);
   
	
}

$j(function(){

		$j("#clinical").validate({
			rules:{	
				
				"clinicalNotePojo.subject":{required:true},
				"clinicalNotePojo.fkNotesCategory":{required:true}
			},
			messages:{	
				
				"clinicalNotePojo.subject": "<s:text name="garuda.cbu.clinicalnote.subject"/>",
				"clinicalNotePojo.fkNotesCategory": "<s:text name="garuda.cbu.clinicalnote.notescategory"/>"
				
			}
				
				
			
			});
		//alert("SAMPLEaTlAB::::"+'<s:property value="orderPojo.sampleAtLab"/>');
		var orderType = '<s:property value="%{#request.orderType}"/>';
		var sampleAtLab = $j("#sampleAtLab").val();
		if(orderType=="CT"){
	 	 	if(sampleAtLab=="Y"){
	 	 		orderType='<s:text name="garuda.orOrderDetail.portletname.ctlaborder"/>';
	 	 	 }
	 	 	else if(sampleAtLab=="N"){
	 	 		orderType='<s:text name="garuda.orOrderDetail.portletname.ctshiporder"/>';
	 	 	 }
	 	 }
	    var orderDate = '<s:property value="%{#request.requestedDate}"/>';
	    
		var data ="";

        if(orderType != '' && orderDate != '')
		    data = orderType + ", " + orderDate;
		document.getElementById("typeTime1").value=data;
		
	});

$j(function() {
	
 	var today = new Date();
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();

	var h=today.getHours();
	var mn=today.getMinutes()+1; 
	var toDayDate1 = today.toString().split(" "); 
	today=toDayDate1[1]+' '+toDayDate1[2]+' ,'+toDayDate1[3]; 
	$j("#dateTime").val(today);
});
function closeModalNoteDiv(){
	
	 $j("#notesModalDiv").dialog("close");
	 $j("#modelPopup7").dialog("close");
	 $j("#notesModalDiv").dialog("destroy");
	 $j("#modelPopup7").dialog("destroy");
}
</script>
<div id="clinicalNoteDiv" class='popupdialog tabledisplay '><s:form
	id="clinical" name="clinical">
	<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
	<s:hidden name="clinicalNotePojo.pkNotes" />
	<s:hidden name="cdrCbuPojo.fkSpecimenId" />
	<s:hidden name="clinicalNotePojo.reply" />
	<s:hidden name="cdrCbuPojo.specimen.pkSpecimen" />
	<s:hidden name="cdrCbuPojo.fkCordCbuStatus" id="cbustatus" />
	<s:hidden name="orderId" id="orderId"></s:hidden>
	<s:hidden name="orderType" id="orderType"></s:hidden>
	<s:set name="orderType" value="orderType" scope="request" />
	<s:hidden name="clinicalNotePojo.noteSeq" />
	<s:hidden name="clinicalNotePojo.fkOrderId" />
	<s:hidden name="clinicalNotePojo.fkNotesType" />
	<s:hidden name="clinicalNotePojo.userName" />
	<s:hidden name="clinicalNotePojo.reply" id="replyVal" />
	<s:hidden name="clinicalNotePojo.requestType" id="typeRequest"/>
	<input type="hidden" name= "clinicalNotePojo.requestDate" value="<s:property value="#request.requestedDate"/>">
	<s:hidden name="divId" value="%{#request.divId}" id="divIdForPf"/>
	<s:hidden name="clinicalNotePojo.keyword" id="keyword1"/>
		<div id="update" style="display: none;" >
	  <jsp:include page="../cb_update.jsp">
	    <jsp:param value="garuda.common.lable.pfNote"  name="message"/>
	    <jsp:param value="notesModalDiv"  name="divName"/>
	  </jsp:include>	   
	</div>
	<table width="100%" cellspacing="0" cellpadding="0" border="1"
		id="status">
		<tr>
		   <td><s:textarea id="sectionContentsta"
				name="clinicalNotePojo.notes" rows="5" cols="50" wrap="hard"
				style="width:800;height:190;"
				onKeyDown="javascript:limitText(this,20);"
				onKeyUp="javascript:limitText(this,20);"></s:textarea><span
				style="color: red;">*</span>
			<div id='note_error' style="display: none";><span
				style="color: red;"><s:text name="garuda.cbu.cordentry.reqnotes"/></span></div>
			</td>
		</tr>
		<s:if test="clinicalNotePojo.reply!=true">
		<tr>
			<td>
			<table cellspacing="0" class="displaycdr">
				<tr>
					<td><s:text name="garuda.clinicalnote.label.subject" /><span
						style="color: red;">*</span></td>
					<td><s:textfield name="clinicalNotePojo.subject" maxlength ="25" id="subject"
						style="width: 200px" /></td>
						<td><s:text name="garuda.clinicalnote.label.tagNote" /></td>
							<td>
							<s:checkbox name="clinicalNotePojo.tagNote" id="checkbox" value="clinicalNotePojo.tagNote"></s:checkbox>
							</td>
				</tr>
				<tr>
					<tr>
						<%-- <td><s:text name="garuda.clinicalnote.label.publicPrivate" /><span
							style="color: red;">*</span></td>

						<td><s:radio name="clinicalNotePojo.noteVisible" id="visible"
							list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]"
							listKey="subType" listValue="description" id="noteVisible" />
						<div id='visible_error' style="display: none;"><span
							style="color: red;"><s:text name="garuda.common.label.SelectVisibility"/> </span></div>
						</td> --%>
						
							
			


					</tr>
				<s:if test="clinicalNotePojo.fkNotesCategory!=null">
					<tr>
						<td><s:text name="garuda.clinicalnote.label.category" /><span
							style="color: red;">*</span></td>
						<td><s:select name="clinicalNotePojo.fkNotesCategory"
							style="width:200px"
							list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT]"
							listKey="pkCodeId" listValue="description" headerKey="" disabled="true"
							headerValue="Select" /></td>
							<s:hidden name="clinicalNotePojo.fkNotesCategory"/>
					</tr>
					</s:if>
					<s:else>
					<tr>
						<td><s:text name="garuda.clinicalnote.label.category" /><span
							style="color: red;">*</span></td>
						<td><s:select name="clinicalNotePojo.fkNotesCategory"
							style="width:200px"
							list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select" /></td>

					</tr>
					</s:else>
					<tr>

						<td><s:text name="garuda.clinicalnote.label.keyword" /></td>
						<td><s:textfield name="clinicalkeyword" id="keyword" maxlength="25"
							onblur="javascript:getKeyword();" style="width: 200px" /></td>
					</tr>
					
					<tr>
					
								<td><s:text name="garuda.clinicalnote.label.requestType"/>
								<s:text name="/"/>
								<s:text name="garuda.clinicalnote.label.requestDate"/><span
									style="color: red;">*</span></td>
								<td><s:textfield id="typeTime1" name="requestType/requestDate" 
												style="width: 200px" disabled="true"/>
												
								</td>

						
					</tr>  
					<!--<s:if test ="#request.orderId>0">
					//for above row content
				</s:if> -->
				<!--<s:else>
		 			<tr>
						<td><s:text name="garuda.clinicalnote.label.requestType" /><span
							style="color: red;">*</span></td>
						<td><s:textfield name="requestType" 
												style="width: 200px" disabled="true" value = "%{#request.orderType}"/></td>

					</tr></s:else>
					<tr>
						<td><s:text name="garuda.clinicalnote.label.dateTime" /><span
							style="color: red;">*</span></td>
						<td><s:date name="dateTime" id="dateTime"  format="MMM dd, yyyy / HH:MM" />
						<s:textfield name="clinicalNotePojo.dateTime" id="dateTime"
							style="width: 200px" /></td>
					</tr> 

					<tr>
						<td><s:text name="garuda.clinicalnote.label.user" /><span
							style="color: red;">*</span></td>
						<td><s:textfield name="clinicalNotePojo.userName"
							disabled="true" id="user" style="width: 200px" /></td>

					</tr> -->
			</table>
			</td>
		</tr></s:if>
	
<tr><td>
		<table>
		<tr>
			<td><jsp:include page="../cb_esignature.jsp" flush="true"></jsp:include></td>
		

			<td align="center">
			<button type="button" id="submit" disabled="disabled"
				onclick="insertData('<s:text name="garuda.clinicalnote.label.visibility_warning_msg"/>','<s:property value="cdrCbuPojo.cordID"/>');"><s:text
				name="garuda.common.save"></s:text></button>
			<button type="button" onclick="closeModalNoteDiv();"><s:text
				name="garuda.common.close"></s:text></button>
			</td>
		</tr></table>
</td></tr>
<s:if test="clinicalNotePojo.reply!=true">
		<tr>
			<td><s:if test="clinicalNotelist.size()>0">
				<div id="historyNotes"
					style="width: 800px; height: 100px; overflow: auto;">
				<table>
					<tr>

						<td>
						<table>
							<s:iterator value="#request.clinicalNotelist">
								<tr>
									<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
									
										<s:if test="tagNote==true">
										<img width="20" height="16" src="images/exclamation.jpg">
										</s:if>
									</td>
									<s:set name="cellValue1" value="createdBy" scope="request" />
									<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>><s:text name="garuda.clinicalnote.label.posted_by"></s:text>
									<%
										String cellValue1 = request.getAttribute("cellValue1")
															.toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();
									%> 			<%=userB.getUserLastName() + " "
														+ userB.getUserFirstName()
												%>
									</td>

									<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>><s:text name="garuda.cdrcbuview.label.progress_notes"></s:text>
									:<s:text name="garuda.cbu.label.progress_note"/>#<s:property value="noteSeq" /> <s:property
										value="getCodeListDescById(fkNotesCategory)" /></td>
									<td><input type="hidden" name="pkNotes" /></td>
									<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>><s:text name="garuda.clinicalnote.label.posted_on"></s:text>
									
									<s:set name="cellValue15" value="createdOn" />
							        <s:date name="cellValue15" id="cellValue15" format="MMM dd, yyyy / HH:MM:SS a"  />
							        <s:property value="cellValue15"/>	
									
									
									</td>

									<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>><s:text name="garuda.clinicalnote.label.subject"></s:text>:
									<s:property value="subject" /></td>
									<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>><s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
									<s:iterator
										value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]">
										<s:if test="subType==noteVisible">
											<s:property value="description" />
										</s:if>
									</s:iterator></td>
									<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
									<td><a href="#"
										onClick="loadPageByGetRequset('revokePfNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId" />&orderType=<s:property value="#request.orderType" />&category=<s:property value="fkNotesCategory" />&clinicalNotePojo.pkNotes='+<s:property value="pkNotes"/>,'clinicalNoteDiv','clinical')">
									<s:text name="garuda.clinicalnote.label.revoke" /></a></td>
									</s:if>
									<td><a href="#"
										onClick="showDataModal('Progress Note for CBU Registry ID','pfViewNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property value="pkNotes"/>','500','700','<s:property value="cdrCbuPojo.registryId"/>')">
									<s:text name="garuda.clinicalnote.label.view" /></a></td>
									<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
									<td>
									<a href ="#" onClick="showModals('Reply Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfReplyNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REPLY_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId" />&orderType=<s:property value="#request.orderType" />&clinicalNotePojo.noteSeq=<s:property value="noteSeq"/>&clinicalNotePojo.fkNotesCategory=<s:property value="fkNotesCategory"/>','500','850','notesModalDiv');"> <s:text name="garuda.clinicalnote.label.reply" /></a>
									</td>
									</s:if>
								</tr>


							</s:iterator>
						</table>

						</td>
					</tr>
				</table>
				</div>

			</s:if></td>
		</tr></s:if> 
		
	</table>

</s:form></div>
<div id="modelPopup7"></div>