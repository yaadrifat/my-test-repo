<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.business.util.VelosMidConstants"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script type="text/javascript">
function showPending(pksite,ordertype,fkordertype){
	
	var sampleAtlab="";
	var reqType=ordertype;
	if(reqType=='<%=VelosMidConstants.CT_LAB_ORDER%>'){ 
		sampleAtlab='Y';
		reqType='CT';
	}
	if(reqType=='<%=VelosMidConstants.CT_SHIP_ORDER%>'){ 
		sampleAtlab='N';
		reqType='CT';
	}
	var statNew='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@NEW)"/>';
	submitpost('pendingOrders',{'pksiteid':pksite,'fkordertype':fkordertype,'filterPending':'1','sampleAtLab':sampleAtlab,'sampleAtlab1':sampleAtlab,'orderStatus1':statNew,'reqType1':fkordertype});
}
</script>
<s:form>
	
	<div id="status">
		<table class="displaycdr">
			<thead>
				<tr>
				<th><s:text name="garuda.landingpage.label.CBB"/></th>
				<th><s:text name="garuda.landingpage.label.totcount"/></th>
				<th><s:text name="garuda.landingpage.label.new"/></th>
				</tr>
			</thead>
			<tbody>
				
					<s:if test="cbbPopupLst!=null && cbbPopupLst.size()>0">
						<s:iterator value="cbbPopupLst" var="rowVal">
						<tr>
							<td><s:property value="%{#rowVal[0]}"/></td>
							<td><s:property value="%{#rowVal[1]}"/></td>
							<td>
							<s:if test="%{#rowVal[2]!=0}">
							<a href="#" onclick="showPending('<s:property value="%{#rowVal[4]}"/>','<s:property value="%{#rowVal[3]}"/>','<s:property value="%{#rowVal[5]}"/>');"><s:property value="%{#rowVal[2]}"/></a>
							</s:if>
							<s:else>
							<s:property value="%{#rowVal[2]}"/>
							</s:else>
							</td>
						</tr>
						</s:iterator>
					</s:if>
				
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3"></td>
				</tr>
			</tfoot>
		</table>
	</div>


</s:form>