<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%String contextpath = request.getContextPath()+"/jsp/modal/"; %>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<script>
function showDocument(AttachmentId){
	if ((document.layers) || (navigator.appName == 'Netscape') || (navigator.appName == 'Opera')){
	var cotextpath = "<%=contextpath%>";
	var signed='true';
	var url='getAttachmentFromDcms?docId='+AttachmentId+'&signed='+signed;
	
	$j('embed#downloaddoc1').attr('type', 'application/pdf');
	$j('embed#downloaddoc1').attr('src', url); 
	
	$j('#downloaddoc').css('display','block');
	//$j('#downloaddoc1').css('display','block');
	//$j('#downloaddoc').load(url);
	$j('#downloaddoc1').css('display','block');
	//$j('#downloaddoc1').attr('data', url);
	//document.getElementById("docIFrame").src = url;

	//loaddiv(url,downloaddoc);
	//window.open( url, "Attachment", "status = 1, height = 500, width = 800, resizable = 0" );
	}else{
		var cotextpath = "<%=contextpath%>";
		var signed='true';
		var url='getAttachmentFromDcms?docId='+AttachmentId+'&signed='+signed;
		window.open( url, "Attachment", "status = 1, height = 500, width = 500, resizable = 0" );
	}
}
	
//$j("#listtab").dataTable();
//$j("#mydiv").attr('target',parent);

function openCreateUnitReport(cordID)
{	
	closeModal();
	showModal('Create Unit Report','createUnitReport?cordID='+cordID,'500','500');
}

function viewunitreportscreen(cordID,pkCordExtInfo)
{
 	var url = cotextpath+"/jsp"+"/unitreportScreen.action?cordID="+cordID+"&pkCordExtInfo="+pkCordExtInfo;
	window.open( url);	
	//loadPage("unitreportScreen?cdrCbuId="+cdrCbuId);
	}

/* $j(function(){
	$j("#viewreport").validate({
			
		rules:{			
				"revokeToId":{checkdbdata:["CdrCbu","registryId"]},					
				
			},
			//messages:{
				"revokeToId":{checkdbdata:"ID does not exists"},
				
			}
		});
	}); */

function revokeCBUUnitReport(cdrCbuId,attachmentId,dcmsAttachId)
	{
		
			var answer = confirm('<s:text name="garuda.uploaddoc.label.confirmationmsgforRevoke"/>');
			if (answer){	
		
		var url='revokeUnitrptDoc?cbuCordId='+cdrCbuId+'&attachmentId='+attachmentId;
		$j.ajax({
			type : "POST",
			async : false,
			url : url,
			//data : $j("#searchOpenOrders2").serialize(),
			success : function(result) {
				//showUnitReport("close");
				
				$j('#unitReportDiv').html(result);
				refreshMultipleDivsOnViewClinical('1637','<s:property value="cdrCbuPojo.cordID"/>');
			}
		
	});
			}
	}

function modifyCBUUnitReport(cdrCbuId,attachmentId,cbuRegisteryId,dcmsAttachId){
	/* if ((document.layers) || (navigator.appName == 'Netscape') || (navigator.appName == 'Opera')){
		showDocument(dcmsAttachId);
	} */
		var url='modifyUnitrpt?cbuCordId='+cdrCbuId+'&attachmentId='+attachmentId+'&modifyUnitrpt=True'+'&cbuRegisteryId='+cbuRegisteryId;
		
		loadPageByGetRequset(url,'modifyUnitRptDiv');
		
		//loadPageByGetRequset(url,'modifyUnitRptDiv');
		//$j('#modifyUnitRptDiv').css('display','block');
		//$j('#modifyUnitrpt').attr('value','True');
		//$j('#modifyUnitRptDiv').css('display','block');
		//$j('#cdrCbucordID').val(attachmentId);
		
		
}
$j(function(){
	   $j('.togglediv').find(".portlet-header .ui-icon").bind("click",function() {
		   if($j(this).is(".ui-icon-minusthick") || $j(this).is(".ui-icon-plusthick"))
			{
				$j(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
				
				$j(this).parents(".portlet:first").find(".portlet-content").toggle();
				
				var divid = $j(this).parent().parent().attr("id");
				if(divid !="undefined" || divid !=null)
				divHeight=document.getElementById(divid).style.height;
				if(divHeight=='100%')
				{
					divHeight = 'auto';
					}	
			    $j(this).parents(".portlet:first").css('height',divHeight);
				
			}
		});
	 });
</script>
<%-- <s:hidden name="categoryName" value='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE" />'></s:hidden>
 --%><div class="col_100 maincontainer"><div class="col_100">
      

<div class="column" id="unitReportDiv">
	<s:form id="viewreport" name="viewreport" method="post">
	
	<s:hidden
				name="cdrCbuPojo.cordID" id="cdrCbucordID"></s:hidden>
	
	<s:hidden name="cdrCbuPojo.registryId" id="registryId"></s:hidden>
	
	<%-- <table class="displaycdr">
		<tr>
			<td align="right"><button  type="button"  onclick="javascript:openCreateUnitReport('<s:property value="cdrCbuPojo.cordID" />');" ><s:text
										name="garuda.cdrcbuview.label.button.uploadunitreport" /></button></td>
		</tr>
		</table>	 --%>							
<div class="portlet" id="searchresultparent" >
<div id="searchresult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all togglediv" >
<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
<s:text name="garuda.cbuentry.label.unitreportsearchresult" /><span class="ui-icon ui-icon-minusthick"></span></div>
<div class="portlet-content">

<div id="searchTble">

<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="listtab" >

	<thead>
		<tr>
		    <th><s:text name="garuda.uploaddoc.label.date" /></th>
			<th><s:text name="garuda.cdrcbuview.label.registry_id" /></th>
			<th><s:text name="garuda.openorder.label.description"/></th>
		    <th><s:text name="garuda.uploaddoc.label.documentuploadedby" /></th>
		    <th><s:text name="garuda.openorder.label.completiondate" /></th>
			<th><s:text name="garuda.uploaddoc.label.view" /></th>
			<th><s:text name="garuda.common.lable.modify" /></th>
			<th><s:text name="garuda.common.lable.revoke" /></th>
		</tr>
	</thead>
	<tbody>
		<s:iterator value="unitReportList" var="cbuunitreport" status="row">
		<s:set name="attachId" value="attachmentId" />
			<tr id="<s:property	value="%{#row.index}"/>" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')" >
						<td><s:date name="createdOn" id="createdOn1" format="MMM dd, yyyy" />
				<s:property  value="%{createdOn1}" /></td>
					<td><s:property value="cdrCbuPojo.registryId" /></td>
				<td><s:if test="#request.attachId!=null"><s:property value="getDescriptionUploadInfo(#attachId)"/></s:if></td>		
				<td>
				<s:set name="cellValue1" value="createdBy" scope="request"/>
				<s:if test="#request.cellValue1!=null">	
					<% 
						String cellValue1 = request.getAttribute("cellValue1").toString();
						UserJB userB = new UserJB();
						userB.setUserId(EJBUtil.stringToNum(cellValue1));
						userB.getUserDetails();													
					%>	
					<%= userB.getUserLastName()+" "+userB.getUserFirstName() %>									
				</s:if>
				</td>
				<td><s:if test="#request.attachId!=null"><s:date name="%{getCompltionDateUploadInfo(#attachId)}" id="CompletionDate" format="MMM dd, yyyy" /><s:property value="%{CompletionDate}"/></s:if></td>
				<td>
				<a href="#"><img src="images/attachment-icon.png"
								onclick="javascript: return showDocument('<s:property value="getDcmsFileAttchId(#attachId)" />')" /></a></td>
				<s:if test="hasEditPermission(#request.viewUnitRepCat)==true">	
				        <td><button type="button" onclick="javascript:modifyDocument('<s:property value="fkCordCdrCbuId"/>','<s:property value="attachmentId"/>','<s:property value="cdrCbuPojo.registryId"/>','<s:property value="getDcmsFileAttchId(#attachId)" />');"><s:text name="garuda.common.lable.modify" /></button></td>
				        <td><button type="button" onclick="javascript: revokeCBUUnitReport('<s:property value="fkCordCdrCbuId"/>','<s:property value="attachmentId"/>','<s:property value="getDcmsFileAttchId(#attachId)" />')" ><s:text name="garuda.common.lable.revoke" /></button></td>
			   </s:if>
			   <s:else>
						<td><button type="button" disabled="disabled"><s:text name="garuda.common.lable.modify" /></button></td>
						<td><button type="button" disabled="disabled"><s:text name="garuda.common.lable.revoke" /></button></td>
				</s:else>
			</tr>
		</s:iterator>
	</tbody>

	<tfoot>
		<tr>
			<td colspan="6"></td>
		</tr>
	</tfoot>
	<tr></tr>
</table>
</div></div></div>
<!-- <div  id="downloaddoc"  style="display : none;"><table style="height: 100%"><tr><td>
<iframe id="docIFrame"  style="display: none;" src="" width="100%" height="300">

 <iframe name="myframe" id="myframe" src="myframe" width="100%" height="100%" frameborder="0" ></iframe> 

<OBJECT classid="downloaddoc" DATA="" TYPE="application/pdf" >
 <param name="mydiv" id="mydiv">
<embed id="downloaddoc1" src="" width="100%" height="100%" type="application/pdf"  >
<A HREF="mlk.html"></A>
</OBJECT>
 <embed id="downloaddoc1" style="display : none;" src="" width="100%" height="100%" type="application/pdf"  >
 <iframe name="myframe" id="myframe" src="http://83.68.223.157/FileData-war/DcmsRetrieveServlet?apiUser=garuda&docId=555&sign=iuBfpZOOYTeO8hCc79gyTCTAXtQ=&embedded=true" width="100%" height="100%" frameborder="0" ></iframe> 
 </td></tr></table></div> -->
 <div  id="downloaddoc"  style="display : none;"><table style="height: 100%"><tr><td>
	<embed id="downloaddoc1" style="display : none;" src="" width="100%" height="100%" type=""  >
<!--  <object id="downloaddoc2" classid="" width="500" height="400" type="application/pdf">
	<param name="src" value="http://83.68.223.157/FileData-war/DcmsRetrieveServlet?apiUser=garuda&docId=555&sign=iuBfpZOOYTeO8hCc79gyTCTAXtQ=" /> 
	
	EMBED tag for Netscape Navigator 2.0+ and Mozilla compatible browsers
	<embed id="downloaddoc2" style="display : none;" src="D:\garudadomain\ISBT\ISBT\Table-RT017-Version-Control-Sheet-v1.3.0.pdf" width="100%" height="100%" type="application/pdf"  >
	<embed id="downloaddoc1" style="display : none;" src="" width="100%" height="100%" type="application/pdf"  >
 </object>  -->
 <!-- <iframe style="border-style: none;" src="http://docs.google.com/viewer?url=http://83.68.223.157/FileData-war/DcmsRetrieveServlet?apiUser=garuda&docId=555&sign=iuBfpZOOYTeO8hCc79gyTCTAXtQ=&embedded=true" height="390" width="400"></iframe>
 --> </td></tr></table></div>
</div>
</s:form>
<div>
<s:if test="#request.submit=='modifyunitrpt' || #request.submit=='revokeunitrpt'">
<div class="portlet" id="modifiedOrRevoked">
	  <jsp:include page="../cb_thankyoucbu.jsp">
	    <jsp:param value="garuda.message.modal.clinical"  name="message" />
	  </jsp:include>	   
	</div>
</s:if>
</div>
<div class="portlet" id="modifyUnitRptDiv" >
<div id="update" style="display: none;" >
	   
	</div>
		      
</div>

</div>
</div></div>