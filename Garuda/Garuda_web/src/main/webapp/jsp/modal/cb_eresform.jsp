<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants" %>
<script>


$j(function(){
    //$j('#modalform table tr td:first-child').css('width','5%');
    //$j('#modalform table tr td:last-child').css('width','25%');
    $j('#overDiv').css('z-index','99999');
    openCaldr();
});

function openCaldr() {	
	if ($j(".datefield") && $j(".datefield").length && $j(".datefield").length > 0){
		$j(".datefield").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: eResYearRange,
			showButtonPanel: true,
			prevText: '',
			nextText: '',
			closeText: 'Done',
			currentText: L_Today,
			dateFormat: jQueryDateFormat
		});
	}
}


function submitModalEresForm(){
	if (validateModalForm(document.modalform)==false) 
	{
		return false;
	}else{
		$j.ajax({
			type: "POST",
			url : "updateFormData.jsp",
			data : $j("#modalform").serialize(),
			success : function(data) {
			},
			error:function() { 
				alert("Error ");
			}
			
			});
			
			modalFormSubmitRefreshDiv('modalform','submitEresForm','eligibleDiv'); 
			$j("#update1").css('display','block');
	    	$j("#status1").css('display','none');   
	}	
}




function submitModalCommonForm(){
	if (validateModalForm(document.modalform)==false) 
	{
		return false;
	}else{
		$j.ajax({
			type: "POST",
			url : "updateFormData.jsp",
			data : $j("#modalform").serialize(),
			success : function(data) {
			modalFormSubmitRefreshDiv('modalform','submitCommonForm','fmhqcontent'); 
			modalFormSubmitRefreshDiv('modalform','submitCommonFormMrq','mrqcontent');
			$j("#update1").css('display','block');
	    	$j("#status1").css('display','none');
	    	$j("#esign").css('display','none');
	    	
			},
			error:function() { 
				alert("Error ");
			}
			
			});
			
			 
	}	
}
</script>
<style>
#modalform select{
width:auto;
}
</style>
<s:if test="#request.formNotAvailable!=null && #request.formNotAvailable==true">
    <h1>Form Not Available!</h1>
</s:if>
<s:else>
    <s:form id="modalform">
	<s:hidden name="entityStatusReasonPojo.pkEntityStatusReason" />
	<s:hidden name="cdrCbuPojo.cordID" />
	<!--  s:hidden name="cdrCbuPojo.fkSpecimenId" id="fkSpecimenId"-->
	<div id="update1" style="display: none;" >
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="garuda.message.modal.eresformsubmit" name="message"/>
		  </jsp:include>	   
	</div>
	<div id="status1">
	 <table cellpadding="0" cellspacing="0" border="0">	
	    <tr>
	      <td>
	          <%=request.getAttribute("form").toString()%>
	         <%
	         //System.out.println("form::"+ request.getAttribute("form").toString());
	         
	         %>
		   </td>
	    </tr>
	    <tr>
	      <td>
	         <table cellpadding="0" cellspacing="0" border="0" bgcolor="#cccccc" id="esign">
	            <tr bgcolor="#cccccc">
					<td width="70%" valign="baseline">
						<jsp:include page="../cb_esignature1.jsp" flush="true">
						    <jsp:param value="eleReviewsubmit" name="submitId"/>
							<jsp:param value="eleReviewinvalid" name="invalid" />
							<jsp:param value="eleReviewminimum" name="minimum" />
							<jsp:param value="eleReviewpass" name="pass" />
						</jsp:include>
					</td> 
					<s:if test="#request.module!=null && #request.module=='CORDENTRY'">
					    <td width="15%" valign="baseline" align="right">
							<input type="button" id="eleReviewsubmit" onclick="submitModalCommonForm()" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
						</td>
					</s:if> 
					<s:else>
					    <td width="15%" valign="baseline" align="right">
							<input type="button" id="eleReviewsubmit" disabled="disabled" onclick="submitModalEresForm()" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
						</td>
					</s:else>						
					<td valign="baseline" width="15%">   
					   <input type="button" onclick="closeModal();"  value="<s:text name="garuda.common.lable.cancel"/>" />
					</td> 		 
			</tr>
	         </table>
	      </td>
	    </tr>
	</table>
	</div>
	</s:form>
</s:else>
