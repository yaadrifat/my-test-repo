<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<script>

$j("#licensehistoryReview").dataTable();
var licFcrChangesDone = false;
$j(function() {
/*	$j( "#datepicker" ).datepicker({dateFormat: 'M dd yy',changeMonth: true,
		changeYear: true});
	$j( "#datepicker1" ).datepicker({dateFormat: 'M dd yy',changeMonth: true,
		changeYear: true});
	*/
	getDatePic();
});

$j(function(){
	
	$j("#licenseReview").validate({
		rules:{			
				"cbuLicStatus":{required:true},
				"reasons":{
											required:{
            									depends: function(element) {
            										return (($j("#cbuLicStatusid").val()==$j("#unLicensedPk").val()) && ('<s:property value="%{isTasksRequired(cdrCbuPojo.cordID)}"/>'=='false'));
														 }
					}
											}
			},
			messages:{

				"cbuLicStatus":"<s:text name="garuda.cbu.license.licensurestatus"/>",
				"reasons":"<s:text name="garuda.cbu.license.unlicreason"/>"
				
			}
		});
	});
	
function revieModal(){
	if($j("#licenseReview").valid()){
		licFcrChangesDone = divChanges('licChanges',$j('#reviewStatus').html());
		if(licFcrChangesDone){
		setTimeout(function(){	
		//modalFormSubmitRefreshDiv('licenseReview','submitReviewLicenseStatus','main');
		var url = 'submitReviewLicenseStatus?pkcordId='+$j("#pkcordId").val()+'&licenceUpdate=True&cordAcceptId='+$j('#cordAcceptId').val()+'&esignFlagFCR=review';
			if('<s:property value="resetFun"/>'=="reset"){
				url = url + '&resetOnce=once';
			}
		loadMoredivs(url,'test,indshowdiv,cbuStau1,licenseReasonRef','licenseReview','reviewUpdate','reviewStatus');
		RefreshPrevious($j('#pkcordId').val());
		var criflag = $j('#criMrqFlg').val();
		criflag=$j.trim(criflag);
		var crimrqflg = $j('#criCompFlg').val();
		crimrqflg=$j.trim(crimrqflg);
		var valLicstatus = $j('#cbuLicStatusid').val()
		valLicstatus=$j.trim(valLicstatus);
		var LicStatusFlg = $j('#cbuLicStatusFlg').val();
		LicStatusFlg=$j.trim(LicStatusFlg);
		if(valLicstatus==LicStatusFlg && criflag!=1 && crimrqflg!=1){
			alert('<s:text name="garuda.cbufinalreview.label.fcr.CRI.alert"/>');
			window.close();
		}
		$j('input[name=acceptance]').attr({'disabled': true});
		},0);
		}
		else{
			showSuccessdiv("reviewUpdate","reviewStatus");
			}
	}
}
$j(document).ready(function(){
	unliceseReason($j("#cbuLicStatusid").val(),'licenseReasonReview','<s:property value="%{isTasksRequired(cdrCbuPojo.cordID)}"/>');
	if('<s:property value="resetFun"/>'=="reset"){
		$j('#cbuLicStatusid').change(function(){
			resetingEligibleProcess(this.value);
		});
	}
});
	function resetingEligibleProcess(val){

		if(('<s:property value="cdrCbuPojo.fkCordCbuEligible"/>'=='<s:property value="#request.pkInEligible"/>' || '<s:property value="cdrCbuPojo.fkCordCbuEligible"/>'=='<s:property value="#request.pkInComplete"/>') && val=='<s:property value="#request.licensedPK"/>'){
			$j("#cbuLicStatusid").val($j("#unLicensedPk").val());
			unliceseReason($j("#cbuLicStatusid").val(),'licenseReasonReview','<s:property value="%{isTasksRequired(cdrCbuPojo.cordID)}"/>');
			$j( "#dialog-resetLicDiv-1" ).dialog({
				resizable: false,
				modal: true,
				closeText: '',
				closeOnEscape: false ,
				buttons: {
					"OK": function() {
						$j('#cbuLicStatusid').removeClass('licChanges'); 
						$j( this ).dialog( "close" );
					}
				}
			});
		}
	}
</script>
<div id="dialog-resetLicDiv-1" style="display: none;">
<table><tr><td><img src='images/warning.png'/></td><td><s:text name="garuda.cbu.license.cntchngtolicweninelig"/></td></tr></table>
</div>
<div id="licenseDiv" class='tabledisplay'>
<s:form id="licenseReview">
<s:hidden name="cdrCbuPojo.unLicesePkid" id="unLicensedPk"></s:hidden>
<s:hidden name="licenceUpdate" value="True"></s:hidden>
<s:hidden name="cdrCbuPojo.cordID" />
<s:hidden name="cdrCbuPojo.fkSpecimenId" />
<s:hidden name="cdrCbuPojo.specimen.pkSpecimen" />
<s:hidden name="esignFlag" id="esignFlag" />
<s:hidden name="orderId" id="orderId" />
<s:hidden name="orderType" id="orderType"/>
<s:hidden name="cbuFinalReviewPojo.licensureConfirmFlag" id="licensureConfirmFlag" />
<s:hidden name="cbuFinalReviewPojo.eligibleConfirmFlag" id="eligibleConfirmFlag" />
<s:hidden name="cbuFinalReviewPojo.finalReviewConfirmStatus" id="finalReviewConfirmStatus" />
<div id="reviewUpdate" style="display: none;" >
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="garuda.message.modal.licence_update" name="message"/>
		  </jsp:include>	   
</div>	
<div id="reviewStatus">
<table width="100%">
	<s:if test="#request.update==true" ></s:if>
<tr>
	<td>
	<s:if test="esignFlag == 'modal' ">
		<!-- <table class="displaycdr" id="licensehistoryReview" >
			<thead>
				<tr>
					<th><s:text name="garuda.common.lable.date" /></th>
					<th><s:text name="garuda.cdrcbuview.label.user_name" /></th>
					<th>License Status</th>
					<th><s:text name="garuda.cdrcbuview.label.eligible_reason" /></th>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="lstLicensure" var="rowVal" status="row">
				<tr>
					<s:iterator value="rowVal" var="cellValue" status="col" >
						<s:if test="%{#col.index == 0}">
							<s:date name="%{#cellValue}"
									id="statusDate" format="MMM dd, yyyy" />
							<td><s:property value="%{statusDate}"/></td>
						</s:if>
						<s:if test="%{#col.index == 1}">
							<s:set name="cellValue1" value="cellValue" scope="request"/>
							<td>
								<%
									//String cellValue1 = request.getAttribute("cellValue1").toString();
									//UserJB userB = new UserJB();
									//userB.setUserId(EJBUtil.stringToNum(cellValue1));
									//userB.getUserDetails();													
								%>	
								<%//serB.getUserLastName()+" "+userB.getUserFirstName() %>
							</td>
						</s:if>
						<s:if test="%{#col.index == 2}">																							
							<td style="valign : top">
								<s:property value="getCodeListDescById(#cellValue)" />
							</td>
						</s:if>
						<s:if test="%{#col.index == 3}">
							<td><s:iterator value="getMultipleReasons(#cellValue)" var="colVal" status="val"><s:property value="getCodeListDescById(#colVal)" /><br></s:iterator></td>
						</s:if>
					</s:iterator>
				</tr>
				</s:iterator>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3"></td>
				</tr>
			</tfoot>
		</table> -->
	</s:if>
	</td>	
</tr>
<tr>
	<td>
		<table id="licStatusTableReview">
		
			<tr>
				<td width="35%">
					<s:text name="garuda.cdrcbuview.label.licensure_status" />:<span style="color: red;">*</span></td>
				<td width="65%">
					<s:select name="cbuLicStatus" id="cbuLicStatusid" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select"  onchange="unliceseReason(this.value,'licenseReasonReview','%{isTasksRequired(cdrCbuPojo.cordID)}');isChangeDone(this,'%{cbuLicStatus}','licChanges');" />
				</td>
			</tr>
			
		</table>
	</td>
</tr>
<s:if test="%{isTasksRequired(cdrCbuPojo.cordID)==false}">
<tr>			
	<td colspan="2">
		<div id="licenseReasonReview"  <s:if test="#request.unlicensed!=true">style="display: none;"</s:if>>
			<table width="100%">
				<tr>
					<td width="35%"><input type="hidden" id="licsDbReasons" value='<s:if test="reasons!=null"><s:property value="%{reasons[0]}"/><s:iterator status="row" begin="1" end="%{reasons.length-1}">,<s:property value="%{reasons[#row.index+1]}"/></s:iterator></s:if>' />
						<s:text name="garuda.cdrcbuview.label.unlicense_reason" />:<span style="color: red;">*</span>
					</td>
					<td width="65%">
						<s:select name="reasons" id="fkCordCbuUnlicenseReasonid" onchange="getDblicsReasons(this,'licChanges');"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON]"
									listKey="pkCodeId" listValue="description" multiple="true" value="reasons" cssStyle="height:100px;"/>
					</td>
				</tr>
			</table>
		</div>
   </td>   
</tr>
</s:if>
<!--  -->
</table>
<table bgcolor="#cccccc">
	<s:if test="esignFlag == 'modal'"  >
	    <tr bgcolor="#cccccc">
			<td>
				<jsp:include page="../cb_esignature.jsp" flush="true">
						<jsp:param value="licReviewModalsubmit" name="submitId"/>
						<jsp:param value="licReviewModalinvalid" name="invalid" />
						<jsp:param value="licReviewModalminimum" name="minimum" />
						<jsp:param value="licReviewModalpass" name="pass" />
				</jsp:include>
			</td>  
			<td align="right"">
			 <input type="button" disabled="disabled" id="licReviewModalsubmit" onclick="revieModal();" value="<s:text name="garuda.unitreport.label.button.submit"/>" /> 
			</td>
			<td>			   
			   <input type="button"  onclick="closeModal();" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />
			</td>
		</tr>
	</s:if>
</table>
</div>
</s:form>
</div>
<script>
function getDblicsReasons(thisVal,classname){
	isChangeDone(thisVal,$j('#licsDbReasons').val(),classname);
}
</script>