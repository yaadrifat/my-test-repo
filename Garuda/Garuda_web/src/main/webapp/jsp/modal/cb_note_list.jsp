<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<%@page import="com.velos.eres.web.user.UserJB"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
<s:hidden name="clinicalNotePojo.pkNotes" />
<s:hidden name="clinicalNotePojo.reply" />
<s:hidden name="clinicalNotePojo.noteSeq"/>
<s:hidden name="clinicalNotePojo.fkNotesType"/>
<div id = "parentClinic">

			<s:if test="clinicalNotelist.size()>0">
  		<div id="clinic" style=" width: 463px; height: 100%;" >  		
  		<table>
  		 	<tr>
  		 	
  		 		<td>   				
					<table>		
		
						<s:iterator value="#request.clinicalNotelist">
						<s:if test = "fkNotesType==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_TYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_TYPE_CODE_SUB_TYPE_CLINIC)">
							<s:if test ="reply==true">
							<tr>
								<td></td>
											<s:set name="cellValue1" value="createdBy" scope="request"/>
													<td colspan="6" <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>	
									</td>
									<td colspan="6" <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
											<s:text name="garuda.clinicalnote.label.posted_on"></s:text>
											<s:set name="cellValue15" value="createdOn" />
							        			<s:date name="cellValue15" id="cellValue15" format="MMM dd, yyyy / hh:mm:ss a"  />
							        			<s:property value="cellValue15"/>
									</td>
									<td colspan="6" <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
											<s:text name="garuda.cdrcbuview.label.clinical_notes"></s:text>
											<s:text name="garuda.cbu.label.reClinical"/><s:property value="noteSeq"/> 
									</td>
									 <s:if test="hasEditPermission(#request.vClnclNote)==true">
									<td>
												<a href ="#" onClick="loadPageByGetRequset('revokeNote2?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes='+<s:property value="pkNotes"/>,'parentClinic')"> <s:text name="garuda.clinicalnote.label.revoke" /></a>
									</td>
									</s:if>
									 <td>
												<a href ="#" onClick="showModal('Clinical Notes for Registry ID <s:property value="cdrCbuPojo.registryId"/>','viewNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property value="pkNotes"/>','500','850');"> <s:text name="View" /></a>
									</td><s:if test="hasEditPermission(#request.vClnclNote)==true">
									<td>
										
										<a href ="#" onClick="showModals('Reply Notes for Registry ID <s:property value="cdrCbuPojo.registryId"/>','replyNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REPLY_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.fkNotesCategory=<s:property value="fkNotesCategory"/>&clinicalNotePojo.noteSeq='+<s:property value="noteSeq"/>,'500','850','notesModalsDiv');"> <s:text name="garuda.clinicalnote.label.reply" /></a>
									</td>
									</s:if>
							</tr>
							<tr>
							<td></td>
						    	<td colspan="7" <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
						    	<div style="overflow: auto;">
						    	<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value="notes"/></strong>
						    	</div>
						    	</td>
						    </tr>
							</s:if>
							<s:else>
							<tr>
							
								<s:set name="cellValue1" value="createdBy" scope="request"/>
													<td colspan="5"  <s:if test="amended==true">style="text-decoration:line-through"</s:if>>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>										
													</td>
									<td colspan="5" <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
								<s:text name="garuda.clinicalnote.label.posted_on"></s:text>
								<s:set name="cellValue15" value="createdOn" />
							        			<s:date name="cellValue15" id="cellValue15" format="MMM dd, yyyy / hh:mm:ss a"  />
							        			<s:property value="cellValue15"/>
								</td>
								<td colspan="5"  <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
								<s:text name="garuda.cdrcbuview.label.clinical_notes"></s:text>:
									<s:text name="garuda.cbu.label.clinical_note"/><s:property value="noteSeq"/><s:property value="getCodeListDescById(fkNotesCategory)" />
									<input type="hidden" name="pkNotes"/>
								</td>
								
							
								
								<td colspan="5" <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
								   
											<s:text name="garuda.clinicalnote.label.assessment"></s:text>
											             <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_ASSESS]">
														    <s:if test="subType==noteAssessment">
														         <s:property value="description"/>
														    </s:if> 
														</s:iterator>
											       
								</td>
								<td colspan="2" <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
								    
											       <s:text name="garuda.clinicalnote.label.visibility"></s:text>
											           <s:if test="visibility==true"><s:text name="garuda.clinicalnote.label.visibleToTc" /></s:if>
											           <s:else><s:text name="garuda.clinicalnote.label.notvisibleToTc" /></s:else>
											       
								</td>
								<td colspan="2" <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
								    
									
									
											       <s:text name="garuda.clinicalnote.label.flagForLater"></s:text>:
											           <s:if test="flagForLater==true"><s:text name="garuda.clinicalnote.label.yes" /></s:if>
											           <s:else><s:text name="garuda.clinicalnote.label.no" /></s:else>
											       
								</td>
						
							 <s:if test="hasEditPermission(#request.vClnclNote)==true">
								<td>
										<a href ="#" onClick="loadPageByGetRequset('revokeNote2?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes='+<s:property value="pkNotes"/>,'parentClinic')"> <s:text name="garuda.clinicalnote.label.revoke" /></a>
								</td>
								</s:if>							
								<td>
										<a href ="#" onClick="showModal('Clinical Notes for Registry ID <s:property value="cdrCbuPojo.registryId"/>','viewNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property value="pkNotes"/>','500','850');"> <s:text name="View" /></a>
								</td>
								<s:if test="hasEditPermission(#request.vClnclNote)==true">
								<td>
										
										<a href ="#" onClick="showModals('Reply Notes for Registry ID <s:property value="cdrCbuPojo.registryId"/>','replyNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REPLY_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.fkNotesCategory=<s:property value="fkNotesCategory"/>&clinicalNotePojo.noteSeq='+<s:property value="noteSeq"/>,'500','850','notesModalsDiv');"> <s:text name="garuda.clinicalnote.label.reply" /></a>
								</td>
								</s:if>
						    </tr>
						    <tr>
						    	<td colspan="26" <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
						    	<div style="overflow: auto;">
						    	<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value="notes"/></strong>
						    	</div>
						    	</td>
						    </tr>	
						   </s:else>
						 
						 </s:if>
						</s:iterator>
				   </table>
				
  		 				</td>
  		 	</tr>
  		</table></div>
  	
  		</s:if>
  	
  		</div>
			