<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%String contextpath = request.getContextPath()+"/jsp/modal/"; %>
<script>
/* $j(document).ready(function(){
var path="D:\Garuda_new_ws\Garuda\Garuda_web\src\main\webapp\jsp\pdf\COI";
var filetype ="application/pdf";
var obj='<object data="'+path+'" height="750" width="100%" type="'+filetype+'"></object>';
$j('#downloaddoc1').append(obj);
}); */
</script>
<div style="overflow: scroll;">
	<div class="column" id="productInsertDiv">
		<table border="1" width="90%" align="center">
		<tr><td>
		<div>
			<jsp:include page="../cb_CBUrecipt.jsp">
					          	 	<jsp:param name="cbuRecipt" value="true"/>
			</jsp:include>
		</div></td></tr> </table> 	
		<table border="1" width="90%" align="center">
		<tr><td>
		<s:form id="productinsertreport" name="productinsertreport" method="post">
			<div class="portlet">
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
						<table width="100%">
							<tr>
								<td colspan="6" align="center">
									<b><s:text name="garuda.productinsert.label.productinsertreportnmdp" />
								</b></td>
							</tr>
						</table>
					</div>
					<div>
						<table border="1">
							<tr>
								<td colspan="6" align="center">
									<s:text name="garuda.productinsert.label.productinsertreporthdrtxtnmdp" />
								</td>
							</tr>
						</table>
					</div>
						<table width="100%">
							<tr>
								<td width="25%"><b><s:text name="garuda.productinsert.label.nmdpcbuid" /></b>:</td>
								<td width="20%"><s:property value="cdrCbuPojo.registryId" /></td>
								<td colspan="2" width="30%"/>
								<td width="15%"><b><s:text name="garuda.unitreport.label.localcbuid" /></b>:</td>
								<td width="10%"><s:property value="cdrCbuPojo.localCbuId" /></td>
								
							</tr>
							<tr>
								<td width="25%"><b><s:text
									name="garuda.cdrcbuview.label.collection_date" />:</b></td>
								<td width="20%">
								<s:if test="%{cdrCbuPojo.specimen.specCollDate != null }">
									<s:text name="collection.date">:
										<s:param name="value" value="cdrCbuPojo.specimen.specCollDate" />
									</s:text>
								</s:if>
								</td>
								<td width="15%"><b><s:text
									name="garuda.productinsert.label.time" />:</b></td>
								<td width="15%"><s:property value="cdrCbuPojo.cbuCollectionTime" /></td>
								<td width="15%"><b><s:text
									name="garuda.productinsert.label.productcode" />:</b></td>
								<td width="10%"><s:property value="cdrCbuPojo.productCode"/></td>
							</tr>
							<tr>
								<td width="25%">
									<b><s:text
										name="garuda.cdrcbuview.label.licensure_status" />:</b></td>
								<td width="20%">
										<s:property value="cdrCbuPojo.license" /></td>
								<td colspan="4"/ width="55%">
							</tr>
							<tr>
								<td colspan="6" width="100%"><b><u><s:text
										name="garuda.productinsert.label.unlicensedcordbloodunitheader" /></u></b></td>
							</tr>
						
							<tr>
								<td width="25%"><b><s:text
									name="garuda.productinsert.label.productname" />:</b></td>
								<td width="20%"><s:property value="cbbProcedurePojo.procName"/></td>
								<td colspan="4"/ width="55%">
							</tr>
							<tr>
								<td width="25%"><b><s:text
									name="garuda.productinsert.label.totalcbuvolumefrzn" />:</b></td>
								<td width="20%">
									<s:iterator value="postTestList" status="row">
										<s:if test="#request.fnpvPk==fktestid">
											<s:property value="testresult"/>
										</s:if>
									</s:iterator>
								</td>
								<td width="15%"><b><s:text
									name="garuda.cbuentry.label.abo" />:</b></td>
								<td width="15%"><s:property value="cdrCbuPojo.bloodGpDes"/></td>
								<td width="15%"><b><s:text
									name="garuda.cbuentry.label.rhtype" />:</b></td>
								<td width="10%"><s:property value="cdrCbuPojo.rhTypeDes"/></td>
							</tr>
							<tr>
								<td width="25%"><b><s:text
									name="garuda.productinsert.label.totalCBUnuclcellcntfrzn" />:</b></td>
								<td width="20%">
									<s:iterator value="postTestList" status="row">
										<s:if test="#request.tcnccPk==fktestid">
											<s:property value="testresult"/>
										</s:if>
									</s:iterator></td>
								<td width="15%"><b><s:text
									name="garuda.openorder.label.processingdate" />:</b></td>
								<td width="15%"><s:date name="cdrCbuPojo.processingDate" id="formFillDt" format="MMM dd, yyyy" /><s:property value="%{formFillDt}"/></td>
								<td colspan="2"/ width="25%">
								
							</tr>
							<tr>
								<td width="25%"><b><s:text
									name="garuda.productinsert.label.productmodification" />:</b></td>
								<td width="20%"><s:property value="getCodeListDescById(cbbProcedureInfoPojo.fkProductModification)"/></td>
								<td colspan="4" width="55%"/>
							</tr>
						
							<tr>
								<td colspan="6" width="100%"><b><u><s:text
										name="garuda.productinsert.label.additivesvolredheader" /></u></b></td>
							</tr>
							<tr>
								<td width="25%"><b><s:text
										name="garuda.productinsert.label.typeofanticoagulant" /></b></td>
								<td width="20%"><b><s:text
										name="garuda.productinsert.label.volume/percent" /></b></td>
								<td colspan="4" width="55%"/>
							</tr>
							<s:if test="cbbProcedureInfoPojo.thouUnitPerMlHeparin!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.thouunitpermlheparin"></s:text>:
										</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.thouUnitPerMlHeparin" /><s:if test="cbbProcedureInfoPojo.thouUnitPerMlHeparinper!=null">/<s:property
											value="cbbProcedureInfoPojo.thouUnitPerMlHeparinper" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.fiveUnitPerMlHeparin!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.fiveunitpermlheparin"></s:text>:
										</td>
										<td  width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.fiveUnitPerMlHeparin" /><s:if test="cbbProcedureInfoPojo.fiveUnitPerMlHeparinper!=null">/<s:property
											value="cbbProcedureInfoPojo.fiveUnitPerMlHeparinper" /></s:if>
											</td>
											<td colspan="4" width="55%"/>
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.tenUnitPerMlHeparin!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.tenunitpermlheparin"></s:text>:
										</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.tenUnitPerMlHeparin" /><s:if test="cbbProcedureInfoPojo.tenUnitPerMlHeparinper!=null">/<s:property
											value="cbbProcedureInfoPojo.tenUnitPerMlHeparinper" /></s:if>
										<td>
										<td colspan="4" width="55%" />
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.sixPerHydroxyethylStarch!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.sixperhydroxyethylstarch"></s:text>:
										</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.sixPerHydroxyethylStarch"/><s:if test="cbbProcedureInfoPojo.sixPerHydroxyethylStarchper!=null">/<s:property
											value="cbbProcedureInfoPojo.sixPerHydroxyethylStarchper" /></s:if>
										</td>
										<td colspan="4" width="55%" />
										
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.cpda!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.cpda"></s:text>:</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.cpda" /><s:if test="cbbProcedureInfoPojo.cpdaper!=null">/<s:property
											value="cbbProcedureInfoPojo.cpdaper" /></s:if>
										</td>
										<td colspan="4" width="55%" />
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.cpd!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.cpd"></s:text>:</td>
										<td width="20%" align="center"><s:property value="cbbProcedureInfoPojo.cpd"/><s:if test="cbbProcedureInfoPojo.cpdper!=null">/<s:property
											value="cbbProcedureInfoPojo.cpdper" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.acd!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.acd"></s:text>:</td>
										<td width="20%" align="center"><s:property value="cbbProcedureInfoPojo.acd"/><s:if test="cbbProcedureInfoPojo.acdper!=null">/<s:property
											value="cbbProcedureInfoPojo.acdper" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.otherAnticoagulant!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.otheranticoagulant"></s:text>:</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.otherAnticoagulant"/><s:if test="cbbProcedureInfoPojo.otherAnticoagulantper!=null">/<s:property
											value="cbbProcedureInfoPojo.otherAnticoagulantper" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
							</tr>
							</s:if>
						
							<tr>
								<td colspan="6" width="100%"><b><u><s:text
										name="garuda.productinsert.label.cryoprotectantsotheradditives" /></u></b></td>
							</tr>
							<tr>
								<td width="25%"><b><s:text
										name="garuda.productinsert.label.typeofcryoprotectant" /></b></td>
								<td width="20%"><b><s:text
										name="garuda.productinsert.label.volume/percent" /></b></td>
								<td colspan="4" width="55%"/>
							</tr>
							<s:if test="cbbProcedureInfoPojo.hunPerDmso!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.hunperdmso"></s:text>:</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.hunPerDmso"/><s:if test="cbbProcedureInfoPojo.hunPerDmsoper!=null">/<s:property
											value="cbbProcedureInfoPojo.hunPerDmsoper" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.hunPerGlycerol!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.hunperglycerol"></s:text>:</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.hunPerGlycerol" /><s:if test="cbbProcedureInfoPojo.hunPerGlycerolper!=null">/<s:property
											value="cbbProcedureInfoPojo.hunPerGlycerolper" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.tenPerDextran_40!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.tenperdextran40"></s:text>:</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.tenPerDextran_40" /><s:if test="cbbProcedureInfoPojo.tenPerDextran_40per!=null">/<s:property
											value="cbbProcedureInfoPojo.tenPerDextran_40per" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.fivePerHumanAlbu!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.fiveperhumanalbu"></s:text>:</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.fivePerHumanAlbu"/><s:if test="cbbProcedureInfoPojo.fivePerHumanAlbuper!=null">/<s:property
											value="cbbProcedureInfoPojo.fivePerHumanAlbuper" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.twenFiveHumAlbu!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.twenfivehumalbu"></s:text>:</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.twenFiveHumAlbu"/><s:if test="cbbProcedureInfoPojo.twenFiveHumAlbuper!=null">/<s:property
											value="cbbProcedureInfoPojo.twenFiveHumAlbuper" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.plasmalyte!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.plasmalyte"></s:text>:</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.plasmalyte" /><s:if test="cbbProcedureInfoPojo.plasmalyteper!=null">/<s:property
											value="cbbProcedureInfoPojo.plasmalyteper" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.othCryoprotectant!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.othcryoprotectant"></s:text>:</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.othCryoprotectant"/><s:if test="cbbProcedureInfoPojo.othCryoprotectantper!=null">/<s:property
											value="cbbProcedureInfoPojo.othCryoprotectantper" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
							</tr>
							</s:if>
						
							<tr>
								<td colspan="6" width="100%"><b><u><s:text
										name="garuda.productinsert.label.diluents" /></u></b></td>
							</tr>
							<tr>
								<td width="25%"><b><s:text
										name="garuda.productinsert.label.otheraddtiveshedr" /></b></td>
								<td width="20%"><b><s:text
										name="garuda.productinsert.label.volume/percent" /></b></td>
								<td colspan="4" width="55%"/>
							</tr>
							<s:if test="cbbProcedureInfoPojo.fivePerDextrose!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.fiveperdextrose"></s:text>:</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.fivePerDextrose" /><s:if test="cbbProcedureInfoPojo.fivePerDextroseper!=null">/<s:property
											value="cbbProcedureInfoPojo.fivePerDextroseper" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.pointNinePerNacl!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.pointninepernacl"></s:text>:</td>
										<td width="20%" align="center"><s:property
											value="cbbProcedureInfoPojo.pointNinePerNacl" /><s:if test="cbbProcedureInfoPojo.pointNinePerNaclper!=null">/<s:property
											value="cbbProcedureInfoPojo.pointNinePerNaclper" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
							</tr>
							</s:if>
							<s:if test="cbbProcedureInfoPojo.othDiluents!=null">
							<tr>
										<td width="25%"><s:text
											name="garuda.cbbprocedures.label.otherdiluents"></s:text>:</td>
										<td width="20%" align="center"> <s:property
											value="cbbProcedureInfoPojo.othDiluents" /><s:if test="cbbProcedureInfoPojo.othDiluentsper!=null">/<s:property
											value="cbbProcedureInfoPojo.othDiluentsper" /></s:if>
										</td>
										<td colspan="4" width="55%"/>
										
							</tr>	
							</s:if>	
						</table>
						<table width="100%">
							<tr>
								<td colspan="6" width="100%"><b><u><s:text
										name="garuda.productinsert.label.recentinfectiousdiseasetesting" /></u>:</b></td>
								<td><s:property value=""/></td>
								
							</tr>
							<tr>
								<td colspan="6" width="100%"><b><s:text
										name="garuda.productinsert.label.warningtext" /></b></td>
							</tr>
							<tr>
								<td colspan="6" width="100%"><b><s:text
										name="garuda.productinsert.label.requiredstoragetemp" /></b></td>
							</tr>
							<tr>
								<td colspan="6" width="100%"><b><s:text
										name="garuda.productinsert.label.estblprovdcordbldunit" /></b></td>
							</tr>
							<tr><td colspan="6" width="100%"/></tr>
							<tr>
								<td width="25%"><b><s:text
										name="garuda.assigntask.label.name" />:</b></td>
								<td  width="45%" align="left" colspan="2"><s:property	value="site.siteName" /></td>
								
								<td  width="20%" align="left" colspan="2"><b><s:text
										name="garuda.productinsert.label.cbbcode" />:</b></td>
								<td width="10%"><s:property	value="site.siteIdentifier" /></td>
								
							</tr>
							<tr>
								<td width="25%"><b><s:text
										name="garuda.productinsert.label.address" />:</b></td>
								<td width="45%" align="left" colspan="2"><s:property value="site.address.address1" /></td>
								
								<td width="20%" align="left" colspan="2"><b><s:text
										name="garuda.productinsert.label.contactname" />:</b></td>
								<td width="10%"><s:property value="garuda.assigntask.label.name" /></td>
			
							</tr>
							<tr>
								<td width="25%"><b><s:text
										name="garuda.cbbdefaults.lable.country" />:</b></td>
								<td width="45%" align="left" colspan="2"><s:property value="site.address.country" /></td>
								
								<td width="20%" align="left" colspan="2"><b><s:text
										name="garuda.productinsert.label.contactphone" />:</b></td>
								<td width="10%"><s:property value="garuda.assigntask.label.name" /></td>
			
							</tr>
							<tr>
								<td colspan="3" width="70%"><s:text
										name="garuda.productinsert.label.bottomtxt" /><br><s:text
										name="garuda.productinsert.label.cautiontxt" />:<s:text
										name="garuda.productinsert.label.bottomtxt1" /><br><s:text
										name="garuda.productinsert.label.bottomstatementtxt" /></td>
								
								<td colspan="3" width="30%"><b><s:text
										name="garuda.productinsert.label.notirradiatewarning" /><br><s:text
										name="garuda.productinsert.label.notxRaywarning" /></b></td>
							</tr>		
						</table>
			
			
			
			</div>
		</s:form></td></tr> </table> 
		<table border="1" width="90%" align="center"><tr><td>
		<div class="column">
					<div class="portlet-content" id="idmtestform">
						<%-- <s:if test="%{idmTestcount!=null && idmTestcount>0}"> --%>
							<jsp:include page="../cb_viewIdmFrProductInsert.jsp"></jsp:include>
						<%-- </s:if> --%>
						<%-- <s:else>
							<jsp:include page="../cb_add_idmtest.jsp"></jsp:include>
						</s:else> --%>			
					
					</div>
	    </div>
	    </td></tr> </table> 
	    <table border="1" width="90%" align="center"><tr><td>
	    <div id="fode" >
	    	<jsp:include page="../cb_finaDecOfEligibleForProductInsert.jsp"></jsp:include>
	    </div>
	    </td></tr> </table> 
	    <%-- <div>
									<table width="100%" cellspacing="0" cellpadding="0">
									    <tr>
									      <td>
												<div onclick="toggleDiv('prodtaglevel')"
												class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
												style="text-align: center;"><span
												class="ui-icon ui-icon-triangle-1-s"></span>
												<s:text name="garuda.cdrcbuview.label.product_tag_level" /></div>
												<div id="prodtaglevel">	
												    <jsp:include page="../cord-entry/cb_product_tag.jsp"></jsp:include>
												</div>
									        </td>
									    </tr>
									  </table>
		</div> --%>
		<table border="1" width="90%" align="center"><tr><td>
				<div id="downloaddoc1"  style="padding-top: 5px;">
			<OBJECT data="pdf/COI.pdf" type="application/pdf" width="100%" height="600"></OBJECT>
		</div>
			</td></tr> </table> 
	
</div></div>