<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants" %>
<script>
$j(document).ready(function(){
	getShipmentTbl();
});
function getShipmentTbl(){

	$j('#cbureceipt').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.attachmentNA"/>"
		},
		"bRetrieve": true,
		"bDestroy" :true
	});
	
	var tdObj = $j('#cbureceipt').find('tbody').find("tr:eq(0)").find("td:eq(0)");
	if($j(tdObj).hasClass('dataTables_empty')){
		$j('#cbureceipt_info').hide();
		$j('#cbureceipt_paginate').hide();
	}
	//$j('#shipmentItern_length').hide();
	//$j('#shipmentItern_filter').hide();
	//$j('#shipmentItern_info').hide();
}

function revokeDocumentModal(cdrCbuId,attachmentId,dcmsAttachId,title,url1,lenght,breadth){
	
 	var orderId=$j("#orderId").val();
	var orderType=$j("#orderType").val();	
	
	var answer = confirm('Please confirm if you want to revoke the document.');
	if (answer){	
				if(orderId!=null && orderId!="" && orderId!="undefined" && orderType!=null && orderType!="" && orderType!="undefined"){
						var url='revokeDocFromPF?orderId='+orderId+'&orderType='+orderType+'&cbuCordId='+cdrCbuId+'&cbuid='+cdrCbuId+'&cordId='+cdrCbuId+'&pkcordId='+cdrCbuId+'&cordID='+cdrCbuId+'&attachmentId='+attachmentId;
						loadMoredivs(url,'DUMNDiv,quicklinksDiv','cbuReceipt','','');
						modalFormSubmitRefreshDiv('cbuReceipt',url1,'searchTble1');
						alert("Document revoked successfully");
					}
				else{
				
						var url='revokeDoc?cbuCordId='+cdrCbuId+'&cbuid='+cdrCbuId+'&cordId='+cdrCbuId+'&pkcordId='+cdrCbuId+'&cordID='+cdrCbuId+'&attachmentId='+attachmentId;
						loadMoredivs(url,'DUMNDiv,quicklinksDiv','cbuReceipt','','');
						modalFormSubmitRefreshDiv('cbuReceipt',url1,'searchTble1');
						alert("Document revoked successfully");
				}
	}
}

</script>
<s:form id="cbuReceipt">
	<s:hidden name="cdrCbuPojo.cdrCbuId" id="cdrCbuId"></s:hidden> 
	<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
	<s:hidden name="cdrCbuPojo.registryId"  id="cburegid"></s:hidden>
		<div id="shipment" class="portlet-content">
			<div id="searchTble1">
			<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="cbureceipt">
				<thead>
					<tr>
						<th class="th2" scope="col"><s:text name="garuda.uploaddoc.label.date" /></th>
						<th><s:text name="garuda.uploaddoc.label.documentuploadedby" /></th>
					    <th><s:text name="garuda.uploaddoc.label.documentcategory" /></th>
				        <th><s:text name="garuda.uploaddoc.label.documentsubcategory" /></th>
				        <th><s:text name="garuda.openorder.label.completiondate" /></th>
						<th class="th3" scope="col"><s:text name="garuda.openorder.label.description" /></th>
						<th class="th5" scope="col"><s:text name="garuda.uploaddoc.label.view" /></th>
						<th><s:text name="garuda.common.lable.modify" /></th>
						<th><s:text name="garuda.common.lable.revoke" /></th>
					</tr>
				</thead>
				<tbody>
						<s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@POST_SHPMNT_CODESUBTYPE]" var="rowVal" status="row">
							<s:set name="cellValue1" value="%{#rowVal[14]}" scope="request"/><tr>
							<td><s:date name="%{#rowVal[10]}" id="uploadDate" format="MMM dd, yyyy / hh:mm:ss a" /><s:property value="%{#uploadDate}" /></td>
							<td>
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>	
					        </td>	
					        <td><s:property value="getCodeListDescByPk(#rowVal[15])" /></td>
					        <td><s:property value="getCodeListDescByPk(#rowVal[12])" /></td>
					        <td><s:date name="%{#rowVal[20]}" id="completionDate" format="MMM dd, yyyy"/><s:property value="%{#completionDate}" /></td>
							<td><s:property value="%{#rowVal[13]}" /></td>
							<td><a href="#"><img src="images/attachment-icon.png"
								onclick="javascript: return showDocument('<s:property value="%{#rowVal[5]}" />')" /></a></td>
							<s:if test="hasEditPermission(#request.viewPostShipCat)==true">
							  <td><button type="button" onclick="javascript:modifyDocument('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="%{#rowVal[3]}"/>','<s:property value="%{#rowVal[5]}" />');"><s:text name="garuda.common.lable.modify" /></button></td>
							  <td><button type="button" onclick="javascript: revokeDocumentModal('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="%{#rowVal[5]}" />','<s:text name="garuda.cdrcbuview.label.button.cbureceipt"/> <s:property value="cdrCbuPojo.registryId"/>','getCbuReceipt?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />','400','500')" ><s:text name="garuda.common.lable.revoke" /></button></td>
							</s:if>	
								<s:else>
									<td><button type="button" disabled="disabled"><s:text name="garuda.common.lable.modify" /></button></td>
									<td><button type="button" disabled="disabled"><s:text name="garuda.common.lable.revoke" /></button></td>
				               </s:else>
							</tr>
						</s:iterator>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="6"></td>
					</tr>
				</tfoot>
			</table>
			</div>
			</div>
</s:form>