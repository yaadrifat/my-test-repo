<%@ page language="java" import="java.util.*,com.velos.eres.web.user.UserJB, com.velos.eres.service.util.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<jsp:include page="../cb_track_session_logging.jsp" />
<%@ taglib prefix="s"  uri="/struts-tags"%>
<%
	Long cbuCateg = null;
	if(request.getParameter("cbuCateg")!=null){
		cbuCateg = Long.parseLong(request.getParameter("cbuCateg"));
	}else if(request.getAttribute("cbuCateg")!=null){
		cbuCateg = (Long)request.getAttribute("cbuCateg");
	}
	request.setAttribute("cbuCateg",cbuCateg);
	
%>
<s:set name="cbuCateg1" value="#request.cbuCateg" />
	<table>
				
				<tbody>
						<s:iterator value="#request.categoryNotes[#cbuCateg1]" var="rowVal" status="row">
						<s:set name="amendFlag1" value="%{#rowVal[5]}" scope="request"/>
						<s:set name="noteSeq" value="%{#rowVal[2]}" scope="request"/>
						<s:set name="replyFlag" value="%{#rowVal[8]}" scope="request"/>
						<s:set name="notes" value="%{#rowVal[9]}" scope="request"/>
						<s:set name="flagForLater" value="%{#rowVal[10]}" scope="request"/>
							<tr>
								<s:iterator value="rowVal" var="cellValue" status="col">
								<s:if test="#request.replyFlag==true">
										<s:if test="%{#col.index == 0}">
												 <s:set name="cellValue1" value="cellValue" scope="request"/>
												 <td></td>
													<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
														String cellValue1 = request.getAttribute("cellValue1").toString();
														UserJB userB = new UserJB();
														userB.setUserId(EJBUtil.stringToNum(cellValue1));
														userB.getUserDetails();													
													%>		
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>										
													</td>
										</s:if>
										<s:if test="%{#col.index == 1}">
										<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text name="garuda.clinicalnote.label.posted_on"></s:text>
									  		<s:set name="cellValue15" value="cellValue" />
							       	 			<s:date name="cellValue15" id="cellValue15" format="MMM dd, yyyy / hh:mm:ss a"  />
												<s:property value="cellValue15"/>
										</td>
									</s:if>
											<s:if test="%{#col.index == 2}">
												<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.cdrcbuview.label.clinical_notes"></s:text>
											<s:text name="garuda.cbu.label.reClinical"/><s:property />
												</td>
										</s:if>								
								<s:if test="hasEditPermission(#request.vClnclNote)==true">
										<s:if test ="%{#col.index == 6}" ><td>
											<a href ="#" onClick="loadParticularDivWithoutForm('revokeCategoryNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&clinicalNotePojo.fkNotesCategory=<s:property value="#cbuCateg1"/>','','','')"> <s:text name="garuda.clinicalnote.label.revoke" /></a>
											
											</td>
											</s:if>
											</s:if>
											<s:if test ="%{#col.index == 6}" >
												<td>
												<a href ="#" onClick="showModal('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','viewNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>','500','850');"> <s:text name="garuda.label.dynamicform.view"/></a>
												
											</td>
											</s:if>
											<s:if test ="%{#col.index == 6}" >
											<td>
												
												<a href ="#" onClick="showModals('Reply Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','replyNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REPLY_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&fkNoteCategory=<s:property value="#request.cbuCateg1"/>&noteSeq=<s:property value="#request.noteSeq"/>','500','850','notesModalsDiv');"> <s:text name="garuda.clinicalnote.label.reply" /></a>
												
											</td>
											</s:if>	
										
								</s:if>
								<s:else>
								<s:if test="%{#col.index == 0}">
												 <s:set name="cellValue1" value="cellValue" scope="request"/>
													<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
														String cellValue1 = request.getAttribute("cellValue1").toString();
														UserJB userB = new UserJB();
														userB.setUserId(EJBUtil.stringToNum(cellValue1));
														userB.getUserDetails();													
													%>		
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>										
													</td>
								</s:if>
									<s:if test="%{#col.index == 1}">
									<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text name="garuda.clinicalnote.label.posted_on"></s:text>
									  <s:set name="cellValue15" value="cellValue" />
							       	 			<s:date name="cellValue15" id="cellValue15" format="MMM dd, yyyy / hh:mm:ss a"   />
												<s:property value="cellValue15"/>
									</td>
								</s:if>
								<s:if test="%{#col.index == 2}">
									<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.cbu.label.clinical"/><s:property />
									</td>
								</s:if>								
							
												<s:if test ="%{#col.index == 3}" >
												 <s:set name="cellValue3" value="cellValue" scope="request"/>
												<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if> style="padding:0">
														
						   										<s:text name="garuda.clinicalnote.label.assessment"></s:text>
														
											        						<s:set name="cellValue3" value="cellValue" scope="request"/> 
														         			 <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_ASSESS]" var="subType" >
														    				 
														    				
														    				<s:if test="subType==#cellValue">
														         			<s:property value="description"/>
														    				</s:if> 
																		</s:iterator>
											        					
												</td>
											</s:if>
											<s:if test ="%{#col.index == 4}" >
											<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if> style="padding:0"><s:set name="cellValue2" value="cellValue" scope="request"/> 
												
						   								<s:text name="garuda.clinicalnote.label.visibility"></s:text>
													
											    				<s:if test ="#request.cellValue2==true" >
											        			<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
											           			<s:text name="garuda.clinicalnote.label.visibleToTc" />
											        			</td>
											        			</s:if>
											        			<s:else>
											        				<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
											           			<s:text name="garuda.clinicalnote.label.notvisibleToTc" />
											        			</td>
											        			</s:else>
											   	 			
											</td>
											
													<td <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if> style="padding:0">
											      		 <s:text name="garuda.clinicalnote.label.flagForLater"></s:text>:
											           <s:if test="#request.flagForLater==true"><s:text name="garuda.clinicalnote.label.yes" /></s:if>
											           <s:else><s:text name="garuda.clinicalnote.label.no" /></s:else>
											       
													</td>
											</s:if>
											 <s:if test="hasEditPermission(#request.vClnclNote)==true">
											<s:if test ="%{#col.index == 6}" ><td>
											<a href ="#" onClick="loadParticularDivWithoutForm('revokeCategoryNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&clinicalNotePojo.fkNotesCategory=<s:property value="#cbuCateg1"/>','','','')"> <s:text name="garuda.clinicalnote.label.revoke" /></a>
											
											</td>
											</s:if></s:if>
											<s:if test ="%{#col.index == 6}" >
												<td>
												<a href ="#" onClick="showModal('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','viewNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>','500','850');"> <s:text name="garuda.clinicalnote.label.view" /></a>
												
											</td>
											</s:if>
											<s:if test="hasEditPermission(#request.vClnclNote)==true">
											<s:if test ="%{#col.index == 6}" >
											<td>
												
												<a href ="#" onClick="showModals('Reply Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','replyNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REPLY_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&fkNoteCategory=<s:property value="#request.cbuCateg1"/>&noteSeq=<s:property value="#request.noteSeq"/>','500','850','notesModalsDiv');"> <s:text name="garuda.clinicalnote.label.reply" /></a>
												
											</td>
											</s:if>	
											</s:if>
								</s:else>			
							</s:iterator>
							</tr>	
							<tr>
							 <s:if test="#request.replyFlag==true">
							<td></td>
							</s:if> 
								<td colspan="9" <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
									<div style="overflow: auto;">
									<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
								    </div>
								</td>
							</tr>
							</s:iterator>					
				</tbody>
				<tfoot>
					<tr>
						<td colspan="6"></td>
					</tr>
				</tfoot>
			</table>