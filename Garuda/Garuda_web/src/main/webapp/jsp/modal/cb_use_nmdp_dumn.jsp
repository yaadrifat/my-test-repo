<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<div id="dumnwidgetcontent" onclick="toggleDiv('dumnwidget')"
		class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.unitreport.label.eligibility.decl_urg_med_need" /></div>
					<div id="dumnwidget">
						<table border="0" align="left" cellpadding="0" cellspacing="0">
						     <tr>
						        <td colspan="4">
						           <s:text name="garuda.unitreport.label.eval_infect_disease"/>
						        </td>						        
						     </tr>
						     <tr>
						        <td >&nbsp;</td><td colspan="3"><s:text name="garuda.unitreport.label.medical_record"/></td>
						     </tr>
						     <tr>
						        <td >&nbsp;</td><td><input type="checkbox" name="inel" <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.inEligiblePkid">checked="checked"</s:if> /><s:text name="garuda.cdrcbuview.label.ineligible" />:</td>
						        <td colspan="2"><s:text name="garuda.unitreport.label.comm_disease_risk"/></td>
						     </tr>
						     <tr>
						       <td></td><td><s:text name="garuda.unitreport.label.OR"/></td><td colspan="2"> <s:text name="garuda.unitreport.label.screening_test_completed"/></td>
						     </tr>
						     <tr>
						        <td >&nbsp;</td><td><input type="checkbox" name="inel" <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId">checked="checked"</s:if> /><s:text name="garuda.cdrcbuview.label.incomplete" />:</td>
						        <td colspan="2"><s:text name="garuda.unitreport.label.incomplete_elig_st1"/></td>
						     </tr>
						     <tr>
						        <td colspan="2"></td><td colspan="2"><s:text name="garuda.unitreport.label.identified_risk"/></td>
						     </tr>
						     <tr>
						        <td colspan="4">
						          <s:text name="garuda.unitreport.label.dec_elig_form"/>
						        </td>
						     </tr>
						     <tr>
						       <td colspan="4">
						         <s:text name="garuda.unitreport.label.unidentified_risk"/>
						       </td>
						     </tr>
						     <tr>
						        <td colspan="4">										          
						                   <s:iterator value="#request.reasonPojos" >	
						                   <s:set name="reasonId" value="fkReasonId" />									                     
						                      <s:if test="getCodeListSubTypeById(#reasonId)==@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_MATERNAL_REASON">
						                         <s:set name="mrq" value="true" scope="request"/>										                         
						                      </s:if>			
						                      <s:if test="getCodeListSubTypeById(#reasonId)==@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_INFECTIOUS_REASON">
						                         <s:set name="idm" value="true" scope="request"/>										                         
						                      </s:if>		
						                      <s:if test="getCodeListSubTypeById(#reasonId)==@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_PHYSICAL_REASON">
						                        <s:set name="physi" value="true" scope="request"/>										                        
						                      </s:if>	
						                      <s:if test="getCodeListSubTypeById(#reasonId)==@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_OTHER_REASON">
						                        <s:set name="other" value="true" scope="request"/>	
						                      </s:if>				                      										                   
						                   </s:iterator>	
						                   <jsp:include page="../cord-entry/cb_dumn_quest.jsp">
							                            <jsp:param value="<%=request.getAttribute(\"mrq\")%>" name="mrq"/>
							                            <jsp:param value="<%=request.getAttribute(\"idm\")%>" name="idm"/>
							                            <jsp:param value="<%=request.getAttribute(\"physi\")%>" name="physi"/>
							                            <jsp:param value="<%=request.getAttribute(\"other\")%>" name="other"/>
							               </jsp:include>
						        </td>										        
						     </tr>
						     <tr>
						        <td colspan="4">
						            <table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults4">
						                 <thead>
											<tr>
												<th class="th2" scope="col"><s:text
													name="garuda.uploaddoc.label.date" /></th>
												<th class="th3" scope="col"><s:text
													name="garuda.openorder.label.description" /></th>
												<th class="th5" scope="col"><s:text
													name="garuda.uploaddoc.label.view" /></th>
											</tr>
										</thead>
										<tbody>															
											<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]!=null">
								                <s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]" var="rowVal" status="row">
								                   <s:if test="%{#rowVal[12]}==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@URG_MED_NEED_CODESUBTYPE)">
								                      <tr>
														<td><s:date name="%{#rowVal[11]}" id="uploadDate"
															format="MMM dd, yyyy" /><s:property value="%{#uploadDate}" /></td>
														<td><s:property value="%{#rowVal[14]}" /></td>
														<td><a href="#"><img src="images/attachment-icon.png"
															onclick="javascript: return showDocument('<s:property value="%{#rowVal[6]}" />')" /></a></td>
													 </tr>
								                   </s:if>
								                </s:iterator>											                
								            </s:if>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="6"></td>
											</tr>
										</tfoot>
									</table>
						        </td>
						     </tr>											 
						  <%--   	<tr>
						        <td><button type="button" onclick="return false;" ><s:text name="garuda.cbuentry.label.notification_to_tc" /></button></td>
						        <td>&nbsp;</td>
						        <td><button type="button" onclick="showModal('Email To Transplant Center','sendMailHome?cdrCbuPojo.fkCordCbuEligibleReason=<s:property value="cdrCbuPojo.fkCordCbuEligibleReason"/>&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID"/>&mailCode=<s:property value="@com.velos.ordercomponent.util.VelosMailConstants@TRANSPLANT_CENTER_CODE"/>&cbuID=<s:property value="registryId" />','350','450')" ><s:text name="garuda.cbuentry.label.email_attachment" /></button></td>
						        <td><button type="button" onclick="printDUMN('dumnwidget',<s:property value="cdrCbuPojo.registryId"/>,'<s:property value="cdrCbuPojo.numberOnCbuBag"/>','<s:property value="cdrCbuPojo.site.siteName"/>','')" ><s:text name="garuda.common.lable.print" /></button></td>
						     </tr>	 --%>						     
						  </table>
						  
						</div>