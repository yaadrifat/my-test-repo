<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<div id="eligibleinfo1">
			<div id="searchTble1">							
				<table>
				    <tr>
				        <td>
				        <strong><s:text name="garuda.cdrcbuview.label.eligibility_determination" /></strong>:
				        </td>
				        <td>
				           <s:property value="CdrCbuPojo.eligibilitydes" />
				        </td>
				        <td>
				           <s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBUASSESSMENT_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBUASSESSMENT_CODESUBTYPE].size > 0 && cdrCbuPojo.site.isCDRUser()==false">
								                <s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBUASSESSMENT_CODESUBTYPE]" var="rowVal" status="row">
														<s:set name="docId" value="%{#rowVal[5]}" scope="request"/>																							 	
												</s:iterator>	
											     <a href="javascript:void(0);" onclick="window.open('getAttachmentFromDcms?docId=<s:property value="#request.docId"/>&signed=true','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><s:text name="garuda.unitreport.label.eligibility.cbu_assessment" /></a>												
							</s:if>
				        </td>
				    </tr>
					    <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.inEligiblePkid || cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId">
					        <tr>
						        <td valign="top">
						           <strong><s:text name="garuda.cdrcbuview.label.ineligible_reason" /></strong>:
						        </td>
						        <td colspan="2">
						           <table width="100%" cellpadding="0" cellspacing="0">
							             <s:iterator value="#request.reasonPojos" >
							             <s:set name="reasonId" value="fkReasonId" />
							              <tr>
							                  <td>
							                     <strong><s:property value="getCodeListDescById(#reasonId)" /></strong>
							                  </td>
							              </tr>
								        </s:iterator>
						           </table>	           
						        </td>										        
						   </tr>
						</s:if>	  
					   <tr>
						     <td colspan="3">
						          <table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults4">
					                 <thead>
										<tr>
											<th class="th2" scope="col"><s:text
												name="garuda.uploaddoc.label.date" /></th>
											<th class="th3" scope="col"><s:text
												name="garuda.openorder.label.description" /></th>
											<th class="th5" scope="col"><s:text
												name="garuda.uploaddoc.label.view" /></th>
										</tr>
									</thead>
									<tbody>															
										<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]!=null">
							                <s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE]" var="rowVal" status="row">
							                   <s:if test="%{#rowVal[12]}==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_CODESUBTYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@FINAL_DEC_OF_ELIG_CODESUBTYPE)">
							                      <tr>
													<td><s:date name="%{#rowVal[11]}" id="uploadDate"
														format="MMM dd, yyyy" /><s:property value="%{#uploadDate}" /></td>
													<td><s:property value="%{#rowVal[14]}" /></td>
													<td><a href="#"><img src="images/attachment-icon.png"
														onclick="javascript: return showDocument('<s:property value="%{#rowVal[6]}" />')" /></a></td>
												 </tr>
							                   </s:if>
							                </s:iterator>											                
							            </s:if>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="6"></td>
										</tr>
									</tfoot>
								</table>
						     </td>
						</tr>
					    <tr>
					        <td colspan="2">
					            <s:hidden name="declPojo.pkEligDecl"></s:hidden>
					            <jsp:include page="../cord-entry/cb_final_decl_quest.jsp" />
					        </td>
					    </tr>									
				</table>
					</div>
					  </div>
<div id="notcbuassessmentcontent" onclick="toggleDiv('notcbuassessment')"
			class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
			style="text-align: center;"><span
			class="ui-icon ui-icon-triangle-1-s"></span> <s:text
			name="garuda.unitreport.label.eligibility.not_cbu_assessment" /></div>
			  <div id="notcbuassessment">
			    <table border="0" align="left" cellpadding="0" cellspacing="0">
			       <tr>
			          <td colspan="4">
			          <s:text name="garuda.unitreport.label.cbu_coll_date"/>
			          </td>
			      </tr>
			      <tr>
			          <td colspan="4">
			          <s:text name="garuda.unitreport.label.cbu_coll_date_cont"/>
			          </td>
			     </tr>
			     <tr>
			          <td colspan="3">
			          <s:text name="garuda.unitreport.label.cbu_coll_date_cont1"/>
			          </td><td>&nbsp;</td>
			     </tr>
			     <tr>
			          <td colspan="3">
			          <strong><s:text name="garuda.unitreport.label.details_cbu_table"/></strong>
			          </td><td>&nbsp;</td>
			     </tr>
			   </table>
</div>
			