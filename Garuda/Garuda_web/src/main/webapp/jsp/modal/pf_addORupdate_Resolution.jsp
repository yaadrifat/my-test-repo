<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script type="text/javascript">
function updateResolution(formname){
	var ordid=$j('#orderId').val();
	var ordType=$j('#orderType').val();
	var pkcord=$j('#pkcordId').val();
	var resolpk=$j('#pResolutionCBB').val();
	/*var chrea=$j('#chgRsn').val();*/
	//alert("ordid:"+ordid+"ordType:"+ordType+"pkcord:"+pkcord+"resolpk:"+resolpk);
	if(resolpk==""){
		$j('#chgreasonspan').removeAttr('style');
	}
	else{
		setTimeout(function(){
			modalFormSubmitRefreshDiv(formname,'addResolutionCode?resolcode='+$j('#pResolutionCBB').val()+'&orderType='+ordType+'&orderId='+ordid+'&pkcordId='+pkcord,'main');	
		},0);
		updateHistoryDiv();
	}
}

</script>

<s:form id="addResolutionform">	
<s:hidden name="resolUpdate" value="True"></s:hidden>
<s:hidden name="orderType" id="orderType"></s:hidden>
<s:hidden name="orderId" id="orderId"></s:hidden>
<s:hidden name="pkcordId" id="pkcordId"></s:hidden>

<div id="update" style="display: none;" >
	  <jsp:include page="../cb_update.jsp">
	    <jsp:param value="garuda.updateresol.label.resoladded" name="message"/>
	  </jsp:include>	   
</div>



<div id="status">	

	<br/>
	<br/>
	<br/>
	<table width="100%" cellspacing="0" cellpadding="0" border="0" >
			<tr>
					<td width="40%"><strong><s:text name="garuda.ctResolution.label.resolutionbycbb"/></strong>:</td>
					<td width="60%">
						<s:select list="lcbustatus"  headerValue="select" listKey="pkcbustatus" listValue="cbuStatusDesc" headerKey="" id="pResolutionCBB" />
					</td>
			</tr>
			<!--<tr>
					<td width="40%"><strong><s:text name="Reason"/></strong>:</td>
					<td width="60%">
					<s:textarea name="reason" id="chgRsn" value="" maxSize="200" style="width: 147px; height: 73px;"></s:textarea>
					</td>
			</tr>
			--><tr>
					<td width="40%"></td>
					<td width="60%" style="color: red;"><span id="chgreasonspan" style="display:none;color:red;"><s:text name="garuda.resolution.label.enter_value"/></span></td>
			</tr>
			
	</table>
		<br/>	<br/>	<br/>	<br/>	
	<table bgcolor="#cccccc" style="padding-top: 5px;"  width="100%">
	<tr bgcolor="#cccccc" valign="baseline">
	 <td width="70%"><jsp:include page="../cb_esignature.jsp" flush="true"></jsp:include></td>
    <td width="15%" align="right">
    <input type="button" id="submit" disabled= "disabled" onclick="updateResolution('addResolutionform');" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
    </td>
    <td width="15%"><input type="button"  onclick="closeModal();" value="<s:text name="garuda.unitreport.label.button.cancel"/>" /></td>
    </tr>	
  </table>

</div>

</s:form>

