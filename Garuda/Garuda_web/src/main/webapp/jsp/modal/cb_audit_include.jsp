<%String contextpath = request.getContextPath()+"/jsp/modal/"; 
String contxtpath = request.getContextPath();
%>
<script type="text/javascript" src="<%=contxtpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<%@ taglib prefix="s" uri="/struts-tags"%>
<script>
$j(function(){
	$j("#audit_dummyForm").find(".datePicWOMinDate").attr("readonly","readonly");
});
function fn_FilterDocInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var cbuInfoParam="";
	var showEntries="";
	var docCat = "";
	var userId = "";
	var docSubCat = "";
	var docAct = "";

	var docCat =$j('#doccat').val();
	var userId=$j('#docusrid').val();
	var docSubCat=$j('#docsubcat').val();
	var docAct=$j('#docact').val();
	
	pStartDateD1=$j('#dateStart14').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd14').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#showsRowsearchResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
			dontflag=1;
			$j('#dateDiv14').show();
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (docCat != "-1")
		cbuInfoParam = '&doccat='+docCat;
	if (userId != "-1")
		cbuInfoParam = cbuInfoParam + '&docusrid='+userId;
	if (docSubCat != "-1")
		cbuInfoParam = cbuInfoParam +  '&docsubcat='+docSubCat;
	if (docAct != "-1")
		cbuInfoParam = cbuInfoParam +  '&docact='+docAct;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		cbuInfoParam= cbuInfoParam + '&fromDocDt='+pStartDateD1+'&toDocDt='+pStartDateD2;
	}	
		$j('#docinfo_tbl_param').val(cbuInfoParam);
		$j('#docinfo_tbl_sentries').val(showEntries);

		if(cbuInfoParam != "") {
			$j('#doc_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=docaudit&iShowRows='+showEntries+cbuInfoParam,'docAuditDiv','trailForm');
			docHistoryOnload();
		}
		$j('#doccat').val(docCat);
		$j('#docusrid').val(userId);
		$j('#docsubcat').val(docSubCat);
		$j('#docact').val(docAct);
		$j('#dateStart14').val(pStartDtD1);
		$j('#dateEnd14').val(pStartDtD2);

		$j('#docCtg').val(docCat);
		$j('#docUsr').val(userId);
		$j('#docSubCtg').val(docSubCat);
		$j('#docActTkn').val(docAct);
}

function fn_FilterCbuInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var cbuInfoParam="";
	var showEntries="";
	var fieldName = "";
	var userId = "";
	var actTaken = "";
	var prevValue = "";
	var newValue = "";

	var fieldName=$j('#fieldName').val();
	var userId=$j('#userId').val();
	var actTaken=$j('#actTaken').val();
	var prevValue=$j('#prevValue').val();
	var newValue=$j('#newValue').val();
	
	pStartDateD1=$j('#dateStart').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#showsRowsearchResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
			$j('#dateDiv').show();
			dontflag=1;
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2"; 
			dontflag=1;  
	}	
	if (fieldName != "-1")
		cbuInfoParam = '&fieldName1='+fieldName;
	if (userId != "-1")
		cbuInfoParam = cbuInfoParam + '&userId1='+userId;
	if (actTaken != "-1")
		cbuInfoParam = cbuInfoParam +  '&actTaken1='+actTaken;
	if (prevValue != "-1")
		cbuInfoParam = cbuInfoParam +  '&prevValue1='+prevValue;
	if (newValue != "-1")
		cbuInfoParam = cbuInfoParam + '&newValue1='+newValue;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		cbuInfoParam= cbuInfoParam + '&fromDt1='+pStartDateD1+'&toDt1='+pStartDateD2;
	}	
		$j('#cbuinfo_tbl_param').val(cbuInfoParam);
		$j('#cbuinfo_tbl_sentries').val(showEntries);

		if(cbuInfoParam != "") {
			$j('#cbu_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=cordaudit&iShowRows='+showEntries+cbuInfoParam,'auditTrailHistoryDiv','trailForm');
			searchReasultOnload();
		}
		$j('#fieldName').val(fieldName);
		$j('#userId').val(userId);
		$j('#actTaken').val(actTaken);
		$j('#prevValue').val(prevValue);
		$j('#newValue').val(newValue);
		$j('#dateStart').val(pStartDtD1);
		$j('#dateEnd').val(pStartDtD2);

		$j('#fldNme').val(fieldName);
		$j('#usrId').val(userId);
		$j('#actTkn').val(actTaken);
		$j('#prvVl').val(prevValue);
		$j('#nwVl').val(newValue);
}
function fn_FilterIdInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var idInfoParam="";
	var showEntries="";
	var fieldName = "";
	var userId = "";
	var actTaken = "";
	var prevValue = "";
	var newValue = "";

	var fieldName=$j('#fieldName2').val();
	var userId=$j('#userId2').val();
	var actTaken=$j('#actTaken2').val();
	var prevValue=$j('#prevValue2').val();
	var newValue=$j('#newValue2').val();
	
	pStartDateD1=$j('#dateStart2').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd2').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#idResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
			$j('#dateDiv2').show();
			dontflag=1;
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (fieldName != "-1")
		idInfoParam = '&fieldName2='+fieldName;
	if (userId != "-1")
		idInfoParam = idInfoParam + '&userId2='+userId;
	if (actTaken != "-1")
		idInfoParam = idInfoParam +  '&actTaken2='+actTaken;
	if (prevValue != "-1")
		idInfoParam = idInfoParam +  '&prevValue2='+prevValue;
	if (newValue != "-1")
		idInfoParam = idInfoParam + '&newValue2='+newValue;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		idInfoParam= idInfoParam + '&fromDt2='+pStartDateD1+'&toDt2='+pStartDateD2;
	}	
		$j('#idinfo_tbl_param').val(idInfoParam);
		$j('#idinfo_tbl_sentries').val(showEntries);

		if(idInfoParam != "") {
			$j('#id_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=id&iShowRows='+showEntries+idInfoParam,'idDiv','trailForm');
			idOnload();
		}
		$j('#fieldName2').val(fieldName);
		$j('#userId2').val(userId);
		$j('#actTaken2').val(actTaken);
		$j('#prevValue2').val(prevValue);
		$j('#newValue2').val(newValue);
		$j('#dateStart2').val(pStartDtD1);
		$j('#dateEnd2').val(pStartDtD2);

		$j('#fldNme2').val(fieldName);
		$j('#usrId2').val(userId);
		$j('#actTkn2').val(actTaken);
		$j('#prvVl2').val(prevValue);
		$j('#nwVl2').val(newValue);
}

function fn_FilterProcInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var idInfoParam="";
	var showEntries="";
	var fieldName = "";
	var userId = "";
	var actTaken = "";
	var prevValue = "";
	var newValue = "";

	var fieldName=$j('#fieldName3').val();
	var userId=$j('#userId3').val();
	var actTaken=$j('#actTaken3').val();
	var prevValue=$j('#prevValue3').val();
	var newValue=$j('#newValue3').val();
	
	pStartDateD1=$j('#dateStart3').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd3').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#procInfoResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid())
			{
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
			}else{
			dontflag=1;
			$j('#dateDiv3').show();
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (fieldName != "-1")
		idInfoParam = '&fieldName3='+fieldName;
	if (userId != "-1")
		idInfoParam = idInfoParam + '&userId3='+userId;
	if (actTaken != "-1")
		idInfoParam = idInfoParam +  '&actTaken3='+actTaken;
	if (prevValue != "-1")
		idInfoParam = idInfoParam +  '&prevValue3='+prevValue;
	if (newValue != "-1")
		idInfoParam = idInfoParam + '&newValue3='+newValue;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		idInfoParam= idInfoParam + '&fromDt3='+pStartDateD1+'&toDt3='+pStartDateD2;
	}	
		$j('#procinfo_tbl_param').val(idInfoParam);
		$j('#procinfo_tbl_sentries').val(showEntries);

		if(idInfoParam != "") {
			$j('#proc_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=procInfo&iShowRows='+showEntries+idInfoParam,'procInfoDiv','trailForm');
			procInfoOnload();
		}
		$j('#fieldName3').val(fieldName);
		$j('#userId3').val(userId);
		$j('#actTaken3').val(actTaken);
		$j('#prevValue3').val(prevValue);
		$j('#newValue3').val(newValue);
		$j('#dateStart3').val(pStartDtD1);
		$j('#dateEnd3').val(pStartDtD2);

		$j('#fldNme3').val(fieldName);
		$j('#usrId3').val(userId);
		$j('#actTkn3').val(actTaken);
		$j('#prvVl3').val(prevValue);
		$j('#nwVl3').val(newValue);
}

function fn_FilterLicInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var idInfoParam="";
	var showEntries="";
	var fieldName = "";
	var userId = "";
	var actTaken = "";
	var prevValue = "";
	var newValue = "";

	var fieldName=$j('#fieldName4').val();
	var userId=$j('#userId4').val();
	var actTaken=$j('#actTaken4').val();
	var prevValue=$j('#prevValue4').val();
	var newValue=$j('#newValue4').val();
	
	pStartDateD1=$j('#dateStart4').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd4').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#showsEntitysearchResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
			dontflag=1;
			$j('#dateDiv4').show();
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (fieldName != "-1")
		idInfoParam = '&fieldName4='+fieldName;
	if (userId != "-1")
		idInfoParam = idInfoParam + '&userId4='+userId;
	if (actTaken != "-1")
		idInfoParam = idInfoParam +  '&actTaken4='+actTaken;
	if (prevValue != "-1")
		idInfoParam = idInfoParam +  '&prevValue4='+prevValue;
	if (newValue != "-1")
		idInfoParam = idInfoParam + '&newValue4='+newValue;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		idInfoParam= idInfoParam + '&fromDt4='+pStartDateD1+'&toDt4='+pStartDateD2;
	}	
		$j('#licinfo_tbl_param').val(idInfoParam);
		$j('#licinfo_tbl_sentries').val(showEntries);

		if(idInfoParam != "") {
			$j('#lic_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=entitystat&iShowRows='+showEntries+idInfoParam,'entityStatusDiv','trailForm');
			entityStatusOnload();
		}
		$j('#fieldName4').val(fieldName);
		$j('#userId4').val(userId);
		$j('#actTaken4').val(actTaken);
		$j('#prevValue4').val(prevValue);
		$j('#newValue4').val(newValue);
		$j('#dateStart4').val(pStartDtD1);
		$j('#dateEnd4').val(pStartDtD2);

		$j('#fldNme4').val(fieldName);
		$j('#usrId4').val(userId);
		$j('#actTkn4').val(actTaken);
		$j('#prvVl4').val(prevValue);
		$j('#nwVl4').val(newValue);
}

function fn_FilterEligInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var idInfoParam="";
	var showEntries="";
	var fieldName = "";
	var userId = "";
	var actTaken = "";
	var prevValue = "";
	var newValue = "";

	var fieldName=$j('#fieldName5').val();
	var userId=$j('#userId5').val();
	var actTaken=$j('#actTaken5').val();
	var prevValue=$j('#prevValue5').val();
	var newValue=$j('#newValue5').val();
	
	pStartDateD1=$j('#dateStart5').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd5').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#multiValuesResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
			dontflag=1;
			$j('#dateDiv5').show();
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (fieldName != "-1")
		idInfoParam = '&fieldName5='+fieldName;
	if (userId != "-1")
		idInfoParam = idInfoParam + '&userId5='+userId;
	if (actTaken != "-1")
		idInfoParam = idInfoParam +  '&actTaken5='+actTaken;
	if (prevValue != "-1")
		idInfoParam = idInfoParam +  '&prevValue5='+prevValue;
	if (newValue != "-1")
		idInfoParam = idInfoParam + '&newValue5='+newValue;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		idInfoParam= idInfoParam + '&fromDt5='+pStartDateD1+'&toDt5='+pStartDateD2;
	}	
		$j('#eliginfo_tbl_param').val(idInfoParam);
		$j('#eliginfo_tbl_sentries').val(showEntries);

		if(idInfoParam != "") {
			$j('#elig_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=multval&iShowRows='+showEntries+idInfoParam,'multiValuesDiv','trailForm');
			multValuesOnload();
		}
		$j('#fieldName5').val(fieldName);
		$j('#userId5').val(userId);
		$j('#actTaken5').val(actTaken);
		$j('#prevValue5').val(prevValue);
		$j('#newValue5').val(newValue);
		$j('#dateStart5').val(pStartDtD1);
		$j('#dateEnd5').val(pStartDtD2);

		$j('#fldNme5').val(fieldName);
		$j('#usrId5').val(userId);
		$j('#actTkn5').val(actTaken);
		$j('#prvVl5').val(prevValue);
		$j('#nwVl5').val(newValue);
}

function fn_FilterRevInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var idInfoParam="";
	var showEntries="";
	var fieldName = "";
	var userId = "";
	var actTaken = "";
	var prevValue = "";
	var newValue = "";

	var fieldName=$j('#fieldName6').val();
	var userId=$j('#userId6').val();
	var actTaken=$j('#actTaken6').val();
	var prevValue=$j('#prevValue6').val();
	var newValue=$j('#newValue6').val();
	
	pStartDateD1=$j('#dateStart6').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd6').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#reviewResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
			dontflag=1;
			$j('#dateDiv6').show();
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (fieldName != "-1")
		idInfoParam = '&fieldName6='+fieldName;
	if (userId != "-1")
		idInfoParam = idInfoParam + '&userId6='+userId;
	if (actTaken != "-1")
		idInfoParam = idInfoParam +  '&actTaken6='+actTaken;
	if (prevValue != "-1")
		idInfoParam = idInfoParam +  '&prevValue6='+prevValue;
	if (newValue != "-1")
		idInfoParam = idInfoParam + '&newValue6='+newValue;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		idInfoParam= idInfoParam + '&fromDt6='+pStartDateD1+'&toDt6='+pStartDateD2;
	}	
		$j('#revinfo_tbl_param').val(idInfoParam);
		$j('#revinfo_tbl_sentries').val(showEntries);

		if(idInfoParam != "") {
			$j('#rev_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=review&iShowRows='+showEntries+idInfoParam,'reviewDiv','trailForm');
			reviewOnload();
		}
		$j('#fieldName6').val(fieldName);
		$j('#userId6').val(userId);
		$j('#actTaken6').val(actTaken);
		$j('#prevValue6').val(prevValue);
		$j('#newValue6').val(newValue);
		$j('#dateStart6').val(pStartDtD1);
		$j('#dateEnd6').val(pStartDtD2);

		$j('#fldNme6').val(fieldName);
		$j('#usrId6').val(userId);
		$j('#actTkn6').val(actTaken);
		$j('#prvVl6').val(prevValue);
		$j('#nwVl6').val(newValue);
}

function fn_FilterHlaInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var idInfoParam="";
	var showEntries="";
	var fieldName = "";
	var userId = "";
	var actTaken = "";
	var prevValue = "";
	var newValue = "";

	var fieldName=$j('#fieldName7').val();
	var userId=$j('#userId7').val();
	var actTaken=$j('#actTaken7').val();
	var prevValue=$j('#prevValue7').val();
	var newValue=$j('#newValue7').val();
	
	pStartDateD1=$j('#dateStart7').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd7').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#hlaResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
			dontflag=1;
			$j('#dateDiv7').show();
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (fieldName != "-1")
		idInfoParam = '&fieldName7='+fieldName;
	if (userId != "-1")
		idInfoParam = idInfoParam + '&userId7='+userId;
	if (actTaken != "-1")
		idInfoParam = idInfoParam +  '&actTaken7='+actTaken;
	if (prevValue != "-1")
		idInfoParam = idInfoParam +  '&prevValue7='+prevValue;
	if (newValue != "-1")
		idInfoParam = idInfoParam + '&newValue7='+newValue;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		idInfoParam= idInfoParam + '&fromDt7='+pStartDateD1+'&toDt7='+pStartDateD2;
	}	
		$j('#hlainfo_tbl_param').val(idInfoParam);
		$j('#hlainfo_tbl_sentries').val(showEntries);

		if(idInfoParam != "") {
			$j('#hla_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=hla&iShowRows='+showEntries+idInfoParam,'hlaDiv','trailForm');
			hlaOnload();
		}
		$j('#fieldName7').val(fieldName);
		$j('#userId7').val(userId);
		$j('#actTaken7').val(actTaken);
		$j('#prevValue7').val(prevValue);
		$j('#newValue7').val(newValue);
		$j('#dateStart7').val(pStartDtD1);
		$j('#dateEnd7').val(pStartDtD2);

		$j('#fldNme7').val(fieldName);
		$j('#usrId7').val(userId);
		$j('#actTkn7').val(actTaken);
		$j('#prvVl7').val(prevValue);
		$j('#nwVl7').val(newValue);
}

function fn_FilterPatHlaInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var idInfoParam="";
	var showEntries="";
	var fieldName = "";
	var userId = "";
	var actTaken = "";
	var prevValue = "";
	var newValue = "";

	var fieldName=$j('#fieldName12').val();
	var userId=$j('#userId12').val();
	var actTaken=$j('#actTaken12').val();
	var prevValue=$j('#prevValue12').val();
	var newValue=$j('#newValue12').val();
	
	pStartDateD1=$j('#dateStart15').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd15').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#pathlaResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
			dontflag=1;
			$j('#dateDiv15').show();
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (fieldName != "-1")
		idInfoParam = '&fieldName12='+fieldName;
	if (userId != "-1")
		idInfoParam = idInfoParam + '&userId12='+userId;
	if (actTaken != "-1")
		idInfoParam = idInfoParam +  '&actTaken12='+actTaken;
	if (prevValue != "-1")
		idInfoParam = idInfoParam +  '&prevValue12='+prevValue;
	if (newValue != "-1")
		idInfoParam = idInfoParam + '&newValue12='+newValue;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		idInfoParam= idInfoParam + '&fromDt12='+pStartDateD1+'&toDt12='+pStartDateD2;
	}	
		$j('#pathlainfo_tbl_param').val(idInfoParam);
		$j('#pathlainfo_tbl_sentries').val(showEntries);

		if(idInfoParam != "") {
			$j('#pathla_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=pathla&iShowRows='+showEntries+idInfoParam,'pathlaDiv','trailForm');
			pathlaOnload();
		}
		$j('#fieldName12').val(fieldName);
		$j('#userId12').val(userId);
		$j('#actTaken12').val(actTaken);
		$j('#prevValue12').val(prevValue);
		$j('#newValue12').val(newValue);
		$j('#dateStart15').val(pStartDtD1);
		$j('#dateEnd15').val(pStartDtD2);

		$j('#fldNme12').val(fieldName);
		$j('#usrId12').val(userId);
		$j('#actTkn12').val(actTaken);
		$j('#prvVl12').val(prevValue);
		$j('#nwVl12').val(newValue);
}




function fn_FilterLabInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var idInfoParam="";
	var showEntries="";
	var fieldName = "";
	var userId = "";
	var actTaken = "";
	var prevValue = "";
	var newValue = "";

	var fieldName=$j('#fieldName8').val();
	var userId=$j('#userId8').val();
	var actTaken=$j('#actTaken8').val();
	var prevValue=$j('#prevValue8').val();
	var newValue=$j('#newValue8').val();
	
	pStartDateD1=$j('#dateStart8').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd8').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#reviewResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
			dontflag=1;
			$j('#dateDiv8').show();
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (fieldName != "-1")
		idInfoParam = '&fieldName8='+fieldName;
	if (userId != "-1")
		idInfoParam = idInfoParam + '&userId8='+userId;
	if (actTaken != "-1")
		idInfoParam = idInfoParam +  '&actTaken8='+actTaken;
	if (prevValue != "-1")
		idInfoParam = idInfoParam +  '&prevValue8='+prevValue;
	if (newValue != "-1")
		idInfoParam = idInfoParam + '&newValue8='+newValue;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		idInfoParam= idInfoParam + '&fromDt8='+pStartDateD1+'&toDt8='+pStartDateD2;
	}	
		$j('#labinfo_tbl_param').val(idInfoParam);
		$j('#labinfo_tbl_sentries').val(showEntries);

		if(idInfoParam != "") {
			$j('#lab_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=labsummary&iShowRows='+showEntries+idInfoParam,'labDiv','trailForm');
			labOnload();
		}
		$j('#fieldName8').val(fieldName);
		$j('#userId8').val(userId);
		$j('#actTaken8').val(actTaken);
		$j('#prevValue8').val(prevValue);
		$j('#newValue8').val(newValue);
		$j('#dateStart8').val(pStartDtD1);
		$j('#dateEnd8').val(pStartDtD2);

		$j('#fldNme8').val(fieldName);
		$j('#usrId8').val(userId);
		$j('#actTkn8').val(actTaken);
		$j('#prvVl8').val(prevValue);
		$j('#nwVl8').val(newValue);
}

function fn_FilterCnInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var idInfoParam="";
	var showEntries="";
	var fieldName = "";
	var userId = "";
	var actTaken = "";
	var prevValue = "";
	var newValue = "";

	var fieldName=$j('#fieldName9').val();
	var userId=$j('#userId9').val();
	var actTaken=$j('#actTaken9').val();
	var prevValue=$j('#prevValue9').val();
	var newValue=$j('#newValue9').val();
	
	pStartDateD1=$j('#dateStart9').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd9').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#notesResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
			dontflag=1;
			$j('#dateDiv9').show();
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (fieldName != "-1")
		idInfoParam = '&fieldName9='+fieldName;
	if (userId != "-1")
		idInfoParam = idInfoParam + '&userId9='+userId;
	if (actTaken != "-1")
		idInfoParam = idInfoParam +  '&actTaken9='+actTaken;
	if (prevValue != "-1")
		idInfoParam = idInfoParam +  '&prevValue9='+prevValue;
	if (newValue != "-1")
		idInfoParam = idInfoParam + '&newValue9='+newValue;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		idInfoParam= idInfoParam + '&fromDt9='+pStartDateD1+'&toDt9='+pStartDateD2;
	}	
		$j('#cninfo_tbl_param').val(idInfoParam);
		$j('#cninfo_tbl_sentries').val(showEntries);

		if(idInfoParam != "") {
			$j('#cn_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=cbnotes&iShowRows='+showEntries+idInfoParam,'notesDiv','trailForm');
			notesOnload();
		}
		$j('#fieldName9').val(fieldName);
		$j('#userId9').val(userId);
		$j('#actTaken9').val(actTaken);
		$j('#prevValue9').val(prevValue);
		$j('#newValue9').val(newValue);
		$j('#dateStart9').val(pStartDtD1);
		$j('#dateEnd9').val(pStartDtD2);

		$j('#fldNme9').val(fieldName);
		$j('#usrId9').val(userId);
		$j('#actTkn9').val(actTaken);
		$j('#prvVl9').val(prevValue);
		$j('#nwVl9').val(newValue);
}

function fn_FilterMcInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var idInfoParam="";
	var showEntries="";
	var fieldName = "";
	var userId = "";
	var actTaken = "";
	var prevValue = "";
	var newValue = "";

	var fieldName=$j('#fieldName10').val();
	var userId=$j('#userId10').val();
	var actTaken=$j('#actTaken10').val();
	var prevValue=$j('#prevValue10').val();
	var newValue=$j('#newValue10').val();
	
	pStartDateD1=$j('#dateStart10').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd10').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#mcResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
				dontflag=1;
				$j('#dateDiv10').show();
				}
		}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (fieldName != "-1")
		idInfoParam = '&fieldName10='+fieldName;
	if (userId != "-1")
		idInfoParam = idInfoParam + '&userId10='+userId;
	if (actTaken != "-1")
		idInfoParam = idInfoParam +  '&actTaken10='+actTaken;
	if (prevValue != "-1")
		idInfoParam = idInfoParam +  '&prevValue10='+prevValue;
	if (newValue != "-1")
		idInfoParam = idInfoParam + '&newValue10='+newValue;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		idInfoParam= idInfoParam + '&fromDt10='+pStartDateD1+'&toDt10='+pStartDateD2;
	}	
		$j('#mcinfo_tbl_param').val(idInfoParam);
		$j('#mcinfo_tbl_sentries').val(showEntries);

		if(idInfoParam != "") {
			$j('#mc_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=mc&iShowRows='+showEntries+idInfoParam,'mcDiv','trailForm');
			mcOnload();
		}
		$j('#fieldName10').val(fieldName);
		$j('#userId10').val(userId);
		$j('#actTaken10').val(actTaken);
		$j('#prevValue10').val(prevValue);
		$j('#newValue10').val(newValue);
		$j('#dateStart10').val(pStartDtD1);
		$j('#dateEnd10').val(pStartDtD2);

		$j('#fldNme10').val(fieldName);
		$j('#usrId10').val(userId);
		$j('#actTkn10').val(actTaken);
		$j('#prvVl10').val(prevValue);
		$j('#nwVl10').val(newValue);
}

function fn_FilterOrInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var idInfoParam="";
	var showEntries="";
	var fieldName = "";
	var userId = "";
	var actTaken = "";
	var prevValue = "";
	var newValue = "";

	var fieldName=$j('#fieldName11').val();
	var userId=$j('#userId11').val();
	var actTaken=$j('#actTaken11').val();
	var prevValue=$j('#prevValue11').val();
	var newValue=$j('#newValue11').val();
	
	pStartDateD1=$j('#dateStart11').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd11').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#pfResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
			dontflag=1;
			$j('#dateDiv11').show();
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (fieldName != "-1")
		idInfoParam = '&fieldName11='+fieldName;
	if (userId != "-1")
		idInfoParam = idInfoParam + '&userId11='+userId;
	if (actTaken != "-1")
		idInfoParam = idInfoParam +  '&actTaken11='+actTaken;
	if (prevValue != "-1")
		idInfoParam = idInfoParam +  '&prevValue11='+prevValue;
	if (newValue != "-1")
		idInfoParam = idInfoParam + '&newValue11='+newValue;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		idInfoParam= idInfoParam + '&fromDt11='+pStartDateD1+'&toDt11='+pStartDateD2;
	}	
		$j('#orinfo_tbl_param').val(idInfoParam);
		$j('#orinfo_tbl_sentries').val(showEntries);

		if(idInfoParam != "") {
			$j('#or_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=pf&iShowRows='+showEntries+idInfoParam,'pfDiv','trailForm');
			pfOnload();
		}
		$j('#fieldName11').val(fieldName);
		$j('#userId11').val(userId);
		$j('#actTaken11').val(actTaken);
		$j('#prevValue11').val(prevValue);
		$j('#newValue11').val(newValue);
		$j('#dateStart11').val(pStartDtD1);
		$j('#dateEnd11').val(pStartDtD2);

		$j('#fldNme11').val(fieldName);
		$j('#usrId11').val(userId);
		$j('#actTkn11').val(actTaken);
		$j('#prvVl11').val(prevValue);
		$j('#nwVl11').val(newValue);
}

function fn_FilterIdmInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var idInfoParam="";
	var showEntries="";

	var formname=$j('#formname1').val();
	var fversion = $j('#fversion1').val();
	var userid=$j('#fuserid1').val();
	
	pStartDateD1=$j('#dateStart12').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd12').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#formsResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
			dontflag=1;
			$j('#dateDiv12').show();
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (formname != "-1")
		idInfoParam = '&formname1='+formname;
	if (fversion != "-1")
		idInfoParam = idInfoParam + '&fversion1='+fversion;
	if (userid != "-1")
		idInfoParam = idInfoParam +  '&fuserid1='+userid;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		idInfoParam= idInfoParam + '&fromDt12='+pStartDateD1+'&toDt12='+pStartDateD2;
	}	
		$j('#idminfo_tbl_param').val(idInfoParam);
		$j('#idminfo_tbl_sentries').val(showEntries);

		if(idInfoParam != "") {
			$j('#idm_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=forms&iShowRows='+showEntries+idInfoParam,'formsDiv','trailForm');
			formsOnload();
		}
		$j('#formname1').val(formname);
		$j('#fuserid1').val(userid);
		$j('#fversion1').val(fversion);
		$j('#dateStart12').val(pStartDtD1);
		$j('#dateEnd12').val(pStartDtD2);

		$j('#frmnme1').val(formname);
		$j('#usid1').val(userid);
		$j('#fversn1').val(fversion);
}

function fn_FilterHhsInfo(){
	var pStartDateD1="";
	var pStartDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var idInfoParam="";
	var showEntries="";

	var formname=$j('#formname2').val();
	var fversion = $j('#fversion2').val();
	var userid=$j('#fuserid2').val();
	
	pStartDateD1=$j('#dateStart13').val();
	pStartDtD1 = pStartDateD1;
	pStartDateD2=$j('#dateEnd13').val();
	pStartDtD2 = pStartDateD2;
	showEntries=$j('#formshhsResults').val();
	if(pStartDateD1!="" && pStartDateD2!=""){
		if($j('#audit_dummyForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}else{
			dontflag=1;
			$j('#dateDiv13').show();
			}
	}else if( ($j.trim(pStartDateD1)!="" && $j.trim(pStartDateD2)=="" ) || ($j.trim(pStartDateD1)=="" && $j.trim(pStartDateD2)!="")){
			alert("Please enter correct Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";   
	}	
	if (formname != "-1")
		idInfoParam = '&formname2='+formname;
	if (fversion != "-1")
		idInfoParam = idInfoParam + '&fversion2='+fversion;
	if (userid != "-1")
		idInfoParam = idInfoParam +  '&fuserid2='+userid;

	if((DateFlag1=="1" ) && (DateFlag1!="2")){
		idInfoParam= idInfoParam + '&fromDt13='+pStartDateD1+'&toDt13='+pStartDateD2;
	}	
		$j('#hhsinfo_tbl_param').val(idInfoParam);
		$j('#hhsinfo_tbl_sentries').val(showEntries);

		if(idInfoParam != "") {
			$j('#hhs_info_filter_flag').val("1");
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=formshhs&iShowRows='+showEntries+idInfoParam,'formshhsDiv','trailForm');
			formshhsOnload();
		}
		$j('#formname2').val(formname);
		$j('#fuserid2').val(userid);
		$j('#fversion2').val(fversion);
		$j('#dateStart13').val(pStartDtD1);
		$j('#dateEnd13').val(pStartDtD2);

		$j('#frmnme2').val(formname);
		$j('#usid2').val(userid);
		$j('#fversn2').val(fversion);
}


function fnResetFilterDocInfo(){
	var flagVal=$j('#doc_info_filter_flag').val();
	$j('#doc_info_filter_flag').val("0");
	$j('#docinfo_tbl_param').val("");
	$j('#doccat').val("-1");
	$j('#docsubcat').val("-1");
	$j('#docusrid').val("-1");
	$j('#docact').val("-1");
	$j('#dateStart14').val('');
	$j('#dateEnd14').val('');
	$j('#docCtg').val('');
	$j('#docUsr').val('');
	$j('#docSubCtg').val('');
	$j('#docActTkn').val('');
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=docaudit','docAuditDiv','trailForm');
		docHistoryOnload();
	}
}

function fnResetFilterCbuInfo(){
		var flagVal=$j('#cbu_info_filter_flag').val();
		$j('#cbu_info_filter_flag').val("0");
		$j('#cbuinfo_tbl_param').val("");
		$j('#fieldName').val("-1");
		$j('#actTaken').val("-1");
		$j('#userId').val("-1");
		$j('#prevValue').val("-1");
		$j('#newValue').val("-1");
		$j('#dateStart').val('');
		$j('#dateEnd').val('');
		$j('#fldNme').val('');
		$j('#usrId').val('');
		$j('#actTkn').val('');
		$j('#prvVl').val('');
		$j('#nwVl').val('');
		resetValidForm('audit_dummyForm');
		if(flagVal=="1"){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=cordaudit','auditTrailHistoryDiv','trailForm');
			searchReasultOnload();
		}
}
function fnResetFilterIdInfo(){
	var flagVal=$j('#id_info_filter_flag').val();
	$j('#id_info_filter_flag').val("0");
	$j('#idinfo_tbl_param').val("");
	$j('#fieldName2').val("-1");
	$j('#actTaken2').val("-1");
	$j('#userId2').val("-1");
	$j('#prevValue2').val("-1");
	$j('#newValue2').val("-1");
	$j('#dateStart2').val('');
	$j('#dateEnd2').val('');
	$j('#fldNme2').val('');
	$j('#usrId2').val('');
	$j('#actTkn2').val('');
	$j('#prvVl2').val('');
	$j('#nwVl2').val('');
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=id','idDiv','trailForm');
		idOnload();
	}
}
function fnResetFilterProcInfo(){
	var flagVal=$j('#proc_info_filter_flag').val();
	$j('#proc_info_filter_flag').val("0");
	$j('#procinfo_tbl_param').val("");
	$j('#fieldName3').val("-1");
	$j('#actTaken3').val("-1");
	$j('#userId3').val("-1");
	$j('#prevValue3').val("-1");
	$j('#newValue3').val("-1");
	$j('#dateStart3').val("");
	$j('#dateEnd3').val("");
	$j('#fldNme3').val("");
	$j('#usrId3').val("");
	$j('#actTkn3').val("");
	$j('#prvVl3').val("");
	$j('#nwVl3').val("");
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=procInfo','procInfoDiv','trailForm');
		procInfoOnload();
	}
}

function fnResetFilterLicInfo(){
	var flagVal=$j('#lic_info_filter_flag').val();
	$j('#lic_info_filter_flag').val("0");
	$j('#licinfo_tbl_param').val("");
	$j('#fieldName4').val("-1");
	$j('#actTaken4').val("-1");
	$j('#userId4').val("-1");
	$j('#prevValue4').val("-1");
	$j('#newValue4').val("-1");
	$j('#dateStart4').val("");
	$j('#dateEnd4').val("");
	$j('#fldNme4').val("");
	$j('#usrId4').val("");
	$j('#actTkn4').val("");
	$j('#prvVl4').val("");
	$j('#nwVl4').val("");
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=entitystat','entityStatusDiv','trailForm');
		entityStatusOnload();
	}
}

function fnResetFilterEligInfo(){
	var flagVal=$j('#elig_info_filter_flag').val();
	$j('#elig_info_filter_flag').val("0");
	$j('#eliginfo_tbl_param').val("");
	$j('#fieldName5').val("-1");
	$j('#actTaken5').val("-1");
	$j('#userId5').val("-1");
	$j('#prevValue5').val("-1");
	$j('#newValue5').val("-1");
	$j('#dateStart5').val("");
	$j('#dateEnd5').val("");
	$j('#fldNme5').val("");
	$j('#usrId5').val("");
	$j('#actTkn5').val("");
	$j('#prvVl5').val("");
	$j('#nwVl5').val("");
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=multval','multiValuesDiv','trailForm');
		multValuesOnload();
	}
}

function fnResetFilterRevInfo(){
	var flagVal=$j('#rev_info_filter_flag').val();
	$j('#rev_info_filter_flag').val("0");
	$j('#revinfo_tbl_param').val("");
	$j('#fieldName6').val("-1");
	$j('#actTaken6').val("-1");
	$j('#userId6').val("-1");
	$j('#prevValue6').val("-1");
	$j('#newValue6').val("-1");
	$j('#dateStart6').val("");
	$j('#dateEnd6').val("");
	$j('#fldNme6').val("");
	$j('#usrId6').val("");
	$j('#actTkn6').val("");
	$j('#prvVl6').val("");
	$j('#nwVl6').val("");
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=review','reviewDiv','trailForm');
		reviewOnload();
	}
}
function fnResetFilterHlaInfo(){
	var flagVal=$j('#hla_info_filter_flag').val();
	$j('#hla_info_filter_flag').val("0");
	$j('#hlainfo_tbl_param').val("");
	$j('#fieldName7').val("-1");
	$j('#actTaken7').val("-1");
	$j('#userId7').val("-1");
	$j('#prevValue7').val("-1");
	$j('#newValue7').val("-1");
	$j('#dateStart7').val("");
	$j('#dateEnd7').val("");
	$j('#fldNme7').val("");
	$j('#usrId7').val("");
	$j('#actTkn7').val("");
	$j('#prvVl7').val("");
	$j('#nwVl7').val("");
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=hla','hlaDiv','trailForm');
		hlaOnload();
	}
}

function fnResetFilterPatHlaInfo(){
	var flagVal=$j('#pathla_info_filter_flag').val();
	$j('#pathla_info_filter_flag').val("0");
	$j('#pathlainfo_tbl_param').val("");
	$j('#fieldName12').val("-1");
	$j('#actTaken12').val("-1");
	$j('#userId12').val("-1");
	$j('#prevValue12').val("-1");
	$j('#newValue12').val("-1");
	$j('#dateStart12').val("");
	$j('#dateEnd12').val("");
	$j('#fldNme12').val("");
	$j('#usrId12').val("");
	$j('#actTkn12').val("");
	$j('#prvVl12').val("");
	$j('#nwVl12').val("");
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=pathla','pathlaDiv','trailForm');
		pathlaOnload();
	}
}

function fnResetFilterLabInfo(){
	var flagVal=$j('#lab_info_filter_flag').val();
	$j('#lab_info_filter_flag').val("0");
	$j('#labinfo_tbl_param').val("");
	$j('#fieldName8').val("-1");
	$j('#actTaken8').val("-1");
	$j('#userId8').val("-1");
	$j('#prevValue8').val("-1");
	$j('#newValue8').val("-1");
	$j('#dateStart8').val("");
	$j('#dateEnd8').val("");
	$j('#fldNme8').val("");
	$j('#usrId8').val("");
	$j('#actTkn8').val("");
	$j('#prvVl8').val("");
	$j('#nwVl8').val("");
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=labsummary','labDiv','trailForm');
		labOnload();
	}
}

function fnResetFilterCnInfo(){
	var flagVal=$j('#cn_info_filter_flag').val();
	$j('#cn_info_filter_flag').val("0");
	$j('#cninfo_tbl_param').val("");
	$j('#fieldName9').val("-1");
	$j('#actTaken9').val("-1");
	$j('#userId9').val("-1");
	$j('#prevValue9').val("-1");
	$j('#newValue9').val("-1");
	$j('#dateStart9').val("");
	$j('#dateEnd9').val("");
	$j('#fldNme9').val("");
	$j('#usrId9').val("");
	$j('#actTkn9').val("");
	$j('#prvVl9').val("");
	$j('#nwVl9').val("");
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=cbnotes','notesDiv','trailForm');
		notesOnload();
	}
}

function fnResetFilterMcInfo(){
	var flagVal=$j('#mc_info_filter_flag').val();
	$j('#mc_info_filter_flag').val("0");
	$j('#mcinfo_tbl_param').val("");
	$j('#fieldName10').val("-1");
	$j('#actTaken10').val("-1");
	$j('#userId10').val("-1");
	$j('#prevValue10').val("-1");
	$j('#newValue10').val("-1");
	$j('#dateStart10').val("");
	$j('#dateEnd10').val("");
	$j('#fldNme10').val("");
	$j('#usrId10').val("");
	$j('#actTkn10').val("");
	$j('#prvVl10').val("");
	$j('#nwVl10').val("");
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=mc','mcDiv','trailForm');
		mcOnload();
	}
}

function fnResetFilterOrInfo(){
	var flagVal=$j('#or_info_filter_flag').val();
	$j('#or_info_filter_flag').val("0");
	$j('#orinfo_tbl_param').val("");
	$j('#fieldName11').val("-1");
	$j('#actTaken11').val("-1");
	$j('#userId11').val("-1");
	$j('#prevValue11').val("-1");
	$j('#newValue11').val("-1");
	$j('#dateStart11').val("");
	$j('#dateEnd11').val("");
	$j('#fldNme11').val("");
	$j('#usrId11').val("");
	$j('#actTkn11').val("");
	$j('#prvVl11').val("");
	$j('#nwVl11').val("");
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=pf','pfDiv','trailForm');
		pfOnload();
	}
}

function fnResetFilterIdmInfo(){
	var flagVal=$j('#idm_info_filter_flag').val();
	$j('#idm_info_filter_flag').val("0");
	$j('#idminfo_tbl_param').val("");
	$j('#formname1').val("-1");
	$j('#fuserid1').val("-1");
	$j('#fversion1').val("-1");
	$j('#frmnme1').val("");
	$j('#fversn1').val("");
	$j('#usid1').val("");
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=forms','formsDiv','trailForm');
		formsOnload();
	}
}

function fnResetFilterHhsInfo(){
	var flagVal=$j('#hhs_info_filter_flag').val();
	$j('#hhs_info_filter_flag').val("0");
	$j('#hhsinfo_tbl_param').val("");
	$j('#formname2').val("-1");
	$j('#fuserid2').val("-1");
	$j('#fversion2').val("-1");
	$j('#frmnme2').val("");
	$j('#fversn2').val("");
	$j('#usid2').val("");
	resetValidForm('audit_dummyForm');
	if(flagVal=="1"){
		refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=formshhs','formshhsDiv','trailForm');
		formshhsOnload();
	}
}

function fnAuditTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#cbuinfo_tbl_sort_col').val(i);
			$j('#cbuinfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#cbuinfo_tbl_sort_col').val(i);
			$j('#cbuinfo_tbl_sort_type').val('desc');
		}
   });
}

function fnIdInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#idinfo_tbl_sort_col').val(i);
			$j('#idinfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#idinfo_tbl_sort_col').val(i);
			$j('#idinfo_tbl_sort_type').val('desc');
		}
   });
}

function fnProcInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#procinfo_tbl_sort_col').val(i);
			$j('#procinfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#procinfo_tbl_sort_col').val(i);
			$j('#procinfo_tbl_sort_type').val('desc');
		}
   });
}

function fnLicInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#licinfo_tbl_sort_col').val(i);
			$j('#licinfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#licinfo_tbl_sort_col').val(i);
			$j('#licinfo_tbl_sort_type').val('desc');
		}
   });
}

function fnEligInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#eliginfo_tbl_sort_col').val(i);
			$j('#eliginfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#eliginfo_tbl_sort_col').val(i);
			$j('#eliginfo_tbl_sort_type').val('desc');
		}
   });
}

function fnRevInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#revinfo_tbl_sort_col').val(i);
			$j('#revinfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#revinfo_tbl_sort_col').val(i);
			$j('#revinfo_tbl_sort_type').val('desc');
		}
   });
}

function fnHlaInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#hlainfo_tbl_sort_col').val(i);
			$j('#hlainfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#hlainfo_tbl_sort_col').val(i);
			$j('#hlainfo_tbl_sort_type').val('desc');
		}
   });
}

function fnPatHlaInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#pathlainfo_tbl_sort_col').val(i);
			$j('#pathlainfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#pathlainfo_tbl_sort_col').val(i);
			$j('#pathlainfo_tbl_sort_type').val('desc');
		}
   });
}


function fnLabInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#labinfo_tbl_sort_col').val(i);
			$j('#labinfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#labinfo_tbl_sort_col').val(i);
			$j('#labinfo_tbl_sort_type').val('desc');
		}
   });
}

function fnCnInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#cninfo_tbl_sort_col').val(i);
			$j('#cninfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#cninfo_tbl_sort_col').val(i);
			$j('#cninfo_tbl_sort_type').val('desc');
		}
   });
}

function fnMcInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#mcinfo_tbl_sort_col').val(i);
			$j('#mcinfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#mcinfo_tbl_sort_col').val(i);
			$j('#mcinfo_tbl_sort_type').val('desc');
		}
   });
}

function fnOrInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#orinfo_tbl_sort_col').val(i);
			$j('#orinfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#orinfo_tbl_sort_col').val(i);
			$j('#orinfo_tbl_sort_type').val('desc');
		}
   });
}

function fnIdmInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#idminfo_tbl_sort_col').val(i);
			$j('#idminfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#idminfo_tbl_sort_col').val(i);
			$j('#idminfo_tbl_sort_type').val('desc');
		}
   });
}

function fnHhsInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#hhsinfo_tbl_sort_col').val(i);
			$j('#hhsinfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#hhsinfo_tbl_sort_col').val(i);
			$j('#hhsinfo_tbl_sort_type').val('desc');
		}
   });
}

function fnDocInfoTableSort(trTab){
	$j('#'+trTab).find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#docinfo_tbl_sort_col').val(i);
			$j('#docinfo_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#docinfo_tbl_sort_col').val(i);
			$j('#docinfo_tbl_sort_type').val('desc');
		}
   });
}

function showDateDiv(el,id,id1,id2){
	var t=$j(el);
	var obj=t.offset();
	var xOffset=180;
	var yOffset=30;
	var val1="";
	var val2="";
	resetValidForm('audit_dummyForm');
	if(id=='dateDiv'){
		val1=$j('#dateStart').val();
		val2=$j('#dateEnd').val();
	}

	if(id=='dateDiv2'){
		val1=$j('#dateStart2').val();
		val2=$j('#dateEnd2').val();
	}

	if(id=='dateDiv3'){
		val1=$j('#dateStart3').val();
		val2=$j('#dateEnd3').val();
	}


	if(id=='dateDiv4'){
		val1=$j('#dateStart4').val();
		val2=$j('#dateEnd4').val();
	}
	if(id=='dateDiv5'){
		val1=$j('#dateStart5').val();
		val2=$j('#dateEnd5').val();
	}

	if(id=='dateDiv6'){
		val1=$j('#dateStart6').val();
		val2=$j('#dateEnd6').val();
	}

	if(id=='dateDiv7'){
		val1=$j('#dateStart7').val();
		val2=$j('#dateEnd7').val();
	}

	if(id=='dateDiv8'){
		val1=$j('#dateStart8').val();
		val2=$j('#dateEnd8').val();
	}
	if(id=='dateDiv9'){
		val1=$j('#dateStart9').val();
		val2=$j('#dateEnd9').val();
	}
	if(id=='dateDiv10'){
		val1=$j('#dateStart10').val();
		val2=$j('#dateEnd10').val();
	}
	if(id=='dateDiv11'){
		val1=$j('#dateStart11').val();
		val2=$j('#dateEnd11').val();
	}

	if(id=='dateDiv12'){
		val1=$j('#dateStart12').val();
		val2=$j('#dateEnd12').val();
	}

	if(id=='dateDiv13'){
		val1=$j('#dateStart13').val();
		val2=$j('#dateEnd13').val();
	}

	if(id=='dateDiv14'){
		val1=$j('#dateStart14').val();
		val2=$j('#dateEnd14').val();
	}
	
	
	$j('#'+id1).val(val1);
	$j('#'+id2).val(val2);
	$j("#"+id)
			.css({left:obj.left-xOffset,top:obj.top+yOffset})
			.fadeIn("fast").show()
			.addClass('dateDivClass')
	$j("#"+id).show();
	if(val1==null || val1==""){
		$j('#'+id1).focus();
	}
}


function customRange1(input) {
	if (input.id == 'dateEnd') {
	    if($j("#dateStart").val()!="" && $j("#dateStart").val()!=null){
	    var minTestDate=new Date($j("#dateStart").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart') {
		  if($j("#dateEnd").val()!="" && $j("#dateEnd").val()!=null){
		  var minTestDate=new Date($j("#dateEnd").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange2(input) {
	if (input.id == 'dateEnd2') {
	    if($j("#dateStart2").val()!="" && $j("#dateStart2").val()!=null){
	    var minTestDate=new Date($j("#dateStart2").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd2').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd2').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart2') {
		  if($j("#dateEnd2").val()!="" && $j("#dateEnd2").val()!=null){
		  var minTestDate=new Date($j("#dateEnd2").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart2').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart2').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange3(input) {
	if (input.id == 'dateEnd3') {
	    if($j("#dateStart3").val()!="" && $j("#dateStart3").val()!=null){
	    var minTestDate=new Date($j("#dateStart3").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd3').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd3').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart3') {
		  if($j("#dateEnd3").val()!="" && $j("#dateEnd3").val()!=null){
		  var minTestDate=new Date($j("#dateEnd3").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart3').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart3').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange4(input) {
	if (input.id == 'dateEnd4') {
	    if($j("#dateStart4").val()!="" && $j("#dateStart4").val()!=null){
	    var minTestDate=new Date($j("#dateStart4").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd4').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd4').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart4') {
		  if($j("#dateEnd4").val()!="" && $j("#dateEnd4").val()!=null){
		  var minTestDate=new Date($j("#dateEnd4").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart4').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart4').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange5(input) {
	if (input.id == 'dateEnd5') {
	    if($j("#dateStart5").val()!="" && $j("#dateStart5").val()!=null){
	    var minTestDate=new Date($j("#dateStart5").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd5').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd5').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart5') {
		  if($j("#dateEnd5").val()!="" && $j("#dateEnd5").val()!=null){
		  var minTestDate=new Date($j("#dateEnd5").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart5').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart5').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange6(input) {
	if (input.id == 'dateEnd6') {
	    if($j("#dateStart6").val()!="" && $j("#dateStart6").val()!=null){
	    var minTestDate=new Date($j("#dateStart6").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd6').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd6').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart6') {
		  if($j("#dateEnd6").val()!="" && $j("#dateEnd6").val()!=null){
		  var minTestDate=new Date($j("#dateEnd6").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart6').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart6').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange7(input) {
	if (input.id == 'dateEnd7') {
	    if($j("#dateStart7").val()!="" && $j("#dateStart7").val()!=null){
	    var minTestDate=new Date($j("#dateStart7").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd7').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd7').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart7') {
		  if($j("#dateEnd7").val()!="" && $j("#dateEnd7").val()!=null){
		  var minTestDate=new Date($j("#dateEnd7").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart7').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart7').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange8(input) {
	if (input.id == 'dateEnd8') {
	    if($j("#dateStart8").val()!="" && $j("#dateStart8").val()!=null){
	    var minTestDate=new Date($j("#dateStart8").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd8').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd8').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart8') {
		  if($j("#dateEnd8").val()!="" && $j("#dateEnd8").val()!=null){
		  var minTestDate=new Date($j("#dateEnd8").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart8').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart8').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange9(input) {
	if (input.id == 'dateEnd9') {
	    if($j("#dateStart9").val()!="" && $j("#dateStart9").val()!=null){
	    var minTestDate=new Date($j("#dateStart9").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd9').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd9').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart9') {
		  if($j("#dateEnd9").val()!="" && $j("#dateEnd9").val()!=null){
		  var minTestDate=new Date($j("#dateEnd9").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart9').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart9').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange10(input) {
	if (input.id == 'dateEnd10') {
	    if($j("#dateStart10").val()!="" && $j("#dateStart10").val()!=null){
	    var minTestDate=new Date($j("#dateStart10").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd10').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd10').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart10') {
		  if($j("#dateEnd10").val()!="" && $j("#dateEnd10").val()!=null){
		  var minTestDate=new Date($j("#dateEnd10").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart10').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart10').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange11(input) {
	if (input.id == 'dateEnd11') {
	    if($j("#dateStart11").val()!="" && $j("#dateStart11").val()!=null){
	    var minTestDate=new Date($j("#dateStart11").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd11').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd11').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart11') {
		  if($j("#dateEnd11").val()!="" && $j("#dateEnd11").val()!=null){
		  var minTestDate=new Date($j("#dateEnd11").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart11').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart11').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange12(input) {
	if (input.id == 'dateEnd12') {
	    if($j("#dateStart12").val()!="" && $j("#dateStart12").val()!=null){
	    var minTestDate=new Date($j("#dateStart12").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd12').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd12').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart12') {
		  if($j("#dateEnd12").val()!="" && $j("#dateEnd12").val()!=null){
		  var minTestDate=new Date($j("#dateEnd12").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart12').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart12').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange13(input) {
	if (input.id == 'dateEnd13') {
	    if($j("#dateStart13").val()!="" && $j("#dateStart13").val()!=null){
	    var minTestDate=new Date($j("#dateStart13").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd13').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd13').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart13') {
		  if($j("#dateEnd13").val()!="" && $j("#dateEnd13").val()!=null){
		  var minTestDate=new Date($j("#dateEnd13").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart13').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart13').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange14(input) {
	if (input.id == 'dateEnd14') {
	    if($j("#dateStart14").val()!="" && $j("#dateStart14").val()!=null){
	    var minTestDate=new Date($j("#dateStart14").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd14').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd14').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart14') {
		  if($j("#dateEnd14").val()!="" && $j("#dateEnd14").val()!=null){
		  var minTestDate=new Date($j("#dateEnd14").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart14').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart14').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}

function customRange15(input) {
	if (input.id == 'dateEnd15') {
	    if($j("#dateStart15").val()!="" && $j("#dateStart15").val()!=null){
	    var minTestDate=new Date($j("#dateStart15").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd15').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd15').datepicker( "option", "minDate", "");
		   }
	  } else if (input.id == 'dateStart15') {
		  if($j("#dateEnd15").val()!="" && $j("#dateEnd15").val()!=null){
		  var minTestDate=new Date($j("#dateEnd15").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  $j('#dateStart15').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart15').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
}


function entityStatusRsnOnload(){
	$j('#widgetPrintDiv').empty();
	if($j('#entitystatrsnspan').is(".ui-icon-plusthick")) {
		if(flag3==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=statrsn','entityStatusRsnDiv','trailForm');
			flag3=1;
		}
	}
	if($j('#entitystatrsnspan').is(".ui-icon-minusthick")) {
		$j('#entityStatusRsnDiv').show();
	}

	var statRsnTbl = $j('#entitystatusrsntab').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
		},
		"bRetrieve": true,
		"bDestroy" :true,	
		"iDisplayLength":1000,"aaSorting": [[ 2, "desc" ]],
		"bSortCellsTop": true,
		"sDom": 'T<"clear">lfrtip',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Processing Information Audit Trail.csv"  
							}
						]
		}				
	});

	$j("#statRsnHead tr").each( function ( i ) {
		if(i!=0){
			$j(this).find('th').each ( function(j) {
				this.innerHTML = fnCreateSelectAudit( statRsnTbl.fnGetColumnData(j),j,'statrsnfilter');
				$j('select', this).change( function () {
					statRsnTbl.fnFilter( $j(this).val(), j );
				});
				
			});
		}
});
	$j('#entitystatusrsntab_info').hide();
	$j('#entitystatusrsntab_paginate').hide();
	$j('#entitystatusrsntab_length').empty();
	$j('#entitystatusrsntab_length').replaceWith('Show <select name="showsEntityRsnsearchResults" id="showsEntityRsnsearchResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="2000"/>">All</option></select> Entries');
	if($j('#entityrsnentries').val()!=null || $j('#entityrsnentries').val()!=""){
		$j('#showsEntityRsnsearchResults').val($j('#entityrsnentries').val());
		}
	$j('#showsEntityRsnsearchResults').change(function(){
		paginationFooter(0,"revokedUnitReportsWidget,showsEntityRsnsearchResults,temp,trailForm,entityStatusRsnDiv,entityStatusRsnOnload,cordID,entityId,statrsn");
	});	

	var wrapperWidth=$j('#entitystatrsnaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#entitystatusrsntab').width(newWidth);
}

function multValuesOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#multivalspan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('multivaluesparent','headerPrnt');
		},500);
		return false;
	}
	if($j('#multivalspan').is(".ui-icon-plusthick")) {
		if(flag4==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=multval','multiValuesDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag4=1;
		}
	}
	if($j('#multivalspan').is(".ui-icon-minusthick")) {
		$j('#multiValuesDiv').show();
	}

	var is_all_entries=$j('#multiValuesResults').val();
	var pageNo=$j('#paginateWrapper25').find('.currentPage').text();
	var showEntries=$j('#eliginfo_tbl_sentries').val();
	var tEntries=$j('#tentriesElig').val();
	var sort=2;
	var sortDir="desc";
	var SortParm=$j('#eliginfo_tbl_sort_col').val();
	var filter_value=$j('#elig_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#eliginfo_tbl_sort_col').val();
		sortDir=$j('#eliginfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#eliginfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#eliginfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="multiValuesResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';
	
	 if(showEntries=="ALL"){
	var multvalTbl = $j('#multivaluestab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=eligInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#eliginfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
    		                	return source[5];
		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
  		                	return source[6];
		                   }}
						],

						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#multivaluestab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#multivaluestab_info').hide();
						    $j("#multiValuesResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnEligInfoTableSort('eligInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Eligibility Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues5').html(temptxt);
	           return temptxt;
	         
	         }				
	});

	$j('#resetElig').click(function(){
		multvalTbl.fnSort( [ [2,'desc'] ] );
		multvalTbl.fnDraw();
		 });
	$j('#multivaluestab_info').after('<div id="entriesvalues5"><div>');
   	//var temptxt='Showing 1 to '+$j('#tentriesId').val()+' of '+$j('#tentriesElig').val()+' entries';
   	//$j('#entriesvalues5').html(temptxt);	

	 }else if(isPrint!="1" && isPrint!="2"){
		 var multvalTbl = $j('#multivaluestab').dataTable({
			 "sScrollY": "150",
	            "sScrollX": "100%",
	            "sAjaxSource": 'getJsonPagination.action?TblFind=eligInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#eliginfo_tbl_param').val(),
	            "bServerSide": true,
	            "bProcessing": true, 
	     		"bRetrieve" : true,
	     		"oSearch": {"sSearch": filter_value},
				"bDestroy": true,
				"bSortCellsTop": true,
	     		"aaSorting": [[ sort, sortDir ]],
	     		"bDeferRender": true,
	     		"sAjaxDataProp": "aaData",
	     		"aoColumnDefs": [
	     		                {		
	  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
	  		                	   return source[1];
	  		                  }},
	  		                  {
	  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
								return source[2];
			                  }},
			                  {
			                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
	  		                	 	return source[3];
	  		                  }},
	  		                  {		
	  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
	  		                	 	return source[4];
	  	                	   }},
	  		                   { 	
	  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
	      		                	return source[5];
	  		                   }}      	 ,
			                   { 	
		                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
	    		                	return source[6];
			                   }}
	  						],
	  						"fnDrawCallback": function() { 
	  							fnEligInfoTableSort('eligInfoTr');
	  						},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
						"aButtons": [
										{
											"sExtends": "xls",
											"sButtonText": "Export",
												"sFileName": "Eligibility Audit Trail.csv"  
										}
									]
					}
		 });
		 multvalTbl.fnAdjustColumnSizing();
	    }else if(((isPrint=="1" || isPrint=="2") && $j('#multivalspan').is(".ui-icon-plusthick"))){
			 var multvalTbl = $j('#multivaluestab').dataTable({
				 
		            "sAjaxSource": 'getJsonPagination.action?TblFind=eligInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#eliginfo_tbl_param').val(),
		            "bServerSide": true,
		            "bProcessing": true, 
		     		"bRetrieve" : true,
		     		"oSearch": {"sSearch": filter_value},
					"bDestroy": true,
					"bSortCellsTop": true,
		     		"aaSorting": [[ sort, sortDir ]],
		     		"bDeferRender": true,
		     		"sAjaxDataProp": "aaData",
		     		"aoColumnDefs": [
		     		                {		
		  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		  		                	   return source[1];
		  		                  }},
		  		                  {
		  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
									return source[2];
				                  }},
				                  {
				                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		  		                	 	return source[3];
		  		                  }},
		  		                  {		
		  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		  		                	 	return source[4];
		  	                	   }},
		  		                   { 	
		  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
		      		                	return source[5];
		  		                   }}      	 ,
				                   { 	
			                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
		    		                	return source[6];
				                   }}
		  						],
		  						"fnDrawCallback": function() { 
		  							fnEligInfoTableSort('eligInfoTr');
		  						},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
							"aButtons": [
											{
												"sExtends": "xls",
												"sButtonText": "Export",
													"sFileName": "Eligibility Audit Trail.csv"  
											}
										]
						}
			 });
		    	
		    }
	 
	 $j('#multivaluestab_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#elig_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,multiValuesResults,temp,trailForm,multiValuesDiv,multValuesOnload,cordID,entityId,multval,eliginfo_tbl_param");
		  }
	});

	 var usedNames = {};
		$j("select[name='fieldName5'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='userId5'] > option").each(function () {
		    if(usedNames[this.text] ||  $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='actTaken5'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='prevValue5'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='newValue5'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		
		var	fieldName=$j('#fieldName5').val();
		var	userId=$j('#userId5').val();
		var	actTaken=$j('#actTaken5').val();
		var	prevValue=$j('#prevValue5').val();
		var	newValue=$j('#newValue5').val();
		var	pStartDateD1=$j('#dateStart5').val();
		var	pStartDateD2=$j('#dateEnd5').val();
		$j('#resetElig').click(function(){
			multvalTbl.fnSort( [ [2,'desc'] ] );
			multvalTbl.fnDraw();
			 });
			
	$j('#multivaluestab_info').hide();
	$j('#multivaluestab_paginate').hide();
	$j('#multivaluestab_length').empty();
	$j('#multivaluestab_length').replaceWith(showEntriesTxt);
	if($j('#multivaluesentries').val()!=null || $j('#multivaluesentries').val()!=""){
		$j('#multiValuesResults').val($j('#multivaluesentries').val());
		}
	$j('#multiValuesResults').change(function(){
		var textval=$j('#multiValuesResults :selected').html();
		$j('#eliginfo_tbl_sentries').val(textval);
		var fieldName=$j('#fieldName5').val();
		var userId=$j('#userId5').val();
		var actTaken=$j('#actTaken5').val();
		var prevValue=$j('#prevValue5').val();
		var newValue=$j('#newValue5').val();
		var pStartDateD1=$j('#dateStart5').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd5').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,multiValuesResults,temp,trailForm,multiValuesDiv,multValuesOnload,cordID,entityId,multval,eliginfo_tbl_param");
		$j('#fieldName5').val(fieldName);
		$j('#userId5').val(userId);
		$j('#actTaken5').val(actTaken);
		$j('#prevValue5').val(prevValue);
		$j('#newValue5').val(newValue);
		$j('#dateStart5').val(pStartDtD1);
		$j('#dateEnd5').val(pStartDtD2);
	});	

	var wrapperWidth=$j('#multvalaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#multivaluestab').width(newWidth);
	var fldNme = $j('#fldNme5').val();
	var usrId = $j('#usrId5').val();
	var actTkn = $j('#actTkn5').val();
	var prvVl = $j('#prvVl5').val();
	var nwVl = $j('#nwVl5').val();

	$j('#fieldName5').val(fldNme);
	$j('#userId5').val(usrId);
	$j('#actTaken5').val(actTkn);
	$j('#prevValue5').val(prvVl);
	$j('#newValue5').val(nwVl);
	if(((isPrint=="1" || isPrint=="2") && $j('#multivalspan').is(".ui-icon-plusthick"))){
		flag4=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('multivaluesparent','headerPrnt');
			},500);
		}
		
	}
}

function reviewOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#reviewspan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('reviewaudittrail','headerPrnt');
		},500);
		return false;
	}
	if($j('#reviewspan').is(".ui-icon-plusthick")) {
		if(flag5==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=review','reviewDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag5=1;
		}
	}
	if($j('#reviewspan').is(".ui-icon-minusthick")) {
		$j('#reviewDiv').show();
	}

	var is_all_entries=$j('#reviewResults').val();
	var pageNo=$j('#paginateWrapper26').find('.currentPage').text();
	var showEntries=$j('#revinfo_tbl_sentries').val();
	var tEntries=$j('#tentriesRev').val();
	var sort=2;
	var sortDir="desc";
	var SortParm=$j('#revinfo_tbl_sort_col').val();
	var filter_value=$j('#rev_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#revinfo_tbl_sort_col').val();
		sortDir=$j('#revinfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#revinfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#revinfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="reviewResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';
	
	 if(showEntries=="ALL"){
			
	var reviewTbl = $j('#reviewtab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=revInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#revinfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
    		                	return source[5];
		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
  		                	return source[6];
		                   }}
						],

						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#reviewtab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#reviewtab_info').hide();
						    $j("#reviewResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnRevInfoTableSort('revInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
		},
		"bRetrieve": true,
		"bDestroy" :true,	
		"iDisplayLength":1000,"aaSorting": [[ 2, "desc" ]],
		"bSortCellsTop": true,
		"sDom": 'T<"clear">lfrtip',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Final Review Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues6').html(temptxt);
	           return temptxt;
	         
	         }						
});
	$j('#reviewtab_info').after('<div id="entriesvalues6"><div>');
   	//var temptxt='Showing 1 to '+$j('#tentriesRev').val()+' of '+$j('#tentriesRev').val()+' entries';
   	//$j('#entriesvalues6').html(temptxt);	

	 }else if(isPrint!="1" && isPrint!="2"){	
		 var reviewTbl = $j('#reviewtab').dataTable({
			 "sScrollY": "150",
	            "sScrollX": "100%",
	            "sAjaxSource": 'getJsonPagination.action?TblFind=revInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#revinfo_tbl_param').val(),
	            "bServerSide": true,
	            "bProcessing": true, 
	     		"bRetrieve" : true,
	     		"oSearch": {"sSearch": filter_value},
				"bDestroy": true,
				"bSortCellsTop": true,
	     		"aaSorting": [[ sort, sortDir ]],
	     		"bDeferRender": true,
	     		"sAjaxDataProp": "aaData",
	     		"aoColumnDefs": [
	     		                {		
	  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
	  		                	   return source[1];
	  		                  }},
	  		                  {
	  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
								return source[2];
			                  }},
			                  {
			                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
	  		                	 	return source[3];
	  		                  }},
	  		                  {		
	  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
	  		                	 	return source[4];
	  	                	   }},
	  		                   { 	
	  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
	      		                	return source[5];
	  		                   }}      	 ,
			                   { 	
		                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
	    		                	return source[6];
			                   }}
	  						],
	  						"fnDrawCallback": function() { 
	  							fnIdInfoTableSort('idInfoTr');
	  						},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
						"aButtons": [
										{
											"sExtends": "xls",
											"sButtonText": "Export",
												"sFileName": "Final Review Audit Trail.csv"  
										}
									]
					}
		 });
		 reviewTbl.fnAdjustColumnSizing();
	    }else if(((isPrint=="1" || isPrint=="2") && $j('#reviewspan').is(".ui-icon-plusthick"))){	
			 var reviewTbl = $j('#reviewtab').dataTable({
				 
		            "sAjaxSource": 'getJsonPagination.action?TblFind=revInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#revinfo_tbl_param').val(),
		            "bServerSide": true,
		            "bProcessing": true, 
		     		"bRetrieve" : true,
		     		"oSearch": {"sSearch": filter_value},
					"bDestroy": true,
					"bSortCellsTop": true,
		     		"aaSorting": [[ sort, sortDir ]],
		     		"bDeferRender": true,
		     		"sAjaxDataProp": "aaData",
		     		"aoColumnDefs": [
		     		                {		
		  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		  		                	   return source[1];
		  		                  }},
		  		                  {
		  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
									return source[2];
				                  }},
				                  {
				                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		  		                	 	return source[3];
		  		                  }},
		  		                  {		
		  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		  		                	 	return source[4];
		  	                	   }},
		  		                   { 	
		  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
		      		                	return source[5];
		  		                   }}      	 ,
				                   { 	
			                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
		    		                	return source[6];
				                   }}
		  						],
		  						"fnDrawCallback": function() { 
		  							fnIdInfoTableSort('idInfoTr');
		  						},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
							"aButtons": [
											{
												"sExtends": "xls",
												"sButtonText": "Export",
													"sFileName": "Final Review Audit Trail.csv"  
											}
										]
						}
			 });
		    	
		    }
	 $j('#reviewtab_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#rev_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,reviewResults,temp,trailForm,reviewDiv,reviewOnload,cordID,entityId,review,revinfo_tbl_param");
		  }
	});
	
	 var usedNames = {};
		$j("select[name='fieldName6'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='userId6'] > option").each(function () {
		    if(usedNames[this.text] ||  $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='actTaken6'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='prevValue6'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='newValue6'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		
		var	fieldName=$j('#fieldName6').val();
		var	userId=$j('#userId6').val();
		var	actTaken=$j('#actTaken6').val();
		var	prevValue=$j('#prevValue6').val();
		var	newValue=$j('#newValue6').val();
		var	pStartDateD1=$j('#dateStart6').val();
		var	pStartDateD2=$j('#dateEnd6').val();

	$j('#resetReview').click(function(){
		reviewTbl.fnSort( [ [2,'desc'] ] );
		reviewTbl.fnDraw();
		 });
		
	$j('#reviewtab_info').hide();
	$j('#reviewtab_paginate').hide();
	$j('#reviewtab_length').empty();
	$j('#reviewtab_length').replaceWith(showEntriesTxt);
	if($j('#reviewentries').val()!=null || $j('#reviewentries').val()!=""){
		$j('#reviewResults').val($j('#reviewentries').val());
		}
	$j('#reviewResults').change(function(){
		var textval=$j('#reviewResults :selected').html();
		$j('#revinfo_tbl_sentries').val(textval);
		var fieldName=$j('#fieldName6').val();
		var userId=$j('#userId6').val();
		var actTaken=$j('#actTaken6').val();
		var prevValue=$j('#prevValue6').val();
		var newValue=$j('#newValue6').val();
		var pStartDateD1=$j('#dateStart6').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd6').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,reviewResults,temp,trailForm,reviewDiv,reviewOnload,cordID,entityId,review,revinfo_tbl_param");
		$j('#fieldName6').val(fieldName);
		$j('#userId6').val(userId);
		$j('#actTaken6').val(actTaken);
		$j('#prevValue6').val(prevValue);
		$j('#newValue6').val(newValue);
		$j('#dateStart6').val(pStartDtD1);
		$j('#dateEnd6').val(pStartDtD2);
		
	});
	var wrapperWidth=$j('#reviewaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#reviewtab').width(newWidth);

	var fldNme = $j('#fldNme6').val();
	var usrId = $j('#usrId6').val();
	var actTkn = $j('#actTkn6').val();
	var prvVl = $j('#prvVl6').val();
	var nwVl = $j('#nwVl6').val();

	$j('#fieldName6').val(fldNme);
	$j('#userId6').val(usrId);
	$j('#actTaken6').val(actTkn);
	$j('#prevValue6').val(prvVl);
	$j('#newValue6').val(nwVl);
	if(((isPrint=="1" || isPrint=="2") && $j('#reviewspan').is(".ui-icon-plusthick"))){
		flag5=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('reviewaudittrail','headerPrnt');
			},500);
		}
	}
}

function hlaOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#hlaspan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('hlaaudittrail','headerPrnt');
		},500);
		return false;
	}
	if($j('#hlaspan').is(".ui-icon-plusthick")) {
		if(flag6==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=hla','hlaDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag6=1;
		}
	}
	if($j('#hlaspan').is(".ui-icon-minusthick")) {
		$j('#hlaDiv').show();
	}
	
	var is_all_entries=$j('#hlaResults').val();
	var pageNo=$j('#paginateWrapper27').find('.currentPage').text();
	var showEntries=$j('#hlainfo_tbl_sentries').val();
	var tEntries=$j('#tentriesHla').val();
	var sort=2;
	var sortDir="desc";
	var SortParm=$j('#hlainfo_tbl_sort_col').val();
	var filter_value=$j('#hla_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#hlainfo_tbl_sort_col').val();
		sortDir=$j('#hlainfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#hlainfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#hlainfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="hlaResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';

	 if(showEntries=="ALL"){

	var hlaTbl = $j('#hlatab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=hlaInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#hlainfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
    		                	return source[5];
		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
  		                	return source[6];
		                   }}
						],

						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#hlatab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#hlatab_info').hide();
						    $j("#hlaResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnHlaInfoTableSort('hlaInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "HLA Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues7').html(temptxt);
	           return temptxt;
	         
	         }					
	});
	$j('#hlatab_info').after('<div id="entriesvalues7"><div>');
   	//var temptxt='Showing 1 to '+$j('#tentriesHla').val()+' of '+$j('#tentriesHla').val()+' entries';
   	//$j('#entriesvalues7').html(temptxt);	

	 }else if(isPrint!="1" && isPrint!="2"){
		 var hlaTbl = $j('#hlatab').dataTable({
         "sScrollY": "150",
         "sScrollX": "100%",
         "sAjaxSource": 'getJsonPagination.action?TblFind=hlaInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#hlainfo_tbl_param').val(),
         "bServerSide": true,
         "bProcessing": true, 
  		"bRetrieve" : true,
  		"oSearch": {"sSearch": filter_value},
			"bDestroy": true,
			"bSortCellsTop": true,
  		"aaSorting": [[ sort, sortDir ]],
  		"bDeferRender": true,
  		"sAjaxDataProp": "aaData",
  		"aoColumnDefs": [
  		                {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
   		                	return source[5];
		                   }}      	 ,
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
 		                	return source[6];
		                   }}
						],
						"fnDrawCallback": function() { 
							fnHlaInfoTableSort('hlaInfoTr');
						},
				"sDom": 'T<"clear">lfrtip',
				"oTableTools": {
					"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
					"aButtons": [
									{
										"sExtends": "xls",
										"sButtonText": "Export",
											"sFileName": "HLA Audit Trail.csv"  
									}
								]
				}

		 });
		 hlaTbl.fnAdjustColumnSizing();
	    }else if(((isPrint=="1" || isPrint=="2") && $j('#hlaspan').is(".ui-icon-plusthick"))){
			 var hlaTbl = $j('#hlatab').dataTable({
		         
		         "sAjaxSource": 'getJsonPagination.action?TblFind=hlaInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#hlainfo_tbl_param').val(),
		         "bServerSide": true,
		         "bProcessing": true, 
		  		"bRetrieve" : true,
		  		"oSearch": {"sSearch": filter_value},
					"bDestroy": true,
					"bSortCellsTop": true,
		  		"aaSorting": [[ sort, sortDir ]],
		  		"bDeferRender": true,
		  		"sAjaxDataProp": "aaData",
		  		"aoColumnDefs": [
		  		                {		
									  "aTargets": [0],"mDataProp": function ( source, type, val ) {
				                	   return source[1];
				                  }},
				                  {
				                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
									return source[2];
				                  }},
				                  {
				                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
				                	 	return source[3];
				                  }},
				                  {		
				                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
				                	 	return source[4];
			                	   }},
				                   { 	
			                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
		   		                	return source[5];
				                   }}      	 ,
				                   { 	
			                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
		 		                	return source[6];
				                   }}
								],
								"fnDrawCallback": function() { 
									fnHlaInfoTableSort('hlaInfoTr');
								},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
							"aButtons": [
											{
												"sExtends": "xls",
												"sButtonText": "Export",
													"sFileName": "HLA Audit Trail.csv"  
											}
										]
						}

				 });
			    }

	 $j('#hlatab_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#hla_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,hlaResults,temp,trailForm,hlaDiv,hlaOnload,cordID,entityId,hla,hlainfo_tbl_param");
		  }
	});
	 
	 var usedNames = {};
		$j("select[name='fieldName7'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='userId7'] > option").each(function () {
		    if(usedNames[this.text] ||  $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='actTaken7'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='prevValue7'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='newValue7'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		
		var	fieldName=$j('#fieldName7').val();
		var	userId=$j('#userId7').val();
		var	actTaken=$j('#actTaken7').val();
		var	prevValue=$j('#prevValue7').val();
		var	newValue=$j('#newValue7').val();
		var	pStartDateD1=$j('#dateStart7').val();
		var	pStartDateD2=$j('#dateEnd7').val();

		

	$j('#resetHla').click(function(){
		hlaTbl.fnSort( [ [2,'desc'] ] );
		hlaTbl.fnDraw();
		 });
	
	$j('#hlatab_info').hide();
	$j('#hlatab_paginate').hide();
	$j('#hlatab_length').empty();
	$j('#hlatab_length').replaceWith(showEntriesTxt);
	if($j('#hlaentries').val()!=null || $j('#hlaentries').val()!=""){
		$j('#hlaResults').val($j('#hlaentries').val());
		}
	$j('#hlaResults').change(function(){
		var textval=$j('#hlaResults :selected').html();
		$j('#hlainfo_tbl_sentries').val(textval);
		var fieldName=$j('#fieldName7').val();
		var userId=$j('#userId7').val();
		var actTaken=$j('#actTaken7').val();
		var prevValue=$j('#prevValue7').val();
		var newValue=$j('#newValue7').val();
		var pStartDateD1=$j('#dateStart7').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd7').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,hlaResults,temp,trailForm,hlaDiv,hlaOnload,cordID,entityId,hla,hlainfo_tbl_param");
		$j('#fieldName7').val(fieldName);
		$j('#userId7').val(userId);
		$j('#actTaken7').val(actTaken);
		$j('#prevValue7').val(prevValue);
		$j('#newValue7').val(newValue);
		$j('#dateStart7').val(pStartDtD1);
		$j('#dateEnd7').val(pStartDtD2);
	});	
	var wrapperWidth=$j('#hlaaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#hlatab').width(newWidth);
	var fldNme = $j('#fldNme7').val();
	var usrId = $j('#usrId7').val();
	var actTkn = $j('#actTkn7').val();
	var prvVl = $j('#prvVl7').val();
	var nwVl = $j('#nwVl7').val();

	$j('#fieldName7').val(fldNme);
	$j('#userId7').val(usrId);
	$j('#actTaken7').val(actTkn);
	$j('#prevValue7').val(prvVl);
	$j('#newValue7').val(nwVl);
	if(((isPrint=="1" || isPrint=="2") && $j('#hlaspan').is(".ui-icon-plusthick"))){
		flag6=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('hlaaudittrail','headerPrnt');
			},500);
		}
	}
}


function pathlaOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#pathlaspan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('pathlaaudittrail','headerPrnt');
		},500);
		return false;
	}
	if($j('#pathlaspan').is(".ui-icon-plusthick")) {
		if(flag16==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=pathla','pathlaDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag16=1;
		}
	}
	if($j('#pathlaspan').is(".ui-icon-minusthick")) {
		$j('#pathlaDiv').show();
	}
	
	var is_all_entries=$j('#pathlaResults').val();
	var pageNo=$j('#paginateWrapper35').find('.currentPage').text();
	var showEntries=$j('#pathlainfo_tbl_sentries').val();
	var tEntries=$j('#tentriespatHla').val();
	var sort=2;
	var sortDir="desc";
	var SortParm=$j('#pathlainfo_tbl_sort_col').val();
	var filter_value=$j('#pathla_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#pathlainfo_tbl_sort_col').val();
		sortDir=$j('#pathlainfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#pathlainfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#pathlainfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="pathlaResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';

	 if(showEntries=="ALL"){

	var pathlaTbl = $j('#pathlatab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=pathlaInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#pathlainfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
    		                	return source[5];
		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
  		                	return source[6];
		                   }}
						],

						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#pathlatab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#pathlatab_info').hide();
						    $j("#pathlaResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnPatHlaInfoTableSort('pathlaInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Patient HLA Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues15').html(temptxt);
	           return temptxt;
	         
	         }					
	});
	$j('#pathlatab_info').after('<div id="entriesvalues15"><div>');
   	//var temptxt='Showing 1 to '+$j('#tentriespatHla').val()+' of '+$j('#tentriespatHla').val()+' entries';
   	//$j('#entriesvalues15').html(temptxt);	

	 }else if(isPrint!="1" && isPrint!="2"){
		 var pathlaTbl = $j('#pathlatab').dataTable({
         "sScrollY": "150",
         "sScrollX": "100%",
         "sAjaxSource": 'getJsonPagination.action?TblFind=pathlaInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#pathlainfo_tbl_param').val(),
         "bServerSide": true,
         "bProcessing": true, 
  		"bRetrieve" : true,
  		"oSearch": {"sSearch": filter_value},
			"bDestroy": true,
			"bSortCellsTop": true,
  		"aaSorting": [[ sort, sortDir ]],
  		"bDeferRender": true,
  		"sAjaxDataProp": "aaData",
  		"aoColumnDefs": [
  		                {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
   		                	return source[5];
		                   }}      	 ,
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
 		                	return source[6];
		                   }}
						],
						"fnDrawCallback": function() { 
							fnPatHlaInfoTableSort('pathlaInfoTr');
						},
				"sDom": 'T<"clear">lfrtip',
				"oTableTools": {
					"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
					"aButtons": [
									{
										"sExtends": "xls",
										"sButtonText": "Export",
											"sFileName": "Patient HLA Audit Trail.csv"  
									}
								]
				}

		 });
		 pathlaTbl.fnAdjustColumnSizing();
	    }else if(((isPrint=="1" || isPrint=="2") && $j('#pathlaspan').is(".ui-icon-plusthick"))){
			 var pathlaTbl = $j('#pathlatab').dataTable({
		         "sScrollY": "150",
		         "sScrollX": "100%",
		         "sAjaxSource": 'getJsonPagination.action?TblFind=pathlaInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#pathlainfo_tbl_param').val(),
		         "bServerSide": true,
		         "bProcessing": true, 
		  		"bRetrieve" : true,
		  		"oSearch": {"sSearch": filter_value},
					"bDestroy": true,
					"bSortCellsTop": true,
		  		"aaSorting": [[ sort, sortDir ]],
		  		"bDeferRender": true,
		  		"sAjaxDataProp": "aaData",
		  		"aoColumnDefs": [
		  		                {		
									  "aTargets": [0],"mDataProp": function ( source, type, val ) {
				                	   return source[1];
				                  }},
				                  {
				                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
									return source[2];
				                  }},
				                  {
				                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
				                	 	return source[3];
				                  }},
				                  {		
				                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
				                	 	return source[4];
			                	   }},
				                   { 	
			                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
		   		                	return source[5];
				                   }}      	 ,
				                   { 	
			                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
		 		                	return source[6];
				                   }}
								],
								"fnDrawCallback": function() { 
									fnPatHlaInfoTableSort('pathlaInfoTr');
								},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
							"aButtons": [
											{
												"sExtends": "xls",
												"sButtonText": "Export",
													"sFileName": "Patient HLA Audit Trail.csv"  
											}
										]
						}

				 });
			    }

	 $j('#pathlatab_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#pathla_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,pathlaResults,temp,trailForm,pathlaDiv,pathlaOnload,cordID,entityId,pathla,pathlainfo_tbl_param");
		  }
	});
	 
	 var usedNames = {};
		$j("select[name='fieldName12'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='userId12'] > option").each(function () {
		    if(usedNames[this.text] ||  $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='actTaken12'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='prevValue12'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='newValue12'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		
		var	fieldName=$j('#fieldName12').val();
		var	userId=$j('#userId12').val();
		var	actTaken=$j('#actTaken12').val();
		var	prevValue=$j('#prevValue12').val();
		var	newValue=$j('#newValue12').val();
		var	pStartDateD1=$j('#dateStart15').val();
		var	pStartDateD2=$j('#dateEnd15').val();

		

	$j('#resetpatHla').click(function(){
		pathlaTbl.fnSort( [ [2,'desc'] ] );
		pathlaTbl.fnDraw();
		 });
	
	$j('#pathlatab_info').hide();
	$j('#pathlatab_paginate').hide();
	$j('#pathlatab_length').empty();
	$j('#pathlatab_length').replaceWith(showEntriesTxt);
	if($j('#pathlaentries').val()!=null || $j('#pathlaentries').val()!=""){
		$j('#pathlaResults').val($j('#pathlaentries').val());
		}
	$j('#pathlaResults').change(function(){
		var textval=$j('#pathlaResults :selected').html();
		$j('#pathlainfo_tbl_sentries').val(textval);
		var fieldName=$j('#fieldName12').val();
		var userId=$j('#userId12').val();
		var actTaken=$j('#actTaken12').val();
		var prevValue=$j('#prevValue12').val();
		var newValue=$j('#newValue12').val();
		var pStartDateD1=$j('#dateStart15').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd15').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,pathlaResults,temp,trailForm,pathlaDiv,pathlaOnload,cordID,entityId,pathla,pathlainfo_tbl_param");
		$j('#fieldName12').val(fieldName);
		$j('#userId12').val(userId);
		$j('#actTaken12').val(actTaken);
		$j('#prevValue12').val(prevValue);
		$j('#newValue12').val(newValue);
		$j('#dateStart15').val(pStartDtD1);
		$j('#dateEnd15').val(pStartDtD2);
	});	
	var wrapperWidth=$j('#pathlaaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#pathlatab').width(newWidth);
	var fldNme = $j('#fldNme12').val();
	var usrId = $j('#usrId12').val();
	var actTkn = $j('#actTkn12').val();
	var prvVl = $j('#prvVl12').val();
	var nwVl = $j('#nwVl12').val();

	$j('#fieldName12').val(fldNme);
	$j('#userId12').val(usrId);
	$j('#actTaken12').val(actTkn);
	$j('#prevValue12').val(prvVl);
	$j('#newValue12').val(nwVl);
	if(((isPrint=="1" || isPrint=="2") && $j('#pathlaspan').is(".ui-icon-plusthick"))){
		flag16=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('pathlaaudittrail','headerPrnt');
			},500);
		}
	}
}



function idOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#idspan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('idparent','headerPrnt');
		},500);
		return false;
	}
	if($j('#idspan').is(".ui-icon-plusthick")) {
		if(flag13==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=id','idDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag13=1;
		}
	}
	if($j('#idspan').is(".ui-icon-minusthick")) {
		$j('#idDiv').show();
	}

	var is_all_entries=$j('#idResults').val();
	var pageNo=$j('#paginateWrapper22').find('.currentPage').text();
	var showEntries=$j('#idinfo_tbl_sentries').val();
	var tEntries=$j('#tentriesId').val();
	var sort=2;
	var sortDir="desc";
	var SortParm=$j('#idinfo_tbl_sort_col').val();
	var filter_value=$j('#id_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#idinfo_tbl_sort_col').val();
		sortDir=$j('#idinfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#idinfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#idinfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="idResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';

	 if(showEntries=="ALL"){

	var idTbl = $j('#idtab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=idInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#idinfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
    		                	return source[5];
		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
  		                	return source[6];
		                   }}
						],

						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#idtab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#idtab_info').hide();
						    $j("#showsRowsearchResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnIdInfoTableSort('idInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "ID Audit Trail.csv"  
							}
						]
		},
			"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
		        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
		        if(iTotal==0){
		         temptxt='Showing 0 to 0 of 0 entries';
		        }
		        if(iMax!=iTotal){
		         temptxt=temptxt+'(filtered from '+iMax+' entries)';
		        }
		           $j('#entriesvalues2').html(temptxt);
		           return temptxt;
		         
		         }						
	});
	$j('#idtab_info').after('<div id="entriesvalues2"><div>');
   	//var temptxt='Showing 1 to '+$j('#tentriesId').val()+' of '+$j('#tentriesId').val()+' entries';
   	//$j('#entriesvalues2').html(temptxt);	

	 }else if(isPrint!="1" && isPrint!="2"){
    		var idTbl=$j('#idtab').dataTable({
            "sScrollY": "150",
            "sScrollX": "100%",
            "sAjaxSource": 'getJsonPagination.action?TblFind=idInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#idinfo_tbl_param').val(),
            "bServerSide": true,
            "bProcessing": true, 
     		"bRetrieve" : true,
     		"oSearch": {"sSearch": filter_value},
			"bDestroy": true,
			"bSortCellsTop": true,
     		"aaSorting": [[ sort, sortDir ]],
     		"bDeferRender": true,
     		"sAjaxDataProp": "aaData",
     		"aoColumnDefs": [
     		                {		
  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
  		                	   return source[1];
  		                  }},
  		                  {
  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
  		                	 	return source[3];
  		                  }},
  		                  {		
  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
  		                	 	return source[4];
  	                	   }},
  		                   { 	
  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
      		                	return source[5];
  		                   }}      	 ,
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
    		                	return source[6];
		                   }}
  						],
  						"fnDrawCallback": function() { 
  							fnIdInfoTableSort('idInfoTr');
  						},
				"sDom": 'T<"clear">lfrtip',
  						"oTableTools": {
  							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
  							"aButtons": [
  											{
  												"sExtends": "xls",
  												"sButtonText": "Export",
  												 /*"fnClick": function ( nButton, oConfig, flash ) {
  							                        this.fnSetText( flash, "\n"+ "\t\t CBU Information Audit Trail" +"\n\n"+
  							                                this.fnGetTableData(oConfig) );
  													},*/
  													"sFileName": "ID Audit Trail.csv"  
  											}
  										]
  						}			
		});
    		idTbl.fnAdjustColumnSizing();
    }else if(((isPrint=="1" || isPrint=="2") && $j('#idspan').is(".ui-icon-plusthick"))){
    		var idTbl=$j('#idtab').dataTable({
           
            "sAjaxSource": 'getJsonPagination.action?TblFind=idInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#idinfo_tbl_param').val(),
            "bServerSide": true,
            "bProcessing": true, 
     		"bRetrieve" : true,
     		"oSearch": {"sSearch": filter_value},
			"bDestroy": true,
			"bSortCellsTop": true,
     		"aaSorting": [[ sort, sortDir ]],
     		"bDeferRender": true,
     		"sAjaxDataProp": "aaData",
     		"aoColumnDefs": [
     		                {		
  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
  		                	   return source[1];
  		                  }},
  		                  {
  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
  		                	 	return source[3];
  		                  }},
  		                  {		
  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
  		                	 	return source[4];
  	                	   }},
  		                   { 	
  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
      		                	return source[5];
  		                   }}      	 ,
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
    		                	return source[6];
		                   }}
  						],
  						"fnDrawCallback": function() { 
  							fnIdInfoTableSort('idInfoTr');
  						},
				"sDom": 'T<"clear">lfrtip',
  						"oTableTools": {
  							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
  							"aButtons": [
  											{
  												"sExtends": "xls",
  												"sButtonText": "Export",
  												 /*"fnClick": function ( nButton, oConfig, flash ) {
  							                        this.fnSetText( flash, "\n"+ "\t\t CBU Information Audit Trail" +"\n\n"+
  							                                this.fnGetTableData(oConfig) );
  													},*/
  													"sFileName": "ID Audit Trail.csv"  
  											}
  										]
  						}			
		});
    	
    }

	 $j('#idtab_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#id_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,idResults,temp,trailForm,idDiv,idOnload,cordID,entityId,id,idinfo_tbl_param");
		  }
	});
	 
	 var usedNames = {};
		$j("select[name='fieldName2'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='userId2'] > option").each(function () {
		    if(usedNames[this.text] ||  $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='actTaken2'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='prevValue2'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='newValue2'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		
		var	fieldName=$j('#fieldName2').val();
		var	userId=$j('#userId2').val();
		var	actTaken=$j('#actTaken2').val();
		var	prevValue=$j('#prevValue2').val();
		var	newValue=$j('#newValue2').val();
		var	pStartDateD1=$j('#dateStart2').val();
		var	pStartDateD2=$j('#dateEnd2').val();

	$j('#resetID').click(function(){
		idTbl.fnSort( [ [2,'desc'] ] );
		idTbl.fnDraw();
		 });

	$j('#idtab_info').hide();
	$j('#idtab_paginate').hide();
	$j('#idtab_length').empty();	
	 

	$j('#idtab_length').replaceWith(showEntriesTxt);
	if($j('#identries').val()!=null || $j('#identries').val()!=""){
		$j('#idResults').val($j('#identries').val());
		}
	
	$j('#idResults').change(function(){
		var textval=$j('#idResults :selected').html();
		$j('#idinfo_tbl_sentries').val(textval);
		var fieldName=$j('#fieldName2').val();
		var userId=$j('#userId2').val();
		var actTaken=$j('#actTaken2').val();
		var prevValue=$j('#prevValue2').val();
		var newValue=$j('#newValue2').val();
		var pStartDateD1=$j('#dateStart2').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd2').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,idResults,temp,trailForm,idDiv,idOnload,cordID,entityId,id,idinfo_tbl_param");
		$j('#fieldName2').val(fieldName);
		$j('#userId2').val(userId);
		$j('#actTaken2').val(actTaken);
		$j('#prevValue2').val(prevValue);
		$j('#newValue2').val(newValue);
		$j('#dateStart2').val(pStartDtD1);
		$j('#dateEnd2').val(pStartDtD2);
	});	
	var wrapperWidth=$j('#idaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#idtab').width(newWidth);
	var fldNme = $j('#fldNme2').val();
	var usrId = $j('#usrId2').val();
	var actTkn = $j('#actTkn2').val();
	var prvVl = $j('#prvVl2').val();
	var nwVl = $j('#nwVl2').val();

	$j('#fieldName2').val(fldNme);
	$j('#userId2').val(usrId);
	$j('#actTaken2').val(actTkn);
	$j('#prevValue2').val(prvVl);
	$j('#newValue2').val(nwVl);
	if(((isPrint=="1" || isPrint=="2") && $j('#idspan').is(".ui-icon-plusthick"))){
		flag13=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('idparent','headerPrnt');
			},500);
		}
	}
}



function procInfoOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#procInfospan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('procInfoparent','headerPrnt');
		},500);
		return false;
	}
	if($j('#procInfospan').is(".ui-icon-plusthick")) {
		if(flag14==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=procInfo','procInfoDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag14=1;
		}
	}
	if($j('#procInfospan').is(".ui-icon-minusthick")) {
		$j('#procInfoDiv').show();
	}

	var is_all_entries=$j('#procInfoResults').val();
	var pageNo=$j('#paginateWrapper23').find('.currentPage').text();
	var showEntries=$j('#procinfo_tbl_sentries').val();
	var tEntries=$j('#tentriesProc').val();
	var sort=2;
	var sortDir="desc";
	var SortParm=$j('#procinfo_tbl_sort_col').val();
	var filter_value=$j('#proc_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#procinfo_tbl_sort_col').val();
		sortDir=$j('#procinfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#procinfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#procinfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="procInfoResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';
	
	if(showEntries=="ALL"){
		var procInfoTbl = $j('#procInfotab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=procInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#procinfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
    		                	return source[5];
		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
  		                	return source[6];
		                   }}
						],
						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#procInfotab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#procInfotab_info').hide();
						    $j("#showsRowsearchResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnProcInfoTableSort('procInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',

		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Processing Information Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues3').html(temptxt);
	           return temptxt;
	         
	         }										
	});
		$j('#procInfotab_info').after('<div id="entriesvalues3"><div>');
	   	//var temptxt='Showing 1 to '+$j('#tentriesId').val()+' of '+$j('#tentriesId').val()+' entries';
	   	//$j('#entriesvalues3').html(temptxt);	

	 }else if(isPrint!="1" && isPrint!="2"){
 		var procInfoTbl=$j('#procInfotab').dataTable({
         "sScrollY": "150",
         "sScrollX": "100%",
         "sAjaxSource": 'getJsonPagination.action?TblFind=procInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#procinfo_tbl_param').val(),
         "bServerSide": true,
         "bProcessing": true, 
  		"bRetrieve" : true,
  		"oSearch": {"sSearch": filter_value},
			"bDestroy": true,
			"bSortCellsTop": true,
  		"aaSorting": [[ sort, sortDir ]],
  		"bDeferRender": true,
  		"sAjaxDataProp": "aaData",
  		"aoColumnDefs": [
  		                {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
   		                	return source[5];
		                   }}      	 ,
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
 		                	return source[6];
		                   }}
						],
						"fnDrawCallback": function() { 
  							fnProcInfoTableSort('procInfoTr');
						},
				"sDom": 'T<"clear">lfrtip',
				"oTableTools": {
							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
							"aButtons": [
											{
												"sExtends": "xls",
												"sButtonText": "Export",
												 /*"fnClick": function ( nButton, oConfig, flash ) {
							                        this.fnSetText( flash, "\n"+ "\t\t CBU Information Audit Trail" +"\n\n"+
							                                this.fnGetTableData(oConfig) );
													},*/
													"sFileName": "Processing Information Audit Trail.csv"  
											}
										]
						}		
		});
 		procInfoTbl.fnAdjustColumnSizing();
 }else if(((isPrint=="1" || isPrint=="2") && $j('#procInfospan').is(".ui-icon-plusthick"))){
	 
		var procInfoTbl=$j('#procInfotab').dataTable({
	         "sAjaxSource": 'getJsonPagination.action?TblFind=procInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#procinfo_tbl_param').val(),
	         "bServerSide": true,
	         "bProcessing": true, 
	  		"bRetrieve" : true,
	  		"oSearch": {"sSearch": filter_value},
				"bDestroy": true,
				"bSortCellsTop": true,
	  		"aaSorting": [[ sort, sortDir ]],
	  		"bDeferRender": true,
	  		"sAjaxDataProp": "aaData",
	  		"aoColumnDefs": [
	  		                {		
								  "aTargets": [0],"mDataProp": function ( source, type, val ) {
			                	   return source[1];
			                  }},
			                  {
			                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
								return source[2];
			                  }},
			                  {
			                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
			                	 	return source[3];
			                  }},
			                  {		
			                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
			                	 	return source[4];
		                	   }},
			                   { 	
		                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
	   		                	return source[5];
			                   }}      	 ,
			                   { 	
		                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
	 		                	return source[6];
			                   }}
							],
							"fnDrawCallback": function() { 
	  							fnProcInfoTableSort('procInfoTr');
							},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
								"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
								"aButtons": [
												{
													"sExtends": "xls",
													"sButtonText": "Export",
													 /*"fnClick": function ( nButton, oConfig, flash ) {
								                        this.fnSetText( flash, "\n"+ "\t\t CBU Information Audit Trail" +"\n\n"+
								                                this.fnGetTableData(oConfig) );
														},*/
														"sFileName": "Processing Information Audit Trail.csv"  
												}
											]
							}		
			});
	 	
	 }
	$j('#procInfotab_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#proc_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,procInfoResults,temp,trailForm,procInfoDiv,procInfoOnload,cordID,entityId,procInfo,procinfo_tbl_param");
		  }
	});
	
	 var usedNames = {};
		$j("select[name='fieldName3'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='userId3'] > option").each(function () {
		    if(usedNames[this.text] ||  $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='actTaken3'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='prevValue3'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='newValue3'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		
		var	fieldName=$j('#fieldName3').val();
		var	userId=$j('#userId3').val();
		var	actTaken=$j('#actTaken3').val();
		var	prevValue=$j('#prevValue3').val();
		var	newValue=$j('#newValue3').val();
		var	pStartDateD1=$j('#dateStart3').val();
		var	pStartDateD2=$j('#dateEnd3').val();

	$j('#resetProc').click(function(){
		procInfoTbl.fnSort( [ [2,'desc'] ] );
		procInfoTbl.fnDraw();
		 });

	
	$j('#procInfotab_info').hide();
	$j('#procInfotab_paginate').hide();
	$j('#procInfotab_length').empty();

	$j('#procInfotab_length').replaceWith(showEntriesTxt);
	if($j('#procInfoentries').val()!=null || $j('#procInfoentries').val()!=""){
		$j('#procInfoResults').val($j('#procInfoentries').val());
		}
	$j('#procInfoResults').change(function(){
		var textval=$j('#procInfoResults :selected').html();
		$j('#procinfo_tbl_sentries').val(textval);
		var fieldName=$j('#fieldName3').val();
		var userId=$j('#userId3').val();
		var actTaken=$j('#actTaken3').val();
		var prevValue=$j('#prevValue3').val();
		var newValue=$j('#newValue3').val();
		var pStartDateD1=$j('#dateStart3').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd3').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,procInfoResults,temp,trailForm,procInfoDiv,procInfoOnload,cordID,entityId,procInfo,procinfo_tbl_param");
		$j('#fieldName3').val(fieldName);
		$j('#userId3').val(userId);
		$j('#actTaken3').val(actTaken);
		$j('#prevValue3').val(prevValue);
		$j('#newValue3').val(newValue);
		$j('#dateStart3').val(pStartDtD1);
		$j('#dateEnd3').val(pStartDtD2);
	});	
	var wrapperWidth=$j('#procInfoaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#procInfotab').width(newWidth);
	var fldNme = $j('#fldNme3').val();
	var usrId = $j('#usrId3').val();
	var actTkn = $j('#actTkn3').val();
	var prvVl = $j('#prvVl3').val();
	var nwVl = $j('#nwVl3').val();

	$j('#fieldName3').val(fldNme);
	$j('#userId3').val(usrId);
	$j('#actTaken3').val(actTkn);
	$j('#prevValue3').val(prvVl);
	$j('#newValue3').val(nwVl);
	if(((isPrint=="1" || isPrint=="2") && $j('#procInfospan').is(".ui-icon-plusthick"))){
		//procInfoTbl.fnAdjustColumnSizing();
		flag14=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('procInfoparent','headerPrnt');
			},500);
		}
	}
}

function labOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#labspan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('labsummaryaudittrail','headerPrnt');
		},500);
		return false;
	}
	if($j('#labspan').is(".ui-icon-plusthick")) {
		if(flag7==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=labsummary','labDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag7=1;
		}
	}
	if($j('#labspan').is(".ui-icon-minusthick")) {
		$j('#labDiv').show();
	}

	var is_all_entries=$j('#labResults').val();
	var pageNo=$j('#paginateWrapper28').find('.currentPage').text();
	var showEntries=$j('#labinfo_tbl_sentries').val();
	var tEntries=$j('#tentriesLab').val();
	var sort=2;
	var sortDir="desc";
	var SortParm=$j('#labinfo_tbl_sort_col').val();
	var filter_value=$j('#lab_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#labinfo_tbl_sort_col').val();
		sortDir=$j('#labinfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#labinfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#labinfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="labResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';

	 if(showEntries=="ALL"){
	

	var labTbl = $j('#labtab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=labInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#labinfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
    		                	return source[5];
		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
  		                	return source[6];
		                   }}
						],

						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#labtab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#labtab_info').hide();
						    $j("#labResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnLabInfoTableSort('labInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Lab Summary Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues8').html(temptxt);
	           return temptxt;
	         
	         }					
	});
	$j('#labtab_info').after('<div id="entriesvalues8"><div>');
   	//var temptxt='Showing 1 to '+$j('#tentriesLab').val()+' of '+$j('#tentriesLab').val()+' entries';
   	//$j('#entriesvalues8').html(temptxt);	

	 }else if(isPrint!="1" && isPrint!="2"){

		 var labTbl = $j('#labtab').dataTable({

			 "sScrollY": "150",
	            "sScrollX": "100%",
	            "sAjaxSource": 'getJsonPagination.action?TblFind=labInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#labinfo_tbl_param').val(),
	            "bServerSide": true,
	            "bProcessing": true, 
	     		"bRetrieve" : true,
	     		"oSearch": {"sSearch": filter_value},
				"bDestroy": true,
				"bSortCellsTop": true,
	     		"aaSorting": [[ sort, sortDir ]],
	     		"bDeferRender": true,
	     		"sAjaxDataProp": "aaData",
	     		"aoColumnDefs": [
	     		                {		
	  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
	  		                	   return source[1];
	  		                  }},
	  		                  {
	  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
								return source[2];
			                  }},
			                  {
			                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
	  		                	 	return source[3];
	  		                  }},
	  		                  {		
	  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
	  		                	 	return source[4];
	  	                	   }},
	  		                   { 	
	  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
	      		                	return source[5];
	  		                   }}      	 ,
			                   { 	
		                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
	    		                	return source[6];
			                   }}
	  						],
	  						"fnDrawCallback": function() { 
	  							fnIdInfoTableSort('idInfoTr');
	  						},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
						"aButtons": [
										{
											"sExtends": "xls",
											"sButtonText": "Export",
												"sFileName": "Lab Summary Audit Trail.csv"  
										}
									]
					}

			});
		 labTbl.fnAdjustColumnSizing();
	    }else if(((isPrint=="1" || isPrint=="2") && $j('#labspan').is(".ui-icon-plusthick"))){

			 var labTbl = $j('#labtab').dataTable({

		            "sAjaxSource": 'getJsonPagination.action?TblFind=labInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#labinfo_tbl_param').val(),
		            "bServerSide": true,
		            "bProcessing": true, 
		     		"bRetrieve" : true,
		     		"oSearch": {"sSearch": filter_value},
					"bDestroy": true,
					"bSortCellsTop": true,
		     		"aaSorting": [[ sort, sortDir ]],
		     		"bDeferRender": true,
		     		"sAjaxDataProp": "aaData",
		     		"aoColumnDefs": [
		     		                {		
		  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		  		                	   return source[1];
		  		                  }},
		  		                  {
		  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
									return source[2];
				                  }},
				                  {
				                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		  		                	 	return source[3];
		  		                  }},
		  		                  {		
		  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		  		                	 	return source[4];
		  	                	   }},
		  		                   { 	
		  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
		      		                	return source[5];
		  		                   }}      	 ,
				                   { 	
			                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
		    		                	return source[6];
				                   }}
		  						],
		  						"fnDrawCallback": function() { 
		  							fnIdInfoTableSort('idInfoTr');
		  						},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
							"aButtons": [
											{
												"sExtends": "xls",
												"sButtonText": "Export",
													"sFileName": "Lab Summary Audit Trail.csv"  
											}
										]
						}

				});
		    	
		    }

	 $j('#labtab_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#lab_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,labResults,temp,trailForm,labDiv,labOnload,cordID,entityId,labsummary,labinfo_tbl_param");
		  }
	});
	 var usedNames = {};
		$j("select[name='fieldName8'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='userId8'] > option").each(function () {
		    if(usedNames[this.text] ||  $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='actTaken8'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='prevValue8'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='newValue8'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		
		var	fieldName=$j('#fieldName8').val();
		var	userId=$j('#userId8').val();
		var	actTaken=$j('#actTaken8').val();
		var	prevValue=$j('#prevValue8').val();
		var	newValue=$j('#newValue8').val();
		var	pStartDateD1=$j('#dateStart8').val();
		var	pStartDateD2=$j('#dateEnd8').val();

	$j('#resetLab').click(function(){
		labTbl.fnSort( [ [2,'desc'] ] );
		labTbl.fnDraw();
		 });

	
	$j('#labtab_info').hide();
	$j('#labtab_paginate').hide();
	$j('#labtab_length').empty();
	$j('#labtab_length').replaceWith(showEntriesTxt);
	if($j('#labentries').val()!=null || $j('#labentries').val()!=""){
		$j('#labResults').val($j('#labentries').val());
		}
	$j('#labResults').change(function(){
		var textval=$j('#labResults :selected').html();
		$j('#labinfo_tbl_sentries').val(textval);
		var fieldName=$j('#fieldName8').val();
		var userId=$j('#userId8').val();
		var actTaken=$j('#actTaken8').val();
		var prevValue=$j('#prevValue8').val();
		var newValue=$j('#newValue8').val();
		var pStartDateD1=$j('#dateStart8').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd8').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,labResults,temp,trailForm,labDiv,labOnload,cordID,entityId,labsummary,labinfo_tbl_param");
		$j('#fieldName8').val(fieldName);
		$j('#userId8').val(userId);
		$j('#actTaken8').val(actTaken);
		$j('#prevValue8').val(prevValue);
		$j('#newValue8').val(newValue);
		$j('#dateStart8').val(pStartDtD1);
		$j('#dateEnd8').val(pStartDtD2);
	});	
	var wrapperWidth=$j('#labsummaryaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#labtab').width(newWidth);
	var fldNme = $j('#fldNme8').val();
	var usrId = $j('#usrId8').val();
	var actTkn = $j('#actTkn8').val();
	var prvVl = $j('#prvVl8').val();
	var nwVl = $j('#nwVl8').val();

	$j('#fieldName8').val(fldNme);
	$j('#userId8').val(usrId);
	$j('#actTaken8').val(actTkn);
	$j('#prevValue8').val(prvVl);
	$j('#newValue8').val(nwVl);
	if(((isPrint=="1" || isPrint=="2") && $j('#labspan').is(".ui-icon-plusthick"))){
		flag7=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('labsummaryaudittrail','headerPrnt');
			},500);
		}
	}
}

function notesOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#notesspan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('cbnotesaudittrail','headerPrnt');
		},500);
		return false;
	}
	if($j('#notesspan').is(".ui-icon-plusthick")) {
		if(flag8==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=cbnotes','notesDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag8=1;
		}
	}
	if($j('#notesspan').is(".ui-icon-minusthick")) {
		$j('#notesDiv').show();
	}
	
	var is_all_entries=$j('#notesResults').val();
	var pageNo=$j('#paginateWrapper29').find('.currentPage').text();
	var showEntries=$j('#cninfo_tbl_sentries').val();
	var tEntries=$j('#tentriesCn').val();
	var sort=2;
	var sortDir="desc";
	var SortParm=$j('#cninfo_tbl_sort_col').val();
	var filter_value=$j('#cn_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#cninfo_tbl_sort_col').val();
		sortDir=$j('#cninfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#cninfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#cninfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="notesResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';

	 if(showEntries=="ALL"){

	
	var notesTbl = $j('#notestab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=cnInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#cninfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
    		                	return source[5];
		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
  		                	return source[6];
		                   }}
						],

						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#notestab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#notestab_info').hide();
						    $j("#notesResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnCnInfoTableSort('cnInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Clinical Notes Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues9').html(temptxt);
	           return temptxt;
	         
	         }					
	});
	$j('#notestab_info').after('<div id="entriesvalues9"><div>');
   	//var temptxt='Showing 1 to '+$j('#tentriesCn').val()+' of '+$j('#tentriesCn').val()+' entries';
   	//$j('#entriesvalues9').html(temptxt);	

	 }else if(isPrint!="1" && isPrint!="2"){
		 var notesTbl = $j('#notestab').dataTable({
			 "sScrollY": "150",
	            "sScrollX": "100%",
	            "sAjaxSource": 'getJsonPagination.action?TblFind=cnInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#cninfo_tbl_param').val(),
	            "bServerSide": true,
	            "bProcessing": true, 
	     		"bRetrieve" : true,
	     		"oSearch": {"sSearch": filter_value},
				"bDestroy": true,
				"bSortCellsTop": true,
	     		"aaSorting": [[ sort, sortDir ]],
	     		"bDeferRender": true,
	     		"sAjaxDataProp": "aaData",
	     		"aoColumnDefs": [
	     		                {		
	  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
	  		                	   return source[1];
	  		                  }},
	  		                  {
	  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
								return source[2];
			                  }},
			                  {
			                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
	  		                	 	return source[3];
	  		                  }},
	  		                  {		
	  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
	  		                	 	return source[4];
	  	                	   }},
	  		                   { 	
	  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
	      		                	return source[5];
	  		                   }}      	 ,
			                   { 	
		                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
	    		                	return source[6];
			                   }}
	  						],
	  						"fnDrawCallback": function() { 
	  							fnCnInfoTableSort('idInfoTr');
	  						},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
						"aButtons": [
										{
											"sExtends": "xls",
											"sButtonText": "Export",
											 /*"fnClick": function ( nButton, oConfig, flash ) {
						                        this.fnSetText( flash, "\n"+ "\t\t CBU Information Audit Trail" +"\n\n"+
						                                this.fnGetTableData(oConfig) );
												},*/
												"sFileName": "Clinical Notes Audit Trail.csv"  
										}
									]
					}
		 });
		 notesTbl.fnAdjustColumnSizing();
	    }else if(((isPrint=="1" || isPrint=="2") && $j('#notesspan').is(".ui-icon-plusthick"))){
			 var notesTbl = $j('#notestab').dataTable({
				 
		            "sAjaxSource": 'getJsonPagination.action?TblFind=cnInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#cninfo_tbl_param').val(),
		            "bServerSide": true,
		            "bProcessing": true, 
		     		"bRetrieve" : true,
		     		"oSearch": {"sSearch": filter_value},
					"bDestroy": true,
					"bSortCellsTop": true,
		     		"aaSorting": [[ sort, sortDir ]],
		     		"bDeferRender": true,
		     		"sAjaxDataProp": "aaData",
		     		"aoColumnDefs": [
		     		                {		
		  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		  		                	   return source[1];
		  		                  }},
		  		                  {
		  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
									return source[2];
				                  }},
				                  {
				                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		  		                	 	return source[3];
		  		                  }},
		  		                  {		
		  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		  		                	 	return source[4];
		  	                	   }},
		  		                   { 	
		  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
		      		                	return source[5];
		  		                   }}      	 ,
				                   { 	
			                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
		    		                	return source[6];
				                   }}
		  						],
		  						"fnDrawCallback": function() { 
		  							fnCnInfoTableSort('idInfoTr');
		  						},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
							"aButtons": [
											{
												"sExtends": "xls",
												"sButtonText": "Export",
												 /*"fnClick": function ( nButton, oConfig, flash ) {
							                        this.fnSetText( flash, "\n"+ "\t\t CBU Information Audit Trail" +"\n\n"+
							                                this.fnGetTableData(oConfig) );
													},*/
													"sFileName": "Clinical Notes Audit Trail.csv"  
											}
										]
						}
			 });
		    	
		    }
	 
	 $j('#notestab_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#cn_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,notesResults,temp,trailForm,notesDiv,notesOnload,cordID,entityId,cbnotes,cninfo_tbl_param");
		  }
	});
			 
	 var usedNames = {};
		$j("select[name='fieldName9'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='userId9'] > option").each(function () {
		    if(usedNames[this.text] ||  $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='actTaken9'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='prevValue9'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='newValue9'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		
		var	fieldName=$j('#fieldName9').val();
		var	userId=$j('#userId9').val();
		var	actTaken=$j('#actTaken9').val();
		var	prevValue=$j('#prevValue9').val();
		var	newValue=$j('#newValue9').val();
		var	pStartDateD1=$j('#dateStart9').val();
		var	pStartDateD2=$j('#dateEnd9').val();

	$j('#resetClnote').click(function(){
		notesTbl.fnSort( [ [2,'desc'] ] );
		notesTbl.fnDraw();
		 });
	
	$j('#notestab_info').hide();
	$j('#notestab_paginate').hide();
	$j('#notestab_length').empty();
	$j('#notestab_length').replaceWith(showEntriesTxt);
	if($j('#notesentries').val()!=null || $j('#notesentries').val()!=""){
		$j('#notesResults').val($j('#notesentries').val());
		}
	$j('#notesResults').change(function(){
		var textval=$j('#notesResults :selected').html();
		$j('#cninfo_tbl_sentries').val(textval);
		var fieldName=$j('#fieldName9').val();
		var userId=$j('#userId9').val();
		var actTaken=$j('#actTaken9').val();
		var prevValue=$j('#prevValue9').val();
		var newValue=$j('#newValue9').val();
		var pStartDateD1=$j('#dateStart9').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd9').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,notesResults,temp,trailForm,notesDiv,notesOnload,cordID,entityId,cbnotes,cninfo_tbl_param");
		$j('#fieldName9').val(fieldName);
		$j('#userId9').val(userId);
		$j('#actTaken9').val(actTaken);
		$j('#prevValue9').val(prevValue);
		$j('#newValue9').val(newValue);
		$j('#dateStart9').val(pStartDtD1);
		$j('#dateEnd9').val(pStartDtD2);
	});	
	var wrapperWidth=$j('#cbnotesaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#notestab').width(newWidth);
	var fldNme = $j('#fldNme9').val();
	var usrId = $j('#usrId9').val();
	var actTkn = $j('#actTkn9').val();
	var prvVl = $j('#prvVl9').val();
	var nwVl = $j('#nwVl9').val();

	$j('#fieldName9').val(fldNme);
	$j('#userId9').val(usrId);
	$j('#actTaken9').val(actTkn);
	$j('#prevValue9').val(prvVl);
	$j('#newValue9').val(nwVl);
	if(((isPrint=="1" || isPrint=="2") && $j('#notesspan').is(".ui-icon-plusthick"))){
		flag8=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('cbnotesaudittrail','headerPrnt');
			},500);
		}
	}
}

function formsOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#formsspan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('formsaudittrail','headerPrnt');
		},500);
		return false;
	}
	if($j('#formsspan').is(".ui-icon-plusthick")) {
		if(flag9==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=forms','formsDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag9=1;
		}
	}
	if($j('#formsspan').is(".ui-icon-minusthick")) {
		$j('#formsDiv').show();
	}

	var is_all_entries=$j('#formsResults').val();
	var pageNo=$j('#paginateWrapper32').find('.currentPage').text();
	var showEntries=$j('#idminfo_tbl_sentries').val();
	var tEntries=$j('#tentriesIdm').val();
	var sort=3;
	var sortDir="desc";
	var SortParm=$j('#idminfo_tbl_sort_col').val();
	var filter_value=$j('#idm_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#idminfo_tbl_sort_col').val();
		sortDir=$j('#idminfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#idminfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#idminfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="formsResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';

	 if(showEntries=="ALL"){
			var formsTbl = $j('#formstab').dataTable({
				"sScrollY": "150",
		        "sScrollX": "100%",
		        "sAjaxSource": 'getJsonPagination.action?TblFind=idmInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#idminfo_tbl_param').val(),
		 		"bServerSide": true,
		 		"bProcessing": true, 
		 		"aaSorting": [[ sort, sortDir ]],
		 		"bRetrieve" : true,
		 		"oSearch": {"sSearch": filter_value},
		 		"bDeferRender": true,
		 		"bDestroy": true,
		 		"bSortCellsTop": true,
		 		"sAjaxDataProp": "aaData",
		 		"aoColumnDefs": [
								   {		
									  "aTargets": [0],"mDataProp": function ( source, type, val ) {
				                	   return source[1];
				                  }},
				                  {
				                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
									return source[2];
				                  }},
				                  {
				                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
				                	 	return source[3];
				                  }},
				                  {		
				                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
				                	 	return source[4];
			                	   }}
								],

								"fnDrawCallback": function() { 
		  							//alert("ALL Call Back........")
		  							$j('#formstab_wrapper').find('.dataTables_scrollFoot').hide();
								    $j('#formstab_info').hide();
								    $j("#formsResults option:contains('ALL')").attr('selected', 'selected'); 	
								    fnIdmInfoTableSort('idmInfoTr');
		  						},
		  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Infectious Disease Markers Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues12').html(temptxt);
	           return temptxt;
	         
	         }					
	});
			$j('#formstab_info').after('<div id="entriesvalues12"><div>');
		   	//var temptxt='Showing 1 to '+$j('#tentriesIdm').val()+' of '+$j('#tentriesIdm').val()+' entries';
		   	//$j('#entriesvalues12').html(temptxt);	

			 }else if(isPrint!="1" && isPrint!="2"){

				 var formsTbl = $j('#formstab').dataTable({
			            "sScrollY": "150",
			            "sScrollX": "100%",
			            "sAjaxSource": 'getJsonPagination.action?TblFind=idmInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#idminfo_tbl_param').val(),
			            "bServerSide": true,
			            "bProcessing": true, 
			     		"bRetrieve" : true,
			     		"oSearch": {"sSearch": filter_value},
						"bDestroy": true,
						"bSortCellsTop": true,
			     		"aaSorting": [[ sort, sortDir ]],
			     		"bDeferRender": true,
			     		"sAjaxDataProp": "aaData",
			     		"aoColumnDefs": [
			     		                {		
			  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
			  		                	   return source[1];
			  		                  }},
			  		                  {
			  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
										return source[2];
					                  }},
					                  {
					                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
			  		                	 	return source[3];
			  		                  }},
			  		                  {		
			  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
			  		                	 	return source[4];
			  	                	   }}
			  						],
			  						"fnDrawCallback": function() { 
			  							fnIdmInfoTableSort('idInfoTr');
			  						},
							"sDom": 'T<"clear">lfrtip',
							"oTableTools": {
								"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
								"aButtons": [
												{
													"sExtends": "xls",
													"sButtonText": "Export",
														"sFileName": "Infectious Disease Markers Audit Trail.csv"  
												}
											]
							}

					});
				 formsTbl.fnAdjustColumnSizing();	
			    }else if(((isPrint=="1" || isPrint=="2") && $j('#formsspan').is(".ui-icon-plusthick"))){

					 var formsTbl = $j('#formstab').dataTable({
				            
				            "sAjaxSource": 'getJsonPagination.action?TblFind=idmInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#idminfo_tbl_param').val(),
				            "bServerSide": true,
				            "bProcessing": true, 
				     		"bRetrieve" : true,
				     		"oSearch": {"sSearch": filter_value},
							"bDestroy": true,
							"bSortCellsTop": true,
				     		"aaSorting": [[ sort, sortDir ]],
				     		"bDeferRender": true,
				     		"sAjaxDataProp": "aaData",
				     		"aoColumnDefs": [
				     		                {		
				  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
				  		                	   return source[1];
				  		                  }},
				  		                  {
				  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
											return source[2];
						                  }},
						                  {
						                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
				  		                	 	return source[3];
				  		                  }},
				  		                  {		
				  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
				  		                	 	return source[4];
				  	                	   }}
				  						],
				  						"fnDrawCallback": function() { 
				  							fnIdmInfoTableSort('idInfoTr');
				  						},
								"sDom": 'T<"clear">lfrtip',
								"oTableTools": {
									"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
									"aButtons": [
													{
														"sExtends": "xls",
														"sButtonText": "Export",
															"sFileName": "Infectious Disease Markers Audit Trail.csv"  
													}
												]
								}

						});
				    	
				    }

	 $j('#formstab_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#idm_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,formsResults,temp,trailForm,formsDiv,formsOnload,cordID,entityId,forms,idminfo_tbl_param");
		  }
	});
	 
	 var usedNames = {};
		$j("select[name='formname1'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='fversion1'] > option").each(function () {
		    if(usedNames[this.text] ||  $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='fuserid1'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});

		var	formname =$j('#formname1').val();
		var	fversion =$j('#fversion1').val();
		var	userid =$j('#fuserid1').val();
		var	pStartDateD1=$j('#dateStart12').val();
		var	pStartDateD2=$j('#dateEnd12').val();

	$j('#resetIDM').click(function(){
		formsTbl.fnSort( [ [3,'desc'] ] );
		formsTbl.fnDraw();
		 });
	
	$j('#formstab_info').hide();
	$j('#formstab_paginate').hide();
	$j('#formstab_length').empty();
	$j('#formstab_length').replaceWith(showEntriesTxt);
	if($j('#formsentries').val()!=null || $j('#formsentries').val()!=""){
		$j('#formsResults').val($j('#formsentries').val());
		}
	$j('#formsResults').change(function(){
		var textval=$j('#formsResults :selected').html();
		$j('#idminfo_tbl_sentries').val(textval);
		var formname=$j('#formname1').val();
		var fversion=$j('#fversion1').val();
		var userid=$j('#fuserid1').val();
		var pStartDateD1=$j('#dateStart12').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd12').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,formsResults,temp,trailForm,formsDiv,formsOnload,cordID,entityId,forms,idminfo_tbl_param");
		$j('#formname1').val(formname);
		$j('#fversion1').val(fversion);
		$j('#fuserid1').val(userid);
		$j('#dateStart12').val(pStartDtD1);
		$j('#dateEnd12').val(pStartDtD2);
	});	
	var wrapperWidth=$j('#formsaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#formstab').width(newWidth);
	var frmnme = $j('#frmnme1').val();
	var fversn = $j('#fversn1').val();
	var usid = $j('#usid1').val();
	$j('#formname1').val(frmnme);
	$j('#fversion1').val(fversn);
	$j('#fuserid1').val(usid);
	if(((isPrint=="1" || isPrint=="2") && $j('#formsspan').is(".ui-icon-plusthick"))){
		flag9=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('formsaudittrail','headerPrnt');
			},500);
		}
	}
}


function formshhsOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#formshhsspan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('formshssaudittrail','headerPrnt');
		},500);
		return false;
	}
	if($j('#formshhsspan').is(".ui-icon-plusthick")) {
		if(flag12==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=formshhs','formshhsDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag12=1;
		}
	}
	if($j('#formshhsspan').is(".ui-icon-minusthick")) {
		$j('#formshhsDiv').show();
	}

	var is_all_entries=$j('#formshhsResults').val();
	var pageNo=$j('#paginateWrapper33').find('.currentPage').text();
	var showEntries=$j('#hhsinfo_tbl_sentries').val();
	var tEntries=$j('#tentriesHhs').val();
	var sort=3;
	var sortDir="desc";
	var SortParm=$j('#hhsinfo_tbl_sort_col').val();
	var filter_value=$j('#hhs_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#hhsinfo_tbl_sort_col').val();
		sortDir=$j('#hhsinfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#hhsinfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#hhsinfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="formshhsResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';

	 if(showEntries=="ALL"){
	

	var formshhsTbl = $j('#formshhstab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=hhsInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#hhsinfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }}
						],

						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#formshhstab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#formshhstab_info').hide();
						    $j("#formshhsResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnHhsInfoTableSort('hhsInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Health History Screening Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues13').html(temptxt);
	           return temptxt;
	         
	         }					
	});
	$j('#formshhstab_info').after('<div id="entriesvalues13"><div>');
   	//var temptxt='Showing 1 to '+$j('#tentriesHhs').val()+' of '+$j('#tentriesHhs').val()+' entries';
   	//$j('#entriesvalues13').html(temptxt);	
	 }else if(isPrint!="1" && isPrint!="2"){
		 var formshhsTbl = $j('#formshhstab').dataTable({
			 "sScrollY": "150",
	            "sScrollX": "100%",
	            "sAjaxSource": 'getJsonPagination.action?TblFind=hhsInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#hhsinfo_tbl_param').val(),
	            "bServerSide": true,
	            "bProcessing": true, 
	     		"bRetrieve" : true,
	     		"oSearch": {"sSearch": filter_value},
				"bDestroy": true,
				"bSortCellsTop": true,
	     		"aaSorting": [[ sort, sortDir ]],
	     		"bDeferRender": true,
	     		"sAjaxDataProp": "aaData",
	     		"aoColumnDefs": [
	     		                {		
	  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
	  		                	   return source[1];
	  		                  }},
	  		                  {
	  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
								return source[2];
			                  }},
			                  {
			                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
	  		                	 	return source[3];
	  		                  }},
	  		                  {		
	  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
	  		                	 	return source[4];
	  	                	   }}
	  						],
	  						"fnDrawCallback": function() { 
	  							fnHhsInfoTableSort('hhsInfoTr');
	  						},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
						"aButtons": [
										{
											"sExtends": "xls",
											"sButtonText": "Export",
												"sFileName": "Health History Screening Audit Trail.csv"  
										}
									]
					},
			});
		 formshhsTbl.fnAdjustColumnSizing();
	    }else if(((isPrint=="1" || isPrint=="2") && $j('#formshhsspan').is(".ui-icon-plusthick"))){
			 var formshhsTbl = $j('#formshhstab').dataTable({
				 
		            "sAjaxSource": 'getJsonPagination.action?TblFind=hhsInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#hhsinfo_tbl_param').val(),
		            "bServerSide": true,
		            "bProcessing": true, 
		     		"bRetrieve" : true,
		     		"oSearch": {"sSearch": filter_value},
					"bDestroy": true,
					"bSortCellsTop": true,
		     		"aaSorting": [[ sort, sortDir ]],
		     		"bDeferRender": true,
		     		"sAjaxDataProp": "aaData",
		     		"aoColumnDefs": [
		     		                {		
		  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		  		                	   return source[1];
		  		                  }},
		  		                  {
		  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
									return source[2];
				                  }},
				                  {
				                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		  		                	 	return source[3];
		  		                  }},
		  		                  {		
		  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		  		                	 	return source[4];
		  	                	   }}
		  						],
		  						"fnDrawCallback": function() { 
		  							fnHhsInfoTableSort('hhsInfoTr');
		  						},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
							"aButtons": [
											{
												"sExtends": "xls",
												"sButtonText": "Export",
													"sFileName": "Health History Screening Audit Trail.csv"  
											}
										]
						},
				});
		    }

	 $j('#formshhstab_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#hhs_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,formshhsResults,temp,trailForm,formshhsDiv,formshhsOnload,cordID,entityId,formshhs,hhsinfo_tbl_param");
		  }
	});
	 
	 var usedNames = {};
		$j("select[name='formname2'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='fversion2'] > option").each(function () {
		    if(usedNames[this.text] ||  $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='fuserid2'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});

		var	formname =$j('#formname2').val();
		var	fversion =$j('#fversion2').val();
		var	userid =$j('#fuserid2').val();
		var	pStartDateD1=$j('#dateStart13').val();
		var	pStartDateD2=$j('#dateEnd13').val();

	$j('#resetHHS').click(function(){
		formshhsTbl.fnSort( [ [3,'desc'] ] );
		formshhsTbl.fnDraw();
		 });
	
	$j('#formshhstab_info').hide();
	$j('#formshhstab_paginate').hide();
	$j('#formshhstab_length').empty();
	$j('#formshhstab_length').replaceWith(showEntriesTxt);
	if($j('#formshhsentries').val()!=null || $j('#formshhsentries').val()!=""){
		$j('#formshhsResults').val($j('#formshhsentries').val());
		}
	$j('#formshhsResults').change(function(){
		var textval=$j('#formshhsResults :selected').html();
		$j('#hhsinfo_tbl_sentries').val(textval);
		var formname=$j('#formname2').val();
		var fversion=$j('#fversion2').val();
		var userid=$j('#fuserid2').val();
		var pStartDateD1=$j('#dateStart13').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd13').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,formshhsResults,temp,trailForm,formshhsDiv,formshhsOnload,cordID,entityId,formshhs,hhsinfo_tbl_param");
		$j('#formname2').val(formname);
		$j('#fversion2').val(fversion);
		$j('#fuserid2').val(userid);
		$j('#dateStart13').val(pStartDtD1);
		$j('#dateEnd13').val(pStartDtD2);
	});	
	var wrapperWidth=$j('#formshssaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#formshhstab').width(newWidth);
	var frmnme = $j('#frmnme2').val();
	var fversn = $j('#fversn2').val();
	var usid = $j('#usid2').val();
	$j('#formname2').val(frmnme);
	$j('#fversion2').val(fversn);
	$j('#fuserid2').val(usid);
	if(((isPrint=="1" || isPrint=="2") && $j('#formshhsspan').is(".ui-icon-plusthick"))){
		flag12=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('formshssaudittrail','headerPrnt');
			},500);
		}
	}
}

function bestHlaOnload(){
	$j('#widgetPrintDiv').empty();
	if($j('#besthlaspan').is(".ui-icon-plusthick")) {
		if(flag10==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=besthla','bestHlaDiv','trailForm');
			flag10=1;
		}
	}
	if($j('#besthlaspan').is(".ui-icon-minusthick")) {
		$j('#bestHlaDiv').show();
	}
	

	var bestHlaTbl = $j('#besthlatab').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
		},
		"bRetrieve": true,
		"bDestroy" :true,	
		"iDisplayLength":1000,"aaSorting": [[ 2, "desc" ]],
		"bSortCellsTop": true	
	});

	$j("#bestHlaHead tr").each( function ( i ) {
		if(i!=0){
			$j(this).find('th').each ( function(j) {
				this.innerHTML = fnCreateSelectAudit( bestHlaTbl.fnGetColumnData(j),j,'besthlafilter');
				$j('select', this).change( function () {
					bestHlaTbl.fnFilter( $j(this).val(), j );
				});
				
			});
		}
});
	$j('#besthlatab_info').hide();
	$j('#besthlatab_paginate').hide();
	$j('#besthlatab_length').empty();
	$j('#besthlatab_length').replaceWith('Show <select name="bestHlaResults" id="bestHlaResults" style="width:50px; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="2000"/>">All</option></select> Entries');
	if($j('#besthlaentries').val()!=null || $j('#besthlaentries').val()!=""){
		$j('#bestHlaResults').val($j('#besthlaentries').val());
		}
	$j('#bestHlaResults').change(function(){
		paginationFooter(0,"revokedUnitReportsWidget,bestHlaResults,temp,trailForm,bestHlaDiv,bestHlaOnload,cordID,entityId,besthla");
	});	
	var wrapperWidth=$j('#besthlaaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#besthlatab').width(newWidth);
	
}

function pfOnload(isPrint){
  
	$j('#widgetPrintDiv').empty();
	
	if((isPrint=="1" && $j('#pfspan').is(".ui-icon-minusthick"))){
	         
		        refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=pf','pfDiv','trailForm');
	}
	if($j('#pfspan').is(".ui-icon-plusthick")) {
		
		    
			if(isPrint=="1"){
			  
			    refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=pf','pfDiv','trailForm');
			}
			else{
			  
		
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=pf'+'&flag='+"1",'pfDiv','trailForm');
			}
			
		
	}
	if($j('#pfspan').is(".ui-icon-minusthick")) {
		$j('#pfDiv').show();
	}


	var is_all_entries=$j('#pfResults').val();
	var pageNo=$j('#paginateWrapper31').find('.currentPage').text();
	var showEntries=$j('#orinfo_tbl_sentries').val();
	var tEntries=$j('#tentriesOr').val();
	var sort=2;
	var sortDir="desc";
	var SortParm=$j('#orinfo_tbl_sort_col').val();
	var filter_value=$j('#or_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#orinfo_tbl_sort_col').val();
		sortDir=$j('#orinfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#orinfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#orinfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="pfResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';

	 if(showEntries=="ALL"){
	  

	var pfTbl = $j('#pftab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=orderInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#orinfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
    		                	return source[5];
		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
  		                	return source[6];
		                   }}
						],

						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#pftab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#pftab_info').hide();
						    $j("#pfResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnIdInfoTableSort('orInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Order Details Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues11').html(temptxt);
	           return temptxt;
	         
	         }					
	});

	
	$j('#pftab_info').after('<div id="entriesvalues11"><div>');
   	//var temptxt='Showing 1 to '+$j('#tentriesOr').val()+' of '+$j('#tentriesOr').val()+' entries';
   	//$j('#entriesvalues11').html(temptxt);	

	 }else if(isPrint!="1" && isPrint!="2"){
           
		 var pfTbl = $j('#pftab').dataTable({

			 "sScrollY": "150",
	            "sScrollX": "100%",
	            "sAjaxSource": 'getJsonPagination.action?TblFind=orderInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#orinfo_tbl_param').val(),
	            "bServerSide": true,
	            "bProcessing": true, 
	     		"bRetrieve" : true,
	     		"oSearch": {"sSearch": filter_value},
				"bDestroy": true,
				"bSortCellsTop": true,
	     		"aaSorting": [[ sort, sortDir ]],
	     		"bDeferRender": true,
	     		"sAjaxDataProp": "aaData",
	     		"aoColumnDefs": [
	     		                {		
	  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
	  		                	   return source[1];
	  		                  }},
	  		                  {
	  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
								return source[2];
			                  }},
			                  {
			                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
	  		                	 	return source[3];
	  		                  }},
	  		                  {		
	  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
	  		                	 	return source[4];
	  	                	   }},
	  		                   { 	
	  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
	      		                	return source[5];
	  		                   }}      	 ,
			                   { 	
		                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
	    		                	return source[6];
			                   }}
	  						],
	  						"fnDrawCallback": function() { 
	  							fnOrInfoTableSort('orInfoTr');
	  						},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
						"aButtons": [
										{
											"sExtends": "xls",
											"sButtonText": "Export",
												"sFileName": "Order Details Audit Trail.csv"  
										}
									]
					}	});
		 pfTbl.fnAdjustColumnSizing();
	    }else if(((isPrint=="1" || isPrint=="2") && $j('#pfspan').is(".ui-icon-plusthick"))){
           
	   var pfTbl = $j('#pftab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=orderInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#orinfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
    		                	return source[5];
		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
  		                	return source[6];
		                   }}
						],

						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#pftab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#pftab_info').hide();
						    $j("#pfResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnIdInfoTableSort('orInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Order Details Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues11').html(temptxt);
	           return temptxt;
	         
	         }					
	});

	
	//$j('#pftab_info').after('<div id="entriesvalues11"><div>');
   	//var temptxt='Showing 1 to '+$j('#tentriesOr').val()+' of '+$j('#tentriesOr').val()+' entries';
   	//$j('#entriesvalues11').html(temptxt);
		    	
		    }
			
			
	else if(((isPrint=="1" || isPrint=="2") && $j('#pfspan').is(".ui-icon-minusthick"))){
                
		
	var pfTbl = $j('#pftab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=orderInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#orinfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
    		                	return source[5];
		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
  		                	return source[6];
		                   }}
						],

						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#pftab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#pftab_info').hide();
						    $j("#pfResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnIdInfoTableSort('orInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Order Details Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues11').html(temptxt);
	           return temptxt;
	         
	         }					
	});

	
	//$j('#pftab_info').after('<div id="entriesvalues11"><div>');
   	//var temptxt='Showing 1 to '+$j('#tentriesOr').val()+' of '+$j('#tentriesOr').val()+' entries';
   	//$j('#entriesvalues11').html(temptxt);
		    	
		    }
		
		 $j('#pftab_filter').find(".searching1").keyup(function(e){ 
			  var val = $j(this).val();
			  val=$j.trim(val);
			  $j('#or_audit_fval').val(val);
			  if(e.keyCode==13)
			  {
				  paginationFooter(0,"revokedUnitReportsWidget,pfResults,temp,trailForm,pfDiv,pfOnload,cordID,entityId,pf,orinfo_tbl_param");
			  }
		});
		 var usedNames = {};
			$j("select[name='fieldName11'] > option").each(function () {
			    if(usedNames[this.text] || $j(this).val() == "" ) {
			        $j(this).remove();
			    } else {
			        usedNames[this.text] = this.value;
			    }
			});
			var usedNames = {};
			$j("select[name='userId11'] > option").each(function () {
			    if(usedNames[this.text] ||  $j(this).val() == "" ) {
			        $j(this).remove();
			    } else {
			        usedNames[this.text] = this.value;
			    }
			});
			var usedNames = {};
			$j("select[name='actTaken11'] > option").each(function () {
			    if(usedNames[this.text] || $j(this).val() == "" ) {
			        $j(this).remove();
			    } else {
			        usedNames[this.text] = this.value;
			    }
			});
			var usedNames = {};
			$j("select[name='prevValue11'] > option").each(function () {
			    if(usedNames[this.text] || $j(this).val() == "" ) {
			        $j(this).remove();
			    } else {
			        usedNames[this.text] = this.value;
			    }
			});
			var usedNames = {};
			$j("select[name='newValue11'] > option").each(function () {
			    if(usedNames[this.text] || $j(this).val() == "" ) {
			        $j(this).remove();
			    } else {
			        usedNames[this.text] = this.value;
			    }
			});
			
			var	fieldName=$j('#fieldName11').val();
			var	userId=$j('#userId11').val();
			var	actTaken=$j('#actTaken11').val();
			var	prevValue=$j('#prevValue11').val();
			var	newValue=$j('#newValue11').val();
			var	pStartDateD1=$j('#dateStart11').val();
			var	pStartDateD2=$j('#dateEnd11').val();

			$j('#resetOrder').click(function(){
				pfTbl.fnSort( [ [2,'desc'] ] );
				pfTbl.fnDraw();
				 });
		 
						
	
	$j('#pftab_info').hide();
	$j('#pftab_paginate').hide();
	$j('#pftab_length').empty();
	$j('#pftab_length').replaceWith(showEntriesTxt);
	if($j('#pfentries').val()!=null || $j('#pfentries').val()!=""){
		$j('#pfResults').val($j('#pfentries').val());
		}
	$j('#pfResults').change(function(){
		var textval=$j('#pfResults :selected').html();
		$j('#orinfo_tbl_sentries').val(textval);
		var fieldName=$j('#fieldName11').val();
		var userId=$j('#userId11').val();
		var actTaken=$j('#actTaken11').val();
		var prevValue=$j('#prevValue11').val();
		var newValue=$j('#newValue11').val();
		var pStartDateD1=$j('#dateStart11').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd11').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,pfResults,temp,trailForm,pfDiv,pfOnload,cordID,entityId,pf,orinfo_tbl_param");
	});	
	var wrapperWidth=$j('#pfaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#pftab').width(newWidth);
	var fldNme = $j('#fldNme11').val();
	var usrId = $j('#usrId11').val();
	var actTkn = $j('#actTkn11').val();
	var prvVl = $j('#prvVl11').val();
	var nwVl = $j('#nwVl11').val();

	$j('#fieldName11').val(fldNme);
	$j('#userId11').val(usrId);
	$j('#actTaken11').val(actTkn);
	$j('#prevValue11').val(prvVl);
	$j('#newValue11').val(nwVl);
	if(((isPrint=="1" || isPrint=="2") && $j('#pfspan').is(".ui-icon-minusthick")) ){
	    
		flag11=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('pfaudittrail','headerPrnt');
			},600);
		}
	}
	
	if(((isPrint==1 || isPrint==2) && $j('#pfspan').is(".ui-icon-plusthick")) ){
	    
		flag11=0;
		if(isPrint==1){
			setTimeout(function(){
				clickheretoprintaudit('pfaudittrail','headerPrnt');
			},600);
		}
	}
}

function mcOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#mcspan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('mcaudittrail','headerPrnt');
		},500);
		return false;
	}
	if($j('#mcspan').is(".ui-icon-plusthick")) {
		if(flag15==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=mc','mcDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag15=1;
		}
	}
	if($j('#mcspan').is(".ui-icon-minusthick")) {
		$j('#mcDiv').show();
	}
	var is_all_entries=$j('#mcResults').val();
	var pageNo=$j('#paginateWrapper30').find('.currentPage').text();
	var showEntries=$j('#mcinfo_tbl_sentries').val();
	var tEntries=$j('#tentriesMc').val();
	var sort=2;
	var sortDir="desc";
	var SortParm=$j('#mcinfo_tbl_sort_col').val();
	var filter_value=$j('#mc_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#mcinfo_tbl_sort_col').val();
		sortDir=$j('#mcinfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#mcinfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#mcinfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="mcResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';

	 if(showEntries=="ALL"){

	var mcTbl = $j('#mctab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=mcInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#mcinfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
    		                	return source[5];
		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
  		                	return source[6];
		                   }}
						],

						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#mctab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#mctab_info').hide();
						    $j("#mcResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnMcInfoTableSort('mcInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Minimum Criteria Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues10').html(temptxt);
	           return temptxt;
	         
	         }					
	});
	$j('#mctab_info').after('<div id="entriesvalues10"><div>');
   	//var temptxt='Showing 1 to '+$j('#tentriesMc').val()+' of '+$j('#tentriesMc').val()+' entries';
   	//$j('#entriesvalues10').html(temptxt);	

	 }else if(isPrint!="1" && isPrint!="2"){

		 var mcTbl = $j('#mctab').dataTable({
			 "sScrollY": "150",
	            "sScrollX": "100%",
	            "sAjaxSource": 'getJsonPagination.action?TblFind=mcInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#mcinfo_tbl_param').val(),
	            "bServerSide": true,
	            "bProcessing": true, 
	     		"bRetrieve" : true,
	     		"oSearch": {"sSearch": filter_value},
				"bDestroy": true,
				"bSortCellsTop": true,
	     		"aaSorting": [[ sort, sortDir ]],
	     		"bDeferRender": true,
	     		"sAjaxDataProp": "aaData",
	     		"aoColumnDefs": [
	     		                {		
	  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
	  		                	   return source[1];
	  		                  }},
	  		                  {
	  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
								return source[2];
			                  }},
			                  {
			                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
	  		                	 	return source[3];
	  		                  }},
	  		                  {		
	  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
	  		                	 	return source[4];
	  	                	   }},
	  		                   { 	
	  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
	      		                	return source[5];
	  		                   }}      	 ,
			                   { 	
		                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
	    		                	return source[6];
			                   }}
	  						],
	  						"fnDrawCallback": function() { 
	  							fnMcInfoTableSort('mcInfoTr');
	  						},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
						"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
						"aButtons": [
										{
											"sExtends": "xls",
											"sButtonText": "Export",
												"sFileName": "Minimum Criteria Audit Trail.csv"  
										}
									]
					}
		 });
		 mcTbl.fnAdjustColumnSizing();	
	    }else if(((isPrint=="1" || isPrint=="2") && $j('#mcspan').is(".ui-icon-plusthick"))){

			 var mcTbl = $j('#mctab').dataTable({
				 
		            "sAjaxSource": 'getJsonPagination.action?TblFind=mcInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#mcinfo_tbl_param').val(),
		            "bServerSide": true,
		            "bProcessing": true, 
		     		"bRetrieve" : true,
		     		"oSearch": {"sSearch": filter_value},
					"bDestroy": true,
					"bSortCellsTop": true,
		     		"aaSorting": [[ sort, sortDir ]],
		     		"bDeferRender": true,
		     		"sAjaxDataProp": "aaData",
		     		"aoColumnDefs": [
		     		                {		
		  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		  		                	   return source[1];
		  		                  }},
		  		                  {
		  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
									return source[2];
				                  }},
				                  {
				                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		  		                	 	return source[3];
		  		                  }},
		  		                  {		
		  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		  		                	 	return source[4];
		  	                	   }},
		  		                   { 	
		  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
		      		                	return source[5];
		  		                   }}      	 ,
				                   { 	
			                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
		    		                	return source[6];
				                   }}
		  						],
		  						"fnDrawCallback": function() { 
		  							fnMcInfoTableSort('mcInfoTr');
		  						},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
							"aButtons": [
											{
												"sExtends": "xls",
												"sButtonText": "Export",
													"sFileName": "Minimum Criteria Audit Trail.csv"  
											}
										]
						}
			 });
		    	
		    }
	 $j('#mctab_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#mc_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,mcResults,temp,trailForm,mcDiv,mcOnload,cordID,entityId,mc,mcinfo_tbl_param");
		  }
	});

	 var usedNames = {};
		$j("select[name='fieldName10'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='userId10'] > option").each(function () {
		    if(usedNames[this.text] ||  $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='actTaken10'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='prevValue10'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='newValue10'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		
		var	fieldName=$j('#fieldName10').val();
		var	userId=$j('#userId10').val();
		var	actTaken=$j('#actTaken10').val();
		var	prevValue=$j('#prevValue10').val();
		var	newValue=$j('#newValue10').val();
		var	pStartDateD1=$j('#dateStart10').val();
		var	pStartDateD2=$j('#dateEnd10').val();

	$j('#resetMC').click(function(){
		mcTbl.fnSort( [ [2,'desc'] ] );
		mcTbl.fnDraw();
		 });
	
	$j('#mctab_info').hide();
	$j('#mctab_paginate').hide();
	$j('#mctab_length').empty();
	$j('#mctab_length').replaceWith(showEntriesTxt);
	if($j('#mcentries').val()!=null || $j('#mcentries').val()!=""){
		$j('#mcResults').val($j('#mcentries').val());
		}
	$j('#mcResults').change(function(){
		var textval=$j('#mcResults :selected').html();
		$j('#mcinfo_tbl_sentries').val(textval);
		var fieldName=$j('#fieldName10').val();
		var userId=$j('#userId10').val();
		var actTaken=$j('#actTaken10').val();
		var prevValue=$j('#prevValue10').val();
		var newValue=$j('#newValue10').val();
		var pStartDateD1=$j('#dateStart10').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd10').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,mcResults,temp,trailForm,mcDiv,mcOnload,cordID,entityId,mc,mcinfo_tbl_param");
		$j('#fieldName10').val(fieldName);
		$j('#userId10').val(userId);
		$j('#actTaken10').val(actTaken);
		$j('#prevValue10').val(prevValue);
		$j('#newValue10').val(newValue);
		$j('#dateStart10').val(pStartDtD1);
		$j('#dateEnd10').val(pStartDtD2);
	});	
	var wrapperWidth=$j('#mcaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#mctab').width(newWidth);
	var fldNme = $j('#fldNme10').val();
	var usrId = $j('#usrId10').val();
	var actTkn = $j('#actTkn10').val();
	var prvVl = $j('#prvVl10').val();
	var nwVl = $j('#nwVl10').val();

	$j('#fieldName10').val(fldNme);
	$j('#userId10').val(usrId);
	$j('#actTaken10').val(actTkn);
	$j('#prevValue10').val(prvVl);
	$j('#newValue10').val(nwVl);
	if(((isPrint=="1" || isPrint=="2") && $j('#mcspan').is(".ui-icon-plusthick"))){
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('mcaudittrail','headerPrnt');
			},500);
		}
	}
}

function showModalA(title,url,height,width)
{
	$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/><img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#modelPopup2").html(progressMsg);
		$j("#modelPopup2").dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#modelPopup2").dialog("destroy");
				    }
				   }
				  ); 
		$j("#modelPopup2").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#modelPopup2").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
}

function showDocument(AttachmentId){
	var cotextpath = "<%=contextpath%>";
	var signed='true';
	var url='getAttachmentFromDcms?docId='+AttachmentId+'&signed='+signed;
	window.open( url, "Attachment", "status = 1, height = 500, width = 500, resizable = 0" );

}

var headerPrt = $j('#headerPrnt').html();

function clickheretoprintall(){
	setTimeout(function(){
		showprogressMgs();
	},0);
	setTimeout(function(){
		docHistoryOnload('2');
		searchReasultOnload('2');
		idOnload('2');
		procInfoOnload('2');
		entityStatusOnload('2');
		multValuesOnload('2');
		reviewOnload('2'); 
		hlaOnload('2');
		pathlaOnload('2');
		labOnload('2'); 
		notesOnload('2');
		formsOnload('2');
		formshhsOnload('2');
		pfOnload('2');
		mcOnload('2');  
		setTimeout(function(){
			clickheretoprint('auditprnt');
		},500);
	},100);
	setTimeout(function(){
		closeprogressMsg();
	},0);
}


</script>