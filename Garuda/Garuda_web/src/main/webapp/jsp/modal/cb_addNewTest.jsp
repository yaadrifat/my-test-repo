<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script>
$j(".positive").numeric({ negative: false }, function() { alert("<s:text name="garuda.common.cbu.negativeValueAlert"/>"); this.value = ""; this.focus(); });

$j(function(){
		getDatePic();		
		jQuery('#datepicker21').datepicker('option', { beforeShow: customRange }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
});

function customRange(input) {
	var today = new Date();
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();
	var h=today.getHours();
	var mn=today.getMinutes()+1;
	var mindate=$j("#specCollDate1").val();
	var mindate1=mindate.split("-");
	if($j("#datepicker4").val()!==undefined){
  if (input.id == 'datepicker21' && $j("#datepicker4").val()!=='' && $j("#datepicker4").val()!=null) {
    /*return {
    	 minDate: new Date(mindate1[0],mindate1[1]-1,++mindate1[2])
    };*/
   // $j('#datepicker21').datepicker( "option", "minDate", new Date(mindate1[0],mindate1[1]-1,mindate1[2]));
	  
		  //  return {
		    	// minDate: new Date(mindate1[0],mindate1[1]-1,++mindate1[2])
		  		//$j('#datepicker91').datepicker( "option", "minDate", new Date(mindate1[0],mindate1[1]-1,mindate1[2]));
		  		var maxTestDate=new Date($j("#datepicker4").val());// processing start date
		        var d2 = maxTestDate.getDate();
		        var m2 = maxTestDate.getMonth();
		        var y2 = maxTestDate.getFullYear();
		        $j('#'+input.id).datepicker( "option", "minDate", new Date(y2,m2,d2));
		    //};
		  } else if(input.id == 'datepicker21' && $j("#datepicker4").val()==='' && $j("#datepicker2").val()!=='' && $j("#datepicker2").val()!=null){// processing start date is null but collection is available
			    var maxTestDate=new Date($j("#datepicker2").val());// collection date
				var d2 = maxTestDate.getDate();
				var m2 = maxTestDate.getMonth();
				var y2 = maxTestDate.getFullYear();
				$j('#'+input.id).datepicker( "option", "minDate", new Date(y2,m2,d2));
				$j('#'+input.id).datepicker( "option", "maxDate", new Date());
			}
	}else if(input.id == 'datepicker21' && $j("#processingDateId").val()!=''){// for view clinical screen block
		 var maxTestDate=new Date($j("#processingDateId").val());// processing start date
	      var d2 = maxTestDate.getDate();
	      var m2 = maxTestDate.getMonth();
	      var y2 = maxTestDate.getFullYear();
	      $j('#'+input.id).datepicker( "option", "minDate", new Date(y2,m2,d2));
	}else if(input.id == 'datepicker91' && $j("#processingDateId").val()==='' && $j("#datepickerbirthlabM").val()!=='' && $j("#datepickerbirthlabM").val()!=null){// processing start date is null but collection is available
	    var maxTestDate=new Date($j("#datepickerbirthlabM").val());// collection date
		var d2 = maxTestDate.getDate();
		var m2 = maxTestDate.getMonth();
		var y2 = maxTestDate.getFullYear();
		$j('#'+input.id).datepicker( "option", "minDate", new Date(y2,m2,d2));
		$j('#'+input.id).datepicker( "option", "maxDate", new Date());
	}
  
}
	
$j(function(){
 $j("#addnewtestform").validate({
	
	rules:{	
			/* "labtestlist1":{checkselect : true}, */
			"timingOfTest":{checkselect : true},
			"patLabsPojo.testDescription":{required : true,maxlength:50},
			"patLabsPojo.fktestspecimen":{checkselect : true},
			"patLabsPojo.testresult":{required : true,maxlength:30},
			 "patLabsPojo.testdate":{
											required:{
														depends: function(element) {
																	
																	var listValues = $j("#timingOfTest").val();
																	var lstTostrng=listValues.toString();
																	var listValues1=lstTostrng.split(",");
																	var compareVal=$j("#newTestDatePkVal").val();
																	var count=0;
																	for(count=0; count<listValues1.length; count++){
																		if(compareVal==listValues1[count]){
																			return true;
																		}
																	}
																	return false;
																 }
															},dpDate:true
													}  
			
		},
		messages:{
			/* "labtestlist1":"<s:text name="garuda.cbu.labtest.testname"/>", */
			"timingOfTest":"<s:text name="garuda.cbu.labtest.testtiming"/>",
			"patLabsPojo.testDescription":{
         		required : "<s:text name="garuda.cbu.labtest.testdesc"/>",
         		maxlength : "<s:text name="garuda.cbu.labtest.testdescmaxlen"/>"
				},
			"patLabsPojo.fktestspecimen":"<s:text name="garuda.cbu.labtest.testspecimen"/>",
			"patLabsPojo.testresult":{
         		required : "<s:text name="garuda.cbu.labtest.testresult"/>",
         		maxlength : "<s:text name="garuda.cbu.labtest.testresultmaxlen"/>"
				},
			 "patLabsPojo.testdate":{required:"<s:text name="garuda.cbu.test.testStartDate"/>"}  
				}
						
		});
});

 function addLabnewtest(fkSpecimenId){
	 	var fkSpecimenId=$j('#fkSpecimenId').val();
		//if($j('#newTestSize').val()<6 || $j('#newTestSize').val()==6){ 
		showModal('Add LabTest','addLabOthertest?fkSpecimenId='+fkSpecimenId,'350','500');
	 //} 
} 
 /* function showDataModal(title,url,height,width,id)
 {
 	var id1 = "#" + id;
 	var title1 = title ;
 	if(height==null || height==""){
 		height = 650;
 	}
 	if(width==null || width==""){
 		width = 750;
 	}

 		$j(id1).dialog(
 				   {autoOpen: false,
 					title: title1  ,
 					resizable: false,
 					closeText: '',
 					closeOnEscape: false ,
 					modal: true, width : width, height : height,
 					close: function() {
 						//$(".ui-dialog-content").html("");
 						//jQuery("#subeditpop").attr("id","subeditpop_old");
 		      		jQuery(id1).dialog("destroy");
 				    }
 				   }
 				  ); 
 		$j(id1).dialog("open");
 	$j.ajax({
         type: "POST",
         url: url,
        // async:false,
         success: function (result, status, error){
 	       	$j(id1).html(result);
         	       	
         },
         error: function (request, status, error) {
         	alert("Error " + error);
             alert(request.responseText);
         }

 	});
 	
 } */

  function showaddTest(title,url,height,width,divId){
	var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/> <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	$j("#showaddtestmodal").html(progressMsg);
		$j("#showaddtestmodal").dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
		      		jQuery("#showaddtestmodal").dialog("destroy");
				    }
				   }
				  ); 
		$j("#showaddtestmodal").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
        data:$j('#'+divId+' *').serialize(),
        success: function (result, status, error){
	       	$j("#showaddtestmodal").html(result);
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
}  

 function addNewTest(specId,cdrCbuId,timingoftestlnght,removeUnknownTestFlag){
	 timingoftestlnght=$j("#timingOfTestHdrLnght").val();
	 if($j("#addnewtestform").valid()){
		 
		 	var listValues = $j("#timingOfTest").val();
			var lstTostrng=listValues.toString();
			var listValues1=lstTostrng.split(",");
			var compareVal=$j("#newTestDatePkVal").val();
			var count=0;
			var newTestDatePrst=0;
			for(count=0; count<listValues1.length; count++){
				if(compareVal==listValues1[count]){
					newTestDatePrst=1;
				}
			}
			/*  var url = "newAddNewTest?specimenId="+specimenId;
		 modalFormSubmitRefreshDiv('addnewtestform',url,'addnewtestdiv'); */
		/* loadDivWithFormSubmit(url,'addnewtestdiv','addnewtestform')  */
		//loadDivWithFormSubmit('newAddNewTest','otherLabsTests','addnewtestform');
		 if(timingoftestlnght>8 && newTestDatePrst==1){
			 alert("You crossed the maximum number of count please deselect new test date");
		 }else{
			 /*var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/><img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
				var processingSaveDialog = document.createElement('div');
				processingSaveDialog.innerHTML=progressMsg;		
				$j(processingSaveDialog).dialog({autoOpen: false,
					resizable: false, width:400, height:90,closeOnEscape: false,
					modal: true}).siblings('.ui-dialog-titlebar').remove();		
				$j(processingSaveDialog).dialog("open");*/
			showprogressMgs();
		   $j.ajax({
				type: "POST",
				url : "newAddNewTest?specId="+specId+"&timingoftestlnght="+timingoftestlnght+"&removeUnknownTestFlag="+removeUnknownTestFlag,
				data : $j("#addnewtestform").serialize(),
				success : function(result) {
					//$j(processingSaveDialog).dialog("close");
					$j("#labSummeryDivR").html(result);
					$j("#updateAddTest").css('display','block');
	            	$j("#statusAddTest").css('display','none');
	            	
				},
				error:function() { 
					alert("Error ");
					closeprogressMsg();	
					alert(request.responseText);
				}
				
				});	     
				closeprogressMsg();
				if($j("#pkViaOther").val()==$j("#viabSelected").val()){
					 $j("#viability").show();
					}else{
						$j("#viability").hide();
					}
				 if($j("#pkOther").val()==$j("#cfuSelected").val()){
					$j("#cfuCount").show();
				   }else{
					$j("#cfuCount").hide();
					  }
	}}
 }
 function addNewTestLab(specId,cdrCbuId,timingoftestlnght,removeUnknownTestFlag){
	 timingoftestlnght=$j("#timingOfTestHdrLnght").val();
	 if($j("#addnewtestform").valid()){
		 
		 var listValues = $j("#timingOfTest").val();
			var lstTostrng=listValues.toString();
			var listValues1=lstTostrng.split(",");
			var compareVal=$j("#newTestDatePkVal").val();
			var count=0;
			var newTestDatePrst=0;
			for(count=0; count<listValues1.length; count++){
				if(compareVal==listValues1[count]){
					newTestDatePrst=1;
				}
			}
		 /*  var url = "newAddNewTest?specimenId="+specimenId;
		 modalFormSubmitRefreshDiv('addnewtestform',url,'addnewtestdiv'); */
		/* loadDivWithFormSubmit(url,'addnewtestdiv','addnewtestform')  */
		//loadDivWithFormSubmit('newAddNewTest','otherLabsTests','addnewtestform');
		 if(timingoftestlnght>=8 && newTestDatePrst==1){
			 alert("You crossed the maximum number of count please deselect new test date");
		 }
		 else{
		 /*var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/><img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
			var processingSaveDialog = document.createElement('div');
			processingSaveDialog.innerHTML=progressMsg;		
			$j(processingSaveDialog).dialog({autoOpen: false,
				resizable: false, width:400, height:90,closeOnEscape: false,
				modal: true}).siblings('.ui-dialog-titlebar').remove();		
			$j(processingSaveDialog).dialog("open");  */
			showprogressMgs();
		 $j.ajax({
				type: "POST",
				url : "newAddNewTestLab?specId="+specId+"&timingoftestlnght="+timingoftestlnght+"&removeUnknownTestFlag="+removeUnknownTestFlag,
				data : $j("#addnewtestform").serialize(),
				success : function(result) {
					$j("#divlabtestupdateform").html(result);
					$j("#labSumm_modeltestDiv").prepend('<input type="text"  style="display:none;" Class="labChanges"/>');
					$j("#updateAddTest").css('display','block');
	            	$j("#statusAddTest").css('display','none');
	            	
				},
				error:function() {
					closeprogressMsg(); 
					alert("Error ");
					alert(request.responseText);
				}
				
				});	     
		 closeprogressMsg();
	 }}
	 
 }
 
 function addNewTestComp(specId,cdrCbuId,timingoftestlnght,removeUnknownTestFlag){
	 timingoftestlnght=$j("#timingOfTestHdrLnght").val();
	 if($j("#addnewtestform").valid()){
		 
			var listValues = $j("#timingOfTest").val();
			var lstTostrng=listValues.toString();
			var listValues1=lstTostrng.split(",");
			var compareVal=$j("#newTestDatePkVal").val();
			var count=0;
			var newTestDatePrst=0;
			for(count=0; count<listValues1.length; count++){
				if(compareVal==listValues1[count]){
					newTestDatePrst=1;
				}
			}
		 /*  var url = "newAddNewTest?specimenId="+specimenId;
		 modalFormSubmitRefreshDiv('addnewtestform',url,'addnewtestdiv'); */
		/* loadDivWithFormSubmit(url,'addnewtestdiv','addnewtestform')  */
		//loadDivWithFormSubmit('newAddNewTest','otherLabsTests','addnewtestform');
		 if(timingoftestlnght>=8 && newTestDatePrst==1){
			 alert("You crossed the maximum number of count please deselect new test date");
		 }
		 else{
		 /*var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/><img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
			var processingSaveDialog = document.createElement('div');
			processingSaveDialog.innerHTML=progressMsg;		
			$j(processingSaveDialog).dialog({autoOpen: false,
				resizable: false, width:400, height:90,closeOnEscape: false,
				modal: true}).siblings('.ui-dialog-titlebar').remove();		
			$j(processingSaveDialog).dialog("open");  */
			showprogressMgs();
		 $j.ajax({
				type: "POST",
				url : "addNewTestComp?specId="+specId+"&timingoftestlnght="+timingoftestlnght+"&removeUnknownTestFlag="+removeUnknownTestFlag,
				data : $j("#addnewtestform").serialize(),
				success : function(result) {
					//alert(result);
					$j("#labSummeryDivR1").html(result);
					$j("#labSummeryDivR1").prepend('<input type="text"  style="display:none;" Class="labChanges"/>');
					$j("#updateAddTest").css('display','block');
	            	$j("#statusAddTest").css('display','none');
	            	
				},
				error:function() {
					closeprogressMsg(); 
					alert("Error ");
					alert(request.responseText);
				}
				
				});	     
		 closeprogressMsg();
	 }}
	 
 }
 
 
 function closeModalsForTest(divname){
		$j("#"+divname).dialog("close");
		 $j("#"+divname).dialog("destroy");
		 $j("#"+divname).html("");
	}
 
 function resetTimings(testId){
	 	
	 	loadPageByGetRequset('resetTimings?cordId=<s:property value="cdrCbuPojo.cordID"/>&specimenId=<s:property value="cdrCbuPojo.fkSpecimenId"/>&cbuOtherTestFlag=False&testId='+testId+'&timingTstDatesArr='+$j("#timingTstDatesArr").val()+'&otherLabTstsLstId='+$j("#otherLabTstsLstId").val(),'timingOfTstsDiv');
	  }
 
 function changeTestDateDivSts(){
		var listValues=$j("#timingOfTest").val();
		var lstTostrng=listValues.toString();
		var listValues1=lstTostrng.split(",");
		var compareVal=$j("#newTestDatePkVal").val();
		var count=0;
		for(count=0; count<listValues1.length; count++){
			if(compareVal==listValues1[count]){
				$j("#tststrdate").show();
				//break;
			}
			else{
				$j("#tststrdate").hide();
			}
		}
		
	}
   
</script>
<s:hidden name="newTestDatePkVal" id="newTestDatePkVal" />
<s:hidden name="timingOfTestHdrLnght" id="timingOfTestHdrLnght" />
<div id="updateAddTest" style="display: none;" >
	  
	  	<%-- <s:if test="#request.editTestFlag=='True'">
	  	<jsp:include page="../cb_update.jsp">
	    <jsp:param value="garuda.common.lable.updateaddnewtest"  name="message" />
	    </jsp:include>
	    </s:if>
	    <s:else>
	    <jsp:include page="../cb_update.jsp">
	    <jsp:param value="garuda.common.lable.addnewtest"  name="message" />
	    </jsp:include>
	    </s:else> --%>
	    <table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr >
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.common.lable.addnewtest" /> </strong></p>
			</div>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td>
			<input type="button" onclick="closeModalsForTest('modelPopup2')" value="<s:text name="garuda.common.close"/>" />
		</td>
	</tr>
</table>
	  	   
</div>
<div style="display: none;">
 <s:date name="specCollDate" id="specCollDate1" format="yyyy-MM-dd" />	
 <s:textfield readonly="true" onkeydown="cancelBack();"  cssClass="width:50%" name="specCollDate" id="specCollDate1"  value="%{specCollDate1}" ></s:textfield>											      
</div>							      
<div class="col_100 maincontainer" id="statusAddTest"><div class="col_100">
	<s:form id="addnewtestform">
	<s:hidden name="timingTstDatesArr" value="%{#request.timingTstDatesArr}" id="timingTstDatesArr"></s:hidden>
	<s:hidden name="otherLabTstsLstId" id="otherLabTstsLstId"></s:hidden>	
	<s:hidden name="specimenId"  />
	<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
	<s:hidden name="editTestFlag" />
	<s:hidden name="rowIndex" />
	<s:hidden name="LabSummLstsMapId" id="LabSummLstsMapId" />
	<s:hidden name="labSummHdrDatesMapId" id="labSummHdrDatesMapId" />
	<s:hidden name="preTestListDbStroed" value="%{#request.preTestListDbStroed}" id="preTestListDbStroed"/>
<s:hidden name="patLabsPojo.pkpatlabs"/>	
	 <div class="column">
	      <div class="portlet" id="addnewtestdiv" >
				<table width="100%" cellpadding="0" cellspacing="0" border="0" >
									 <%--  <tr>
									      <td width="30%">
									         <s:text name="garuda.cbuentry.label.testname" /><span style="color: red;">*</span>
									      </td>
									      <td>
									      	<s:textfield name="testName" readonly="true" onkeydown="cancelBack();" />
									      </td>
									      <s:if test="labtestlist1.size>0">
									      
									      <td width="60%">
									      	<select name="labtestlist1" id="labtestlist1" onchange="javascript:resetTimings(this.value)">
												 <option value="-1">Select Test Name
													</option>
												 <s:iterator value="%{labtestlist1}" var="row">
													<option value="<s:property value="%{#row[0]}"/>">
														<s:property value="%{#row[1]}"/>
													</option>
												</s:iterator> 
											</select>
									      </td>
									      </s:if>
									      <td width="10%">
									      <button type="button" onclick="addLabnewtest(<s:property value="specimenId"/>);"><s:text name="garuda.cbu.test.addTest"/></button>
									   	   </td> 
									   </tr> --%>
									  <tr>
									      <td width="30%">
									         <s:text name="garuda.cbuentry.label.testdescription" /><span style="color: red;">*</span>
									      </td>
									      <td width="60%">
									      	<s:textfield name="patLabsPojo.testDescription" id="patLabsPojo.testDescription"/>
									      </td>
									      <td width="10%">&nbsp;</td>
									   </tr>
									   <tr>
										   <td id="timingOfTstsDiv"  colspan="2" width="90%"><jsp:include page="cb_refreashTimingsOfTest.jsp"
															flush="true"></jsp:include> 
										  </td>
										  <td width="10%">&nbsp;</td>
									  </tr>
									   <tr id="tststrdate" style="display :none;">
									   		<td>
									         <s:text name="garuda.cbuentry.label.testStartDate" /><span style="color: red;">*</span>
									      </td>
									      <td>
									         <s:date name="patLabsPojo.testdate" id="datepicker21" format="MMM dd, yyyy" />
											<s:textfield readonly="true" onkeydown="cancelBack();"  name="patLabsPojo.testdate" id="datepicker21" value="%{datepicker21}" cssClass="datePicWMaxDate"></s:textfield>
										</td>									      
									  </tr> 
									  <tr>
									   		<td width="30%">
									         <s:text name="garuda.cbuentry.label.sampletype" />:<span style="color: red;">*</span>
									      </td>
									      <td width="60%">
									         <s:select id="patLabsPojo.fktestspecimen" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" name="patLabsPojo.fktestspecimen" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select Sample Type"></s:select>
									      </td>	
									      <td width="10%">&nbsp;</td>								      
									  </tr>
									  <tr>
									      <td width="30%">
									         <s:text name="garuda.cbuentry.label.testresult" /><span style="color: red;">*</span>
									      </td>
									      <td width="60%">
									      	<s:textfield name="patLabsPojo.testresult" cssClass="width:50%" id="patLabsPojo.testresult" value=""/>
									      </td>
									      <td width="10%">&nbsp;</td>
									   </tr>
									  </table>
									  <table bgcolor="#cccccc" width="100%" border="0" cellspacing="0" cellpadding="0">
									 	<tr>
									   		<td><jsp:include page="../cb_esignature.jsp" flush="true">
									   					<jsp:param value="testsubmit" name="submitId"/>
									   					<jsp:param value="invalid1" name="invalid" />
														<jsp:param value="minimum1" name="minimum" />
														<jsp:param value="pass1" name="pass" />
									   				</jsp:include></td>
									     		<s:if test="#request.cbuOtherTestFlag=='True'">
									   		<td >
									   			<button type="button" disabled="disabled" id="testsubmit"
												 
													onclick="javascript:addNewTestLab('<s:property value="specimenId"/>','<s:property value="cdrCbuPojo.cordID"/>','<s:property value="#request.timingOfTestmap.size()"/>','false')"><s:if test="#request.editTestFlag=='True'"><s:text name="garuda.unitreport.label.button.submit" /></s:if><s:else><s:text name="garuda.unitreport.label.button.submit" /></s:else></button>
										
									   		</td>
									   		</s:if>
									   		<s:elseif test="#request.cbuOtherTestFlag=='compScrFlag'">
									   			<td >
									   				<button type="button" disabled="disabled" id="testsubmit"
												 
														onclick="javascript:addNewTestComp('<s:property value="specimenId"/>','<s:property value="cdrCbuPojo.cordID"/>','<s:property value="#request.timingOfTestmap.size()"/>','false')"><s:if test="#request.editTestFlag=='True'"><s:text name="garuda.unitreport.label.button.submit" /></s:if><s:else><s:text name="garuda.unitreport.label.button.submit" /></s:else></button>
										
									   		</td>
									   		</s:elseif>
									   		<s:else>
									   		<td>
									   		<button type="button" disabled="disabled" id="testsubmit"
												onclick="javascript:addNewTest('<s:property value="specimenId"/>','<s:property value="cdrCbuPojo.cordID"/>','<s:property value="#request.timingOfTestmap.size()"/>','true')"><s:if test="#request.editTestFlag=='True'"><s:text name="garuda.unitreport.label.button.submit" /></s:if><s:else><s:text name="garuda.unitreport.label.button.submit" /></s:else></button>
										
									   		</td></s:else>
									   		<td><input type="button"  onclick="closeModalsForTest('modelPopup2')" value="<s:text name="garuda.common.lable.cancel"/>" /></td>
									   		<td>&nbsp;</td>
									   </tr>
									   </table>
											
			</div></div>
		</s:form>
	</div></div>
	<!-- <div id="showaddtestmodal"></div> -->