<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%
	UserJB userB = (UserJB) session.getAttribute("currentUser");
	request.setAttribute("assessmentuser",userB.getUserId());
%>
<script type="text/javascript">
$j(document).ready(function(){
	$j('.assessmentDiv input[type=checkbox]').each(function(index,el) {
		var s=$j(el).attr('checked');
		if(s==true){
			$j(el).val(true);
		}else if(s==false){
			$j(el).val(false);
		}
	});
	$j('.assessment').click();
//	getJQButton();
});

function showassessment(shname,val){
	$j('#'+shname+'_assessmentReview').addClass('warningFlag');
	$j('#'+shname+'_consultFlag').val(val);
	$j('#'+shname+'_assessmentReview').show();
	$j('#'+shname+'_assessmentconsult').hide();
	$j('#'+shname+"_editFlag").val("1");
}
function showEmail(id,shname){
	//alert($j('#cburegid').val());
	var cordid=$j('#regid').val();
	var formname=$j('#subentitytypedec').val();
	var title="IDM Notification";
	var userid=$j('#'+shname+'_assess'+id+'user').val();
	var url='assessmentMail?mailCode=<s:property value="@com.velos.ordercomponent.util.VelosMailConstants@ASSESSMENT_CODE"/>&cbuID='+cordid+'&formname='+formname+'&userid='+userid;
	//alert("url = "+url);
	if(userid!=null && userid!=""){
		showModal(title,url,'350','450');
	}else{
		//alert("Null");
		$j('#'+shname+'_'+id+'_'+'error').show();
	}
}
function setAssessvalus(shname){
	var response=$j('#'+shname+'_response').val();
	var responseidm=$j('#'+shname+'_assessmentresponse').val();

	if($j('#'+shname+'_consultFlag').val()=="1"){
		$j('#'+shname+'_assessconsult').hide();
		$j('#'+shname+'_consultby').show();
	}else if( $j('#'+shname+'_consultflag').val()=="1"){
		$j('#'+shname+'_assessconsult').hide();
		$j('#'+shname+'_consultby').show();
	}else{
		$j('#'+shname+'_assessconsult').show();
		$j('#'+shname+'_consultby').hide();
	}
	
	if($j('#'+shname+'_pkassessment').val()!=null && $j('#'+shname+'_pkassessment').val()!=""){
		$j('#'+shname+'_assessmentReview').hide();
		$j('#'+shname+'_assessmentconsult').show();
		$j('#'+shname+'_assessmentReview').removeClass('warningFlag');
	}else if($j('#'+shname+'_entityType').val()!=null && $j('#'+shname+'_entityType').val()!=""){
			$j('#'+shname+'_assessmentReview').hide();
			$j('#'+shname+'_assessmentconsult').show();
			$j('#'+shname+'_assessmentReview').removeClass('warningFlag');
	}else{
		$j('#'+shname+'_assessmentReview').show();
		$j('#'+shname+'_assessmentconsult').hide();
		$j('#'+shname+'_assessmentReview').addClass('warningFlag');
	}
	
	if(response!=null && response!=""){
		$j('#'+shname+'_'+response+'_asessmentres').attr("checked","checked");
		$j('#'+shname+'_'+response+'_asessresponse').attr("checked","checked");
		$j('#'+shname+'_response').val(response);
		
	}else if(responseidm!=null && responseidm!=""){
		$j('#'+shname+'_'+responseidm+'_asessmentres').attr("checked","checked");
		$j('#'+shname+'_'+responseidm+'_asessresponse').attr("checked","checked");
		$j('#'+shname+'_response').val(responseidm);
	
	}
	if($j('#'+shname+'_flagforLater').val()=="true"){
		$j('#'+shname+'_assessmentflag').attr("checked","true");
		$j('#'+shname+'_assessmentflag').val(true);
		$j('#'+shname+'_assessmentflag2').attr("checked","true");
		$j('#'+shname+'_Flagcomment').show();
		
	}else if($j('#'+shname+'_flaglater').val()=="true"){
		$j('#'+shname+'_assessmentflag').attr("checked","true");
		$j('#'+shname+'_assessmentflag').val(true);
		$j('#'+shname+'_assessmentflag2').attr("checked","true");
	}

	if($j('#'+shname+'_visibletcFlag').val()=="true"){
		$j('#'+shname+'_visibleTCFlag').attr("checked","true");
		$j('#'+shname+'_visibleTCFlag').val(true);
		$j('#'+shname+'_visibleflag').attr("checked","true");
	}else if($j('#'+shname+'_visibletotcflag').val()=="true"){
		$j('#'+shname+'_visibleTCFlag').attr("checked","true");
		$j('#'+shname+'_visibleTCFlag').val(true);
		$j('#'+shname+'_visibleflag').attr("checked","true");
	}
	
	if($j('#'+shname+'_assessmentRemarks').val()!=null && $j('#'+shname+'_assessmentRemarks').val()!=""){
		$j('#'+shname+'_assesmentcomment').val($j('#'+shname+'_assessmentRemarks').val());
	}else if($j('#'+shname+'_assessmentremark').val()!=null && $j('#'+shname+'_assessmentremark').val()!=""){
		$j('#'+shname+'_assesmentcomment').val($j('#'+shname+'_assessmentremark').val());
	}
}

function assignval(obj,shname){
	var value = $j(obj).val();
	if(value!="" && value!=null){
		$j('#'+shname+"_ass_err").hide();
		if(value == $j('#assess_ncd').val()){
			$j('#'+shname+'_response').val(value);
			$j('#'+shname+'_assessmentresponse').val('');
		}else if(value!=$j('#assess_ncd').val()){
			$j('#'+shname+'_assessmentresponse').val(value);
			$j('#'+shname+'_response').val('');
			if($j('#iscdrMember').val() == 'true'){
				var cordId = $j('#cordID').val();
				var cordregid = $j('#regid').val();
				var deferId = $j(obj).attr('id');	
				var url='updateCbuStatus?licenceUpdate=False&cbustatusin=IS_LOCAL_REG&cdrCbuPojo.cordID='+cordId+'&assessmentstatus=true'+'&shrtName='+shname+'&orderId=&orderType=&applyResol=&iscordavailfornmdp=&deferId='+deferId;
				 var upiob = $j('#numberOnCbuBag').val();
					var upiob_data = '';
					if(upiob!="" && upiob != null && upiob != "undefined"){
						upiob_data = ', <s:text name="garuda.cbuentry.label.idbag"/>:'+upiob;
					}
				
				cordregid = cordregid+upiob_data;
				setTimeout("showModal('<s:text name='garuda.common.title.cbustatustitle'/> "+cordregid+"','"+url+"','450','700')",0);
			}
		}
	}else{
		$j('#'+shname+"_ass_err").show();
	}
}
function saveassess(url,shname,obj,responseval){
	var shname1 = shname;
	var fkTestId=$j('#'+shname+'_fktestid').val();
	if(responseval!=null && responseval!="" && responseval!='undefined'){
		shname = responseval+"_"+shname; 
	}else{
		shname = "_"+shname;
	}
	var changeFlagVal = divChanges('assessmentChngCls',$j('#'+shname+'_assessmentDiv').html());
	
	if($j('#'+shname+'_pkassess').val()!=null){
		var pkassessment=$j('#'+shname+'_pkassess').val();
	}else if($j('#'+shname+'_pkassessment').val()!=null){
		var pkassessment=$j('#'+shname+'_pkassessment').val();
	}
	
	if($j('#'+shname+'_assessmentresponse').val()!=null && $j('#'+shname+'_assessmentresponse').val()!=""){
		var asessmentres=$j('#'+shname+'_assessmentresponse').val();	
	}else{
		var asessmentres=$j('#'+shname+'_response').val();
	}
	var assessmentflag=$j('#'+shname+'_assessmentflag').val();
	var flagComment = "";
	var flagComentreq="";
	if(assessmentflag=='true'){
		flagComment = $j('#'+shname+'_assesmentFlagComment').val();
		/*if(flagComment!=""){
			flagComentreq = true;
		}else{
			flagComentreq = false;
		}*/
	}/*else{
		flagComentreq = true;
	}*/
	var assesmentcomment=$j('#'+shname+'_assesmentcomment').val();
	$j("#"+shname+"_assessmentRemarks").val(assesmentcomment);
	var visibleTCFlag=$j('#'+shname+'_visibleTCFlag').val();
	var cordId=$j('#cordID').val();
	var subentitytype=$j('#subentityType').val();
	var fkSpecId=$j('#fkSpecimenId').val();
	var consultFlag=$j('#'+shname+'_consultFlag').val();
	
	if($j('#'+shname+'_assessconsultby').val()!=null && $j('#'+shname+'_assessconsultby').val()!=""){
		var assessmentconsultby=$j('#'+shname+'_assessconsultby').val();
	}else if($j('#'+shname+'_assessmentconsultby').val()!=null && $j('#'+shname+'_assessmentconsultby').val()!=""){
		var assessmentconsultby=$j('#'+shname+'_assessmentconsultby').val();
	}else{
		var assessmentconsultby=$j('#'+shname+'_assessconsultuser').val();
	}
	
	if($j('#'+shname+'_assesspostedby').val()!=null && $j('#'+shname+'_assesspostedby').val()!=""){
		var assessmentpostedby=$j('#'+shname+'_assesspostedby').val();
	}else if($j('#'+shname+'_assessmentpostedby').val()!=null && $j('#'+shname+'_assessmentpostedby').val()!=""){
		var assessmentpostedby=$j('#'+shname+'_assessmentpostedby').val();
	}else{
		var assessmentpostedby=$j('#'+shname+'_currentuser').val();
	}
	if($j("#"+shname+"_availableDate").val()!=null && $j("#"+shname+"_availableDate").val()!=""){
		var availableDate=$j("#"+shname+"_availableDate").val();
	}else{
		var availableDate="";
	}
	if($j('#'+shname+'_subEntityId').val()!=null){
		var subEntityId=$j('#'+shname+'_subEntityId').val();
	}
	var columnReference = $j("#"+shname+"_questionNo").val();
	var responseval = $j("#"+shname+"_responseval").val();
	var formflag = $j("#"+shname+"_formflag").val();
	var textflag = $j("#"+shname+"_textflag").val();
	var responsestr = $j("#"+shname+"_responsestr").val();
	var assessDivName = $j("#"+shname+"_assessDivName").val();
	
	//var data='assessmentresponse='+asessmentres+'&columnReference='+columnReference+'&flagforLater='+assessmentflag+'&assessmentRemarks='+assesmentcomment+'&visibleTCFlag='+visibleTCFlag+'&entityId='+cordId+'&fkTestid='+fkTestId+'&subentitytype='+subentitytype+'&shrtName='+shname1+'&assessmentpostedby='+assessmentpostedby+'&pkassessment='+pkassessment+'&subEntityId='+subEntityId+'&responseval='+responseval+'&formflag='+formflag+'&textflag='+textflag+'&responsestr='+responsestr+'&assessDivName='+assessDivName+'&assessmentFlagComment='+flagComment;
	var dummyForm = "<form id='"+shname+"_dummyForm' style='display:none'>";
		dummyForm += "<input type='hidden' name='assessmentresponse' value='"+asessmentres+"'/>";
		dummyForm += "<input type='hidden' name='columnReference' value='"+columnReference+"'/>";
		dummyForm += "<input type='hidden' name='flagforLater' value='"+assessmentflag+"'/>";
		dummyForm += "<input type='hidden' id='"+shname+"_assessmentRemarks' name='assessmentRemarks'/>";
		dummyForm += "<input type='hidden' name='visibleTCFlag' value='"+visibleTCFlag+"'/>";
		dummyForm += "<input type='hidden' name='entityId' value='"+cordId+"'/>";
		dummyForm += "<input type='hidden' name='fkTestid' value='"+fkTestId+"'/>";
		dummyForm += "<input type='hidden' name='subentitytype' value='"+subentitytype+"'/>";
		dummyForm += "<input type='hidden' name='shrtName' value='"+shname1+"'/>";
		dummyForm += "<input type='hidden' name='assessmentpostedby' value='"+assessmentpostedby+"'/>";
		dummyForm += "<input type='hidden' name='pkassessment' value='"+pkassessment+"'/>";
		dummyForm += "<input type='hidden' name='subEntityId' value='"+subEntityId+"'/>";
		dummyForm += "<input type='hidden' name='responseval' value='"+responseval+"'/>";
		dummyForm += "<input type='hidden' name='formflag' value='"+formflag+"'/>";
		dummyForm += "<input type='hidden' name='textflag' value='"+textflag+"'/>";
		dummyForm += "<input type='hidden' name='responsestr' id='responsestr'/>";
		dummyForm += "<input type='hidden' name='assessDivName' value='"+assessDivName+"'/>";
		dummyForm += "<input type='hidden' id='"+shname+"_assessmentFlagComment' name='assessmentFlagComment'/>";
		dummyForm += "</form>";
	$j('#'+shname+"_assessmentDivDummy").append(dummyForm);
	$j('#'+shname+"_assessmentDivDummy").find('#responsestr').val(responsestr);
	$j('#'+shname+"_assessmentDivDummy").find('#'+shname+"_dummyForm").find('#'+shname+"_assessmentRemarks").val(assesmentcomment);
	$j('#'+shname+"_assessmentDivDummy").find('#'+shname+"_dummyForm").find('#'+shname+"_assessmentFlagComment").val(flagComment);
	if(asessmentres!="" && assesmentcomment!=""){
		if(changeFlagVal || true){
			
			refershSaveass(shname,url);
		}else{
			$j("#"+shname+"_assessmentReview").hide();
		  	$j("#"+shname+"_assessmentconsult").show();
		}
		$j('#'+shname+"_editFlag").val('1');
		$j('#'+shname+'_assessmentReview').removeClass('warningFlag');
		$j('.'+shname1+'_res').removeClass('unexpectedRes');
		$j('.'+shname1+'_res').removeClass('assesReqPriorToShip');
		$j('.'+shname1+'_assessError').hide();
		$j('.'+shname1+'_res').css('border','1px solid #DBDAD9');
	}else{
		if(asessmentres==""){
			$j('#'+shname+"_ass_err").show();
		}
		if(assesmentcomment==""){
			$j('#'+shname+"_comm_err").show();
		}
	}
}
function refershSaveass(shname,url){
	$j.ajax({
		type: "POST",
	    url: url,
	    async:false,
	    data : $j('#'+shname+"_dummyForm").serialize(),
	    success: function (result){
	    	var $response=$j(result);
	        var errorpage = $response.find('#errorForm').html();
	        if(errorpage != null &&  errorpage != "" ){
	          	$j('#main').html(result);
	        }else{
	        	  	$j("#"+shname+"_assessmentDiv").html(result);
	        	  	$j("#"+shname+"_assessmentReview").hide();
	        	  	$j("#"+shname+"_assessmentconsult").show();
	        	  	//$j('#'+shname+"_dummyForm").remove();
	        	  	$j("#"+shname+"_assessChngChkFlag").addClass('assessmentChngCls');
	        }
	    },
	    error: function (request, status, error) {
	       	alert("Error " + error);
	        alert(request.responseText);
	    }
	});
}
function checkcom(value,shname){
	if(value!=""){
		$j('#'+shname+"_comm_err").hide();
	}else{
		$j('#'+shname+"_comm_err").show();
	}
}
function setvalues(id,shname){
	var s=$j('#'+id).attr('checked');
	if(s==true){
		$j('#'+id).val(true);
		$j('#'+shname+"_Flagcomment").show();
	}else{
		$j('#'+id).val(false);
		$j('#'+shname+"_Flagcomment").hide();
	}
}/*
function checkError(shname){
	if($j('#'+shname+'_assessmentflag').val()!="false"){
		$j('#'+shname+'_flag_err').hide();
	}
}*/
function clearError(id,shname){
	if($j('#'+shname+'_assess'+id+'user').val()!=null && $j('#'+shname+'_assess'+id+'user').val()!=""){
		$j('#'+shname+'_'+id+'_'+'error').hide();
	}else{
		$j('#'+shname+'_'+id+'_'+'error').show();
	}
}
function closeAssessment(assessDivName,shname,resval){
	
	if($j('#'+assessDivName).length==0){
	if(assessDivName.indexOf("QqQqQ")!=-1){
		assessDivName=assessDivName.replace("QqQqQ","");
	}
	}
	var editFlag = $j('#'+resval+"_"+shname+"_editFlag").val();
	if(shname != "" && shname != null && editFlag!='1'){
		$j('#'+assessDivName).hide();
		$j("."+shname+"_showassess").show();
		$j("."+shname+"_hideassess").hide();
	}else{
		$j('#'+resval+"_"+shname+"_editFlag").val('0');
		$j('#'+resval+"_"+shname+'_assessmentReview').removeClass('warningFlag');
		$j('#'+resval+"_"+shname+'_assessmentReview').hide();
		$j('#'+resval+"_"+shname+'_assessmentconsult').show();
	}
}
</script>
<style>
.assessmentDiv table td {
	border-left: none;
	border-right: none;
}

.assessmentDiv table td table td {
	border-left: none;
	border-right: none;
}
</style>
<div id="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_assessmentDiv" class="assessmentDiv" style="text-align: left;">
<div id="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_assessmentDivDummy"></div>
	<s:hidden name="assessmentPojo.pkAssessment" id="%{responseval}_%{shrtName}_pkassess" />
	<s:hidden name="assessmentPojo.assessmentResp" id="%{responseval}_%{shrtName}_response" />
	<s:hidden name="assessmentPojo.flagforLater" id="%{responseval}_%{shrtName}_flagforLater" />
	<s:hidden name="assessmentPojo.assessmentRemarks" id="%{responseval}_%{shrtName}_assessmentRemarks" />
	<s:hidden name="assessmentPojo.visibleTCFlag" id="%{responseval}_%{shrtName}_visibletcFlag" />
	<s:hidden name="assessmentPojo.consultFlag" id="%{responseval}_%{shrtName}_consultFlag" />
	<s:hidden name="assessmentPojo.entityId" id="%{responseval}_%{shrtName}_entityId" />
	<s:hidden name="assessmentPojo.availableDate" id="%{responseval}_%{shrtName}_availabledate" />
	<s:hidden name="assessmentPojo.assessmentpostedby" id="%{responseval}_%{shrtName}_assesspostedby" />
	<s:hidden name="assessmentPojo.subEntityId" id="%{responseval}_%{shrtName}_subEntityId" />
	<s:hidden name="assessmentPojo.entityType" id="%{responseval}_%{shrtName}_entityType" />
	<s:hidden name="assessmentPojo.assessmentconsultby" id="%{responseval}_%{shrtName}_assessconsultby" />
	<s:hidden name="assessresponseval" id="%{responseval}_%{shrtName}_responseval" value="%{responseval}"/>
	<s:hidden name="assessresponsestr" id="%{responseval}_%{shrtName}_responsestr" value="%{responsestr}"/>
	<s:hidden name="responsestr" id="%{responseval}_%{shrtName}_responsestr1"/>
	<s:hidden name="assesscurrentuser" id="%{responseval}_%{shrtName}_currentuser" value="%{#request.assessmentuser}" />
	
	<s:hidden name="pkassessment" id="%{responseval}_%{shrtName}_pkassessment" />
	<s:hidden name="assessmentremark" id="%{responseval}_%{shrtName}_assessmentremark" />
	<s:hidden name="flagforlater" id="%{responseval}_%{shrtName}_flaglater" />
	<s:hidden name="assessmentresponse" id="%{responseval}_%{shrtName}_assessmentresponse" />
	<s:hidden name="visibletotcflag" id="%{responseval}_%{shrtName}_visibletotcflag" />
	<s:hidden name="informflag" id="%{responseval}_%{shrtName}_informflag" />
	<s:hidden name="consultflag" id="%{responseval}_%{shrtName}_consultflag" />
	<s:hidden name="reviewedflag" id="%{responseval}_%{shrtName}_reviewedflag" />
	<s:hidden name="assessmentpostedby" id="%{responseval}_%{shrtName}_assessmentpostedby" />
	<s:hidden name="assessmentdate" id="%{responseval}_%{shrtName}_assessmentdate" />
	<s:hidden name="assessmentconsultby"id="%{responseval}_%{shrtName}_assessmentconsultby" />
	<s:hidden name="consulteddate" id="%{responseval}_%{shrtName}_consulteddate" />
	<s:hidden name="assessmentPojo.columnReference" id="%{responseval}_%{shrtName}_questionNo" value="%{questionNo}"></s:hidden>
	<s:hidden name="assessDivName" id="%{responseval}_%{shrtName}_assessDivName" value="%{assessDivName}"></s:hidden>
	<s:hidden name="assessmentPojo.formflag" id="%{responseval}_%{shrtName}_formflag" value="%{formflag}"></s:hidden>
	<s:hidden name="assessmentPojo.textflag" id="%{responseval}_%{shrtName}_textflag" value="%{textflag}"></s:hidden>
	<s:hidden name="editFlag" id="%{responseval}_%{shrtName}_editFlag"></s:hidden>
	<s:hidden id="%{responseval}_%{shrtName}_assessChngChkFlag"></s:hidden>
	
	<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SUBENTITY_CODE_TYPE]">
	<s:if test="%{pkCodeId==subentitytype}">
		<s:hidden id="subentitytypedec" value="%{description}"></s:hidden>
	</s:if>
	</s:iterator>

	<s:if test="assessmentPojo.assessmentpostedby!=null">
		<s:set name="assessmentpostuser" value="%{assessmentPojo.assessmentpostedby}" scope="request"></s:set>
	</s:if><s:elseif test="assessmentpostedby!=null">
		<s:set name="assessmentpostuser" value="%{assessmentpostedby}" scope="request"></s:set>
	</s:elseif><s:else>
		<s:set name="assessmentpostuser" value="%{#request.assessmentuser}" scope="request"></s:set>
	</s:else>

	<s:if test="assessmentPojo.assessmentconsultby!=null">
		<s:set name="assessmentconsultuser" value="%{assessmentPojo.assessmentconsultby}" scope="request"></s:set>
	</s:if><s:elseif test="assessmentconsultby!=null">
		<s:set name="assessmentconsultuser" value="%{assessmentconsultby}" scope="request"></s:set>
	</s:elseif><s:else>
		<s:set name="assessmentconsultuser" value="%{#request.assessmentuser}" scope="request"></s:set>
	</s:else>

<!-- 	<s:hidden name="user" value="%{#request.assessmentpostuser}"></s:hidden>-->
<button type="button" class="assessment" onclick="setAssessvalus('<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>')"style="visibility: hidden;"></button>

<table id="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_assessmentReview" width="100%">
	<tr>
		<td valign="top">
			<s:text	name="garuda.label.assessment.assessfor"/>&nbsp;<s:property value="%{responsestr}"/>&nbsp;<s:text name="garuda.label.dynamicform.response"/><span class="error">*</span>
		</td>
		<td>
			<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_ASSESS]">
				<input type="radio" name="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_assessmentresponse"
					   value="<s:property value='pkCodeId'/>" class="iseditable"
					   id="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_<s:property value='pkCodeId'/>_asessmentres"
					   onclick="assignval(this,'<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>');"
					   onchange="isChangeDone(this,'<s:property value="%{assessmentPojo.assessmentResp}"/>','assessmentChngCls');" 
					   />
				<label id="<s:property value='pkCodeId'/>_label" 
					for="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_<s:property value='pkCodeId'/>_asessmentres">
					<s:property value="description" />
				</label>
				<br />
			</s:iterator>
			<br/>
			<span style="display:none" id="<s:property value='%{responseval}'/>_<s:property value='shrtName'/>_ass_err" class="error">
				<s:text name="garuda.label.dynamicform.errormsg"/>
			</span>
		</td>
		<td valign="top">
			<s:checkbox	name="%{responseval}_%{shrtName}_flagforLater" id="%{responseval}_%{shrtName}_assessmentflag" value=""
						onclick="setvalues(this.id,'%{responseval}_%{shrtName}');" cssClass="iseditable" 
						onchange="isChangeDone(this,'%{assessmentPojo.flagforLater}','assessmentChngCls');"/>
			<s:text name="garuda.clinicalnote.label.flagForLater"></s:text>
		</td>
	</tr>
	<tr>
		<td>
			<s:text name="garuda.label.assessment.assesscomment"></s:text><span class="error">*</span>
		</td>
		<td><br/>
			<textarea rows="5" cols="30" name="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_assessmentRemarks" class="iseditable" maxlength="3000" 
						id="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_assesmentcomment" onchange="checkcom(this.value,'<s:property value="%{responseval}"/>_<s:property value="%{shrtName}"/>');
						isChangeDone(this,'<s:property value="%{assessmentPojo.assessmentRemarks}" escapeJavaScript="true"/>','assessmentChngCls');"></textarea>
			<br/>
			<span id='<s:property value='%{responseval}'/>_<s:property value="shrtName"/>_comm_err' class="error" style="display:none">
				<s:text name="garuda.label.dynamicform.errormsg"/>
			</span>
		</td>
		<td style="display: none;" id="<s:property value='%{responseval}'/>_<s:property value="shrtName"/>_Flagcomment">
			<s:text name="garuda.label.assessment.commentforflag"></s:text><br/>
			<textarea rows="5" cols="30" name="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_assessmentFlagComment" class="iseditable"
						id="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_assesmentFlagComment" maxlength="3000"
						onchange="isChangeDone(this,'<s:property value="%{assessmentPojo.assessmentFlagComment}" escapeJavaScript="true"/>','assessmentChngCls');"><s:property value='%{assessmentPojo.assessmentFlagComment}'/></textarea>
			<br/>
			<span id='<s:property value='%{responseval}'/>_<s:property value="shrtName"/>_Flagcomm_err' class="error" style="display:none">
				<s:text name="garuda.label.dynamicform.errormsg"/>
			</span>
		</td>
	</tr>
	<tr>
		<td>
			<s:checkbox name="%{responseval}_%{shrtName}_visibleTCFlag" id="%{responseval}_%{shrtName}_visibleTCFlag" value=""
						onclick="setvalues(this.id);" cssClass="iseditable"
						onchange="isChangeDone(this,'%{assessmentPojo.visibleTCFlag}','assessmentChngCls');"/>
			<s:text name="garuda.clinicalnote.label.visibleToTc"></s:text>
		</td>
		<!-- <td style="visibility: hidden;">
			<button type="button" name="informassessment"
					onclick="showEmail('inform','<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>');">
				<span><s:text name="garuda.clinicalnote.label.button.inform" /></span>
			</button>
		</td>
		<td style="visibility: hidden;">
			<s:if test="userLst!=null && userLst.size()>0">
				<s:select list="userLst" headerKey="" headerValue="-Select-"
					value="" listKey="userId" listValue="firstName" id="%{responseval}_%{shrtName}_assessinformuser" onchange="clearError('inform','%{responseval}_%{shrtName}');"/>
				<div id="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_inform_error" style="display: none;">
					<label class="error"><s:text name="garuda.label.assessment.plsSelectuser"></s:text></label>
				</div>
			</s:if>
		</td>
		<td nowrap="nowrap" style="visibility: hidden;">
			<button type="button" name="sendconsult"
				onclick="showEmail('consult','<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>');">
			<span><s:text name="garuda.label.assessment.sendforconsult" /></span>
			</button>
		</td>
		<td style="visibility: hidden;">
			<s:if test="userLst!=null && userLst.size()>0">
			<s:select list="userLst" headerKey="" headerValue="-Select-"
				   value="" listKey="userId" listValue="firstName" id="%{responseval}_%{shrtName}_assessconsultuser" onchange="clearError('consult','%{responseval}_%{shrtName}');"/>
			<div id='<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_consult_error' style="display: none;">
					<label class="error"><s:text name="garuda.cbu.assessment.user"/></label>
			</div>
			</s:if>
			
		</td>-->
		<td colspan="2">
			<button type="button" name="saveassessment" style="width: 65px" class="iseditable"
					onclick="saveassess('saveassessment','<s:property value='%{shrtName}'/>',this,'<s:property value='%{responseval}'/>');">
				<span><s:text name="garuda.common.save" /></span>
			</button>
		&nbsp;&nbsp;
			<button type="button" name="closeassessment" style="width: 65px" id='<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_close'
					onclick="closeAssessment('<s:property value='%{assessDivName}'/>','<s:property value='%{shrtName}'/>','<s:property value='%{responseval}'/>');">
				<span><s:text name="garuda.common.lable.cancel"/></span>
			</button>
		</td>
	</tr>
	<!-- <tr style="visibility: hidden;">
	<td></td>
		<td colspan="2">
			<button type="button" name="saveassessment"
					onclick="saveassess('saveassessment','<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>',this);">
				<span><s:text name="garuda.common.save" /></span>
			</button>
		</td>
		<td colspan="2">
			<button type="button" name="saveassessment"
					onclick="closeAssessment(this);">
				<span><s:text name="garuda.common.lable.cancel"/></span>
			</button>
		</td>
		<td></td>
	</tr>-->
</table>

<table id="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_assessmentconsult"	style="display: none;" width="100%">
	<tr>
		<td>
			<table>
				<tr>
					<td>
						<%
							String assessmentpostedby = request.getAttribute("assessmentpostuser").toString();
						    userB = new UserJB();
							if(assessmentpostedby!=null && assessmentpostedby!=""){
								userB.setUserId(EJBUtil.stringToNum(assessmentpostedby));
								userB.getUserDetails();
							}
							
						%>
						<s:text name="garuda.label.assessment.assesspostedby" />&nbsp;<%=userB.getUserLastName() + " " + userB.getUserFirstName()%>
						<s:text name="garuda.label.assessment.assesspostedon" />&nbsp; 
						<s:if test="%{assessmentPojo.assessmentpostedon!=null}">
							<s:date	name="%{assessmentPojo.assessmentpostedon}"
								id="assessmentPostedDate" format="MMM dd, yyyy" />
						</s:if>
						<s:elseif test="%{assessmentdate!=null}">
							<s:date	name="%{assessmentdate}"
								id="assessmentPostedDate" format="MMM dd, yyyy" />
						</s:elseif>
						<s:property value="%{assessmentPostedDate}" />
					</td>
				</tr>
				<tr style="display: none;" id="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_consultby">
					<td>
						<%
							String assessmentconsultuser = request.getAttribute("assessmentconsultuser").toString();
						    userB = new UserJB();
							if(assessmentconsultuser!=null && assessmentconsultuser!=""){
								userB.setUserId(EJBUtil.stringToNum(assessmentconsultuser));
								userB.getUserDetails();
							}
						%>
						<s:text name="garuda.label.assessment.assessconsultby"/>&nbsp;<%=userB.getUserLastName() + " " + userB.getUserFirstName()%>
						<s:text name="garuda.label.assessment.assesspostedon"/>&nbsp;
						<s:if test="%{assessmentconsulton!=null}">
							<s:date	name="%{assessmentPojo.assessmentconsulton}"
								id="consulteddate" format="MMM dd, yyyy" />
						</s:if>
						<s:elseif test="%{consulteddate!=null}">	
							<s:date	name="%{consulteddate}"
								id="consulteddate" format="MMM dd, yyyy" />
						</s:elseif>
						<s:property value="%{consulteddate}" />
					</td>
				</tr>
				<tr>
					<td>
						<s:text name="garuda.label.assessment.assessnote"></s:text>
						<div style="width: 200px;overflow: auto;padding: 5px;">
						<s:if test="%{assessmentPojo.assessmentRemarks!=null}">
							<s:property value="%{assessmentPojo.assessmentRemarks}" />
						</s:if>
						<s:elseif test="%{assessmentremark!=null}">
							<s:property value="%{assessmentremark}" />
						</s:elseif>
						</div>
					</td>
				</tr>
				<s:if test="%{assessmentPojo.assessmentFlagComment!=null}">
				<tr>
					<td>
						<s:text name="garuda.label.assessment.commentforflag"></s:text>
						<div style="width: 200px;overflow: auto;padding: 5px;">
							<s:property value="%{assessmentPojo.assessmentFlagComment}" />
						</div>
					</td>
				</tr>
				</s:if>
			</table>
		</td>
		<td>
		<fieldset>
			<legend style="white-space: normal;">
				<label style="float: left;">
					<s:text name="garuda.label.assessment.assessfor"/>&nbsp;<s:property value="%{responsestr}"/>
				</label>
			</legend><br/>
			<div style="clear: both;padding-top: 20px;">
				 <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_ASSESS]">
					<s:if test="pkCodeId==assessmentPojo.assessmentResp">
						<input type="radio" disabled="disabled"
							name="%{<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_assessmentresponse}"
							value="<s:property value='pkCodeId'/>"
							id="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_<s:property value='pkCodeId'/>_asessresponse" />
						<label id="<s:property value='pkCodeId'/>_label"><s:property
							value="description" /></label>
					</s:if>
					<s:elseif test="pkCodeId==assessmentresponse">
						<input type="radio" disabled="disabled"
							name="%{<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_assessmentresponse}"
							value="<s:property value='pkCodeId'/>"
							id="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_<s:property value='pkCodeId'/>_asessresponse" />
						<label id="<s:property value='pkCodeId'/>_label"><s:property
							value="description" /></label>
					</s:elseif>
				</s:iterator>
			</div>
		</fieldset>
		</td>

		<td>
		<fieldset><legend><s:text name="garuda.label.assessment.visibility" /></legend> <s:checkbox
			name="%{responseval}_%{shrtName}_visibleTCFlag"
			id="%{responseval}_%{shrtName}_visibleflag" value="" disabled="true"
			onclick="setvalues(this.id);" /> <s:text
			name="garuda.label.assessment.tcusers"></s:text></fieldset>
		</td>
		<td>
			<fieldset>
				<legend>
					<s:text name="garuda.clinicalnote.label.flagForLater" />
				</legend> 
				<s:checkbox	name="%{responseval}_%{shrtName}_flagforLater" id="%{responseval}_%{shrtName}_assessmentflag2" value=""
							onclick="setvalues(this.id);" disabled="true"/>
				<s:text name="garuda.clinicalnote.label.flagForLater"></s:text>
			</fieldset>
		</td>
	<!--
		<td>
		<a style="cursor: pointer; text-decoration: underline;"
			onclick="showassessment('<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>','0');">
			<s:text name="garuda.label.assessment.review" /> </a></td>
		<td id="<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>_assessconsult">
		 <a style="cursor: pointer; text-decoration: underline;"
			onclick="showassessment('<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>','1');">
		<s:text name="garuda.label.assessment.consult" /> </a></td>-->
	</tr>
	<tr>
	<td colspan="4">
		<button type="button" onclick="showassessment('<s:property value='%{responseval}'/>_<s:property value='%{shrtName}'/>','0');" class="iseditable">
			<s:text name="garuda.common.lable.edit" />
		</button>
	</td>
	</tr>
</table>
</div>