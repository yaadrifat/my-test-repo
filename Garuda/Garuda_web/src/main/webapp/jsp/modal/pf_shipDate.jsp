<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script type="text/javascript">
function getCordDetails(pkcord,regId){
	//var url = "loadClinicalDataFromPF?cdrCbuPojo.cordID="+pkcord;
	jQuery("#modelPopup1").dialog("destroy");
	//window.location=url;
	submitpost('loadClinicalDataFromPF',{'cdrCbuPojo.cordID':pkcord,'moduleEntityId':pkcord,'moduleEntityType':"<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU' />",'moduleEntityIdentifier':regId,'moduleEntityEventCode':'<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_CBU" />'});
}
</script>
<s:form>
	
	<div id="status">
		<table class="displaycdr">
			<thead>
				<tr>
				<th><s:text name="garuda.shipping.label.shipdate"/></th>
				</tr>
			</thead>
			<tbody>
					<s:if test="shedShipDtpopLst!=null && shedShipDtpopLst.size()>0">
						<s:iterator value="shedShipDtpopLst" var="rowVal">
						<tr>
							<td align="center"><a href='#' onclick="getCordDetails('<s:property value="%{#rowVal[3]}"/>','<s:property value="%{#rowVal[1]}"/>')" ><s:property value="%{#rowVal[1]}"/></a> <s:text name="garuda.shipping.message.scheduled"/>  <s:property value="%{#rowVal[0]}"/></td>
						</tr>
						</s:iterator>
					</s:if>
				
			</tbody>
			<tfoot>
				<tr>
					<td></td>
				</tr>
			</tfoot>
		</table>
	</div>


</s:form>