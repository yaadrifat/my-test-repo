<jsp:include page="../widget-setting.jsp">
<jsp:param value="28" name="pageId"/>
</jsp:include>

<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="java.util.Date"%>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch1" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch2" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch3" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch4" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch5" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch6" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch7" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch8" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch9" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch10" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch11" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch12" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch13" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch14" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch15" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginateSearch16" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%String contextpath = request.getContextPath()+"/jsp/modal/"; 
String contxtpath = request.getContextPath();
%>
<script type="text/javascript" src="<%=contxtpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<script type="text/javascript" src="<%=contxtpath%>/jsp/js/jquery/tabletools/js/TableTools.js" ></script>
<script type="text/javascript" src="<%=contxtpath%>/jsp/js/jquery/tabletools/js/ZeroClipboard.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=contxtpath%>/jsp/js/jquery/tabletools/css/TableTools.css" media="screen" />
<s:include value="cb_audit_include.jsp" />
<script>
flag=0;flag1=0;flag2=0;flag3=0;flag4=0;flag5=0;flag6=0;flag7=0;flag8=0;flag9=0;flag10=0;flag11=0;flag12=0;flag13=0;flag14=0;flag15=0;flag16=0;

function closeModalAudit(){
	 $j("#auditModalDiv").dialog("close");
	 $j("#auditModalDiv").dialog("close");
	 $j("#auditModalDiv").dialog("destroy");
	 $j("#auditModalDiv").dialog("destroy");
}
function resetValidForm(formId){
	$j("#"+formId).validate().resetForm();
}
function fnCreateSelectAudit( aData,j,val)
{
		var r='<select id="'+val+j+'"><option value="">-Select-</option>', i, iLen=aData.length;
		aData.sort();
		for ( i=0 ; i<iLen ; i++ )
		{
			r += '<option value="'+aData[i]+'">'+aData[i]+'</option>';
		}
	return r+'</select>';
}

(function($j){
	var validator = $j("#audit_dummyForm").validate({
		invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    }
	});
getDatePic();

	jQuery(function() {
		  jQuery('#dateEnd14, #dateStart14').datepicker('option', {
		    beforeShow: customRange14
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange14(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});

	jQuery(function() {
		  jQuery('#dateEnd, #dateStart').datepicker('option', {
		    beforeShow: customRange1
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange1(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});

	jQuery(function() {
		  jQuery('#dateEnd4, #dateStart4').datepicker('option', {
		    beforeShow: customRange4
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange4(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});

	jQuery(function() {
		  jQuery('#dateEnd5, #dateStart5').datepicker('option', {
		    beforeShow: customRange5
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange5(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});

	jQuery(function() {
		  jQuery('#dateEnd6, #dateStart6').datepicker('option', {
		    beforeShow: customRange6
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange6(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});

	jQuery(function() {
		  jQuery('#dateEnd7, #dateStart7').datepicker('option', {
		    beforeShow: customRange7
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange7(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	}); 

	jQuery(function() {
		  jQuery('#dateEnd2, #dateStart2').datepicker('option', {
		    beforeShow: customRange2
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange2(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});
	
	jQuery(function() {
		  jQuery('#dateEnd3, #dateStart3').datepicker('option', {
		    beforeShow: customRange3
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange3(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});

	jQuery(function() {
		  jQuery('#dateEnd8, #dateStart8').datepicker('option', {
		    beforeShow: customRange8
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange8(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});

	jQuery(function() {
		  jQuery('#dateEnd9, #dateStart9').datepicker('option', {
		    beforeShow: customRange9
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange9(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});

	jQuery(function() {
		  jQuery('#dateEnd12, #dateStart12').datepicker('option', {
		    beforeShow: customRange12
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange12(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});

	jQuery(function() {
		  jQuery('#dateEnd13, #dateStart13').datepicker('option', {
		    beforeShow: customRange13
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange13(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});

	jQuery(function() {
		  jQuery('#dateEnd11, #dateStart11').datepicker('option', {
		    beforeShow: customRange11
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange11(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});

	jQuery(function() {
		  jQuery('#dateEnd10, #dateStart10').datepicker('option', {
		    beforeShow: customRange10
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange10(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});

	jQuery(function() {
		  jQuery('#dateEnd15, #dateStart15').datepicker('option', {
		    beforeShow: customRange15
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange15(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});
	

	$j.fn.dataTableExt.oApi.fnGetColumnData = function ( oSettings, iColumn, bUnique, bFiltered, bIgnoreEmpty ) {
		if ( typeof iColumn == "undefined" ) return new Array();
		
		if ( typeof bUnique == "undefined" ) bUnique = true;
		if ( typeof bFiltered == "undefined" ) bFiltered = true;
		if ( typeof bIgnoreEmpty == "undefined" ) bIgnoreEmpty = true;
		var aiRows;
		if (bFiltered == true) aiRows = oSettings.aiDisplay; 
		else aiRows = oSettings.aiDisplayMaster; // all row numbers

		var asResultData = new Array();
		
		for (var i=0,c=aiRows.length; i<c; i++) {
			iRow = aiRows[i];
			var aData = this.fnGetData(iRow);
			var sValue = aData[iColumn];
			if (bIgnoreEmpty == true && sValue.length == 0) continue;
			else if (bUnique == true && jQuery.inArray(sValue, asResultData) > -1) continue;
			else asResultData.push(sValue);
		}
		return asResultData;
}}($j));

function fnResetAuditFilters(val1,val2,val3){
	var i=0;
	var tot_th_count=$j('#'+val1).find('tr')[0].cells.length;
	while(i<tot_th_count){
		var temp='#'+val3+''+i;
		$j(temp).val('');
		var oTable3 = $j('#'+val2).dataTable();
		oTable3.fnFilter('', i );
		i++;
	}
}
function docHistoryOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#doctrailspan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('docaudittrailparent','headerPrnt');
		},500);
		return false;
	}
	if($j('#doctrailspan').is(".ui-icon-plusthick")) {
		if(flag==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=docaudit','docAuditDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag=1;
		}
	}
	if($j('#doctrailspan').is(".ui-icon-minusthick")) {
		$j('#docAuditDiv').show();
	}

	var is_all_entries=$j('#showsDocsearchResults').val();
	var pageNo=$j('#paginateWrapper34').find('.currentPage').text();
	var showEntries=$j('#docinfo_tbl_sentries').val();
	var tEntries=$j('#tentriesDoc').val();
	var sort=3;
	var sortDir="desc";
	var SortParm=$j('#docinfo_tbl_sort_col').val();
	var filter_value=$j('#doc_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#docinfo_tbl_sort_col').val();
		sortDir=$j('#docinfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#docinfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#docinfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsDocsearchResults" id="showsDocsearchResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';

	 if(showEntries=="ALL"){
		 var docTable = $j('#docAuditHistory').dataTable({
				"sScrollY": "150",
		        "sScrollX": "100%",
		        "sAjaxSource": 'getJsonPagination.action?TblFind=docInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#docinfo_tbl_param').val(),
		 		"bServerSide": true,
		 		"bProcessing": true, 
		 		"aaSorting": [[ sort, sortDir ]],
		 		"bRetrieve" : true,
		 		"oSearch": {"sSearch": filter_value},
		 		"bDeferRender": true,
		 		"bDestroy": true,
		 		"bSortCellsTop": true,
		 		"sAjaxDataProp": "aaData",
		 		"aoColumnDefs": [
								   {		
									  "aTargets": [0],"mDataProp": function ( source, type, val ) {
				                	   return source[0];
				                  }},
				                  {
				                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
									return source[1];
				                  }},
				                  {
				                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
				                	 	return source[2];
				                  }},
				                  {		
				                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
				                	 	return source[3];
			                	   }},
				                   { 	
			                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
		    		                	return source[4];
				                   }},
				                   { 	
			                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
				                	   var temp="";
				  		               if (source[4] == 'Revoke') {
					  		               temp = "<a href='#'> <img src=images/attachment-icon.png onclick=showDocument("+source[5]+")> </a>";
				  		               }
				  		               return temp;
				                   }}
								],

								"fnDrawCallback": function() { 
		  							//alert("ALL Call Back........")
		  							$j('#doctab_wrapper').find('.dataTables_scrollFoot').hide();
								    $j('#doctab_info').hide();
								    $j("#showsDocsearchResults option:contains('ALL')").attr('selected', 'selected'); 	
								    fnDocInfoTableSort('docInfoTr');
		  						},
		  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
								 /*"fnClick": function ( nButton, oConfig, flash ) {
			                        this.fnSetText( flash, "\n"+ "\t\t CBU Information Audit Trail" +"\n\n"+
			                                this.fnGetTableData(oConfig) );
									},*/
									"sFileName": "Document Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues14').html(temptxt);
	           return temptxt;
	         
	         }					
	});
		 $j('#doctab_info').after('<div id="entriesvalues14"><div>');
		   //	var temptxt='Showing 1 to '+$j('#tentriesDoc').val()+' of '+$j('#tentriesDoc').val()+' entries';
		   	//$j('#entriesvalues14').html(temptxt);	
	 }else if(isPrint!="1" && isPrint!="2"){
		 var docTable = $j('#docAuditHistory').dataTable({
			 "sScrollY": "150",
	            "sScrollX": "100%",
	            "sAjaxSource": 'getJsonPagination.action?TblFind=docInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#docinfo_tbl_param').val(),
	            "bServerSide": true,
	            "bProcessing": true, 
	     		"bRetrieve" : true,
	     		"oSearch": {"sSearch": filter_value},
				"bDestroy": true,
				"bSortCellsTop": true,
	     		"aaSorting": [[ sort, sortDir ]],
	     		"bDeferRender": true,
	     		"sAjaxDataProp": "aaData",
		 		"aoColumnDefs": [
								   {		
									  "aTargets": [0],"mDataProp": function ( source, type, val ) {
				                	   return source[0];
				                  }},
				                  {
				                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
									return source[1];
				                  }},
				                  {
				                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
				                	 	return source[2];
				                  }},
				                  {		
				                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
				                	 	return source[3];
			                	   }},
				                   { 	
			                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
		    		                	return source[4];
				                   }},
				                   { 	
			                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
				  		               var temp="";
				  		               if (source[4] == 'Revoke') {
					  		               temp = "<a href='#'> <img src=images/attachment-icon.png onclick=showDocument("+source[5]+")> </a>";
				  		               }
				  		               return temp; 	
				                   }}
								],"fnDrawCallback": function() { 
		  							fnDocInfoTableSort('docInfoTr');
		  						},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
							"aButtons": [
											{
												"sExtends": "xls",
												"sButtonText": "Export",
												 /*"fnClick": function ( nButton, oConfig, flash ) {
							                        this.fnSetText( flash, "\n"+ "\t\t CBU Information Audit Trail" +"\n\n"+
							                                this.fnGetTableData(oConfig) );
													},*/
													"sFileName": "Document Audit Trail.csv"  
											}
										]
						}
			});
		 docTable.fnAdjustColumnSizing();
	    }else if(((isPrint=="1" || isPrint=="2") && $j('#doctrailspan').is(".ui-icon-plusthick"))){
			 var docTable = $j('#docAuditHistory').dataTable({
				 
		            "sAjaxSource": 'getJsonPagination.action?TblFind=docInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#docinfo_tbl_param').val(),
		            "bServerSide": true,
		            "bProcessing": true, 
		     		"bRetrieve" : true,
		     		"oSearch": {"sSearch": filter_value},
					"bDestroy": true,
					"bSortCellsTop": true,
		     		"aaSorting": [[ sort, sortDir ]],
		     		"bDeferRender": true,
		     		"sAjaxDataProp": "aaData",
			 		"aoColumnDefs": [
									   {		
										  "aTargets": [0],"mDataProp": function ( source, type, val ) {
					                	   return source[0];
					                  }},
					                  {
					                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
										return source[1];
					                  }},
					                  {
					                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
					                	 	return source[2];
					                  }},
					                  {		
					                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
					                	 	return source[3];
				                	   }},
					                   { 	
				                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
			    		                	return source[4];
					                   }},
					                   { 	
				                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
					  		               var temp="";
					  		               if (source[4] == 'Revoke') {
						  		               temp = "<a href='#'> <img src=images/attachment-icon.png onclick=showDocument("+source[5]+")> </a>";
					  		               }
					  		               return temp; 	
					                   }}
									],"fnDrawCallback": function() { 
			  							fnDocInfoTableSort('docInfoTr');
			  						},
							"sDom": 'T<"clear">lfrtip',
							"oTableTools": {
								"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
								"aButtons": [
												{
													"sExtends": "xls",
													"sButtonText": "Export",
													 /*"fnClick": function ( nButton, oConfig, flash ) {
								                        this.fnSetText( flash, "\n"+ "\t\t CBU Information Audit Trail" +"\n\n"+
								                                this.fnGetTableData(oConfig) );
														},*/
														"sFileName": "Document Audit Trail.csv"  
												}
											]
							}
				});
		    	
		    }						
	 
	 $j('#docAuditHistory_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#doc_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,showsDocsearchResults,temp,trailForm,docAuditDiv,docHistoryOnload,cordID,entityId,docaudit,docinfo_tbl_param");
		  }
	});

	 var usedNames = {};
		$j("select[name='doccat'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='docusrid'] > option").each(function () {
		    if(usedNames[this.text] ||  $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='docsubcat'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='docact'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		
		var	doccat=$j('#doccat').val();
		var	docusr=$j('#docusrid').val();
		var	docsubcat=$j('#docsubcat').val();
		var	docact=$j('#docact').val();
		var	pStartDateD1=$j('#dateStart14').val();
		var	pStartDateD2=$j('#dateEnd14').val();

	$j('#resetDoc').click(function(){
		docTable.fnSort( [ [3,'desc'] ] );
		docTable.fnDraw();
		 });

	
	$j('#docAuditHistory_info').hide();
	$j('#docAuditHistory_paginate').hide();
	$j('#docAuditHistory_length').replaceWith(showEntriesTxt);
	$j('#docAuditHistory_length').replaceWith('Show <select name="showsDocsearchResults" id="showsDocsearchResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="2000"/>">All</option></select> Entries');
	if($j('#docentries').val()!=null || $j('#docentries').val()!=""){
		$j('#showsDocsearchResults').val($j('#docentries').val());
		}
	$j('#showsDocsearchResults').change(function(){
		var textval=$j('#showsDocsearchResults :selected').html();
		$j('#docinfo_tbl_sentries').val(textval);
		var	doccat=$j('#doccat').val();
		var	docusr=$j('#docusrid').val();
		var	docsubcat=$j('#docsubcat').val();
		var	docact=$j('#docact').val();
		var pStartDateD1=$j('#dateStart14').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd14').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,showsDocsearchResults,temp,trailForm,docAuditDiv,docHistoryOnload,cordID,entityId,docaudit,docinfo_tbl_param");
		$j('#doccat').val(doccat);
		$j('#docusrid').val(docusr);
		$j('#docsubcat').val(docsubcat);
		$j('#docact').val(docact);
		$j('#dateStart14').val(pStartDtD1);
		$j('#dateEnd14').val(pStartDtD2);
	});	

	var wrapperWidth=$j('#doccordaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#docAuditHistory').width(newWidth);
	var docct = $j('#docCtg').val();
	var docusrr = $j('#docUsr').val();
	var docsubcateg = $j('#docSubCtg').val();
	var docacttk = $j('#docActTkn').val();
	$j('#doccat').val(docct);
	$j('#docusrid').val(docusrr);
	$j('#docsubcat').val(docsubcateg);
	$j('#docact').val(docacttk);
	if(((isPrint=="1" || isPrint=="2") && $j('#doctrailspan').is(".ui-icon-plusthick"))){
		flag=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('docaudittrailparent','headerPrnt');
			},500);
		}
	}
}


function searchReasultOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#audittrailspan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('cordaudittrailparent','headerPrnt');
		},500);
		return false;
	}
	if($j('#audittrailspan').is(".ui-icon-plusthick")) {
		if(flag1==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=cordaudit','auditTrailHistoryDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag1=1;
		}
	}
	if($j('#audittrailspan').is(".ui-icon-minusthick")) {
		$j('#auditTrailHistoryDiv').show();
	}
	var is_all_entries=$j('#showsRowsearchResults').val();
	var pageNo=$j('#paginateWrapper21').find('.currentPage').text();
	var showEntries=$j('#cbuinfo_tbl_sentries').val();
	var tEntries=$j('#tentriescbuinfo').val();
	var sort=2;
	var sortDir="desc";
	var SortParm=$j('#cbuinfo_tbl_sort_col').val();
	var filter_value=$j('#cbu_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#cbuinfo_tbl_sort_col').val();
		sortDir=$j('#cbuinfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#cbuinfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#cbuinfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="showsRowsearchResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';
	
    if(showEntries=="ALL"){
    	
    	var auditTbl=$j('#auditTrailHistory').dataTable({
            "sScrollY": "180px",
            "sScrollX": "300px",
            "sAjaxSource": 'getJsonPagination.action?TblFind=cbuInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#cbuinfo_tbl_param').val(),
     		"bServerSide": true,
     		"bProcessing": true, 
     		"aaSorting": [[ sort, sortDir ]],
     		"bRetrieve" : true,
     		"oSearch": {"sSearch": filter_value},
     		"bDeferRender": true,
     		"bDestroy": true,
     		"bSortCellsTop": true,
     		"sAjaxDataProp": "aaData",
     		"aoColumnDefs": [
  						   {		
  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
  		                	   return source[1];
  		                  }},
  		                  {
  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
  		                	 	return source[3];
  		                  }},
  		                  {		
  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
  		                	 	return source[4];
  	                	   }},
  		                   { 	
  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
      		                	return source[5];
  		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
    		                	return source[6];
		                   }}
  		                   
  						],
  						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#auditTrailHistory_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#auditTrailHistory_info').hide();
						    $j("#showsRowsearchResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnAuditTableSort('cbuInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
  						"oTableTools": {
  							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
  							"aButtons": [
  											{
  												"sExtends": "xls",
  												"sButtonText": "Export",
  												 /*"fnClick": function ( nButton, oConfig, flash ) {
  							                        this.fnSetText( flash, "\n"+ "\t\t CBU Information Audit Trail" +"\n\n"+
  							                                this.fnGetTableData(oConfig) );
  													},*/
  													"sFileName": "CBU Information Audit Trail.csv"  
  											}
  										]
  						},
  						"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
  					        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
  					        if(iTotal==0){
  					         temptxt='Showing 0 to 0 of 0 entries';
  					        }
  					        if(iMax!=iTotal){
  					         temptxt=temptxt+'(filtered from '+iMax+' entries)';
  					        }
  					           $j('#entriesvalues').html(temptxt);
  					           return temptxt;
  					         
  					         }			
		});
    	$j('#auditTrailHistory_info').after('<div id="entriesvalues"><div>');
       	//var temptxt='Showing 1 to '+$j('#tentriescbuinfo').val()+' of '+$j('#tentriescbuinfo').val()+' entries';
       	//$j('#entriesvalues').html(temptxt);	
    	
    }else if(isPrint!="1" && isPrint!="2"){
    		var auditTbl=$j('#auditTrailHistory').dataTable({
            "sScrollY": "150",
            "sScrollX": "300px",
            "sAjaxSource": 'getJsonPagination.action?TblFind=cbuInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#cbuinfo_tbl_param').val(),
            "bServerSide": true,
            "bProcessing": true, 
     		"bRetrieve" : true,
     		"oSearch": {"sSearch": filter_value},
			"bDestroy": true,
			"bSortCellsTop": true,
     		"aaSorting": [[ sort, sortDir ]],
     		"bDeferRender": true,
     		"sAjaxDataProp": "aaData",
     		"aoColumnDefs": [
     		                {		
  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
  		                	   return source[1];
  		                  }},
  		                  {
  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
  		                	 	return source[3];
  		                  }},
  		                  {		
  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
  		                	 	return source[4];
  	                	   }},
  		                   { 	
  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
      		                	return source[5];
  		                   }}      	 ,
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
    		                	return source[6];
		                   }}
  						],
  						"fnDrawCallback": function() { 
  							fnAuditTableSort('cbuInfoTr');
  						},
				"sDom": 'T<"clear">lfrtip',
  						"oTableTools": {
  							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
  							"aButtons": [
  											{
  												"sExtends": "xls",
  												"sButtonText": "Export",
  													"sFileName": "CBU Information Audit Trail.csv"  
  											}
  										]
  						}			
		});
    		auditTbl.fnAdjustColumnSizing();
    }else if(((isPrint=="1" || isPrint=="2") && $j('#audittrailspan').is(".ui-icon-plusthick"))){
    		var auditTbl=$j('#auditTrailHistory').dataTable({
            
            "sAjaxSource": 'getJsonPagination.action?TblFind=cbuInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#cbuinfo_tbl_param').val(),
            "bServerSide": true,
            "bProcessing": true, 
     		"bRetrieve" : true,
     		"oSearch": {"sSearch": filter_value},
			"bDestroy": true,
			"bSortCellsTop": true,
     		"aaSorting": [[ sort, sortDir ]],
     		"bDeferRender": true,
     		"sAjaxDataProp": "aaData",
     		"aoColumnDefs": [
     		                {		
  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
  		                	   return source[1];
  		                  }},
  		                  {
  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
  		                	 	return source[3];
  		                  }},
  		                  {		
  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
  		                	 	return source[4];
  	                	   }},
  		                   { 	
  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
      		                	return source[5];
  		                   }}      	 ,
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
    		                	return source[6];
		                   }}
  						],
  						"fnDrawCallback": function() { 
  							fnAuditTableSort('cbuInfoTr');
  						},
				"sDom": 'T<"clear">lfrtip',
  						"oTableTools": {
  							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
  							"aButtons": [
  											{
  												"sExtends": "xls",
  												"sButtonText": "Export",
  													"sFileName": "CBU Information Audit Trail.csv"  
  											}
  										]
  						}			
		});
    	
    }
	
    $j('#auditTrailHistory_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#cbu_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,showsRowsearchResults,temp,trailForm,auditTrailHistoryDiv,searchReasultOnload,cordID,entityId,cordaudit,cbuinfo_tbl_param");
		  }
	});
    
	var usedNames = {};
	$j("select[name='fieldName'] > option").each(function () {
	    if(usedNames[this.text] || $j(this).val() == "" ) {
	        $j(this).remove();
	    } else {
	        usedNames[this.text] = this.value;
	    }
	});
	var usedNames = {};
	$j("select[name='userId'] > option").each(function () {
	    if(usedNames[this.text] ||  $j(this).val() == "" ) {
	        $j(this).remove();
	    } else {
	        usedNames[this.text] = this.value;
	    }
	});
	var usedNames = {};
	$j("select[name='actTaken'] > option").each(function () {
	    if(usedNames[this.text] || $j(this).val() == "" ) {
	        $j(this).remove();
	    } else {
	        usedNames[this.text] = this.value;
	    }
	});
	var usedNames = {};
	$j("select[name='prevValue'] > option").each(function () {
	    if(usedNames[this.text] || $j(this).val() == "" ) {
	        $j(this).remove();
	    } else {
	        usedNames[this.text] = this.value;
	    }
	});
	var usedNames = {};
	$j("select[name='newValue'] > option").each(function () {
	    if(usedNames[this.text] || $j(this).val() == "" ) {
	        $j(this).remove();
	    } else {
	        usedNames[this.text] = this.value;
	    }
	});
	
	var	fieldName=$j('#fieldName').val();
	var	userId=$j('#userId').val();
	var	actTaken=$j('#actTaken').val();
	var	prevValue=$j('#prevValue').val();
	var	newValue=$j('#newValue').val();
	var	pStartDateD1=$j('#dateStart').val();
	var	pStartDateD2=$j('#dateEnd').val();
	
	$j('#resetCBUInfo').click(function(){
		auditTbl.fnSort( [ [2,'desc'] ] );
		auditTbl.fnDraw();
		 });

	$j('#auditTrailHistory_info').hide();
	$j('#auditTrailHistory_paginate').hide();
	$j('#auditTrailHistory_length').empty();

	
	$j('#auditTrailHistory_length').replaceWith(showEntriesTxt);
	if($j('#auditentries').val()!=null || $j('#auditentries').val()!=""){
		$j('#showsRowsearchResults').val($j('#auditentries').val());
		}
	$j('#showsRowsearchResults').change(function(){
		var textval=$j('#showsRowsearchResults :selected').html();
		$j('#cbuinfo_tbl_sentries').val(textval);
		var fieldName=$j('#fieldName').val();
		var userId=$j('#userId').val();
		var actTaken=$j('#actTaken').val();
		var prevValue=$j('#prevValue').val();
		var newValue=$j('#newValue').val();
		var pStartDateD1=$j('#dateStart').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd').val();
		var pStartDtD2 = pStartDateD2;
		
		paginationFooter(0,"revokedUnitReportsWidget,showsRowsearchResults,temp,trailForm,auditTrailHistoryDiv,searchReasultOnload,cordID,entityId,cordaudit,cbuinfo_tbl_param");

		$j('#fieldName').val(fieldName);
		$j('#userId').val(userId);
		$j('#actTaken').val(actTaken);
		$j('#prevValue').val(prevValue);
		$j('#newValue').val(newValue);
		$j('#dateStart').val(pStartDtD1);
		$j('#dateEnd').val(pStartDtD2);
	});
	var wrapperWidth=$j('#cordaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#auditTrailHistory').width(newWidth);

	var fldNme = $j('#fldNme').val();
	var usrId = $j('#usrId').val();
	var actTkn = $j('#actTkn').val();
	var prvVl = $j('#prvVl').val();
	var nwVl = $j('#nwVl').val();

	
	$j('#fieldName').val(fldNme);
	$j('#userId').val(usrId);
	$j('#actTaken').val(actTkn);
	$j('#prevValue').val(prvVl);
	$j('#newValue').val(nwVl);
	if(((isPrint=="1" || isPrint=="2") && $j('#audittrailspan').is(".ui-icon-plusthick"))){
		flag1=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('cordaudittrailparent','headerPrnt');
			},500);
		}
	}
}

function entityStatusOnload(isPrint){
	$j('#widgetPrintDiv').empty();
	if((isPrint=="1" && $j('#entitystatspan').is(".ui-icon-minusthick"))){
		setTimeout(function(){
			clickheretoprintaudit('entitystatusaudittrailparent','headerPrnt');
		},500);
		return false;
	}
	if($j('#entitystatspan').is(".ui-icon-plusthick")) {
		if(flag2==0){
			refreshDiv('revokedUnitReportsWidget?entityId='+$j("#entityId").val()+'&cordID='+$j("#cordID").val()+'&widget=entitystat','entityStatusDiv','trailForm');
			if(isPrint!="1" && isPrint!="2")
				flag2=1;
		}
	}
	if($j('#entitystatspan').is(".ui-icon-minusthick")) {
		$j('#entityStatusDiv').show();
	}

	var is_all_entries=$j('#showsEntitysearchResults').val();
	var pageNo=$j('#paginateWrapper24').find('.currentPage').text();
	var showEntries=$j('#licinfo_tbl_sentries').val();
	var tEntries=$j('#tentriesLic').val();
	var sort=2;
	var sortDir="desc";
	var SortParm=$j('#licinfo_tbl_sort_col').val();
	var filter_value=$j('#lic_audit_fval').val();
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#licinfo_tbl_sort_col').val();
		sortDir=$j('#licinfo_tbl_sort_type').val();
	}	
	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#licinfo_tbl_sentries').val(is_all_entries);
		showEntries=$j('#licinfo_tbl_sentries').val();
	}

	showEntriesTxt='Show <select name="showsRow" id="showsEntitysearchResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';
	
	 if(showEntries=="ALL"){
	var entityStatTab = $j('#entitystatusaudittrailtab').dataTable({
		"sScrollY": "150",
        "sScrollX": "100%",
        "sAjaxSource": 'getJsonPagination.action?TblFind=licInfo&allEntries=ALL&cordpk='+$j('#cordID').val()+$j('#licinfo_tbl_param').val(),
 		"bServerSide": true,
 		"bProcessing": true, 
 		"aaSorting": [[ sort, sortDir ]],
 		"bRetrieve" : true,
 		"oSearch": {"sSearch": filter_value},
 		"bDeferRender": true,
 		"bDestroy": true,
 		"bSortCellsTop": true,
 		"sAjaxDataProp": "aaData",
 		"aoColumnDefs": [
						   {		
							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		                	   return source[1];
		                  }},
		                  {
		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
							return source[2];
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		                	 	return source[3];
		                  }},
		                  {		
		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		                	 	return source[4];
	                	   }},
		                   { 	
	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
  		                	return source[5];
		                   }},
		                   { 	
	                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
		                	return source[6];
		                   }}
						],
						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#entitystatusaudittrailtab_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#entitystatusaudittrailtab_info').hide();
						    $j("#showsEntitysearchResults option:contains('ALL')").attr('selected', 'selected'); 	
						    fnLicInfoTableSort('licInfoTr');
  						},
  						"sDom": 'T<"clear">lfrtipS',
		"oTableTools": {
			"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
							{
								"sExtends": "xls",
								"sButtonText": "Export",
									"sFileName": "Licensure Audit Trail.csv"  
							}
						]
		},
		"fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
	        var temptxt='Showing 1 to '+iTotal+' of '+iTotal+' entries';
	        if(iTotal==0){
	         temptxt='Showing 0 to 0 of 0 entries';
	        }
	        if(iMax!=iTotal){
	         temptxt=temptxt+'(filtered from '+iMax+' entries)';
	        }
	           $j('#entriesvalues4').html(temptxt);
	           return temptxt;
	         
	         }					
	});

	$j('#entitystatusaudittrailtab_info').after('<div id="entriesvalues4"><div>');
   	//var temptxt='Showing 1 to '+$j('#tentriesLic').val()+' of '+$j('#tentriesLic').val()+' entries';
   	//$j('#entriesvalues4').html(temptxt);
	 }else if(isPrint!="1"  && isPrint!="2"){
		 var entityStatTab = $j('#entitystatusaudittrailtab').dataTable({
			 "sScrollY": "150",
	            "sScrollX": "100%",
	            "sAjaxSource": 'getJsonPagination.action?TblFind=licInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#licinfo_tbl_param').val(),
	            "bServerSide": true,
	            "bProcessing": true, 
	     		"bRetrieve" : true,
	     		"oSearch": {"sSearch": filter_value},
				"bDestroy": true,
				"bSortCellsTop": true,
	     		"aaSorting": [[ sort, sortDir ]],
	     		"bDeferRender": true,
	     		"sAjaxDataProp": "aaData",
	     		"aoColumnDefs": [
	     		                {		
	  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
	  		                	   return source[1];
	  		                  }},
	  		                  {
	  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
								return source[2];
			                  }},
			                  {
			                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
	  		                	 	return source[3];
	  		                  }},
	  		                  {		
	  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
	  		                	 	return source[4];
	  	                	   }},
	  		                   { 	
	  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
	      		                	return source[5];
	  		                   }}      	 ,
			                   { 	
		                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
	    		                	return source[6];
			                   }}
	  						],
	  						"fnDrawCallback": function() { 
	  							fnLicInfoTableSort('licInfoTr');
	  						},
					"sDom": 'T<"clear">lfrtip',
					"oTableTools": {
	  							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
	  							"aButtons": [
	  											{
	  												"sExtends": "xls",
	  												"sButtonText": "Export",
	  													"sFileName": "Licensure Audit Trail.csv"  
	  											}
	  										]
	  						}
			});
		 entityStatTab.fnAdjustColumnSizing();
	    }else if(((isPrint=="1" || isPrint=="2") && $j('#entitystatspan').is(".ui-icon-plusthick"))){
			 var entityStatTab = $j('#entitystatusaudittrailtab').dataTable({
				 
		            "sAjaxSource": 'getJsonPagination.action?TblFind=licInfo&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&cordpk='+$j('#cordID').val()+$j('#licinfo_tbl_param').val(),
		            "bServerSide": true,
		            "bProcessing": true, 
		     		"bRetrieve" : true,
		     		"oSearch": {"sSearch": filter_value},
					"bDestroy": true,
					"bSortCellsTop": true,
		     		"aaSorting": [[ sort, sortDir ]],
		     		"bDeferRender": true,
		     		"sAjaxDataProp": "aaData",
		     		"aoColumnDefs": [
		     		                {		
		  							  "aTargets": [0],"mDataProp": function ( source, type, val ) {
		  		                	   return source[1];
		  		                  }},
		  		                  {
		  		                 "aTargets": [1],"mDataProp": function ( source, type, val ) {
									return source[2];
				                  }},
				                  {
				                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
		  		                	 	return source[3];
		  		                  }},
		  		                  {		
		  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
		  		                	 	return source[4];
		  	                	   }},
		  		                   { 	
		  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
		      		                	return source[5];
		  		                   }}      	 ,
				                   { 	
			                	    	"aTargets": [5], "mDataProp": function ( source, type, val ) { 
		    		                	return source[6];
				                   }}
		  						],
		  						"fnDrawCallback": function() { 
		  							fnLicInfoTableSort('licInfoTr');
		  						},
						"sDom": 'T<"clear">lfrtip',
						"oTableTools": {
		  							"sSwfPath": "<%=contxtpath%>/jsp/js/jquery/tabletools/swf/copy_csv_xls_pdf.swf",
		  							"aButtons": [
		  											{
		  												"sExtends": "xls",
		  												"sButtonText": "Export",
		  													"sFileName": "Licensure Audit Trail.csv"  
		  											}
		  										]
		  						}
				});
		    	
		    }
	 
	 $j('#entitystatusaudittrailtab_filter').find(".searching1").keyup(function(e){ 
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#lic_audit_fval').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"revokedUnitReportsWidget,showsEntitysearchResults,temp,trailForm,entityStatusDiv,entityStatusOnload,cordID,entityId,entitystat,licinfo_tbl_param");
		  }
	});

	 var usedNames = {};
		$j("select[name='fieldName4'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='userId4'] > option").each(function () {
		    if(usedNames[this.text] ||  $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='actTaken4'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='prevValue4'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		var usedNames = {};
		$j("select[name='newValue4'] > option").each(function () {
		    if(usedNames[this.text] || $j(this).val() == "" ) {
		        $j(this).remove();
		    } else {
		        usedNames[this.text] = this.value;
		    }
		});
		
		var	fieldName=$j('#fieldName4').val();
		var	userId=$j('#userId4').val();
		var	actTaken=$j('#actTaken4').val();
		var	prevValue=$j('#prevValue4').val();
		var	newValue=$j('#newValue4').val();
		var	pStartDateD1=$j('#dateStart4').val();
		var	pStartDateD2=$j('#dateEnd4').val();

	$j('#resetLic').click(function(){
		entityStatTab.fnSort( [ [2,'desc'] ] );
		entityStatTab.fnDraw();
		 });

	
	$j('#entitystatusaudittrailtab_info').hide();
	$j('#entitystatusaudittrailtab_paginate').hide();
	$j('#entitystatusaudittrailtab_length').empty();
	$j('#entitystatusaudittrailtab_length').replaceWith(showEntriesTxt);
	if($j('#entityentries').val()!=null || $j('#entityentries').val()!=""){
		$j('#showsEntitysearchResults').val($j('#entityentries').val());
		}
	$j('#showsEntitysearchResults').change(function(){
		var textval=$j('#showsEntitysearchResults :selected').html();
		$j('#licinfo_tbl_sentries').val(textval);
		var fieldName=$j('#fieldName4').val();
		var userId=$j('#userId4').val();
		var actTaken=$j('#actTaken4').val();
		var prevValue=$j('#prevValue4').val();
		var newValue=$j('#newValue4').val();
		var pStartDateD1=$j('#dateStart4').val();
		var pStartDtD1 = pStartDateD1;
		var pStartDateD2=$j('#dateEnd4').val();
		var pStartDtD2 = pStartDateD2;
		paginationFooter(0,"revokedUnitReportsWidget,showsEntitysearchResults,temp,trailForm,entityStatusDiv,entityStatusOnload,cordID,entityId,entitystat,licinfo_tbl_param");
		$j('#fieldName4').val(fieldName);
		$j('#userId4').val(userId);
		$j('#actTaken4').val(actTaken);
		$j('#prevValue4').val(prevValue);
		$j('#newValue4').val(newValue);
		$j('#dateStart4').val(pStartDtD1);
		$j('#dateEnd4').val(pStartDtD2);
	});	
	var wrapperWidth=$j('#entitystatusaudittrail').width();
	var newWidth=wrapperWidth*0.99;
	$j('#entitystatusaudittrailtab').width(newWidth);
	var fldNme = $j('#fldNme4').val();
	var usrId = $j('#usrId4').val();
	var actTkn = $j('#actTkn4').val();
	var prvVl = $j('#prvVl4').val();
	var nwVl = $j('#nwVl4').val();

	$j('#fieldName4').val(fldNme);
	$j('#userId4').val(usrId);
	$j('#actTaken4').val(actTkn);
	$j('#prevValue4').val(prvVl);
	$j('#newValue4').val(nwVl);
	if(((isPrint=="1" || isPrint=="2") && $j('#entitystatspan').is(".ui-icon-plusthick"))){
		flag2=0;
		if(isPrint=="1"){
			setTimeout(function(){
				clickheretoprintaudit('entitystatusaudittrailparent','headerPrnt');
			},500);
		}
	}
}



</script>
<style type="text/css">
#cbuAuditTbody td{
	padding: 1.5px 10px !important;
    /*white-space: nowrap;*/ 
}
#idAuditTbody td{
	padding: 1px 10px !important;
    /*white-space: nowrap;*/ 
}
</style>

<s:hidden name="cbuinfo_tbl_sort_col" id="cbuinfo_tbl_sort_col"/>
<s:hidden name="cbuinfo_tbl_sort_type" id="cbuinfo_tbl_sort_type"/>
<s:hidden name="cbuinfo_tbl_sentries" id="cbuinfo_tbl_sentries"/>
<s:hidden name="cbuinfo_tbl_param" id="cbuinfo_tbl_param"/>
<s:hidden name="cbu_audit_fval" id="cbu_audit_fval"/>
<s:hidden name="cbu_info_filter_flag" id="cbu_info_filter_flag"/>

<s:hidden name="idinfo_tbl_sort_col" id="idinfo_tbl_sort_col"/>
<s:hidden name="idinfo_tbl_sort_type" id="idinfo_tbl_sort_type"/>
<s:hidden name="idinfo_tbl_sentries" id="idinfo_tbl_sentries"/>
<s:hidden name="idinfo_tbl_param" id="idinfo_tbl_param"/>
<s:hidden name="id_audit_fval" id="id_audit_fval"/>
<s:hidden name="id_info_filter_flag" id="id_info_filter_flag"/>


<s:hidden name="procinfo_tbl_sort_col" id="procinfo_tbl_sort_col"/>
<s:hidden name="procinfo_tbl_sort_type" id="procinfo_tbl_sort_type"/>
<s:hidden name="procinfo_tbl_sentries" id="procinfo_tbl_sentries"/>
<s:hidden name="procinfo_tbl_param" id="procinfo_tbl_param"/>
<s:hidden name="proc_audit_fval" id="proc_audit_fval"/>
<s:hidden name="proc_info_filter_flag" id="proc_info_filter_flag"/>

<s:hidden name="licinfo_tbl_sort_col" id="licinfo_tbl_sort_col"/>
<s:hidden name="licinfo_tbl_sort_type" id="licinfo_tbl_sort_type"/>
<s:hidden name="licinfo_tbl_sentries" id="licinfo_tbl_sentries"/>
<s:hidden name="licinfo_tbl_param" id="licinfo_tbl_param"/>
<s:hidden name="lic_audit_fval" id="lic_audit_fval"/>
<s:hidden name="lic_info_filter_flag" id="lic_info_filter_flag"/>

<s:hidden name="eliginfo_tbl_sort_col" id="eliginfo_tbl_sort_col"/>
<s:hidden name="eliginfo_tbl_sort_type" id="eliginfo_tbl_sort_type"/>
<s:hidden name="eliginfo_tbl_sentries" id="eliginfo_tbl_sentries"/>
<s:hidden name="eliginfo_tbl_param" id="eliginfo_tbl_param"/>
<s:hidden name="elig_audit_fval" id="elig_audit_fval"/>
<s:hidden name="elig_info_filter_flag" id="elig_info_filter_flag"/>

<s:hidden name="revinfo_tbl_sort_col" id="revinfo_tbl_sort_col"/>
<s:hidden name="revinfo_tbl_sort_type" id="revinfo_tbl_sort_type"/>
<s:hidden name="revinfo_tbl_sentries" id="revinfo_tbl_sentries"/>
<s:hidden name="revinfo_tbl_param" id="revinfo_tbl_param"/>
<s:hidden name="rev_audit_fval" id="rev_audit_fval"/>
<s:hidden name="rev_info_filter_flag" id="rev_info_filter_flag"/>

<s:hidden name="hlainfo_tbl_sort_col" id="hlainfo_tbl_sort_col"/>
<s:hidden name="hlainfo_tbl_sort_type" id="hlainfo_tbl_sort_type"/>
<s:hidden name="hlainfo_tbl_sentries" id="hlainfo_tbl_sentries"/>
<s:hidden name="hlainfo_tbl_param" id="hlainfo_tbl_param"/>
<s:hidden name="hla_audit_fval" id="hla_audit_fval"/>
<s:hidden name="hla_info_filter_flag" id="hla_info_filter_flag"/>

<s:hidden name="pathlainfo_tbl_sort_col" id="pathlainfo_tbl_sort_col"/>
<s:hidden name="pathlainfo_tbl_sort_type" id="pathlainfo_tbl_sort_type"/>
<s:hidden name="pathlainfo_tbl_sentries" id="pathlainfo_tbl_sentries"/>
<s:hidden name="pathlainfo_tbl_param" id="pathlainfo_tbl_param"/>
<s:hidden name="pathla_audit_fval" id="pathla_audit_fval"/>
<s:hidden name="pathla_info_filter_flag" id="pathla_info_filter_flag"/>

<s:hidden name="labinfo_tbl_sort_col" id="labinfo_tbl_sort_col"/>
<s:hidden name="labinfo_tbl_sort_type" id="labinfo_tbl_sort_type"/>
<s:hidden name="labinfo_tbl_sentries" id="labinfo_tbl_sentries"/>
<s:hidden name="labinfo_tbl_param" id="labinfo_tbl_param"/>
<s:hidden name="lab_audit_fval" id="lab_audit_fval"/>
<s:hidden name="lab_info_filter_flag" id="lab_info_filter_flag"/>

<s:hidden name="cninfo_tbl_sort_col" id="cninfo_tbl_sort_col"/>
<s:hidden name="cninfo_tbl_sort_type" id="cninfo_tbl_sort_type"/>
<s:hidden name="cninfo_tbl_sentries" id="cninfo_tbl_sentries"/>
<s:hidden name="cninfo_tbl_param" id="cninfo_tbl_param"/>
<s:hidden name="cn_audit_fval" id="cn_audit_fval"/>
<s:hidden name="cn_info_filter_flag" id="cn_info_filter_flag"/>

<s:hidden name="mcinfo_tbl_sort_col" id="mcinfo_tbl_sort_col"/>
<s:hidden name="mcinfo_tbl_sort_type" id="mcinfo_tbl_sort_type"/>
<s:hidden name="mcinfo_tbl_sentries" id="mcinfo_tbl_sentries"/>
<s:hidden name="mcinfo_tbl_param" id="mcinfo_tbl_param"/>
<s:hidden name="mc_audit_fval" id="mc_audit_fval"/>
<s:hidden name="mc_info_filter_flag" id="mc_info_filter_flag"/>

<s:hidden name="orinfo_tbl_sort_col" id="orinfo_tbl_sort_col"/>
<s:hidden name="orinfo_tbl_sort_type" id="orinfo_tbl_sort_type"/>
<s:hidden name="orinfo_tbl_sentries" id="orinfo_tbl_sentries"/>
<s:hidden name="orinfo_tbl_param" id="orinfo_tbl_param"/>
<s:hidden name="or_audit_fval" id="or_audit_fval"/>
<s:hidden name="or_info_filter_flag" id="or_info_filter_flag"/>

<s:hidden name="idminfo_tbl_sort_col" id="idminfo_tbl_sort_col"/>
<s:hidden name="idminfo_tbl_sort_type" id="idminfo_tbl_sort_type"/>
<s:hidden name="idminfo_tbl_sentries" id="idminfo_tbl_sentries"/>
<s:hidden name="idminfo_tbl_param" id="idminfo_tbl_param"/>
<s:hidden name="idm_audit_fval" id="idm_audit_fval"/>
<s:hidden name="idm_info_filter_flag" id="idm_info_filter_flag"/>

<s:hidden name="hhsinfo_tbl_sort_col" id="hhsinfo_tbl_sort_col"/>
<s:hidden name="hhsinfo_tbl_sort_type" id="hhsinfo_tbl_sort_type"/>
<s:hidden name="hhsinfo_tbl_sentries" id="hhsinfo_tbl_sentries"/>
<s:hidden name="hhsinfo_tbl_param" id="hhsinfo_tbl_param"/>
<s:hidden name="hhs_audit_fval" id="hhs_audit_fval"/>
<s:hidden name="hhs_info_filter_flag" id="hhs_info_filter_flag"/>

<s:hidden name="docinfo_tbl_sort_col" id="docinfo_tbl_sort_col"/>
<s:hidden name="docinfo_tbl_sort_type" id="docinfo_tbl_sort_type"/>
<s:hidden name="docinfo_tbl_sentries" id="docinfo_tbl_sentries"/>
<s:hidden name="docinfo_tbl_param" id="docinfo_tbl_param"/>
<s:hidden name="doc_audit_fval" id="doc_audit_fval"/>
<s:hidden name="doc_info_filter_flag" id="doc_info_filter_flag"/>

<s:hidden name="fldNme" id="fldNme"/>
<s:hidden name="usrId" id="usrId"/>
<s:hidden name="actTkn" id="actTkn"/>
<s:hidden name="prvVl" id="prvVl"/>
<s:hidden name="nwVl" id="nwVl"/>
<s:hidden name="fldNme2" id="fldNme2"/>
<s:hidden name="usrId2" id="usrId2"/>
<s:hidden name="actTkn2" id="actTkn2"/>
<s:hidden name="prvVl2" id="prvVl2"/>
<s:hidden name="nwVl2" id="nwVl2"/>
<s:hidden name="fldNme3" id="fldNme3"/>
<s:hidden name="usrId3" id="usrId3"/>
<s:hidden name="actTkn3" id="actTkn3"/>
<s:hidden name="prvVl3" id="prvVl3"/>
<s:hidden name="nwVl3" id="nwVl3"/>
<s:hidden name="fldNme4" id="fldNme4"/>
<s:hidden name="usrId4" id="usrId4"/>
<s:hidden name="actTkn4" id="actTkn4"/>
<s:hidden name="prvVl4" id="prvVl4"/>
<s:hidden name="nwVl4" id="nwVl4"/>
<s:hidden name="fldNme5" id="fldNme5"/>
<s:hidden name="usrId5" id="usrId5"/>
<s:hidden name="actTkn5" id="actTkn5"/>
<s:hidden name="prvVl5" id="prvVl5"/>
<s:hidden name="nwVl5" id="nwVl5"/>
<s:hidden name="fldNme6" id="fldNme6"/>
<s:hidden name="usrId6" id="usrId6"/>
<s:hidden name="actTkn6" id="actTkn6"/>
<s:hidden name="prvVl6" id="prvVl6"/>
<s:hidden name="nwVl6" id="nwVl6"/>
<s:hidden name="fldNme7" id="fldNme7"/>
<s:hidden name="usrId7" id="usrId7"/>
<s:hidden name="actTkn7" id="actTkn7"/>
<s:hidden name="prvVl7" id="prvVl7"/>
<s:hidden name="nwVl7" id="nwVl7"/>
<s:hidden name="fldNme8" id="fldNme8"/>
<s:hidden name="usrId8" id="usrId8"/>
<s:hidden name="actTkn8" id="actTkn8"/>
<s:hidden name="prvVl8" id="prvVl8"/>
<s:hidden name="nwVl8" id="nwVl8"/>
<s:hidden name="fldNme9" id="fldNme9"/>
<s:hidden name="usrId9" id="usrId9"/>
<s:hidden name="actTkn9" id="actTkn9"/>
<s:hidden name="prvVl9" id="prvVl9"/>
<s:hidden name="nwVl9" id="nwVl9"/>
<s:hidden name="fldNme10" id="fldNme10"/>
<s:hidden name="usrId10" id="usrId10"/>
<s:hidden name="actTkn10" id="actTkn10"/>
<s:hidden name="prvVl10" id="prvVl10"/>
<s:hidden name="nwVl10" id="nwVl10"/>
<s:hidden name="fldNme11" id="fldNme11"/>
<s:hidden name="usrId11" id="usrId11"/>
<s:hidden name="actTkn11" id="actTkn11"/>
<s:hidden name="prvVl11" id="prvVl11"/>
<s:hidden name="nwVl11" id="nwVl11"/>
<s:hidden name="fldNme12" id="fldNme12"/>
<s:hidden name="usrId12" id="usrId12"/>
<s:hidden name="actTkn12" id="actTkn12"/>
<s:hidden name="prvVl12" id="prvVl12"/>
<s:hidden name="nwVl12" id="nwVl12"/>
<s:hidden name="frmnme1" id="frmnme1"/>
<s:hidden name="fversn1" id="fversn1"/>
<s:hidden name="usid1" id="usid1"/>
<s:hidden name="frmnme2" id="frmnme2"/>
<s:hidden name="fversn2" id="fversn2"/>
<s:hidden name="usid2" id="usid2"/>
<s:hidden name= "docCtg" id="docCtg"/>
<s:hidden name= "docUsr" id="docUsr"/>
<s:hidden name= "docSubCtg" id="docSubCtg"/>
<s:hidden name= "docActTkn" id="docActTkn"/>


<s:form id="audit_dummyForm">
<div style="display: none;" id="dateDiv" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart" id="dateStart" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart" id="dateStart" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd" id="dateEnd" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd" id="dateEnd" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear" onclick="fnclear('dateStart','dateEnd')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt1" onclick="$j('#dateDiv').hide(); fn_FilterCbuInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt1" onclick="$j('#dateDiv').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>

<div style="display: none;" id="dateDiv2" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv2').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart2" id="dateStart2" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart2" id="dateStart2" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd2" id="dateEnd2" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd2" id="dateEnd2" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear2" onclick="fnclear('dateStart2','dateEnd2')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt2" onclick="$j('#dateDiv2').hide(); fn_FilterIdInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt2" onclick="$j('#dateDiv2').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>

<div style="display: none;" id="dateDiv3" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv3').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart3" id="dateStart3" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart3" id="dateStart3" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd3" id="dateEnd3" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd3" id="dateEnd3" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear3" onclick="fnclear('dateStart3','dateEnd3')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt3" onclick="$j('#dateDiv3').hide(); fn_FilterProcInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt3" onclick="$j('#dateDiv3').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>

<div style="display: none;" id="dateDiv4" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv4').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart4" id="dateStart4" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart4" id="dateStart4" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd4" id="dateEnd4" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd4" id="dateEnd4" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear3" onclick="fnclear('dateStart4','dateEnd4')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt4" onclick="$j('#dateDiv4').hide(); fn_FilterLicInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt4" onclick="$j('#dateDiv4').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>

<div style="display: none;" id="dateDiv5" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv5').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart5" id="dateStart5" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart5" id="dateStart5" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd5" id="dateEnd5" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd5" id="dateEnd5" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear5" onclick="fnclear('dateStart5','dateEnd5')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt5" onclick="$j('#dateDiv5').hide(); fn_FilterEligInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt5" onclick="$j('#dateDiv5').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>

<div style="display: none;" id="dateDiv6" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv6').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart6" id="dateStart6" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart6" id="dateStart6" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd6" id="dateEnd6" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd6" id="dateEnd6" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear6" onclick="fnclear('dateStart6','dateEnd6')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt6" onclick="$j('#dateDiv6').hide(); fn_FilterRevInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt6" onclick="$j('#dateDiv6').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>

<div style="display: none;" id="dateDiv7" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv7').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart7" id="dateStart7" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart7" id="dateStart7" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd7" id="dateEnd7" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd7" id="dateEnd7" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear7" onclick="fnclear('dateStart7','dateEnd7')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt7" onclick="$j('#dateDiv7').hide(); fn_FilterHlaInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt7" onclick="$j('#dateDiv7').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>

<div style="display: none;" id="dateDiv8" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv8').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart8" id="dateStart8" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart8" id="dateStart8" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd8" id="dateEnd8" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd8" id="dateEnd8" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear8" onclick="fnclear('dateStart8','dateEnd8')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt8" onclick="$j('#dateDiv8').hide(); fn_FilterLabInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt8" onclick="$j('#dateDiv8').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>
<div style="display: none;" id="dateDiv9" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv9').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart9" id="dateStart9" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart9" id="dateStart9" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd9" id="dateEnd9" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd9" id="dateEnd9" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear9" onclick="fnclear('dateStart9','dateEnd9')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt9" onclick="$j('#dateDiv9').hide(); fn_FilterCnInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt9" onclick="$j('#dateDiv9').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>
<div style="display: none;" id="dateDiv10" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv10').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart10" id="dateStart10" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart10" id="dateStart10" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd10" id="dateEnd10" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd10" id="dateEnd10" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear10" onclick="fnclear('dateStart10','dateEnd10')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt10" onclick="$j('#dateDiv10').hide(); fn_FilterMcInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt10" onclick="$j('#dateDiv10').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>

<div style="display: none;" id="dateDiv11" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv11').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart11" id="dateStart11" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart11" id="dateStart11" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd11" id="dateEnd11" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd11" id="dateEnd11" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear11" onclick="fnclear('dateStart11','dateEnd11')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt11" onclick="$j('#dateDiv11').hide(); fn_FilterOrInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt11" onclick="$j('#dateDiv11').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>

<div style="display: none;" id="dateDiv12" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv12').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart12" id="dateStart12" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart12" id="dateStart12" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd12" id="dateEnd12" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd12" id="dateEnd12" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear12" onclick="fnclear('dateStart12','dateEnd12')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt12" onclick="$j('#dateDiv12').hide(); fn_FilterIdmInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt12" onclick="$j('#dateDiv12').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>

<div style="display: none;" id="dateDiv13" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv13').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart13" id="dateStart13" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart13" id="dateStart13" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd13" id="dateEnd13" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd13" id="dateEnd13" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear13" onclick="fnclear('dateStart13','dateEnd13')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt13" onclick="$j('#dateDiv13').hide(); fn_FilterHhsInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt13" onclick="$j('#dateDiv13').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>

<div style="display: none;" id="dateDiv14" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv14').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart14" id="dateStart14" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart14" id="dateStart14" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd14" id="dateEnd14" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd14" id="dateEnd14" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear14" onclick="fnclear('dateStart14','dateEnd14')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt14" onclick="$j('#dateDiv14').hide(); fn_FilterDocInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt14" onclick="$j('#dateDiv14').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>

<div style="display: none;" id="dateDiv15" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv15').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart15" id="dateStart15" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart15" id="dateStart15" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd15" id="dateEnd15" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd15" id="dateEnd15" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear15" onclick="fnclear('dateStart15','dateEnd15')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt15" onclick="$j('#dateDiv15').hide(); fn_FilterPatHlaInfo();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt15" onclick="$j('#dateDiv15').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>


</s:form>
<div id="auditprnt">
<s:form id="trailForm">
</s:form>

<div id="widgetPrintDiv" style="display:none;"> 
<!-- This Used For Print -->
</div>
	<s:hidden name="cdrCbuPojo.cordID" id="cdrCbucordID"></s:hidden>
	<s:hidden name="cdrCbuPojo.registryId" id="registryId"></s:hidden> 
	
	<%
	Date dt = new Date();
	request.setAttribute("dt",dt);
	%>
	<div id="headerPrnt">
	<table width="100%">
	<tr><td colspan ="2" align="center" ><font size="2"> <b><s:text name="garuda.audittrail.label.auditrephead"></s:text> </b> </font></td> </tr>
	<tr><td nowrap><font size="1"> <b><s:text name="garuda.cbuentry.label.registrycbuid"/>: <s:property value="cdrCbuPojo.registryId" /></b> </font></td> <td align="left" nowrap><font size="1"><b><s:text name="garuda.pendingorderpage.label.localcbuid"></s:text>: <s:property value="cdrCbuPojo.localCbuId" /></b></font></td></tr>
	<tr><td nowrap><font size="1"> <b><s:text name="garuda.cbbdefaults.lable.cbbid"/>: <s:property value="cdrCbuPojo.site.siteIdentifier"/></b> </font></td>  <td align="left" nowrap><font size="1"> <b><s:text name="garuda.cordentry.label.isbtid"></s:text>: <s:property value="cdrCbuPojo.cordIsbiDinCode" /></b></font></td></tr>
	<tr><td nowrap><font size="1"> <b><s:text name="garuda.cbu.unitreport.runDateTime"/> <s:property value="%{getText('{0,date,MMM dd, yyyy hh:mm:ss aa}',{#request.dt})}"/> </b> </font></td> <td>&nbsp;</td> <td style="float:right"><span class="ui-icon ui-icon-print" onclick="clickheretoprintall()"></span> </td></tr>
	</table> 
	</div>
	
	
	<s:form>
	<div class="portlet" id="docaudittrailparent" > 
    <div id="doccordaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all "> <span class="ui-icon ui-icon-print" onclick="docHistoryOnload('1')"></span>
      <span class="ui-icon ui-icon-plusthick" id="doctrailspan" onclick="docHistoryOnload()"></span>
       Document Audit Trail
      </div>

      <div class="portlet-content" id="docAuditDiv" style="display: none;">
<div>
		<input type="hidden" name="entries" id="docentries" value=<%=paginateSearch4.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesDoc" value=<%=paginateSearch4.getiTotalRows() %> />
		 <table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetDoc">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterDocInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterDocInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
			
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="docAuditHistory" class="displaycdr">
	        <thead id="docTblHead">
	           <tr id="docInfoTr">
	              <th><s:text name="garuda.audittrail.label.category"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.subcategory"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.actiontaken"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.doclink"></s:text></th>
	           </tr>
	           <tr>
	           	 <th>
	           	 <select id="doccat" name="doccat" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="revokedUnitReportList" var="row" status="status">
	           	 		<s:property value="%{#row[0]}"/>
								<option value='<s:property value="%{#row[0]}"/>'><s:property value="%{#row[0]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	 <th>
	           	  <select id="docsubcat" name="docsubcat" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="revokedUnitReportList" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	 
	           	 <th>
	           	 <select id="docusrid" name="docusrid" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="revokedUnitReportList" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	  <th> <button type="button" onclick="showDateDiv(this,'dateDiv14','dateStart14','dateEnd14');"><s:text name="garuda.pendingorder.button.dateRange"/></button> </th>
	           	 <th>
	           	 <select id="docact" name="docact" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="revokedUnitReportList" var="row" status="status">
	           	 		<s:property value="%{#row[4]}"/>
								<option value='<s:property value="%{#row[4]}"/>'><s:property value="%{#row[4]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	
	           	 <th></th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="revokedUnitReportList111" var="row" status="status">
	                <tr>
	                    <td>
	                        <s:property value="%{#row[0]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                     <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                   
	                    <td>
	                        <s:property value="%{#row[4]}"  />
	                    </td>
	                    <td>
	                    <s:if test ="%{#row[4] == 'Revoke'}" >
	                    <a href="#"><img src="images/attachment-icon.png"
								onclick="javascript: return showDocument('<s:property value="%{#row[5]}" />')" /></a>
						</s:if>		
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	            <tr>
					<td colspan="6">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="docAuditDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="showsDocsearchResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="docHistoryOnload" name="bodyOnloadFn"/>
						<jsp:param value="docHistory" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="docaudit" name="widget"/>
						<jsp:param value="34" name="idparam"/>
						<jsp:param value="docinfo_tbl_param" name="docInfoParamId"/>
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>

<s:form>
  <div class="portlet" id="cordaudittrailparent" >
  
    <div id="cordaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss">
    <div
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"> <span class="ui-icon ui-icon-print" onclick="searchReasultOnload('1')"></span>
				<span
				class="ui-icon ui-icon-plusthick" id="audittrailspan" onclick="searchReasultOnload()"></span>
				
				 CBU Information Audit Trail</div>	
      <div class="portlet-content" id="auditTrailHistoryDiv" style="display: none;">
<div id="">
<s:hidden name="cdrCbuPojo.cordID" id="cordID" value="%{cdrCbuPojo.cordID}"/>
	<s:hidden name="cdrCbuPojo.cordID" id="entityId" value="%{cdrCbuPojo.cordID}"/>
		<input type="hidden" name="entries" id="auditentries" value=<%=paginateSearch.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriescbuinfo" value=<%=paginateSearch.getiTotalRows() %> />
		      <s:hidden name="cdrCbuPojo.cordID" id="cdrCbucordID"></s:hidden>
		
		<table align="right">
		<tr align="right" style="vertical-align: top;">
		<td>
			<a href="#" id="resetCBUInfo">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterCbuInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterCbuInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
		</td>
		</tr>
		</table>
		
	     <table cellpadding="0" cellspacing="0" border="0" id="auditTrailHistory" class="displaycdr">
	        <thead id="auditTblHead">
	           <tr id="cbuInfoTr">
	              <th class="auditTblHead"> <s:text name="garuda.audittrail.label.fieldname"></s:text></th>
	              <th class="auditTblHead"><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	              <th class="auditTblHead"><s:text name="garuda.audittrail.label.actiontaken"> </s:text></th>
	              <th class="auditTblHead"><s:text name="garuda.audittrail.label.old"> </s:text></th>
	              <th class="auditTblHead"><s:text name="garuda.audittrail.label.new"> </s:text></th>
	           </tr>
	           <tr>
	           <div>
	           	 <th>
	           	 		<select id="fieldName" name="fieldName" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailAllData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select>   
	           	 </th>
	           	 <th>
	           	 	<select id="userId" name="userId" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailAllData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select>   
	           	 </th>
	           	 <th><button type="button" onclick="showDateDiv(this,'dateDiv','dateStart','dateEnd');"><s:text name="garuda.pendingorder.button.dateRange"/></button></th>
	           	 <th>
	           	 		<select id="actTaken" name="actTaken" width="auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailAllData" var="row" status="status">
								<option value='<s:property value="%{#row[4]}"/>'><s:property value="%{#row[4]}"/></option>
						 </s:iterator>		
						</select> 
	           	 </th>
	           	 <th>
	           	 	<select id="prevValue" name="prevValue" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailAllData" var="row" status="status">
								<option value='<s:property value="%{#row[5]}"/>'><s:property value="%{#row[5]}"/></option>
						 </s:iterator>		
						</select> 
	           	 </th>
	           	 <th>
	           	 <select id="newValue" name="newValue" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailAllData" var="row" status="status">
								<option value='<s:property value="%{#row[6]}"/>'><s:property value="%{#row[6]}"/></option>
						 </s:iterator>		
						</select> 
	           	 </th>
	           	 </div>
	           </tr>
	        </thead>
	        <tbody id="cbuAuditTbody">
	             <s:iterator value="auditTrailRowData111" var="row" status="status">
	                <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[5]}"  />
	                    </td>
	                    <td>
	                        <s:property value="%{#row[6]}"/>
	                    </td>
	                    	                   
	                </tr>
	             </s:iterator>
	        </tbody>
	        
	        <tfoot>
	            <tr>
					<td colspan="6">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="auditTrailHistoryDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="showsRowsearchResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="searchReasultOnload" name="bodyOnloadFn"/>
						<jsp:param value="auditStatus" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="cordaudit" name="widget"/>
						<jsp:param value="21" name="idparam"/>
						<jsp:param value="cbuinfo_tbl_param" name="cbuInfoParamId"/>
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>

<s:form>
<div class="portlet" id="idparent">
    <div id="idaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"> <span class="ui-icon ui-icon-print" onclick="idOnload('1')"></span>
       <span class="ui-icon ui-icon-plusthick" id="idspan" onclick="idOnload()"></span>
       <s:text name="ID Audit Trail" />
      </div>
      <div class="portlet-content"  id="idDiv" style="display: none;">
<div>
		<input type="hidden" name="entries" id="identries" value=<%=paginateSearch13.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesId" value=<%=paginateSearch13.getiTotalRows() %> />
	     <table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetID">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterIdInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterIdInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="idtab" class="displaycdr">
	        <thead id="idHead">
	            <tr id="idInfoTr">
	              <th><s:text name="garuda.audittrail.label.fieldname"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.actiontaken"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.old"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.new"> </s:text></th>
	           </tr>
	            <tr>
	           	 <th>
	           	 <select id="fieldName2" name="fieldName2" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailIdRowData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select>   
	           	 </th>
	           	 <th>
	           	 <select id="userId2" name="userId2" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailIdRowData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	 <th><button type="button" onclick="showDateDiv(this,'dateDiv2','dateStart2','dateEnd2');"><s:text name="garuda.pendingorder.button.dateRange"/></button> </th>
	           	 <th>
	           	 	           	 		<select id="actTaken2" name="actTaken2" width="auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailIdRowData" var="row" status="status">
								<option value='<s:property value="%{#row[4]}"/>'><s:property value="%{#row[4]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 	<select id="prevValue2" name="prevValue2" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailIdRowData" var="row" status="status">
								<option value='<s:property value="%{#row[5]}"/>'><s:property value="%{#row[5]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 <select id="newValue2" name="newValue2" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailIdRowData" var="row" status="status">
								<option value='<s:property value="%{#row[6]}"/>'><s:property value="%{#row[6]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           </tr>
	        </thead>
	        <tbody id="idAuditTbody">
	             <s:iterator value="auditTrailIdRowData111" var="row" status="status">
	              <tr>
	                    <td>
	                       <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[5]}"  />
	                    </td>
	                    <td>
	                        <s:property value="%{#row[6]}"/>
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	             <tr>
					<td colspan="6">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="idDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="idResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="idOnload" name="bodyOnloadFn"/>
						<jsp:param value="id" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="id" name="widget"/>
						<jsp:param value="22" name="idparam"/>
						<jsp:param value="idinfo_tbl_param" name="idInfoParamId"/>
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>
<s:form>
<div class="portlet" id="procInfoparent">
    <div id="procInfoaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"> <span class="ui-icon ui-icon-print" onclick="procInfoOnload('1')"></span>
       <span class="ui-icon ui-icon-plusthick" id="procInfospan" onclick="procInfoOnload()"></span>
       <s:text name="Processing Information Audit Trail" />
      </div>
      <div class="portlet-content"  id="procInfoDiv" style="display: none;">
<div>
		<input type="hidden" name="entries" id="procInfoentries" value=<%=paginateSearch14.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesProc" value=<%=paginateSearch14.getiTotalRows() %> />
	     <table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetProc">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterProcInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterProcInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="procInfotab" class="displaycdr">
	        <thead id="procInfoHead">
	            <tr id="procInfoTr">
	              <th><s:text name="garuda.audittrail.label.fieldname"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.actiontaken"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.old"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.new"> </s:text></th>
	           </tr>
	            <tr>
	           	  <th>
	           	 <select id="fieldName3" name="fieldName3" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailProcInfoRowData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select>   
	           	 </th>
	           	 <th>
	           	 <select id="userId3" name="userId3" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailProcInfoRowData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	 <th><button type="button" onclick="showDateDiv(this,'dateDiv3','dateStart3','dateEnd3');"><s:text name="garuda.pendingorder.button.dateRange"/></button> </th>
	           	 <th>
	           	 	           	 		<select id="actTaken3" name="actTaken3" width="auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailProcInfoRowData" var="row" status="status">
								<option value='<s:property value="%{#row[4]}"/>'><s:property value="%{#row[4]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 	<select id="prevValue3" name="prevValue3" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailProcInfoRowData" var="row" status="status">
								<option value='<s:property value="%{#row[5]}"/>'><s:property value="%{#row[5]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 <select id="newValue3" name="newValue3" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailProcInfoRowData" var="row" status="status">
								<option value='<s:property value="%{#row[6]}"/>'><s:property value="%{#row[6]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailProcInfoRowData111" var="row" status="status">
	              <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[5]}"  />
	                    </td>
	                    <td>
	                        <s:property value="%{#row[6]}"/>
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	             <tr>
					<td colspan="6">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="procInfoDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="procInfoResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="procInfoOnload" name="bodyOnloadFn"/>
						<jsp:param value="procInfo" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="procInfo" name="widget"/>
						<jsp:param value="23" name="idparam"/>
						<jsp:param value="procinfo_tbl_param" name="procInfoParamId"/>
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>

<s:form>
  <div class="portlet" id="entitystatusaudittrailparent" >
    <div id="entitystatusaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-print" onclick="entityStatusOnload('1')"></span>
      <span
				class="ui-icon ui-icon-plusthick" id="entitystatspan" onclick="entityStatusOnload()"></span>
       <s:text name="Licensure Audit Trail" />
      </div>
      <div class="portlet-content" id="entityStatusDiv" style="display: none;">
<div> 
		<input type="hidden" name="entries" id="entityentries" value=<%=paginateSearch1.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesLic" value=<%=paginateSearch1.getiTotalRows() %> />
		
		<table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetLic">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterLicInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterLicInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
		
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="entitystatusaudittrailtab" class="displaycdr">
	        <thead id="entityStatTbl">
	            <tr id="licInfoTr">
	              <th><s:text name="garuda.audittrail.label.fieldname"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.actiontaken"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.old"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.new"> </s:text></th>
	           </tr>
	           <tr>
	           	 <th>
	           	 <select id="fieldName4" name="fieldName4" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailEntityStatusRowData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select>   
	           	 </th>
	           	 <th>
	           	 <select id="userId4" name="userId4" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailEntityStatusRowData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	 <th><button type="button" onclick="showDateDiv(this,'dateDiv4','dateStart4','dateEnd4');"><s:text name="garuda.pendingorder.button.dateRange"/></button> </th>
	           	 <th>
	           	 	           	 		<select id="actTaken4" name="actTaken4" width="auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailEntityStatusRowData" var="row" status="status">
								<option value='<s:property value="%{#row[4]}"/>'><s:property value="%{#row[4]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 	<select id="prevValue4" name="prevValue4" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailEntityStatusRowData" var="row" status="status">
								<option value='<s:property value="%{#row[5]}"/>'><s:property value="%{#row[5]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 <select id="newValue4" name="newValue4" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailEntityStatusRowData" var="row" status="status">
								<option value='<s:property value="%{#row[6]}"/>'><s:property value="%{#row[6]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailEntityStatusRowData111" var="row" status="status">
	              <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[5]}"  />
	                    </td>
	                    <td>
	                        <s:property value="%{#row[6]}"/>
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	             <tr>
					<td colspan="6">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="entityStatusDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="showsEntitysearchResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="entityStatusOnload" name="bodyOnloadFn"/>
						<jsp:param value="entityStatus" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="entitystat" name="widget"/>
						<jsp:param value="24" name="idparam"/>
						<jsp:param value="licinfo_tbl_param" name="licInfoParamId"/>
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>

<s:form>
  <div class="portlet" id="multivaluesparent">
    <div id="multvalaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"> <span class="ui-icon ui-icon-print" onclick="multValuesOnload('1')"></span>
       <span class="ui-icon ui-icon-plusthick" id="multivalspan" onclick="multValuesOnload()"></span>
       <s:text name="Eligibility Audit Trail" />
      </div>
      <div class="portlet-content"  id="multiValuesDiv" style="display: none;">
<div>
		<input type="hidden" name="entries" id="multivaluesentries" value=<%=paginateSearch3.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesElig" value=<%=paginateSearch3.getiTotalRows() %> />
		<table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetElig">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterEligInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterEligInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
	    
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="multivaluestab" class="displaycdr">
	        <thead id="multvalHead">
	            <tr id="eligInfoTr">
	              <th><s:text name="garuda.audittrail.label.fieldname"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.actiontaken"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.old"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.new"> </s:text></th>
	           </tr>
	           <tr>
	           	 <th>
	           	 <select id="fieldName5" name="fieldName5" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailMultipleValuesRowData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select>   
	           	 </th>
	           	 <th>
	           	 <select id="userId5" name="userId5" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailMultipleValuesRowData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	 <th><button type="button" onclick="showDateDiv(this,'dateDiv5','dateStart5','dateEnd5');"><s:text name="garuda.pendingorder.button.dateRange"/></button> </th>
	           	 <th>
	           	 	           	 		<select id="actTaken5" name="actTaken5" width="auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailMultipleValuesRowData" var="row" status="status">
								<option value='<s:property value="%{#row[4]}"/>'><s:property value="%{#row[4]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 	<select id="prevValue5" name="prevValue5" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailMultipleValuesRowData" var="row" status="status">
								<option value='<s:property value="%{#row[5]}"/>'><s:property value="%{#row[5]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 <select id="newValue5" name="newValue5" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailMultipleValuesRowData" var="row" status="status">
								<option value='<s:property value="%{#row[6]}"/>'><s:property value="%{#row[6]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailMultipleValuesRowData111" var="row" status="status">
	              <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[5]}"  />
	                    </td>
	                    <td>
	                        <s:property value="%{#row[6]}"/>
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	             <tr>
					<td colspan="6">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="multiValuesDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="multiValuesResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="multValuesOnload" name="bodyOnloadFn"/>
						<jsp:param value="multvalues" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="multval" name="widget"/>
						<jsp:param value="25" name="idparam"/>
						<jsp:param value="eliginfo_tbl_param" name="eligInfoParamId"/>
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>
<s:form>
<div class="portlet" id="reviewparent">
    <div id="reviewaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"> <span class="ui-icon ui-icon-print" onclick="reviewOnload('1')"></span>
       <span class="ui-icon ui-icon-plusthick" id="reviewspan" onclick="reviewOnload()"></span>
       <s:text name="Final Review Audit Trail" />
      </div>
      <div class="portlet-content"  id="reviewDiv" style="display: none;">
<div>
		<input type="hidden" name="entries" id="reviewentries" value=<%=paginateSearch5.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesRev" value=<%=paginateSearch5.getiTotalRows() %> />
		 <table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetReview">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterRevInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterRevInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
		
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="reviewtab" class="displaycdr">
	        <thead id="reviewHead">
	            <tr id="revInfoTr">
	              <th><s:text name="garuda.audittrail.label.fieldname"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.actiontaken"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.old"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.new"> </s:text></th>
	           </tr>
	            <tr>
	           	  <th>
	           	 <select id="fieldName6" name="fieldName6" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailReviewRowData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select>   
	           	 </th>
	           	 <th>
	           	 <select id="userId6" name="userId6" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailReviewRowData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	 <th><button type="button" onclick="showDateDiv(this,'dateDiv6','dateStart6','dateEnd6');"><s:text name="garuda.pendingorder.button.dateRange"/></button> </th>
	           	 <th>
	           	 	           	 		<select id="actTaken6" name="actTaken6" width="auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailReviewRowData" var="row" status="status">
								<option value='<s:property value="%{#row[4]}"/>'><s:property value="%{#row[4]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 	<select id="prevValue6" name="prevValue6" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailReviewRowData" var="row" status="status">
								<option value='<s:property value="%{#row[5]}"/>'><s:property value="%{#row[5]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 <select id="newValue6" name="newValue6" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailReviewRowData" var="row" status="status">
								<option value='<s:property value="%{#row[6]}"/>'><s:property value="%{#row[6]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailReviewRowData111" var="row" status="status">
	              <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[5]}"  />
	                    </td>
	                    <td>
	                        <s:property value="%{#row[6]}"/>
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	             <tr>
					<td colspan="6">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="reviewDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="reviewResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="reviewOnload" name="bodyOnloadFn"/>
						<jsp:param value="cbureview" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="review" name="widget"/>
						<jsp:param value="26" name="idparam"/>
						<jsp:param value="revinfo_tbl_param" name="revInfoParamId"/>
						
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>
<s:form>
<div class="portlet" id="hlaparent">
    <div id="hlaaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"> <span class="ui-icon ui-icon-print" onclick="hlaOnload('1')"></span>
       <span class="ui-icon ui-icon-plusthick" id="hlaspan" onclick="hlaOnload()"></span>
       <s:text name="HLA Audit Trail" />
      </div>
      <div class="portlet-content"  id="hlaDiv" style="display: none;">
<div>
		<input type="hidden" name="entries" id="hlaentries" value=<%=paginateSearch6.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesHla" value=<%=paginateSearch6.getiTotalRows() %> />
		<table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetHla">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterHlaInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterHlaInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
		
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="hlatab" class="displaycdr">
	        <thead id="hlaHead">
	            <tr id="hlaInfoTr">
	              <th><s:text name="garuda.audittrail.label.fieldname"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.actiontaken"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.old"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.new"> </s:text></th>
	           </tr>
	            <tr>
	           	  <th>
	           	 <select id="fieldName7" name="fieldName7" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailHlaRowData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select>   
	           	 </th>
	           	 <th>
	           	 <select id="userId7" name="userId7" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailHlaRowData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	 <th><button type="button" onclick="showDateDiv(this,'dateDiv7','dateStart7','dateEnd7');"><s:text name="garuda.pendingorder.button.dateRange"/></button> </th>
	           	 <th>
	           	 	           	 		<select id="actTaken7" name="actTaken7" width="auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailHlaRowData" var="row" status="status">
								<option value='<s:property value="%{#row[4]}"/>'><s:property value="%{#row[4]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 	<select id="prevValue7" name="prevValue7" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailHlaRowData" var="row" status="status">
								<option value='<s:property value="%{#row[5]}"/>'><s:property value="%{#row[5]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 <select id="newValue7" name="newValue7" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailHlaRowData" var="row" status="status">
								<option value='<s:property value="%{#row[6]}"/>'><s:property value="%{#row[6]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailHlaRowData111" var="row" status="status">
	              <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[5]}"  />
	                    </td>
	                    <td>
	                        <s:property value="%{#row[6]}"/>
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	             <tr>
					<td colspan="6">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="hlaDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="hlaResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="hlaOnload" name="bodyOnloadFn"/>
						<jsp:param value="hla" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="hla" name="widget"/>
						<jsp:param value="27" name="idparam"/>
						<jsp:param value="hlainfo_tbl_param" name="hlaInfoParamId"/>
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>

<s:form>
<div class="portlet" id="pathlaparent">
    <div id="pathlaaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"> <span class="ui-icon ui-icon-print" onclick="pathlaOnload('1')"></span>
       <span class="ui-icon ui-icon-plusthick" id="pathlaspan" onclick="pathlaOnload()"></span>
       <s:text name="Patient HLA Audit Trail" />
      </div>
      <div class="portlet-content"  id="pathlaDiv" style="display: none;">
<div>
		<input type="hidden" name="entries" id="pathlaentries" value=<%=paginateSearch16.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriespatHla" value=<%=paginateSearch16.getiTotalRows() %> />
		<table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetpatHla">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterPatHlaInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterPatHlaInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
		
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="pathlatab" class="displaycdr">
	        <thead id="pathlaHead">
	            <tr id="pathlaInfoTr">
	              <th><s:text name="garuda.audittrail.label.fieldname"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.actiontaken"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.old"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.new"> </s:text></th>
	           </tr>
	            <tr>
	           	  <th>
	           	 <select id="fieldName12" name="fieldName12" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailPatHlaRowData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select>   
	           	 </th>
	           	 <th>
	           	 <select id="userId12" name="userId12" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailPatHlaRowData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	 <th><button type="button" onclick="showDateDiv(this,'dateDiv15','dateStart15','dateEnd15');"><s:text name="garuda.pendingorder.button.dateRange"/></button> </th>
	           	 <th>
	           	 	           	 		<select id="actTaken12" name="actTaken12" width="auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailPatHlaRowData" var="row" status="status">
								<option value='<s:property value="%{#row[4]}"/>'><s:property value="%{#row[4]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 	<select id="prevValue12" name="prevValue12" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailPatHlaRowData" var="row" status="status">
								<option value='<s:property value="%{#row[5]}"/>'><s:property value="%{#row[5]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 <select id="newValue12" name="newValue12" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailPatHlaRowData" var="row" status="status">
								<option value='<s:property value="%{#row[6]}"/>'><s:property value="%{#row[6]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailPatHlaRowData111" var="row" status="status">
	              <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[5]}"  />
	                    </td>
	                    <td>
	                        <s:property value="%{#row[6]}"/>
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	             <tr>
					<td colspan="6">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="pathlaDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="pathlaResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="pathlaOnload" name="bodyOnloadFn"/>
						<jsp:param value="pathla" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="pathla" name="widget"/>
						<jsp:param value="35" name="idparam"/>
						<jsp:param value="pathlainfo_tbl_param" name="pathlaInfoParamId"/>
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>


<s:form>
<div class="portlet" id="labsummary">
    <div id="labsummaryaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"> <span class="ui-icon ui-icon-print" onclick="labOnload('1')"></span>
      <span class="ui-icon ui-icon-plusthick" id="labspan" onclick="labOnload()"></span>
       <s:text name="Lab Summary Audit Trail" />
      </div>
      <div class="portlet-content"  id="labDiv" style="display: none;">
<div>
		<input type="hidden" name="entries" id="labentries" value=<%=paginateSearch7.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesLab" value=<%=paginateSearch7.getiTotalRows() %> />
		<table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetLab">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterLabInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterLabInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
		
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="labtab" class="displaycdr">
	        <thead id="labHead">
	            <tr id="labInfoTr">
	              <th><s:text name="garuda.audittrail.label.fieldname"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.actiontaken"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.old"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.new"> </s:text></th>
	           </tr>
	           <tr>
	           	  <th>
	           	 <select id="fieldName8" name="fieldName8" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailLabSummaryRowData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select>   
	           	 </th>
	           	 <th>
	           	 <select id="userId8" name="userId8" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailLabSummaryRowData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	 <th><button type="button" onclick="showDateDiv(this,'dateDiv8','dateStart8','dateEnd8');"><s:text name="garuda.pendingorder.button.dateRange"/></button> </th>
	           	 <th>
	           	 	           	 		<select id="actTaken8" name="actTaken8" width="auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailLabSummaryRowData" var="row" status="status">
								<option value='<s:property value="%{#row[4]}"/>'><s:property value="%{#row[4]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 	<select id="prevValue8" name="prevValue8" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailLabSummaryRowData" var="row" status="status">
								<option value='<s:property value="%{#row[5]}"/>'><s:property value="%{#row[5]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 <select id="newValue8" name="newValue8" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailLabSummaryRowData" var="row" status="status">
								<option value='<s:property value="%{#row[6]}"/>'><s:property value="%{#row[6]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailLabSummaryRowData111" var="row" status="status">
	              <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[5]}"  />
	                    </td>
	                    <td>
	                        <s:property value="%{#row[6]}"/>
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	             <tr>
					<td colspan="6">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="labDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="labResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="labOnload" name="bodyOnloadFn"/>
						<jsp:param value="lab" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="labsummary" name="widget"/>
						<jsp:param value="28" name="idparam"/>
						<jsp:param value="labinfo_tbl_param" name="labInfoParamId"/>
						
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>

</s:form>
<s:form>
<div class="portlet" id="notesparent">
    <div id="cbnotesaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"> <span class="ui-icon ui-icon-print" onclick="notesOnload('1')"></span>
       <span class="ui-icon ui-icon-plusthick" id="notesspan" onclick="notesOnload()"></span>
       <s:text name="Clinical Notes Audit Trail" />
      </div>
      <div class="portlet-content"  id="notesDiv" style="display: none;">
<div>
		<input type="hidden" name="entries" id="notesentries" value=<%=paginateSearch8.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesCn" value=<%=paginateSearch8.getiTotalRows() %> />
		<table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetClnote">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterCnInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterCnInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
		
		
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="notestab" class="displaycdr">
	        <thead id="notesHead">
	            <tr id="cnInfoTr">
	              <th><s:text name="garuda.audittrail.label.fieldname"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.actiontaken"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.old"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.new"> </s:text></th>
	           </tr>
	           <tr>
	           	  <th>
	           	 <select id="fieldName9" name="fieldName9" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailNotesRowData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select>   
	           	 </th>
	           	 <th>
	           	 <select id="userId9" name="userId9" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailNotesRowData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	 <th><button type="button" onclick="showDateDiv(this,'dateDiv9','dateStart9','dateEnd9');"><s:text name="garuda.pendingorder.button.dateRange"/></button> </th>
	           	 <th>
	           	 	           	 		<select id="actTaken9" name="actTaken9" width="auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailNotesRowData" var="row" status="status">
								<option value='<s:property value="%{#row[4]}"/>'><s:property value="%{#row[4]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 	<select id="prevValue9" name="prevValue9" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailNotesRowData" var="row" status="status">
								<option value='<s:property value="%{#row[5]}"/>'><s:property value="%{#row[5]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 <select id="newValue9" name="newValue9" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailNotesRowData" var="row" status="status">
								<option value='<s:property value="%{#row[6]}"/>'><s:property value="%{#row[6]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailNotesRowData111" var="row" status="status">
	              <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property escapeHtml="false" value="%{#row[5]}"  />
	                    </td>
	                    <td>
	                        <s:property escapeHtml="false" value="%{#row[6]}"/>
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	             <tr>
					<td colspan="6">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="notesDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="notesResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="notesOnload" name="bodyOnloadFn"/>
						<jsp:param value="notes" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="cbnotes" name="widget"/>
						<jsp:param value="29" name="idparam"/>
						<jsp:param value="cninfo_tbl_param" name="cnInfoParamId"/>
						
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>

<s:form>
<div class="portlet" id="formsparent">
    <div id="formsaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"> <span class="ui-icon ui-icon-print" onclick="formsOnload('1')"></span>
       <span class="ui-icon ui-icon-plusthick" id="formsspan" onclick="formsOnload()"></span>
       <s:text name="Infectious Disease Markers Audit Trail" />
      </div>
      <div class="portlet-content"  id="formsDiv" style="display: none;">
<div>
		<input type="hidden" name="entries" id="formsentries" value=<%=paginateSearch9.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesIdm" value=<%=paginateSearch9.getiTotalRows() %> />
		 <table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetIDM">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterIdmInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterIdmInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="formstab" class="displaycdr">
	        <thead id="formsHead">
	            <tr id="idmInfoTr">
	              <th>Form Name</th>
	              <th>Form Version</th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	           </tr>
	            <tr>
	              <th>
	              <select id="formname1" name="formname1" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailFormsRowData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select> 
	              
	              </th>
	              <th>
	               <select id="fversion1" name="fversion1" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailFormsRowData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select> 
	              </th>
	              <th>
	              
	              <select id="fuserid1" name="fuserid1" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailFormsRowData" var="row" status="status">
	           	 		<s:property value="%{#row[3]}"/>
								<option value='<s:property value="%{#row[3]}"/>'><s:property value="%{#row[3]}"/></option>
						 </s:iterator>		
						</select> 
	              </th>
	              <th>
	              <button type="button" onclick="showDateDiv(this,'dateDiv12','dateStart12','dateEnd12');"><s:text name="garuda.pendingorder.button.dateRange"/></button> 
	              </th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailFormsRowData111" var="row" status="status">
	              <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	             <tr>
					<td colspan="4">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="formsDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="formsResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="formsOnload" name="bodyOnloadFn"/>
						<jsp:param value="forms" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="forms" name="widget"/>
						<jsp:param value="32" name="idparam"/>
						<jsp:param value="idminfo_tbl_param" name="idmInfoParamId"/>
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>

<s:form>
<div class="portlet" id="formshhsparent">
    <div id="formshssaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"> <span class="ui-icon ui-icon-print" onclick="formshhsOnload('1')"></span>
       <span class="ui-icon ui-icon-plusthick" id="formshhsspan" onclick="formshhsOnload()"></span>
       <s:text name="Health History Screening Audit Trail" />
      </div>
      <div class="portlet-content"  id="formshhsDiv"  style="display: none;"> 
<div>
		<input type="hidden" name="entries" id="formshhsentries" value=<%=paginateSearch12.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesHhs" value=<%=paginateSearch12.getiTotalRows() %> />
		<table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetHHS">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterHhsInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterHhsInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
		
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="formshhstab" class="displaycdr">
	        <thead id="formshhsHead">
	            <tr id="hhsInfoTr">
	              <th>Form Name</th>
	              <th>Form Version</th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	           </tr>
	            <tr>
	               <th>
	              <select id="formname2" name="formname2" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailFormshssRowData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select> 
	              
	              </th>
	              <th>
	               <select id="fversion2" name="fversion2" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailFormshssRowData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select> 
	              </th>
	              <th>
	              
	              <select id="fuserid2" name="fuserid2" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailFormshssRowData" var="row" status="status">
	           	 		<s:property value="%{#row[3]}"/>
								<option value='<s:property value="%{#row[3]}"/>'><s:property value="%{#row[3]}"/></option>
						 </s:iterator>		
						</select> 
	              </th>
	              <th>
	              <button type="button" onclick="showDateDiv(this,'dateDiv13','dateStart13','dateEnd13');"><s:text name="garuda.pendingorder.button.dateRange"/></button> 
	              </th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailFormshssRowData111" var="row" status="status">
	              <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[3]}"/>
	                    </td>
	                    
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	             <tr>
					<td colspan="4">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="formshhsDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="formshhsResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="formshhsOnload" name="bodyOnloadFn"/>
						<jsp:param value="formshhs" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="formshhs" name="widget"/>
						<jsp:param value="33" name="idparam"/>
						<jsp:param value="hhsinfo_tbl_param" name="hhsInfoParamId"/>
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>

<s:form>
<div class="portlet" id="mcparent">
    <div id="mcaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"> <span class="ui-icon ui-icon-print" onclick="mcOnload('1')"></span>
       <span class="ui-icon ui-icon-plusthick" id="mcspan" onclick="mcOnload()"></span>
       <s:text name="Minimum Criteria Audit Trail" />
      </div>
      <div class="portlet-content"  id="mcDiv" style="display: none;">
<div>
		<input type="hidden" name="entries" id="mcentries" value=<%=paginateSearch15.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesMc" value=<%=paginateSearch15.getiTotalRows() %> />
		
		 <table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetMC">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterMcInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterMcInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="mctab" class="displaycdr">
	        <thead id="mcHead">
	            <tr id="mcInfoTr">
	              <th><s:text name="garuda.audittrail.label.fieldname"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.actiontaken"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.old"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.new"> </s:text></th>
	           </tr>
	            <tr>
	           	  <th>
	           	 <select id="fieldName10" name="fieldName10" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailMinCritInfoRowData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select>   
	           	 </th>
	           	 <th>
	           	 <select id="userId10" name="userId10" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailMinCritInfoRowData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	 <th><button type="button" onclick="showDateDiv(this,'dateDiv10','dateStart10','dateEnd10');"><s:text name="garuda.pendingorder.button.dateRange"/></button> </th>
	           	 <th>
	           	 	           	 		<select id="actTaken10" name="actTaken10" width="auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailMinCritInfoRowData" var="row" status="status">
								<option value='<s:property value="%{#row[4]}"/>'><s:property value="%{#row[4]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 	<select id="prevValue10" name="prevValue10" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailMinCritInfoRowData" var="row" status="status">
								<option value='<s:property value="%{#row[5]}"/>'><s:property value="%{#row[5]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 <select id="newValue10" name="newValue10" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailMinCritInfoRowData" var="row" status="status">
								<option value='<s:property value="%{#row[6]}"/>'><s:property value="%{#row[6]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailMinCritInfoRowData111" var="row" status="status">
	              <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                         <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property escapeHtml="false" value="%{#row[5]}"  />
	                    </td>
	                    <td>
	                        <s:property escapeHtml="false" value="%{#row[6]}"/>
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	             <tr>
					<td colspan="6">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="mcDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="mcResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="mcOnload" name="bodyOnloadFn"/>
						<jsp:param value="mc" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="mc" name="widget"/>
						<jsp:param value="30" name="idparam"/>
						<jsp:param value="mcinfo_tbl_param" name="mcInfoParamId"/>
						
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>

<s:form>
<div class="portlet" id="pfparent">
    <div id="pfaudittrail" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all auditCss" >
      <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"> <span class="ui-icon ui-icon-print" onclick="pfOnload('1')"></span>
       <span class="ui-icon ui-icon-plusthick" id="pfspan" onclick="pfOnload()"></span>
       <s:text name="Order Details Audit Trail" />
      </div>
      <div class="portlet-content"  id="pfDiv" style="display: none;">
<div>
		<input type="hidden" name="entries" id="pfentries" value=<%=paginateSearch11.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesOr" value=<%=paginateSearch11.getiTotalRows() %> />
		
		<table align="right">
			<tr align="right" style="vertical-align: top;">
				<td>
			<a href="#" id="resetOrder">Reset Sort</a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_FilterOrInfo()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterOrInfo()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
			</td>
			</tr>
		</table>
	     <table width="100%" cellpadding="0" cellspacing="0" border="0" id="pftab" class="displaycdr">
	        <thead id="pfHead">
	            <tr id="orInfoTr">
	              <th><s:text name="garuda.audittrail.label.fieldname"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.userid"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.datetime"></s:text></th>
	              <th><s:text name="garuda.audittrail.label.actiontaken"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.old"> </s:text></th>
	              <th><s:text name="garuda.audittrail.label.new"> </s:text></th>
	           </tr>
	            <tr>
	           	  <th>
	           	 <select id="fieldName11" name="fieldName11" style = "width:auto;" width= "auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailPfRowData" var="row" status="status">
	           	 		<s:property value="%{#row[1]}"/>
								<option value='<s:property value="%{#row[1]}"/>'><s:property value="%{#row[1]}"/></option>
						 </s:iterator>		
						</select>   
	           	 </th>
	           	 <th>
	           	 <select id="userId11" name="userId11" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailPfRowData" var="row" status="status">
	           	 		<s:property value="%{#row[2]}"/>
								<option value='<s:property value="%{#row[2]}"/>'><s:property value="%{#row[2]}"/></option>
						 </s:iterator>		
						</select>  
	           	 </th>
	           	 <th><button type="button" onclick="showDateDiv(this,'dateDiv11','dateStart11','dateEnd11');"><s:text name="garuda.pendingorder.button.dateRange"/></button> </th>
	           	 <th>
	           	 	           	 		<select id="actTaken11" name="actTaken11" width="auto">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailPfRowData" var="row" status="status">
								<option value='<s:property value="%{#row[4]}"/>'><s:property value="%{#row[4]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 	<select id="prevValue11" name="prevValue11" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailPfRowData" var="row" status="status">
								<option value='<s:property value="%{#row[5]}"/>'><s:property value="%{#row[5]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           	 <th>
	           	 	           	 <select id="newValue11" name="newValue11" style = "width:auto;">
	           	 		<option value="-1">Select</option>
	           	 		<s:iterator value="auditTrailPfRowData" var="row" status="status">
								<option value='<s:property value="%{#row[6]}"/>'><s:property value="%{#row[6]}"/></option>
						 </s:iterator>		
						</select> 
	           	 
	           	 </th>
	           </tr>
	        </thead>
	        <tbody>
	             <s:iterator value="auditTrailPfRowData111" var="row" status="status">
	              <tr>
	                    <td>
	                        <s:property value="%{#row[1]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[2]}"/>
	                    </td>
	                    <td>
	                         <s:property value="%{#row[3]}"/>
	                    </td>
	                    <td>
	                        <s:property value="%{#row[4]}"/>
	                    </td>
	                    <td>
	                        <s:property escapeHtml="false" value="%{#row[5]}"  />
	                    </td>
	                    <td>
	                        <s:property escapeHtml="false" value="%{#row[6]}"/>
	                    </td>
	                </tr>
	             </s:iterator>
	        </tbody>
	        <tfoot>
	             <tr>
					<td colspan="6">
						<jsp:include page="../paginationFooter.jsp">
						<jsp:param value="pfDiv" name="divName"/>
						<jsp:param value="trailForm" name="formName"/>
						<jsp:param value="pfResults" name="showsRecordId"/>
						<jsp:param value="revokedUnitReportsWidget" name="url"/>
						<jsp:param value="pfOnload" name="bodyOnloadFn"/>
						<jsp:param value="pf" name="paginateSearch"/>
						<jsp:param value="cordID" name="cordID"/>
						<jsp:param value="entityId" name="entityId"/>
						<jsp:param value="pf" name="widget"/>
						<jsp:param value="31" name="idparam"/>
						<jsp:param value="orinfo_tbl_param" name="orInfoParamId"/>
						
						</jsp:include>
					</td>
				</tr>
	        </tfoot>
	     </table>	
</div></div></div></div>
</s:form>
<div>&nbsp;</div>
<div>

<s:form>
<table align="center">
<tr><td align="center"><input type="button" onclick="closeModalAudit();" value="<s:text name="garuda.common.close"/>"/></td></tr>
</table>

</s:form>
</div>
</div>
