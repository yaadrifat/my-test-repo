<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<jsp:include page="../cb_track_session_logging.jsp" />
<script type="text/javascript">

$j(".numeric").numeric();
$j(".integer").numeric(false, function() { alert("<s:text name="garuda.common.procedure.integersalert"/>"); this.value = ""; this.focus(); });
$j(".positive").numeric({ negative: false }, function() { alert("<s:text name="garuda.common.procedure.negativevaluesalert"/>"); this.value = ""; this.focus(); });
$j(".positivecheck").numeric();
$j(".positive-integer").numeric({ decimal: false, negative: false }, function() { alert("<s:text name="garuda.common.procedure.positiveintegersalert"/>"); this.value = ""; this.focus(); });
/*$j("#maxValueAdd").decimalMask({separator: ".",decSize: 1,intSize: 4});
$j("#maxValue2Add").decimalMask({separator: ".",decSize: 1,intSize: 4});
$j("#thouUnitPerMlHeparin").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#thouUnitPerMlHeparinper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#fiveUnitPerMlHeparinper").decimalMask({separator: ".",decSize: 1,intSize: 1});
$j("#fiveUnitPerMlHeparin").decimalMask({separator: ".",decSize: 1,intSize: 1});
$j("#tenUnitPerMlHeparin").decimalMask({separator: ".",decSize: 1,intSize: 1});
$j("#tenUnitPerMlHeparinper").decimalMask({separator: ".",decSize: 1,intSize: 1});
$j("#sixPerHydroxyethylStarch").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#sixPerHydroxyethylStarchper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#cpda").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#cpdaper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#cpd").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#cpdper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#acd").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#acdper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#otherAnticoagulantAdd").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#otherAnticoagulantAddper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#hunPerDmso").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#hunPerDmsoper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#hunPerGlycerol").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#hunPerGlycerolper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#tenPerDextran_40").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#tenPerDextran_40per").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#fivePerHumanAlbu").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#fivePerHumanAlbuper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#twenFiveHumAlbu").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#twenFiveHumAlbuper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#plasmalyte").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#plasmalyteper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#othCryoprotectantAdd").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#othCryoprotectantAddper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#fivePerDextrose").decimalMask({separator: ".",decSize: 1,intSize: 3});
$j("#fivePerDextroseper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#pointNinePerNacl").decimalMask({separator: ".",decSize: 1,intSize: 3});
$j("#pointNinePerNaclper").decimalMask({separator: ".",decSize: 1,intSize: 2});
$j("#othDiluentsAdd").decimalMask({separator: ".",decSize: 1,intSize: 3});
$j("#othDiluentsAddper").decimalMask({separator: ".",decSize: 1,intSize: 2});
*/
$j(function(){
	$j(".decimalPoint").keydown(function(e){
		var val = $j(this).val();
		var maxLen = $j(this).attr("maxlength");
		if(val.length == 0) {
		    if(e.keyCode == 190 || e.keyCode == 110)
			return false;
		}

		 /* if(e.keyCode!=8 && e.keyCode!=46)
		  {
			  if(val.length == maxLen -1){
				  if(e.keyCode != null)
				  {}
				  else 	
				  	$j(this).val(val+"0");
				  
			  } 	 	
			  if(val.length == maxLen-2){
				  if(e.which!=190 || e.which != 110){
					   $j(this).val(val+".");
				  }
			  }
			  
		  }*/
	});
});

function eitherOne(id){
	if($j("#"+id).val() != '') {
		$j("#"+id+"per").attr('disabled','disabled');
	}
	else
	{
	   $j("#"+id+"per").removeAttr('disabled');
	}
}

function eitherOneOther(id){
	var id1 = id.substring(0, id.length - 3)
	if($j("#"+id).val() != '') {
		$j("#"+id1).attr('disabled','disabled');
	}
	else
	{
		$j("#"+id1).removeAttr('disabled');
	}
}




function checkFormat(val,id)
{
	if(val == null || val == "")
		return false;
	if(val.indexOf('.') != -1)
	{}
	else
	{
		$j("#"+id).val(val+".0");
	}
	if(val.charAt(val.length -1) == '.'){
		$j("#"+id).val(val+"0");
	}
	if(val.charAt(val.length -3) == '.'){
		$j("#"+id).val(val.slice(0,-1)); 
	}
	if(val.charAt(val.length -4) == '.'){
		$j("#"+id).val(val.slice(0,-2)); 
	}
	if(val.charAt(val.length -5) == '.'){
		$j("#"+id).val(val.slice(0,-3)); 
	}
	getOtherAnticoagulant();
	getOtherCyroprotectant();
	getOtherDiluents();
}

function changeProceedNo(){
	var procName = $j("#addProcNameAdd").val();
	var cbbId = $j("#cbbId").val(); 

	if (procName != '' && cbbId != '') {
		refreshDiv('checkForProcName?statusUpdate=False&ProcName='+trim(procName)+'&cbbId='+cbbId,'procProcExistsDiv','addNewCBBProcedureForm1');
		if($j("#procNameExistsFlag").val()=='1'){
			alert("<s:text name="garuda.cbu.procedure.procNameExistsAlert"/>")
			$j("#addProcNameAdd").val("");
			$j("#addProcNameAdd").focus();
			$j("#ProceedNo").html("");
			
		}else{
			$j("#ProceedNo").html(procName);
		}
	}	
}

function showhelpmsg(id){
	msg=$j('#'+id).html();
	overlib(msg);
	$j('#overDiv').css('z-index','99999');
}
function refreshDiv(url,divname,formId){
	//$('.progress-indicator').css( 'display', 'block' );
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
  
}

function validateProcNo(procno){
	if(procno.length==2){
	
	$j("#addProcNoAdd").val(procno+".");
	}
	else{
		$j("#addProcNoAdd").val(procno);
	}
}

$j(function() {
	var frozenProd = new Array("thouUnitPerMlHeparin","cpda","fiveUnitPerMlHeparin","cpd","tenUnitPerMlHeparin",
			"acd","sixPerHydroxyethylStarch","otherAnticoagulantAdd","hunPerDmso","twenFiveHumAlbu","hunPerGlycerol","plasmalyte",
			"tenPerDextran_40","othCryoprotectantAdd","fivePerHumanAlbu","fivePerDextrose","pointNinePerNacl","othDiluentsAdd");
	for(i=0;i<frozenProd.length;i++){
		eitherOneEdit(frozenProd[i]);
		eitherOneEditOther(frozenProd[i]+"per");
	}
	/*var today = new Date();
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();
	var h=today.getHours();
	var mn=today.getMinutes()+1;
	$j( "#datepicker10" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker11" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker12" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker13" ).datepicker({dateFormat: 'M dd, yy',minDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	*/
	getDatePic();
	$j("#fkIfAutomatedAdd").attr("disabled","disabled");
	$j("#fkIfAutomatedAdd").attr("readonly","true");
	//$j("#otherAutomatedDivAdd").hide();
	//$j("#otherProdModiDivAdd").hide();
	//$j("#otherFreezerManufacDivAdd").hide();
	//$j("#ifBagSelectedDivAdd").hide();
	//$j("#otherAnticoagulantDivAddition").hide();
	//$j("#otherCryoprotectantDivAdd").hide();
	//$j("#otherDiluentsDivAdd").hide();
	//$j("#ifautomatedspan").hide();
	//$j("#otherFrozenInDivAdd").hide();
	//$j("#addProcNoAdd").decimalMask({separator: ".",decSize: 2,intSize: 2});
	//$j("#bagTypeDivAdd").hide();
	//$j("#bag12TypeDivAdd").hide();
	//$j("#otherBagTypeDivAdd").hide();
	
	getOtherAutomated();
	getOtherProdModi();
	getOtherFreezerManufac();
	getOtherAnticoagulant();
	getOtherCyroprotectant();
	getOtherDiluents();
	getIfAutomated();
	getIfBagSelected_onload();
	if ($j("#fkFrozenInAdd").val()==$j("#pkFrozenInBagAdd").val()) {
		//alert("fkFrozenInAdd"+$j("#fkFrozenInAdd").val()+"pkFrozenInBagAdd"+$j("#pkFrozenInBagAdd").val())
		var v1=$j("#fkNoOfBagsAdd").val();
		getNoOfBagsSelected_onload(v1);
		var v2=$j('#fkBag1TypeAdd').val();
		if(v2!="" && v2!=null){
		    getOtherBagTypeSelected(v2);
		}
		var v3=$j('#fkBag2TypeAdd').val();
		if(v3!="" && v3!=null){
			getOtherBagType2Selected(v3);
		}
	}
	$j("#cbbProcedureAddnewdiv").show();
	$j(".hasDatepicker").each(function(){
		if(!$j(this).is(':visible'))
			$j(this).next('img').hide();
	});
});


jQuery(function() {
	  jQuery('#datepicker11, #datepicker10').datepicker('option', {
	    beforeShow: customRange
	  }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	});

	function customRange(input) {
		if (input.id == 'datepicker11') {
		    var minTestDate=new Date($j("#datepicker10").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		//	alert("minTestDate::"+minTestDate);
			if($j("#datepicker10").val()!="" && $j("#datepicker10").val()!=null){
			  $j('#datepicker11').datepicker( "option", "minDate", new Date(y1,m1,d1));
			}else{
			$j('#datepicker11').datepicker( "option", "minDate", "");
			}
		  } else if (input.id == 'datepicker10') {
			  	var minTestDate=new Date($j("#datepicker11").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
			//	alert("minTestDate::"+minTestDate);
				if($j("#datepicker11").val()!="" && $j("#datepicker11").val()!=null){
			  		$j('#datepicker10').datepicker( "option", "maxDate", new Date(y1,m1,d1));
				}else{
				$j('#datepicker10').datepicker( "option", "maxDate", "");
			}
		  }
	}



	jQuery(function() {
		  jQuery('#datepicker13, #datepicker12').datepicker('option', {
		    beforeShow: customRange1
		  }).focus(function(){
				if(!$j("#ui-datepicker-div").is(":visible"))
					customRange1(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
		});

		function customRange1(input) {
			if (input.id == 'datepicker13') {
			    var minTestDate=new Date($j("#datepicker12").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
				//alert("minTestDate::"+minTestDate);
				if($j("#datepicker12").val()!="" && $j("#datepicker12").val()!=null){
				  $j('#datepicker13').datepicker( "option", "minDate", new Date(y1,m1,d1));
				}else{
				$j('#datepicker13').datepicker( "option", "minDate", "");
				}
			  } else if (input.id == 'datepicker12') {
				  var minTestDate=new Date($j("#datepicker13").val()); 	 
					var d1 = minTestDate.getDate();
					var m1 = minTestDate.getMonth();
					var y1 = minTestDate.getFullYear();
					if($j("#datepicker13").val()!="" && $j("#datepicker13").val()!=null){
				  		$j('#datepicker12').datepicker( "option", "maxDate", new Date(y1,m1,d1));
					}else{
						$j('#datepicker12').datepicker( "option", "maxDate", "");
					}
				//	alert("minTestDate::"+minTestDate);
			  }
		}
		


function getIfAutomated(){
	if($j("#fkProcMethIdAdd").val()==$j("#pkProcMethodIdAdd").val()){
		$j("#ifautomatedspan").show();
		$j("#fkIfAutomatedAdd").removeAttr("disabled");
		$j("#fkIfAutomatedAdd").removeAttr("readonly");
		}
	else{
		$j("#ifautomatedspan").hide();
		$j("#fkIfAutomatedAdd").val("");
		$j("#fkIfAutomatedAdd").attr("disabled","enabled");
		$j("#fkIfAutomatedAdd").attr("readonly","false");
		$j("#otherProcessingAdd").val("");
		$j("#otherAutomatedDivAdd").hide();
		}
}

function getOtherAutomated(){
	if($j("#fkIfAutomatedAdd").val()==$j("#pkIfAutomatedOtherAdd").val()){
		$j("#otherAutomatedDivAdd").show();
		}
	else{
		$j("#otherProcessingAdd").val("");
		$j("#otherAutomatedDivAdd").hide();
		}
}
function getOtherProdModi(){
	if($j("#fkProductModificationAdd").val()==$j("#pkProdModiOtherAdd").val()){
		$j("#otherProdModiDivAdd").show();
		}
	else{
		$j("#otherProdModiAdd").val("");
		$j("#otherProdModiDivAdd").hide();
		}
}

function getOtherFreezerManufac(){
	if($j("#fkFreezManufacAdd").val()==$j("#pkFreezManuOtherAdd").val()){
		$j("#otherFreezerManufacDivAdd").show();
		}
	else{
		$j("#otherFreezManufacAdd").val("");
		$j("#otherFreezerManufacDivAdd").hide();
		}
}

function getIfBagSelected() {
	if ($j("#fkFrozenInAdd").val()==$j("#pkFrozenInBagAdd").val()) {
		$j("#ifBagSelectedDivAdd").show();
		$j("#otherFrozenInDivAdd").hide();
		$j("#otherFrozenContAdd").val("");
		$j("#bag12TypeDivAdd").hide();
		$j("#bagTypeDivAdd").hide();
		$j("#otherBagTypeDivAdd").hide();
		$j("#fkBag1TypeAdd").val("");
		$j("#fkBag2TypeAdd").val("");
	}
	else if($j("#fkFrozenInAdd").val()==$j("#pkFrozenInOtherAdd").val()){
		$j("#otherFrozenInDivAdd").show();
		$j("#fkBagTypeAdd").val("");
		$j("#cryoBagManufacAdd").val("");
		$j("#maxValueAdd").val("");
		$j("#fkBag1TypeAdd").val("");
		$j("#fkBag2TypeAdd").val("");
		$j('#cryoBag2ManufacAdd').val("");
		$j('#maxValue2Add').val("");
		$j('#fkNoOfBagsAdd').val("");
		$j("#ifBagSelectedDivAdd").hide();
		$j("#bag12TypeDivAdd").hide();
		$j("#bagTypeDivAdd").hide();
		$j("#otherBagTypeDivAdd").hide();
		
		
	}
	else{
		$j("#fkBagTypeAdd").val("");
		$j("#cryoBagManufacAdd").val("");
		$j("#maxValueAdd").val("");
		$j("#otherFrozenContAdd").val("");
		$j("#ifBagSelectedDivAdd").hide();
		$j("#otherFrozenInDivAdd").hide();
		$j("#bag12TypeDivAdd").hide();
		$j("#bagTypeDivAdd").hide();
		$j("#otherBagTypeDivAdd").hide();
		$j("#fkBag1TypeAdd").val("");
		$j("#fkBag2TypeAdd").val("");
		$j("#fkNoOfBagsAdd").val("");
		$j('#cryoBag2ManufacAdd').val("");
		$j('#maxValue2Add').val("");
		$j('#fkNoOfBagsAdd').val("");
	}
	}
function getIfBagSelected_onload() {
	if ($j("#fkFrozenInAdd").val()==$j("#pkFrozenInBagAdd").val()) {
		$j("#ifBagSelectedDivAdd").show();
		$j("#otherFrozenInDivAdd").hide();
		$j("#bag12TypeDivAdd").hide();
		$j("#bagTypeDivAdd").hide();
		$j("#otherBagTypeDivAdd").hide();
	}
	else if($j("#fkFrozenInAdd").val()==$j("#pkFrozenInOtherAdd").val()){
		$j("#otherFrozenInDivAdd").show();
		$j("#ifBagSelectedDivAdd").hide();
		$j("#bag12TypeDivAdd").hide();
		$j("#bagTypeDivAdd").hide();
		$j("#otherBagTypeDivAdd").hide();
	}
	else{
		$j("#ifBagSelectedDivAdd").hide();
		$j("#otherFrozenInDivAdd").hide();
		$j("#bag12TypeDivAdd").hide();
		$j("#bagTypeDivAdd").hide();
		$j("#otherBagTypeDivAdd").hide();
	}
}


function getNoOfBagsSelected(val) {

	if(val == $j("#pkBag1Type").val()){
		$j("#ifBagSelectedDivAdd").show();
		$j("#otherFrozenInDivAdd").hide();
		$j("#otherFrozenContAdd").val("");
		$j("#bagTypeDivAdd").show();
		$j("#bagTypeLabel1").hide();
		$j("#cryoLabel1").hide();
		$j("#maxVolLabel1").hide();
		$j("#bagTypeLabel").show();
		$j("#cryoLabel").show();
		$j("#maxVolLabel").show();
		$j("#bag12TypeDivAdd").hide();
		$j("#fkBag1TypeAdd").val("");
		$j("#fkBag2TypeAdd").val("");
		$j("#otherBagTypeDivAdd").hide();
		$j("#otherBagType1DivAdd").hide();
		}
	else if(val == $j("#pkBag2Type").val()){
		$j("#ifBagSelectedDivAdd").show();
		$j("#otherFrozenInDivAdd").hide();
		$j("#otherFrozenContAdd").val("");
		$j("#bag12TypeDivAdd").show();
		$j("#bagTypeDivAdd").show();
		$j("#bagTypeLabel").hide();
		$j("#cryoLabel").hide();
		$j("#maxVolLabel").hide();
		$j("#bagTypeLabel1").show();
		$j("#cryoLabel1").show();
		$j("#maxVolLabel1").show();
		$j("#otherBagTypeDivAdd").hide();
		$j("#otherBagType1DivAdd").hide();
		$j("#otherBagType2DivAdd").hide();
		$j("#fkBag1TypeAdd").val("");
		$j("#fkBag2TypeAdd").val("");
		
		
		

	}
	else if(val == $j("#pkNoOfBagsOthers").val()){

		$j("#fkBagTypeAdd").val("");
		$j("#cryoBagManufacAdd").val("");
		$j("#maxValueAdd").val("");
		$j("#otherFrozenContAdd").val("");
		$j("#otherFrozenInDivAdd").hide();
		$j("#bagTypeDivAdd").hide();
		$j("#bag12TypeDivAdd").hide();
		$j("#otherBagTypeDivAdd").show(); 
		$j("#fkBag1TypeAdd").val("");
		$j("#fkBag2TypeAdd").val("");
		
	}
	else
	{
		$j("#fkBagTypeAdd").val("");
		$j("#cryoBagManufacAdd").val("");
		$j("#maxValueAdd").val("");
		$j("#otherFrozenContAdd").val("");
		$j("#otherFrozenInDivAdd").hide();
		$j("#bagTypeDivAdd").hide();
		$j("#bag12TypeDivAdd").hide();
		$j("#otherBagTypeDivAdd").hide(); 
		$j("#fkBag1TypeAdd").val("");
		$j("#fkBag2TypeAdd").val("");
	}
}
function getNoOfBagsSelected_onload(val) {

	if(val == $j("#pkBag1Type").val()){
		$j("#ifBagSelectedDivAdd").show();
		$j("#otherFrozenInDivAdd").hide();
		$j("#bagTypeDivAdd").show();
		$j("#bagTypeLabel1").hide();
		$j("#cryoLabel1").hide();
		$j("#maxVolLabel1").hide();
		$j("#bagTypeLabel").show();
		$j("#cryoLabel").show();
		$j("#maxVolLabel").show();
		$j("#bag12TypeDivAdd").hide();
		$j("#otherBagTypeDivAdd").hide();
		$j("#otherBagType1DivAdd").hide();
	}
	else if(val == $j("#pkBag2Type").val()){
		$j("#ifBagSelectedDivAdd").show();
		$j("#otherFrozenInDivAdd").hide();
		$j("#bag12TypeDivAdd").show();
		$j("#bagTypeDivAdd").show();
		$j("#bagTypeLabel").hide();
		$j("#cryoLabel").hide();
		$j("#maxVolLabel").hide();
		$j("#otherBagTypeDivAdd").hide();
		$j("#otherBagType1DivAdd").hide();
		$j("#otherBagType2DivAdd").hide();
	}
	else if(val == $j("#pkNoOfBagsOthers").val()){
		$j("#otherFrozenInDivAdd").hide();
		$j("#bagTypeDivAdd").hide();
		$j("#bag12TypeDivAdd").hide();
		$j("#otherBagTypeDivAdd").show(); 
	}
	else
	{
		$j("#otherFrozenInDivAdd").hide();
		$j("#bagTypeDivAdd").hide();
		$j("#bag12TypeDivAdd").hide();
		$j("#otherBagTypeDivAdd").hide(); 
	}
}

function cbuAutoDefer(val,Id){
	if (val == '<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FROZEN_IN,@com.velos.ordercomponent.util.VelosGarudaConstants@FROZEN_IN_VIALS)"/>'	 || val == '<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FROZEN_IN,@com.velos.ordercomponent.util.VelosGarudaConstants@FROZEN_IN_TUBES)"/>')
	{
		jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
				function(r) {
					if (r == false) {
					   $j("#"+Id).val("");
					  }else if(r == true){
						  autoDefer = true;
					  }
					});
	}
	
}

function tempAutoDefer(val,Id){
	if (val == '<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@STORAGE_TEMPERATURE,@com.velos.ordercomponent.util.VelosGarudaConstants@STORAGE_TEMPERATURE_GTMINUSONETHIRTYFIVE)"/>')
	{
		jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
				function(r) {
					if (r == false) {
					   $j("#"+Id).val("");
					  }else if(r == true){
						  autoDefer = true;
					  }
					});
	}
}

function segAutoDefer(val,Id){
	if (val !='' && (val == 0 || val == 1))
	{
		jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
				function(r) {
					if (r == false) {
					   $j("#"+Id).val("");
					  }else if(r == true){
						  autoDefer = true;
					  }
					});
	}
}

function procNameDispaly(val){
	$j("#ProceedNo").html(val);
}

function saveCBBProcedureInfo(){
	changeProceedNo();
	var dateElements = $j('.hasDatepicker').get();
	$j.each(dateElements, function(){
		  if($j(this).is(':visible'))
			  customRange(this);
	  });
	modalFormSubmitRefreshDiv('addNewCBBProcedureForm1','saveCBBProcedures','main');
}

$j(function(){
	
	$j("#addNewCBBProcedureForm1").validate({
		invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    },
		rules:{	
				"cbbProcedurePojo.procName":{required:true},
				"cbbProcedurePojo.fkSite":{required:true},
				"procStartDateStrAdd":{required:true,dpDate:true},
				"cbbProcedureInfoPojo.fkProcMethId":{required:true},
				"cbbProcedureInfoPojo.fkIfAutomated":{required:
														{
															depends: function(element){
																	return $j("#fkProcMethIdAdd").val()==$j("#pkProcMethodIdAdd").val();
																}
														}
													},
				"cbbProcedureInfoPojo.otherProcessing":{required:
														{
														depends: function(element){
																return $j("#fkIfAutomatedAdd").val()==$j("#pkIfAutomatedOtherAdd").val();
															}
														}
														},
				"cbbProcedureInfoPojo.fkProductModification":{required:true},
				"cbbProcedureInfoPojo.otherProdModi":{required:
														{
															depends: function(element){
																	return $j("#fkProductModificationAdd").val()==$j("#pkProdModiOtherAdd").val();
																}
															}
														},
				//"cbbProcedureInfoPojo.fkStorMethod":{required:true},
				//"cbbProcedureInfoPojo.fkFreezManufac":{required:true},
				"cbbProcedureInfoPojo.otherFreezManufac":{required:
															{
																depends: function(element){
																		return $j("#fkFreezManufacAdd").val()==$j("#pkFreezManuOtherAdd").val();
																	}
																}
															},
				"cbbProcedureInfoPojo.fkFrozenIn":{required:true},
				"cbbProcedureInfoPojo.noOfBags":{required:
														{
														depends: function(element){
																return $j("#fkFrozenInAdd").val()==$j("#pkFrozenInBagAdd").val();
															}
														}
													},
				"cbbProcedureInfoPojo.fkBag1Type":{required:
													{
														depends: function(element){
															if($j("#fkNoOfBagsAdd").val()==$j("#pkBag1Type").val()){
																return true;
															}
															if($j("#fkNoOfBagsAdd").val()==$j("#pkBag2Type").val()){
																return true;
															}
															}
														}
													},
				"cbbProcedureInfoPojo.fkBag2Type":{required:
													{
														depends: function(element){
																return $j("#fkNoOfBagsAdd").val()==$j("#pkBag2Type").val();
															}
														}
													},	

				"cbbProcedureInfoPojo.otherBagType":{required:
											{
												depends: function(element){
														return $j("#fkNoOfBagsAdd").val()==$j("#pkNoOfBagsOthers").val();
													}
												}
											},
				"cbbProcedureInfoPojo.otherBagType1":{required:
														{
															depends: function(element){
																	return $j("#fkBag1TypeAdd").val()==$j("#pkOtherBagType").val();
																}
															}
														},	
				"cbbProcedureInfoPojo.otherBagType2":{required:
														{
															depends: function(element){
																	return $j("#fkBag2TypeAdd").val()==$j("#pkOtherBagType").val();
																}
															}
														},																																																					
				"cbbProcedureInfoPojo.cryoBagManufac":{required:
														{
															depends: function(element){
																if($j("#fkNoOfBagsAdd").val()==$j("#pkBag1Type").val()){
																	return true;
																}
																if($j("#fkNoOfBagsAdd").val()==$j("#pkBag2Type").val()){
																	return true;
																}
																}
															}

														},
				
				"cbbProcedureInfoPojo.fkStorTemp":{required:true},																								
				"cbbProcedureInfoPojo.maxValue":{required:
													{
														depends: function(element){
															
															if($j("#fkNoOfBagsAdd").val()==$j("#pkBag1Type").val()){
																return true;
															}
															if($j("#fkNoOfBagsAdd").val()==$j("#pkBag2Type").val()){
																return true;
															}
															}
														}, range:[0,9999]

													},
				"cbbProcedureInfoPojo.cryoBagManufac2":{required:
													{
														depends: function(element){
																return $j("#fkNoOfBagsAdd").val()==$j("#pkBag2Type").val();
															}
														}

													},
				"cbbProcedureInfoPojo.maxValue2":{required:
													{
														depends: function(element){
																return $j("#fkNoOfBagsAdd").val()==$j("#pkBag2Type").val();
															}
														}, range:[0,9999]

													},
				"cbbProcedureInfoPojo.otherFrozenCont":{required:
															{
																depends: function(element){
																		return $j("#fkFrozenInAdd").val()==$j("#pkFrozenInOtherAdd").val();
																	}
																}
											
															},
				//"cbbProcedureInfoPojo.fkContrlRateFreezing":{required:true},
				"cbbProcedureInfoPojo.noOfIndiFrac":{required:true,range: [1, 99]},
				
				"cbbProcedureInfoPojo.thouUnitPerMlHeparin":{range: [0, 20]},
				"cbbProcedureInfoPojo.thouUnitPerMlHeparinper":{range: [0, 25]},
				"cbbProcedureInfoPojo.fiveUnitPerMlHeparin":{range:[0.4,4]},
				"cbbProcedureInfoPojo.tenUnitPerMlHeparin":{range:[0,2]},
				"cbbProcedureInfoPojo.sixPerHydroxyethylStarch":{range:[0,50]},
				"cbbProcedureInfoPojo.cpda":{range:[0,50]},
				"cbbProcedureInfoPojo.cpd":{range:[0,50]},
				"cbbProcedureInfoPojo.acd":{range:[0,50]},
				"cbbProcedureInfoPojo.otherAnticoagulant":{range:[0,50]},
				
				"cbbProcedureInfoPojo.hunPerDmso":{range:[0,10]},
				"cbbProcedureInfoPojo.hunPerGlycerol":{range:[0,10]},
				"cbbProcedureInfoPojo.tenPerDextran_40":{range:[0,10]},
				"cbbProcedureInfoPojo.fivePerHumanAlbu":{range:[0,10]},
				"cbbProcedureInfoPojo.twenFiveHumAlbu":{range:[0,10]},
				"cbbProcedureInfoPojo.plasmalyte":{range:[0,10]},
				"cbbProcedureInfoPojo.othCryoprotectant":{range:[0,50]},
				"cbbProcedureInfoPojo.fivePerDextrose":{range:[0,100]},
				"cbbProcedureInfoPojo.pointNinePerNacl":{range:[0,100]},
				"cbbProcedureInfoPojo.othDiluents":{range:[0,100]},
				"cbbProcedureInfoPojo.noOfSegments":{required:true},
				"cbbProcedureInfoPojo.filterPaper":{required:true},
				"cbbProcedureInfoPojo.rbcPallets":{required:true},
				"cbbProcedureInfoPojo.noOfExtrDnaAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfNonviableAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfSerumAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfPlasmaAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfViableCellAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfExtrDnaMaterAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfCellMaterAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfSerumMaterAliquots":{required:true},
				"cbbProcedureInfoPojo.noOfPlasmaMaterAliquots":{required:true},
				"cbbProcedureInfoPojo.fiveUnitPerMlHeparinper":{range:[0,5]},
				"cbbProcedureInfoPojo.tenUnitPerMlHeparinper":{range:[0,3]},
				"cbbProcedureInfoPojo.sixPerHydroxyethylStarchper":{range:[0,25]},
				"cbbProcedureInfoPojo.cpdaper":{range:[0,60]},
				"cbbProcedureInfoPojo.cpdper":{range:[0,60]},
				"cbbProcedureInfoPojo.acdper":{range:[0,60]},
				"cbbProcedureInfoPojo.otherAnticoagulant":{range:[0,50]},
				"cbbProcedureInfoPojo.otherAnticoagulantper":{range:[0,60]},
				"cbbProcedureInfoPojo.hunPerDmsoper":{range:[0,20]},
				"cbbProcedureInfoPojo.hunPerGlycerolper":{range:[0,20]},
				"cbbProcedureInfoPojo.tenPerDextran_40per":{range:[0,20]},
				"cbbProcedureInfoPojo.fivePerHumanAlbuper":{range:[0,20]},
				"cbbProcedureInfoPojo.twenFiveHumAlbuper":{range:[0,20]},
				"cbbProcedureInfoPojo.plasmalyteper":{range:[0,40]},
				"cbbProcedureInfoPojo.othCryoprotectantper":{range:[0,60]},
				"cbbProcedureInfoPojo.fivePerDextroseper":{range:[0,60]},
				"cbbProcedureInfoPojo.pointNinePerNaclper":{range:[0,60]},
				"cbbProcedureInfoPojo.othDiluentsper":{range:[0,60]},
				"cbbProcedureInfoPojo.noOfViableSampleNotRep":{required:true},
				"cbbProcedureInfoPojo.noOfOthRepAliqProd":{required:true},
				"cbbProcedureInfoPojo.noOfOthRepAliqAltCond":{required:true},
				"cbbProcedureInfoPojo.specifyOthAnti":{required:
				{
					depends: function(element){
							return ($j("#otherAnticoagulantAdd").val()!= '' || $j("#otherAnticoagulantAddper").val() != '');
						}
					}

				},
				"cbbProcedureInfoPojo.specOthCryopro":{required:
				{
					depends: function(element){
							return ($j("#othCryoprotectantAdd").val()!= '' || $j("#othCryoprotectantAddper").val() != '');
						}
					}

				},
				"cbbProcedureInfoPojo.specOthDiluents":{required:
				{
					depends: function(element){
							return ($j("#othDiluentsAdd").val()!= '' || $j("#othDiluentsAddper").val() != '');
						}
					}

				},
				},
			messages:{	
				"cbbProcedurePojo.procName":"<s:text name="garuda.pf.procedure.procname"/>",
				"cbbProcedurePojo.fkSite":"<s:text name="garuda.pf.procedure.site"/>",
				"procStartDateStrAdd":{required:"<s:text name="garuda.pf.procedure.startdate"/>"},
				"cbbProcedureInfoPojo.fkProcMethId":"<s:text name="garuda.pf.procedure.procmethod"/>",
				"cbbProcedureInfoPojo.fkIfAutomated":"<s:text name="garuda.pf.procedure.ifauto"/>",
				"cbbProcedureInfoPojo.otherProcessing":"<s:text name="garuda.pf.procedure.otherautometh"/>",
				"cbbProcedureInfoPojo.fkProductModification":"<s:text name="garuda.pf.procedure.prodmodif"/>",
				"cbbProcedureInfoPojo.otherProdModi":"<s:text name="garuda.pf.procedure.otherprodmod"/>",
				//"cbbProcedureInfoPojo.fkStorMethod":"<s:text name="garuda.pf.procedure.storagemeth"/>",
				//"cbbProcedureInfoPojo.fkFreezManufac":"<s:text name="garuda.pf.procedure.freezmanuf"/>",
				"cbbProcedureInfoPojo.otherFreezManufac":"<s:text name="garuda.pf.proceudre.othfreezmanuf"/>",
				"cbbProcedureInfoPojo.fkFrozenIn":"<s:text name="garuda.pf.procedure.frozenin"/>",
				"cbbProcedureInfoPojo.noOfBags":"<s:text name="garuda.pf.procedure.numbags"/>",
				"cbbProcedureInfoPojo.fkBag1Type":"<s:text name="garuda.pf.procedure.bag1type"/>",
				"cbbProcedureInfoPojo.fkBag2Type":"<s:text name="garuda.pf.procedure.bag2type"/>",
				"cbbProcedureInfoPojo.otherBagType":"<s:text name="garuda.pf.procedure.othbagtype"/>",
				"cbbProcedureInfoPojo.otherBagType1":"<s:text name="garuda.pf.procedure.othbagtype"/>",
				"cbbProcedureInfoPojo.otherBagType2":"<s:text name="garuda.pf.procedure.othbagtype"/>",
				"cbbProcedureInfoPojo.cryoBagManufac":"<s:text name="garuda.pf.procedure.cryobagmanu"/>",
				"cbbProcedureInfoPojo.cryoBagManufac2":"<s:text name="garuda.pf.procedure.cryobagmanu"/>",
				"cbbProcedureInfoPojo.fkStorTemp":"<s:text name="garuda.pf.procedure.storagetemp"/>",
				"cbbProcedureInfoPojo.maxValue":"<s:text name="garuda.pf.procedure.maxval"/>",
				"cbbProcedureInfoPojo.maxValue2":"<s:text name="garuda.pf.procedure.maxval"/>",
				//"cbbProcedureInfoPojo.fkContrlRateFreezing":"<s:text name="garuda.pf.procedure.controlratefreez"/>",
				"cbbProcedureInfoPojo.noOfIndiFrac":"<s:text name="garuda.common.range.decbetween1to99"/>",
				"cbbProcedureInfoPojo.thouUnitPerMlHeparin":"<s:text name="garuda.common.range.decbetween0to20"/>",
				"cbbProcedureInfoPojo.fiveUnitPerMlHeparin":"<s:text name="garuda.common.range.decbetween0p4to4"/>",
				"cbbProcedureInfoPojo.tenUnitPerMlHeparin":"<s:text name="garuda.common.range.decbetween0to2"/>",
				"cbbProcedureInfoPojo.sixPerHydroxyethylStarch":"<s:text name="garuda.common.range.decbetween0to50"/>",
				"cbbProcedureInfoPojo.cpda":"<s:text name="garuda.common.range.decbetween0to50"/>",
				"cbbProcedureInfoPojo.cpd":"<s:text name="garuda.common.range.decbetween0to50"/>",
				"cbbProcedureInfoPojo.acd":"<s:text name="garuda.common.range.decbetween0to50"/>",
				"cbbProcedureInfoPojo.otherAnticoagulant":"<s:text name="garuda.common.range.decbetween0to50"/>",
				"cbbProcedureInfoPojo.hunPerDmso":"<s:text name="garuda.common.range.decbetween0to10"/>",
				"cbbProcedureInfoPojo.hunPerGlycerol":"<s:text name="garuda.common.range.decbetween0to10"/>",
				"cbbProcedureInfoPojo.tenPerDextran_40":"<s:text name="garuda.common.range.decbetween0to10"/>",
				"cbbProcedureInfoPojo.fivePerHumanAlbu":"<s:text name="garuda.common.range.decbetween0to10"/>",
				"cbbProcedureInfoPojo.twenFiveHumAlbu":"<s:text name="garuda.common.range.decbetween0to10"/>",
				"cbbProcedureInfoPojo.plasmalyte":"<s:text name="garuda.common.range.decbetween0to10"/>",
				"cbbProcedureInfoPojo.othCryoprotectant":"<s:text name="garuda.common.range.decbetween0to50"/>",
				"cbbProcedureInfoPojo.fivePerDextrose":"<s:text name="garuda.common.range.decbetween0to100"/>",
				"cbbProcedureInfoPojo.pointNinePerNacl":"<s:text name="garuda.common.range.decbetween0to100"/>",
				"cbbProcedureInfoPojo.othDiluents":"<s:text name="garuda.common.range.decbetween0to100"/>",
				"cbbProcedureInfoPojo.noOfSegments":"<s:text name="garuda.pf.procedure.noofsegments"/>",
				"cbbProcedureInfoPojo.filterPaper":"<s:text name="garuda.cbu.samples.filterpaper"/>",
				"cbbProcedureInfoPojo.rbcPallets":"<s:text name="garuda.cbu.samples.rbcpallets"/>",
				"cbbProcedureInfoPojo.noOfExtrDnaAliquots":"<s:text name="garuda.cbu.samples.numextracteddna"/>",
				"cbbProcedureInfoPojo.noOfNonviableAliquots":"<s:text name="garuda.cbu.samples.numnonviable"/>",
				"cbbProcedureInfoPojo.noOfSerumAliquots":"<s:text name="garuda.cbu.samples.numserum"/>",
				"cbbProcedureInfoPojo.noOfPlasmaAliquots":"<s:text name="garuda.cbu.samples.numplasma"/>",
				"cbbProcedureInfoPojo.noOfViableCellAliquots":"<s:text name="garuda.cbu.samples.numviable"/>",
				"cbbProcedureInfoPojo.noOfExtrDnaMaterAliquots":"<s:text name="garuda.cbu.samples.matextracteddna"/>",
				"cbbProcedureInfoPojo.noOfCellMaterAliquots":"<s:text name="garuda.cbu.samples.nummiscmat"/>",
				"cbbProcedureInfoPojo.noOfSerumMaterAliquots":"<s:text name="garuda.cbu.samples.nummatserum"/>",
				"cbbProcedureInfoPojo.noOfPlasmaMaterAliquots":"<s:text name="garuda.cbu.samples.nummatplasma"/>",
				"cbbProcedureInfoPojo.otherFrozenCont":"<s:text name="garuda.pf.procedure.frozencontainer"/>",
				"cbbProcedureInfoPojo.thouUnitPerMlHeparinper":"<s:text name="garuda.common.range.decbetween0to25"/>",
				"cbbProcedureInfoPojo.fiveUnitPerMlHeparinper":"<s:text name="garuda.common.range.decbetween0to5"/>",
				"cbbProcedureInfoPojo.tenUnitPerMlHeparinper":"<s:text name="garuda.common.range.decbetween0to3"/>",
				"cbbProcedureInfoPojo.sixPerHydroxyethylStarchper":"<s:text name="garuda.common.range.decbetween0to25"/>",
				"cbbProcedureInfoPojo.cpdaper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.cpdper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.acdper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.otherAnticoagulantper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.hunPerDmsoper":"<s:text name="garuda.common.range.decbetween0to20"/>",
				"cbbProcedureInfoPojo.hunPerGlycerolper":"<s:text name="garuda.common.range.decbetween0to20"/>",
				"cbbProcedureInfoPojo.tenPerDextran_40per":"<s:text name="garuda.common.range.decbetween0to20"/>",
				"cbbProcedureInfoPojo.fivePerHumanAlbuper":"<s:text name="garuda.common.range.decbetween0to20"/>",
				"cbbProcedureInfoPojo.twenFiveHumAlbuper":"<s:text name="garuda.common.range.decbetween0to20"/>",
				"cbbProcedureInfoPojo.plasmalyteper":"<s:text name="garuda.common.range.decbetween0to40"/>",
				"cbbProcedureInfoPojo.othCryoprotectantper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.fivePerDextroseper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.pointNinePerNaclper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.othDiluentsper":"<s:text name="garuda.common.range.decbetween0to60"/>",
				"cbbProcedureInfoPojo.noOfViableSampleNotRep":"<s:text name="garuda.cbu.samples.numviablesamplenotrep"/>",
				"cbbProcedureInfoPojo.noOfOthRepAliqProd":"<s:text name="garuda.cbu.samples.numothrepaliqprod"/>",
				"cbbProcedureInfoPojo.noOfOthRepAliqAltCond":"<s:text name="garuda.cbu.samples.numothrepaliqaltcond"/>"
					}
								});
});

function getOtherAnticoagulant(){
	   if( ($j("#otherAnticoagulantAdd").val() != '') ||  ($j("#otherAnticoagulantAddper").val() != '') ){
			$j("#otherAnticoagulantDivAddition").show();
		}
		else{
		$j("#specifyOthAntiAdd").val("");
		$j('#otherAnticoagulantDivAddition').hide();
		}	
}

function getOtherCyroprotectant(){
	if( ($j("#othCryoprotectantAdd").val() != '') ||  ($j("#othCryoprotectantAddper").val() != '') ){	
		$j("#otherCryoprotectantDivAdd").show();
	}
	else{
		$j("#specOthCryoproAdd").val("");
		$j("#otherCryoprotectantDivAdd").hide();
		}
}


function getOtherDiluents(){
		if( ($j("#othDiluentsAdd").val() != '') ||  ($j("#othDiluentsAddper").val() != '') ){	
		$j("#otherDiluentsDivAdd").show();
		}		
	else{
		$j("#specOthDiluentsAdd").val("");
		$j("#otherDiluentsDivAdd").hide();
		}
}

function getTotalCBUAliquots(){
	var var_noOfExtrDnaAliquotsAdd=$j("#noOfExtrDnaAliquotsAdd").val();
	var var_noOfNonviableAliquotsAdd=$j("#noOfNonviableAliquotsAdd").val();
	var var_noOfPlasmaAliquotsAdd=$j("#noOfPlasmaAliquotsAdd").val();
	var var_noOfSerumAliquotsAdd=$j("#noOfSerumAliquotsAdd").val();
	var var_noOfViableCellAliquotsAdd=$j("#noOfViableCellAliquotsAdd").val();
	if(var_noOfExtrDnaAliquotsAdd==""||var_noOfExtrDnaAliquotsAdd==null){
		var_noOfExtrDnaAliquotsAdd=0;
	}
	if(var_noOfNonviableAliquotsAdd==""||var_noOfNonviableAliquotsAdd==null){
		var_noOfNonviableAliquotsAdd=0;
	}
	if(var_noOfPlasmaAliquotsAdd==""||var_noOfPlasmaAliquotsAdd==null){
		var_noOfPlasmaAliquotsAdd=0;
	}
	if(var_noOfSerumAliquotsAdd==""||var_noOfSerumAliquotsAdd==null){
		var_noOfSerumAliquotsAdd=0;
	}
	if(var_noOfViableCellAliquotsAdd==""||var_noOfViableCellAliquotsAdd==null){
		var_noOfViableCellAliquotsAdd=0;
	}
	$j("#totCbuAliquotsAdd").val(parseInt(var_noOfExtrDnaAliquotsAdd)+parseInt(var_noOfNonviableAliquotsAdd)+parseInt(var_noOfPlasmaAliquotsAdd)+parseInt(var_noOfSerumAliquotsAdd)+parseInt(var_noOfViableCellAliquotsAdd));
}

function getTotMaternalAliquots(){
	var var_noOfExtrDnaMaterAliquotsAdd=$j("#noOfExtrDnaMaterAliquotsAdd").val();
	var var_noOfCellMaterAliquotsAdd=$j("#noOfCellMaterAliquotsAdd").val();
	var var_noOfSerumMaterAliquotsAdd=$j("#noOfSerumMaterAliquotsAdd").val();
	var var_noOfPlasmaMaterAliquotsAdd=$j("#noOfPlasmaMaterAliquotsAdd").val();
	if(var_noOfExtrDnaMaterAliquotsAdd==""||var_noOfExtrDnaMaterAliquotsAdd==null){
		var_noOfExtrDnaMaterAliquotsAdd=0;
	}
	if(var_noOfCellMaterAliquotsAdd==""||var_noOfCellMaterAliquotsAdd==null){
		var_noOfCellMaterAliquotsAdd=0;
	}
	if(var_noOfSerumMaterAliquotsAdd==""||var_noOfSerumMaterAliquotsAdd==null){
		var_noOfSerumMaterAliquotsAdd=0;
	}
	if(var_noOfPlasmaMaterAliquotsAdd==""||var_noOfPlasmaMaterAliquotsAdd==null){
		var_noOfPlasmaMaterAliquotsAdd=0;
	}
	$j("#totMaterAliquotsAdd").val(parseInt(var_noOfExtrDnaMaterAliquotsAdd)+parseInt(var_noOfCellMaterAliquotsAdd)+parseInt(var_noOfSerumMaterAliquotsAdd)+parseInt(var_noOfPlasmaMaterAliquotsAdd));
}


function validateDate(enddate,strtdate,strtdatelabl,enddatelabl){
	var date =$j("#"+enddate).val(); 
	var date1 =$j("#"+strtdate).val();
	
	if(date1!=""){
		document.getElementById(enddatelabl).innerHTML="";
		date = Date.parse(date);
		date1 = Date.parse(date1);
	if(date < date1){
		document.getElementById(strtdatelabl).innerHTML="<s:text name="garuda.cbu.procedure.chckTermStartDate"/>";
		document.getElementById(enddatelabl).innerHTML="";
		$j("#"+enddate).val("");
		$j("#"+strtdate).val("");
	}
	else{
		document.getElementById(strtdatelabl).innerHTML="";
		document.getElementById(enddatelabl).innerHTML="";
	}
	}
	else{
		$j("#"+enddate).val("");
		document.getElementById(enddatelabl).innerHTML="<s:text name="garuda.cbu.procedure.startDate"/>";

	}
}
function getOtherBagTypeSelected(val){

	if(val == $j("#pkOtherBagType").val()){
		$j("#otherBagType1DivAdd").show();
	}
	else{
		$j("#otherBagType1DivAdd").hide();
		$j("#otherBagType1Add").val("");
	}
	
}
function getOtherBagType2Selected(val){

	if(val == $j("#pkOtherBagType").val()){
		$j("#otherBagType2DivAdd").show();
	}
	else{
		$j("#otherBagType2DivAdd").hide();
		$j("#otherBagType2Add").val("");
	}
	
}

function eitherOneEdit(id){
	 if($j("#"+id).val() != '') {
		 $j("#"+id+"per").attr('disabled','disabled');
	 }
}
function eitherOneEditOther(id){
	var id1 = id.substring(0, id.length - 3)
	 if($j("#"+id).val() != '') {
		 $j("#"+id1).attr('disabled','disabled');
	 }
}


</script>

<s:form id="addNewCBBProcedureForm1">
	<s:hidden name="statusUpdate" value="True"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkProcMethodId"
		id="pkProcMethodIdAdd"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkIfAutomatedOther"
		id="pkIfAutomatedOtherAdd"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkProdModiOther"
		id="pkProdModiOtherAdd"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkFreezManuOther"
		id="pkFreezManuOtherAdd"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkFrozenInOther"
		id="pkFrozenInOtherAdd"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkFrozenInBag"
		id="pkFrozenInBagAdd"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkOtherBagType"
		 id="pkOtherBagType"></s:hidden>
		 
	<s:hidden name="cbbProcedureInfoPojo.pkBag1Type"
	 id="pkBag1Type"></s:hidden>
	 <s:hidden name="cbbProcedureInfoPojo.pkBag2Type"
	 id="pkBag2Type"></s:hidden>
	<s:hidden name="cbbProcedureInfoPojo.pkNoOfBagsOthers"
	 id="pkNoOfBagsOthers"></s:hidden>
	<div id="update" style="display: none;"><jsp:include
		page="../cb_update.jsp">
		<jsp:param value="garuda.message.modal.cbbProcedureAddInfo"
			name="message" />
	</jsp:include></div>
	<div id="procProcExistsDiv">
		<s:hidden name="procNameExistsFlag" id="procNameExistsFlag"></s:hidden>
	</div>
	<table width="100%" cellspacing="0" cellpadding="0" border="0"
		id="status">
		<tr>
			<td>
			<div id="cbbProcedureAddnewdiv">
			<table>

				<tr>
					<td>

					<div class="column">
					<div class="portlet" id="cbbProcedureAddparent">
					<div id="cbbProcedureAdd"
						class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
					<div
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-minusthick"></span><s:text
						name="garuda.cbbprocedures.label.processingprocedure"></s:text>:<b><u><s:label
						id="ProceedNo"></s:label></u></b></div>
					<div>
					<div id="<s:property value="cbbProcedureInfoPojo.pkProcMethodId" /><s:property value="cbbProcedurePojo.procVersion" />content" 
					onclick="toggleDiv('<s:property value="cbbProcedureInfoPojo.pkProcMethodId" /><s:property value="cbbProcedurePojo.procVersion" />')"
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-triangle-1-s"></span> <s:text
						name="garuda.cbbprocedures.label.cbbprocessingprocedures"></s:text>
					</div>
					<div id="<s:property value="cbbProcedureInfoPojo.pkProcMethodId" /><s:property value="cbbProcedurePojo.procVersion" />">
					<table>
						<tr>
							<!-- <td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.procno"></s:text>:<span style="color: red;">*</span></td> -->
							<td width="20%" align="left" 
							onmouseover='return showhelpmsg("toolTipMsg")' onmouseout="return nd();"> 
							<s:text
								name="garuda.cbbprocedures.label.procname"></s:text>:<span style="color: red;">*</span></td>	
							<!-- <td width="30%" align="left"><s:textfield
								name="cbbProcedurePojo.procNo" id="addProcNoAdd"
								onChange="changeProceedNo(this.value);" /> <br />-->	
							<td width="30%" align="left"><s:textfield
								name="cbbProcedurePojo.procName" id="addProcNameAdd" onblur ="procNameDispaly(this.value);"
								maxlength="50"/> <br /> 
							
							<s:label id="ProcNolbl" ></s:label></td>
							
						<!-- 	<td width="20%" align="left"><s:text name="garuda.cbuentry.label.productcode" /><s:text
								name="garuda.cbbprocedures.label.procversion"></s:text>:</td>
							<td width="30%" align="left"><s:textfield name="cbbProcedurePojo.productCode" id="productCode" maxlength="50"></s:textfield><s:textfield
								name="cbbProcedurePojo.procVersion" readonly="true" onkeydown="cancelBack();" 
								disabled="dispabled" value="PV2" /></td> -->
						</tr>
						<tr>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.procstartdate" />:<span style="color: red;">*</span></td>
							<td width="30%" align="left"><s:date
								name="cbbProcedures.procStartDate" id="datepicker10"
								format="MMM dd, yyyy" /> <s:textfield readonly="true" onkeydown="cancelBack();"  cssClass="datePicWOMinDate"
								name="procStartDateStrAdd" class="datepic" id="datepicker10"
								value="%{datepicker10}" />
							<div id="validDateDiv1"><span id="invalidDate1" class="error" ></span>
							</div>
							</td>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.proctermidate"></s:text>:</td>
							<td width="30%" align="left"><s:date
								name="cbbProcedures.procTermiDate" id="datepicker11"
								format="MMM dd, yyyy" /> <s:textfield readonly="true" onkeydown="cancelBack();" 
								name="procTermiDateStrAdd" class="datepic" id="datepicker11"
								value="%{datepicker11}"
								 cssClass="datePicWMinDate"/>
							<div id="validDateDiv"><span id="invalidDate" class="error" ></span></div>
							</td>
						</tr>
						 <tr>
					     <td width="20%" align="left">
					     	<s:text name="garuda.cbuentry.label.CBB" /><span class="error"><s:text name="garuda.label.dynamicform.astrik"></s:text> </span>
					     </td>
					     <td width="30%" align="left">
					      <s:select name="cbbProcedurePojo.fkSite" id ="cbbId" listKey="siteId" listValue="siteIdentifier" list="sites" headerKey="" headerValue="Select"></s:select>
					     </td>
					     <td width="20%" align="left">
					     </td>
					     <td width="30%" align="left">
					     </td>
						</tr>
					</table>
					</div>
					</div>
					<div>
					<div id="<s:property value="statusUpdate" /><s:property value="cbbProcedureInfoPojo.pkProcMethodId" />content" onclick="toggleDiv('<s:property value="statusUpdate" /><s:property value="cbbProcedureInfoPojo.pkProcMethodId" />')"
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-triangle-1-s"></span> <s:text
						name="garuda.cbbprocedures.label.productmodification"></s:text></div>
					<div id="<s:property value="statusUpdate" /><s:property value="cbbProcedureInfoPojo.pkProcMethodId" />">
					<table>
						<tr>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.processing"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%" align="left"><s:select
								name="cbbProcedureInfoPojo.fkProcMethId" id="fkProcMethIdAdd"
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING]"
								listKey="pkCodeId" listValue="description" headerKey=""
								headerValue="Select" onchange="getIfAutomated();" /></td>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.ifautomated"></s:text>:<span id="ifautomatedspan" style="color: red;">*</span>
								</td>
							<td width="30%" align="left"><s:select
								name="cbbProcedureInfoPojo.fkIfAutomated" id="fkIfAutomatedAdd"
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IF_AUTOMATED]"
								listKey="pkCodeId" listValue="description" headerKey=""
								headerValue="Select" onchange="getOtherAutomated();" /></td>
						</tr>
						<tr id="otherAutomatedDivAdd">
									<td width="20%" align="left"></td>
									<td width="30%" align="left"></td>
									
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.specifyoth"></s:text>:<span id="ifautomatedotherspan" style="color: red;">*</span></td>
									<td width="30%" align="left"><s:textfield
										name="cbbProcedureInfoPojo.otherProcessing"
										id="otherProcessingAdd" maxlength="30"/></td>
						</tr>
						<tr>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.productmodification"></s:text>:<span style="color: red;">*</span>
							</td>
							<td width="30%" align="left"><s:select
								name="cbbProcedureInfoPojo.fkProductModification"
								id="fkProductModificationAdd"
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PRODUCT_MODI]"
								listKey="pkCodeId" listValue="description" headerKey=""
								headerValue="Select" onchange="getOtherProdModi();" /></td>
							<td width="20%" align="left"></td>
							<td width="30%" align="left"></td>
						</tr>
						<tr id="otherProdModiDivAdd">
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.specifyoth"></s:text>:<span id="prodmodiotherspan" style="color: red;">*</span></td>
									<td width="30%" align="left"><s:textfield
										name="cbbProcedureInfoPojo.otherProdModi"
										id="otherProdModiAdd" maxlength="30"/></td>
									<td width="20%" align="left"></td>
									<td width="30%" align="left"></td>
						</tr>
					</table>
					</div>
					</div>
					<div>
					<div id="<s:property value="statusUpdate" /><s:property value="cbbProcedureInfoPojo.pkIfAutomatedOther" />content" onclick="toggleDiv('<s:property value="statusUpdate" /><s:property value="cbbProcedureInfoPojo.pkIfAutomatedOther" />')"
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-triangle-1-s"></span> <s:text
						name="garuda.cbbprocedures.label.storageconditions"></s:text></div>
					<div id="<s:property value="statusUpdate" /><s:property value="cbbProcedureInfoPojo.pkIfAutomatedOther" />">
					<table>
						<tr>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.stormethod"></s:text>:</td>
							<td width="30%" align="left"><s:select
								name="cbbProcedureInfoPojo.fkStorMethod" id="fkStorMethodAdd"
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@STORAGE_METHOD]"
								listKey="pkCodeId" listValue="description" headerKey=""
								headerValue="Select" /></td>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.freezmanufac"></s:text>:</td>
							<td width="30%" align="left"><s:select
								name="cbbProcedureInfoPojo.fkFreezManufac"
								id="fkFreezManufacAdd"
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FREEZER_MANUFACTURER]"
								listKey="pkCodeId" listValue="description" headerKey=""
								headerValue="Select" onchange="getOtherFreezerManufac();" /></td>
						</tr>
						<tr id="otherFreezerManufacDivAdd">
									<td width="20%" align="left"></td>
									<td width="30%" align="left"></td>
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.freezmanufac"></s:text> - <s:text
										name="garuda.cbbprocedures.label.specifyoth"></s:text>:<span id="freezmanuotherspan" style="color: red;">*</span></td>
									<td width="30%" align="left"><s:textfield
										name="cbbProcedureInfoPojo.otherFreezManufac"
										id="otherFreezManufacAdd" maxlength="50"/></td>
						</tr>
						<tr>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.frozenin"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%" align="left"><s:select
								name="cbbProcedureInfoPojo.fkFrozenIn" id="fkFrozenInAdd"
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FROZEN_IN]"
								listKey="pkCodeId" listValue="description" headerKey=""
								headerValue="Select" onchange="cbuAutoDefer(this.value,this.id);getIfBagSelected();" /></td>
							<td width="20%" align="left"></td>
							<td width="30%" align="left"></td>
						</tr>
						<tr id="otherFrozenInDivAdd">
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.specifyother"></s:text>:<span id="frozeninotherspan" style="color: red;">*</span></td>
							<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.otherFrozenCont"
								id="otherFrozenContAdd" maxlength="30"/></td>
							<td width="20%" align="left"></td>
							<td width="30%" align="left"></td>
						</tr>
						<tr id="ifBagSelectedDivAdd">
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.noofbags"></s:text>:<span id="bagtypespan" style="color: red;">*</span></td>
							<td width="30%" align="left"><s:select
								name="cbbProcedureInfoPojo.noOfBags" id="fkNoOfBagsAdd"
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NUM_OF_BAGS]"
								listKey="pkCodeId" listValue="description" headerKey=""
								headerValue="Select" onchange="getNoOfBagsSelected(this.value);"/></td>
						 </tr>
						<tr id="otherBagTypeDivAdd">
						<td width="50%" align="left" colspan="2">
								<table>
									<tr>
										<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
										</s:text><span id="otherbagtypespan" style="color: red;">*</span>
										</td>
										<td width="30%" align="left"><s:textfield name="cbbProcedureInfoPojo.otherBagType" id="otherBagTypeAdd" maxlength="30"></s:textfield>
										</td>
									</tr>
								</table>
						</td>
						</tr>
						<tr id="bagTypeDivAdd">
						<td align="left" colspan="4">
						<table>
						<tr>
						<td id ="bagTypeLabel" width="20%" align="left"><s:text
							name="garuda.cbbprocedures.label.bagtype"></s:text>:<span id="bag1typespan" style="color: red;">*</span></td>
						
						<td id ="bagTypeLabel1" width="20%" align="left"><s:text
							name="garuda.cbbprocedures.label.bag1type"></s:text>:<span id="bag1typespan" style="color: red;">*</span></td>
						<td width="30%" align="left"><s:select
							name="cbbProcedureInfoPojo.fkBag1Type" id="fkBag1TypeAdd"
							list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BAG_TYPE]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select"  onchange="getOtherBagTypeSelected(this.value);"/></td>
						</tr>
						<tr>
							<td id="cryoLabel" width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.cryobagmanufac"></s:text>:<span id="cyrobagspan" style="color: red;">*</span></td>
							<td id="cryoLabel1" width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.cryobagmanufac.bagtype1"></s:text>:<span id="cyrobagspan" style="color: red;">*</span></td>
							<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.cryoBagManufac"
								id="cryoBagManufacAdd" maxlength="50"/></td>
							<td id="maxVolLabel" width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.maxvalue"></s:text>(<s:text
								name="garuda.cbbprocedures.label.ml"></s:text>):<span id="maxvaluespan" style="color: red;">*</span></td>
							
							<td id="maxVolLabel1" width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.maxvalue.bagtype1"></s:text>(<s:text
								name="garuda.cbbprocedures.label.ml"></s:text>):<span id="maxvaluespan" style="color: red;">*</span></td>
							<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.maxValue" id="maxValueAdd" maxlength="4" 
								cssClass="positive-integer"/></td>
						</tr>
						<tr id="otherBagType1DivAdd">
						<td width="50%" align="left" colspan="2">
								<table>
									<tr>
										<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
										</s:text><span id="otherbagtypespan" style="color: red;">*</span>
										</td>
										<td width="30%" align="left"><s:textfield name="cbbProcedureInfoPojo.otherBagType1" id="otherBagType1Add" maxlength="30"></s:textfield>
										</td>
									</tr>
								</table>
						</td>
						</tr>
						</table>
						</td>
						</tr>			
						<tr id="bag12TypeDivAdd">
						<td align="left" colspan="4">
						<table>
						<tr>
						<td width="20%" align="left"><s:text
							name="garuda.cbbprocedures.label.bag2type"></s:text>:<span id="bag1typespan" style="color: red;">*</span></td>
						<td width="30%" align="left"><s:select
							name="cbbProcedureInfoPojo.fkBag2Type" id="fkBag2TypeAdd"
							list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BAG_TYPE]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select"  onchange="getOtherBagType2Selected(this.value);"/></td>
						</tr>
						<tr>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.cryobagmanufac.bagtype2"></s:text>:<span id="cyrobag2span" style="color: red;">*</span></td>
							<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.cryoBagManufac2"
								id="cryoBag2ManufacAdd" maxlength="50"/></td>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.maxvalue.bagtype2"></s:text>(<s:text
								name="garuda.cbbprocedures.label.ml"></s:text>):<span id="maxvaluespan" style="color: red;">*</span></td>
							<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.maxValue2" id="maxValue2Add" maxlength="4"
								cssClass="positive-integer" /></td>
						</tr>
						<tr id="otherBagType2DivAdd">
						<td width="50%" align="left" colspan="2">
								<table>
									<tr>
										<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
										</s:text><span id="otherbagtypespan" style="color: red;">*</span>
										</td>
										<td width="30%" align="left"><s:textfield name="cbbProcedureInfoPojo.otherBagType2" id="otherBagType2Add" maxlength="30"></s:textfield>
										</td>
									</tr>
								</table>
						</td>
						</tr>
						</table>
						</td>
						</tr>
						<tr>
						
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.stortemp"></s:text>:<span style="color: red;">*</span></td>
								
								<td width="30%" align="left">
								<select name="cbbProcedureInfoPojo.fkStorTemp" id="fkStorTempAdd" onchange="tempAutoDefer(this.value,this.id);">
								<option value="">Select</option>
								<s:iterator value = "#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@STORAGE_TEMPERATURE]">
									<s:if test="%{pkCodeId==cbbProcedureInfoPojo.fkStorTemp}">
										<option value ='<s:property value='%{pkCodeId}'/>' selected="selected"><s:property value="%{description}" escapeHtml="false"/></option>
									</s:if><s:else>
										<option value ='<s:property value='%{pkCodeId}'/>'> <s:property value="%{description}" escapeHtml="false"/></option>
									</s:else>
								</s:iterator>
								</select>
								</td>	
								
							<td width="20%" align="left"></td>
							<td width="30%" align="left"></td>
						</tr> 
						<tr>
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.controlledratefreezing"></s:text>:
							</td>
							<td width="30%" align="left"><s:select
								name="cbbProcedureInfoPojo.fkContrlRateFreezing"
								id="fkContrlRateFreezingAdd"
								list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CONTR_RATE_FREEZ]"
								listKey="pkCodeId" listValue="description" headerKey=""
								headerValue="Select" /></td>
							<td width="20%" align="left"
							onmouseover='return showhelpmsg("tooltipMsgThawed")' onmouseout="return nd();">
							<s:text	name="garuda.cbbprocedures.label.noofindifrac"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%" align="left"><s:textfield
								name="cbbProcedureInfoPojo.noOfIndiFrac" id="noOfIndiFracAdd"
								maxlength="2" cssClass="positive-integer" /></td>
						</tr>
					</table>
					</div>
					</div>

					<div>
					<div id="<s:property value="cbbProcedureInfoPojo.pkProdModiOther" /><s:property value="cbbProcedureInfoPojo.pkIfAutomatedOther" />content" onclick="toggleDiv('<s:property value="cbbProcedureInfoPojo.pkProdModiOther" /><s:property value="cbbProcedureInfoPojo.pkIfAutomatedOther" />')"
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-triangle-1-s"></span> <s:text
						name="garuda.cbbprocedures.label.frozenproductcomposition"></s:text>
					</div>
					<div id="<s:property value="cbbProcedureInfoPojo.pkProdModiOther" /><s:property value="cbbProcedureInfoPojo.pkIfAutomatedOther" />">
					<fieldset><legend> <s:text
						name="garuda.cbbprocedures.label.additivesbeforevolumereductions"></s:text>
					</legend>
					<table>
						<tr>
							
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
									
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.thouunitpermlheparin"></s:text>:
							</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.thouUnitPerMlHeparin" id="thouUnitPerMlHeparin" maxlength="4" cssClass="positivecheck decimalPoint" onblur = "eitherOne(this.id);checkFormat(this.value,this.id); ">
								</s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.thouUnitPerMlHeparinper" id="thouUnitPerMlHeparinper" cssClass="positivecheck decimalPoint"  onblur = "eitherOneOther(this.id);checkFormat(this.value,this.id);" maxlength="4">
								</s:textfield></td>
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.cpda"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.cpda" id="cpda" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id); eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.cpdaper" id="cpdaper" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id); eitherOneOther(this.id);"></s:textfield></td>
							
							
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.fiveunitpermlheparin"></s:text>:
							</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.fiveUnitPerMlHeparin" id="fiveUnitPerMlHeparin" maxlength="3"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id); eitherOne(this.id);">
								</s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.fiveUnitPerMlHeparinper" id="fiveUnitPerMlHeparinper" maxlength="3"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id); eitherOneOther(this.id);">
								</s:textfield></td>
							
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.cpd"></s:text>:</td>
							<td width="15%"><s:textfield name="cbbProcedureInfoPojo.cpd" id="cpd" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id); eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield name="cbbProcedureInfoPojo.cpdper" id="cpdper" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id); eitherOneOther(this.id);"></s:textfield></td>
							
						
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.tenunitpermlheparin"></s:text>:
							</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.tenUnitPerMlHeparin" id="tenUnitPerMlHeparin" maxlength="3"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id); eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.tenUnitPerMlHeparinper" id="tenUnitPerMlHeparinper" maxlength="3"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id); eitherOneOther(this.id);"></s:textfield></td>
								
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.acd"></s:text>:</td>
							<td width="15%"><s:textfield name="cbbProcedureInfoPojo.acd" id="acd" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>	
							<td width="15%"><s:textfield name="cbbProcedureInfoPojo.acdper" id="acdper" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
						</tr>
						<tr valign="top">
								
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.sixperhydroxyethylstarch"></s:text>:
							</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.sixPerHydroxyethylStarch" id="sixPerHydroxyethylStarch" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.sixPerHydroxyethylStarchper" id="sixPerHydroxyethylStarchper" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.otheranticoagulant"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.otherAnticoagulant"
								id="otherAnticoagulantAdd" onChange ="getOtherAnticoagulant();" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.otherAnticoagulantper"
								id="otherAnticoagulantAddper" onChange ="getOtherAnticoagulant();" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
						</tr>

						<tr id="otherAnticoagulantDivAddition" valign="top">
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.specifyothanti"></s:text>:<span style="color: red;">*</span></td>
									<td width="30%" align="left" colspan="2"><s:textfield
										name="cbbProcedureInfoPojo.specifyOthAnti"
										id="specifyOthAntiAdd" maxlength="30"/></td>
									<td width="20%" align="left"></td>
									<td width="30%" align="left" colspan="2"></td>
						</tr>
					</table>
					</fieldset>
					<fieldset><legend> <s:text
						name="garuda.cbbprocedures.label.cryoprotectantsandotheradditives"></s:text>
					</legend>
					<table>
						<tr valign="top">
							
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
									
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.hunperdmso"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.hunPerDmso" id="hunPerDmso" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.hunPerDmsoper" id="hunPerDmsoper" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.twenfivehumalbu"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.twenFiveHumAlbu" id="twenFiveHumAlbu" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.twenFiveHumAlbuper" id="twenFiveHumAlbuper" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.hunperglycerol"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.hunPerGlycerol" id="hunPerGlycerol" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.hunPerGlycerolper" id="hunPerGlycerolper" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.plasmalyte"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.plasmalyte" id="plasmalyte" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.plasmalyteper" id="plasmalyteper" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
					
						</tr>
						<tr valign="top">
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.tenperdextran40"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.tenPerDextran_40" id="tenPerDextran_40" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.tenPerDextran_40per" id="tenPerDextran_40per" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
								
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.othcryoprotectant"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.othCryoprotectant"
								id="othCryoprotectantAdd" onchange="getOtherCyroprotectant();" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>	

							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.othCryoprotectantper"
								id="othCryoprotectantAddper" onchange="getOtherCyroprotectant();" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.fiveperhumanalbu"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.fivePerHumanAlbu" id="fivePerHumanAlbu" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.fivePerHumanAlbuper" id="fivePerHumanAlbuper" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%" colspan="2"></td>
							
						</tr>
						<tr id="otherCryoprotectantDivAdd" valign="top">
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.specothcryopro"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%" align="left" colspan="2"><s:textfield
								name="cbbProcedureInfoPojo.specOthCryopro"
								id="specOthCryoproAdd" maxlength="30"/></td>
							
							<td width="20%" align="left"></td>
							<td width="30%" align="left" colspan="2"></td>						
						</tr>
					</table>
					</fieldset>
					<fieldset><legend> <s:text
						name="garuda.cbbprocedures.label.diluents"></s:text></legend>
					<table width="100%">
						<tr valign="top">
							
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
							<td width="20%">
							</td>
							<td width="15" align="center">
							</td>
							<td width="15" align="center">
							</td>
									
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.fiveperdextrose"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.fivePerDextrose" id="fivePerDextrose" maxlength="5"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.fivePerDextroseper" id="fivePerDextroseper" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%"colspan="2"></td>
						</tr>
						<tr valign="top">		
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.pointninepernacl"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.pointNinePerNacl" id="pointNinePerNacl" maxlength="5"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.pointNinePerNaclper" id="pointNinePerNaclper" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%"colspan="2"></td>	
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.otherdiluents"></s:text>:</td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.othDiluents" id="othDiluentsAdd"
								onchange="getOtherDiluents();" maxlength="5"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOne(this.id);"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cbbProcedureInfoPojo.othDiluentsper" id="othDiluentsAddper"
								onchange="getOtherDiluents();" maxlength="4"
								cssClass="positivecheck decimalPoint" onblur = "checkFormat(this.value,this.id);eitherOneOther(this.id);"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%"colspan="2"></td>
						</tr>
						<tr id="otherDiluentsDivAdd" valign="top">
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.specothdiluents"></s:text>:<span style="color: red;">*</span></td>
									<td width="30%" align="left" colspan="2"><s:textfield
										name="cbbProcedureInfoPojo.specOthDiluents"
										id="specOthDiluentsAdd" maxlength="30"/></td>
									<td width="20%" align="left"></td>
									<td width="30%" align="left" colspan="2"></td>
									
									
						</tr>
					</table>
					</fieldset>
					</div>
					</div>

					<div>
					<div id="<s:property value="cbbProcedureInfoPojo.filterPaper" /><s:property value="statusUpdate" />_cbusmplcontent" onclick="toggleDiv('<s:property value="cbbProcedureInfoPojo.filterPaper" /><s:property value="statusUpdate" />_cbusmpl')"
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-triangle-1-s"></span> <s:text
						name="garuda.cbbprocedures.label.cbusamples"></s:text></div>
					<div id="<s:property value="cbbProcedureInfoPojo.filterPaper" /><s:property value="statusUpdate" />_cbusmpl">
					<table>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.filterpaper"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.filterPaper"
								cssClass="positive-integer" maxlength="2"></s:textfield></td>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.rbcpellets"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.rbcPallets"
								cssClass="positive-integer" maxlength="2"></s:textfield></td>	
						</tr>
						<tr valign="top">
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.noofextrdnaaliquots"></s:text>:<span style="color: red;">*</span>
							</td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.noOfExtrDnaAliquots"
								id="noOfExtrDnaAliquotsAdd" 
								cssClass="positive-integer" maxlength="2"></s:textfield></td>
							<td width="20%" onmouseover='return showhelpmsg("toolTipMsgSerum")' onmouseout="return nd();"><s:text
								name="garuda.cbbprocedures.label.noofserumaliquots"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.noOfSerumAliquots"
								id="noOfSerumAliquotsAdd" 
								cssClass="positive-integer" maxlength="2" /></td>	
						</tr>
						<tr valign="top">
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.noofplasmaaliquots"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.noOfPlasmaAliquots"
								id="noOfPlasmaAliquotsAdd" 
								cssClass="positive-integer" maxlength="2"></s:textfield></td>
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.noofnonviablealiquots"></s:text>:<span style="color: red;">*</span>
							</td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.noOfNonviableAliquots"
								id="noOfNonviableAliquotsAdd"
								cssClass="positive-integer" maxlength="2"></s:textfield></td>
							
						</tr>
						<tr>
							
							<!--<td width="20%"><s:text
								name="garuda.cbbprocedures.label.noofviablecellaliquots"></s:text>:<span style="color: red;">*</span>
							</td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.noOfViableCellAliquots"
								id="noOfViableCellAliquotsAdd" onfocus="getTotalCBUAliquots();"
								cssClass="positive-integer" maxlength="2"></s:textfield></td>
						-->
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.noofviablesampnotrep"></s:text>:<span style="color: red;">*</span>
							</td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.noOfViableSampleNotRep"
								id="noOfViableCellAliquotsAdd" 
								cssClass="positive-integer" maxlength="2"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%"></td>	
						</tr>		
						</table>
						
						<fieldset>
						<legend>
							<s:text name="garuda.cbbprocedures.label.aliqrepoffinalprod"></s:text>
						</legend>
						
						
						<table>		
						
						
						<tr valign="top">		<td width="22%"><s:text
								name="garuda.cbbprocedures.label.noofsegments"></s:text>:<span style="color: red;">*</span></td>
							<td width="28%"><s:textfield
								name="cbbProcedureInfoPojo.noOfSegments"
								cssClass="positive-integer" maxlength="2" onblur = "segAutoDefer(this.value,this.id);"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%"></td>		
						</tr>
						<tr>
							
						</tr>
						<tr>
						<td width="22%"><s:text
								name="garuda.cbbprocedures.label.noofothrepaliqprod"></s:text>:<span style="color: red;">*</span></td>
							<td width="28%"><s:textfield
								name="cbbProcedureInfoPojo.noOfOthRepAliqProd" cssClass="positive-integer" maxlength="2"></s:textfield>
							</td>
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.noofothrepaliqaltcond"></s:text>:<span style="color: red;">*</span></td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.noOfOthRepAliqAltCond" cssClass="positive-integer" maxlength="2"></s:textfield>
							</td>	
						</tr>
					</table>
					</fieldset>
					</div>
					</div>
					
				
					<div>
					<div id="<s:property value="cbbProcedureInfoPojo.noOfPlasmaMaterAliquots" /><s:property value="statusUpdate" />_matsmplcontent" onclick="toggleDiv('<s:property value="cbbProcedureInfoPojo.noOfPlasmaMaterAliquots" /><s:property value="statusUpdate" />_matsmpl')"
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-triangle-1-s"></span> <s:text
						name="garuda.cbbprocedures.label.maternalsamples"></s:text></div>
					<div id="<s:property value="cbbProcedureInfoPojo.noOfPlasmaMaterAliquots" /><s:property value="statusUpdate"/>_matsmpl">
					<table>
					<tr valign="top">
					<td width="20%"><s:text
								name="garuda.cbbprocedures.label.noofserummateraliquots"></s:text>:<span style="color: red;">*</span>
							</td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.noOfSerumMaterAliquots"
								id="noOfSerumMaterAliquotsAdd"
								cssClass="positive-integer"
								maxlength="2"></s:textfield></td>
								
					<td width="20%"><s:text
								name="garuda.cbbprocedures.label.noofplasmamateraliquots"></s:text>:<span style="color: red;">*</span>
							</td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.noOfPlasmaMaterAliquots"
								id="noOfPlasmaMaterAliquotsAdd"
								cssClass="positive-integer"
								maxlength="2"></s:textfield></td>			
					</tr>
					<tr valign="top">
					<td width="20%"><s:text
								name="garuda.cbbprocedures.label.noofextrdnamateraliquots"></s:text>:<span style="color: red;">*</span>
							</td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.noOfExtrDnaMaterAliquots"
								id="noOfExtrDnaMaterAliquotsAdd"
								cssClass="positive-integer"
								maxlength="2"></s:textfield></td>
								
					<td width="20%" onmouseover='return showhelpmsg("tooltipMsgMisMaternal")' onmouseout="return nd();"><s:text
								name="garuda.cbbprocedures.label.noofcellmateraliquots"></s:text>:<span style="color: red;">*</span>
							</td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.noOfCellMaterAliquots"
								id="noOfCellMaterAliquotsAdd"
								cssClass="positive-integer"
								maxlength="2"></s:textfield></td>			
					</tr>
					</table>
					</div>
					</div>
					
					<!-- div>
					<div id="<s:property value="cbbProcedureInfoPojo.pkFrozenInBag" /><s:property value="statusUpdate" />content" onclick="toggleDiv('<s:property value="cbbProcedureInfoPojo.pkFrozenInBag" /><s:property value="statusUpdate" />')"
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span
						class="ui-icon ui-icon-triangle-1-s"></span> <s:text
						name="garuda.cbbprocedures.label.sop"></s:text></div>
					<div id="<s:property value="cbbProcedureInfoPojo.pkFrozenInBag" /><s:property value="statusUpdate" />">
					<table>
						<tr>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.soprefno"></s:text>:</td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.sopRefNo"></s:textfield></td>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.soptitle"></s:text>:</td>
							<td width="30%"><s:textfield
								name="cbbProcedureInfoPojo.sopTitle"></s:textfield></td>
						</tr>
						<tr>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.sopstrtdate"></s:text>:</td>
							<td width="30%"><s:date
								name="cbbProcedureInfoPojo.sopStrtDate" id="datepicker12"
								format="MMM dd, yyyy" /> <s:textfield readonly="true" onkeydown="cancelBack();" 
								name="sopStrtDateStrAdd" class="datepic" id="datepicker12" cssClass="datePicWMaxDate"
								value="%{datepicker12}" onchange="validateDate('datepicker13','datepicker12','invalidDate3','invalidDate2');" />
							<div id="validDateDiv2"><span id="invalidDate2" class="error" ></span>
							</div>
							</td>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.soptermidate"></s:text>:</td>
							<td width="30%"><s:date
								name="cbbProcedureInfoPojo.sopTermiDate" id="datepicker13"
								format="MMM dd, yyyy" /> <s:textfield readonly="true" onkeydown="cancelBack();" 
								name="sopTermiDateStrAdd" class="datepic" id="datepicker13"
								value="%{datepicker13}" cssClass="datePicWMaxDate"
								onchange="validateDate('datepicker13','datepicker12','invalidDate3','invalidDate2');" />
							<div id="validDateDiv3"><span id="invalidDate3" class="error" ></span>
							</div>
							</td>
						</tr>
					</table>
					</div>
					</div-->
					<div>
					<table width="100%" bgcolor="#cccccc">
						<tr valign="top">
							<td align="center" width="70%"><jsp:include page="../cb_esignature.jsp"
								flush="true"></jsp:include></td>
							<td width="15%">
							<button type="button" id="submit" disabled="disabled"
								onclick="saveCBBProcedureInfo();"><s:text
								name="garuda.cbbprocedures.labelsave"></s:text></button>
							</td>
							<td width="15%">
							<button type="button" onclick="closeModal();"><s:text
								name="garuda.cbbprocedures.label.cancel"></s:text></button>
							</td>
						</tr>
					</table>
					</div>
					</div>
					</div>
					</div>
					</td>
				</tr>

			</table>
			</div>
			</td>
		</tr>
	</table>
</s:form>
<div style="display: none;">
<span id="toolTipMsg"><s:text name="garuda.cbbprocedures.label.procnametooltip"></s:text></span>
</div>
<div style="display: none;">
<span id="tooltipMsgThawed"><s:text name="garuda.cbbprocedure.label.numberThawedtooltip"></s:text></span>
</div>
<div style="display: none;">
<span id="toolTipMsgSerum"><s:text name="garuda.cbbprocedure.label.numberSerumToolTip"></s:text></span>
</div>
<div style="display: none;">
<span id="tooltipMsgMisMaternal"><s:text name="garuda.cbbprocedure.label.numberMisMatToolTip"/></span>
</div>