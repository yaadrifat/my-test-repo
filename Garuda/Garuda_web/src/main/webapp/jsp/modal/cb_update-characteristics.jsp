<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<%
String contextpath=request.getContextPath();
%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.eres.web.site.SiteJB" %>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@ page import="java.util.List,com.velos.eres.service.util.*"%>
<%
UserJB userB = (UserJB)session.getAttribute("currentUser");
request.setAttribute("userid",Integer.parseInt(userB.getUserSiteId()));											
%>
<jsp:include page="../cb_track_session_logging.jsp" />
<style>
#cbuCharacteristicsMain td{
padding-left: 5px;
}
</style>
<script>
var cbuChangesDone = false;
function autoDeferModalFormSubmitRefreshDiv(formid,url,divname,updateDivId,statusDivId){
	url=url+'&criChangeFlag='+true
	//if($j("#"+formid).valid()){	
		showprogressMgs();
			$j.ajax({
		        type: "POST",
		        url: url,
		        async:false,
		        data : $j("#"+formid).serialize(),
		        success: function (result){
		        	$j('.ui-datepicker').html("");		        
		            var $response=$j(result);		           
		            var errorpage = $response.find('#errorForm').html();		           
		            if(errorpage != null &&  errorpage != "" ){
		            	$j('#main').html(result);
		            }else{
		            	if(divname!=""){ 
			            	 $j("#"+divname).html(result);
			            }
		            	if(updateDivId!=undefined && updateDivId!=""){
		            		$j("#"+updateDivId).css('display','block');			            	
		            	}else{
		            		$j("#update").css('display','block');			            	
		            	}
		            	if(statusDivId!=undefined && statusDivId!=""){
		            		$j("#"+statusDivId).css('display','none');			            	
		            	}else{
		            		$j("#status").css('display','none');			            	
		            	}		            	
		            }
		        },
		        error: function (request, status, error) {
		        	closeprogressMsg();
		        	alert("Error " + error);
		            alert(request.responseText);
		        }

			});	
			closeprogressMsg();
    //}	
}

function multBirthAutoDefer(val,Id){
	elementValue = autoDeferField_1;	
 	if (($j("#"+Id).val()) == '1'){
		jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
				function(r) {
					if (r == false) {
						$j("#autodeferfieldflag").val("false");
					     // $j("#"+Id).val("");
					      $j("#"+Id).focus();
					      $j("#"+Id+" option[value="+elementValue+"]").attr("selected", "selected");
					      $j("#"+Id).removeClass("cbuChanges");
					  }else if(r == true){
						  //autoDefer = true;
						  $j("#autodeferfieldflag").val("true");
						  mutex_Lock = true;
						  element_Id = Id;
						  element_Name ="cdrCbuPojoModify.fkMultipleBirth";
						  autoDeferFormSubmitParam[0]='cbuCharacteristicsMain';
						  autoDeferFormSubmitParam[1] ='submitCharacteristics?';
						  autoDeferFormSubmitParam[2]='';
						  refresh_DivNo=1025;
						  refresh_CordID = '<s:property value="cdrCbuPojo.cordID"/>';
						  refreshDiv_Id[0]="defer";
						  refreshDiv_Id[1]="status1";
						  refreshDiv_Id[2]='';//"CRI";
						  commonMethodForSaveAutoDefer();
						 // autoDeferModalFormSubmitRefreshDiv('cbuCharacteristicsMain','submitCharacteristics','');
						 // refreshMultipleDivsOnViewClinical('1025','<s:property value="cdrCbuPojo.cordID"/>');
						  	if($j('#dialogForCRI').length!=0){
								closeModals('dialogForCRI');
							}
						  }
					});
	}
}

$j(function(){
	   jQuery.validator.addMethod("checkSelectCordEntry", function(value, element,params) {
		   if($j("#"+params[0]).val()==value || ($j("#"+params[0]).val()=="" && (parseInt(value) == -1))){
             return true;
			}else{
				return (true && (parseInt(value) != -1));
			}			
		}, "<%=LC.L_PlsSel_Val%>");/*}, "Please Select Value");*****/
		
		jQuery.validator.addMethod("validateRequiredCordEntry", function(value, element,params) {
	        var flag = true;
	        if($j("#"+params[0]).val()==value || ($j("#"+params[0]).val()=="")){
	        	return true;
	        }else{
	        	if(value==""){
					flag =false;
				}
			}
			return (true && flag);		 
		}, "<%=MC.M_PlsEtr_IsbtData%>");/*}, "Please Enter Valid ISBT Data");*****/			
		
		
		jQuery.validator.addMethod("checkDatesCordEntry", function(value, element, params) {
			if($j("#"+params[2]).val()==value){
				return true;				
			}else{
				return (true && (checkDates(params[0],params[1])));
			}			 		 
		}, "<%=MC.M_EtrVal_XxxxFrmt%>");/*}, "Please Check The Date You Entered");*****/

		jQuery.validator.addMethod("checkTimeFormatCordEntry", function(value, element,params) {
			if($j("#"+params[0]).val()==value){
				return true;				
			}else{
				if(value==""){
					return true;
				}else{
				 return (true && (dateTimeFormat(value)));		 
				}
			}
		}, "<%=MC.M_EtrVal_XxxxFrmt%>");/*}, "Please Enter Time format hh:mm ");*****/
		
	});  

$j(function() {
	/*var today = new Date();
	
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();

	var h=today.getHours();
	var mn=today.getMinutes()+1;

	
	$j( "#datepicker2" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
	$j( "#datepicker3" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true});
*/
//getDatePic();
assignDatePicker('birthDateId');
assignDatePicker('collectionDateId');

	var validator = $j("#cbuCharacteristicsMain").validate({
		invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    },
		rules:{	
				"cdrCbuPojoModify.fkCBUDeliveryType":{checkSelectCordEntry : ["localFkCBUDeliveryType"]},
				"babyGenderId":{validateRequiredCordEntry:["localBabyGenderId"]},
				"cdrCbuPojoModify.fkCBUCollectionType":{checkSelectCordEntry : ["localFkCBUCollectionType"]},
				"cdrCbuPojoModify.fkMultipleBirth":{checkSelectCordEntry : ["localFkMultipleBirth"]},					
				"birthDateStr":{validateRequiredCordEntry:["localBirthDateStr"],checkDatesCordEntry:["birthDateId","collectionDateId","localBirthDateStr"],dpDate: true},				
				"cdrCbuPojoModify.ethnicity":{checkSelectCordEntry : ["localEthnicity"]},
				"race":{validateRequiredCordEntry:["localRace"]},
				"cbuCollectionDateStr":{validateRequiredCordEntry:["localCbuCollectionDateStr"],checkDatesCordEntry:["collectionDateId","birthDateId","collectionDateId","localCbuCollectionDateStr"],dpDate: true},
				"cdrCbuPojoModify.cbuCollectionTime":{validateRequiredCordEntry:["localCbuCollectionTime"],checkTimeFormatCordEntry:["localCbuCollectionTime"]},
				"cdrCbuPojoModify.babyBirthTime":{validateRequiredCordEntry:["localBabyBirthTime"],checkTimeFormatCordEntry:["localBabyBirthTime"]}
			},
			messages:{
				"cdrCbuPojoModify.fkCBUDeliveryType":"<s:text name="garuda.cbu.cordentry.deliverytype"/>",
				"cdrCbuPojoModify.fkCBUCollectionType":"<s:text name="garuda.cbu.cordentry.collectiontype"/>",
				"cdrCbuPojoModify.fkMultipleBirth":"<s:text name="garuda.cbu.cordentry.MultipleBirth"/>",
				"birthDateStr":{validateRequiredCordEntry:"<s:text name="garuda.cbu.cordentry.birthdate"/>",checkDatesCordEntry:"<s:text name="garuda.cbu.cordentry.birthdatecheck"/>"},
				"babyGenderId":"<s:text name="garuda.cbu.cordentry.babygenderid"/>",
				"cdrCbuPojoModify.ethnicity":"<s:text name="garuda.cbu.cordentry.birthethnicity"/>",
				"race":"<s:text name="garuda.cbu.cordentry.race"/>",
				"cbuCollectionDateStr":{validateRequiredCordEntry:"<s:text name="garuda.cbu.cordentry.collectiondate"/>",checkDatesCordEntry:"<s:text name="garuda.cbu.cordentry.checkdates"/>"},
				"cdrCbuPojoModify.cbuCollectionTime":{validateRequiredCordEntry:"<s:text name="garuda.cbu.cordentry.collectiontime"/>",checkTimeFormatCordEntry:"<s:text name="garuda.common.validation.timeformat"/>"},
				"cdrCbuPojoModify.babyBirthTime":{validateRequiredCordEntry:"<s:text name="garuda.cbu.cordentry.babybirthtime"/>",checkTimeFormatCordEntry:"<s:text name="garuda.common.validation.timeformat"/>"}
			}
		});	
	
});
/*
jQuery(function() {
	  jQuery('#collectionDateId, #birthDateId').datepicker('option', {
	    beforeShow: customRange
	  });
	});*/

	function customRange(input) {
	 /* if (input.id == 'collectionDateId') {
	    return {
	      minDate: jQuery('#birthDateId').datepicker("getDate")
	    };
	  } else if (input.id == 'birthDateId') {
	    return {
	      maxDate: jQuery('#collectionDateId').datepicker("getDate")
	    };
	  }*/
		 if (input.id == 'collectionDateId') {
			 if($j("#birthDateId").val()!="" && $j("#birthDateId").val()!=null){
			    var minTestDate=new Date($j("#birthDateId").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
				  $j('#collectionDateId').datepicker( "option", "minDate", new Date(y1,m1,d1));
			 }else{
				 $j('#collectionDateId').datepicker( "option", "minDate", "");
			}
			  } else if (input.id == 'birthDateId') {
				  if($j("#collectionDateId").val()!="" && $j("#collectionDateId").val()!=null){
				  var minTestDate=new Date($j("#collectionDateId").val()); 	 
					var d1 = minTestDate.getDate();
					var m1 = minTestDate.getMonth();
					var y1 = minTestDate.getFullYear();
				  	$j('#birthDateId').datepicker( "option", "maxDate", new Date(y1,m1,d1));
				  }else{
					  $j('#birthDateId').datepicker( "option", "maxDate", "");
				}
			  }
			  var result = $j.browser.msie ? !this.fixFocusIE : true;
			  this.fixFocusIE = false;
			  return result;
	}

	$j(".timeValid").keypress(function(e) {
		  var val = $j(this).val();
		  if(e.keyCode!=8 && e.keyCode!=46 )
		  {
			  if(val.length==2){						  			  
				  if(e.which!=58){
					   $j(this).val(val+":");
				  }
			  }
		  }
	});


	function submitcbuinfoReviewForm(autoDeferSubmit){
	
		cbuChangesDone = divChanges('cbuChanges',$j('#indDiv').html());

		 if(autoDeferStatus(autoDeferSubmit, cbuChangesDone)){
			 	
		 
		
		var dateElements = $j("#cbuCharacteristicsMain").find('.hasDatepicker').get();
		$j.each(dateElements, function(){
			customCordDateRanges(this);
		});
		if($j("#cbuCharacteristicsMain").valid() && setMinDateWidRespCollDat() && validateHours('birthDateId','babybirthtime','collectionDateId','cbuCollectionTime','collTimeRangeErrorMsg')){
			var noMandatory=true;
			$j('#indDiv .cbuInfoMandatory').each(function(){
				 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
					 noMandatory=false;
					}
				  });
			var url = 'submitCharacteristics?noMandatory='+noMandatory+'&criChangeFlag='+true;
			if(cbuChangesDone){
			modalFormPageRequest('cbuCharacteristicsMain',url,'finalreviewmodel',$j("#cordId").val(),$j("#orderId").val(),$j("#orderType").val(),'','');
			}else/* if(($j("#cbuCharacteristicsMain_cdrCbuPojoModify_fkMultipleBirth").val()!=1))*/{
				showSuccessdiv("update","status");
			}
		}
	   }
	}

	function modalFormPageRequest(formid,url,divname,cordid,orderid,ordertype,updateDivId,statusDivId){
		showprogressMgs();
		$j.ajax({
	        type: "POST",
	        url: url,
	       // async:false,
	        data : $j("#"+formid).serialize(),
	        success: function (result){
	        	$j('.ui-datepicker').html("");
	        	//$('.tabledisplay').html("");
	            var $response=$j(result);
	            //query the jq object for the values
	            var errorpage = $response.find('#errorForm').html();
	           // alert(oneval);
	           // var subval = $response.find('#sub').text();
	            if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{
	            	if(divname!=""){ 
		            	 $j("#"+divname).html(result);
		            }
	            	if(updateDivId!=undefined && updateDivId!=""){
	            		$j("#"+updateDivId).css('display','block');			            	
	            	}else{
	            		$j("#update").css('display','block');			            	
	            	}
	            	if(statusDivId!=undefined && statusDivId!=""){
	            		$j("#"+statusDivId).css('display','none');			            	
	            	}else{
	            		$j("#status").css('display','none');			            	
	            	}
	            	/*if(orderid!=null && orderid!="" && typeof(orderid)!="undefined" && ordertype!=null && ordertype!="" && typeof(ordertype)!="undefined"){
	                	var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID='+cordid+'&orderId='+orderid+'&orderType='+ordertype+'&pkcordId='+cordid+'&currentReqFlag=true';
	            		loadMoredivs(urlb,'viewcbutoppanel,quicklinksDiv,taskCompleteFlagsDiv,ctReqClinInfoContentDiv,finalreviewcontentDiv,currentRequestProgressMainDiv','cbuProcedureMain','update','status');//url,divname,formId,updateDiv,statusDiv
	            		setpfprogressbar();
	            		getprogressbarcolor();
	                }
	            	else{
	            	var urla = "loadClinicalData?cdrCbuPojo.cordID="+cordid;
	            	loadPageByGetRequset(urla,'cordSelectedData');
	            	}*/		            	
	            }
	        },
	        error: function (request, status, error) {
	        	closeprogressMsg();
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});	
		closeprogressMsg();
		}
	
	function submitCBUInfo(cordId,autoDeferSubmit){

		cbuChangesDone = divChanges('cbuChanges',$j('#indDiv').html());
		
		if( autoDeferStatus(autoDeferSubmit, cbuChangesDone)){
		  

		var dateElements = $j("#cbuCharacteristicsMain").find('.hasDatepicker').get();
		$j.each(dateElements, function(){
			customCordDateRanges(this);
		});
		if(cbuChangesDone){
			if($j("#cbuCharacteristicsMain").valid() && setMinDateWidRespCollDat() && validateHours('birthDateId','babybirthtime','collectionDateId','cbuCollectionTime','collTimeRangeErrorMsg')){
				modalFormSubmitRefreshDiv('cbuCharacteristicsMain','submitCharacteristics','','','','1','<s:property value="cdrCbuPojo.cordID"/>');
				refreshMultipleDivsOnViewClinical('1025','<s:property value="cdrCbuPojo.cordID"/>');
				if($j('#fcrFlag').val() == "Y"){
					refreshMultipleDivsOnViewClinical('2','<s:property value="cdrCbuPojo.cordID"/>');
					}
				$j('#fromViewClinicalChar').val('1');
			}
		}else /*if(($j("#cbuCharacteristicsMain_cdrCbuPojoModify_fkMultipleBirth").val()!=1))*/{
			showSuccessdiv("update","status");
		}
	 }
	}
	
	 $j('#indDiv .cbuInfoMandatory').each(function(){		   
		 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
			 $j(this).css("border", "");
			 $j(this).css("border", "1px solid red");			
			}			 
		  });
	  $j(function(){
		   autoDeferField_1 = $j('select[name="cdrCbuPojoModify.fkMultipleBirth"] option:selected').val();		 
		  });
	  function setMinDateWidRespCollDat(){
		  var collectionDate = new Date($j("#collectionDateId").val());
		  var procStartDt; 
		  var flag = true;
     	  /*var procId = $j("#fkCbbProcedureId").val();
				  $j.ajax({
			        type: "POST",
			        url: 'deferCheckProcedure?procPk='+procId,
			        async:false,
			        success: function (result){
						procStartDt = new Date(result.procStartDateStrAdd);
						procTermiDt = new Date(result.procTermiDateStrAdd);
			  }
			  });*/
			  procStartDt = new Date($j("#procStartDtinupdateChar").val());
			  procTermiDt = new Date($j("#procTermiDtinupdateChar").val());
		  if(collectionDate < procStartDt) {
			  $j("#dynaErrorProcedureDt").show();
			  flag = false;
		  } else 
		  {
			  $j("#dynaErrorProcedureDt").hide(); 
		  }
		  
		 /* if(collectionDate>FreezeDate){
			  $j("#dynaErrorFreezeDat").show();
			  $j("#datepicker5").val("");
			  flag = false;
		  }else if(FreezeDate>=collectionDate){
			  $j("#dynaErrorFreezeDat").hide();
			}*/
		  return flag;
	  }

  function autoDeferStatus(param, cbuChangesDone) {	       
	    	 
	    		  var returnType = true;
	    		  
	    	    	 if('<s:property value="cdrCbuPojoModify.fkMultipleBirth"/>' != $j('select[name="cdrCbuPojoModify.fkMultipleBirth"]').val() && cbuChangesDone){  	    		
		    	  
		    	  if($j("#cbuCharacteristicsMain_cdrCbuPojoModify_fkMultipleBirth").val()==1){
		    		  returnType = false;
		    		  multBirthAutoDefer($j('select[name="cdrCbuPojoModify.fkMultipleBirth"]').val(),$j('select[name="cdrCbuPojoModify.fkMultipleBirth"]').attr('id'));
				    $j("#autodeferfieldflag").val("true");
		    	  }else{
		    		$j("#autodeferfieldflag").val("false");
			     
			     }
		        }
		    	 return returnType;	    			 
	}
	  
</script>
<s:form id="cbuCharacteristicsMain">
<div id="indDiv">
<s:hidden name="cordCbuStatus"/>
<s:hidden name="fromViewClinicalChar" id="fromViewClinicalChar"></s:hidden>
<s:hidden name="cdrCbuPojo.fkCBUDeliveryType" id="localFkCBUDeliveryType"></s:hidden>
<s:hidden name="localBabyGenderId" id="localBabyGenderId" value="%{#request.babyGenderId}"></s:hidden>
<s:hidden name="cdrCbuPojo.fkCBUCollectionType" id="localFkCBUCollectionType" ></s:hidden>
<s:hidden name="cdrCbuPojo.fkMultipleBirth" id="localFkMultipleBirth" ></s:hidden>
<s:hidden name="cdrCbuPojo.ethnicity" id="localEthnicity" ></s:hidden>
<s:hidden name="localRace" id="localRace" value="%{#request.race}"></s:hidden>
<s:hidden name="cdrCbuPojo.cbuCollectionTime" id="localCbuCollectionTime" ></s:hidden>
<s:hidden name="cdrCbuPojo.babyBirthTime" id="localBabyBirthTime" ></s:hidden>
<s:hidden id="userid" value="%{#request.userid}" name="userid"></s:hidden>
<s:hidden name="cdrCbuPojo.fkSponsorId" id="fkSponsorId"></s:hidden>
<s:hidden name="cdrCbuPojo.pkNMDPSponsorId" id="pkNMDPSponsorId"></s:hidden>
<s:hidden name="cdrCbuPojo.pkOtherSponsorId" id="pkOtherSponsorId"></s:hidden>
<s:hidden name="cdrCbuPojo.birthDate" id="bdate" ></s:hidden>
<s:hidden name="licenceUpdate" value="True"></s:hidden>
<s:hidden name="cdrCbuPojo.cordID" />
<s:hidden name="cdrCbuPojo.fkSpecimenId" />
<s:hidden name="cdrCbuPojo.specimen.pkSpecimen" />
<s:hidden name="esignFlag" value="%{esignFlag}"></s:hidden>
<s:hidden name="cdrCbuPojo.fkCbbProcedure" id="fkCbbProcedureId"></s:hidden>
<s:hidden value="%{#request.procStartDt}" id="procStartDtinupdateChar"></s:hidden>
<s:hidden value="%{#request.procTermiDt}" id="procTermiDtinupdateChar"></s:hidden>
<s:hidden name="submitButtonAutoDefer" id="autodeferfieldflag" value="false"/>
<s:iterator value="idmformversionlst" var="rowVal" status="row">
	<s:set name="IdmbloodCollectionDate" value="%{#rowVal[3]}"></s:set>
	<s:date name="IdmbloodCollectionDate" id="IdmbloodCollectionDate" format="MMM dd, yyyy" />
	<s:hidden value="%{IdmbloodCollectionDate}" id="idmbloodCollectionDate" name="IdmbloodCollectionDate"/>
</s:iterator>
  <div id="update" style="display: none;" >
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="garuda.message.modal.cbu" name="message"/>
		  </jsp:include>	   
	
</div>

<table width="100%" cellspacing="0" cellpadding="0" border="0" id="status">
   <s:if test="#request.update==true" ></s:if>
	<tr>
  <td colspan="2"/>
  </tr>
  <tr>
  <td>
   <!--<fieldset>
					<legend><s:text name="garuda.cdrcbuview.label.ids" /></legend>
  <table cellpadding="0" cellspacing="0" border="0" >
	<tr>			
		<td width="25%">
			<s:text name="garuda.cbuentry.label.externalcbuid" />:<span style="color: red;">*</span>
		</td>
		<td width="25%">
			<s:textfield name="externalCbuId" value="%{externalCbuId}" ></s:textfield>
		</td >
		<td width="25%">
			<s:text name="garuda.cbuentry.label.idoncbubag" />:
		</td>
		<td width="25%">
			<s:select name="cbuOnBag" multiple="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_IDS]" listKey="pkCodeId" listValue="description" ></s:select>
		</td>
	</tr>
	<tr align="left">
		<td width="25%"><s:text name="garuda.cbuentry.label.registrycbuid"/>:<span style="color: red;">*</span>					
		</td>
		<td width="25%"><s:textfield id="cburegid" name="cdrCbuPojoModify.registryId" ></s:textfield>
		</td>
		<td width="25%"><s:text name="garuda.cbuentry.label.maternalregistryid"/>:<span style="color: red;">*</span></td>				  
		<td width="25%"><s:textfield id="regMaternalId" name="cdrCbuPojoModify.registryMaternalId" ></s:textfield>
		</td>
	</tr >
	<tr align="left">
		<td width="25%"><s:text name="garuda.cbuentry.label.localcbuid"/>:<span style="color: red;">*</span></td>
		<td width="25%"><s:textfield id="loccbuid" name="cdrCbuPojoModify.localCbuId" ></s:textfield>
		</td>
		<td width="25%"><s:text name="garuda.cbuentry.label.maternallocalid"/>:<span style="color: red;">*</span></td>
		<td width="25%"><s:textfield id="matlocalid" name="cdrCbuPojoModify.localMaternalId" ></s:textfield>
		</td>
	</tr>
	<tr align="left">
	    <td width="25%"><s:text name="garuda.cordentry.label.isbtid"/>:<span style="color: red;">*</span></td> 
		<td width="25%"><s:textfield id="isbidin" name="cdrCbuPojoModify.cordIsbiDinCode" ></s:textfield>
		</td>
		<td width="25%"><s:text name="garuda.cdrcbuview.label.isbt_product_code"/>:
		</td><td width="25%"><s:textfield name="cdrCbuPojoModify.cordIsitProductCode" ></s:textfield>					              
		</td>
	</tr>
  </table>
</fieldset>
--></td>
</tr>
<tr >
<td>
	<fieldset>
					<legend><s:text name="garuda.cordentry.label.cbudemographics" /></legend>
  <table cellpadding="0" cellspacing="0" border="0" >
	<tr align="left">
	
		<td width="25%">
			<s:text name="garuda.cbuentry.label.gender" />:
        </td>
        <td width="25%">
        	<s:select cssClass="cbuInfoMandatory" name="babyGenderId" onchange="isChangeDone(this,'%{babyGenderId}','cbuChanges');" id="babyGenderId1" list="populatedData.genderIdList" listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select" />
        </td>
        <td colspan="2">&nbsp;
        </td>
     </tr>
     <tr align="left">
		<td width="25%">
         	<s:text name="garuda.cbuentry.label.birthdate" />:
        </td>
        <td width="25%" valign="top"><s:date name="birthDate" id="birthDateId" format="MMM dd, yyyy" />
        <s:hidden name="localBirthDateStr" id="localBirthDateStr" value="%{birthDateId}"></s:hidden>
        <s:textfield readonly="true" onkeydown="cancelBack();"  name="birthDateStr" class="datepic" id="birthDateId" value="%{birthDateId}" cssClass="birthDate" onchange='isChangeDone(this,"%{birthDateId}","cbuChanges");'
        />
		</td>
        <td width="25%">
        	<s:text name="garuda.cbuentry.label.collectiondate" />:
        </td>
        <td width="25%"><s:date name="cbuCollectionDate" id="collectionDateId" format="MMM dd, yyyy" />
        <s:hidden name="localCbuCollectionDateStr" id="localCbuCollectionDateStr" value="%{collectionDateId}"></s:hidden>
        <s:textfield readonly="true" onkeydown="cancelBack();"  name="cbuCollectionDateStr" class="datepic" id="collectionDateId"  value="%{collectionDateId}" cssClass="collectDate cbuInfoMandatory"
        	onchange="setMinDateWidRespCollDat();validateIDMBloodCollDate(this.value,'%{IdmbloodCollectionDate}','IdmbldWrngMsg');isChangeDone(this,'%{collectionDateId}','cbuChanges');"/>
        <br/><span style="display:none" id="dynaErrorProcedureDt" class="error"><s:text name="garuda.cbu.cordentry.prcdrDateComp"></s:text> </span>
        <span style="display:none" id="birthdateErrorMsg" class="error"><s:text name="garuda.cbu.cordentry.birthdatemust"></s:text> </span>
    	<span style="display:none" id="birthdateRangeErrorMsg" class="error"><s:text name="garuda.cbu.cordentry.collectiondatehrs"></s:text> </span>
    	<span style="display:none" id="IdmbldWrngMsg" class='error'><br/><s:text name="garuda.label.dynamicform.bldWrngMsg"></s:text></span>				
		</td>
     </tr>
     <tr align="left">
		  <td width="25%" ><s:text name="garuda.cbuentry.label.babybirthtime" />:</td>
	      <td width="25%" ><s:textfield name="cdrCbuPojoModify.babyBirthTime" id="babybirthtime" cssClass="timeValid" maxlength="5" onchange="isChangeDone(this,'%{cdrCbuPojoModify.babyBirthTime}','cbuChanges');"></s:textfield>(HH:MM 24 HR)</td>	
	      <td width="25%" ><s:text name="garuda.cbuentry.label.collectiontime" />:</td>
	      <td width="25%" ><s:textfield name="cdrCbuPojoModify.cbuCollectionTime" cssClass="timeValid" id="cbuCollectionTime" maxlength="5" onchange="validateHours('birthDateId','babybirthtime','collectionDateId','cbuCollectionTime','collTimeRangeErrorMsg');isChangeDone(this,'%{cdrCbuPojoModify.cbuCollectionTime}','cbuChanges');"></s:textfield>(HH:MM 24 HR) 
	      <br/><span style="display:none" id="collTimeRangeErrorMsg" class="error"><s:text name="garuda.cbu.cordentry.collectiondateTimehrs"></s:text> </span>
	      </td>
	 </tr>
	  <!-- 
     <tr align="left">
		<td width="25%">
			<s:text name="garuda.cbuentry.label.abo" />:
        </td>
        <td width="25%">
        	<s:select name="aboBloodType" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BLOOD_GROUP]"
						listKey="pkCodeId" listValue="description" headerKey=""	headerValue="Select"  />
        </td>
        <td width="25%">
        	<s:text name="garuda.cbuentry.label.rhtype" />:
        </td>
        <td width="25%">
        	<s:select name="rhType" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@RH_TYPE]"
						listKey="pkCodeId" listValue="description" headerKey=""	headerValue="Select"  />
        </td>
     </tr>
     <tr align="left">
		<td width="25%">
			<s:text name="garuda.cbuentry.label.bcresult" />:
        </td>
        <td width="25%">
        	<s:select name="bacterialResult" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS]"
						listKey="pkCodeId" listValue="description" headerKey=""	headerValue="Select"  />
		</td>
        <td width="25%">
        	<s:text name="garuda.cbuentry.label.fcresult" />:
        </td>
        <td width="25%">
        	<s:select name="fungalResult" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS]" listKey="pkCodeId" listValue="description"
							headerKey="" headerValue="Select"  />
         </td>

    </tr>
     -->
    <tr align="left">
		<td width="25%"><s:text name="garuda.cordentry.label.ethnicity"/>:</td> 
		<td width="25%"><s:select id="ethid" name="cdrCbuPojoModify.ethnicity" cssClass="cbuInfoMandatory" onchange="isChangeDone(this,'%{cdrCbuPojoModify.ethnicity}','cbuChanges'); (typeof yourFunctionName == 'function' && changeProgress(1,'ethid'));" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ETHNICITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select Ethnicity"></s:select>
		</td>
		<td width="25%"><s:text name="garuda.cbuentry.label.race"/>:</td><input type="hidden" id="cbuRaceQ" value='<s:if test="race!=null"><s:property value="%{race[0]}"/><s:iterator status="row" begin="1" end="%{race.length-1}">,<s:property value="%{race[#row.index+1]}"/></s:iterator></s:if>' />
		<td width="25%"><s:select multiple="true" name="race" style="height:80px;" cssClass="cbuInfoMandatory" onchange="getCbuRace(this,'cbuChanges');" size="#request.race.size" list="populatedData.raceList" listKey="pkCodeId" listValue="description" ></s:select>					              
		</td>
	</tr>	
	<tr align="left">			   
		<td width="25%"><s:text name="garuda.cordentry.label.cbucolltype" />:</td>
		<td width="25%">
		<s:select cssClass="cbuInfoMandatory" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_COLLECTION_TYPE]" onchange="isChangeDone(this,'%{cdrCbuPojoModify.fkCBUCollectionType}','cbuChanges');" name="cdrCbuPojoModify.fkCBUCollectionType" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" ></s:select>
		</td>
		<td width="25%">
			<s:text name="garuda.cordentry.label.cbudelivtype" />:</td>
		<td width="25%">
			<s:select cssClass="cbuInfoMandatory" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_DELIV_TYPE]" name="cdrCbuPojoModify.fkCBUDeliveryType" onchange="isChangeDone(this,'%{cdrCbuPojoModify.fkCBUDeliveryType}','cbuChanges');" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" ></s:select>
		</td>									     
	</tr>
	<tr align="left">
		<td width="25%"><s:text name="garuda.cordentry.label.multiplebirth" />:</td>
		<td width="25%">
			<s:select list="#{1:'Yes',0:'No'}" name="cdrCbuPojoModify.fkMultipleBirth" headerKey="-1" headerValue="Select" cssClass="cbuInfoMandatory" onchange="multBirthAutoDefer(this.value,this.id);isChangeDone(this,'%{cdrCbuPojoModify.fkMultipleBirth}','cbuChanges');"></s:select>
		</td>
		<td width="25%"><s:text name="garuda.cordentry.label.nmdpbroadrace" />:</td>
		<td width="25%">
		          <s:if test="cdrCbuPojoModify.nmdpRaceId!=null">
				        <s:select disabled="true" name="cdrCbuPojoModify.nmdpRaceId" onchange="isChangeDone(this,'%{cdrCbuPojoModify.nmdpRaceId}','cbuChanges');" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_BROAD_RACE]" listKey="pkCodeId" listValue="description" ></s:select>
				  </s:if>
				  <s:else>
				        <s:textfield name="cdrCbuPojoModify.nmdpRaceId" readonly="true" onkeydown="cancelBack();" onchange="isChangeDone(this,'%{cdrCbuPojoModify.nmdpRaceId}','cbuChanges');" ></s:textfield>
				  </s:else>
		</td>
	</tr>
	<tr align="left">
	<%-- 	<td width="25%">
			   <s:text name="garuda.cordentry.label.hrsabroadrace" />:</td>
		<td width="25%">
			   <s:if test="cdrCbuPojoModify.hrsaRaceRollUp!=null">
				        <s:select disabled="true" name="cdrCbuPojoModify.hrsaRaceRollUp" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@RACE_ROLLUP]" listKey="pkCodeId" listValue="description" ></s:select>
				</s:if>
				<s:else>
				        <s:textfield name="cdrCbuPojoModify.hrsaRaceRollUp" readonly="true" onkeydown="cancelBack();" ></s:textfield>					
				</s:else>
		</td> --%>	
		 <td colspan="2"></td>
	</tr>
    </table>
    </fieldset>
    </td>
    </tr>
   <tr>
   <tr bgcolor="#cccccc">
   <td align="center" width="100%">
   <table><tr><td colspan="2" align="right">
   			<jsp:include page="../cb_esignature.jsp" flush="true">
   				<jsp:param value="cbuinfosubmit" name="submitId"/>
				<jsp:param value="cbuinfoinvalid" name="invalid" />
				<jsp:param value="cbuinfominimum" name="minimum" />
				<jsp:param value="cbuinfopass" name="pass" />
   			</jsp:include></td>
   			<td colspan="2" align="left">
           	<s:if test="esignFlag == 'review'">
           	<input type="button" name="characteristicsEdit" disabled="disabled" id="cbuinfosubmit" value="<s:text name="garuda.unitreport.label.button.submit"/>" onclick="submitcbuinfoReviewForm('submitButtonAutoDefer');">
           	</s:if>
           	<s:else>
           	<input type="button" name="characteristicsEdit" disabled="disabled" id="cbuinfosubmit" value="<s:text name="garuda.unitreport.label.button.submit"/>" onclick="submitCBUInfo('<s:property value="cdrCbuPojo.cordID"/>','submitButtonAutoDefer==true');">
           	</s:else>
          	<input type="button" onclick="closeModal();" value="<s:text name="garuda.common.lable.cancel"/>" />
      		</td></tr>
   </table>
   </td>    
   </tr>
   </table>
  
  </div>
</s:form>
<script>

function getCbuRace(thisval,classname){
	isChangeDone(thisval,$j('#cbuRaceQ').val(),classname);
}

$j('[name="cordCbuStatus"]').val('<s:property value="cdrCbuPojo.cordCbuStatuscode"/>');

</script>