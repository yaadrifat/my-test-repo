<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@ include file="../cb_includes.jsp"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<script>
$j(function(){

	var orderType = $j("#requestType").val();
	var sampleAtLab = $j("#sampleAtLab").val();
	if(orderType=="CT"){
 	 	if(sampleAtLab=="Y"){
 	 		orderType='<s:text name="garuda.orOrderDetail.portletname.ctlaborder"/>';
 	 	 }
 	 	else if(sampleAtLab=="N"){
 	 		orderType='<s:text name="garuda.orOrderDetail.portletname.ctshiporder"/>';
 	 	 }
 	 }
    var orderDate = $j("#requestDate1").val();
	var data = orderType + ", " + orderDate;
	$j(".typeTimeVal").each(function(){
		$j(this).val(data);
	});
});

</script>
<div id="clinicalNoteDiv" class='popupdialog tabledisplay' >
<s:form id="clinical" name="clinical">
<s:hidden name="cdrCbuPojo.cordID"></s:hidden>
<s:hidden name="clinicalNotePojo.pkNotes" />
<s:hidden name="clinicalNotePojo.userName"/>	
<s:hidden name="clinicalNotePojo.createdBy" id="createdBy"></s:hidden>
<s:hidden name="getCodeListDescById(clinicalNotePojo.requestType)" id="requestType"/>
<s:date name="clinicalNotePojo.requestDate" id="dateTime" format="MMM dd, yyyy" />
<s:hidden name="dateTime" id="requestDate1"/>
<s:set name="createdBy" value="clinicalNotePojo.createdBy" scope="request"/>
  <table width="100%" cellspacing="0" cellpadding="0" border="1" id="status">
    <tr><td><span style="color: red;">
  <s:if test="clinicalNotePojo.amended==true">
  <s:text name="garuda.clinicalnote.label.attention_msg"/>
  </s:if></span></td></tr>
     <tr>
   <td >
				<s:text name="garuda.common.message.note" />
				
	</td>
	</tr>
	<tr>
   <td>  
   <div style="overflow: auto;">
    <s:property escapeHtml="false" value="%{clinicalNotePojo.notes}" />
    </div>
    </td>
   </tr>
  <tr>
   <td>
   <table cellspacing="0" class="displaycdr">
       	<tr>
   			<td>
   					<s:text name="garuda.clinicalnote.label.subject" />
   			</td>
   			<td>
					<s:textfield name="clinicalNotePojo.subject" id="subject" style="width: 200px" disabled="true"/>
			</td>
			<td><s:text name="garuda.clinicalnote.label.tagNote" /></td>
		<td>
					<s:checkbox name="clinicalNotePojo.tagNote" id="checkbox" disabled="true" value="clinicalNotePojo.tagNote"></s:checkbox>
		</td>
   	</tr>
   <tr>
   <tr>
		<%-- <td>
				<s:text name="garuda.clinicalnote.label.publicPrivate" />
		</td>

				<td>
				<s:radio name="clinicalNotePojo.noteVisible" id="visible"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]" listKey="subType" listValue="description" id="noteVisible" disabled="true" />
   				</td> --%>
   		
   				

	</tr>
	<tr>
			<td>
    				<s:text name="garuda.clinicalnote.label.category" />
    		</td>
    		<td>
    				<s:select name="clinicalNotePojo.fkNotesCategory" style="width:200px"	list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT]"
					listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select" disabled="true" />
   			</td>
			
	</tr>
	
		<tr>
    	
  				<td>
						<s:text name="garuda.clinicalnote.label.keyword" />
				</td>
				<td>
						<s:textfield name="clinicalNotePojo.keyword" id="keyword" disabled="true" onblur="javascript:getKeyword();" style="width: 200px"  />
				</td>
   		</tr>	
   							<tr>
					
								<td><s:text name="garuda.clinicalnote.label.requestType"/>
								<s:text name="/"/>
								<s:text name="garuda.clinicalnote.label.requestDate"/></td>
								<td>		
										
									<s:textfield id="typeTimeVal" disabled="true" style="width:200px"  cssClass="typeTimeVal"/>		
								</td>

						
						</tr> 
   		<%-- <tr>
   		<td>
   				<s:text name="garuda.clinicalnote.label.dateTime" />
   		</td>
   				<td>
   					<s:date name="clinicalNotePojo.createdOn" id="dateTime" format="MMM dd, yyyy / hh:mm:ss a" />
					<s:textfield name="dateTime" readonly="true" onkeydown="cancelBack();"  style="width:200px" />
   			
   				</td>
   		</tr>
   		
   		<tr>
   				<td>
   						<s:text name="garuda.clinicalnote.label.user" />
   				</td>
   						<td>
   					
   						<%
														String cellValue1 = request.getAttribute("createdBy").toString();
														UserJB userB = new UserJB();
														userB.setUserId(EJBUtil.stringToNum(cellValue1));
														userB.getUserDetails();													
													%>		
													<%=userB.getUserFirstName()+" "+userB.getUserLastName() %>	
   						
   						
   															
					</td>
   				
   		</tr> --%>
	</table>
	</td>
   </tr>
        
    <tr>
  
  </tr>
   
   <tr>
  
 
    		
   
 </table>

  </s:form> 
</div>