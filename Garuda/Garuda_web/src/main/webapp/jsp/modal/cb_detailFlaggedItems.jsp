<%@page import="com.velos.ordercomponent.business.util.Utilities"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s"  uri="/struts-tags"%>

<%
	UserJB userB = (UserJB) session.getAttribute("currentUser");
	request.setAttribute("assessmentuser",userB.getUserId());
%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<script>
function closePerticularDiv(){
	if('<s:property value="esignFlag"/>'=="review"){
		hideDiv();
		}
	else
		closeModals('dialogForCRI');
	
}
$j(function(){
	calculateWidth();
	$j('#dialogForCRI').bind( "dialogresize", function(event, ui) {
		calculateWidth();
    });
});
function calculateWidth(){
	var dialogWidth = $j('#dialogForCRI').parent().width();
    var rightcontWidth = dialogWidth - 250;
	$j('#assessmentRemark_Div').width(rightcontWidth);
	$j('#assessmentFlagComment_Div').width(rightcontWidth);
}
</script>
<s:form>
<fieldset>
	<s:if test="assessmentPojo.assessmentpostedby!=null">
		<s:set name="assessmentpostuser" value="%{assessmentPojo.assessmentpostedby}" scope="request"></s:set>
	</s:if><s:elseif test="assessmentpostedby!=null">
		<s:set name="assessmentpostuser" value="%{assessmentpostedby}" scope="request"></s:set>
	</s:elseif><s:else>
		<s:set name="assessmentpostuser" value="%{#request.assessmentuser}" scope="request"></s:set>
	</s:else>
<legend><s:property value="#request.entityType"/></legend>
			<center>
			<%
							String assessmentpostedby = request.getAttribute("assessmentpostuser").toString();
							userB = new UserJB();
							if(assessmentpostedby!=null && assessmentpostedby!=""){
								userB.setUserId(EJBUtil.stringToNum(assessmentpostedby));
								userB.getUserDetails();
							}
							
						%>
						<s:text name="garuda.clinicalnote.label.detailsEnteredBy"/>&nbsp;<%=userB.getUserLastName() + " " + userB.getUserFirstName()%>
						<s:text name="garuda.completeReqInfo.label.on"/>&nbsp; 
						<s:if test="%{assessmentPojo.assessmentpostedon!=null}">
							<s:date	name="%{assessmentPojo.assessmentpostedon}"
								id="assessmentPostedDate" format="MMM dd, yyyy" />
						</s:if>
						<s:elseif test="%{assessmentPojo.createdOn!=null}">
							<s:date	name="%{assessmentPojo.createdOn}"
								id="assessmentPostedDate" format="MMM dd, yyyy" />
						</s:elseif>
						<s:property value="%{assessmentPostedDate}" />
			</center>
							<br/>
							<s:if test="%{formsDataList != null && formsDataList.size()>0}">
									<table class="displaycdr" width="100%">
										<thead>
											<tr>
												<th><s:text name="garuda.label.dynamicform.question"/></th>
												<th><s:text name="garuda.label.dynamicform.description"/></th>
												<th> <s:text name="garuda.label.dynamicform.response"/> </th>
											</tr>
										</thead>
										<tbody>
										<s:iterator value="%{formsDataList}" var="rowVal" status="rowindex" begin="0" end="%{formsDataList.size() - 5}">
												<s:if test="%{#rowindex.odd}">
												<tr>
													<s:set value="%{#rowVal}" scope="request" name="formQues"/>
													<% 
														String quest = request.getAttribute("formQues").toString();
														String[] questArry = null;
														String quesNo = null;
														String QuesDesc = null;
														String lastStr = quest.substring(quest.length()-1);
														if(quest != null && !quest.equals("")){
															questArry = quest.split("\\)");
														}
														if(questArry[0]!=null){
															quesNo = questArry[0];
															request.setAttribute("quesNo",quesNo);
														}
														if(questArry[1]!=null){
															QuesDesc = questArry[1];
															for(int i=2;i<questArry.length;i++){
																QuesDesc +=") "+questArry[i];
															}
															if(lastStr.equals(")")){
																QuesDesc = QuesDesc.concat(lastStr);
															}
															request.setAttribute("QuesDesc",QuesDesc);
														}
													%>
													<td><s:property value="%{#request.quesNo}"/></td>
													<td><s:property value="%{#request.QuesDesc}" escapeHtml="false"/></td>
												</s:if>
												<s:if test="%{#rowindex.even}">
													<td><s:property value="%{#rowVal}"/></td>
												</tr>
												</s:if>
										</s:iterator>
										</tbody>
									</table>
							</s:if>
							<table width="100%">
							<s:if test="%{clinicalNotePojo!=null && clinicalNotePojo.notes!=null}">
							<tr>
								<td>&nbsp;</td>
								<td><s:text name="garuda.cdrcbuview.label.clinical_notes"></s:text>:</td>
								<td><s:property value="clinicalNotePojo.notes"  escapeHtml="false"/></td>							
							</tr>
							</s:if>
							<s:if test='assessmentPojo.columnReference=="FungalCulture"'>
							<tr>
							<td>&nbsp;</td>
							<td><s:text name="garuda.minimumcriteria.label.fungaltesting"></s:text>:</td>
							<td><s:property value="getCodeListDescByPk(cdrCbuPojo.fungalResult)"/></td>
							</tr>
							</s:if>
							<s:if test='assessmentPojo.columnReference=="Hemoglobinopathy Testing"'>
							<tr>
							<td>&nbsp;</td>
							<td><s:text name="garuda.minimumcriteria.label.hemoglobin"></s:text>:</td>
							<td><s:property value="getCodeListDescByPk(cdrCbuPojo.hemoglobinScrn)"/></td>
							</tr>
							</s:if>
							<s:if test="%{assessmentPojo.assessmentFlagComment!=null && assessmentPojo.assessmentFlagComment!=''}">
								<tr>
								<td>&nbsp;</td>
								<td><s:text name="garuda.label.assessment.commentforflag"></s:text></td>
								<td>
									<div style="overflow: auto;padding: 5px;" id="assessmentFlagComment_Div">
										<s:property value="assessmentPojo.assessmentFlagComment" escapeHtml="false"/>
									</div>
								</td>
								</tr>
							</s:if>
							<tr><td></td><td><s:text name="garuda.clinicalnote.label.assessment"></s:text></td><td><s:property value="getCodeListDescByPk(assessmentPojo.assessmentResp)"/></td>
							</tr>
							<s:if test="%{assessmentPojo.assessmentRemarks!=null && assessmentPojo.assessmentRemarks!=''}">
								<tr>
								<td>&nbsp;</td>
								<td><s:text name="garuda.label.assessment.assessnote"></s:text></td>
									<td>
										<div style="overflow: auto;padding: 5px;" id="assessmentRemark_Div">
											<s:property value="assessmentPojo.assessmentRemarks" escapeHtml="false"/>
										</div>
									</td>
								</tr>
							</s:if>
							<s:if test="clinicalNotePojo!=null && clinicalNotePojo.fkNotesCategory!=null">
							<tr><td></td><td><s:text name="garuda.finalcbu.eligibility.category"></s:text>:</td><td><s:property value="getCodeListDescByPk(clinicalNotePojo.fkNotesCategory)"/></tr>
							</s:if>
							<tr><td>&nbsp;</td><td><s:text name="garuda.cbufinalreview.label.flaggedItems.reference"></s:text>:</td><td><s:property value="assessmentPojo.columnReference"/></td></tr>
							<tr><td></td><td><s:text name="garuda.clinicalnote.label.visibleToTc"></s:text>:</td><td><s:if test="assessmentPojo.visibleTCFlag==0"><s:text name="garuda.clinicalnote.label.no" /></s:if><s:else><s:text name="garuda.clinicalnote.label.yes" /></s:else> </td></tr>
							<!-- <tr><td></td><td><s:text name="garuda.clinicalnote.label.visibleCm"></s:text>:&nbsp;</td><td></td></tr> -->
							</table>
							<table align="center">
							<tr><td align="center"><input type="button" onclick="closePerticularDiv();" value="<s:text name="garuda.common.close"/>"/></td></tr>
							</table>
							
</fieldset>
</s:form>