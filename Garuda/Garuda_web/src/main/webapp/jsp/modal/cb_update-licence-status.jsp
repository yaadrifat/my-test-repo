<%@ taglib prefix="s"  uri="/struts-tags"%>
<%
String contextpath=request.getContextPath();
%>
<%@ page import="java.util.List,com.velos.eres.service.util.*"%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/common_custom_methods.js" ></script>

<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:include page="../cb_track_session_logging.jsp" />
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%
	HttpSession tSession = request.getSession(true); 
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	int updateLicStat1=0;

	updateLicStat1 = Integer.parseInt(grpRights.getFtrRightsByValue("CB_LICSTAT"));

	request.setAttribute("updateLicStat1",updateLicStat1);
%>
<script>

$j("#licensehistory").dataTable();
var licChangesDone = false;
	
$j(function() {
	/*$j( "#datepicker" ).datepicker({dateFormat: 'M dd yy',changeMonth: true,
		changeYear: true});
	$j( "#datepicker1" ).datepicker({dateFormat: 'M dd yy',changeMonth: true,
		changeYear: true});
	*/
	getDatePic();
});

function formSubmission(){
	modalFormSubmitRefreshDiv('license','submitStatusLicense','main');
}

function modalFormSubmit(formid,url){
if($j("#"+formid).valid()){ 	
		$j("#update").div('display','block');
	$j.ajax({
		type : "POST",
		async : false,
		url : url,
		data : $j("#"+formid).serialize(),
		success : function(result) {
			//showUnitReport("close");
			$j('#main').html(result);
			//jQuery(".popupdialog").dialog("refresh");
			//closePopUp();	
			//document.getElementById("lbl1").innerHTML="Data Saved Successfully";
			
		}
	
	});
	
}
}
$j(function(){
	
	$j("#license").validate({
		rules:{			
				"cbuLicStatus":{required:true},
				"licChangeReason":{required:true},
				
				"reasons":{
											required:{
            									depends: function(element) {
														 return (($j("#cbuLicStatus").val()==$j("#unLicensedPk").val()) && ('<s:property value="%{isTasksRequired(cdrCbuPojo.cordID)}"/>'=='false'));
														 }
					}
											}
			},
			messages:{

				"cbuLicStatus":"<s:text name="garuda.cbu.license.licensurestatus"/>",
				"licChangeReason":"<s:text name="garuda.update.status.resonToChange"/>",
				"reasons":"<s:text name="garuda.cbu.license.unlicreason"/>"
							
			}
		});
	
	});

function licenSubmit(){
//alert('Page not found');
	$j('#licensureerror').html('');
	var unLicRes = $j('#unLStatusRes').val();
	unLicRes = $j.trim(unLicRes);
	if($j('#cbuLStatus').val()==null || $j('#cbuLStatus').val()==""){
		$j('#licensureerror').html('<s:text name="garuda.cbu.fcr.license.req"/>');
	}
	else if($j('#cbuLStatus').val()==$j('#unlicenStatus').val() && (unLicRes=="" || unLicRes=="0") && ('<s:property value="%{isTasksRequired(cdrCbuPojo.cordID)}"/>'=='false')){
		$j('#licensureerror').html('<s:text name="garuda.cbu.fcr.unlicense.req"/>');
	}
	else{
		if($j('#reviewLicConfirmFlg').val()!=1){
			refreshDiv1('submitLicensureFinalReview','first_finalreivew','license');	
			RefreshPrevious('<s:property value="cdrCbuPojo.cordID"/>');
		}else{
			hideDiv();
			}
	}

}

$j(document).ready(function(){
	
	//lert($j('input:radio[name=acceptance]:checked').val()+'<s:property value="cbuFinalReviewPojo.reviewCordAcceptance"/>');
	
	var licReasonChange = $j('#validate_count').val();
	if(licReasonChange!=null){
		licReasonChange = $j.trim(licReasonChange);
	}
	if($j('#licensureConfirmFlag').val()=="1" && licReasonChange=="" && $j('#eligibleConfirmFlag').val()=="1" && $j('#finalReviewConfirmStatus').val()=="Y"){
			$j('#cbuLicStatus').attr('disabled',true);
			if('<s:property value="%{isTasksRequired(cdrCbuPojo.cordID)}"/>'=='false'){
			$j('#fkCordCbuUnlicenseReason').attr('disabled',true);
			}
		}
	
	$j('#licenseDiv .licensureMandatory').each(function(){
		 $j(this).css("border", "1px solid #DBDAD9");
		 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
			 $j(this).css("border", "1px solid red");
			 $j('#licensureFlag').val("0");
			}
	});
	//$j('#cbuLicStatus').change(function(){
		//resetingEligibleProcess(this.value);
	//});
	
});

/*function resetingEligibleProcess(val){
	if(('<s:property value="cdrCbuPojo.fkCordCbuEligible"/>'=='<s:property value="#request.pkEligible"/>') && val=='<s:property value="cdrCbuPojo.unLicesePkid"/>'){
		jConfirm('<s:text name="garuda.update.eligibility.resetConfirm"/>', '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
		    if(r==true){
		    	showModal('Update Licensure Status','updateModalLicenseStatus?licenceUpdate=True&resetFun=reset&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal&cbuLicStatus='+val,'400','500');
			    }
		    else{
			   $j('#cbuLicStatusid').val('<s:property value="cbuLicStatusid"/>');
			   closeModal();
		    }
		});
	}
}
*/
function enableFields(value){
	value = $j.trim(value);
	if(value==""){
		document.getElementById("errormsg").innerHTML="<s:text name="garuda.update.status.resonToChange"/>";
		$j('#cbuLicStatus').attr('disabled',true);
		if('<s:property value="%{isTasksRequired(cdrCbuPojo.cordID)}"/>'=='false'){
		$j('#fkCordCbuUnlicenseReason').attr('disabled',true);
		}
		}
	if(value!=""){
		document.getElementById("errormsg").innerHTML="";
		$j('#cbuLicStatus').attr('disabled',false);
		if('<s:property value="%{isTasksRequired(cdrCbuPojo.cordID)}"/>'=='false'){
		$j('#fkCordCbuUnlicenseReason').attr('disabled',false);
		}
		}
}
function beforelicensuresubmit(formid){
	
	licChangesDone = divChanges('licChanges',$j('#licenseDiv').html());
	if($j("#"+formid).valid()){
		if(licChangesDone){
			setEmptyOrders();
			var licReasonChange = $j('#validate_count').val();
			if(licReasonChange!=null){
				licReasonChange = $j.trim(licReasonChange);
			}
			if($j('#licensureConfirmFlag').val()=="1" && $j('#validate_count').val()=="" && $j('#eligibleConfirmFlag').val()=="1" && $j('#finalReviewConfirmStatus').val()=="Y"){
				 document.getElementById("errormsg").innerHTML="<s:text name="garuda.update.status.resonToChange"/>";
			}
			else{
				
				var historyArr=new Array();
				var visibleHistoryPart="";
				var notvisibleHistoryPart="";
				var i=0; 
				
			
				setTimeout(function(){
					$j('.detailhstry').each(function(){
						if($j(this).is(':visible')){
							var idval=$j(this).attr("id");
							historyArr[i]=idval;
							i++;
						}
					});
				},0);
				
				
				if($j('#cReqhistory').is(":visible")){
					visibleHistoryPart="cReqhistory";
					notvisibleHistoryPart="chistory";
				}
				if($j('#chistory').is(":visible")){
					visibleHistoryPart="chistory";
					notvisibleHistoryPart="cReqhistory";
				}
				
				if($j("#orderId").val()!=null && $j("#orderId").val()!='' && $j("#orderId").val()!='undefined'){
					setTimeout(function(){
						loadMoredivs('submitStatusLicenseFromPF?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j('#cordId').val(),'indshowdiv,eligiLiceDiv,reqInfoFinalReviewDiv,ctReqClinInfoContentDiv,finalreviewcontentDiv,currentRequestProgressMainDiv,taskCompleteFlagsDiv,chistory,cReqhistory,quicklinksDiv','license','update','status');
						$j('input[name=acceptance]').attr({'disabled': true});
						setpfprogressbar();
				 		getprogressbarcolor();
				 		historyTblOnLoad();
				 		loadMoredivs('getctshipdetails?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val(),'cbuShipmentInfoContentDiv,quicklinksDiv','cbuCordInformationForm');
						cbushipflag=1;
						//getDayfromshipdate($j("#datepicker15").val(),'cbushimentdayDiv');
					},0);
				}else{
					setTimeout(function(){
						modalFormSubmitRefreshDiv('license','submitStatusLicense?pkcordId='+$j('#cordId').val(),'','','','1','<s:property value="cdrCbuPojo.cordID"/>');
					},0);
					setTimeout(function(){
						loadMoredivs('getCbuHistory?pkcordId=<s:property value="cdrCbuPojo.cordID" />','cReqhistory,chistory','cbuCordInformationForm');
						if(visibleHistoryPart!=""){
							$j('#'+visibleHistoryPart).show();
							$j('#'+notvisibleHistoryPart).hide();
						}
						$j('#cbuHistoryForm').removeClass('historyLoaded')
						historyTblOnLoad();
						
					},0);
					setTimeout(function(){
						refreshMultipleDivsOnViewClinical('1025','<s:property value="cdrCbuPojo.cordID"/>');
						if('<s:property value="cdrCbuPojo.cordnmdpstatus"/>==<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@AC)"/> || <s:property value="cdrCbuPojo.fkCordCbuStatus"/>==<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH)"/> || <s:property value="cdrCbuPojo.fkCordCbuStatus"/>==<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)"/>'){
							if('<s:property value="cdrCbuPojo.site.isCDRUser()"/>'=='true'){
								loadMoredivs('getCurrReqDetailsFromViewClin?orderId='+$j("#orid").val()+'&orderType='+$j("#ortype").val()+'&pkcordId=<s:property value="cdrCbuPojo.cordID" />','currentRequestProgressMainDiv','cbuCordInformationForm');
								currReqflag=1;
								$j("#userIdval").width("100px");
								getSearchableSelect('userIdval','id');
								setpfprogressbar();
								getprogressbarcolor();
							}
						}		
					},2000);
				}
				
				setTimeout(function(){
					//alert("historyArr::length"+historyArr.length)
					$j.each(historyArr,function(i,val){
						var Index=val.charAt(val.length-1);
						$j("#icondiv"+Index).triggerHandler('click');
					});
				},0);
				
			}
		}else{
			showSuccessdiv("update","status");
		}
	}
}
function getLicUpdateFlag(){
	
	$j.ajax({
        type: "POST",
        url : 'getCRIFlag?pkcordId='+'<s:property value="cdrCbuPojo.cordID"/>',
        async:false,
        dataType: 'json',
        success: function (result){
							var flag=result.completedFlag;
							//alert("flag"+flag)
							if(flag==1){
								
								$j('#CRIDiv').hide();
								$j('#CRICompleted').show();
								$j('#CRINotCompleted').hide();
							} 
							if(flag==0){
			                    $j('#CRIDiv').hide();
			                    $j('#CRINotCompleted').show();
			                    if($j('#CRICompleted').is(':visible')){
			                        $j('#CRICompleted').hide();
			                    }
			                }
        }
	});
	
}

function revieModal(){
	modalFormSubmitRefreshDiv('license','submitReviewLicenseStatus?reviewModal=reviewModal&pkcordId='+$j('#cordId').val(),'main');
}
function licenCRISubmit(){
	licChangesDone = divChanges('licChanges',$j('#licenseDiv').html());
	//alert(licChangesDone);
	var noMandatory=true;
	if($j('#cbuLicStatus').val()==""){
		noMandatory=false;
	}else if($j('#cbuLicStatus').val()=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON)"/>'){
		if('<s:property value="%{isTasksRequired(cdrCbuPojo.cordID)}"/>'=='false'){
		if($j('#fkCordCbuUnlicenseReason').val() == null){
			noMandatory=false;
			}
		}
	}
	var url = "submitReviewLicenseStatus?reviewModal=reviewModal&pkcordId="+$j('#cordId').val()+"&noMandatory="+noMandatory+"&esignFlag=reviewCRI"+'&criChangeFlag='+true;
	if($j("#license").valid()){
		if(licChangesDone){
			modalFormPageRequest('license',url,'finalreviewmodel',$j("#cordId").val(),$j("#orderId").val(),$j("#orderType").val(),'','');
		}else{
			showSuccessdiv("update","status");
		}
	}
}

function limitText(txtField, maxLength) {
	if (txtField.value.length > maxLength) {
		txtField.value = txtField.value.substring(0, maxLength)
	} 
}

function modalFormPageRequest(formid,url,divname,cordid,orderid,ordertype,updateDivId,statusDivId){
	showprogressMgs();
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        data : $j("#"+formid).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	if(divname!=""){ 
	            	 $j("#"+divname).html(result);
	            }
            	if(updateDivId!=undefined && updateDivId!=""){
            		$j("#"+updateDivId).css('display','block');			            	
            	}else{
            		$j("#update").css('display','block');			            	
            	}
            	if(statusDivId!=undefined && statusDivId!=""){
            		$j("#"+statusDivId).css('display','none');			            	
            	}else{
            		$j("#status").css('display','none');			            	
            	}
            	/*if(orderid!=null && orderid!="" && typeof(orderid)!="undefined" && ordertype!=null && ordertype!="" && typeof(ordertype)!="undefined"){
                	var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID='+cordid+'&orderId='+orderid+'&orderType='+ordertype+'&pkcordId='+cordid+'&currentReqFlag=true';
            		loadMoredivs(urlb,'viewcbutoppanel,quicklinksDiv,taskCompleteFlagsDiv,ctReqClinInfoContentDiv,chistory,cReqhistory,finalreviewcontentDiv,currentRequestProgressMainDiv','cbuProcedureMain','update','status');//url,divname,formId,updateDiv,statusDiv
            		setpfprogressbar();
			 		getprogressbarcolor();
			 		historyTblOnLoad();
                }
            	else{
            	var urla = "loadClinicalData?cdrCbuPojo.cordID="+cordid;
            	loadPageByGetRequset(urla,'cordSelectedData');
            	}*/		            	
            }
        },
        error: function (request, status, error) {
        	closeprogressMsg();
        	alert("Error " + error);
            alert(request.responseText);
        }

	});	
	closeprogressMsg();
	}


function setEmptyOrders(){
	var orderId = $j("#orderId").val();
	var orderType = $j("#orderType").val();
	
	if(orderId!=null && orderId==0){
		$j("#orderId").val("");
		orderId="";
	}
	if(typeof(orderId)=='undefined'){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")!=-1){
		$j("#orderId").val("");
		orderId="";
	}
	
	if(typeof(orderType)=='undefined'){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")!=-1){
		$j("#orderType").val("");
		orderType="";
	}
}
$j('#cbuLicStatus').change(function(){
	resetingEligibleProcess(this.value);
});
function resetingEligibleProcess(val){
	if(('<s:property value="cdrCbuPojo.fkCordCbuEligible"/>'=='<s:property value="#request.pkInEligible"/>' || '<s:property value="cdrCbuPojo.fkCordCbuEligible"/>'=='<s:property value="#request.pkInComplete"/>') && val=='<s:property value="#request.licensedPK"/>'){
		$j("#cbuLicStatus").val($j("#unLicensedPk").val());
		if('<s:property value="%{isTasksRequired(cdrCbuPojo.cordID)}"/>'=='false'){
		unliceseReason($j("#cbuLicStatus").val(),'licenseReason','<s:property value="%{isTasksRequired(cdrCbuPojo.cordID)}"/>');
		var reasonttt = new Array();
		var i=0;
		var leng='<s:property value="reasons.length"/>';
		for(i=0;i<leng;i++){
			var fieldId="liCReasons["+i+"]";
				reasonttt[i]=document.getElementById(fieldId).value;
		}
	
		$j("#fkCordCbuUnlicenseReason").val(reasonttt);
		}
		$j( "#dialog-resetLicDiv" ).dialog({
			resizable: false,
			modal: true,
			closeText: '',
			closeOnEscape: false ,
			buttons: {
				"OK": function() {
					$j( this ).dialog( "close" );
					$j('#cbuLicStatus').removeClass('.licChanges');
					}
			}
		});
	}
}

</script>
<s:set name="tempVal" value="cbuFinalReviewPojo.finalReviewConfirmStatus"></s:set>
<div id="dialog-resetLicDiv" style="display: none;">
<table><tr><td><img src='images/warning.png'/></td><td><s:text name="garuda.cbu.license.cntchngtolicweninelig"/></td></tr></table>
</div>
<div id="licenseDiv" class='tabledisplay'>
<s:form id="license">
<s:hidden name="cdrCbuPojo.unLicesePkid" id="unLicensedPk"></s:hidden>
<s:hidden name="cdrCbuPojo.fkCordCbuStatus" id="fkCordCbuStatus"></s:hidden>
<s:hidden name="licenceUpdate" value="True"></s:hidden>
<s:hidden name="cdrCbuPojo.cordID" id="cordId"/>
<s:hidden name="cdrCbuPojo.fkSpecimenId" />
<s:hidden name="cdrCbuPojo.specimen.pkSpecimen" />
<s:if test="esignFlag!='reviewCRIview'">
<s:hidden name="esignFlag" id="esignFlag" />
</s:if>
<s:hidden name="orderId" id="orderId" />
<s:hidden name="orderType" id="orderType"></s:hidden>
<s:hidden name="cbuFinalReviewPojo.licensureConfirmFlag" id="licensureConfirmFlag" />
<s:hidden name="cbuFinalReviewPojo.eligibleConfirmFlag" id="eligibleConfirmFlag" />
<s:hidden name="cbuFinalReviewPojo.finalReviewConfirmStatus" id="finalReviewConfirmStatus" />
<s:hidden name="unlicInternat" id="unlicInternat" />
<s:hidden name="cdrCbuPojo.isSiteDomestic" id="domesticSite"/>
<div>
<s:iterator value="reasons" var="val" status="row">
<s:hidden value="%{reasons[#row.index]}" id="liCReasons[%{#row.index}]"></s:hidden>
</s:iterator>
</div>
<div id="update" style="display: none;" >
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="garuda.message.modal.licence_update" name="message"/>
		  </jsp:include>	   
</div>	
<div id="status">
<table width="100%">
	<s:if test="#request.update==true" ></s:if>
<tr>
	<td>
	<s:if test="esignFlag == 'modal' ">
	<!-- 
	<div style="display: none;">
		<table class="displaycdr" id="licensehistory" >
			<thead>
				<tr>
					<th><s:text name="garuda.common.lable.date" /></th>
					<th><s:text name="garuda.cdrcbuview.label.user_name" /></th>
					<th><s:text name="garuda.license.label.status"/></th>
					<th><s:text name="garuda.cdrcbuview.label.eligible_reason" /></th>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="lstLicensure" var="rowVal" status="row">
				<tr>
					<s:iterator value="rowVal" var="cellValue" status="col" >
						<s:if test="%{#col.index == 0}">
							<s:date name="%{#cellValue}"
									id="statusDate" format="MMM dd, yyyy" />
							<td><s:property value="%{statusDate}"/></td>
						</s:if>
						<s:if test="%{#col.index == 1}">
							<s:set name="cellValue1" value="cellValue" scope="request"/>
							<td>
								<%
									//String cellValue1 = request.getAttribute("cellValue1").toString();
									//UserJB userB = new UserJB();
									//userB.setUserId(EJBUtil.stringToNum(cellValue1));
									//userB.getUserDetails();													
								%>	
								<%//userB.getUserLastName()+" "+userB.getUserFirstName() %>
							</td>
						</s:if>
						<s:if test="%{#col.index == 2}">																							
							<td style="valign : top">
								<s:property value="getCodeListDescById(#cellValue)" />
							</td>
						</s:if>
						<s:if test="%{#col.index == 3}">
							<td><s:iterator value="getMultipleReasons(#cellValue,null,0)" var="colVal" status="val"><s:property value="getCodeListDescById(#colVal)" /><br></s:iterator></td>
						</s:if>
					</s:iterator>
				</tr>
				</s:iterator>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3"></td>
				</tr>
			</tfoot>
		</table>
		</div> -->
	</s:if>
	<s:if test="esignFlag == 'review'">
	<div id="indshowdiv">
	<input type="hidden" id="cbuLStatus" value='<s:property value="cdrCbuPojo.cbuLicStatus"/>' />
	<input type="hidden" id="unlicenStatus" value='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON)"/>' />
	<input type="hidden" id="unLStatusRes" value='<s:property value="reasons"/>' />
			<s:if test="patientInfoLst!=null && patientInfoLst.size()>0">
			<s:iterator value="patientInfoLst" var="rowVal">
			<s:if test="%{(#rowVal[9]!=null && #rowVal[9]!='') || (#rowVal[8]!=null && #rowVal[8]!='')}">
			<fieldset>
			<legend><s:text name="garuda.cbuentry.label.ind" /></legend>
			<table align="center">
				<tr>
					<td>
						<s:text name="garuda.cbuentry.label.ind_number" />:<s:property
							value="%{#rowVal[9]}" />
						
					</td>
				</tr>
				<tr>	
					<td>
						<s:text name="garuda.cbuentry.label.ind_sponsor" />:<s:property
							value="%{#rowVal[8]}" />
					</td>
				</tr>
			</table>
			</fieldset>
			</s:if>
			</s:iterator>
			</s:if>
			</div>
	</s:if>
	</td>	
</tr>
<s:if test="esignFlag == 'review'">
<tr>
        <td colspan="2">
				<div id="cbuStau"><strong><s:text name="garuda.licence.label.confirmLicStatus"/> <s:property value="CdrCbuPojo.registryId" /> <s:text name="garuda.licence.label.as"/></strong></div>
        </td>
    </tr>
</s:if>
<tr>
	<td>
	<div id="cbuStau1">
		<table id="licStatusTable">
		<s:if test='cbuFinalReviewPojo.licensureConfirmFlag == 1 && cbuFinalReviewPojo.eligibleConfirmFlag == 1 && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y" && esignFlag == "modal"'>
	    <tr>
			<td width="35%">
					<s:text name="garuda.common.message.reasonForChange" />:<span style="color: red;">*</span></td>
				<td width="65%">
					<textarea onchange="isChangeDone(this,'<s:property value="%{licChangeReason}" escapeJavaScript="true"/>','licChanges');" name="licChangeReason" cols="73" rows="5" style="width:290px;" id="validate_count" onkeyup="enableFields(this.value); limitText(this,'200');" ><s:property value="%{licChangeReason}"/></textarea>
					<table cellpadding="0" cellspacing="0" align="left">
							 <tr>
							 <td >
							  <label id="errormsg" style="color:#FF0000;font-size:12px;"></label>
							 </td>
							 </tr>
							</table>
				</td>
		</tr>
		</s:if>
	
			<tr>
				<td width="35%">
					<s:text name="garuda.cdrcbuview.label.licensure_status" />:<s:if test="esignFlag != 'review'" ><span style="color: red;">*</span></s:if></td>
				<td width="65%">
				<s:if test="esignFlag == 'review'" >
				<div id="licensreStat"><s:property value="getCodeListDescById(cbuLicStatus)"/></div>
				</s:if>
				<s:else>
					<s:select name="cbuLicStatus" id="cbuLicStatus" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS]"
							listKey="pkCodeId" cssStyle="width:290px;" listValue="description" headerKey="" cssClass="licensureMandatory"
							headerValue="Select"  onchange="isChangeDone(this,'%{cbuLicStatus}','licChanges');unliceseReason(this.value,'licenseReason','%{isTasksRequired(cdrCbuPojo.cordID)}');setValueFromId(this.value,'fkCordCbuUnlicenseReason','unlicInternat','domesticSite');"/>
				</s:else>			
				</td>
			</tr>
		</table>
		</div>
	</td>
</tr>
<s:if test="%{isTasksRequired(cdrCbuPojo.cordID)==false}">
<tr>			
	<td colspan="2">
	<div id="licenseReasonRef">
		<input type="hidden" id="criCompFlg" value='<s:property value="cbuCompleteReqInfoPojo.completeReqinfoFlag"/> ' />
		<input type="hidden" id="criMrqFlg" value='<s:property value="cbuCompleteReqInfoPojo.mrqFlag"/> ' />
		<input type="hidden" id="cbuLicStatusFlg" value='<s:property value="cbuLicStatus"/> ' />
		<input type="hidden" id="reviewLicConfirmFlg" value='<s:property value="cbuFinalReviewPojo.licensureConfirmFlag" />' />
		<div id="licenseReason"  <s:if test="#request.unlicensed!=true">style="display: none;"</s:if>>
			<table width="100%">
			<s:if test="esignFlag == 'review'" >
			<s:if test="#request.licensedPK!=cbuLicStatus">
				<tr>
					<td width="35%">
						<s:text name="garuda.cdrcbuview.label.unlicense_reason" />:<s:if test="esignFlag != 'review'" ><span style="color: red;">*</span></s:if>
					</td>
					<td width="65%">
					<s:iterator value="reasons" var="element">
						<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON]" >
						<s:if test="%{#element==pkCodeId}">
						<strong><s:property value="getCodeListDescById(#element)"/><br/></strong>
						</s:if>
						</s:iterator>
			        </s:iterator>
					</td>
				</tr>
				</s:if>
				</s:if>
				<s:else>
					  <tr>
						<td width="35%">
							<s:text name="garuda.cdrcbuview.label.unlicense_reason" />:<span style="color: red;">*</span>
							<input type="hidden" id="licDbReasons" value='<s:if test="reasons!=null"><s:property value="%{reasons[0]}"/><s:iterator status="row" begin="1" end="%{reasons.length-1}">,<s:property value="%{reasons[#row.index+1]}"/></s:iterator></s:if>' />
						</td>
						<s:if test="esignFlag == 'reviewCRIview'" >
						<td width="65%">
						<s:select name="reasons" id="fkCordCbuUnlicenseReason" cssClass="licensureMandatory" onchange="getDblicReasons(this,'licChanges');"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON]"
									listKey="pkCodeId" listValue="description" multiple="true" value="reasons" cssStyle="height:120px;width:290px;"/>
						</td>
						</s:if>
						<s:else>
						<td width="65%">
						<s:select name="reasons" id="fkCordCbuUnlicenseReason" onchange="getDblicReasons(this,'licChanges');"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON]"
									listKey="pkCodeId" listValue="description" multiple="true" value="reasons" cssStyle="height:120px;width:290px;"/>
						</td>
						</s:else>
					</tr>
				</s:else>	
			</table>
		</div></div>
   </td>
</tr> 
<tr>
<td colspan="2" align="center"><span class="error"><label id="licensureerror"></label> </span></td>
</tr></s:if>
<tr>
				<td valign="middle" align="center" colspan="2">
				<input type="hidden" id="fkCordCbuStatus" value='<s:property value="cdrCbuPojo.fkCordCbuStatus"/>' />
				
				<s:if test="esignFlag == 'review'">
					<s:if test="hasEditPermission(#request.conLicStus)==true">
					<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
					</s:if>
					<s:else>
					<button type="button" id="editFinalLicensure" onclick="showModal('Update Licensure Status'+' <s:text name="garuda.message.modal.headerregid"/>: <s:property value="cdrCbuPojo.registryId" />, '+'<s:text name="garuda.cbuentry.label.idbag"/>: '+'<s:property value="cdrCbuPojo.numberOnCbuBag"/>','updateReviewLicenseStatus?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&esignFlag=modal&resetFun=reset&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR_EDIT_LICENSURE" />','400','850');">
								<s:text name="garuda.common.lable.edit"/></button>
					
					</s:else>
					</s:if>
					</s:if>
				</td>
				</tr>
</table>
<table bgcolor="#cccccc">
	<s:if test="esignFlag == 'modal' ">
		<tr bgcolor="#cccccc">
			<td width="70%" valign="baseline">
				<jsp:include page="../cb_esignature.jsp" flush="true">
						<jsp:param value="licModelsubmit" name="submitId"/>
						<jsp:param value="licModelinvalid" name="invalid" />
						<jsp:param value="licModelminimum" name="minimum" />
						<jsp:param value="licModelpass" name="pass" />
				</jsp:include></td>  
			<td width="15%" valign="baseline" align="right">
				<input type="button" id="licModelsubmit" disabled="disabled" onclick="beforelicensuresubmit('license');" value="<s:text name="garuda.unitreport.label.button.submit"/>" />
			</td>
			<td valign="baseline" width="15%">   
			   <input type="button" onclick="closeModal();"  value="<s:text name="garuda.unitreport.label.button.cancel"/>" />
			</td> 		 
		</tr>
	</s:if>
	<s:if test="esignFlag == 'review'" >
	   <s:if test="hasEditPermission(#request.conLicStus)==true">
	    <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
  				  </s:if>
   				 <s:else>	
	    <tr bgcolor="#cccccc">
			<td>
				<jsp:include page="../cb_esignature.jsp" flush="true">
						<jsp:param value="licReviewsubmit" name="submitId"/>
						<jsp:param value="licReviewinvalid" name="invalid" />
						<jsp:param value="licReviewminimum" name="minimum" />
						<jsp:param value="licReviewpass" name="pass" />
				</jsp:include>
			</td>  
			<td align="right"">
			 <input type="button" disabled="disabled" id="licReviewsubmit" onclick="licenSubmit();" value="<s:text name="garuda.common.lable.confirm"/>" /> 
			</td>
			<td>			   
			   <input type="button"  onclick="hideDivliceli();" value="<s:text name="garuda.cbbdefaults.lable.cancel"/>" />
			</td>
		</tr>
		</s:else>
		</s:if>
	</s:if>
		<s:if test="esignFlag == 'reviewCRIview'" >
	    <tr bgcolor="#cccccc">
			<td>
				<jsp:include page="../cb_esignature.jsp" flush="true">
						<jsp:param value="licReviewsubmit" name="submitId"/>
						<jsp:param value="licReviewinvalid" name="invalid" />
						<jsp:param value="licReviewminimum" name="minimum" />
						<jsp:param value="licReviewpass" name="pass" />
				</jsp:include>
			</td>  
			<td align="right">
			 <input type="button" disabled="disabled" id="licReviewsubmit" onclick="licenCRISubmit();" value="<s:text name="garuda.common.lable.confirm"/>" /> 
			</td>
			<td>			   
			   <input type="button"  onclick="closeModal();" value="<s:text name="garuda.cbbdefaults.lable.cancel"/>" />
			</td>
			
		</tr>
	</s:if>
</table>
</div>
</s:form>
</div>
<script>
function getDblicReasons(thisVal,classname){
	isChangeDone(thisVal,$j('#licDbReasons').val(),classname);
}
</script>