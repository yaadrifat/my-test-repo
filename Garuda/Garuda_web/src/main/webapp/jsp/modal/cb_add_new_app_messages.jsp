<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<script>


$j(function() {
/*	$j( "#datepicker10" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		changeYear: true});
	$j( "#datepicker11" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		changeYear: true});
	*/
	getDatePic();
});
jQuery(function() {
	  jQuery('#datepicker11, #datepicker10').datepicker('option', {
	    beforeShow: customRange
	  }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	});

	function customRange(input) {
		if (input.id == 'datepicker11') {
			if($j("#datepicker10").val()!="" && $j("#datepicker10").val()!=null){
			    var minTestDate=new Date($j("#datepicker10").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
				  $j('#datepicker11').datepicker( "option", "minDate", new Date(y1,m1,d1));
			}else{
				$j('#datepicker11').datepicker( "option", "minDate", "");
			}
		  } else if (input.id == 'datepicker10') {
			  if($j("#datepicker11").val()!="" && $j("#datepicker11").val()!=null){
				  	var minTestDate=new Date($j("#datepicker11").val()); 	 
					var d1 = minTestDate.getDate();
					var m1 = minTestDate.getMonth();
					var y1 = minTestDate.getFullYear();
				  	$j('#datepicker10').datepicker( "option", "maxDate", new Date(y1,m1,d1));
			  }else{
				  $j('#datepicker10').datepicker( "option", "maxDate", "");
			  }
		  }
	}
function showVisibilityGrpDiv(val){
	if(val=="" || val!='<s:property value="pkLoginPage"/>'){
		$j('#visibilityLogin').hide()
		}
	else{
		$j('#visibilityLogin').show()
		}
}
function showVisibilityDatesTr(val){
	if(val=="" || val=='<s:property value="pkActiveAllways"/>'){
		$j('#datesTr').hide();
		}
	else{
		$j('#datesTr').show();
		}
}
$j(document).ready(function(){
	showVisibilityGrpDiv('<s:property value="appMsgDispAt"/>');
	showVisibilityDatesTr('<s:property value="appMsgStatus"/>');
});
	$j(function(){
		$j("#applMessagesForm").validate({
			rules:{			
					"applicationMsgDesc":{required:true},
					"appMsgDispAt":{required:true},
					"appMsgStatus":{required:true},
					"appMsgStartDtStr":{required:{depends: function(element) {
															 return ($j("input[name='appMsgStatus']:checked").val()!='<s:property value="pkActiveAllways"/>');
															 }
												  			},dpDate:true
														},
					"appMsgEndDtStr":{required:{depends: function(element) {
						 									return ($j("input[name='appMsgStatus']:checked").val()!='<s:property value="pkActiveAllways"/>');
															}
											   			   },dpDate:true
									    				},
					"appMsgfkSite":{required:{depends: function(element) {
														return ($j("input[name='appMsgDispAt']:checked").val()=='<s:property value="pkLoginPage"/>');
														}
	   												   }
													},
					"appMsgfkGrps":{required:{depends: function(element) {
														return ($j("input[name='appMsgDispAt']:checked").val()=='<s:property value="pkLoginPage"/>');
														}
													   }
													}
				},
				messages:{

					"applicationMsgDesc":"<s:text name="garuda.common.validation.msgdesc"/>",
					"appMsgStartDtStr":{required:"<s:text name="garuda.common.validation.startdate"/>"},
					"appMsgEndDtStr":{required:"<s:text name="garuda.common.validation.enddate"/>"},
					"appMsgfkSite":"<s:text name="garuda.common.validation.organization"/>",
					"appMsgfkGrps":"<s:text name="garuda.common.validation.group"/>"
				}
			});
		});
	
function appMsgSubmitForAdd(){
	 var dateElements = $j('.hasDatepicker').get();
	  $j.each(dateElements, function(){
			  customRange(this);
	  });
	modalFormSubmitRefreshDiv('applMessagesForm','submitAddAppMsg','main');
}
function appMsgSubmitForEdit(){
	var dateElements = $j('.hasDatepicker').get();
	  $j.each(dateElements, function(){
			  customRange(this);
	  });
	modalFormSubmitRefreshDiv('applMessagesForm','submitUpdateAppMsg?appMessagesPojo.pkAppMsg='+$j('#pkAppMsg').val(),'main');
}
function showEsign(){
	$j('#hideConfirmBtn').hide();
	$j('#showEsign').show();
}
function appMsgSubmitForDelete(){
	modalFormSubmitRefreshDiv('applMessagesForm','submitdeleteAppMessages?appMessagesPojo.pkAppMsg='+$j('#pkAppMsg').val(),'main');
}
/*
function startDateandEndDate(selectedVal){
	if(selectedVal=="31268"){
		$j('#datesTr').hide()
		}
	else
		$j('#datesTr').show()
}
*/
$j(document).ready(function(){
	$j('.breakradio input[type=radio]+label').each(function(index,el) {
		$j(el).append('<br>');
	});
});

function getGroupBySiteId(val,divname){
	var url = "showGrpsBySiteId?appMsgfkSite="+val;
	refreshDiv11(url,divname,'applMessagesForm');
}
function refreshDiv11(url,divname,formId){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).replaceWith($j('#'+divname, $j(result)));
            	//constructTable();
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
}
</script>
<div id="appMsgDiv" class='tabledisplay'>
<s:form id="applMessagesForm" name="applMessagesForm">
<s:hidden name="addNewMsg" value="True"></s:hidden>
<s:hidden name="esignFlag" id="esignFlag" />
<s:hidden name="pkAppMsgs" id="pkAppMsg" value="%{pkAppMsgs}"></s:hidden>
<div id="update" style="display: none;" >
<s:if test="esignFlag=='add'">
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="Application Message Inserted Successfully" name="message"/>
		  </jsp:include>
</s:if>		  
		  	
<s:if test="esignFlag=='edit'">
		   <jsp:include page="../cb_update.jsp">
		    <jsp:param value="Application Message Updated Successfully" name="message"/>
		    </jsp:include>
</s:if> 

<s:if test="esignFlag=='delete'">
		   <jsp:include page="../cb_update.jsp">
		    <jsp:param value="Application Message Deleted Successfully" name="message"/>
		   </jsp:include> 
</s:if> 
</div>
<div id="status">
<s:if test="esignFlag=='delete'">
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
<td colspan="4" align="center"><s:text name="garuda.applMessge.label.confirmationMsgTodelete"></s:text></td>
</tr>
<tr id="hideConfirmBtn">
<td colspan="4" align="center"><input type="button" value="<s:text name="garuda.opentask.label.yes"/>" onclick="showEsign();"/><input type="button" value="<s:text name="garuda.clinicalnote.label.no"/>" onclick="closeModal();"/></td>
</tr>
<tr bgcolor="#cccccc" id="showEsign" style="display: none;">
	<td colspan="2" align="center">
		<jsp:include page="../cb_esignature.jsp" flush="true">
			<jsp:param value="appMsgDeletesubmit" name="submitId"/>
			<jsp:param value="appMsgDeleteinvalid" name="invalid" />
			<jsp:param value="appMsgDeleteminimum" name="minimum" />
			<jsp:param value="appMsgDeletepass" name="pass" />
		</jsp:include>
	</td><td colspan="2" align="center">	
	 <input type="button" disabled="disabled" id="appMsgDeletesubmit" onclick="appMsgSubmitForDelete();" value="<s:text name="garuda.common.lable.delete"/>" /><input type="button"  onclick="closeModal()" value="<s:text name="garuda.common.lable.cancel"/>" /> 
	</td>
</tr>
</table>
</s:if>
<s:else>
<table cellspacing="0" cellpadding="0" width="100%">
<tr>
	<td><s:text name="garuda.applMessge.label.message"></s:text>:<span style="color: red;">*</span></td>
	<td colspan="3"><s:textarea name="applicationMsgDesc" maxlength="200"  style="width: 437px; height: 40px;"/></td>
</tr>

<tr class="breakradio">
	<td><s:text name="garuda.applMessge.label.messageDispAt"></s:text>:<span style="color: red;">*</span></td>
	<td><s:radio name="appMsgDispAt" id="appMsgDispAt" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PAGE_TYPE]" listKey="pkCodeId" listValue="description" onclick="showVisibilityGrpDiv(this.value);"/>
		<label class="error" for="appMsgDispAt" style="display: none;"><s:text name="garuda.applMessge.label.message.selectmsgDispAt"/></label>
	</td>

	<td><s:text name="garuda.applMessge.label.messageStatus"></s:text>:<span style="color: red;">*</span></td>
	<td><s:radio name="appMsgStatus" id="appMsgStatus" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@APPMSG_STATUS]" listKey="pkCodeId" listValue="description" onclick="showVisibilityDatesTr(this.value);" />
		<label class="error" for="appMsgStatus" style="display: none;"><s:text name="garuda.applMessge.label.message.selectmsgStatus"/></label>
	</td>
</tr>
<tr>			
	<td colspan="4">
		<div id="datesTr" style="display: none;">
		 <fieldset>	
		 <legend><s:text name="garuda.common.label.visibilityDate"></s:text></legend>
		 <table width="100%">
			<tr>
				<td width="20%" align="left"><s:text
					name="garuda.common.label.startDateTime" />:<span style="color: red;">*</span></td>
				<td width="30%" align="left"><s:date
					name="appMsgStartDt" id="datepicker10"
					format="MMM dd, yyyy" /> <s:textfield readonly="true" onkeydown="cancelBack();" 
					name="appMsgStartDtStr" class="datepic" id="datepicker10"
					value="%{datepicker10}" cssClass="datePicWOMinDate"/>
				<div id="validDateDiv1"><span id="invalidDate1" class="error" ></span>
				</div>
				</td>
				<td width="20%" align="left"><s:text
					name="garuda.common.label.endDateTime"></s:text>:<span style="color: red;">*</span></td>
				<td width="30%" align="left"><s:date
					name="appMsgEndDt" id="datepicker11"
					format="MMM dd, yyyy" /> <s:textfield readonly="true" onkeydown="cancelBack();" 
					name="appMsgEndDtStr" class="datepic" id="datepicker11" value="%{datepicker11}" cssClass="datePicWOMinDate"/>
				<div id="validDateDiv"><span id="invalidDate" class="error" ></span></div>
				</td>
			</tr>
			</table>
		  </fieldset>
	</div>
	</td>
</tr>		  

<tr>			
	<td colspan="4">
		<div id="visibilityLogin" style="display: none;">
		 <fieldset>	
		 <legend><s:text name="garuda.applMessge.label.message.visibility"></s:text></legend>
			<table width="100%">
				<tr>
					<td>
						<s:text name="garuda.appmessages.label.organization" />:<span class="error">*</span>
					</td>
					<td>
						<s:select name="appMsgfkSite" id="fkSite" list="#request.CbbList" listKey="siteId" listValue="siteName" headerKey=""  headerValue="Select" onchange="getGroupBySiteId(this.value,'groupDiv');" />
					</td>
					<td>
						<s:text name="garuda.appmessages.label.userGroup" />:<span class="error">*</span>
					</td>
					<td>
					<div id="groupDiv">
						<s:select name="appMsgfkGrps" id="fkGrps" list="#request.grpList" listKey="pkGrp" listValue="grpName" headerKey=""  headerValue="Select" />
					</div>	
					</td>
				</tr>
			</table>
		  </fieldset>	
		</div>
   </td>
</tr>
<s:if test="esignFlag=='edit'">
<tr>
	<td><s:text name="garuda.applMessge.label.message.lastModBy"></s:text>:</td>
	<td>
	<s:if test="appMessagesPojo.lastModifiedBy==null">
	<s:set name="userName" value="%{appMessagesPojo.createdBy}" scope="request"/>
	</s:if>
	<s:else>
	<s:set name="userName" value="%{appMessagesPojo.lastModifiedBy}" scope="request"/>
	</s:else>
					 			<%
									String finaldetails = request.getAttribute("userName").toString();
									UserJB user = new UserJB();
									user.setUserId(EJBUtil.stringToNum(finaldetails));
									user.getUserDetails();													
								%>	
							 <%=user.getUserLastName()+" "+user.getUserFirstName() %>
	</td>						
	<td><s:text name="garuda.applMessge.label.message.lastModOn"></s:text>:</td>
	<td><s:if test="appMessagesPojo.lastModifiedOn==null">
	<s:date name="%{appMessagesPojo.createdOn}"	id="modifyDate" format="MMM dd, yyyy" />
	</s:if>
	<s:else>
	<s:date name="%{appMessagesPojo.lastModifiedOn}" id="modifyDate" format="MMM dd, yyyy" />
	</s:else>
	<s:property value="%{modifyDate}"/>
</tr>
</s:if>
<s:if test="esignFlag=='add'">
<tr bgcolor="#cccccc">
	<td colspan="2" align="center">
		<jsp:include page="../cb_esignature.jsp" flush="true">
		<jsp:param value="AppMsgaddsubmit" name="submitId"/>
		</jsp:include>
	</td><td colspan="2" align="center">	
	 <input type="button" disabled="disabled" id="AppMsgaddsubmit" onclick="appMsgSubmitForAdd();" value="<s:text name="garuda.cbbprocedures.labelsave"/>" /><input type="button"  onclick="closeModal()" value="<s:text name="garuda.cbbprocedures.label.cancel"/>" /> 
	</td>
</tr>
</s:if>	

<s:if test="esignFlag=='edit'">
<tr bgcolor="#cccccc">
	<td colspan="2" align="center">
		<jsp:include page="../cb_esignature.jsp" flush="true">
			<jsp:param value="appMsgeditsubmit" name="submitId"/>
			<jsp:param value="appMsgeditinvalid" name="invalid" />
			<jsp:param value="appMsgeditminimum" name="minimum" />
			<jsp:param value="appMsgeditpass" name="pass" />
		</jsp:include>
	</td><td colspan="2" align="center">	
	 <input type="button" disabled="disabled" id="appMsgeditsubmit" onclick="appMsgSubmitForEdit();" value="<s:text name="garuda.unitreport.label.button.submit"/>" /><input type="button"  onclick="closeModal()" value="<s:text name="garuda.unitreport.label.button.cancel"/>" /> 
	</td>
</tr>
</s:if>	
</table>
</s:else>
</div>
</s:form>
</div>