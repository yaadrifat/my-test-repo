<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<script>
$j(document).ready(function(){
	getDatePic();
	$j("#SYP_SUPP").show();
	$j('#idmesign').hide();
	checkNotDoneother();
	checkMandatory();
	showsubquestions();
	checkdependent();
	//alert('33--<s:property value="esignFlag"/>');
	//if('<s:property value="esignFlag"/>'=="review"){
		//cbuIDM();
	//}
});

function commenttoggle(id,val){
	var pos=$j("#"+id).position()
	var left=pos.left;
	var top=pos.top+$j("#"+id).height();
	$j('#content_'+val).css({left:left,top:top});
	$j('#content_'+val).toggle();
}
function hidecomment(val){
	$j('#content_'+val).toggle();
}

function editidmother(id,obj){
	$j('#'+id).removeAttr('disabled');
	$j(obj).hide();
	$j(obj).next().show();
	
}
function showsubquestions(){
	var i;
	var shname=['','','','HB','ANHB','ANHCV','ANHIV','ANHIV1','ANHTLV','ANCMV','NATH','HIVNAT','HIVP','HCNAT','HBNAT','SYPH','WNNAT','CHA','SYP_SUPP','FTA_ABS'];
	for(i=3;i<20;i++){
		var SubQ1=shname[i]+'_SubQ1';
		var SubQ2=shname[i]+'_SubQ2';
		var subques1=shname[i]+'_sq1res1';
		var subques2=shname[i]+'_sq2res1';
		var testResponse=shname[i]+'_testres1';
		var testDate=shname[i]+'_testDate1';
		var testDateR=shname[i]+'_Testdate';
		if($j('#'+testResponse).val()==$j('#pkTestNotDone').val() || $j('#'+testResponse).val()==""){
			$j('#'+testDateR).hide();
			$j('#'+SubQ1).hide();
			$j('#'+SubQ2).hide();
			$j('#'+testDate).val('');
			$j('#'+subques1).val('');
			$j('#'+subques2).val('');
		}else{
			$j('#'+testDateR).show();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				if($j('#'+subques1).val()!=""){
				}else{
					$j('#'+subques1).val('');
				}
				$j('#'+SubQ1).show();
			}else{
				if($j('#subques1').val()==$j('#idmsubquesnopk').val()){
					$j('#'+subques1).val('0');
				}else{
					$j('#'+subques1).val('1');
				}
				$j('#'+SubQ1).hide();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				if($j('#'+subques2).val()!=""){
				}else{
					$j('#'+subques2).val('');
				}
				$j('#'+SubQ2).show();
			}else{
				if($j('#subques2').val()==$j('#idmsubquesnopk').val()){
					$j('#'+subques2).val('0');
				}else{
					$j('#'+subques2).val('1');
				}
				$j('#'+SubQ2).hide();
			}
		}
		if(testResponse=='FTA_ABS_testres1'){
			if($j('#'+testResponse).val()=='0' || $j('#'+testResponse).val()==''){
				$j('#'+testDateR).hide();
				$j('#'+testDate).val("");
			}else{
				$j('#'+testDateR).show();
			}
		}
	}
}
function checkNotDoneother(){
	$j('.checkNotDoneOther').each(function(){
		if($j(this).val()==$j('#pkTestNotDone').val() || $j(this).val()==""){
			$j(this).parent().parent().next('tr').hide();
		}else{
			$j(this).parent().parent().next('tr').show();
		}
	});
}
function checkdependent(){
	var i;
	var shname=['','','','HB','ANHB','ANHCV','ANHIV','ANHIV1','ANHTLV','ANCMV','NATH','HIVNAT','HIVP','HCNAT','HBNAT','SYPH','WNNAT','CHA','SYP_SUPP','FTA_ABS'];
	for(i=3;i<20;i++){
		var id=shname[i]+'_testres1';
		var testResponse=shname[i]+'_testres1';
		var testDate=shname[i]+'_testDate1';
		var subques1=shname[i]+'_sq1res1';
		var subques2=shname[i]+'_sq2res1';
	if(id=='ANHIV_testres1'){
		if($j('#ANHIV_testres1').val()!=""){
		if($j('#ANHIV_testres1').val()==$j("#pkTestNotDone").val()){
			$j('#ANHIV1_test').show();
			if($j('#ANHIV1_testres1').val()!=$j("#pkTestNotDone").val() && $j('#ANHIV1_testres1').val()!=""){
				$j('#ANHIV1_Testdate').show();
			}else{
				$j('#ANHIV1_Testdate').hide();
			}
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#ANHIV1_SubQ1').show();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#ANHIV1_SubQ2').show();
			}
		}else{
			$j('#ANHIV1_testres1').val("");
			$j('#ANHIV1_testres1').removeClass("idmMandatory");
			$j('#ANHIV1_testDate1').removeClass("idmMandatory");
			$j('#ANHIV1_test').hide();
			$j('#ANHIV1_Testdate').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#ANHIV1_SubQ1').hide();
				$j('#ANHIV1_sq1res1').removeClass("idmMandatory");
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#ANHIV1_SubQ2').hide();
				$j('#ANHIV1_sq2res1').removeClass("idmMandatory");
			}
			}
		}else{
			$j('#ANHIV1_test').hide();
			$j('#ANHIV1_Testdate').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#ANHIV1_SubQ1').hide();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#ANHIV1_SubQ2').hide();
			}
		}
	}
	if(id=='NATH_testres1'){
		if($j('#NATH_testres1').val()!=""){
		if($j('#NATH_testres1').val()==$j("#pkTestNotDone").val()){
			/*$j('#ANHIV1_test').show();
			if($j('#ANHIV1_testres1').val()!=$j("#pkTestNotDone").val() && $j('#ANHIV1_testres1').val()!=""){
				$j('#ANHIV1_Testdate').show();
			}else{
				$j('#ANHIV1_Testdate').hide();
			}*/
			$j('#HIVNAT_test').show();
			if($j('#HIVNAT_testres1').val()!=$j("#pkTestNotDone").val() && $j('#HIVNAT_testres1').val()!=""){
				$j('#HIVNAT_Testdate').show();
			}else{
				$j('#HIVNAT_Testdate').hide();
			}
			$j('#HCNAT_test').show();
			if($j('#HCNAT_testres1').val()!=$j("#pkTestNotDone").val() && $j('#HCNAT_testres1').val()!=""){
				$j('#HCNAT_Testdate').show();
			}else{
				$j('#HCNAT_Testdate').hide();
			}
			$j('#HBNAT_test').show();
			if($j('#HBNAT_testres1').val()!=$j("#pkTestNotDone").val() && $j('#HBNAT_testres1').val()!=""){
				$j('#HBNAT_Testdate').show();
			}else{
				$j('#HBNAT_Testdate').hide();
			}
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				//$j('#ANHIV1_SubQ1').show();
				$j('#HIVNAT_SubQ1').show();
				$j('#HCNAT_SubQ1').show();	
				$j('#HBNAT_SubQ1').show();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				//$j('#ANHIV1_SubQ2').show();
				$j('#HIVNAT_SubQ2').show();
				$j('#HCNAT_SubQ2').show();	
				$j('#HBNAT_SubQ2').show();
			}
		}else if($j('#NATH_testres1').val()==$j('#idmposiPk').val() || $j('#NATH_testres1').val()==$j('#idmnegiPk').val()){

			$j('#HIVNAT_testres1').val("");
			$j('#HIVNAT_testres1').removeClass("idmMandatory");
			$j('#HIVNAT_testDate1').removeClass("idmMandatory");
			$j('#HCNAT_testres1').val("");
			$j('#HCNAT_testres1').removeClass("idmMandatory");
			$j('#HCNAT_testDate1').removeClass("idmMandatory");
			$j('#HBNAT_testres1').val("");
			$j('#HBNAT_testres1').removeClass("idmMandatory");
			$j('#HBNAT_testDate1').removeClass("idmMandatory");
			$j('#HIVNAT_test').hide();
			$j('#HIVNAT_Testdate').hide();
			$j('#HCNAT_test').hide();
			$j('#HBNAT_Testdate').hide();
			$j('#HBNAT_test').hide();
			$j('#HBNAT_Testdate').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				//$j('#ANHIV1_SubQ1').hide();
				//$j('#ANHIV1_sq1res1').removeClass("idmMandatory");
				$j('#HIVNAT_SubQ1').hide();
				$j('#HIVNAT_sq1res1').removeClass("idmMandatory");
				$j('#HCNAT_SubQ1').hide();
				$j('#HCNAT_sq1res1').removeClass("idmMandatory");
				$j('#HBNAT_SubQ1').hide();
				$j('#HBNAT_sq1res1').removeClass("idmMandatory");	
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				//$j('#ANHIV1_SubQ2').hide();
				//$j('#ANHIV1_sq2res1').removeClass("idmMandatory");
				$j('#HIVNAT_SubQ2').hide();
				$j('#HIVNAT_sq2res1').removeClass("idmMandatory");
				$j('#HCNAT_SubQ2').hide();
				$j('#HCNAT_sq2res1').removeClass("idmMandatory");
				$j('#HBNAT_SubQ2').hide();	
				$j('#HBNAT_sq2res1').removeClass("idmMandatory");
			}			
		}
		}else{
			//$j('#ANHIV1_test').hide();
			//$j('#ANHIV1_Testdate').hide();
			$j('#HIVNAT_test').hide();
			$j('#HIVNAT_Testdate').hide();
			$j('#HCNAT_test').hide();
			$j('#HBNAT_Testdate').hide();
			$j('#HBNAT_test').hide();
			$j('#HBNAT_Testdate').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				//$j('#ANHIV1_SubQ1').hide();
				$j('#HIVNAT_SubQ1').hide();
				$j('#HCNAT_SubQ1').hide();	
				$j('#HBNAT_SubQ1').hide();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
			//	$j('#ANHIV1_SubQ2').hide();
				$j('#HIVNAT_SubQ2').hide();
				$j('#HCNAT_SubQ2').hide();	
				$j('#HBNAT_SubQ2').hide();
			}
		}
	}
	if(id=='NATH_testres1' || id=='HIVNAT_testres1'){
		if($j('#NATH_testres1').val()!="" || $j('#HIVNAT_testres1').val()!=""){
			if($j('#NATH_testres1').val()==$j("#pkTestNotDone").val() && $j('#HIVNAT_testres1').val()==$j("#pkTestNotDone").val()){
				$j('#HIVP_test').show();
				if($j('#HIVP_testres1').val()!=$j("#pkTestNotDone").val() && $j('#HIVP_testres1').val()!=""){
					$j('#HIVP_Testdate').show();
				}else{
					$j('#HIVP_Testdate').hide();
				}
				if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
					$j('#HIVP_SubQ1').show();
				}
				if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
					$j('#HIVP_SubQ2').show();
				}
			}else{
				$j('#HIVP_testres1').val("");
				$j('#HIVP_testres1').removeClass("idmMandatory");
				$j('#HIVP_testDate1').removeClass("idmMandatory");
				$j('#HIVP_test').hide();
				$j('#HIVP_Testdate').hide();
				if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
					$j('#HIVP_SubQ1').hide();
					$j('#HIVP_sq1res1').removeClass("idmMandatory");
				}
				if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
					$j('#HIVP_SubQ2').hide();
					$j('#HIVP_sq2res1').removeClass("idmMandatory");
				}
			}
		}else{
			$j('#HIVP_test').hide();
			$j('#HIVP_Testdate').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#HIVP_SubQ1').hide();
			}
			if($j('#subques2').val()==$j('#idmsubquessomepk').val()){
				$j('#HIVP_SubQ2').hide();
			}
		}
	}
	if(id=='SYPH_testres1'){
		if($j('#SYPH_testres1').val()!=""){
		if($j('#SYPH_testres1').val()==$j("#idmposiPk").val()){
			$j('#SYP_SUPP_test').show();
			if($j('#SYP_SUPP_testres1').val()!=$j("#pkTestNotDone").val() && $j('#SYP_SUPP_testres1').val()!=""){
				$j('#SYP_SUPP_Testdate').show();
			}else{
				$j('#SYP_SUPP_Testdate').hide();
			}
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#SYP_SUPP_SubQ1').show();
			}
		}else{
			$j('#SYP_SUPP_testres1').val("");
			$j('#SYP_SUPP_testres1').removeClass("idmMandatory");
			$j('#SYP_SUPP_testDate1').removeClass("idmMandatory");
			$j('#SYP_SUPP_test').hide();
			$j('#SYP_SUPP_Testdate').hide();
			$j('#FTA_ABS_Testdate').hide();
			if($j('#subques1').val()==$j('#idmsubquessomepk').val()){
				$j('#SYP_SUPP_SubQ1').hide();
				$j('#SYP_SUPP_sq1res1').removeClass("idmMandatory");
			}
		}
		}else{
			$j('#SYP_SUPP_test').hide();
			$j('#SYP_SUPP_Testdate').hide();
			$j('#SYP_SUPP_SubQ1').hide();
		}
	}
	if(id=='SYP_SUPP_testres1'){
		if($j('#SYP_SUPP_testres1').val()!=""){
		if($j('#SYPH_testres1').val()==$j("#idmposiPk").val() && $j('#SYP_SUPP_testres1').val()==$j("#idmnegiPk").val()){
			$j('#FTA_ABS_test').show();
			if($j('#FTA_ABS_testres1').val()!='0' && $j('#FTA_ABS_testres1').val()==""){
				$j('#FTA_ABS_Testdate').show();
			}else{
				$j('#FTA_ABS_Testdate').hide();
			}
		}else{
			$j('#FTA_ABS_testres1').val("");
			$j('#FTA_ABS_testres1').removeClass("idmMandatory");
			$j('#FTA_ABS_testDate1').removeClass("idmMandatory");
			$j('#FTA_ABS_test').hide();
			$j('#FTA_ABS_Testdate').hide();
		}
		}else{
			$j('#FTA_ABS_test').hide();
			$j('#FTA_ABS_Testdate').hide();
		}
	}
}
}
function checkMandatory(){
	var i;
	var shname=['','','','HB','ANHB','ANHCV','ANHIV','ANHIV1','ANHTLV','ANCMV','NATH','HIVNAT','HIVP','HCNAT','HBNAT','SYPH','WNNAT','CHA','SYP_SUPP','FTA_ABS'];
	if($j('#licenseid').val()==$j("#unLicensedPk").val()){
		$j('#fktestsource').addClass('idmMandatory');
		$j('#bloodcdatestr').addClass('idmMandatory');
		$j('#subques1').addClass('idmMandatory');
		$j('#subques2').addClass('idmMandatory');
		$j('.showmantry').show();
		for(i=3;i<20;i++){
			var testResponse=shname[i]+'_testres1';
			var testDate=shname[i]+'_testDate1';
			var subques1=shname[i]+'_sq1res1';
			var subques2=shname[i]+'_sq2res1';
			var manditory=shname[i]+'_manditory';
			var testDateman=shname[i]+'_testDateman';
			var SubQus1man=shname[i]+'_SubQus1man';
			var SubQusman=shname[i]+'_SubQusman';
			$j('#'+manditory).show();
			$j('#'+testDateman).show();
			$j('#'+SubQus1man).show();
			$j('#'+SubQusman).show();
			
			
			if($j('#'+testResponse).val()==""){
				$j('#'+testResponse).addClass('idmMandatory');
				/*$j('#'+testDate).addClass('idmMandatory');
				if($j('#subques1').val()==$j('#idmsubquessomepk').val() || $j('#subques1').val()==""){
					$j('#'+subques1).addClass('idmMandatory');
				}else{
					$j('#'+subques1).removeClass('idmMandatory');
				}
				if($j('#subques2').val()==$j('#idmsubquessomepk').val() || $j('#subques2').val()==""){
					$j('#'+subques2).addClass('idmMandatory');
				}else{
					$j('#'+subques2).removeClass('idmMandatory');
				}*/
			}else{
				$j('#'+testResponse).removeClass('idmMandatory');
				if($j('#'+testResponse).val()!=$j('#pkTestNotDone').val()){
					if($j('#'+testDate).val()==""){
						$j('#'+testDate).addClass('idmMandatory');
					}else{
						$j('#'+testDate).removeClass('idmMandatory');
					}
					if($j('#subques1').val()==$j('#idmsubquessomepk').val() || $j('#subques1').val()==""){
						if($j('#'+subques1).val()==""){
							$j('#'+subques1).addClass('idmMandatory');
						}else{
							$j('#'+subques1).removeClass('idmMandatory');
						}
					}else{
						$j('#'+subques1).removeClass('idmMandatory');
					}
					if($j('#subques2').val()==$j('#idmsubquessomepk').val() || $j('#subques2').val()==""){
						if($j('#'+subques2).val()==""){
							$j('#'+subques2).addClass('idmMandatory');
						}else{
							$j('#'+subques2).removeClass('idmMandatory');
						}
					}else{
						$j('#'+subques2).removeClass('idmMandatory');
					}
				}else{
					$j('#'+testDate).removeClass('idmMandatory');
					$j('#'+subques1).removeClass('idmMandatory');
					$j('#'+subques2).removeClass('idmMandatory');
				}
			}
			if(testResponse=='FTA_ABS_testres1'){
				if($j('#'+testResponse).val()==''){
					$j('#'+testResponse).addClass('idmMandatory');
					$j('#'+testDate).addClass('idmMandatory');
				}else{
					$j('#'+testResponse).removeClass('idmMandatory');
					$j('#'+testDate).removeClass('idmMandatory');
				}
				if($j('#'+testResponse).val()=='1'){
					$j('#'+testDate).addClass('idmMandatory');
				}else if($j('#'+testResponse).val()=='0'){
					$j('#'+testDate).removeClass('idmMandatory');
				}
			}
		}
	}
}
function saveidmother(id,obj,rowId){
	var pkidm=$j('#pk_'+id).val();
	if($j('#'+id).val()!="" && $j('#'+id).val()!=null){
		url="updateidmques";
		var progressMsg="<table  width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
		var processingSaveDialog = document.createElement('div');
			processingSaveDialog.innerHTML=progressMsg;		
			$j(processingSaveDialog).dialog({autoOpen: false,
				resizable: false, width:400, height:90,closeOnEscape: false,
				modal: true}).siblings('.ui-dialog-titlebar').remove();		
			$j(processingSaveDialog).dialog("open");
			$j('#cbuidmform select').each(function(){
				$j(this).removeAttr('disabled');
			});
			$j('#cbuidmform input[type=text]').each(function(){
				$j(this).removeAttr('disabled');
			});
			var noMandatory=true;
			$j('.idmMandatory').each(function(){
				var id=$j(this).parent().parent().attr('id');
				if($j('#'+id+':visible')){
					 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
						 noMandatory=false;
						}
				}		
			});
			url=url+"?noMandatory="+noMandatory;
			divname = "idmtestform";
			if($j('#idmesignFlag').val()=="review"){
				divname="finalreviewmodel";
				}
		$j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        data:$j('#cbuidmform *').serialize(),
	        success: function (result){
	            	$j("#"+divname).html(result);
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	        	$j(processingSaveDialog).dialog("close");
	            alert(request.responseText);
	        }
		});
		$j(processingSaveDialog).dialog("close");
		$j('#progressMsg').remove();
	}
	$j(obj).hide();
	$j(obj).prev().show();
}
function showhelpmsg(id){
	msg=$j('#'+id+'_msg').html();
	overlib(msg,CAPTION,'Help');
	$j('#overDiv').css('z-index','99999');
}
function addtestdate(){
	var fkspecId=$j('#fkSpecimenId').val();
	refreshIdmDiv('addnewidmtest?cdrCbuPojo.fkSpecimenId='+fkspecId,'idmtestform',null)
}

function refreshIdmDiv(url,divname,data){
	var progressMsg="<table  width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	var processingSaveDialog = document.createElement('div');
		processingSaveDialog.innerHTML=progressMsg;		
		$j(processingSaveDialog).dialog({autoOpen: false,
			resizable: false, width:400, height:90,closeOnEscape: false,
			modal: true}).siblings('.ui-dialog-titlebar').remove();		
		$j(processingSaveDialog).dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data:data,
        success: function (result){
        	$j('.ui-datepicker').html("");
            var $response=$j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            	$j(processingSaveDialog).dialog("close");
            }else{
            	$j("#"+divname).html(result);
            	$j(processingSaveDialog).dialog("close");
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
        	$j(processingSaveDialog).dialog("close");
            alert(request.responseText);
        }
	});
	$j('#progressMsg').remove();
}
function showassessment1(pkpatlab,shname,row,col){
	var rc=row+col;
	var entityId=$j('#cordID').val();
	var subEntityId=pkpatlab;
	var subEntitytype=$j('#subentityType').val();
	var divname=shname+"_assess";
	var id="";
	if(shname!='subques1' && shname!='subques2'){
		id=shname+'_testres'+col;
	}else{
		id=shname;
	}
	//alert("id:"+id);
	var responsestr=$j('#'+id+' option:selected').text();
	//alert('responsestr:'+responsestr);
	var data='entityId='+entityId+'&subEntityId='+subEntityId+'&shrtName='+shname+'&subentitytype='+subEntitytype+'&responsestr='+responsestr+'&questionNo=IDM Q#'+row;
	//alert("data : "+data);
	refreshIdmDiv('getassessment',divname,data);
	$j('#'+divname).show();
	$j('#'+shname+"_showassess"+rc).hide();
	$j('#'+shname+"_hideassess"+rc).show();
	$j('#'+shname+"_test td").each(function(index,el) {
		if(col!=index){
			$j('#'+shname+"_assess"+index).hide();
		}
	});
}
function hideassessment(shname,row,col){
	var divname=shname+"_assess";
	var rc=row+col;
	$j('#'+divname).hide();
	$j('#'+shname+"_hideassess"+rc).hide();
	$j('#'+shname+"_showassess"+rc).show();
	$j('#'+shname+"_test td").each(function(index,el) {
		if(col!=index){
			$j('#'+shname+"_assess"+index).show();
		}
	});
}
function editidm(shname,row,col,div1,div2,disab){
	var rc=row+col;
	$j('#'+shname+"_"+disab+col).removeAttr('disabled');
	$j('#'+shname+"_"+div1+rc).hide();
	$j('#'+shname+"_"+div2+rc).show();
}
function saveidm(shname,row,col){
	//alert("check saveidm");
	var id=shname+"_testres"+col;
	var rc=row+col;
	var cordid=$j('#cordID').val();
	var sq1res="";
	var sq2res="";
	var pkpatlab=$j('#'+shname+'_pkpatlabs_'+col).val();
	var fktestid=$j('#'+shname+'_fktestid_'+col).val();
	var fktestgrp=$j('#'+shname+'_fktestgrp_'+col).val();
	var fktestsource=$j('#fttestsource').val();
	var count=$j('#'+shname+"_testcount_"+col).val();
	var comment=$j('#'+shname+"_testComment_"+col).val();
	var testres=$j('#'+shname+"_testres"+col).val();
	var testdate=$j('#'+shname+"_testDate"+col).val();
	var idmpk=$j('#pkidm').val();
	if($j('#'+shname+"_sq1res"+col).html()){
		sq1res=$j('#'+shname+"_sq1res"+col).val();
	}
	if($j('#'+shname+"_sq2res"+col).html()){
		sq2res=$j('#'+shname+"_sq2res"+col).val();
	}
	var noMandatory=true;
	$j('.idmMandatory').each(function(){
		var id=$j(this).parent().parent().attr('id');
		if($j('#'+id+':visible')){
			 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
				 noMandatory=false;
				}
		}
	});
	var url="updateidm?pkpatlab="+pkpatlab+"&fktestid="+fktestid+"&fktestgrp="+fktestgrp+"&fktestsource="+fktestsource+"&fktestresult="+testres+"&testdate="+testdate+"&subq1res="+sq1res+"&subq2res="+sq2res+"&cdrCbuPojo.cordID="+cordid+"&testcount="+count+"&idmpojo.pkidm="+idmpk+"&testcomments="+comment+"&noMandatory="+noMandatory+"&esignFlag="+$j('#idmesignFlag').val();
	var divname="idmtestform";
	if($j('#idmesignFlag').val()=="review"){
		divname="finalreviewmodel";
		}
	if(testres!=""){
		if(testres!=$j('#pkTestNotDone').val() && testres!=0){
			if($j('#'+shname+"_sq1res"+col).html() && $j('#'+shname+"_sq2res"+col).html()){
				if(sq1res!="" && sq2res!="" && testdate!=""){
					refreshIdmDiv(url,divname,null);	
				}else{
					if(testdate==""){
						$j('#'+shname+"_testDate"+col+"_err").show();
					}
					if(sq1res==""){
						$j('#'+shname+"_sq1res"+col+"_err").show();
					}
					if(sq2res==""){
						$j('#'+shname+"_sq2res"+col+"_err").show();
					}
				}
			}else{
				if($j('#'+shname+"_sq1res"+col).html()){
					if(sq1res!="" && testdate!=""){
						refreshIdmDiv(url,divname,null);	
					}else{
						if(testdate==""){
							$j('#'+shname+"_testDate"+col+"_err").show();
						}
						if(sq1res==""){
							$j('#'+shname+"_sq1res"+col+"_err").show();
						}
					}
				}else{
					if(testdate!=""){
						refreshIdmDiv(url,divname,null);	
					}else{
							$j('#'+shname+"_testDate"+col+"_err").show();
					}
				}
			}
		}else{
			if(sq1res=="" && sq2res=="" && testdate==""){
				refreshIdmDiv(url,divname,null);	
			}else{
				if(testdate!="" || sq1res!="" || sq2res!=""){
					$j('#'+shname+"_testres"+col+"_err1").show();
				}
			}
	}
	}else{
		$j('#'+shname+"_testres"+col+"_err").show();	
	}
}
function checknull(id){
	if($j('#'+id).val()!=null && $j('#'+id).val()!=""){
		$j('#'+id+'_err').hide();
		$j('#'+id+'_err1').hide();
	}else{
		$j('#'+id+'_err').show();
	}
}
function checkcommentnull(shname,count,indexval){
	var id=shname+'_testComment_'+count;
	if($j('#'+id).val()!=null && $j('#'+id).val()!=""){
		$j('#'+id+'_err').hide();
		$j('#'+id+'_err1').hide();
		saveidm(shname,indexval,count);
	}else{
		$j('#'+id+'_err').show();
	}
}
function checknotdone(id,shname,col){
	var value=$j('#'+shname+"_testres"+col).val();
	var notdonepk=$j('#pkTestNotDone').val();
	var testdateId=$j('#'+shname+"_testDate"+col).attr('id');
	var sq1Id=$j('#'+shname+"_sq1res"+col).attr('id');
	var sq2Id=$j('#'+shname+"_sq2res"+col).attr('id');

	if(value==notdonepk || value==0){
		$j('#'+shname+"_testDate"+col).val("");
		$j('#'+shname+"_testDate"+col).removeClass("idmMandatory");
		$j('#'+shname+"_sq1res"+col).val("");
		$j('#'+shname+"_sq1res"+col).removeClass("idmMandatory");
		$j('#'+shname+"_sq2res"+col).val("");
		$j('#'+shname+"_sq2res"+col).removeClass("idmMandatory");
		$j('#'+testdateId+'_err').hide();
		$j('#'+sq1Id+'_err').hide();
		$j('#'+sq2Id+'_err').hide();
	}else{
		$j('#'+shname+"_testDate"+col).addClass("idmMandatory");
		$j('#'+shname+"_sq1res"+col).addClass("idmMandatory");
		$j('#'+shname+"_sq2res"+col).addClass("idmMandatory");
	}
}
function setidmvalue(val){
	$j('#fktestsource').val(val);
}
</script>
<style>
#IDMinformation table{
	border: 1px solid #7dcfd5;
}
.IDMinformation table td{
	border-left: 1px solid #7dcfd5;
	border-right: 1px solid #7dcfd5;
}
.childquestions td{	

	padding-left: 25px;
}
.commentbox{
	position: absolute;
	background: -moz-linear-gradient(center top , #FFFFFF, #EEEEEE) repeat scroll 0 0 #7dcfd5;
	z-index:9999;
	border: 1px solid #DBDAD9;
	padding: 2px;
}

</style>
<s:if test="%{idmQuestionList!=null}">

<s:set name="flagtestsrc" value="0"></s:set>
	<s:iterator value="idmQuestionList" var="idmpojo">
		<s:if test="%{fktestsource!=null && #flagtestsrc!='1'}">
			<s:set name="flagtestsrc" value="1"></s:set>
			<s:hidden name="fktestsource" id="fttestsource"></s:hidden>
			<div onclick="toggleDiv('idmForm')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span>
				<s:iterator value="%{#application.codeListMapByIds[fktestsource]}">
					<s:property value="%{description}"/>
				</s:iterator>
			</div>
		</s:if>
	</s:iterator>
</s:if>
<div id="idmForm">
<!-- <table border="0" class="displaycdr">
<tr>
<td>-->
	<div id="IDMinformation" class="IDMinformation">
	<s:hidden name="esignFlag" id="idmesignFlag"/>
	<s:hidden name="cdrCbuPojo.fkSpecimenId" id="fkSpecimenId"/>
	<s:hidden id="idmTestcount" name="idmTestcount"/>
	<s:hidden name="subentitytype" id="subentityType"/>
	<s:hidden name="pkTestNotDone" id="pkTestNotDone" />
	<s:hidden name="idmposiPk" id="idmposiPk" />
	<s:hidden name="idmnegiPk" id="idmnegiPk" />
	<s:hidden name="idmsubquesnopk" id="idmsubquesnopk"/>
	<s:hidden name="idmsubquesyespk" id="idmsubquesyespk"/>
	<s:hidden name="idmsubquessomepk" id="idmsubquessomepk"/>
	<s:hidden name="idmsubquesnopk" id="idmsubquesnopk"/>
	<s:hidden name="idmsubquesyespk" id="idmsubquesyespk"/>
	<s:hidden name="idmsubquessomepk" id="idmsubquessomepk"/>
	<s:hidden name="idmstatus" value="0" id="idmstatus"/>
	<s:hidden name="assess_ncd" id="assess_ncd" value="%{#request.assess_ncd}"/>
	<table border="0" class="displaycdr" id="idmsearchResults" >
	<thead>
	<tr>
	<th scope="col"><s:text	name="Question #" /></th>
	<th scope="col"><s:text	name="Question" /></th>
	<s:iterator var="idmcount" begin="1" end="1">
	<th scope="col">
		<s:text name="garuda.label.dynamicform.response"/><s:property value="%{#idmcount}"/>
	</th>
	</s:iterator>
	</tr>
	<tr bgcolor="#E6E6E5" id="idmtestsource">
	<td>
	</td>
	<td>
		<s:text name="garuda.idm.label.source"></s:text>
		<span class="error showmantry" style="display:none">
			<s:text name="garuda.label.dynamicform.astrik"/>
		</span>
	</td>
	<s:iterator value="idmQuestionList" var="idmpojo" >
		<td>
			<s:hidden name="idmpojo.pkidm" id="pkidm" value="%{pkidm}"></s:hidden>
		<select id="fktestsource" name="idmpojo.fktestsource" onchange="setidmvalue(this.value);" 
				disabled="disabled">
			<option value="">Select</option>
			<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES]">
				<s:if test="%{pkCodeId!=pkpatientID && pkCodeId==fktestsource}">
					<option value='<s:property value='pkCodeId'/>' selected="selected"><s:text name="description"/></option>
				</s:if>
				<s:elseif test="%{pkCodeId!=pkpatientID}">
					<option value='<s:property value='pkCodeId'/>'><s:text name="description"/></option>
				</s:elseif>
			</s:iterator>
		</select>
			<br/>
		<table width="100%" class="hideDivClass">
			<tr><td align="right">
			<a href="#" id="edittestsrc" onclick="editidmother('fktestsource',this)">
				<s:text name="Edit"/>
			</a>
			<a href="#" id="savetestsrc" onclick="saveidmother('fktestsource',this,'idmtestsource')" style="display: none;">
				<s:text name="Save"/>
			</a>
		</td>
		</tr>
		</table>
	</td>
	</s:iterator>
	</tr>
	<tr bgcolor="#E6E6E5" id="bcDate">
	<td>
	</td>
	<td><s:text name="Blood Collection Date"></s:text><span class="error showmantry" style="display:none">
			<s:text name="garuda.label.dynamicform.astrik"/>
		</span></td>
	<s:iterator value="idmQuestionList" var="idmpojo" >
		<td>
			<s:textfield readonly="true" onkeydown="cancelBack();"  name="idmpojo.bloodcdatestr" cssClass="datePicWMaxDate" 
						 value="%{getText('collection.date',{bloodcdate})}" disabled="true" id="bloodcdatestr"/>
			<br/>
		<table width="100%" class="hideDivClass">
			<tr><td align="right">
			<a href="#" id="editbcdate" onclick="editidmother('bloodcdatestr',this)">
				<s:text name="Edit"/>
			</a>
			<a href="#" id="savebcdate" onclick="saveidmother('bloodcdatestr',this,'bcDate')" style="display: none;">
				<s:text name="Save"/>
			</a>
		</td>
		</tr>
		</table>
		</td>
	</s:iterator>
	</tr>
	</thead>
	<tbody>
	<tr id="idmsubques1">
		<td align="center"><s:text name="1"></s:text></td>
		<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IDMTEST_QUES]">
		<s:if test="%{subType=='subques1'}">
		<td>
			<s:hidden name="pkCodeId" id="pkforques1"></s:hidden>
			<img id="cmsapprovedlab" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
			<s:property value="%{description}"/>
			<span class="error showmantry" style="display:none">
			<s:text name="garuda.label.dynamicform.astrik"/>
		</span>
			<div id="subques1_assess">
			</div>
		</td>
		
		<s:iterator value="idmQuestionList" var="idmpojo" >
		<td>
			<s:select  name="idmpojo.cmsapprovedlab" id="subques1" disabled="true" cssClass="idmMandatory" onchange="showsubquestions();"
				list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IDMTEST_SUBQUES]"
				listKey="pkCodeId" listValue="description"  value="%{cmsapprovedlab}" headerKey="" headerValue="Select"/>
			<table width="100%" class="hideDivClass">
			<tr><td>
				<a href="#" id="subques1_showassess11" onclick="javascript: return showassessment1('<s:property value="%{pkCodeId}"/>','subques1','1','1')" >
					<s:text name="Show Assessment"/>
				</a>
				<a href="#" id="subques1_hideassess11" onclick="hideassessment('subques1','1','1')" style="display: none;">
					<s:text name="Hide Assessment"/>
				</a>
			</td>
			<td align="right">
			<a href="#" id="editidmqus1" onclick="editidmother('subques1',this)">
				<s:text name="Edit"/>
			</a>
			<a href="#" id="saveidmqus1" onclick="saveidmother('subques1',this,'idmsubques1')" style="display: none;">
				<s:text name="Save"/>
			</a>
			</td>
			</tr>
			</table>
		</td>
		</s:iterator>
		</s:if>
		</s:iterator>
	</tr>
	<tr id="idmsubques2">
		<td align="center"><s:text name="2"></s:text></td>
		
		<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IDMTEST_QUES]">
		<s:if test="%{subType=='subques2'}">
		<td>
			<s:hidden name="pkCodeId" id="pkforques2"></s:hidden>
			<img id="fdalicensedlab" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
			<s:property value="%{description}"/>
			<span class="error showmantry" style="display:none">
			<s:text name="garuda.label.dynamicform.astrik"/>
		</span>
			<div id="subques2_assess">
			</div>
		</td>
		<s:iterator value="idmQuestionList" var="idmpojo" >
		<td>
			<s:select  name="idmpojo.fdalicensedlab" id="subques2" disabled="true" cssClass="idmMandatory" onchange="showsubquestions();"
				list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IDMTEST_SUBQUES]"
				listKey="pkCodeId" listValue="description"  value="%{fdalicensedlab}" headerKey="" headerValue="Select"/>
			
			<table width="100%" class="hideDivClass">
			<tr><td>
				<a href="#" id="subques2_showassess22" onclick="javascript: return showassessment1('<s:property value="%{pkCodeId}"/>','subques2','2','2')" >
					<s:text name="Show Assessment"/>
				</a>
				<a href="#" id="subques2_hideassess22" onclick="hideassessment('subques2','2','2')" style="display: none;">
					<s:text name="Hide Assessment"/>
				</a>
			</td>
			<td align="right">
			<a href="#" id="editidmqus2" onclick="editidmother('subques2',this)">
				<s:text name="Edit"/>
			</a>
			<a href="#" id="saveidmqus2" onclick="saveidmother('subques2',this,'idmsubques2')" style="display: none;">
				<s:text name="Save"/>
			</a>
			</td>
			</tr>
			</table>
		</td>
		</s:iterator>
		</s:if>
		</s:iterator>
	</tr>
	<tr>
		<td id="title_screeningtests" style="background:#E6E6E5;font-weight: bold; padding: 10px;" colspan="3">
			<s:text name="garuda.idm.label.screeningtests"/>
		</td>
	</tr>
	<s:iterator value="idmTestlist" var="labTestPojo" status="row1">
	<s:if test="%{fktestgroup!=pktestgrp}">
		<s:set value="%{#row1.index+3}" name="indexval"></s:set>
		<tr style="display: none;" id="<s:property value='%{shrtName}'/>">
			<td colspan="3" style="background:#E6E6E5;font-weight: bold; padding: 10px;" id="<s:property value='%{shrtName}'/>_head">
				<s:text name="garuda.idm.label.confirmatorytest"/>
			</td>
		</tr>
		<tr id="<s:property value='%{shrtName}'/>_test"> 
			<td align="center" id="<s:property value='%{shrtName}'/>_testindex"> <s:property value='%{#indexval}'/></td>
			<td id="<s:property value='%{shrtName}'/>_name">
				<img id="<s:property value='%{shrtName}'/>" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
				<span><b><s:property value="labtestName" /></b></span>
				<span class="error" id="<s:property value='%{shrtName}'/>_manditory" style="display:none">
			<s:text name="garuda.label.dynamicform.astrik"/>
		</span>
				&nbsp;&nbsp;
				<a href="#" id="testcomment_<s:property value='%{#indexval}'/>" onclick="commenttoggle(this.id,<s:property value='%{#indexval}'/>);" style="text-decoration: none;white-space: nowrap;">
				<img height="15px" src="./images/addcomment.png">
				<s:text name="garuda.label.dynamicform.comment"/></a>&nbsp;&nbsp;
				<br/>
				<div id="<s:property value='%{shrtName}'/>_assess">
				</div>
			</td>
			
			<s:iterator var="count" begin="1" end="1">
			<s:set name="flag" value="0"></s:set>
			<s:iterator value="idmTests" var="idmtestpojo" status="row">
			<s:if test="%{fktestsource!=null}">
					<s:hidden name="fktestsource" id="fktestsource"></s:hidden>
			</s:if>
			<s:if test='%{pkpatlabs!=null && pkpatlabs!=""}'>
			<s:if test="%{pkLabtest==fktestid}">
			<s:if test="%{testcount==#count}">
			<s:set name="flag" value="1" ></s:set>
			
			<td id="<s:property value='%{shrtName}'/>_col<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
			<div id="content_<s:property value='%{#indexval}'/>" class="commentbox" style="display: none;">
					<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<s:text name="Comment Box" />
				</div>									
					<s:textarea name="patLabs[%{#row1.index}].testcomments" cols="20" rows="5" value="%{testcomments}"
					id="%{shrtName}_testComment_%{#count}"/>
					<br/>
					<span id="<s:property value='%{shrtName}'/>_testComment_<s:property value='%{#count}'/>_err" class='error'
					style="display:none">
						<s:text name="garuda.label.dynamicform.errormsg"></s:text>
					</span><br/>
					<input type="button" id="button_<s:property value='%{#indexval}'/>" value="Save"
					onclick='checkcommentnull("<s:property value='%{shrtName}'/>","<s:property value='%{#count}'/>","<s:property value='%{#indexval}'/>");'/>
					<input type="button" onclick="hidecomment(<s:property value='%{#indexval}'/>)" id="button_<s:property value='%{#indexval}'/>" value="Close"/>
				</div>
			</div>
				<s:hidden name="patLabs[%{#row1.index}].pkpatlabs" id="%{shrtName}_pkpatlabs_%{#count}" value="%{pkpatlabs}"></s:hidden>
				<s:hidden name="patLabs[%{#row1.index}].fktestid" id="%{shrtName}_fktestid_%{#count}" value="%{fktestid}"></s:hidden>
				<s:hidden name="patLabs[%{#row1.index}].fktestgroup" id="%{shrtName}_fktestgrp_%{#count}" value="%{fklabgroup}"></s:hidden>
				<s:hidden name="fktestsource" id="%{shrtName}_fktestsource_%{#count}" value="%{fktestsource}"></s:hidden>
				<s:hidden name="patLabs[%{#row1.index}].testcount" id="%{shrtName}_testcount_%{#count}" value="%{#count}" ></s:hidden>
				
				<s:if test="%{shrtName!='FTA_ABS' && shrtName!='SYP_SUPP'}">
					<select name="patLabs[<s:property value='%{#row1.index}'/>].fktestoutcome" id='<s:property value='%{shrtName}'/>_testres<s:property value='%{#count}'/>'
					 disabled="disabled" onchange="checknull(this.id);checknotdone(this.id,'<s:property value='%{shrtName}'/>','<s:property value='%{#count}'/>');
					 showsubquestions();">
					<option value="">Select</option>
					<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_OUTCOMES]">
						<s:if test="%{subType!='indeter'}">
							<s:if test="%{fktestoutcome==pkCodeId}">
								<option value='<s:property value='%{pkCodeId}'/>' selected="selected"> <s:text name="%{description}"></s:text> </option>
							</s:if>
							<s:else>
								<option value='<s:property value='%{pkCodeId}'/>'> <s:text name="%{description}"></s:text> </option>
							</s:else>
						</s:if>
					</s:iterator>
					</select>
				</s:if>
				<s:if test="%{shrtName=='FTA_ABS'}">
					<s:select name="patLabs[%{#row1.index}].fktestoutcome" 
						list="#{'0':'No','1':'Yes'}" headerKey="" headerValue="Select" value="fktestoutcome" 
						id="%{shrtName}_testres%{#count}" disabled="true"
						onchange="checknull(this.id);checknotdone(this.id,'%{shrtName}','%{#count}');showsubquestions();"/>
				</s:if>
				<s:if test="%{shrtName=='SYP_SUPP'}">
					<s:select name="patLabs[%{#row1.index}].fktestoutcome" id="%{shrtName}_testres%{#count}" 
							list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_OUTCOMES]"
							listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select" value="fktestoutcome" 
							disabled="true" onchange="checknull(this.id);checknotdone(this.id,'%{shrtName}','%{#count}');showsubquestions();"/>
				</s:if>
				
				<table width="100%" class="hideDivClass">
				<tr>
				<s:if test="%{pkassessment!=null}">
				<td>
				<div id="<s:property value='%{shrtName}'/>_assess<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_showassess<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="javascript: return showassessment1('<s:property value='%{pkpatlabs}'/>','<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Show Assessment"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_hideassess<s:property value='%{#indexval}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="hideassessment('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Hide Assessment"/>
					</a>
				</div>
				</div>
				</td>
				</s:if>
				<td align="right">
				<div id="<s:property value='%{shrtName}'/>_editorsave<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_editidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="editidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>','editidm','saveidm','testres')">
						<s:text name="Edit"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_saveidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="saveidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Save"/>
					</a>
				</div>
				</div>
				</td>
				</tr>
				</table>
				<span class="error" id="<s:property value='%{shrtName}'/>_testres<s:property value='%{#count}'/>_err" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"/></span>
				<span class="error" id="<s:property value='%{shrtName}'/>_testres<s:property value='%{#count}'/>_err1" style="display:none"><s:text name="Test Response is Not Done."/></span>
			</td>
			</s:if>
			</s:if>
			</s:if>
			</s:iterator>
			
			<s:if test="%{#flag==0}">
			
			<td id="<s:property value='%{shrtName}'/>_col<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
				<div id="content_<s:property value='%{#indexval}'/>" class="commentbox" style="display: none;">
					<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<s:text name="Comment Box" />
				</div>									
					<s:textarea name="patLabs[%{#row1.index}].testcomments" cols="20" rows="5" value="%{testcomments}"
					id="%{shrtName}_testComment_%{#count}"/>
					<br/>
					<span id="<s:property value='%{shrtName}'/>_testComment_<s:property value='%{#count}'/>_err" class='error'
					style="display:none">
						<s:text name="garuda.label.dynamicform.errormsg"></s:text>
					</span><br/>
					<input type="button" id="button_<s:property value='%{#indexval}'/>" value="Save"
					onclick='checkcommentnull("<s:property value='%{shrtName}'/>","<s:property value='%{#count}'/>","<s:property value='%{#indexval}'/>");'/>&nbsp;&nbsp;
					<input type="button" onclick="hidecomment(<s:property value='%{#indexval}'/>)" id="button_<s:property value='%{#indexval}'/>" value="Close"/>
				</div>
			</div>
				<s:hidden name="patLabs[%{#row1.index}].pkpatlabs" id="%{shrtName}_pkpatlabs_%{#count}" value="%{pkpatlabs}"></s:hidden>
				<s:hidden name="patLabs[%{#row1.index}].fktestgroup" id="%{shrtName}_fktestgrp_%{#count}" value="%{fktestgroup}"></s:hidden>
				<s:hidden name="patLabs[%{#row1.index}].fktestid" id="%{shrtName}_fktestid_%{#count}" value="%{pkLabtest}"></s:hidden>
				<s:hidden name="fktestsource" id="%{shrtName}_fktestsource_%{#count}" value=""></s:hidden>
				<s:hidden name="patLabs[%{#row1.index}].testcount" id="%{shrtName}_testcount_%{#count}" value="%{#count}"></s:hidden>
				
			<s:if test="%{shrtName!='FTA_ABS' && shrtName!='SYP_SUPP'}">
					<select name="patLabs[<s:property value='%{#row1.index}'/>].fktestoutcome" id='<s:property value='%{shrtName}'/>_testres<s:property value='%{#count}'/>'
					 disabled="disabled" onchange="checknull(this.id);checknotdone(this.id,'<s:property value='%{shrtName}'/>','<s:property value='%{#count}'/>');
					 showsubquestions();">
					<option value="">Select<s:property value="%{fktestoutcome}"/></option>
					<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_OUTCOMES]">
						<s:if test="%{subType!='indeter'}">
							<s:if test="%{fktestoutcome==pkCodeId}">
								<option value='<s:property value='%{pkCodeId}'/>' selected="selected"><s:text name="%{description}"></s:text> </option>
							</s:if>
							<s:else>
								<option value='<s:property value='%{pkCodeId}'/>'> <s:text name="%{description}"></s:text> </option>
							</s:else>
						</s:if>
					</s:iterator>
					</select>
				</s:if>
			<s:if test="%{shrtName=='FTA_ABS'}">
				<s:select  name="patLabs[%{#row1.index}].fktestoutcome" 
					list="#{'0':'No','1':'Yes'}" headerKey="" headerValue="Select" value="fktestoutcome" 
					id="%{shrtName}_testres%{#count}" disabled="true" onchange="checknull(this.id);
					checknotdone(this.id,'%{shrtName}','%{#count}');showsubquestions();"/>
			</s:if>
			<s:if test="%{shrtName=='SYP_SUPP'}">
				<s:select name="patLabs[%{#row1.index}].fktestoutcome" id="%{shrtName}_testres%{#count}" 
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_OUTCOMES]"
						listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select" value="fktestoutcome" 
						disabled="true" onchange="checknull(this.id);checknotdone(this.id,'%{shrtName}','%{#count}');
						showsubquestions();"/>
			</s:if>
			<table width="100%" class="hideDivClass">
				<tr>
				<td align="right">
				<div id="<s:property value='%{shrtName}'/>_editorsave<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_editidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="editidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>','editidm','saveidm','testres')">
						<s:text name="Edit"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_saveidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="saveidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Save"/>
					</a>
				</div>
				</div>
				</td>
				</tr>
			</table>
			<span class="error" id="<s:property value='%{shrtName}'/>_testres<s:property value='%{#count}'/>_err" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"/></span>
			<span class="error" id="<s:property value='%{shrtName}'/>_testres<s:property value='%{#count}'/>_err1" style="display:none"><s:text name="Test Response is Not Done."/></span>
			</td>
			</s:if>
			</s:iterator>
		</tr>
		<tr id="<s:property value='%{shrtName}'/>_Testdate">
			<td align="center" id="<s:property value='%{shrtName}'/>_TdateIndex">
				<s:property value='%{#indexval}'/><s:text name="a"/>
			</td>
			<td style="padding-left: 30px" id="<s:property value='%{shrtName}'/>_tdateL">
				<s:text name="garuda.idm.label.testdate"/>
				<span class="error" style="display:none" id="<s:property value='%{shrtName}'/>_testDateman">
			<s:text name="garuda.label.dynamicform.astrik"/>
		</span>
			</td>
			<s:iterator var="count" begin="1" end="1">
			<s:set name="flag" value="0"></s:set>
			<s:iterator value="idmTests" var="idmtestpojo" status="row">
			<s:if test='%{pkpatlabs!=null && pkpatlabs!=""}'>
			<s:if test="%{fktestid==pkLabtest}">
			<s:if test="%{testcount==#count}">
			<s:set name="flag" value="1"></s:set>
			<td id="<s:property value='%{shrtName}'/>_tdate<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
				<s:date	name="testDate" id="%{shrtName}_testDate%{#count}"format="MMM dd, yyyy"/>
				<s:textfield readonly="true" onkeydown="cancelBack();"  name="patLabs[%{#row1.index}].testDateStr" cssClass="datePicWMaxDate idmMandatory"
					value="%{getText('collection.date',{testDate})}" id="%{shrtName}_testDate%{#count}" 
					disabled="true" onchange="checknull(this.id);"/>
			<table width="100%" class="hideDivClass">
				<tr>
				<td align="right">
				<div id="<s:property value='%{shrtName}'/>_tdeditorsave<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_tdeditidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="editidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>','tdeditidm','tdsaveidm','testDate')">
						<s:text name="Edit"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_tdsaveidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="saveidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Save"/>
					</a>
				</div>
				</div>
				</td>
				</tr>
			</table>
			<span class="error" id="<s:property value='%{shrtName}'/>_testDate<s:property value='%{#count}'/>_err" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"/></span>
			</td>
			</s:if>
			</s:if>
			</s:if>
			</s:iterator>
			
			<s:if test="%{#flag==0}">
			<td id="<s:property value='%{shrtName}'/>_tdate<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
				<s:hidden name="pkLabtest" id="%{shrtName}_fktestid_%{#count}"></s:hidden>
				<s:date	name="testDate" id="%{shrtName}_testDate%{#count}"format="MMM dd, yyyy"/>
				<s:textfield readonly="true" onkeydown="cancelBack();"  name="patLabs[%{#row1.index}].testDateStr" cssClass="datePicWMaxDate idmMandatory"
					value="%{getText('collection.date',{testDate})}" id="%{shrtName}_testDate%{#count}"
					disabled="true" onchange="checknull(this.id);"/>
			<table width="100%" class="hideDivClass">
				<tr>
				<td align="right">
				<div id="<s:property value='%{shrtName}'/>_tdeditorsave<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_tdeditidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="editidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>','tdeditidm','tdsaveidm','testDate')">
						<s:text name="Edit"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_tdsaveidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="saveidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Save"/>
					</a>
				</div>
				</div>
				</td>
				</tr>
			</table>
			<span class="error" id="<s:property value='%{shrtName}'/>_testDate<s:property value='%{#count}'/>_err" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"/></span>
			</td>
			</s:if>
		</s:iterator>
		</tr>
		<s:if test="%{shrtName!='FTA_ABS'}">
		<tr id="<s:property value='%{shrtName}'/>_SubQ1">
			<td align="center" id="<s:property value='%{shrtName}'/>_SubQ1Index">
				<s:property value='%{#indexval}'/><s:text name="b"/>
			</td>
			<td style="padding-left: 30px" id="<s:property value='%{shrtName}'/>_SubQ1L">
				<s:text name="garuda.idm.label.cmsapprovedlab"/>
				<span class="error" style="display:none" id="<s:property value='%{shrtName}'/>_SubQus1man">
			<s:text name="garuda.label.dynamicform.astrik"/>
		</span>
			</td>
			<s:iterator var="count" begin="1" end="1">
			<s:set name="flag" value="0"></s:set>
			<s:iterator value="idmTests" var="idmtestpojo" status="row">
			<s:if test='%{pkpatlabs!=null && pkpatlabs!=""}'>
			<s:if test="%{fktestid==pkLabtest}">
			<s:if test="%{testcount==#count}">
			<s:set name="flag" value="1"></s:set>
			<td id="<s:property value='%{shrtName}'/>_SQ1R<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
				<s:select name="patLabs[%{#row1.index}].cmsapprovedlab" id="%{shrtName}_sq1res%{#count}"
						list="#{'0':'No','1':'Yes'}" value="cmsapprovedlab" headerKey="" headerValue="Select" 
						disabled="true" onchange="checknull(this.id);"/>
			<table width="100%" class="hideDivClass">
				<tr>
				<td align="right">
				<div id="<s:property value='%{shrtName}'/>_sq1editorsave<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_sq1editidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="editidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>','sq1editidm','sq1saveidm','sq1res')">
						<s:text name="Edit"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_sq1saveidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="saveidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Save"/>
					</a>
				</div>
				</div>
				</td>
				</tr>
			</table>
			<span class="error" id="<s:property value='%{shrtName}'/>_sq1res<s:property value='%{#count}'/>_err" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"/></span>
			</td>
			</s:if>
			</s:if>
			</s:if>
			</s:iterator>
			<s:if test="%{#flag==0}">
			<td id="<s:property value='%{shrtName}'/>_SQ1R<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
				<s:hidden name="pkLabtest" id="%{shrtName}_fktestid_%{#count}"></s:hidden>
				<s:select name="patLabs[%{#row1.index}].cmsapprovedlab" id="%{shrtName}_sq1res%{#count}" 
				list="#{'0':'No','1':'Yes'}"value="cmsapprovedlab" headerKey=""	headerValue="Select"
				disabled="true" onchange="checknull(this.id);"/>
			<table width="100%" class="hideDivClass">
				<tr>
				<td align="right">
				<div id="<s:property value='%{shrtName}'/>_sq1editorsave<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_sq1editidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="editidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>','sq1editidm','sq1saveidm','sq1res')">
						<s:text name="Edit"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_sq1saveidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="saveidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Save"/>
					</a>
				</div>
				</div>
				</td>
				</tr>
			</table>
			<span class="error" id="<s:property value='%{shrtName}'/>_sq1res<s:property value='%{#count}'/>_err" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"/></span>
			</td>
			</s:if>
			</s:iterator>
		</tr> 
		<s:if test="%{shrtName!='SYP_SUPP'}">
		<tr id="<s:property value='%{shrtName}'/>_SubQ2">
			<td align="center" id="<s:property value='%{shrtName}'/>_SubQ2Index">
				<s:property value='%{#indexval}'/><s:text name="c"/>
			</td>
			<td style="padding-left: 30px" id="<s:property value='%{shrtName}'/>_SubQ2L">
				<s:text name="garuda.idm.label.fdalicensedlab"/>
				<span class="error" style="display:none" id="<s:property value='%{shrtName}'/>_SubQusman">
			<s:text name="garuda.label.dynamicform.astrik"/>
		</span>
			</td>
			<s:iterator var="count" begin="1" end="1">
			<s:set name="flag" value="0"></s:set>
			<s:iterator value="idmTests" var="idmtestpojo" status="row">
			<s:if test='%{pkpatlabs!=null && pkpatlabs!=""}'>
			<s:if test="%{fktestid==pkLabtest}">
			<s:if test="%{testcount==#count}">
			<s:set name="flag" value="1"></s:set>
			<td id="<s:property value='%{shrtName}'/>_SQ2R<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
				<s:select  name="patLabs[%{#row1.index}].fdalicensedlab" id="%{shrtName}_sq2res%{#count}"
				  list="#{'0':'No','1':'Yes'}" value="fdalicensedlab" headerKey="" headerValue="Select"
				 disabled="true" onchange="checknull(this.id);"/>
			<table width="100%" class="hideDivClass">
				<tr>
				<td align="right">
				<div id="<s:property value='%{shrtName}'/>_sq2editorsave<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_sq2editidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="editidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>','sq2editidm','sq2saveidm','sq2res')">
						<s:text name="Edit"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_sq2saveidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="saveidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Save"/>
					</a>
				</div>
				</div>
				</td>
				</tr>
			</table>
			<span class="error" id="<s:property value='%{shrtName}'/>_sq2res<s:property value='%{#count}'/>_err" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"/></span>
			</td>
			</s:if>
			</s:if>
			</s:if>
			</s:iterator>
			<s:if test="%{#flag==0}">
			<td id="<s:property value='%{shrtName}'/>_SQ2R<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
				<s:hidden name="pkLabtest" id="%{shrtName}_fktestid_%{#count}"></s:hidden>
				<s:select name="patLabs[%{#row1.index}].fdalicensedlab" id="%{shrtName}_sq2res%{#count}" 
						list="#{'0':'No','1':'Yes'}" value="fdalicensedlab" headerKey=""	headerValue="Select"
						disabled="true" onchange="checknull(this.id);"/>
			<table width="100%" class="hideDivClass">
				<tr>
				<td align="right">
				<div id="<s:property value='%{shrtName}'/>_sq2editorsave<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_sq2editidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="editidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>','sq2editidm','sq2saveidm','sq2res')">
						<s:text name="Edit"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_sq2saveidm<s:property value='%{#indexval}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="saveidm('<s:property value='%{shrtName}'/>','<s:property value='%{#indexval}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Save"/>
					</a>
				</div>
				</div>
				</td>
				</tr>
			</table>
			<span class="error" id="<s:property value='%{shrtName}'/>_sq2res<s:property value='%{#count}'/>_err" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"/></span>
			</td>
			</s:if>
			</s:iterator>
		</tr>
		</s:if>
		</s:if>
		</s:if>
		</s:iterator>
		<tr>
			<td colspan="3" style="background:#E6E6E5;font-weight: bold; padding: 10px;" id="additIDM">
				<s:text name="Additional / Miscellaneous IDM Tests"/>
			</td>
		</tr>
		<s:iterator value="idmtestlist1" var="pojo" status="row">
			<s:set value="%{#row.index+20}" name="indexval1" scope="request" ></s:set>
			<s:set value="%{#row.index+17}" name="rowstatus" scope="request" ></s:set>
			<tr id="<s:property value='%{shrtName}'/>_test"> 
			<td align="center" id="<s:property value='%{shrtName}'/>_testindex"> <s:property value='%{#request.indexval1}'/></td>
			<td id="<s:property value='%{shrtName}'/>_name">
				<span><b><s:property value="labtestName" /></b></span>
				&nbsp;&nbsp;
				<a href="#" id="testcomment_<s:property value='%{#request.indexval1}'/>" onclick="commenttoggle(this.id,<s:property value='%{#request.indexval1}'/>);" style="text-decoration: none;white-space: nowrap;">
				<img height="15px" src="./images/addcomment.png">
				<s:text name="garuda.label.dynamicform.comment"/></a>&nbsp;&nbsp;
				<br/>
				<div id="<s:property value='%{shrtName}'/>_assess">
				</div>
			</td>
			<s:iterator var="count" begin="1" end="1">
			<s:set name="flag" value="0"></s:set>
			<s:iterator value="idmTests" var="idmtestpojo" status="row">
			<s:if test='%{pkpatlabs!=null && pkpatlabs!=""}'>
			<s:if test="%{pkLabtest==fktestid}">
			<s:if test="%{testcount==#count}">
			<s:set name="flag" value="1"></s:set>
			
			<td id="<s:property value='%{shrtName}'/>_col<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>">
				<div id="content_<s:property value='%{#request.indexval1}'/>" class="commentbox" style="display: none;">
					<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<s:text name="Comment Box" />
				</div>									
					<s:textarea name="patLabs[%{#row1.index}].testcomments" cols="20" rows="5" value="%{testcomments}"
					id="%{shrtName}_testComment_%{#count}"/>
					<br/>
					<span id="<s:property value='%{shrtName}'/>_testComment_<s:property value='%{#count}'/>_err" class='error'
					style="display:none">
						<s:text name="garuda.label.dynamicform.errormsg"></s:text>
					</span><br/>
					<input type="button" id="button_<s:property value='%{#request.indexval1}'/>" value="Save"
					onclick='checkcommentnull("<s:property value='%{shrtName}'/>","<s:property value='%{#count}'/>","<s:property value='%{#indexval}'/>");'/>&nbsp;&nbsp;
					<input type="button" onclick="hidecomment(<s:property value='%{#request.indexval1}'/>)" id="button_"+<s:property value='%{#request.indexval}'/> value="Close"/>
				</div>
			</div>
				<s:hidden name="patLabs[%{#row1.index}].pkpatlabs" id="%{shrtName}_pkpatlabs_%{#count}" value="%{pkpatlabs}"></s:hidden>
				<s:hidden name="patLabs[%{#row1.index}].fktestid" id="%{shrtName}_fktestid_%{#count}" value="%{fktestid}"></s:hidden>
				<s:hidden name="patLabs[%{#row1.index}].fklabgroup" id="%{shrtName}_fktestgrp_%{#count}" value="%{fklabgroup}"></s:hidden>
				<s:hidden name="fktestsource" id="%{shrtName}_fktestsource_%{#count}"></s:hidden>
				<s:hidden name="patLabs[%{#row1.index}].testcount" id="%{shrtName}_testcount_%{#count}" value="%{#count}"></s:hidden>
				
				<s:select name="patLabs[%{#row.index}].fktestoutcome" id="%{shrtName}_testres%{#count}" 
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_OUTCOMES]"
						listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select" value="fktestoutcome" 
						disabled="true" onchange="checknull(this.id);checknotdone(this.id,'%{shrtName}','%{#count}');" cssClass="checkNotDoneOther"/>
				<table width="100%"  class="hideDivClass">
				<tr>
				<s:if test="%{pkassessment!=null}">
				<td>
				<div id="<s:property value='%{shrtName}'/>_assess<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_showassess<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="javascript: return showassessment1('<s:property value='%{pkpatlabs}'/>','<s:property value='%{shrtName}'/>','<s:property value='%{#request.indexval1}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Show Assessment"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_hideassess<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="hideassessment('<s:property value='%{shrtName}'/>','<s:property value='%{#request.indexval1}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Hide Assessment"/>
					</a>
				</div>
				</div>
				</td>
				</s:if>
				<td align="right">
				<div id="<s:property value='%{shrtName}'/>_editorsave<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_editidm<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="editidm('<s:property value='%{shrtName}'/>','<s:property value='%{#request.indexval1}'/>','<s:property value='%{#count}'/>','editidm','saveidm','testres')">
						<s:text name="Edit"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_saveidm<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="saveidm('<s:property value='%{shrtName}'/>','<s:property value='%{#request.indexval1}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Save"/>
					</a>
				</div>
				</div>
				</td>
				</tr>
				</table>
				<span class="error" id="<s:property value='%{shrtName}'/>_testres<s:property value='%{#count}'/>_err" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"/></span>
				<span class="error" id="<s:property value='%{shrtName}'/>_testres<s:property value='%{#count}'/>_err1" style="display:none"><s:text name="Test Response is Not Done."/></span>
			</td>
			</s:if>
			</s:if>
			</s:if>
			</s:iterator>
			<s:if test="%{#flag==0}">
			
			<td id="<s:property value='%{shrtName}'/>_col<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>">
				<div id="content_<s:property value='%{#indexval}'/>" class="commentbox" style="display: none;">
					<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<s:text name="Comment Box" />
				</div>									
					<s:textarea name="patLabs[%{#row1.index}].testcomments" cols="20" rows="5" value="%{testcomments}"
					id="%{shrtName}_testComment_%{#count}"/>
					<br/>
					<span id="<s:property value='%{shrtName}'/>_testComment_<s:property value='%{#count}'/>_err" class='error'
					style="display:none">
						<s:text name="garuda.label.dynamicform.errormsg"></s:text>
					</span><br/>
					<input type="button" id="button_<s:property value='%{#request.indexval1}'/>" value="Save"
					onclick='checkcommentnull("<s:property value='%{shrtName}'/>","<s:property value='%{#count}'/>","<s:property value='%{#indexval}'/>");'/>&nbsp;&nbsp;
					<input type="button" onclick="hidecomment(<s:property value='%{#indexval}'/>)" id="button_<s:property value='%{#indexval}'/>" value="Close"/>
				</div>
				</div>
				<s:hidden name="patLabs[%{#row1.index}].fktestid" id="%{shrtName}_fktestid_%{#count}" value="%{pkLabtest}"></s:hidden>
				<s:hidden name="patLabs[%{#row1.index}].testcount" id="%{shrtName}_testcount_%{#count}" value="%{#count}"></s:hidden>
				<s:hidden name="patLabs[%{#row1.index}].pkpatlabs" id="%{shrtName}_pkpatlabs_%{#count}" value="%{pkpatlabs}"></s:hidden>
				<s:hidden name="patLabs[%{#row1.index}].fklabgroup" id="%{shrtName}_fktestgrp_%{#count}" value="%{fktestgroup}"></s:hidden>
				<s:hidden name="fktestsource" id="%{shrtName}_fktestsource_%{#count}" value=""></s:hidden>
				
				<s:select name="patLabs[%{#row.index}].fktestoutcome" id="%{shrtName}_testres%{#count}" 
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_OUTCOMES]"
						listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select" value="fktestoutcome" 
						disabled="true" onchange="checknull(this.id);checknotdone(this.id,'%{shrtName}','%{#count}');" cssClass="checkNotDoneOther"/>
			<table width="100%"  class="hideDivClass">
				<tr>
				<td align="right">
				<div id="<s:property value='%{shrtName}'/>_editorsave<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_editidm<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="editidm('<s:property value='%{shrtName}'/>','<s:property value='%{#request.indexval1}'/>','<s:property value='%{#count}'/>','editidm','saveidm','testres')">
						<s:text name="Edit"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_saveidm<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="saveidm('<s:property value='%{shrtName}'/>','<s:property value='%{#request.indexval1}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Save"/>
					</a>
				</div>
				</div>
				</td>
				</tr>
			</table>
			<span class="error" id="<s:property value='%{shrtName}'/>_testres<s:property value='%{#count}'/>_err" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"/></span>
			<span class="error" id="<s:property value='%{shrtName}'/>_testres<s:property value='%{#count}'/>_err1" style="display:none"><s:text name="Test Response is Not Done."/></span>
			</td>
			</s:if>
			</s:iterator>
		</tr>
		<tr id="<s:property value='%{shrtName}'/>_Testdate">
			<td align="center" id="<s:property value='%{shrtName}'/>_TdateIndex">
				<s:property value='%{#request.indexval1}'/><s:text name="a"/>
			</td>
			<td style="padding-left: 30px" id="<s:property value='%{shrtName}'/>_tdateL">
				<s:text name="garuda.idm.label.testdate"/>
			</td>
			<s:iterator var="count" begin="1" end="1">
			<s:set name="flag" value="0"></s:set>
			<s:iterator value="idmTests" var="idmtestpojo" status="row">
			<s:if test='%{pkpatlabs!=null && pkpatlabs!=""}'>
			<s:if test="%{fktestid==pkLabtest}">
			<s:if test="%{testcount==#count}">
			<s:set name="flag" value="1"></s:set>
			<td id="<s:property value='%{shrtName}'/>_tdate<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>">
				<s:date	name="testDate" id="%{shrtName}_testDate%{#count}"format="MMM dd, yyyy"/>
				<s:textfield readonly="true" onkeydown="cancelBack();"  name="patLabs[%{#row.index}].testDateStr" cssClass="datePicWMaxDate idmMandatory"
					value="%{getText('collection.date',{testDate})}" id="%{shrtName}_testDate%{#count}" 
					disabled="true" onchange="checknull(this.id);"/>
			<table width="100%" class="hideDivClass">
				<tr>
				<td align="right">
				<div id="<s:property value='%{shrtName}'/>_tdeditorsave<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_tdeditidm<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="editidm('<s:property value='%{shrtName}'/>','<s:property value='%{#request.indexval1}'/>','<s:property value='%{#count}'/>','tdeditidm','tdsaveidm','testDate')">
						<s:text name="Edit"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_tdsaveidm<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="saveidm('<s:property value='%{shrtName}'/>','<s:property value='%{#request.indexval1}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Save"/>
					</a>
				</div>
				</div>
				</td>
				</tr>
			</table>
			<span class="error" id="<s:property value='%{shrtName}'/>_testDate<s:property value='%{#count}'/>_err" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"/></span>
			</td>
			</s:if>
			</s:if>
			</s:if>
			</s:iterator>
			<s:if test="%{#flag==0}">
			<td id="<s:property value='%{shrtName}'/>_tdate<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>">
				<s:hidden name="pkLabtest" id="%{shrtName}_fktestid_%{#count}"></s:hidden>
				<s:date	name="testDate" id="%{shrtName}_testDate%{#count}"format="MMM dd, yyyy"/>
				<s:textfield readonly="true" onkeydown="cancelBack();"  name="patLabs[%{#row.index}].testDateStr" cssClass="datePicWMaxDate idmMandatory"
					value="%{getText('collection.date',{testDate})}" id="%{shrtName}_testDate%{#count}"
					disabled="true" onchange="checknull(this.id);"/>
			<table width="100%" class="hideDivClass">
				<tr>
				<td align="right">
				<div id="<s:property value='%{shrtName}'/>_tdeditorsave<s:property value='%{#count}'/>">
				<div id="<s:property value='%{shrtName}'/>_tdeditidm<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>">
					<a href="#" onclick="editidm('<s:property value='%{shrtName}'/>','<s:property value='%{#request.indexval1}'/>','<s:property value='%{#count}'/>','tdeditidm','tdsaveidm','testDate')">
						<s:text name="Edit"/>
					</a>
				</div>
				<div id="<s:property value='%{shrtName}'/>_tdsaveidm<s:property value='%{#request.indexval1}'/><s:property value='%{#count}'/>" style="display: none">
					<a href="#" onclick="saveidm('<s:property value='%{shrtName}'/>','<s:property value='%{#request.indexval1}'/>','<s:property value='%{#count}'/>')">
						<s:text name="Save"/>
					</a>
				</div>
				</div>
				</td>
				</tr>
			</table>
			<span class="error" id="<s:property value='%{shrtName}'/>_testDate<s:property value='%{#count}'/>_err" style="display:none"><s:text name="garuda.label.dynamicform.errormsg"/></span>
			</td>
			</s:if>
			</s:iterator>
		</tr>
	</s:iterator>
	</tbody>
</table>
</div>
<!--</td>
 <td valign="top" nowrap>
	<button type="button" onclick="addtestdate();">Add Test Date</button>
</td>

</tr>
</table>
-->
<div id="helpmsg" style="display: none;">
	<div id=HB_msg>
			<table>
	   			<tr><td><u>Current Evaluation and Action</u></td></tr>
	   			<tr><td>If Reactive/Positive - Defer</td></tr>
	   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
	   			<tr><td>If Not Done - Defer</td></tr>
	   			<tr></tr>
	   			<tr><td><u>Historical Evaluation</u></td></tr>
	   			<tr><td>Same as current evaluation and action for completed tests.NMDP and 10-CBA requires valid test for HBsAg.</td></tr>
	   		</table>
   		</div>
   		<div id=ANHB_msg>
   			<table>
   				<tr><td><u>Current Evaluation and Action</u></td></tr>
   				<tr><td>If Reactive/Positive - Ineligible if collection date on or after May 25, 2005.</td></tr>  
   				<tr><td>If Non-Reactive/Negative - No action</td></tr>
   				<tr><td>If Not Done - Incomplete if collection date on or after May 25, 2005.</td></tr>
   				<tr></tr>
   				<tr><td><u>Historical Evaluation</u></td></tr>
   				<tr><td>Same as current evaluation and action for completed tests.</td></tr>
   				<tr><td>Accommodates incomplete eligibility status when testing not performed using FA approved donor screening tests; implemented on October 1, 2011.</td></tr>
   			</table>
   		</div>
   		<div id=ANHCV_msg>
   			   		<table>
	   			<tr><td><u>Current Evaluation and Action</u></td></tr>
	   			<tr><td>If Reactive/Positive - Defer</td></tr>
	   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
	   			<tr><td>If Not Done - Defer</td></tr>
	   			<tr></tr>
	   			<tr><td><u>Historical Evaluation</u></td></tr>
	   			<tr><td>Same as current evaluation and action for completed tests.NMDP and 10-CBA requires valid test for anti-HCV.</td></tr>
	   		</table>
   		</div>
   		<div id="ANHIV_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Reactive/Positive - Defer</td></tr>
   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
   			<tr><td>If Not Done - then Question 7 is required, Defer if Question 7 is also Not Done.</td></tr>
   			<tr><td>If test completed, Question 7 will be inactivated.</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Same as current evaluation and action for completed tests.</td></tr>
   			<tr><td>NMDP and 10-CBA requires valid test for anti-HIV 1/2.  Anti-HIV 1/2 + O testing is required when related questions are not included as part of maternal donor screnning for eligiblity determination.</td></tr>
   		</table>
   		</div>
   		<div id="ANHIV1_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Reactive/Positive - Defer</td></tr>
   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
   			<tr><td>If Not Done - then Question 6 is required, Defer if Question 6 is also Not Done.</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Same as current evaluation and action for completed tests.</td></tr>
   			<tr><td>NMDP and 10-CBA requires valid test for anti-HIV 1/2.</td></tr>
   		</table>  
   		</div>
   		<div id="ANHTLV_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Reactive/Positive - Defer</td></tr>
   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
   			<tr><td>If Not Done - Incomplete if the collection date is on or after May 25, 2005.</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Same as current evaluation and action for completed tests.</td></tr>
   			<tr><td>Accommodates incomplete eligibility status when testing not performed using FDA approved donor screening tests; implemented on October 1, 2011.</td></tr>
   		</table>
   		</div>
   		<div id="ANCMV_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Reactive/Positive - No action</td></tr>
   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Same as current evaluation and action for completed tests.</td></tr>
   			<tr><td>Anti-CMV Total is not part of eligibility testing.</td></tr>
   			<tr><td>Anti-CMV Total (total IgG + IgM) was required by the NMDP by August 27, 2007.  If Anti-CMV Total was not performed, record IgG or IgM results in Additional/Miscellaneious IDM Tests  section.</td></tr>
   		</table>
   		</div>
   		<div id="NATH_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Reactive/Positive -  Defer</td></tr>
   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
   			<tr><td>If Not Done - Questions 11 and 13 are required and Question 14 is optional.  Defer if Questions 10, 11 and 12 are all Not Done.</td></tr>
   			<tr><td>If tests performed, Question 11, 13 and 14 are inactivated.</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Same as current evaluation and action for completed tests.</td></tr>
   			<tr><td>Combination MPX test added October 1, 2011 as an option for reporting NAT results.  If test performed on historical inventory, results may be reported in this field.</td></tr>
   			<tr><td>NMDP and 10-CBA require valid test for HIV-1 NAT or historical results for HIV p24 Antigen.  For units collected prior to February 28, 2004, HIV p24 was an acceptable test.</td></tr>
   		</table>
   		</div>
   		<div id="HIVNAT_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Reactive/Positive -  Defer</td></tr>
   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
   			<tr><td>If Not Done - and Question 10 is also Not Done, then Question 12 is required.</td></tr>
   			<tr><td>If tests performed, Questions 10 and 12 are inactivated.</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Same as current evaluation and action for completed tests.</td></tr>
   			<tr><td>NMDP and 10-CBA require valid test for HIV-1 NAT or historical results for HIV p24 Antigen.  For units collected prior to February 28, 2004, HIV p24 was an acceptable test.</td></tr>
   		</table>
   		</div>
   		<div id="HIVP_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Reactive/Positive -  Defer</td></tr>
   			<tr><td>If Non-Reactive/Negative - Defer if collection date on or after February 28, 2004.  No action if collection date before February 28, 2004.</td></tr>
   			<tr><td>If Not Done - Defer</td></tr>
   			<tr><td>Required if MPX Question 10 and Question 11 are both Not Done, otherwise inactive.</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Same as current evaluation and action for reactive/positive tests.</td></tr>
   			<tr><td>NMDP and 10-CBA require valid test for HIV-1 NAT or historical results for HIV p24 Antigen.  For units collected prior to February 28, 2004, HIV p24 was an acceptable test.</td></tr>
   		</table>
   		</div>
   		<div id="HCNAT_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Reactive/Positive -  Defer</td></tr>
   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
   			<tr><td>If Not Done - Incomplete if the collection date is on or after May 25, 2005.</td></tr>
   			<tr><td>Required if Question 10 is Not Done.</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Same as current evaluation and action for reactive/positive tests.</td></tr>
   			<tr><td>Accommodates incomplete eligiblity status when testing not performed using FDA approved donor screening tests; implemented on October 1, 2011.</td></tr>
   		</table>
   		</div>
   		<div id="HBNAT_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Reactive/Positive -  Defer</td></tr>
   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
   			<tr><td>If Not Done - No Action</td></tr>
   			<tr><td>Optional if Question 10 is Not Done.</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Same as current evaluation and action for reactive/positive tests.</td></tr>
   			<tr><td>Optional test as of October 1, 2011.</td></tr>
   		</table>
   		</div>
   		<div id="SYPH_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Reactive/Positive -  Evaluate against the confirmatory test results. Question 18 is required.</td></tr>
   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
   			<tr><td>If Not Done -Incomplete if the collection date is on or after May 25, 2005.</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Same as current evaluation and action for reactive/positive tests.</td></tr>
   			<tr><td>Accommodates incomplete eligibility status when testing not performed using FDA-cleared screening tests or FDA-cleared diagnostic serologic tests; implemented on October 1, 2011.</td></tr>
   		</table>
   		</div>
   		<div id="WNNAT_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Reactive/Positive - Defer</td></tr>
   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
   			<tr><td>If Not Done -Incomplete if the collection date is on or after May 25, 2005.</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Same as current evaluation and action for reactive/positive tests.</td></tr>
   			<tr><td>Accommodates incomplete eligibility status when testing not performed using FDA approved donor screening tests; implemented on October 1, 2011.</td></tr>
   			<tr><td>WNV NAT testing was required by the NMDP by October 19, 2004.</td></tr>
   		</table>
   		</div>
   		<div id="CHA_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Reactive/Positive - Defer</td></tr>
   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
   			<tr><td>If Not Done -Incomplete if the collection date is on or after January 1, 2008.</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Same as current evaluation and action for reactive/positive tests.</td></tr>
   			<tr><td>Accommodates incomplete eligibility status when testing not performed using FDA approved donor screening tests; implemented on October 1, 2011.</td></tr>
   			<tr><td>Chagas (T.cruzi) testing was not required by January 1, 2008.</td></tr>
   		</table>
   		</div>
   		<div id="SYP_SUPP_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Reactive/Positive - Defer</td></tr>
   			<tr><td>If Non-Reactive/Negative - No action</td></tr>
   			<tr><td>If Indeterminate - Defer</td></tr>
   			<tr><td>If Not Done - Defer</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Same as current evaluation and action for reactive/positive tests.</td></tr>
   			<tr><td>As of October 1, 2011, specific FDA-approved confirmatory test (FTA-ABS) must be used (see Question 19)  or unit is deferred.</td></tr>
   		</table>
   		</div>
   		<div id="FTA_ABS_msg">
   		<table>
   			<tr><td><u>Current Evaluation and Action</u></td></tr>
   			<tr><td>If Yes - Ineligible (Question 15 is Reactive/Positive, Question 18 is Non-Reactive/Negative)</td></tr>
   			<tr><td>If No - Defer</td></tr>
   			<tr><td>Question is activated based on response to Question 18.  This question active if Non-Reactive/Negative response to syphilis confirmatory Question 18.</td></tr>
   			<tr></tr>
   			<tr><td><u>Historical Evaluation</u></td></tr>
   			<tr><td>Specific FDA-approved confirmatory test (FTA-ABS) must be used in order to status as ineligible.</td></tr>
   		</table>
   		</div>
   		<div id="fdalicensedlab_msg">
   			<table>
   				<tr><td><u>Current Evaluation and Action</u></td></tr>
   				<tr><td>If Yes - no action.</td></tr>
   				<tr><td>If Some or None - the CBU should be considered Incomplete if the collection  date is on or after May 25, 2005.</td></tr>
   				<tr><td>If Some or None - no action if the collection date is before May 25, 2005.</td></tr>
   				<tr></tr>
   				<tr><td><u>Historical Evaluation</u></td></tr>
   				<tr><td>Same as current evaluation and action for completed tests.</td>
   				<tr><td>Accommodates incomplete eligibility status when testing not performed using FA approved donor screening tests; implemented on October 1, 2011.</td></tr>
   			</table>
   		</div>
   		<div id="cmsapprovedlab_msg">
   			<table>
   				<tr><td><u>Current Evaluation and Action</u></td></tr>
   				<tr><td>If Yes - no action.</td></tr>
   				<tr><td>If Some or None - the CBU should be considered Incomplete if the collection  date is on or after May 25, 2005.</td></tr>
   				<tr><td>If Some or None - no action if the collection date is before May 25, 2005.</td></tr>
   				<tr></tr>
   				<tr><td><u>Historical Evaluation</u></td></tr>
   				<tr><td>Same as current evaluation and action for completed tests.</td></tr>
   				<tr><td>Accommodates incomplete eligibility status when testing not performed using FA approved donor screening tests; implemented on October 1, 2011.</td></tr>
   			</table>
   		</div>	
   	</div>
</div>
