<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<%
	String cbuDocCateg = null;
	if(request.getParameter("cbuDocCateg")!=null){
		cbuDocCateg = request.getParameter("cbuDocCateg");
	}else if(request.getAttribute("cbuDocCateg")!=null){
		cbuDocCateg = (String)request.getAttribute("cbuDocCateg");
	}
	request.setAttribute("cbuDocCateg",cbuDocCateg);
	//System.out.println("unitDocCateg.......>>>>>>>>>>>>>.???????????"+ unitDocCateg);
%>
<script>
function revokeDocuments(cdrCbuId,attachmentId,dcmsAttachId,title,url1,lenght,breadth){
	
 	var orderId=$j("#orderId").val();
	var orderType=$j("#orderType").val();	
	var answer = confirm('Please confirm if you want to revoke the document.');
		if (answer){	
						var url='revokeDoc?cbuCordId='+cdrCbuId+'&cordID='+cdrCbuId+'&cordId='+cdrCbuId+'&pkcordId='+cdrCbuId+'&attachmentId='+attachmentId;
						loadMoredivs(url,'DUMNDiv,quicklinksDiv,loadCbuDocInfoDiv,loadEligibilityDocInfoDiv,loadLabSummaryDocInfoDiv,loadProcessingDocInfoDiv,loadHlaDocInfoDiv,loadIdmDocInfoDiv,loadHealthHistroyDocInfoDiv,loadOthrDocInfoDiv,cbuAssmentEligibleDiv','searchOpenOrders2','','');
						alert("Document revoked successfully");
		}
}

function editModifyRevokePer(docDivId){
	var listSize=$j("#size").val();
	for( i=0;i<listSize+1;i++)
	 {
	    $j("#modfy"+docDivId+i).attr('disabled','disabled'); 
	    $j("#revok"+docDivId+i).attr('disabled','disabled'); 
	 }
}
</script>
<s:set name="cbuDocCateg1" value="#request.cbuDocCateg" />
<s:set name="unDocCateg" value="@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE"  scope="request" />
<s:if test="hasViewPermission(#request.viewUploadCat)==true">
<s:if test="(#request.categoryMap[#cbuDocCateg1]!=null && #request.categoryMap[#cbuDocCateg1].size > 0)||(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size > 0)">
	<div id="searchTble1" style="width:100%; height: 100px; overflow: scroll;">
		<s:hidden name="cdrCbuPojo.cordID" id="cdrCbucordID"></s:hidden>	
		<s:hidden name="cdrCbuPojo.registryId" id="registryId"></s:hidden>
		<input type="hidden" id="size" value="<s:property value="#request.categoryMap[#cbuDocCateg1].size"/>"></input>		
		<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="lodedDoc">
			<thead>
				<tr>
					<th class="th2" scope="col"><s:text name="garuda.uploaddoc.label.date" /></th>
					<th><s:text name="garuda.uploaddoc.label.documentuploadedby" /></th>
					<th><s:text name="garuda.uploaddoc.label.documentcategory" /></th>
				    <th><s:text name="garuda.uploaddoc.label.documentsubcategory" /></th>
					<s:if test="(#request.cbuDocCateg !='other_rec_cat')"><th><s:text name="garuda.openorder.label.completiondate" /></th></s:if>
					<s:if test="(#request.cbuDocCateg !='eligibile_cat') && (#request.cbuDocCateg !='hlth_scren')"><th><s:if test="(#request.cbuDocCateg =='hla') ||(#request.cbuDocCateg =='lab_sum_cat') ||(#request.cbuDocCateg =='infect_mark')"><s:text  name="garuda.openorder.label.testdate"/></s:if>
					<s:if test="(#request.cbuDocCateg =='cbu_info_cat') "><s:text  name="garuda.openorder.label.reportdate"/></s:if>
					<s:if test="(#request.cbuDocCateg =='process_info')"><s:text  name="garuda.openorder.label.processingdate"/></s:if>
					<s:if test="(#request.cbuDocCateg =='other_rec_cat')"><s:text  name="garuda.openorder.label.receiveddate"/></s:if>
					</th></s:if>
					<th class="th3" scope="col"><s:text name="garuda.openorder.label.description" /></th>
					<th class="th5" scope="col"><s:text name="garuda.uploaddoc.label.view" /></th>
					<th><s:text name="garuda.common.lable.modify" /></th>
					<th><s:text name="garuda.common.lable.revoke" /></th>
				</tr>
			</thead>
						
			<tbody>		
			<s:iterator value="#request.categoryMap[#cbuDocCateg1]" var="rowVal" status="row">
			<s:set name="cellValue1" value="%{#rowVal[14]}" scope="request"/>
				<tr>
					<td><s:date name="%{#rowVal[10]}" id="uploadDate" format="MMM dd, yyyy / hh:mm:ss a" /><s:property value="%{#uploadDate}" /></td>
					<td>
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>	
					</td>									
					<td><s:property value="getCodeListDescByPk(#rowVal[15])" /></td>
					<td><s:property value="getCodeListDescByPk(#rowVal[12])" /></td>
					<s:if test="(#request.cbuDocCateg !='other_rec_cat')"><td><s:if test="#rowVal[20] != null"><s:date name="%{#rowVal[20]}" id="completionDate" format="MMM dd, yyyy" /><s:property value="%{#completionDate}" /></s:if></td></s:if>
					<s:if test="(#request.cbuDocCateg !='eligibile_cat') && (#request.cbuDocCateg !='hlth_scren')">
                      <td>
					   <s:if test="#rowVal[16] != null"><s:date name="%{#rowVal[16]}" id="reportDate" format="MMM dd, yyyy" /><s:property value="%{#reportDate}" /></s:if>
				       <s:if test="#rowVal[17] != null"><s:date name="%{#rowVal[17]}" id="processDate" format="MMM dd, yyyy" /><s:property value="%{#processDate}" /></s:if>
				       <s:if test="#rowVal[18] != null"><s:date name="%{#rowVal[18]}" id="testDate" format="MMM dd, yyyy" /><s:property value="%{#testDate}" /></s:if>
				       <s:if test="#rowVal[19] != null"><s:date name="%{#rowVal[19]}" id="receivedDate" format="MMM dd, yyyy" /><s:property value="%{#receivedDate}" /></s:if>
				     </td></s:if>
					<td><s:property value="%{#rowVal[13]}" /></td>
					<td><a href="#"><img src="images/attachment-icon.png" 
									onclick="javascript: return showDocument('<s:property value="%{#rowVal[5]}" />')" /></a></td>
					<td>
						<button type="button" id="modfy<s:property value="#request.cbuDocCateg"/><s:property value="%{#row.index}"/>" onclick="javascript:modifyDocument('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="%{#rowVal[3]}"/>','<s:property value="%{#rowVal[5]}" />');"><s:text name="garuda.common.lable.modify" /></button>
			    </td>
					
					<td><button type="button" id="revok<s:property value="#request.cbuDocCateg"/><s:property value="%{#row.index}"/>" onclick="javascript: revokeDocuments('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="%{#rowVal[5]}" />','searchTble1')" ><s:text name="garuda.common.lable.revoke" /></button></td>
				</tr>	
			</s:iterator>
			<s:if test="(#request.cbuDocCateg !='eligibile_cat')&& (#request.cbuDocCateg !='hlth_scren')&& (#request.cbuDocCateg !='other_rec_cat')">
			<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size > 0 && hasViewPermission(#request.viewUnitRepCat)==true">
					<s:iterator value="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]" var="rowVal" status="row">
							<s:set name="cellValue1" value="%{#rowVal[14]}" scope="request"/>
							<tr>
								<td><s:date name="%{#rowVal[10]}" id="uploadDate"
												format="MMM dd, yyyy" /><s:property value="%{#uploadDate}" /></td>
										<td>
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>	
					</td>									
					<td><s:property value="getCodeListDescByPk(#rowVal[15])" /></td>
					<td><s:property value="getCodeListDescByPk(#rowVal[12])" /></td>
							<td>
				       <s:date name="%{#rowVal[20]}" id="completionDate" format="MMM dd, yyyy" /><s:property value="%{#completionDate}" />
				       </td>
				       <td></td>
					<td><s:property value="%{#rowVal[13]}" /></td>
								<td><a href="#"><img src="images/attachment-icon.png"
												onclick="javascript: return showDocument('<s:property value="%{#rowVal[5]}" />')" /></a></td>
							<s:if test="hasEditPermission(#request.viewUnitRepCat)!=true">
							      <td><button type="button" disabled="disabled"><s:text name="garuda.common.lable.modify" /></button></td>
							        <td><button type="button" disabled="disabled"><s:text name="garuda.common.lable.revoke" /></button></td>
							 </s:if>  
							 <s:else>   
								<td><button type="button" id="modify<s:property value="#request.cbuDocCateg"/><s:property value="%{#row.index}"/>"onclick="javascript:modifyDocument('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="%{#rowVal[3]}"/>','<s:property value="%{#rowVal[5]}" />');"><s:text name="garuda.common.lable.modify" /></button></td>
								<td><button type="button" id="revokee<s:property value="#request.cbuDocCateg"/><s:property value="%{#row.index}"/>" onclick="javascript: revokeDocument('<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="%{#rowVal[5]}" />')" ><s:text name="garuda.common.lable.revoke" /></button></td>
							</s:else>
							</tr>
			   </s:iterator>
			</s:if>
			</s:if>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="6"></td>
				</tr>
			</tfoot>
		</table>
	</div>	
</s:if>
</s:if> 	
	 <s:if test="(#request.cbuDocCateg =='process_info')&&(hasEditPermission(#request.viewProcesInfoCat)==false)"> 
	 <input type="hidden" id="doc_div_process_info" value="<s:property value="#request.cbuDocCateg"/>"></input>	
	      <s:if test="hasEditPermission(#request.viewProcesInfoCat)==false">				 
						 <script>
						 var docDivId = $j("#doc_div_process_info").val();
						 editModifyRevokePer(docDivId);	 	 
						</script>
		 </s:if>	
						
	</s:if>
	<s:if test="(#request.cbuDocCateg =='cbu_info_cat')&&(hasEditPermission(#request.viewCbuInfocat)==false)"> 
			<input type="hidden" id="doc_div_cbu_info_cat" value="<s:property value="#request.cbuDocCateg"/>"></input>			 
						 <script>
						 	var docDivId = $j("#doc_div_cbu_info_cat").val();
						 	 editModifyRevokePer(docDivId);	 
						</script>
	</s:if>
	<s:if test="(#request.cbuDocCateg =='hla')&&(hasEditPermission(#request.viewHlaCat)!=true)"> 
			<input type="hidden" id="doc_div_hla" value="<s:property value="#request.cbuDocCateg"/>"></input>			 
						 <script>
						 	 var docDivId = $j("#doc_div_hla").val();
						 	editModifyRevokePer(docDivId);	 	 
						</script>
	</s:if>
		  <s:if test="(#request.cbuDocCateg =='eligibile_cat')&&(hasEditPermission(#request.viewEligbCat)!=true)"> 
				<input type="hidden" id="doc_div_eligibile_cat" value="<s:property value="#request.cbuDocCateg"/>"></input>	 
						 <script>
						 	 var docDivId = $j("#doc_div_eligibile_cat").val();
						 	editModifyRevokePer(docDivId);	  
						</script>
	       </s:if>
	        <s:if test="(#request.cbuDocCateg =='lab_sum_cat')&&(hasEditPermission(#request.viewLabSumCat)!=true)"> 
				<input type="hidden" id="doc_div_lab_sum_cat" value="<s:property value="#request.cbuDocCateg"/>"></input>		
                        <script>
						 	 var docDivId = $j("#doc_div_lab_sum_cat").val();
						 	editModifyRevokePer(docDivId);	  
						</script>
	       </s:if>
	       <s:if test="(#request.cbuDocCateg =='infect_mark')&&(hasEditPermission(#request.viewIDMCat)!=true)"> 
					<input type="hidden" id="doc_div_infect_mark" value="<s:property value="#request.cbuDocCateg"/>"></input>	 
						 <script>
						 	 var docDivId = $j("#doc_div_infect_mark").val();
						 	editModifyRevokePer(docDivId);	 	 
						</script>
	        </s:if>
	        <s:if test="(#request.cbuDocCateg =='hlth_scren')&&(hasEditPermission(#request.viewHealthHisCat)!=true)"> 
					<input type="hidden" id="doc_div_hlth_scren" value="<s:property value="#request.cbuDocCateg"/>"></input>	
						 <script>
						 	 var docDivId = $j("#doc_div_hlth_scren").val();
						 	editModifyRevokePer(docDivId);	 	 
						</script>
	          </s:if>
	                 <s:if test="(#request.cbuDocCateg =='other_rec_cat')&&(hasEditPermission(#request.viewOtherCat)!=true)"> 
					<input type="hidden" id="doc_div_other_rec_cat" value="<s:property value="#request.cbuDocCateg"/>"></input>	
						 <script>
						 	 var docDivId = $j("#doc_div_other_rec_cat").val();
						 	editModifyRevokePer(docDivId);	 	 
						</script>
	          </s:if>