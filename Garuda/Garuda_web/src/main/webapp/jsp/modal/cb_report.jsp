<%@ taglib prefix="s"  uri="/struts-tags"%>
<%
String contextpath=request.getContextPath();
%>
<%@ page import="java.util.List,com.velos.eres.service.util.*"%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/common_custom_methods.js" ></script>

<%@ page import="com.velos.eres.web.grpRights.GrpRightsJB" %>

<%
	HttpSession tSession = request.getSession(true); 
	GrpRightsJB grpRights =null;
	if (tSession.getAttribute("LocalGRights")!=null){
	 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
	}else
	{
	 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	}
	
	int viewCbuSumRep = 0;
	int viewCbuDetailRep = 0;
	int viewCbuCompRep = 0;
	int viewEvalHistoryRep = 0;
	int creatShipPack = 0;
	
	if(grpRights.getFtrRightsByValue("CB_CBUSUM")!=null && !grpRights.getFtrRightsByValue("CB_CBUSUM").equals(""))
	{
		viewCbuSumRep = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUSUM"));
	}
	else
		viewCbuSumRep = 4;
	
	if(grpRights.getFtrRightsByValue("CB_DETAILCBU")!=null && !grpRights.getFtrRightsByValue("CB_DETAILCBU").equals(""))
	{
		viewCbuDetailRep = Integer.parseInt(grpRights.getFtrRightsByValue("CB_DETAILCBU"));
	}
	else
		viewCbuDetailRep = 4;
	
	if(grpRights.getFtrRightsByValue("CB_CBUCOMP")!=null && !grpRights.getFtrRightsByValue("CB_CBUCOMP").equals(""))
	{
		viewCbuCompRep = Integer.parseInt(grpRights.getFtrRightsByValue("CB_CBUCOMP"));
	}
	else
		viewCbuCompRep = 4;
	
	if(grpRights.getFtrRightsByValue("CB_EVALHIST")!=null && !grpRights.getFtrRightsByValue("CB_EVALHIST").equals(""))
	{
		viewEvalHistoryRep = Integer.parseInt(grpRights.getFtrRightsByValue("CB_EVALHIST"));
	
	}else
		viewEvalHistoryRep = 4;
	if(grpRights.getFtrRightsByValue("CB_SHIPMNTREP")!=null && !grpRights.getFtrRightsByValue("CB_SHIPMNTREP").equals(""))
	{
		creatShipPack = Integer.parseInt(grpRights.getFtrRightsByValue("CB_SHIPMNTREP"));
	
	}else
		creatShipPack = 4;
	
	
	request.setAttribute("viewCbuSumRep",viewCbuSumRep);
	request.setAttribute("viewCbuDetailRep",viewCbuDetailRep);
	request.setAttribute("viewCbuCompRep",viewCbuCompRep);
	request.setAttribute("viewEvalHistoryRep",viewEvalHistoryRep);
	request.setAttribute("creatShipPack",creatShipPack);
	
%>
<script type="text/javascript">
function getMailwithAttachment(pkreport,params,repName){
	refreshDiv('cbuPdfReportsAttachment?repId='+pkreport+'&repName='+repName+'&selDate=&params='+params,'TempDiv','reportForm');
	var path=$j('#filepath').val();
	
	if(path!=null && path!=''){
   		window.location="mailto:?attachment="+path;
		
	}
}

function notAllowedAlert(){
	alert("It can not be completed until following tasks are completed \na.Required Clinical Information \nb.Final CBU Review");
}

</script>
<s:form id="reportForm">
<div id="TempDiv" ><input type="hidden" name="filepath" id="filepath" value=<%=session.getAttribute("filePath")%>/></div>

<table class="displaycdr" cellpadding="0" cellspacing="0" border="1" id="report">
<thead>
<tr>
<th><s:text name="garuda.report.label.display"/></th>
<th><s:text name="garuda.report.label.repname"/></th>
<!-- th><s:text name="garuda.report.label.repattach"/></th> -->
</tr>
</thead>
<tbody>

<s:if test="hasViewPermission(#request.viewCbuSumRep)==true">
<tr>

<td align="center">
	<a href=#  onclick="window.open('cbuPdfReports?repId=161&repName=CBU Summary Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:IDM','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><img src="./images/filetype_pdf.png"  align="center" border="0" /> </a>
</td>
<td>
	<s:text name="garuda.report.label.cbuSummaryReport"/>
</td>
<!-- td align="center">
	<img src="./images/Email.gif" border="0" id="summaryImg" onclick="getMailwithAttachment('161','<s:property value="cdrCbuPojo.cordID" />:IDM','CBU Summary Report');"/>
</td> -->

</tr>
</s:if>


<s:if test="hasViewPermission(#request.viewCbuDetailRep)==true">
<tr>

<td align ="center">
	<s:if test="(orderId != '') && (orderId!=null)">
	<a href=#  onclick="window.open('cbuPdfReports?repId=167&repName=Detail CBU Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:<s:property value="orderId" />:MRQ:FMHQ:IDM','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><img src="./images/filetype_pdf.png"  align="center" border="0" /> </a>
	</s:if>
	<s:else>
	<a href=#  onclick="window.open('cbuPdfReports?repId=167&repName=Detail CBU Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:0000:MRQ:FMHQ:IDM','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><img src="./images/filetype_pdf.png"  align="center" border="0" /> </a>
	</s:else>
</td>

<td>
	<s:text name="garuda.report.label.detailreport" />
</td>
<!-- td align="center">
	<s:if test="(orderId != '') && (orderId!=null)">
	<img src="./images/Email.gif" border="0" id="detailImg" onclick="getMailwithAttachment('167','<s:property value="cdrCbuPojo.cordID" />:<s:property value="orderId" />:MRQ:FMHQ:IDM','Detail CBU Report');"/>
	</s:if>
	<s:else>
	<img src="./images/Email.gif" border="0" id="detailImg" onclick="getMailwithAttachment('167','<s:property value="cdrCbuPojo.cordID" />:0000:MRQ:FMHQ:IDM','Detail CBU Report');"/>
	</s:else>
</td>  -->

</tr>
</s:if>


<s:if test="hasViewPermission(#request.viewCbuCompRep)==true">
<tr>

<td align="center">

		<s:if test="(orderId != '') && (orderId!=null)">
		<a href=#  onclick="window.open('cbuPdfReports?repId=168&repName=CBU Comprehensive Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:<s:property value="orderId" />:MRQ:FMHQ:IDM','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><img src="./images/filetype_pdf.png"  align="center" border="0" /> </a>
		</s:if>
		<s:else>
		<a href=#  onclick="window.open('cbuPdfReports?repId=168&repName=CBU Comprehensive Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:0000:MRQ:FMHQ:IDM','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><img src="./images/filetype_pdf.png"  align="center" border="0" /> </a>
		</s:else>

</td>
<td>
		<s:text name="garuda.report.label.comprehensivereport" />
</td>
<!-- td align="center">
		<s:if test="(orderId != '') && (orderId!=null)">
		<img src="./images/Email.gif" border="0" id="detailImg" onclick="getMailwithAttachment('168','<s:property value="cdrCbuPojo.cordID" />:<s:property value="orderId" />:MRQ:FMHQ:IDM','CBU Comprehensive Report');"/>
		</s:if>
		<s:else>
		<img src="./images/Email.gif" border="0" id="detailImg" onclick="getMailwithAttachment('168','<s:property value="cdrCbuPojo.cordID" />:0000:MRQ:FMHQ:IDM','CBU Comprehensive Report');"/>
		</s:else>
</td>  -->

</tr>
</s:if>
<s:if test="hasViewPermission(#request.viewEvalHistoryRep)==true">
<tr>

<td align="center">
		<s:if test="(orderId != '') && (orderId!=null)">
		<a href=#  onclick="window.open('cbuPdfReports?repId=177&repName=CBU Evaluation&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:<s:property value="orderId" />','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><img src="./images/filetype_pdf.png"  align="center" border="0" /> </a>
		</s:if>
		<s:else>
		<a href=#  onclick="window.open('cbuPdfReports?repId=177&repName=CBU Evaluation&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:0000','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><img src="./images/filetype_pdf.png"  align="center" border="0" /> </a>
		</s:else>
</td>
<td>
		<s:text name="garuda.report.label.evalutionreport" />
</td>
<!-- td align="center">
		<img src="./images/Email.gif" border="0" onclick="getMailwithAttachment('177','<s:property value="cdrCbuPojo.cordID" />','CBU Evaluation');" />
</td> -->
</tr>
</s:if>
<s:if test="hasViewPermission(#request.creatShipPack)==true">
 <s:if test='cbuFinalReviewPojo.finalReviewConfirmStatus.equals("Y")'>
<tr>
<td align="center">
		<a href=#  onclick="$j('#genpackageslipbar').addClass('progressbarselected');window.open('productInsert?cordID=<s:property value="cdrCbuPojo.cordID" />&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>&repId=172&repName=Infectious Disease Markers Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:IDM','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><img src="./images/filetype_pdf.png"  align="center" border="0" /> </a>
	<%-- 
	<s:else> 
		<a href=#  onclick="javascript:notAllowedAlert();"><img src="./images/filetype_pdf.png"  align="center" border="0" /> </a>
	</s:else>  --%>
</td>
<td>
		<s:text name="garuda.productinsert.label.product_productinsert" />
</td>
<!-- <td align="center">
		<img src="./images/Email.gif" border="0" /> 
</td> -->
</tr>
</s:if></s:if>
</tbody>
</table>
</s:form>	