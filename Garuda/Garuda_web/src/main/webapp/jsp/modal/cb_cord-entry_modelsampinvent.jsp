<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<jsp:include page="../cb_track_session_logging.jsp" />
<script>
<%-- 
Extends jQyery to add method initVal()
* This method return Default Value of the Field
* This will work only for text field
--%>
jQuery.fn.extend({
    initVal: function () { 
     var returnVal;
	 var elementId=$j(this).attr('id');		    
	     try{
    	   returnVal=document.getElementById(elementId).defaultValue;
	    }catch(e){
	     window.console.warn('Some Exception Occur');
	     window.console.error(e);
	    }		     
        return returnVal;
    }
});
function loadDivWithFormSubmitDefer(formid,url,divname,updateDivId,statusDivId){
	
		/*var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
		var processingSaveDialog = document.createElement('div');
		processingSaveDialog.innerHTML=progressMsg;		
		$j(processingSaveDialog).dialog({autoOpen: false,
			resizable: false, width:400, height:90,closeOnEscape: false,
			modal: true}).siblings('.ui-dialog-titlebar').remove();		
		$j(processingSaveDialog).dialog("open");		
		*/
		showprogressMgs();
			$j.ajax({
		        type: "POST",
		        url: url,
		       // async:false,
		        data : $j("#"+formid).serialize(),
		        success: function (result){
		        	$j('.ui-datepicker').html("");
		        	//$('.tabledisplay').html("");
		            var $response=$j(result);
		            //query the jq object for the values
		            var errorpage = $response.find('#errorForm').html();
		           // alert(oneval);
		           // var subval = $response.find('#sub').text();
		            if(errorpage != null &&  errorpage != "" ){
		            	$j('#main').html(result);
		            }else{
		            	if(divname!=""){ 
			            	 $j("#"+divname).html(result);
			            }
		            	if(updateDivId!=undefined && updateDivId!=""){
		            		$j("#"+updateDivId).css('display','block');			            	
		            	}else{
		            		$j("#update").css('display','block');			            	
		            	}
		            	if(statusDivId!=undefined && statusDivId!=""){
		            		$j("#"+statusDivId).css('display','none');			            	
		            	}else{
		            		$j("#status").css('display','none');			            	
		            	}		            	
		            }
		        },
		        error: function (request, status, error) {
		        	closeprogressMsg();
		        	alert("Error " + error);
		            alert(request.responseText);
		        }

			});	
			closeprogressMsg();
   
	
}
//numeric only
jQuery.fn.forceNumericOnly =
function()
{
    return this.each(function()
    {
        $j(this).keydown(function(e)
        {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            return (
                key == 8 || 
                key == 9 ||
                key == 46 ||
                (key >= 37 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};

$j(function(){
	$j(".positive").forceNumericOnly();
		  //$j(".positive").numeric({ negative: false }, function() { alert("No negative values"); this.value = ""; this.focus(); });
		 var validator = $j("#formprocessinfa").validate({
				
			   rules:{	
							"entitySamplesPojo.noSegAvail":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},
							"entitySamplesPojo.filtPap":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},
							"entitySamplesPojo.rbcPel":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},
							"entitySamplesPojo.extDnaAli":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},
							"entitySamplesPojo.nonViaAli":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},
							"entitySamplesPojo.noSerAli":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},
							"entitySamplesPojo.noPlasAli":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},
							"entitySamplesPojo.viaCelAli":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},
							"entitySamplesPojo.totCbuAli":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},
							"entitySamplesPojo.extDnaMat":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},	
							"entitySamplesPojo.celMatAli":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},	
							"entitySamplesPojo.serMatAli":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},	
							"entitySamplesPojo.plasMatAli":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},
							"entitySamplesPojo.totMatAli":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},
							"entitySamplesPojo.cbuOthRepConFin":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},
							"entitySamplesPojo.cbuRepAltCon":{required:{
                                depends: function(element){
                                return (($j(element).initVal()==$j(element).val()))?false:true;
                             }}},
							"entitySamplesPojo.noMiscMat":{required:{
                                 depends: function(element){
                                 return (($j(element).initVal()==$j(element).val()))?false:true;
                              }}}

		             },
			 messages:{	
							"entitySamplesPojo.noSegAvail":"<s:text name="garuda.pf.procedure.noofsegments"/>",
							"entitySamplesPojo.filtPap":"<s:text name="garuda.cbu.samples.filterpaper"/>",
							"entitySamplesPojo.rbcPel":"<s:text name="garuda.cbu.samples.rbcpallets"/>",
							"entitySamplesPojo.extDnaAli":"<s:text name="garuda.cbu.samples.numextracteddna"/>",
							"entitySamplesPojo.nonViaAli":"<s:text name="garuda.cbu.samples.numnonviable"/>",
							"entitySamplesPojo.noSerAli":"<s:text name="garuda.cbu.samples.numserum"/>",
							"entitySamplesPojo.noPlasAli":"<s:text name="garuda.cbu.samples.numplasma"/>",
							"entitySamplesPojo.viaCelAli":"<s:text name="garuda.cbu.samples.numviable"/>",
							"entitySamplesPojo.totCbuAli":"<s:text name="garuda.cbu.samples.totcbuali"/>",
							"entitySamplesPojo.extDnaMat":"<s:text name="garuda.cbu.samples.matextracteddna"/>",
							"entitySamplesPojo.celMatAli":"<s:text name="garuda.cbu.samples.cellmaternal"/>",
							"entitySamplesPojo.serMatAli":"<s:text name="garuda.cbu.samples.nummatserum"/>",
							"entitySamplesPojo.plasMatAli":"<s:text name="garuda.cbu.samples.nummatplasma"/>",
							"entitySamplesPojo.totMatAli":"<s:text name="garuda.pf.procedure.frozencontainer"/>",
							"entitySamplesPojo.cbuOthRepConFin":"<s:text name="garuda.pf.procedure.numfinalproduct"/>",
							"entitySamplesPojo.cbuRepAltCon":"<s:text name="garuda.pf.procedure.numaltcond"/>",
							"entitySamplesPojo.noMiscMat":"<s:text name="garuda.cbu.samples.nummiscmat"/>"

					   }
                  });		
				 
		   });


function savefForSampleSearching(autoDeferSubmit){
	var procallChanges = divChanges('procChanges',$j('#proc_bodyDiv').html());
	
	    //alert("orderId:"+$j("#orderId").val()+" orderType:"+$j("#orderType").val()+" pkcordId:"+$j("#pkcordId").val());
		  var url='saveProcessInfo?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val()+"&"+autoDeferSubmit;
			          if($j("#formprocessinfa").valid())
				      {
					      if(procallChanges){
					      if($j("#orderId").val()!=null && $j("#orderId").val()!='' && $j("#orderId").val()!='undefined'){
								loadMoredivs('saveProcessInfoFromPF?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+"&"+autoDeferSubmit,'dummyDiv','formprocessinfa','update','status');
						  }else{
							  modalFormSubmitRefreshDiv('formprocessinfa',url,'','','','1','<s:property value="cdrCbuPojo.cordID"/>');
							  refreshMultipleDivsOnViewClinical('32','<s:property value="cdrCbuPojo.cordID"/>');
							  if($j('#fcrFlag').val() == "Y"){
									refreshMultipleDivsOnViewClinical('2','<s:property value="cdrCbuPojo.cordID"/>');
									}
							  }
					          $j("#status1").css('display','none');
					          $j('#fromViewClinicalProc2').val('1');
					          
					  }
					      else{
					    	  showSuccessdiv('update','proc_bodyDiv');
						      }
				      }
 }
function segAutoDefer(val,Id){
	//onchange="segAutoDefer(this.value,this.id);"
	elementValue = autoDeferField_1;
	 var url='saveProcessInfo?orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val();
	if (val !='' && (val == 0 || val == 1))
	{
		jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
				function(r) {
					if (r == false) {
					   $j("#"+Id).val(elementValue);
					   $j("#"+Id).focus();
					  }else if(r == true){
						  autoDefer = true;
						  mutex_Lock = true;
						  element_Id = "";
						  element_Name = "entitySamplesPojo.noSegAvail";
						  autoDeferFormSubmitParam[0]= 'formprocessinfa';
						  autoDeferFormSubmitParam[1]= url;
						  autoDeferFormSubmitParam[2]= '';
						  autoDeferFormSubmitParam[3]= '';
						  refresh_DivNo = 1056;
						  refreshDiv_Id[0]= 'defer';
						  refreshDiv_Id[1]= 'status1';
						  commonMethodForSaveAutoDefer();						  
						  //loadDivWithFormSubmitDefer('formprocessinfa',url,'');
						 // refreshMultipleDivsOnViewClinical('1056','<s:property value="cdrCbuPojo.cordID"/>');
						  //$j("#defer").css('display','block');
						  //$j("#status1").css('display','none');	
						  }
					});
	}
}
$j(function(){
	autoDeferField_1 = $j('input[name="entitySamplesPojo.noSegAvail"]').val();
	  });
</script>

 <div id="procandcountparent"> 
 
   
 <s:form id="formprocessinfa">
 		<s:hidden name="fromViewClinicalProc2" id="fromViewClinicalProc2"></s:hidden>
 		<s:hidden name="cdrCbuPojo.cordID" id="pkcordId"></s:hidden>
		<s:hidden name="entitySamplesPojo.entityId"></s:hidden>
		<s:hidden name="entitySamplesPojo.entityType"></s:hidden>
		<s:hidden name="entitySamplesPojo.pkEntitySamples"></s:hidden>
		<s:hidden name="orderId" id="orderId"></s:hidden>
		<s:hidden name="orderType" id="orderType"></s:hidden>
 
      			<div id="update" style="display: none;" >
						<jsp:include page="../cb_update.jsp">
						     <jsp:param value="garuda.common.lable.cbumatSample"  name="message" />
				        </jsp:include>	   
				</div>
				
				<div id="defer" style="display: none;" >
						     
				</div>
				<div id="proc_bodyDiv">
							
   	<table id="status">
       	
    <s:if test="entitySamplesPojo.convFiltPap!=null || entitySamplesPojo.convRbcPel!=null || entitySamplesPojo.convExtDnaAli!=null || entitySamplesPojo.convNoSerAli!=null ||
     entitySamplesPojo.convNoPlasAli!=null || entitySamplesPojo.convNonViaAli!=null || entitySamplesPojo.convViaCelAli!=null || entitySamplesPojo.convNoSegAvail!=null || 
     entitySamplesPojo.convSerMatAli!=null || entitySamplesPojo.convPlasMatAli!=null || entitySamplesPojo.convExtDnaMat!=null || entitySamplesPojo.convNoMiscMat!=null ">
      	<tr>
			<td>
			
				<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<s:text name="garuda.cbbprocedures.label.sampInvent" />
				  </div>			
			
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
																										 
							<tr>
								<td><s:text name="garuda.cordentry.label.filtPap"></s:text>:</td>																									    
								<td><s:textfield name="entitySamplesPojo.convFiltPap" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
								<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.rbcPel"></s:text>:</td>																									     
								<td><s:textfield name="entitySamplesPojo.convRbcPel" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
							</tr>
							<tr>
							   <td><s:text name="garuda.cordentry.label.extDnaAli"></s:text>:</td>																									    
							   <td><s:textfield name="entitySamplesPojo.convExtDnaAli" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
							   <td style="padding-left:10px;" onmouseover="return overlib('<s:text name='garuda.cbbprocedure.label.numberSerumToolTip'/>');" 
								   onmouseout="return nd();"><s:text name="garuda.cordentry.label.noSerAli"></s:text>:</td>																									  
							   <td><s:textfield name="entitySamplesPojo.convNoSerAli" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
					       </tr>
						   <tr>
							   <td><s:text name="garuda.cordentry.label.noPlasAli"></s:text>:</td>																									   
							   <td><s:textfield name="entitySamplesPojo.convNoPlasAli" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
							   <td style="padding-left:10px;"><s:text name="garuda.cordentry.label.nonViaAli"></s:text>:</td>																									   
							   <td><s:textfield name="entitySamplesPojo.convNonViaAli" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
						   </tr>
					       <tr>
							  <td><s:text name="garuda.cordentry.label.convViaCelAli"></s:text>:</td>																									    
							  <td><s:textfield name="entitySamplesPojo.convViaCelAli" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
							  <td style="padding-left:10px;"><s:text name="garuda.cordentry.label.segAvail"></s:text>:</td>
							  <td><s:textfield name="entitySamplesPojo.convNoSegAvail" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
						  </tr>
						  
						  <tr>
						  		<td><s:text name="garuda.cordentry.label.convSerMatAli"></s:text>:</td>																									 
								<td><s:textfield name="entitySamplesPojo.convSerMatAli" cssClass="positive" maxlength="2" disabled="true"> </s:textfield></td>
								<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.plasMatAli"></s:text>:</td>	
								<td><s:textfield name="entitySamplesPojo.convPlasMatAli" cssClass="positive" maxlength="2" disabled="true"> </s:textfield></td>
								
						  </tr>
					      <tr>
								<td><s:text name="garuda.cordentry.label.extDnaMat"></s:text>:</td>	
								<td><s:textfield name="entitySamplesPojo.convExtDnaMat" cssClass="positive" maxlength="2" disabled="true"> </s:textfield></td>
								
								<td style="padding-left:10px;" onmouseover="return overlib('<s:text name='garuda.cbbprocedure.label.numberMisMatToolTip'/>');" 
										onmouseout="return nd();"><s:text name="garuda.cbbprocedures.label.noMiscMat"></s:text>:</td>	
								<td><s:textfield name="entitySamplesPojo.convNoMiscMat" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
								
					     </tr>
					 </table>
				  </td>
			   </tr>
    </s:if>	
		 
		<tr>
			<td>
				<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<s:text name="garuda.cbbprocedures.label.cbusample" />
				  </div>			
			
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
																										 
							<tr>
								<td><s:text name="garuda.cordentry.label.filtPap"></s:text>:</td>																									     <td>
								<td><s:textfield name="entitySamplesPojo.filtPap" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.filtPap}','procChanges');" maxlength="2"></s:textfield></td>
																											  
								<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.rbcPel"></s:text>:</td>																									     <td>
								<td><s:textfield name="entitySamplesPojo.rbcPel" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.rbcPel}','procChanges');" maxlength="2"></s:textfield></td>
							</tr>
							<tr>
							   <td ><s:text name="garuda.cordentry.label.extDnaAli"></s:text>:</td>																									     <td>
							   <td><s:textfield name="entitySamplesPojo.extDnaAli" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.extDnaAli}','procChanges');" maxlength="2"></s:textfield></td>
							   <td style="padding-left:10px;" onmouseover="return overlib('<s:text name='garuda.cbbprocedure.label.numberSerumToolTip'/>');" 
								   onmouseout="return nd();"><s:text name="garuda.cordentry.label.noSerAli"></s:text>:</td>																									     <td>
							   <td><s:textfield name="entitySamplesPojo.noSerAli" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.noSerAli}','procChanges');" maxlength="2"></s:textfield></td>
					       </tr>
						   <tr>
							   <td><s:text name="garuda.cordentry.label.noPlasAli"></s:text>:</td>																									     <td>
							   <td><s:textfield name="entitySamplesPojo.noPlasAli" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.noPlasAli}','procChanges');" maxlength="2"></s:textfield></td>
							   <td style="padding-left:10px;"><s:text name="garuda.cordentry.label.nonViaAli"></s:text>:</td>																									     <td>
							   <td><s:textfield name="entitySamplesPojo.nonViaAli" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.nonViaAli}','procChanges');" maxlength="2"></s:textfield></td>
						   </tr>
					       <tr>
							  <td><s:text name="garuda.cordentry.label.viaCelAli"></s:text>:</td>																									     <td>
							  <td><s:textfield name="entitySamplesPojo.viaCelAli" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.viaCelAli}','procChanges');" maxlength="2"></s:textfield></td>
							  <td></td>
							  <td></td>
						  </tr>
						  <tr>
					         <td>
						        <b><s:text name="garuda.cbbprocedures.label.aliqrepoffinalprod"></s:text></b>
					         </td>
				           </tr>	
						  
						  <tr>
						  		<td><s:text name="garuda.cordentry.label.segAvail"></s:text>:</td>																									     <td>
								<td><s:textfield name="entitySamplesPojo.noSegAvail" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.noSegAvail}','procChanges');" maxlength="2"></s:textfield></td>
						  </tr>
					      <tr>
								<td><s:text name="garuda.cordentry.label.cbuOthRepConFin"></s:text>:</td>	
								<td></td>
								<td><s:textfield name="entitySamplesPojo.cbuOthRepConFin" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.cbuOthRepConFin}','procChanges');" maxlength="2"></s:textfield></td>
								<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.cbuRepAltCon"></s:text>:</td>																									     <td>
								<td><s:textfield name="entitySamplesPojo.cbuRepAltCon" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.cbuRepAltCon}','procChanges');" maxlength="2"></s:textfield></td>
								<td></td>
					     </tr>
					 </table>
				  </td>
			   </tr>
			
                   <tr>
                     <td>
				    	 <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
							<s:text name="garuda.cbbprocedures.label.matsample" />
						  </div>
				     </td>
				    </tr>
						<tr>        
						    <td>	
								<table>
								     <tr>
										<td><s:text name="garuda.cordentry.label.serMatAli"></s:text>:</td>
										<td><s:textfield name="entitySamplesPojo.serMatAli" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.serMatAli}','procChanges');" maxlength="2"></s:textfield></td>
										<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.plasMatAli"></s:text>:</td>	
										<td><s:textfield name="entitySamplesPojo.plasMatAli" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.plasMatAli}','procChanges');" maxlength="2"></s:textfield></td>
									 </tr>
								     <tr>
										<td><s:text name="garuda.cordentry.label.extDnaMat"></s:text>:</td>	
										<td><s:textfield name="entitySamplesPojo.extDnaMat" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.extDnaMat}','procChanges');" maxlength="2"></s:textfield></td>
										<td style="padding-left:10px;" onmouseover="return overlib('<s:text name='garuda.cbbprocedure.label.numberMisMatToolTip'/>');" 
										onmouseout="return nd();"><s:text name="garuda.cbbprocedures.label.noMiscMat"></s:text>:</td>	
										<td><s:textfield name="entitySamplesPojo.noMiscMat" cssClass="positive" onchange="isChangeDone(this,'%{entitySamplesPojo.noMiscMat}','procChanges');" maxlength="2"></s:textfield></td>
									 </tr>
			            		</table>
			            	</td>
			             </tr>	  					    
	    </table>	 
	    					    
		  					    
		  					      <table id="status1" cellpadding="0" cellspacing="0" width="100%">
		            					  <tr bgcolor="#cccccc">
		            						  <td colspan="2" align="right"><jsp:include page="../cb_esignature.jsp" flush="true">
		            						  <jsp:param value="samplesubmit" name="submitId"/>
				                              <jsp:param value="sampleinvalid" name="invalid" />
			                                  <jsp:param value="sampleminimum" name="minimum" />
				                              <jsp:param value="samplepass" name="pass" />
		            						  </jsp:include></td>
												 <td colspan="2" align="left">
													<button type="button"  disabled="disabled"  id="samplesubmit" onclick="savefForSampleSearching('submitButtonAutoDefer=false')"><s:text name="garuda.unitreport.label.button.submit"/></button>
													<button type="button" onclick="closeModal();"><s:text
													  name="garuda.unitreport.label.button.cancel"></s:text></button>		
												 </td>  	   
								           </tr>         
		            			 </table>
							  
		</div> 					  
				
		
	</s:form>	
	</div>