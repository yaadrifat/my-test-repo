<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<script>
$j(function(){
	   $j("#searchResults1").dataTable();
	   var tdObj = $j('#searchResults1').find('tbody').find("tr:eq(0)").find("td:eq(0)");
		if($j(tdObj).hasClass('dataTables_empty')){
			$j('#searchResults1_info').hide();
			$j('#searchResults1_paginate').hide();
		}
});

function openUpdateAlreadySubmittedCord(registryId){
	$j("#cbuid").val(registryId);
	$j("#lookupmenu").submit();
}

function checkCordSubmiited(cordId,registryId){
	var cordSubmit = true;
	var url1 = "getValidateDbData?entityName=CdrCbu&entityField=cordID&entityValue="+cordId+"&entityField1=cordSearchable&entityValue1=1";
	$j.ajax({
		type : "GET",
		async : false,
		url : url1,
		success : function(result) {
		    cordSubmit = result.dbValue;
		    if(cordSubmit==false){
		    	openUpdateAlreadySubmittedCord(registryId);
			}else{
				submitpost('openWholeCordEntry',{'cdrCbuPojo.cordID':cordId});
			}
		}			
	});      		
}

function showModalOneImportHistory(title,url,height,width,regid)
{
	var regIdSerialize = $j("#"+regid).serialize();
	var regId = "";
	if(regIdSerialize!="" && regIdSerialize.indexOf("=")!=-1){
		var index = regIdSerialize.indexOf("=");
		index++;
		regId = regIdSerialize.substring(index,regIdSerialize.length);		
	}
	if(regId!="")
	   url = url + "registryId="+regId;	
	$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#modelPopup2").html(progressMsg);
		$j("#modelPopup2").dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: true,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#modelPopup2").dialog("destroy");
				    }
				   }
				  ); 
		$j("#modelPopup2").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#modelPopup2").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
}
</script>
       <div class="column">
		        <div class="portlet" id="cbuimporterrormessagesparent" >
					<div id="cbuimporterrormessages" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
						<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
						<span class="ui-icon ui-icon-minusthick"></span>					
						<s:text name="garuda.cordimport.label.cbuimporterrormessages" />
						</div>
						<div class="portlet-content">
						   <table width="100%" cellpadding="0" cellspacing="0" border="0" class="displaycdr searchResults" id="searchResults1">
						      <thead>
						         <tr>
						             <th><s:text name="garuda.cordimport.label.msgid"/></th>
						             <th><s:text name="garuda.entry.label.cbuid"/></th>
						             <th><s:text name="garuda.cordimport.label.msgtype"/></th>
						             <th><s:text name="garuda.cordimport.label.msgtext"/></th>			
						             <s:if test="#request.level==@com.velos.ordercomponent.util.VelosGarudaConstants@MESSAGE_LEVEL_ZERO">
						               <th><s:text name="garuda.cordimport.label.msgpercent"/></th> 
						             </s:if>			             				             
						         </tr>
						      </thead>
						      <tbody>
						          <s:iterator value="#request.listCordImportErrorMessages" status="row">
						            <tr>
						               <td>
						                  <s:property value="messageId"/>
						               </td>
						               <td>
						                  <s:property value="cbuRegistryId"/>
						               </td>
						               <td>
						                  <s:property value="messageType"/>
						               </td>
						               <td>
						                  <s:property value="messageText"/>
						               </td>
						               <s:if test="#request.level==@com.velos.ordercomponent.util.VelosGarudaConstants@MESSAGE_LEVEL_ZERO">
						                 <td>
						                    <s:if test="messageType==@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_IMPORT_INFORMATION && (submitted==null || submitted==false)">
						                        <a href="#" onclick="checkCordSubmiited('<s:property value="fkCordId"/>','<s:property value="cbuRegistryId"/>')"><s:property value="cordImportProgress"/></a>
						                    </s:if>
						                    <s:elseif test="messageType==@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_IMPORT_INFORMATION && (submitted!=null || submitted==true)">
						                        <a onclick="openUpdateAlreadySubmittedCord('<s:property value="cbuRegistryId"/>')" href="#"><s:property value="cordImportProgress"/></a>
						                    </s:elseif>
						                    <s:else>
						                        <input type="hidden" value="<s:property value='cbuRegistryId'/>" name="registryId<s:property value='%{#row.index}'/>" id="registryId<s:property value='%{#row.index}'/>" />
						                        <a href="#" onclick="showModalOneImportHistory('Cord Import Error History','getCordImportHistoryErrorMessages?historyPojo.pkCordimportHistory=<s:property value="fkCordImportHistory"/>&level=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@MESSAGE_LEVEL_ONE"/>&','700','700','registryId<s:property value="%{#row.index}"/>')"><s:text name="garuda.cordimport.label.viewerrordetails"/></a>
						                    </s:else>						                    
						                 </td>
						               </s:if>
						             </tr>
						          </s:iterator>
						      </tbody>
						   </table>
						</div>
			         </div>
			   </div>	 
	  </div> 