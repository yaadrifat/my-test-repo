<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<script type="text/javascript">
function validateaddititypingAvail(){
	var hlatypingavail=$j('input:radio[name=HlaTypingAvail]:checked').val();
	if(hlatypingavail==null || hlatypingavail==''){
		$j("#erroraddititypingavailDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.cbu.cord.addititypingAvailable"/>");
		 });
		$j("#erroraddititypingavailDiv").show();
		return false;
	}
	
	else{
		$j("#erroraddititypingavailDiv").find("span").each(function(){
      		$j(this).text("");
	 });
		$j("#erroraddititypingavailDiv").hide();
	
		return true;
	}
}
function updateAdditiTyping(){
	if(validateaddititypingAvail()){
	setEmptyOrders();
	var hlatypingflag=$j('input:radio[name=HlaTypingAvail]:checked').val();
	var historyArr=new Array();
	var i=0; 
		$j('.detailhstry').each(function(){
		if($j(this).is(':visible')){
			var idval=$j(this).attr("id");
			historyArr[i]=idval;
			i++;
		}
	}); 
	setTimeout(function(){
		loadMoredivs('updateAdditiTyping?cordId='+$j("#pkcordId").val()+'&pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&HlaTypingAvail='+hlatypingflag+'&sampleAtLab='+$j("#sampleAtLab").val(),'currentRequestProgress,additityingandtestcontentDiv,taskCompleteFlagsDiv,chistory,cReqhistory','ctOrderDetailsForm','updateAdditiHlamodal','statusAdditiHlamodal');
		setpfprogressbar();
		getprogressbarcolor();	
		historyTblOnLoad();
	},0);
	currenthlatbl_onload();
	hlatblOther_onload()
	setTimeout(function(){
		$j.each(historyArr,function(i,val){
			var Index=val.charAt(val.length-1);
			$j("#icondiv"+Index).triggerHandler('click');
		});
	},0);
	
	}
}
</script>
<s:form name="AdditionalHlaTypingmodalform">
<div id="updateAdditiHlamodal" style="display: none;">
	<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.common.lable.addititypingavail" /></strong></p>
			</div>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td>
			<input type="button" onclick="closeModal()" value="<s:text name="garuda.common.close"/>" />
		</td>
	</tr>
	</table>
</div>
<div id="statusAdditiHlamodal">
	<div class="portlet-content" id="additityingandtestcontentDiv">
	<s:hidden name="hlaLoadedflag" id="hlaLoadedflag" value="0"></s:hidden>
	<table width="100%">
		<tr>
			<td width="50%"><strong><s:text
				name="garuda.addTyping&Testing.label.recenthlatypavail" /></strong><span
				style="color: red;">*</span></td>
			<td width="50%">
				<s:if test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
				<s:iterator value="ctOrderDetailsList" var="rowVal">
					<s:if test="%{#rowVal[59]!=null && #rowVal[59]!=''}">
						<s:radio name="HlaTypingAvail" id="HlaTypingAvail"
							list="#{'Y':'Yes','N':'No'}" value="%{#rowVal[59]}"
							onclick="openModalforHlaTyping('button')" />
					</s:if>
					<s:else>
						<s:radio name="HlaTypingAvail" id="HlaTypingAvail"
							list="#{'Y':'Yes','N':'No'}"
							onclick="openModalforHlaTyping('button')" />
					</s:else>
				</s:iterator>
			</s:if>
			<div id="erroraddititypingavailDiv"><span
					id="addititypingAvailConfirmlbl" class="error"></span></div>		
			</td>
		</tr>
		<tr id="ctAdditiTypingsubmitDiv">
			<td width="100%" colspan="2">
				<!-- esign for additioal tying -->
				<table bgcolor="#cccccc" class="tabledisplay disable_esign" width="100%">
					<tr bgcolor="#cccccc" valign="baseline">
						<td width="70%"><jsp:include
							page="../cb_esignature.jsp" flush="true">
							<jsp:param value="submitAdditiTyping" name="submitId" />
							<jsp:param value="invalidAdditiTyping" name="invalid" />
							<jsp:param value="minimumAdditiTyping" name="minimum" />
							<jsp:param value="passAdditiTyping" name="pass" />
						</jsp:include></td>
						<td align="left" width="30%"><input type="button"
							id="submitAdditiTyping" onclick='updateAdditiTyping();'
							value="<s:text name="garuda.unitreport.label.button.submit"/>"
							disabled="disabled" /><input type="button"  onclick="closeModal()" value="<s:text name="garuda.unitreport.label.button.cancel"/>" /></td>
					</tr>
				</table>
			<!-- esign for additional typing -->
			</td>
		</tr>
	</table>
	</div>
	</div>
</s:form>