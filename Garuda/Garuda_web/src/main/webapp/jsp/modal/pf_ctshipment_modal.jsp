<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<%
String contextpath=request.getContextPath();
%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<script type="text/javascript">

function updateCtshipment(flag){
	if(switchwf_flag==0){
		var historyArr=new Array();
		var i=0; 
 		$j('.detailhstry').each(function(){
			if($j(this).is(':visible')){
				var idval=$j(this).attr("id");
				historyArr[i]=idval;
				i++;
			}
		}); 
		setTimeout(function(){
			loadMoredivs('updateCtshipment?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+
					'&schshipdate='+$j("#ctModalShipDateId").val()+'&shipcompany='+$j("#shipCompanyModal").val()+'&trackNo='+encodeURIComponent($j("#trackNoModal").val())+'&samtype='+$j("#modalSampleTypeAvailable").val()+'&aliqtype='+$j("#AliquotsTypeModal").val()+'&shipmentType='+$j("#pkCbuShipment").val()+'&labCode='+$j("#labCodeModal").val()+'&prevSampleType='+$j("#prevSampleType").val()+'&prevAliqType='+$j("#prevAliqType").val()+'&novalidate='+flag+'&pkPackingSlip='+$j("#pkPackingSlipId").val(),'currentRequestProgress,quicklinksDiv,taskCompleteFlagsDiv,chistory,cReqhistory,ctShipmentInfoContentDiv','ctshipmentmodalform','updatectshipmentmodal','statusctshipmentmodal');
			if($j("#ctShipDateId").val()!=null && $j("#ctShipDateId").val()!='' && $j("#ctShipDateId").val()!='undefined'){
				getDayfromshipdate($j("#ctShipDateId").val(),'ctshimentdayDiv');
			}
			getAliquotsType($j("#SamTypeAvailable").val());
			setpfprogressbar();
			getprogressbarcolor();
			historyTblOnLoad();
			},0);
		setTimeout(function(){
			//alert("historyArr::length"+historyArr.length)
			$j.each(historyArr,function(i,val){
				var Index=val.charAt(val.length-1);
				$j("#icondiv"+Index).triggerHandler('click');
			});
		},0);
	}else if(switchwf_flag==1){
		var historyArr=new Array();
		var i=0; 
 		$j('.detailhstry').each(function(){
			if($j(this).is(':visible')){
				var idval=$j(this).attr("id");
				historyArr[i]=idval;
				i++;
			}
		});
		setTimeout(function(){
			loadMoredivs('updateCtshipment?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+
					'&switchWfFlag='+switchwf_flag+'&shipmentType='+$j("#pkCbuShipment").val()+'&prevSampleType='+$j("#prevSampleType").val()+'&prevAliqType='+$j("#prevAliqType").val(),'currentRequestProgress,ctShipmentInfoContentDiv','ctshipmentmodalform','updatectshipmentmodal','statusctshipmentmodal');
	},0);
	getAliquotsType($j("#modalSampleTypeAvailable").val());
	setTimeout(function(){
		//alert("historyArr::length"+historyArr.length)
		$j.each(historyArr,function(i,val){
			var Index=val.charAt(val.length-1);
			$j("#icondiv"+Index).triggerHandler('click');
		});
	},0);
	}
}

function ctShipmentValidity(){
		if(validateCtShipment()){
			updateCtshipment();
		}
}

function getSampleCount(aliquotType){
	if(aliquotType!=null && aliquotType!=''){
		loadMoredivs('getCodelstCustCol?pkCodelst='+aliquotType+'&cdrCbuPojo.cordID='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val(),'sampleInventoryModalDiv','ctshipmentmodalform');
	}
	if(aliquotType!=null && aliquotType!='' && $j("#sampleCountId").val()!=null && $j("#sampleCountId").val()!="" && $j("#sampleCountId").val()<=0){
		alert('<s:text name="garuda.cbu.message.sampleNotExist"/>');
	}
	
}
function getAliquotsType(sampleType){
	var var_pkaliquots='<s:property value="pkaliquots"/>';
	if(var_pkaliquots!=null && var_pkaliquots!="" && sampleType!=null && sampleType!="" && var_pkaliquots==sampleType){
		$j("#aliquotstypeModalDiv").show();
	}
	else{
		$j("#aliquotstypeModalDiv").hide();
		$j("#AliquotsTypeModal").val("");
	}
}


$j(function() {
	if(switchwf_flag==1){
		$j("#ctModalShipDateId").val("");
		$j("#shipCompanyModal").val("");
		$j("#trackNoModal").val("");
		$j("#modalSampleTypeAvailable").val("");
		$j("#AliquotsTypeModal").val("");
		$j("#labCodeModal").val("");
		$j("#ctModalShipDateId").focus();
	}
	getHiddedValues();
	getDatePic();
	var sampleType=$j("#modalSampleTypeAvailable").val();
	getAliquotsType(sampleType);
	var shipmentdate=$j('#ctModalShipDateId').val();
	if(shipmentdate!=null && shipmentdate!='' && shipmentdate!='undefined'){
		getDayfromshipdate(shipmentdate,'shimentdayModalDiv');
	}
	var validator = $j("#ctshipmentmodalformID").validate({
		invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    }
	});
});

function validateCtShipment(){
	if($j("#ctModalShipDateId").val()==null || $j("#ctModalShipDateId").val()==''){
		$j("#errorShipmentDateDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.cbu.order.selectShipmentDate"/>");
		 });
		$j("#errorShipmentDateDiv").show();
		return false;
		
	}
	else{
		$j("#errorShipmentDateDiv").find("span").each(function(){
      		$j(this).text("");
	 });
		$j("#errorShipmentDateDiv").hide();
	}
	/*if($j("#shipCompanyModal").val()==null || $j("#shipCompanyModal").val()==''){
		$j("#errorShipmentCmpyDiv").find("span").each(function(){
	      		$j(this).text("<s:text name="garuda.cbu.order.selectShippingCompany"/>");
		 });
		$j("#errorShipmentCmpyDiv").show();
		return false;

	}
	else{
		$j("#errorShipmentCmpyDiv").find("span").each(function(){
      		$j(this).text("");
	 });
	$j("#errorShipmentCmpyDiv").hide();
	}
	if($j("#modalSampleTypeAvailable").val()==null || $j("#modalSampleTypeAvailable").val()==''){
		
		$j("#errorShipmentSamTypDiv").find("span").each(function(){
			$j(this).text("<s:text name="garuda.cbu.order.selectSampleTypeAvailable"/>");
		 });
		$j("#errorShipmentSamTypDiv").show();
		return false;
	}
	else{
		$j("#errorShipmentSamTypDiv").find("span").each(function(){
			$j(this).text("");
		 });
		$j("#errorShipmentSamTypDiv").hide();
	}*/
	if($j("#AliquotsTypeModal").val()==null || $j("#AliquotsTypeModal").val()==''){
		if($j("#modalSampleTypeAvailable").val()==$j("#pkaliquots").val()){
			$j("#errorAliquotsDiv").find("span").each(function(){
		      		$j(this).text("<s:text name="garuda.cbu.sample.aliquotType"/>");
			 });
			$j("#errorAliquotsDiv").show();
			return false;
		}
		else{
			$j("#errorAliquotsDiv").find("span").each(function(){
	      		$j(this).text("");
		 	});
			$j("#errorAliquotsDiv").hide();
		}
		
	}if(($j("#ctModalShipDateId").val()!=null || $j("#ctModalShipDateId").val()!='')
		//&& ($j("#shipCompanyModal").val()!=null || $j("#shipCompanyModal").val()!='')
		&& (($j("#modalSampleTypeAvailable").val()!=null || $j("#modalSampleTypeAvailable").val()!='') ||($j("#AliquotsTypeModal").val()!=null || $j("#AliquotsTypeModal").val()!=''))
		){
			$j("#errorShipmentDateDiv").hide();
			//$j("#errorShipmentCmpyDiv").hide();
			//$j("#errorShipmentTrackNoDiv").hide();
			//$j("#errorShipmentSamTypDiv").hide();
			$j("#errorAliquotsDiv").hide();
			return true;

	}
}

function openModalForSampleInventory(){
	showModal('View Sample Inventory','updateProcessInfo?entityId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val()+'&pkcordId='+$j("#pkcordId").val());
}

</script>
<s:form name="ctshipmentmodalform" id="ctshipmentmodalformID">
<div id="sampleInventoryModalDiv">
	<s:if test="codeLstCustColLst!=null && codeLstCustColLst.size()>0">
	<s:iterator value="codeLstCustColLst" var="rowVal">
		<s:hidden name="tableName" id="tableName"></s:hidden>
		<s:hidden name="columnName" id="columnName"></s:hidden>
	</s:iterator>
	</s:if>
	<s:hidden name="sampleCount" id="sampleCountId"></s:hidden>
</div>
<div id="updatectshipmentmodal" style="display: none;">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="garuda.message.modal.ctshipment" /></strong></p>
			</div>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td>
			<input type="button" onclick="closeModals('ctshipmodaldiv')" value="<s:text name="garuda.common.close"/>" />
		</td>
	</tr>
</table>
</div>
<table width="100%" cellspacing="0" cellpadding="0" border="0" id="statusctshipmentmodal">
<tr>
<td>
<div class="portlet" id="ctshipmentmodalbarDivparent">
			<div id="ctshipmentmodalbarDiv"
					class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
					<div
						class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span class="ui-icon ui-icon-print" onclick="clickheretoprint('ctshipmentbarDiv','<s:property value="cdrCbuPojo.registryId" />')"></span><span
						class="ui-icon ui-icon-newwin"></span>
						<!--<span class="ui-icon ui-icon-plusthick" id="ctshipmentspan" onclick="getctshipmentDetails();"></span>-->
						<!--<span class="ui-icon ui-icon-close"></span>-->
						<span class="ui-icon ui-icon-minusthick"></span>
						<s:text name="garuda.ctOrderDetail.portletname.ctShipmentInfo"/> 
					</div>
					<div class="portlet-content" id="ctShipmentmodalContentDiv">
					<s:hidden name="pkCbuShipment" id="pkCbuShipment"></s:hidden>
					<s:if test="shipmentInfoLst!=null && shipmentInfoLst.size()>0">
					<s:iterator value="shipmentInfoLst" var="rowVal">
					<s:hidden name="prevSampleType" id="prevSampleType" value="%{#rowVal[5]}"></s:hidden>
					<s:hidden name="prevAliqType" id="prevAliqType" value="%{#rowVal[6]}"></s:hidden>
					<s:hidden name="dataModifiedFlag" id="dataModifiedFlag" value="%{#rowVal[24]}"></s:hidden>
					<s:hidden name="pkShipmentId" id="pkShipmentId" value="%{#rowVal[25]}"></s:hidden>
					<s:hidden name="pkPackingSlip" id="pkPackingSlipId"></s:hidden>
					<table>
						<tr>
							<td>
						        <table width="100%">
								<tr>
								<td width="25%">
									<strong><s:text name="garuda.ctShipmentInfo.label.ctshipdate"/></strong>:<span style="color: red;">*</span>
								</td>
								<td width="25%">
									<s:date	name="ctshipDate" id="ctModalShipDateId" format="MMM dd, yyyy" />
									<s:textfield readonly="true" onkeydown="cancelBack();"  name="shipmentPojo.schShipmentDate" cssClass="datePicWOMinDate" id="ctModalShipDateId" onchange="getDayfromshipdate(this.value,'shimentdayModalDiv');" value="%{#rowVal[0]}" onfocus="onFocusCall(this);"/>
									<div id="errorShipmentDateDiv"><span id="shipmentDatelbl" class="error" ></span></div>
								</td>
								<td width="25%">
									<s:if test="orderPojo.resolByTc!=null && orderPojo.resolByTc!='' && pkCanceled!=null && pkCanceled!='' && orderPojo.resolByTc==pkCanceled">
									<span style="font-style: italic;color: red;font-size: 10px"><s:text name="garuda.ctshipmetInfo.label.shipmentcancelled"></s:text></span>
									</s:if>
								</td>
								<td width="25%" id="shimentdayModalDiv">
	    							<strong><s:text name="garuda.cbushipmetInfo.label.day"></s:text></strong>:<span style="font-style: italic;color: blue"></span>
	    						</td>
								</tr>
								<tr>
								<td width="25%">
									<strong><s:text name="garuda.ctShipmentInfo.label.shippingcompany"/></strong>:
								</td>
								<td width="25%">
									<s:select name="shipmentPojo.fkshipingCompanyId" id="shipCompanyModal" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPER]" listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select Shipping Company" value="%{#rowVal[2]}" ></s:select>
									<div id="errorShipmentCmpyDiv"><span id="shipmentCmpylbl" class="error" ></span></div>	
								</td>
								<td width="25%">
									<strong><s:text name="garuda.ctShipmentInfo.label.trackingno"/></strong>:
								</td>
								<td width="25%">
									<s:textfield name="shipmentPojo.shipmentTrackingNo" id="trackNoModal" onchange="validateCtShipment();" value="%{#rowVal[3]}" maxlength="30"/>
									<div id="errorShipmentTrackNoDiv"><span id="shipmentTrackNolbl" class="error" ></span></div>
								</td>
								</tr>
								<tr>
								<td width="25%">
									<strong><s:text name="garuda.ctShipmentInfo.label.sampletypeavail"/></strong>:
									<img id="sampleInventoryhelp" height="15px" src="./images/help_24x24px.png" onclick="readNMDPXML('4')">
								</td>
								<td width="25%">
									<s:select name="modalSampleTypeAvailable" id="modalSampleTypeAvailable"	list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_SAMPLE_TYPE]" listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select Sample type" value="%{#rowVal[5]}" onchange="getAliquotsType(this.value);getSampleCount(this.value);"/>
									<div id="errorShipmentSamTypDiv"><span id="shipmentSamTyplbl" class="error" ></span></div>
								</td>
								<td width="50%" colspan="2">
									
									<strong><a href="#" onclick="openModalForSampleInventory();"><s:text name="garuda.ctShipmentInfo.label.updatesampleinventory"/></a></strong>
								</td>
								</tr>
								<tr>
									<td width="100%" colspan="4">
										<div id="aliquotstypeModalDiv">
											<table width="100%">
												<tr>	
													<td width="25%">
														<strong><s:text name="garuda.ctShipmentInfo.label.aliquotstype"/></strong>:<span style="color: red;">*</span>
													</td>
													<td width="25%">
														<s:select name="AliquotsTypeModal" id="AliquotsTypeModal"	list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ALIQUOTS_TYPE]" listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select" value="%{#rowVal[6]}" onchange="getSampleCount(this.value);"/>
														<div id="errorAliquotsDiv"><span id="errorAliquotslbl" class="error" ></span></div>
													</td>
													<td width="25%">
														
													</td>
													<td width="25%">
														
													</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
								<tr>
									<td width="25%">
										<strong><s:text name="garuda.ctResolution.label.labcode"></s:text></strong>:
										<img id="labCodehelp" height="15px" src="./images/help_24x24px.png" onclick="readNMDPXML('5')">
									</td>
									<td width="25%">
										<s:select name="labCode" id="labCodeModal" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_CODE]" listKey="pkCodeId" listValue="description" value="%{#rowVal[21]}"></s:select>
									</td>
									<td width="25%">
										<strong><s:text name="garuda.ctResolution.label.noofdaystoshipct"></s:text></strong>:
									</td>
									<td width="25%">
										<s:textfield name="noofdaystoshipct" value="%{#rowVal[22]}" disabled="true"></s:textfield>
									</td>
								</tr>
								<tr>
								<td width="50%" colspan="2" align="left">
								         <s:if test="hasViewPermission(#request.vCTinstns)==true">
									<input type="button" value='<s:text name="garuda.ctResolution.label.viewctinstructions"/>' onclick="readNMDPXML('3')"/>
								          </s:if>
								</td>
								<td width="50%" colspan="2" id="genpackageslipbarDiv" align="right">
									<s:if test="hasViewPermission(#request.crtePckngSlip)==true">
							          <s:if test='orderPojo.resolByTc!=pkCanceled && orderPojo.completeReqCliniInfoFlag=="Y"'>
										<input type="button" value='<s:text name="garuda.ctShipmentInfo.button.label.createpackageslip"></s:text>' onclick="createPackageSlip();"/>
							          </s:if>
								 	</s:if>
								 </td>
	    						</tr>
	          					</table>
						     </td>
						</tr>
						<s:hidden id="shipschFlag" value="%{#rowVal[11]}"></s:hidden>
						<s:hidden id="packageslipFlag" value="%{#rowVal[13]}"></s:hidden>
						</table>
						</s:iterator>
						
							<table bgcolor="#cccccc" class="tabledisplay disable_esign" width="100%">
						 		<tr bgcolor="#cccccc" valign="baseline">
								<td width="70%">
									<jsp:include page="../cb_esignature.jsp" flush="true">
										<jsp:param value="editCtshipmentmodal" name="submitId"/>
										<jsp:param value="invalidCtshipmentmodal" name="invalid" />
										<jsp:param value="minimumCtshipmentmodal" name="minimum" />
										<jsp:param value="passCtshipmentmodal" name="pass" />
									</jsp:include>
								</td>
								<td align="left" width="30%"><input type="button" id="editCtshipmentmodal" onclick="updateCtshipment('true');" value="<s:text name="garuda.unitreport.label.button.submit"/>" disabled="disabled"/>
									<input type="button"  onclick="closeModals('ctshipmodaldiv')" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />
								</td>
								</tr>
 		    				 	</table>
						</s:if>
						
						<s:else>
						<table>
							<tr>
							<td>
						        <table width="100%">
								<tr>
								<td width="25%">
									<strong><s:text name="garuda.ctShipmentInfo.label.ctshipdate"/></strong>:<span style="color: red;">*</span>
								</td>
								<td width="25%">
									<s:date	name="ctshipDate" id="ctModalShipDateId" format="MMM dd, yyyy" />
									<s:textfield readonly="true" onkeydown="cancelBack();"  name="shipmentPojo.schShipmentDate" class="datepic" id="ctModalShipDateId" cssClass="datePicWOMinDate"
										value=""  onchange="getDayfromshipdate(this.value,'shimentdayModalDiv');validateCtShipment();" onfocus="onFocusCall(this);"/><div id="errorShipmentDateDiv"><span id="shipmentDatelbl" class="error" ></span></div>
								</td>
								<td width="25%">
									<strong><s:text name="garuda.cbushipmetInfo.label.day"></s:text></strong>:
								</td>
								<td width="25%" id="shimentdayModalDiv">
	    							<span style="font-style: italic;color: blue"></span>
	    						</td>
								</tr>
								<tr>
								<td width="25%">
									<strong><s:text name="garuda.ctShipmentInfo.label.shippingcompany"/></strong>:
								</td>
								<td>
									<s:select name="shipmentPojo.fkshipingCompanyId" id="shipCompanyModal" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPPER]" listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select Shipping Company" value="%{defaultshipper}" onchange="validateCtShipment();"></s:select><div id="errorShipmentCmpyDiv"><span id="shipmentCmpylbl" class="error" ></span></div>																		
								</td>
								<td width="25%">
									<strong><s:text name="garuda.ctShipmentInfo.label.trackingno"/></strong>:
								</td>
								<td width="25%">
									<s:textfield name="shipmentPojo.shipmentTrackingNo" id="trackNoModal" onchange="validateCtShipment();" maxlength="30"/><div id="errorShipmentTrackNoDiv"><span id="shipmentTrackNolbl" class="error" ></span></div>
								</td>
								</tr>
								<tr>
								<td width="25%">
									<strong><s:text name="garuda.ctShipmentInfo.label.sampletypeavail"/></strong>:
									<img id="sampleInventoryhelp" height="15px" src="./images/help_24x24px.png" onclick="readNMDPXML('4')">
								</td>
								<td width="25%">
									<s:select name="modalSampleTypeAvailable" id="modalSampleTypeAvailable"	list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_SAMPLE_TYPE]" listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select Sample type" value="" onchange="validateCtShipment();getAliquotsType(this.value);getSampleCount(this.value);"/>
									
									<div id="errorShipmentSamTypDiv"><span id="shipmentSamTyplbl" class="error" ></span></div>
								</td>
								<td width="50%" colspan="2">									
									<strong><a href="#" onclick="openModalForSampleInventory();"><s:text name="garuda.ctShipmentInfo.label.updatesampleinventory"/></a></strong>
								</td>
								</tr>
								<tr>
									<td width="100%" colspan="4">
										<div id="aliquotstypeModalDiv">
											<table width="100%">
												<tr>	
													<td width="25%">
														<strong><s:text name="garuda.ctShipmentInfo.label.aliquotstype"/></strong>:<span style="color: red;">*</span>
													</td>
													<td width="25%">
														<s:select name="AliquotsTypeModal" id="AliquotsTypeModal"	list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ALIQUOTS_TYPE]" listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select" value="" onchange="validateCtShipment();getSampleCount(this.value);"/><div id="errorAliquotsDiv"><span id="errorAliquotslbl" class="error" ></span></div></td>
													<td width="25%">
														
													</td>
													<td width="25%">
														
													</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
								<tr>
									<td width="25%">
										<strong><s:text name="garuda.ctResolution.label.labcode"></s:text></strong>:
										<img id="labCodehelp" height="15px" src="./images/help_24x24px.png" onclick="readNMDPXML('5')">
									</td>
									<td width="25%">
										<s:select name="labCode" id="labCodeModal" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_CODE]" listKey="pkCodeId" listValue="description"></s:select>
									</td>
									<td width="25%">
										
									</td>
									<td width="25%">
										
									</td>
								</tr>
								<tr>
								<td width="50%" colspan="2" align="left">
									<input type="button" value='<s:text name="garuda.ctResolution.label.viewctinstructions"/>' onclick="readNMDPXML('3')"/>
								</td>
								<td width="50%" colspan="2" align="right">
									
								</td>
	    						</tr>
	          					</table>
						     </td>
						</tr>
						
					</table>
					
					<table bgcolor="#cccccc" class="tabledisplay disable_esign" width="100%">
			 		<tr bgcolor="#cccccc" valign="baseline">
					<td width="70%">
						<jsp:include page="../cb_esignature.jsp" flush="true">
							<jsp:param value="submitCtshipmentmodal" name="submitId"/>
							<jsp:param value="invalidCtshipmentmodal" name="invalid" />
							<jsp:param value="minimumCtshipmentmodal" name="minimum" />
							<jsp:param value="passCtshipmentmodal" name="pass" />
						</jsp:include>
					</td>
					<td align="left" width="30%"><input type="button" id="submitCtshipmentmodal" onclick="ctShipmentValidity();" value="<s:text name="garuda.unitreport.label.button.submit"/>" disabled="disabled"/>
						<input type="button"  onclick="closeModals('ctshipmodaldiv')" value="<s:text name="garuda.unitreport.label.button.cancel"/>" />
					</td>
						
					</tr>
		    				 	</table>
					</s:else>
		</div>
		</div>
	</div>
</td></tr></table>
</s:form>