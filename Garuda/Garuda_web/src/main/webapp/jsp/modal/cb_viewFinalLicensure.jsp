<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>

<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<script>

$j("#licensehistory").dataTable();
$j(function(){
	if(('<s:property value="cdrCbuPojo.fkCordCbuStatus"/>'=='<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH)"/>' || '<s:property value="cdrCbuPojo.fkCordCbuStatus"/>'=='<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)"/>')){
				$j("input[name=licenseEdit]").hide();
	}
	$j('#licenseDiv .licensureMandatory').each(function(){
		 $j(this).css("border", "1px solid #D9D9D9");
		 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
			 $j(this).css("border", "1px solid red");
			 $j('#licensureFlag').val("0");
			}
	});
});
</script>
<div id="licenseDiv" class='tabledisplay'>
<s:form id="license">
<s:hidden name="cdrCbuPojo.unLicesePkid" id="unLicensedPk"></s:hidden>
<s:hidden name="cdrCbuPojo.cordID" />
<s:hidden name="cdrCbuPojo.fkSpecimenId" />
<s:hidden name="cdrCbuPojo.specimen.pkSpecimen" />
<s:hidden name="esignFlag" id="esignFlag" />
<!--<s:hidden name="orderId" id="orderId" />-->
<table width="100%">
<!-- <tr id="liensehistroyTR">
	<td>
		<table class="displaycdr" id="licensehistory" >
			<thead>
				<tr>
					<th><s:text name="garuda.common.lable.date" /></th>
					<th><s:text name="garuda.cdrcbuview.label.user_name" /></th>
					<th>License Status</th>
					<th><s:text name="garuda.cdrcbuview.label.eligible_reason" /></th>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="lstLicensure" var="rowVal" status="row">
				<tr>
					<s:iterator value="rowVal" var="cellValue" status="col" >
						<s:if test="%{#col.index == 0}">
							<s:date name="%{#cellValue}"
									id="statusDate" format="MMM dd, yyyy" />
							<td><s:property value="%{statusDate}"/></td>
						</s:if>
						<s:if test="%{#col.index == 1}">
							<s:set name="cellValue1" value="cellValue" scope="request"/>
							<td>
								<%
									//String cellValue1 = request.getAttribute("cellValue1").toString();
									//UserJB userB = new UserJB();
									//userB.setUserId(EJBUtil.stringToNum(cellValue1));
									//userB.getUserDetails();													
								%>	
								<%//userB.getUserLastName()+" "+userB.getUserFirstName() %>										
							</td>
						</s:if>
						<s:if test="%{#col.index == 2}">																							
							<td style="valign : top">
								<s:property value="getCodeListDescById(#cellValue)" />
							</td>
						</s:if>
						<s:if test="%{#col.index == 3}">
							<td><s:iterator value="getMultipleReasons(#cellValue)" var="colVal" status="val"><s:property value="getCodeListDescById(#colVal)" /><br></s:iterator></td>
						</s:if>
					</s:iterator>
				</tr>
				</s:iterator>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3"></td>
				</tr>
			</tfoot>
		</table>
	</td>	
</tr> -->
<tr>
	<td>
		<table>
			<tr>
				<td width="35%">
					<s:text name="garuda.cdrcbuview.label.licensure_status" />:</td>
				<td width="65%">
					<s:select name="cbuLicStatus" id="cbuLicStatus" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS]"
							listKey="pkCodeId" listValue="description" headerKey=""
							headerValue="Select"  onchange="unliceseReason(this.value,'licenseReason');" disabled="true" cssClass="licensureMandatory" />
				</td>
			</tr>
		</table>
	</td>
</tr>
<s:if test="%{isTasksRequired(cdrCbuPojo.cordID)==false}">
<tr>			
	<td colspan="2">
		<div id="licenseReason"  <s:if test="#request.unlicensed!=true">style="display: none;"</s:if>>
			<table width="100%">
				<tr>
					<td width="35%">
						<s:text name="garuda.cdrcbuview.label.unlicense_reason" />:
					</td>
					<td width="65%">
						<s:select name="reasons" id="fkCordCbuUnlicenseReason" cssClass="licensureMandatory"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON]"
									listKey="pkCodeId" listValue="description" multiple="true" value="reasons" cssStyle="height:100px;" disabled="true"/>
					</td>
				</tr>
			</table>
		</div>
   </td>
</tr>
</s:if>
</table>
<table bgcolor="#cccccc" id="licensureTable">
		<tr>
  			<td align="center">
	  			<input type="button" name="licenseEdit" value='<s:text name="garuda.cbuentry.label.change"/>'
						onclick="showModal('Edit Licensure Status','updateStatusLicense?licenceUpdate=False&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&esignFlag=modal&orderId=<s:property value="orderId"/>&orderType=<s:property value="orderType"/>','400','500');">
  			</td>
  		</tr>
</table>
</s:form>
</div>