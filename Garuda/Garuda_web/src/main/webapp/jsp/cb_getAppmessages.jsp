<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%
HttpSession tSession = request.getSession(true);
GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
int cbuAppMssgsMenuRght = 0;
if(grpRights.getFtrRightsByValue("CB_APPMSGS")!=null && !grpRights.getFtrRightsByValue("CB_APPMSGS").equals(""))
{cbuAppMssgsMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_APPMSGS"));}
else
	cbuAppMssgsMenuRght = 4;
request.setAttribute("cbuAppMssgsMenuRght",cbuAppMssgsMenuRght);
%>
<script>
function confirmDel(){
	var msgId =0;
	if($j("input[name='pkAppMsg']").is(':checked')==true){
		msgId = $j("input[name='pkAppMsg']:checked").val()
		}
	
	if(msgId==0){
		alert("<s:text name="garuda.cbu.appMsg.selectMsgDel"/>");
		}
	else{
		showModal('Delete Application Message','deleteAppMessages?addNewMsg=False&appMessagesPojo.pkAppMsg='+msgId,'500','600');
		}
	
}

function showModalforEdit(){
	var msgId =0;
	if($j("input[name='pkAppMsg']").is(':checked')==true){
		msgId = $j("input[name='pkAppMsg']:checked").val()
		}
	
	if(msgId==0){
		alert("<s:text name="garuda.cbu.appMsg.selectMsgEdit"/>");
		}
	else{
		showModal('Edit Application Messages','updateAppMessages?addNewMsg=False&appMessagesPojo.pkAppMsg='+msgId,'500','600');
		}
}
$j(document).ready(function(){
	if($j("#activeMsgDiv").height()>125){
		$j("#activeMsgDiv").css({
			'height' : '125',
			 'position' : 'relative',
			 'overflow-y' : 'scroll'
			});
		}
	if($j("#pandingMsgDiv").height()>125){
		$j("#pandingMsgDiv").css({
			'height' : '125',
			 'position' : 'relative',
			 'overflow-y' : 'scroll'
			});
		}
	if($j("#inActiveMsgDiv").height()>125){
		$j("#inActiveMsgDiv").css({
			 'height' : '125',
			 'position' : 'relative',
			 'overflow-y' : 'scroll'
			});
		}
});

</script>
<div class="col_100 maincontainer "><div class="col_100">
<s:form>
<div class="column">
		      <div class="portlet" id="appmessagesparent" >
					<div id="appmessages" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
							<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
							<span class="ui-icon ui-icon-minusthick"></span>
							<!--<span class="ui-icon ui-icon-close" ></span>-->
							<s:text name="garuda.applMessge.label.applicationMsg"/>
							</div>
							
							<fieldset>
								<legend><s:text name="garuda.applMessge.label.activeMsg"></s:text></legend>
								<div id="activeMsgDiv">
								<table id="activeMsg" align="center" >
									<tbody>
										<s:iterator value="#request.activeMsgList" var="appMessagesPojo" status="row" >
										<tr>
											<td>&nbsp;&nbsp;<input type="radio" name="pkAppMsg" value='<s:property value="pkAppMsg"/>' /><s:property value="msgDesc" /> <span class="error" ><s:if test="msgDispAt==pkLoginPage">(Login Page)</s:if> <s:if test="msgDispAt==pkLandingPage">(Landing Page)</s:if></span> </td>
										</tr>
										</s:iterator>
										<s:if test="#request.activeMsgList!=null && #request.activeMsgList.size()==0">
										<tr>
											<td><s:text name="garuda.applMessge.label.message.noActivemsg"></s:text></td>
										</tr>
										</s:if>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="1"></td>
										</tr>
									</tfoot>
								</table>
								</div>
							</fieldset>
				<br/>
							<fieldset>
								<legend><s:text name="garuda.applMessge.label.pendingMsg"></s:text></legend>
								<div id="pandingMsgDiv">
								<table id="pandingMsg" align="center">
									<tbody>
										<s:iterator value="#request.pendingMsgList" var="appMessagesPojo" status="row">
										<tr>
											<td>&nbsp;&nbsp;<input type="radio" name="pkAppMsg" value='<s:property value="pkAppMsg"/>' /><s:property value="msgDesc" /> <span class="error" ><s:if test="msgDispAt==pkLoginPage">(Login Page)</s:if> <s:if test="msgDispAt==pkLandingPage">(Landing Page)</s:if></span></td>
										</tr>
										</s:iterator>
										<s:if test="#request.pendingMsgList!=null && #request.pendingMsgList.size()==0">
										<tr>
											<td><s:text name="garuda.applMessge.label.message.noPendingmsg"></s:text></td>
										</tr>
										</s:if>
									</tbody>
									<tfoot>
										<tr>
											<td colspan="1"></td>
										</tr>
									</tfoot>
								</table>
								</div>
							</fieldset>	
				<br/>
							<fieldset>
								<legend><s:text name="garuda.applMessge.label.inActiveMsg"></s:text></legend>
								<div id="inActiveMsgDiv">
								<table id="inActiveMsg" align="center">
									<tbody>
										<s:iterator value="#request.inactiveMsgList" var="appMessagesPojo" status="row">
										<tr>
											<td>&nbsp;&nbsp;<input type="radio" name="pkAppMsg" value='<s:property value="pkAppMsg"/>' /><s:property value="msgDesc" /> <span class="error" ><s:if test="msgDispAt==pkLoginPage">(Login Page)</s:if> <s:if test="msgDispAt==pkLandingPage">(Landing Page)</s:if></span></td>
										</tr>
										</s:iterator>
										<s:if test="%{#request.inactiveMsgList!=null && #request.inactiveMsgList.size()==0}">
										<tr>
											<td><s:text name="garuda.applMessge.label.message.noInactivemsg"></s:text></td>
										</tr>	
										</s:if>
										
									</tbody>
									<tfoot>
										<tr>
											<td colspan="1"></td>
										</tr>
									</tfoot>
								</table>
								</div>
								</fieldset>
				<br/>
								<table>
									<tr>
										<td>
										       <s:if test="hasNewPermission(#request.cbuAppMssgsMenuRght)==true">
										<input type="button" onclick="showModal('Add New Application Messages','addNewAppMessages?addNewMsg=False','400','600');" value="<s:text name="garuda.common.lable.add"/>"/>
										        </s:if>
										        <s:if test="hasEditPermission(#request.cbuAppMssgsMenuRght)==true">
										<input type="button" onclick="showModalforEdit();" value="<s:text name="garuda.common.lable.edit"/>"/>
										<input type="button" value="<s:text name="garuda.common.lable.delete"/>" onclick='confirmDel();'/>
										         </s:if>
										</td>
									</tr>
								</table>
		</div>
		</div>
		</div>
</s:form>
</div>
</div>
