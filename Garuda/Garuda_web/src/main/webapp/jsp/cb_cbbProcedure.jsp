<jsp:include page="widget-setting.jsp">
<jsp:param value="30" name="pageId"/>
</jsp:include>

<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:useBean id ="sessionmaint" scope="session" class="com.velos.eres.service.util.SessionMaint"/>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<% String contextpath=request.getContextPath();%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>

<%
	HttpSession tSession = request.getSession(true); 
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	int updatePrssngProc= 0;
	
	updatePrssngProc= Integer.parseInt(grpRights.getFtrRightsByValue("CB_PROCEDURE"));
	
	request.setAttribute("updatePrssngProc",updatePrssngProc);
	%>

<style>
#procTblTbody td{
	padding: 1px 10px !important;
    white-space: nowrap; 
}
</style>
<script>
var flag = 0;
var selectrcnt = [];
$j(document).ready(function() {
	getSessionData();
	flag = 1;
	searchReasultOnload();
	callDate();
	var rcnt=$j('#rcntId').val();
	if($j("#statusUpdateId").val()!="true" && selectrcnt.length==0){
		temp="0,"+rcnt;
		$j('#loginIds').val(temp);
	}
	if(selectrcnt.length>0){
			for(var im=0;im<selectrcnt.length;im++){
				$j("#checkbox"+selectrcnt[im]).addClass("autoChecked").attr("checked","checked");
				if(im==0){
					showDiv();
				}
			}
	}
	else{
			 $j('.autoChecked').attr("checked","checked");
	}
	var validator = $j("#procedureForm").validate({
		invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    }
	});
	jQuery(function() {
		  jQuery('#dateEnd1, #dateStart1').datepicker('option', {
		    beforeShow: customRange1
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange1(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});
	
	jQuery(function() {
		  jQuery('#dateEnd2, #dateStart2').datepicker('option', {
		    beforeShow: customRange2
		  }).focus(function(){
			  if(!$j("#ui-datepicker-div").is(":visible"))
			  		customRange2(this);
				if($j(this).val()!=""){
					var thisdate = new Date($j(this).val());
					if(!isNaN(thisdate)){
						var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
						$j(this).val(formattedDate);
					}
				}
			});
	});
	$j("#placeHolderOfid_procCordsMaxCollDt").hide();
});

function callDate(){
/*	var today = new Date();
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();
	var h=today.getHours();
	var mn=today.getMinutes()+1;
	$j( ".datepicker" ).datepicker({
		dateFormat: 'M dd, yy',
		maxDate: new Date(y, m, d),
		changeMonth: true,
		changeYear: true,
		showButtonPanel:true,
		prevText:'',
		nextText:''});
	*/
	getDatePic();
}
function resetValidForm(formId){
	$j("#"+formId).validate().resetForm();
}
function searchReasultOnload(){
	
	var is_all_entries=$j('#showsRowsearchResults').val();
	var pageNo=$j('#paginateWrapper1').find('.currentPage').text();
	var recentPrcId=$j('#rcntId').val();
	var showEntries=$j('#proc_tbl_sentries').val();
	var tEntries=$j('#tentriesProc').val();
	var sort=3;
	var sortDir="desc";
	var SortParm=$j('#proc_tbl_sort_col').val();
	var searchTxt=$j('#proc_filter_value').val();
	
	//alert("showEntries::::"+showEntries)
	
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#proc_tbl_sort_col').val();
		sortDir=$j('#proc_tbl_sort_type').val();
	}	
	

	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#proc_tbl_sentries').val(is_all_entries);
		showEntries=$j('#proc_tbl_sentries').val();
	}
	
	showEntriesTxt='Show <select name="showsRow" id="showsRowsearchResults" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';
	
    if(showEntries=="ALL"){
    	
    	$j('#searchResults').dataTable({
            "sScrollY": "150",
            "sScrollX": "100%",
            "sDom": "frtiS",
            "sAjaxSource": 'getJsonPagination.action?TblFind=pProcedure&allEntries=ALL'+$j('#proc_tbl_param').val(),
     		"bServerSide": true,
     		"bProcessing": true, 
     		"aaSorting": [[ sort, sortDir ]],
     		"oSearch": {"sSearch": searchTxt},
     		"bRetrieve" : true,
     		"bDeferRender": true,
     		"bDestroy": true,
     		"bSortCellsTop": true,
     		"sAjaxDataProp": "procedure",
     		"aoColumnDefs": [
  						   {		
  							  "aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
  		                	   return "";
  		                  }},
  		                  {
  		                 "aTargets": [1],"bSortable": false,"mDataProp": function ( source, type, val ) {
  		                	var tempStr="";
  		                	if(source.pkProcId==recentPrcId){
								tempStr="<input type=checkbox value="+source.pkProcId+" id=checkbox"+source.pkProcId+" class=autoChecked onclick=getCheck("+source.pkProcId+",this) />";
							}else{
								tempStr="<input type=checkbox value="+source.pkProcId+" id=checkbox"+source.pkProcId+" onclick=getCheck("+source.pkProcId+",this) />";
							}
							return tempStr;
		                  }},
		                  {
		                    	 	"aTargets": [2],"mDataProp": function ( source, type, val ) {
  		                	 	return source.procName;
  		                  }},
  		                  {		
  		                	  "aTargets": [3], "mDataProp": function ( source, type, val ) {
  		                		var returntxt=newformatDate(source.procStartDate);
  		                	 	return returntxt;
  	                	   }},
  		                   { 	
  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
  	                	    	var returntxt=newformatDate(source.procTermiDate);
      		                	return returntxt;
  		                   }}
  						],
  						"fnDrawCallback": function() { 
  							//alert("ALL Call Back........")
  							$j('#searchResults_wrapper').find('.dataTables_scrollFoot').hide();
						    $j('#searchResults_info').show();
						    $j("#showsRowsearchResults option:contains('ALL')").attr('selected', 'selected');
						    if(selectrcnt.length>0){
                 				for(var im=0;im<selectrcnt.length;im++){
	                 				$j("#checkbox"+selectrcnt[im]).addClass("autoChecked").attr("checked","checked");	
                 			}
  						}
  							else{
  								 $j('.autoChecked').attr("checked","checked");
  								}
						    $j("#showsRowsearchResults option:contains('ALL')").attr('selected', 'selected'); 	
						    ProcedureSort();
						    showStatusFilterText('procedureFiltStatusTxt','procStatFilterFlag');
  						}
		});
    	$j('#searchResults_filter').before(showEntriesTxt);    	
    	
    }else{
    	
    	$j('#searchResults').dataTable({
            "sScrollY": "150",
            "sScrollX": "100%",
            "sAjaxSource": 'getJsonPagination.action?TblFind=pProcedure&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+$j('#proc_tbl_param').val(),
            "bServerSide": true,
            "bProcessing": true, 
     		"bRetrieve" : true,
			"bDestroy": true,
			"bSortCellsTop": true,
			"oSearch": {"sSearch": searchTxt},
     		"aaSorting": [[ sort, sortDir ]],
     		"bDeferRender": true,
     		"sAjaxDataProp": "procedure",
     		"aoColumnDefs": [
     		                {		
    							  "aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
    		                	   return "";
    		               }},
  		                  {
  		                 			"aTargets": [1],"bSortable": false,"mDataProp": function (source, v) {   // o, v contains the object and value for the column
		                    	 	var tempStr="";
		                    	 
									if(source.pkProcId==recentPrcId){
										tempStr="<input type=checkbox value="+source.pkProcId+" id=checkbox"+source.pkProcId+" class=autoChecked onclick=getCheck("+source.pkProcId+",this) />";
									}else{
										tempStr="<input type=checkbox value="+source.pkProcId+" id=checkbox"+source.pkProcId+" onclick=getCheck("+source.pkProcId+",this) />";
									}
									return tempStr;
		                  }},
		                  {
		                    	"aTargets": [2],"mDataProp": function ( source, type, val ) {
  		                	 	return source.procName;
  		                  }},
  		                  {		
  		                	    "aTargets": [3], "mDataProp": function ( source, type, val ) {
  		                		var returntxt=newformatDate(source.procStartDate);
  		                	 	return returntxt;
  	                	   }},
  		                   { 	
  	                	    	"aTargets": [4], "mDataProp": function ( source, type, val ) { 
  	                	    	var returntxt=newformatDate(source.procTermiDate);
      		                	return returntxt;
  		                   }}
  						],
  						"fnDrawCallback": function() { 
  							
  							ProcedureSort();
  							showStatusFilterText('procedureFiltStatusTxt','procStatFilterFlag');
  							
  							if(selectrcnt.length>0){
                 				for(var im=0;im<selectrcnt.length;im++){
                 					$j("#checkbox"+selectrcnt[im]).addClass("autoChecked").attr("checked","checked");	
                 			}
  						}
  							else{
  								 $j('.autoChecked').attr("checked","checked");
  								}
  							}
  						
		});
    	
    }
	
	
	$j('#searchResults_info').hide();
	$j('#searchResults_paginate').hide();
	$j('#searchResults_length').empty();
	$j('#searchResults_length').replaceWith(showEntriesTxt);
	if($j('#entries').val()!=null || $j('#entries').val()!=""){
		$j('#showsRowsearchResults').val($j('#entries').val());
	}
	$j('#showsRowsearchResults').change(function(){
		var textval=$j('#showsRowsearchResults :selected').html();
		$j('#proc_tbl_sentries').val(textval);
		paginationFooter(0,"cbbProcedure,showsRowsearchResults,temp,procedureForm,searchTble,searchReasultOnload,proc_tbl_param");
		//$j("#loginIds").val("0");
		showDiv();
	});	
	
	$j('.procTblHead').click(function(){
		//$j("#loginIds").val("0");
		showDiv();
	});
	
	
	$j('#searchResults_filter').find(".searching1").keyup(function(e){
		
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#proc_filter_value').val(val);
		  if(e.keyCode==13)
		  {
			  if(val!=null && val!=""){
			  $j('#proc_filter_flag').val('1');
			  //$j("#loginIds").val("0");
			  showDiv();
			  }
			  paginationFooter(0,"cbbProcedure,showsRowsearchResults,temp,procedureForm,searchTble,searchReasultOnload,proc_tbl_param");
		  }
	}); 
	
	//var temp=150;
	//$j('#searchResults').tableScroll({"height":temp});
	if (flag != 1) {
		//$j("#loginIds").val("0");
		showDiv();
	}
	
}
$j(window).unload(function(){
	setSessionData();
});

function setSessionData(){
	
	var showEntriesTxt=$j('#showsRowsearchResults :selected').html();
	var tbl1_site_filter=$j('#proc_tbl_param').val();
	var tlast_col_sort=$j('#proc_tbl_sort_col').val();
	var tlast_col_type=$j('#proc_tbl_sort_type').val();
	if($j('#loginIds').val()!="0"){
	var lastrctIds=$j('#loginIds').val();
	lastrctIds=$j.trim(lastrctIds.replace(/'/gi,""));
	}
	var last_col_sort="";
	var last_col_type="";
	var str="";
	var param1="";
	var url="";
	
	if(tbl1_site_filter!='undefined' && tbl1_site_filter!=''){
		  
		 str=tbl1_site_filter.indexOf("&procedureName");
		  
		   if(str!=-1){
			  var len=tbl1_site_filter.length;
			  var removedStr1=tbl1_site_filter.substring(str,len);
			  param1=removedStr1.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
			  if(param1==""){
				  param1=tbl1_site_filter.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
			  }
		   } 
		  
	}
	
	
	if(tlast_col_sort!=null && tlast_col_sort!=undefined && tlast_col_sort!=""){
		last_col_sort=tlast_col_sort;
		last_col_type=tlast_col_type;
	}
	
	url="setSessionValuesProc?l_proc_SE="+showEntriesTxt+"&l_proc_fval="+param1+"&l_proc_sort_col="+last_col_sort+"&l_proc_sort_typ="+last_col_type+"&l_proc_rcntIds="+lastrctIds;
	setValueInSession(url);
}
function getSessionData(){
	
	var sv_proc_se='<%=session.getAttribute("V_PROC_SE")%>';
	var sv_proc_sort_col='<%=session.getAttribute("V_SORT_COL_IND")%>';
	var sv_proc_sort_typ='<%=session.getAttribute("V_SORT_COL_TYP")%>';
	var sv_proc_filt_val='<%=session.getAttribute("V_FILTER_VALUES")%>';
	var sv_last_rcnt_ids='<%=session.getAttribute("V_RCNT_ID_VALUES")%>';
	var filterparam="";
	
	if(sv_proc_se!=null && sv_proc_se!='null' && sv_proc_se!="" && sv_proc_se!=undefined){
		$j('#proc_tbl_sentries').val(sv_proc_se);	
	}
	if(sv_proc_sort_col!=null && sv_proc_sort_col!='null' && sv_proc_sort_col!="" && sv_proc_sort_col!=undefined){
		$j('#proc_tbl_sort_col').val(sv_proc_sort_col);	
	}
	if(sv_proc_sort_typ!=null && sv_proc_sort_typ!='null' && sv_proc_sort_typ!="" && sv_proc_sort_typ!=undefined){
		$j('#proc_tbl_sort_type').val(sv_proc_sort_typ);	
	}
	if(sv_proc_filt_val!=null && sv_proc_filt_val!='null' && sv_proc_filt_val!="" && sv_proc_filt_val!=undefined){
		param3=sv_proc_filt_val.replace(/----/gi,'&');	
		$j('#proc_filter_flag').val("1");
		$j('#proc_tbl_param').val(param3);
		$j('#procStatFilterFlag').val('1');
	}
	if(sv_last_rcnt_ids!=null && sv_last_rcnt_ids!='null' && sv_last_rcnt_ids!="" && sv_last_rcnt_ids!=undefined && sv_last_rcnt_ids!="0"){
		var ids = sv_last_rcnt_ids.split(",");
		$j('#loginIds').val(sv_last_rcnt_ids);
		for(var i=0;i<ids.length;i++){
			selectrcnt[i]=ids[i];
		}
		
	}
	//searchReasultOnload();
}
function customRange1(input) {
		if (input.id == 'dateEnd1') {
		    if($j("#dateStart1").val()!="" && $j("#dateStart1").val()!=null){
		    var minTestDate=new Date($j("#dateStart1").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
			  $j('#dateEnd1').datepicker( "option", "minDate", new Date(y1,m1,d1));
		    }else{
		    	$j('#dateEnd1').datepicker( "option", "minDate", null);
			   }
		  } else if (input.id == 'dateStart1') {
			  if($j("#dateEnd1").val()!="" && $j("#dateEnd1").val()!=null){
			  var minTestDate=new Date($j("#dateEnd1").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
			  $j('#dateStart1').datepicker( "option", "maxDate", new Date(y1,m1,d1));
			  }else{
				  $j('#dateStart1').datepicker( "option", "maxDate", null);
			}
		  }
		  
}
function customRange2(input) {
	 
	if (input.id == 'dateEnd2') {
	  
	    if($j("#dateStart2").val()!="" && $j("#dateStart2").val()!=null){
	    var minTestDate=new Date($j("#dateStart2").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd2').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd2').datepicker( "option", "minDate", null);
		   }
	  } else if (input.id == 'dateStart2') {
		  if($j("#dateEnd2").val()!="" && $j("#dateEnd2").val()!=null){
		  var minTestDate=new Date($j("#dateEnd2").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		    $j('#dateStart2').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart2').datepicker( "option", "maxDate", null);
		}
	  }
	 
}
function fn_setSort_ord(label,el){
	var sort_to="";
	var sorted=false;
	$j("#ord_header").val(label);
	var pagno=$j('#ordTblFoot div:first').find('b').find('.currentPage').text();
	if($j(el).hasClass('sorting') || $j(el).hasClass('sorting_desc')){
		sort_to='asc';
		var temp_sort_Param='&orderBy='+label+' '+sort_to;
		$j('#ord_sortParam').val(temp_sort_Param);
		$j("#ord_sort_type").val('sorting_asc');
		paginationFooter(0,"cbbProcedure,showsRowsearchResults,temp,procedureForm,searchTble,searchReasultOnload,ord_sortParam");
		//paginationFooter(0,"getCbbDetails,showsRow,inputs,Tableform,searchTble,manageProfile,ord_sortParam");
		sorted=true;
	}
	if($j(el).hasClass('sorting_asc') && !sorted){
		sort_to='desc';
		var temp_sort_Param='&orderBy='+label+' '+sort_to;
		$j('#ord_sortParam').val(temp_sort_Param);
		$j("#ord_sort_type").val('sorting_desc');
		paginationFooter(0,"cbbProcedure,showsRowsearchResults,temp,procedureForm,searchTble,searchReasultOnload,ord_sortParam");
		//paginationFooter(0,"getCbbDetails,showsRow,inputs,Tableform,searchTble,manageProfile,ord_sortParam");
		sorted=true;
	}
}


function showDiv()
{	
		var temp=$j("#loginIds").val();
		var ids = temp.split(",");
		
		for(var i=0;i<ids.length;i++){
			selectrcnt[i]=ids[i];
		}
		
		refreshCbbProcDiv('getCBBProcedures?loginIds='+$j("#loginIds").val(),'cbbProcedureMaindiv','cbbProcedureForm');
		$j('#cbbProcedureMaindiv').addClass('loaded');
	
}

function getCheck(val,This){
	if(This.checked){
	var val1 = $j("#loginIds").val();
		
		if(val1==null || val1=="" ){			
			val1="'0'";
		}
		var newVal = val1+","+val;
		$j("#loginIds").val(newVal);
		
		showDiv();
		
	}
	else{
		 var checkIfRated = $j("#loginIds").val();
		checkIfRated = checkIfRated.replace(","+val,"");
		$j("#loginIds").val(checkIfRated);
		showDiv();
	}
}

function addNewProcessingProcedure(){
	
	showModal('Add New CBB Processing Procedure','addNewCbbProcedure?statusUpdate=False'+'&moduleEntityId='+' '+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_PROCESSING_PROCEDURE"/>&moduleEntityIdentifier='+'' +'&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_PROS_PROC"/>','500','700');
}

function showModelforcbb(procId,procName){
		refreshDiv('checkForProcProcedure?procId='+procId,'procProcAssignToCordDiv','cbbProcedureForm');
		showModal('Edit CBB Processing Procedure','updateCbbProcedure?statusUpdate=False&pkProcId='+procId+'&loginIds='+$j("#loginIds").val()+'&assignedToCordFlag='+$j("#procProcAssignToCordFlag").val()+'&moduleEntityId='+procId+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_PROCESSING_PROCEDURE"/>&moduleEntityIdentifier='+procName+'&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_PROS_PROC"/>','500','700');
		$j("#procProcAssignToCordFlag").val("");
}


function fncopyProcedure(procId){
	showModal('Copied-New CBB Processing Procedure','copyCbbProcedure?pkProcId='+procId+'&loginIds='+$j("#loginIds").val()+'&moduleEntityId='+' '+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_PROCESSING_PROCEDURE"/>&moduleEntityIdentifier='+'' +'&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_COPY_PROS_PROC"/>','500','700');
}
function refreshCbbProcDiv(url,divname,formId){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
            var $jresponse=$j(result);
            var errorpage = $jresponse.find("#errorForm").html();
            if(errorpage != null &&  errorpage != "" ){
            	$j("#main").html(result);
            }else{
            	$j("#"+divname).replaceWith($j("#"+divname, $j(result)));
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	
}
function showDateDiv(el,id,id1,id2){
	var t=$j(el);
	var obj=t.offset();
	var xOffset=180;
	var yOffset=30;
	var val1="";
	var val2="";
	
	if(id=='dateDiv1'){
		val1=$j('#fprocStrDt1').val();
		val2=$j('#fprocStrDt2').val();
	}
	if(id=='dateDiv2'){
		val1=$j('#fprocTerDt1').val();
		val2=$j('#fprocTerDt2').val();
	}
		
	$j('#'+id1).val(val1);
	$j('#'+id2).val(val2);
	
	if(navigator.appName=="Microsoft Internet Explorer"){
			
		    yOffset=yOffset+15;
			xOffset=xOffset+50;
			if(id=="dateDiv2"){
					xOffset=xOffset+70;
			}
			
	}
	if(navigator.appName!="Microsoft Internet Explorer"){
		
	    	yOffset=yOffset+15;
			xOffset=xOffset+26;
			if(id=="dateDiv2"){
				xOffset=xOffset+70;
			}
					
	}
	$j("#"+id)
			.css({left:obj.left-xOffset,top:obj.top+yOffset})
			.fadeIn("fast").show()
			.addClass('dateDivClass')
	$j("#"+id).show();
	if(val1==null || val1==""){
		$j('#'+id1).focus();
	}
	
		
}
/*function showAddWidget(url){
	 url = url + "?pageId=21" ; 
	 showAddWidgetModal(url);
}*/

function constructTable(){
	
}
function toggleDiv1(obj,id){
	$j('#'+id).toggle();
	$j(obj).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
}
function fn_FilterProcedure(){
	
	//alert("Inside filter function");
	
	var procName="";
	var pStartDateD1="";
	var pStartDateD2="";
	var pTermDateD1="";
	var pTermDateD2="";
	var DateFlag1="0";
	var DateFlag2="0";
	var procParam="";
	var showEntries="";
	$j('#procStatFilterFlag').val('1');
	procName=$j('#fprocedureName').val();
	pStartDateD1=$j('#dateStart1').val();
	pStartDateD2=$j('#dateEnd1').val();
	pTermDateD1=$j('#dateStart2').val();
	pTermDateD2=$j('#dateEnd2').val();
	showEntries=$j('#showsRowsearchResults').val();
	
	
		
	if($j('#dateStart1').val()!="" && $j('#dateEnd1').val()!=""){
		var dateElements = $j('.datepic').get();
		  $j.each(dateElements, function(){
			  if(this.id=='dateStart1' || this.id=='dateEnd1')
				  customRange1(this);
			  else
				  customRange2(this);
		  });
		if($j('#procedureForm').valid()){
			pStartDateD1=parseDateValue(pStartDateD1);
			pStartDateD2=parseDateValue(pStartDateD2);
			DateFlag1="1";
		}
		else{
			$j('#dateDiv1').show();
			dontflag=1;
			
		}
	}else if( ($j('#dateStart1').val()!="" && $j('#dateEnd1').val()=="" ) || ($j('#dateStart1').val()=="" && $j('#dateEnd1').val()!="")){
			alert("Please enter correct Procedure Start Date Range");
			pStartDateD1="";
			pStartDateD2="";
			DateFlag1="2";  
			dontflag=1; 
	}	
	
	
	if($j('#dateStart2').val()!="" && $j('#dateEnd2').val()!=""){
	var dateElements = $j('.datepic').get();
		 $j.each(dateElements, function(){
			  if(this.id=='dateStart1' || this.id=='dateEnd1')
				  customRange1(this);
			  else
				  customRange2(this);
		  });
		if($j('#procedureForm').valid()){
		pTermDateD1=parseDateValue(pTermDateD1);
		pTermDateD2=parseDateValue(pTermDateD2);
		DateFlag1="1";
		dontflag=1; 
		}
		else{
			$j('#dateDiv2').show();
			dontflag=1;
			
		}
	}else if( ($j('#dateStart2').val()!="" && $j('#dateEnd2').val()=="" ) || ($j('#dateStart2').val()=="" && $j('#dateEnd2').val()!="")){
			alert("Please enter correct Procedure Termination Date Range");
			pTermDateD1="";
			pTermDateD2="";
			DateFlag2="2";  
			dontflag=1; 
	}	
	
	//alert("value:::"+procName+":DateFlag1:"+DateFlag1+":DateFlag2:"+DateFlag2);
	
	if((procName!="" || DateFlag1=="1" || DateFlag2=="1") && (DateFlag1!="2" && DateFlag2!="2")){
		procParam='&procedureName='+procName+"&proStrDt1="+pStartDateD1+'&proStrDt2='+pStartDateD2+'&procTermDt1='+pTermDateD1+'&procTermDt2='+pTermDateD2;
		$j('#proc_tbl_param').val(procParam);
		$j('#proc_filter_flag').val("1");
		$j('#proc_tbl_sentries').val(showEntries);
		//alert("Param:::::::::::::::::::::::::::"+$j('#proc_tbl_param').val())
		loaddiv('cbbProcedure?iShowRows='+showEntries+procParam,'searchTble');
		searchReasultOnload();
		$j("#loginIds").val("0");
		if(selectrcnt.length>0){
			for(var im=0;im<selectrcnt.length;im++){
				$j("#checkbox"+selectrcnt[im]).removeClass("autoChecked").removeAttr("checked");
			}
		}
		showDiv();
		
		}
	}
		
function fn_ResetSortProcedure(){
	if($j('#proc_tbl_sort_col').val()!=undefined && $j('#proc_tbl_sort_col').val()!=null && $j('#proc_tbl_sort_col').val()!=""){
		
		$j('#proc_tbl_sort_col').val('');
		$j('#proc_tbl_sort_type').val('');
		var oTable = $j('#searchResults').dataTable();
		oTable.fnSort( [ [0,'asc'] ] );
		oTable.fnDraw();
	}
}
function fnResetFilterProcedure(){
	
	var flagVal=$j('#proc_filter_flag').val();
	resetValidForm('procedureForm');
	$j('#procStatFilterFlag').val('0');
	if(flagVal=="1"){

		procParam='cbbProcedure?iShowRows='+$j('#showsRowsearchResults').val();
		$j('#proc_tbl_param').val(procParam);
		$j('#proc_filter_flag').val("0");
		$j('#proc_tbl_param').val("");
		//alert("Param:::::::::::::::::::::::::::"+$j('#proc_tbl_param').val())
		loaddiv(procParam,'searchTble');
		searchReasultOnload();
	}
	
	$j('#fprocedureName').val('');
	$j('#dateStart1').val('');
	$j('#dateEnd1').val('');
	$j('#dateStart2').val('');
	$j('#dateEnd2').val('');
	$j("#loginIds").val("0");
	if(selectrcnt.length>0){
		for(var im=0;im<selectrcnt.length;im++){
			$j("#checkbox"+selectrcnt[im]).removeClass("autoChecked").removeAttr("checked");
		}
	}
	showDiv();
	
}
function ProcedureSort(){
	$j('#procTblThead').find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#proc_tbl_sort_col').val(i);
			$j('#proc_tbl_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#proc_tbl_sort_col').val(i);
			$j('#proc_tbl_sort_type').val('desc');
		}
   });
}
</script>
<s:hidden name="ord_sortParam" id="ord_sortParam"/>
<s:hidden id="procId" name="procId"/>
<s:hidden id="procName" name="procName"/>
<s:hidden name="ord_sort_type" id="ord_sort_type"/>
<s:hidden name="ord_header" id="ord_header"/>
<s:hidden name="proc_tbl_sort_col" id="proc_tbl_sort_col"/>
<s:hidden name="proc_tbl_sort_type" id="proc_tbl_sort_type"/>
<s:hidden name="proc_tbl_param" id="proc_tbl_param"/>
<s:hidden name="proc_filter_flag" id="proc_filter_flag"/>
<s:hidden name="proc_filter_value" id="proc_filter_value"/>
<s:hidden name="proc_tbl_sentries" id="proc_tbl_sentries"/>
<s:hidden name="procStatFilterFlag" id="procStatFilterFlag"/>
<div id="emptyDiv"></div>
<s:form id="procedureForm">
<div style="display: none;" id="dateDiv2" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv2').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart2" id="dateStart2" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart2" id="dateStart2" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd2" id="dateEnd2" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd2" id="dateEnd2" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear" onclick="fnclear('dateStart2','dateEnd2')" ><s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt1" onclick="$j('#dateDiv2').hide();fn_FilterProcedure();" ><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt1" onclick="$j('#dateDiv2').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>
<div style="display: none;" id="dateDiv1" class="dataEnteringDiv">
<div class="closeDiv" onclick="$j('#dateDiv1').hide()" ><img src="images/cross.png" /></div>
																	<table align="center">
																		<tr align="center">
																		<td>
																		<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart1" id="dateStart1" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart1" id="dateStart1" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																		</td>
																		<td>
																		<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd1" id="dateEnd1" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd1" id="dateEnd1" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																		</td>
																		<td align="center">
																		<button type="button" id="clear" onclick="fnclear('dateStart1','dateEnd1')" >
																		<s:text name="garuda.pendingorderpage.label.clear"/></button>
																		</td>
																		</tr>
																		<tr align="center">
																		<td colspan="3">
																		<button type="button" id="dateStartbt1" onclick="$j('#dateDiv1').hide();fn_FilterProcedure();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																		<button type="button" id="closedt1" onclick="$j('#dateDiv1').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																		</td>
																		</tr>
																	</table>
</div>
</s:form>
<div class="col_100 maincontainer ">
<div class="col_100">
<s:form>
<table>
	<tr>
		<td>
		<div class="portlet" id="cbbProcViewparent">
		<div id="cbbProcView"
			class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
		<div
			class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
			style="text-align: center;"><span
			class="ui-icon ui-icon-minusthick"></span><!--<span
			class="ui-icon ui-icon-close"></span>--> <s:text
			name="garuda.cbbprocedures.label.processingprocedure"></s:text></div>
		<div class="portlet-content">
		<div id="searchTble">
		<s:hidden name="fprocStrDt1" id="fprocStrDt1"/>
		<s:hidden name="fprocStrDt2" id="fprocStrDt2"/>
		<s:hidden name="fprocTerDt1" id="fprocTerDt1"/>
		<s:hidden name="fprocTerDt2" id="fprocTerDt2"/>
		
		<input type="hidden" name="entries" id="entries" value=<%=paginateSearch.getiShowRows() %> />
		<input type="hidden" name="tentries" id="tentriesProc" value=<%=paginateSearch.getiTotalRows() %> />
		<table align="right">
		<tr align="right" style="vertical-align: top;">
		<td>
			<a href="#" onclick="fn_FilterProcedure()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fnResetFilterProcedure()" ><s:text name="garuda.landingpage.button.label.reset"/></a>&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="fn_ResetSortProcedure()"  id="resetSortProc"><s:text name="garuda.pendingorderpage.label.resetsort"/></a>
		</td>
		</tr>
		</table>
		<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults">
			<thead>
				<tr id="procTblThead">
					<th><!-- EmptyHead --></th>
					<th><s:text name="garuda.cbbprocedures.label.select"></s:text></th>
					<th class="procTblHead"><s:text name="garuda.cbbprocedures.label.procname"></s:text></th>
					<th class="procTblHead"><s:text name="garuda.cbbprocedures.label.procstartdate"></s:text></th>
					<th class="procTblHead"><s:text name="garuda.cbbprocedures.label.proctermidate"></s:text></th>
				</tr>
				<tr>
					<th><!-- EmptyHead --></th>
					<th><!-- EmptyHead --></th>
					<th><s:textfield name="fprocedureName" id="fprocedureName" ></s:textfield></th>
					<th><button type="button" onclick="showDateDiv(this,'dateDiv1','dateStart1','dateEnd1'); resetValidForm('procedureForm');"><s:text name="garuda.pendingorder.button.dateRange"/></button></th>
					<th><button type="button" onclick="showDateDiv(this,'dateDiv2','dateStart2','dateEnd2'); resetValidForm('procedureForm');"><s:text name="garuda.pendingorder.button.dateRange"/></button></th>
				</tr>
			</thead>
			<tbody id="procTblTbody">
				<s:iterator value="cbbList123" var="cbbProcedurePojo" status="row">
					<tr id="<s:property	value="%{#row.index}"/>" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')">
						<td></td>
						<td align="left"><s:if
							test="pkProcId==rcntPkProcId || list.contains(pkProcId.toString().trim())">
							<s:checkbox name="checkbox"
								id="checkbox<s:property value='cbbProcedures.pkProcId'/>"
								 cssClass="autoChecked" onclick="getCheck('%{pkProcId}',this)"/>
						</s:if> <s:else>
							<s:checkbox name="checkbox"
								id="checkbox<s:property value='cbbProcedures.pkProcId'/>"
								onclick="getCheck('%{pkProcId}',this)" />
						</s:else></td>
						 <!-- <td><s:property value="procNo" /></td> -->
						<td><s:property value="%{procName}" /></td>
						<!-- <td><s:property value="procVersion" /></td> -->
					<%--<td><s:label cssClass="datePicWMaxDate" value="%{getText('collection.date',{procStartDate})}"></s:label></td>
						<td><s:label cssClass="datePicWMaxDate" value="%{getText('collection.date',{procTermiDate})}"></s:label></td> --%>	
						<td><s:property value="%{getText('collection.date',{procStartDate})}"/></td>
						<td><s:property value="%{getText('collection.date',{procTermiDate})}"/></td>
						
					</tr>

				</s:iterator>
			</tbody>
			<tfoot>
				<tr id="ordTblFoot">
					<td colspan="4">
						<jsp:include page="paginationFooter.jsp">
						<jsp:param value="searchTble" name="divName"/>
						<jsp:param value="procedureForm" name="formName"/>
						<jsp:param value="showsRowsearchResults" name="showsRecordId"/>
						<jsp:param value="cbbProcedure" name="url"/>
						<jsp:param value="searchReasultOnload" name="bodyOnloadFn"/>
						<jsp:param value="1" name="idparam"/>
						<jsp:param value="proc_tbl_param" name="procFilterVals"/>
						</jsp:include>
					</td>
				</tr>
			</tfoot>
		</table>
		<div align="center"><span id="procedureFiltStatusTxt"></span></div>
		</div>
		</div>
		</div>
		</div>
		</td>
	</tr>
</table>
</s:form>
<s:form id="cbbProcedureForm">
	<s:hidden name="fkProcId" id="loginIds" />
	<s:hidden name="statusUpdate" id="statusUpdateId"></s:hidden>
	<s:hidden name="rcntPkProcId" id="rcntId" />
	<s:hidden name="" id="userid" value=""></s:hidden>
	<div id="procProcAssignToCordDiv">
		<s:hidden name="procProcAssignToCordFlag" id="procProcAssignToCordFlag"></s:hidden>
		 <s:textfield name="procCordsMaxCollDt" id="procCordsMaxCollDt" cssClass="datePicWOMinDate" cssStyle="display:none"> </s:textfield>
	</div>
	<table>
		<tr>
			<td>
			<div class="quicklinkpanel" style="width: 98%; padding-bottom: 10px;">
			<table width="100%" cellpadding="0" cellspacing="0"
				style="padding-top: 5px; padding-left: 5px;">
				<tr bordercolor="black">

					<th Style="text-align: left;">
					     <s:if test="hasNewPermission(#request.updatePrssngProc)==true">
					<button type="button" onclick="addNewProcessingProcedure();"><s:text
						name="garuda.cbbprocedures.label.addproc"></s:text></button>
						 </s:if>
					<button type="button"
						onclick="javaScript:return cbbprocedurescreen();"><s:text
						name="garuda.cbbprocedures.label.print" /></button>
					</th>
				</tr>
			</table>
			</div>
			</td>
		</tr>
		<tr>
			<td>
			<div id="cbbProcedureMaindiv">
			
			<table>
				<s:iterator value="cbbProdList" var="cbbProcedureInfoPojo" status="row">
					<tr>
						<td>
						<div class="portlet" id="cbbProcedureViewparent"  >
						<div id="cbbProcedureView"
							class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
						<div 
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-minusthick" onclick="toggleDiv1(this,'cbbProcCont<s:property value="cbbProcedures.procName"/>')"></span><!--<span
							class="ui-icon ui-icon-close"></span>--><s:text
							name="garuda.cbbprocedures.label.processingprocedure"></s:text>:
							<b><u><s:property value="cbbProcedures.procName"></s:property></u></b>
							<%-- <b><u><s:property value="cbbProcedures.procName"></s:property></u></b>--%>
							</div>
							<s:hidden name="pName" id="pName" value="%{cbbProcedures.procName}"/>
							<s:hidden name="pId" id="pId" value="%{cbbProcedures.pkProcId}"/>
							
							<s:hidden value="%{getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@VELOS_EMTRAX_VERSION_TYPE,@com.velos.ordercomponent.util.VelosGarudaConstants@VELOS_EMTRAX_VERSION_SUBTYPE)}" id="date_id"></s:hidden>
							
							<s:set scope="request" value="%{cbbProcedures.pkProcId}" name="moduleEntityId"></s:set>
							<s:set scope="request" value="%{cbbProcedures.procName}" name="moduleEntityIdentifier"></s:set>
							<s:set scope="request" value="%{@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_PROCESSING_PROCEDURE}" name="moduleEntityType"></s:set>
							<s:set scope="request" value="%{@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_PROS_PROC}" name="moduleEntityEventCode"></s:set>
						<div class="portlet-content" id="cbbProcCont<s:property value="cbbProcedures.procName"/>">
						<div>
						<div align="right">
						<table>
							<tr>
								<td align="right">
                                    <s:if test="hasEditPermission(#request.updatePrssngProc)==true">
								<button type="button"
									onclick="showModelforcbb('<s:property value="cbbProcedures.pkProcId" />','<s:property value="cbbProcedures.procName"  escapeJavaScript="true"/>');"><s:text
									name="garuda.cbbprocedures.label.edit"></s:text></button>
									</s:if>
								    <s:if test="hasNewPermission(#request.updatePrssngProc)==true">
								<button type="button"
									onclick="fncopyProcedure('<s:property value="cbbProcedures.pkProcId" />');"><s:text
									name="garuda.cbbprocedures.label.copyprocedure"></s:text></button>
									</s:if>
								</td>
							</tr>
						</table>
						</div>
						<div id="<s:property value="cbbProcedures.pkProcId" /><s:property value="fkProcMethId" />content"
							onclick="toggleDiv('<s:property value="cbbProcedures.pkProcId" /><s:property value="fkProcMethId" />');"
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-triangle-1-s"></span> <s:text
							name="garuda.cbbprocedures.label.cbbprocessingprocedures"></s:text>
						</div>
						<div
							id="<s:property value="cbbProcedures.pkProcId" /><s:property value="fkProcMethId" />">

						<table>
							<tr>
								<!-- <td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.procno"></s:text>:</td>
								<td width="30%" align="left"><s:textfield
									name="cbbProcedures.procNo" disabled="true" /></td> -->
									
								 <td width="20%" align="left" onmouseover="return overlib('<s:text
									name="garuda.cbbprocedures.label.procnametooltip"></s:text>');" 
							onmouseout="return nd();">
							<s:text
								name="garuda.cbbprocedures.label.procname"></s:text>:</td>
								<td width="30%" align="left"><s:textfield
									name="cbbProcedures.procName" disabled="true" /></td> 
									
								
									<s:date  name="cbbProcedures.createdOn"  format="dd-MMM-yyyy" id="datepicker10"/>
									
									<s:date name="cbbProcedures.procStartDate" id="" format="dd-MMM-yyyy" />							
									<s:set name="procDate"  value="datepicker10"/>                                  
                            <!--     
                                <s:property value="datepicker9"/> 
                                <s:property value="datepicker10"/>
                                <s:property value ="%{!(checkContantDate(#datepicker10))}"/>
                                 -->
                                   <s:if test="%{checkContantDate(#datepicker10)}">
       						             <td width="20%" align="left"><s:text name="garuda.cbuentry.label.productcode" /></td> 
       							         <td><s:textfield width="auto" name="cbbProcedures.productCode" id="productCode" disabled="true"></s:textfield> </td>        								 
        					       </s:if>
        							<s:else>
        								<!--   <s:text name="garuda.cbbprocedures.label.procversion"></s:text>:</td>
										<td width="30%" align="left">
										    <s:textfield width="auto" name="cbbProcedures.productCode" id="productCode" disabled="true"></s:textfield>
										    <s:textfield name="cbbProcedures.procVersion" disabled="true" />
										</td>	-->
        							</s:else>		
									
								<!-- <td width="20%" align="left"><s:text name="garuda.cbuentry.label.productcode" /><s:text
									name="garuda.cbbprocedures.label.procversion"></s:text>:</td>
								<td width="30%" align="left"><s:textfield name="cbbProcedures.productCode" id="productCode" disabled="true"></s:textfield><s:textfield
									name="cbbProcedures.procVersion" disabled="true" /></td> -->
									
							</tr>
							<tr>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.procstartdate"></s:text>:</td>
								<td width="30%" align="left"><s:textfield readonly="true" onkeydown="cancelBack();" 
										name="procStartDateStr" cssClass="datePicWMaxDate"
										value="%{getText('collection.date',{cbbProcedures.procStartDate})}" id="procStartDate%{#row.index}" disabled="true"/></td>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.proctermidate"></s:text>:</td>
								<td width="30%" align="left">
								<s:textfield readonly="true" onkeydown="cancelBack();" 
										name="procTermiDateStr" cssClass="datePicWMaxDate"
										value="%{getText('collection.date',{cbbProcedures.procTermiDate})}" id="procTermiDateStr%{#row.index}" disabled="true"/></td>
							</tr>
						
							 <tr>
						     <td width="20%" align="left">
						     	<s:text name="garuda.cbuentry.label.CBB" />
						     </td>
						     <td width="30%" align="left">
						       <s:select name="cbbProcedures.fkSite" listKey="siteId" listValue="siteIdentifier" list="sites" headerKey="" headerValue="Select" disabled="true"></s:select>
						     </td>
						     <td width="20%" align="left">
						     </td>
						     <td width="30%" align="left">
						     </td>
							</tr> 	
						</table>
						</div>
						</div>
						<div>
						<div id="<s:property value="cbbProcedures.pkProcId" /><s:property value='fkBagType'/>content"
							onclick="toggleDiv('<s:property value="cbbProcedures.pkProcId" /><s:property value="fkBagType"/>')"
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-triangle-1-s"></span> <s:text
							name="garuda.cbbprocedures.label.productmodification"></s:text></div>
						<div id="<s:property value="cbbProcedures.pkProcId" /><s:property value='fkBagType'/>">
						<table>
							<tr>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.processing"></s:text>:</td>
								<td width="30%" align="left"><s:select name="fkProcMethId"
									id="fkProcMethId"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING]"
									listKey="pkCodeId" listValue="description" headerKey=""
									headerValue="Select" disabled="true" /></td>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.ifautomated"></s:text>:</td>
								<td width="30%" align="left"><s:select name="fkIfAutomated"
									id="fkIfAutomated"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IF_AUTOMATED]"
									listKey="pkCodeId" listValue="description" headerKey=""
									headerValue="Select" disabled="true" /></td>
							</tr>
							<s:if test="fkIfAutomated==pkIfAutomatedOther">
								<tr>
								<td width="20%" align="left"></td>
								<td width="30%" align="left"></td>

								<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.specifyoth"></s:text>:</td>
								<td width="30%" align="left"><s:textfield
											name="otherProcessing" disabled="true" /></td>
							</tr>
							</s:if>
							
							<tr>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.productmodification"></s:text>:</td>
								<td width="30%" align="left"><s:select
									name="fkProductModification" id="fkProductModification"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PRODUCT_MODI]"
									listKey="pkCodeId" listValue="description" headerKey=""
									headerValue="Select" disabled="true" /></td>
								<td width="20%" align="left">
								</td>
								<td width="30%" align="left"></td>
							</tr>
							<s:if test="fkProductModification==pkProdModiOther">
							<tr>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.specifyoth"></s:text>:</td>
								<td width="30%" align="left"><s:textfield
									name="otherProdModi" disabled="true" /></td>
								<td width="20%" align="left"></td>
								<td width="30%" align="left"></td>
							</tr>
							</s:if>
						</table>
						</div>
						</div>
						<div>
						<div id="<s:property value="cbbProcedures.pkProcId" /><s:property value="fkStorMethod" />content"
							onclick="toggleDiv('<s:property value="cbbProcedures.pkProcId" /><s:property value="fkStorMethod" />')"
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-triangle-1-s"></span> <s:text
							name="garuda.cbbprocedures.label.storageconditions"></s:text></div>
						<div id="<s:property value="cbbProcedures.pkProcId" /><s:property value='fkStorMethod'/>">
						<table>
							<tr>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.stormethod"></s:text>:</td>
								<td width="30%" align="left"><s:select name="fkStorMethod"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@STORAGE_METHOD]"
									listKey="pkCodeId" listValue="description" headerKey=""
									headerValue="Select" disabled="true" /></td>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.freezmanufac"></s:text>:</td>
								<td width="30%" align="left"><s:select
									name="fkFreezManufac" id="fkFreezManufac"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FREEZER_MANUFACTURER]"
									listKey="pkCodeId" listValue="description" headerKey=""
									headerValue="Select" disabled="true" /></td>
							</tr>
							<s:if test="fkFreezManufac==pkFreezManuOther">
							<tr>
								<td width="20%" align="left"></td>
								<td width="30%" align="left"></td>

								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.freezmanufac"></s:text> - <s:text
									name="garuda.cbbprocedures.label.specifyoth"></s:text>:</td>
								<td width="30%" align="left"><s:textfield
									name="otherFreezManufac" disabled="true" /></td>
							</tr>
							</s:if>
							<tr>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.frozenin"></s:text>:</td>
								<td width="30%" align="left"><s:select name="fkFrozenIn"
									id="fkFrozenIn"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FROZEN_IN]"
									listKey="pkCodeId" listValue="description" headerKey=""
									headerValue="Select" disabled="true" /></td>
								<td width="20%" align="left"></td>
								<td width="30%" align="left"></td>
							</tr>
							<s:if test="fkFrozenIn==pkFrozenInBag">
							<tr>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.noofbags"></s:text>:</td>
								<td width="30%" align="left"><s:select
									name="noOfBags" id="fkNoOfBags"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NUM_OF_BAGS]"
									listKey="pkCodeId" listValue="description" headerKey=""
									headerValue="Select" disabled="true"/></td>
								<td width="20%" align="left"></td>
								<td width="30%" align="left"></td>
						 	</tr>
							</s:if>
							<s:if test="fkFrozenIn==pkFrozenInOther">
							<tr>
								<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
								</s:text>
								</td>
								<td width="30%" align="left"><s:textfield name="otherFrozenCont" disabled="true"></s:textfield>
								</td>
							</tr>
							</s:if>
							<s:if test="noOfBags==pkBag1Type">
								<tr>
								<td align="left" colspan="4">
								<table>
								<tr>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.bagtype"></s:text>:</td>
								<td width="30%" align="left"><s:select
									name="fkBag1Type" id="fkBag1TypeView"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BAG_TYPE]"
									listKey="pkCodeId" listValue="description" headerKey=""
									headerValue="Select"  disabled="true"/></td>
								</tr>
								<tr>
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.cryobagmanufac"></s:text>:</td>
									<td width="30%" align="left"><s:textfield
										name="cryoBagManufac"
										id="cryoBagManufacView" maxlength="50" disabled="true"/></td>
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.maxvalue"></s:text>(<s:text
										name="garuda.cbbprocedures.label.ml"></s:text>):</td>
									<td width="30%" align="left"><s:textfield
										name="maxValue" id="maxValueView"
										disabled="true"/></td>
								</tr>
								<s:if test="fkBag1Type==pkOtherBagType">
								<tr>
								<td width="50%" align="left" colspan="2">
										<table>
											<tr>
												<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
												</s:text>
												</td>
												<td width="30%" align="left"><s:textfield name="otherBagType1" id="otherBagType1View" disabled="true"></s:textfield>
												</td>
											</tr>
										</table>
								</td>
								</tr>
								</s:if>
								
								</table>
								</td>
								</tr>
							</s:if>
							<s:if test="noOfBags==pkBag2Type">
								<tr>
								<td align="left" colspan="4">
								<table>
								<tr>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.bag1type"></s:text>:</td>
								<td width="30%" align="left"><s:select
									name="fkBag1Type" id="fkBag1TypeView"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BAG_TYPE]"
									listKey="pkCodeId" listValue="description" headerKey=""
									headerValue="Select"  disabled="true"/></td>
								</tr>
								<tr>
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.cryobagmanufac.bagtype1"></s:text>:</td>
									<td width="30%" align="left"><s:textfield
										name="cryoBagManufac"
										id="cryoBagManufacView" maxlength="50" disabled="true"/></td>
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.maxvalue.bagtype1"></s:text>(<s:text
										name="garuda.cbbprocedures.label.ml"></s:text>):</td>
									<td width="30%" align="left"><s:textfield
										name="maxValue" id="maxValueView"
										disabled="true"/></td>
								</tr>
								<s:if test="fkBag1Type==pkOtherBagType">
								<tr>
								<td width="50%" align="left" colspan="2">
										<table>
											<tr>
												<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
												</s:text>
												</td>
												<td width="30%" align="left"><s:textfield name="otherBagType1" id="otherBagType1View" disabled="true"></s:textfield>
												</td>
											</tr>
										</table>
								</td>
								</tr>
								</s:if>
								
								</table>
								</td>
								</tr>
								<tr>
								<td align="left" colspan="4">
								<table>
								<tr>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.bag2type"></s:text>:</td>
								<td width="30%" align="left"><s:select
									name="fkBag2Type" id="fkBag2TypeView"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BAG_TYPE]"
									listKey="pkCodeId" listValue="description" headerKey=""
									headerValue="Select" disabled="true"/></td>
								</tr>
								<tr>
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.cryobagmanufac.bagtype2"></s:text>:</td>
									<td width="30%" align="left"><s:textfield
										name="cryoBagManufac2"
										id="cryoBag2ManufacView" disabled="true"/></td>
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.maxvalue.bagtype2"></s:text>(<s:text
										name="garuda.cbbprocedures.label.ml"></s:text>):</td>
									<td width="30%" align="left"><s:textfield
										name="maxValue2" disabled="true"/></td>
								</tr>
								<s:if test="fkBag2Type==pkOtherBagType">
								<tr>
								<td width="50%" align="left" colspan="2">
										<table>
											<tr>
												<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
												</s:text>
												</td>
												<td width="30%" align="left"><s:textfield name="otherBagType2" id="otherBagType2View" disabled="true"></s:textfield>
												</td>
											</tr>
										</table>
								</td>
								</tr>
								</s:if>
								</table>
								</td>
								</tr>
							</s:if>
							<s:if test="noOfBags==pkNoOfBagsOthers">
							<tr>
								<td width="50%" align="left" colspan="2">
										<table>
											<tr>
												<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
												</s:text>
												</td>
												<td width="30%" align="left"><s:textfield name="otherBagType" id="otherBagTypeView" disabled="true"></s:textfield>
												</td>
											</tr>
										</table>
								</td>
							</tr>
							</s:if>
							<tr>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.stortemp"></s:text>:</td>
									
								<td width="30%" align="left">
								<select name="fkStorTemp" disabled="true">
								<option value="">Select</option>
								<s:iterator value = "#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@STORAGE_TEMPERATURE]">
									<s:if test="%{pkCodeId==fkStorTemp}">
										<option value ='<s:property value='%{pkCodeId}'/>' selected="selected"><s:property value="%{description}" escapeHtml="false"/></option>
									</s:if><s:else>
										<option value ='<s:property value='%{pkCodeId}'/>'> <s:property value="%{description}" escapeHtml="false"/></option>
									</s:else>
								</s:iterator>
								</select> </td>
									
									
								<td width="20%" align="left"></td>
								<td width="30%" align="left"></td>
							</tr>
							<tr>
								<td width="20%" align="left"><s:text
									name="garuda.cbbprocedures.label.controlledratefreezing"></s:text>:</td>
								<td width="30%" align="left"><s:select
									name="fkContrlRateFreezing"
									list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CONTR_RATE_FREEZ]"
									listKey="pkCodeId" listValue="description" headerKey=""
									headerValue="Select" disabled="true" /></td>
								<td width="20%" align="left" onmouseover="return overlib('<s:text
									name="garuda.cbbprocedure.label.numberThawedtooltip"></s:text>');" 
							        onmouseout="return nd();">
									<s:text
									name="garuda.cbbprocedures.label.noofindifrac"></s:text>:</td>
								<td width="30%" align="left"><s:textfield
									name="noOfIndiFrac" disabled="true" /></td>
							</tr>
						</table>
						</div>
						</div>

						<div>
						<div id="<s:property value="cbbProcedures.pkProcId" /><s:property value="fkContrlRateFreezing" />content"
							onclick="toggleDiv('<s:property value="cbbProcedures.pkProcId" /><s:property value="fkContrlRateFreezing" />')"
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-triangle-1-s"></span> <s:text
							name="garuda.cbbprocedures.label.frozenproductcomposition"></s:text>
						</div>
						<div id="<s:property value="cbbProcedures.pkProcId" /><s:property value='fkContrlRateFreezing'/>">
						<fieldset><legend> <s:text
							name="garuda.cbbprocedures.label.additivesbeforevolumereductions"></s:text>
						</legend>
						<table>
						<tr>
							
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
									
						</tr>
						<tr>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.thouunitpermlheparin"></s:text>:
							</td>
							<td width="15%"><s:textfield
								name="thouUnitPerMlHeparin" disabled="true">
								</s:textfield></td>
							<td width="15%"><s:textfield
								name="thouUnitPerMlHeparinper" disabled="true">
								</s:textfield></td>
							
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.cpda"></s:text>:</td>
							
							<td width="15%"><s:textfield
								name="cpda" disabled="true"></s:textfield></td>
							<td width="15%"><s:textfield
								name="cpdaper" disabled="true"></s:textfield></td>
				
							
							
						</tr>
						<tr>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.fiveunitpermlheparin"></s:text>:
							</td>
							<td width="15%"><s:textfield
								name="fiveUnitPerMlHeparin" disabled="true">
							</s:textfield></td>
							
							
							<td width="15%"><s:textfield
								name="fiveUnitPerMlHeparinper" disabled="true">
								</s:textfield></td>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.cpd"></s:text>:</td>
							<td width="15%"><s:textfield name="cpd" disabled="true"></s:textfield></td>
							<td width="15%"><s:textfield name="cpdper" disabled="true"></s:textfield></td>
						
						</tr>
						<tr>
								<td width="20%"><s:text
								name="garuda.cbbprocedures.label.tenunitpermlheparin"></s:text>:
							</td>
							<td width="15%"><s:textfield
								name="tenUnitPerMlHeparin" disabled="true"></s:textfield></td>
							
							<td width="15%"><s:textfield
								name="tenUnitPerMlHeparinper" disabled="true"></s:textfield></td>
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.acd"></s:text>:</td>
							<td width="15%"><s:textfield name="acd" disabled="true"></s:textfield></td>

							<td width="15%"><s:textfield name="acdper" disabled="true"></s:textfield></td>
						</tr>
						<tr>
								
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.sixperhydroxyethylstarch"></s:text>:
							</td>
							<td width="15%"><s:textfield
								name="sixPerHydroxyethylStarch" disabled="true"></s:textfield></td>

							<td width="15%"><s:textfield
								name="sixPerHydroxyethylStarchper" disabled="true"></s:textfield></td>
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.otheranticoagulant"></s:text>:</td>
							<td width="15%"><s:textfield
								name="otherAnticoagulant"
								disabled="true"></s:textfield></td>

							<td width="15%"><s:textfield
								name="otherAnticoagulantper"
								disabled="true"></s:textfield></td>
						</tr>


						<s:if test="(otherAnticoagulant != null)  || (otherAnticoagulantper != null)">
						
						<tr id="otherAnticoagulantDivAdd"> 
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.specifyothanti"></s:text>:</td>
									<td width="30%" align="left" colspan="2"><s:textfield
										name="specifyOthAnti"
										disabled="true"/></td>
									<td width="20%" align="left"></td>
									<td width="30%" align="left" colspan="2"></td>
						</tr>
						</s:if>
					</table>
						</fieldset>
						<fieldset><legend> <s:text
							name="garuda.cbbprocedures.label.cryoprotectantsandotheradditives"></s:text>:
						</legend>
						<table>
						<tr>
							
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
									
						</tr>
						<tr>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.hunperdmso"></s:text>:</td>
							<td width="15%"><s:textfield
								name="hunPerDmso" disabled="true"></s:textfield></td>
							<td width="15%"><s:textfield
								name="hunPerDmsoper" disabled="true"></s:textfield></td>
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.twenfivehumalbu"></s:text>:</td>
							<td width="15%"><s:textfield
								name="twenFiveHumAlbu" disabled="true"></s:textfield></td>
							<td width="15%"><s:textfield
								name="twenFiveHumAlbuper" disabled="true"></s:textfield></td>
							
							
						</tr>
						<tr>
							
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.hunperglycerol"></s:text>:</td>
							<td width="15%"><s:textfield
								name="hunPerGlycerol" disabled="true"></s:textfield></td>
							<td width="15%"><s:textfield
								name="hunPerGlycerolper" disabled="true"></s:textfield></td>
						
						    <td width="20%"><s:text
								name="garuda.cbbprocedures.label.plasmalyte"></s:text>:</td>
							<td width="15%"><s:textfield
								name="plasmalyte" disabled="true"></s:textfield></td>	
							<td width="15%"><s:textfield
								name="plasmalyteper" disabled="true"></s:textfield></td>
							
				
						</tr>
						<tr>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.tenperdextran40"></s:text>:</td>
							<td width="15%"><s:textfield
								name="tenPerDextran_40" disabled="true"></s:textfield></td>
							
							<td width="15%"><s:textfield
								name="tenPerDextran_40per" disabled="true"></s:textfield></td>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.othcryoprotectant"></s:text>:</td>
							<td width="15%"><s:textfield
								name="othCryoprotectant"
								disabled="true"></s:textfield></td>
							<td width="15%"><s:textfield
								name="othCryoprotectantper"
								disabled="true"></s:textfield></td>
						</tr>
						<tr>
							
										
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.fiveperhumanalbu"></s:text>:</td>
							<td width="15%"><s:textfield
								name="fivePerHumanAlbu" disabled="true"></s:textfield></td>
							<td width="15%"><s:textfield
								name="fivePerHumanAlbuper" disabled="true"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%" colspan="2"></td>
							
						</tr>
						
						<s:if test="(othCryoprotectant != null)  || (othCryoprotectantper != null)">
						<tr id="otherCryoprotectantDivDis">
							<td width="20%" align="left"><s:text
								name="garuda.cbbprocedures.label.specothcryopro"></s:text>:</td>
							<td width="30%" align="left" colspan="2"><s:textfield
								name="specOthCryopro"
								disabled="true"/></td>
							
							<td width="20%" align="left"></td>
							<td width="30%" align="left" colspan="2"></td>						
						</tr>
						</s:if>
					</table>
						</fieldset>
						<fieldset><legend> <s:text
							name="garuda.cbbprocedures.label.diluents"></s:text> </legend>
						<table width="100%">
						<tr>
							
							<td width="20%">
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
							</td>
							<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
							</td>
							<td width="20%">
							</td>
							<td width="15" align="center">
							</td>
							<td width="15" align="center">
							</td>
									
						</tr>
						<tr>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.fiveperdextrose"></s:text>:</td>
							<td width="15%"><s:textfield
								name="fivePerDextrose" disabled="true"></s:textfield></td>
							<td width="15%"><s:textfield
								name="fivePerDextroseper" disabled="true"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%"colspan="2"></td>
						</tr>
						<tr>	
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.pointninepernacl"></s:text>:</td>
							<td width="15%"><s:textfield
								name="pointNinePerNacl" disabled="true"></s:textfield></td>
							<td width="15%"><s:textfield
								name="pointNinePerNaclper" disabled="true"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%"colspan="2"></td>	
						</tr>
						<tr>
							<td width="20%"><s:text
								name="garuda.cbbprocedures.label.otherdiluents"></s:text>:</td>
							<td width="15%"><s:textfield
								name="othDiluents" disabled="true"></s:textfield></td>
							<td width="15%"><s:textfield
								name="othDiluentsper" disabled="true"></s:textfield></td>
							<td width="20%"></td>
							<td width="30%"colspan="2"></td>
						</tr>
						<s:if test="(othDiluents != null)  || (othDiluentsper != null)">
						
						<tr id="otherDiluentsDivDis">
									<td width="20%" ><s:text
										name="garuda.cbbprocedures.label.specothdiluents"></s:text>:</td>
									<td width="30%"  colspan="2"><s:textfield
										name="specOthDiluents" disabled="true"> </s:textfield></td>
									<td width="20%" ></td>
									<td width="30%"  colspan="2"></td>
						</tr>
						</s:if>
						
					</table>
						</fieldset>
						</div>
						</div>

						<div>
						<div id="<s:property value="cbbProcedures.pkProcId" /><s:property value="%{filterPaper}" />_cbusmplcontent"
							onclick="toggleDiv('<s:property value="cbbProcedures.pkProcId" /><s:property value="%{filterPaper}" />_cbusmpl')"
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-triangle-1-s"></span> <s:text
							name="garuda.cbbprocedures.label.cbusamples"></s:text></div>
						<div id="<s:property value="cbbProcedures.pkProcId" /><s:property value='filterPaper'/>_cbusmpl">
						<table>
							<tr>
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.filterpaper"></s:text>:</td>
								<td width="30%"><s:textfield name="filterPaper"
									disabled="true"></s:textfield></td>
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.rbcpellets"></s:text>:</td>
								<td width="30%"><s:textfield name="rbcPallets"
									disabled="true"></s:textfield></td>
							</tr>
							<tr>
								
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.noofextrdnaaliquots"></s:text>:
								</td>
								<td width="30%"><s:textfield name="noOfExtrDnaAliquots"
									disabled="true"></s:textfield></td>
									
								<td width="20%" align="left" onmouseover="return overlib('<s:text
									name="garuda.cbbprocedure.label.numberSerumToolTip"></s:text>');" 
									onmouseout="return nd();">
							<s:text
									name="garuda.cbbprocedures.label.noofserumaliquots"></s:text>:</td>
								<td width="30%"><s:textfield name="noOfSerumAliquots"
									disabled="true"></s:textfield></td>	
							</tr>
							<tr>
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.noofplasmaaliquots"></s:text>:
								</td>
								<td width="30%"><s:textfield name="noOfPlasmaAliquots"
									disabled="true"></s:textfield></td>
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.noofnonviablealiquots"></s:text>:
								</td>
								<td width="30%"><s:textfield name="noOfNonviableAliquots"
									disabled="true"></s:textfield></td>
								
							</tr>
							<tr>
								
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.noofviablesampnotrep"></s:text>:
								</td>
								<td width="30%"><s:textfield name="noOfViableSampleNotRep"
									disabled="true"></s:textfield></td>
								<td width="20%"></td>
								<td width="30%"></td>
							</tr>	
							</table>	
							
							<fieldset>
							<legend>
								<s:text name="garuda.cbbprocedures.label.aliqrepoffinalprod"></s:text>
							</legend>
							<table>	
							<tr>
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.noofsegments"></s:text>:</td>
								<td width="30%"><s:textfield name="noOfSegments"
									disabled="true"></s:textfield></td>
								<td width="20%"></td>
								<td width="30%"></td>
							</tr>
							<tr>
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.noofothrepaliqprod"></s:text>:</td>
								<td width="30%"><s:textfield name="noOfOthRepAliqProd"
									disabled="true"></s:textfield></td>
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.noofothrepaliqaltcond"></s:text>:
								</td>
								<td width="30%"><s:textfield
									name="noOfOthRepAliqAltCond" disabled="true"></s:textfield>
								</td>
							</tr>
							</table>
							</fieldset>
						</div>
						</div>
							

				
						<div>
						<div id="<s:property value="cbbProcedures.pkProcId" /><s:property value="%{noOfSerumMaterAliquots}" />_matsmplcontent"
							onclick="toggleDiv('<s:property value="cbbProcedures.pkProcId" /><s:property value="%{noOfSerumMaterAliquots}" />_matsmpl')"
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-triangle-1-s"></span> <s:text
							name="garuda.cbbprocedures.label.maternalsamples"></s:text></div>
						<div id="<s:property value="cbbProcedures.pkProcId" /><s:property value='noOfSerumMaterAliquots'/>_matsmpl">
						<table>
							<tr>
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.noofserummateraliquots"></s:text>:
								</td>
								<td width="30%"><s:textfield name="noOfSerumMaterAliquots"
									disabled="true"></s:textfield></td>
								
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.noofplasmamateraliquots"></s:text>:
								</td>
								<td width="30%"><s:textfield name="noOfPlasmaMaterAliquots"
									disabled="true"></s:textfield></td>
							</tr>
							<tr>
								
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.noofextrdnamateraliquots"></s:text>:
								</td>
								<td width="30%"><s:textfield name="noOfExtrDnaMaterAliquots"
									disabled="true"></s:textfield></td>
								
								<td width="20%" align="left" onmouseover="return overlib('<s:text
									name="garuda.cbbprocedure.label.numberMisMatToolTip"></s:text>');" 
									onmouseout="return nd();"><s:text
									name="garuda.cbbprocedures.label.noofcellmateraliquots"></s:text>:</td>
								<td width="30%"><s:textfield name="noOfCellMaterAliquots"
									disabled="true"></s:textfield></td>
							</tr>
							
						</table>
						</div>
						</div>

						<!-- <div>
						<div id="<s:property value="cbbProcedures.pkProcId" /><s:property value="fkStorTemp" />content"
							onclick="toggleDiv('<s:property value="cbbProcedures.pkProcId" /><s:property value="fkStorTemp" />')"
							class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span
							class="ui-icon ui-icon-triangle-1-s"></span> <s:text
							name="garuda.cbbprocedures.label.sop"></s:text></div>
						<div
							id="<s:property value="cbbProcedures.pkProcId" /><s:property value='fkStorTemp'/>">
						<table>
							<tr>
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.soprefno"></s:text>:</td>
								<td width="30%"><s:textfield name="sopRefNo"
									disabled="true"></s:textfield></td>
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.soptitle"></s:text>:</td>
								<td width="30%"><s:textfield name="sopTitle"
									disabled="true"></s:textfield></td>
							</tr>
							<tr>
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.sopstrtdate"></s:text>:</td>
								<td width="30%"><s:date name="sopStrtDate" id="datepicker4"
									format="MMM dd, yyyy" /> <s:textfield readonly="true" onkeydown="cancelBack();" 
									name="sopStrtDateStr" class="datepic" id="datepicker4"
									disabled="true" value="%{datepicker4}" Cssclass="datePicWMaxDate"/></td>
								<td width="20%"><s:text
									name="garuda.cbbprocedures.label.soptermidate"></s:text>:</td>
								<td width="30%"><s:date name="sopTermiDate"
									id="datepicker5" format="MMM dd, yyyy" /> <s:textfield
									readonly="true" onkeydown="cancelBack();"  name="sopTermiDateStr" class="datepic"
									id="datepicker5" disabled="true" value="%{datepicker5}" Cssclass="datePicWMaxDate"/></td>
							</tr>
							
						</table>
						</div>
						</div>-->
						<table>
							<tr>
								<td colspan="4">
								<fieldset><legend><s:text
									name="garuda.cbbprocedures.label.lastmodifby"></s:text> </legend>
								<table>
									<tr>
										<td width="20%"><s:text name="garuda.cbbprocedures.label.uid"></s:text>:
										</td>
										<td width="30%">
										
											<!--<s:property value="lastModifiedBy" />-->
											<s:if test="lastModifiedBy!=null">
												<s:set name="eligibleReviewName" value="%{lastModifiedBy}" scope="request"/>	
											</s:if>
											<s:else>
											<s:set name="eligibleReviewName" value="%{createdBy}" scope="request"/>
											</s:else>
										
									 			<%
													String finaldetails = request.getAttribute("eligibleReviewName").toString();
													UserJB user = new UserJB();
													user.setUserId(EJBUtil.stringToNum(finaldetails));
													user.getUserDetails();													
												%>	
											 <%=user.getUserLastName()+" "+user.getUserFirstName() %>   
										</td>
										<td width="20%"><s:text name="garuda.clinicalnote.label.dateTime"></s:text>:
										</td>
										<td width="30%">
											<s:if test="lastModifiedOn!=null">
												<s:date name="lastModifiedOn" id="datepicker2"
							format="MMM dd, yyyy / HH:mm" /><s:property value="datepicker2" />
											</s:if>
											<s:else>
												<s:date name="createdOn" id="datepicker2"
							format="MMM dd, yyyy / HH:mm" /><s:property value="datepicker2" />
											</s:else>
											
										</td>
										<td></td>
										<td></td>
									</tr>
								</table>
								</fieldset>
								</td>
							</tr>
						</table>
						</div>
						</div>
						</div>
						</td>
					</tr>
				</s:iterator>
			</table>
			</div>
			</td>
		</tr>
	</table>
</s:form>
</div>
</div>
<script>
function cbbprocedurescreen()
{	
	var logi = $j("#loginIds").val();
	var ids = logi.split(",");
	if(logi=="'0'" || logi == "" || logi == 0){
		alert("<s:text name="garuda.cbu.procedure.select_alert"/>");	
		return;
	}
	else if(ids.length == 2){
		
		var url = cotextpath+"/jsp"+"/cbbProcedureScreen.action?statusUpdate=False&pkProcId="+logi+"&loginIds="+logi+'&moduleEntityId='+logi+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_PROCESSING_PROCEDURE"/>&moduleEntityIdentifier='+$j('#pName').val()+'&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_PRINT_PROS_PROC"/>';
		window.open( url);
	}
	else 
	{
		showModal('Procedures Print List','showProcPrintList?loginIds='+logi+'&moduleEntityId='+$j("#procId").val()+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_PROCESSING_PROCEDURE"/>&moduleEntityIdentifier='+$j("#procName").val()+'&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_PRINT_PROS_PROC"/>','170','250');
	} 
	/*
	if(logi != 0){	
		var ids = logi.split(",");
		for(var i=2;i<ids.length;i++){
		   if(ids[i]!=0){
		   alert("please select only one procedure");
		   return;
		   }	
		}
		var url = cotextpath+"/jsp"+"/cbbProcedureScreen.action?statusUpdate=False&pkProcId="+logi+"&loginIds="+logi;
		window.open( url);
	}
	else
		alert("please select one procedure");*/
}
</script>
<jsp:include page="./cb_track_session_logging.jsp" />