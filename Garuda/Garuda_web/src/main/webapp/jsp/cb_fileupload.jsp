<%@taglib prefix="s" uri="/struts-tags"%>
<s:if test="#request.fileUpload==true" >
<script>
    var documentId ='<s:property value="attachmentPojo.attachmentId"/>';
	if(documentId != null && documentId != ""){
		window.opener.document.getElementById("attachmentId").value = documentId;
		window.close();
	}
</script>
</s:if>
<div id="newdocpop" class="col_100 left ">
	<s:form action="adddocument" method="post" enctype="multipart/form-data" id="newDocumentForm"  ><!--
	<s:hidden name="attachmentPojo.studyId" id="studyId" value="%{studyId}"/>
	<s:hidden name="velosDocumentPojo.pageType" id="pageType" value="%{pageType}" />
	<s:hidden name="velosDocumentPojo.documentId" id="documentId" value="%{velosDocumentPojo.documentId}" />
	
		-->
		<s:hidden name="attCodeType" value="%{attCodeType}" id="attCodeType"></s:hidden>
		<s:hidden name="attCodeSubType" value="%{attCodeSubType}"></s:hidden>
		<s:hidden name="codeEntityType" value="%{codeEntityType}"></s:hidden>
		<s:hidden name="codeEntitySubType" value="%{codeEntitySubType}"></s:hidden>
		<s:hidden name="cordID" value="%{cordID}"></s:hidden>
		<s:hidden name="attachmentPojo.attachmentId" id="attachId" ></s:hidden>
		<input type="hidden" name="attachId1" id="attachId1" value="%{attachmentPojo.attachmentId}" />
		<table class="col_100">
		<s:if test="#request.fileUpload==true" >
		<tr><td colspan="2">
							<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
							<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
							<strong><s:text name="garuda.cbu.fileattach.uploadSuccessAlert"/>!</strong></p>
							</div></td></tr>
		</s:if>
		<tr><td > <s:text name="garuda.unitreport.label.selectyourfile" />:</td>
			<td>  	<s:file name="fileUpload" /> </td>
		</tr>
		<tr> 
			<td colspan="2" align="center"> 
			 	<!--  <input type="button" value="submit" name="submit" onclick="saveDocuments();" /> -->
			 <button type="submit" ><s:text name="garuda.unitreport.label.button.submit"/></button> 
			  	
			</td>
		</tr>
		</table>
	</s:form>		
</div>	
