<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
	<!--<div id="idscontent" onclick="toggleDiv('ids')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text
				name="garuda.cdrcbuview.label.ids" /></div>
			<div id="ids">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr align="left">
					<td width="20%;"><s:text name="garuda.cbuentry.label.externalcbuid" />:
					</td>
					<td width="30%;"><s:textfield disabled="true"
						name="CdrCbuPojo.externalCbuId" id="externalCbuId" readonly="true" onkeydown="cancelBack();"  />
					</td>					
					<td width="50%;" colspan="2"></td>
				</tr >
				<tr align="left">
				  <td width="20%;"><s:text name="garuda.cbuentry.label.registrycbuid"/>:
				        <s:if test="#request.registryIdOnBag==true">
						   <span style="color: red;">(ID On Bag)</span>
						</s:if>					
				  </td>
				  <td width="30%;"><s:textfield name="cdrCbuPojo.registryId" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
				  </td>
				  <td width="20%;"><s:text name="garuda.cbuentry.label.maternalregistryid"/>:</td>				  
				  <td width="30%;"><s:textfield name="cdrCbuPojo.registryMaternalId" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
				  </td>
				</tr >
				<tr align="left">
				  <td width="20%;"><s:text name="garuda.cbuentry.label.localcbuid"/>:
				    <s:if test="#request.localIdOnBag==true">
						   <span style="color: red;">(ID On Bag)</span>
					</s:if>
				  </td>
				  <td width="30%;"><s:textfield name="cdrCbuPojo.localCbuId" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
				  </td>
				  <td width="20%;"><s:text name="garuda.cbuentry.label.maternallocalid"/>:</td>
				  <td width="30%;"><s:textfield name="cdrCbuPojo.localMaternalId" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
				  </td>
				</tr>
				<tr align="left"><td width="20%;"><s:text name="garuda.cordentry.label.isbtid"/>:
				       <s:if test="#request.isbtDinOnBag==true">
						   <span style="color: red;">(ID On Bag)</span>
					   </s:if></td> 
				    <td width="30%;"><s:textfield id="isbidin" name="cdrCbuPojo.cordIsbiDinCode" readonly="true" onkeydown="cancelBack();"  ></s:textfield>
					</td>
			         <td width="20%;"><s:text name="garuda.cdrcbuview.label.isbt_product_code"/>:
				  </td><td width="30%;"><s:textfield name="cdrCbuPojo.cordIsitProductCode" readonly="true" onkeydown="cancelBack();"  ></s:textfield>					              
				   </td>
			   </tr>			   	   
			</table>
			</div>
			--><jsp:include page="cb_viewcbuinfo.jsp"></jsp:include>
			<div id="cbuNotescontent" onclick="toggleDiv('cbuNotes')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;">
				<span class="ui-icon ui-icon-triangle-1-s"></span> 
				<s:text name="garuda.cdrcbuview.label.button.cbuinfoclinicalnotes" />
			</div>
			<table cellpadding="0" cellspacing="0" width="100%">
			<tr><td>				
				<div id="cbuNotes" style=" width: 463px; height: 100%;">
					<div id="loadCbuCharacteristicClinicalNoteDiv">
							<s:set name="cbuCateg" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CLINIC_CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_NOTE_CATEGORY_CODESUBTYPE) " />
							<jsp:include page="modal/cb_load_clinicalNote.jsp"></jsp:include>
					</div>
			<s:if test="hasNewPermission(#request.vClnclNote)==true">			
			<div>
			<%-- <s:if test='(cbuFinalReviewPojo.eligibleConfirmFlag == 1 && cbuFinalReviewPojo.licensureConfirmFlag == 1 && cbuFinalReviewPojo.finalReviewConfirmStatus == "Y" ) || (cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)) '> --%>
			<s:if test='(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)) '>
			<button type="button" disabled = "disabled" ><s:text name="garuda.cdrcbuview.label.button.addnotes" /></button>
			</s:if>
			<s:else>
			<button type="button"
				onclick="fn_showModalCbuNotes('Clinical Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','clinicalNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&cat=CBU_NOTE_CATEGORY_CODESUBTYPE','550','850','notesModalsDiv');"><s:text name="garuda.cdrcbuview.label.button.addnotes" /></button>
			</s:else>
			</div>
		
			</s:if>
			</div></td></tr><tr><td>
			<div id="loadCbuDocInfoDiv">
			<s:if test="(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_INFORMATION_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_INFORMATION_CODESUBTYPE].size > 0 && hasViewPermission(#request.viewCbuInfocat)==true) ||(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size > 0 && hasViewPermission(#request.viewUnitRepCat)==true)">
			<div id="loadCbuDocInfocontent" onclick="toggleDiv('loadCbuDocInfo')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;">
				<span class="ui-icon ui-icon-triangle-1-s"></span> 
				<s:text name="garuda.uploaddoc.label.cbuinfouploaddocument" />
			</div>
			<div id="loadCbuDocInfo">			
			<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_INFORMATION_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_INFORMATION_CODESUBTYPE].size > 0||#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null ||#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size>0">
		  <s:if test="hasViewPermission(#request.viewCbuInfocat)==true">
		   <s:set name="cbuDocCateg" value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_INFORMATION_CODESUBTYPE"  scope="request" />
		  </s:if> 				
						<jsp:include page="modal/cb_load_upload_document.jsp">
						<jsp:param value="<%=request.getAttribute(\"cbuDocCateg\")%>" name="cbuDocCateg"/>
						</jsp:include>						
			
			</s:if>
			</div>
			</s:if>
			</div>
			</td>
			</tr>
			</table>
			<script>
			function fn_showModalCbuNotes(title,url,hight,width,id){
			var patientId = $j('#patientId').val();
			var patient_data = '';
			if(patientId!="" && patientId != null && patientId != "undefined"){
				patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
			}
			title=title+patient_data;
			showModals(title,url,hight,width,id);
			}
			</script>