<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<div id="idmForm">
	<div>
		<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
						<table width="100%">
							<tr>
								<td colspan="6" align="center">
									<b><s:text name="garuda.productinsert.label.productinsertreportIDM" /><br>
									<s:text name="garuda.productinsert.label.cbbfull" />:<s:text name="site.siteIdentifier" />
								</b></td>
							</tr>
						</table>
					</div>
		<table>
			<tr>
					<td width="25%"><b><s:text name="garuda.productinsert.label.nmdpcbuid" /></b>:</td>
					<td width="20%"><s:property value="cdrCbuPojo.registryId" /></td>
					<td colspan="2" width="30%"/>
					<td width="15%"><b><s:text name="garuda.unitreport.label.localcbuid" /></b>:</td>
					<td width="10%"><s:property value="cdrCbuPojo.localCbuId" /></td>
								
			</tr>
			<tr>
					<td width="15%"><b><s:text name="garuda.productinsert.label.nmdpmaternalid" /></b>:</td>
					<td width="15%"><s:property value="cdrCbuPojo.registryMaternalId" /></td>
					<td width="15%"><b><s:text name="garuda.productinsert.label.formnum" /></b>:</td>
					<td width="10%"><s:property value="Unknown" /></td>
					<td width="25%"><b><s:text name="garuda.unitreport.label.localmaternalid" /></b>:</td>
					<td width="20%"><s:property value="cdrCbuPojo.localMaternalId" /></td>
								
			</tr>
			<tr>
								
								<td width="15%"><b><s:text
									name="garuda.productinsert.label.validationprtrule" />:</b></td>
								<td width="15%"><s:property value="Unknown" /></td>
								<td width="15%"><b><s:text
									name="garuda.productinsert.label.donrefnumber" />:</b></td>
								<td width="10%"><s:property value="NOT Avail"/></td>
								<td width="25%"><b><s:text
									name="garuda.cdrcbuview.label.collection_date" />:</b></td>
								<td width="20%">
								<s:if test="%{cdrCbuPojo.specimen.specCollDate != null }">
									<s:text name="collection.date">:
										<s:param name="value" value="cdrCbuPojo.specimen.specCollDate" />
									</s:text>
								</s:if>
								</td>
							</tr>
		</table>
	</div>
	<div id="IDMinformation" class="IDMinformation">
	<table border="0" class="displaycdr" id="idmsearchResults" >
	<thead>
		<tr>
			<th width="60%" colspan="2" align="left"><s:text name="garuda.cbuentry.label.testname" /></th>
			<th width="20%" colspan="2" align="left"><s:text name="garuda.openorder.label.status" /></th>
			<th width="20%" colspan="2" align="left"><s:text name="garuda.openorder.label.testdate" /></th>
		</tr>
	</thead>
	<tbody>
	<tr id="idmsubques1">
		<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IDMTEST_QUES]">
		<s:if test="%{subType=='subques1'}">
		<td width="60%" colspan="2">
			<s:hidden name="pkCodeId" id="pkforques1"></s:hidden>
			<s:property value="%{description}"/>
		</td>
		
		<s:iterator value="idmQuestionList" var="idmpojo">
		<s:set name="cmsapprovedlabDesc" value="%{cmsapprovedlab}" />
		<td width="20%" colspan="2">
			<s:property value="getCodeListDescById(#cmsapprovedlabDesc)"/>
			<%-- <s:select  name="idmpojo.cmsapprovedlab" id="subques1" disabled="true" cssClass="idmMandatory"
				list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IDMTEST_SUBQUES]"
				listKey="pkCodeId" listValue="description"  value="%{cmsapprovedlab}" headerKey="" headerValue="Select"/> --%>
		</td>
		
		</s:iterator>
		<td width="20%" colspan="2"></td>
		</s:if>
		</s:iterator>
	</tr>
	<tr id="idmsubques2">		
		<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IDMTEST_QUES]">
		<s:if test="%{subType=='subques2'}">
		<td width="60%" colspan="2">
			<s:hidden name="pkCodeId" id="pkforques2"></s:hidden>
			<s:property value="%{description}"/>
		</td>
		<s:iterator value="idmQuestionList" var="idmpojo">
		<td width="20%" colspan="2">
			<s:hidden name="idmpojo.pkidm" id="pk_subques2" value="%{pkidm}"></s:hidden>
			<s:set name="fdalicensedlabDesc" value="%{fdalicensedlab}" />
			<s:property value="getCodeListDescById(#fdalicensedlabDesc)"/>
			<%-- <s:select  name="idmpojo.fdalicensedlab" id="subques2" disabled="true" cssClass="idmMandatory"
				list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IDMTEST_SUBQUES]"
				listKey="pkCodeId" listValue="description"  value="%{fdalicensedlab}" headerKey="" headerValue="Select"/>
			 --%>
		</td>
		</s:iterator>
		<td width="20%" colspan="2"></td>
		</s:if>
		</s:iterator>
	</tr>
	<tr>
		<td colspan ="6" id="title_screeningtests" style="background:#E6E6E5;font-weight: bold; padding: 10px;">
			<s:text name="garuda.idm.label.screeningtests"/>
		</td>
	</tr>
		<s:iterator value="idmTests" var="idmTestsLstPojo" status="row1">
		 <s:set name="scrTest" value="%{shrtName}" scope="request"/>
		 <s:set name="fklabgrp" value="%{fklabgroup}" scope="request"/>
		 <s:if test="#request.fklabgrp!=#request.labGrpPkByGrpType">
			<s:if test="#request.scrTest != 'FTA_ABS' || #request.scrTest != 'SYP_SUPP' ">
			<s:set name="testout" value="%{testoutcomedec}" scope="request"/>
			<s:if test="#request.testout != null && #request.testout != ''">	
				<tr>
					<td width="60%" colspan ="2">
						<s:property value="%{labtestname}"/>
					</td>
					<td width="20%" colspan ="2">
						<s:property value="%{testoutcomedec}"/>
					</td>
					<td width="20%" colspan ="2">
						<s:property value="%{testDate}"/>
					</td>
					
				</tr>
			</s:if>
			</s:if>
		</s:if>
		</s:iterator>
		<tr>
		<td colspan ="6" id="title_screeningtests" style="background:#E6E6E5;font-weight: bold; padding: 10px;">
			<s:text name="garuda.idm.label.confirmatorytest"/>
		</td>
		</tr>
		<s:iterator value="idmTests" var="idmTestsLstPojo" status="row1">
		<s:set name="scrTest" value="%{shrtName}" scope="request"/>
		<s:if test="#request.scrTest=='FTA_ABS' || #request.scrTest == 'SYP_SUPP' ">	
			<s:set name="testout" value="%{testoutcomedec}" scope="request"/>
			<s:if test="#request.testout != null && #request.testout != ''">
			<tr>
				<td width="60%" colspan ="2">
					<s:property value="%{labtestname}"/>
				</td>
				<td width="20%" colspan ="2">
					<s:property value="%{testoutcomedec}"/>
				</td>
				<td width="20%" colspan ="2">
					<s:property value="%{testDate}"/>
				</td>
			</tr>
			</s:if>
		</s:if>
		</s:iterator>
		<tr>
			<td colspan="6" style="background:#E6E6E5;font-weight: bold; padding: 10px;" id="additIDM">
				<s:text name="garuda.idm.label.additionaltest"/>
			</td>
		</tr>
		<s:iterator value="idmTests" var="pojo" status="row">
		 <s:set name="fklabgrp" value="%{fklabgroup}" scope="request"/>
		 <s:if test="#request.fklabgrp==#request.labGrpPkByGrpType">
		<tr>
				<td width="60%" colspan ="2">
					<s:property value="%{labtestname}"/>
				</td>
				<td width="20%" colspan ="2">
					<s:property value="%{testoutcomedec}"/>
				</td>
				<td width="20%" colspan ="2">
					<s:property value="%{testDate}"/>
				</td>
			</tr>
			</s:if>
		</s:iterator>
	
		
	</tbody>
</table>
</div>
</div>