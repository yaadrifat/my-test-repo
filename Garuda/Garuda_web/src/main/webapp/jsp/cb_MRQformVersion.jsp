<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<script>
function getMRQForm(iseditable,fkformv,frmvers,cordId,forseq,isAdd){
	var moduleEntityEventCode = ""
		if(iseditable=="1"){
			moduleEntityEventCode = "<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_FORM'/>";
		}else if(iseditable=="0"){
			moduleEntityEventCode = "<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_FORM'/>";
		}
		if(isAdd=="0"){
			moduleEntityEventCode = "<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_FORM'/>";
		}
		moduleEntityEventCode = moduleEntityEventCode.replace("@"," MRQ ");
		moduleEntityEventCode +=" - Version "+frmvers;
		if(isAdd!="0"){
	var isCordEntryPage=$j('#mrqisCordEntryPage').val();
	if(isCordEntryPage=='true'){
		var cbuRegID = $j('#cburegid').val();
		if(cbuRegID == "" || cbuRegID == null || cbuRegID == "undefined"){
			cbuRegID = "";
		}
		if($j('#licenseid').val()!="" && $j('#licenseid').val()!=null && $j('#licenseid').val()!=undefined && $j('#licenseid').val()!="-1"){
			if($j('#fkCbbId').val()!='' && $j('#fkCbbId').val()!=null && $j('#fkCbbId').val()!='undefined' && $j('#fkCbbId').val()!="-1"){
				fn_showModalMRQ('<s:text name="garuda.label.forms.mrqcbuid"/> '+cbuRegID,'getDynamicFrm?formId=MRQ&entityId='+cordId+'&iseditable='+iseditable+'&fkfrmversion='+fkformv+'&formvers='+frmvers+'&isCordEntry=1'+'&isCordEntryPage=true&seqnumber='+forseq+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU"/>&moduleEntityIdentifier='+cbuRegID+"&moduleEntityEventCode="+moduleEntityEventCode,'500','950','MRQModalForm',true,true);
			}else{
				alert('<s:text name="garuda.common.cbu.reqSiteAlert"/>');
				$j('#fkCbbId').focus();
			}
		}else{
			alert('<s:text name="garuda.common.cbu.reqLicensureAlert"/>');
			$j('#licenseid').focus();
		}
	}else{
		var cbuRegID = $j('#cburegIdHidden').val();
		if(cbuRegID == "" || cbuRegID == null || cbuRegID == "undefined"){
			cbuRegID = "";
		}
		fn_showModalMRQ('<s:text name="garuda.label.forms.mrqcbuid"/>: '+cbuRegID,'getDynamicFrm?formId=MRQ&entityId='+cordId+'&iseditable='+iseditable+'&fkfrmversion='+fkformv+'&formvers='+frmvers+'&isCordEntry=0&seqnumber='+forseq+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU"/>&moduleEntityIdentifier='+cbuRegID+"&moduleEntityEventCode="+moduleEntityEventCode,'500','950','MRQModalForm',true,true);
	}
		}else{
			fn_showModalMRQ('<s:text name="garuda.label.forms.mrqcbuid"/>: <s:property value="cdrCbuPojo.registryId" />','getDynamicFrm?formId=MRQ&entityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU"/>&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode='+moduleEntityEventCode,'500','950','MRQModalForm',true,true);
		}
}
function fn_showModalMRQ(title,url,hight,width,id,min,max){
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title=title+patient_data;
	showModals(title,url,hight,width,id,min,max);
}
	if($j('#fmhqisCordEntryPage').val()!='true'){
		showformsFromStatic();	
	}
	$j(function(){
			if($j("#mrqisCordEntryPage").val()=="true"){
			if(mrqcount==0){
			    if($j("#mrqformversionlstsize").val()!=null && $j('#mrqformversionlstsize').val()>0){
			   		mrqcount++;
			   		val = parseInt((mrqcount*100)/(noOfMandatoryMrqField));
			   		$j("#MRQbar").progressbar({
						value: val
					}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
			   		addTotalCount();		
		   		}
			    //call method to refersh total count progress
				  totalcount--;
			      addTotalCount();
			   // Load Validation If All Widget Is Fully Loaded	   
	      		LoadedPage.mrq=true;
	      		if(LoadedPage.idm && LoadedPage.fmhq && LoadedPage.processingProc && LoadedPage.cbuHla && LoadedPage.clinicalNote && LoadedPage.labSummary){
	    	  		jQueryValidationInitializer();
	      		}
			}
			
		}
		
	});	
</script>
<s:div id="MRQ_formVersion">
<s:hidden name="isCordEntryPage" id="mrqisCordEntryPage"></s:hidden>
<s:hidden value="%{mrqformversionlst.size()}" id="mrqformversionlstsize" name="mrqFormVersion"></s:hidden>
	<table class="displaycdr" style="width: 98%">
		<thead>
			<tr>
				<th width="20%"><s:text name="garuda.label.dynamicform.date"></s:text></th>
				<th width="55%"><s:text name="garuda.label.dynamicform.formver"></s:text></th>
				<th width="20%"></th>
			</tr>
		</thead>
		<tbody>
		<s:set name='mrqversionflag' value="" id="mrqversionflag"></s:set>
			<s:if test="%{mrqformversionlst!=null && mrqformversionlst.size()>0}">
				<s:iterator value="mrqformversionlst" var="rowVal" status="row">
					<tr>
						<td><s:property value="%{getText('collection.date',{#rowVal[7]})}"/></td>
						<td><s:if test="%{#rowVal[6]!=mrqversion}"><s:property value="%{#rowVal[6]}"/></s:if><s:else><s:text name="garuda.label.forms.current"/></s:else></td>
						<td>
							<s:if test="%{#row.index==0}">
								<s:set name="mrqversionflag" value="%{#rowVal[6]}" id="mrqversionflag"/>
								<a href="#" name="mrqFormViewLink<s:property value="%{#row.index}"/>" id="mrqFormViewLink<s:property value="%{#row.index}"/>" style="cursor: pointer;" onclick="getMRQForm('1','<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="%{#rowVal[2]}"/>','1')">
									<s:text name="garuda.label.dynamicform.view"/>
								</a>&nbsp;&nbsp;
								<s:if test="%{isCordEntryPage==false}">
									<s:if test="cdrCbuPojo.site.isCDRUser()==true && #mrqversionflag==mrqversion && hasEditPermission(#request.updateHlthHis)==true">
										<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
										</s:if>
										<s:else>
											<a href="#" name="mrqFormUpdateLink<s:property value="%{#row.index}"/>" id="mrqFormUpdateLink<s:property value="%{#row.index}"/>" style="cursor: pointer;" onclick="getMRQForm('0','<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="%{#rowVal[2]}"/>','1');" class="formsClass">
												<s:text name="garuda.label.dynamicform.update"/>
											</a>
										</s:else>
									</s:if>
								</s:if>
								<s:else>
									<a href="#" name="mrqFormUpdateLink<s:property value="%{#row.index}"/>" id="mrqFormUpdateLink<s:property value="%{#row.index}"/>" style="cursor: pointer;" onclick="getMRQForm('0','<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="%{#rowVal[2]}"/>','1');" class="formsClass">
										<s:text name="garuda.label.dynamicform.update"/>
									</a>
								</s:else>
							</s:if>
							<s:else>
								<a href="#" name="mrqFormViewLink<s:property value="%{#row.index}"/>" id="mrqFormViewLink<s:property value="%{#row.index}"/>" style="cursor: pointer;" onclick="getMRQForm('1','<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="%{#rowVal[2]}"/>','1')">
									<s:text name="garuda.label.dynamicform.view"/>
								</a>
							</s:else>
						</td>
					</tr>
				</s:iterator>
			</s:if>
<s:if test="%{isCordEntryPage==false}">
<tr class="formsClass"><td colspan="3">
	<s:if test="hasEditPermission(#request.updateHlthHis)==true && cdrCbuPojo.site.isCDRUser()==true">
		<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
		</s:if>
		<s:else>
		<s:if test="%{mrqformversionlst.size()==0}">
			<button type="button" name="mrqFormButton" id="mrqFormButton" onclick="getMRQForm('','','<s:property value="%{mrqversion}"/>','','','0')"><s:text name="garuda.label.forms.addmrq" /></button>
		</s:if>
		<s:else>
			<s:if test="%{#mrqversionflag!=mrqversion}">
				<button type="button" name="mrqFormButton" id="mrqFormButton" onclick="getMRQForm('','','<s:property value="%{mrqversion}"/>','','','0')"><s:text name="garuda.label.forms.addmrq" /></button>
			</s:if>
		</s:else>
		</s:else>
		</s:if>
</td></tr>
</s:if>
<s:else>
<tr class="formsClass"><td colspan="3">
	<s:if test="%{mrqformversionlst.size()==0}">
		<button type="button" name="mrqFormButton" id="mrqFormButton" onclick="getDynameicForms('MRQ','<s:property value="%{mrqversion}"/>');"><s:text name="garuda.label.forms.addmrq" /></button>
		<span class="error" id="mrqFormMan" style="display:none"><s:text name="garuda.cordentry.label.mrq"/>&nbsp;<s:text name="garuda.label.forms.validationmsg" /></span>
	</s:if>
	<s:else>
		<s:if test="%{#mrqversionflag!=mrqversion}">
			<button type="button" name="mrqFormButton" id="mrqFormButton" onclick="getDynameicForms('MRQ','<s:property value="%{mrqversion}"/>');"><s:text name="garuda.label.forms.addmrq" /></button>
			<span class="error" id="mrqFormMan" style="display:none"><s:text name="garuda.cordentry.label.mrq"/>&nbsp;<s:text name="garuda.label.forms.validationmsg" /></span>
		</s:if>
	</s:else>	
</td></tr>
</s:else>
</tbody>
</table>
</s:div>