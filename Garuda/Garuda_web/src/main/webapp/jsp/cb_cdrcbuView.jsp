<%@ include file="cb_includes.jsp"%>
<jsp:include page="cb_widget-setting.jsp" flush="true">
<jsp:param value="16" name="pageId"/>
</jsp:include>
<script>

$j(function() {
	$j( "#datepicker" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		changeYear: true});
	$j( "#datepicker1" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		changeYear: true});
	
});

function showNewStudy(mode,divname,status){
	if(mode =='open'){
		//$j("#errormessagetd").html("");
		jQuery("#"+divname).dialog("destroy");
		$j("#"+divname).dialog({
			 title : 'Edit the '+status+' of CDR CBU ID:'+$j("#cbuid").val(),
			 width : 600,
		 	height : 600,
		 	autoOpen: true,
			modal: true,
			close: function() {
	  		jQuery("#"+divname).dialog("destroy");
			}
		 });		 
	}else{
		$j("#"+divname).dialog(mode);
	}
		//$j("#newstudydialog").dialog(mode);
}




$j(function() {
  $j(".popupdialog").dialog({
   autoOpen: false,
	   modal: true, width:600, height:450,
		close: function() {
			//$j(".ui-dialog-content").html("");
			//jQuery("#subeditpop").attr("id","subeditpop_old");
		jQuery(".popupdialog").dialog("destroy");				
   		}
  });
  jQuery(".popupdialog").dialog("destroy");
});




function getEdit(divname,status){
	showNewStudy('open',divname,status);
	}


function unliceseReason(value,divName){
		//alert("1 : "+value+"===="+document.getElementById("unLicensedPk").value);
		
		if(value == document.getElementById("unLicensedPk").value){
			//alert("2 if open");
			//alert(divName);
			var div = document.getElementById(divName);
			//alert(div);
			//div.removeAttribute("style");
			div.setAttribute("style","display:block");
			 //jQuery("#"+divName).show();
			//document.getElementById(divName).style.display='block';	
			}
		else{
			//alert("2.1 else close");
			//alert(divName);
			var div = document.getElementById(divName);
			//alert(div);
			//div.removeAttribute("style");
			div.setAttribute("style","display:none");
			//jQuery("#"+divName).hide();
			//document.getElementById(divName).style.display='none';
		}
}

function inEligibleReason(value,divName){
	//alert(value);
	if(value == document.getElementById("inEligiblePk").value){
		document.getElementById(divName).style.display='block';	
		}
	else
		document.getElementById(divName).style.display='none';
		if(value != document.getElementById("inEligiblePk").value){
			document.getElementById(divName).style.display='none';
			}
		else
			document.getElementById(divName).style.display='block';
}

$j(document).ready(function(){

	
	if(document.getElementById('tncfrozenpatwt').value == " " && document.getElementById('tncfrozenpatwt') == null){
	document.getElementById('tncfrozenpatwt').value = 0;
	}
	if(document.getElementById('totcelfrozen').value == " " && document.getElementById('totcelfrozen') == null){
	document.getElementById('totcelfrozen').value = 0;
	}
	

	if(document.getElementById('unLicensed').value == document.getElementById('cbuLicSta').value){
		document.getElementById('unlicensed').setAttribute("checked","checked");
		document.getElementById('licensed').removeAttribute("checked");					
		}
		else{
		document.getElementById('licensed').setAttribute("checked","checked");
		document.getElementById('unlicensed').removeAttribute("checked");
		}	

	//alert("fk eligible -->"+$j("#fkCordCbuEligible").val());
	//alert("ineligible-->"+document.getElementById("inEligiblePk").value)
	
	//alert("fk cbuLicStatus -->"+$j("#cbuLicStatus").val());
	//alert("unLicensedPk -->"+document.getElementById("unLicensedPk").value)
	
	
	if($j("#fkCordCbuEligible").val() == document.getElementById("inEligiblePk").value){
		//alert("uEligible 11 if");	
		document.getElementById('inEligibleReason').style.display='block';	
		}
	else{
		//alert("uEligible v11 else");
		document.getElementById('inEligibleReason').style.display='none';
		}
		

	if($j("#cbuLicStatus").val() == document.getElementById("unLicensedPk").value){
		//alert("license 11 if open");	
		document.getElementById('licenseReason').style.display='block';	
		}
	else{
		//alert("license 11 else close");
		document.getElementById('licenseReason').style.display='none';
		}
		
		
});


function updateStatusLicense(){
	
    	if($j("#statusChange").valid()){ 	
		$j.ajax({
			type : "POST",
			async : false,
			url : 'updateStatusLicense?cbuLicStatus='+$j("#cbuLicStatus").val()+'&fkCordCbuUnlicenseReason='+$j("#fkCordCbuUnlicenseReason").val(),
			data : $j("#statusChange").serialize(),
			success : function(result) {
				//showUnitReport("close");
				$j('#main').html(result);
				jQuery(".popupdialog").dialog("refresh");
				//document.getElementById("lbl1").innerHTML="Data Saved Successfully";
				
			}
		
		});
		
    }
    
}

function updateEligibleReason(){
	
	if($j("#statusChange").valid()){ 	
		$j.ajax({
			type : "POST",
			async : false,
			url : 'updateEligiblestatus?fkCordCbuEligible='+$j("#fkCordCbuEligible").val()+'&fkCordCbuEligibleReason='+$j("#fkCordCbuEligibleReason").val()+'&addtionalInfo='+$j("#cordAdditionalInfo").val(),
			data : $j("#statusChange").serialize(),
			success : function(result) {
				//showUnitReport("close");
				$j('#main').html(result);
				//document.getElementById("lbl1").innerHTML="Data Saved Successfully";
				
			}
		
		});
		
    }


}


function updateClinicalStatus(){
	if($j('#clinicalStatus').val()==null || $j('#clinicalStatus').val()==""){
		document.getElementById("lbl1").innerHTML="Select status..";
		}
	else{
		
	if($j("#statusChange").valid()){ 	
		$j.ajax({
			type : "POST",
			async : false,
			url : 'updateClinicalStatus?clinicalStatus='+$j("#clinicalStatus").val(),
			data : $j("#statusChange").serialize(),
			success : function(result) {
				//showUnitReport("close");
				$j('#main').html(result);
				//document.getElementById("lbl1").innerHTML="Data Saved Successfully";
				
			}
		
		});
		
    }

	}
    
}

function closePopUp(){
	 jQuery(".popupdialog").dialog("destroy");
}
</script>

<s:form name="statusChange" id="statusChange" >
<div class="portlet" id="cbuClinicalRecordparent" ><div id="cbuClinicalRecord" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
	<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
		
		<s:text name="garuda.cdrcbuview.label.cbu_clinical_record" />
	</div>
	<div class="portlet-content" style="height: auto; width: auto; ">
	<div id="cbuDashDiv1">
			
<s:hidden name="cdrCbuPojo.unLicesePkid" id="unLicensedPk"></s:hidden>
<s:hidden name="cdrCbuPojo.inEligiblePkid" id="inEligiblePk"></s:hidden>
<s:hidden name="cdrCbuPojo.unLicesePkid" id="unLicensed"></s:hidden>
<s:hidden name="cdrCbuPojo.cbuLicStatus" id="cbuLicSta"></s:hidden>		
<s:hidden name="cdrCbuPojo.cordID" id="cordID"  ></s:hidden>
<s:hidden name="cdrCbuPojo.cdrCbuId" id="cbuid" ></s:hidden>
<s:hidden name="cdrCbuPojo.birthDate" ></s:hidden>
<s:hidden name="cdrCbuPojo.cbuCollectionDate" ></s:hidden>
<s:hidden name="cdrCbuPojo.registryId" ></s:hidden>
<s:hidden name="cdrCbuPojo.localCbuId" ></s:hidden>
<s:hidden name="cdrCbuPojo.bacterialResult" ></s:hidden>
<s:hidden name="cdrCbuPojo.fungalResult" ></s:hidden>
<s:hidden name="cdrCbuPojo.aboBloodType" ></s:hidden>
<s:hidden name="cdrCbuPojo.rhType" ></s:hidden>
<s:hidden name="cdrCbuPojo.babyGenderId" ></s:hidden>
<s:hidden name="cdrCbuPojo.creationDate" ></s:hidden>
<s:hidden name="cdrCbuPojo.cdrCbuCreatedBy" ></s:hidden>
<s:hidden name="cdrCbuPojo.cdrCbuLstModDate" ></s:hidden>
<s:hidden name="cdrCbuPojo.cdrCbuLstModBy" ></s:hidden>
<s:hidden name="cdrCbuPojo.fkCordCbuStatus" ></s:hidden>
<s:hidden name="cdrCbuPojo.cord_Add_info" ></s:hidden>
<s:hidden name="cdrCbuPojo.cordIsbiDinCode" ></s:hidden>
<s:hidden name="cdrCbuPojo.cordIsitProductCode" ></s:hidden>
<s:hidden name="cdrCbuPojo.fkCordCbuEligible" ></s:hidden>
<s:hidden name="cdrCbuPojo.fkCordCbuEligibleReason" ></s:hidden>
<s:hidden name="cdrCbuPojo.fkCordCbuUnlicenseReason" ></s:hidden>
<s:hidden name="cdrCbuPojo.cdrCbuStatus" ></s:hidden>
<s:hidden name="cdrCbuPojo.fkCbbId" ></s:hidden>
<s:hidden name="cdrCbuPojo.clinicalStatus" ></s:hidden>
<s:hidden name="CdrCbuPojo.cord_Add_info" ></s:hidden>

<table id="container">
	
		<tr>
		
			<td colspan="2">
			
			<div class="portlet" id="cdrviewparent" ><div id="cdrview" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
				<span class="ui-icon ui-icon-minusthick"></span>
				<span class="ui-icon ui-icon-close" ></span>
				<br />
			</div>
			<div class="portlet-content" style="height: auto; width: auto;">
			<div id="cbuDashDiv1">
			<s:iterator value="lstObject" var="rowVal" status="row">
			<table id="container1" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>				
					<td style="vertical-align: middle; width: 70%"  height="100%">
					<fieldset>
					<legend><s:text name="garuda.cdrcbuview.label.cbu_information" /></legend>
					<table>
					<tr>
					<td style="vertical-align: middle;">
					<table cellpadding="0" cellspacing="0" border="0" align="center">
						<tr>
							<td  align="left"><b><s:text name="garuda.cdrcbuview.label.registry_id" /> :</b></td>
							<td  align="left"><s:property value="%{#rowVal[0]}" /></td>
							<td  align="left"><b><s:text name="garuda.cdrcbuview.label.collection_date" /> :</b></td>
							
							<td  align="left">
							<s:text name="collection.date">
								<s:param name="value" value="%{#rowVal[1]}"/>
							</s:text>
							</td>
						</tr>
						
						<tr>
							<td  align="left"><b><s:text name="garuda.cdrcbuview.label.isbt_din" /> :</b></td>
							<td  align="left"><s:property value="%{#rowVal[2]}" /></td>
							<td  align="left"><b><s:text name="garuda.cdrcbuview.label.cbb"></s:text> :</b></td>
							<td  align="left"><s:property value="%{#rowVal[3]}" /></td>
						</tr>
						<tr>
							<td  align="left"><b><s:text name="garuda.cdrcbuview.label.isbt_product_code"></s:text> :</b></td>
							<td  align="left"><s:property value="%{#rowVal[4]}" /></td>
							<td  align="left"><b><s:text name="garuda.cdrcbuview.label.tnc_frozen"></s:text> :</b></td>
							<td  align="left"><s:property value="%{#rowVal[5]}" /></td>
						</tr>
						<tr>
							<td  align="left"><b><s:text name="garuda.cdrcbuview.label.id_on_bag"></s:text> :</b></td>
							<td  align="left"><s:property value="%{#rowVal[6]}" /></td>
							<td></td>
							<td></td>
						</tr>
						<tr >
						<td colspan="4">
						<fieldset>
							<legend><b><s:text name="garuda.cdrcbuview.label.licensure_status" /></b></legend>
							<table>
							<tr>
							<td>
							<s:property value="cdrCbuPojo.license" />
							</td>
							
							<s:if test="cdrCbuPojo.cbuLicStatus != cdrCbuPojo.fkCordCbuUnlicenseReason" >
							<td>
							<s:text name="garuda.cdrcbuview.label.licencure_reason" /> : <s:property value="cdrCbuPojo.unlicenseReason" />
							</td>
							</s:if>
							
							<td>
							<input type="button" name="licenseEdit" value="<s:text name="garuda.common.lable.edit"/>" onclick="getEdit('licenseDiv','Licensure Status');">
							</td>
							</tr>
							</table>
						</fieldset>	
						</td>
						</tr>
						</table>
					</td>
					<td style="vertical-align: middle;">
					<fieldset>
							<legend><b><s:text name="garuda.cdrcbuview.label.eligibility_determination" /></b></legend>
								<table>
								<tr>
								<td>
								<br />
								</td>
								</tr>
								<tr >
								<s:if test="CdrCbuPojo.fkCordCbuEligible==CdrCbuPojo.fkCordCbuEligibleReason">
								<td><s:property value="CdrCbuPojo.eligibilitydes" /></td>
								</s:if>
								</tr>
								<s:else>
								<tr ><td><s:property value="CdrCbuPojo.eligibilitydes" /></td></tr>
								<tr ><td><s:text name="garuda.cdrcbuview.label.eligibility_reason" /> : <s:property value="CdrCbuPojo.inEligibleReason" /></td></tr>
								</s:else>
								<tr>
								<td align="right"><input type="button" name="eligibleEdit" value="Edit" onclick="getEdit('ineligibleDiv','Eligiblity Status');"></td>
								</tr>
								<tr>
								<td>
								<br />
								</td>
								</tr>
								<tr>
								<td>
								<br />
								</td>
								</tr>
								
								<!--<tr><td align="right"><input type="button" name="eligibleEdit" value="Edit" onclick="getEdit('ineligible');"></td></tr>-->
								</table>
							</fieldset>
					</td>
					</tr>
					</table>
					</fieldset>
					</td>
					<td style="vertical-align: middle; width: 40%" >
					<fieldset>
					<legend><s:text name="garuda.cdrcbuview.label.patient_information" /></legend>
					<table cellpadding="0" cellspacing="0" border="0" align="center" width="30%" height="100%">
					<tr>
					<td>
								<br />
								</td>
								</tr>
								<tr>
								<td>
								<br />
								</td>
								</tr>
						<tr >
						<td><h3><font color="red"><s:text name="garuda.cbu.label.MedicallyDeferred"/></font></h3></td>
						</tr>
						<tr >
						<td><s:text name="garuda.cbu.label.reason_for_deferral"/></td>
						</tr>
						<tr>
								<td>
								<br />
								</td>
								
						</tr>
								<tr>
								<td>
								<br />
								</td>
								</tr>
								<tr>
								<td>
								<br />
								</td>
								</tr>
						</table>
					</fieldset>
					</td>
				</tr>
			</table>
			</s:iterator>

			</div>
			</div>
			</div>
			</div>
			
			</td>	
		</tr>
		
		
		<tr>
			<td colspan="2">
			
							<table class="container" cellpadding="1" cellspacing="1" border="0" >
							<tr   bordercolor="black">
							<th Style="text-align:left;">
								<s:text name="garuda.cdrcbuview.label.quicklinks" />
								<button type="submit" onclick="return false;" disabled="disabled"><s:text
										name="garuda.cdrcbuview.label.button.unitreport" /></button>
								<button type="submit" disabled onclick="return false;" disabled="disabled" ><s:text
										name="garuda.cdrcbuview.label.button.addclinicalnotes" /></button>
								<button type="submit" onclick=""><s:text
										name="garuda.cdrcbuview.label.button.uploaddocument" /></button>
								<button type="button"  onclick="getEdit('clinicalDiv','Clinical Status');"><s:text
										name="garuda.cdrcbuview.label.button.updateclinicalstatus" /></button>
								<button type="submit" disabled onclick="return false;" disabled="disabled" ><s:text
										name="garuda.cdrcbuview.label.button.compfinaleligibility" /></button>
								<button type="submit"  disabled onclick="return false;" disabled="disabled"><s:text
										name="garuda.cdrcbuview.label.button.printcbureport" /></button>
								<button type="submit" disabled onclick="return false;" disabled="disabled"><s:text
										name="garuda.cdrcbuview.label.button.audittrail" /></button>
										
							</th>
							</tr>
							</table>
			
			</td>
		</tr>
		
		<tr>
			<td width="50%">
				<div class="column">
				<div class="portlet" id="cbuCharacteristicsparent" >
				<div id="cbuCharacteristics" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
				<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<s:text name="garuda.cdrcbuview.label.cbu_characteristics" />
				</div>
			<div class="portlet-content">
			<div id="cbuDashDiv1">
			<table width="100%">
			<tr>
				<td>
					
					<div class="portlet" id="idsparent" >
					<div id="cbuIds" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
						<span class="ui-icon ui-icon-minusthick"></span>
						<span class="ui-icon ui-icon-close" ></span>
						<s:text name="garuda.cdrcbuview.label.ids" />
					</div>
			<div class="portlet-content">
			<div id="cbuDashDiv1">
			<table cellpadding="0" cellspacing="0" width="100%" >
				<tr align="left">			
					<td  style="width: 25%;">
						<s:text name="garuda.cbuentry.label.externalcbuid" />:     				
   					</td>
					<td style="width: 25%;">
						<s:textfield name="CdrCbuPojo.externalCbuId" id="externalCbuId" readonly="true" onkeydown="cancelBack();"   />
					</td>
        			<td  style="width: 25%;">
   						<s:text name="garuda.cbuentry.label.idoncbubag" />:
					</td>
        			<td  style="width: 25%;">
        				<s:textfield name="CdrCbuPojo.numberOnCbuBag" readonly="true" onkeydown="cancelBack();"   />
        			</td>
             	</tr>
			</table>
			</div>
			</div>
		</div>
		</div>
</td>
</tr>
<tr>
<td>
	<div class="portlet" id="cbuProductparent" >
	<div id="cbuProduct" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
				<span class="ui-icon ui-icon-minusthick"></span>
				<span class="ui-icon ui-icon-close" ></span>
				<s:text name="garuda.unitreport.label.prodinfo.subheading" />
			</div>
	<div class="portlet-content">
	<div id="cbuDashDiv1">
		<table cellpadding="0" cellspacing="0" width="100%">
			<tr align="left">
				<td style="width: 25%;">
					<s:text name="garuda.cbuentry.label.gender" />:
                </td>
                <td style="width: 25%;">
                	<s:select name="CdrCbuPojo.babyGenderId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BABY_GENDER]"
																listKey="pkCodeId" listValue="description" headerKey=""
																headerValue="Select" disabled="true"  />
                </td>
                <td style="width: 25%;">
           			<s:text name="garuda.cbuentry.label.birthdate" />:
                </td>
                <td style="width: 25%;">
                	<s:date name="CdrCbuPojo.birthDate" id="datepicker" format="MMM dd, yyyy" />						
                    <s:textfield class="txtarea75" name="CdrCbuPojo.birthDate" placeholder="Date" id="datepicker" disabled="true"  value="%{datepicker}"/>
                </td>
             </tr>
             <tr align="left">
				<td style="width: 25%;">
					<s:text name="garuda.cbuentry.label.cbulicensure" />:
               	</td>
                <td style="width: 25%;">
                	<input type="radio" id="unlicensed"  readonly="true" onkeydown="cancelBack();"   /><s:text name="garuda.cdrcbuview.label.unlicensed" />
                	<input type="radio" id="licensed" readonly="true" onkeydown="cancelBack();"   /><s:text name="garuda.cdrcbuview.label.licensed" />
                </td>
                <td style="width: 25%;">
           			<s:text name="garuda.cbuentry.label.collectiondate" />:
                </td>
                <td style="width: 25%;">
                	<s:date name="CdrCbuPojo.cbuCollectionDate" id="datepicker1" format="MMM dd, yyyy" />						
                    <s:textfield class="txtarea75" name="CdrCbuPojo.cbuCollectionDate" placeholder="Date" id="datepicker1" disabled="true"  value="%{datepicker1}"/>
                </td>
              </tr>
              <tr align="left">
				<td style="width: 25%;">
					<s:text name="garuda.cbuentry.label.abo" />:
                </td>
                <td style="width: 25%;">
                	<s:textfield name="CdrCbuPojo.bloodGpDes" readonly="true" onkeydown="cancelBack();"   />
                </td>
                <td style="width: 25%;">
           			<s:text name="garuda.cbuentry.label.rhtype" />:
                </td>
                <td style="width: 25%;">
                	<s:textfield name="CdrCbuPojo.rhTypeDes" readonly="true" onkeydown="cancelBack();"   />
                </td>
              </tr>
              <tr align="left">
				<td style="width: 25%;">
					<s:text name="garuda.cbuentry.label.bcresult" />:
                </td>
                <td style="width: 25%;">
                	<s:select name="CdrCbuPojo.bacterialResult" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS]"
								listKey="pkCodeId" listValue="description" headerKey=""	headerValue="Select" disabled="true"  />
				</td>
                <td style="width: 25%;">
           			<s:text name="garuda.cbuentry.label.fcresult" />:
                </td>
                <td style="width: 25%;">
                	<s:select name="CdrCbuPojo.fungalResult" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS]" listKey="pkCodeId" listValue="description"
									headerKey="" headerValue="Select" disabled="true"  />
                </td>
              </tr>
			</table>
		</div>
		</div>
		</div>
		</div>
			</td>
		</tr>
		<tr>
			<td>
	
		<div class="portlet" id="cbuProcessingparent" >
		<div id="cbuProcessing" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
				<span class="ui-icon ui-icon-minusthick"></span>
				<span class="ui-icon ui-icon-close" ></span>
				<s:text name="garuda.unitreport.label.processinginfo.subheading" />
			</div>
		<div class="portlet-content">
		<div id="cbuDashDiv1">
		<table cellpadding="0" cellspacing="0" width="100%" >
			<tr align="left">
				<td style="width: 25%;">
					<s:text name="garuda.cbuentry.label.tncfrozen" />:
                </td>
                <td style="width: 25%;">
                	<s:textfield name="CdrCbuPojo.tncFrozen" id="tncfrozen" readonly="true" onkeydown="cancelBack();"   />
                </td>
                <td style="width: 25%;">
           			<s:text name="garuda.cbuentry.label.tncfrozenpatwt" />:
                </td>
                <td style="width: 25%;">
                	<s:textfield name="CdrCbuPojo.frozenPatWt" id="tncfrozenpatwt" maxlength="5"  readonly="true" onkeydown="cancelBack();"  />
                </td>
             </tr>
             <tr align="left">
	         	<td style="width: 25%;">
					<s:text name="garuda.cbuentry.label.totcelfrozen" />:
	            </td>
	            <td style="width: 25%;">
					<s:textfield  name="CdrCbuPojo.cellCount" id="totcelfrozen" maxlength="5"   readonly="true" onkeydown="cancelBack();"   />
	            </td>
	            <td style="width: 25%;" colspan="2">
	             </td>
             </tr>
		</table>
		</div>
		</div>
		</div>
		</div>
		</td>
</tr>
</table>
</div></div>
				</div>
			</div>
			</div>
</td>
		
		<td width="50%">
		<div class="column">
				<div class="portlet" id="healthparent" >
				<div id="healthHistory" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
				<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<span class="ui-icon ui-icon-close" ></span>
					<s:text name="garuda.cdrcbuview.label.health_history_screening" />
				</div>
					<div class="portlet-content">
						<div id="cbuDashDiv1">
						
						
						</div>
					</div>
				</div>
			</div>
			</div>
		</td>
		</tr>
		
		
		
		<tr>
		<td width="50%">
		<div class="column">
				<div class="portlet" id="infectDiseaseparent" >
				<div id="infectDisease" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
				<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<span class="ui-icon ui-icon-close" ></span>
					<s:text name="garuda.cdrcbuview.label.infectious_disease_markers" />
				</div>
					<div class="portlet-content">
						<div id="cbuDashDiv1">
						
						
						</div>
					</div>
				</div>
			</div>
			</div>
		</td>
		
		<td width="50%">
		<div class="column">
				<div class="portlet" id="otherTestResultparent" >
				<div id="otherTestResult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
				<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<span class="ui-icon ui-icon-close" ></span>
					<s:text name="garuda.cdrcbuview.label.other_test_results" />
				</div>
					<div class="portlet-content">
						<div id="cbuDashDiv1">
						
						
						</div>
					</div>
				</div>
			</div>
			</div>
		</td>
		</tr>
		
		
		<tr>
		<td width="50%">
		<div class="column">
		<div class="portlet" id="cbuHlaparent" >
		<div id="cdrview" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
		<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
			<span class="ui-icon ui-icon-minusthick"></span>
			<span class="ui-icon ui-icon-close" ></span>
				<s:text name="garuda.cdrcbuview.label.hla" />
		</div>
			<div class="portlet" id="cbuHlaparent" >
			<div id="cdrview" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			<span class="ui-icon ui-icon-minusthick"></span>
			<span class="ui-icon ui-icon-close" ></span>
			<br /></div><div class="portlet-content">
						<div id="searchTble">
						<table style="border: 1px;" align="center" cellpadding="0"	cellspacing="0" class="col_100" id="searchResults">
							<thead>
								<tr>
									<th><s:text name="garuda.cbuentry.label.hlaLocus" /></th>
									<th><s:text name="garuda.cbuentry.label.method" /></th>
									<th><s:text name="garuda.cbuentry.label.type1" /></th>
									<th><s:text name="garuda.cbuentry.label.type2" /></th>
								</tr>
							</thead>
							<tbody>
			
			
									<%String oddEven=""; %>
								<s:iterator value="lstHla" var="rowVal" status="row" >
								<% oddEven = (oddEven == "even") ? "odd" : "even"; %>
								
									<tr class=<%=oddEven %> >
										<s:iterator value="rowVal" var="cellValue" status="col">
											<s:if test="%{#col.index == 0}">
												<td><s:property /></td>
											</s:if>
											<s:if test="%{#col.index == 1}">
												<td><s:property /></td>
											</s:if>
											<s:if test="%{#col.index == 2}">
												<td><s:property /></td>
											</s:if>
											<s:if test="%{#col.index == 3}">
												<td><s:property /></td>
											</s:if>
										</s:iterator>
									</tr>
								</s:iterator>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4"></td>
								</tr>
							</tfoot>
						</table>
						</div>
			
					</div>
				</div>	

</div>
				</div>
			</div>
			</div>
		</td>
		
		<td width="50%">
		<div class="column">
				<div class="portlet" id="clinicalNotesparent" >
				<div id="clinicalNotes" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
				<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<span class="ui-icon ui-icon-close" ></span>
					<s:text name="garuda.cdrcbuview.label.clinical_notes" />
				</div>
					<div class="portlet-content">
						<div id="cbuDashDiv1">
						
						
						</div>
					</div>
				</div>
			</div>
			</div>
		</td>
		
		</tr>
		
		
		<tr>
		<td width="50%">
		<div class="column">
				<div class="portlet" id="otherRelevantparent" >
				<div id="otherRelevant" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
				<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<span class="ui-icon ui-icon-close" ></span>
					<s:text name="garuda.cdrcbuview.label.other_relevant" />
				</div>
					<div class="portlet-content">
						<div id="cbuDashDiv1">
						
						
						</div>
					</div>
				</div>
			</div>
			</div>
		</td>
		
		<td width="50%">
		<div class="column">
				<div class="portlet" id="eligibilityInfoparent" >
				<div id="eligibilityInfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
				<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<span class="ui-icon ui-icon-close" ></span>
					<s:text name="garuda.cdrcbuview.label.eligibility" />
				</div>
					<div class="portlet-content">
						<div id="cbuDashDiv1">
						
						
						</div>
					</div>
				</div>
			</div>
			</div>
		</td>
		
		</tr>

</table>

</div></div></div></div>
<!-- 
=================================================================================================
 -->

<div id="licenseDiv" class='popupdialog tabledisplay '>


  <table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tr>
  <td colspan="2"/>
  </tr>
  <tr>
  <td colspan="2"/>
  </tr>
 
   <tr>
  <td style="height: 35px;" valign="middle"><s:text name="garuda.cdrcbuview.label.licensure_status" /> :</td>
	<td ><s:select name="CdrCbuPojo.cbuLicStatus" id="cbuLicStatus" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS]"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select"  onchange="unliceseReason(this.value,'licenseReason');" /></td>
	</tr>
	
	<tr>			
   <td colspan="2">
   <div id="licenseReason" style="display: none">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
				
		 		<tr>
			       <td style="height: 35px;" valign="middle"><s:text name="garuda.cdrcbuview.label.unlicense_reason" /> :</td>
			      <td><s:select name="CdrCbuPojo.fkCordCbuUnlicenseReason" id="fkCordCbuUnlicenseReason" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON]"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select"  /></td>
			    </tr>
			     <tr>
			<td>
			<br />
			</td>
			</tr>
			<tr>
			<td>
			<br />
			</td>
			</tr>

	</table>
	</div>
   </td>
   </tr>
	<tr>			
    <td colspan="2" align="center"><input type="button" onclick="closePopUp()" value="Cancel" /><input type="button" onclick="updateStatusLicense()" value="Submit" /></td>
    </tr>
  </table>


</div>

<div id="ineligibleDiv" class='popupdialog tabledisplay '>


  <table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tr>
  <td colspan="2"/>
  </tr>
  <tr>
  <td colspan="2"/>
  </tr>
  <tr>
  <td></td>
  <td>
		</td>
  </tr>
  
   <tr>
  <td style="height: 35px;" valign="middle"><s:text name="garuda.cdrcbuview.label.eligible_status" /> :</td>
	<td ><s:select name="CdrCbuPojo.fkCordCbuEligible" id="fkCordCbuEligible" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS]"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select"  onchange="inEligibleReason(this.value,'inEligibleReason');" /></td>
	</tr>
	
	<tr>			
   <td colspan="2">
   <div id="inEligibleReason" style="display: none">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
				
		 		<tr>
			       <td style="height: 35px;" valign="middle"><s:text name="garuda.cdrcbuview.label.ineligible_reason" /> :</td>
			      <td><s:select name="CdrCbuPojo.fkCordCbuEligibleReason" id="fkCordCbuEligibleReason" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON]"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select"  /></td>
			    </tr>
			    <tr>
			    <td style="vertical-align: top;"><s:text name="garuda.cdrcbuview.label.ineligible_reason_notes" />   </td>
			    <td><s:textarea name="CdrCbuPojo.cord_Add_info" id="cordAdditionalInfo" cols="35" rows="5" />   </td>
			    </tr>
			     <tr>
			<td>
			<br />
			</td>
			</tr>
			<tr>
			<td>
			<br />
			</td>
			</tr>
			   
	</table>
	</div>
   </td>
   </tr>
	<tr>			
    <td colspan="2" align="center"><input type="button" onclick="closePopUp()" value="Cancel" /><input type="button" onclick="updateEligibleReason()" value="Submit" /></td>
    </tr>
  </table>


</div>


<div id="clinicalDiv" class='popupdialog tabledisplay '>


  <table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tr>
  <td colspan="2"/>
  </tr>
  <tr>
  <td>
  </td>
  <td>
  
  <table cellpadding="0" cellspacing="0" align="center">
						 <tr>
						 <td align="left">
						  <label id="lbl1" style="color:#FF0000;"></label>
						 </td>
						 </tr>
						</table>
	</td>					
  </tr>
  
   <tr>
  <td style="height: 35px;" valign="middle"><s:text name="garuda.cbuentry.label.clinicalstatus" /> :</td>
	<td ><s:select name="CdrCbuPojo.clinicalStatus" id="clinicalStatus" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CLINICAL_STATUS]"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select"  /></td>
	</tr>
	
	<tr>
	<td>
	
						
						
	</td>
	</tr>
	
	
	<tr>	
			
    <td colspan="2" align="center"><input type="button" onclick="closePopUp()" value="Cancel" /><input type="button" onclick="updateClinicalStatus()" value="Submit" /></td>
    </tr>
   			 <tr>
			<td>
			<br />
			</td>
			</tr>
			<tr>
			<td>
			<br />
			</td>
			</tr>
			
  </table>


</div>
</s:form>
<div id="open" style="display:none;">
  <jsp:include page="cb_add-widgets.jsp" flush="true">
    <jsp:param value="16" name="pageId"/>
  </jsp:include>
</div>