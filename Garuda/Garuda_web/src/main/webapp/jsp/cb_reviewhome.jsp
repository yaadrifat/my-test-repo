<%@taglib prefix="s" uri="/struts-tags"%>
<% String contextpath=request.getContextPath();%>
<script>
	$j(function() {
		$j( "#datepicker" ).datepicker({changeMonth: true,
			changeYear: true});
	});
	

	function constructTable() {
			 $j('#searchResults').dataTable();
			 var tdObj = $j('#searchResults').find('tbody').find("tr:eq(0)").find("td:eq(0)");
				if($j(tdObj).hasClass('dataTables_empty')){
					$j('#searchResults_info').hide();
					$j('#searchResults_paginate').hide();
				}
			}
	

		function reviewScreen(formname,pkCordExtInfo,pkCordHlaExt){
			$j("pkCordExtInfo").val(pkCordExtInfo);
			$j("pkCordHlaExt").val(pkCordHlaExt);
			submitform(formname);
			}

		var cotextpath3 = "<%=contextpath%>";
		var path=cotextpath3+"/pages";
		function showPdf(attachmentId){
			var url = cotextpath+"/viewdocument.action?attachmentId="+attachmentId;
			window.open( url);
			}

		 function showDiv()
		 {
					$j('#searchresultparent').css('display','block');
					refreshDiv('review','searchTble','searchCordInfo');
		 }				

</script>

<div class="col_100 maincontainer ">
<div class="col_100">
<table width="100%" id="container1" >
<tr><td>
	<div class="column">
	<div class="portlet" id="cdrsearchparent">
		<div id="cdrsearch" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			<span class="ui-icon ui-icon-minusthick"></span>
			<!--<span class="ui-icon ui-icon-close" ></span>-->
			<s:text name="garuda.cbuentry.label.cdrcbusearch" />
			</div>
		<div class="portlet-content">
		<div id="studyDashDiv1" > 
  
				
	        <s:form id="searchCordInfo">
			         <table cellpadding="0" cellspacing="0" border="0"
					style="width: 100%; height: auto;" class="col_100">         
			         	<tr>
						   <td ><s:text name="garuda.cbuentry.label.cbuid" />:</td>
						   <td ><s:textfield  name="cdrCbuPojo.cdrCbuId" placeholder="CBU ID" onkeypress="return enter2Refreshdiv(event);"/></td> 
						   <td><button type="button" onclick="showDiv();"><s:text name="garuda.opentask.label.search"/></button> </td>
						</tr>				
					</table>
	        	</s:form>
	        	
		</div></div></div></div></div>
</td></tr>
</table>
			
<table width="100%" id="container2" ><tr><td>
<div class="column">
<div class="portlet" id="searchresultparent" style="display: none;"><div id="searchresult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span><!--<span class="ui-icon ui-icon-close" ></span>--><s:text name="garuda.cbuentry.label.cdrcbusearchresult" /></div><div class="portlet-content">
<s:form id="search" name="search" action="reviewHome">
<s:hidden name="pkCordExtInfo" id="pkCordExtInfo"></s:hidden>
<s:hidden name="pkCordHlaExt" id="pkCordHlaExt"></s:hidden>
<div id="searchTble">

<table style="height:100%"  border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="searchResults" >
       
	
                <thead>
                      <tr>                        
                        <th class="th2" scope="col"><div style="float:center;text-align:center">
                        <s:text
		name="garuda.reviewunitreport.label.orderid" /></div></th>
                        <th class="th3" scope="col"><div style="float:center;text-align:center">
                        <s:text
		name="garuda.reviewunitreport.label.cbuid"/></div></th>
                        <th class="th4" scope="col"><div style="float:center;text-align:center">
                        <s:text
		name="garuda.reviewunitreport.label.reviewrequired"/></div></th>
                        <th class="th5" scope="col"><div style="float:center;text-align:center">
                        <s:text
		name="garuda.reviewunitreport.label.view"/></div></th>                                             
                      </tr>
			         </thead>  
			  <tbody>     
			  
			<s:iterator value="cordInfoList" var="rowVal" status="row">
			        
				<tr>
					<td></td>		
					<td align="center"><a href="#" onclick="javaScript:reviewScreen('search','<s:property value="%{#rowVal[1]}" />','<s:property value="%{#rowVal[2]}" />')" ><s:property value="%{#rowVal[0]}" /></a></td>
					<td align="center"><a href="#" onclick="javaScript:reviewScreen('search','<s:property value="%{#rowVal[1]}" />','<s:property value="%{#rowVal[2]}" />')"><s:text
		name="garuda.reviewunitreport.label.requiredyes" /></a></td>
					<td align="center"><a href="#" onclick="javaScript:return showPdf('<s:property value="%{#rowVal[3]}" />')" ><s:property value="%{#rowVal[4]}" /></a></td> 
				</tr>
			 </s:iterator>
						
				      
              </tbody>  
               
                
				  <tfoot>
					<tr><td colspan="6"></td></tr>
				  </tfoot>
                
                </table>
	
        </div>
        </s:form>
		</div>
       
         </div>

</div>
</div>
</td></tr>     
       
 </table>       
</div>
</div>