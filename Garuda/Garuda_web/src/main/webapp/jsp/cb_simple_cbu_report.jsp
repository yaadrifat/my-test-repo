<%@ taglib prefix="s"  uri="/struts-tags"%>

<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants"%><jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="userLB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<script>
function saveFilter(){
	if($j("#filter_name").val()=="" || $j("#filter_name").val()==undefined){
		$j("#criteriaerror").css('display','block');
	}else{
		$j("#criteriaerror").css('display','none');
	  loaddiv('submitQueryBuilderCriteria?filterdatapojo.fk_module=14001&filterdatapojo.fk_sub_module=12001&filterdatapojo.filter_name='+$j("#filter_name").val(),'saveQueryBuilderCriteria');
	}
	
}

function getReportData(value){
	clearData();	
		if('<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_ANTIGEN_REPORT"/>'==value){
		   // loadPageByGetRequset('getSimpleQueryPaginationData?iShowRows=10&iPageNo=0&paginateModule='+value,'searchTbleReport');
			asyncLoadPageByGetRequset('getSimpleQueryPaginationData?inelig_incomple_flag=<s:property value="#request.inelig_incomple_flag"/>&unlicensed_flag=<s:property value="#request.unlicensed_flag"/>&selSiteIdentifier=<s:property value="#request.selSiteIdentifier"/>&selectedSiteId=<s:property value="#request.selectedSiteId"/>&cbuCount=&iShowRows=10&iPageNo=0&paginateModule='+value+'&isMultipleCBBSelected='+$j("#isMultipleCBBSelected").val()+'&isWorkflowActivitySelected='+$j("#isWorkflowActivitySelected").val()+'&unlicensed_flag='+$j('#v_unlicensed_flag').val()+'&inelig_incomple_flag='+$j('#v_inelig_incomple_flag').val()+'&qinelig_flag='+$j('#v_qinelig_flag').val()+'&qincomp_flag='+$j('#v_qincomp_flag').val(),'searchTbleReport',value);			
		}else if('<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_LICENSEANDELIGIBILTY_REPORT"/>'==value){
			//loadPageByGetRequset('getSimpleQueryPaginationData?iShowRows=10&iPageNo=0&paginateModule='+value,'searchTbleReport');
			asyncLoadPageByGetRequset('getSimpleQueryPaginationData?inelig_incomple_flag=<s:property value="#request.inelig_incomple_flag"/>&unlicensed_flag=<s:property value="#request.unlicensed_flag"/>&selSiteIdentifier=<s:property value="#request.selSiteIdentifier"/>&selectedSiteId=<s:property value="#request.selectedSiteId"/>&cbuCount=&iShowRows=10&iPageNo=0&paginateModule='+value+'&isMultipleCBBSelected='+$j("#isMultipleCBBSelected").val()+'&isWorkflowActivitySelected='+$j("#isWorkflowActivitySelected").val()+'&unlicensed_flag='+$j('#v_unlicensed_flag').val()+'&inelig_incomple_flag='+$j('#v_inelig_incomple_flag').val()+'&qinelig_flag='+$j('#v_qinelig_flag').val()+'&qincomp_flag='+$j('#v_qincomp_flag').val(),'searchTbleReport',value);
		}else if('<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_QUICKSUMMARY_REPORT"/>'==value){
			//loadPageByGetRequset('getSimpleQueryPaginationData?iShowRows=10&iPageNo=0&paginateModule='+value,'searchTbleReport');
			asyncLoadPageByGetRequset('getSimpleQueryPaginationData?inelig_incomple_flag=<s:property value="#request.inelig_incomple_flag"/>&unlicensed_flag=<s:property value="#request.unlicensed_flag"/>&selSiteIdentifier=<s:property value="#request.selSiteIdentifier"/>&selectedSiteId=<s:property value="#request.selectedSiteId"/>&cbuCount=&iShowRows=10&iPageNo=0&paginateModule='+value+'&isMultipleCBBSelected='+$j("#isMultipleCBBSelected").val()+'&isWorkflowActivitySelected='+$j("#isWorkflowActivitySelected").val()+'&unlicensed_flag='+$j('#v_unlicensed_flag').val()+'&inelig_incomple_flag='+$j('#v_inelig_incomple_flag').val()+'&qinelig_flag='+$j('#v_qinelig_flag').val()+'&qincomp_flag='+$j('#v_qincomp_flag').val(),'searchTbleReport',value);
		}else if('<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_IDSUMMARY_REPORT"/>'==value){
			//loadPageByGetRequset('getSimpleQueryPaginationData?iShowRows=10&iPageNo=0&paginateModule='+value,'searchTbleReport');
			asyncLoadPageByGetRequset('getSimpleQueryPaginationData?inelig_incomple_flag=<s:property value="#request.inelig_incomple_flag"/>&unlicensed_flag=<s:property value="#request.unlicensed_flag"/>&selSiteIdentifier=<s:property value="#request.selSiteIdentifier"/>&selectedSiteId=<s:property value="#request.selectedSiteId"/>&cbuCount=<s:property value="#request.uniqCbuCount"/>&iShowRows=10&iPageNo=0&paginateModule='+value+'&isMultipleCBBSelected='+$j("#isMultipleCBBSelected").val()+'&isWorkflowActivitySelected='+$j("#isWorkflowActivitySelected").val()+'&unlicensed_flag='+$j('#v_unlicensed_flag').val()+'&inelig_incomple_flag='+$j('#v_inelig_incomple_flag').val()+'&qinelig_flag='+$j('#v_qinelig_flag').val()+'&qincomp_flag='+$j('#v_qincomp_flag').val(),'searchTbleReport',value);
		}
	
 }

function loadCallback(value){
	if('<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_LICENSEANDELIGIBILTY_REPORT"/>'==value){
		f_Lic_Elig_grid();
	}else if('<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_QUICKSUMMARY_REPORT"/>'==value){
		f_Shipped_CBU_Grid();
	}else if('<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_CBU_IDSUMMARY_REPORT"/>'==value){
		f_CBU_Sumary_Grid();
	}
}	
function saveCriteria(){
	
	var progressMsg="";
	var title = "Save Filter Criteria";
	progressMsg="<table width='100%'><tr><td width='100%' align='center'><table><tr><td align='center' ><strong>Enter Criteria Name</strong></td><td><input type='text' id='filter_name' name='filterdatapojo.filter_name' /></td></tr><tr style='display:none;' id='criteriaerror'><td colspan='2' align='right'><span style='color: red;'>Please Enter Criteria Name</span></td></tr></table></td></tr>";
	progressMsg += "</table>";				
    progressMsg += "<tr><td width='100%' align='right'><input type='button' name='Save' value='Save' onclick='saveFilter();' /><input type='button' name='Close' value='Close' onclick='closeModal();' /></td></tr>";
    progressMsg += "</table>"; 		 
    $j("#modelPopup1").html(progressMsg);
	$j("#modelPopup1").dialog(
			   {autoOpen: false,
				title: title  ,
				resizable: false,
				closeText: '',
				closeOnEscape: false ,
				modal: true, width : 400, height : 150,
				close: function() {
					//$(".ui-dialog-content").html("");
					//jQuery("#subeditpop").attr("id","subeditpop_old");
	      		$j("#modelPopup1").dialog("destroy");
			    }
			   }
			  ); 
	//$j("#modelPopup1").dialog().parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
	$j("#modelPopup1").dialog("open");
}
function printGrid(){
	
	var reportpk=$j('#reportType').val();
	if($j('#simpleCBUForm').valid() && reportpk!=""){	
		var url='generateQBuilderRep?qbReport='+reportpk+"&cbuCount="+$j('#countVal').val();
		loadDivWithFormSubmit(url,'dummydiv','simpleCBUForm');
		var url='getQBPdf?inelig_incomple_flag=<s:property value="#request.inelig_incomple_flag"/>&unlicensed_flag=<s:property value="#request.unlicensed_flag"/>&selSiteIdentifier=<s:property value="#request.selSiteIdentifier"/>&selectedSiteId=<s:property value="#request.selectedSiteId"/>&cbuCount=<s:property value="#request.cbuCount"/>&iShowRows=10&iPageNo=0&paginateModule='+value+'&isMultipleCBBSelected='+$j("#isMultipleCBBSelected").val()+'&isWorkflowActivitySelected='+$j("#isWorkflowActivitySelected").val()+'&unlicensed_flag='+$j('#v_unlicensed_flag').val()+'&inelig_incomple_flag='+$j('#v_inelig_incomple_flag').val()+'&qinelig_flag='+$j('#v_qinelig_flag').val()+'&qincomp_flag='+$j('#v_qincomp_flag').val()+'&reportPkval='+'licEligRep';
		window.open(url,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
	}
	
}
function clearData(){
	$j('#lookup_search_txt').val("");
	$j('#lookup_tbl_sentries').val("");
	$j('#lookup_tbl_sort_col').val("");
	$j('#lookup_tbl_sort_type').val("");
}
</script>
<s:form name="Tableform" id="Tableform">   
  <s:hidden name="inputs" id="inputs" value=""></s:hidden>	
  <s:hidden name="isMultipleCBBSelected" id="isMultipleCBBSelected" />
  <s:hidden name="unlicensed_flag" id="v_unlicensed_flag" />
  <s:hidden name="inelig_incomple_flag" id="v_inelig_incomple_flag" />
  <s:hidden name="isWorkflowActivitySelected" id="isWorkflowActivitySelected" />
  <s:hidden name="qinelig_flag" id="v_qinelig_flag" />
  <s:hidden name="qincomp_flag" id="v_qincomp_flag" />
</s:form>

<s:hidden name="lookup_search_txt" id="lookup_search_txt"/>
<s:hidden name="lookup_tbl_sentries" id="lookup_tbl_sentries"/>
<s:hidden name="lookup_tbl_sort_col" id="lookup_tbl_sort_col"/>
<s:hidden name="lookup_tbl_sort_type" id="lookup_tbl_sort_type"/>
<s:hidden name="query_builder_tbl" id="query_builder_tbl"/>


<div id="filterResultDivParent" class="portlet">
  <div id="filterResultDiv" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
		style="text-align: center;"><span class="ui-icon ui-icon-minusthick" ></span>		
		<s:text	name="Filter Results" />
	</div>
	<s:hidden name="countVal" id="countVal" value="%{#request.cbuCount}"></s:hidden>
		<div class="portlet-content" >
			<div id="saveQueryBuilderCriteria">
				<table >
				  <tr>
					     <td colspan="4">
					      <strong><s:text name = "garuda.queryBuilder.label.selectionCriteria"/><br></strong>  <s:property value="%{#request.selectedCriteria}" escapeHtml="false"/>
					    </td>
				  </tr>
				  <s:if test="#request.cbuCount!=null && #request.cbuCount!=0">
					  <tr> 
					     	<td >
					        	 <s:property value="#request.uniqCbuCount"/> records returned. 
					     	</td>
					    	 <td>
					    		<s:text name="garuda.queryBuilder.label.reportToView" />
							</td>
							<td colspan="2">
							   <s:select name="reportType" style="width:auto;" 	list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@REPORT_CATEGORY]"
									listKey="subType" listValue="description"  headerKey="" headerValue="Select Report" onchange="getReportData(this.value)" />
									
							   <button onclick="cancelQueryRequest()" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"><span class="ui-button-text"><s:text name="garuda.common.lable.cancel"/></span></button>
							</td>
							<!--<td>
							   <s:if test="customFilter!=null && customFilter.size()>0"><s:select name="customFilters" id="customFilters" list="customFilter" onchange="ExecuteCustomFilterOrd(this.value)" listKey="filter_paraameter" listValue="filter_name" headerKey="" headerValue="-Custom Filter-"/></s:if>
							</td>
					  --></tr> 
					  <!--<tr>
					  		<td colspan="4" align=left>
					  				<button type="button" onclick="javascript:saveCriteria();"><s:text
										name="garuda.queryBuilder.label.submitFilter" ></s:text></button>
					  		</td>
					  </tr>  
				--></s:if>
				<s:else>
						<s:text name="garuda.queryBuilder.label.noRecordFound"/>
				</s:else>
				</table> 
			</div>
	        <div id="searchTbleReport">
	        
	      </div>
        </div>
    </div>
</div>