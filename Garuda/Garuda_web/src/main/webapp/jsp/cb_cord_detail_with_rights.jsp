<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<%@page import="com.velos.ordercomponent.business.util.VelosMidConstants"%>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<jsp:include page="cb_track_session_logging.jsp" />
<%
  if(request.getParameter("widgetSetting")==null || request.getParameter("widgetSetting")=="")
  {
%>
	<jsp:include page="widget-setting.jsp">
	  <jsp:param value="16" name="pageId"/>
	</jsp:include>
<%
  }
%>
<script>

/* variable use for auto defer */
var element_Id="";
var element_Name="";
var mutex_Lock=false;
var autoDeferField_1="";
var autoDeferField_2="";
var autoDeferField_3="";
var autoDeferField_4="";
var elementValue ="";
var refresh_DivNo="";
var refresh_CordID = "";
var refreshDiv_Id = new Array();
var autoDeferFormSubmitParam = new Array();
var assessLinknFlag_Id = new Array();

function vidSaved(id){
	$j("#"+id).show();
	//alert("ffffff");
}

function vid(num){
$j("#showDropDownDn"+num).show();
	//alert("Still Wating ! Is That you ");
}

var switchwf_flag=0;
function showProductInsertWindow(url){
	window.open(url,"ProductInsertReport","height=700,width=1000,scrollbars=1,resizable = 1");
}

	/* window.open( url, "Product Insert Report", "status = 1, height = 1024, width = 1024, resizable = 1","autoscroll=true" );
} */

function showModelWindow(){
	var var_cordId=$j("#pkcord").val();
	showModal('Update Cord Status','updateCordStatus?orderId='+var_cordId,'300','400');
}

function minimumcriteria(cordId){
	var patientId = $j('#patientId').val();
	var idOnBag = $j('#idonBagHidden').val();
	var cbuRegID = $j('#cburegIdHidden').val();
	
	if(patientId == "" || patientId == null || patientId=="undefined"){
		patientId = ""
	}
	
	if(idOnBag == "" || idOnBag == null || idOnBag == "undefined"){
		idOnBag = ""
	}
	
	if(cbuRegID == "" || cbuRegID == null || cbuRegID == "undefined"){
		cbuRegID = ""
	}
	var divIdd="leftcont";
	var divTitle="Minimum Criteria";
	url="minimumcriteria?cdrCbuPojo.cordID="+cordId+'&orderId=<s:property value="orderPojo.pk_OrderId"/>&orderType=<s:property value="orderPojo.orderType" />&moduleEntityId='+cordId+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU"/>&moduleEntityIdentifier='+cbuRegID+'&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_MINIMUMCRITERIA"/>';
	var title = '<div id="notesPopup"><div style="float:left" id="leftTitleCont"><s:text name="garuda.minimumcriteria.label.widgetname"/> <s:text name="garuda.message.modal.headerregid"/> '+cbuRegID+' , <s:text name="garuda.cbuentry.label.idbag"/> : '+idOnBag+' , <s:text name="garuda.recipient&tcInfo.label.recipientid"/> : '+patientId+'</div><div style="float:right" id="rightTitleCont"><span class="ui-icon ui-icon-print" onclick="printQuicklinksWidget(\''+divIdd+'\',\''+cbuRegID+'\',\''+divTitle+'\');"></span></div></div>';
	showModals(title,url,'500','1100','cbuMinimumCriteria',true,true);
	$j('#ui-dialog-title-cbuMinimumCriteria').width('98%');
	$j('#leftTitleCont').width('95%');
	$j('#rightTitleCont').width('5%');
}

function loadPage(url){  
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        success: function (result){
	        	$j('#main').html(result);
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});
}

function constructTable(){
	getDatePic();
	$j('#datepicker13').datepicker({
		dateFormat: 'M dd, yy',
		ampm: true
	});
	$j("#formsListTable").dataTable({"bSort": false,"bRetrieve": true,
		"bDestroy" :true});
	$j("#formsListTable1").dataTable({"bSort": false,"bRetrieve": true,
		"bDestroy" :true});
}
function datePicship(){
	$j('#datepicker15').datepicker({
		dateFormat: 'M dd, yy',
		changeMonth: true,
		changeYear: true,
		prevText: '',
		nextText: '',
		showButtonPanel: true,
		closeText: "Done",
		currentText: "Today",
		ampm: true
	});
}

$j(function() {
	 $j('.lookupmenu :text').removeAttr("disabled");
	getDatePic();
	datePicship();
	maximizePage();
	
	 if($j("#pkViaOther").val()==$j("#viabSelected").val()){
		 $j("#viabilityView").show();
		}else{
			$j("#viabilityView").hide();
			}

		if($j("#pkOther").val()==$j("#cfuSelected").val()){
		 $j("#cfuCountView").show();
		}else{
			$j("#cfuCountView").hide();
		}
	
	/*$j('#datepicker3').datetimepicker({
		dateFormat: 'M dd, yy',
		ampm: true
	});*/
	
	$j('#datepicker13').datepicker({
		dateFormat: 'M dd, yy',
		ampm: true
	});
	
});

function finalReview(cordId){
	setEmptyOrders();
	//window.location='finalCbuReview?licenceUpdate=False&esignFlag=review&cdrCbuPojo.cordID='+cordId;
	submitpost('finalCbuReview',{'licenceUpdate':'False','esignFlag':'review','cdrCbuPojo.cordID':cordId});
	}

function setEmptyOrders(){
	var orderId = $j("#orderId").val();
	var orderType = $j("#orderType").val();

	if(typeof(orderId)=='undefined'){
		$j("#orderId").val("");
		orderId="";
	}else if(orderId!=null && orderId.indexOf(",")!=-1){
		$j("#orderId").val("");
		orderId="";
	}
	
	if(typeof(orderType)=='undefined'){
		$j("#orderType").val("");
		orderType="";
	}else if(orderType!=null && orderType.indexOf(",")!=-1){
		$j("#orderType").val("");
		orderType="";
	}
}
function completeReqInf(cordId){
	var divIdd="completeTd";
	var cbuRegID='<s:property value="cdrCbuPojo.registryId"/>';
	var divTitle='Complete Required Information';
	setEmptyOrders();
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	showModals('<div id="notesPopup"><div style="float:left"><s:text name="garuda.completeReqInfo.label.modal.title"/>:'+$j("#registryId").val() + patient_data+'</div><div style="float:right"><span class="ui-icon ui-icon-print" onclick="printQuicklinksWidget(\''+divIdd+'\',\''+cbuRegID+'\',\''+divTitle+'\');"></span></div></div>','completeReqInfo?esignFlag=review&cdrCbuPojo.cordID='+cordId+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CRI_VIEW_CRI" />','570','1100','dialogForCRI');
	$j('#ui-dialog-title-dialogForCRI').width('98%');
}

function enableAcknowledgeDate(value){
	var temp = $j('#pkTempUnavailable').val();
	if(value == temp ){
		$j("#acknowledgeDiv").attr('style','display:block');
	}
	else{
		$j("#acknowledgeDiv").attr('style','display:none');
	}
}
$j(document).ready(function(){
	//commited without order
	/*if($j('#orderId').val()==""){
		$j('#finalReviewBtn').attr('disabled','true');
		$j('#mincriteriaBtn').attr('disabled','true');
		}*/
	
    var vordStat=$j('#orderstatus').val();
    var pkresolved=$j('#pkresolveid').val();
	var vflag=$j('#flag').val();
	$j('button,input[type=submit],input[type=button]').button();
});


function showReqClinicInfo(){
	setEmptyOrders();
	showModal('Required Clinical Information Checklist','showReqClinicInfo?cdrCbuPojo.cordID='+$j("#pkcordId").val());
}

function showModalCbuStatus(title,url,height,width)
{
	$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
	if($j('#modelPopup1').html()==null){
		var obj="<div id='modelPopup1'></div>";
		$j('body').append(obj);
	}
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Please Wait.... <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	
	$j("#modelPopup1").html(progressMsg);
		$j("#modelPopup1").dialog(
				   {autoOpen: false,
					   open: function(event, ui) {
					   $j(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
					   $j(this).closest('.ui-dialog').find('.ui-dialog-title').append('<span  class="ui-dialog-titlebar-close ui-corner-all" style="cursor: pointer;"><span class="ui-icon ui-icon-closethick"/></span>');
					   $j('.ui-dialog-titlebar-close').bind("mouseover",function(){ 
						    $j(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').addClass('ui-state-hover');
						});
						$j('.ui-dialog-titlebar-close').bind("mouseout",function(){ 
						    $j(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').removeClass('ui-state-hover');
						});
						$j('.ui-dialog-titlebar-close').bind("click",function(){ 
							  conformcloseModal();
						});
					},
					title: title  ,
					resizable: true,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {						
		      		    jQuery("#modelPopup1").dialog("destroy");
				    }
				   }
				  ); 
		$j("#modelPopup1").dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#modelPopup1").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
	$j('.progress-indicator').css( 'display', 'none' );
	
}
function showcbustatus(cordId,applyResolFlag, autoDeferRollbackStatus){
	var cbustatustype=$j("#cbustatustype").val();
	setEmptyOrders();
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	var url='updateCbuStatus?licenceUpdate=False&cbustatusin=IS_LOCAL_REG&cdrCbuPojo.cordID='+cordId+'&autoDeferRollbackStatus='+autoDeferRollbackStatus;
	showModalCbuStatus('<s:text name="garuda.cbustatus.label.updatecbustatus"/>:<s:property value="cdrCbuPojo.registryId" />'+patient_data,url,'450','600');
} 
	function addTest(cordId,cbuOtherTestFlag){
	  var specCollDate = $j("#datepicker1").val();
	  var specimenId =$j("#fkSpecimenId").val();
	  var headerLnght = $j("#added thead th").length;
	  //alert(headerLnght);
		var preTstDate =document.getElementById("preTestDateStrn").value;
		//alert(preTstDate);
		var postTstDate =document.getElementById("postTestDateStrn").value;
		//alert(postTstDate);
		var postTstThawDate = new Array();
		//postTstThawDate=null;
		var counter=0;
			for(counter=0; counter<headerLnght-3 ; counter++){
				 var value1 = counter+1;
				 //var temp = document.getElementById("otherTestDateStrn"+value1).value;
			var val =document.getElementById("otherTestDateStrn"+value1).value;
			postTstThawDate[counter] = val.replace(",",":");
			//alert(postTstThawDate[counter]);
			}
		  showModal('Add New Test','addNewTest?cordId='+cordId+'&specimenId='+specimenId+'&cbuOtherTestFlag='+cbuOtherTestFlag+'&specCollDate='+specCollDate+'&headerLnght='+headerLnght+'&preTstDate='+preTstDate+'&postTstDate='+postTstDate+'&postTstThawDate='+postTstThawDate,'500','700');
		
	}
function editTest(rowIndex,pkPatLabs){
	  var specCollDate = $j("#datepicker1").val();
	  var specimenId =$j("#fkSpecimenId").val();
	  showModal('Edit Test','editAddNewTestLabs?rowIndex='+rowIndex+'&specimenId='+specimenId+'&pkPatLabs='+pkPatLabs+'&specCollDate='+specCollDate,'500','700'); 
}



function showCbbDetails(cbbid){
	showModal('Cord Blood Bank ID:<s:property value="cdrCbuPojo.site.siteIdentifier"/>','showCBBDefaults?cbbDefault='+cbbid+'&diableCBBEditAndPrintFlag=1','400','500');
}
function callPending(){
	window.location="pendingOrders";
}


function getsearchResultsBack(){
	//window.location='getMainCBUDetails?cbuid='+$j("#cbuid").val();
	document.getElementById('sampleform').submit();
}

$j(function(){
	flagCbuInfo = true;
	flagFinalElig = true;
	flagmIdm = true;
	flagOtherData = true;
	flagCordId = true;
	flagCordProc = true;
	flagHla = true;
	flagHealthHistory = true;
	flagNoteList = true;
	flagLabSummary = true;
	flagmStaticPanel = true;
});

var cbuHistoryFlag=0;
function getcbuhistory(){
	if($j('#cbuhistoryspan').is(".ui-icon-plusthick")){
		if(cbuHistoryFlag==0){
			loadMoredivs('getCbuHistory?pkcordId=<s:property value="cdrCbuPojo.cordID" />','cbuhistorycontentDiv','cbuCordInformationForm');
			cbuHistoryFlag=1;
			$j('#cbuReqHistoryTbl').css("width","100%");
			$j("#cbuRHistory1").attr('checked',true);
			$j('#cReqhistory').show();
			historyTblOnLoad();
			 
		}
	}
	else if($j('#cbuhistoryspan').is(".ui-icon-minusthick")) {
		$j('#cbuhistorycontentDiv').show();
	}
}

var currReqflag=0;

function getCurrent(){
	
	if($j('#currentReqProSpan').is(".ui-icon-plusthick")){
		if(currReqflag==0){
			loadMoredivs('getCurrReqDetailsFromViewClin?orderId=<s:property value="orderPojo.pk_OrderId"/>&orderType=<s:property value="getCodeListDescById(orderPojo.orderType)"/>&pkcordId=<s:property value="cdrCbuPojo.cordID" />','currentRequestProgressMainDiv,quicklinksDiv','cbuCordInformationForm');
			currReqflag=1;
			$j("#userIdval").width("100px");
			getSearchableSelect('userIdval','id');
			setpfprogressbar();
			getprogressbarcolor();
		}
	}
	else if($j('#currentReqProSpan').is(".ui-icon-minusthick")){
		$j('#currentRequestProgressMainDiv').show();
	}
}

function loadOrderDetails(divId)
{
	var ordType='<s:property value="getCodeListDescById(orderPojo.orderType)"/>';
	var sampleAtlab='<s:property value="orderPojo.sampleAtLab"/>';
	
	if(ordType=='CT' && sampleAtlab=='Y'){
		ordType='<%=VelosMidConstants.CT_LAB_ORDER%>';
	}
	if(ordType=='CT' && sampleAtlab=='N'){
		ordType='<%=VelosMidConstants.CT_SHIP_ORDER%>';
	}	
	
	var url='getGroupsByIdsForOrderType?fkCbbId=<s:property value="cdrCbuPojo.fkCbbId"/>&orderId=<s:property value="orderPojo.pk_OrderId"/>&orderType='+ordType+'&pkcordId=<s:property value="cdrCbuPojo.cordID" />&divId='+divId+'&regIdval=<s:property value="cdrCbuPojo.registryId" />';
	loadPageByGetRequset(url,'usersitegrpsdropdown');
}


function historyTblOnLoad(){
	
	var staticpanelwidth=$j(".viewcbutoppanel").width();
	var staticpanelheight=$j(".viewcbutoppanel").height();
	var columnwidth=staticpanelwidth*0.98;
	var RecCount=$j('#htentries').val();
	
	var is_all_entries=$j('#showsRowcbuhistorytbl').val();
	var pageNo=$j('#paginateWrapper5').find('.currentPage').text();
	var showEntries=$j('#history_tbl_se').val();
	var sort=1;
	var sortDir="desc";
	var SortParm=$j('#history_sort_col').val();
	var cordIdval=$j("#cbucordidval").val();
	var seachTxt="";
	var preTxt=$j('#history_search_txt').val();
	

	fn_events1('cbuhistorytbl','targetall1','ulSelectColumn1','showsRowcbuhistorytbl','PENDING');
	
	if(SortParm!=undefined && SortParm!=null && SortParm!=""){
		sort=$j('#history_sort_col').val();
		sortDir=$j('#history_sort_typ').val();
	}	
	
	if((preTxt!=null && $j.trim(preTxt)!="")){
		seachTxt=preTxt;
	}
	

	if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
		is_all_entries='5';
		$j('#history_tbl_se').val(is_all_entries);
		showEntries=$j('#history_tbl_se').val();
	}
	
	showEntriesTxt='Show <select name="showsRow" id="showsRowcbuhistorytbl" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+RecCount+'">ALL</option></select> Entries';
	
	
	if(showEntries=='ALL'){

					hstrytbl1 =	$j('#cbuhistorytbl').dataTable({
						"sScrollY": staticpanelheight,
						"sDom": "frtiS",
						"sAjaxSource": 'getJsonPagination.action?TblFind=historytbl&allEntries=ALL&pkcordId='+cordIdval+'&pkorderid='+$j("#orderId").val(),
						"bServerSide": true,
						"bProcessing": true,
						"bRetrieve" : true,
						"bDestroy": true,
						"aaSorting": [[ sort, sortDir ]],
						"oSearch": {"sSearch": seachTxt},
						"bSortCellsTop": true,
						"bDeferRender": true,
						"bAutoWidth": false,
						"sAjaxDataProp": "aaData",
						"aoColumnDefs": [
										   {		
												"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
						                	 	return "";
						                  }},
						                  {		
						                	    "aTargets": [1], "mDataProp": function ( source, type, val ) {
					                	 	     return source[0];
					                	  }},
						                  { 	
					                	         "aTargets": [2], "mDataProp": function ( source, type, val ) { 
					                	    	 return source[1];
						                  }},
										  {
						                		"aTargets": [3], "mDataProp": function ( source, type, val ) { 
												 return source[2];
										  }},
										  {
												 "aTargets": [4], "mDataProp": function ( source, type, val ) {
											 	 return source[3];
										  }}
										  		
										],
					"fnInitComplete": function(){
						
										},
										"fnDrawCallback": function() { 
											             setSortData('historyTHeadTr','history_sort_col','history_sort_typ');
													     ColManForAll('cbuhistorytbl','targetall1','true');
														 $j('#cbuhistorytbl_wrapper').find('.dataTables_scrollFoot').hide();
													     $j('#cbuhistorytbl_info').show();
														 $j("#showsRowcbuhistorytbl option:contains('ALL')").attr('selected', 'selected');
										}
					});  
					$j('#cbuhistorytbl_filter').before(showEntriesTxt);
	
		}else{
	
					hstrytbl1 =	$j('#cbuhistorytbl').dataTable({
						"sScrollY": staticpanelheight,
						"sAjaxSource": 'getJsonPagination.action?TblFind=historytbl&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries+'&pkcordId='+cordIdval+'&pkorderid='+$j("#orderId").val(),
						"bServerSide": true,
						"bProcessing": true,
						"bRetrieve" : true,
						"bDestroy": true,
						"aaSorting": [[ sort, sortDir ]],
						"oSearch": {"sSearch": seachTxt},
						"bSortCellsTop": true,
						"bDeferRender": true,
						"bAutoWidth": false,
						"sAjaxDataProp": "aaData",
						"aoColumnDefs": [
										   {		
												"aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
						                	 	return "";
						                  }},
						                  {		
						                	    "aTargets": [1], "mDataProp": function ( source, type, val ) {
					                	 	     return source[0];
					                	  }},
						                  { 	
					                	         "aTargets": [2], "mDataProp": function ( source, type, val ) { 
					                	    	 return source[1];
						                  }},
										  {
						                		"aTargets": [3], "mDataProp": function ( source, type, val ) { 
												 return source[2];
										  }},
										  {
												 "aTargets": [4], "mDataProp": function ( source, type, val ) {
											 	 return source[3];
										  }}
										  		
										],
					"fnInitComplete": function(){
						
										},
										"fnDrawCallback": function() { 
															setSortData('historyTHeadTr','history_sort_col','history_sort_typ');
													        ColManForAll('cbuhistorytbl','targetall1','true');
										}
					});  
    }
	
	$j('#cbuhistorytbl_info').hide();
	$j('#cbuhistorytbl_paginate').hide();
	$j('#cbuhistorytbl_length').empty();
	$j('#cbuhistorytbl_length').replaceWith(showEntriesTxt);
	
	
	if($j('#hentries').val()!=null || $j('#hentries').val()!=""){
		$j('#showsRowcbuhistorytbl').val($j('#hentries').val());
		}
	
	$j('#showsRowcbuhistorytbl').change(function(){
		var textval=$j('#showsRowcbuhistorytbl :selected').html();
		$j('#history_tbl_se').val(textval);
		paginationFooter(0,"getCbuHistory,showsRowcbuhistorytbl,temp,cbuCordInformationForm,chistory,historyTblOnLoad,cbucordidval");
	});
	
	
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('position','static');
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('z-index','1000');
	
	//$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').after('<div style="clear:both" class="clearDiv"></div>');

	/* $j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHeadInner table').width(columnwidth);
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollFootInner table').width(columnwidth);
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').find('.dataTables_scrollHeadInner table').width(columnwidth);
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollBody table').width(columnwidth);  */
	
	
	$j('#cbuhistorytbl_wrapper').css('overflow','hidden');
	$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollBody').scroll(function(){
		$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('overflow','hidden');
	});
	$j('#cbuhistorytbl_wrapper').find('#thSelectColumn div').click(function(){
		$j('#cbuhistorytbl_wrapper').find('.dataTables_scrollHead').css('overflow','visible');
	}); 
	
	$j('#cbuhistorytbl_filter').find(".searching1").keyup(function(e){
		  var val = $j(this).val();
		  val=$j.trim(val);
		  $j('#history_search_txt').val(val);
		  if(e.keyCode==13)
		  {
			  paginationFooter(0,"getCbuHistory,showsRowcbuhistorytbl,temp,cbuCordInformationForm,chistory,historyTblOnLoad,cbucordidval");
		  }
	});
	
	if(! $j("#cbuRHistory1").is(':checked')){
		$j("#cbuHistory1").attr('checked',true);
		$j('#chistory').show();
	}
    if(!$j('#cbuHistoryForm').hasClass('historyLoaded')){
    	fn_events('cbuReqHistoryTbl','targetall2','ulSelectColumn2');
    	$j('#cbuHistoryForm').addClass('historyLoaded');
    }
	
	$j('.detailhstry').css('max-height',staticpanelheight);
	
	$j('#cbuHistory1').bind('click',function(ui){
		var oTable = $j('div.dataTables_scrollBody> #cbuhistorytbl', ui.panel).dataTable();
		if ( oTable.length > 0 ) {
			oTable.fnAdjustColumnSizing(false);
		}
	});
}

function setpfprogressbar(){
	$j("#cbuavailconfbar").progressbar({value: 100});
	$j("#completeReqInfobar").progressbar({value: 100});
	$j("#additityingandtest").progressbar({value: 100});
	$j("#resRecbar").progressbar({value: 100});
	$j("#Resolutionbar").progressbar({value: 100});
	$j("#Acknowledgebar").progressbar({value: 100});
	$j("#ctshipmentbar").progressbar({value: 100});
	$j("#ctshipdatebar").progressbar({value: 100});
	$j("#genpackageslipbar").progressbar({value: 100});
	$j('#finalReviewbar').progressbar({value: 100});
	$j('#cordShipedbar').progressbar({value: 100});
	$j('#nmdpsamplebar').progressbar({value: 100});
}

function getprogressbarcolor(){
	var todate='';
	var schshipdat='';
	if($j("#ctShipDateId").val()!=null && $j("#ctShipDateId").val()!=""){
		schshipdat=$j.datepicker.parseDate("M dd, yy",$j("#ctShipDateId").val());
		todate=$j.datepicker.parseDate("M dd, yy",$j("#todate").val());
	}
	var currentOrdStat='<s:property value="orderPojo.orderStatus"/>';
	var status_resolved='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED)" />';
	var status_completed='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)" />';
	var disableUnComplTask=0;	
	if(currentOrdStat==status_resolved || currentOrdStat==status_completed){
		disableUnComplTask=1;
	}
	
	if($j("#tskConfirmCbuAvailcompl").val()==1 || $j("#cordavailflag").val()=='Y'){
		$j("#cbuavailconfbar").addClass('progressbarselected');
		$j("#cbuavailconfbar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass" ></img>');
	}
	if(disableUnComplTask==1 && ( $j("#tskConfirmCbuAvailcompl").val()!=1 && $j("#cordavailflag").val()!='Y')){
		$j("#cbuavailconfbar").addClass('UnCompleTask');
		$j("#cbuavailconfbar").find('div').after('<img src="images/tsk_nt_needed.png"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass" ></img>');
	}
	
	if($j("#tskAdditiTypingcompl").val()==1 || ($j("#hlaTypingAvailFlag").val()!=null && $j("#hlaTypingAvailFlag").val()!='')){
		$j("#additityingandtest").addClass('progressbarselected');
		$j("#additityingandtest").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"  ></img>');
	}

	if(disableUnComplTask==1 && ($j("#tskAdditiTypingcompl").val()!=1 && ($j("#hlaTypingAvailFlag").val()==null || $j("#hlaTypingAvailFlag").val()==''))){
		$j("#additityingandtest").addClass('UnCompleTask');
		$j("#additityingandtest").find('div').after('<img src="images/tsk_nt_needed.png"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	
	if($j("#tskShipmentInfocompl").val()==1 || $j("#shipschFlag").val()=='Y'){
		$j("#ctshipmentbar").addClass('progressbarselected');
		$j("#ctshipmentbar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	if(disableUnComplTask==1 && ($j("#tskShipmentInfocompl").val()!=1 && $j("#shipschFlag").val()!='Y')){
		$j("#ctshipmentbar").addClass('UnCompleTask');
		$j("#ctshipmentbar").find('div').after('<img src="images/tsk_nt_needed.png"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	
	if($j("#tskGenPackageSlipcompl").val()==1 || $j("#packageslipFlag").val()=='Y'){
		$j("#genpackageslipbar").addClass('progressbarselected');
		$j("#genpackageslipbar").find('div').after('<img src="images/pfprogressbar.jpg" style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	if(schshipdat!=null && schshipdat!='' && schshipdat<todate){
		$j("#ctshipdatebar").addClass('progressbarselected');
		$j("#ctshipdatebar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	if(disableUnComplTask==1 && (schshipdat==null || schshipdat=='' || schshipdat>=todate)){
		$j("#ctshipdatebar").addClass('UnCompleTask');
		$j("#ctshipdatebar").find('div').after('<img src="images/tsk_nt_needed.png"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	if(schshipdat==null || schshipdat=='' || schshipdat>=todate){
		$j("#ctshipdatebar").removeClass('progressbarselected');
	}

	if(disableUnComplTask==1 && ($j("#tskGenPackageSlipcompl").val()!=1 && $j("#packageslipFlag").val()!='Y')){
		$j("#genpackageslipbar").addClass('UnCompleTask');
		$j("#genpackageslipbar").find('div').after('<img src="images/tsk_nt_needed.png" style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	
	if($j("#tskShipmentConfirmcompl").val()==1 || $j("#cordShipedFlag").val()=='Y'){
		$j("#cordShipedbar").addClass('progressbarselected');
		$j("#cordShipedbar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	if(disableUnComplTask==1 && ($j("#tskShipmentConfirmcompl").val()!=1 && $j("#cordShipedFlag").val()!='Y')){
		$j("#cordShipedbar").addClass('UnCompleTask');
		$j("#cordShipedbar").find('div').after('<img src="images/tsk_nt_needed.png" style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	
	if($j("#tskReqClincInfocompl").val()==1 || $j("#completeReqInfoFlag").val()=='Y'){
		$j("#completeReqInfobar").addClass('progressbarselected');
		$j("#completeReqInfobar").find('div').after('<img src="images/pfprogressbar.jpg" style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}else{
		$j("#completeReqInfobar").removeClass('progressbarselected');
	}
	if($j("#tskSampleShipmentcompl").val()==1 || $j("#nmdpSampleShipped").val()=='Y'){
		$j("#nmdpsamplebar").addClass('progressbarselected');
		$j("#nmdpsamplebar").find('div').after('<img src="images/pfprogressbar.jpg" style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	if(disableUnComplTask==1 && ($j("#tskSampleShipmentcompl").val()!=1 && $j("#nmdpSampleShipped").val()!='Y')){
		$j("#nmdpsamplebar").addClass('UnCompleTask');
		$j("#nmdpsamplebar").find('div').after('<img src="images/tsk_nt_needed.png" style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	
	if($j("#tskFinalCbuReviewcompl").val()==1 || $j("#finalReviewTaskFlag").val()=='Y'){
		$j("#finalReviewbar").addClass('progressbarselected');
		$j("#finalReviewbar").find('div').after('<img src="images/pfprogressbar.jpg" style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}else{
		$j("#finalReviewbar").removeClass('progressbarselected');
	}

	if(disableUnComplTask==1 && ($j("#tskFinalCbuReviewcompl").val()!=1 && $j("#finalReviewTaskFlag").val()!='Y')){
		$j("#finalReviewbar").addClass('UnCompleTask');
		$j("#finalReviewbar").find('div').after('<img src="images/tsk_nt_needed.png" style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	
	if($j("#recRecDateFlag").val()!='Not Available' && ($j("#recRecDateFlag").val()!=null && $j("#recRecDateFlag").val()!='')){
		$j("#resRecbar").addClass('progressbarselected');
		$j("#resRecbar").find('div').after('<img src="images/pfprogressbar.jpg"  style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}

	if(disableUnComplTask==1 && ($j("#recRecDateFlag").val()=='Not Available' || ($j("#recRecDateFlag").val()==null || $j("#recRecDateFlag").val()==''))){
		$j("#resRecbar").addClass('UnCompleTask');
		$j("#resRecbar").find('div').after('<img src="images/tsk_nt_needed.png" style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	
	if($j("#resolByTCFlag").val()!=null && $j("#resolByTCFlag").val()!=''){
		$j("#Resolutionbar").addClass('progressbarselected');
		$j("#Resolutionbar").find('div').after('<img src="images/pfprogressbar.jpg" style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	if($j("#resolByCBBFlag").val()!=null && $j("#resolByCBBFlag").val()!=''){
		$j("#Resolutionbar").addClass('progressbarselected');
		$j("#Resolutionbar").find('div').after('<img src="images/pfprogressbar.jpg" style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
	if($j("#tskAcknowledgeResolcompl").val()==1 || $j("#acknowledgeFlag").val()=='Y'){
		$j("#Acknowledgebar").addClass('progressbarselected');
		$j("#Acknowledgebar").find('div').after('<img src="images/pfprogressbar.jpg"   style="display: none;width: 100%; height: 30px;border: none;" class="printClass"   ></img>');
	}
}

function showPdfForms(cordID,doc){
	//alert("Hi ! I get you "+   "  " +doc +" " +cordID);
	var url='getPdfObject?cdrCbuPojo.CordID='+cordID+'&pdfDocument='+doc;
	//alert(url);
   window.open(url);
	//window.open('getPdfObject?cdrCbuPojo.CordID='+cordID+'&pdfDocument='+doc,'PDF Form','toolbar=no,scrollbars=no,resizable=yes,menubar=no,status=yes,width=800,height=500');
}
function showPdfFormsWithMrq(cordID,doc){
	var url='getPdfObject?cdrCbuPojo.CordID='+cordID+'&pdfDocument='+doc+'&repId=184&repName=&params='+cordID+":MRQ";
	//alert(url);
	window.open(url);
	//window.open('getPdfObject?cdrCbuPojo.CordID='+cordID+'&pdfDocument='+doc,'PDF Form','toolbar=no,scrollbars=no,resizable=yes,menubar=no,status=yes,width=800,height=500');
}
function showPdfFormsWithMrqForSystem(cordID,doc){
	var url='getPdfObject?cdrCbuPojo.CordID='+cordID+'&pdfDocument='+doc+'&repId=186,188&repName=';
	//alert(url);
	window.open(url);
	//window.open('getPdfObject?cdrCbuPojo.CordID='+cordID+'&pdfDocument='+doc,'PDF Form','toolbar=no,scrollbars=no,resizable=yes,menubar=no,status=yes,width=800,height=500');
}

function createPackageSlip(){
	var pkPakageslip=$j("#pkPackingSlipId").val();
	var flag=$j("#hlaTypingAvailFlag").val();
	var cordId='<s:property value="cdrCbuPojo.cordID" />';
	var orderId='<s:property value="orderPojo.pk_OrderId"/>';
	var orderType='<s:property value="getCodeListDescById(orderPojo.orderType)"/>';
	if($j("#tskAdditiTypingcompl").val()==null || $j("#tskAdditiTypingcompl").val()==0){
		alert("<s:text name="garuda.cbu.order.updatedHlaTyping"/>");
	}
	else
	{
		if(flag=='Y' && $j("#recHlaEnteredFlag").val()!=null && $j("#recHlaEnteredFlag").val()=='Y'){
		if($j("#dataModifiedFlag").val()!=null && $j("#dataModifiedFlag").val()!='' && $j("#dataModifiedFlag").val()=='Y'){
			jConfirm('<s:text name="garuda.cbu.order.generatePakageSlipAlert"/>', '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
			    if(r){
			    	loadMoredivs('updatePackingSlipDetFromViewCBU?pkcordId='+cordId+'&orderId='+orderId+'&orderType='+orderType+'&schshipdate='+$j("#ctShipDateId").val()+'&shipcompany='+$j("#shipCompany").val()+'&trackNo='+$j("#trackNo").val()+'&samtype='+$j("#SamTypeAvailable").val()+'&aliqtype='+$j("#AliquotsType").val()+'&pkShipmentId='+$j("#pkShipmentId").val()+'&cbbId=<s:property value="cdrCbuPojo.site.siteId"/>&pkHlaId='+$j("#recentHlaId").val()+'&sampleAtLab=<s:property value="orderPojo.sampleAtLab"/>','currentRequestProgressMainDiv','sampleform');
					pkPakageslip=$j("#pkPackingSlipId").val();
			    }
			    loadMoredivs('createPackageSlipFromViewCBU?orderId='+orderId+'&pkcordId='+cordId+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&orderType='+orderType+'&cdrCbuPojo.cordID='+cordId,'currentRequestProgressMainDiv,chistory,cReqhistory','sampleform');
			    setpfprogressbar();
				getprogressbarcolor();
				historyTblOnLoad();
			    window.open('cbuPdfReports?repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+cordId,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
			});

		}
		else if($j("#dataModifiedFlag").val()!=null && $j("#dataModifiedFlag").val()!='' && $j("#dataModifiedFlag").val()=='N'){
			loadMoredivs('updatePackingSlipDetFromViewCBU?pkcordId='+cordId+'&orderId='+orderId+'&orderType='+orderType+'&schshipdate='+$j("#ctShipDateId").val()+'&shipcompany='+$j("#shipCompany").val()+'&trackNo='+$j("#trackNo").val()+'&samtype='+$j("#SamTypeAvailable").val()+'&aliqtype='+$j("#AliquotsType").val()+'&pkShipmentId='+$j("#pkShipmentId").val()+'&cbbId=<s:property value="cdrCbuPojo.site.siteId"/>&pkHlaId='+$j("#recentHlaId").val()+'&sampleAtLab=<s:property value="orderPojo.sampleAtLab"/>','currentRequestProgressMainDiv','sampleform');
			pkPakageslip=$j("#pkPackingSlipId").val();
			loadMoredivs('createPackageSlipFromViewCBU?orderId='+orderId+'&pkcordId='+cordId+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&orderType='+orderType+'&cdrCbuPojo.cordID='+cordId,'currentRequestProgressMainDiv,chistory,cReqhistory','sampleform');
		    setpfprogressbar();
			getprogressbarcolor();
			historyTblOnLoad();
		    window.open('cbuPdfReports?repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+cordId,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
		}
		else if($j("#dataModifiedFlag").val()!=null && $j("#dataModifiedFlag").val()==''){
			loadMoredivs('createPackageSlipFromViewCBU?orderId='+orderId+'&pkcordId='+cordId+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&orderType='+orderType+'&cdrCbuPojo.cordID='+cordId,'currentRequestProgressMainDiv,chistory,cReqhistory','sampleform');
		    setpfprogressbar();
			getprogressbarcolor();
			historyTblOnLoad();
		    window.open('cbuPdfReports?repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+cordId,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
		}
		}
		else if(flag=='N'){
			if($j("#dataModifiedFlag").val()!=null && $j("#dataModifiedFlag").val()!='' && $j("#dataModifiedFlag").val()=='Y'){
				jConfirm('<s:text name="garuda.cbu.order.generatePakageSlipAlert"/>', '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
				    if(r){
				    	loadMoredivs('updatePackingSlipDetFromViewCBU?pkcordId='+cordId+'&orderId='+orderId+'&orderType='+orderType+'&schshipdate='+$j("#ctShipDateId").val()+'&shipcompany='+$j("#shipCompany").val()+'&trackNo='+$j("#trackNo").val()+'&samtype='+$j("#SamTypeAvailable").val()+'&aliqtype='+$j("#AliquotsType").val()+'&pkShipmentId='+$j("#pkShipmentId").val()+'&cbbId='+$j("#cbbId").val()+'&pkHlaId='+$j("#recentHlaId").val()+'&sampleAtLab='+$j("#sampleAtLabVal").val(),'ctShipmentInfoContentDiv,currentRequestProgressMainDiv','cbuCordInformationForm');
						pkPakageslip=$j("#pkPackingSlipId").val();
				    }
				    loadMoredivs('createPackageSlipFromViewCBU?orderId='+orderId+'&pkcordId='+cordId+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&orderType='+orderType+'&cdrCbuPojo.cordID='+cordId,'currentRequestProgressMainDiv,chistory,cReqhistory','sampleform');
				    setpfprogressbar();
					getprogressbarcolor();
					historyTblOnLoad();
				    window.open('cbuPdfReports?repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+cordId,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
				});

			}
			else if($j("#dataModifiedFlag").val()!=null && $j("#dataModifiedFlag").val()!='' && $j("#dataModifiedFlag").val()=='N'){
				loadMoredivs('updatePackingSlipDetFromViewCBU?pkcordId='+cordId+'&orderId='+orderId+'&orderType='+orderType+'&schshipdate='+$j("#ctShipDateId").val()+'&shipcompany='+$j("#shipCompany").val()+'&trackNo='+$j("#trackNo").val()+'&samtype='+$j("#SamTypeAvailable").val()+'&aliqtype='+$j("#AliquotsType").val()+'&pkShipmentId='+$j("#pkShipmentId").val()+'&cbbId='+$j("#cbbId").val()+'&pkHlaId='+$j("#recentHlaId").val()+'&sampleAtLab='+$j("#sampleAtLabVal").val(),'ctShipmentInfoContentDiv,currentRequestProgressMainDiv','cbuCordInformationForm');
				pkPakageslip=$j("#pkPackingSlipId").val();
				loadMoredivs('createPackageSlipFromViewCBU?orderId='+orderId+'&pkcordId='+cordId+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&orderType='+orderType+'&cdrCbuPojo.cordID='+cordId,'currentRequestProgressMainDiv,chistory,cReqhistory','sampleform');
			    setpfprogressbar();
				getprogressbarcolor();
				historyTblOnLoad();
			    window.open('cbuPdfReports?repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+cordId,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
			}
			else if($j("#dataModifiedFlag").val()!=null && $j("#dataModifiedFlag").val()=='' && $j("#dataModifiedFlag").val()!="undefined"){
				loadMoredivs('createPackageSlipFromViewCBU?orderId='+orderId+'&pkcordId='+cordId+'&shipmentType='+$j("#pkCbuShipment").val()+'&pkPackingSlip='+$j("#pkPackingSlipId").val()+'&orderType='+orderType+'&cdrCbuPojo.cordID='+cordId,'currentRequestProgressMainDiv,chistory,cReqhistory','sampleform');
			    setpfprogressbar();
				getprogressbarcolor();
				historyTblOnLoad();
			    window.open('cbuPdfReports?repId=174&repName=Packaging Slip&selDate=&params='+pkPakageslip+':'+cordId,'Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
			}
		}
		else{
			alert('<s:text name="garuda.cbu.order.updatedHlaTypingalert"/>');
		}
	}
}
function submitAssignto(){
	$j('#ctassignto').hide();
	var assigntoval=$j('#userIdval').val();
	setEmptyOrders();
	var refDiv='currentRequestProgress';
	
	if(assigntoval!=null && assigntoval!=""){
		var url='updateAssignOrderFromViewClinical?pkcordId='+'<s:property value="cdrCbuPojo.cordID" />'+'&orderId='+'<s:property value="orderPojo.pk_OrderId"/>'+'&orderType='+'<s:property value="getCodeListDescById(orderPojo.orderType)"/>'+'&assigntopk='+assigntoval+'&uflag=assignedto'
		loaddiv(url,refDiv);
		$j("#userIdval").width("100px");
		getSearchableSelect('userIdval','id');
		setpfprogressbar();
    	getprogressbarcolor();	
	}
	else{
		alert("<s:text name="garuda.cbu.detail.selectUserAlert"/>");
	}
	
	
}
function showEsignAssignTo(el){
	
	var t=$j(el);
	var obj=t.offset();
	var xOffset=180;
	var yOffset=30;
	if(navigator.appName=="Microsoft Internet Explorer"){yOffset=yOffset+15;}
	
	if('<s:property value="getCodeListDescById(orderPojo.orderType)"/>'=='HE'){
		xOffset=xOffset+200;
	}
	
	var oldAssignto=$j('#hAssignTo').val();
	
	
	if($j(el).val()!=""){
	
		var reAssignto=$j(el).val();
		
		if(oldAssignto!=null && oldAssignto!=undefined && oldAssignto!='undefined' && oldAssignto!=reAssignto){
			
			$j("#ctassignto")
			.css({left:obj.left-xOffset,top:obj.top+yOffset})
			.fadeIn("fast").show()
			.addClass('dateDivClass');
			$j('#ctassignto').show();
			
		}
			
	}
}

function showFinalCbuReviewInfo(){
	setEmptyOrders();	
		if($j('#compReqIdFlag').val() == 1)
			{
			if ( $j.browser.msie ){
			if (document.all) {
				   var xMax = screen.width, yMax = screen.height
				  }
				  else if (document.layers) {
				   var xMax = window.outerWidth, yMax = window.outerHeight
				  }
				  else {
				   var xMax = 640, yMax=480
				  }; 
				window.open('finalCbuReview?licenceUpdate=False&esignFlag=review&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR" />','','width='+xMax+',height='+yMax+',top=0,left=0,resizable=yes');
			}
			
		else{
			
			window.open('finalCbuReview?licenceUpdate=False&esignFlag=review&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_FCR" />');
		}
			}
		
		else{
			alert("<s:text name='garuda.cbu.CRI.alert'/>");
		}
}
function showSwitchWorkflowHelpMsg(id){
	msg=$j('#'+id).html();
	overlib(msg,CAPTION,'Workflow Switched details');
}
function fn_ResetHistorySort(){
	if($j('#history_sort_col').val()!=undefined && $j('#history_sort_col').val()!=null && $j('#history_sort_col').val()!=""){
			
			$j('#history_sort_col').val('');
			$j('#history_sort_type').val('');
			var oTable = $j('#cbuhistorytbl').dataTable();
			oTable.fnSort( [ [1,'desc'] ] );
			oTable.fnDraw();
		}
	} 
$j(function(){
    $j("#esign_cancel").click(function(){
    	
        if(element_Id !=""){
            $j("#"+element_Id).val(elementValue);
            $j("#"+element_Id).removeClass('labChanges');
        	//$j("#"+element_Id+" option[value="+elementValue+"]").attr("selected", "selected");
        	parent.document.getElementById(element_Id).focus();
        	
        if(element_Id=='bacterialResult'){
        	
        	 bacterial(elementValue);
        }
        else if(element_Id=='fungalResultModal'){
        	fungal(elementValue);
        	if(assessLinknFlag_Id[0]=='true'){
        		$j("."+assessLinknFlag_Id[1]).show();
        		$j("."+assessLinknFlag_Id[2]).hide();
        	}
        }
        else if(element_Id=='hemoglobinScrnTest' && assessLinknFlag_Id[0]=='true'){
        		$j("."+assessLinknFlag_Id[1]).show();
        		$j("."+assessLinknFlag_Id[2]).hide();
        }
        }     
        else{
            var x = document.getElementsByName(element_Name);  
        	$j('input[name="'+element_Name+'"]').val(elementValue);
        	(x.length == 2 )?(x[1].focus()):x[0].focus();
        }
      jQuery("#modalEsign1").dialog("destroy");
        $j('#modalEsign1').css('display',"none");
		$j('#cordIdEntryValid1').css('display',"none");
		$j('#cordIdEntryMinimum1').css('display',"none");
		$j('#cordIdEntryPass1').css('display',"none");
		$j("#submitcdrsearch1").attr("disabled","disabled");
		 mutex_Lock = false;
		 element_Id ="";
		 element_Name ="";
   });

   $j("#submitcdrsearch1").click(function(){
	   
	   $j('#autodeferfieldflag').val('true');
         if(mutex_Lock == true){
         jQuery("#modalEsign1").dialog("destroy");
         
         if(autoDeferFormSubmitParam.length == 3){
           autoDeferModalFormSubmitRefreshDiv(autoDeferFormSubmitParam[0],autoDeferFormSubmitParam[1],autoDeferFormSubmitParam[2]);
           refreshMultipleDivsOnViewClinical(refresh_DivNo,refresh_CordID);
           if(refreshDiv_Id.length==3 && refreshDiv_Id[2]=="CRI"){          	  
          	  setTimeout(function(){closeModal(),2000}); 
          	  closeModals('dialogForCRI');
           }else if(refreshDiv_Id[2]== ''){
        	   setTimeout(function(){
       			closeprogressMsg();
       		},0);
           }
         }
         else if(autoDeferFormSubmitParam.length == 5){             
        	 autoDeferModalFormSubmitRefreshDiv(autoDeferFormSubmitParam[0],autoDeferFormSubmitParam[1],autoDeferFormSubmitParam[2],autoDeferFormSubmitParam[3],autoDeferFormSubmitParam[4]);
             refreshMultipleDivsOnViewClinical(refresh_DivNo,refresh_CordID);                          
         }  
         else if(autoDeferFormSubmitParam.length == 4){
        	 loadDivWithFormSubmitDefer(autoDeferFormSubmitParam[0],autoDeferFormSubmitParam[1],autoDeferFormSubmitParam[3]);
			 refreshMultipleDivsOnViewClinical(refresh_DivNo,refresh_CordID);
             
         }
         else if(autoDeferFormSubmitParam.length == 8){
        	 modalFormPageRequest(autoDeferFormSubmitParam[0],autoDeferFormSubmitParam[1],autoDeferFormSubmitParam[2],autoDeferFormSubmitParam[3],autoDeferFormSubmitParam[4],autoDeferFormSubmitParam[5],autoDeferFormSubmitParam[6],autoDeferFormSubmitParam[7]);
         }
        if(refreshDiv_Id.length==2){
        	$j("#"+refreshDiv_Id[0]).css('display','block');
			$j("#"+refreshDiv_Id[1]).css('display','none');
        }
        else if(refreshDiv_Id.length!=0){
        	$j("#"+refreshDiv_Id[0]).css('display','none');
        }
         mutex_Lock = false;
        $j('#modalEsign1').css('display',"none");
		$j('#cordIdEntryValid1').css('display',"none");
		$j('#cordIdEntryMinimum1').css('display',"none");
		$j('#cordIdEntryPass1').css('display',"none");
		$j("#submitcdrsearch1").attr("disabled","disabled");
		
		refresh_DivNo="";
		refresh_CordID="";
		element_Id ="";
		element_Name = "";
		autoDeferField_1="";
		autoDeferField_2="";
		autoDeferField_3="";
		elementValue="";
		refreshDiv_Id.length=0;
		autoDeferFormSubmitParam.length=0;
     }    
   });
});
</script>
<s:hidden name="history_search_txt" id="history_search_txt"/>
<s:hidden name="history_sort_col" id="history_sort_col"/>
<s:hidden name="history_sort_typ" id="history_sort_typ"/>
<s:hidden name="history_tbl_se" id="history_tbl_se"/>
<s:form id ="sampleform" name="sampleform" action="getMainCBUDetails" method="post">
<s:hidden name="cbuid" id="cbuid"/>
<s:hidden name="cdrCbuPojo.registryId" id="registryId"/>
<s:hidden name="cdrCbuPojo.fkSpecimenId" id="fkSpecimenId"></s:hidden> 
</s:form>
<s:form id="cbuCordInformationForm">
<div id="cbuCordInfoMainDiv">	
	
	<div id="widgetPrintDiv" style="display:none;"> 
	<!-- This Used For Print -->
	</div>
	<div id="shipperUrlDiv">
		<s:if test="codeLstCustColLst!=null && codeLstCustColLst.size()>0">
		<s:iterator value="codeLstCustColLst" var="rowVal">
			<s:hidden name="shipperUrl" id="shipperUrl" value="%{#rowVal[0]}"></s:hidden>
		</s:iterator>
		</s:if>		
	</div>

<div id="sampleInventoryDiv">	
	<s:if test="codeLstCustColLst!=null && codeLstCustColLst.size()>0">
	<s:iterator value="codeLstCustColLst" var="rowVal">
		<s:hidden name="tableName" id="tableName"></s:hidden>
		<s:hidden name="columnName" id="columnName"></s:hidden>
	</s:iterator>
	</s:if>
	<s:hidden name="sampleCount" id="sampleCount"></s:hidden>
</div>
<s:if test="hasViewPermission(#request.staicPanl)==true">
<div id="loadStaticPanelDiv">
	<jsp:include page="cb_load_static_panel.jsp"></jsp:include>
</div>
</s:if>
<div class="columnDiv cbuDetailsDiv">	
	  <div id="Basicinfo">
	     <!-- <div class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" id="historydiv" style="height: 100%;">
				<div style="text-align: center;" class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
				<span class="ui-icon ui-icon-newwin"></span>
				<span class="ui-icon ui-icon-plusthick"></span>
				<s:text name="garuda.cdrcbuview.label.pfwidget" />
				</div>
				<div id="loadCbuHistoryDiv">
				    Load cb_cbuHistory.jsp in the cbuHistoryDiv on click of this widget 				   
				</div>		
	     </div>-->
	     
	     <s:if test='cdrCbuPojo.cordnmdpstatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@AC) || cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus==getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)'>
	    <table width="100%">
		<s:if test="isCdrUser==true"><tr><td>
		<div id="currentReqProgInViewClinical" style="width:100%">
			<jsp:include page="cb_current_req_progress.jsp"/>
		</div></td></tr>
		</s:if>
		</table>
		</s:if>
        
         <div class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" id="clinicalrecorddiv" style="height: 100%;">
			<div style="text-align: center;
			 font-size:16pt;" class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			<s:text name="garuda.cdrcbuview.label.cdrwidget" />
			</div>
            <div class="portlet-content">
               <div class="column Lcolumn">
					<s:if test="hasViewPermission(#request.updateCBUInfowidget)==true">			
						<div class="portlet" id="cbuCharacteristicsparent">
							<div id="cbuCharacteristics"
								class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
							<div
								class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
								style="text-align: center;"><span class="ui-icon ui-icon-print" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','1','loadCbuCharacteristicDiv','cbuCharacteristicsparent','50','');clickheretoprint('cbuCharacteristics','<s:property value="cdrCbuPojo.registryId" />')"></span><span 
								class="ui-icon ui-icon-newwin"></span><span
								class="ui-icon ui-icon-plusthick" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','1','loadCbuCharacteristicDiv','cbuCharacteristicsparent','50','')"></span> <!--<span
								class="ui-icon ui-icon-close"></span>-->
								 <s:text
								name="garuda.cdrcbuview.label.cbu_information" /></div>
								<div class="portlet-content" style="display: none;">
								    <div id="loadCbuCharacteristicDiv" >
								      <!-- Load cb_viewcbuinfo.jsp and edit button of edit cbu characteristics on click of this widget and notes data -->
								      <s:if test="%{foruploadDoc==true}"><jsp:include page="cb_load_cbu_characteristic.jsp"/></s:if>	
								    </div>
								</div>
							</div>
						</div>
				 </s:if>
				 <s:if test="hasViewPermission(#request.updateIDwidget)==true">
					<div class="portlet" id="cbuIdsparent">
						<div id="cbuIds" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
						<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span class="ui-icon ui-icon-print" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','5','loadCordIdsDataDiv','cbuIdsparent','2','');clickheretoprint('cbuIds','<s:property value="cdrCbuPojo.registryId" />')"></span><span
							class="ui-icon ui-icon-newwin"></span>
							<span class="ui-icon ui-icon-plusthick" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','5','loadCordIdsDataDiv','cbuIdsparent','2','')"></span>
							<!--  span class="ui-icon ui-icon-report" onclick="window.open('cbuPdfReports?repId=169&repName=IDs Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"></span>--> 
							<!--<span class="ui-icon ui-icon-close"></span>--> <s:text
							name="garuda.advancelookup.label.id" />
						</div>
							<div class="portlet-content" style="display: none;">
							    <div id="loadCordIdsDataDiv" >
								      <!-- Load cbu ids data and notes on click of this widget -->	
							    </div>
							</div>
						</div>
					</div>
				</s:if>
				 <s:if test="hasViewPermission(#request.updateLabSmmry)==true">
					<div class="portlet" id="labsummarydivparent">
					    <div id="labsummarydiv"	class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
						    <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span class="ui-icon ui-icon-print" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','10','loadLabSummaryDiv','labsummarydivparent','98','');clickheretoprint('labsummarydiv','<s:property value="cdrCbuPojo.registryId" />')"></span>
							<span
							class="ui-icon ui-icon-newwin"></span><span
							class="ui-icon ui-icon-plusthick" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','10','loadLabSummaryDiv','labsummarydivparent','98','')"></span> 
							<!-- span class="ui-icon ui-icon-report" onclick="window.open('cbuPdfReports?repId=173&repName=Lab Summary Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"></span> -->
							<s:text	name="garuda.cordentry.label.labsummary" /></div>						
								<div class="portlet-content" style="display: none;">
								    <div id="loadLabSummaryDiv">
								      <!-- Load Lab summary data on click of this widget and notes data -->	
								    <s:if test="foruploadDoc==true">  <jsp:include page="cb_labSummeryForCordInfo.jsp"/></s:if>
								    </div>
								</div>
						</div>
					</div>
				</s:if>
				<s:if test="hasViewPermission(#request.updateProInfoWidget)==true">
					<div class="portlet" id="cbuprocessinginfodivparant">
						<div id="cbuprocessinginfodiv" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
							<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
								style="text-align: center;"><span class="ui-icon ui-icon-print" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','6','loadProcessingProcedureDataDiv','cbuprocessinginfodivparant','554','');printProcessing('loadProcessingProcedureDataDiv','<s:property value="cdrCbuPojo.registryId" />');"></span><span
								class="ui-icon ui-icon-newwin"></span>
								<span class="ui-icon ui-icon-plusthick" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','6','loadProcessingProcedureDataDiv','cbuprocessinginfodivparant','554','')"></span> 
								<!-- span class="ui-icon ui-icon-report" onclick="window.open('cbuPdfReports?repId=170&repName=Processing Information Report&selDate=&params=<s:property value="cdrCbuPojo.cordID"/>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"></span> -->
								<!--<span class="ui-icon ui-icon-close"></span>--> <s:text
								name="garuda.unitreport.label.processinginfo.subheading" />
							</div>
							<div class="portlet-content" style="display: none;">
							   <div id="loadProcessingProcedureDataDiv" >
								      <!-- Load processign procedures data and documents and notes on click of this widget -->
								   <s:if test="foruploadDoc==true">   <jsp:include page="cb_load_cord_proc_info.jsp"/></s:if>	
							   </div>
							</div>
						</div>
					</div>
				</s:if>				
				<s:if test="hasViewPermission(#request.updateElgblty)==true">
					<div class="portlet" id="eligibilityInfoparent">
						<div id="eligibilityInfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
							<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
								style="text-align: center;"><span class="ui-icon ui-icon-print" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','2','loadEligibilityDiv','eligibilityInfoparent','130','');clickheretoprint('eligibilityInfo','<s:property value="cdrCbuPojo.registryId" />')"></span><span
								class="ui-icon ui-icon-newwin"></span><span
								class="ui-icon ui-icon-plusthick" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','2','loadEligibilityDiv','eligibilityInfoparent','130','')"></span>
								<!--  span class="ui-icon ui-icon-report" onclick="window.open('cbuPdfReports?repId=171&repName=Licensure and Eligibility Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"></span>-->
								<s:text	name="garuda.cdrcbuview.label.eligibility" />
							</div>
								<div class="portlet-content" style="display: none;">
								    <div id="loadEligibilityDiv" >
								      <!-- Load document upload data for eligibility and final cbu review eligibility questions and dumn data with product tag  on click of this widget -->
								   <s:if test="foruploadDoc==true">  <jsp:include page="cb_load_eligibility_final_cbu_review.jsp"></jsp:include></s:if>	
								    </div>
								</div>
						</div>
					</div>
				</s:if>		
            </div>
            <div class="column Mcolumn"></div>
            <div class="column Rcolumn">
				<s:if test="hasViewPermission(#request.updateCbuHLA)==true">
					<div class="portlet" id="cbuHlaparent">
						<div id="cbuHla" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
						<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
							style="text-align: center;"><span class="ui-icon ui-icon-print" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','7','loadHlaDataDiv','cbuHlaparent','38','');clickheretoprint('cbuHla','<s:property value="cdrCbuPojo.registryId" />')"></span><span
							class="ui-icon ui-icon-newwin"></span><span
							class="ui-icon ui-icon-plusthick" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','7','loadHlaDataDiv','cbuHlaparent','38','')"></span><!--<span
							class="ui-icon ui-icon-close"></span>--> <s:text
							name="garuda.cdrcbuview.label.hla" />
						</div>
						     <div class="portlet-content" style="display: none;">
								 <div id="loadHlaDataDiv" >
									      <!-- Load hla data and documents and notes on click of this widget -->
									  <s:if test="foruploadDoc==true">    <jsp:include page="cb_load_hla_data.jsp"/></s:if>	
								 </div>
						    </div>
						</div>
					</div>
				</s:if>
				<s:if test="hasViewPermission(#request.updateInfectDisease)==true">	
					<div class="portlet" id="infectDiseaseparent">
						<div id="infectDisease"	class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
							<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
								style="text-align: center;"><span class="ui-icon ui-icon-print" onclick="window.open('cbuPdfReports?repId=185&repName=IDM Widget Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:IDM','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"></span><span
								class="ui-icon ui-icon-newwin"></span><span
								class="ui-icon ui-icon-plusthick" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','3','loadIdmDataDiv','infectDiseaseparent','290','8')"></span><!--<span
								class="ui-icon ui-icon-close"></span>--> 
								 <!-- <span class="ui-icon ui-icon-report" onclick="window.open('cbuPdfReports?repId=172&repName=Infectious Disease Markers Report&selDate=&params=<s:property value="cdrCbuPojo.cordID" />:IDM','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"></span> --> 
								<s:text
								name="garuda.cdrcbuview.label.infectious_disease_markers" />
							</div>
								<div class ="portlet-content" style="display: none;">
								     <div id="loadIdmDataDiv" >
								      <!-- Load idm motes , idm documents and idm form data with patient info(TBD)  on click of this widget -->
								  <s:if test="foruploadDoc==true">    <jsp:include page="cb_load_idm_data.jsp"/></s:if>	
								    </div>
								</div>
						</div>
					</div>
				</s:if>
				<s:if test="hasViewPermission(#request.updateHlthHis)==true">
					<div class="portlet" id="healthHistoryparent">
						<div id="healthHistory"	class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
							<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
								style="text-align: center;"><span class="ui-icon ui-icon-print" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','8','loadFormsDataDiv','healthHistoryparent','290','6');clickheretoprint('healthHistory','<s:property value="cdrCbuPojo.registryId" />')"></span><span
								class="ui-icon ui-icon-newwin"></span><span
								class="ui-icon ui-icon-plusthick" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','8','loadFormsDataDiv','healthHistoryparent','290','6')"></span><!--<span
								class="ui-icon ui-icon-close"></span>--><s:text
								name="garuda.cdrcbuview.label.health_history_screening" />
							</div>
								<div class="portlet-content" style="display: none;">
								    <div id="loadFormsDataDiv" >
									      <!-- Load fmhq and mrq and health screening notes and health screening documnets data on click of this widget -->
									      <s:if test="foruploadDoc==true"><jsp:include page="cb_load_health_history_screen.jsp"/></s:if>
									      	
								    </div>
								</div>
						</div>
					</div>
				</s:if>
					<div class="portlet" id="otherTestResultparent">					   
						<div id="otherTestResult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
							<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
								style="text-align: center;"><span class="ui-icon ui-icon-print" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','4','loadOthersDataDiv','otherTestResultparent','2','');clickheretoprint('otherTestResult','<s:property value="cdrCbuPojo.registryId" />')"></span><span
								class="ui-icon ui-icon-newwin"></span><span
								class="ui-icon ui-icon-plusthick" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','4','loadOthersDataDiv','otherTestResultparent','2','')"></span><!--<span
								class="ui-icon ui-icon-close"></span>--> <s:text
								name="garuda.cdrcbuview.label.other_relevant" />
							</div>
								<div class="portlet-content" style="display: none;">
								    <div id="loadOthersDataDiv" >
								      <!-- Load others document data on click of this widget -->	
								      <s:if test="foruploadDoc==true">   <jsp:include page="cb_load_other_data.jsp"/></s:if>
								    </div>
								</div>
						</div>
					</div>
				<s:if test="hasViewPermission(#request.updateClnclNotes)==true">
					<div class="portlet" id="clinicalNotesparent">
					<div id="clinicalNotes"	class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
						style="text-align: center;"><span class="ui-icon ui-icon-print" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','9','loadWholeNotesDiv','clinicalNotesparent','2','');clickheretoprint('clinicalNotes','<s:property value="cdrCbuPojo.registryId" />')"></span><span
						class="ui-icon ui-icon-newwin"></span><span
						class="ui-icon ui-icon-plusthick" onclick="loadClinicalWidgetData('<s:property value="cdrCbuPojo.cordID"/>','9','loadWholeNotesDiv','clinicalNotesparent','2','')"></span><!--<span
						class="ui-icon ui-icon-close"></span>--> <s:text
						name="garuda.cdrcbuview.label.clinical_notes" />
					</div>
						<div class="portlet-content" style="display: none;">
						    <div id="loadWholeNotesDiv" >
								 <!-- Load all notes (include cb_note_list.jsp) click of this widget -->	
							</div>
						</div>
					</div>
					</div>
				</s:if>
            </div>
	     </div>
       </div>
       <s:if test="hasViewPermission(#request.viewRqstCBUhis)==true">
	      <input type="hidden" name="cbucordtest" id="cbucordidval" value=<s:property value="cdrCbuPojo.cordID" /> />
			<div class="portlet" id="historydivparent">
				<div id="historydiv"
					class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="height: 100%;">
				<div
					class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
					style="text-align: center;font-size:16pt;"><span
					class="ui-icon ui-icon-print"
					onclick="getcbuhistory(); printHistoryWidget('cbuhistorycontentDiv','<s:property value="cdrCbuPojo.registryId"/>');"></span><span
					class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id="cbuhistoryspan" onclick="getcbuhistory();"></span>
				<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
					class="ui-icon ui-icon-minusthick"></span>--><s:text
					name="garuda.ctOrderDetail.portletname.cbunrequesthistory" /></div>
				<div class="portlet-content" style="display: none;" id="cbuhistorycontentDiv">
					<jsp:include page="cb_cbuHistory.jsp"></jsp:include>
				</div>
				</div>
			</div>
		</s:if>
      </div>
</div>
</div>
</s:form>
<div id="usersitegrpsdropdown">
			<s:hidden value="#request.grpList.size()" />
	        <s:if test="#request.grpList.size()>1">
				<jsp:include page="pf_userSiteGroups.jsp"></jsp:include>
			</s:if>
</div>
<div id="newDiv" style="display: none;"></div>
<script>
function printProcessing(div,regId){
	var iddiv = $j('#procinfo').html() + '<br/>' + $j('#cbbProcedureViewparent').html() + '<br/>'+ $j('#procandcountCbu').html() + '<br/>' + $j('#procandcountMat').html() + '<br/>' + $j('#procccingNotes').html() +'<br/>' + $j('#loadProcessingDocInfoDiv').html();
	$j('#newDiv').html(iddiv);
	$j('#newDiv #refCbbProcOnly').html("");
	$j('#newDiv #sampleInventoryDiv').html("");
	$j('#newDiv #processnote').removeAttr('style');
	clickheretoprint('newDiv',regId);
}

</script>