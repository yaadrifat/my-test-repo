<%@ taglib prefix="s"  uri="/struts-tags"%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", 0); 
%>
<!--<div id="accordionReadOnly">
	<h3><a href="#"><s:text name="garuda.common.lable.sectionone"/></a></h3>
	<div>
		<table width="100%" cellspacing="0" cellpadding="0">
		  <tr>
		     <td><s:text name="garuda.fdoe.level.cbb" />:</td>
		     <td>
		        <s:if test="#request.sites!=null">
				   <s:select name="cdrCbuPojo.fkCbbId" listKey="siteId" listValue="siteName" list="#request.sites" disabled="true"></s:select>
				</s:if>
		     
		        <s:if test="cdrCbuPojo.site!=null">
		           <input type="text" name="producttagsname" value="<s:property value="cdrCbuPojo.site.siteName"/>" readonly="readonly"/>
		        </s:if>
		     </td>																     
		  </tr>
		  <tr>																  
		      <td>
		         <s:text name="garuda.productinsert.label.address" />
		      </td>
		      <td>
		         <s:property value="cbbAddress.address1" />
		      </td>																      
		  </tr>
		  <tr>
		     <td>
		         <s:text name="garuda.cbbdefaults.lable.country" />
		      </td>
		      <td>
		         <s:property value="cbbAddress.country" />
		      </td>																  
		  </tr>
		  <tr>
		     <td>
		         <s:text name="garuda.fdoe.level.eligible" />
		      </td>
		      <td>
		         <input type="checkbox" name="eligibledeclfdoe" <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.fkCordCbuEligibleReason">checked="checked"</s:if> ></input>
		      </td>		
		  </tr>
		  <tr>
		     <td>
		        <s:text name="garuda.fdoe.level.incomplete" />
		     </td>
		     <td>
		        <input type="checkbox" name="incompletedeclfdoe" <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId">checked="checked"</s:if> ></input>
		     </td>
		  </tr>
		  <tr>
		     <td>
		        <s:text name="garuda.fdoe.level.incomplete_ques1" />
		     </td>
		     <td>
		        <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId">
		         <div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
		           <input type="checkbox" name="incompletedeclfdoe" <s:if test="declPojo.mrqQues1b==false">checked="checked"</s:if> ></input>
		         </div>  
		       </s:if>
		       <s:else>
		           <input type="checkbox" name="incompletedeclfdoe" <s:if test="declPojo.mrqQues1b==false">checked="checked"</s:if> ></input>
		       </s:else>
		     </td>
		  </tr>
		  <tr>
		     <td>
		        <s:text name="garuda.fdoe.level.incomplete_ques2" />
		     </td>
		     <td>
		       <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId">
		         <div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
		           <input type="checkbox" name="incompletedeclfdoe" <s:if test="declPojo.mrqQues1b==true">checked="checked"</s:if> ></input>
		         </div>  
		       </s:if>
		       <s:else>
		           <input type="checkbox" name="incompletedeclfdoe" <s:if test="declPojo.mrqQues1b==true">checked="checked"</s:if> ></input>
		       </s:else>																	        
		     </td>
		  </tr>
		  <tr>
		     <td>
		         <s:text name="garuda.fdoe.level.ineligible" />
		      </td>
		      <td>
		         <input type="checkbox" name="ineligibledeclfdoe" <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.inEligiblePkid">checked="checked"</s:if>  ></input>
		      </td>		
		  </tr>
		  <tr>
		     <td>
		        <s:text name="garuda.fdoe.level.notapplicable" />
		     </td>
		     <td>
		        <input type="checkbox" name="notapplideclfdoe" <s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.notCollectedToPriorReasonId">checked="checked"</s:if> ></input>
		     </td>
		  </tr>
		</table>
	</div>	
	<h3><a href="#"><s:text name="garuda.common.lable.sectiontwo"/></a></h3>
	<div>
		<fieldset >
						<legend> 
							<s:text name="garuda.fdoe.level.sectionformname1"></s:text>
			</legend>
		    <table width="100%" cellspacing="0" cellpadding="0">
		       <tr>
			       <td>
			         <s:text name="garuda.fdoe.level.sectionformname1_ques1" />
			       </td>
			       <td>
			         <input type="checkbox" name="sectionformname1_ques1" <s:if test="declPojo.idmQues7a==false">checked="checked"</s:if> ></input>
			       </td>
		       </tr>
		       <tr><td style="padding-top:10px;" colspan="2"></td></tr>
		       <tr>
			       <td>
			         <s:text name="garuda.fdoe.level.sectionformname1_ques2" />
			       </td>
			       <td>
			         <input type="checkbox" name="sectionformname1_ques2" <s:if test="declPojo.idmQues6==false">checked="checked"</s:if> ></input>
			       </td>
		       </tr>
		       <tr><td style="padding-top:10px;" colspan="2"></td></tr>
		       <tr>
			       <td>
			         <s:text name="garuda.fdoe.level.sectionformname1_ques3" />
			       </td>
			       <td>
			         <input type="checkbox" name="sectionformname1_ques3" <s:if test="declPojo.idmQues8==true">checked="checked"</s:if> ></input>
			       </td>
		       </tr>
		       <tr><td style="padding-top:10px;" colspan="2"></td></tr>
		       <tr>
			       <td>
			         <s:text name="garuda.fdoe.level.sectionformname1_ques4" />
			       </td>
			       <td>
			         <input type="checkbox" name="sectionformname1_ques4" ></input>
			       </td>
		       </tr>
		    </table>
		</fieldset>
		<fieldset >
						<legend> 
							<s:text name="garuda.fdoe.level.sectionformname2"></s:text>
			</legend>
		    <table width="100%" cellspacing="0" cellpadding="0">
		       <tr>
			       <td>
			         <s:text name="garuda.fdoe.level.sectionformname2_ques1" />
			       </td>
			       <td>
			         <input type="checkbox" name="sectionformname2_ques1" <s:if test="declPojo.mrqQues2==true">checked="checked"</s:if> ></input>
			       </td>
		       </tr>
		       <tr><td style="padding-top:10px;" colspan="2"></td></tr>
		       <tr>
			       <td>
			         <s:text name="garuda.fdoe.level.sectionformname2_ques2" />
			       </td>
			       <td>
			         <input type="checkbox" name="sectionformname2_ques2" <s:if test="declPojo.phyFindQues5==true">checked="checked"</s:if> ></input>
			       </td>
		       </tr>
		       <tr><td style="padding-top:10px;" colspan="2"></td></tr>
		       <tr>
			       <td>
			         <s:text name="garuda.fdoe.level.sectionformname2_ques3" />
			       </td>
			       <td>
			         <input type="checkbox" name="sectionformname2_ques3" <s:if test="declPojo.phyFindQues4==true">checked="checked"</s:if> ></input>
			       </td>
		       </tr>
		       <tr><td style="padding-top:10px;" colspan="2"></td></tr>
		       <tr>
			       <td>
			         <s:text name="garuda.fdoe.level.sectionformname2_ques4" />
			       </td>
			       <td>
			         <input type="checkbox" name="sectionformname2_ques4" <s:if test="declPojo.mrqQues2==false">checked="checked"</s:if> ></input>
			       </td>
		       </tr>
		       <tr><td style="padding-top:10px;" colspan="2"></td></tr>
		       <tr>
			       <td>
			         <s:text name="garuda.fdoe.level.sectionformname2_ques5" />
			       </td>
			       <td>
			         <input type="checkbox" name="sectionformname2_ques5" ></input>
			       </td>
		       </tr>
		    </table>
		</fieldset>
	</div>	
	<h3><a href="#"><s:text name="garuda.common.lable.sectionthree"/></a></h3>
	<div>
		 <fieldset>
						<legend> 
							<s:text name="garuda.fdoe.level.sectionformname3"></s:text>
			</legend>
		    <table width="100%" cellspacing="0" cellpadding="0">		    
		     <tr>
		         <td><s:text name="garuda.fdoe.level.name" /></td>  
		         <td style="padding-right:30px;">
		         <s:set name="finalReviewCompletedBy" value="cbuFinalReviewPojo.finalReviewCreatedBy" ></s:set>
		         <s:property value="getUserNameById(#finalReviewCompletedBy)" /></td>
		         <td><s:text name="garuda.fdoe.level.signature" /></td>
		         <td></td>
		         <td><s:text name="garuda.fdoe.level.date" /></td>
		         <td><s:date name="%{cbuFinalReviewPojo.finalReviewCreatedOn}"	id="finalReviewDate" format="MMM dd, yyyy" /><s:property value="%{finalReviewDate}"/></td>
		     </tr>
		     <tr>
		        <td colspan="6">
		        </td>
		     </tr>
		     <tr>
		        <td colspan="3">
		           <fieldset>
									<legend> 
										<s:text name="garuda.fdoe.level.req"></s:text>
						</legend>
					    <table width="100%" cellspacing="0" cellpadding="0">
					       <tr>
					           <td>
					              <s:text name="garuda.fdoe.level.estab"></s:text>
					           </td>
					           <td>
					              <input type="checkbox" name="estabfdoe" />
					           </td>
					       </tr>
					       <tr>
					           <td>
					              <s:text name="garuda.fdoe.level.compestab"></s:text>
					           </td>
					           <td>
					              <input type="checkbox" name="compestabfdoe" />
					           </td>
					       </tr>
					       <tr>
					           <td>
					              <s:text name="garuda.fdoe.level.nmdpadd"></s:text>
					           </td>
					           <td>
					              <input type="checkbox" name="nmdpaddfdoe" />
					           </td>
					       </tr>
					    </table>
				   </fieldset>
		        </td>
		        <td colspan="3">
		           <table width="100%" cellspacing="0" cellpadding="0">
		              <tr>
		                  <td><s:text name="garuda.fdoe.level.estabname" /></td>
		              </tr>
		              <tr>
		                  <td></td>
		              </tr>
		              <tr>
		                  <td><s:text name="garuda.fdoe.level.estabadd" /></td>
		              </tr>
		           </table>
		        </td>
		     </tr>
		    </table>
		 </fieldset>
	</div>													
 </div>
 --><s:if test="#request.readonly=='true'">
	  <script>
		 $j("#accordionReadOnly").find('input, textarea','select').each(function(){
			 	if($j(this).attr('type')!="radio"){
			 		$j(this).attr('disabled', 'disabled');
			  	}else{
				 	 $j(this).removeAttr("onclick");
					 $j(this).bind("click",function(){
						 return false;
					 });
					 if(!$j(this).is(":checked")){
						 $j(this).attr('disabled', 'disabled');
					}
			  	}
		  });
	  </script>
</s:if>					