<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.eres.web.site.SiteJB" %>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.address.AddressJB" %>
<jsp:useBean id="cbuAction" class="com.velos.ordercomponent.action.CBUAction"/>
<%
String contextpath=request.getContextPath();
%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<script>

$j(function(){
getDatePic();
jQuery('#datepicker91').datepicker('option', { beforeShow: customRange }).focus(function(){
	if(!$j("#ui-datepicker-div").is(":visible"))
		customRange(this);
	if($j(this).val()!=""){
		var thisdate = new Date($j(this).val());
		if(!isNaN(thisdate)){
			var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
			$j(this).val(formattedDate);
		}
	}
});
	$j("#labsumAddTest").validate();
});

function addInfo(fkSpeciId,test,patlabs,divId,fieldName){
	document.getElementById('fkSpecimenId').value=fkSpeciId;
	showModal('Add Test Date Info','saveTestDateInfo?fkSpeciId='+fkSpeciId+'&test='+test+'&patlabs='+patlabs+'&refDiv='+divId+'&fieldName='+fieldName,'500','600');
} 
var nameInc=0;
function addcol(fkSpeciId,noOfLabTests){
	var date = $j("#datepicker91").val();
	if($j("#removeUnknownTestFlag").val()!=null && $j("#removeUnknownTestFlag").val()=='true'){
		noOfLabTests=noOfLabTests-1;
	}
	//alert(date);
	if(date==null || date=="" || date.length<=0){
		alert("<s:text name="garuda.cbu.cordentry.reqTestDateAlert"/>");
		return false;
	}else{
		if($j("#labsumAddTest").valid()){
			closeModal();
		}else{
			return false;
		}
	}
	//else{
		//$j('#dialogadd').css( 'display','none' );;		
	//}
	
	document.getElementById('fkSpecimenId').value=fkSpeciId;
 	var c = $j("#added thead th").length;
 	var totalCount=c
	var maxAllowed=6;
	
	if(totalCount>3)
	{
		totalCount=totalCount-3
		maxAllowed=maxAllowed-totalCount;
		nameInc=totalCount;
	}

	var n=0;
	nameInc++;
	if(maxAllowed>0){	
	$j("#added thead tr").append("<th>Post-Cryopreservation / Thaw</br><span>Test Date</span><input type='text' class='otherTestDateStrn' id='otherTestDateStrn"+nameInc+"' readonly='readonly' value='"+date+"' name='otherTestDateStrn"+nameInc+"' style='width: 210px;'></th>");

	$j("#added tr:gt(1)").each(function(){
				var tempName = 'saveOtherPostTestList'+nameInc+'['+n+'].testresult';
				var fieldName = 'saveOtherPostTestList'+nameInc+'['+n+']';
				var test = $j("#test"+n).val();
				var prepostFlag="otherThaw";
				var patlabs = $j("#patlabs"+n).val();
				var divna = 'saveOtherPostTestList'+nameInc+n+'div';
				var uncrctdLabel="";
				var testIdElement="";
				var measureUnit="";
				if(n<1 || n>=noOfLabTests){
					var firstElement="&nbsp;";
					 uncrctdLabel="&nbsp;";
					 testIdElement="&nbsp;";
					 measureUnit="&nbsp;";
				}else{
					switch(n){
					case 1: 
						measureUnit=" x 10<sup>7</sup>";
						break;
					case 2: 
						measureUnit=" x 10<sup>3</sup>"+"/<s:text name="garuda.cbbprocedures.label.millilitres" />";
						break;
					case 3:
						measureUnit=" <s:text name="garuda.cbbprocedures.label.millilitres" />";
						break;
					/* case 4:
						measureUnit=" x 10<sup>7</sup>";
						break; */
					case 4:
						measureUnit=" x 10<sup>6</sup>";
						break;
					case 5:
						measureUnit=" %";
						break;
					case 6:
						measureUnit=" %";
						break;
					case 7:
						measureUnit=" %";
						break;
					case 8:
						measureUnit=" x 10<sup>6</sup>";
						break;
					case 9:
						measureUnit=" %";
						break;
					case 10:
						measureUnit=" x 10<sup>6</sup>";
						break;	
					case 11:
						measureUnit=" %";					
						break;
					case 12:
						measureUnit=" / 10<sup>5</sup>";
						break;
					case 13:
						measureUnit=" %";
						break;
					default:
						measureUnit="";	
					}
					if(test==<%=request.getAttribute("TNCFkTestId")%>||test==<%=request.getAttribute("TNCUncFkTestId")%>||test==<%=request.getAttribute("TNCUnkIfUncFkTestId")%>){
					 	testIdElement=" <input type='hidden' name='saveOtherPostTestList"+nameInc+"["+n+"].fktestid' value='"+<%=request.getAttribute("TNCUncFkTestId")%>+"' /> ";
					 	uncrctdLabel=" <s:text name="garuda.labtest.label.uncrctd"/>";
					}
					else{
						testIdElement="<input type='hidden' name='saveOtherPostTestList"+nameInc+"["+n+"].fktestid' value='"+test+"' /> ";
						uncrctdLabel="";
					}
					var firstElement="<s:text name="garuda.cbuentry.label.testresult"></s:text>"+"<br><input type='text' style='width:45%' class='positive procclass' size='15' name='"+tempName+"' id='saveOtherPostTestList"+nameInc+n+"testresult' onchange='vid("+nameInc+n+",this.value)' onkeyup=\"checkRangeMthd(this.id,"+"'"+test+"'"+",this.value,"+"'"+prepostFlag+"'"+")\" />"+measureUnit+"<br/><span style='display:none' id='saveOtherPostTestList"+nameInc+n+"testresult_error' class='error'><s:text name=''></s:text> </span><input type='hidden' id='saveOtherPostTestList"+nameInc+n+"testresult_dbStoredVal' value='%{testresult}' />";
				}
				var selectName="saveOtherPostTestList"+nameInc+"["+n+"]" ;
				var firstElement2= getDropDownByType('test_reason',selectName+".fktestreason","reasonFrTestDescOthTm"+nameInc+n);
				firstElement2=firstElement2+"<br/><span style='display:none' id='saveOtherPostTestList"+nameInc+n+"fktestreason_error' class='error'><s:text name="garuda.common.validation.value"></s:text> </span>";
				if(test==<%=request.getAttribute("vaibilityFkTestId")%>){
					var firstElement1= getDropDownByType('vaibility',selectName+".fktestmethod","testMethDescOthTm"+nameInc+n);
					firstElement1=firstElement1+"<br/><span style='display:none' id='saveOtherPostTestList"+nameInc+n+"fktestmethod_error' class='error'><s:text name="garuda.common.validation.value"></s:text> </span>";
				}
				else{
					var firstElement1= getDropDownByType('cfu_cnt',selectName+".fktestmethod","testMethDescOthTm"+nameInc+n);
					firstElement1=firstElement1+"<br/><span style='display:none' id='saveOtherPostTestList"+nameInc+n+"fktestmethod_error' class='error'><s:text name="garuda.common.validation.value"></s:text> </span>";
				}
				//var firstElement1= getDropDownByType('cfu_cnt',selectName+".fktestmethod");
				var firstElement3= getDropDownByType('samp_type',selectName+".fktestspecimen","samp");
				firstElement3=firstElement3+"<br/><span style='display:none' id='saveOtherPostTestList"+nameInc+n+"fktestspecimen_error' class='error'><s:text name="garuda.common.validation.value"></s:text> </span>";
				//var firstElement4= getDropDownByType('vaibility',selectName+".fktestmethod");
				if(test==<%=request.getAttribute("vaibilityFkTestId")%> || test==<%=request.getAttribute("cfuCountFkTestId")%> || <%=request.getAttribute("isTasksRequiredFlag")%> == false){
				var dataDesc="<span style='color: red;'>*</span><s:text name="garuda.common.label.reaForTest"/> "+firstElement2+"<div id='reasonFrTestDescOthTm"+nameInc+n+"' style='display: none;'> <span style='color: red;'>*</span><s:text name="garuda.common.label.describe"/>  "
				+"<br/><input type='text' id='saveOtherPostTestList"+nameInc+n+"<s:text name="garuda.cbu.label.reasonTestDesc"/>'  maxlength='50' name='saveOtherPostTestList"+nameInc+"["+n+"].reasonTestDesc' onkeyup='chekDepandtFldPrs(this.id,this.value)' /><br/><span style='display:none' id='saveOtherPostTestList"+nameInc+n+"reasonTestDesc_error' class='error'><s:text name="garuda.common.validation.description"></s:text> </span></div>"
				+"<span style='color: red;'>*</span><s:text name="garuda.cbuentry.label.sampletype"/> "+firstElement3
				+"<span style='color: red;'>*</span><s:text name="garuda.common.lable.method"/> "+firstElement1+"<div id='testMethDescOthTm"+nameInc+n+"' style='display: none;'> <span style='color: red;'>*</span><s:text name="garuda.common.lable.describe"/>"
				+"<br/><input type='text' id='saveOtherPostTestList"+nameInc+n+"testMthdDesc' maxlength='50' name='saveOtherPostTestList"+nameInc+"["+n+"].testMthdDesc' onkeyup='chekDepandtFldPrs(this.id,this.value)'/><br/><span style='display:none' id='saveOtherPostTestList"+nameInc+n+"testMthdDesc_error' class='error'><s:text name="garuda.common.validation.description"></s:text> </span></div>";
				}
				else{
					var dataDesc="<span style='color: red;'>*</span><s:text name="garuda.common.label.reaForTest"/> "+firstElement2+"<div id='reasonFrTestDescOthTm"+nameInc+n+"' style='display: none;'> <span style='color: red;'>*</span><s:text name="garuda.common.label.describe"/>  "
					+"<br/><input type='text' id='saveOtherPostTestList"+nameInc+n+"<s:text name="garuda.cbu.label.reasonTestDesc"/>'  maxlength='50' name='saveOtherPostTestList"+nameInc+"["+n+"].reasonTestDesc' onkeyup='chekDepandtFldPrs(this.id,this.value)' /><br/><span style='display:none' id='saveOtherPostTestList"+nameInc+n+"reasonTestDesc_error' class='error'><s:text name="garuda.common.validation.description"></s:text> </span></div>"
					+"<span style='color: red;'>*</span><s:text name="garuda.cbuentry.label.sampletype"/> "+firstElement3
					//+"<span style='color: red;'>*</span><s:text name="garuda.common.lable.method"/> "+firstElement1+"<div id='testMethDescOthTm"+nameInc+n+"' style='display: none;'> <span style='color: red;'>*</span><s:text name="garuda.common.lable.describe"/>"
					//+"<br/><input type='text' id='saveOtherPostTestList"+nameInc+n+"testMthdDesc' maxlength='50' name='saveOtherPostTestList"+nameInc+"["+n+"].testMthdDesc' onkeyup='chekDepandtFldPrs(this.id,this.value)'/><br/><span style='display:none' id='saveOtherPostTestList"+nameInc+n+"testMthdDesc_error' class='error'><s:text name="garuda.common.validation.description"></s:text> </span></div>";
					}
				
				var selectList="";
				var cfuCount="";
				$j(this).append("<TD align='left' class='BasicText'>"+testIdElement+"<div id='saveOtherPostTestList"+nameInc+n+"div'>"+firstElement+"</div><div id='measureUnit"+nameInc+n+"'>"+uncrctdLabel+"</div><div id='showDropDown"+nameInc+n+"' style='display: none;'>"+dataDesc+"</div></TD>");
			n++;														
           });
	}else{
		alert("<s:text name="garuda.cbu.cordentry.maxCountAlert"/>");
		}
	customRangeF();
    }

/*$j('#test').click(function(){
	alert("aaaaaaaaaaaaaaaaa");
	var type="cfu_cnt";
	var typeViability = "Viability";
	var typeFirst ="test_reason"
	var typeSecond ="specimen_type"
	var fkSpecimenId =$j("#fkSpecimenId").val();
	$j.ajax({
		      url : "loadOptionType?codeType="+type+"&codeViab="+typeViability,
		      url : "loadOptionType?codeType="+type+"codeVia="+typeViability,
		      async : false,
		      error : function (request, status, error){
		      alert(request.responseText);
		      },
		      success :function(data) {
			  alert("sucess"+data.htmlTest);
			  alert("fkSpecimenId"+fkSpecimenId);
		      addcol(fkSpecimenId);
		      }
		  });  });*/
		  function customRangeF() {
			  	getClearButton();
				var today = new Date();
				var d = today.getDate();
				var m = today.getMonth();
				var y = today.getFullYear();
				var h=today.getHours();
				var mn=today.getMinutes()+1;
				var mindate=$j("#specCollDate2").val();
				var mindate1=mindate.split("-");
				var y1=++mindate1[2];
				var m1=mindate1[1]-1;
				var d1=mindate1[0];
				$j( ".otherTestDateStrn").removeAttr("readonly").addClass("dpDate").each(function(){
					if($j(this).is(":disabled")){
						$j(this).datepicker("disable");
					}
					var s = this;
			        var selectHolder = $j('<span>').css({ position: 'relative' });
				    $j(this).wrap(selectHolder);
			        var titleSpan = $j('<label>').attr({'class': 'tooltipspan placeHolderOfid_'+$j(this).attr("id"),'id':'placeHolderOfid_'+$j(this).attr("id")}).html("mm/dd/yyyy")
				                				.css({'width':0,'height':0,'top':-5,'left':5}).click(function(){
				                					var txtFldId = $j(this).attr("id"); 
				                					txtFldId = txtFldId.replace("placeHolderOfid_","");
				                					if(!$j("#"+txtFldId).is(":disabled")){
					                					$j("#"+txtFldId).focus();
						                			}
				                				});
					if($j(this).val()!=""){
						$j(titleSpan).css({'display':'none'});
					}else{
						$j(titleSpan).css({'display':'block'});
					}
			        $j(this).parent().append(titleSpan);
				}).datepicker({
					dateFormat: 'M dd, yy',
					//minDate: new Date(d1,m1,y1),
					maxDate: new Date(y, m, d),
					changeMonth: true,
					changeYear: true,
					showButtonPanel: true, 
					prevText: '',
					nextText: '',
					showOn: "button",
					buttonImage: "./images/calendar-icon.png",
					buttonImageOnly: true,

			        /* blur needed to correctly handle placeholder text */
			        onSelect: function(dateText, inst) {
			              this.fixFocusIE = true;
			              $j(this).blur().change().focus();
			        },
			        beforeShow: function(input, inst) {
			        	customRange(input);
			        },
			   	   onChangeMonthYear:function(y, m, i){                                
			  	        var d = i.selectedDay;
			  	        $j(this).datepicker('setDate', new Date(y, m - 1, d));
			  	      	$j(this).val(dateFormat(new Date(y, m - 1, d),'mm/dd/yyyy'));
			  	    }
				}).focus(function(){
					$j(".placeHolderOfid_"+$j(this).attr("id")).hide();
					if($j(this).val()!=""){
						var thisdate = new Date($j(this).val());
						if(!isNaN(thisdate)){
							var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
							$j(this).val(formattedDate);
						}
					}
				}).keydown(function(e) {
					  var val = $j(this).val();
					  if(e.keyCode==191 || e.keyCode==111){
						  return false;
					  }else if(e.keyCode!=8 && e.keyCode!=46 && e.keyCode!=191 && e.keyCode!=111){
						  if(val.length==2 || val.length==5){
							  $j(this).val(val+"/");
						  }
					  }else if(e.keyCode==8){
						if(val==""){
							return false;
						}
					  }
				}).focusout(function(){
					if($j(this).val()!=""){
						$j(".placeHolderOfid_"+$j(this).attr("id")).hide();
					}else{
						$j(".placeHolderOfid_"+$j(this).attr("id")).show();
					}
				});
			}
		  
		  function customRange(input) {
				var today = new Date();
				var d = today.getDate();
				var m = today.getMonth();
				var y = today.getFullYear();
				var h=today.getHours();
				var mn=today.getMinutes()+1;
				var mindate=$j("#specCollDate2").val();
				var mindate1=mindate.split("-");
				 if ($j("#datepicker4").val()!=='' && $j("#datepicker4").val()!=null) {
					  //  return {
					    	// minDate: new Date(mindate1[0],mindate1[1]-1,++mindate1[2])
					  		//$j('#datepicker91').datepicker( "option", "minDate", new Date(mindate1[0],mindate1[1]-1,mindate1[2]));
					  		var maxTestDate=new Date($j("#datepicker4").val());// processing start date
					        var d2 = maxTestDate.getDate();
					        var m2 = maxTestDate.getMonth();
					        var y2 = maxTestDate.getFullYear();
					        $j('#'+input.id).datepicker( "option", "minDate", new Date(y2,m2,d2));
					    //};
					  } else if($j("#datepicker4").val()==='' && $j("#datepicker2").val()!=='' && $j("#datepicker2").val()!=null){// processing start date is null but collection is available
						    var maxTestDate=new Date($j("#datepicker2").val());// collection date
							var d2 = maxTestDate.getDate();
							var m2 = maxTestDate.getMonth();
							var y2 = maxTestDate.getFullYear();
							$j('#'+input.id).datepicker( "option", "minDate", new Date(y2,m2,d2));
							$j('#'+input.id).datepicker( "option", "maxDate", new Date());
						}
			}
		  function checkCurrentDateWithCordDate(){
				 var today = new Date().toLocaleString();
				 var babyBrthDate=new Date($j("#specCollDate2").val()).toLocaleString();
				 var todayCompStr=today.split(" ");
				 var babyBrthDateCompStr=babyBrthDate.split(" ");
				 todayCompStr=todayCompStr[1]+todayCompStr[2]+todayCompStr[3];
				 babyBrthDateCompStr=babyBrthDateCompStr[1]+babyBrthDateCompStr[2]+babyBrthDateCompStr[3];
				 if(todayCompStr==babyBrthDateCompStr){alert("<s:text name='garuda.message.modal.babybirthdatecantbecurdate'/>");}
			 }

	function getDropDownByType(type,name,hideDivId){
			  var html = "";
	  $j.ajax({
	      url : "loadOptionType?codeType="+type+"&listName="+name+"&hideDivId="+hideDivId,
	   //  url : "loadOptionType?codeType="+type+"codeVia="+typeViability,
	      async : false,
	      error : function (request, status, error){
	      alert(request.responseText);
	      },
	      success :function(data) {
		 // alert("sucess"+data.htmlTest);
		//  alert("fkSpecimenId"+fkSpecimenId);
	   //   addcol(fkSpecimenId);
	      html =  data.htmlTest;
	      }
	  });
	return html;
}
</script>
					 <div id="dialogadd" style="display: none;" title="<s:text name="garuda.cordentry.label.addTestTime"/>">
						 <jsp:include page="cb_dialog_addtestdateinfo.jsp" flush="true">
	    				     <jsp:param value=""  name="fkSpeciId" />
			             </jsp:include>
					 </div>	    
<s:form id="labsumAddTest" name="labsumAddTest">
<div style="display: none;">
 <s:date name="specCollDate" id="specCollDate2" format="yyyy-MM-dd" />	
 <s:textfield readonly="true" onkeydown="cancelBack();"  name="specCollDate" id="specCollDate2"  value="%{specCollDate2}" ></s:textfield>											      
</div>         

<s:hidden name="showOtherColumn" id="showOtherColumn"/>
<s:hidden name="cdrCbuPojo.fkSpecimenId" id="fkSpecimenId"/>
<s:hidden name="removeUnknownTestFlag" value="#request.removeUnknownTestFlag"/>
<!--<s:property value="cdrCbuPojo.fkSpecimenId" /> ,<s:property value="pkLabtest" />,<s:property value="fktestid" />,<s:property value="pkpatlabs" /> ,-->

	<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				 <td colspan="2"><s:text name="garuda.common.lable.testDate" > </s:text><font  style="color: red;">*</font>
					<s:date name="otherPatlabsPojo.testdate" id="datepicker91" format="MMM dd, yyyy" />
					<s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn" id="datepicker91" onchange="changeProgress(1,'datepicker91')" value="%{datepicker91}" cssClass="datePicWMaxDate"></s:textfield>
		        </td>
	        </tr>
	      
	       <tr>
		       <td>
		           <input type="button" 
		           class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-focus" 
		           onclick="addcol('<s:property value="cdrCbuPojo.fkSpecimenId" />','<s:property value="#request.labGroupTypTests"/>');" value="<s:text name="garuda.cdrcbuview.label.button.addNewTest"/>"/>
		      </td>	
		       <td>
		           <input type="button" 
		           class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-focus" 
		           onclick="closeModal();" value="<s:text name="garuda.common.lable.cancel"/>"/>
		      </td>
		  </tr>
   </table>
</s:form>