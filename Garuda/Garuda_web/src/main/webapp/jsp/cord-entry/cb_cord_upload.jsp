<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<script>
var completeReq = false;
$j(function(){
	manageImportedCords();
});
function showImportFile(historyId){
	var url='exportCordImportFile?historyPojo.pkCordimportHistory='+historyId;
	window.open( url, "Attachment", "status = 1, height = 500, width = 800, resizable = 0" );
}

function getTheValuesFromServer(){
	while(!completeReq){
		var progress;
		var totalProcessedCords;
		$j.ajax({
	        type: "POST",
	        url: 'getCordImportData',
	        async:false,
	        success: function (result){
			progress = result.progress;
			totalProcessedCords = result.totalProcessedCords;
				if(progress != undefined && totalProcessedCords != undefined){
					var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.cbu.cordentry.progress"/> "+progress+" <s:text name="garuda.common.message.of"/> "+totalProcessedCords+" <s:text name="garuda.cbu.cordentry.records"/> <span id='prog'></span> </td></tr></table></td></tr></table>";
					
					$j("#modelPopup1").html(progressMsg);
						$j("#modelPopup1").dialog(
								   {autoOpen: false,
									title: title  ,
									resizable: false,
									closeText: '',
									closeOnEscape: false ,
									modal: true, width : width, height : height,
									close: function() {
										//$(".ui-dialog-content").html("");
										//jQuery("#subeditpop").attr("id","subeditpop_old");
						      		jQuery("#modelPopup1").dialog("destroy");
								    }
								   }
								  ); 
						$j("#modelPopup1").dialog("open");
				}
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});   
    }
}

function showFileUploadProgress(title,url,height,width)
{
	showprogressMgs();
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
/*var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'>Progress <span id='prog'></span> </td></tr></table></td></tr></table>";
	
	$j("#modelPopup1").html(progressMsg);
		$j("#modelPopup1").dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#modelPopup1").dialog("destroy");
				    }
				   }
				  ); 
		$j("#modelPopup1").dialog("open");*/
		var xhr = $j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#cimport").serialize(),
        success: function (result, status, error){
	        completeReq = true;
	       //	$j("#modelPopup1").html(result);
	       	$j("#cordImportFormatStatus").html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
		/*xhr.onreadystatechange = function() {
			  if (xhr.readyState == 3 && xhr.status == 200) {
				  alert("proc"+xhr.responseText);
			  }else if (xhr.readyState == 4 && xhr.status == 200) {
				  alert("danish comp"+xhr.responseText);
			  } else if (xhr.readyState >= 1 && xhr.status == 200) {
				  alert(xhr.responseText);
			  }
			};*/
			closeprogressMsg();
}

function submitUpload(){
	var timestamp = $j("#timestamp").val();
	
	if(timestamp=="" || timestamp==undefined){
        alert("Please select XML file for import!");
    	$j("#cordImportFormatStatus").html("");
	}else{
		var url = "cordUpload";
		showFileUploadProgress('Cord Import Progress',url,'100','250');
	}
	document.getElementById('frame1').contentWindow.document.getElementById('selFileName').style.display='none';
	$j("#timestamp").val("");
	$j("#selFileName").css("display","none");	
}
function fn_ResetCbuImportSort(){
	if($j('#cimport_tbl_sort_col').val()!=undefined && $j('#cimport_tbl_sort_col').val()!=null && $j('#cimport_tbl_sort_col').val()!=""){
			
			$j('#cimport_tbl_sort_col').val('');
			$j('#cimport_tbl_sort_type').val('');
			var oTable = $j('#searchResults').dataTable();
			oTable.fnSort( [ [0,'asc'] ] );
			oTable.fnDraw();
		}	
	}
</script>
<style  type="text/css">
#cimportTblTBody td{
	padding: 2px 10px !important;
    white-space: nowrap; 
    
}
</style>
<s:form name="Tableform" id="Tableform">
</s:form>

<s:hidden name="cimport_search_txt" id="cimport_search_txt"/>
<s:hidden name="cimport_tbl_sentries" id="cimport_tbl_sentries"/>
<s:hidden name="cimport_tbl_sort_col" id="cimport_tbl_sort_col"/>
<s:hidden name="cimport_tbl_sort_type" id="cimport_tbl_sort_type"/>
<div class="col_100 maincontainer" style="margin-top:10px">
    <div class="col_100">
       <s:form action="cordImportResult" id="cimport">
         <div class="column">
		        <div class="portlet" id="cbuimportparent" >
		        <s:if test="hasViewPermission(#request.ImprtXmlfle)==true">
					<div id="cbuimport" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
						<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
						<span class="ui-icon ui-icon-minusthick"></span>					
						<s:text name="garuda.cordimport.label.cbuimport" />
						</div>
						<div class="portlet-content">
						  <div id="cordImportFormatStatus">
						  </div>
						  <table cellpadding="0" cellspacing="0" width="100%">						       
						       <tr >
						           <td colspan="2">
						               <s:hidden name="timestamp" id="timestamp"/>
						               <iframe id="frame1" width="100%"  height="80px" frameborder="0" style="background: #F7F7F7;"
																scrolling="no" src="cb_cord_file_upload.jsp">
							           </iframe> 
						           </td>
						       </tr>  
						       <tr>
							      <td align="right" colspan="2">				
							         <button type="button" onclick="submitUpload();" ><s:text name="garuda.cordimport.label.previewimport"/></button>
							          
							      </td>
							      <!--<td align="left">
							         <button type="button" onclick="#" ><s:text name="garuda.common.lable.cancel"/></button>
							      </td>
							   --></tr>
						  </table>
						  </div>
			         </div>
			         </s:if>
			   </div>	 
		    </div>	
       </s:form>
       <div class="column">
       <s:if test="hasViewPermission(#request.cbuImprtHis)==true">
		        <div class="portlet" id="cbuimporthistoryparent" >
					<div id="cbuimporthistory" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
						<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
						<span class="ui-icon ui-icon-minusthick"></span>					
						<s:text name="garuda.cordimport.label.cbuimporthistory" />
						</div>
						<div class="portlet-content">
						<div align="right"><a href="#" onclick="fn_ResetCbuImportSort()"  id="resetSortLookup"><s:text name="garuda.pendingorderpage.label.resetsort"/></a></div><br>
						  <div id="searchTble1">
						    <jsp:include page="cb_set_pagination_cord_import.jsp"></jsp:include>
						  </div>
						</div>
			         </div>
			   </div>
		</s:if>	 
	  </div> 
     </div>
</div>        