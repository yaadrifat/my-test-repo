<%@ taglib prefix="s"  uri="/struts-tags"%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", 0); 
%>
<script>
jQuery.fn.forceNumericOnly =
	function()
	{
	    return this.each(function()
	    {
	        $j(this).keydown(function(e)
	        {
	            var key = e.charCode || e.keyCode || 0;
	            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
	            return (
	                key == 8 || 
	                key == 9 ||
	                key == 46 ||
	                (key >= 37 && key <= 40) ||
	                (key >= 48 && key <= 57) ||
	                (key >= 96 && key <= 105));
	        });
	    });
	};
$j(document).ready(function(){
	if($j("#esignFlag").val()=='sampleInventory'){
		var div=$j('#sampleInventoryDiv').html();
		var formContent='<form id="sampleInventoryForm">'+div+'</form>';
		$j('#sampleInventoryDiv').replaceWith(formContent);
	}
	if($j("#modelPopup1").is(":visible")){
		if($j("#modelPopup1").find("#esignFlag").val()==''){
			$j("#modelPopup1").find('#sampleInventoryDiv .cbuProcMandatory').each(function(){
			  $j(this).attr("disabled","disabled");	 
		});
	}
	}
	});
function updateSampleInventory(){
	//alert("Functionality Not Available");
	modalFormSubmitRefreshDiv('sampleInventoryDiv','updateSampleInventory','main');
}

function segAutoDefer(val,Id,className,element,ckVal){
	var autoDefer = false,flag=false;
	$j('#autoDifferFlag').val(autoDefer);
	//alert($j("#sampAuto").val());
	if($j("#sampAuto").val()=="cordEntryAuto"){
		if (val !='' && (val == 0 || val == 1)){
			if((typeof procesingInfoElementsVal !="undefined") && (typeof procesingInfoElements !="undefined")){
				var param = prepareParam(element,className,procesingInfoElements,procesingInfoElementsVal);
				AutoDeferChange.deferElement(param);
				flag=true;
			}
			jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
					function(r) {
						if (r == false) {
							if(flag==true){
								 $j("#"+Id).val(AutoDeferElementInitVal[$j(element).attr('name')]);							
							     AutoDeferChange.elementProgress();
							     AutoDeferChange.elementFocus();
							     AutoDeferChange.gc();
							}else{
						        $j("#"+Id).val("");
						        $j('#'+Id).focus();
							}
						  }else if(r == true){
							  var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
							  	$j('#autoDifferFlag').val(true);
							 	var value = field.GetHTML(true);
							  	$j('#sectionContentsta').val(value);
							    $j("#cordSearchable").val("1");	  
							    commonMethodForSaveAutoDefer();
							    //atuoDeferEsignId = Id;
							    ckTextField =ckVal;
							    ckTextFieldId =Id;						
						  }
						});
		}
	}
	
}
$j(function(){
	//For Numeric only
	$j(".positiveInteger").forceNumericOnly();
	
if($j("#ProcedureModalForm").is(":visible") && $j("#ProcedureModalForm").find("#esignFlag").val()!=''){
	$j("#ProcedureModalForm").find('#sampleInventoryDiv .cbuProcMandatory').each(function(){
		
	 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
		 $j(this).css("border", "1px solid red");
		
		}
	 else
		 {
		 $j(this).css("border", "1px solid #DBDAD9");
		 }
	  });
}
   		if($j("#cordentryform1").length<=0){
			$j(".asterickClass").each (function(){
            $j(this).css("display", "none");
  		   });
		 }
				
});
<%-- For Processing Information Widget Progress--%>
if((typeof procesingInfoElements !="undefined") && (typeof procesingInfoElementsVal !="undefined")&&
   (typeof proccount !="undefined")             && (typeof noOfMandatoryProcField !="undefined")  &&
   (typeof (window.addTotalCount) !="undefined")){
proccount=0;
noOfMandatoryProcField=0;
procesingInfoElements.length=0;
procesingInfoElementsVal.length=0;

$j(function(){
   $j(".procclass").each(function(){
	   noOfMandatoryProcField++;
	   noOfMandatoryField++;
  });

 var procfocus = "";   
 
   $j(".procclass").each(function(){
	   var val1 = $j(this).val();	 
	   procesingInfoElements[procesingInfoElements.length]= $j(this).attr("id");
	   procesingInfoElementsVal[procesingInfoElementsVal.length]=val1;
	   		   	 
	   if(val1!="" && val1!=-1)
	   {		      			    
			  proccount++;	
			  proccount=(proccount > noOfMandatoryProcField)?noOfMandatoryProcField:proccount;
			  val = parseInt((proccount*100)/(noOfMandatoryProcField));
			  $j("#processinginfobar").progressbar({
					value: val
				}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
			  addTotalCount();	
	   }					   	
   });
 
	
   $j(".procclass").bind({change:function() {
	    var val = $j(this).val();
		 var j=0;
		 var key = $j(this).attr("id");	
		 for(j=0; j<procesingInfoElements.length;j++){
			 if(procesingInfoElements[j]==key)
			     break;
		 }
		 procfocus=procesingInfoElementsVal[j];
		 procesingInfoElementsVal[j]=val;
		  if((procfocus=="" || procfocus==-1) && val!="" && val!=-1){
			 proccount++;
			  val = parseInt((proccount*100)/(noOfMandatoryProcField));
			  $j("#processinginfobar").progressbar({
					value: val
				}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
			  addTotalCount();	
		 }else if((procfocus!="" && procfocus!=-1) && (val=="" || val==-1)){
		      proccount--;
			  val = parseInt((proccount*100)/(noOfMandatoryProcField));
			  $j("#processinginfobar").progressbar({
					value: val
				}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
			  minusTotalCount();
		 }
	  }
	});
   //call method to refersh total count progress
	  totalcount--;
      addTotalCount();
   // Load Validation If All Widget Is Fully Loaded	   
      LoadedPage.processingProc=true;
      if(LoadedPage.idm && LoadedPage.fmhq && LoadedPage.mrq && LoadedPage.cbuHla && LoadedPage.clinicalNote && LoadedPage.labSummary){
    	  jQueryValidationInitializer();
      }
});
}
</script>
<div id="sampleInventoryDiv">
     <div class="column">
      <s:hidden name="esignFlag" id="esignFlag"></s:hidden>
      <s:hidden name="entitySamplesPojo.entityId"></s:hidden>
       <s:hidden name="entitySamplesPojo.entityType"></s:hidden>
       <input type="hidden" name="autoDifferFlag" id="autoDifferFlag"/>
       <input type="hidden" id="sampAuto" value="<s:property value="#request.sampAuto"/>">
      <s:hidden name="entitySamplesPojo.orderId" id="orderId"></s:hidden>
      <s:hidden name="entitySamplesPojo.orderType" id="orderType"></s:hidden>
      <input type="hidden" value="<s:property value="entitySamplesPojo.filtPap"/>" id="filtPap">
      <input type="hidden" value="<s:property value="entitySamplesPojo.extDnaAli"/>" id="extDnaAli">
      <input type="hidden" value="<s:property value="entitySamplesPojo.rbcPel"/>" id="rbcPel">
      <input type="hidden" value="<s:property value="entitySamplesPojo.noSerAli"/>" id="noSerAli">
      
      <input type="hidden" value="<s:property value="entitySamplesPojo.noPlasAli"/>" id="noPlasAli">
      <input type="hidden" value="<s:property value="entitySamplesPojo.nonViaAli"/>" id="nonViaAli">
      <input type="hidden" value="<s:property value="entitySamplesPojo.viaCelAli"/>" id="viaCelAli">
      <input type="hidden" value="<s:property value="entitySamplesPojo.noSegAvail"/>" id="noSegAvail">
      
      
      <input type="hidden" value="<s:property value="entitySamplesPojo.cbuOthRepConFin"/>" id="cbuOthRepConFin">
      <input type="hidden" value="<s:property value="entitySamplesPojo.cbuRepAltCon"/>" id="cbuRepAltCon">
      <input type="hidden" value="<s:property value="entitySamplesPojo.serMatAli"/>" id="serMatAli">
      <input type="hidden" value="<s:property value="entitySamplesPojo.plasMatAli"/>" id="plasMatAli">
       

      <input type="hidden" value="<s:property value="entitySamplesPojo.extDnaMat"/>" id="extDnaMat">
      <input type="hidden" value="<s:property value="entitySamplesPojo.noMiscMat"/>" id="noMiscMat">
 		 <div class="portlet" id="procandcountparent" >
			<div id="procandcount" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
				  <div id= "<s:property value="esignFlag" />cbusmplcontent" onclick="toggleDiv('<s:property value="esignFlag" />cbusmpl')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
				   <span class="ui-icon ui-icon-triangle-1-s"></span><s:text name="garuda.cbbprocedures.label.cbusample" />
				  </div>
				<div id ="<s:property value="esignFlag" />cbusmpl" class="portlet-content">
					<table width="100%" cellpadding="0" cellspacing="0" >																										 
							<tr>
								<td><s:text name="garuda.cordentry.label.filtPap"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>
								<td><s:textfield name="entitySamplesPojo.filtPap" id="cri_entitySamplesPojo_filtPap" cssClass="positiveInteger procclass cbuProcMandatory" onchange="isChangeDone(this,'%{entitySamplesPojo.filtPap}','procChanges');" maxlength="2"></s:textfield></td>										  
								<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.rbcPel"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>																						
								<td><s:textfield name="entitySamplesPojo.rbcPel" id="cri_entitySamplesPojo_rbcPel" cssClass="positiveInteger procclass cbuProcMandatory" onchange="isChangeDone(this,'%{entitySamplesPojo.rbcPel}','procChanges');" maxlength="2"></s:textfield></td>
							</tr>															
							<tr>
							   <td><s:text name="garuda.cordentry.label.extDnaAli"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>																								
							   <td><s:textfield name="entitySamplesPojo.extDnaAli" id="cri_entitySamplesPojo_extDnaAli" cssClass="positiveInteger procclass cbuProcMandatory" onchange="isChangeDone(this,'%{entitySamplesPojo.extDnaAli}','procChanges');" maxlength="2"></s:textfield></td>														
							   <td style="padding-left:10px;" onmouseover="return overlib('<s:text name='garuda.cbbprocedure.label.numberSerumToolTip'/>');" 
								   onmouseout="return nd();"><s:text name="garuda.cordentry.label.noSerAli"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>																								
							   <td><s:textfield name="entitySamplesPojo.noSerAli" id="cri_entitySamplesPojo_noSerAli" cssClass="positiveInteger procclass cbuProcMandatory" onchange="isChangeDone(this,'%{entitySamplesPojo.noSerAli}','procChanges');" maxlength="2"></s:textfield></td>
					       </tr>																											 
						   <tr>
							    <td><s:text name="garuda.cordentry.label.noPlasAli"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>																									   
							    <td><s:textfield name="entitySamplesPojo.noPlasAli" id="cri_entitySamplesPojo_noPlasAli" cssClass="positiveInteger procclass cbuProcMandatory" onchange="isChangeDone(this,'%{entitySamplesPojo.noPlasAli}','procChanges');" maxlength="2"></s:textfield></td>
								<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.nonViaAli"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>																									     
								<td><s:textfield name="entitySamplesPojo.nonViaAli" id="cri_entitySamplesPojo_nonViaAli" cssClass="positiveInteger procclass cbuProcMandatory" onchange="isChangeDone(this,'%{entitySamplesPojo.nonViaAli}','procChanges');" maxlength="2"></s:textfield></td>
						   </tr>
						    <tr>
								<td> <s:text name="garuda.cordentry.label.viaCelAli"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>																									     
								<td><s:textfield name="entitySamplesPojo.viaCelAli" id="cri_entitySamplesPojo_viaCelAli" cssClass="positiveInteger procclass cbuProcMandatory" onchange="isChangeDone(this,'%{entitySamplesPojo.viaCelAli}','procChanges');" maxlength="2"></s:textfield></td>
								<td></td>																									    
								<td></td>
						  </tr>																				  
					
					<tr>
					   <td>
						  <b><s:text name="garuda.cbbprocedures.label.aliqrepoffinalprod"></s:text></b>
					  </td>
				    </tr>	
						
							 <tr>
							    <td><s:text name="garuda.cordentry.label.segAvail"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>																									    
								<td><s:textfield name="entitySamplesPojo.noSegAvail" id="cri_entitySamplesPojo_noSegAvail" cssClass="positiveInteger procclass cbuProcMandatory autodeferclass" onchange="isChangeDone(this,'%{entitySamplesPojo.noSegAvail}','procChanges');" maxlength="2" onblur="segAutoDefer(this.value,this.id,'procclass',this,0);"></s:textfield></td>
								<td></td>																									     
								<td></td>
						  </tr>																			  
					      <tr>
								<td><s:text name="garuda.cordentry.label.cbuOthRepConFin"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>	
								<td><s:textfield name="entitySamplesPojo.cbuOthRepConFin" id="cri_entitySamplesPojo_cbuOthRepConFin" cssClass="positiveInteger procclass cbuProcMandatory" onchange="isChangeDone(this,'%{entitySamplesPojo.cbuOthRepConFin}','procChanges');" maxlength="2"></s:textfield></td>
								<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.cbuRepAltCon"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>																									    
								<td><s:textfield name="entitySamplesPojo.cbuRepAltCon" id="cri_entitySamplesPojo_cbuRepAltCon" cssClass="positiveInteger procclass cbuProcMandatory" onchange="isChangeDone(this,'%{entitySamplesPojo.cbuRepAltCon}','procChanges');" maxlength="2"></s:textfield></td>								
					      </tr>	
						</table>
					
				</div>
		     </div>  
		     		 <div id="procandcount" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
	    	 <div id= "<s:property value="esignFlag" />matsmplcontent" onclick="toggleDiv('<s:property value="esignFlag" />matsmpl')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
				<span class="ui-icon ui-icon-triangle-1-s"></span><s:text name="garuda.cbbprocedures.label.matsample" />
			  </div>
					<div id="<s:property value="esignFlag" />matsmpl" class="portlet-content">		  
					     <table width="100%" cellpadding="0" cellspacing="0" border="0">
			     			  <tr>
								    <td><s:text name="garuda.cordentry.label.serMatAli"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>
									<td><s:textfield name="entitySamplesPojo.serMatAli" id="cri_entitySamplesPojo_serMatAli" cssClass="positiveInteger procclass cbuProcMandatory" onchange="isChangeDone(this,'%{entitySamplesPojo.serMatAli}','procChanges');" maxlength="2"></s:textfield></td>
									<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.plasMatAli"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>	
									<td><s:textfield name="entitySamplesPojo.plasMatAli" id="cri_entitySamplesPojo_plasMatAli" cssClass="positiveInteger procclass cbuProcMandatory" onchange="isChangeDone(this,'%{entitySamplesPojo.plasMatAli}','procChanges');" maxlength="2"></s:textfield></td>
							  </tr>
			     	           <tr>
								    <td><s:text name="garuda.cordentry.label.extDnaMat"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>	
								    <td><s:textfield name="entitySamplesPojo.extDnaMat" id="cri_entitySamplesPojo_extDnaMat" cssClass="positiveInteger procclass cbuProcMandatory" onchange="isChangeDone(this,'%{entitySamplesPojo.extDnaMat}','procChanges');" maxlength="2"></s:textfield></td>
								    <td style="padding-left:10px;" onmouseover="return overlib('<s:text name='garuda.cbbprocedure.label.numberMisMatToolTip'/>');" 
										onmouseout="return nd();">
								    	<s:text name="garuda.cbbprocedures.label.noMiscMat"></s:text>:<span class="asterickClass" style="color: red;">*</span></td>	
									<td><s:textfield name="entitySamplesPojo.noMiscMat" id="cri_entitySamplesPojo_noMiscMat" cssClass="positiveInteger procclass cbuProcMandatory" onchange="isChangeDone(this,'%{entitySamplesPojo.noMiscMat}','procChanges');" maxlength="2"></s:textfield></td>
							  </tr>
					     
					     
						  </table>	
					</div>
					<s:if test="esignFlag!=null">
					  <s:if test="#request.showProc!=true">
						<table style="background-color: #cccccc;" id="esignTbl">
							<tr valign="baseline">
								<td width="100%">
									<jsp:include page="../cb_esignature.jsp" flush="true">
									<jsp:param value="submitSampleInv" name="submitId"/>
									<jsp:param value="invalidSampleInv" name="invalid" />
									<jsp:param value="minimumSampleInv" name="minimum" />
									<jsp:param value="passSampleInv" name="pass" />
									</jsp:include>
								</td>
								<td align="center" width="30%">
									<input type="button" onclick="updateSampleInventory();" value="<s:text name="garuda.unitreport.label.button.submit"/>" id="submitSampleInv" disabled="disabled"/>
								</td>
							</tr>
						</table>
					  </s:if>
					</s:if>
			</div></div>
		  </div>		
	</div>