<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.eres.web.site.SiteJB" %>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.address.AddressJB" %>

<script>
function saveOpenWind(pkpatlabs){
		var url='saveTestDateExtraInfo?pkpatlabs='+pkpatlabs;
		if($j("#refDiv").val()!=null && $j("#refDiv").val()!="" && $j("#fieldName").val()!=null && $j("#fieldName").val()!="")
		{
		//url += "&fieldName="+$j("#fieldName").val();
		//alert(url+"sdsd"+$j("#refDiv").val())
		modalFormSubmitRefreshDiv('saveTestDateExtraInfoForm',url,$j("#refDiv").val());
		}
	}
</script>
<div id="update" style="display: none;" >
		 <jsp:include page="../cb_update.jsp">
			    <jsp:param value="garuda.common.label.testMessage"  name="message" />
		 </jsp:include>	   
</div>	
<s:form id="saveTestDateExtraInfoForm">
		<s:hidden name="patLabsPojo.fkTimingOfTest"></s:hidden>
		<s:hidden name="patLabsPojo.testresult"/>
		<s:hidden name="patLabsPojo.fktestid"/>
		<s:hidden name="patLabsPojo.fkspecimen"/>
		<s:hidden name="patLabsPojo.otherListValue"/>
        <s:hidden name="refDiv" id="refDiv"></s:hidden>
        <s:hidden name="fieldName" id="fieldName"></s:hidden>
 <div id="saveTestDateDiv">
 <s:property value="pkpatlabs" />
	<table width="100%" cellpadding="0" cellspacing="0" border="0" id="status">
	    <tr>
	      <td><s:text name="garuda.common.label.reaForTest"></s:text></td>
	      <td><s:select name="patLabsPojo.fktestreason" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select></td>
	    
	      <td><s:text name="garuda.common.label.describe"></s:text></td>
	      <td><s:textfield  name="patLabsPojo.reasonTestDesc"></s:textfield></td>
	   </tr>

	   <tr>  
	      <td><s:text name="garuda.common.label.sampType"></s:text></td>
	      <td><s:select name="patLabsPojo.fktestspecimen" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SPEC_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select></td>
	      
	      <td><s:text name="garuda.common.label.testMthd"></s:text></td>
	      <td><s:select name="patLabsPojo.fktestmethod"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select></td>
	   </tr>
	     
	   <tr>
	      <td><s:text name="garuda.common.label.describe"></s:text></td>
	      <td><s:textfield  name="patLabsPojo.testMthdDesc"></s:textfield></td>
	   </tr>
	
	   <tr>  
	      <td><button type="button" id="submit" onclick="saveOpenWind('<s:property value="patLabsPojo.pkpatlabs" />');"><s:text name="garuda.cbbprocedures.labelsave"></s:text></button></td>
	   </tr>
    </table>
	 
	</div>
</s:form>

