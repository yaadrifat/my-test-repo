<%@ taglib prefix="s"  uri="/struts-tags"%>
		<s:hidden name="otherLabTstsLstId" value="%{#request.otherLabTstsLstId}" id="otherLabTstsLstId"></s:hidden>												
		<%-- <div
			class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
			style="text-align: center;">
			<span class="ui-icon ui-icon-minusthick"></span>
			<!--<span class="ui-icon ui-icon-close" ></span>-->
			<s:text name="garuda.cordentry.label.otherLabTests" />
		</div> --%>
		<div id="otherLabTestSrchTable" class="portlet-content" style="height:100%;">
			<table border="1" align="left" cellpadding="0"
				cellspacing="0" class="displaycdr" id="othertests">
					<%-- <thead>
						<tr>
							<th><s:text
								name="garuda.cbuentry.label.testdescription" />
							</th>
							<th><s:text
								name="garuda.cbuentry.label.testresult" /></th>
						</tr>
					</thead> --%>
					<tbody>
						<s:iterator value="patLabs" var="rowVal" status="row">
							<%-- <s:if test="#request.testId1!=fktestid"> --%>
							<tr id="<s:property	value="%{#row.index}"/>"
								onMouseOver="highlight('<s:property	value="%{#row.index}"/>')"
								onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')">
								<td><s:property value="testDescription" />
								</td>
								<%-- <s:set name="testId1" value="fktestid" scope="request"/>
								<s:iterator value="patLabs" var="rowVal" status="row">
								<s:if test="#request.testId1==fktestid"> --%>
								<%-- <s:if test="fkTimingOfTest==#request.valuePreFkTimingTest"> --%>
								<td>
									<table>
										<tr>
											<td width="50%"><s:text
																name="garuda.cbuentry.label.testresult" />:</td>
											<td width="50%"><s:property
																value="testresult" />
											</td>
										</tr>
										<tr>
											<td width="50%"><s:text
																name="garuda.cbuentry.label.sampletype" />:</td>
											<td width="50%"><s:property
																value="getCodeListDescById(fktestspecimen)" />
											</td>
										</tr>
										<tr>
											<td width="50%"><s:text
																name="garuda.cbuentry.label.timeofTest" />:</td>
											<s:set name="fkTimingOfTest" value="fkTimingOfTest" scope="request"/>
											<td width="50%">
											<s:if test="#request.fkTimingOfTest!=null && #request.fkTimingOfTest!=''">
											<s:property
																value="getCodeListDescById(fkTimingOfTest)" />
											</s:if>
											<s:else>
												<s:text name="garuda.common.lable.addTestOther" />
											</s:else>
											</td>
										</tr>
																					<%--  <tr>
																						<td width="50%"><s:text
																								name="garuda.cbuentry.label.testStartDate" />:</td>
																						<td width="50%"><s:date name="testdate"
																								id="datepickert" format="MMM dd, yyyy" /> <s:property
																								value="%{datepickert}" />
																						</td>
																					</tr> --%>
																					<%-- <tr>
																						<td align="right" colspan="2"><input
																							type="button"
																							value="<s:text name="garuda.common.lable.edit" />"
																							onclick="javascript:editTest('<s:property	value="%{#row.index}"/>','<s:property	value="pkpatlabs"/>');" />
																						</td>
																					</tr> --%>
											</table></td>
										<%-- </s:if> --%>
									<%-- </s:iterator> --%>
								</tr>
								<%-- </s:if> --%>
							</s:iterator>
						</tbody>
						<tfoot>
								<tr>
									<td colspan="2"></td>
								</tr>
						</tfoot>
					</table>
				</div>