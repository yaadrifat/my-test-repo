<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_track_session_logging.jsp" />
<%
String contextpath=request.getContextPath();
%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>	
<SCRIPT language="javascript">
var calcCbuNbrcFlag=true;

var autoDeferField1="";
var autoDeferField2="";
var autoDeferField3="";
var autoDeferField4="";
var autoDeferField5="";

/* function showTest(fkSpeciId){
	var id="datepickerbirthlabModal";
	var msg="<s:text name='garuda.message.modal.babybirthdatecantbecurdate'/>";
	var compCurNCordDate=checkCurrentDateCordDate(id,msg);
	if(compCurNCordDate){
	document.getElementById('fkSpecimenId').value=fkSpeciId;
	//document.getElementById('fktestid').value=fktestid[2];
    // alert("hiiii :"+fkSpeciId);
    //  alert("hello >>>>>>>>>>>>>>>>>>>: "+fktestid[2]);
        $j("#dialog").dialog();
        //$jmyWindow.show();
       // $jmyWindow.dialog("open");
       	$jmyWindow=jQuery("#dialog");
         $jmyWindow.show();
        // $j("#dialog").css('display','none');
         //$jmyWindow.hide();

        // $j('#dialog').dialog('close');
	  }} */
function onChangeProcnCountFldVald(fieldId,fieldValue){

	var txtFldClsValueMand="onChngMand";
	var dbStoredFieldVal=$j("#"+fieldId+"_dbStoredVal").val();
	if((fieldValue==dbStoredFieldVal) ||(fieldValue!="" && fieldValue!=-1)){
		$j("#"+fieldId+"_error").hide();
		$j("#"+fieldId).removeClass(txtFldClsValueMand);
	}
	else{
		$j("#"+fieldId+"_error").show();
		$j("#"+fieldId).addClass(txtFldClsValueMand);
		$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
	}
}

function checkRangeMthd(fieldId,fkTestId,fieldVal,prePostOthFlag){
	var preFlag="pre";
	var postFlag="post";
	var othThawFlag="otherThaw";
	var txtFldClsValueMand="onChngMand";
	var txtFldClsValRangChk="onChngRangeChk";
	var txtFldCls="";
	var chkMinVal=null;
	var chkMaxVal=null;
	var dispMsg1="";
	var dbStoredFieldVal=$j("#"+fieldId+"_dbStoredVal").val();
	var cbuAbsFldId="";
	var cbuPrcntFldId="";
	var tbncCntFldId="";
	var calcValucNbrcAbs="";
	if(prePostOthFlag==postFlag){
		cbuAbsFldId="savePostTestList10testresult";
		cbuPrcntFldId="savePostTestList11testresult";
		tbncCntFldId="savePostTestList1testresult";
	}
	var CBU_volume_test = document.getElementById('CBU_VOL').value;
	var Ttl_CBU_Ncltd_cnt = document.getElementById('TCNCC').value;
	var Ttl_CBU_Ncltd_cnt_uncrtd = document.getElementById('UNCRCT').value;
	var Ttl_CBU_Ncltd_cnt_unkif_uncrtd = document.getElementById('TCBUUN').value;
	if(fkTestId==Ttl_CBU_Ncltd_cnt || fkTestId==Ttl_CBU_Ncltd_cnt_uncrtd || fkTestId==Ttl_CBU_Ncltd_cnt_unkif_uncrtd){
		chkMinVal=12;
		chkMaxVal=999;
		dispMsg1="<s:text name="garuda.common.range.testval12to999"/>";
	}
	var CBU_Ncltd_Cell_Cncton = document.getElementById('CBUNCCC').value;
	if(fkTestId==CBU_Ncltd_Cell_Cncton){
		chkMinVal=5000;
		chkMaxVal=200000;
		dispMsg1="<s:text name="garuda.common.range.testval5000to200000"/>";
	}
	if(fkTestId==CBU_Ncltd_Cell_Cncton && prePostOthFlag==preFlag){
		chkMinVal=1000;
		chkMaxVal=50000;
		dispMsg1="<s:text name="garuda.common.range.testval1000to50000"/>";
	}
	var Final_Pdct_Vol = document.getElementById('FNPV').value;
	if(fkTestId==Final_Pdct_Vol){
		chkMinVal=10;
		chkMaxVal=600;
		dispMsg1="<s:text name="garuda.common.range.testval10to600"/>";
	}
	var Ttl_CD34_Cel_Cnt = document.getElementById('TCDAD').value;
	if(fkTestId==Ttl_CD34_Cel_Cnt){
		chkMinVal=0.0;
		chkMaxVal=999.9;
		dispMsg1="<s:text name="garuda.common.range.val0to999"/>";
	}
	var prcnt_of_CD34_Vab = document.getElementById('PERCD').value;
	if(fkTestId==prcnt_of_CD34_Vab){
		chkMinVal=0.0;
		chkMaxVal=100;
		dispMsg1="<s:text name="garuda.common.range.val0to100"/>";
	}
	var prcnt_of_CD34_ttl_Monclr_Cl_Cnt = document.getElementById('PERTMON').value;
	if(fkTestId==prcnt_of_CD34_ttl_Monclr_Cl_Cnt){
		chkMinVal=0.0;
		chkMaxVal=10.0;
		dispMsg1="<s:text name="garuda.common.range.testrange0to10"/>";
	}
	var prcnt_of_Monclr_Cls_Ttl_Ncltd_Cnt = document.getElementById('PERTNUC').value;
	if(fkTestId==prcnt_of_Monclr_Cls_Ttl_Ncltd_Cnt){
		chkMinVal=20.0;
		chkMaxVal=99.9;
		dispMsg1="<s:text name="garuda.common.range.val20to99"/>";
	}
	var Ttl_CD3_Cel_Cnt = document.getElementById('TOTCD').value;
	if(fkTestId==Ttl_CD3_Cel_Cnt){
		chkMinVal=0;
		chkMaxVal=999.9;
		dispMsg1="<s:text name="garuda.common.range.val0to999"/>";
	}
	var prcnt_CD3_Cls_Ttl_Monclr_Cl_Cnt = document.getElementById('PERCD3').value;
	if(fkTestId==prcnt_CD3_Cls_Ttl_Monclr_Cl_Cnt){
		chkMinVal=0;
		chkMaxVal=99.9;
		dispMsg1="<s:text name="garuda.common.range.val0to99"/>";
	}
	var CBU_nRBC_Abslt_No = document.getElementById('CNRBC').value;
	if(fkTestId==CBU_nRBC_Abslt_No){
		chkMinVal=0;
		chkMaxVal=9999;
		dispMsg1="<s:text name="garuda.common.range.val0to9999"/>";
	}
	var CBU_nRBC_prcnt = document.getElementById('PERNRBC').value;
	if(fkTestId==CBU_nRBC_prcnt){
		chkMinVal=0;
		chkMaxVal=100;
		dispMsg1="<s:text name="garuda.common.range.val0to100"/>";
	}
	var CFU_Cnt_test = document.getElementById('CFUCNT').value;
	if(fkTestId==CFU_Cnt_test){
		chkMinVal=0;
		chkMaxVal=999;
		dispMsg1="<s:text name="garuda.common.range.testrange0to999"/>";
	}
	var Viability_test = document.getElementById('VIAB').value;
	if(fkTestId==Viability_test){
		chkMinVal=0;
		chkMaxVal=100;
		dispMsg1="<s:text name="garuda.common.range.val0to100"/>";
	}
	/* var NAT_HIV_test = document.getElementById('NATHIV').value;
	var NAT_HCV_test = document.getElementById('NATHCV').value;
	if(fkTestId==NAT_HIV_test || fkTestId==NAT_HCV_test){
		chkMinVal=0;
		chkMaxVal=0;
		dispMsg1="";
	} */
	switch(fkTestId){
	case CBU_volume_test: 
			if((fieldVal>=40 && fieldVal<=500) || (fieldVal==dbStoredFieldVal)){
				$j("#"+fieldId+"_error").hide();
				$j("#"+fieldId).removeClass(txtFldClsValueMand);
				$j("#"+fieldId).removeClass(txtFldClsValRangChk);
			}
			else{
				$j("#"+fieldId+"_error").show();
				if(prePostOthFlag==preFlag)
					{
						if(fieldVal==null || fieldVal==''){
							txtFldCls=txtFldClsValueMand;
							$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
						}
						else{
							txtFldCls=txtFldClsValRangChk;
							$j("#"+fieldId+"_error").text("<s:text name="garuda.common.range.testval40to500"/>");
						}
				}
				else if(prePostOthFlag==postFlag){
					if(fieldVal==null || fieldVal==''){
						$j("#"+fieldId).removeClass(txtFldClsValueMand);
						$j("#"+fieldId).removeClass(txtFldClsValRangChk);
					}
					else{
						txtFldCls=txtFldClsValRangChk;
						$j("#"+fieldId+"_error").text("<s:text name="garuda.common.range.testval40to500"/>");
					}
				}
				else if(prePostOthFlag==othThawFlag){
					if(fieldVal==null || fieldVal==''){
						$j("#"+fieldId).removeClass(txtFldClsValueMand);
						$j("#"+fieldId).removeClass(txtFldClsValRangChk);
					}
					else{
						txtFldCls=txtFldClsValRangChk;
						$j("#"+fieldId+"_error").text("<s:text name="garuda.common.range.testval40to500"/>");
					}
				}
				$j("#"+fieldId).addClass(txtFldCls);
			}
			break;
	case Ttl_CBU_Ncltd_cnt:
	case Ttl_CBU_Ncltd_cnt_uncrtd: 
	case Ttl_CBU_Ncltd_cnt_unkif_uncrtd:
		if((fieldVal!="" && fieldVal>=chkMinVal && fieldVal<=chkMaxVal) || (fieldVal==dbStoredFieldVal)){
			$j("#"+fieldId+"_error").hide();
			$j("#"+fieldId).removeClass(txtFldClsValueMand);
			$j("#"+fieldId).removeClass(txtFldClsValRangChk);
		}
		else{
			$j("#"+fieldId+"_error").show();
			if(prePostOthFlag==preFlag)
				{
					if(fieldVal==null || fieldVal==''){
						$j("#"+fieldId+"_error").hide();
						$j("#"+fieldId).removeClass(txtFldClsValueMand);
						$j("#"+fieldId).removeClass(txtFldClsValRangChk);
					}
					else{
						txtFldCls=txtFldClsValRangChk;
						$j("#"+fieldId+"_error").text(dispMsg1);
					}
			}
			else if(prePostOthFlag==postFlag){
				if(fieldVal==null || fieldVal==''){
					txtFldCls=txtFldClsValueMand;
					$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
				}
				else{
					txtFldCls=txtFldClsValRangChk;
					$j("#"+fieldId+"_error").text(dispMsg1);
				}
				
			}
			else if(prePostOthFlag==othThawFlag){
				if(fieldVal==null || fieldVal==''){
					$j("#"+fieldId+"_error").hide();
					$j("#"+fieldId).removeClass(txtFldClsValueMand);
					$j("#"+fieldId).removeClass(txtFldClsValRangChk);
				}
				else{
					txtFldCls=txtFldClsValRangChk;
					$j("#"+fieldId+"_error").text(dispMsg1);
				}
			}
			$j("#"+fieldId).addClass(txtFldCls);
		}
		break;
	case CBU_Ncltd_Cell_Cncton:
	case Final_Pdct_Vol:
	case prcnt_of_CD34_Vab: 
	case prcnt_of_CD34_ttl_Monclr_Cl_Cnt:
	case prcnt_of_Monclr_Cls_Ttl_Ncltd_Cnt:
	case Ttl_CD3_Cel_Cnt: 
	case prcnt_CD3_Cls_Ttl_Monclr_Cl_Cnt:
	case Ttl_CD34_Cel_Cnt: 
	case CBU_nRBC_Abslt_No: 
	case CBU_nRBC_prcnt: 
	case CFU_Cnt_test: 
	case Viability_test: 
		if((fieldVal>=chkMinVal && fieldVal<=chkMaxVal) || (fieldVal==dbStoredFieldVal)){
			$j("#"+fieldId+"_error").hide();
			$j("#"+fieldId).removeClass(txtFldClsValueMand);
			$j("#"+fieldId).removeClass(txtFldClsValRangChk);
		}
		else{
			$j("#"+fieldId+"_error").show();
				if(fieldVal==null || fieldVal==''){
					$j("#"+fieldId+"_error").hide();
					$j("#"+fieldId).removeClass(txtFldClsValueMand);
					$j("#"+fieldId).removeClass(txtFldClsValRangChk);
				}
				else{
					txtFldCls=txtFldClsValRangChk;
					$j("#"+fieldId+"_error").text(dispMsg1);
				}
				$j("#"+fieldId).addClass(txtFldCls);
		}
		break;
	/* case NAT_HIV_test: 
	case NAT_HCV_test: 
		$j("#"+fieldId+"_error").hide();
		break; */
	default:
		$j("#"+fieldId+"_error").hide();
		$j("#"+fieldId).removeClass(txtFldClsValueMand);
		$j("#"+fieldId).removeClass(txtFldClsValRangChk);
	}	
	if(prePostOthFlag==postFlag && calcCbuNbrcFlag==true){
	if( fkTestId==Ttl_CBU_Ncltd_cnt || fkTestId==Ttl_CBU_Ncltd_cnt_uncrtd || fkTestId==Ttl_CBU_Ncltd_cnt_unkif_uncrtd || fkTestId==CBU_nRBC_prcnt || fkTestId==CBU_nRBC_Abslt_No){
		if(fkTestId==Ttl_CBU_Ncltd_cnt || fkTestId==Ttl_CBU_Ncltd_cnt_uncrtd || fkTestId==Ttl_CBU_Ncltd_cnt_unkif_uncrtd ){
			if(fieldVal==null || fieldVal==''){
					$j("#"+cbuAbsFldId).val("");
					$j("#"+cbuPrcntFldId).val("");
			}
			else{
				if(($j("#"+cbuAbsFldId).val()!=null && $j("#"+cbuAbsFldId).val()!='') && ($j("#"+cbuPrcntFldId).val()==null || $j("#"+cbuPrcntFldId).val()=='')){
					calcValucNbrcAbs=($j("#"+cbuAbsFldId).val()/($j("#"+tbncCntFldId).val()*10))*100;
					$j("#"+cbuPrcntFldId).val(Math.round(calcValucNbrcAbs));
				}
				else if(($j("#"+cbuAbsFldId).val()==null && $j("#"+cbuAbsFldId).val()=='') && ($j("#"+cbuPrcntFldId).val()!=null || $j("#"+cbuPrcntFldId).val()!='')){
					calcValucNbrcAbs=($j("#"+cbuPrcntFldId).val()/100)*($j("#"+tbncCntFldId).val()*10);
					$j("#"+cbuAbsFldId).val(calcValucNbrcAbs);
				}
				else if(($j("#"+cbuAbsFldId).val()!=null && $j("#"+cbuAbsFldId).val()!='') && ($j("#"+cbuPrcntFldId).val()!=null || $j("#"+cbuPrcntFldId).val()!='')){
					$j("#"+cbuAbsFldId).val("");
					$j("#"+cbuPrcntFldId).val("");
				}
			}
		}
		else if(fkTestId==CBU_nRBC_prcnt){
				if(fieldVal==null || fieldVal==''){
					$j("#"+cbuAbsFldId).val("");
				}
				else if($j("#"+tbncCntFldId).val()!=null && $j("#"+tbncCntFldId).val()!=''){
					calcValucNbrcAbs=($j("#"+cbuPrcntFldId).val()/100)*($j("#"+tbncCntFldId).val()*10);
					if(calcValucNbrcAbs<0 || calcValucNbrcAbs>9999){
						$j("#"+cbuAbsFldId).val("");
						$j("#"+cbuPrcntFldId).val("");
						$j("#"+fieldId+"_error").show();
						$j("#"+fieldId+"_error").text("Please enter valid value");
					}
					else{
						$j("#"+cbuAbsFldId+"_error").hide();
						$j("#"+cbuAbsFldId).val(calcValucNbrcAbs);
					}
				}
				else if($j("#"+tbncCntFldId).val()==null || $j("#"+tbncCntFldId).val()==''){
					$j("#"+cbuPrcntFldId).val("");
					alert("Please Enter value for Total CBU Nucleated Cell Count");
				}
		}
		else if(fkTestId==CBU_nRBC_Abslt_No){
			if(fieldVal==null || fieldVal==''){
				$j("#"+cbuPrcntFldId).val("");
			}
			else if($j("#"+tbncCntFldId).val()!=null && $j("#"+tbncCntFldId).val()!=''){
				calcValucNbrcAbs=($j("#"+cbuAbsFldId).val()/($j("#"+tbncCntFldId).val()*10))*100;
				if(Math.round(calcValucNbrcAbs)<0 || Math.round(calcValucNbrcAbs)>100){
					$j("#"+cbuAbsFldId).val("");
					$j("#"+cbuPrcntFldId).val("");
					$j("#"+fieldId+"_error").show();
					$j("#"+fieldId+"_error").text("Please enter valid value");
				}
				else{
					$j("#"+cbuPrcntFldId+"_error").hide();
				$j("#"+cbuPrcntFldId).val(Math.round(calcValucNbrcAbs));
				}
			}
			else if($j("#"+tbncCntFldId).val()==null || $j("#"+tbncCntFldId).val()==''){
				$j("#"+cbuAbsFldId).val("");
				alert("Please Enter value for Total CBU Nucleated Cell Count");
			}
	}
	}}
}	  

function validateThawOtherRow(formId){
	var flag=true;
	var rangeFlag=true;
	var headerLnght = $j("#added1 thead th").length;
	var thawRow=headerLnght-3;
	var thawOtherRow=1;
	for(thawRow=3;thawRow<headerLnght;thawRow++){
		var n=0;
		$j("#added1 tr:gt(0)").each(function(){
			if(n>=$j("#noOfLabGrpTypTests").val()){
				var fieldId = 'savePreTestList'+n+'testresult';
				if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
				var tempVarbl=document.getElementById(fieldId).value;
				if(tempVarbl != null && tempVarbl !=''){
					$j("#"+fieldId+"_error").hide();
					var fieldfktestspecimenId = 'savePreTestList'+n+'fktestspecimen';
					var tempfktestspecimenVarbl=document.getElementById(fieldfktestspecimenId).value;
					if(tempfktestspecimenVarbl != null && tempfktestspecimenVarbl >0){
						$j("#"+fieldfktestspecimenId+"_error").hide();
					}
					else{
						$j("#"+fieldfktestspecimenId+"_error").show();
						$j("#"+fieldfktestspecimenId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
						flag=false;
					}
				}
				else{
					$j("#"+fieldId+"_error").show();
					$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
					flag=false;
				}
			}
				fieldId = 'savePostTestList'+n+'testresult';
				if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
				var tempVarbl=document.getElementById(fieldId).value;
				if(tempVarbl != null && tempVarbl !=''){
					$j("#"+fieldId+"_error").hide();
					var fieldfktestspecimenId = 'savePostTestList'+n+'fktestspecimen';
					var tempfktestspecimenVarbl=document.getElementById(fieldfktestspecimenId).value;
					if(tempfktestspecimenVarbl != null && tempfktestspecimenVarbl >0){
						$j("#"+fieldfktestspecimenId+"_error").hide();
					}
					else{
						$j("#"+fieldfktestspecimenId+"_error").show();
						$j("#"+fieldfktestspecimenId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
						flag=false;
					}
				}
				else{
					$j("#"+fieldId+"_error").show();
					$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
					flag=false;
				}
			}
				fieldId = 'saveOtherPostTestList'+thawOtherRow+n+'testresult';
				if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
				var tempVarbl=document.getElementById(fieldId).value;
				if(tempVarbl != null && tempVarbl !=''){
					$j("#"+fieldId+"_error").hide();
					var fieldfktestspecimenId = 'saveOtherPostTestList'+thawOtherRow+n+'fktestspecimen';
					var tempfktestspecimenVarbl=document.getElementById(fieldfktestspecimenId).value;
					if(tempfktestspecimenVarbl != null && tempfktestspecimenVarbl >0){
						$j("#"+fieldfktestspecimenId+"_error").hide();
					}
					else{
						$j("#"+fieldfktestspecimenId+"_error").show();
						$j("#"+fieldfktestspecimenId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
						flag=false;
					}
				}
				else{
					$j("#"+fieldId+"_error").show();
					$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
					flag=false;
				}
			}
				n++;
			}
			else{
			var fieldId = 'saveOtherPostTestList'+thawOtherRow+n+'testresult';
			if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
			var tempVarbl=document.getElementById(fieldId).value;
			if(tempVarbl != null && tempVarbl !=''){
				//var fieldName='saveOtherPostTestList'+thawOtherRow+'['+n+']'+'.fktestid';
				//var fkTestId=document.getElementsByName(fieldName);
				//rangeFlag = checkRangeMthd(fieldId,fkTestId,tempVarbl);
				var fieldfktestreasonId = 'saveOtherPostTestList'+thawOtherRow+n+'fktestreason';
				var tempfktestreasonVarbl=document.getElementById(fieldfktestreasonId).value;
				if(tempfktestreasonVarbl != null && tempfktestreasonVarbl >0){
					var tempvalueTestReasonOtherPkVal=document.getElementById('valueTestReasonOtherPkVal').value;
					if(tempfktestreasonVarbl==tempvalueTestReasonOtherPkVal){
						var fieldreasonTestDescId = 'saveOtherPostTestList'+thawOtherRow+n+'reasonTestDesc';
						var tempreasonTestDescVarbl=document.getElementById(fieldreasonTestDescId).value;
						if(tempreasonTestDescVarbl != null && tempreasonTestDescVarbl != ''){
							$j("#"+fieldreasonTestDescId+"_error").hide();
						}
						else{
							$j("#"+fieldreasonTestDescId+"_error").show();
							flag=false;
						}
					}
					$j("#"+fieldfktestreasonId+"_error").hide();
				}
				else{
					$j("#"+fieldfktestreasonId+"_error").show();
					flag=false;
				}
				var fieldfktestspecimenId = 'saveOtherPostTestList'+thawOtherRow+n+'fktestspecimen';
				var tempfktestspecimenVarbl=document.getElementById(fieldfktestspecimenId).value;
				if(tempfktestspecimenVarbl != null && tempfktestspecimenVarbl >0){
					$j("#"+fieldfktestspecimenId+"_error").hide();
				}
				else{
					$j("#"+fieldfktestspecimenId+"_error").show();
					flag=false;
				}
				var fieldfktestmethodId = 'saveOtherPostTestList'+thawOtherRow+n+'fktestmethod';
				if(document.getElementById(fieldfktestmethodId)!=null && document.getElementById(fieldfktestmethodId)!='undefined'){
				var tempfktestmethodVarbl=document.getElementById(fieldfktestmethodId).value;
				if(tempfktestmethodVarbl != null && tempfktestmethodVarbl >0){
					var tempValueCfuOtherPkVal=document.getElementById('valueCfuOtherPkVal').value;
					var tempValueViablityOtherPkVal=document.getElementById('valueViablityOtherPkVal').value;
					if(tempfktestmethodVarbl==tempValueCfuOtherPkVal || tempfktestmethodVarbl==tempValueViablityOtherPkVal){
						var fieldtestMthdDescId = 'saveOtherPostTestList'+thawOtherRow+n+'testMthdDesc';
						var temptestMthdDescVarbl=document.getElementById(fieldtestMthdDescId).value;
						if(temptestMthdDescVarbl != null && temptestMthdDescVarbl != ''){
							$j("#"+fieldtestMthdDescId+"_error").hide();
						}
						else{
							$j("#"+fieldtestMthdDescId+"_error").show();
							flag=false;
						}
					}
					$j("#"+fieldfktestmethodId+"_error").hide();
				}
				else{
					$j("#"+fieldfktestmethodId+"_error").show();
					flag=false;
				}
				}
			}
			}
			n++;
			}
		});
		thawOtherRow++;
	}
	return flag;
}
/* function validateCfuAndViab(){
	var flag=true;
	if($j("#savePostTestList12testresult").val() != $j("#savePostTestList12testresult_dbStoredVal").val() && $j("#savePostTestList12testresult").val()=='' && $j("#cfuSelected").val()!=-1){
		$j("#savePostTestList12testresult_error").show();
		flag=false;
	}
} */
var autoFilled= false;
var autoFillee= false;
function showAssment(Id,buttonTd,divId,mDivId,hideButtonTd){
	var assesmentCheckFlag="";
	
	var responsestr=$j('#'+Id+' option:selected').text();

	var val = $j('#'+Id+' option:selected').val();

	var assesTest1=$j("#alphaThalismiaTraitPkVal").val();
	
	var assesTest2=$j("#hemozygousNoDiesasePkVal").val();
	
	var assesTest3=$j("#hemoAlphaThalassmiaPkVal").val();
	
	var assesTest4=$j("#hemoTraitPkVal").val();

	var assesTest5=$j("#hemozygousNoDonePkVal").val();

	var assesTest6=$j("#hemozygousUnknownPkVal").val();
	
	var assesTest7=$j("#hemoMultiTraitPkVal").val();
	
	var fungLabAssesPoes=$j("#fungalCulpostive").val();
	
	var fungLabAssesPoes=$j("#fungalCulpostive").val();

	var fungLabAssesPoes1=$j("#fungalCulnotdone").val();

	var data=null;
	if(val==fungLabAssesPoes1){
		if(val==$j("#fungalResultModal_dbStoredVal").val()){
			assesmentCheckFlag="true";
		}
		else{
			assesmentCheckFlag="false";
		}
		data='entityId=<s:property value="cdrCbuPojo.cordID"/>&subEntityId='+val+'&shrtName=LAB_SUM_Fung&subentitytype='+$j("#subentityType").val()+'&questionNo=FungalCulture&responsestr='+responsestr+'&assessDivName='+mDivId+'&responseval='+val;
		
	}
	else if(val==assesTest1 || val==assesTest2 || val==assesTest3 || val==assesTest4 || val==assesTest5 || val==assesTest6 || val==assesTest7){
		if(val==$j("#hemoglobinScrnTest_dbStoredVal").val()){
			assesmentCheckFlag="true";
		}
		else{
			assesmentCheckFlag="false";
		}
		data='entityId=<s:property value="cdrCbuPojo.cordID"/>&subEntityId='+val+'&shrtName=LAB_SUM_Hemo&subentitytype='+$j("#subentityType").val()+'&questionNo=Hemoglobinopathy Testing&responsestr='+responsestr+'&assessDivName='+mDivId+'&responseval='+val;
	}

	if(val!=null && val!= ""){
		$j('#'+mDivId).show();
		$j('#'+hideButtonTd).show();
		$j('#'+buttonTd).hide();
		if($j('#'+divId).html() != null && $j('#'+divId).html() != "" && $j('#'+divId).html() != 'undefined'){
			//$j('#'+divId).show();
		}else{
		refreshassessment1('getassessmentHemo?assesmentCheckFlag='+assesmentCheckFlag,divId,data);
		}
	}else{
		$j('#'+mDivId).hide();
		$j('#'+buttonTd).hide();
	}
}

function showTest(fkSpeciId){
	var id="datepickerColllabModalCri";
	var msg="<s:text name='garuda.message.modal.colldatecantbecurdate'/>";
	/* var compCurNCordDate=checkCurrentDateCordDate(id,msg);
	if(compCurNCordDate){ */
	document.getElementById('fkSpecimenId').value=fkSpeciId;
	//document.getElementById('fktestid').value=fktestid[2];
    // alert("hiiii :"+fkSpeciId);
    //  alert("hello >>>>>>>>>>>>>>>>>>>: "+fktestid[2]);
       // $j("#dialog").dialog({closeText: ''});
        //$jmyWindow.show();
       // $jmyWindow.dialog("open");
      // 	$jmyWindow=jQuery("#dialog");
       //  $jmyWindow.show();
         collectionDateId = $j("#collectionDateId").val();
       	 showModals("Add Test Date","addLabTest?fkSpecimenId="+fkSpeciId+"&specCollDate="+collectionDateId+"&removeUnknownTestFlag=true","200","300","dialog");
         $j('.removeFocus').datepicker( "hide" );
         $j('#addtestButn').focus();
        // $j("#dialog").css('display','none');
         //$jmyWindow.hide();

        // $j('#dialog').dialog('close');
	 /*  } */
}
function closeModalsTemp(divname){
	$j("#"+divname).dialog("close");
	 $j("#"+divname).dialog("destroy");
}
	  
function hideAssment(mDivId,shwbuttonTd,hideButtonTd){
	$j('#'+mDivId).hide();
	$j('#'+shwbuttonTd).show();
	$j('#'+hideButtonTd).hide();
}

function checkassessment1(tname,masterDiv,buttonId,hideButtonTd,id,val){

	var divid=tname;

	var mDiv=masterDiv;
	var assesmentCheckFlag="";
	var assesTest1=$j("#alphaThalismiaTraitPkVal").val();
	
	var assesTest2=$j("#hemozygousNoDiesasePkVal").val();
	
	var assesTest3=$j("#hemoAlphaThalassmiaPkVal").val();
	
	var assesTest4=$j("#hemoTraitPkVal").val();

	var assesTest5=$j("#hemozygousNoDonePkVal").val();

	var assesTest6=$j("#hemozygousUnknownPkVal").val();
	
	var assesTest7=$j("#hemoMultiTraitPkVal").val();
	
	var fungLabAssesPoes=$j("#fungalCulpostive").val();

	var fungLabAssesPoes1=$j("#fungalCulnotdone").val();
	
	//alert(data);
	//alert("tname : "+tname);
	var responsestr=$j('#'+id+' option:selected').text();
	
	//var data='entityId='+$j("#cordID").val()+'&subEntityId='+$j("#cordID").val()+'&shrtName=LAB_SUM&subentitytype='+$j("#subentityType").val()+'&&questionNo=Hemoglobinopathy&responsestr='+responsestr;
	$j('#'+hideButtonTd).hide();
	if(val!=null && val!= "" && (val==assesTest1 || val==assesTest2 || val==assesTest3 || val==assesTest4 || val==assesTest5 || val==assesTest6 || val==assesTest7)){
		if(val==$j("#hemoglobinScrnTest_dbStoredVal").val()){
			assesmentCheckFlag="true";
		}
		else{
			assesmentCheckFlag="false";
		}
		var data='entityId=<s:property value="cdrCbuPojo.cordID"/>&subEntityId='+val+'&shrtName=LAB_SUM_Hemo&subentitytype='+$j("#subentityType").val()+'&questionNo=Hemoglobinopathy Testing&responsestr='+responsestr+'&assessDivName='+mDiv+'&responseval='+val;	
		$j('#'+mDiv).show();
		$j('#'+buttonId).hide();
		$j('#'+hideButtonTd).show();
		refreshassessment1('getassessmentHemo?assesmentCheckFlag='+assesmentCheckFlag,divid,data);
		$j("#"+id).removeClass('addAssessmentMandatory');
	}
	else if(val==fungLabAssesPoes1){
		if(val==$j("#fungalResultModal_dbStoredVal").val()){
			assesmentCheckFlag="true";
		}
		else{
			assesmentCheckFlag="false";
		}
		var data='entityId=<s:property value="cdrCbuPojo.cordID"/>&subEntityId='+val+'&shrtName=LAB_SUM_Fung&subentitytype='+$j("#subentityType").val()+'&questionNo=FungalCulture&responsestr='+responsestr+'&assessDivName='+mDiv+'&responseval='+val;
		$j('#'+mDiv).show();
		//$j('#'+buttonId).show();
		$j('#'+buttonId).hide();
		$j('#'+hideButtonTd).show();
		refreshassessment1('getassessmentHemo?assesmentCheckFlag='+assesmentCheckFlag,divid,data);
		$j("#"+id).removeClass('addAssessmentMandatory');
	}
	else{
			$j('#'+mDiv).hide();
			//$j('#showassesmentFung').hide();
			$j('#'+buttonId).hide();
			$j("#"+id).removeClass('addAssessmentMandatory');
		}
	//2nd Type Assessment
}
function refreshassessment1(url,divname,data){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data:data,
        success: function (result){
            var $response=$j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).html(result);
            	if('<s:property value="esignFlag"/>'=='review'){
            		$j("#"+divname+" button").hide();
            	}
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
}

function closeModalscompreq(divname){
	$j("#"+divname).dialog("close");
	 $j("#"+divname).dialog("destroy");
	 $j("#"+divname).html("");
}

function addTest(cordId,cbuOtherTestFlag){
	var id="datepickerColllabModalCri";
	var msg="<s:text name='garuda.message.modal.colldatecantbecurdate'/>";
	/* var compCurNCordDate=checkCurrentDateCordDate(id,msg);
	if(compCurNCordDate){ */
		var headerLnght = $j("#added1 thead th").length;
		//alert(headerLnght);
		var preTstDate =document.getElementById("preTestDateStrnM").value;
		var postTstDate =document.getElementById("postTestDateStrnM").value;
		var postTstThawDate = new Array();
		//postTstThawDate=null;
		var counter=0;
		for(counter=0; counter<headerLnght-3 ; counter++){
			 var value1 = counter+1;
			 //var temp = document.getElementById("otherTestDateStrn"+value1).value;
		var val1 =$j("#otherTestDateStrnM"+value1).val();
		postTstThawDate[counter] = val1.replace(",",":");
		//otherLabTstsLstId=$j("#otherLabTstsLstId").val();
		//alert(otherLabTstsLstId);
		 /* postTstThawDate[1] =document.getElementById("otherTestDateStrn2").value;
		alert(postTstThawDate[1]);
		 postTstThawDate[2] =document.getElementById("otherTestDateStrn3").value;
		alert(postTstThawDate[2]);
		 postTstThawDate[3] =document.getElementById("otherTestDateStrn4").value;
		alert(postTstThawDate[3]);
		 postTstThawDate[4] =document.getElementById("otherTestDateStrn5").value;
		alert(postTstThawDate[4]);
		 postTstThawDate[5] =document.getElementById("otherTestDateStrn6").value;
		alert(postTstThawDate[5]); */
		//counter++;
		}
		var specCollDate = $j("#datepickerColllabModalCri").val();
		showModalWithData('Add New Test for CBU Registry ID <s:property value="CdrCbuPojo.registryId"/>','addNewTest?cordId='+cordId+'&specCollDate='+specCollDate+'&specimenId=<s:property value="cdrCbuPojo.fkSpecimenId"/>&cbuOtherTestFlag='+cbuOtherTestFlag+'&headerLnght='+headerLnght+'&preTstDate='+preTstDate+'&postTstDate='+postTstDate+'&postTstThawDate='+postTstThawDate,'500','700','labSummaryForm','modelPopup2');
		/* } */
	}

function addTestDateModel(specId,removeUnknownTestFlag){
		var specCollDate = "<s:property value='CdrCbuPojo.specimen.specCollDate'/>";
	 var url="addtestdatemodal?cordentry=true&fkSpecimenId="+specId+"&specCollDate="+specCollDate+"&removeUnknownTestFlag="+removeUnknownTestFlag;
	 showModal('ADD TEST DATE',url,'200','300');
}

function vidSaved(id,value){
	if(value!=null && value!="" && value!="undefined"){
		$j("#"+id).show();
		}
		else{
			$j("#"+id).hide();
			$j("#"+id).children().each(function(n, i) {
				  var idParent = this.id;
				 if((idParent.indexOf('fktestreason') != -1 || idParent.indexOf('fktestspecimen') != -1 || idParent.indexOf('fktestmethod') != -1)&& 
						  (idParent.indexOf('fktestreason_error') == -1 || idParent.indexOf('fktestspecimen_error') == -1 || idParent.indexOf('fktestmethod_error') == -1)){
						$j("#"+idParent+" option[value='-1']").attr("selected", "selected");
				  }
				 if(idParent.indexOf('reasonFrTestDescOthTm')!= -1 || idParent.indexOf('testMethDescOthTm')!= -1 ){
						$j("#"+idParent).children().each(function(n, i) {
							var idChild = this.id;
						  	if((idChild.indexOf('reasonTestDesc') != -1 || idChild.indexOf('testMthdDesc') != -1 )&& 
									  (idChild.indexOf('reasonTestDesc_error') == -1  || idChild.indexOf('testMthdDesc_error') == -1)){
									$j("#"+idChild).val('');
							  }
						});
						$j("#"+idParent).hide();
				 }
		 });
		}
	//alert("ffffff");
}

function showAssessment(val)
{
	//alert(val);
}

function vid(num,value){
if(value!=null && value!="" && value!="undefined"){
$j("#showDropDown"+num).show();
}
else{
	$j("#showDropDown"+num).hide();	

	$j("#showDropDown"+num).children().each(function(n, i) {
		  var id = this.id;
		 if((id.indexOf('fktestreason') != -1 || id.indexOf('fktestspecimen') != -1 || id.indexOf('fktestmethod') != -1)&& 
				  (id.indexOf('fktestreason_error') == -1 || id.indexOf('fktestspecimen_error') == -1 || id.indexOf('fktestmethod_error') == -1)){
				$j("#"+id+" option[value='-1']").attr("selected", "selected");
		  }
		 if(id.indexOf('reasonFrTestDescOthTm')!= -1 || id.indexOf('testMethDescOthTm')!= -1 ){
				$j("#"+id).children().each(function(n, i) {
					var idChild = this.id;
				  	if((idChild.indexOf('reasonTestDesc') != -1 || idChild.indexOf('testMthdDesc') != -1 )&& 
							  (idChild.indexOf('reasonTestDesc_error') == -1  || idChild.indexOf('testMthdDesc_error') == -1)){
							$j("#"+idChild).val('');
					  }
				});
				$j("#"+id).hide();
		 }
 });
}
	//alert("Still Wating ! Is That you ");
}
function showDescField(divId,compValue,fieldval,fieldId){
	if(compValue==fieldval){
		$j("#"+divId).show();
}else{
	$j("#"+divId).hide();
	$j("#"+divId).children().each(function(n, i) {
		  var id = this.id;
		  if(id.indexOf('reasonTestDesc') != -1 && id.indexOf('reasonTestDesc_error') == -1){
				$j("#"+id).val('');
						  }
		  if(id.indexOf('testMthdDesc') != -1 && id.indexOf('testMthdDesc_error') == -1){
				$j("#"+id).val('');
		  }
		});
}
	if(fieldval==-1 || fieldval==''){
		$j("#"+fieldId+"_error").show();
		$j("#"+fieldId+"_error").text("<s:text name="garuda.common.validation.value"></s:text>");
	}
	else{
		$j("#"+fieldId+"_error").hide();
	}
}
function chekDepandtFldPrs(fieldId,fieldval){
	if(fieldval==null || fieldval==''){
		$j("#"+fieldId+"_error").show();
	}
	else{
		$j("#"+fieldId+"_error").hide();
	}
}

function addInfo(fkSpecimenId,fktestid,pkpatlabs){
	//alert("ssssss");
	}
			function cfuCount(cfuVal){
				
				if(cfuVal ==$j("#pkOther").val()){
				document.getElementById("cfuCount").style.display="block";
					}
				else{
					document.getElementById("cfuCount").style.display="none";
					$j("#cfuDescOther").val('');
					}
			}
function test(viabilityVal){
	if(viabilityVal ==$j("#pkViaOther").val()){
		 document.getElementById("viability").style.display="block";
		}
	else{
		document.getElementById("viability").style.display="none";
		$j("#viaDescOther").val('');
		}
	     }

function saveValues(obJ)
{
	var fldValue="#flD"+obJ.id;
	var selVal= $j("#"+obJ.id).val();
	
	if(selVal!=""){
		var selMulPk = $j.map($j("#"+obJ.id+" option:selected"), function (el, i) {
			if($j(el).val()!=""){
			return $j(el).val();
			}else{
			   return ;
			}
		});
	   	 $j(fldValue).val(selMulPk.join(","));
	}else{

	   	 $j(fldValue).val("");
	}
}
		     
   </SCRIPT>
	
<script>

 function customRangeM(input) {
		var today = new Date();
		var d = today.getDate();
		var m = today.getMonth();
		var y = today.getFullYear();
		var h=today.getHours();
		var mn=today.getMinutes()+1;
		var mindate=$j("#compCollecDate").val();
		//var minTestDate=$j("#compBrthDate").val();  previous reqiurements
		var minTestDate=$j("#compCollecDate").val();  //new requirements
		var minTestDate1=minTestDate.split("-");
		var mindate1=mindate.split("-");
		if (input.id == 'datepicker4' ||input.id == 'datepicker5' ||input.id == 'datepicker6' ||input.id == 'datepicker7') {
			/*return {
		    	 minDate: new Date(mindate1[0],mindate1[1]-1,mindate1[2])
		    };*/
			$j('#'+input.id).datepicker( "option", "minDate", new Date(mindate1[0],mindate1[1]-1,mindate1[2]));
		  }
		else if(input.id == 'preTestDateStrnM' ||input.id == 'postTestDateStrnM' ||input.id == 'otherTestDateStrnM1' ||input.id == 'otherTestDateStrnM2' ||input.id == 'otherTestDateStrnM3' ||input.id == 'otherTestDateStrnM4' ||input.id == 'otherTestDateStrnM5' ||input.id == 'otherTestDateStrnM6'){
			/*return {
		    	 minDate: new Date(minTestDate1[0],minTestDate1[1]-1,++minTestDate1[2])
		    };*/
			$j('#'+input.id).datepicker( "option", "minDate", new Date(minTestDate1[0],minTestDate1[1]-1,minTestDate1[2]));
			if(input.id == 'preTestDateStrnM'){
		    	var maxTestDate=new Date($j("#datepicker4").val());// processing start date
				var d1 = maxTestDate.getDate();
				var m1 = maxTestDate.getMonth();
				var y1 = maxTestDate.getFullYear();
				if(today.getTime() > maxTestDate.getTime()){
					$j('#'+input.id).datepicker( "option", "maxDate", new Date(y1,m1,d1));
				}else{
					$j('#'+input.id).datepicker( "option", "maxDate", new Date(y,m,d));
				}
		    }else if(input.id !== 'otherTestDateStrnM5' && input.id !== 'otherTestDateStrnM6'){
		    	LabSumPostCustomDate(input,'datepicker4');
			 }		
		}
	}
 var LabSumPostCustomDate=function(input,id){
	    var today = new Date();
		var d = today.getDate();
		var m = today.getMonth();
		var y = today.getFullYear();
		
		var maxTestDate=new Date($j("#"+id).val());// processing start date
		var d1 = maxTestDate.getDate();
		var m1 = maxTestDate.getMonth();
		var y1 = maxTestDate.getFullYear();
		
		$j('#'+input.id).datepicker( "option", "minDate", new Date(y1,m1,d1));
		$j('#'+input.id).datepicker( "option", "maxDate", new Date(y,m,d));
}
 function checkTimeFormatForCord(fieldId){
	  var value=$j("#"+fieldId).val();
	  var returnFlag=dateTimeFormat(value);
	  if(returnFlag!=true && value!=''){
				 $j("#"+fieldId).val("");
				 $j("#"+"dynaError"+fieldId).show();
			 }
			 else{
				 $j("#"+"dynaError"+fieldId).hide();
			 }
	  return returnFlag;
 }
     function restrict(mytextarea) 
     {  
         var maxlength = 15;
         mytextarea.value = mytextarea.value.substring(0,maxlength);
     }    

     function rhSpecify(val){
           if(val=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@RH_TYPE, @com.velos.ordercomponent.util.VelosGarudaConstants@RH_TYPE_OTHER)"/>'){
               $j("#rhothspec").show();
               $j('#specRhTypeOther').addClass("cbuLabSummMandatory");
           }else{
        	   $j("#rhothspec").hide();
        	   $j('#specRhTypeOther').removeClass("cbuLabSummMandatory");
        	   $j('#specRhTypeOther').val('');
           }
     }
     function setDefBactFungDate(procDateStrId,bactDateStrId,fungDateStrId,emptyChngFlag){
    		if($j("#"+procDateStrId).val()!=null){
    			if(document.getElementById(bactDateStrId)!=null && document.getElementById(bactDateStrId)!='undefined' && $j("#"+bactDateStrId).is(':visible')==true){
    				if($j("#"+bactDateStrId).val()=='' && emptyChngFlag=="procChng"){
    				$j("#"+bactDateStrId).val($j("#"+procDateStrId).val());
    				}else if($j("#"+bactDateStrId).val()=='' && emptyChngFlag=="bactChng"){
    					$j("#"+bactDateStrId).val($j("#"+procDateStrId).val());
    				}
    			}
    			if(document.getElementById(fungDateStrId)!=null && document.getElementById(fungDateStrId)!='undefined' && $j("#"+fungDateStrId).is(':visible')==true){
    				if($j("#"+fungDateStrId).val()=='' && emptyChngFlag=="procChng"){
    				$j("#"+fungDateStrId).val($j("#"+procDateStrId).val());
    				}else if($j("#"+fungDateStrId).val()=='' && emptyChngFlag=="fungChng"){
    					$j("#"+fungDateStrId).val($j("#"+procDateStrId).val());
    				}
    			}
    		}
    		if($j("#"+bactDateStrId).val()!=""){
    			$j(".placeHolderOfid_"+bactDateStrId).hide();
    		}else{
    			$j(".placeHolderOfid_"+bactDateStrId).show();
    		}
    		if($j("#"+fungDateStrId).val()!=""){
    			$j(".placeHolderOfid_"+fungDateStrId).hide();
    		}else{
    			$j(".placeHolderOfid_"+fungDateStrId).show();
    		}
    	}
	</script>
			<script>
					function limitText(limitField, limitNum) {
					    if (limitField.value.length > limitNum) {
					        limitField.value = limitField.value.substring(0, limitNum);
					    } 
					}

					$j(document).ready(function(){						
						if($j("#esignFlag").val()=='labSummary'){
							var div=$j('#labDiv').html();
							var formContent='<form id="labSummaryForm">'+div+'</form>';
							$j('#labDiv').replaceWith(formContent);
						}
						if('<s:property value="esignFlag"/>'=='review'){		
							
							var div=$j('#labSummaryModal').html();
							var formContent='<div id="labSumTargetDiv" class="rightcont" style="width: 100%;overflow: auto;"><form id="labSummaryForm">'+div+'</form></div>';
							$j('#labSumTargetDiv').replaceWith(formContent);
							$j('#procandcount select,#procandcount input[type=radio],#procandcount input[type=checkbox]').attr('disabled',true);
							$j('#procandcount input[type=text]').attr('disabled',true);
							$j('#procandcount input[type=button]').css('display','none');
							$j('#procandcount button').css('display','none');
							if('<s:property value="cdrCbuPojo.bactComment"/>'==null || '<s:property value="cdrCbuPojo.bactComment"/>'==''){
								$j('#addBactComment').css('display','none');
								$j('#addBactCommentPlusPic').css('display','none');
							}
							else if('<s:property value="cdrCbuPojo.bactComment"/>'!=null && '<s:property value="cdrCbuPojo.bactComment"/>'!=''){
								$j('#addBactComment').text("<s:text name="garuda.cbufinalreview.label.flaggedItems.viewcomment"></s:text>");
							}
							if('<s:property value="cdrCbuPojo.fungComment"/>'==null || '<s:property value="cdrCbuPojo.fungComment"/>'==''){
								$j('#addFungComment').css('display','none');
								$j('#addFungCommentPlusPic').css('display','none');
							}
							else if('<s:property value="cdrCbuPojo.fungComment"/>'!=null && '<s:property value="cdrCbuPojo.fungComment"/>'!=''){
								$j('#addFungComment').text("<s:text name="garuda.cbufinalreview.label.flaggedItems.viewcomment"></s:text>");
							}		
						}
						if('<s:property value="esignFlag"/>'=='modal'){
							var div=$j('#labSummaryModal').html();
							$j('#labSumTargetDiv').empty();
							var formContent='<form id="labSummaryForm">'+div+'</form>';
							$j('#labSummaryModal').html(formContent);
							$j('#esignForLabSummary').show();
							}
						bacterial('<s:property value="cdrCbuPojo.bacterialResult"/>');
						fungal('<s:property value="cdrCbuPojo.fungalResult"/>');
							if('<s:property value="esignFlag"/>'=='modal' || '<s:property value="esignFlag"/>'=='labSummary'){	
								var validator = $j("#labSummaryForm").validate();
							}
						});

					 getDatePic();
					 //jQuery('#datepicker4').datepicker('option', { beforeShow: customRangeM });
					 //jQuery('#datepicker5').datepicker('option', { beforeShow: customRangeM });
					 assignDatePicker('datepicker4');
					 assignDatePicker('datepicker5');
					 jQuery('#datepicker6').datepicker('option', { beforeShow: customRangeM }).focus(function(){
							if(!$j("#ui-datepicker-div").is(":visible"))
								customRangeM(this);
							if($j(this).val()!=""){
								var thisdate = new Date($j(this).val());
								if(!isNaN(thisdate)){
									var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
									$j(this).val(formattedDate);
								}
							}
						});
					 jQuery('#datepicker7').datepicker('option', { beforeShow: customRangeM }).focus(function(){
							if(!$j("#ui-datepicker-div").is(":visible"))
								customRangeM(this);
							if($j(this).val()!=""){
								var thisdate = new Date($j(this).val());
								if(!isNaN(thisdate)){
									var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
									$j(this).val(formattedDate);
								}
							}
						});
					 jQuery('#preTestDateStrnM').datepicker('option', { beforeShow: customRangeM }).focus(function(){
							if(!$j("#ui-datepicker-div").is(":visible"))
								customRangeM(this);
							if($j(this).val()!=""){
								var thisdate = new Date($j(this).val());
								if(!isNaN(thisdate)){
									var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
									$j(this).val(formattedDate);
								}
							}
						});
					 jQuery('#postTestDateStrnM').datepicker('option', { beforeShow: customRangeM }).focus(function(){
							if(!$j("#ui-datepicker-div").is(":visible"))
								customRangeM(this);
							if($j(this).val()!=""){
								var thisdate = new Date($j(this).val());
								if(!isNaN(thisdate)){
									var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
									$j(this).val(formattedDate);
								}
							}
						});
					 jQuery('#otherTestDateStrnM1').datepicker('option', { beforeShow: customRangeM }).focus(function(){
							if(!$j("#ui-datepicker-div").is(":visible"))
								customRangeM(this);
							if($j(this).val()!=""){
								var thisdate = new Date($j(this).val());
								if(!isNaN(thisdate)){
									var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
									$j(this).val(formattedDate);
								}
							}
						});
					 jQuery('#otherTestDateStrnM2').datepicker('option', { beforeShow: customRangeM }).focus(function(){
							if(!$j("#ui-datepicker-div").is(":visible"))
								customRangeM(this);
							if($j(this).val()!=""){
								var thisdate = new Date($j(this).val());
								if(!isNaN(thisdate)){
									var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
									$j(this).val(formattedDate);
								}
							}
						});
					 jQuery('#otherTestDateStrnM3').datepicker('option', { beforeShow: customRangeM }).focus(function(){
							if(!$j("#ui-datepicker-div").is(":visible"))
								customRangeM(this);
							if($j(this).val()!=""){
								var thisdate = new Date($j(this).val());
								if(!isNaN(thisdate)){
									var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
									$j(this).val(formattedDate);
								}
							}
						});
					 jQuery('#otherTestDateStrnM4').datepicker('option', { beforeShow: customRangeM }).focus(function(){
							if(!$j("#ui-datepicker-div").is(":visible"))
								customRangeM(this);
							if($j(this).val()!=""){
								var thisdate = new Date($j(this).val());
								if(!isNaN(thisdate)){
									var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
									$j(this).val(formattedDate);
								}
							}
						});
					 jQuery('#otherTestDateStrnM5').datepicker('option', { beforeShow: customRangeM }).focus(function(){
							if(!$j("#ui-datepicker-div").is(":visible"))
								customRangeM(this);
							if($j(this).val()!=""){
								var thisdate = new Date($j(this).val());
								if(!isNaN(thisdate)){
									var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
									$j(this).val(formattedDate);
								}
							}
						});
					 jQuery('#otherTestDateStrnM6').datepicker('option', { beforeShow: customRangeM }).focus(function(){
							if(!$j("#ui-datepicker-div").is(":visible"))
								customRangeM(this);
							if($j(this).val()!=""){
								var thisdate = new Date($j(this).val());
								if(!isNaN(thisdate)){
									var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
									$j(this).val(formattedDate);
								}
							}
						});
					 if('<s:property value="esignFlag"/>'=='review'){
					//for removing datepicker icon in view mode
						$j("img.ui-datepicker-trigger").remove();
					 }
					$j('.timeValid').keypress(function(e){
						//console.log('key pressing');
						  var val = $j(this).val();
						  if(val !="" && val!=null ){
						   if(e.keyCode!=8 && e.keyCode!=46 )
						   {
							  if(val.length==2){
										if(e.which!=58){
										$j(this).val(val+":");
								  }
							  }
						   }
						 }
			        });
					$j("input").bind({keydown:function(e) {
						 var key = e.charCode || e.keyCode;
						 if(key==9){
							 calcCbuNbrcFlag=false;
						 }
						 else{
							 calcCbuNbrcFlag=true;
						 }
					}});
					if($j("#pkViaOther").val()==$j("#viabSelected").val()){
		    			 $j("#viability").show();
		    			}else{
		    				$j("#viability").hide();
		    				}

		     		if($j("#pkOther").val()==$j("#cfuSelected").val()){
		    			 $j("#cfuCount").show();
		    			}else{
		    				$j("#cfuCount").hide();
		    			}
					
					function fungal(value){
						if(value==$j("#bacterialPositiveFung").val() || value==$j("#bacterialNegativeFung").val()){
							$j("#fungDateText").show();
							$j("#fungDate").show();
							$j('#datepicker7').addClass('cbuLabSummMandatory');
						}
						else{
							$j("#fungDateText").hide();
							$j("#fungDate").hide();
							$j("#datepicker7").val(null);
							$j('#datepicker7').removeClass('cbuLabSummMandatory');
						}
						if(value!="<s:property value='cdrCbuPojo.fungalResult'/>" && (value!=$j("#bacterialPositiveFung").val() || value!=$j("#bacterialNegativeFung").val())){
							$j("#datepicker7").val(null);
						}else{
							$j("#datepicker7").val($j("#datepicker7_dbStoredVal").val());
						}
						$j("#dropValueFung").val(value);
						if($j("#datepicker7").val()!=""){
							$j(".placeHolderOfid_datepicker7").hide();
						}else{
							$j(".placeHolderOfid_datepicker7").show();
						}
					}
					function bacterial(value)
					{
						if(value==$j("#bacterialPositive").val() || value==$j("#bacterialNegative").val())
						{
							$j("#bactDateText").show();
							$j("#bactDate").show();
							$j('#datepicker6').addClass('cbuLabSummMandatory');
						}else{
							$j("#bactDateText").hide();
							$j("#bactDate").hide();
							$j("#datepicker6").val(null);
							$j('#datepicker6').removeClass('cbuLabSummMandatory');
						}
						if(value!="<s:property value='cdrCbuPojo.bacterialResult'/>" && (value!=$j("#bacterialPositive").val() || value!=$j("#bacterialNegative").val())){
							$j("#datepicker6").val(null);
						}else{
							$j("#datepicker6").val($j("#datepicker6_dbStoredVal").val());
						}
							$j("#dropValue").val(value);
							if($j("#datepicker6").val()!=""){
								$j(".placeHolderOfid_datepicker6").hide();
							}else{
								$j(".placeHolderOfid_datepicker6").show();
							}
					}
					function textFunction(divId,textAreaId,saveButtonId,savedComment)
					{
							$j("#"+divId).show();
							if('<s:property value="esignFlag"/>'=='review'){
							$j("#"+textAreaId).attr('readonly',true);
							$j('#comClose1').show();
							$j('#comClose2').show();}
							textareaValue=document.getElementById(textAreaId).value;
							if(savedComment!=null && savedComment!=''){
								$j("#"+textAreaId).attr('readonly',true);
								$j("#"+saveButtonId).hide();
							}
					}

					function commentSave(divId,textAreaId,addComment)
					{
						    textareaValue=document.getElementById(textAreaId).value;
							if(textareaValue==''){
									$j("#"+addComment).text("Add Comment");
									//$j("#"+textId).name("Add Comment");
								}
							else{
									$j("#"+addComment).text("Add/View Comment");
								}
							$j("#"+divId).hide();
					}

					function commentClose(divId,textAreaId)
					{
							$j('#textArea').removeClass('labChanges');
							document.getElementById(textAreaId).value = textareaValue;
							$j("#"+divId).hide();
					}
					$j('#labSummaryModal .cbuLabSummMandatory').each(function(){
						 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
							 $j(this).css("border", "");							 
							 $j(this).css("border", "1px solid red");							
							}							 
					});
					function addPreTestDate(value){
						if($j("#preTestDateStrnM").val()=="" ){
							autoFilled= true;
							$j("#preTestDateStrnM").val(value);
						  }
						 }
						function addPostTestDateScnd(value){
						if($j("#postTestDateStrnM").val()==""){
							autoFillee= true;
							$j("#postTestDateStrnM").val(value);
						}
						}
						/** HEMOGLOBINOPATHY AUTO DEFER FUNCTION **/
					
			</script>
				<div id="successLab" style="display: none;">
			        <%-- <jsp:include page="../cb_update.jsp">
				    	<jsp:param value="garuda.completeReqInfo.label.labsummaryupdateSucc" name="message"/>
				  	</jsp:include> --%>
				  	<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr >
							<td colspan="2">
								<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
									<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
									<strong><s:text name="garuda.completeReqInfo.label.labsummaryupdateSucc" /> </strong></p>
								</div>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="button" onclick="closeModalscompreq('labSummaryForComplete')" value="<s:text name="garuda.common.close"/>" />
							</td>
						</tr>
				</table>
			</div>
			<div style="display: none;">
				<s:date name="CdrCbuPojo.specimen.specCollDate" id="compCollecDate" format="yyyy-MM-dd" />	
 				<s:textfield readonly="true" name="compCollecDate" id="compCollecDate"  value="%{compCollecDate}"></s:textfield>											      				
			</div>
			<div style="display: none;">
				<s:date name="CdrCbuPojo.birthDate" id="compBrthDate" format="yyyy-MM-dd" />	
 				<s:textfield readonly="true" name="compBrthDate" id="compBrthDate"  value="%{compBrthDate}" ></s:textfield>											      				
			</div>	
			<div id="labSummaryModal">
			 <div id="labSumm_bodyDiv">
				<div id="procandcount">
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all labsumHidDiv" style="text-align: center;" >
					<span class="ui-icon ui-icon-minusthick"></span>
					<span class="ui-icon ui-icon-close" ></span>
					<s:text name="garuda.cordentry.label.labsummary" />
					</div>
					   <s:hidden name="esignFlag" id="esignFlag"></s:hidden>
					   <s:hidden value="%{#request.bacterialPositiveFung}" id="bacterialPositiveFung"></s:hidden>
					   <s:hidden value="%{#request.bacterialNegativeFung}" id="bacterialNegativeFung"></s:hidden>	
					   <s:hidden value="%{#request.bacterialPositive}" id="bacterialPositive"></s:hidden>
					   <s:hidden value="%{#request.bacterialNegative}" id="bacterialNegative"></s:hidden>
					   <s:hidden value="%{#request.bacterialNotDone}" id="bacterialNotDone"></s:hidden>	
	                   <s:hidden name="pkCfuOther" id="pkOther"></s:hidden>	
	                   <s:hidden name="pkViabilityOther" id="pkViaOther"></s:hidden>
	                   <input type="hidden" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN,@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_HEMOSICK)"/>" id="hemoSickBeta"/>
					   <input type="hidden" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN,@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_SICKLE)"/>" id="hemoSickCell"/>
					   <input type="hidden" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN,@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_ALPHA)"/>" id="hemoAlphaSev"/>
					   <input type="hidden" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN,@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_BETA)"/>" id="hemoBeatThal" />
					   <input type="hidden" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN,@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_MULT)"/>" id="multTrait"/>	
	                   <s:hidden value="%{cdrCbuPojo.registryId}" id="regid" />
	                  <!--<s:hidden name="prePatLabsPojo.fkTimingOfTest" id="fkTimingOfTest"></s:hidden>
					    <s:hidden name="postPatLabsPojo.fkTimingOfTest" id="fkTimingOfTest"></s:hidden>
					    <s:hidden name="otherPatLabsPojo.fkTimingOfTest" id="fkTimingOfTest"></s:hidden>
					    -->
					   <s:hidden name="patLabsPojo.fkTimingOfTest"></s:hidden>
					   <s:hidden name="patLabsPojo.fktestid" id="fktestid"></s:hidden>
					   <s:hidden name="cdrCbuPojo.fkSpecimenId" id="fkSpecimenId"></s:hidden>
					   <s:hidden name="patLabsPojo.testresult"></s:hidden>
					   <s:hidden name="dropValueFung" id="dropValueFung"></s:hidden>
					   <s:hidden name="dropValue" id="dropValue"></s:hidden>
					   <s:hidden name="subentityType" id="subentityType" value="%{#request.subentitytype}"/>
					   <s:hidden name="cdrCbuPojo.rhTypeOther" id="rhtypeother" ></s:hidden>
					   <s:hidden value="%{#request.rhSpecOtherPk}" id="rhSpecOtherPk"></s:hidden>
					  <s:hidden name ="assess_ncd" id="assess_ncd" value="%{#request.assess_ncd}"/>
						<s:hidden id='iscdrMember' value="%{cdrCbuPojo.site.isCDRUser()}"></s:hidden>
					   <s:hidden value="%{#request.valuePreFkTimingTest}" id="valuePreFkTimingTest"></s:hidden>
					   <s:hidden value="%{#request.valuePostFkTimingTest}" id="valuePostFkTimingTest"></s:hidden>
					   <s:hidden value="%{#request.valueOtherFkTimingTest}" id="valueOtherFkTimingTest"></s:hidden>
					   
					   <s:hidden value="%{#request.valueOtherPostTestFirst}" id="valueOtherPostTestFirst"></s:hidden>
					   <s:hidden value="%{#request.valueOtherPostTestSecond}" id="valueOtherPostTestSecond"></s:hidden>
					   <s:hidden value="%{#request.valueOtherPostTestThird}" id="valueOtherPostTestThird"></s:hidden>
					   <s:hidden value="%{#request.valueOtherPostTestFourth}" id="valueOtherPostTestFourth"></s:hidden>
					   <s:hidden value="%{#request.valueOtherPostTestFifth}" id="valueOtherPostTestFifth"></s:hidden>
					   	<s:date name="cdrCbuPojo.getSpecimen().getSpecCollDate()" id="collectionDateId" format="MMM dd, yyyy" />
						<s:hidden value="%{collectionDateId}" id="collectionDateId" cssClass="collectDate" name="collectiondateandtime"/>
						<s:date name="cdrCbuPojo.getSpecimen().getSpecCollDate()" id="collectionTimeId" format="HH:mm" />
						<s:hidden value="%{collectionTimeId}" id="collectionTimeId" name="collectionTime"/>
						<s:hidden value="%{#request.procStartDt}" id="procStartDtinLabSum"></s:hidden>
						<s:hidden value="%{#request.procTermiDt}" id="procTermiDtinLabSum"></s:hidden>
						 <s:hidden name="cordCbuStatus"></s:hidden>
   						 <s:hidden name="autoDeferExecFlag" id="labSumaryAutoDeferExecFlag"></s:hidden>
					   			<table width="100%" border="1">
					   				<tr>
					   					<td>
						     			 <table width="100%" cellpadding="0" cellspacing="0" border="0">	
																<tr>
							                 						<td width="20%"><s:text name="garuda.cordentry.label.prostartdate"></s:text>:<br/><font size="1">&nbsp;</font></td>
											 						<td width="20%">
											 							<s:date name="cdrCbuPojo.processingDate" id="datepicker4" format="MMM dd, yyyy" />
													 					<s:textfield style="width:auto;" readonly="true" onkeydown="cancelBack();"  name="prcsngStartDateStr" id="datepicker4" value="%{datepicker4}" cssClass="labclass cbuLabSummMandatory onChangeMandDate procStartDate" onchange="validateDateData('datepicker4','cordProcessingTime','datepicker5','cordFreezeTime','precessDatebffrz','precessDatebffrztime');addPreTestDate(this.value),addPostTestDateScnd(this.value);setDefBactFungDate('datepicker4','datepicker6','datepicker7','procChng');isChangeDone(this,'%{datepicker4}','labChanges');"></s:textfield>
																		<s:hidden value="%{datepicker4}" id="datepicker4_dbStoredVal"/>
																		<span style="display:none" id="dynaErrorPrcngDt" class="error"><br/><s:text name="garuda.cbu.cordentry.prcssngdtwrtproc"/></span><font size="1">&nbsp;</font>
																		<span style="display:none" id="processdateErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.collectionmust"></s:text></span>
													                    <span style="display:none" id="processdateErrorMsg1" class="error"><br/><s:text name="garuda.cbu.cordentry.procdateaftercoldate"></s:text></span>
													                    <span style="display:none" id="precessDatebffrz" class="error prcessError"><br/><s:text name="garuda.cbu.cordentry.procbeforefreezdate"/></span>
					 													<font size="2">&nbsp;</font><span style="display:none" id="datepicker4_error" class="error"><br/><s:text name=""></s:text></span>					 													
																	</td>
																	<td width="20%"><s:text name="garuda.cordentry.label.proStartTime"></s:text>:<br/><font size="1">&nbsp;</font></td>	
																     <td width="20%"><s:textfield style="width:auto;" name="cdrCbuPojo.processingTime" id="cordProcessingTime" onchange="checkTimeFormatForCord('cordProcessingTime');validateDateData('datepicker4','cordProcessingTime','datepicker5','cordFreezeTime','precessDatebffrz','precessDatebffrztime');onChangeProcnCountFldVald(this.id,this.value);isChangeDone(this,'%{cdrCbuPojo.processingTime}','labChanges');" cssClass="timeValid labclass cbuLabSummMandatory" maxlength="5"></s:textfield><font size="1">(HH:MM 24 HR)</font>								  						
											 						<s:hidden value="cdrCbuPojo.processingTime" id="cordProcessingTime_dbStoredVal"/>
					 												<span style="display:none" id="cordProcessingTime_error" class="error"><br/><s:text name=""></s:text> </span>
											 						<span style="display:none" id="dynaErrorcordProcessingTime" class="error"><br/><s:text name="garuda.common.validation.timeformat"></s:text></span>
											 						<span style="display:none" id="precessDatebffrztime" class="error prcessError"><br/><s:text name="garuda.cbu.cordentry.procbeforefreeztime"/></span>
											 						</td>
											 						<td width="20%"></td>
																</tr>
																<tr>
																 
																	<td width="20%" style="padding-left:10px;" ><s:text name="garuda.cordentry.label.freezedate"></s:text>:<br/><font size="1">&nbsp;</font></td>
																	<td width="20%">
												     					<s:date name="cdrCbuPojo.frzDate" id="datepicker5" format="MMM dd, yyyy" />
													 					<s:textfield style="width:auto;" readonly="true" onkeydown="cancelBack();"  name="frzDateStr" id="datepicker5" value="%{datepicker5}" cssClass="labclass cbuLabSummMandatory onChangeMandDate freezDate" onchange="validateDateData('datepicker4','cordProcessingTime','datepicker5','cordFreezeTime','frzdateAftprcStart','frzTimeRangebfProcssTime'),validateCordDates('datepicker4','datepicker5','frzdateErrorMsg');validateCordDateRange('collectionDateId','datepicker5','frzdateRangeErrorMsg',true);isChangeDone(this,'%{datepicker5}','labChanges');"></s:textfield><br/><font size="1">&nbsp;</font>
									            						<s:hidden value="%{datepicker5}" id="datepicker5_dbStoredVal"/>
					 													<span style="display:none" id="datepicker5_error" class="error"><br/><s:text name=""></s:text></span>
					 													<span style="display:none" id="dynaErrorFreezeDt" class="error"><br/><s:text name="garuda.cbu.cordentry.freezedtwrtproc"/></span>
					 													<span style="display:none" id="frzdateErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.procdatebeforefrzdate"></s:text></span>
										                                <span style="display:none" id="frzdateRangeErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.frzdateTimehrs"></s:text> </span>
										                                <span style="display:none" id="frzdateAftprcStart" class="error prcessError"><br/><s:text name="garuda.cbu.cordentry.frzdateafterproc"></s:text></span>
									            					</td>
									    							 <td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.freezeTime"></s:text>:<br/><font size="1">&nbsp;</font></td>	
									     							 <td width="20%"><s:textfield style="width:auto;" name="cdrCbuPojo.freezeTime"  cssClass="timeValid cbuclass" maxlength="5" id="cordFreezeTime" onchange="checkTimeFormatForCord('cordFreezeTime');validateHours('collectionDateId','collectionTimeId','datepicker5','cordFreezeTime','frzTimeRangeErrorMsg');validateDateData('datepicker4','cordProcessingTime','datepicker5','cordFreezeTime','frzdateAftprcStart','frzTimeRangebfProcssTime');isChangeDone(this,'%{cdrCbuPojo.freezeTime}','labChanges');"></s:textfield><font size="1">(HH:MM 24 HR)</font>					  							
																     <span style="display:none" id="dynaErrorcordFreezeTime" class="error"><br/><s:text name="garuda.common.validation.timeformat"></s:text> </span>
																     <span style="display:none" id="frzTimeRangeErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.frzTimehrs"></s:text></span>
																     <span style="display:none" id="frzTimeRangebfProcssTime" class="error prcessError"><br/><s:text name="garuda.cbu.cordentry.frztimeafterproctime"></s:text> </span>
																     </td>
																     
																     <td width="20%"></td>
						        							    </tr>
																<tr>
																	  <td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.bactcult"></s:text>:</td>
																      <td width="20%"><s:select style="width:auto;" name="cdrCbuPojo.bacterialResult" cssClass="labclass cbuLabSummMandatory" id="bacterialResult" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="bacterial(this.value),bactAutoDefer(this.value,this.id,'bactDateText','bactDateStrId','bactDate');setDefBactFungDate('datepicker4','datepicker6','datepicker7','bactChng');onChngFldValdNDepFldVald(this.id,this.value,'datepicker6','bacterialNegative','notReq');isChangeDone(this,'%{cdrCbuPojo.bacterialResult}','labChanges');"></s:select>
																      <s:hidden value="%{cdrCbuPojo.bacterialResult}" id="bacterialResult_dbStoredVal"/>
					 												  <span style="display:none" id="bacterialResult_error" class="error"><br/><s:text name=""></s:text> </span>
																      </td>
																  	  <td width="20%" id="bactDateText" <s:if test="(cdrCbuPojo.bacterialResult!=#request.bacterialNegative && cdrCbuPojo.bacterialResult!=#request.bacterialPositive) || (cdrCbuPojo.bacterialResult==null)">style="display : none;"</s:if>><s:text name="garuda.cordimport.label.bactCultDate"></s:text>:</td>	
																	  <td width="20%" id="bactDate" <s:if test="(cdrCbuPojo.bacterialResult!=#request.bacterialNegative && cdrCbuPojo.bacterialResult!=#request.bacterialPositive) || (cdrCbuPojo.bacterialResult==null)">style="display : none;"</s:if>>
																			<s:date name="cdrCbuPojo.bactCultDate" id="datepicker6" format="MMM dd, yyyy" />
																			<s:textfield style="width:70%;" readonly="true" onkeydown="cancelBack();"  name="bactCultDateStr" id="datepicker6" value="%{datepicker6}" cssClass="datePicWMaxDate onChangeMandDate" onchange="isChangeDone(this,'%{datepicker6}','labChanges');"></s:textfield>
										 							  		<s:hidden value="%{datepicker6}" id="datepicker6_dbStoredVal"/>
					 												  		<span style="display:none" id="datepicker6_error" class="error"><br/><s:text name=""></s:text> </span>
										 							  </td>	
					  					   							  <td width="20%">
																		<s:set name="bactComment" value="cdrCbuPojo.bactComment" scope="request" />
																	   <!-- <s:property value="cdrCbuPojo.bactComment" default="value not fetched"/>-->
																		<s:if test ="#request.bactComment=='' || #request.bactComment == null">
																			<a href="#" onclick="textFunction('textArea1','textAreaForBact','comSave1','<s:property value="cdrCbuPojo.bactComment"/>')" id="com1" ><img height="15px" id="addBactCommentPlusPic" src="./images/addcomment.png"><span id="addBactComment"><s:text name="garuda.cbu.cordentry.addcomment"/></span></a>
																	    </s:if>
																		<s:else>
																			<a href="#" onclick="textFunction('textArea1','textAreaForBact','comSave1','<s:property value="cdrCbuPojo.bactComment"/>')" id="com3" ><img height="15px" id="addBactCommentPlusPic" src="./images/addcomment.png"><span id="addBactComment"><s:text name="garuda.cbufinalreview.label.flaggedItems.viewcomment"/></span></a>
																		</s:else>
																		<div id="textArea1" style="display: none;" >
																		<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
																		<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
																			<s:text name="garuda.label.dynamicform.commentbox" />
																		</div>
																		<table>
																			<tr>
																				<td colspan="2"> 
																					<s:textarea style="width:auto;" name="cdrCbuPojo.bactComment" cols="40" rows="5" id="textAreaForBact" onChange="isChangeDoneBactComment(this,'labChanges');"/> 
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<button type="button" onclick="commentSave('textArea1','textAreaForBact','addBactComment')" id="comSave1" ><s:text name="garuda.common.save"/></button>
																				</td>
																				<td>
																				    <button type="button" onclick="commentClose('textArea1','textAreaForBact')" id="comClose1" ><s:text name="garuda.common.close"/></button>
																				</td>
																			</tr>
																		</table></div>
																	</div>
																	 </td>			  						
							     	 							</tr>
							     	 				     	 	<tr>	<td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.fungcult"></s:text>:</td>
															           <s:hidden name="fungalCulpostive" id="fungalCulpostive" value="%{#request.fungalCulpostive}" />
															           <s:set name="fungalCulpostive1" value="%{#request.fungalCulpostive}" scope="request"/>
															            <s:hidden name="fungalCulnotdone" id="fungalCulnotdone" value="%{#request.fungalCulnotdone}" />
															           <s:set name="fungalCulnotdone1" value="%{#request.fungalCulnotdone}" scope="request"/>
															           <td width="20%"><s:select style="width:auto;" name="cdrCbuPojo.fungalResult" cssClass="labclass cbuLabSummMandatory" id="fungalResultModal" onchange="fungal(this.value);checkassessment1('Assessment_fungalCulture','Assessment_LabSummeryFung','showassesmentFung','hideassesmentFung',this.id,this.value);fungAutoDefer('fungalResultModal','fungDateText','fungDate','Assessment_LabSummeryFung');setDefBactFungDate('datepicker4','datepicker6','datepicker7','fungChng');onChngFldValdNDepFldVald(this.id,this.value,'datepicker7','bacterialNegativeFung','notReq');isChangeDone(this,'%{cdrCbuPojo.fungalResult}','labChanges');" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS]" listKey="pkCodeId" listValue="description"  headerKey="-1" headerValue="Select" ></s:select>
															           <s:hidden value="%{cdrCbuPojo.fungalResult}" id="fungalResultModal_dbStoredVal"/>
					 												  <br/><span style="display:none" id="fungalResultModal_error" class="error"><s:text name=""></s:text> </span>
															           </td>
														     	 	   <td width="20%" id="fungDateText" <s:if test="(cdrCbuPojo.fungalResult!=#request.bacterialNegativeFung && cdrCbuPojo.fungalResult!=#request.bacterialPositiveFung) || (cdrCbuPojo.fungalResult==null)">style="display : none;"</s:if>><s:text name="garuda.cordimport.label.fungCultDate"></s:text>:</td>	
																       <td width="20%" id="fungDate" <s:if test="(cdrCbuPojo.fungalResult!=#request.bacterialNegativeFung && cdrCbuPojo.fungalResult!=#request.bacterialPositiveFung) || (cdrCbuPojo.fungalResult==null)">style="display : none;"</s:if>>
																		 <s:date name="cdrCbuPojo.fungCultDate" id="datepicker7" format="MMM dd, yyyy" />
																		 <s:textfield style="width:70%;" onkeydown="cancelBack();"  name="fungCultDateStr" id="datepicker7" value="%{datepicker7}" cssClass="datePicWMaxDate onChangeMandDate" onchange="isChangeDone(this,'%{datepicker7}','labChanges');"></s:textfield>
																       	<s:hidden value="%{datepicker7}" id="datepicker7_dbStoredVal"/>
					 												  <br/><span style="display:none" id="datepicker7_error" class="error"><s:text name=""></s:text> </span>
																       </td>	
												                       <td width="20%">
																		<s:set name="fungComment" value="cdrCbuPojo.fungComment" scope="request" />
																	    <!--<s:property value="cdrCbuPojo.bactComment" default="default value"/>-->
																		<s:if test = "(#request.fungComment == '' || #request.fungComment == null)" >
																			<a href="#" onclick="textFunction('textArea2','textAreaForFung','comSave2','<s:property value="cdrCbuPojo.fungComment"/>')" id="com2" ><img height="15px" id="addFungCommentPlusPic" src="./images/addcomment.png"><span id="addFungComment"><s:text name="garuda.cbu.cordentry.addcomment"/></span></a>
																	    </s:if>
																		<s:else>
																			<a href="#" onclick="textFunction('textArea2','textAreaForFung','comSave2','<s:property value="cdrCbuPojo.fungComment"/>')" id="com4" ><img height="15px" id="addFungCommentPlusPic" src="./images/addcomment.png"><span id="addFungComment"><s:text name="garuda.cbufinalreview.label.flaggedItems.viewcomment"/></span></a>
																		</s:else>
																		<div id="textArea2" style="display: none;" >
																		<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
																		<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
																			<s:text name="garuda.label.dynamicform.commentbox" />
																		</div>
																		<table>
																			<tr>
																				<td colspan="2"> 
																					<s:textarea style="width:auto;" name="cdrCbuPojo.fungComment" cols="40" rows="5" id="textAreaForFung" onchange="isChangeDoneFungComment(this,'labChanges');"/> 
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<button type="button" onclick="commentSave('textArea2','textAreaForFung','addFungComment')" id="comSave2" ><s:text name="garuda.common.save"/></button>
																				</td>
																				<td>
																				    <button type="button" onclick="commentClose('textArea2','textAreaForFung')" id="comClose2" ><s:text name="garuda.common.close"/></button>
																				</td>
																			</tr>
																		</table></div>
																	</div>
																	</td>
											
					                       							<%-- <s:textfield name="cdrCbuPojo.fungComment"></s:textfield> --%>		  										  							
						              								 </tr>
						               									<tr>
														               		<td>
														               			<span id="showassesmentFung" class="LAB_SUM_Fung_showassess"><s:if test="(cdrCbuPojo.fungalResult==#request.fungalCulnotdone1) && (#request.assesDoneFrFung==true || #request.assesDoneFrFungForModal==true)"><a href="#"  id="showassesmentFungButton" onclick="showAssment('fungalResultModal','showassesmentFung','Assessment_fungalCulture','Assessment_LabSummeryFung','hideassesmentFung');"><s:text name="garuda.label.dynamicform.showassess"/></a></s:if></span>
														               			<div id="hideassesmentFung" style="display: none;" class="LAB_SUM_Fung_hideassess"><s:if test="(cdrCbuPojo.fungalResult==#request.fungalCulnotdone1)"><a href="#"  id="hideassesmentFungButton" onclick="hideAssment('Assessment_LabSummeryFung','showassesmentFung','hideassesmentFung');"><s:text name="garuda.label.dynamicform.hideassess"/></a></s:if></div>
														               		</td>
														               </tr>
																		<tr id="Assessment_LabSummeryFung"  style="display: none;"><td colspan="5">
															               	
															               	<fieldset><legend><s:text name="garuda.clinicalnote.label.assessment"></s:text>&nbsp;For&nbsp;<s:text name="garuda.cordentry.label.fungcult"/>
																				</legend><table>
																				<tr >
																					<td id="Assessment_fungalCulture"></td>
													
																		</tr></table></fieldset></td>
																</tr>
																<tr>	  
																	<td width="20%"><s:text name="garuda.cordentry.label.bloodtype"></s:text>:</td>
																      <td width="20%"><s:select style="width:auto;" name="cdrCbuPojo.aboBloodType" id="aboBloodTypeCri" cssClass="labclass cbuLabSummMandatory" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BLOOD_GROUP]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select Type" onChange="onChangeProcnCountFldVald(this.id,this.value);isChangeDoneABO(this,'labChanges');"></s:select>
																      <s:hidden value="%{cdrCbuPojo.aboBloodType}" id="aboBloodTypeCri_dbStoredVal"/>
					 												  <br/><span style="display:none" id="aboBloodTypeCri_error" class="error"><s:text name=""></s:text> </span>
																      </td>
																	      
																		 <td width="20%"><s:text name="garuda.cordentry.label.rhtype"></s:text>:</td>
																	     <td width="20%">
																	     <s:select style="width:auto;" name="cdrCbuPojo.rhType" id="cdrcburhtype" cssClass="labclass cbuLabSummMandatory" 
																	     list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@RH_TYPE]" 
																	     listKey="pkCodeId" onchange="rhSpecify(this.value);onChngFldValdNDepFldVald(this.id,this.value,'specRhTypeOther','rhSpecOtherPk','notReq');
																	     isChangeDone(this,'%{cdrCbuPojo.rhType}','labChanges');" listValue="description" headerKey="-1"  headerValue="Select Type"></s:select>
																	     <s:hidden value="%{cdrCbuPojo.rhType}" id="cdrcburhtype_dbStoredVal"/>
					 												  <br/><span style="display:none" id="cdrcburhtype_error" class="error"><s:text name=""></s:text> </span>
																	     </td>
																		 <td width="60%" colspan="3">
																			<div id="rhothspec" style="display: none;">
																				<table>
																				     <tr>
																					    <td><s:text name="garuda.cbbprocedures.label.specifyother"></s:text>:</td>
																					     <td>
																						   <s:textfield style="width:auto;" name="cdrCbuPojo.specRhTypeOther" id="specRhTypeOther" maxLength="30" onkeyup="chkDepOnChange(this.id,this.value)" onchange="isChangeDone(this,'%{cdrCbuPojo.specRhTypeOther}','labChanges');"></s:textfield>
																							<s:hidden value="%{cdrCbuPojo.specRhTypeOther}" id="specRhTypeOther_dbStoredVal"/>
					 												  						<br/><span style="display:none" id="specRhTypeOther_error" class="error"><s:text name=""></s:text> </span>
																						</td>
																					 </tr>
																					</table>
																				</div>
									     								  </td>
					             									</tr>
																  <tr>
 																	  <td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.hemoglobinScrn"></s:text>:</td>
															          <s:hidden name="alphaThalismiaTraitPkVal" id="alphaThalismiaTraitPkVal" value="%{#request.alphaThalismiaTraitPkVal}" />
															          <s:set name="alphaThalismiaTraitPkVal1" value="%{#request.alphaThalismiaTraitPkVal}" scope="request"/>
															          <s:hidden name="hemozygousNoDiesasePkVal" id="hemozygousNoDiesasePkVal" value="%{#request.hemozygousNoDiesasePkVal}" />
															          <s:set name="hemozygousNoDiesasePkVal1" value="%{#request.hemozygousNoDiesasePkVal}" scope="request"/>
															          <s:hidden name="hemoAlphaThalassmiaPkVal" id="hemoAlphaThalassmiaPkVal" value="%{#request.hemoAlphaThalassmiaPkVal}" />
															          <s:set name="hemoAlphaThalassmiaPkVal1" value="%{#request.hemoAlphaThalassmiaPkVal}" scope="request"/>
															          <s:hidden name="hemoTraitPkVal" id="hemoTraitPkVal" value="%{#request.hemoTraitPkVal}" />
															          <s:set name="hemoTraitPkVal1" value="%{#request.hemoTraitPkVal}" scope="request"/>
															          <s:hidden name="hemozygousNoDonePkVal" id="hemozygousNoDonePkVal" value="%{#request.hemozygousNoDonePkVal}" />
															          <s:set name="hemozygousNoDonePkVal1" value="%{#request.hemozygousNoDonePkVal}" scope="request"/>
															          <s:hidden name="hemozygousUnknownPkVal" id="hemozygousUnknownPkVal" value="%{#request.hemozygousUnknownPkVal}" />
															          <s:set name="hemozygousUnknownPkVal1" value="%{#request.hemozygousUnknownPkVal}" scope="request"/>
															          <s:hidden name="hemoMultiTraitPkVal" id="hemoMultiTraitPkVal" value="%{#request.hemoMultiTraitPkVal}" />
																	  <s:set name="hemoMultiTraitPkVal1" value="%{#request.hemoMultiTraitPkVal}" scope="request"/>
															          <td width="20%">
															          <s:select style="width:auto;" name="cdrCbuPojo.hemoglobinScrn" id="hemoglobinScrnTest" 
															          cssClass="labclas cbuLabSummMandatory" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN]" 
															          listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"
															          onChange="checkassessment1('Assessment_Hemoglobinopathy','Assessment_LabSummeryHemo','showassessbutton','hideassesmentHemo',this.id,this.value);
															           onChangeProcnCountFldVald(this.id,this.value);isChangeDoneHemo(this,'labChanges');autoDeferHemoGloScrn(this.value,this.id);"></s:select>
															          <s:hidden value="%{cdrCbuPojo.hemoglobinScrn}" id="hemoglobinScrnTest_dbStoredVal"/>
					 												  <br/><span style="display:none" id="hemoglobinScrnTest_error" class="error"><s:text name=""></s:text> </span>
															          </td>
													   			       <td width="20%"></td>
													   			      
													          	 </tr>     
													          	 <tr><td>
													             	<span id="showassessbutton" class="LAB_SUM_Hemo_showassess"><s:if test="(cdrCbuPojo.hemoglobinScrn==#request.alphaThalismiaTraitPkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDonePkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousUnknownPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDiesasePkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoAlphaThalassmiaPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoTraitPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoMultiTraitPkVal1) && (#request.assesDoneFrHemo==true || #request.assesDoneFrHemoForModal==true)"><br><a href="#"  id="showassesmentHemoButton" onclick="showAssment('hemoglobinScrnTest','showassessbutton','Assessment_Hemoglobinopathy','Assessment_LabSummeryHemo','hideassesmentHemo');"><s:text name="garuda.label.dynamicform.showassess"/></a></s:if></span>
													             	<div id="hideassesmentHemo" style="display: none;" class="LAB_SUM_Hemo_hideassess"><s:if test="cdrCbuPojo.hemoglobinScrn==#request.alphaThalismiaTraitPkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDonePkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousUnknownPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDiesasePkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoAlphaThalassmiaPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoTraitPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoMultiTraitPkVal1"><a href="#"  id="hideassesmentHemoButton" onclick="hideAssment('Assessment_LabSummeryHemo','showassessbutton','hideassesmentHemo');"><s:text name="garuda.label.dynamicform.hideassess"/></a></s:if></div>
													             	</td>
													             </tr>
										             			<tr id="Assessment_LabSummeryHemo"  style="display: none;"><td colspan="5">
															          	<fieldset><legend><s:text name="garuda.clinicalnote.label.assessment"></s:text>&nbsp;For&nbsp;<s:text name="garuda.cordentry.label.hemoglobinScrn"/>
																					</legend><table>
																					<tr >
																			<td colspan="5" id="Assessment_Hemoglobinopathy"></td>
																			</tr></table></fieldset></td>
																			<td></td>
																		</tr>	    
								    					           						       
	                      										</table>
	                      									</td>
	                      								</tr>
	                      							</table></div>
																<div id="labSummeryDivR1">
																	<jsp:include page="cb_comp-reqinfo-lab-Tests.jsp"
																	flush="true"></jsp:include></div>
																	
	<s:if test="%{flagisMinimumCri=='true'}">
		<div style="overflow:auto;">
			<div id="labsummaryMininotecontent" onclick="toggleDiv('labsummaryMininote')"
				class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				style="text-align: center;"><span
				class="ui-icon ui-icon-triangle-1-s"></span> <s:text name="garuda.cdrcbuview.label.button.labsummclinicalnotes" /></div>				
			<div id="labsummaryMininote">
				<div id="loadLabSummaryClinicalNoteDiv">
				<s:set name="cbuCateg" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@CLINIC_CATEGORY,@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_NOTE_CATEGORY_CODESUBTYPE) " />
					<jsp:include page="../modal/cb_load_clinicalNote.jsp"></jsp:include>
				</div>
			</div>
		</div>
		<div id="loadLabSummaryDocMinInfoDiv">
			<s:if test="(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE].size>0 && hasViewPermission(#request.viewLabSumCat)==true)||(#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size > 0 && hasViewPermission(#request.viewUnitRepCat)==true)">
           	<div id="loadLabSummaryDocMinContent" onclick="toggleDiv('loadLabSummaryDocMinInfo')"
			     class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
				 style="text-align: center;">
				<span class="ui-icon ui-icon-triangle-1-s"></span> 
				<s:text name="garuda.uploaddoc.label.button.labsummryuploaddocument" />
			</div>	
			<div id="loadLabSummaryDocMinInfo">
				<s:if test=" #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE]!=null && #request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE].size>0 ||#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE]!=null ||#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@UNIT_REPORT_CODESUBTYPE].size>0">		
					 <s:if test="hasViewPermission(#request.viewLabSumCat)==true">
					 <s:set name="cbuDocCateg" value="@com.velos.ordercomponent.util.VelosGarudaConstants@LAB_SUMMARY_CODESUBTYPE"  scope="request" />
					 </s:if>
					 <jsp:include page="../modal/cb_load_upload_document.jsp">
					 	<jsp:param value="<%=request.getAttribute(\"cbuDocCateg\")%>" name="cbuDocCateg"/>
					 </jsp:include>	 
				</s:if>
			</div>
			</s:if>
		</div>
	</s:if>
				              <!--<div>
						  	
						  </div>	
				
			-->
		
	
	<div id="esignForLabSummary" style="display: none;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="labSummarySubmit" bgcolor="#cccccc">
			 <tr valign="baseline" bgcolor="#cccccc">
			   <td><jsp:include page="../cb_esignature.jsp" flush="true">
	   					<jsp:param value="labSummarysubmit" name="submitId"/>
						<jsp:param value="labSummaryinvalid" name="invalid" />
						<jsp:param value="labSummaryminimum" name="minimum" />
						<jsp:param value="labSummarypass" name="pass" />
   					</jsp:include>
   				</td>	
			<td align="center" colspan="2">
			   <input type="button" disabled="disabled" id="labSummarysubmit" onclick="submitTableLabSummary('submitButtonAutoDefer=false')"  value="<s:text name="garuda.common.lable.confirm"/>" />
			   <input type="button"  onclick="closeModalscompreq('labSummaryForComplete');" value="<s:text name="garuda.openorder.label.button.cancel"/>" />
			   </td>	
			</tr>
			</table>
	</div>
	</div>
	</div>
	<script>
	var dialogheight = $j('#dialogForCRI').height();
	$j("#labSumTargetDiv").height(dialogheight - 130);
	</script>
<script>
$j(function(){
	rhSpecify($j('#cdrcburhtype').val());
	$j('.labsumHidDiv').hide();
});
</script>
<script>
function submitTableLabSummary(autoDeferSubmit){
	
	var labSummChangesTemp = divChanges('labChanges',$j('#labSumm_bodyDiv').html());
	if(!labSummChangesTemp){
		labSummChangesTemp = divChanges('assessmentChngCls',$j('#labSumm_bodyDiv').html());
	}
	
      if(autoDeferStatus(autoDeferSubmit, labSummChangesTemp)){ 
	  var url="saveTestWithFields?cordId="+'<s:property value="cdrCbuPojo.cordID"/>'+"&"+autoDeferSubmit+'&criChangeFlag='+true;
	  var noMandatory=true;
   		$j('#labSummaryModal .cbuLabSummMandatory').each(function(){
   			 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
   				 noMandatory=false;
   				}
   			  });
   		if(noMandatory){
			var len = $j('#labSummaryModal .addAssessmentMandatory').length;
			if(len != 0){
				noMandatory = false;
			}
		}	
		var validTime = checkTimeFormatForCord('cordProcessingTime');
		 if(validTime){
          validTime = checkTimeFormatForCord('cordFreezeTime');
		 }
   		var flag=validateThawOtherRow('labSummaryForm');
		  var rangeFlag=true;
		  var dateFlag=true;
		 $j(".onChngMand").each(function() {
			  rangeFlag=false;
			});
		  $j(".onChngRangeChk").each(function() {
			  rangeFlag=false;
			}); 
		  $j(".onChangeMandDate").each(function() {
			  dateFlag=chkDateOnChange(this.id,this.value);
			  if(dateFlag==false){
				  flag=false;
			  }
		});
		  $j(".onChngDepMand").each(function() {
			  dateFlag=chkDepOnChange(this.id,this.value);
			  if(dateFlag==false){
				  flag=false;
			  }
		});
			var labSummChanges = divChanges('labChanges',$j('#labSumm_bodyDiv').html());
			if(!labSummChanges){
				labSummChanges = divChanges('assessmentChngCls',$j('#labSumm_bodyDiv').html());
			}
		  if(validTime==true && flag==true && rangeFlag==true && setMinDateWidRespCollDat() && validateHours('collectionDateId','collectionTimeId','datepicker5','cordFreezeTime','frzTimeRangeErrorMsg') && validateDateData('datepicker4','cordProcessingTime','datepicker5','cordFreezeTime','frzdateAftprcStart','frzTimeRangebfProcssTime') && $j("#labSummaryForm").valid()/* && $j("#autodeferfieldflag").val()=='false'*/){
			  if(labSummChanges==true){
			 url = url+"&esignFlag=review&noMandatory="+noMandatory+'&criChangeFlag='+true;
		      modalFormPageRequest('labSummaryForm',url,'finalreviewmodel',$j("#cordId").val(),$j("#orderId").val(),$j("#orderType").val(),'successLab','labSummaryModal');
			  }
			  else if(updateStatusCriDiv){
				  showSuccessdiv('successLab','labSumm_bodyDiv');
				  }
		  }
		}
	}
	function chkDepOnChange(fieldId,fieldValue){
		if(fieldValue!=''){
			$j("#"+fieldId+"_error").hide();
			$j("#"+fieldId).removeClass("onChngDepMand");
			return true;
		}
		else{
			$j("#"+fieldId+"_error").show();
			$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
			return false;
		}
	}
	function chkDateOnChange(fieldId,fieldValue){
		
		 if($j('#'+fieldId).is(':visible')){
		
				if(fieldValue==$j("#"+fieldId+"_dbStoredVal").val() || fieldValue!=''){
					$j("#"+fieldId+"_error").hide();
					return true;
				}
				else{
					$j("#"+fieldId+"_error").show();
					$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
					return false;
				}
		
		 }
		  
			return true;// field is hidden so skip validation 
	}
	
	function modalFormPageRequest(formid,url,divname,cordid,orderid,ordertype,updateDivId,statusDivId){
		showprogressMgs();
		$j.ajax({
	        type: "POST",
	        url: url,
	       // async:false,
	        data : $j("#"+formid).serialize(),
	        success: function (result){
	        	$j('.ui-datepicker').html("");
	        	//$('.tabledisplay').html("");
	            var $response=$j(result);
	            //query the jq object for the values
	            var errorpage = $response.find('#errorForm').html();
	           // alert(oneval);
	           // var subval = $response.find('#sub').text();
	            if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{
	            	if(divname!=""){ 
		            	 $j("#"+divname).html(result);
		            }
	            	if(updateDivId!=undefined && updateDivId!=""){
	            		$j("#"+updateDivId).css('display','block');			            	
	            	}else{
	            		$j("#update").css('display','block');			            	
	            	}
	            	if(statusDivId!=undefined && statusDivId!=""){
	            		$j("#"+statusDivId).css('display','none');			            	
	            	}else{
	            		$j("#status").css('display','none');			            	
	            	}
	            	/*if(orderid!=null && orderid!="" && typeof(orderid)!="undefined" && ordertype!=null && ordertype!="" && typeof(ordertype)!="undefined"){
	                	var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID"/>&orderId='+orderid+'&orderType='+ordertype+'&pkcordId=<s:property value="cdrCbuPojo.cordID"/>&currentReqFlag=true';
	            		loadMoredivs(urlb,'viewcbutoppanel,quicklinksDiv,taskCompleteFlagsDiv,ctReqClinInfoContentDiv,finalreviewcontentDiv,currentRequestProgressMainDiv','cbuProcedureMain','update','status');//url,divname,formId,updateDiv,statusDiv
	            		setpfprogressbar();
	            		getprogressbarcolor();
	                }
	            	else{
	            	
	            	}*/		 
	            	var urla = "loadClinicalData?cdrCbuPojo.cordID="+cordid;
	            	loadPageByGetRequset(urla,'cordSelectedData');
	            }
	            closeprogressMsg();
	        },
	        error: function (request, status, error) {
	        	closeprogressMsg();
	        	alert("Error " + error);
	            alert(request.responseText);
	        }

		});	
		}

	function autoDeferHemoGloScrn(Val,Id){
		elementValue = autoDeferField5;
		var cordId =<s:property value ="cdrCbuPojo.cordID"/>;
		var url="saveTestWithFields?cordId="+'<s:property value="cdrCbuPojo.cordID"/>';
		if(Val==$j("#hemoBeatThal").val() || Val==$j("#hemoSickBeta").val() || Val==$j("#hemoSickCell").val() || Val==$j("#hemoAlphaSev").val()){
		      jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
	      function(r) {
				if (r == false) {
					$j("#"+Id).removeClass('labChanges');									 
				   $j("#"+Id+" option[value="+elementValue+"]").attr("selected", "selected");
				   $j('#'+Id).focus();			   
				   if(elementValue==$j("#alphaThalismiaTraitPkVal").val() || elementValue==$j("#hemozygousNoDiesasePkVal").val() || elementValue==$j("#hemoAlphaThalassmiaPkVal").val() || elementValue==$j("#hemoTraitPkVal").val() || elementValue==$j("#hemozygousNoDonePkVal").val() || elementValue==$j("#hemozygousUnknownPkVal").val() || elementValue==$j("#hemoMultiTraitPkVal").val()){
					   $j("#showassessbutton").show();
					   $j("#hideassesmentHemo").hide();
				   }
				  }else if(r == true){
					  autoDefer = true;
					  mutex_Lock = true;
					  element_Id = Id;
					  element_Name='cdrCbuPojo.hemoglobinScrn';
					  autoDeferFormSubmitParam[0]= 'labSummaryForm';
				      autoDeferFormSubmitParam[1]= url;
					      autoDeferFormSubmitParam[2]= 'finalreviewmodel';
						       autoDeferFormSubmitParam[3]= $j("#cordId").val();
							       autoDeferFormSubmitParam[4]= $j("#orderId").val();
								       autoDeferFormSubmitParam[5]= $j("#orderType").val();
								 	       autoDeferFormSubmitParam[6]= 'successLab';
										       autoDeferFormSubmitParam[7]= 'labSummaryModal';
			   refreshDiv_Id[0]='defer';
			   refreshDiv_Id[1]='status3';	
			   commonMethodForSaveAutoDefer();
			   if(elementValue==$j("#alphaThalismiaTraitPkVal").val() || elementValue==$j("#hemozygousNoDiesasePkVal").val() || elementValue==$j("#hemoAlphaThalassmiaPkVal").val() || elementValue==$j("#hemoTraitPkVal").val() || elementValue==$j("#hemozygousNoDonePkVal").val() || elementValue==$j("#hemozygousUnknownPkVal").val() || elementValue==$j("#hemoMultiTraitPkVal").val()){
					  assessLinknFlag_Id[0]= 'true';
					  assessLinknFlag_Id[1]= 'LAB_SUM_Hemo_showassess';
					  assessLinknFlag_Id[2]= 'LAB_SUM_Hemo_hideassess';
				   }
				  }
				});
		   }
	}
	function bactAutoDefer(Val,Id,datetextId,datePicId,dateId){
		elementValue = autoDeferField1;
		//alert("Val :"+Val);
		elementValue = autoDeferField1;
		//alert($j("#"+datetextId).val());
		var cordId =<s:property value ="cdrCbuPojo.cordID"/>;
		var url="saveTestWithFields?cordId="+'<s:property value="cdrCbuPojo.cordID"/>';	
		if (Val==$j("#bacterialPositive").val() || Val ==$j("#bacterialNotDone").val()){
			jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
					function(r) {
						if (r == false) {
						   $j("#"+Id).removeClass('labChanges');
						   $j("#"+Id+" option[value="+elementValue+"]").attr("selected", "selected");
						   $j("#"+Id).focus();					       
						   $j("#"+datetextId).hide();
						   $j("#"+dateId).hide();
						   $j("#"+datePicId).hide();
						   bacterial(elementValue);
						   	}else if(r == true){
							  autoDefer = true;
							  mutex_Lock = true;
							  element_Id = Id;
							  element_Name = 'cdrCbuPojo.bacterialResult';
							  autoDeferFormSubmitParam[0]= 'labSummaryForm';
								  autoDeferFormSubmitParam[1]= url;
									  autoDeferFormSubmitParam[2]= 'finalreviewmodel';
										  autoDeferFormSubmitParam[3]= $j("#cordId").val();
											  autoDeferFormSubmitParam[4]= $j("#orderId").val();
												  autoDeferFormSubmitParam[5]= $j("#orderType").val();
													  autoDeferFormSubmitParam[6]= 'successLab';
														  autoDeferFormSubmitParam[7]= 'labSummaryModal';
							   refreshDiv_Id[0]='defer';
							       refreshDiv_Id[1]='status3';	
							   commonMethodForSaveAutoDefer();    						  
							  //modalFormPageRequest('labSummaryForm',url,'finalreviewmodel',$j("#cordId").val(),$j("#orderId").val(),$j("#orderType").val(),'successLab','labSummaryModal');
							  //$j("#defer").css('display','block');
							  //$j("#status3").css('display','none');	
							  }
						});
			}
		}

	function fungAutoDefer(Id,datetextId,dateId,assessId){
		elementValue = autoDeferField2;
		var cordId =<s:property value ="cdrCbuPojo.cordID"/>;
		var url="saveTestWithFields?cordId="+'<s:property value="cdrCbuPojo.cordID"/>';
		  if ($j("#"+Id).val()==$j("#fungalCulpostive").val()){
			  jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
						function(r) {
							if (r == false) {
								$j("#"+Id).removeClass('labChanges');
							   $j("#"+Id+" option[value="+elementValue+"]").attr("selected", "selected");
							   $j("#"+Id).focus();
							   $j("#"+datetextId).hide();
							   $j("#"+dateId).hide();
							   $j("#"+assessId).hide();
							   fungal(elementValue);
							   if(elementValue==$j("#fungalCulnotdone").val()){
								   $j("#showassesmentFung").show();
								   $j("#hideassesmentFung").hide();
							   }
							  }else if(r == true){
								  autoDefer = true;
								  mutex_Lock = true;
								  element_Id = Id;
								  element_Name = 'cdrCbuPojo.fungalResult';
								  autoDeferFormSubmitParam[0]= 'labSummaryForm';
								      autoDeferFormSubmitParam[1]= url;
									      autoDeferFormSubmitParam[2]= 'finalreviewmodel';
										       autoDeferFormSubmitParam[3]= $j("#cordId").val();
											       autoDeferFormSubmitParam[4]= $j("#orderId").val();
												       autoDeferFormSubmitParam[5]= $j("#orderType").val();
												 	       autoDeferFormSubmitParam[6]= 'successLab';
														       autoDeferFormSubmitParam[7]= 'labSummaryModal';
							   refreshDiv_Id[0]='defer';
							       refreshDiv_Id[1]='status3';	
							   commonMethodForSaveAutoDefer(); 
							   if(elementValue==$j("#fungalCulnotdone").val()){
									  assessLinknFlag_Id[0]= 'true';
									  assessLinknFlag_Id[1]= 'LAB_SUM_Fung_showassess';
									  assessLinknFlag_Id[2]= 'LAB_SUM_Fung_hideassess';
								   }
								 // modalFormPageRequest('labSummaryForm',url,'finalreviewmodel',$j("#cordId").val(),$j("#orderId").val(),$j("#orderType").val(),'successLab','labSummaryModal');
								 // $j("#defer").css('display','block');
								 // $j("#status3").css('display','none');	
								  }
			});
		  }
		}


	function autoDeferViab(Val,Id){
		elementValue = autoDeferField4;
		var cordId =<s:property value ="cdrCbuPojo.cordID"/>;
		var url="saveTestWithFields?cordId="+'<s:property value="cdrCbuPojo.cordID"/>';
		if( Val !='' && Val>=0 && Val<85 ){
		//alert("ID"+Id);alert($j("#"+Id).val());
			jConfirm('Response you entered would cause CBU to be automatically status as permanently medically deferred. Please confirm if you would like to proceed', '<s:text name="garuda.common.lable.confirm"/>',
				function(r) {
					if (r == false) {
						$j("#"+Id).removeClass('labChanges');
					   $j("#"+Id).val(elementValue);
					   $j('#'+Id).focus();
					  }else if(r == true){
							  autoDefer = true;
							  mutex_Lock = true;
							  element_Id = "";
							  element_Name = 'savePostTestList[13].testresult';
							  autoDeferFormSubmitParam[0]= 'labSummaryForm';
					              autoDeferFormSubmitParam[1]= url;
						              autoDeferFormSubmitParam[2]= 'finalreviewmodel';
							              autoDeferFormSubmitParam[3]= $j("#cordId").val();
								              autoDeferFormSubmitParam[4]= $j("#orderId").val();
									              autoDeferFormSubmitParam[5]= $j("#orderType").val();
									 	              autoDeferFormSubmitParam[6]= 'successLab';
											              autoDeferFormSubmitParam[7]= 'labSummaryModal';
						  commonMethodForSaveAutoDefer();
						  refreshDiv_Id.length=0;				          
							  //modalFormPageRequest('labSummaryForm',url,'finalreviewmodel',$j("#cordId").val(),$j("#orderId").val(),$j("#orderType").val(),'successLab','labSummaryModal');
					
						  }
				});
			  }
		  }

	function autoDeferCFU(Val,Id){
		elementValue = autoDeferField3;
		var cordId =<s:property value ="cdrCbuPojo.cordID"/>;
		var url="saveTestWithFields?cordId="+'<s:property value="cdrCbuPojo.cordID"/>';
		if(Val !=''&& Val==0){
			//alert("ID"+Id);alert($j("#"+Id).val());
			jConfirm('Response you entered would cause CBU to be automatically status as permanently medically deferred. Please confirm if you would like to proceed', '<s:text name="garuda.common.lable.confirm"/>',
				function(r) {
					if (r == false) {
						$j("#"+Id).removeClass('labChanges');
					   $j("#"+Id).val(elementValue);
					   $j('#'+Id).focus();
					  }else if(r == true){
							  autoDefer = true;
							  mutex_Lock = true;
							  element_Id = "";
							  element_Name = 'savePostTestList[12].testresult';
							  autoDeferFormSubmitParam[0]= 'labSummaryForm';
						          autoDeferFormSubmitParam[1]= url;
							          autoDeferFormSubmitParam[2]= 'finalreviewmodel';
								          autoDeferFormSubmitParam[3]= $j("#cordId").val();
									          autoDeferFormSubmitParam[4]= $j("#orderId").val();
										          autoDeferFormSubmitParam[5]= $j("#orderType").val();
										 	          autoDeferFormSubmitParam[6]= 'successLab';
												          autoDeferFormSubmitParam[7]= 'labSummaryModal';
							  commonMethodForSaveAutoDefer();
							  refreshDiv_Id.length=0;				          
							  //modalFormPageRequest('labSummaryForm',url,'finalreviewmodel',$j("#cordId").val(),$j("#orderId").val(),$j("#orderType").val(),'successLab','labSummaryModal');
					
						  }
				});
			  }
		  }
	function onChngFldValdNDepFldVald(fieldId,fieldValue,depFieldId,depOnValueId,depOnFieldId){
		if(fieldValue == -1 && fieldValue != $j("#"+fieldId+"_dbStoredVal").val()){
			$j("#"+fieldId+"_error").show();
			$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
			$j("#"+fieldId).addClass("onChngMand");
		}
		else{
			if(fieldValue==$j("#"+depOnValueId).val() && $j("#"+depFieldId).val()==''){
				$j("#"+fieldId+"_error").show();
				$j("#"+depFieldId).addClass("onChngDepMand");
			}
			else{
				$j("#"+depFieldId).removeClass("onChngDepMand");
			}
			$j("#"+fieldId+"_error").hide();
			$j("#"+fieldId).removeClass("onChngMand");
		}
		if(depOnFieldId!='notReq' && $j("#"+depOnFieldId).val()==''){
			$j("#"+fieldId+"_error").hide();
			$j("#"+fieldId).removeClass("onChngMand");
		}
	}
	$j(function(){
		autoDeferField1=$j('#bacterialResult').val();
		autoDeferField2=$j('#fungalResultModal.labclass').val();
		autoDeferField3=$j('#savePostTestList12testresult').val();
		autoDeferField4=$j('#savePostTestList13testresult').val();		
		autoDeferField5=$j('#hemoglobinScrnTest').val();
		});
	function setMinDateWidRespCollDat(){
		  var FreezeDate=new Date($j("#datepicker5").val());
		  var ProcessingDate=new Date($j("#datepicker4").val());
		  var flag = true;
		  var procStartDt = new Date($j("#procStartDtinLabSum").val());
		  var procTermiDt = new Date($j("#procTermiDtinLabSum").val());
		  
		  if (ProcessingDate < procStartDt || ProcessingDate > procTermiDt) {
			  $j("#dynaErrorPrcngDt").show();
			  flag = false;
		  }
		  else
		  {
			  $j("#dynaErrorPrcngDt").hide();
		  }

		  if (FreezeDate < procStartDt || FreezeDate > procTermiDt) {
			  $j("#dynaErrorFreezeDt").show();
			  flag = false;
		  }
		  else
		  {
			  $j("#dynaErrorFreezeDt").hide();
		  }
		  return flag;
	  }
	  function isChangeDoneABO(thisVal,classname){
	  isChangeDone(thisVal,'<s:property value="cdrCbuPojo.aboBloodType" />',classname);
	  }
	  function isChangeDoneHemo(thisVal,classname){
	  isChangeDone(thisVal,'<s:property value="cdrCbuPojo.hemoglobinScrn"/>',classname);
	  }
	  function isChangeDoneBactComment(thisVal,classname){
	  isChangeDone(thisVal,'<s:property value="cdrCbuPojo.bactComment"/>',classname);
	  }
	  function isChangeDoneFungComment(thisVal,classname){
	  isChangeDone(thisVal,'<s:property value="cdrCbuPojo.fungComment"/>',classname);
	  }
	  
	  $j('[name="cordCbuStatus"]').val($j('#cordCbuStatus').val());
	  $j('#executeAutoDeferFlagID').val('labSumaryAutoDeferExecFlag');
	  
	</script>
	<div id="addTestDialog"></div>