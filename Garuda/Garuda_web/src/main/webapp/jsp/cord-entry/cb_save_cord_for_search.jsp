<%@ taglib prefix="s"  uri="/struts-tags"%>
 <table>
    <tr>
      <td align='center'><strong><s:property value="#request.updateCords"/> cords out of <s:property value="#request.totalCords"/> are submitted!</strong></td>
    </tr>
    <s:if test="#request.errorCords>0">
       <tr>
	      <td align='center'><strong><s:property value="#request.errorCords"/> out of <s:property value="#request.totalCords"/> has an Error please check the save in progress cords!</strong></td>
	   </tr>
    </s:if>
    <tr>
      <td align='center'>
          <input type="button" value="Ok" onclick="location.href='cordEntryInProgress.action'" />
      </td>
    </tr>
 </table>
