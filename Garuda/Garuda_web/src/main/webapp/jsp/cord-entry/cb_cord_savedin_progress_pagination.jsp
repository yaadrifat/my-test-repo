 <%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="userLB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants"%>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<% String contextpath=request.getContextPath();%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<script>
$j(function(){
	checkeligibleForSubmit();
});

var completeSearchReq = false;
var cordsCount = 0;
var totalProcessedCords = 0;

function validateIscordSubmitted(cordId){
	  var dbValue = false;
	  $j.ajax({
			type : "GET",
			async : false,
			url : 'getDeferStatus?cdrCbuPojo.cordID='+cordId,
			success : function(result) {
				dbValue = result.submitCord;
				if(dbValue){
					jConfirm('<s:text name="garuda.cbu.cordentry.alreadysubmitted"/>', '<s:text name="garuda.common.lable.cancel"/>',
							function(r) {
								if (r == true){
									  window.location.href =  "cordEntryInProgress";
								}
				 });
				}else{
					submitpost('openWholeCordEntry',{'cdrCbuPojo.cordID':cordId});
				}									
			}			
	 });		  
  
}

function validateIscordSubmitted(){
	  var dbValue = false;
	  $j.ajax({
			type : "GET",
			async : false,
			url : 'getDeferStatus?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />',
			success : function(result) {
				dbValue = result.submitCord;									
			}			
	 }); 
   return dbValue;
}

function checkeligibleForSubmit(){
	var eligibleForSubmitLen = $j('.eligibleForSubmit').length;
	if(eligibleForSubmitLen == 0){
		$j('#selectAllButton').attr('disabled',true);
		$j('#selectAll').attr('disabled',true);
	}
	else
		{
		$j('#selectAllButton').attr('disabled',false);
		$j('#selectAll').attr('disabled',false);
		}
}
function constructSavedinProgTable() {

	var showEntries_value= $j('#sip_tbl_se').val();
	//var showEntries_val='Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;" ><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="paginateSearch.getiTotalRows()"/>">ALL</option></select> Entries';
	var showEntries_val='Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;" ><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries';
	var lastSort=$j('#sip_sortParam').val();
	var pageNo=$j('#paginateWrapper2').find('.currentPage').text();
	var sort="0";
	var sortDir="asc";
	
	if(showEntries_value==null || showEntries_value=="" || showEntries_value=="undefined"){
		showEntries_value=5;
	}
	
	if(lastSort!=undefined && lastSort!=null && lastSort!=""){
		sort=$j('#sip_sortParam').val();
		sortDir=$j('#sip_sort_type').val();
	}
	
	
	if(showEntries_value=="ALL"){
		
		
		var oTable =  $j('#searchResults').dataTable({
			"sScrollY": "150", 
	        "sDom": "frtiS",
	        "sAjaxSource": 'getJsonPagination.action?TblFind=notAvailCBUs&isSavedInProgress=1&allEntries=ALL'+$j('#sipfilteparams').val(),
	        "bServerSide": true,
	        "bProcessing": true,
	        "bRetrieve" : true,
	        "bDestroy": true,
	        "bDeferRender": true,
	        "bAutoWidth": false,
	        "aaSorting": [[ sort, sortDir ]],
	        "bSortCellsTop": true,
	        "sAjaxDataProp": "aaData",
	        "oLanguage": {
				"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
			},
	        "aoColumnDefs": [
							  {
	    						   "aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
	        						   	var str = "";
	        						   	if(source[2]<100){
	        						   		str ="<input type='checkbox' class='selCordsForSubmitClass' id='selCordsForSubmit' name='selCordsForSubmit' onclick='enableSubmitCBUForSearch(this)' value='"+source[0]+"' disabled='disabled'/>";
	            						}else{
	        						   		str ="<input type='checkbox' class='selCordsForSubmitClass eligibleForSubmit' id='selCordsForSubmit' name='selCordsForSubmit' onclick='enableSubmitCBUForSearch(this)' value='"+source[0]+"'/>";
	            						}
							        	return str;
							   }}, 
	                          {
	                              "aTargets": [1], "mDataProp": function ( source, type, val ) {
	                                 return source[3];
	                             }},
	                            {
	                                    "aTargets": [2], "mDataProp": function ( source, type, val ) {
	                                        var str = "<div style='display:none'>"+removeSymbolfromId(source[1])+"</div>";
	                                        	str = "<u onclick=\"submitpost('openWholeCordEntry',{'cdrCbuPojo.cordID':'"+source[0]+"'});\" style='cursor: pointer;'>"+source[1]+"</u>";
	                                     	return str;
	                             }},
	                          	{
	                                 "aTargets": [3], "mDataProp": function ( source, type, val ) {
	                                    return source[2];
	                             }}
	                        ],
	        "fnDrawCallback": function() {
	                            $j('#searchResults_wrapper').find('.dataTables_scrollFoot').hide();
	                            $j('#searchResults_info').show();
	                            $j("#showsRow option:contains('ALL')").attr('selected', 'selected');
	                            $j('#searchResults_wrapper').find('.dataTables_scrollHeadInner').find('table').css('position','');
	                            sipSort();
	                            var footerObj = $j("#searchResults_wrapper").find(".dataTables_scrollFoot");
	                            $j(footerObj).css({'display':'block'});
	                            checkeligibleForSubmit();
	                            showStatusFilterText('sipStatFilterFlagTxt','sipStatFilterFlag');
	        				}
	    });
		$j('#searchResults_filter').before(showEntries_val);
	    $j('#notavilTableFooter').hide();
		
	}else{
		
		var oTable =  $j('#searchResults').dataTable({
			"sScrollY": "150", 
	        "sAjaxSource": 'getJsonPagination.action?TblFind=notAvailCBUs&otherThanAll=1&isSavedInProgress=1&allEntries=ALL&iShowRows='+showEntries_value+'&iPageNo='+pageNo+$j('#sipfilteparams').val(),
	        "bServerSide": true,
	        "bProcessing": true,
	        "bRetrieve" : true,
	        "bDestroy": true,
	        "aaSorting": [[ sort, sortDir ]],
	        "bDeferRender": true,
	        "bAutoWidth": false,
	        "bSortCellsTop": true,
	        "sAjaxDataProp": "aaData",
	        "oLanguage": {
				"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
			},
	        "aoColumnDefs": [
							  {
	    						   "aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
	        						   	var str = "";
	        						   	if(source[2]<100){
	        						   		str ="<input type='checkbox' class='selCordsForSubmitClass' id='selCordsForSubmit' name='selCordsForSubmit' onclick='enableSubmitCBUForSearch(this)' value='"+source[0]+"' disabled='disabled'/>";
	            						}else{
	        						   		str ="<input type='checkbox' class='selCordsForSubmitClass eligibleForSubmit' id='selCordsForSubmit' name='selCordsForSubmit' onclick='enableSubmitCBUForSearch(this)' value='"+source[0]+"'/>";
	            						}
							        	return str;
							   }}, 
	                          {
	                              "aTargets": [1], "mDataProp": function ( source, type, val ) {
	                                 return source[3];
	                             }},
	                            {
	                                    "aTargets": [2], "mDataProp": function ( source, type, val ) {
	                                        var str = "<div style='display:none'>"+removeSymbolfromId(source[1])+"</div>";
	                                        	str = "<u onclick=\"submitpost('openWholeCordEntry',{'cdrCbuPojo.cordID':'"+source[0]+"'});\" style='cursor: pointer;'>"+source[1]+"</u>";
	                                     	return str;
	                             }},
	                          	{
	                                 "aTargets": [3], "mDataProp": function ( source, type, val ) {
	                                    return source[2];
	                             }}
	                        ],
	        "fnDrawCallback": function() {
	                            $j('#searchResults_wrapper').find('.dataTables_scrollHeadInner').find('table').css('position','');
	                            var footerObj = $j("#searchResults_wrapper").find(".dataTables_scrollFoot");
	                            $j(footerObj).css({'display':'block'});
	                            checkeligibleForSubmit();
	                            sipSort();
	                            showStatusFilterText('sipStatFilterFlagTxt','sipStatFilterFlag');
	        				}
	    });
		
	}
	$j('#searchResults_info').hide();
	$j('#searchResults_paginate').hide();
	$j('#searchResults_length').empty();
	$j('#searchResults_length').replaceWith(showEntries_val);
	if($j('#entries').val()!=null || $j('#entries').val()!=""){
		$j('#showsRow').val($j('#entries').val());
	}
	$j('#showsRow').change(function(){
	     var selectedText = $j('#showsRow :selected').html();
	     $j('#sip_tbl_se').val(selectedText);
	     paginationFooter(0,"getCordSaveInProgressPaginationData,showsRow,inputs,Tableform,searchTble,constructSavedinProgTable",'<s:property value="cbuCount"/>','<s:property value="paginateModule"/>');
	});
	
	$j('#resetSortCESIP').click(function(){
		if($j('#sip_sortParam').val()!=undefined && $j('#sip_sortParam').val()!=null && $j('#sip_sortParam').val()!="" && $j('#sip_sortParam').val()!=0){
			$j('#sip_sortParam').val('');
			$j('#sip_sort_type').val('');
			var oTable = $j('#searchResults').dataTable();
			oTable.fnSort( [ [0,'asc'] ] );
			oTable.fnDraw();
		}
	});
	
	$j('#dateStart1').val($j('#sipStrDt').val());
	$j('#dateEnd1').val($j('#sipEndDt').val());
    
}

function sipSort(){
	$j('#sipTheadTr').find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#sip_sortParam').val(i);
			$j('#sip_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#sip_sortParam').val(i);
			$j('#sip_sort_type').val('desc');
		}
   });
}

function selectAllFunc(elem){
	if($j("#selectAll").is(':checked')){
		$j("#esign").attr("disabled",true);		
		resetEsign();
		selectAllChkFunc();
	}else{		
		selectAllChkFunc();		
		$j("#esign").attr("disabled",false);		
		enableEsign();
	}
}
function selectAllChkFunc(){	
    var orderList = document.getElementsByName('selCordsForSubmit');
    var enableFlag = false;
    if($j("#selectAll").is(':checked')){
        $j("#selectAll").attr('checked', false);
        for(var i=0;i<orderList.length;i++){
            $j(orderList[i]).attr('checked', false);
        }
        $j("#selCordsForSubmitButton").attr("disabled",true);
        $j("#selCordsForSubmitButton").addClass('ui-state-disabled');
        resetEsign();
	}else{
		$j("#selectAll").attr('checked', true);
		for(var i=0;i<orderList.length;i++){
			if(!$j(orderList[i]).is(':disabled')){
				$j(orderList[i]).attr('checked', true);
				if(!enableFlag){
	            	enableFlag = true;
	            }
			} 
		}
	}
}

function selectAllFromChkBox(elem){
	if($j("#selectAll").is(':checked')){
		$j("#esign").attr("disabled",false);
		enableEsign();
		selectAllChkBox();
	}else{
		selectAllChkBox();		
		$j("#esign").attr("disabled",true);
		resetEsign();
	}
   
}
function selectAllChkBox(){
	 //$j("#selCordsForSubmitButton").removeClass('ui-state-disabled');
	var orderList = document.getElementsByName('selCordsForSubmit');
	var enableFlag = false;
	if($j("#selectAll").is(':checked')){
		for(var i=0;i<orderList.length;i++){
			if(!$j(orderList[i]).is(':disabled')){
				$j(orderList[i]).attr('checked', true);
				if(!enableFlag){
	            	enableFlag = true;
	            }
			}
		}		
	}else{
		$j("#selectAll").attr('checked', false);
		for(var i=0;i<orderList.length;i++){
			if(!$j(orderList[i]).is(':disabled')){
				$j(orderList[i]).attr('checked', false);
			} 
		}
		 $j("#selCordsForSubmitButton").attr("disabled",true);
		 $j("#selCordsForSubmitButton").addClass('ui-state-disabled');
		 resetEsign();
	}
}
function manageCordEntryInProgress(){
	var RecCount=$j('#tentries').val();
	$j('#searchResults').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
		},
		"bRetrieve": true,
		"bDestroy" :true,
		"bSortCellsTop": true,
		"iDisplayLength":1000,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] }],
		"fnDrawCallback": function() {
			showStatusFilterText('sipStatFilterFlagTxt','sipStatFilterFlag');
		}
	});
	 $j('#searchResults_info').hide();
		$j('#searchResults_paginate').hide();
		$j('#searchResults_length').empty();
		//$j('#searchResults_length').replaceWith('Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;" ><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="paginateSearch.getiTotalRows()"/>">All</option></select> Entries');
		$j('#searchResults_length').replaceWith('Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;" ><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries');
		if($j('#entries').val()!=null || $j('#entries').val()!=""){
			$j('#showsRow').val($j('#entries').val());
			}
		$j('#showsRow').change(function(){
	        var selectedText = $j('#showsRow :selected').html();
	        if(selectedText=="All"){
	        	constructSavedinProgTable();
	        }else{
	        	paginationFooter(0,"getCordSaveInProgressPaginationData,showsRow,inputs,Tableform,searchTble,manageCordEntryInProgress",'<s:property value="cbuCount"/>','<s:property value="paginateModule"/>');
	        }
	    });
		
		$j('#searchResults_filter').find('input:text').keyup(function(){
			 f_countRows('searchResults','2','fromsearchall','HeaderSelectOrd','empty','empty','empty','empty1');
		}); 
		$j('#dateStart1').val($j('#sipStrDt').val());
		$j('#dateEnd1').val($j('#sipEndDt').val());
}

function enableSubmitCBUForSearch(elem){	
	if($j(elem).is(':checked')){
		$j("#esign").attr("disabled",false);
		enableEsign();
	}else{
		var flag=false;
		$j(".selCordsForSubmitClass").each(function(){
			if($j(this).attr("checked")){
				flag=true;	        
	       }
		});		
		if(flag==true){
			$j("#esign").attr("disabled",false);
			enableEsign();			
		}else{
		   enableSubmitCBUSearch();
		   $j("#esign").attr("disabled",true);
		   $j("#selectAll").attr('checked', false);
		   resetEsign();
		}
	}	
}
function enableSubmitCBUSearch(){
	var flag = false;
	$j(".selCordsForSubmitClass").each(function(){
		if($j(this).attr("checked")){
         flag = true;
       }
	});	
    	$j("#selCordsForSubmitButton").attr("disabled",true);
        $j("#selCordsForSubmitButton").addClass('ui-state-disabled');
        resetEsign();  
}
function resetEsign(){
	$j("#esign").val("");
	$j('#cordIdEntryValid').css('display',"none");
	$j('#cordIdEntryMinimum').css('display',"none");
	$j('#cordIdEntryPass').css('display',"none");
	$j("#esign").css("background","#dcdcdd");
}
function enableEsign(){
	$j("#esign").css("background","#FFFFFF");
}
function getTheValuesFromServer(){
	while(!completeSearchReq){
		setTimeout( function() {
			setTimeSubmitForSearch();
		}, 1000 );
    }
}

function setTimeSubmitForSearch(){
	$j.ajax({
        type: "POST",
        url: 'getDivNameFromServer',
        async:true,
        success: function (result){
		totalProcessedCords = result.divName;
			if(cordsCount != undefined && totalProcessedCords != undefined){
				var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><strong>"+totalProcessedCords+" cord out of "+cordsCount+" are submiited!</strong> <span id='prog'></span> </td></tr></table></td></tr></table>";
				
				$j("#modelPopup1").html(progressMsg);
					$j("#modelPopup1").dialog(
							   {autoOpen: false,
								title: title  ,
								resizable: false,
								closeText: '',
								closeOnEscape: false ,
								modal: true, width : width, height : height,
								close: function() {
									//$(".ui-dialog-content").html("");
									//jQuery("#subeditpop").attr("id","subeditpop_old");
					      		jQuery("#modelPopup1").dialog("destroy");
							    }
							   }
							  ); 
					$j("#modelPopup1").dialog("open");
			}
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});   
}
var submittedCord=0;
var count=0;
var deferCords = "";
var cordsCount = 0;
var deferCount = 0;
var cordArr = new Array();
var entryToken=0;
function autoDeferSubmit(cordId,registryId){
	jConfirm('<s:text name="Selected Cord "/>' +registryId+ '<s:text name=" for submit will be auto deferred do you want to continue?"/>', '<s:text name="garuda.common.lable.confirm"/>',
			function(r) {
				if (r == true) {
					$j.ajax({
						type : "GET",
						async : false,
						url : 'saveAutoDeferCordsForSearch?cdrCbuPojo.cordID='+cordId,
						success : function(result) {
							if(result.successFlag = true){
								submittedCord++;
								if(deferCount==0){
									deferCords += registryId ;
		    					}else{
			    					deferCords += "," + registryId ;
			    				}
								deferCount++;
								if(count<(cordArr.length)){
									count++;
									setTimeout(function(){
										saveForCBUSearching();
									},0);
			        			}
	    					}else{
	    						if(count<(cordArr.length)){
			        				count++;
			        				setTimeout(function(){
										saveForCBUSearching();
									},0);
			        			}
	    					}
	    				}
	    			});     				
        		}else{
            		if(count<(cordArr.length)){
                		count++;
                		setTimeout(function(){
							saveForCBUSearching();
						},0);
                	}
	    	    }
	   		});
}
function saveForCBUSearching(){
	var data = $j("#searchTble :input").serialize();
	var title = "Submit Cords for Search";
	var url = "saveCordsForSearch";
	var autoDeferArr =new Array() ;
	var cordsCount = 0;
	var statusFlag;
	var orderList = document.getElementsByName('selCordsForSubmit');
	for(var i=0;i<orderList.length;i++){
		if($j(orderList[i]).is(':checked'))
		{
		cordArr[cordsCount]=$j(orderList[i]).val();		
		cordsCount++;
		}
	}
	/* $j(".selCordsForSubmitClass").each(function(){
		if($j(this).attr("checked")){
			cordArr[cordsCount]=$j(this).val();			
			cordsCount++;			
       }
	}); */	
		var cordId = cordArr[count];
		var deferValue = false;
		var registryId=" ";
		var isSubmit = false;
		if(count<(cordArr.length)){
			if(validateIscordSubmitted()){
				count++;
		    }else{
				$j.ajax({
					type : "GET",
					async : false,
					url : 'getDeferStatus?cdrCbuPojo.cordID='+cordId+'&submitExternal='+true,
					success : function(result) {
						deferValue = result.defer;	
						registryId = result.autoDeferRegistryId;
						isSubmit = result.submitCord;						
						if(deferValue==true){
							 autoDeferSubmit(cordId,registryId);
						}else{	
							submittedCord++;				 	
							if(count<(cordArr.length))
							{
								count++;
								setTimeout(function(){
									saveForCBUSearching();
								},0);
							 	//saveForCBUSearching();
							}
						}			
					}			
				}); 
		    }
		}
		showprogressMgs();			
if(count==(cordArr.length) && entryToken<1){
	entryToken++;
	var progressMsg="";
	 		 if(cordsCount != undefined){
	 			if(submittedCord==0)
					 progressMsg="<table width='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><strong>None of the cord submitted!</strong></td></tr></table></td></tr>";
				else if(submittedCord==1)
				      progressMsg="<table width='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><strong>"+submittedCord+" cord submitted!</strong></td></tr></table></td></tr>";
				
				else{
					  progressMsg="<table width='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><strong>"+submittedCord+" cords are submitted!</strong></td></tr></table></td></tr>";
				}if(deferCords!="")
				  progressMsg += "<tr><td width='100%' align='center'><table><tr><td align='center'><strong>Following Cords having Registry ID are Auto Defered:- "+deferCords+"</strong></td></tr></table></td></tr>";
				  progressMsg += "</table>";				
				  progressMsg += "<tr><td width='100%' align='center'><input type='button' name='Close' value='Close' onclick='closeMultipleCordSubmitPopup();' /></td></tr>";
			      progressMsg += "</table>"; 
			        
		$j("#modelPopup1").html(progressMsg);
			$j("#modelPopup1").dialog(
					   {autoOpen: false,
						title: title  ,
						resizable: false,
						closeText: '',
						closeOnEscape: false ,
						modal: true, width : 400, height : 150,
						close: function() {
							//$(".ui-dialog-content").html("");
							//jQuery("#subeditpop").attr("id","subeditpop_old");
			      		$j("#modelPopup1").dialog("destroy");
					    }
					   }
					  ); 
			closeprogressMsg();
			$j("#modelPopup1").dialog().parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
			$j("#modelPopup1").dialog("open");
	}	 
		}
}
function customRange1(input) {
	/* if (input.id == 'dateEnd1') {
		    return {
		      minDate: jQuery('#dateStart1').datepicker("getDate")
		    };
		  } else if (input.id == 'dateStart1') {
		    return {
		      maxDate: jQuery('#dateEnd1').datepicker("getDate")
		    };
		  }*/
		if (input.id == 'dateEnd1') {
		    /*return {
		      minDate: jQuery('#dateStart').datepicker("getDate")
		    };*/
		    if($j("#dateStart1").val()!="" && $j("#dateStart1").val()!=null){
		    var minTestDate=new Date($j("#dateStart1").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
			  $j('#dateEnd1').datepicker( "option", "minDate", new Date(y1,m1,d1));
		    }else{
		    	$j('#dateEnd1').datepicker( "option", "minDate", null);
			   }
		  } else if (input.id == 'dateStart1') {
			  if($j("#dateEnd1").val()!="" && $j("#dateEnd1").val()!=null){
			  var minTestDate=new Date($j("#dateEnd1").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
		   /* return {
		      maxDate: jQuery('#dateEnd').datepicker("getDate")
		    };*/
			  $j('#dateStart1').datepicker( "option", "maxDate", new Date(y1,m1,d1));
			  }else{
				  $j('#dateStart1').datepicker( "option", "maxDate", null);
			}
		  }
		  
}
function closeMultipleCordSubmitPopup(){
	$j("#modelPopup1").dialog("close");
	$j("#modelPopup1").dialog("destroy");
	window.location.href="cordEntryInProgress.action";
	
}

/* function saveForCBUSearching(){	
	var data = $j("#searchTble :input").serialize();
	var title = "Submit Cords for Search";
	var url = "saveCordsForSearch";
	$j(".selCordsForSubmitClass").each(function(){
		if($j(this).attr("checked")){
			cordsCount++;
       }
	});
	var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><strong>Submitting "+cordsCount+" cords for search ! <strong></td></tr></table></td></tr></table>";
	$j("#modelPopup1").html(progressMsg);
		$j("#modelPopup1").dialog(
				   {autoOpen: false,
					title : title,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : 300, height : 100,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#modelPopup1").dialog("destroy");
				    }
				   }
				  ); 
		$j("#modelPopup1").dialog().parents(".ui-dialog").find(".ui-dialog-titlebar-close").remove();
		$j("#modelPopup1").dialog("open");
		var xhr = $j.ajax({
        type: "POST",
        url: url,
        async:true,
        data : data,
        success: function (result, status, error){
			completeSearchReq = true;	        
	       $j("#modelPopup1").html(result);        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
		
		
}*/
/* $j(window).unload(function(){
	  var sipShowEntries=$j("#entries").val();
	  if(sipShowEntries!=null && sipShowEntries!=''){
		  var pageCookie1=sipShowEntries;
		  $j.cookie('sipSE', pageCookie1);
	  }
});  */
function showNoDiv(el,id,id1){
	var t=$j(el);
	var obj=t.offset();
	var xOffset=180;
	var yOffset=30;
	var preval=$j('#sipPerComp').val();
	
	if(id=='perCentageDiv'){
		xOffset=180+170;
		yOffset=30;
	}
	if($j('.dataEnteringDiv').is(':visible')){
		$j('.dataEnteringDiv').hide();
	}
	
	if(preval!=null && preval!=''){
		$j('#sippercentval').val(preval);
	}
	
	
	if(navigator.appName=="Microsoft Internet Explorer"){yOffset=yOffset+15;}
	$j("#"+id)
			.css({left:obj.left-xOffset,top:obj.top+yOffset})
			.fadeIn("fast").show()
			.addClass('dateDivClass');
	//alert("id"+id+"id1"+id1)
	$j("#"+id).show();
	$j('#'+id1).focus();
}
function fn_filterSIP(){
	
	var cbuRegid="";
	var startDate="";
	var startEnd="";
	var perCent="";
	var dateData="0";
	var noofdaysData="0";
	var filterParams="";
	
	
	
	cbuRegid=$j.trim($j('#sipregId').val());
	perCent=$j.trim($j('#sippercentval').val());
	
	
	
	if($j('#dateStart1').val()!="" && $j('#dateEnd1').val()!=""){
		var dateElements = $j('.datepic').get();
		  $j.each(dateElements, function(){
			  customRange1(this);
		  });
		if($j("#dummyForm").valid()){
		startDate=parseDateValue($j('#dateStart1').val());
		startEnd=parseDateValue($j('#dateEnd1').val());
		dateData="1";
		}
		else{
			$j('#dateDiv1').show();
			dontflag=1;
			}
		
	}else if( ($j.trim($j('#dateStart1').val())!="" && $j.trim($j('#dateEnd1').val())=="" ) || ($j.trim($j('#dateStart1').val())=="" && $j.trim($j('#dateEnd1').val())!="")){
		alert("Please enter correct Date Range");
		dontflag=1;
	}
	
	
	if(perCent!=""){
		if(IsNumeric(perCent)){
			perCent=$j('#sippercentval').val();
			noofdaysData="1";
		}else{
			alert("Please enter numerical value only");
			$j('#sippercentval').val('');
			$j('#percentage').triggerHandler('click');
			dontflag=1;
		}
	}
	var is_all_entries=$j('#showsRow :selected').text();
	
	
	if(cbuRegid!="" || dateData=="1" || noofdaysData=="1"){
		filterParams='&cbuRegId='+cbuRegid+"&perCentComp="+perCent+"&startDate="+startDate+"&endDate="+startEnd;
		$j('#sipfilteparams').val(filterParams);
		$j('#sip_filterFlag').val("1");
		$j('#sipStatFilterFlag').val('1');
		var selectedText = $j('#showsRow :selected').html();
		paginationFooter(0,"getCordSaveInProgressPaginationData,showsRow,inputs,Tableform,searchTble,constructSavedinProgTable",'<s:property value="cbuCount"/>','<s:property value="paginateModule"/>');
		
	}
	
}
function fn_resetfilterSIP(){
	
	
	var cbuRegid="";
	var startDate="";
	var startEnd="";
	var perCent="";
	var dateData="0";
	var noofdaysData="0";
	var filterParams="";
	var filterflag=$j('#sip_filterFlag').val();
	resetValidForm('dummyForm');
	$j('#sipStatFilterFlag').val('0');
	if($j('#dateStart1').val()!="" && $j('#dateEnd1').val()!=""){
		dateData="1";
	}
	
	cbuRegid=$j.trim($j('#sipregId').val());
	perCent=$j.trim($j('#sippercentval').val());
	
	if(filterflag=="1"){
		var filterParams='&empty=';
		$j('#sipfilteparams').val(filterParams);
		var selectedText = $j('#showsRow :selected').html();
		$j('#sip_filterFlag').val("0");
		paginationFooter(0,"getCordSaveInProgressPaginationData,showsRow,inputs,Tableform,searchTble,constructSavedinProgTable",'<s:property value="cbuCount"/>','<s:property value="paginateModule"/>');
	}
	
	
	$j('#sipregId').val('');
	$j('#sippercentval').val('');
	$j('#dateStart1').val('');
    $j('#dateEnd1').val('');
    $j('#sipfilteparams').val('');
	
	
}
$j(function(){
	$j("#esign").attr("disabled","disabled");
	$j("#esign").css("height","20px");
	$j("#esign").css("background","#dcdcdd");
	$j("span#cordIdEntryMinimum").css("padding","3px");
	$j("#esign").focusin(function(){
		$j(this).css("border","1px solid #DBDAA9");
		});
	$j("#esign").focusout(function(){
		$j(this).css("border","");
		});
});
</script>
<style type="text/css">
#sipTbody td{
    padding: 0px 10px !important;
    white-space: nowrap; 
    
}
</style>
<div>

</div>
<s:form>
 <s:hidden name="sipStrDt" id="sipStrDt"/>
 <s:hidden name="sipEndDt" id="sipEndDt"/>
 <s:hidden name="sipPerComp" id="sipPerComp"/>
 <input type="hidden" name="entries" id="entries" value=<%=paginateSearch.getiShowRows() %> />
 <input type="hidden" name="tentries" id="tentries" value=<%=paginateSearch.getiTotalRows() %> />
  
 <table align="right">
				<tr align="right" style="vertical-align: top;">
						<td>
						<a href="#" onclick="fn_filterSIP()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>
						&nbsp;&nbsp;
						<a href="#" onclick="fn_resetfilterSIP()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
						&nbsp;&nbsp;
						<a href="#"  id="resetSortCESIP"><span><s:text name="garuda.pendingorderpage.label.resetsort"/></span></a>
					    </td>
				</tr>
</table>
 
 <table width="100%" cellpadding="0" cellspacing="0" border="0" class="displaycdr searchResults" id="searchResults">
    <thead>
     <tr id="sipTheadTr">
     	 <th><s:text name="garuda.cordentry.label.selectAll"/></th> 
         <th><s:text name="garuda.cordentry.label.starteddate"/></th><th><s:text name="garuda.cbuentry.label.registrycbuid"/></th><th><s:text name="garuda.cordentry.label.percent"/></th>
     </tr>
     <tr>
     <th>
     <input type="checkbox" id ="selectAll" onclick ="selectAllFromChkBox(this)" align="left" />
     <button id="selectAllButton" type="button" onclick="selectAllFunc(this);"> 
	 	<span><s:text name="garuda.cordentry.label.selectAll"/></span>
	 </button>
     </th>
     <th>
     <button id="dateRange2" type="button" onclick="showDateDiv(this,'dateDiv1','dateStart1','dateEnd1'); resetValidForm('dummyForm');"> 
	 <span><s:text name="garuda.pendingorder.button.dateRange"/></span>
	 </button>
     </th>
     <th>
     <s:textfield name="sipCBURegId" id="sipregId" placeholder="Enter CBU Registry ID"/>
     </th>
     <th>
     <button id="percentage" onclick="showNoDiv(this,'perCentageDiv','sippercentval')" type="button">
     <span><s:text name='garuda.pendingorderpage.label.percentcompgreater' /></span>
     </button>
     </th>
     </tr>
     
 </thead>
 <tbody id="sipTbody">
    <s:iterator value="cordInfoList" var="rowVal" status="row">
     <tr id="notavilTr_<s:property value='%{#row.index}'/>" onMouseOver="highlight('notavilTr_<s:property value="%{#row.index}"/>')" onmouseout="removeHighlight('notavilTr_<s:property value="%{#row.index}"/>')">
        <td><input type="checkbox" class="selCordsForSubmitClass <s:if test='%{#rowVal[2]>=100}'>eligibleForSubmit</s:if>" id="selCordsForSubmit" name="selCordsForSubmit" onclick="enableSubmitCBUForSearch(this)" value="<s:property value="%{#rowVal[0]}" />" <s:if test="%{#rowVal[2]<100}">disabled="disabled"</s:if>/></td>
        <td><s:date name="%{#rowVal[3]}" id="datepicker" format="MMM dd, yyyy" />
        <s:property value="%{datepicker}" />
        </td>
        <td>
        	<div style="display:none"><s:property value="%{removeSymbol(#rowVal[1],'-')}"/></div>
        	 <s:if test="hasEditPermission(#request.saveinprg)==true">
        	    <u onclick="validateIscordSubmitted('<s:property value="%{#rowVal[0]}" />');" style="cursor: pointer;">
        		   <s:set name="registryIdFld" value="%{#rowVal[1]}" />
        	     	<s:property value="insertHyphenIntoRegistryId(#registryIdFld)"/>
        	    </u>
        	</s:if>
        		<s:elseif test="hasEditPermission(#request.saveinprg)==false">
					   <span onclick="alert('User does not has edit permission for Saved in Progress CBUs');"  style="cursor: pointer;">
						 <s:set name="registryIdFld" value="%{#rowVal[1]}" />
        		        <s:property value="insertHyphenIntoRegistryId(#registryIdFld)"/>
					   </span>
				</s:elseif>	
        </td>
        <td><s:property value="%{#rowVal[2]}" />
        </td>
     </tr>
 </s:iterator>     
 </tbody>
 <tfoot>
	<tr><td colspan="4"></td></tr>			
	<tr>
	   <td colspan="2" id="notavilTableFooter">
		    <jsp:include page="../paginationFooter.jsp">
			  	<jsp:param value="searchTble" name="divName"/>
			  	<jsp:param value="Tableform" name="formName"/>
			  	<jsp:param value="showsRow" name="showsRecordId"/>
			  	<jsp:param value="getCordSaveInProgressPaginationData" name="url"/>
			  	<jsp:param value="inputs" name="cbuid"/>
			  	<jsp:param value="workflow" name="paginateSearch"/>
			  	<jsp:param value="2" name="idparam"/>
			  	<jsp:param value="constructSavedinProgTable" name="bodyOnloadFn"/>
		    </jsp:include>
       </td>           
   </tr>
</tfoot>	
<table width="100%">
<tr>
<td colspan="" >
							    <table bgcolor="#cccccc" width="100%">
							         <tr bgcolor="#cccccc" valign="baseline">
									   <td width="100%">
									             <jsp:include page="../cb_esignature.jsp" flush="true">
								                        <jsp:param value="selCordsForSubmitButton" name="submitId"/>
														<jsp:param value="cordIdEntryValid" name="invalid" />
														<jsp:param value="cordIdEntryMinimum" name="minimum" />
														<jsp:param value="cordIdEntryPass" name="pass" />
									             </jsp:include>
									   </td>
									   <td colspan="2" align="right">
                                            <input type="button" style="width:200px;" disabled="disabled" id="selCordsForSubmitButton" onclick="saveForCBUSearching();" value="<s:text name="garuda.cordentry.label.submitcbuforsearching"/>"/>
                                        </td>
									   </tr>
							    </table>						          		           
						      </td>
</tr>
</table>						     								     
 </table>
</s:form>