<%@ taglib prefix="s"  uri="/struts-tags"%>	
  <style type="text/css">
 div#added1Div {
     overflow-x:auto;  
     margin-left:15em;
     overflow-y:hidden;
     border-left: .1em solid #333333;
        
}
.headcolCri {
    position:absolute;
    width:15.2em;
    margin-left:1em;
    left:0;
    top:auto;
    text-align:center;
    vertical-align:top;
    border-left: 0em solid #333333;
    border-right: 0em solid #333333; 
    border-top:.1em solid #333333;/* only relevant for first row*/
	border-bottom-width:0px;
    margin-top:-0em; /*compensate for top border*/
} 
</style> 
 <script>
if('<s:property value="esignFlag"/>'=='review'){
	$j(".headcolCri").each(function() {
		  $j(this).removeClass("headcolCri");
		});
	$j('#added1Div').attr('id','changeLater');
}
</script> 
<s:hidden name="autodeferfieldflag" id="autodeferfieldflag" value="false"></s:hidden>
<s:hidden name="otherLabTstsLstId" id="otherLabTstsLstId"></s:hidden>
<s:hidden name="pkCfuOther" id="pkOther"></s:hidden>	
<s:hidden name="pkViabilityOther" id="pkViaOther"></s:hidden>	
<s:hidden name="cdrCbuPojo.fkSpecimenId"></s:hidden>			
<s:hidden name="cdrCbuPojo.cordID" id="cordID"></s:hidden>
<s:hidden name="esignFlag" id="esignFlag"></s:hidden>

<s:hidden value="%{#request.valuePreFkTimingTest}" id="valuePreFkTimingTest"></s:hidden>
<s:hidden value="%{#request.valuePostFkTimingTest}" id="valuePostFkTimingTest"></s:hidden>
<s:hidden value="%{#request.valueOtherFkTimingTest}" id="valueOtherFkTimingTest"></s:hidden>
					   
<s:hidden value="%{#request.valueOtherPostTestFirst}" id="valueOtherPostTestFirst"></s:hidden>
<s:hidden value="%{#request.valueOtherPostTestSecond}" id="valueOtherPostTestSecond"></s:hidden>
<s:hidden value="%{#request.valueOtherPostTestThird}" id="valueOtherPostTestThird"></s:hidden>
<s:hidden value="%{#request.valueOtherPostTestFourth}" id="valueOtherPostTestFourth"></s:hidden>
<s:hidden value="%{#request.valueOtherPostTestFifth}" id="valueOtherPostTestFifth"></s:hidden>
<s:iterator value="processingTest" var="rowVal" status="row">
<s:hidden value="%{pkLabtest}" id="%{shrtName}" name="%{labtestName}"></s:hidden>
</s:iterator>
<s:hidden value="%{#request.labGroupTypTests}" id="noOfLabGrpTypTests" name="noOfLabGrpTypTests"/>
<s:hidden value="#request.TNCUncFkTestId" id="UNCRCT" name="Total CBU Nucleated Cell Count (uncorrected)"/>
<s:hidden value="#request.TNCUnkIfUncFkTestId" id="TCBUUN" name="Total CBU Nucleated Cell Count (unknown if uncorrected)"/>
<s:hidden value="%{#request.cfuOtherPkVal}" id="valueCfuOtherPkVal"></s:hidden>
<s:hidden value="%{#request.testReasonOtherPkVal}" id="valueTestReasonOtherPkVal"></s:hidden>
<s:hidden value="%{#request.viablityOtherPkVal}" id="valueViablityOtherPkVal"></s:hidden>
<!--
<input type="hidden" id="savePreTestList0testresult" value="<s:property value="preTestList[0].testresult"/>"/>
<input type="hidden" id="savePostTestList4testresult" value="<s:property value="postTestList[3].testresult"/>"/>
<input type="hidden" id="savePostTestList5testresult" value="<s:property value="postTestList[4].testresult"/>"/>
<input type="hidden" id="savePostTestList6testresult" value="<s:property value="postTestList[5].testresult"/>"/>
<input type="hidden" id="savePostTestList12testresult" value="<s:property value="postTestList[12].testresult"/>"/>
<input type="hidden" id="savePostTestList13testresult" value="<s:property value="postTestList[13].testresult"/>"/>
<input type="hidden" id="savePostTestList14testresult" value="<s:property value="postTestList[14].testresult"/>"/>
<input type="hidden" id="savePostTestList15testresult" value="<s:property value="postTestList[15].testresult"/>"/>

<input type="hidden" id="savePostTestList14fktestmethod" value="<s:property value="postTestList[14].fktestmethod"/>"/>
<input type="hidden" id="savePostTestList15fktestmethod" value="<s:property value="postTestList[15].fktestmethod"/>"/>
-->
	<div id="editlabsummarytest" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all labsumHidDiv" >
			<span class="ui-icon ui-icon-minusthick"></span>
			<span class="ui-icon ui-icon-close" ></span>
			<s:text name="garuda.cordentry.label.labsummaryTest" />
	</div> 
	<div style="display: none;">
		<s:date name="CdrCbuPojo.birthDate" id="datepickerbirthlabModalCri" format="MMM dd, yyyy" />	
 		<s:textfield readonly="true" onkeydown="cancelBack();" name="datepickerbirthlabModalCri" id="datepickerbirthlabModalCri"  value="%{datepickerbirthlabModalCri}" ></s:textfield>											      				
		<s:date name="CdrCbuPojo.processingDate" id="processingDateId" format="MMM dd, yyyy" />	
		<s:hidden name="processingDateId" id="processingDateId" value="%{processingDateId}"/>	
	</div> 
	<div style="display: none;">
		<s:date name="CdrCbuPojo.specimen.specCollDate" id="datepickerColllabModalCri" format="MMM dd, yyyy" />	
	 	<s:textfield readonly="true" name="datepickerColllabModalCri" id="datepickerColllabModalCri"  value="%{datepickerColllabModalCri}" ></s:textfield>											      					
	</div>
							
		<%-- <div id="update" style="display: none;" >
					  <table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td colspan="2">
								<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
									<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
									<strong><s:text name="garuda.common.lable.testMessage" /></strong></p>
								</div>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="button" onclick="closeModalTest('labSummeryTests')" value="<s:text name="garuda.common.close"/>" />
							</td>
						</tr>
					</table>	   
		</div>	 --%>		
		<table width="100%" cellpadding="0" style="table-layout : fixed;" cellspacing="0" border="1" >   
		<tr>
		<td>
		<div id="added1Div" style="overflow: scroll;">	
		<table id="status" width="100%" cellpadding="0" cellspacing="0" border="0">   
		   <tr>
		   			<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0" >
							<tr>
								<td class="headcolCri">
								</td>
								<td align="right">
									<button type="button"  id="addTestDate" onclick="showTest('<s:property  value="cdrCbuPojo.fkSpecimenId" />'),closeModal();"><s:text name="garuda.cordentry.label.addTestTime"/></button>
								</td>
							</tr>
						</table>
					</td>
			</tr>
		   <tr>
			   <td> 
				  <table width="100%" cellpadding="0" cellspacing="0" border="1" id="added1">
					<% int headerCount=3; %>
					<thead>
					  <tr>
						  <th class="headcolCri"><s:text name="garuda.common.lable.test"></s:text></th>
						  <th >
						      <s:text name="garuda.common.lable.preProcess"></s:text><br></br>
						      <s:text name="garuda.common.lable.testingDate"></s:text>
							  <s:date name="prePatLabsPojo.testdate" id="datepicker52" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="preTestDateStrn" id="preTestDateStrnM" onchange="isChangeDone(this,'%{datepicker52}','labChanges'); changeProgress(1,'datepicker52');" value="%{datepicker52}" cssClass="datePicWMaxDate onChangeMandDate" cssStyle="width: 210px;"></s:textfield>
						  	  <s:hidden value="%{datepicker52}" id="preTestDateStrnM_dbStoredVal"/>
					 		  <br/><span style="display:none" id="preTestDateStrnM_error" class="error"><s:text name=""></s:text> </span>
						  </th>
							        	
						  <th >
					        <s:text name="garuda.common.lable.postProcess"></s:text><br></br>
					        <s:text name="garuda.common.lable.testingDate"></s:text>
						    <s:date name="postPatLabsPojo.testdate" id="datepicker53" format="MMM dd, yyyy" />
						    <s:textfield readonly="true" onkeydown="cancelBack();"  name="postTestDateStrn" id="postTestDateStrnM" onchange="isChangeDone(this,'%{datepicker53}','labChanges'); changeProgress(1,'datepicker53');" value="%{datepicker53}" cssClass="datePicWMaxDate onChangeMandDate" cssStyle="width: 210px;"></s:textfield>
					  		<s:hidden value="%{datepicker53}" id="postTestDateStrnM_dbStoredVal"/>
					 		<br/><span style="display:none" id="postTestDateStrnM_error" class="error"><s:text name=""></s:text> </span>
					  </th>
		                 <s:if test="otherPostTestList!=null && otherPostTestList.size()>0 ">
		                 	 <%  headerCount=headerCount+1; %>
						    <th ><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
						      <s:date name="otherPatLabsPojo.testdate" id="datepicker92" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn1" id="otherTestDateStrnM1" onchange="isChangeDone(this,'%{datepicker92}','labChanges'); changeProgress(1,'datepicker92');" value="%{datepicker92}" cssClass="datePicWMaxDate onChangeMandDate" cssStyle="width: 210px;"></s:textfield>
						      <s:hidden value="%{datepicker92}" id="otherTestDateStrnM1_dbStoredVal"/>
					 		  <br/><span style="display:none" id="otherTestDateStrnM1_error" class="error"><s:text name=""></s:text> </span>
						    </th>
						  </s:if>
						  
						<s:if test="otherPostTestListFirst!=null && otherPostTestListFirst.size()>0 ">
							 <%  headerCount=headerCount+1; %>
						    <th ><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
						      <s:date name="otherPatLabsPojoFirst.testdate" id="datepicker94" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn2" id="otherTestDateStrnM2" onchange="isChangeDone(this,'%{datepicker94}','labChanges'); changeProgress(1,'datepicker94');" value="%{datepicker94}" cssClass="datePicWMaxDate onChangeMandDate" cssStyle="width: 210px;"></s:textfield>
						      <s:hidden value="%{datepicker94}" id="otherTestDateStrnM2_dbStoredVal"/>
					 		  <br/><span style="display:none" id="otherTestDateStrnM2_error" class="error"><s:text name=""></s:text> </span>
						    </th>
						</s:if>
						  
						 <s:if test="otherPostTestListSecond!=null && otherPostTestListSecond.size()>0 ">
						 	 <%  headerCount=headerCount+1; %>
							    <th ><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
							   <s:date name="otherPatLabsPojoSecond.testdate" id="datepicker95" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn3" id="otherTestDateStrnM3" onchange="isChangeDone(this,'%{datepicker95}','labChanges'); changeProgress(1,'datepicker95');" value="%{datepicker95}" cssClass="datePicWMaxDate onChangeMandDate" cssStyle="width: 210px;"></s:textfield>
							  <s:hidden value="%{datepicker95}" id="otherTestDateStrnM3_dbStoredVal"/>
					 		  <br/><span style="display:none" id="otherTestDateStrnM3_error" class="error"><s:text name=""></s:text> </span> 
							    </th>
						 </s:if>
						<s:if test="otherPostTestListThird!=null && otherPostTestListThird.size()>0 ">
							 <%  headerCount=headerCount+1; %>
							    <th ><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
							    <s:date name="otherPatLabsPojoThird.testdate" id="datepicker96" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn4" id="otherTestDateStrnM4" onchange="isChangeDone(this,'%{datepicker96}','labChanges'); changeProgress(1,'datepicker96');" value="%{datepicker96}" cssClass="datePicWMaxDate onChangeMandDate" cssStyle="width: 210px;"></s:textfield>
							  <s:hidden value="%{datepicker96}" id="otherTestDateStrnM4_dbStoredVal"/>
					 		  <br/><span style="display:none" id="otherTestDateStrnM4_error" class="error"><s:text name=""></s:text> </span>
							    </th>
						 </s:if>
					 
						<s:if test="otherPostTestListFourth!=null && otherPostTestListFourth.size()>0 ">
							 <%  headerCount=headerCount+1; %>
							    <th ><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
							    <s:date name="otherPatLabsPojoFourth.testdate" id="datepicker97" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn5" id="otherTestDateStrnM5" onchange="isChangeDone(this,'%{datepicker97}','labChanges'); changeProgress(1,'datepicker97');" value="%{datepicker97}" cssClass="datePicWMaxDate onChangeMandDate" cssStyle="width: 210px;"></s:textfield>
							    <s:hidden value="%{datepicker97}" id="otherTestDateStrnM5_dbStoredVal"/>
					 		  <br/><span style="display:none" id="otherTestDateStrnM5_error" class="error"><s:text name=""></s:text> </span>
							    </th>
						 </s:if>
					 
						 <s:if test="otherPostTestListFifth!=null && otherPostTestListFifth.size()>0 ">
						 	 <%  headerCount=headerCount+1; %>
							  <th ><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
								<s:date name="otherPatLabsPojoFive.testdate" id="datepicker98" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn6" id="otherTestDateStrnM6" onchange="isChangeDone(this,'%{datepicker98}','labChanges'); changeProgress(1,'datepicker98');" value="%{datepicker98}" cssClass="datePicWMaxDate onChangeMandDate" cssStyle="width: 210px;"></s:textfield>
 							  <s:hidden value="%{datepicker98}" id="otherTestDateStrnM6_dbStoredVal"/>
					 		  <br/><span style="display:none" id="otherTestDateStrnM6_error" class="error"><s:text name=""></s:text> </span>
 							  </th>
						 </s:if>
						  
					  </tr>	                       <%--  <th width="25%"><s:text name="garuda.common.lable.addTesting"></s:text></th> --%>
					
				</thead>	
					<tbody>
						<% int testCount=1; %> 
						<s:iterator value="processingTest" var="rowVal" status="row">
								<tr style="width: 100%;" id="<s:property	value="%{#row.index}"/>">
									<% request.setAttribute("testCount",testCount); %>            	
								 
								 <s:if test="labtestHower!=' '"> 										            	
								      <td class="headcolCri" width="30%" ><s:property value="labtestName"/></td>
								  </s:if>
								  <s:else>
									  <td class="headcolCri" width="30%" onmouseover="return overlib('<s:property value="labtestHower"/>');"
											onmouseout="return nd();"> <s:property value="labtestName"/>
									  </td>
								  </s:else>	
		  
								<%-- 	<% int precount=0; %> --%>
								<s:set name="checkEmpty" value="0" scope="request"/>
									<td class="BasicText" align="left">
									 <s:iterator value="preTestList">
						            <%-- ondblclick="addInfo('<s:property value="cdrCbuPojo.fkSpecimenId" />','<s:property value="fktestid" />','<s:property value="pkpatlabs" />');	 
								          <s:property value="cdrCbuPojo.fkSpecimenId" />	  <s:property value="pkLabtest" /> <s:property value="fktestid" /><s:property value="pkpatlabs" /> 
									 <s:property value="pkLabtest" /> <s:property value="fktestid" /> <s:property value="pkpatlabs" />  --%>
							
									   	  <s:if test="pkLabtest==fktestid">
									   	   <s:if test="#request.valuePreFkTimingTest==fkTimingOfTest">
									   	   <s:set name="checkEmpty" value="1" scope="request"/>
									   	<%--   <%precount++; %> --%>
									   	  <%--  preeee<s:property value="fkTimingOfTest"/> --%>
									   		<s:set name="check" value="%{'check'}" />
										      
													 <s:hidden name="savePreTestList[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
													 <s:hidden name="savePreTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden> 
													<s:hidden name="savePreTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePreFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"></s:if>
												<s:else></s:else>
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count' || labtestName=='CBU Nucleated Cell Count Concentration'">
														<s:text name="garuda.cbuentry.label.testresult"></s:text>
														<input style="width:40%" type="text" name="savePreTestList[<s:property value="%{#row.index}"/>].testresult" id="savePreTestList<s:property value="%{#row.index}"/>testresult" value="<s:property value="%{testresult}"/>" size="10" <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' ">class="labclass"</s:if> onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'pre')"/>
					 									<s:hidden value="%{testresult}" id="savePreTestList%{#row.index}testresult_dbStoredVal"/>
					 									<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
					 							</s:if>	
												<s:else>
													 <s:set name="testresult" value="%{testresult}" scope="request"/>
													<s:if test="#request.testresult != null">
													<s:text name="garuda.cbuentry.label.testresult"></s:text>
													<s:textfield name="savePreTestList[%{#row.index}].testresult" id="savePreTestList%{#row.index}testresult" cssClass="width:45%" value="%{testresult}" size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeydown="cancelBack();" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'pre')" /> 
													<s:hidden  value="%{testresult}" id="savePreTestList%{#row.index}testresult_dbStoredVal"/>
													<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													<s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePreTestList[%{#row.index}].fktestspecimen" id="savePreTestList%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="isChangeDone(this,'%{fktestspecimen}','labChanges'); showDescField('notReq','notReq',this.value,this.id);"></s:select>
													<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
													</s:if> 
												</s:else>
													   <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
													   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;10<sup>7</sup></s:if>
													   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;10<sup>3</sup><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
						            </s:if>
					            	</s:if>	
					              </s:iterator>
					             
								    <s:if test="#request.checkEmpty==0 && (preTestList.size()>0)">
					               		 		&nbsp;
					               	</s:if> 
					               <%--<%=precount%> --%>
							           <s:if test="%{#check!='check'}">	
								           		<s:hidden name="savePreTestList[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
									             <s:hidden name="savePreTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden> 
									              <s:hidden name="savePreTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePreFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"></s:if>
												<s:else></s:else>
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count' || labtestName=='CBU Nucleated Cell Count Concentration'">		 
					 								 <s:text name="garuda.cbuentry.label.testresult"></s:text>
					 								 <input type="text" style="width:40%" name="savePreTestList[<s:property value="%{#row.index}"/>].testresult" id="savePreTestList<s:property value="%{#row.index}"/>testresult" value="<s:property value="%{testresult}"/>" onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" size="10" <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count' || labtestName=='CBU Nucleated Cell Count Concentration'">class="labclass"</s:if> onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'pre')"/>
				 							    	<s:hidden  value="%{testresult}" id="savePreTestList%{#row.index}testresult_dbStoredVal"/>
				 							    	<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
				 							    </s:if>	
											    <s:else>
											    <s:set name="testresult" value="%{testresult}" scope="request"/>
													<s:if test="#request.testresult != null">
													<s:text name="garuda.cbuentry.label.testresult"></s:text>
													<s:textfield name="savePreTestList[%{#row.index}].testresult" id="savePreTestList%{#row.index}testresult" cssClass="width:45%" value="%{testresult}" onchange="isChangeDone(this,'%{testresult}','labChanges');" size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'pre')"/> 
													<s:hidden  value="%{testresult}" id="savePreTestList%{#row.index}testresult_dbStoredVal"/>
													<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													<s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePreTestList[%{#row.index}].fktestspecimen" id="savePreTestList%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="isChangeDone(this,'%{fktestspecimen}','labChanges'); showDescField('notReq','notReq',this.value,this.id);"></s:select>
													<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
													</s:if> 
											  	</s:else>
													<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												    <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;10<sup>7</sup></s:if>
												    <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;10<sup>3</sup><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
					                	</s:if></td>
					                	
 	                           			<%-- <% int postcount=0;  %> --%>
										<s:set name="checkEmpty" value="0" scope="request"/>
										<s:set name="cellCountFlag" value="0" scope="request"/>
 							           <td class="BasicText" align="left">
 							           <s:iterator value="postTestList">
 									     <s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     <s:if test="#request.valuePostFkTimingTest==fkTimingOfTest">
									     <s:if test="#request.cellCountFlag==0">
									        <s:set name="checkEmpty" value="1" scope="request"/>  
									        <s:set name="check1" value="%{'check'}"/>
									         	   <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												   <s:text name="garuda.cbuentry.label.testresult"></s:text>
									           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
									           	    <s:iterator value="postTestList" status="innerrow">
									           	    <s:if test="fktestid==#request.TNCUncFkTestId">
									           	    <s:if test="#request.cellCountFlag==0">
									           	    <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
									           	   <input type="text" style="width:45%" id="savePostTestList<s:property value="%{#row.index}"/>testresult"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')"/>
											       	<s:hidden  value="%{testresult}" id="savePostTestList%{#row.index}testresult_dbStoredVal"/>
											       	<br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
											       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
											       	<s:set name="cellCountFlag" value="1" scope="request"/>
											       	</s:if></s:if>
											       	</s:iterator>
											       </s:if>
												   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
												   	<s:iterator value="postTestList" status="innerrow">
									           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
									           	    <s:if test="#request.cellCountFlag==0">
									           	    <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>
									           	   	
											       	<input type="text" style="width:45%" id="savePostTestList<s:property value="%{#row.index}"/>testresult"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')"/>
											       	<s:hidden  value="%{testresult}" id="savePostTestList%{#row.index}testresult_dbStoredVal"/>
											       	<br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
											       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
											       	<s:set name="cellCountFlag" value="1" scope="request"/>
											       	</s:if></s:if>
											       	</s:iterator>
												   	<s:if test="#request.cellCountFlag==0">
												   	<s:iterator value="postTestList" status="innerrow">
									           	    <s:if test="fktestid==#request.TNCUncFkTestId">
									           	    <s:if test="#request.cellCountFlag==0">
									           	    <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
											       	<input type="text" style="width:45%" id="savePostTestList<s:property value="%{#row.index}"/>testresult"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')"/>
											       	<s:hidden  value="%{testresult}" id="savePostTestList%{#row.index}testresult_dbStoredVal"/>
											       	<br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
											       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
											       	<s:set name="cellCountFlag" value="1" scope="request"/>
											       	</s:if></s:if>
											       	</s:iterator>
												   	</s:if>
											       	</s:elseif>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										      </s:if></s:if>
 									     </s:if>
									     <s:elseif test="pkLabtest==fktestid &&!(pkLabtest == #request.TNCFkTestId) && (labtestName!='NAT HIV' && labtestName!='NAT HCV')">
									        <s:if test="#request.valuePostFkTimingTest==fkTimingOfTest">
									        	<s:set name="checkEmpty" value="1" scope="request"/>  
									       <%--  <%postcount++;%> --%>
									     <%--   <%=postcount %>--%>
									         <%-- post<s:property value="fkTimingOfTest" /> --%>
									          
									          <s:set name="check1" value="%{'check'}"/>
									         <%-- <%if(postcount>precount){%>
									         <td>&nbsp;</td> 
									         <%} %> --%>
									         		
												 <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>
												 <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Viability' || labtestName=='Total CD34+ Cell Count' || labtestName=='CBU nRBC Absolute Number' || labtestName=='CBU nRBC %' || labtestName=='CFU Count' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'"></s:if>
												   <%-- <s:if test=""></s:if>
												   <s:if test=" "></s:if>
												   <s:if test=""></s:if>
												   <s:if test=""></s:if>
												  <s:if test=""></s:if> --%>
												  <s:else></s:else>
												<%-- 
												   <s:textfield name="savePostTestList[%{#row.index}].testresult" value="%{testresult}" size="10" theme="simple" />
												
											       --%>
											      <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      	&nbsp;
											      </s:if>
											      <s:else>
											      <s:text name="garuda.cbuentry.label.testresult"></s:text>
											      	
												  <input type="text" style="width:45%" id="savePostTestList<s:property value="%{#row.index}"/>testresult"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" <s:if test="cdrCbuPojo.site.isCDRUser()==true && labtestName=='Viability'">onchange="autoDeferViab(this.value,this.id); isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');"</s:if><s:if test="labtestName=='CFU Count'">onchange="autoDeferCFU(this.value,this.id); isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');"</s:if> onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');"  onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')" />
										           <s:hidden  value="%{testresult}" id="savePostTestList%{#row.index}testresult_dbStoredVal"/>
										           <br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
										           <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
												   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
												   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
												   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
												   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
												   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
												    <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
												   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Viability'">%</s:if>
												   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   </s:else>
												 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL">
												    <div id="savePostTestListcfuCount"> 
												    <br><s:text name="garuda.common.lable.method"></s:text>
												    <s:select name="savePostTestList[%{#row.index}].fktestmethod" id="cfuSelected"  value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="cfuCount(this.value);onChngFldValdNDepFldVald(this.id,this.value,'cfuDescOther','pkOther','savePostTestList12testresult'); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
												   	 <s:hidden value="%{fktestmethod}" id="cfuSelected_dbStoredVal"/>
													<br/><span style="display:none" id="cfuSelected_error" class="error"><s:text name=""></s:text></span> 
												   	 </div>
												   <div id="cfuCount"> 
													   <s:text name="garuda.common.lable.describe"></s:text><span style="color: red;">*</span>
													  <span>&nbsp;&nbsp;</span><s:textfield name="savePostTestList[%{#row.index}].testMthdDesc" maxlength="50" value="%{testMthdDesc}" id="cfuDescOther" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');"  onkeypress="validate();" onkeyup="chkDepOnChange(this.id,this.value)"></s:textfield>
														<s:hidden value="%{testMthdDesc}" id="cfuDescOther_dbStoredVal"/>
														<br/><span style="display:none" id="cfuDescOther_error" class="error"><s:text name=""></s:text></span>
													</div>
												 </s:if>
												   <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
													    <div id="savePostTestListviability"> 
													    <br><s:text name="garuda.common.lable.method"></s:text>
													    <s:select name="savePostTestList[%{#row.index}].fktestmethod" id="viabSelected" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value);onChngFldValdNDepFldVald(this.id,this.value,'viaDescOther','pkViaOther','savePostTestList13testresult');isChangeDone(this,'%{fktestmethod}','labChanges');"><br></s:select>
												        <s:hidden value="%{fktestmethod}" id="viabSelected_dbStoredVal"/>
														<br/><span style="display:none" id="viabSelected_error" class="error"><s:text name=""></s:text></span>
												        </div>
												      <div id="viability"> 
														<br><s:text name="garuda.common.lable.describe"></s:text><span style="color: red;">*</span>
														 <span>&nbsp;&nbsp;</span><s:textfield name="savePostTestList[%{#row.index}].testMthdDesc" maxlength="50" id="viaDescOther" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeypress="validate" onkeyup="chkDepOnChange(this.id,this.value)"></s:textfield>
														<s:hidden value="%{testMthdDesc}" id="viaDescOther_dbStoredVal"/>
														<br/><span style="display:none" id="viaDescOther_error" class="error"><s:text name=""></s:text></span>
													  </div>													 
												   </s:if>
												   <s:if test="#request.testCount>#request.labGroupTypTests">
												   	<s:text name="garuda.common.label.sampType"></s:text>
												  	<s:select name="savePostTestList[%{#row.index}].fktestspecimen" id="savePostTestList%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
												   <br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
												   </s:if>
									            
									        </s:if>
									        
									       </s:elseif>
									       
									   </s:iterator>
									  
									     <s:if test="#request.checkEmpty==0 && postTestList.size()>=#request.labGroupTypTests">
					               		 		&nbsp;
					               		</s:if>  
					               		<s:if test="#request.testCount<=#request.labGroupTypTests"> 
									     <s:if test="%{#check1!='check'}">
									    <s:if test="pkLabtest == #request.TNCFkTestId">
									         		<s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
									         				<s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													</s:if>
													<s:else>
												 			<s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>  
												  	</s:else>
												   <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												   <s:text name="garuda.cbuentry.label.testresult"></s:text>
									           	   <input type="text" style="width:45%" id="savePostTestList<s:property value="%{#row.index}"/>testresult"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" onchange="isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')"/>
									           	   <s:hidden  value="%{testresult}" id="savePostTestList%{#row.index}testresult_dbStoredVal"/>
									           	   <br/><span style="display:none" id="savePostTestList%{#row.index}testresult_error" class="error"><s:text name=""></s:text> </span>
									           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
													<s:text name="garuda.labtest.label.uncrctd"></s:text>
											       </s:if>
												   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
											       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
											       	</s:elseif>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										 
 									     </s:if>
 									     <s:elseif test="!(pkLabtest == #request.TNCFkTestId) && (labtestName!='NAT HIV' && labtestName!='NAT HCV')">
									           	   <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
												   <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Viability' || labtestName=='Total CD34+ Cell Count' || labtestName=='CBU nRBC Absolute Number' || labtestName=='CBU nRBC %' || labtestName=='CFU Count' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'"></s:if>
												   <%-- <s:if test=""></s:if>
												   <s:if test=" "></s:if>
												   <s:if test=""></s:if>
												   <s:if test=""></s:if>
												  <s:if test=""></s:if> --%>
												  <s:else><span>&nbsp;&nbsp;</span></s:else>
												   <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      	&nbsp;
											      </s:if>
											      <s:else>
											      <s:text name="garuda.cbuentry.label.testresult"></s:text>
												<input type="text" style="width: 45%"
													id="savePostTestList<s:property value="%{#row.index}"/>testresult"
													name="savePostTestList[<s:property value="%{#row.index}"/>].testresult"
													value="<s:property value="%{testresult}"/>" size="10"
													theme="simple"
													<s:if test="cdrCbuPojo.site.isCDRUser()==true && labtestName=='Viability'">onchange="autoDeferViab(this.value,this.id); isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');"</s:if>
													<s:if test="labtestName=='CFU Count'">onchange="autoDeferCFU(this.value,this.id); isChangeDone(this,'<s:property value="%{testresult}"/>','labChanges');"</s:if>
													onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')" />
												<s:hidden  value="%{testresult}" id="savePostTestList%{#row.index}testresult_dbStoredVal"/>
										           <br/><span style="display:none" id="savePostTestList%{#row.index}testresult_error" class="error"><s:text name=""></s:text> </span>
										           <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										 		   <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;10^7</s:if>   
												   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
												   
												   <s:if test="labtestName=='CBU nRBC %' ">%</s:if>
												   <s:if test="labtestName=='% of CD34+  Viable' ">%</s:if>
												   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count' ">%</s:if>
												   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count' ">%</s:if>
												    <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
												   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												    <s:if test="labtestName=='Viability'">%</s:if>
												    <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   </s:else>
											    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL">
												  <div id="savePostTestListcfuCount"> 
												  <br><s:text name="garuda.common.lable.method"></s:text>
												     <span>&nbsp;&nbsp;</span><s:select name="savePostTestList[%{#row.index}].fktestmethod" id="cfuSelected" value="%{fktestmethod}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="cfuCount(this.value);onChngFldValdNDepFldVald(this.id,this.value,'cfuDescOther','pkOther','savePostTestList12testresult'); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
												    	<s:hidden value="%{fktestmethod}" id="cfuSelected_dbStoredVal"/>
														<br/><span style="display:none" id="cfuSelected_error" class="error"><s:text name=""></s:text></span>
												    </div>  	
												  <div id="cfuCount">   
													 <br><s:text name="garuda.common.lable.describe"></s:text><span style="color: red;">*</span>
													 <span>&nbsp;&nbsp;</span> <s:textfield name="savePostTestList[%{#row.index}].testMthdDesc" maxlength="50" value="%{testMthdDesc}" id="cfuDescOther" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeypress="validate();" onkeyup="chkDepOnChange(this.id,this.value)"></s:textfield>
										             <s:hidden value="%{testMthdDesc}" id="cfuDescOther_dbStoredVal"/>
														<br/><span style="display:none" id="cfuDescOther_error" class="error"><s:text name=""></s:text></span>
										          </div>		
										        </s:if>
												
												<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
										              <div id="savePostTestListviability"> 
										              <br><s:text name="garuda.common.lable.method"></s:text>
										               <br><s:select name="savePostTestList[%{#row.index}].fktestmethod" id="viabSelected" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value);onChngFldValdNDepFldVald(this.id,this.value,'viaDescOther','pkViaOther','savePostTestList13testresult'); isChangeDone(this,'%{fktestmethod}','labChanges');"><br></s:select>
												   		<s:hidden value="%{fktestmethod}" id="viabSelected_dbStoredVal"/>
														<br/><span style="display:none" id="viabSelected_error" class="error"><s:text name=""></s:text></span>
												   	</div>
												   <div id="viability"> 
												       <s:text name="garuda.common.lable.describe"></s:text><span style="color: red;">*</span>
												       <span>&nbsp;&nbsp;</span><s:textfield name="savePostTestList[%{#row.index}].testMthdDesc" maxlength="50" id="viaDescOther" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeypress="validate" onkeyup="chkDepOnChange(this.id,this.value)"></s:textfield>
												 	 	<s:hidden value="%{testMthdDesc}" id="viaDescOther_dbStoredVal"/>
														<br/><span style="display:none" id="viaDescOther_error" class="error"><s:text name=""></s:text></span>
												  </div>
												</s:if>
									        	<s:if test="#request.testCount>#request.labGroupTypTests">
												   	<s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePostTestList[%{#row.index}].fktestspecimen" id="savePostTestList%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
												  	<div id="notReq"></div>
												  	<br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
												  </s:if></s:elseif>
									      </s:if>
									      </s:if>
									     </td>
											<s:if test="otherPostTestList!=null && otherPostTestList.size()>0 ">					  
													<s:set name="checkEmpty" value="0" scope="request"/>
													<s:set name="cellCountFlag" value="0" scope="request"/>
													<td Class="BasicText">
													<s:iterator value="otherPostTestList">
													  <s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherFkTimingTest==fkTimingOfTest">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check2" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
															
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestList" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
											           	    <s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestList" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
												           	     
														       	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
														       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
														       	 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
														       	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														     </s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestList" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>
											           	     
													       	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
													       	  <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       		
													       	</s:if>
													       	</s:elseif>		
													       		<s:if test="#request.cellCountFlag==1">
													       			<div id="showHardDrop1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																		<s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason"  id="saveOtherPostTestList1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																		 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																		<s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																		</div>
																									 	
																	    <s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   		<s:text name="garuda.common.lable.describe"></s:text>
																	   		<s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeypress="validate"></s:textfield>
																   			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																   			</div></s:if>
																		</div>
													       		</s:if>							   
											      			</s:if></s:if>
 									    				 </s:if>
 									    				 
								    				 
 									    				 
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
														<s:if test="#request.valueOtherFkTimingTest==fkTimingOfTest">
														 <s:set name="checkEmpty" value="1" scope="request"/>
														 <s:set name="check2" value="%{'check'}"/>
																	<s:set name="test" value="pkLabtest" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:set name="abc" value="%{#row.index}" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
																	 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	
																	 	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" cssClass="width:45%" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
																	 	 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	
																	 <s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
																	 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
														           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																   <s:if test="labtestName=='Viability'">%</s:if>						 
																   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																																 
																		<div id="showHardDrop1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if> >											
																		      <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason"  id="saveOtherPostTestList1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			<div id="reasonFrTestDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>						 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   		<s:text name="garuda.common.lable.describe"></s:text>
																	   		<s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																   			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																   			</div></s:if>
																   		</div></s:else>
																		     
																        <%--   
																							  
																	  <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																          <s:text name="garuda.common.lable.viability"></s:text>
																          <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		  <s:text name="garuda.common.lable.describe"></s:text>
																		  <s:textfield name="saveOtherPostTestList1[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	 </s:if>
																 --%>
											 				 </s:if>
												 				 
											 				</s:elseif>
											 				
										 				</s:iterator>
										 				  <s:if test="#request.checkEmpty==0 && otherPostTestList.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if> 
										               	<s:if test="#request.testCount<=#request.labGroupTypTests"> 
										 		    	<s:if test="%{#check2!='check'}">
										 		        <s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		<s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
															
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
														  	
											           	   	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/> 
													       	  <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																		<s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason"  id="saveOtherPostTestList1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																		<s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																		</div>
																									 	
																	    <s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   		<s:text name="garuda.common.lable.describe"></s:text>
																	   		<s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																   			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																   			</div></s:if>
																   		</div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	bdvghbsxfvds
																	 	<s:property value="%{#row.index}"></s:property>
																	 	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" cssClass="width:45%" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
																	 	 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     						<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     				</s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																
																 <s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" cssClass="width:45%" value="%{testresult}" size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
																		 <s:hidden  value="%{testresult}" id="saveOtherPostTestList1%{#row.index}testresult_dbStoredVal"/>
																		 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>								 
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="test" value="pkLabtest" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 
												                    <div id="showHardDrop1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																		<s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason"  id="saveOtherPostTestList1%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																		<s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');"></s:textfield>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																		</div>
																									 	
																	    <s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   		<s:text name="garuda.common.lable.describe"></s:text>
																	   		<s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value); isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeypress="validate"></s:textfield>
																   			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																   			</div></s:if>
																   		</div></s:else>
													               <%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																       <s:text name="garuda.common.lable.viability"></s:text>
																       <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																	   <s:text name="garuda.common.lable.describe"></s:text>
																	   <s:textfield name="saveOtherPostTestList1[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																 </s:if> --%>
														</s:elseif>     	 
											        </s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										     </s:if>
										     
										     
											<s:if test="otherPostTestListFirst!=null && otherPostTestListFirst.size()>0">
												<s:set name="checkEmpty" value="0" scope="request"/>					  
												<td Class="BasicText">
												<s:iterator value="otherPostTestListFirst">
												<s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestFirst==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check3" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValue%{#row.index}"></s:hidden>
														     <div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
														  
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListFirst" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
													       	  <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestListFirst" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	   	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
														       	   <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
														       	  <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														    </s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListFirst" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
													       	   <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
													       	  <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>
													       		<s:if test="#request.cellCountFlag==1">
													       			<div id="showHardDrop2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		 
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																      
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason"  id="saveOtherPostTestList2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeypress="validate"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																			<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">					 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								    <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																	   		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																	   		</div></s:if>
																	   </div>
													       		</s:if>
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
														 <s:if test="#request.valueOtherPostTestFirst==otherListValue">
														 	<s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check3" value="%{'check'}"/>
																
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValue%{#row.index}"></s:hidden>
																	 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" cssClass="width:45%" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw'); isChangeDone(this,'%{testresult}','labChanges');"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	   <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" cssClass="width:45%" value="%{testresult}"  size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw'); isChangeDone(this,'%{testresult}','labChanges');"></s:textfield> 
																		 <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>						      
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																      <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																			
																	 <div id="showHardDrop2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		
																			  <s:text name="garuda.common.label.reaForTest"></s:text>															     
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason"  id="saveOtherPostTestList2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">					 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								    <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}testMthdDesc"  value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																  			<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																  			</div></s:if>
																		</div></s:else>
																
																	<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																           <s:text name="garuda.common.lable.viability"></s:text>
																           <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		   <s:text name="garuda.common.lable.describe"></s:text>
																		   <s:textfield name="saveOtherPostTestList2[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	 </s:if>
																 --%>
																
											 				 </s:if>
											 				 
											 				</s:elseif>
											 					
										 				</s:iterator>
										 				 <s:if test="#request.checkEmpty==0 && otherPostTestListFirst.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>  
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check3!='check'}">
										 		    	<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		 
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																      
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason"  id="saveOtherPostTestList2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">					 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								    <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																	   		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																	   		</div></s:if>
																	   </div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" cssClass="width:45%" value="%{testresult}"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" cssClass="width:45%" value="%{testresult}" size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw');"  >  </s:textfield>
																		 <s:hidden  value="%{testresult}" id="saveOtherPostTestList2%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>		
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>			
																		
																	 <div id="showHardDrop2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		 
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																      
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason"  id="saveOtherPostTestList2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">					 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								    <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeypress="validate"></s:textfield>
																	   		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																	   		</div></s:if>
																	   </div></s:else>    
																		     
																	 <%-- 
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList2[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if> --%>
													          </s:elseif>
													          </s:if></s:if></td> <s:set name="cellCountFlag" value="0" scope="request"/>
										                  </s:if>	     
										     
										    
		                         <s:if test="otherPostTestListSecond!=null && otherPostTestListSecond.size()>0 ">
		                         	<s:set name="checkEmpty" value="0" scope="request"/>					  
												<td Class="BasicText">
												<s:iterator value="otherPostTestListSecond">
												 <s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestSecond==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check4" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListSecond" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   		<s:iterator value="otherPostTestListSecond" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
														       	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
														       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
														       	 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														       	</s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListSecond" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden> 
													       	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>
													       		<s:if test="#request.cellCountFlag==1">
													       			<div id="showHardDrop3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason"  id="saveOtherPostTestList3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
															           		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
															           		</div></s:if>
															           </div>
													       		</s:if>												   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
												 	<s:if test="#request.valueOtherPostTestSecond==otherListValue">
												 		<s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check4" value="%{'check'}"/>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
											                         	
																	 <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValue%{#row.index}"></s:hidden>
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	   <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" ></s:textfield> 
																		 <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>									       
																       
																       <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																		
																		 <div id="showHardDrop3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>	
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																														     
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason"  id="saveOtherPostTestList3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																										 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
															           		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
															           		</div></s:if>
															           	</div></s:else>
															
																		<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList3[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if> --%>
																 
											 				</s:if>
											 			 </s:elseif>	
										 				</s:iterator>
										 				 <s:if test="#request.checkEmpty==0 && otherPostTestListSecond.size()>=#request.labGroupTypTests">
										               		 		&nbsp; 
										               	</s:if>  
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		   		 <s:if test="%{#check4!='check'}">
										 		    		<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason"  id="saveOtherPostTestList3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
															           		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
															           		</div></s:if>
															           </div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																    <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%" size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" >  </s:textfield>
																		 <s:hidden  value="%{testresult}" id="saveOtherPostTestList3%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>							 
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												                     <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
												                      
												                      <div id="showHardDrop3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason"  id="saveOtherPostTestList3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
															           		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
															           		</div></s:if>
															           </div></s:else>
																		<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList3[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	    </s:if> --%>
											                 </s:elseif>
											                  </s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										               </s:if>	     

					<s:if test="otherPostTestListThird!=null && otherPostTestListThird.size()>0 ">	
					<s:set name="checkEmpty" value="0" scope="request"/>				  
												<td Class="BasicText">
												<s:iterator value="otherPostTestListThird">
												  <s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestThird==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check5" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValue%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListThird" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   		<s:iterator value="otherPostTestListThird" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
														       	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
														       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
														       	 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       <s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														       	</s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListThird" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
															<s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
															<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
															<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>
													       		<s:if test="#request.cellCountFlag==1">
													       			<div id="showHardDrop4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>      
																	        <s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason"  cssClass="dependantOnFldRequired" id="saveOtherPostTestList4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" cssClass="dependantOnFldRespRequired" maxlength="50" id="saveOtherPostTestList4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" cssClass="dependantOnFldRequired" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" cssClass="dependantOnFldRequired" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" cssClass="dependantOnFldRequired" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc"  maxlength="50" cssClass="dependantOnFldRespRequired" id="saveOtherPostTestList4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
														                	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
														                	</div></s:if>
														                </div>
													       		</s:if>												   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
												  		<s:if test="#request.valueOtherPostTestThird==otherListValue">
														  <s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check5" value="%{'check'}"/>
																 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValue%{#row.index}"></s:hidden>
																	 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" ></s:textfield> 
																		<s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																		<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
																        <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																		<s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		<s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		<s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		<s:if test="labtestName=='CBU nRBC %'">%</s:if>
																		<s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																		<s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		<s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																		<s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		<s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		<s:if test="labtestName=='Viability'">%</s:if>
																		<s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>				     
																     <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																			
																	<div id="showHardDrop4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <s:text name="garuda.common.label.reaForTest"></s:text>																																															     
																			  <s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason"  id="saveOtherPostTestList4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
														                	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
														                	</div></s:if>
														                	</div></s:else>
																	  <%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																           <s:text name="garuda.common.lable.viability"></s:text>
																           <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		   <s:text name="garuda.common.lable.describe"></s:text>
																		   <s:textfield name="saveOtherPostTestList4[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if> --%>
																
											 				  </s:if>
											 				 </s:elseif>			
										 				</s:iterator>
										 				  <s:if test="#request.checkEmpty==0 && otherPostTestListThird.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>  
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    	<s:if test="%{#check5!='check'}">
										 		    	<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>      
																	        <s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason"  id="saveOtherPostTestList4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
														                	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
														                	</div></s:if>
														                </div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%" size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     				</s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%" size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"></s:textfield>
																		<s:hidden  value="%{testresult}" id="saveOtherPostTestList4%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>								 
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												                     <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	        
																	  <div id="showHardDrop4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>      
																	        <s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason"  id="saveOtherPostTestList4%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
														                	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
														                	</div></s:if>
														                </div></s:else>
																	<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList4[%{#row.index}].notes" value="%{notes}" maxlength="50" onkeypress="validate"></s:textfield>
																	   </s:if> --%>
											              </s:elseif>
											              </s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										         </s:if>	     
			
								<s:if test="otherPostTestListFourth!=null && otherPostTestListFourth.size()>0 ">					  
												<s:set name="checkEmpty" value="0" scope="request"/>
												<td Class="BasicText">
												<s:iterator value="otherPostTestListFourth">
												  <s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestFourth==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check6" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValue%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListFourth" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop5%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestListFourth" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
														       	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop5%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
														       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
														       	 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														       	</s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListFourth" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop5%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>
													       		<s:if test="#request.cellCountFlag==1">
													       			<div id="showHardDrop5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>	
																			  <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason"  id="saveOtherPostTestList5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																</div>
															</s:if>											   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
														<s:if test="#request.valueOtherPostTestFourth==otherListValue">
														  <s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check6" value="%{'check'}"/>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValue%{#row.index}"></s:hidden>
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw'); "> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	  <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop5%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"></s:textfield> 
																		 <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
																		<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>									
																		<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																		poiliyhutgyujtg
																		<div id="showHardDrop5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			 <s:text name="garuda.common.label.reaForTest"></s:text>
																			 <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason"  id="saveOtherPostTestList5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																		</div></s:else>
																	<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].notes" value="%{notes}" maxlength="50" onkeypress="validate"></s:textfield>
																		 	 
																	   </s:if>
																 --%>
											 				</s:if>
											 			  </s:elseif>
										 				</s:iterator>
										 				  <s:if test="#request.checkEmpty==0 && otherPostTestListFourth.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>  
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check6!='check'}">
										 		   		<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop5%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       <div id="showHardDrop5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>	
																			  <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason"  id="saveOtherPostTestList5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																		</div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																 <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}"  cssClass="width:45%" size="10" theme="simple"  onchange="vidSaved('showHardDrop5%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')">  </s:textfield>
																	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList5%{#row.index}testresult_dbStoredVal"/>
																	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
														           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																   <s:if test="labtestName=='Viability'">%</s:if>
																   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>								 
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												                     <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
												
																	<div id="showHardDrop5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>	
																			  <s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason"  id="saveOtherPostTestList5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>					 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																		</div></s:else>
																		   
	
																	 <%--    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																             <s:text name="garuda.common.lable.viability"></s:text>
																             <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if> --%></s:elseif>
											         </s:if> </s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										     </s:if>	     
			
							
										     
								<s:if test="otherPostTestListFifth!=null && otherPostTestListFifth.size()>0 ">					  
										<s:set name="checkEmpty" value="0" scope="request"/>
										<td Class="BasicText">
										<s:iterator value="otherPostTestListFifth">
											<s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId || fktestid==#request.TNCUnkIfUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestFifth==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check7" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValue%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   <s:if test="cdrCbuPojo.isSystemCord=='yes'||cdrCbuPojo.isSystemCord==null">
											           	    <s:iterator value="otherPostTestListFifth" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	  <s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       </s:if>
														   <s:elseif test="cdrCbuPojo.isSystemCord=='no'">
														   	<s:iterator value="otherPostTestListFifth" status="innerrow">
												           	    <s:if test="fktestid==#request.TNCUncFkTestId">
												           	    <s:if test="#request.cellCountFlag==0">
												           	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
														       	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
														       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
														       	 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
														       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
														       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
														       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
														       	<s:set name="cellCountFlag" value="1" scope="request"/>
														       	</s:if></s:if>
														     </s:iterator>
													       	<s:if test="#request.cellCountFlag==0">
													       	<s:iterator value="otherPostTestListFifth" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUnkIfUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUnkIfUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.unkwnifuncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>
													       	</s:if>
													       	</s:elseif>
													       		 <s:if test="#request.cellCountFlag==1">
													       		 	<div id="showHardDrop6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason"  id="saveOtherPostTestList6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																		</div>
													       		 </s:if>												   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
												 		<s:if test="#request.valueOtherPostTestFifth==otherListValue">
														<s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check7" value="%{'check'}"/>
																
																
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValue%{#row.index}"></s:hidden>
																	 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{testMthdDesc}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	 <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" ></s:textfield> 
																	<s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
																	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
														           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																   <s:if test="labtestName=='Viability'">%</s:if>
																   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>						    
																    <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																		
																	<div id="showHardDrop6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason"  id="saveOtherPostTestList6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																			</div></s:else>
																		<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																             <s:text name="garuda.common.lable.viability"></s:text>
																             <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	    </s:if>
																 --%>
											 			 </s:if>
											 	    </s:elseif>
										 	</s:iterator>
										 	  <s:if test="#request.checkEmpty==0 && otherPostTestListFifth.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if> 
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check7!='check'}">
										 		    	<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
													       	 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason"  id="saveOtherPostTestList6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																	</div>
													       
													   		<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
											      			
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">
											      						&nbsp;
											     				</s:if>
											     				<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
																	 	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     				<s:else>
																 <div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" size="10" cssClass="width:45%"  theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value); isChangeDone(this,'%{testresult}','labChanges');" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" >  </s:textfield>
																	<s:hidden  value="%{testresult}" id="saveOtherPostTestList6%{#row.index}testresult_dbStoredVal"/>
																	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>	
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>											
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												                      <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
												
												                        <div id="showHardDrop6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason"  id="saveOtherPostTestList6%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestreason}','labChanges');"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onchange="isChangeDone(this,'%{reasonTestDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id); isChangeDone(this,'%{fktestspecimen}','labChanges');"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id); isChangeDone(this,'%{fktestmethod}','labChanges');"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onchange="isChangeDone(this,'%{testMthdDesc}','labChanges');" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																			</div></s:else>
																		    
											         					
											         					 <%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																             <s:text name="garuda.common.lable.viability"></s:text>
																             <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	    </s:if> --%></s:elseif>
											            </s:if></s:if>  </td><s:set name="cellCountFlag" value="0" scope="request"/>
										      </s:if>	     
                                        </tr>
                                       <s:if test="#request.testCount==#request.labGroupTypTests && processingTest.size()>#request.labGroupTypTests">
                                        <tr> <td class="headcolCri">
                                        <div
											class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
											style="text-align: left;">
											 <s:text name="garuda.cordentry.label.otherLabTests"/>
										</div>
                                       </td> 
                                       <td colspan="<%=headerCount%>">
                                        <div
											class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
											style="text-align: left;">
											 <%-- <s:text name="garuda.cordentry.label.otherLabTests"/>  --%>
										</div>
                                       </td>
                                        </tr></s:if>
                                        <% testCount=testCount+1; %> 
                                       <s:set name="check" value="%{'uncheck'}"/> 
                                        <s:set name="check1" value="%{'uncheck'}"/>
                                        <s:set name="check2" value="%{'uncheck'}"/>
                                        <s:set name="check3" value="%{'uncheck'}"/>
                                        <s:set name="check4" value="%{'uncheck'}"/>
                                        <s:set name="check5" value="%{'uncheck'}"/>
                                        <s:set name="check6" value="%{'uncheck'}"/>
                                        <s:set name="check7" value="%{'uncheck'}"/> 
								  </s:iterator>
						     </tbody>
					      </table>      
						</td>
					</tr>
					<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0" >
						<tr>
						<td class="headcolCri">
                         </td>
                          <td colspan="<%=headerCount%>-1" style="border: 0px;">
                           </td>
                           </tr>
						</table>
					</td>
                    </tr>
				</table></div></td>
                   </tr>
				</table> 	
			<table >
					  <tr>
						  		<td align="left">
									<input type="button" id="addnewtest"
										value="<s:text name="garuda.cdrcbuview.label.button.addNewTest" />"
										onclick="javascript:addTest('<s:property value="cdrCbuPojo.cordID" />','compScrFlag');" />
								</td>
			</tr></table>
			</div>
			<script>
			$j(function(){
				if('<s:property value="cdrCbuPojo.site.isCDRUser()"/>'=="true")
	            {
	            $j('#preTestDateStrnM').addClass('cbuLabSummMandatory');
	            $j('#postTestDateStrnM').addClass('cbuLabSummMandatory');
	            $j('#savePreTestList0testresult').addClass('cbuLabSummMandatory');
	            $j('#savePostTestList1testresult').addClass('cbuLabSummMandatory');
	           $j('#hemoglobinScrnTest').addClass('buLabSummMandatory');
	            $j('#datepicker4').addClass('cbuLabSummMandatory');
	            $j('#datepicker5').addClass('cbuLabSummMandatory');
	            $j('#cdrCbuPojo_aboBloodType').addClass('cbuLabSummMandatory');
	            $j('#cdrcburhtype').addClass('cbuLabSummMandatory');
	            $j('#bacterialResult').addClass('cbuLabSummMandatory');
	            $j('#fungalResultModal').addClass('cbuLabSummMandatory');
	                 
	            }
				if('<s:property value="cdrCbuPojo.fungalResult"/>'=='<s:property value="#request.fungalCulnotdone"/>'&& '<s:property value="#request.assesDoneFrFung"/>'=='false'){
					$j('#fungalResultModal').addClass('addAssessmentMandatory');
					$j('#fungalResultModal').removeClass('cbuLabSummMandatory');
				}
				else{
					$j('#fungalResultModal').removeClass('addAssessmentMandatory');
				}
				if(('<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.alphaThalismiaTraitPkVal"/>'||'<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.hemozygousNoDonePkVal"/>'||'<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.hemozygousUnknownPkVal"/>'||'<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.hemozygousNoDiesasePkVal"/>'||'<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.hemoAlphaThalassmiaPkVal"/>'||'<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.hemoTraitPkVal"/>' ||'<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.hemoMultiTraitPkVal"/>')&& '<s:property value="#request.assesDoneFrHemo"/>'=='false'){
					$j('#hemoglobinScrnTest').addClass('addAssessmentMandatory');
					$j('#hemoglobinScrnTest').removeClass('cbuLabSummMandatory');
				}
				else{
					$j('#hemoglobinScrnTest').removeClass('addAssessmentMandatory');
				}
				
			});
			$j('#status .cbuLabSummMandatory').each(function(){
				 $j(this).css("border", "");
				 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
					 $j(this).css("border", "1px solid red");
					
					}
					 
				  });
			$j('.addAssessmentMandatory').each(function(){
				if((('<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.alphaThalismiaTraitPkVal"/>'||'<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.hemozygousNoDonePkVal"/>'||'<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.hemozygousUnknownPkVal"/>'||'<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.hemozygousNoDiesasePkVal"/>'||'<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.hemoAlphaThalassmiaPkVal"/>'||'<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.hemoTraitPkVal"/>' ||'<s:property value="cdrCbuPojo.hemoglobinScrn"/>'=='<s:property value="#request.hemoMultiTraitPkVal"/>')&& '<s:property value="#request.assesDoneFrHemo"/>'=='false')||('<s:property value="cdrCbuPojo.fungalResult"/>'=='<s:property value="#request.fungalCulnotdone"/>'&& '<s:property value="#request.assesDoneFrFung"/>'=='false')){
					$j(this).css("border", "");
					 $j(this).css("border", "1px solid red");
					 $j("#"+this.id+"_error").show();
					$j("#"+this.id+"_error").text("<s:text name="garuda.label.dynamicform.assessmentmsg"/>");
				}
				});
			
			var updateStatusCriDiv = true;	
			function autoDeferStatus(param, labSummChangesTemp) {
				var returnFlag = true; 
				
				 if(labSummChangesTemp){
					 if($j("#hemoglobinScrnTest").val()==$j("#alphaThalismiaTraitPkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDiesasePkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoAlphaThalassmiaPkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoTraitPkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDonePkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemozygousUnknownPkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoMultiTraitPkVal").val() || $j("#hemoglobinScrnTest").val() == $j("#multTrait").val()){
				    		updateStatusDiv=true;
				    		returnFlag = true;
				    		$j("#autodeferfieldflag").val("true");
	    				 }					 
				    	else if(('<s:property value="cdrCbuPojo.bacterialResult"/>' != $j("#bacterialResult").val()) && ($j("#bacterialResult").val()==$j("#bacterialPositive").val() || $j("#bacterialResult").val()==$j("#bacterialNotDone").val())){
				    		  updateStatusCriDiv = false;
							  returnFlag =false;
	   					      $j("#autodeferfieldflag").val("true");
	   					      bactAutoDefer($j('select[name="cdrCbuPojo.bacterialResult"]:last').val(),$j('select[name="cdrCbuPojo.bacterialResult"]:last').attr('id'),'bactDateText','bactDateStrId','bactDate');
	 				      }
				    	else if(('<s:property value="cdrCbuPojo.fungalResult"/>' != $j("#fungalResultModal").val()) && ($j("#fungalResultModal").val()==$j("#bacterialPositiveFung").val())){	
							  updateStatusCriDiv = false;
							  returnFlag =false;
		   					  $j("#autodeferfieldflag").val("true");
		   					  fungAutoDefer($j('select[name="cdrCbuPojo.fungalResult"]:last').attr('id'),'','','');
		   				 }  
				    	else if(('<s:property value="cdrCbuPojo.hemoglobinScrn"/>' != $j("#hemoglobinScrnTest").val()) && ($j("#hemoglobinScrnTest").val()==$j("#hemoBeatThal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoSickBeta").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoSickCell").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoAlphaSev").val()/* || $j("#hemoglobinScrnTest").val()==$j("#multTrait").val()*/)){
		  		   		     updateStatusCriDiv = false;
							 returnFlag =false;
 	   					 $j("#autodeferfieldflag").val("true");
 	   					autoDeferHemoGloScrn($j('select[name="cdrCbuPojo.hemoglobinScrn"]:last').val(),$j('select[name="cdrCbuPojo.hemoglobinScrn"]:last').attr('id'));	
					     }
				    	else if((document.getElementById('savePostTestList12testresult').defaultValue != $j('input[name="savePostTestList[12].testresult"]:last').val()) && (($j('input[name="savePostTestList[12].testresult"]:last').val())!='' && ($j('input[name="savePostTestList[12].testresult"]:last').val())==0)){	   					      
							 updateStatusCriDiv = false;
							 returnFlag =false;
	   					     autoDeferCFU($j('input[name="savePostTestList[12].testresult"]:last').val(),$j('input[name="savePostTestList[12].testresult"]:last').attr('id'));	
	   					     $j("#autodeferfieldflag").val("true");
					     }
				    	else if((document.getElementById('savePostTestList13testresult').defaultValue != $j('input[name="savePostTestList[13].testresult"]:last').val()) && (($j('input[name="savePostTestList[13].testresult"]:last').val())!='' && ($j('input[name="savePostTestList[13].testresult"]:last').val())>=0 && ($j('input[name="savePostTestList[13].testresult"]:last').val())<85)){
						     updateStatusCriDiv = false;
						     returnFlag =false;
						     autoDeferViab($j('input[name="savePostTestList[13].testresult"]:last').val(),$j('input[name="savePostTestList[13].testresult"]:last').attr('id'));
	   					     $j("#autodeferfieldflag").val("true");
						}
				    	  else{
				    		$j("#autodeferfieldflag").val("false");
					      }
			    	   }
			       return returnFlag;
					}
				
			
			</script>