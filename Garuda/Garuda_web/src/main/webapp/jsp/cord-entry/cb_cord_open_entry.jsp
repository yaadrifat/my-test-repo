<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.eres.web.site.SiteJB" %>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.address.AddressJB" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.ordercomponent.util.VelosGarudaConstants" %>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<jsp:include page="../cb_track_session_logging.jsp" />
<script>
var calcCbuNbrcFlag=true;
var autoDefer = false;
var textareaValue = "";
var additionalIdCount = 0; 
var maxadditionalidcount=0;
var callFrom = "";
var procesingInfoElements = new Array();
var procesingInfoElementsVal = new Array();
var assessLinknFlag_Id = new Array();
var urlArr = new Array();
var divArr = new Array();
var LoadedPage={
		labSummary:false,
		clinicalNote:false,
		cbuHla:false,
		processingProc:false,
		mrq:false,
        fmhq:false,
        idm:false
};

var HlaLoadedObject = {hlaMandatoryField:0,hlaCount:0,percentage:0};

function queueManager(url,divname){
  urlArr.push(url);
  divArr.push(divname);  
}


function loadAsyncRequest_1(url,divname){
	$j.ajax({
	        type: "GET",
	        url: url,
	        async:true,
	        success: function (result){	      
		    	 $j('.ui-datepicker').html("");
		         var $response=$j(result);
		         //query the jq object for the values
		         var errorpage = $response.find('#errorForm').html();
		         if(errorpage != null &&  errorpage != "" ){
		         	$j("#main").html(result);
		         }else{
		         	$j("#"+divname).html(result);
		       }
                 if(urlArr.length>0){
		          loadAsyncRequest_1(urlArr.pop(),divArr.pop());
                 }else{                	
                	 onloadWholePage();                	 
                 }
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }
		});
}

function loadAsyncRequest_2(url,divname){  
	$j.ajax({
	        type: "GET",
	        url: url,
	        async:true,
	        success: function (result){	      
		    	 $j('.ui-datepicker').html("");
		         var $response=$j(result);
		         //query the jq object for the values
		         var errorpage = $response.find('#errorForm').html();
		         if(errorpage != null &&  errorpage != "" ){
		         	$j("#main").html(result);
		         }else{
		         	$j("#"+divname).html(result);
		       }
                 if(urlArr.length>0){
		          loadAsyncRequest_2(urlArr.pop(),divArr.pop());
                 }else{                	
                	 onloadWholePage();
                 }
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }
	});	
}

function onloadWholePage(){
	hideMrqFmhqProgress();	
}

function hideMrqFmhqProgress(){
	showforms();//for hide progressbar of fmhq, mrq
}
function loadDivWithFormSubmitDefer(url,divname,formId){
	
	if(callFrom!="" && callFrom=="notes")
		{
		 var clinicalFlag=true;
			//clinicalFlag = clinicalValidation();
			
			var visibleToFlag=0;
			var categoryName = $j("#fkNotescategory option:selected").text();
			var flagForLater= $j('#flagForLater:checked').val();
			var categoryId = $j("#fkNotescategory option:selected").val();
			var noteKeyword = $j("#keyword").val();
			var flagComment = $j("#commentNote").val();
			if(flagComment!=null)
				{
				$j("#comment").hide();
				$j("#commentText").hide();
				}
			var assessment = $j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val();
			var assessmentId;
			if(assessment=='no_cause')
				{
				assessmentId = assessment;
				assessment = 'No Cause For Deferral';
				}
				else
					{
					assessmentId = assessment;
					assessment = 'Defer';
					}
			var visibleToTc = $j('#visibileToTC:checked').val();
			if(visibleToTc == "true")
				{
				visibleToTc= "<s:text name="garuda.clinicalnote.label.visibleToTc" />";
				visibleToFlag = "true";
				}
			else
				{
				visibleToTc= "<s:text name="garuda.clinicalnote.label.notvisibleToTc" />";
				visibleToFlag = "false";
				}
			var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
		 	var value = field.GetHTML(true);
		 	
			
		}
	$j('#progress-indicator').css( 'display', 'block' );  
	 $j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
        	//$('.tabledisplay').html("");
            var $response=$j(result);
            //query the jq object for the values
            var errorpage = $response.find('#errorForm').html();
           // alert(oneval);
           // var subval = $response.find('#sub').text();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).html(result);
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('#progress-indicator').css( 'display', 'none' );	
	jQuery("#modalEsign1").dialog("destroy");
}
function submitCordForSearch(url,divname,formId){
	showprogressMgs();  
	 $j.ajax({
        type: "POST",
        url: url,
        async:false,
        data : $j("#"+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
            var $response=$j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            	closeprogressMsg();
            }else{
            	$j("#"+divname).html(result);
            	closeprogressMsg();
            }
        },
        error: function (request, status, error) {
        	closeprogressMsg();
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
}
var atuoDeferEsignId = "";
var ckTextField = "";
var ckTextFieldId ="";
var radioField = "";
function cancelAutoDeferEsign(){	
	  if(AutoDeferChange.elementType=='select'/*ckTextField!="" && ckTextField==true*/){
			$j(AutoDeferChange.element).val(AutoDeferChange.elementInitialVal);		    
		    AutoDeferChange.elementProgress();
		    AutoDeferChange.elementFocus();		    
		}
		else if(AutoDeferChange.elementType=='text'/*ckTextField!="" && ckTextField==false*/){
			$j(AutoDeferChange.element).val(AutoDeferChange.elementInitialVal);
			AutoDeferChange.elementProgress();
		    AutoDeferChange.elementFocus();		    
		}else if(radioField!="" && radioField==true){
		     if(atuoDeferEsignId!=""){
		     	$j("#"+atuoDeferEsignId).attr('checked',false);
			    $j("#"+atuoDeferEsignId).focus();
			    parent.document.getElementById(atuoDeferEsignId).focus();			
		  }
        }
	 if($j(AutoDeferChange.element).attr("id")=='bacterialResult'){
     	 bacterial(AutoDeferChange.elementInitialVal);
     }
     else if($j(AutoDeferChange.element).attr("id")=='fungalResult'){
     	fungal(AutoDeferChange.elementInitialVal);
     	if(assessLinknFlag_Id[0]=='true'){
    		$j("."+assessLinknFlag_Id[1]).show();
    		$j("."+assessLinknFlag_Id[2]).hide();
    	}
     }
     else if($j(AutoDeferChange.element).attr("id")=='hemoglobinScrnTest' && assessLinknFlag_Id[0]=='true'){
 		$j("."+assessLinknFlag_Id[1]).show();
 		$j("."+assessLinknFlag_Id[2]).hide();
 	}
	jQuery("#modalEsign1").dialog("destroy");
    AutoDeferChange.gc();
	validateSign("","submitcdrsearch1");
	$j("#modalEsign1").css('display',"none");
	$j("#cordIdEntryValid1").css('display',"none");
	$j("#cordIdEntryMinimum1").css('display',"none");
	$j("#cordIdEntryPass1").css('display',"none");	
}

function commonMethodForSaveAutoDefer(id){
	$j("#modalEsign1").dialog(
			   {autoOpen: false,
				resizable: false,
				closeText: '',
				closeOnEscape: false ,
				modal: true, width : 700, height : 100,
				close: function() {
					//$(".ui-dialog-content").html("");
					//jQuery("#subeditpop").attr("id","subeditpop_old");
				jQuery("#modalEsign1").dialog("destroy");
				     		
			    }
			   }
			  ); 
  $j("#modalEsign1").dialog().parents(".ui-dialog").find(".ui-dialog-titlebar").remove();
  $j("#modalEsign1").dialog("open");
}

var AutoDeferChange={
		element:"",
		elementType:"",
        elementInitialVal:"",
        elementCurrentVal:"",
        elementWidgetFieldKeysArr:"",        
        elementWidgetFieldValsArr:"",
        elementWidgetProgClass:"",
        elementProgress:function(){
            var elementBeforeChangeVal="",count=0,mandatoryCount=0;
            var widgetProgressParams= new Array();
        	var key = $j(this.element).attr("id");
        	var currVal= $j(this.element).val();
        	for(var j=0; j<this.elementWidgetFieldKeysArr.length;j++){
        		if(this.elementWidgetFieldKeysArr[j]==key){
        			elementBeforeChangeVal=this.elementWidgetFieldValsArr[j];
        			this.elementWidgetFieldValsArr[j]=currVal;
        			break;
        		}
        	}
        	   widgetProgressParams=WidProgCountGetter(this.elementWidgetProgClass);
        	   count=widgetProgressParams[0];
        	   mandatoryCount=widgetProgressParams[2];
        		if((elementBeforeChangeVal=="" || elementBeforeChangeVal==-1 || elementBeforeChangeVal==null) && currVal!="" && currVal!=-1){
        			count++;
        			val = parseInt((count*100)/(mandatoryCount));
        			$j("#"+widgetProgressParams[1]).progressbar({
        			    value: val
        			}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
        			addTotalCount();
        			}else if((elementBeforeChangeVal!="" && elementBeforeChangeVal!=-1) && (currVal=="" || currVal==-1)){
        				count--;
        				val = parseInt((count*100)/(mandatoryCount));
        				$j("#"+widgetProgressParams[1]).progressbar({
        				    value: val
        				}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
        				minusTotalCount();
        				} 
        		WidProgCountSetter(this.elementWidgetProgClass,count);
        	},
        	deferElement:function(param){
            	        this.element=param.objectElement;
            	        this.elementType=param.objectType;
	                    this.elementInitialVal=param.objectInitVal;
	                    this.elementCurrentVal=param.objectCurrVal;
	                    this.elementWidgetFieldKeysArr=param.objectWidKeyArr;
	                    this.elementWidgetFieldValsArr=param.objectWidValArr;
	                    this.elementWidgetProgClass=param.objectWidProgClass;
        	},
        	elementFocus:function(){
            	 document.getElementById($j(AutoDeferChange.element).attr('id')).focus();
        	},
        	gc:function(){
            	this.element="";
            	this.elementType="";
            	this.elementInitialVal="";
                this.elementCurrentVal="";
                this.elementWidgetFieldKeysArr="";        
                this.elementWidgetFieldValsArr="";
                this.elementWidgetProgClass="";
            	
        	}             
}
function prepareParam(element,className,keyArr,valArr){
	  var type="";
	  if(($j(element).attr('type')).lastIndexOf('-') > -1)
	         type=($j(element).attr('type')).substring(0,($j(element).attr('type')).lastIndexOf('-'));
	  else
	    	 type=$j(element).attr('type');
      var objectParam={
		   objectElement:element,
		   objectType:type,
		   objectInitVal:AutoDeferElementInitVal[$j(element).attr('name')],
		   objectCurrVal:$j(element).val(),
		   objectWidKeyArr:keyArr,
		   objectWidValArr:valArr,
		   objectWidProgClass:className
     }
      return objectParam;
}
function WidProgCountGetter(className){
	  var returnVal=new Array();
	  switch(className){
	       case "cbuclass":
	    	   returnVal[0]= cbucount;
	    	   returnVal[1]= 'cbuinfobar';
	    	   returnVal[2]=noOfMandatoryCBUField;
	    	   break;
	       case "procclass":
	    	   returnVal[0]= proccount;
	    	   returnVal[1]= 'processinginfobar';
	    	   returnVal[2]=noOfMandatoryProcField;
	    	   break;
	       case "labclass":
	    	   returnVal[0]= labsummarycount;
	    	   returnVal[1]= 'labsummarybar';
	    	   returnVal[2]=noOfMandatoryLabSummaryField;
	    	   break;
	       case "eligibclass":
	    	   returnVal[0]= eligibilitycount;
	    	   returnVal[1]= 'eligibilitybar';
	    	   returnVal[2]=noOfMandatoryEligibilityField;
	    	   break;	       	   	   
	  }
	  return returnVal;
}
function WidProgCountSetter(className,val){
	 switch(className){
          case "cbuclass":
        	  cbucount= val;
 	          break;
          case "procclass":
        	  proccount= val;
 	          break;
          case "labclass":
        	  labsummarycount= val;
 	          break;
          case "eligibclass":
        	  eligibilitycount= val;
 	          break;          	   	   
      }
}
function commonAutodefer(Id,val,className,element,elementKeyArray,elementValArray){	 
	if(Id=="multiplebirthid" || Id=="hemoglobinScrnTest"){
	    var param = prepareParam(element,className,elementKeyArray,elementValArray);
	    AutoDeferChange.deferElement(param); 
	}
	var flag = false;
	jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
			function(r) {
				if (r == false) {			         						
					if(Id=="multiplebirthid" || Id=="hemoglobinScrnTest"){
						 $j("#"+Id).val(AutoDeferElementInitVal[$j(element).attr('name')]);
						 var elementValue=AutoDeferElementInitVal[$j(element).attr('name')];
						 if(elementValue==$j("#alphaThalismiaTraitPkVal").val() || elementValue==$j("#hemozygousNoDiesasePkVal").val() || elementValue==$j("#hemoAlphaThalassmiaPkVal").val() || elementValue==$j("#hemoTraitPkVal").val() || elementValue==$j("#hemozygousNoDonePkVal").val() || elementValue==$j("#hemozygousUnknownPkVal").val() || elementValue==$j("#hemoMultiTraitPkVal").val()){
							   $j("#showassesmentHemo").show();
							   $j("#hideassesmentHemo").hide();
						   }
					     AutoDeferChange.elementProgress();
					     AutoDeferChange.gc();					     										
					  }else
						$j("#"+Id).val('-1');				 						 
			            $j("#"+Id).removeAttr("checked");
			            $j("#"+Id).focus();			         		         
				  }else if(r == true){
					    var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
					 	var value = field.GetHTML(true);
					 	$j("#esignAutoDefer").val("");
					  	$j('#sectionContentsta').val(value);
					    $j("#cordSearchable").val("1");					    
					    commonMethodForSaveAutoDefer();	
					    atuoDeferEsignId = Id; 
					    var elementValue=AutoDeferElementInitVal[$j(element).attr('name')];
					    if(elementValue==$j("#alphaThalismiaTraitPkVal").val() || elementValue==$j("#hemozygousNoDiesasePkVal").val() || elementValue==$j("#hemoAlphaThalassmiaPkVal").val() || elementValue==$j("#hemoTraitPkVal").val() || elementValue==$j("#hemozygousNoDonePkVal").val() || elementValue==$j("#hemozygousUnknownPkVal").val() || elementValue==$j("#hemoMultiTraitPkVal").val()){
							  assessLinknFlag_Id[0]= 'true';
							  assessLinknFlag_Id[1]= 'LAB_SUM_Hemo_showassess';
							  assessLinknFlag_Id[2]= 'LAB_SUM_Hemo_hideassess';
						   }
				  }
	});	
	return flag;	
}

function fungAutoDefer(Id,datetextId,dateId,assessId,className,element){
  if ($j("#fungalResult").val()==$j("#fungalCulpostive").val()){
	    var param = prepareParam(element,className,labSummaryElements,labSummaryElementVal);
	    AutoDeferChange.deferElement(param);
	  jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
				function(r) {
					if (r == false) {
						 $j("#"+Id).val(AutoDeferElementInitVal[$j(element).attr('name')]);							
					     AutoDeferChange.elementProgress();
					     AutoDeferChange.gc();					   
					     $j("#"+datetextId).hide();
					     $j("#"+dateId).hide();
					     $j("#"+assessId).hide();
					     fungal(AutoDeferElementInitVal[$j(element).attr('name')])
					     var elementValue=AutoDeferElementInitVal[$j(element).attr('name')];
					     if(elementValue==$j("#fungalCulnotdone").val()){
							   $j("#showassesmentFung").show();
							   $j("#hideassesmentFung").hide();
						   }
					     $j("#"+Id).focus();
					  }else if(r == true){
						  var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
						 	var value = field.GetHTML(true);
						  	$j('#sectionContentsta').val(value);
						    $j("#cordSearchable").val("1");	  
						    commonMethodForSaveAutoDefer();
						    var elementValue=AutoDeferElementInitVal[$j(element).attr('name')];
						    if(elementValue==$j("#fungalCulnotdone").val()){
								  assessLinknFlag_Id[0]= 'true';
								  assessLinknFlag_Id[1]= 'LAB_SUM_Fung_showassess';
								  assessLinknFlag_Id[2]= 'LAB_SUM_Fung_hideassess';
							   }
						    atuoDeferEsignId = Id; 						
					  }
	});
  }
}
function bactAutoDefer(Val,Id,datetextId,dateId,className,element){
	 if(Val==$j("#bacterialNotDone").val() || Val ==$j("#bacterialPositive").val()){
		   var param = prepareParam(element,className,labSummaryElements,labSummaryElementVal);
		   AutoDeferChange.deferElement(param);
		  jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
					function(r) {
						if (r == false) {
							$j("#"+Id).val(AutoDeferElementInitVal[$j(element).attr('name')]);			
						     AutoDeferChange.elementProgress();
						     AutoDeferChange.gc();
						   $j("#"+datetextId).hide();
						   $j("#"+dateId).hide();
						   bacterial(AutoDeferElementInitVal[$j(element).attr('name')]);
						   $j("#"+Id).focus();
						  // $j('select#bacterialResult.labclass').trigger('change');
						  }else if(r == true){
							  var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
							 	var value = field.GetHTML(true);
							  	$j('#sectionContentsta').val(value);
							    $j("#cordSearchable").val("1");	  
							    commonMethodForSaveAutoDefer();
							    atuoDeferEsignId = Id; 						
						  }
		});
	  }
		}

function autoDeferHEMO(Val,Id,className,element){
   if(Val==$j("#hemoBeatThal").val() || Val==$j("#hemoSickBeta").val() || Val==$j("#hemoSickCell").val() || Val==$j("#hemoAlphaSev").val()){
	   commonAutodefer(Id,Val,className,element,labSummaryElements,labSummaryElementVal);	
	   }
  }

function autoDeferForMultiplePregnancy(val,Id,className,element){
	if(val=="1"){
        commonAutodefer(Id,val,className,element,cbuInfoElement,cbuInfoElementVal);
    }
}

function textFunction(divId,textAreaId,saveButtonId,savedComment)
{
		$j("#"+divId).show();
		textareaValue=document.getElementById(textAreaId).value;
		if(savedComment!=null && savedComment!=''){
			$j("#"+textAreaId).attr('readonly',true);
			$j("#"+saveButtonId).hide();
		}
}

function commentSave(divId,textAreaId,addComment)
{
		textareaValue=document.getElementById(textAreaId).value;
		if(textareaValue==''){
				$j("#"+addComment).text("Add Comment");
				
			}
		else{
				$j("#"+addComment).text("Add/View Comment");
			}
		$j("#"+divId).hide();
}

function commentClose(divId,textAreaId)
{
		document.getElementById(textAreaId).value = textareaValue;
		$j("#"+divId).hide();
}

function bacterial(value)
{
	if(value==$j("#bacterialPositive").val() || value==$j("#bacterialNegative").val())
	{
		$j("#bactDateText").show();
		$j("#bactDate").show();
	}else{
		$j("#bactDateText").hide();
		$j("#bactDate").hide();
		$j("#datepicker6").val(null);
	}
	if(value!="<s:property value='cdrCbuPojo.bacterialResult'/>" && (value!=$j("#bacterialPositive").val() || value!=$j("#bacterialNegative").val())){
		$j("#datepicker6").val(null);
	}else{
		$j("#datepicker6").val(document.getElementById("datepicker6").defaultValue);
	}
		$j("#dropValue").val(value);
		if($j("#datepicker6").val()!=""){
			$j(".placeHolderOfid_datepicker6").hide();
		}else{
			$j(".placeHolderOfid_datepicker6").show();
		}
}

function addTestDate(value){
if($j("#preTestDateStrn").val()==""){
	autoFilled= true;
	$j("#preTestDateStrn").val(value);
	if($j("#preTestDateStrn").val()!=""){
		$j(".placeHolderOfid_preTestDateStrn").hide();
	}else{
		$j(".placeHolderOfid_preTestDateStrn").show();
	}
  }
 }
function addTestDateScnd(value){
if($j("#postTestDateStrn").val()==""){
	autoFillee= true;
	$j("#postTestDateStrn").val(value);
	if($j("#postTestDateStrn").val()!=""){
		$j(".placeHolderOfid_postTestDateStrn").hide();
	}else{
		$j(".placeHolderOfid_postTestDateStrn").show();
	}
}
}

function fungal(value){
	if(value==$j("#bacterialPositiveFung").val() || value==$j("#bacterialNegativeFung").val()){
		$j("#fungDateText").show();
		$j("#fungDate").show();
	}
	else{
		$j("#fungDateText").hide();
		$j("#fungDate").hide();
		$j("#datepicker7").val(null);
	}
	if(value!="<s:property value='cdrCbuPojo.fungalResult'/>" && (value!=$j("#bacterialPositiveFung").val() || value!=$j("#bacterialNegativeFung").val())){
		$j("#datepicker7").val(null);
	}else{
		$j("#datepicker7").val(document.getElementById("datepicker7").defaultValue);
	}
	$j("#dropValueFung").val(value);
	if($j("#datepicker7").val()!=""){
		$j(".placeHolderOfid_datepicker7").hide();
	}else{
		$j(".placeHolderOfid_datepicker7").show();
	}
}

function checkCordEntryFormat(value){
	
	  return (((value.indexOf("-")===4) && (value.lastIndexOf("-")===9) && (value.length===11))?true:false);
	  
}

   var sumTotal =  0;
   var totalcount = 0;
   var cbucount = 0;
   var idcount = 0;
   var licensecount = 0;
   var proccount = 0;
   var labsummarycount = 0;
   var mrqcount = 0;
   var fmhqcount = 0;
   var idmcount = 0;
   var hlacount = 0;
   var eligibilitycount = 0;
   var clinicalnotecount = 0;
  var noOfMandatoryField = 0;
   var noOfMandatoryCBUField = 0;
   var noOfMandatoryIDField = 0;
   var noOfMandatoryLicensureField = 0;
   var noOfMandatoryProcField = 0;
   var noOfMandatoryLabSummaryField = 0;
   var noOfMandatoryMrqField = 1;
   var noOfMandatoryFmhqField = 1;
   var noOfMandatoryIdmField = 1;
   var noOfMandatoryHlaField = 0;
   var noOfMandatoryEligibilityField = 0;
   var noOfMandatoryClinicalField = 0;
   var addedCBUField = "";
   var addedField = "";
   var addedProcField = "";
   var addedLabSummaryField = "";
   var addedHlaField = "";
   var addedEligibilityField = "";
   var addedClinicalField = "";
   var onFocusProcVal = -1;
   var formPerFlag = false;
   var cbuInfoElement = new Array(); 
   var cbuInfoElementVal=new Array();
   var labSummaryElements =new Array();
   var labSummaryElementVal = new Array();
   var eligibilityElement=new Array();
   var eligibilityElementVal=new Array();
	
   $j(document).ready(function(){
		
		 $j('#word_count').each(function(){
		     //maximum limit of characters allowed.
		     var maxlimit = 300;
		     // get current number of characters
		     var length = $j(this).val().length;
		     if(length >= maxlimit) {
		   $j(this).val($j(this).val().substring(0, maxlimit));
		   length = maxlimit;
		  }
		     // update count on page load
		     $j(this).parent().find('#counter').html( (maxlimit - length) + ' characters left');
		     // bind on key up event
		     $j(this).keyup(function(){
		  // get new length of characters
		  var new_length = $j(this).val().length;
		  if(new_length >= maxlimit) {
		    $j(this).val($j(this).val().substring(0, maxlimit));
		    //update the new length
		    new_length = maxlimit;
		   }
		  // update count
		  $j(this).parent().find('#counter').html( (maxlimit - new_length) + ' characters left');
		     });
		 });
	});


	  

   
   $j(function(){
	   
	   $j(".positive").numeric({ negative: false }, function() { 
	    this.value = ""; this.focus(); });
	   $j("#searchResults").dataTable();
	   $j("#totalbar").progressbar({
			value: 0
		}).children('.ui-progressbar-value').html('0%');
	   $j("#idinfobar").progressbar({
			value: 0
		}).children('.ui-progressbar-value').html('0%');
	   $j("#cbuinfobar").progressbar({
			value: 0
		}).children('.ui-progressbar-value').html('0%');
	   $j("#licensurebar").progressbar({
			value: 0
		}).children('.ui-progressbar-value').html('0%');
	   $j("#processinginfobar").progressbar({
			value: 0
		}).children('.ui-progressbar-value').html('0%');
	   $j("#labsummarybar").progressbar({
			value: 0
		}).children('.ui-progressbar-value').html('0%');
	   $j("#MRQbar").progressbar({
			value: 0
		}).children('.ui-progressbar-value').html('0%');
	   $j("#FMHQbar").progressbar({
			value: 0
		}).children('.ui-progressbar-value').html('0%');
	   $j("#IDMbar").progressbar({
			value: 0
		}).children('.ui-progressbar-value').html('0%');
	   $j("#hlabar").progressbar({
			value: 0
		}).children('.ui-progressbar-value').html('0%');
	   $j("#eligibilitybar").progressbar({
			value: 0
		}).children('.ui-progressbar-value').html('0%');
	//   $j("#clinicalnotesbar").progressbar();
	  

		 
		 getDatePic();
		 assignDatePicker('datepicker2'); 
		 assignDatePicker('datepicker3');
		 assignDatePicker('datepicker4');
		 assignDatePicker('datepicker5');		
	//	 jQuery('#datepicker2').datepicker('option', { beforeShow: customRanges });//}//for collection date
	//	 jQuery('#datepicker3').datepicker('option', { beforeShow: customRanges });//}//for birth date	
	//	 jQuery('#datepicker4').datepicker('option', { beforeShow: customRanges });//}//for processing date	
	//	 jQuery('#datepicker5').datepicker('option', { beforeShow: customRanges });//}//for freez date         
			 showforms();
							
		   $j(".cancel").click(function(){
			   validator.resetForm();	
			});

		$j(".timeValid").keypress(function(e) {
			  var val = $j(this).val();
			  if(e.keyCode!=8 && e.keyCode!=46 )
			  {
				  if(val.length==2){						  			  
					  if(e.which!=58){
						   $j(this).val(val+":");
					  }
				  }
			  }
		});		

		$j(document).keypress(function(e){
			var keynum; 
	        if(window.event) 
	        { 
	            keynum = e.keyCode;
	        }else if(e.which)
	        { 
	            keynum = e.which;
	        }
	        if(keynum == 13){  
	        	e.preventDefault(); 
	        }
		});		
   });

   function jQueryValidationInitializer(){

	   jQuery.validator.addMethod("checkSelectCordEntry", function(value, element) {
		    if($j("#cordSearchable").val()=='0'){
               return true;
			}else{
				return (true && (parseInt(value) != -1));
			}			
		}, "<s:text name='garuda.cordentry.PlsSel'/>");/*}, "Please Select Value");*****/
		
		jQuery.validator.addMethod("validateRequiredCordEntry", function(value, element,params) {
	        var flag = true;
	        if($j("#cordSearchable").val()=='0'){
	        	return true;
	        }else{
	        	if(($j("#"+params[0]).val()=="" || $j("#"+params[0]).val()==undefined) && value==""){
					flag =false;
				}
			}
			return (true && flag);		 
		}, "<s:text name='garuda.cordentry.M_PlsEtr_IsbtData'/>");/*}, "Please Enter Valid ISBT Data");*****/

		jQuery.validator.addMethod("validateRequiredByNameCordEtnry", function(value, element,params) {
	        var flag = true;
	        if($j("#cordSearchable").val()=='0'){
	        	return true;
	        }else{
	        	if($j("#"+params[0]).val()==undefined){
					return true;
		        }
				if($j("#"+params[0]).val()!="" && value==""){
					flag =false;
				}
		    }
			return (true && flag);		 
		}, "Please Enter Data");	
		 
		jQuery.validator.addMethod("checkSelectByNameCordEntry", function(value, element,params) {
			var flag = true;
			if($j("#cordSearchable").val()=='0'){
				return true;
			}else{
				if($j("#"+params[0]).val()==undefined){
					return true;
				}
				if($j("#"+params[0]).val()!="" && (parseInt(value) == -1)){
		                flag = false;
			    }
			}
			return (true && flag);
		}, "<s:text name='garuda.cordentry.PlsSel'/>");/*}, "Please Select Value");*****/		
		
		
		jQuery.validator.addMethod("checkDatesCordEntry", function(value, element, params) {
			if($j("#cordSearchable").val()=='0' ){
				if(value==""){
                  return true;
				}
				else if(($j("#"+params[0]).val()!=undefined && $j("#"+params[0]).val()!="") && ($j("#"+params[1]).val()!=undefined && $j("#"+params[1]).val()!=""))
				 {
					return (true && (checkDates(params[0],params[1])));
			     }
				else
				{			
						return true;
				}
			}else
			{			
				return (true && (checkDates(params[0],params[1])));
		    }			
		}, "<s:text name='garuda.cordentry.M_EtrVal_XxxxFrmt'/>");/*}, "Please Check The Date You Entered");*****/

		jQuery.validator.addMethod("checkHourcordentry", function(value, element, params) {
			if($j("#cordSearchable").val()=='0'){
				if(value==""){
	                   return true;
					}else
				return (true && (checkHour(params[0],params[1])));
		     }else{
			    return (true && (checkHour(params[0],params[1])));	
		     }	 
		}, "<s:text name='garuda.cordentry.M_EtrVal_XxxxFrmt'/>");/*}, "Please Check The Hour You Entered");*****/
	     /**************** ID widget validation mehode *******************/
		jQuery.validator.addMethod("validateValues1", function(value, element,params) {
		if($j("#"+params[2]).val()==value){
           return true;
		 }else if($j("#"+params[0]).val()=="" && $j("#"+params[1]).val() ==""){
			 return true;
		 }
		 else{
			return (true && (validateValues(params[0],params[1])));
		 }		 
	 }, "");/*}, "");*****/
	 
	 jQuery.validator.addMethod("checkdbdata11", function(value, element, params) {
		if($j("#"+params[2]).val()==value){
           return true;
		 }else{
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
		 return (true && (validateDbData(url)));
		 }
	}, "");/*}, "Please Enter Valid Data");*****/	

	jQuery.validator.addMethod("checkcbbdbdata11", function(value, element, params) {
		if($j("#"+params[2]).val()==value){
           return true;
		 }else{
			    var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[3]+"&entityValue1="+$j("#fkCbbId").val();
		        if(true && (validateDbAdditionalData(url))){
			       return true; 
			   }
		        else{
			        var url1 = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
					if(true && (validateDbAdditionalData(url1))){
					    return false;
					}
					else{  return true;
					}			   
			   }
		 }
	}, "");/*}, "Please Enter Valid Data");*****/ 

	 jQuery.validator.addMethod("checkdbdata01", function(value, element, params) {
		 if($j("#"+params[2]).val()==value){
           return true;
		 }else{
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
		 return (true && (validateDbData(url)));
		 }
	}, "");/*}, "Please Enter Valid Data");*****/

	jQuery.validator.addMethod("checkdbadditionaldata1", function(value, element, params) {
		if($j("#"+params[3]).val()==value){
           return true;
		 }else{
			 if($j(element).attr("id")=='matlocalid'){
		           value=value.replace(/&/g,"%26");}
			 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField2="+params[2]+"&entityValue2="+$j("#regMaternalId").val()+"&entityField1="+params[4]+"&entityValue1="+$j("#fkCbbId").val();
			 var url1 = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[4]+"&entityValue1="+$j("#fkCbbId").val();
			 if(value==""){return true;}else{
				if(validateDbAdditionalData(url)){
					return true;
				}else{				
					return validateDbData(url1);
				} 		  
			 }		
		 }
	}, "");/*}, "Please Enter Valid Data");*****/

	jQuery.validator.addMethod("checklocalcbuidadditionaldbdata01", function(value, element, params) {
		if($j("#"+params[3]).val()==value){
           return true;
		 }else{
			 if($j(element).attr("id")=='loccbuid'){
		           value=value.replace(/&/g,"%26");}
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[2]+"&entityValue1="+$j("#fkCbbId").val();
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}	
		 }
	}, "");/*}, "Please Enter Valid Data");*****/

	jQuery.validator.addMethod("checklocalcbuidadditionaldbdata11", function(value, element, params) {
		if($j("#"+params[3]).val()==value){
           return true;
		 }else{
			 if($j(element).attr("id")=='loccbuid' || $j(element).attr("id")=='matlocalid'){
		           value=value.replace(/&/g,"%26");}
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[2]+"&entityValue1="+$j("#fkCbbId").val();
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}	
		 }
	}, "");/*}, "Please Enter Valid Data");*****/

	jQuery.validator.addMethod("checkisbtdbdata1", function(value, element,params) {
		if($j("#"+params[2]).val()==value){
           return true;
		 }else{
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&"+$j.param({'entityValue':value})+"&entityField1=fkCbbId&entityValue1="+$j("#fkCbbId").val();
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}		
		 } 
	}, "");/*}, "Please Enter Valid ISBT Data");*****/

	jQuery.validator.addMethod("checkisbtdata1", function(value, element,params) {
		if($j("#"+params[1]).val()==value){
           return true;
		 }else{
		 var url = "getCodeValidate?refnum="+params[0]+"&"+$j.param({'codeval':value});
		 if(value==""){return true;}
		 else{ return (true && (validateDbData(url)));}		 
		 }
	}, "");/*}, "Please Enter Valid ISBT Data");*****/

	jQuery.validator.addMethod("checkcordentryformat", function(value, element) {
		 return (true && (checkCordEntryFormat(value)));		 
	}, "Please Enter Value in xxxx-xxxx-x format");	

	jQuery.validator.addMethod("hlaformat", function(value, element) {
		  if(value!="" && value.indexOf(":")==-1){
           return false;
	      }else{
           return true;
		  }		 		 
	}, "Please Enter Value in xx:xx or xx:xxx format");	
	
	jQuery.validator.addMethod("checkregcbbdbdata", function(value, element, params) {
		 if($j("#"+params[2]).val()==value){
          return true;
		 }else{
			 if(nonSystemCordStatus() && $j(element).attr("id")=='cburegid' || $j(element).attr("id")=='regMaternalId'){
	              value=value.replace(/&/g,"%26");}		
			 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[3]+"&entityValue1="+$j("#fkCbbId").val();
		 return (true && (validateDbData(url)));
		 }
	}, "");/*}, "Please Enter Valid Data");*****/
	jQuery.validator.addMethod("checkdbmatregcbbdbdata", function(value, element, params) {
		 if($j("#"+params[2]).val()==value){
         return true;
		 }else{
			 if(nonSystemCordStatus() && $j(element).attr("id")=='cburegid' || $j(element).attr("id")=='regMaternalId'){
	              value=value.replace(/&/g,"%26");}
			 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value+"&entityField1="+params[3]+"&entityValue1="+$j("#fkCbbId").val();
		 return (true && (validateDbData(url)));
		 }
	}, "");/*}, "Please Enter Valid Data");*****/
	jQuery.validator.addMethod("checkselect", function (value, element, params) {
		if($j("#cordSearchable").val()=='0'){			
                  return true;
		}
		else{
		return (true && (parseInt(value) != -1));}
		}, "Please Select Value");
	
	
var validator = $j("#cordentryform1").validate({
			//onfocusout:false,
			invalidHandler: function(form, validator) {
		        var errors = validator.numberOfInvalids();
		        if (errors) {
		            validator.errorList[0].element.focus();
		        }
		    },
		    errorPlacement: function(error, element) {
			    if(element.next().next().hasClass('custom')){
			    	element.next().next().remove();
				   if ( element.is(":radio") )
					   error.insertAfter(element.next().next().next().next());
				  else 
		            error.insertAfter( element);
			    }else{
			    	if ( element.is(":radio") )
			    	error.insertAfter(element.next().next().next().next());
					  else 
			            error.insertAfter( element);
			    }	
			    },
			rules:{	
					"cbuLicStatus":{checkSelectCordEntry : true},
					/*"licenseReasons":{
						validateRequiredCordEntry:{
			            			depends: function(element) {
											return $j("#licenseid").val()==$j("#unLicensedPk").val();
										}
								}
		             },*/
					"cdrCbuPojo.fkCbbId":{checkSelectCordEntry : true},
					"cdrCbuPojo.registryId":{required:true,checkspecialchar :{depends: function(element){ return !(nonSystemCordStatus());}},checklength:{depends: function(element){ return !(nonSystemCordStatus());}},checkcordentryformat:{depends: function(element){	return !(nonSystemCordStatus());}},validateValues1:["cburegid","regMaternalId","cbucordregid"],checksum :{depends: function(element){return !(nonSystemCordStatus());}},checkdbdata11:{param:["CdrCbu","registryId","cbucordregid"],depends: function(element){return !(nonSystemCordStatus());}},checkdbdata01:{param:["CdrCbu","registryMaternalId","cbucordregid"],depends: function(element){return !(nonSystemCordStatus());}},checkregcbbdbdata:{param:["CdrCbu","registryId","cbucordregid","fkCbbId"],depends:function(element){return nonSystemCordStatus();}},checkdbmatregcbbdbdata:{param:["CdrCbu","registryMaternalId","cbucordregid","fkCbbId"],depends:function(element){return nonSystemCordStatus();}}},
	                "cdrCbuPojo.registryMaternalId":{required:{depends: function(element){ return !(nonSystemCordStatus());}},checkspecialchar : {depends: function(element){ return !(nonSystemCordStatus());}},checklength:{depends: function(element){ return !(nonSystemCordStatus());}},checkcordentryformat:{depends: function(element){ return !(nonSystemCordStatus());}},validateValues1:["cburegid","regMaternalId","regcordmatid"],checksum :{depends: function(element){ return !(nonSystemCordStatus());}},checkcbbdbdata11:{param:["CdrCbu","registryMaternalId","regcordmatid","fkCbbId"],depends: function(element){return !(nonSystemCordStatus());}},checkdbdata01:{param:["CdrCbu","registryId","regcordmatid"],depends: function(element){return !(nonSystemCordStatus());}},checkregcbbdbdata:{param:["CdrCbu","registryId","regcordmatid","fkCbbId"],depends:function(element){return nonSystemCordStatus();}},checkdbmatregcbbdbdata:{param:["CdrCbu","registryMaternalId","regcordmatid","fkCbbId"],depends:function(element){return nonSystemCordStatus();}}},
					"cdrCbuPojo.localMaternalId":{validateValues1:["matlocalid","loccbuid","localcordmatid"],checkdbadditionaldata1:["CdrCbu","localMaternalId","registryMaternalId","localcordmatid","fkCbbId"],checklocalcbuidadditionaldbdata11:["CdrCbu","localCbuId","fkCbbId","localcordmatid"]},
					"cdrCbuPojo.localCbuId":{required:
						{
												depends: function(element){
														return !($j("#loccbuid").val()=="" && (nonSystemCordStatus()));
													}
												},validateValues1:["matlocalid","loccbuid","localcordcbuid"],checklocalcbuidadditionaldbdata01:["CdrCbu","localMaternalId","fkCbbId","localcordcbuid"],checklocalcbuidadditionaldbdata11:["CdrCbu","localCbuId","fkCbbId","localcordcbuid"]},
					"cdrCbuPojo.cordIsbiDinCode":{required:
												{
													depends: function(element){
															return ($j("#loccbuid").val()=="" && !(nonSystemCordStatus()));
														}
													},checkisbtdbdata1:["CdrCbu","cordIsbiDinCode","isbidincordcode"],checkisbtdata1:["001","isbidincordcode"]},
					"cbuOnBag":{checkselect:true},													
					"cdrCbuPojo.fkCbbProcedure":{checkSelectCordEntry : true},
					"cdrCbuPojo.fkCBUDeliveryType":{checkSelectCordEntry : true},
					"cdrCbuPojo.fkCBUCollectionType":{checkSelectCordEntry : true},
					"cdrCbuPojo.fkMultipleBirth":{checkSelectCordEntry : true},					
					"birthDateStr":{validateRequiredCordEntry:true,checkDatesCordEntry:["datepicker3","datepicker2"],dpDate: true},
					"cdrCbuPojo.babyGenderId":{checkSelectCordEntry : true},
					"cdrCbuPojo.ethnicity":{checkSelectCordEntry : true},
					"race":{validateRequiredCordEntry : true},
					"cbuCollectionDateStr":{validateRequiredCordEntry:true,checkDatesCordEntry:["datepicker3","datepicker2"],dpDate: true},
					"cdrCbuPojo.cbuCollectionTime":{validateRequiredCordEntry:true,checkTimeFormat:true},
					"cdrCbuPojo.babyBirthTime":{validateRequiredCordEntry:true,checkTimeFormat:true},
					/*"hlas[0].hlaType1":{validateRequiredCordEntry:true,validateAntigen:["0hlaantigen1"]},
					"hlas[0].hlaType2":{validateAntigen:["0hlaantigen2"]},					
					"hlas[0].fkHlaMethodId":{checkSelectCordEntry:true},
					"hlas[1].hlaType1":{validateRequiredCordEntry:true,validateAntigen:["1hlaantigen1"]},
					"hlas[1].hlaType2":{validateAntigen:["1hlaantigen2"]},					
					"hlas[1].fkHlaMethodId":{checkSelectCordEntry:true},
					"hlas[2].hlaType1":{validateAntigen:["2hlaantigen1"]},
					"hlas[2].hlaType2":{validateAntigen:["2hlaantigen2"]},					
					"hlas[3].hlaType1":{validateRequiredCordEntry:true,validateAntigen:["3hlaantigen1"]},
					"hlas[3].hlaType2":{validateAntigen:["3hlaantigen2"]},
					"hlas[3].fkHlaMethodId":{checkSelectCordEntry:true},
					"hlas[4].hlaType1":{validateAntigen:["4hlaantigen1"]},
					"hlas[4].hlaType2":{validateAntigen:["4hlaantigen2"]},					
					"hlas[5].hlaType1":{validateAntigen:["5hlaantigen1"]},
					"hlas[5].hlaType2":{validateAntigen:["5hlaantigen2"]},					
					"hlas[6].hlaType1":{validateAntigen:["6hlaantigen1"]},
					"hlas[6].hlaType2":{validateAntigen:["6hlaantigen2"]},					
					"hlas[7].hlaType1":{validateAntigen:["7hlaantigen1"]},
					"hlas[7].hlaType2":{validateAntigen:["7hlaantigen2"]},					
					"hlas[8].hlaType1":{validateAntigen:["8hlaantigen1"]},
					"hlas[8].hlaType2":{validateAntigen:["8hlaantigen2"]},					
					"maternalHlas[0].hlaType1":{validateAntigen:["0hlamaternalantigen1"]},
					"maternalHlas[0].hlaType2":{validateAntigen:["0hlamaternalantigen2"]},					
					"maternalHlas[1].hlaType1":{validateAntigen:["1hlamaternalantigen1"]},
					"maternalHlas[1].hlaType2":{validateAntigen:["1hlamaternalantigen2"]},					
					"maternalHlas[2].hlaType1":{validateAntigen:["2hlamaternalantigen1"]},
					"maternalHlas[2].hlaType2":{validateAntigen:["2hlamaternalantigen2"]},					
					"maternalHlas[3].hlaType1":{validateAntigen:["3hlamaternalantigen1"]},
					"maternalHlas[3].hlaType2":{validateAntigen:["3hlamaternalantigen2"]},					
					"maternalHlas[4].hlaType1":{validateAntigen:["4hlamaternalantigen1"]},
					"maternalHlas[4].hlaType2":{validateAntigen:["4hlamaternalantigen2"]},					
					"maternalHlas[5].hlaType1":{validateAntigen:["5hlamaternalantigen1"]},
					"maternalHlas[5].hlaType2":{validateAntigen:["5hlamaternalantigen2"]},					
					"maternalHlas[6].hlaType1":{validateAntigen:["6hlamaternalantigen1"]},
					"maternalHlas[6].hlaType2":{validateAntigen:["6hlamaternalantigen2"]},					
					"maternalHlas[7].hlaType1":{validateAntigen:["7hlamaternalantigen1"]},
					"maternalHlas[7].hlaType2":{validateAntigen:["7hlamaternalantigen2"]},					
					"maternalHlas[8].hlaType1":{validateAntigen:["8hlamaternalantigen1"]},
					"maternalHlas[8].hlaType2":{validateAntigen:["8hlamaternalantigen2"]},*/					
					"prcsngStartDateStr":{validateRequiredCordEntry:true,checkDatesCordEntry:["datepicker2","datepicker4"],dpDate: true},
					"frzDateStr":{validateRequiredCordEntry:true,checkHourcordentry:["datepicker2","datepicker5"],dpDate: true},
					"cdrCbuPojo.bacterialResult":{checkSelectCordEntry : true},
					"cdrCbuPojo.fungalResult":{checkSelectCordEntry : true},
					"cdrCbuPojo.aboBloodType":{checkSelectCordEntry : true},
					"cdrCbuPojo.hemoglobinScrn":{checkSelectCordEntry : true},
					"cdrCbuPojo.rhType":{checkSelectCordEntry : true},
                    "entitySamplesPojo.noSegAvail":{validateRequiredCordEntry:true},
					"entitySamplesPojo.filtPap":{validateRequiredCordEntry:true},
					"entitySamplesPojo.rbcPel":{validateRequiredCordEntry:true},
					"entitySamplesPojo.extDnaAli":{validateRequiredCordEntry:true},
					"entitySamplesPojo.nonViaAli":{validateRequiredCordEntry:true},
					"entitySamplesPojo.noSerAli":{validateRequiredCordEntry:true},
					"entitySamplesPojo.noPlasAli":{validateRequiredCordEntry:true},
					"entitySamplesPojo.viaCelAli":{validateRequiredCordEntry:true},
					"entitySamplesPojo.totCbuAli":{validateRequiredCordEntry:true},
					"entitySamplesPojo.extDnaMat":{validateRequiredCordEntry:true},	
					"entitySamplesPojo.celMatAli":{validateRequiredCordEntry:true},	
					"entitySamplesPojo.serMatAli":{validateRequiredCordEntry:true},	
					"entitySamplesPojo.plasMatAli":{validateRequiredCordEntry:true},
					"entitySamplesPojo.totMatAli":{validateRequiredCordEntry:true},
					"entitySamplesPojo.cbuOthRepConFin":{validateRequiredCordEntry:true},
					"entitySamplesPojo.cbuRepAltCon":{validateRequiredCordEntry:true},
					"entitySamplesPojo.noMiscMat":{validateRequiredCordEntry:true},					
					"cdrCbuPojo.processingTime":{validateRequiredCordEntry:true},
					"_LAB_SUM_Hemo_assessmentRemarks":{ required:{
																depends: function(element) {
																	if($j("#hemoglobinScrnTest").val()==$j("#alphaThalismiaTraitPkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDiesasePkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemoAlphaThalassmiaPkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemoTraitPkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDonePkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemozygousUnknownPkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoMultiTraitPkVal").val()){
																		return true;
																	}
																	else{return false;}
																	}
																},
																	maxlength:200
															},
					"_LAB_SUM_Fung_assessmentRemarks":{ required:{
																	depends: function(element) {
																		if($j("#fungalResult").val()==$j("#fungalCulnotdone").val()){
																			return true;
																		}
																		else{return false;}
																		}
																	},
																		maxlength:200
																},
					"_LAB_SUM_Hemo_assessmentresponse":{ required :{
																	depends: function(element) {
																		if($j("#hemoglobinScrnTest").val()==$j("#alphaThalismiaTraitPkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDiesasePkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemoAlphaThalassmiaPkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemoTraitPkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDonePkVal").val()||$j("#hemoglobinScrnTest").val()==$j("#hemozygousUnknownPkVal").val() || $j("#hemoglobinScrnTest").val()==$j("#hemoMultiTraitPkVal").val()){
																			return true;
																		}
																		else{return false;}
																		}
																	}
														},
					"_LAB_SUM_Fung_assessmentresponse":{ required :{
															depends: function(element) {
																if($j("#fungalResult").val()==$j("#fungalCulnotdone").val()){
																	return true;
																}
																else{return false;}
																}
															}
												},
					"savePostTestList[12].fktestmethod":{checkSelectCordEntry:true},
					"savePostTestList[13].fktestmethod":{checkSelectCordEntry:true},					
					"selectName":{min:1},
					"fkCordCbuEligible":{checkSelectCordEntry:true},
					"reasons":{
						                validateRequiredCordEntry:{
	            									depends: function(element) {
															 return ($j("#fkCordCbuEligible").val()==$j("#inEligiblePk").val()
																	 ||$j("#fkCordCbuEligible").val()==$j("#incompleteReasonId").val());
															 }
														}
												},
					"cordAdditionalInfo":{
										validateRequiredCordEntry:{
	            									depends: function(element) {
														 return ($j("#fkCordCbuEligible").val()==$j("#notCollectedToPriorReasonId").val());
														 }
													},
												maxlength:300
											},
					"clinicalNotePojo.fkReason":{validateRequiredCordEntry:
											{
											  depends: function(element){
												  return ($j("#reason").is(':visible') );
												}
											}	
										},
					"clinicalNotePojo.comments":{validateRequiredCordEntry:
											{
											  depends: function(element){
												  return ($j("#comment").is(':visible') );
												}
											}	
									     },
					"clinicalNotePojo.fkCbuStatus":{validateRequiredCordEntry:
											{
											  depends: function(element){
												  return ($j("#cbu_status").is(':visible') );
												}
											}
								     	},
					"clinicalNotePojo.availableDate":{validateRequiredCordEntry:
										{	  depends: function(element){
												  return ($j("#datepicker9").is(':visible'));
												}
											}			
										},	
					/* "clinicalNotePojo.fkNotesCategory":{validateRequiredCordEntry:
						{	  depends: function(element){
							  return ($j("#cordSearchable").val()!='0');
							}
						}			
					},		 */							
					"cdrCbuPojo.specRhTypeOther":{validateRequiredCordEntry:
										{	  depends: function(element){
											        return ($j("#rhtypeother").val()==$j("#cdrcburhtype").val());
												}
											}			
									     },
									     
				"bactCultDateStr":{validateRequiredCordEntry:
					            {	  depends: function(element){
										        return ($j("#dropValue").val()==$j("#bacterialPositive").val() || $j("#dropValue").val()==$j("#bacterialNegative").val() || $j("#bacterialResult").val()==$j("#bacterialPositive").val() || $j("#bacterialResult").val()==$j("#bacterialNegative").val());
											}
										},dpDate: true			
								     },

			"savePostTestList[12].testMthdDesc":{required:
				{
				  depends: function(element){
				    return ($j("#cfuDescOther").is(':visible') && $j("#cordSearchable").val()!='0');
						}
			        }	
			     },

				 "savePostTestList[13].testMthdDesc":{required:
					{
					  depends: function(element){
						  return ($j("#viaDescOther").is(':visible')  && $j("#cordSearchable").val()!='0');
						}
					}	
				},
					
													     	

				"fungCultDateStr":{validateRequiredCordEntry:
					 {	  depends: function(element){
						       return ($j("#dropValueFung").val()==$j("#bacterialPositiveFung").val() || $j("#dropValueFung").val()==$j("#bacterialNegativeFung").val() || $j("#fungalResult").val()==$j("#bacterialPositiveFung").val() || $j("#fungalResult").val()==$j("#bacterialNegativeFung").val());
								}
							},dpDate: true			
						 },		
				"preTestDateStrn":{validateRequiredCordEntry:true,dpDate: true},	
				"postTestDateStrn":{validateRequiredCordEntry:true,dpDate: true},	
				"otherTestDateStrn1":{validateRequiredCordEntry:true,dpDate: true},
				"otherTestDateStrn2":{validateRequiredCordEntry:true,dpDate: true},
				"otherTestDateStrn3":{validateRequiredCordEntry:true,dpDate: true},
				"otherTestDateStrn4":{validateRequiredCordEntry:true,dpDate: true},
			 	"otherTestDateStrn5":{validateRequiredCordEntry:true,dpDate: true},
				"otherTestDateStrn6":{validateRequiredCordEntry:true,dpDate: true},
				},
				
				messages:{
                    "cdrCbuPojo.registryId":{checkspecialchar : "<s:text name="garuda.common.validation.alphanumeric"/>",checklength:"<s:text name="garuda.common.validation.alphalength"/>",checkcordentryformat:"<s:text name="garuda.cbu.cordentry.checkformat"/>",validateValues1:"<s:text name="garuda.cbu.cordentry.cbumatsameregid"/>",checksum :"<s:text name="garuda.common.validation.checksumrid"/>",checkdbdata11:"<s:text name="garuda.common.id.exist"/>",checkdbdata01:"<s:text name="garuda.common.id.exist"/>",checkregcbbdbdata:"<s:text name="garuda.cbu.cordentry.cburegid"/>",checkdbmatregcbbdbdata:"<s:text name="garuda.cbu.cordentry.cburegidasmatregid"/>"},
                    "cdrCbuPojo.registryMaternalId":{checkspecialchar : "<s:text name="garuda.common.validation.alphanumeric"/>",checklength:"<s:text name="garuda.common.validation.alphalength"/>",checkcordentryformat:"<s:text name="garuda.cbu.cordentry.checkformat"/>",validateValues1:"<s:text name="garuda.cbu.cordentry.cbumatsameregid"/>",checksum :"<s:text name="garuda.common.validation.checksummid"/>",checkcbbdbdata11:"<s:text name="garuda.cbu.cordentry.samematregid"/>",checkdbdata01:"<s:text name="garuda.cbu.cordentry.samematregidcbuid"/>",checkregcbbdbdata:"<s:text name="garuda.cbu.cordentry.matregid"/>",checkdbmatregcbbdbdata:"<s:text name="garuda.cbu.cordentry.matregidascburegid"/>"},
                    "cdrCbuPojo.localMaternalId":{validateValues1:"<s:text name="garuda.cbu.cordentry.cbumatlocsamecbulocid"/>",checkdbadditionaldata1:"<s:text name="garuda.cbu.cordentry.cbumatlocexistdiffmatid"/>",checklocalcbuidadditionaldbdata11:"<s:text name="garuda.cbu.cordentry.locmatidexistascblocid"/>"},
    				"cdrCbuPojo.localCbuId":{required:"<s:text name="garuda.cbu.cordentry.reqcbulocid"/>",validateValues1:"<s:text name="garuda.cbu.cordentry.cbulocsamematlocid"/>",/*checklocalcbuiddbdata1:"<s:text name="garuda.common.id.exist"/>",*/checklocalcbuidadditionaldbdata01:"<s:text name="garuda.cbu.cordentry.localCbuIdexistasMatId"/>",checklocalcbuidadditionaldbdata11:"<s:text name="garuda.cbu.cordentry.localCbuIdexist"/>"},
					"cdrCbuPojo.cordIsbiDinCode":{required:"<s:text name="garuda.cbu.cordentry.reqisbtdin"/>",checkisbtdbdata1:"<s:text name="garuda.common.id.exist"/>",checkisbtdata1:"<s:text name="garuda.cbu.cordentry.reqvalidisbtdin"/>"},	
					"cbuOnBag":{checkselect:"<s:text name="garuda.common.validation.value"/>"},
                    "savePostTestList[12].testMthdDesc":"<s:text name="garuda.cbu.cordentry.cfuOther"/>",
					"savePostTestList[13].testMthdDesc":"<s:text name="garuda.cbu.cordentry.viaOther"/>",
					"fungCultDateStr":{validateRequiredCordEntry:"<s:text name="garuda.cbu.cordentry.fungalstartdate"/>"},
					"bactCultDateStr":{validateRequiredCordEntry:"<s:text name="garuda.cbu.cordentry.bactculstartdate"/>"},
					"cbuLicStatus":"<s:text name="garuda.cbu.license.licensurestatus"/>",
					"cdrCbuPojo.fkCbbId":"<s:text name="garuda.pf.procedure.site"/>",
					"cdrCbuPojo.fkCbbProcedure":"<s:text name="garuda.cbu.cordentry.cbbprocedure"/>",
					"cdrCbuPojo.fkCBUDeliveryType":"<s:text name="garuda.cbu.cordentry.deliverytype"/>",
					"cdrCbuPojo.fkCBUCollectionType":"<s:text name="garuda.cbu.cordentry.collectiontype"/>",
					"cdrCbuPojo.fkMultipleBirth":"<s:text name="garuda.cbu.cordentry.MultipleBirth"/>",
					"birthDateStr":{validateRequiredCordEntry:"<s:text name="garuda.cbu.cordentry.birthdate"/>",checkDatesCordEntry:"<s:text name="garuda.cbu.cordentry.birthdatecheck"/>"},
					"cdrCbuPojo.babyGenderId":"<s:text name="garuda.cbu.cordentry.babygenderid"/>",
					"cdrCbuPojo.ethnicity":"<s:text name="garuda.cbu.cordentry.birthethnicity"/>",
					"race":"<s:text name="garuda.cbu.cordentry.race"/>",
					"cbuCollectionDateStr":{validateRequiredCordEntry:"<s:text name="garuda.cbu.cordentry.collectiondate"/>",checkDatesCordEntry:"<s:text name="garuda.cbu.cordentry.checkbabydates"/>"},
					"cdrCbuPojo.cbuCollectionTime":{validateRequiredCordEntry:"<s:text name="garuda.cbu.cordentry.collectiontime"/>",checkTimeFormat:"<s:text name="garuda.common.validation.timeformat"/>"},
					"cdrCbuPojo.babyBirthTime":{validateRequiredCordEntry:"<s:text name="garuda.cbu.cordentry.babybirthtime"/>",checkTimeFormat:"<s:text name="garuda.common.validation.timeformat"/>"},
					"hlas[0].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>", validateRequiredCordEntry:"<s:text name="garuda.cbu.hlatype.hlatype1"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[0].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[1].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateRequiredCordEntry:"<s:text name="garuda.cbu.hlatype.hlatype1"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[1].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[2].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[2].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[3].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateRequiredCordEntry:"<s:text name="garuda.cbu.hlatype.hlatype1"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[3].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[4].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[4].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[5].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[5].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[6].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[6].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[7].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[7].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[8].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[8].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[9].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[9].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[10].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[10].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[11].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[11].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"hlas[0].hlaTypingDate":{validateRequiredCordEntry:"<s:text name="garuda.cbu.hlatype.receiveddate"/>"},
					"hlas[1].hlaTypingDate":{validateRequiredCordEntry:"<s:text name="garuda.cbu.hlatype.receiveddate"/>"},
					"hlas[3].hlaTypingDate":{validateRequiredCordEntry:"<s:text name="garuda.cbu.hlatype.receiveddate"/>"},
					"maternalHlas[0].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[0].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[1].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[1].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[2].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[2].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[3].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[3].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[4].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[4].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[5].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[5].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[6].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[6].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[7].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[7].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[8].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[8].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[9].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[9].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[10].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[10].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[11].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"maternalHlas[11].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
					"fkCordCbuEligible":"<s:text name="garuda.cbu.cordentry.cordeligible"/>",
					"clinicalNotePojo.fkReason": "<s:text name="garuda.cbu.clinicalnote.reason"/>",
					"clinicalNotePojo.availableDate": "<s:text name="garuda.common.validation.availabledate"/>",
					"clinicalNotePojo.fkCbuStatus": "<s:text name="garuda.cbu.cordentry.status"/>",
					"clinicalNotePojo.comments": "<s:text name="garuda.common.validation.comment"/>",
					//"clinicalNotePojo.fkNotesCategory":"<s:text name="garuda.cbu.clinicalnote.notescategory"/>",
					"reasons":"<s:text name="garuda.cbu.clinicalnote.reason"/>",
					/*"licenseReasons":"<s:text name="garuda.cbu.license.licensurestatus"/>",*/
					"prcsngStartDateStr":{validateRequiredCordEntry:"<s:text name="garuda.cbu.cordentry.procstartdate"/>",checkDatesCordEntry:"<s:text name="garuda.cbu.cordentry.procDateComp"/>"},
					"frzDateStr":{validateRequiredCordEntry:"<s:text name="garuda.cbu.cordentry.freezedate"/>",checkHourcordentry:"<s:text name="garuda.cbu.cordentry.freezedatehour"/>"},
					"cdrCbuPojo.aboBloodType":"<s:text name="garuda.cbu.cordentry.abobloodtype"/>",
					"cdrCbuPojo.rhType":"<s:text name="garuda.cbu.cordentry.rhtype"/>",
					"cdrCbuPojo.bacterialResult":"<s:text name="garuda.cbu.cordentry.bactcul"/>",
					"cdrCbuPojo.fungalResult":"<s:text name="garuda.cbu.cordentry.fungcul"/>",
					"cdrCbuPojo.hemoglobinScrn":"<s:text name="garuda.cbu.cordentry.hemotesting"/>",
					"entitySamplesPojo.noSegAvail":"<s:text name="garuda.pf.procedure.noofsegments"/>",
					"entitySamplesPojo.filtPap":"<s:text name="garuda.cbu.samples.filterpaper"/>",
					"entitySamplesPojo.rbcPel":"<s:text name="garuda.cbu.samples.rbcpallets"/>",
					"entitySamplesPojo.extDnaAli":"<s:text name="garuda.cbu.samples.numextracteddna"/>",
					"entitySamplesPojo.nonViaAli":"<s:text name="garuda.cbu.samples.numnonviable"/>",
					"entitySamplesPojo.noSerAli":"<s:text name="garuda.cbu.samples.numserum"/>",
					"entitySamplesPojo.noPlasAli":"<s:text name="garuda.cbu.samples.numplasma"/>",
					"entitySamplesPojo.viaCelAli":"<s:text name="garuda.cbu.samples.numviable"/>",
					"entitySamplesPojo.totCbuAli":"<s:text name="garuda.cbu.samples.totcbuali"/>",
					"entitySamplesPojo.extDnaMat":"<s:text name="garuda.cbu.samples.matextracteddna"/>",
					"entitySamplesPojo.celMatAli":"<s:text name="garuda.cbu.samples.cellmaternal"/>",
					"entitySamplesPojo.serMatAli":"<s:text name="garuda.cbu.samples.nummatserum"/>",
					"entitySamplesPojo.plasMatAli":"<s:text name="garuda.cbu.samples.nummatplasma"/>",
					"entitySamplesPojo.totMatAli":"<s:text name="garuda.pf.procedure.frozencontainer"/>",
					"entitySamplesPojo.cbuOthRepConFin":"<s:text name="garuda.pf.procedure.numfinalproduct"/>",
					"entitySamplesPojo.cbuRepAltCon":"<s:text name="garuda.pf.procedure.numaltcond"/>",
					"entitySamplesPojo.noMiscMat":"<s:text name="garuda.cbu.samples.nummiscmat"/>",
					"_LAB_SUM_Hemo_assessmentRemarks":{
		         		required : "<s:text name="garuda.label.dynamicform.errormsg"/>",
		         		maxlength : "<s:text name="garuda.cbu.cordentry.cordadditionalinfomaxlength"/>"
						},
					"_LAB_SUM_Fung_assessmentRemarks":{
			         	required : "<s:text name="garuda.label.dynamicform.errormsg"/>",
			         	maxlength : "<s:text name="garuda.cbu.cordentry.cordadditionalinfomaxlength"/>"
						},
					"_LAB_SUM_Hemo_assessmentresponse":"<s:text name="garuda.label.dynamicform.errormsg"/>",
					"_LAB_SUM_Fung_assessmentresponse":"<s:text name="garuda.label.dynamicform.errormsg"/>",
					"savePostTestList[12].fktestmethod":"<s:text name="garuda.cbu.cordentry.cfucount"/>",
					"savePostTestList[13].fktestmethod":"<s:text name="garuda.cbu.cordentry.viability"/>",					
					"cdrCbuPojo.processingTime":{validateRequiredCordEntry:"<s:text name="garuda.cbu.cordentry.proctime"/>"},					
					"cordAdditionalInfo":{
		                 		validateRequiredCordEntry : "<s:text name="garuda.cbu.cordentry.cordadditionalinfo"/>",
		                 		maxlength : "<s:text name="garuda.cbu.cordentry.maxchar"/>"
						},						
					"cdrCbuPojo.specRhTypeOther":{validateRequiredCordEntry:"<s:text name="garuda.common.validation.reason"/>"},
					"preTestDateStrn":{validateRequiredCordEntry :"<s:text name="garuda.cbu.cordentry.inputId"/>"},
					"postTestDateStrn":{validateRequiredCordEntry :"<s:text name="garuda.cbu.cordentry.inputId"/>"},
					"otherTestDateStrn1":{validateRequiredCordEntry :"<s:text name="garuda.cbu.cordentry.inputId"/>"},
					"otherTestDateStrn2":{validateRequiredCordEntry :"<s:text name="garuda.cbu.cordentry.inputId"/>"},
					"otherTestDateStrn3":{validateRequiredCordEntry : "<s:text name="garuda.cbu.cordentry.inputId"/>"},
					"otherTestDateStrn4":{validateRequiredCordEntry :"<s:text name="garuda.cbu.cordentry.inputId"/>"},
					"otherTestDateStrn5":{validateRequiredCordEntry : "<s:text name="garuda.cbu.cordentry.inputId"/>"},
					"otherTestDateStrn6":{validateRequiredCordEntry :"<s:text name="garuda.cbu.cordentry.inputId"/>"}
				}
			});	
			
addRules(new_cord_entry_elig_quest_validtaion_rules);
}//End of jquery Validator initializer

   
  function showAddWidget(url){
		 url = url + "?pageId=20" ; 
		 showAddWidgetModal(url);
	 }

 function setProcedurePrevVal(value){
	 onFocusProcVal = value;
	 addTotalCount();
 }

function addTotalCount(){
	var val1;	
		  totalcount++;	
		   sumTotal =  cbucount+  idcount+  licensecount+  proccount+ labsummarycount+ idmcount+ hlacount+  eligibilitycount
		 
		 	if($j('#licenseid').val()==$j("#unLicensedPk").val()){
			sumTotal += mrqcount+ fmhqcount;
			}
	     val1 = parseInt((sumTotal*100)/(noOfMandatoryField));
  
		//  val1 = parseInt((totalcount*100)/(noOfMandatoryField));
		  $j("#totalbar").progressbar({
				value: val1
			}).children('.ui-progressbar-value').html(val1.toPrecision(3) + '%');
	$j("#cordProgress").val(val1);		  
}

  function minusTotalCount(){	  	
	     var val1;
		 totalcount--;
		   sumTotal =  cbucount+  idcount+  licensecount+  proccount+ labsummarycount + idmcount+ hlacount+  eligibilitycount
		 	if($j('#licenseid').val()==$j("#unLicensedPk").val()){
			sumTotal = mrqcount+ fmhqcount;
			}
	     val1 = parseInt((sumTotal*100)/(noOfMandatoryField));
		//  val1 = parseInt((totalcount*100)/(noOfMandatoryField));

		  $j("#totalbar").progressbar({
				value: val1
			}).children('.ui-progressbar-value').html(val1.toPrecision(3) + '%');
		
		  $j("#cordProgress").val(val1);	 
  }  
/* function clinicalValidation()
{
	var categorySelected = document.getElementById('fkNotescategory');
	var clinicalFlag=true;
	var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
 	var value = field.GetHTML(true); 	
 	if(value=='<br>')
 		{
 		value="";
 		
 		}
 	if(value.length>4000)
	{
 		$j("#note_error1").show();
	}
 	if(value.length<4000)
 		{
 		$j("#note_error1").hide();
 		}
 	if(value.length>0)
		{
		$j("#note_error").hide();
		if($j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val()==null)
		{
			$j("#note_assess").show();
			clinicalFlag=false;
		}
		if(categorySelected.selectedIndex==0)
			{
				$j("#note_category").show();
				clinicalFlag=false;
			} 
		}
	if($j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val()!=null)
		{
		if(value.length==0)
			{
			$j("#note_error").show();
			clinicalFlag=false;
			}
		if(categorySelected.selectedIndex==0)
			{
			$j("#note_category").show();
			clinicalFlag=false;
		}  
		}
	  if($j("#fkNotescategory").val()!=""){
		if(value.length==0)
		{
		$j("#note_error").show();
		clinicalFlag=false;
		}
		if($j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val()==null)
		{
			$j("#note_assess").show();
			clinicalFlag=false;
		}
	 } 
	return clinicalFlag;
	}*/ //$j('input:radio[value="false"][name="'+index+'"]').next()
	
	var ElementNameWithHierarchyIndex={};   
   
	var ElementWithHierarchyIndexInit={	 	
	init:function(){
		    var i=0;
		    ElementNameWithHierarchyIndex={};
			$j('#cordentryform1 input,select,textarea').each(function(){
	             if(!($j(this).is(':hidden'))){
		          ElementNameWithHierarchyIndex[$j(this).attr('name')]=i;
		          i++;
		          }
		  });
	  }
	};

    var FCKEditorInstance={
			 val:function(id){		           
	  	            $j('#'+id).val(FCKeditorAPI.GetInstance(id).GetHTML(true));
	       }
	};
	$j(function(){

		$j('input,select,textarea').bind({
			  change: function() {
			  if($j(this).is(':radio')){
			     if($j('input:radio[value="false"][name="'+$j(this).attr('name')+'"]').next().next('label.error').length > 0){
				    if($j('input:radio[value="false"][name="'+$j(this).attr('name')+'"]').next().next('label.error').hasClass('custom')){
					    $j('input:radio[value="false"][name="'+$j(this).attr('name')+'"]').next().next('label.error').hide();
					}
				 }
			  }
			  
			    
			  }/*,
			  mouseenter: function() {
			    // do something on mouseenter
			  }*/
			});
		});
	var focusElem=null;
	var appendErrors=function(resObj){	
		if(resObj.fieldErrors.isCordSubmitted=='true'){
			/* jConfirm('<s:text name="garuda.cbu.cordentry.alreadysubmitted"/>', '<s:text name="garuda.common.lable.cancel"/>',
						function(r) {
							if (r == true){
								  window.location.href =  "cordEntryInProgress";
								  showprogressMgs();
							}
			 });*/
			 jAlert('<s:text name="garuda.cbu.cordentry.alreadysubmitted"/>', '<s:text name="garuda.common.lable.information"/>',
					function(r) {
						if (r == true){
							  window.location.href =  "cordEntryInProgress";
							  showprogressMgs();
						}
		  });
		}
	   else{		
       (function(resObj){
    	   if (resObj.fieldErrors) {
    		   focusElem=null;
    		   if(resObj.fieldErrors['clinicalNotePojo.noteAssessment']){
    				$j('input:radio[value="na"][name="clinicalNotePojo.noteAssessment"]').next().next('div#note_assess.custom').show();
    				focusElem=$j('[name="clinicalNotePojo.noteAssessment"]');
    			}
    		   if(resObj.fieldErrors['clinicalNotePojo.notes']){
   				   $j('#note_error').show();
   				   focusElem=$j('[name="clinicalNotePojo.notes"]');
   			   }
    			$j.each(resObj.fieldErrors, function(index, value) {    				
    				var elem=$j('[name="'+index+'"]');    		
    				elem=!(elem.is(':radio'))?
    	    				(index==='clinicalNotePojo.noteAssessment')?
    	    	    				$j('input:radio[value="na"][name="'+index+'"]')
    	    	    				:
        	    	    				elem
        	    	    				:
            	    	    				$j('input:radio[value="false"][name="'+index+'"]');
    				
    				(elem.next().next('label.error').length > 0)?
    						elem.next().next('label.error')
    				        .text(value[0].toString())
    				        .show()
    				                :
                                           (elem.next().next('span.error').length > 0)?
                                            		elem.next().next('span.error')
                            				        .text(value[0].toString())
                            				        .show()
                				                       :
    				                                        elem.after( '<br><label for="'+elem.attr('id')+'" generated="true" class="error custom">'+value[0].toString()+'</label>');
                   focusElem=(focusElem === null)?elem:focusElem;
                   if(ElementNameWithHierarchyIndex[focusElem.attr('name')] > ElementNameWithHierarchyIndex[index]){focusElem=elem;}
    			});
    			if(resObj.fieldErrors.cordAutoDeferStatus=='true'){
    				jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
							function(r) {
								if (r == false) {
								  }else if(r == true){
									  $j("#cordentryform1").attr('action','saveCordEntry');
									  document.getElementById('cordentryform1').submit();
									  showprogressMgs();
								  }
					});
    			}
    		}/*else{        		
    			 $j("#cordentryform1").attr('action','savePartialCordEntry');
				  document.getElementById('cordentryform1').submit();
    		}*/
           })(resObj);
		}
		if(focusElem!==null){
			
			ElementFocus(focusElem);
		}
	};
	var ElementFocus=function(elem){
		switch(elem.attr('id')){
		
			case 'idmformversionlstsize':
				$j('#idminfo').attr("tabindex",-1).focus();;
				break;
			case 'mrqformversionlstsize':
				$j('#MRQ_formVersion').attr("tabindex",-1).focus();;
				break;
			case 'fmhqformversionlstsize':
				$j('#fmhqAsyncData').attr("tabindex",-1).focus();;
				break;
			case 'hlaPercentageId':
				$j('#hlaAsyncData').attr("tabindex",-1).focus();;
				break;
			default:
				elem.focus();
		}			
	}

	function validateCordSession(){
		var val = "";
		$j.ajax({
	        type: "GET",
	        url: "cord-entry/cb_validate_cord_session.jsp",
	        async:false,	       
	        success: function (result){
	             //alert(result);
	             val = $j.trim(result);
	        },
	        error: function (request, status, error) {	       
		        	alert("Error " + error);
		            alert(request.responseText);	          	
	        }	        
		});	
		if($j("#cordEditEntryToken").val()==val)
			return true;
		return false;
	}
	
function saveInProgress(){//review

   if(validateCordSession()){
	$j('.dpDate').focusout();
	/* Set Fck Editor Field Value*/
	FCKEditorInstance.val('sectionContentsta');
	$j("#cordSearchable").val("0");
		$j.ajax({
	        type: "POST",
	        url: "cordEntryValidate",
	        async:true,	       
	        data:$j('#cordentryform1').serialize()+'&struts.enableJSONValidation=true&struts.validateOnly=false&wrapWithComments=false&cordSearchable='+$j('#cordSearchable').val(),
	        success: function (result,error){
	             //$j('#userbody').html(result);				
				 saveNotesData();  
	        	 $j("#cordentryform1").attr('action','savePartialCordEntry');
				  document.getElementById('cordentryform1').submit();
				  showprogressMgs();
	        },
	        error: function (request, status, error) {
		        if(request.status===200 && request.statusText==='OK' && status==='parsererror'){		        
		        	appendErrors(eval("("+request.responseText.substring(request.responseText.indexOf("\/\*")+2, request.responseText.lastIndexOf("\*\/"))+")"))
				 }
		        else{
		        	alert("Error " + error);
		            alert(request.responseText);
		        }	        	
	        }	        
		});	
		//$j('.custom').hide();
		$j('label.error,span.error').hide();
		ElementWithHierarchyIndexInit.init();
		
	/*if(validateIscordSubmitted()){
		  jConfirm('<s:text name="garuda.cbu.cordentry.alreadysubmitted"/>', '<s:text name="garuda.common.lable.cancel"/>',
					function(r) {
						if (r == true){
							  window.location.href =  "cordEntryInProgress";
						}
		 });
	  }else{		
			setTimeout(function(){
		  		showprogressMgs();
			},0);
			setTimeout(function(){
				var validTime= checkTimeFormatForCord('babybirthtime');
				if(validTime){
					validTime= checkTimeFormatForCord('cbuCollectionTime');
				} 
				if(validTime){
				    validTime= checkTimeFormatForCord('cordProcessingTime');
				} 
				if(validTime){
					validTime= checkTimeFormatForCord('cordFreezeTime');
				}
			var labFlag=validateRangeBeforSaveInPrg('cordentryform1');
			var dateChckFlag = setMinDateWidRespCollDat();
			var collectionTimeFlag = validateHours('datepicker3','babybirthtime','datepicker2','cbuCollectionTime','collTimeRangeErrorMsg');
			var frzTimeFlag = validateHours('datepicker2','cbuCollectionTime','datepicker5','cordFreezeTime','frzTimeRangeErrorMsg');
			var procDate = validateCordDates('datepicker2','datepicker4','processdateErrorMsg');
			var prcssDate = validateDateData('datepicker4','cordProcessingTime','datepicker5','cordFreezeTime','precessDatebffrz','precessDatebffrztime');
			var colDateRange = validateCordDateRange('datepicker3','datepicker2','birthdateRangeErrorMsg',true);
			var prcDateRange = validateCordDateRange('datepicker2','datepicker4','processdateErrorMsg1',true);
			var frzDateRange = validateCordDateRange('datepicker2','datepicker5','frzdateRangeErrorMsg',true)
			var rangeFlag=true;
			var clinicalFlag=true;
			  $j(".onChngRangeChk").each(function() {
				  rangeFlag=false;
				});
			  $j(".onChngMand").each(function() {
				  var id=this.id;
				  $j("#"+id).removeClass('onChngMand');
				  $j("#"+id+"_error").hide();
				});
				   clinicalFlag = clinicalValidation();
				   if(clinicalFlag)
				   {
					   saveNotesData();  
				   }
				   
				   var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
			 	   var value = field.GetHTML(true);
			  	   $j('#sectionContentsta').val(value);
		  	
			  $j("#cordSearchable").val("0");
			  var dateElements = $j('.hasDatepicker').get();
			  $j.each(dateElements, function(){
				  if(this.id=='datepicker2' || this.id=='datepicker3' || this.id=='datepicker4' || this.id=='datepicker5')
					  customCordDateRanges(this);
				  else
					  customRange(this);
			  });
			  if($j("#cordentryform1").valid() && colDateRange==true && prcDateRange==true && frzDateRange==true && rangeFlag==true && clinicalFlag==true && dateChckFlag==true && collectionTimeFlag==true && frzTimeFlag==true && procDate==true && prcssDate==true && validTime==true){
			         //if($j("#cordentryform1").valid()){
		   			  //submitCordForSearch('savePartialCordEntry','main','cordentryform1');
			  //}
					  if (validateDeferCbu()){
							jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
									function(r) {
										if (r == false) {
										  }else if(r == true){
											  $j("#cordentryform1").attr('action','savePartialCordEntry');
											  document.getElementById('cordentryform1').submit();
										  }
							});
						}else{
							  $j("#cordentryform1").attr('action','savePartialCordEntry');
							  document.getElementById('cordentryform1').submit();
						}		
			   }
			},100);
			setTimeout(function(){
				closeprogressMsg();
			},0);*/
	  }else{
		  jAlert('<s:text name="garuda.cbu.cordentry.alreadysubmittedInProgress"/>', '<s:text name="garuda.common.lable.information"/>',
					function(r) {
						if (r == true){
							submitpost('openWholeCordEntry',{'cdrCbuPojo.cordID':cordId});
						}
		 });
	  }
	}
		
function saveForCBUSearching(){
	$j('.dpDate').focusout();
	/* Set Fck Editor Field Value*/
	 FCKEditorInstance.val('sectionContentsta');
	 $j("#cordSearchable").val("1");
			$j.ajax({
		        type: "POST",
		        url: "cordEntryValidate",
		        async:true,	       
		        data:$j('#cordentryform1').serialize()+'&struts.enableJSONValidation=true&struts.validateOnly=false&wrapWithComments=false&cordSearchable='+$j('#cordSearchable').val(),
		        success: function (result,error){
				     saveNotesData();  				 
					 $j("#cordentryform1").attr('action','saveCordEntry');
			         document.getElementById('cordentryform1').submit();
			         showprogressMgs();
		        },
		        error: function (request, status, error) {
			        if(request.status===200 && request.statusText==='OK' && status==='parsererror'){		        
			        	appendErrors(eval("("+request.responseText.substring(request.responseText.indexOf("\/\*")+2, request.responseText.lastIndexOf("\*\/"))+")"))
					 }
			        else{
			        	alert("Error " + error);
			            alert(request.responseText);
			        }	        	
		        }	        
			});	
			//$j('.custom').hide();
			$j('label.error,span.error').hide();
			ElementWithHierarchyIndexInit.init();	
	/*if(validateIscordSubmitted()){
		  jConfirm('<s:text name="garuda.cbu.cordentry.alreadysubmitted"/>', '<s:text name="garuda.common.lable.cancel"/>',
					function(r) {
						if (r == true){
							  window.location.href =  "cordEntryInProgress";
						}
		 });
	  }else{
	  	setTimeout(function(){
	  		showprogressMgs();
		},0);
	  setTimeout(function(){
	  var labFlag=validateThawOtherRow('cordentryform1');
	  var dateChckFlag = setMinDateWidRespCollDat();
	  var collectionTimeFlag = validateHours('datepicker3','babybirthtime','datepicker2','cbuCollectionTime','collTimeRangeErrorMsg');
	  var frzTimeFlag = validateHours('datepicker2','cbuCollectionTime','datepicker5','cordFreezeTime','frzTimeRangeErrorMsg');
	  var procDate = validateCordDates('datepicker2','datepicker4','processdateErrorMsg');
	  var prcssDate = validateDateData('datepicker4','cordProcessingTime','datepicker5','cordFreezeTime','precessDatebffrz','precessDatebffrztime');
	  var colDateRange = validateCordDateRange('datepicker3','datepicker2','birthdateRangeErrorMsg',true);
	  var prcDateRange = validateCordDateRange('datepicker2','datepicker4','processdateErrorMsg1',true);
	  var frzDateRange = validateCordDateRange('datepicker2','datepicker5','frzdateRangeErrorMsg',true)
		
	  var rangeFlag=true;
	 $j(".onChngMand").each(function() {
		  rangeFlag=false;
		});
	  $j(".onChngRangeChk").each(function() {
		  rangeFlag=false;
		}); 
	    clinicalFlag = clinicalValidation();
	    if(clinicalFlag)
		   {
		   saveNotesData();
		   }
	    
		var field = FCKeditorAPI.GetInstance('sectionContentsta');
	 	var value = field.GetHTML(true);
	  	$j('#sectionContentsta').val(value);
	    $j("#cordSearchable").val("1");	  
	    var dateElements = $j('.hasDatepicker').get();
		  $j.each(dateElements, function(){
			  if(this.id=='datepicker2' || this.id=='datepicker3' || this.id=='datepicker4' || this.id=='datepicker5')
				  customCordDateRanges(this);
			  else
				  customRange(this);
		  });
	    var flag = true;	    
	    if (validateDeferCbu()){
			jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
					function(r) {
						if (r == false) {
						  }else if(r == true){
							  //loadDivWithFormSubmitDefer('saveAutoDeferCordEntry?autoDeferFlag=false','maincontainerdiv','cordentryform1');
							  $j("#cordentryform1").attr('action','savePartialCordEntry');
							  document.getElementById('cordentryform1').submit();
						  }
			});
		}else if(colDateRange==true && prcDateRange==true && frzDateRange==true && flag == true && labFlag == true && rangeFlag == true && clinicalFlag == true && dateChckFlag == true && collectionTimeFlag == true && frzTimeFlag == true && procDate == true && prcssDate==true){ 
			if($j("#cordentryform1").valid()){
			    if(validateForms()){
			    	
				}else{
					flag = false;
					focusDiv('mrqinfoparent');
				}
			}else{
				flag = false;
				if(validateForms()){
				}else{
					flag = false;
				}
			}
			if(flag){
				if(parseInt(HlaLoadedObject.percentage) != 100){
					  jConfirm('HLA required (A,B,DRB1) not yet entered. Please enter required HLA typing(A,B,DRB1)!', '<s:text name="garuda.common.lable.cancel"/>',
								function(r) {
									if (r == true){
										focusDiv('hlainfoparent');
									}else{
										focusDiv('hlainfoparent');
									}
					 });
				  }else{
			         $j("#cordentryform1").attr('action','saveCordEntry');
			         document.getElementById('cordentryform1').submit();
				  }
		    }
	    }else{
	    	$j("#cordentryform1").valid();
		}
	  },100);
	  setTimeout(function(){
		closeprogressMsg();
	},0);
   }*/
 }

  function validateIscordSubmitted(){
	  var dbValue = false;
	  $j.ajax({
			type : "GET",
			async : false,
			url : 'getDeferStatus?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />',
			success : function(result) {
				dbValue = result.submitCord;									
			}			
	 }); 
     return dbValue;
  }

  function validateDeferCbu(){
	  /* To prevent assessment autodefer confirm popup*/
	  /* if(!($j("#hemoglobinScrnTest").val()==$j("#alphaThalismiaTraitPkVal").val()
			    || $j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDiesasePkVal").val()
			    || $j("#hemoglobinScrnTest").val()==$j("#hemoAlphaThalassmiaPkVal").val()
			    || $j("#hemoglobinScrnTest").val()==$j("#hemoTraitPkVal").val()
			    || $j("#hemoglobinScrnTest").val()==$j("#hemozygousNoDonePkVal").val()
			    || $j("#hemoglobinScrnTest").val()==$j("#hemozygousUnknownPkVal").val()
			    || $j("#hemoglobinScrnTest").val()==$j("#hemoMultiTraitPkVal1").val()

			    || $j("#fungalResult").val()==$j("#fungalCulnotdone").val()			         
	      )){ */
				var dbValue = false;
					$j.ajax({
					        type: "POST",
					        url: "getDeferStatus",
					        data:$j("#cordentryform1").serialize(),
					        async:false,
					        success: function (result){
						       dbValue = result.defer;
					        },
					        error: function (request, status, error) {
					        	alert("Error " + error);
					            alert(request.responseText);
					        }		
						});
			   return dbValue;	
	  /*  } */
  }

  function focusDiv(id){
	  window.location.hash = id;
  }

  function validateHlaFormat(id,val){
	  if(val.length==2){
		  $j("#"+id).val(val+":");
	  }else{
		  $j("#"+id).val(val);
	  }
  }

  function getSiteInfo(siteId,divName){
	  loadPageByGetRequset('setCBBInfo?site.siteId='+siteId+'&flag=1','cbbinforefresh');  
	  refreshingDiv('setCBBInfo?site.siteId='+siteId+'&flag=0',divName,'cordentryform1');  
  }

  function refreshingDiv(url,divName,formId){
		$j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        data : $j("#"+formId).serialize(),
	        success: function (result){
	        	$j('.ui-datepicker').html("");
	            var $response=$j(result);
	            var errorpage = $response.find('#errorForm').html();
	            if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	            }else{
		            $j("#"+divName).html(result);
	            }
	        },
	        error: function (request, status, error) {
	            alert(request.responseText);
	        }
		});
	}
function manageProcCount(procedureId,element){
	if(navigator.appName == 'Microsoft Internet Explorer' && procedureId == -1){
   	 var val = $j(element).val();
 		 var j=0;
 		 var key = $j(element).attr("id");	
 		 for(j=0; j<procesingInfoElements.length;j++){
 			 if(procesingInfoElements[j]==key)
 			     break;
 		 }
 		 var procfoc=procesingInfoElementsVal[j];
 		 procesingInfoElementsVal[j]=val;
 		  if((procfoc=="" || procfoc==-1) && val!="" && val!=-1){
 			 proccount++;
 			  val = parseInt((proccount*100)/(noOfMandatoryProcField));
 			  $j("#processinginfobar").progressbar({
 					value: val
 				}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
 			  addTotalCount();	
 		 }else if((procfoc!="" && procfoc!=-1) && (val=="" || val==-1)){
 		      proccount--;
 			  val = parseInt((proccount*100)/(noOfMandatoryProcField));
 			  $j("#processinginfobar").progressbar({
 					value: val
 				}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
 			  minusTotalCount();
 		 }
 		 addTotalCount();             
      } 
    
	addTotalCount();
}
var beforeChangeVal="";
  function getProcedureInfo(procedureId,id,className,element){
	  var currProc = $j("#currProc").val();
	  var flag=false;
	  manageProcCount(procedureId,element);   
	  $j.ajax({
	        type: "POST",
	        url: 'deferCheckProcedure?procPk='+procedureId,
	        async:false,
	        success: function (result){
			  var collectionDate=new Date($j("#datepicker2").val());
			  var procStartDt = new Date(result.procStartDateStrAdd);			 
			  var prcgStDate = new Date($j("#datepicker4").val());	
			  var procTermiDt = new Date(result.procTermiDateStrAdd);
			  var FreezeDate=new Date($j("#datepicker5").val());

			  if(collectionDate < procStartDt) {
				  alert("<s:text name="garuda.cbu.cordentry.prcdrDateComp"/>");
				  $j("#selfkCbbProcedure").val(beforeChangeVal==""?currProc:beforeChangeVal);
				  beforeChangeVal=(beforeChangeVal=="")?currProc:beforeChangeVal;
				  manageProcCount(procedureId,element);
				  flag=true; 	
				  addTotalCount();
				  return;
			  }

			  if (prcgStDate < procStartDt ||  prcgStDate > procTermiDt){
					 alert("<s:text name="garuda.cbu.cordentry.prcssngDateComp"/>");
					 $j("#selfkCbbProcedure").val(beforeChangeVal==""?currProc:beforeChangeVal);
					 beforeChangeVal=(beforeChangeVal=="")?currProc:beforeChangeVal;
					 manageProcCount(procedureId,element);
					 flag=true;
					 addTotalCount();
					 return;
			  }

			  if (FreezeDate < procStartDt ||  FreezeDate > procTermiDt){
					 alert("<s:text name="garuda.cbu.cordentry.freezedtComp"/>");
					 $j("#selfkCbbProcedure").val(beforeChangeVal==""?currProc:beforeChangeVal);
					 beforeChangeVal=(beforeChangeVal=="")?currProc:beforeChangeVal;
					 manageProcCount(procedureId,element);
					 flag=true;
					 addTotalCount();
					 return;
			  }
	     if(procedureId != -1 &&  procedureId != '') {
		  if(result.checkDefer== "true"){
			  if(procedureId!=currProc){
				   flag=true;				    
			  jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
						function(r) {
							if (r == false) {
								 var param = prepareParam(element,className,procesingInfoElements,procesingInfoElementsVal);
								 AutoDeferChange.deferElement(param);								        	 
								 $j("#"+id).val((beforeChangeVal==""?currProc:beforeChangeVal));
							     AutoDeferChange.elementProgress();
							     AutoDeferChange.elementFocus();
							     AutoDeferChange.gc();								
							  }else if(r == true){
									AutoDeferElementInitVal[$j(element).attr('name')]=	(beforeChangeVal==""?currProc:beforeChangeVal);
									var param = prepareParam(element,className,procesingInfoElements,procesingInfoElementsVal);
									AutoDeferChange.deferElement(param);						
							        var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
								 	var value = field.GetHTML(true);
								  	$j('#sectionContentsta').val(value);
								    $j("#cordSearchable").val("1");	
								    commonMethodForSaveAutoDefer();										
							  }
							});
			  }
		  }
		  else{		         
			  if (	
					  ($j("#filtPap").val()==null) || ($j("#filtPap").val()=="") &&
					 ($j("#rbcPel").val()==null) || ($j("#rbcPel").val()=="") &&
					 ($j("#extDnaAli").val()==null) || ($j("#extDnaAli").val()=="") &&
					 ($j("#noSerAli").val()==null) || ($j("#noSerAli").val()=="") ||
					 ($j("#noPlasAli").val()==null) || ($j("#noPlasAli").val()=="") &&
					 ($j("#nonViaAli").val()==null) || ($j("#nonViaAli").val()=="") &&
					 ($j("#viaCelAli").val()==null) || ($j("#viaCelAli").val()=="") &&
					 ($j("#noSegAvail").val()==null) || ($j("#noSegAvail").val()=="") &&
					 ($j("#cbuOthRepConFin").val()==null) || ($j("#cbuOthRepConFin").val()=="") &&
					 ($j("#cbuRepAltCon").val()==null) || ($j("#cbuRepAltCon").val()=="") &&
					 ($j("#celMatAli").val()==null) || ($j("#celMatAli").val()=="") &&
					 ($j("#plasMatAli").val()==null) || ($j("#plasMatAli").val()=="") &&
					 ($j("#extDnaMat").val()==null) || ($j("#extDnaMat").val()=="") &&
					 ($j("#noMiscMat").val()==null) || ($j("#noMiscMat").val()=="") ){				    
				   
				     noOfMandatoryField=noOfMandatoryField-noOfMandatoryProcField;
					 totalcount=totalcount-proccount;
				     loadPageByGetRequset('refreshCBBProcedures?cbbProcedures.pkProcId='+procedureId,'cbbprocedurerefresh');  
				     addTotalCount();  
			  }else{
				  if(procedureId!=beforeChangeVal){
					    flag=true;					  
					    var param = prepareParam(element,className,procesingInfoElements,procesingInfoElementsVal);
						AutoDeferChange.deferElement(param);
				  jConfirm("<s:text name="garuda.common.cbu.procProcedureAlert"/>", '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
					    if(r==true){
					    	 beforeChangeVal=procedureId;						   
						    //noOfMandatoryField=noOfMandatoryField-noOfMandatoryProcField;
						    //totalcount=totalcount-proccount;						    					    	
					    	loadPageByGetRequset('refreshInternalCBBProcedures?cbbProcedures.pkProcId='+procedureId,'refCbbProcOnly'); 
						}else{							 
							 $j("#"+id).val((beforeChangeVal==""?currProc:beforeChangeVal));							
						     AutoDeferChange.elementProgress();
						     AutoDeferChange.elementFocus();
						     AutoDeferChange.gc();							
						}
				  });
			  }
				  addTotalCount();
			  }			  
		  }
	     }
	        else {
	        	$j("#selfkCbbProcedure").val("");
	        	$j("#selfkCbbProcedure").focus();
	        	$j("#dynaErrorPrcngDt").hide();
	        	$j("#dynaErrorFreezeDt").hide();
	        	addTotalCount();
		        }    
	  }
		});
		if(flag==false)
	    beforeChangeVal=procedureId;
		resetSessionWarningOnTimer();		
		addTotalCount();	
  }

  function getAntigen(locus,method,genomicformat,divId,ext){
	  var meth = $j("#"+method).val();
	  if(parseInt(meth)!=-1){
		  var url = 'getAntigenIdType1?hlaLocus='+locus+'&genomicFormat='+genomicformat+'&typingMethod='+meth+'&ext='+ext;
		  loadPageByGetRequsetFromCordEntry(url,divId);  
	  }
  }

  function getAntigen2(locus,method,genomicformat,divId,ext){
       var meth = $j("#"+method).val();       
	   if(parseInt(meth)!=-1){		  
		  loadPageByGetRequsetFromCordEntry('getAntigenIdType2?hlaLocus='+locus+'&genomicFormat='+genomicformat+'&typingMethod='+meth+'&ext='+ext,divId);
	  }  
  }

  function getMaternalAntigen(locus,method,genomicformat,divId,ext){
	  var meth = $j("#"+method).val();
	  if(parseInt(meth)!=-1){
	     var url = 'getMaternalAntigenIdType1?hlaLocus='+locus+'&genomicFormat='+genomicformat+'&typingMethod='+meth+'&ext='+ext;
	    loadPageByGetRequsetFromCordEntry(url,divId);  
     }
  }

  function getMaternalAntigen2(locus,method,genomicformat,divId,ext){
	  var meth = $j("#"+method).val();
	  if(parseInt(meth)!=-1){
	  loadPageByGetRequsetFromCordEntry('getMaternalAntigenIdType2?hlaLocus='+locus+'&genomicFormat='+genomicformat+'&typingMethod='+meth+'&ext='+ext,divId);  
    }
  }

  function refreshType1AndType2(method,rowindex,locus){
	 if(parseInt(method)!=-1){
	     var val1 = $j("#"+rowindex+"type1").val();
	     var divId = rowindex+"type1div";
	     var divId2 = rowindex+"type2div";
	     var val2 = $j("#"+rowindex+"type2").val();
	     if(val1!=""){
	    	 var url = 'getAntigenIdType1?hlaLocus='+locus+'&genomicFormat='+val1+'&typingMethod='+method+'&ext='+rowindex;
	    	 loadPageByGetRequsetFromCordEntry(url,divId);
	     }
	     if(val2!=""){
	    	 var url1 = 'getAntigenIdType2?hlaLocus='+locus+'&genomicFormat='+val2+'&typingMethod='+method+'&ext='+rowindex;
	    	 loadPageByGetRequsetFromCordEntry(url,divId2);
	     }
	 }
  }

  function refreshMaternalType1AndType2(method,rowindex,locus){
       if(parseInt(method)!=-1){
	     var val1 = $j("#"+rowindex+"maternaltype1").val();
	     var divId = rowindex+"maternaltype1div";
	     var divId2 = rowindex+"maternaltype2div";
	     var val2 = $j("#"+rowindex+"maternaltype2").val();
	     if(val1!=""){
	    	 var url = 'getMaternalAntigenIdType1?hlaLocus='+locus+'&genomicFormat='+val1+'&typingMethod='+method+'&ext='+rowindex;
	    	 loadPageByGetRequsetFromCordEntry(url,divId);
	     }
	     if(val2!=""){
	    	 var url1 = 'getMaternalAntigenIdType2?hlaLocus='+locus+'&genomicFormat='+val2+'&typingMethod='+method+'&ext='+rowindex;
	    	 loadPageByGetRequsetFromCordEntry(url,divId2);
	     }
      }
  }
  
  $j(function(){
		 if($j("#datepicker3").val()!=null && $j("#datepicker3").val()!=""){
			$j('#addnewtest:input').removeAttr('disabled');
		 }
		 var licfocus = "";
		    
		  $j(".licclass").each(function(){
			   noOfMandatoryLicensureField++;
			   noOfMandatoryField++;	
		  });

		  $j(".idclass").each(function(){
			   noOfMandatoryIDField++;
			   noOfMandatoryField++;	
		  });
		  $j(".cbuclass").each(function(){
			  noOfMandatoryCBUField++;
			   noOfMandatoryField++;	
		  });
		  
		  $j(".eligibclass").each(function(){
			  noOfMandatoryEligibilityField++;
			   noOfMandatoryField++;	
		  });

		

		 
		  /*FOR MRQ, FMHQ and IDM */
		  noOfMandatoryField +=1;
		  if($j('#licenseid').val()==$j("#unLicensedPk").val()){
			  noOfMandatoryField +=2;
			  formPerFlag=true;
		  }
		  var licElement= new Array();
		  var licElementVal=new Array();
		  $j(".licclass").each(function(){
			   var val1 = $j(this).val();
			   licElement[licElement.length]=$j(this).attr("id");
				licElementVal[licElementVal.length]=val1;	   	   
			   if(val1!="" && val1!=-1)
			   {
				   licensecount++;
				   val = parseInt((licensecount*100)/(noOfMandatoryLicensureField));
				   $j("#licensurebar").progressbar({
							value: val
				   }).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
				   addTotalCount();		
			   }
		   });		   
		
				 $j(".licclass").bind({change:function() {
			       var val = $j(this).val();
			       var key = $j(this).attr("id");
					 var j=0;
					 for(j=0; j<licElement.length;j++){
						 if(licElement[j]==key)
						     break;
					 }
					 licfocus=licElementVal[j];
					 licElementVal[j]=val;
				 if((licfocus=="" || licfocus==-1) && val!="" && val!=-1){
					licensecount++;
					val1 = parseInt((licensecount*100)/(noOfMandatoryLicensureField));
					   $j("#licensurebar").progressbar({
								value: val1
					   }).children('.ui-progressbar-value').html(val1.toPrecision(3) + '%');
					   addTotalCount();
				 }else if((licfocus!="" && licfocus!=-1) && (val=="" || val==-1)){
					 licensecount--;
					 val1 = parseInt((licensecount*100)/(noOfMandatoryLicensureField));
					   $j("#licensurebar").progressbar({
								value: val1
					   }).children('.ui-progressbar-value').html(val1.toPrecision(3) + '%');
					   minusTotalCount();
				 }
				 if($j('#licenseid').val()==$j("#unLicensedPk").val()){
					 if(formPerFlag==false){
					 	noOfMandatoryField+=2;
					 	var flag="true";
					 	 if(mrqcount>0){
					 		addTotalCount();
					 		flag="false";}
					 	 if(fmhqcount>0){
					 		addTotalCount();
					 		flag="false";}	
					 	if(flag=="true"){
						 	  val1 = parseInt((totalcount*100)/(noOfMandatoryField));
							  $j("#totalbar").progressbar({
									value: val1
								}).children('.ui-progressbar-value').html(val1.toPrecision(3) + '%');						
							  $j("#cordProgress").val(val1);
	                         }					 	
					 	formPerFlag = true;
					 }
				 }else{
					 if(formPerFlag==true){
						noOfMandatoryField -= 2;
						 var flag="true";
						if(mrqcount>0){
							minusTotalCount();flag="false";}
					 	 if(fmhqcount>0){
					 		minusTotalCount();flag="false";}
                         if(flag=="true"){
					 	  val1 = parseInt((totalcount*100)/(noOfMandatoryField));
						  $j("#totalbar").progressbar({
								value: val1
							}).children('.ui-progressbar-value').html(val1.toPrecision(3) + '%');						
						  $j("#cordProgress").val(val1);
                         }												  						
						 formPerFlag = false;
					 }
				}			 
				 addTotalCount();	
			}});

			var idfocus = ""; 
			   var idElements = new Array();
			   var idElementsVal= new Array();    
			   $j(".idclass").each(function(){
				   var val1 = $j(this).val();
				   idElements[idElements.length]=$j(this).attr("id");
				   idElementsVal[idElementsVal.length]=	val1;   	   	   
				   if(val1!="" && val1!=-1)
				   {
					    idcount++;  
					    val = parseInt((idcount*100)/(noOfMandatoryIDField));
						$j("#idinfobar").progressbar({
								value: val
							}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
					   addTotalCount();		
				   }
			   });			  
			
				$j(".idclass").bind({change:function() {
				 var val = $j(this).val();
				 var key = $j(this).attr("id");
				 var j=0;
				 for(j=0; j<idElements.length;j++){
					 if(idElements[j]==key)
					     break;
				 }
				 idfocus=idElementsVal[j];
				 idElementsVal[j]= val;
					 if((idfocus=="" || idfocus==-1) && val!="" && val!=-1){
						 idcount++;  
						val = parseInt((idcount*100)/(noOfMandatoryIDField));
						$j("#idinfobar").progressbar({
									value: val
								}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
						addTotalCount();
					 }else if((idfocus!="" && idfocus!=-1) && (val=="" || val==-1)){
						 idcount--;  
						 val = parseInt((idcount*100)/(noOfMandatoryIDField));
						 $j("#idinfobar").progressbar({
								value: val
							}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
						 minusTotalCount();
					 }
				}});	
			

			var cbufocus = "";
			   $j(".cbuclass").each(function(){
				   var val1 = $j(this).val();
				   cbuInfoElement[cbuInfoElement.length]=$j(this).attr("id");
				   cbuInfoElementVal[cbuInfoElementVal.length]=val1;		   
				   if(val1!="" && val1!=-1 && val1!=null)
				   {
					    cbucount++;  
					    val = parseInt((cbucount*100)/(noOfMandatoryCBUField));
						$j("#cbuinfobar").progressbar({
								value: val
							}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
					   addTotalCount();		
				   }
			   });
			
				$j(".cbuclass").bind({change:function() {					
					 var val = $j(this).val();
					 var val1=val;
					 var j=0;
					 var key = $j(this).attr("id");	
					 for(j=0; j<cbuInfoElement.length;j++){
						 if(cbuInfoElement[j]==key)
						     break;
					 }
				     cbufocus = cbuInfoElementVal[j];
					 if((cbufocus=="" || cbufocus==-1 || cbufocus==null) && val!="" && val!=-1){
						cbucount++;  
						val = parseInt((cbucount*100)/(noOfMandatoryCBUField));
						$j("#cbuinfobar").progressbar({
									value: val
								}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
						addTotalCount();
					 }else if((cbufocus!="" && cbufocus!=-1) && (val=="" || val==-1 || val==null)){
						 cbucount--;  
						 val = parseInt((cbucount*100)/(noOfMandatoryCBUField));
						 $j("#cbuinfobar").progressbar({
								value: val
							}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
						 minusTotalCount();
						 }
					 cbuInfoElementVal[j]=val1;
				}});								   

									var eligfocus = "";                                        
									   $j(".eligibclass").each(function(){
										   var val1 = $j(this).val();
										   eligibilityElement[eligibilityElement.length]=$j(this).attr("id");
										   eligibilityElementVal[eligibilityElementVal.length]=	val1;		   	   
										   if(val1!="" && val1!=-1)
										   {
											   eligibilitycount++;		  
											   val = parseInt((eligibilitycount*100)/(noOfMandatoryEligibilityField));
											   $j("#eligibilitybar").progressbar({
														value: val
													}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
											   addTotalCount();	
										   }
									   });
									
										$j(".eligibclass").bind({change:function() {
										   var val = $j(this).val();
										   var j=0;
										   var key = $j(this).attr("id");
										   for(j=0; j<eligibilityElement.length;j++){
											   if(eligibilityElement[j]==key)
												 break;
										   }
										   eligfocus=eligibilityElementVal[j];
										   eligibilityElementVal[j]=val;
											 if((eligfocus=="" || eligfocus==-1) && val!="" && val!=-1){
												 eligibilitycount++;		  
												 val = parseInt((eligibilitycount*100)/(noOfMandatoryEligibilityField));
												 $j("#eligibilitybar").progressbar({
														value: val
													}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
												 addTotalCount();
											 }else if((eligfocus!="" && eligfocus!=-1) && (val=="" || val==-1)){
												
												 eligibilitycount--;		  
												 val = parseInt((eligibilitycount*100)/(noOfMandatoryEligibilityField));
												 $j("#eligibilitybar").progressbar({
														value: val
													}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
												 minusTotalCount();	
											 }
											 addTotalCount();									
										}});

		 var clinicalfocus = ""; 											 
         $j("#cburegid").keydown(function(e) {
        	 if($j('#non_system_cord').val()!="false"){
			  var val = $j("#cburegid").val();
			  if(e.keyCode!=8 && e.keyCode!=46 )
			  {
				  if(val.length==4 || val.length==9){
					  if(e.which!=109){
						  if(val.length==9 && val.indexOf("-")==-1)
						     $j("#cburegid").val(val);
						  else
							 $j("#cburegid").val(val+"-");
					  }
				  }
			  }
        	 }
		});
	 
	   $j("#regMaternalId").keydown(function(e) {
		   if($j('#non_system_cord').val()!="false"){
			  var val = $j("#regMaternalId").val();
			  if(e.keyCode!=8 && e.keyCode!=46 )
			  {
				  if(val.length==4 || val.length==9){
					  if(e.which!=109){
					      if(val.length==9 && val.indexOf("-")==-1)
						     $j("#regMaternalId").val(val);
						  else
							 $j("#regMaternalId").val(val+"-");
					  }
				  }
			  }
		   }
		});											 
						
	 });	 
 
  
  function addTest(cordId,cbuOtherTestFlag){
	  if($j("#datepicker2").val()==null || $j("#datepicker2").val()==""){
		  alert("<s:text name="garuda.cbu.cordentry.selectCollDate"/>");
		  document.cordentryform1.cbuCollectionDateStr.focus();
	  }
	  else{		
		  		var id="datepicker2";
				var msg="<s:text name='garuda.message.modal.colldatecantbecurdate'/>";
				/* var compCurNCordDate=checkCurrentDateCordDate(id,msg);
				if(compCurNCordDate){ */
		  		var headerLnght = $j("#added thead th").length;
		  		var preTstDate =document.getElementById("preTestDateStrn").value;
		  		var postTstDate =document.getElementById("postTestDateStrn").value;
		  		var postTstThawDate = new Array();
		  		var counter=0;
		  		for(counter=0; counter<headerLnght-3 ; counter++){
		  			 var value1 = counter+1;
		  		var val =$j("#otherTestDateStrn"+value1).val();
		  		postTstThawDate[counter] = val.replace(",",":");
		  		}
	  			var specCollDate = $j("#datepicker2").val();
	  			
	  			var upiob = $j('#idonbagvalresult').val();
				var upiob_data = '';
				if(upiob!="" && upiob != null && upiob != "undefined"){
					upiob_data = ', <s:text name="garuda.cbuentry.label.idbag"/>:'+upiob;
				}
				var title='<s:text name="garuda.cdrcbuview.label.title.addNewTest" /> <s:property value="cdrCbuPojo.registryId" />'+upiob_data;
	  			showModalWithData(title,'addNewTest?cordId='+cordId+'&specimenId=<s:property value="cdrCbuPojo.fkSpecimenId"/>&specCollDate='+specCollDate+'&cbuOtherTestFlag='+cbuOtherTestFlag+'&headerLnght='+headerLnght+'&preTstDate='+preTstDate+'&postTstDate='+postTstDate+'&postTstThawDate='+postTstThawDate,'500','750','cordentryform1','modelPopup2');
  			/* } */
		}
  }
  function editTest(rowIndex,pkPatLabs){
	  var specCollDate = $j("#datepicker3").val();
	  showModal('Edit Test','editAddNewTest?rowIndex='+rowIndex+'&specCollDate='+specCollDate,'500','700'); 
	  }
	  

  function populateHlaTypingDate(id,val){
	  var today = new Date();
	  var d = today.getDate();
	  var m = today.getMonth();
	  var y = today.getFullYear();
	  var h=today.getHours();
	  var mn=today.getMinutes()+1;
	  var date = $j.datepicker.formatDate('M dd, yy', new Date(y, m, d));
	  $j("#"+id).val(date);
  }
  function getDynameicForms(FormName,frmvers){
	  var divname=FormName+"ModalForm";
	  	moduleEntityEventCode = "<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_FORM'/>";
	  	moduleEntityEventCode = moduleEntityEventCode.replace("@"," "+FormName+" ");
	  	moduleEntityEventCode+=" - Version "+frmvers;
	  	
		if($j('#licenseid').val()!="" && $j('#licenseid').val()!=null && $j('#licenseid').val()!='undefined' && $j('#licenseid').val()!="-1"){
			if($j('#fkCbbId').val()!='' && $j('#fkCbbId').val()!=null && $j('#fkCbbId').val()!='undefined' && $j('#fkCbbId').val()!="-1"){
				
				var upiob = $j('#idonbagvalresult').val();
				var upiob_data = '';
				if(upiob!="" && upiob != null && upiob != "undefined"){
					upiob_data = ', <s:text name="garuda.cbuentry.label.idbag"/>:'+upiob;
				}
				var title='<s:text name="garuda.label.forms.addform"/>'+FormName+' <s:text name="garuda.label.forms.formcap"/> for CBU Registry ID <s:property value="cdrCbuPojo.registryId" />'+upiob_data;
				showModals(title,'getDynamicFrm?formId='+FormName+'&siteId='+$j('#fkCbbId').val()+'&isCordEntry=1&entityId=<s:property value="cdrCbuPojo.cordID" />'+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU"/>&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode='+moduleEntityEventCode+'&collectionDate='+$j("#datepicker2").val(),'500','950',divname,true,true);
			}else{
				alert('<s:text name="garuda.common.cbu.reqSiteAlert"/>');
				$j('#fkCbbId').focus();
			}
		}else{
			alert('<s:text name="garuda.common.cbu.reqLicensureAlert"/>');
			$j('#licenseid').focus();
		}
	}
  function f_callDetailReport(cordID){
	  
	  window.open('cbuPdfReports?repId=167&repName=Detail CBU Report&selDate=&params='+cordID+':0000:MRQ:FMHQ:IDM','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');
	 
  }
  
  function setMinDateWidRespCollDat(){
	  var collectionDate = new Date($j("#datepicker2").val());
	  var FreezeDate = new Date($j("#datepicker5").val());
	  var ProcessingDate = new Date($j("#datepicker4").val());

	  var procStartDt; 
	  var val = $j("#datepicker22").val();
	  var flag = true;
	  if (val == 'undefined' || typeof(val) == 'undefined')
	  {	  procStartDt = new Date($j("#procStartDt").val());
		  procTermiDt = new Date($j("#procTermiDt").val());
	  } else
	  {
		  procStartDt = new Date($j("#datepicker22").val());
		  procTermiDt = new Date($j("#datepicker33").val());	
	  }
	  if(collectionDate < procStartDt) {
		  $j("#dynaErrorProcedureDt").show();
		  //$j("#datepicker2").val("");
		  flag = false;
	  } else{
		  $j("#dynaErrorProcedureDt").hide(); 
	  }
	  
	  if(collectionDate>FreezeDate){
		  $j("#dynaErrorFreezeDat").show();
		  $j("#datepicker5").val("");
		  flag = false;
	  }else if(FreezeDate>=collectionDate){
		  $j("#dynaErrorFreezeDat").hide();
		}
	  if(collectionDate>ProcessingDate){
		  $j("#dynaErrorProcessinDat").show();
		  //$j("#datepicker4").val("");
		  flag = false;
	  }else if(ProcessingDate>=collectionDate){
		  $j("#dynaErrorProcessinDat").hide();
	  }

	  if (ProcessingDate < procStartDt || ProcessingDate > procTermiDt) {
		  $j("#dynaErrorPrcngDt").show();
		//  $j("#datepicker4").val("");
		  flag = false;
	  }else{
		  $j("#dynaErrorPrcngDt").hide();
	  }

	  if (FreezeDate < procStartDt || FreezeDate > procTermiDt) {
		  $j("#dynaErrorFreezeDt").show();
		 // $j("#datepicker5").val("");
		  flag = false;
	  }else{
		  $j("#dynaErrorFreezeDt").hide();
	  }
	  return flag;
  }

  function validateSignCordEntry(value,id,progid,invalid,minimum,pass){
		if(id=="" || id==null){
			id="submit";			
		}
		if(invalid=="" || invalid==null){
			invalid="invalid";			
		}
		if(minimum=="" || minimum==null){
			minimum="minimum";			
		}
		if(pass=="" || pass==null){
			pass="pass";			
		}
		if(value.length<4){
			$j("#"+minimum).css('display','block');
			$j("#"+invalid).css('display','none');
			$j("#"+pass).css('display','none');
			$j("#"+id).attr("disabled", true);
			$j("#"+id).addClass('ui-state-disabled');
			$j("#"+progid).attr("disabled", true);
			$j("#"+progid).addClass('ui-state-disabled');
			return false;
		}else{
		var url = "getEsignDetails?userPojo.signature="+value;
			$j.ajax({
				type : "POST",
				url : url,
				async : false,
				url : 'getEsignDetails?userPojo.signature='+value,
				success : function(result) {
				if(result.message=="true"){	
					$j("#"+minimum).css('display','none');
					$j("#"+invalid).css('display','none');
					$j("#"+pass).css('display','block'); 
				    $j("#"+id).attr("disabled", false);
				    $j("#"+id).removeClass('ui-state-disabled');
				    $j("#"+progid).attr("disabled", false);
					$j("#"+progid).removeClass('ui-state-disabled');
	              }else{
	            	  $j("#"+minimum).css('display','none');
	            	  $j("#"+invalid).css('display','block');
	            	  $j("#"+pass).css('display','none'); 
					  $j("#"+id).attr("disabled", true);
					  $j("#"+id).addClass('ui-state-disabled');
					  $j("#"+progid).attr("disabled", true);
					  $j("#"+progid).addClass('ui-state-disabled');
				 }
			   }
			});		
		}
	}
  
  function checkTimeFormatForCord(fieldId){
	  var value=$j("#"+fieldId).val();
	  if(value=="" || value==undefined){
         return true;
	   }else{
		  var returnFlag=dateTimeFormat(value);
		  if(returnFlag!=true){
					 //$j("#"+fieldId).val();
					 $j("#"+"dynaError"+fieldId).show();
				 }
				 else{
					 $j("#"+"dynaError"+fieldId).hide();
				 }
			 return returnFlag;
	   }
  }
  function deleteRow(rowid) {            	 
		$j('#additionalid_row'+rowid).remove();	
      maxadditionalidcount--;	
      if(maxadditionalidcount < 10){            			    
		   $j("#addbutton").css("cursor","pointer");
      }			
   /* removing the additional ID Unique Product identity on bag drop down.*/           	
		htmlUniqueIdOnBag();
		if(parseInt($j('#cbuOnBag').val()) ==-1 ){
		  	   $j("#idonbagvalresult").val("");}	 
  }	
  function addAdditionalId(){	            
		 if(maxadditionalidcount < 10){
			$j('#additionalidtable').each(function(){			
		       var n = 0;              
			       n = (additionalIdCount==0) ? 0  : additionalIdCount ; 
					   additionalIdCount++;
					   maxadditionalidcount++;			       
              			  
		    	   var $table = $j(this);    	   
		    	   var tds = '';		    	  
		    	       tds += '<tr id="additionalid_row'+n+'"><td><s:text name="garuda.cordentry.label.additionalIdDesc"></s:text>:<span style="color: red;">*</span></td><td><s:textfield cssClass="required additionalids" id="additionalIds'+n+'additionalIdDescid"  name="additionalIds['+n+'].additionalIdDesc" maxlength="30" onchange="additonalIdscall('+n+')" ></s:textfield></td>'
		        	   tds +='<td><s:text name="garuda.cordentry.label.additionalTypeId"></s:text>:<span style="color: red;">*</span></td><td><select id="additionalIds'+n+'additionalIdValueTypeid" name="additionalIds['+n+'].additionalIdValueType" class="required additionalids" onchange="additonalIdscall('+n+')" ><option value="">Select</option><option value="cbu">CBU</option><option value="maternal">Maternal</option></select></td>';
		       	       tds += '<td><s:text name="garuda.cordentry.label.additionalId"></s:text>:<span style="color: red;">*</span></td><td><s:textfield cssClass="required additionalids" id="additionalIds'+n+'additionalId" name="additionalIds['+n+'].additionalId" maxlength="30"  onchange="additonalIdscall('+n+')"></s:textfield></td><td><img style="cursor: pointer;vertical-align:middle;"  src="images/cross.png" border="0"  onclick="javascript:deleteRow('+n+');" /></td></tr>';
	          	       $j(this).append(tds);   
		           	if(maxadditionalidcount==10){
		   		      $j("#addbutton").css("cursor","default");
		   	        } 	   
		       });			   
		 }				 
				    else{
				      $j("#addbutton").css("cursor","default");
				  }
	 }	

 function htmlUniqueIdOnBag(){
	     var paramObj={
	    	     data:$j('#idonbagvalresult').val(),
	    	     regId:$j('#cburegid').val(),
	    	     isbt:$j('#isbidin').val(),
	    	     locId:$j('#loccbuid').val()
	     }
     var  url = "getUniqIdOnBagHtml?"+$j.param(paramObj);
      $j.ajax({
        type: "GET",
        url: url,
        async:true,
		data : $j(".additionalids").serialize(),
        success: function (result, status, error){		        	
        	     	$j("#idonbag").html(result.htmlStr);
        	     	setIdOnBagValResult()
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});	            
}
 function uniqueIdOnBagVal(val){ 
	   setIdOnBagValResult();
 }
 
 function additonalIdscall(rownumber){
     var description   = $j("#additionalIds"+rownumber+"additionalIdDescid").val();
		  var typeofid      = $j("#additionalIds"+rownumber+"additionalIdValueTypeid").val();
		  var additionalid  = $j("#additionalIds"+rownumber+"additionalId").val();
		  if(description !='' && typeofid !='Select' && additionalid !=''){
		     htmlUniqueIdOnBag();				 
		 }
}   
 function setIdOnBagValResult(){ 
	     var val1= $j("#cbuOnBag option:selected").text();
	    val1= (val1=="Select")? "": $j("#cbuOnBag option:selected").text();	       
	       $j("#idonbagvalresult").val(val1);	     
	       $j('input#idonbagvalresult.idclass').trigger('change');          
	  }

 function collDateCustomRange(input){		
	 if($j('#datepicker3').val() !=""){
		var minTestDate=new Date($j("#datepicker3").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		$j('#datepicker2').datepicker( "option", "minDate", new Date(y1,m1,d1));	
         }//end of if block
			else{
				$j('#datepicker2').datepicker( "option", "minDate", new Date(null));		
			 }	
	 var result = $j.browser.msie ? !this.fixFocusIE : true;
     this.fixFocusIE = false;
     return result; 
 }// end of collDateCustomRange function
 function birthcustomRange(input){
	 if($j('#datepicker2').val() !=""){
		    var minTestDate=new Date($j("#datepicker2").val());
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
			 $j('#datepicker3').datepicker( "option", "maxDate", new Date(y1,m1,d1));
	 }
	else{
	       $j('#datepicker3').datepicker( "option", "minDate", "");

		}
			 var result = $j.browser.msie ? !this.fixFocusIE : true;
     this.fixFocusIE = false;
     return result;	
 }
 function procCustomRange(input){
	 if($j('#datepicker2').val() !=""){
		    var minTestDate=new Date($j("#datepicker2").val());
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();			
			$j('#datepicker4').datepicker( "option", "minDate", new Date(y1,m1,d1));
	 }
	 else{		
		 $j('#datepicker4').datepicker( "option", "minDate", new Date(null));	
		}
	 var result = $j.browser.msie ? !this.fixFocusIE : true;
     this.fixFocusIE = false;
     return result;	
 }
 function freezCustomMinRange(input){
	 if($j('#datepicker2').val() !=""){
		    var minTestDate=new Date($j("#datepicker2").val());
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
			//Getter
			var maxDate = $j('#datepicker5').datepicker( "option", "maxDate" );
			var minDate = $j('#datepicker5').datepicker( "option", "minDate" );
			//Setter
			$j('#datepicker5').datepicker( "option", "maxDate", new Date(y1,m1,d1+2));
			$j('#datepicker5').datepicker( "option", "minDate", new Date(y1,m1,d1));
		
	 }
	 else{
		//Getter
			var minDate = $j('#datepicker5').datepicker( "option", "minDate" );
		//Setter
		 $j('#datepicker5').datepicker( "option", "minDate", new Date(null));			
		}	
		 var result = $j.browser.msie ? !this.fixFocusIE : true;
	     this.fixFocusIE = false;
	     return result;
 }

function showforms(){
	if($j('#licenseid').val()==$j("#unLicensedPk").val()){
		$j('.formsClass').show();
		//noOfMandatoryField +=2;
		//addTotalCount();
	}else{
		$j('.formsClass').hide();
		//minusTotalCount();
	}
}
 function changeLicensure(value,PrvValue,className,element){
	 if(((value == document.getElementById("inEligiblePk").value) || (value == document.getElementById("incompleteReasonId").value))&&($j("#licenseid").val()!='<s:property value="cdrCbuPojo.unLicesePkid"/>')&&($j("#licenseid").val()!='-1')){
		 var param = prepareParam(element,className,eligibilityElement,eligibilityElementVal);
		 AutoDeferChange.deferElement(param);
		 jConfirm('<table><tr><td><s:text name="garuda.update.license.resetConfirm"/></td></tr></table>', '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
			    if(r==true){
				    if($j("#licenseid").val()!='<s:property value="cdrCbuPojo.unLicesePkid"/>'){
					    $j("#licenseid").val('<s:property value="cdrCbuPojo.unLicesePkid"/>');
					    unliceseReason('<s:property value="cdrCbuPojo.unLicesePkid"/>','licenseReason');
					    $j("#fkCordCbuUnlicenseReason").val('<s:property value="cdrCbuPojo.unLicesePkid"/>');				    						     
					    $j("#licenseid").focus();					     
					    showforms();
					    $j('select#licenseid.licclass').trigger('change');
				    }
				    AutoDeferChange.elementProgress();
				    AutoDeferChange.gc();
				    addTotalCount();
				}
			     else{
			          PrvValue=(PrvValue=="")? -1:PrvValue;					     
			    	  $j("#fkCordCbuEligible").val(PrvValue);
			         //$j("#"+Id).val(AutoDeferElementInitVal[$j(element).attr('name')]);							
					  AutoDeferChange.elementProgress();
					  AutoDeferChange.elementFocus();
					  AutoDeferChange.gc();				    
			    	inEligibleReason(PrvValue,'inEligibleReason');
			    	addTotalCount();
			    }
		});
	 }
	 //eligibilityStatus=value;
 }
	$j(function(){
		$j('#licenseid').change(function(){
			resetingEligibleProcess(this.value);
		});
	});
	function resetingEligibleProcess(valu){
		if((($j("#fkCordCbuEligible").val()== document.getElementById("inEligiblePk").value) || ($j("#fkCordCbuEligible").val() == document.getElementById("incompleteReasonId").value))&&($j("#licenseid").val()!='<s:property value="cdrCbuPojo.unLicesePkid"/>')&&($j("#licenseid").val()!='-1')){
			$j("#licenseid").val('<s:property value="cdrCbuPojo.unLicesePkid"/>');
			unliceseReason($j("#licenseid").val(),'licenseReason');
			var leng='<s:property value="licenseReasons.length"/>';
			if(leng>0){
				var reasonttt = new Array();
				var i=0;
				for(i=0;i<leng;i++){
					var fieldId="liCReasons["+i+"]";
						reasonttt[i]=document.getElementById(fieldId).value;
				}
				$j("#fkCordCbuUnlicenseReason").val(reasonttt);
			}
			else{
				$j("#fkCordCbuUnlicenseReason").val('<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON, @com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON_INTERNATIONAL)"/>');
			}
			$j( "#dialog-resetLicDiv" ).dialog({
				resizable: false,
				modal: true,
				closeText: '',
				closeOnEscape: false ,				
				beforeClose: function(e) {
					    $j('select#licenseid.licclass').trigger('change');				
					},				
				buttons: {
					"OK": function() {
						$j( this ).dialog( "close" );					
					}
				}
			});
		}
		addTotalCount();
	}
	
 function inRefEligibleReason(value,divName){
	 if((value == document.getElementById("inEligiblePk").value) || (value == document.getElementById("incompleteReasonId").value)){
	 	document.getElementById(divName).style.display='block';
	 	$j("#ineligiblereason").show();
	 	if(value == document.getElementById("inEligiblePk").value){
	 		$j("#inelreason").css('display','block');
	 		$j("#increason").css('display','none');
	 		$j("#priorreason").css('display','none');
	 		$j("#additonalinfo").css('display','none');
	 	}else if(value == document.getElementById("incompleteReasonId").value){
	 		$j("#inelreason").css('display','none');
	 		$j("#increason").css('display','block');
	 		$j("#priorreason").css('display','none');
	 		$j("#additonalinfo").css('display','none');
	 	}
	 }
	 else{
	 	document.getElementById(divName).style.display='none';
	 	if(value == document.getElementById("notCollectedToPriorReasonId").value){
	 		document.getElementById(divName).style.display='block';
	 		document.getElementById("ineligiblereason").style.display='none';
	 		if($j("#newQstns").length>0){
	 			$j("#newQstns").hide();
	 		}
	 	}else if(value != document.getElementById("inEligiblePk").value && value != document.getElementById("incompleteReasonId").value){
	 		document.getElementById(divName).style.display='none';
	 		}
	 	else{
	 		document.getElementById(divName).style.display='block';
	 		}
	 	$j("#additonalinfo").css('display','block');
	 }
}
function validateForms(){
	var flag = true;
	if($j('#licenseid').val() == $j("#unLicensedPk").val()){
		if($j("#fmhqformversionlstsize").val()!=null && $j('#fmhqformversionlstsize').val()>0){
			$j('#fmhqFormMan').hide();
		}else{
			flag = false;
			$j('#fmhqFormMan').show();
		}
		if($j("#mrqformversionlstsize").val()!=null && $j('#mrqformversionlstsize').val()>0){
			$j('#mrqFormMan').hide();
		}else{
			flag = false;
			$j('#mrqFormMan').show();
		}
		if($j("#idmformversionlstsize").val()!=null && $j('#idmformversionlstsize').val()>0){
			$j('#idmFormMan').hide();
		}else{
			flag = false;
			$j('#idmFormMan').show();
		}
	}
	if($j('#licenseid').val() != $j("#unLicensedPk").val() && $j('#licenseid').val()!='-1'){
		if($j("#idmformversionlstsize").val()!=null && $j('#idmformversionlstsize').val()>0){
			$j('#idmFormMan').hide();
		}else{
			flag = false;
			$j('#idmFormMan').show();
		}
	}
	return flag;
}


function updateProgress(){
	
	  var cordProgress = parseInt((sumTotal*100)/(noOfMandatoryField));	
	  var url = "updateCordProgress?cdrCbuPojo.cordID=<s:property value='cdrCbuPojo.cordID'/>&cdrCbuPojo.cordEntryProgress="+cordProgress;
	  $j.ajax({
        type: "GET",
        url: url,
        async:false,
        success: function (result){	  	        	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
}          

 
 function checkRangeMthd(fieldId,fkTestId,fieldVal,prePostOthFlag,fromSearchingFlag){
		var preFlag="pre";
		var postFlag="post";
		var othThawFlag="otherThaw";
		var txtFldClsValueMand="onChngMand";
		var txtFldClsValRangChk="onChngRangeChk";
		var fromSearchingFlag=fromSearchingFlag;
		var txtFldCls="";
		var chkMinVal=null;
		var chkMaxVal=null;
		var dispMsg1="";
		var cbuAbsFldId="";
		var cbuPrcntFldId="";
		var tbncCntFldId="";
		var calcValucNbrcAbs="";
		if(prePostOthFlag==postFlag){
			cbuAbsFldId="savePostTestList10testresult";
			cbuPrcntFldId="savePostTestList11testresult";
			tbncCntFldId="savePostTestList1testresult";
		}
		//var dbStoredFieldVal=$j("#"+fieldId+"_dbStoredVal").val();
		var CBU_volume_test = document.getElementById('CBU_VOL').value;
		var Ttl_CBU_Ncltd_cnt = document.getElementById('TCNCC').value;
		var Ttl_CBU_Ncltd_cnt_uncrtd = document.getElementById('UNCRCT').value;
		var Ttl_CBU_Ncltd_cnt_unkif_uncrtd = document.getElementById('TCBUUN').value;
		if(fkTestId==Ttl_CBU_Ncltd_cnt || fkTestId==Ttl_CBU_Ncltd_cnt_uncrtd || fkTestId==Ttl_CBU_Ncltd_cnt_unkif_uncrtd){
			chkMinVal=12;
			chkMaxVal=999;
			dispMsg1="<s:text name="garuda.common.range.testval12to999"/>";
		}
		var CBU_Ncltd_Cell_Cncton = document.getElementById('CBUNCCC').value;
		if(fkTestId==CBU_Ncltd_Cell_Cncton){
			chkMinVal=5000;
			chkMaxVal=200000;
			dispMsg1="<s:text name="garuda.common.range.testval5000to200000"/>";
		}
		if(fkTestId==CBU_Ncltd_Cell_Cncton && prePostOthFlag==preFlag){
			chkMinVal=1000;
			chkMaxVal=50000;
			dispMsg1="<s:text name="garuda.common.range.testval1000to50000"/>";
		}
		var Final_Pdct_Vol = document.getElementById('FNPV').value;
		if(fkTestId==Final_Pdct_Vol){
			chkMinVal=10;
			chkMaxVal=600;
			dispMsg1="<s:text name="garuda.common.range.testval10to600"/>";
		}
		var Ttl_CD34_Cel_Cnt = document.getElementById('TCDAD').value;
		if(fkTestId==Ttl_CD34_Cel_Cnt){
			chkMinVal=0.0;
			chkMaxVal=999.9;
			dispMsg1="<s:text name="garuda.common.range.val0to999"/>";
		}
		var prcnt_of_CD34_Vab = document.getElementById('PERCD').value;
		if(fkTestId==prcnt_of_CD34_Vab){
			chkMinVal=0.0;
			chkMaxVal=100;
			dispMsg1="<s:text name="garuda.common.range.val0to100"/>";
		}
		var prcnt_of_CD34_ttl_Monclr_Cl_Cnt = document.getElementById('PERTMON').value;
		if(fkTestId==prcnt_of_CD34_ttl_Monclr_Cl_Cnt){
			chkMinVal=0.0;
			chkMaxVal=10.0;
			dispMsg1="<s:text name="garuda.common.range.testrange0to10"/>";
		}
		var prcnt_of_Monclr_Cls_Ttl_Ncltd_Cnt = document.getElementById('PERTNUC').value;
		if(fkTestId==prcnt_of_Monclr_Cls_Ttl_Ncltd_Cnt){
			chkMinVal=20.0;
			chkMaxVal=99.9;
			dispMsg1="<s:text name="garuda.common.range.val20to99"/>";
		}
		var Ttl_CD3_Cel_Cnt = document.getElementById('TOTCD').value;
		if(fkTestId==Ttl_CD3_Cel_Cnt){
			chkMinVal=0;
			chkMaxVal=999.9;
			dispMsg1="<s:text name="garuda.common.range.val0to999"/>";
		}
		var prcnt_CD3_Cls_Ttl_Monclr_Cl_Cnt = document.getElementById('PERCD3').value;
		if(fkTestId==prcnt_CD3_Cls_Ttl_Monclr_Cl_Cnt){
			chkMinVal=0;
			chkMaxVal=99.9;
			dispMsg1="<s:text name="garuda.common.range.val0to99"/>";
		}
		var CBU_nRBC_Abslt_No = document.getElementById('CNRBC').value;
		if(fkTestId==CBU_nRBC_Abslt_No){
			chkMinVal=0;
			chkMaxVal=9999;
			dispMsg1="<s:text name="garuda.common.range.val0to9999"/>";
		}
		var CBU_nRBC_prcnt = document.getElementById('PERNRBC').value;
		if(fkTestId==CBU_nRBC_prcnt){
			chkMinVal=0;
			chkMaxVal=100;
			dispMsg1="<s:text name="garuda.common.range.val0to100"/>";
		}
		var CFU_Cnt_test = document.getElementById('CFUCNT').value;
		if(fkTestId==CFU_Cnt_test){
			chkMinVal=0;
			chkMaxVal=999;
			dispMsg1="<s:text name="garuda.common.range.testrange0to999"/>";
		}
		var Viability_test = document.getElementById('VIAB').value;
		if(fkTestId==Viability_test){
			chkMinVal=0;
			chkMaxVal=100;
			dispMsg1="<s:text name="garuda.common.range.val0to100"/>";
		}
		/* var NAT_HIV_test = document.getElementById('NATHIV').value;
		var NAT_HCV_test = document.getElementById('NATHCV').value;
		if(fkTestId==NAT_HIV_test || fkTestId==NAT_HCV_test){
			chkMinVal=0;
			chkMaxVal=0;
			dispMsg1="";
		} */
		switch(fkTestId){
		case CBU_volume_test: 
				if((fieldVal>=40 && fieldVal<=500)){
					$j("#"+fieldId+"_error").hide();
					$j("#"+fieldId).removeClass(txtFldClsValueMand);
					$j("#"+fieldId).removeClass(txtFldClsValRangChk);
				}
				else{
					$j("#"+fieldId+"_error").show();
					if(prePostOthFlag==preFlag)
						{
							if(fieldVal==null || fieldVal==''){
								txtFldCls=txtFldClsValueMand;
								$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
							}
							else{
								txtFldCls=txtFldClsValRangChk;
								$j("#"+fieldId+"_error").text("<s:text name="garuda.common.range.testval40to500"/>");
							}
					}
					else if(prePostOthFlag==postFlag){
						if(fieldVal==null || fieldVal==''){
							$j("#"+fieldId).removeClass(txtFldClsValueMand);
							$j("#"+fieldId).removeClass(txtFldClsValRangChk);
						}
						else{
							txtFldCls=txtFldClsValRangChk;
							$j("#"+fieldId+"_error").text("<s:text name="garuda.common.range.testval40to500"/>");
						}
					}
					else if(prePostOthFlag==othThawFlag){
						if(fieldVal==null || fieldVal==''){
							$j("#"+fieldId).removeClass(txtFldClsValueMand);
							$j("#"+fieldId).removeClass(txtFldClsValRangChk);
						}
						else{
							txtFldCls=txtFldClsValRangChk;
							$j("#"+fieldId+"_error").text("<s:text name="garuda.common.range.testval40to500"/>");
						}
					}
					$j("#"+fieldId).addClass(txtFldCls);
				}
				break;
		case Ttl_CD34_Cel_Cnt: 
		case CBU_nRBC_Abslt_No: 
		case CBU_nRBC_prcnt: 
		case CFU_Cnt_test: 
		case Viability_test:
		case Ttl_CBU_Ncltd_cnt:
		case Ttl_CBU_Ncltd_cnt_uncrtd: 
		case Ttl_CBU_Ncltd_cnt_unkif_uncrtd:
			if((fieldVal!="" && fieldVal>=chkMinVal && fieldVal<=chkMaxVal)){
				$j("#"+fieldId+"_error").hide();
				$j("#"+fieldId).removeClass(txtFldClsValueMand);
				$j("#"+fieldId).removeClass(txtFldClsValRangChk);
			}
			else{
				$j("#"+fieldId+"_error").show();
				if(prePostOthFlag==preFlag)
					{
						if(fieldVal==null || fieldVal==''){
							$j("#"+fieldId+"_error").hide();
							$j("#"+fieldId).removeClass(txtFldClsValueMand);
							$j("#"+fieldId).removeClass(txtFldClsValRangChk);
						}
						else{
							txtFldCls=txtFldClsValRangChk;
							$j("#"+fieldId+"_error").text(dispMsg1);
						}
				}
				else if(prePostOthFlag==postFlag){
					if(fieldVal==null || fieldVal==''){
						txtFldCls=txtFldClsValueMand;
						$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
					}
					else{
						txtFldCls=txtFldClsValRangChk;
						$j("#"+fieldId+"_error").text(dispMsg1);
					}
					
				}
				else if(prePostOthFlag==othThawFlag){
					if(fieldVal==null || fieldVal==''){
						$j("#"+fieldId+"_error").hide();
						$j("#"+fieldId).removeClass(txtFldClsValueMand);
						$j("#"+fieldId).removeClass(txtFldClsValRangChk);
					}
					else{
						txtFldCls=txtFldClsValRangChk;
						$j("#"+fieldId+"_error").text(dispMsg1);
					}
				}
				$j("#"+fieldId).addClass(txtFldCls);
			}
			break;
		case CBU_Ncltd_Cell_Cncton:
		case Final_Pdct_Vol:
		case prcnt_of_CD34_Vab: 
		case prcnt_of_CD34_ttl_Monclr_Cl_Cnt:
		case prcnt_of_Monclr_Cls_Ttl_Ncltd_Cnt:
		case Ttl_CD3_Cel_Cnt: 
		case prcnt_CD3_Cls_Ttl_Monclr_Cl_Cnt:
			if((fieldVal>=chkMinVal && fieldVal<=chkMaxVal)){
				$j("#"+fieldId+"_error").hide();
				$j("#"+fieldId).removeClass(txtFldClsValueMand);
				$j("#"+fieldId).removeClass(txtFldClsValRangChk);
			}
			else{
				$j("#"+fieldId+"_error").show();
					if(fieldVal==null || fieldVal==''){
						$j("#"+fieldId+"_error").hide();
						$j("#"+fieldId).removeClass(txtFldClsValueMand);
						$j("#"+fieldId).removeClass(txtFldClsValRangChk);
					}
					else{
						txtFldCls=txtFldClsValRangChk;
						$j("#"+fieldId+"_error").text(dispMsg1);
					}
					$j("#"+fieldId).addClass(txtFldCls);
			}
			break;
		/* case NAT_HIV_test: 
		case NAT_HCV_test: 
			$j("#"+fieldId+"_error").hide();
			break; */
		default:
			$j("#"+fieldId+"_error").hide();
			$j("#"+fieldId).removeClass(txtFldClsValueMand);
			$j("#"+fieldId).removeClass(txtFldClsValRangChk);
		}
		$j("#labSummeryDivR input").live({keydown:function(e) {
			 var key = e.charCode || e.keyCode;
			 if(key==9){
				 calcCbuNbrcFlag=false;
			 }
			 else{
				 calcCbuNbrcFlag=true;
			 }
		}});
		if(prePostOthFlag==postFlag && fromSearchingFlag!='fromSearching' && calcCbuNbrcFlag==true){
		if( fkTestId==Ttl_CBU_Ncltd_cnt || fkTestId==Ttl_CBU_Ncltd_cnt_uncrtd || fkTestId==Ttl_CBU_Ncltd_cnt_unkif_uncrtd || fkTestId==CBU_nRBC_prcnt || fkTestId==CBU_nRBC_Abslt_No){
			if(fkTestId==Ttl_CBU_Ncltd_cnt || fkTestId==Ttl_CBU_Ncltd_cnt_uncrtd || fkTestId==Ttl_CBU_Ncltd_cnt_unkif_uncrtd ){
				if(fieldVal==null || fieldVal==''){
						$j("#"+cbuAbsFldId).val("");
						$j("#"+cbuPrcntFldId).val("");
				}
				else{
					if(($j("#"+cbuAbsFldId).val()!=null && $j("#"+cbuAbsFldId).val()!='') && ($j("#"+cbuPrcntFldId).val()==null || $j("#"+cbuPrcntFldId).val()=='')){
						calcValucNbrcAbs=($j("#"+cbuAbsFldId).val()/($j("#"+tbncCntFldId).val()*10))*100;
						$j("#"+cbuPrcntFldId).val(Math.round(calcValucNbrcAbs));
					}
					else if(($j("#"+cbuAbsFldId).val()==null && $j("#"+cbuAbsFldId).val()=='') && ($j("#"+cbuPrcntFldId).val()!=null || $j("#"+cbuPrcntFldId).val()!='')){
						calcValucNbrcAbs=($j("#"+cbuPrcntFldId).val()/100)*($j("#"+tbncCntFldId).val()*10);
						$j("#"+cbuAbsFldId).val(calcValucNbrcAbs);
					}
					else if(($j("#"+cbuAbsFldId).val()!=null && $j("#"+cbuAbsFldId).val()!='') && ($j("#"+cbuPrcntFldId).val()!=null || $j("#"+cbuPrcntFldId).val()!='')){
						$j("#"+cbuAbsFldId).val("");
						$j("#"+cbuPrcntFldId).val("");
					}
				}
			}
			else if(fkTestId==CBU_nRBC_prcnt){
					if(fieldVal==null || fieldVal==''){
						$j("#"+cbuAbsFldId).val("");
					}
					else if($j("#"+tbncCntFldId).val()!=null && $j("#"+tbncCntFldId).val()!=''){
						calcValucNbrcAbs=($j("#"+cbuPrcntFldId).val()/100)*($j("#"+tbncCntFldId).val()*10);
						if(calcValucNbrcAbs<0 || calcValucNbrcAbs>9999){
							$j("#"+cbuAbsFldId).val("");
							$j("#"+cbuPrcntFldId).val("");
							$j("#"+fieldId+"_error").show();
							$j("#"+fieldId+"_error").text("Please enter valid value");
						}
						else{
							$j("#"+cbuAbsFldId+"_error").hide();
							$j("#"+cbuAbsFldId).val(calcValucNbrcAbs);
						}
					}
					else if($j("#"+tbncCntFldId).val()==null || $j("#"+tbncCntFldId).val()==''){
						$j("#"+cbuPrcntFldId).val("");
						alert("Please Enter value for Total CBU Nucleated Cell Count");
					}
			}
			else if(fkTestId==CBU_nRBC_Abslt_No){
				if(fieldVal==null || fieldVal==''){
					$j("#"+cbuPrcntFldId).val("");
				}
				else if($j("#"+tbncCntFldId).val()!=null && $j("#"+tbncCntFldId).val()!=''){
					calcValucNbrcAbs=($j("#"+cbuAbsFldId).val()/($j("#"+tbncCntFldId).val()*10))*100;
					if(Math.round(calcValucNbrcAbs)<0 || Math.round(calcValucNbrcAbs)>100){
						$j("#"+cbuAbsFldId).val("");
						$j("#"+cbuPrcntFldId).val("");
						$j("#"+fieldId+"_error").show();
						$j("#"+fieldId+"_error").text("Please enter valid value");
					}
					else{
						$j("#"+cbuPrcntFldId+"_error").hide();
					$j("#"+cbuPrcntFldId).val(Math.round(calcValucNbrcAbs));
					}
				}
				else if($j("#"+tbncCntFldId).val()==null || $j("#"+tbncCntFldId).val()==''){
					$j("#"+cbuAbsFldId).val("");
					alert("Please Enter value for Total CBU Nucleated Cell Count");
				}
		}
		}}
		$j("input#savePreTestList0testresult.labclass").trigger("change");
		$j("input#savePostTestList1testresult.labclass").trigger("change");
		$j("input#savePostTestList4testresult.labclass").trigger("change");
		$j("input#savePostTestList10testresult.labclass").trigger("change");
		$j("input#savePostTestList11testresult.labclass").trigger("change");
		$j("input#savePostTestList12testresult.labclass").trigger("change");
		$j("select#cfuSelected.labclass").trigger("change");
		$j("input#savePostTestList13testresult.labclass").trigger("change");
		$j("select#viabSelected.labclass").trigger("change");
		addTotalCount();
	}
 function validateRangeBeforSaveInPrg(formId){
	 	var flag=true;
		var rangeFlag=true;
		var headerLnght = $j("#added thead th").length;
		var thawRow=headerLnght-3;
		var thawOtherRow=1;
		for(thawRow=0;thawRow<headerLnght;thawRow++){
			var n=0;
			$j("#added tr:gt(1)").each(function(){
				if(n>=$j("#noOfLabGrpTypTests").val()){
					
				}
				else{
				var fieldId = 'savePreTestList'+n+'testresult';
				if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
					var tempVarbl=document.getElementById(fieldId).value;
					if(tempVarbl != null){
						var fieldName='savePreTestList['+n+'].fktestid';
						var fkTestId=$j("input[name='"+fieldName+"']").val();
						checkRangeMthd(fieldId,fkTestId,tempVarbl,'pre','fromSearching');
					}
				}
					fieldId = 'savePostTestList'+n+'testresult';
					if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
						var tempVarbl=document.getElementById(fieldId).value;
						if(tempVarbl != null){
							var fieldName='savePostTestList['+n+'].fktestid';
							var fkTestId=$j("input[name='"+fieldName+"']").val();
							checkRangeMthd(fieldId,fkTestId,tempVarbl,'post','fromSearching');
						}
					}
					fieldId = 'saveOtherPostTestList'+thawOtherRow+n+'testresult';
				if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
				var tempVarbl=document.getElementById(fieldId).value;
				if(tempVarbl != null && tempVarbl !=''){
					var fieldName='saveOtherPostTestList'+thawOtherRow+'['+n+'].fktestid';
					var fkTestId=$j("input[name='"+fieldName+"']").val();
					checkRangeMthd(fieldId,fkTestId,tempVarbl,'otherThaw');
					}
				}
				n++;
				}
			});
			thawOtherRow++;
		}
		return flag; 
 }
 function validateThawOtherRow(formId){
		var flag=true;
		var rangeFlag=true;
		var headerLnght = $j("#added thead th").length;
		var thawRow=headerLnght-3;
		var thawOtherRow=1;
		for(thawRow=0;thawRow<headerLnght;thawRow++){
			var n=0;
			$j("#added tr:gt(1)").each(function(){
				if(n>=$j("#noOfLabGrpTypTests").val()){
					var fieldId = 'savePreTestList'+n+'testresult';
					if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
					var tempVarbl=document.getElementById(fieldId).value;
					if(tempVarbl != null && tempVarbl !=''){
						$j("#"+fieldId+"_error").hide();
						var fieldfktestspecimenId = 'savePreTestList'+n+'fktestspecimen';
						var tempfktestspecimenVarbl=document.getElementById(fieldfktestspecimenId).value;
						if(tempfktestspecimenVarbl != null && tempfktestspecimenVarbl >0){
							$j("#"+fieldfktestspecimenId+"_error").hide();
						}
						else{
							$j("#"+fieldfktestspecimenId+"_error").show();
							$j("#"+fieldfktestspecimenId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
							flag=false;
						}
					}
					else{
						$j("#"+fieldId+"_error").show();
						$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
						flag=false;
					}
				}
					fieldId = 'savePostTestList'+n+'testresult';
					if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
					var tempVarbl=document.getElementById(fieldId).value;
					if(tempVarbl != null && tempVarbl !=''){
						$j("#"+fieldId+"_error").hide();
						var fieldfktestspecimenId = 'savePostTestList'+n+'fktestspecimen';
						var tempfktestspecimenVarbl=document.getElementById(fieldfktestspecimenId).value;
						if(tempfktestspecimenVarbl != null && tempfktestspecimenVarbl >0){
							$j("#"+fieldfktestspecimenId+"_error").hide();
						}
						else{
							$j("#"+fieldfktestspecimenId+"_error").show();
							$j("#"+fieldfktestspecimenId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
							flag=false;
						}
					}
					else{
						$j("#"+fieldId+"_error").show();
						$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
						flag=false;
					}
				}
					fieldId = 'saveOtherPostTestList'+thawOtherRow+n+'testresult';
					if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
					var tempVarbl=document.getElementById(fieldId).value;
					if(tempVarbl != null && tempVarbl !=''){
						$j("#"+fieldId+"_error").hide();
						var fieldfktestspecimenId = 'saveOtherPostTestList'+thawOtherRow+n+'fktestspecimen';
						var tempfktestspecimenVarbl=document.getElementById(fieldfktestspecimenId).value;
						if(tempfktestspecimenVarbl != null && tempfktestspecimenVarbl >0){
							$j("#"+fieldfktestspecimenId+"_error").hide();
						}
						else{
							$j("#"+fieldfktestspecimenId+"_error").show();
							$j("#"+fieldfktestspecimenId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
							flag=false;
						}
					}
					else{
						$j("#"+fieldId+"_error").show();
						$j("#"+fieldId+"_error").text("<s:text name="garuda.cbu.cordentry.inputId"/>");
						flag=false;
					}
				}
				n++;	
				}
				else{
				var fieldId = 'savePreTestList'+n+'testresult';
				if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
					var tempVarbl=document.getElementById(fieldId).value;
					if(tempVarbl != null){
						var fieldName='savePreTestList['+n+'].fktestid';
						var fkTestId=$j("input[name='"+fieldName+"']").val();
						checkRangeMthd(fieldId,fkTestId,tempVarbl,'pre','fromSearching');
					}
				}
					fieldId = 'savePostTestList'+n+'testresult';
					if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
						var tempVarbl=document.getElementById(fieldId).value;
						if(tempVarbl != null){
							var fieldName='savePostTestList['+n+'].fktestid';
							var fkTestId=$j("input[name='"+fieldName+"']").val();
							checkRangeMthd(fieldId,fkTestId,tempVarbl,'post','fromSearching');
						}
					}
					fieldId = 'saveOtherPostTestList'+thawOtherRow+n+'testresult';
				if(document.getElementById(fieldId)!=null && document.getElementById(fieldId)!='undefined'){
				var tempVarbl=document.getElementById(fieldId).value;
				if(tempVarbl != null && tempVarbl !=''){
					var fieldName='saveOtherPostTestList'+thawOtherRow+'['+n+'].fktestid';
					var fkTestId=$j("input[name='"+fieldName+"']").val();
					checkRangeMthd(fieldId,fkTestId,tempVarbl,'otherThaw');
					//var fieldName='saveOtherPostTestList'+thawOtherRow+'['+n+']'+'.fktestid';
					//var fkTestId=document.getElementsByName(fieldName);
					//rangeFlag = checkRangeMthd(fieldId,fkTestId,tempVarbl);
					var fieldfktestreasonId = 'saveOtherPostTestList'+thawOtherRow+n+'fktestreason';
					var tempfktestreasonVarbl=document.getElementById(fieldfktestreasonId).value;
					if(tempfktestreasonVarbl != null && tempfktestreasonVarbl >0){
						var tempvalueTestReasonOtherPkVal=document.getElementById('valueTestReasonOtherPkVal').value;
						if(tempfktestreasonVarbl==tempvalueTestReasonOtherPkVal){
							var fieldreasonTestDescId = 'saveOtherPostTestList'+thawOtherRow+n+'reasonTestDesc';
							var tempreasonTestDescVarbl=document.getElementById(fieldreasonTestDescId).value;
							if(tempreasonTestDescVarbl != null && tempreasonTestDescVarbl != ''){
								$j("#"+fieldreasonTestDescId+"_error").hide();
							}
							else{
								$j("#"+fieldreasonTestDescId+"_error").show();
								flag=false;
							}
						}
						$j("#"+fieldfktestreasonId+"_error").hide();
					}
					else{
						$j("#"+fieldfktestreasonId+"_error").show();
						flag=false;
					}
					var fieldfktestspecimenId = 'saveOtherPostTestList'+thawOtherRow+n+'fktestspecimen';
					var tempfktestspecimenVarbl=document.getElementById(fieldfktestspecimenId).value;
					if(tempfktestspecimenVarbl != null && tempfktestspecimenVarbl >0){
						$j("#"+fieldfktestspecimenId+"_error").hide();
					}
					else{
						$j("#"+fieldfktestspecimenId+"_error").show();
						flag=false;
					}
					var fieldfktestmethodId = 'saveOtherPostTestList'+thawOtherRow+n+'fktestmethod';
					if(document.getElementById(fieldfktestmethodId)!=null && document.getElementById(fieldfktestmethodId)!='undefined'){
					var tempfktestmethodVarbl=document.getElementById(fieldfktestmethodId).value;
					if(tempfktestmethodVarbl != null && tempfktestmethodVarbl >0){
						var tempValueCfuOtherPkVal=document.getElementById('valueCfuOtherPkVal').value;
						var tempValueViablityOtherPkVal=document.getElementById('valueViablityOtherPkVal').value;
						if(tempfktestmethodVarbl==tempValueCfuOtherPkVal || tempfktestmethodVarbl==tempValueViablityOtherPkVal){
							var fieldtestMthdDescId = 'saveOtherPostTestList'+thawOtherRow+n+'testMthdDesc';
							var temptestMthdDescVarbl=document.getElementById(fieldtestMthdDescId).value;
							if(temptestMthdDescVarbl != null && temptestMthdDescVarbl != ''){
								$j("#"+fieldtestMthdDescId+"_error").hide();
							}
							else{
								$j("#"+fieldtestMthdDescId+"_error").show();
								flag=false;
							}
						}
						$j("#"+fieldfktestmethodId+"_error").hide();
					}
					else{
						$j("#"+fieldfktestmethodId+"_error").show();
						flag=false;
					}
					}
				}
				}
				n++;
				}
			});
			thawOtherRow++;
		}
		return flag;
	}
 function showDescField(divId,compValue,fieldval,fieldId){
		if(compValue==fieldval){
			$j("#"+divId).show();
	}else{
		$j("#"+divId).hide();
	}
		if(fieldval==-1 || fieldval==''){
			$j("#"+fieldId+"_error").show();
			$j("#"+fieldId+"_error").text("<s:text name="garuda.common.validation.value"></s:text>");
		}
		else{
			$j("#"+fieldId+"_error").hide();
		}
	}
	function chekDepandtFldPrs(fieldId,fieldval){
		if(fieldval==null || fieldval==''){
			$j("#"+fieldId+"_error").show();
		}
		else{
			$j("#"+fieldId+"_error").hide();
		}
	}
function setDefBactFungDate(procDateStrId,bactDateStrId,fungDateStrId,emptyChngFlag){
	if($j("#"+procDateStrId).val()!=null){
		if(document.getElementById(bactDateStrId)!=null && document.getElementById(bactDateStrId)!='undefined' && $j("#"+bactDateStrId).is(':visible')==true){
			if($j("#"+bactDateStrId).val()=='' && emptyChngFlag=="procChng"){
			$j("#"+bactDateStrId).val($j("#"+procDateStrId).val());
			}else if(emptyChngFlag=="bactChng"){
				$j("#"+bactDateStrId).val($j("#"+procDateStrId).val());
			}
		}
		if(document.getElementById(fungDateStrId)!=null && document.getElementById(fungDateStrId)!='undefined' && $j("#"+fungDateStrId).is(':visible')==true){
			if($j("#"+fungDateStrId).val()=='' && emptyChngFlag=="procChng"){
			$j("#"+fungDateStrId).val($j("#"+procDateStrId).val());
			}else if(emptyChngFlag=="fungChng"){
				$j("#"+fungDateStrId).val($j("#"+procDateStrId).val());
			}
		}
	}
	if($j("#"+bactDateStrId).val()!=""){
		$j(".placeHolderOfid_"+bactDateStrId).hide();
	}else{
		$j(".placeHolderOfid_"+bactDateStrId).show();
	}
	if($j("#"+fungDateStrId).val()!=""){
		$j(".placeHolderOfid_"+fungDateStrId).hide();
	}else{
		$j(".placeHolderOfid_"+fungDateStrId).show();
	}
}
function nonSystemCordStatus()
{
	if($j('#non_system_cord').val()=="true"){
		return false;
	}
	else{
		return true;
	}
}
var AutoDeferElementInitVal={};
$j(function(){	
	$j('.autodeferclass').each(function(){
		          var temp=$j(this).attr('name');
		          AutoDeferElementInitVal[temp]=$j(this).val();			
		  });
	$j("#labSummeryDivR input").bind({keydown:function(e) {
		 var key = e.charCode || e.keyCode;
		 if(key==9){
			 calcCbuNbrcFlag=false;
		 }
		 else{
			 calcCbuNbrcFlag=true;
		 }
	}});
});
function validateIDMBldCollDate(coldate){
	var IdmbloodCollectionDate = $j('#idmbloodCollectionDate').val();	
	validateIDMBloodCollDate(coldate,IdmbloodCollectionDate,'IdmbldWrngMsg');
}
</script>
<style>
.posrel{
	position: relative;
	z-index: 100;
	background: #ffffff;
	border: 1px solid #0073AE;
	padding:10px 0 0 10px;
	-moz-border-radius:5px;
	left:10px;
}
.elv{
	height: 10px;
	width: 96%;
	position: absolute;
}
</style>
<div id="maincontainerdiv">
	<table width="98%" class="posrel">
		<tr>
			<td ><div id="totalbar"></div></td>
		    <td style="cursor: pointer;" ><div id="idinfobar" onclick="focusDiv('idinfoparent')"></div></td>
		    <td style="cursor: pointer;" ><div id="cbuinfobar" onclick="focusDiv('cbuinfoparent')"></div></td>
		    <td style="cursor: pointer;" ><div id="licensurebar" onclick="focusDiv('licensureparent')"></div></td>
		    <td style="cursor: pointer;" ><div id="processinginfobar" onclick="focusDiv('procinfoparent')"></div></td>
		    <td style="cursor: pointer;" ><div id="labsummarybar" onclick="focusDiv('labsummaryparent')"></div></td> 
		    <td style="cursor: pointer;display: none;" class="formsClass"><div id="MRQbar" onclick="focusDiv('mrqinfoparent')"></div></td>
		    <td style="cursor: pointer;display: none;" class="formsClass"><div id="FMHQbar" onclick="focusDiv('fmhqinfoparent')"></div></td>
		    <td style="cursor: pointer;" ><div id="IDMbar" onclick="focusDiv('idminfoparent')"></div></td>
		    <td style="cursor: pointer;" ><div id="hlabar" onclick="focusDiv('hlainfoparent')"></div></td>
		    <td style="cursor: pointer;" ><div id="eligibilitybar" onclick="focusDiv('eliginfoparent')"></div></td>  
		</tr>	
		<tr style="color: black;">
		    <td><s:text name="garuda.cbu.cordentry.total"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		    <td><s:text name="garuda.advancelookup.label.id"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		    <td><s:text name="garuda.cbu.cordentry.cbuInfo"/>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		    <td><s:text name="garuda.cordentry.label.licensure"/></td>
		    <td><s:text name="garuda.cordentry.label.procinfo"/></td>	    
		    <td><s:text name="garuda.cordentry.label.labsummary"/>&nbsp;&nbsp;</td>
		    <td style="display: none;" class="formsClass"><s:text name="garuda.cordentry.label.mrq"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	    
		    <td style="display: none;" class="formsClass"><s:text name="garuda.cordentry.label.fmhq"/>&nbsp;&nbsp;&nbsp;&nbsp;</td>	    
		    <td><s:text name="garuda.cordentry.label.idm"/>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	        <td><s:text name="garuda.cordentry.label.hla"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		    <td><s:text name="garuda.cordentry.label.eligibility"/>&nbsp;&nbsp;&nbsp;&nbsp;</td><!--
		    <td><s:text name="garuda.cordentry.label.clinicalnotes"/>&nbsp;&nbsp;</td>
		--></tr>	
	</table>	
	<div class="col_100 maincontainer" style="margin-top:10px"><div class="col_100">
	<s:form id="cordentryform1" action="saveCordEntry">
	<s:hidden name="cdrCbuPojo.fkCordCbuStatus" id="CrdStatus"></s:hidden>
	<input type="hidden" id="bacterialNotDone"  value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@NOTDONE)"/>"/> 
	<input type="hidden" id="bacterialPositive" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@POSITIVE)"/>"/> 
	<input type="hidden" id="bacterialNegative" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@NEGATIVE)"/>"/>
	
	<s:hidden name="dropValue" id="dropValue"></s:hidden>
	<input type="hidden" id="bacterialPositiveFung" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@POSITIVE)"/>"/> 
	<input type="hidden" id="bacterialNegativeFung" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@NEGATIVE)"/>"/>
	<s:hidden name="dropValueFung" id="dropValueFung"></s:hidden> 
	<s:hidden name="positionofmain" id="positionofmain"/>
	<s:hidden name="positionofprog" id="positionofprog"/>
	<s:hidden name="cdrCbuPojo.cordID" id="cordID"></s:hidden>
	<s:hidden name="cdrCbuPojo.site.siteId" ></s:hidden>
	<s:hidden name="cdrCbuPojo.cordIsitProductCode"></s:hidden>
	<s:hidden value ="%{cdrCbuPojo.fkCbbProcedure}" id="currProc"></s:hidden>
	<s:hidden value ="%{cbuOnBag}"></s:hidden>
	<s:hidden name="timestamp"></s:hidden>
	<s:hidden name="cdrCbuPojo.cordEntryProgress" id="cordProgress"></s:hidden>
	<s:hidden name="cdrCbuPojo.cordSearchable" id="cordSearchable"></s:hidden>		
	<s:hidden name="cdrCbuPojo.specimen.pkSpecimen" /> 
	<input type="hidden" id="sampAuto" value="<s:property value="#request.sampAuto"/>">
	<input type="hidden" id="eligiblePkId" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBLE)"/>"/>
	<input type="hidden" name="cdrCbuPojo.inEligiblePkid" id="inEligiblePk" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON)"/>"/>
	<s:hidden name="NAEligibleFlag" id="NAEligibleFlag"/>
	<s:hidden name="cdrCbuPojo.specimen.specCollDate" id="specCollDate"></s:hidden>
	<input type="hidden" name="cdrCbuPojo.incompleteReasonId" id="incompleteReasonId" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@INCOMPLETE_REASON)"/>"/>
	<input type="hidden" name="cdrCbuPojo.notCollectedToPriorReasonId" id="notCollectedToPriorReasonId" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@NOT_COLLECTED_TO_PRIOR_REASON)"/>"/>
	
	<input type="hidden"  id="unLicensedPk" name="unLicensedPk" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON)"/>"/>
	<input type="hidden" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN,@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_BETA)"/>" id="hemoBeatThal"/>
	<input type="hidden" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN,@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_SICKLE)"/>" id="hemoSickBeta"/>
	<input type="hidden" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN,@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_HEMOSICK)"/>" id="hemoSickCell"/>
	<input type="hidden" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN,@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_ALPHA)"/>" id="hemoAlphaSev"/>
	<input type="hidden" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN,@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_MULT)"/>" id="multTrait"/>
	<input type="hidden" value="<s:property value="#request.shipMentFlag"/>" id="shipmentFlag"/>
	<input type="hidden" value="<s:property value="fkCordCbuEligible"/>" id="eligibilityVal"/>
	<s:hidden value="%{cdrCbuPojo.registryId}" name="defaultRegId" id="cbucordregid" />
	<s:hidden value="%{cdrCbuPojo.registryId}" id="regid" />
	<s:hidden value="%{cdrCbuPojo.registryMaternalId}" name="defaultMatId" id="regcordmatid"/>
    <s:hidden value="%{cdrCbuPojo.localCbuId}" name="defaultLocCbuId" id="localcordcbuid"/>
    <s:hidden value="%{cdrCbuPojo.localMaternalId}" name="defaultMatLocId" id="localcordmatid"/>
    <s:hidden value="%{cdrCbuPojo.cordIsbiDinCode}" name="defaultISBTCode" id="isbidincordcode" />
    <s:hidden  name="cdrCbuPojo.guid" />
    <s:hidden  name="cdrCbuPojo.matGuid" />
    <input type="hidden" id="uRegId" name="cdrCbuPojo.uniqueRegId" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_IDS,@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_REGIS_ID)"/>"/>
    <input type="hidden" id="uLocalId" name="cdrCbuPojo.uniqueLocaId" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_IDS,@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_LOCAL_ID)"/>"/>
    <input type="hidden" id="uIsbtId" name="cdrCbuPojo.uniqueIsbtId" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_IDS,@com.velos.ordercomponent.util.VelosGarudaConstants@ISBT_DIN_ID)"/>"/>
    <input type="hidden" id="uAddId" name="cdrCbuPojo.uniqueAddId" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_IDS,@com.velos.ordercomponent.util.VelosGarudaConstants@ADDITIONAL_ID)"/>"/>
    <s:hidden id="regmatidstatus" name="registrymaternalidstaus" />
	<input type="hidden" name ="assess_ncd" id="assess_ncd" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_ASSESS, @com.velos.ordercomponent.util.VelosGarudaConstants@ASSESS_NCD)"/>"/>
	<s:hidden id='iscdrMember' name="iscdrMember" value="true"></s:hidden> 
	<s:hidden id="non_system_cord" name="isCDRUser" value="%{cdrCbuPojo.site.isCDRUser()}"/>
	<s:hidden name="todayDate"/>
	<s:hidden id="cordEditEntryToken" name="cordEditEntryToken" value="%{#session.cordEditEntryToken}"/>
	<div class="column">
    	<div style="float: right;">
    		<s:if test="hasViewPermission(#request.viewCbuDetailRep)==true"><button type="button" name="reportBt" onclick="f_callDetailReport('<s:property value="cdrCbuPojo.cordID"/>');"><s:text name="garuda.report.label.detailreport" /></button></s:if>
    	</div>
    	<div class="portlet" id="idinfoparent">
    		<div id="idnumberhead" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
    			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
    				<span class="ui-icon ui-icon-minusthick"></span>
    				<s:text name="garuda.advancelookup.label.id" />
    			</div>
    		 <div id="idinfo" class="portlet-content">
    		 	<table id="additionalidtable">
    		 		<tr>
    		 			<td width="10%" style="padding-left: 5px;" onmouseover="return overlib('<s:text name="garuda.cbuentry.label.cbuRegToolTip"></s:text>');" onmouseout="return nd();">
    		 				<s:text name="garuda.cbuentry.label.registrycbuid" />:<span style="color: red;">*</span>
    		 			</td>
    		 			<td width="23%" style="padding-left: 5px;">
    		 				<s:if test="hasEditPermission(#request.updateCbuRegid)==false ">
    		 					<s:textfield maxlength="11" cssClass="idclass" name="cdrCbuPojo.registryId" id="cburegid" readonly="true" onkeydown="cancelBack();" ></s:textfield>
    		 				</s:if>
    		 				<s:else>
    		 					<s:if test="cdrCbuPojo.site.isCDRUser()==true">
    		 						<s:textfield maxlength="11" cssClass="idclass" name="cdrCbuPojo.registryId" id="cburegid" readonly="false" onchange="htmlUniqueIdOnBag()"></s:textfield>
    		 					</s:if>
								<s:else>
									<s:textfield maxlength="30" cssClass="idclass" name="cdrCbuPojo.registryId" id="cburegid" readonly="false" onchange="htmlUniqueIdOnBag()"></s:textfield>
								</s:else>
							</s:else>
						</td>													      
						<td width="10%" style="padding-left: 5px;" onmouseover="return overlib('<s:text name="garuda.cbuentry.label.matRegToolTip"></s:text>');" onmouseout="return nd();">
							<s:text name="garuda.cbuentry.label.maternalregistryid" />:<s:if test="cdrCbuPojo.site.isCDRUser()==true"><span style="color: red;">*</span></s:if>
						</td>
						<td width="23%" style="padding-left: 5px;">
							<s:if test="hasEditPermission(#request.updateCbuMaternalid)==false">
								<s:textfield cssClass="idclass" maxlength="11" id="regMaternalId" name="cdrCbuPojo.registryMaternalId" readonly="true" onkeydown="cancelBack();" ></s:textfield>
							</s:if>
							<s:else>
								<s:if test="cdrCbuPojo.site.isCDRUser()==true">
									<s:textfield cssClass="idclass" maxlength="11" id="regMaternalId" name="cdrCbuPojo.registryMaternalId" readonly="false"></s:textfield>
								</s:if>
								<s:else>
									<s:textfield cssClass="idclass" maxlength="30" id="regMaternalId" name="cdrCbuPojo.registryMaternalId" readonly="false"></s:textfield>
								</s:else>
							</s:else>
						</td>
						<td width="10%" style="padding-left: 5px;" onmouseover="return overlib('<s:text name="garuda.cbuentry.label.isbtDinToolTip"></s:text>');" onmouseout="return nd();">
							<s:text name="garuda.cordentry.label.isbtid" />:
						</td>
						<td width="24%" style="padding-left: 5px;">
							<s:textfield id="isbidin" name="cdrCbuPojo.cordIsbiDinCode" readonly="false" onchange="htmlUniqueIdOnBag()"></s:textfield>
						</td>
					</tr>
					<tr>
						<td width="10%" style="padding-left: 5px;" onmouseover="return overlib('<s:text name="garuda.cbuentry.label.cbuLocalToolTip"></s:text>');" onmouseout="return nd();">
							<s:text name="garuda.cbuentry.label.localcbuid" />:<s:if test="cdrCbuPojo.site.isCDRUser()==true"><span style="color: red;">*</span></s:if>
						</td>
						<td width="23%" style="padding-left: 5px;"><s:textfield cssClass="idclass"  maxlength="30" id="loccbuid" name="cdrCbuPojo.localCbuId" readonly="false" onchange="htmlUniqueIdOnBag()"></s:textfield></td>
						<td width="10%" style="padding-left: 5px;" onmouseover="return overlib('<s:text name="garuda.cbuentry.label.matLocalToolTip"></s:text>');" onmouseout="return nd();">
							<s:text name="garuda.cbuentry.label.maternallocalid" />:
						</td>
						<td width="23%" style="padding-left: 5px;"><s:textfield maxlength="30" id="matlocalid" name="cdrCbuPojo.localMaternalId"  readonly="false"></s:textfield></td>
						<td width="10%" style="padding-left: 5px;" onmouseover="return overlib('<s:text name="garuda.cbuentry.label.uniqueIdToolTip"></s:text>');" onmouseout="return nd();">
							<s:text name="garuda.cbuentry.label.idbag"></s:text>:<span style="color: red;">*</span>
						</td>
						<td width="23%" style="padding-left: 5px;">	
							<input type="text" name="cdrCbuPojo.numberOnCbuBag" value="<s:property value="cdrCbuPojo.numberOnCbuBag"/>" id="idonbagvalresult" class="idclass" style="display:none">
							<s:hidden value="%{cdrCbuPojo.numberOnCbuBag}" id="numberOnCbuBag" />												              	             
						    <div id="idonbag">
						    	<select name="cbuOnBag"  id="cbuOnBag" >
						    		<option value="-1">Select</option>
						    	</select>
						    </div>
						</td>	
					</tr>
					<s:if test="#request.additionalIds!=null && #request.additionalIds.size>0">
					<s:iterator value="#request.additionalIds" status="row">
					<input type="hidden" name="additionalIds[<s:property value="%{#row.index}"/>].pkAdditionalIds" value="<s:property value="pkAdditionalIds"/>" />
					<tr id="additionalid_row<s:property value="%{#row.index}"/>">
						<td width="10%"><s:text name="garuda.cordentry.label.additionalIdDesc"></s:text>:<span style="color: red;">*</span></td>
						<td width="23%"><input type="text" class="required additionalids" maxlength="30" onchange="additonalIdscall(<s:property value="%{#row.index}"/>);"  name="additionalIds[<s:property value="%{#row.index}"/>].additionalIdDesc" value="<s:property value="additionalIdDesc"/>" /></td>
						<td width="10%"><s:text name="garuda.cordentry.label.additionalTypeId"></s:text>:<span style="color: red;">*</span></td>
						<td width="23%">
							<select name="additionalIds[<s:property value="%{#row.index}"/>].additionalIdValueType" class="required additionalids" onchange="additonalIdscall(<s:property value="%{#row.index}"/>);" id="addID<s:property value="%{#row.index}"/>">
								<option value="">Select</option>
								<option <s:if test="additionalIdValueType=='cbu'">selected</s:if> value="cbu">CBU</option>
								<option <s:if test="additionalIdValueType=='maternal'">selected</s:if> value="maternal">Maternal</option>
							</select>
						</td>
						<td width="10%"><s:text name="garuda.cordentry.label.additionalId"></s:text>:<span style="color: red;">*</span></td>
						<td width="23%"><input type="text" class="required additionalids"  maxlength="30"onchange="additonalIdscall(<s:property value="%{#row.index}"/>);" id="additionalId<s:property value="%{#row.index}"/>" name="additionalIds[<s:property value="%{#row.index}"/>].additionalId" value="<s:property value="additionalId" />"  /></td>
						<td><img style="cursor: pointer;vertical-align:middle;"  src="images/cross.png" border="0"  onclick="javascript:deleteRow(<s:property value="%{#row.index}"/>);" /></td>
					</tr>	
					</s:iterator>	
					</s:if>
				</table>
			    <table>
					<tr>
						<td colspan="5" width="30%">
							<span style="cursor: pointer;" onclick="addAdditionalId()" id="addbutton">
								<img style="vertical-align: middle;" src="images/addcomment.png" border="0" id="plus" />
								<s:text name="garuda.cordentry.label.addAdditionalId"></s:text>
							</span>
						</td>
					</tr>
				</table>
			</div>
    		</div>
    	</div>
    </div>
    <div class="column">
		<div class="portlet" id="licensureparent" >
	    <div>
			<s:iterator value="licenseReasons" var="val" status="row">
				<s:hidden value="%{licenseReasons[#row.index]}" id="liCReasons[%{#row.index}]"></s:hidden>
			</s:iterator>
		</div>
	    <div style="clear: both;"></div>
		<div id="licensure" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
				<span class="ui-icon ui-icon-minusthick"></span>
				<s:text name="garuda.cordentry.label.licensure" />
			</div>
			<div class="portlet-content">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td><s:text name="garuda.cdrcbuview.label.licensure_status" />:<span style="color: red;">*</span></td>
						<td>
							<s:select id="licenseid"  headerValue="Select Licensure" headerKey="-1" cssClass="licclass" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS]" name="cbuLicStatus" listKey="pkCodeId" listValue="description" value="cbuLicStatus"  onchange="unliceseReason(this.value,'licenseReason');showforms();" ></s:select>
						</td>							      
					</tr>
					<tr>			
						<td colspan="2">
							<%-- <div id="licenseReason"  style="display: none;">
								<table width="100%">
									<tr>
										<td width="30%">
											<s:text name="garuda.cdrcbuview.label.unlicense_reason" />:<span style="color: red;">*</span>
										</td>
										<td width="70%">
											<s:select name="licenseReasons" id="fkCordCbuUnlicenseReason"
													list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@UNLICENSE_REASON]"
													listKey="pkCodeId" listValue="description" multiple="true" value="licenseReasons" cssStyle="height:100px;"/>
										</td>
									</tr>
								</table>
							</div>--%>
						</td>
					</tr>
				</table>	
				</div>
			</div>
		</div>
	</div>
	<div class="column">
		<div class="portlet" id="cbuinfoparent" >
			<div id="cbuinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
				<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<s:text name="garuda.cordentry.label.cbuinfo" />
				</div>
				<div class="portlet-content">
					<div id="cbbinfohead">									
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="6">
									<fieldset>
										<div id="cbbdetailcontent" onclick="toggleDiv('cbbdetail')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
											<span class="ui-icon ui-icon-triangle-1-s"></span><s:text name="garuda.cbbdefaults.lable.cbbinformation" />
										</div>
					                    <div id="cbbdetail" class="portlet-content">
											<table>
												<tr>
													<td colspan="6" align="left">
														<s:hidden name="cdrCbuPojo.fkCbbId"   id="fkCbbId" />
														<s:set name="addId" value="site.sitePerAdd" scope="request"/>
														<div id="cbbinforefresh">
															<jsp:include page="../cb_setsiteinfo.jsp">
																<jsp:param name="addId" value="<%=request.getAttribute(\"addId\")%>" />
															</jsp:include>
														</div>
													</td>
												</tr>
											</table>
										</div>
									</fieldset>
								</td>
							</tr>
						</table>
					</div>
					<table>
						<tr>
							<td colspan="6">
								<fieldset>
									<div id="cbbdemocontent" onclick="toggleDiv('cbbDemo')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
										<span class="ui-icon ui-icon-triangle-1-s"></span><s:text name="garuda.cordentry.label.cbudemographics" />
									</div>
									<div id="cbbDemo">
										<table>
											<tr>
												<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cbuentry.label.birthdate" />:<span style="color: red;">*</span></td>
												<td width="25%" style="padding-left: 5px;">
													<s:date name="cdrCbuPojo.birthDate" id="datepicker3" format="MMM dd, yyyy" />
													<s:textfield readonly="true" name="birthDateStr" id="datepicker3" value="%{datepicker3}" cssClass="birthDate cbuclass"></s:textfield>
													<span style="display:none" id="birthdatebeforeColDate" class="error"><br/><s:text name="garuda.cbu.cordentry.birthdate"></s:text> </span>
												</td>
												<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cbuentry.label.collectiondate" />:<span style="color: red;">*</span></td>
												<td width="25%" style="padding-left: 5px;">
													<s:date name="cdrCbuPojo.specimen.specCollDate" id="datepicker2" format="MMM dd, yyyy" />									      
													<s:textfield readonly="true" onkeydown="cancelBack();"  name="cbuCollectionDateStr" id="datepicker2" onchange="setMinDateWidRespCollDat();validateIDMBldCollDate(this.value);" value="%{datepicker2}" cssClass="collectDate cbuclass" ></s:textfield>
													<span style="display:none" id="dynaErrorProcedureDt" class="error"><br/><s:text name="garuda.cbu.cordentry.prcdrDateComp"></s:text> </span>
													<span style="display:none" id="birthdateErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.birthdatemust"></s:text> </span>
													<span style="display:none" id="birthdateRangeErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.collectiondatehrs"></s:text> </span>
													<span style="display:none" id="IdmbldWrngMsg" class='error'><br/><s:text name="garuda.label.dynamicform.bldWrngMsg"></s:text></span>
												</td>
												<td width="30%" colspan="2">&nbsp;</td>
											</tr>
											<tr>
												<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cbuentry.label.babybirthtime" />:<span style="color: red;">*</span></td>
												<td width="25%" style="padding-left: 5px;"><s:textfield name="cdrCbuPojo.babyBirthTime" cssClass="timeValid cbuclass" maxlength="5" id="babybirthtime"></s:textfield>(HH:MM 24 HR)</td>
												<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cbuentry.label.collectiontime" />:<span style="color: red;">*</span></td>
												<td width="25%" style="padding-left: 5px;">
													<s:textfield name="cdrCbuPojo.cbuCollectionTime" id="cbuCollectionTime" cssClass="timeValid cbuclass" maxlength="5" onchange="validateHours('datepicker3','babybirthtime','datepicker2','cbuCollectionTime','collTimeRangeErrorMsg');"></s:textfield>(HH:MM 24 HR)
													<span style="display:none" id="collTimeRangeErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.collectiondateTimehrs"></s:text> </span>
												</td>
												<td width="30%" colspan="2">&nbsp;</td>
										  	</tr>
											<tr>
												<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cbuentry.label.gender" />:<span style="color: red;">*</span></td>
												<td width="20%" style="padding-left: 5px;"><s:select id="babygenid" name="cdrCbuPojo.babyGenderId" cssClass="cbuclass" list="populatedData.genderIdList" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select></td>
												<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cbuentry.label.race" />:<span style="color: red;">*</span></td>
												<td width="25%" style="padding-left: 5px;"><s:select id="raceId" cssStyle="height:80px;" multiple="true"  cssClass="cbuclass" name="race" list="populatedData.raceList" listKey="pkCodeId" listValue="description" ></s:select></td>	
												<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cordentry.label.ethnicity" />:<span style="color: red;">*</span></td>
												<td width="25%" style="padding-left: 5px;"><s:select id="ethid" name="cdrCbuPojo.ethnicity" cssStyle = "width:auto" cssClass="cbuclass" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ETHNICITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select"></s:select></td>
											</tr>
											<tr>
												<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cordentry.label.multiplebirth" />:<span style="color: red;">*</span></td>
												<td width="20%" style="padding-left: 5px;"><s:select id="multiplebirthid" list="#{1:'Yes',0:'No'}" name="cdrCbuPojo.fkMultipleBirth" headerKey="-1" headerValue="Select" cssClass="cbuclass autodeferclass" onchange=" autoDeferForMultiplePregnancy(this.value,this.id,'cbuclass',this);" ></s:select></td>
												<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cordentry.label.cbucolltype" />:<span style="color: red;">*</span></td>
												<td width="25%" style="padding-left: 5px;"><s:select id="cbucolltypeid" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_COLLECTION_TYPE]" name="cdrCbuPojo.fkCBUCollectionType" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" cssClass="cbuclass" ></s:select></td>
												<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cordentry.label.cbudelivtype" />:<span style="color: red;">*</span></td>
												<td width="25%" style="padding-left: 5px;"><s:select id="cbudeltypeid" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_DELIV_TYPE]" name="cdrCbuPojo.fkCBUDeliveryType" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" cssClass="cbuclass" ></s:select></td>
											</tr>
											<tr>
												<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cordentry.label.nmdpbroadrace" />:</td>
												<td width="20%" style="padding-left: 5px;">
													<s:if test="cdrCbuPojo.nmdpRaceId!=null">
														<s:select tabindex="-1" disabled="true" name="cdrCbuPojo.nmdpRaceId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NMDP_BROAD_RACE]" listKey="pkCodeId" listValue="description" ></s:select>
													</s:if>
													<s:else>
														<s:textfield tabindex="-1" name="cdrCbuPojo.nmdpRaceId" readonly="true" onkeydown="cancelBack();" ></s:textfield>
													</s:else>
												</td>
										      	<td width="70%" colspan="4"></td>
										    </tr>
										</table>
									</div>
								</fieldset>
							 </td>
						</tr>
					</table>		
				</div>
			</div>
		</div>
	</div>
	<div class="column">
	      <div class="portlet" id="procinfoparent" >
					<div id="procinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<!--<span class="ui-icon ui-icon-close" ></span>-->
					<s:text name="garuda.unitreport.label.processinginfo" />
					</div>
					<div class="portlet-content" id="procedureAsyncData">									
					</div>					
					</div>					
					</div>
		 </div>


	    <div class="column">
					<div class="portlet" id="labsummaryparent">
						<div id="labsummary"
							class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
							<div
								class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
								style="text-align: center;">
								<span class="ui-icon ui-icon-minusthick"></span>
								<!--<span class="ui-icon ui-icon-close" ></span>-->
								<s:text name="garuda.cordentry.label.labsummary" />
							</div>
							<div class="portlet-content">
								<table border="0" align="left" cellpadding="0" cellspacing="0"
									class="displaycdr">
									<tr>
										<td>
											<div id="labDiv" style="overflow: scroll">
												<div class="portlet" id="procandcountparent" >
										    <table>
										    	<tr>
													<td>
													<div id="labSummaryModal">
														<div id="procandcount" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
														<%-- <div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
															<span class="ui-icon ui-icon-minusthick"></span>
															<span class="ui-icon ui-icon-close" ></span>
																<s:text name="garuda.cordentry.label.labsummaryProcess" />
														</div> --%>
														<div class="portlet-content">
															 <table width="100%" cellpadding="0" cellspacing="0" border="0">	
																<tr>
							                 						<td width="20%" align="left" onmouseover="return overlib('<s:text
																	name="garuda.cordentry.label.procStartDateToolTip"></s:text>');" 
																	onmouseout="return nd();"><s:text name=
																	"garuda.cordentry.label.prostartdate"></s:text>:<span style="color: red;">*</span></td>
											 						<td width="20%">
											 							<s:date name="cdrCbuPojo.processingDate" id="datepicker4" format="MMM dd, yyyy" />
													 					<s:textfield readonly="true" onkeydown="cancelBack();"  name="prcsngStartDateStr" id="datepicker4" value="%{datepicker4}" cssClass="procStartDate labclass" onchange="addTestDate(this.value),addTestDateScnd(this.value);setDefBactFungDate('datepicker4','datepicker6','datepicker7','procChng');"></s:textfield>
																		<span style="display:none" id="dynaErrorProcessinDat" class="error"><br/><s:text name="garuda.cbu.cordentry.procDateComp"></s:text> </span>
																		<span style="display:none" id="precessDateMust" class="error"><br/><s:text name="garuda.cbu.cordentry.procstartdate"/></span>
																		<span style="display:none" id="precessDatebffrz" class="error prcessError"><br/><s:text name="garuda.cbu.cordentry.procbeforefreezdate"/></span>
																		<span style="display:none" id="processdateErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.collectionmust"></s:text></span>
																		<span style="display:none" id="processdateErrorMsg1" class="error"><br/><s:text name="garuda.cbu.cordentry.procdateaftercoldate"></s:text></span>
																		<span style="display:none" id="dynaErrorPrcngDt" class="error"><br/><s:text name="garuda.cbu.cordentry.prcssngdtwrtproc"/></span>
																	</td>
																	<td width="20%"><s:text name="garuda.cordentry.label.proStartTime"></s:text>:<span style="color: red;">*</span></td>	
																    <td width="20%"><s:textfield name="cdrCbuPojo.processingTime" id="cordProcessingTime" cssClass="timeValid cbuLabSummMandatory" maxlength="5" onchange="checkTimeFormatForCord('cordProcessingTime');validateDateData('datepicker4','cordProcessingTime','datepicker5','cordFreezeTime','precessDatebffrz','precessDatebffrztime');"></s:textfield>(HH:MM 24 HR)
																    <br/><span style="display:none" id="dynaErrorcordProcessingTime" class="error"><s:text name="garuda.common.validation.timeformat"></s:text> </span>
																    <span style="display:none" id="precessDatebffrztime" class="error prcessError"><br/><s:text name="garuda.cbu.cordentry.procbeforefreeztime"/></span>
																    </td>								  						
																	
																	<td width="20%"></td>
																</tr>
																<tr>
																<td width="20%" style="padding-left:10px;" onmouseover="return overlib('<s:text
																	name="garuda.cordentry.label.freezeStartDateToolTip"></s:text>');" 
																	onmouseout="return nd();"><s:text name="garuda.cordentry.label.freezedate"></s:text>:<span style="color: red;">*</span></td>
																	<td width="20%">
												     					<s:date name="cdrCbuPojo.frzDate" id="datepicker5" format="MMM dd, yyyy" />
													 					<s:textfield readonly="true" onkeydown="cancelBack();"  name="frzDateStr" id="datepicker5" value="%{datepicker5}" cssClass="freezDate labclass cbuLabSummMandatory"></s:textfield>
													 					<span style="display:none" id="dynaErrorFreezeDat" class="error"><br/><s:text name="garuda.cbu.date.freezeDatecollDate"></s:text> </span>
													 					<span style="display:none" id="dynaErrorFreezeDt" class="error"><br/><s:text name="garuda.cbu.cordentry.freezedtwrtproc"/></span>
													 					<span style="display:none" id="frzdateErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.procdatebeforefrzdate"></s:text></span>
													 					<span style="display:none" id="frzdateRangeErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.frzdateTimehrs"></s:text> </span>
													 					<span style="display:none" id="frzdateAftprcStart" class="error prcessError"><br/><s:text name="garuda.cbu.cordentry.frzdateafterproc"></s:text></span>
									            					</td>
									    							 <td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.freezeTime"></s:text>:<span style="color: red;">*</span></td>	
									     							 <td width="20%">
									     							 <s:textfield name="cdrCbuPojo.freezeTime" id="cordFreezeTime" cssClass="timeValid" maxlength="5" onchange="checkTimeFormatForCord('cordFreezeTime');validateHours('datepicker2','cbuCollectionTime','datepicker5','cordFreezeTime','frzTimeRangeErrorMsg');validateDateData('datepicker4','cordProcessingTime','datepicker5','cordFreezeTime','frzdateAftprcStart','frzTimeRangebfProcssTime')"></s:textfield>(HH:MM 24 HR)
									     							 	<span style="display:none" id="dynaErrorcordFreezeTime" class="error"><br/><s:text name="garuda.common.validation.timeformat"></s:text> </span>
									     							 	<span style="display:none" id="frzTimeRangeErrorMsg" class="error"><br/><s:text name="garuda.cbu.cordentry.frzTimehrs"></s:text> </span>
									     							 	<span style="display:none" id="frzTimeRangebfProcssTime" class="error prcessError"><br/><s:text name="garuda.cbu.cordentry.frztimeafterproctime"></s:text> </span>
									     							 </td>					  							
																     <td width="20%"></td>
						        							    </tr>
																<tr>
																	  <td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.bactcult"></s:text>:<span style="color: red;">*</span></td>
																      <td width="20%"><s:select name="cdrCbuPojo.bacterialResult" cssClass="labclass cbuLabSummMandatory autodeferclass" id="bacterialResult" list="populatedData.bactCulList" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="bacterial(this.value);bactAutoDefer(this.value,this.id,'bactDateText','bactDate','labclass',this);setDefBactFungDate('datepicker4','datepicker6','datepicker7','bactChng');"></s:select></td>
																  	  <td width="20%" id="bactDateText" <s:if test="(cdrCbuPojo.bacterialResult!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@NEGATIVE) && cdrCbuPojo.bacterialResult!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@POSITIVE)) || (cdrCbuPojo.bacterialResult==null)">style="display : none;"</s:if>><s:text name="garuda.cordimport.label.bactCultDate"></s:text>:<span style="color: red;">*</span></td>	
																	  <td width="20%" id="bactDate" <s:if test="(cdrCbuPojo.bacterialResult!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@NEGATIVE) && cdrCbuPojo.bacterialResult!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@POSITIVE)) || (cdrCbuPojo.bacterialResult==null)">style="display : none;"</s:if>>
																			<s:date name="cdrCbuPojo.bactCultDate" id="datepicker6" format="MMM dd, yyyy" />
																			<s:textfield readonly="true" onkeydown="cancelBack();"  name="bactCultDateStr" id="datepicker6" value="%{datepicker6}" cssClass="datePicWMaxDate"></s:textfield>
										 							  </td>	
					  					   							  <td width="20%">
																		<s:set name="bactComment" value="cdrCbuPojo.bactComment" scope="request" />
																	   <!-- <s:property value="cdrCbuPojo.bactComment" default="value not fetched"/>-->
																		<s:if test ="#request.bactComment=='' || #request.bactComment == null">
																			<a href="#" onclick="textFunction('textArea1','textAreaForBact','comSave1','<s:property value="cdrCbuPojo.bactComment"/>')" id="com1" ><img height="15px" src="./images/addcomment.png"><span id="addBactComment"><s:text name="garuda.cbu.cordentry.addcomment"/></span></a>
																	    </s:if>
																		<s:else>
																			<a href="#" onclick="textFunction('textArea1','textAreaForBact','comSave1','<s:property value="cdrCbuPojo.bactComment"/>')" id="com3" ><img height="15px" src="./images/addcomment.png"><span id="addBactComment"><s:text name="garuda.cbufinalreview.label.flaggedItems.viewcomment"/></span></a>
																		</s:else>
																		<div id="textArea1" style="display: none;" >
																		<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
																		<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
																			<s:text name="garuda.label.dynamicform.commentbox" />
																		</div>
																		<table>
																			<tr>
																				<td colspan="2"> 
																					<s:textarea name="cdrCbuPojo.bactComment" cols="40" rows="5" id="textAreaForBact" /> 
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<button type="button" onclick="commentSave('textArea1','textAreaForBact','addBactComment')" id="comSave1" ><s:text name="garuda.common.save"/></button>
																				</td>
																				<td>
																				    <button type="button" onclick="commentClose('textArea1','textAreaForBact')" id="comClose1" ><s:text name="garuda.common.close"/></button>
																				</td>
																			</tr>
																		</table></div>
																	</div>
																	 </td>			  						
							     	 							</tr>
							     	 				     	 	<tr>	<td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.fungcult"></s:text>:<span style="color: red;">*</span></td>
															           <input type="hidden" name="fungalCulpostive" id="fungalCulpostive" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@POSITIVE)"/>"/>
															           <s:set name="fungalCulpostive1" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@POSITIVE)" scope="request"/>
															           <input type="hidden" name="fungalCulnotdone" id="fungalCulnotdone" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@NOTDONE)"/>"/>
															           <s:set name="fungalCulnotdone1" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS, @com.velos.ordercomponent.util.VelosGarudaConstants@NOTDONE)" scope="request"/>
															           <td width="20%"><s:select name="cdrCbuPojo.fungalResult" cssClass="labclass cbuLabSummMandatory autodeferclass" id="fungalResult" onchange="fungal(this.value);checkassessment1('Assessment_fungalCulture','Assessment_LabSummeryFung','showassesmentFung','hideassesmentFung',this.id,this.value),fungAutoDefer(this.id,'fungDateText','fungDate','Assessment_LabSummeryFung','labclass',this);setDefBactFungDate('datepicker4','datepicker6','datepicker7','fungChng');" list="populatedData.funCulList" listKey="pkCodeId" listValue="description"  headerKey="-1" headerValue="Select" ></s:select></td>
														     	 	   <td width="20%" id="fungDateText" <s:if test="(cdrCbuPojo.fungalResult!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@NEGATIVE) && cdrCbuPojo.fungalResult!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@POSITIVE)) || (cdrCbuPojo.fungalResult==null)">style="display : none;"</s:if>><s:text name="garuda.cordimport.label.fungCultDate"></s:text>:<span style="color: red;">*</span></td>	
																       <td width="20%" id="fungDate" <s:if test="(cdrCbuPojo.fungalResult!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@NEGATIVE) && cdrCbuPojo.fungalResult!=getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS,@com.velos.ordercomponent.util.VelosGarudaConstants@POSITIVE)) || (cdrCbuPojo.fungalResult==null)">style="display : none;"</s:if>>
																		 <s:date name="cdrCbuPojo.fungCultDate" id="datepicker7" format="MMM dd, yyyy" />
																		 <s:textfield readonly="true" onkeydown="cancelBack();"  name="fungCultDateStr" id="datepicker7" value="%{datepicker7}" cssClass="datePicWMaxDate"></s:textfield>
																       </td>	
												                       <td width="20%">
																		<s:set name="fungComment" value="cdrCbuPojo.fungComment" scope="request" />
																	    <!--<s:property value="cdrCbuPojo.bactComment" default="default value"/>-->
																		<s:if test = "(#request.fungComment == '' || #request.fungComment == null)" >
																			<a href="#" onclick="textFunction('textArea2','textAreaForFung','comSave2','<s:property value="cdrCbuPojo.fungComment"/>')" id="com2" ><img height="15px" src="./images/addcomment.png"><span id="addFungComment"><s:text name="garuda.cbu.cordentry.addcomment"/></span></a>
																	    </s:if>
																		<s:else>
																			<a href="#" onclick="textFunction('textArea2','textAreaForFung','comSave2','<s:property value="cdrCbuPojo.fungComment"/>')" id="com4" ><img height="15px" src="./images/addcomment.png"><span id="addFungComment"><s:text name="garuda.cbufinalreview.label.flaggedItems.viewcomment"/></span></a>
																		</s:else>
																		<div id="textArea2" style="display: none;" >
																		<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
																		<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
																			<s:text name="garuda.label.dynamicform.commentbox" />
																		</div>
																		<table>
																			<tr>
																				<td colspan="2"> 
																					<s:textarea name="cdrCbuPojo.fungComment" cols="40" rows="5" id="textAreaForFung" /> 
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<button type="button" onclick="commentSave('textArea2','textAreaForFung','addFungComment')" id="comSave2" ><s:text name="garuda.common.save"/></button>
																				</td>
																				<td>
																				    <button type="button" onclick="commentClose('textArea2','textAreaForFung')" id="comClose2" ><s:text name="garuda.common.close"/></button>
																				</td>
																			</tr>
																		</table></div>
																	</div></td>	
																	   
																	   
																	   
																	   <%-- <s:textfield name="cdrCbuPojo.fungComment"></s:textfield> --%>
																	   		  							
						               							</tr>
						               							<tr>
						               								<td>
						               									<div id="showassesmentFung" class="LAB_SUM_Fung_showassess" <s:if test="(cdrCbuPojo.fungalResult!=#request.fungalCulnotdone1)">style="display: none;" </s:if>><a href="#"  class="LAB_SUM_Fung_showassess" id="showassesmentFungButton" onclick="showAssment('fungalResult','showassesmentFung','Assessment_fungalCulture','Assessment_LabSummeryFung','hideassesmentFung');"><s:text name="garuda.label.dynamicform.showassess"/></a></div>
																	 	<div id="hideassesmentFung" style="display: none;" class="LAB_SUM_Fung_hideassess"><a href="#"  class="LAB_SUM_Fung_hideassess" id="hideassesmentFungButton" onclick="hideAssment('Assessment_LabSummeryFung','showassesmentFung','hideassesmentFung');" <s:if test="(cdrCbuPojo.fungalResult!=#request.fungalCulnotdone1)">style="display: none;"</s:if>><s:text name="garuda.label.dynamicform.hideassess"/></a></div>
							               							</td>
						               							</tr>
						               							<tr id="Assessment_LabSummeryFung"  style="display: none;"><td colspan="5">
						               								<fieldset><legend><s:text name="garuda.clinicalnote.label.assessment"></s:text>&nbsp;For&nbsp;<s:text name="garuda.cordentry.label.fungcult"/>
																	</legend><table>
																	<tr>
																	
																		<td id="Assessment_fungalCulture"></td>
																		<td></td>
																	</tr></table></fieldset></td>
																</tr>
																  <tr>
 																	  <td width="20%"><s:text name="garuda.cordentry.label.bloodtype"></s:text>:<span style="color: red;">*</span></td>
																      <td width="20%"><s:select name="cdrCbuPojo.aboBloodType" cssClass="labclass cbuLabSummMandatory" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BLOOD_GROUP]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select Type"></s:select></td>
															          	
															           <td width="20%"><s:text name="garuda.cordentry.label.rhtype"></s:text>:<span style="color: red;">*</span></td>
																	    <td width="20%"><s:select name="cdrCbuPojo.rhType" id="cdrcburhtype" cssClass="labclass cbuLabSummMandatory" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@RH_TYPE]" listKey="pkCodeId" onchange="rhSpecify(this.value)" listValue="description" headerKey="-1"  headerValue="Select Type"></s:select></td>
																		     <s:set name="rhTypeOtherVar" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@RH_TYPE, @com.velos.ordercomponent.util.VelosGarudaConstants@RH_TYPE_OTHER)" scope="request" />
																			 <td width="60%" colspan="3">
																				<div id="rhothspec" <s:if test="cdrCbuPojo.rhType!=#request.rhTypeOtherVar">style="display: none;"</s:if>>
																				  <table>
																					 <tr>
																						<td><s:text name="garuda.cbbprocedures.label.specifyother"></s:text>:<span style="color: red;">*</span></td>
																						   <td>
																							  <s:textfield name="cdrCbuPojo.specRhTypeOther" maxLength="30" cssClass="cbuLabSummMandatory"></s:textfield>
																						  </td>
																					 </tr>
																				  </table>
																				</div>	
																			</td>
															           </tr>     
													          	 
								    					           <tr>
									     							  <td width="20%" style="padding-left:10px;"><s:text name="garuda.cordentry.label.hemoglobinScrn"></s:text>:<span style="color: red;">*</span></td>
															          <input type="hidden" name="alphaThalismiaTraitPkVal" id="alphaThalismiaTraitPkVal" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_ALPHA_TRAIT)"/>"/>
															          <s:set name="alphaThalismiaTraitPkVal1" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_ALPHA_TRAIT)" scope="request"/>
															          <input type="hidden" name="hemozygousNoDiesasePkVal" id="hemozygousNoDiesasePkVal" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_NO_DISEASE)"/>"/>
															          <s:set name="hemozygousNoDiesasePkVal1" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_NO_DISEASE)" scope="request"/>
															          <input type="hidden" name="hemoAlphaThalassmiaPkVal" id="hemoAlphaThalassmiaPkVal" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_ALPHA_THAL_SIL)"/>"/>
															          <s:set name="hemoAlphaThalassmiaPkVal1" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_ALPHA_THAL_SIL)" scope="request"/>
															          <input type="hidden" name="hemoTraitPkVal" id="hemoTraitPkVal" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_TRAIT)"/>"/>
															          <s:set name="hemoTraitPkVal1" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_TRAIT)" scope="request"/>
															          <input type="hidden" name="hemozygousNoDonePkVal" id="hemozygousNoDonePkVal" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_NOTDONE)"/>"/>
															          <s:set name="hemozygousNoDonePkVal1" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_NOTDONE)" scope="request"/>
															          <input type="hidden" name="hemozygousUnknownPkVal" id="hemozygousUnknownPkVal" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_UNKNOWN)"/>"/>
															          <s:set name="hemozygousUnknownPkVal1" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_UNKNOWN)" scope="request"/>
															          <input type="hidden" name="hemoMultiTraitPkVal" id="hemoMultiTraitPkVal" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_MULT)"/>"/>
															          <s:set name="hemoMultiTraitPkVal1" value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN, @com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHY_MULT)" scope="request"/>
															          <td width="20%"><s:select name="cdrCbuPojo.hemoglobinScrn" id="hemoglobinScrnTest" cssClass="labclass autodeferclass" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onChange="checkassessment1('Assessment_Hemoglobinopathy','Assessment_LabSummeryHemo','showassesmentHemo','hideassesmentHemo',this.id,this.value),autoDeferHEMO(this.value,this.id,'labclass',this);"></s:select></td>
													   			      <td width="20%"></td>
					             								</tr>
					             								<tr>
					             									<td>
					             										<span id="showassesmentHemo" class="LAB_SUM_Hemo_showassess"><s:if test="cdrCbuPojo.hemoglobinScrn==#request.alphaThalismiaTraitPkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDonePkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousUnknownPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDiesasePkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoAlphaThalassmiaPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoTraitPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoMultiTraitPkVal1"><a href="#"  id="showassesmentHemoButton" onclick="showAssment('hemoglobinScrnTest','showassesmentHemo','Assessment_Hemoglobinopathy','Assessment_LabSummeryHemo','hideassesmentHemo');"><s:text name="garuda.label.dynamicform.showassess"/></a></s:if>
																	  </span>
																	  <div id="hideassesmentHemo" style="display: none;" class="LAB_SUM_Hemo_hideassess"><s:if test="cdrCbuPojo.hemoglobinScrn==#request.alphaThalismiaTraitPkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDonePkVal1 ||cdrCbuPojo.hemoglobinScrn==#request.hemozygousUnknownPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemozygousNoDiesasePkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoAlphaThalassmiaPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoTraitPkVal1 || cdrCbuPojo.hemoglobinScrn==#request.hemoMultiTraitPkVal1"><a href="#"  id="hideassesmentHemoButton" onclick="hideAssment('Assessment_LabSummeryHemo','showassesmentHemo','hideassesmentHemo');"><s:text name="garuda.label.dynamicform.hideassess"/></a></s:if></div>
															          	
					             									</td>
					             								</tr>
					             								<tr id="Assessment_LabSummeryHemo"  style="display: none;"><td colspan="5">
															          	<fieldset><legend><s:text name="garuda.clinicalnote.label.assessment"></s:text>&nbsp;For&nbsp;<s:text name="garuda.cordentry.label.hemoglobinScrn"/>
																					</legend><table>
																					<tr>
																			<td colspan="5" id="Assessment_Hemoglobinopathy"></td>
																			</tr></table></fieldset></td>
																		<td></td>
																  </tr>     
					             								
	                      										</table></div>
	                      										<div id="labSummeryDivR">
																</div>
															</div>
														</div>
													</td>
												</tr>												
												<tr>
													<td align="left"><input type="button" id="addnewtest"
														value="<s:text name="garuda.cdrcbuview.label.button.addNewTest" />"
														onclick="javascript:addTest('<s:property value="cdrCbuPojo.cordID" />','False');" />
													</td>
												</tr>
											
											</table>
											</div>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
	     <!--  <div class="column">
	      <div class="portlet" id="procinfoparent" >
					<div id="procinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<span class="ui-icon ui-icon-close" ></span>
					<s:text name="garuda.cordentry.label.procinfo" />
					</div>
					<div class="portlet-content">
					<jsp:include page="cb_cord-entry_sampinvent.jsp" flush="true"></jsp:include>					
					</div>					
					</div>					
					</div>
		 </div>	-->    
	     <div class="column">
	      <div class="portlet" id="mrqinfoparent" >
					<div id="mrqinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<!--<span class="ui-icon ui-icon-close" ></span>-->
					<s:text name="garuda.cordentry.label.mrq" />
					</div>
					<div class="portlet-content" id="mrqAsyncData">
					</div></div></div>
	    </div>
	     <div class="column">
	      <div class="portlet" id="fmhqinfoparent" >
					<div id="fmhqinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<!--<span class="ui-icon ui-icon-close" ></span>-->
					<s:text name="garuda.cordentry.label.fmhq" />
					</div>
					<div class="portlet-content" id="fmhqAsyncData">
					</div></div></div>
	    </div>
	     <div class="column">
	      <div class="portlet" id="idminfoparent" >
					<div id="idminfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<!--<span class="ui-icon ui-icon-close" ></span>-->
					<s:text name="garuda.cordentry.label.idm" />
					</div>
					<div id="idmtestinfoforcbu" class="portlet-content"></div>
					</div></div>
	    </div>
	    <div class="column">
	      <div class="portlet" id="hlainfoparent" >
					<div id="hlainfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<!--<span class="ui-icon ui-icon-close" ></span>-->
					<s:text name="garuda.cordentry.label.hla" />
					</div>
					<s:hidden id="hlaPercentageId" name="hlaPercentage" />
					<div class="portlet-content" id="hlaAsyncData">
					 </div></div></div>
	    </div>
	    <div class="column">
	      <div class="portlet" id="eliginfoparent" >
					<div id="eliginfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<!--<span class="ui-icon ui-icon-close" ></span>-->
					<s:text name="garuda.cordentry.label.eligibility" />
					</div>
					<div class="portlet-content">
					 <div id="eligbdata">
					   <table>
					   		    <tr>
        						<td colspan="2">
        								<s:if test = "#request.shipMentFlag==true">
        								<div id="newQstns">
        									<s:hidden name="declNewPojo.pkNewEligDecl"></s:hidden>
            								<jsp:include page="../cb_new_final_decl_quest.jsp"/>
            								</div>
        							  </s:if>
        							<s:else>
       									<s:hidden name="declPojo.pkEligDecl"></s:hidden>
            							<jsp:include page="../cord-entry/cb_final_decl_quest.jsp"/>
        							</s:else>
        						</td>
   						 </tr>
							<tr>
								<td colspan="2">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
								  		 <tr>
							  				<td width="25%" style="align:left"><s:text name="garuda.cdrcbuview.label.eligibility_determination" />:<span style="color: red;">*</span></td>
											<td width="75%" style="align:left"><s:select name="fkCordCbuEligible" cssStyle="width:470px;" id="fkCordCbuEligible" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS]"
											listKey="pkCodeId" listValue="description" headerKey="-1"
											headerValue="Select" cssClass="eligibclass autodeferclass" onchange="inEligibleReason(this.value,'inEligibleReason');changeLicensure(this.value,'%{fkCordCbuEligible}','eligibclass',this);" /></td>
										 </tr>
									</table>
								</td>
							</tr>	
							<tr>			
							   	<td colspan="2">
							   		<div id="inEligibleReason" style="display: none;">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
								  		 <tr id="ineligiblereason">
											<td width="25%" style="vertical-align: centre;align:left;">
											 <div id="inelreason" style="display: none"><s:text name="garuda.cdrcbuview.label.ineligible_reason" />:<span style="color: red;">*</span></div>
											 <div id="increason" style="display: none"><s:text name="garuda.cdrcbuview.label.incomplete_reason" />:<span style="color: red;">*</span></div>
											 <div id="priorreason" style="display: none"><s:text name="garuda.cdrcbuview.label.prior_reason" />:<span style="color: red;">*</span></div></td>
											<td width="75%" style="align:left"><s:select cssStyle="height:100px;width:85%;" name="reasons" id="fkCordCbuEligibleReason" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@INELIGIBLE_REASON]"
												listKey="pkCodeId" listValue="description"  value="reasons"
												  multiple="true"/></td>
								  		</tr>
								 		<tr id="additonalinfo" >
										<td width="25%" style="vertical-align: middle; align:left;" ><s:text name="garuda.cdrcbuview.label.ineligible_reason_notes" />:</td>
										<td width="75%" style="align:left"><s:textarea name="cordAdditionalInfo" cols="73" rows="5" id="word_count" cssStyle="width:470px;"  /> 
										<br/>
										<span id="counter"></span>
									  </td>
								   </tr>			   
								</table>
								</div>
							   </td>
					        </tr>

					   </table>					
					</div></div></div>
	    </div></div>
	    <div class="column">
	      <div class="portlet" id="clnotesinfoparent" >
					<div id="clnotesinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<!--<span class="ui-icon ui-icon-close" ></span>-->
					<s:text name="garuda.cordentry.label.clinicalnotes" />
					</div>
					<div class="portlet-content" id="notesAsyncData">
					</div></div></div>
	    </div>
	     <div class="column">
		      <div class="portlet" id="cbuinfoparent" >
					<div id="cbuinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
						<div class="portlet-content">
							<table bgcolor="#cccccc" width="100%">
						         <tr bgcolor="#cccccc" valign="baseline">
								   <td width="50%">
						                 <table width="98%" border="0" cellspacing="0" cellpadding="0" bgcolor="#cccccc">
											<tr valign="baseline" bgcolor="#cccccc">
											    <td>
												<span id="cordIdEntryValid" style="display: none;" class="validation-fail"><s:text name="garuda.common.esign.invalid"/></span>
												<span id="cordIdEntryMinimum" style="display: none;" class="validation-fail"><s:text name="garuda.common.esign.minimumLength"/></span>
												<span id="cordIdEntryPass" style="display: none;" class="validation-pass"><s:text name="garuda.common.esign.valid"/></span>
												</td>
												<td nowrap="nowrap">
												<s:text name="garuda.esignature.lable.esign" /><span style="color:red;">*</span></td>
												<td><s:password name="userPojo.signature" id="%{#request.esign}" size="10" style="font-style:bold; color:#000000;" onkeyup="validateSignCordEntry(this.value,'submitcdrsearch','submitcdrinprogress','cordIdEntryValid','cordIdEntryMinimum','cordIdEntryPass')" cssStyle="width:100px"></s:password>
												</td>		
											</tr>
										</table>
									</td>
							           <td width="50%" align="right" valign="top">
							           <s:if test="hasEditPermission(#request.editCBURght)==true && (deferedCord==null || deferedCord==false)">
							              <button type="button" id="submitcdrinprogress" disabled="disabled" onclick="saveInProgress()" ><s:text name="garuda.cordentry.label.saveinprogress"/></button>
							              <button type="button" id="submitcdrsearch" disabled="disabled" onclick="saveForCBUSearching()"><s:text name="garuda.cordentry.label.submitcbuforsearching"/></button>
							           </s:if>					          
							       </td>
						        </tr>
						   </table>	
					   </div>
				   </div>
			</div>
	    </div>
	 </s:form>
	</div></div>
	<s:if test="cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.inEligiblePkid
	|| cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.incompleteReasonId || cdrCbuPojo.fkCordCbuEligible==cdrCbuPojo.notCollectedToPriorReasonId">
	  <script>
	  inRefEligibleReason('<s:property value="cdrCbuPojo.fkCordCbuEligible"/>','inEligibleReason');    
	  </script>  
	</s:if>
	<s:if test="cdrCbuPojo.cbuLicStatus==cdrCbuPojo.unLicesePkid">
	   <script>
	   unliceseReason('<s:property value="cdrCbuPojo.unLicesePkid"/>','licenseReason');    
	  </script>
	</s:if>	
	<s:if test="deferedCord!=null && deferedCord==true">
	  <script>
	  $j('input, textarea, select')
      .attr('disabled', 'disabled');
      $j('.lookupmenu input').removeAttr('disabled');
	  </script>
	</s:if>
	<script>    	  
	  $j(function() {	
		  var temp=0;
		  <s:if test="#request.additionalIds!=null">
		    temp=<s:property value="additionalIds.size"/>;
		  </s:if>		
		  (temp> 0) ?( maxadditionalidcount=additionalIdCount = temp) : additionalIdCount = 0;	    
	     htmlUniqueIdOnBag();	         
	  });
	</script>
	<s:if test="cdrCbuPojo.cordEntryProgress==null || cdrCbuPojo.cordEntryProgress==''">
	   <script>	      
          window.onload = updateProgress;
	   </script>
	</s:if>
</div>
<div style="display: none;" id="modalEsign1">
   <table bgcolor="#cccccc" width="100%">
         <tr bgcolor="#cccccc" valign="baseline">
		   <td width="50%">
                 <table width="98%" border="0" cellspacing="0" cellpadding="0" bgcolor="#cccccc">
					<tr valign="baseline" bgcolor="#cccccc">
					    <td>
						<span id="cordIdEntryValid1" style="display: none;" class="validation-fail"><s:text name="garuda.common.esign.invalid"/></span>
						<span id="cordIdEntryMinimum1" style="display: none;" class="validation-fail"><s:text name="garuda.common.esign.minimumLength"/></span>
						<span id="cordIdEntryPass1" style="display: none;" class="validation-pass"><s:text name="garuda.common.esign.valid"/></span>
						</td>
						<td nowrap="nowrap">
						<s:text name="garuda.esignature.lable.esign" /><span style="color:red;">*</span></td>
						<td><s:password name="userPojo.signature" id="esignAutoDefer" size="10" style="font-style:bold; color:#000000;" onkeyup="validateSign(this.value,'submitcdrsearch1','cordIdEntryValid1','cordIdEntryMinimum1','cordIdEntryPass1')" cssStyle="width:100px"></s:password>
						</td>		
					</tr>
				</table>
			</td>
	           <td width="50%" align="right" valign="top">
	           <s:if test="(deferedCord==null || deferedCord==false)">
	              <button type="button" id="submitcdrsearch1" disabled="disabled" onclick="loadDivWithFormSubmitDefer('saveAutoDeferPartialCordEntry?autoDeferFlag=false','maincontainerdiv','cordentryform1');"><s:text name="garuda.unitreport.label.button.submit"/></button>	              
	           </s:if>
	           <button type="button" onclick="cancelAutoDeferEsign()"><s:text name="garuda.common.lable.cancel"/></button>					          
	       </td>
        </tr>
   </table>	
</div>
<div id="dialog-resetLicDiv" style="display: none;">
<div id="dialog-NAppconfirm-1" style="display: none;">
	<s:text name="garuda.common.validation.notapplicableelig"/> 
</div>
<table><tr><td><img src='images/warning.png'/></td><td><s:text name="garuda.cbu.license.cntchngtolicweninelig"/></td></tr></table>
</div>
<script> 
	queueManager('loadAsyncIdmFormCordEntryData?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID"/>&formDataToBeLoaded=8','idmtestinfoforcbu');
	queueManager('loadAsyncFmhqFormCordEntryData?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID"/>&formDataToBeLoaded=4','fmhqAsyncData');
	queueManager('loadAsyncMrqFormCordEntryData?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID"/>&formDataToBeLoaded=2','mrqAsyncData');
	queueManager('loadAsyncProcedureCordEntryData?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID"/>&cdrCbuPojo.fkCbbProcedure=<s:property value="cdrCbuPojo.fkCbbProcedure"/>&cdrCbuPojo.fkCbbId=<s:property value="cdrCbuPojo.fkCbbId"/>','procedureAsyncData');
	queueManager('loadAsyncHlaCordEntryData?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID"/>','hlaAsyncData');
	queueManager('loadAsyncNotesCordEntryData?registryId=<s:property value="cdrCbuPojo.registryId"/>&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID"/>','notesAsyncData');
	queueManager('loadAsyncLabSummaryCordEntryData?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID"/>&cdrCbuPojo.rhType=<s:property value="cdrCbuPojo.rhType"/>&cdrCbuPojo.specRhTypeOther=<s:property value="cdrCbuPojo.specRhTypeOther"/>&cdrCbuPojo.fkSpecimenId=<s:property value="cdrCbuPojo.fkSpecimenId"/>&cdrCbuPojo.hemoglobinScrn=<s:property value="cdrCbuPojo.hemoglobinScrn"/>&cdrCbuPojo.fungalResult=<s:property value="cdrCbuPojo.fungalResult"/>','labSummeryDivR');
   loadAsyncRequest_1(urlArr.pop(),divArr.pop());
   loadAsyncRequest_2(urlArr.pop(),divArr.pop());
</script>
<jsp:include page="../cb_eligibility_questionnaire_validation.jsp"></jsp:include>