 <%@page import="com.velos.eres.web.user.UserJB"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="userLB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants"%>
<%@page import="com.velos.ordercomponent.business.util.VelosMidConstants"%>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<% String contextpath=request.getContextPath();%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<script>
function constructTableNACBU() {
	
	var show_Entries_txt=$j('#nacbu_tbl_se').val();
	//var showEntries_val='Show <select name="showsRow1" id="showsRow1" style="width:auto; height:25px;" ><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+$j('#NAVtotalRows').val()+'">ALL</option></select> Entries';
	var showEntries_val='Show <select name="showsRow1" id="showsRow1" style="width:auto; height:25px;" ><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries';
	var pageNo=$j('#paginateWrapper1').find('.currentPage').text();
	var lastSort=$j('#nacbu_sortParam').val();
	var sort="0";
	var sortDir="asc";
	
	if(show_Entries_txt==null || show_Entries_txt=="" || show_Entries_txt=="undefined"){
		show_Entries_txt=5;
	}
	
	if(lastSort!=undefined && lastSort!=null && lastSort!=""){
		sort=$j('#nacbu_sortParam').val();
		sortDir=$j('#nacbu_sort_type').val();
	}
	
    if(show_Entries_txt=="ALL"){
    	
    	oTable =  $j('#searchResults1').dataTable({
    		"sScrollY": "200", 
            "sDom": "frtiS",
            "sAjaxSource": 'getJsonPagination.action?TblFind=notAvailCBUs&allEntries=ALL'+$j('#nacbufilteparams').val(),
            "bServerSide": true,
            "bProcessing": true,
            "bRetrieve" : true, 
            "bDestroy": true,
            "aaSorting": [[ sort, sortDir ]],
            "bDeferRender": true,
            "bAutoWidth": false,
            "sAjaxDataProp": "aaData",
             "oLanguage": {
    			"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>",
    			"sZeroRecords": "<s:text name="garuda.common.message.noMatchingRecordsforNACBU"/>"
    		}, 
    		"bSortCellsTop": true,
            "aoColumnDefs": [
                              {
                            	  "aTargets": [0], "mDataProp": function ( source, type, val ) {
                                      var str=""
                                      if("<s:property value='hasEditPermission(#request.notavilCbu)'/>"=="true"){
                                          if(source[4]!=null && source[4]==0){
                                          	str="<u onclick=\"submitpost('openWholeCordEntry',{'cdrCbuPojo.cordID':'"+source[0]+"'})\"  style='cursor: pointer;'>"+source[1]+"</u>";
                                          }else if(source[4]!=null && source[4]==1){
                                          	str="<u style='cursor: pointer;' onclick=\"callDeferMethod('"+source[1]+"')\">"+source[1]+"</u>";
                                          }else{
                                            str = source[1];
                                          }
                                      }else{
                                    	  if("<s:property value='hasEditPermission(#request.notavilCbu)'/>"=="false"){
                                        	str = "<span onclick=\"alert('User do not have permission to edit Not Available CBUs ')\"  style='cursor: pointer;'>"+source[1]+"<span>";
                                       	  }else{
                                           	str = source[1];
                                       	  }
                                      }
                                     return str;
                                 }},
                                {
                                	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
                                         return source[2];
                                 }},
                              	{
                                	 "aTargets": [2], "mDataProp": function ( source, type, val ) {
                                        return source[3];
                                 }}
                            ],
                            "fnDrawCallback": function() {
                                $j('#searchResults1_wrapper').find('.dataTables_scrollFoot').hide();
                                $j('#searchResults1_info').show();
                                $j("#showsRow1 option:contains('ALL')").attr('selected', 'selected');
                                $j('#searchResults1_wrapper').find('.dataTables_scrollHeadInner').find('table').css('position','');
                                showStatusFilterText('naCBUStatFilterFlagTxt','naCBUStatFilterFlag');
                                nacbuSort();
                            }
        });
    	$j('#searchResults1_filter').before(showEntries_val);
    	
    }else{
    	var oTable =$j('#searchResults1').dataTable().fnDestroy();
		  oTable =  $j('#searchResults1').dataTable({
    		"sScrollY": "200", 
            "sAjaxSource": 'getJsonPagination.action?TblFind=notAvailCBUs&otherThanAll=1&allEntries=ALL&iShowRows='+show_Entries_txt+'&iPageNo='+pageNo+$j('#nacbufilteparams').val(),
            "bServerSide": true,
            "bProcessing": true,
            "bRetrieve" : true, 
            "bDestroy": true,
            "bDeferRender": true,
            "aaSorting": [[ sort, sortDir ]],
            "bAutoWidth": false,
            "sAjaxDataProp": "aaData",
             "oLanguage": {
    			"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>",
    			"sZeroRecords": "<s:text name="garuda.common.message.noMatchingRecordsforNACBU"/>"
    		}, 
    		"bSortCellsTop": true,
            "aoColumnDefs": [
                              {
                            	  "aTargets": [0], "mDataProp": function ( source, type, val ) {
                                      var str=""
                                      if("<s:property value='hasEditPermission(#request.notavilCbu)'/>"=="true"){
                                          if(source[4]!=null && source[4]==0){
                                          	str="<u onclick=\"submitpost('openWholeCordEntry',{'cdrCbuPojo.cordID':'"+source[0]+"'})\"  style='cursor: pointer;'>"+source[1]+"</u>";
                                          }else if(source[4]!=null && source[4]==1){
                                          	str="<u style='cursor: pointer;' onclick=\"callDeferMethod('"+source[1]+"')\">"+source[1]+"</u>";
                                          }else{
                                            str = source[1];
                                          }
                                      }else{
                                    	  if("<s:property value='hasEditPermission(#request.notavilCbu)'/>"=="false"){
                                        	str = "<span onclick=\"alert('User do not have permission to edit Not Available CBUs ')\"  style='cursor: pointer;'>"+source[1]+"<span>";
                                       	  }else{
                                           	str = source[1];
                                       	  }
                                      }
                                     return str;
                                 }},
                                {
                                	 "aTargets": [1], "mDataProp": function ( source, type, val ) {
                                         return source[2];
                                 }},
                              	{
                                	 "aTargets": [2], "mDataProp": function ( source, type, val ) {
                                        return source[3];
                                 }}
                            ],
                            "fnDrawCallback": function() {
                                $j('#searchResults1_wrapper').find('.dataTables_scrollHeadInner').find('table').css('position','');
                                showStatusFilterText('naCBUStatFilterFlagTxt','naCBUStatFilterFlag');
                                nacbuSort();
                            }
        });
    	
    }
	
    $j('#searchResults1_info').hide();
	$j('#searchResults1_paginate').hide();
	$j('#searchResults1_length').empty();
	$j('#searchResults1_length').replaceWith(showEntries_val);
	
	if($j('#entries1').val()!=null || $j('#entries1').val()!=""){
		$j('#showsRow1').val($j('#entries1').val());
	}
    
    $j('#showsRow1').change(function(){
        var selectedText = $j('#showsRow1 :selected').html();
        $j('#nacbu_tbl_se').val(selectedText);
        paginationFooter(0,"getNotAvailCordsPaginationData,showsRow1,inputs,Tableform,searchTble1,constructTableNACBU",'<s:property value="paginationSearch.getiTotalRows()"/>','<s:property value="paginateModule"/>');
    }); 
    
	$j('#resetSortCENACBU').click(function(){
		if($j('#nacbu_sortParam').val()!=undefined && $j('#nacbu_sortParam').val()!=null && $j('#nacbu_sortParam').val()!="" && $j('#nacbu_sortParam').val()!=0){
			$j('#nacbu_sortParam').val('');
			$j('#nacbu_sort_type').val('');
			var oTable = $j('#searchResults1').dataTable();
			oTable.fnSort( [ [0,'asc'] ] );
			oTable.fnDraw();
		}
	});
    
    $j('#dateStart').val($j('#naCBUStrDt').val());
	$j('#dateEnd').val($j('#naCBUEndDt').val());
}
function nacbuSort(){
	$j('#nacbuTheadTr').find('th').each(function(i){
		
	   	if($j(this).hasClass('sorting_asc')){
			$j('#nacbu_sortParam').val(i);
			$j('#nacbu_sort_type').val('asc');
		}
	   	if($j(this).hasClass('sorting_desc')){
			$j('#nacbu_sortParam').val(i);
			$j('#nacbu_sort_type').val('desc');
		}
   });
}
function manageNotAvailableCords(){
	var oTable =$j('#searchResults1').dataTable().fnDestroy();
	$j('#searchResults1').dataTable({
		"sScrollY": "200",
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>",
			"sZeroRecords": "<s:text name="garuda.common.message.noMatchingRecordsforNACBU"/>"
		},
		"bRetrieve": true,
		"bDestroy" :true, 
		"bSortCellsTop": true,
		"bDeferRender": true,
		"iDisplayLength":1000,
		"fnDrawCallback": function() {
			showStatusFilterText('naCBUStatFilterFlagTxt','naCBUStatFilterFlag');
		}
	});
	 $j('#searchResults1_info').hide();
		$j('#searchResults1_paginate').hide();
		$j('#searchResults1_length').empty();
		//$j('#searchResults1_length').replaceWith('Show <select name="showsRow1" id="showsRow1" style="width:auto; height:25px;" ><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="<s:property value="paginationSearch.getiTotalRows()"/>">All</option></select> Entries');
		$j('#searchResults1_length').replaceWith('Show <select name="showsRow1" id="showsRow1" style="width:auto; height:25px;" ><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option></select> Entries');
		if($j('#entries1').val()!=null || $j('#entries1').val()!=""){
			$j('#showsRow1').val($j('#entries1').val());
			}
		$j('#showsRow1').change(function(){
			var selectedText = $j('#showsRow1 :selected').html();
			if(selectedText=="All"){	
				constructTableNACBU();
			}else{
				paginationFooter(0,"getNotAvailCordsPaginationData,showsRow1,inputs,Tableform,searchTble1,manageNotAvailableCords",'<s:property value="paginationSearch.getiTotalRows()"/>','<s:property value="paginateModule"/>');
			}
		});
		$j('#searchResults1_filter').find('input:text').keyup(function(){
			 f_countRows('searchResults1','1','fromsearchall','HeaderSelectOrd','empty','empty','empty','empty1');
		}); 
		$j('#dateStart').val($j('#naCBUStrDt').val());
		$j('#dateEnd').val($j('#naCBUEndDt').val());
}
function customRange(input) {
	  if (input.id == 'dateEnd') {
	    /*return {
	      minDate: jQuery('#dateStart').datepicker("getDate")
	    };*/
	    if($j("#dateStart").val()!="" && $j("#dateStart").val()!=null){
	    var minTestDate=new Date($j("#dateStart").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#dateEnd').datepicker( "option", "minDate", new Date(y1,m1,d1));
	    }else{
	    	$j('#dateEnd').datepicker( "option", "minDate", null);
		 }
	  } else if (input.id == 'dateStart') {
		  if($j("#dateEnd").val()!="" && $j("#dateEnd").val()!=null){
		  var minTestDate=new Date($j("#dateEnd").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
	   /* return {
	      maxDate: jQuery('#dateEnd').datepicker("getDate")
	    };*/
		  $j('#dateStart').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#dateStart').datepicker( "option", "maxDate", null);
			 }
	  }
	  
}
function callDeferMethod(regID){
	var resultString = "";
	var result = removeSymbolfromId(regID);
	//console.log("result::"+result);
	deferedCord(result);
}

function fn_filterNACBU(){
	
	var cbuRegid="";
	var startDate="";
	var startEnd="";
	var cbuStatus="";
	var dateData="0";
	
	
	
	cbuRegid=$j.trim($j('#regId').val());
	cbuStatus=$j('#cbuReason1').val();
	
	
	if($j('#dateStart').val()!="" && $j('#dateEnd').val()!=""){
		var dateElements = $j('.datepic').get();
		  $j.each(dateElements, function(){
			  customRange(this);
		  });
		if($j('#dummyForm').valid()){
		startDate=parseDateValue($j('#dateStart').val());
		startEnd=parseDateValue($j('#dateEnd').val());
		dateData="1";
		}
		else{
			dontflag=1;
			$j('#dateDiv').show();
			}
		
	}else if( ($j.trim($j('#dateStart').val())!="" && $j.trim($j('#dateEnd').val())=="" ) || ($j.trim($j('#dateStart').val())=="" && $j.trim($j('#dateEnd').val())!="")){
		
		alert("Please enter correct Date Range");
		dontflag=1;
	}
	var is_all_entries=$j('#showsRow1 :selected').text();
	
	
	if(cbuRegid!="" || cbuStatus!="" || dateData=="1"){
		filterParams='&cbuRegId='+cbuRegid+"&cbuStatus="+cbuStatus+"&startDate="+startDate+"&endDate="+startEnd+'&notAvailCbuCount=0';
		$j('#nacbufilteparams').val(filterParams);
		$j('#naCBUStatFilterFlag').val('1');
		$j('#nacbu_filterFlag').val("1");
		var selectedText = $j('#showsRow1 :selected').html();
		paginationFooter(0,"getNotAvailCordsPaginationData,showsRow1,inputs,Tableform,searchTble1,constructTableNACBU",'<s:property value="paginationSearch.getiTotalRows()"/>','<s:property value="paginateModule"/>');
	}
	
	
}
function fn_resetFilterNACBU(){
	
	
	var cbuRegid="";
	var dateStart="";
	var dateEnd="";
	var cbuStatus="";
	var dateData="0";
	var filterFlag=$j('#nacbu_filterFlag').val();
	resetValidForm('dummyForm');
	$j('#naCBUStatFilterFlag').val('0');
	if($j('#dateStart').val()!="" && $j('#dateEnd').val()!=""){
		dateData="1";
	}
	
	cbuRegid=$j.trim($j('#regId').val());
	cbuStatus=$j('#cbuReason1').val();
	
	if(filterFlag=="1"){
		
		$j('#nacbufilteparams').val('&notAvailCbuCount=0');
		var selectedText = $j('#showsRow1 :selected').html();
		$j('#nacbu_filterFlag').val("0");
		paginationFooter(0,"getNotAvailCordsPaginationData,showsRow1,inputs,Tableform,searchTble1,constructTableNACBU",'<s:property value="paginationSearch.getiTotalRows()"/>','<s:property value="paginateModule"/>');
	}
	
	
	$j('#regId').val('');
	$j('#cbuReason1').val('');
	$j('#dateStart').val('');
    $j('#dateEnd').val('');
    $j('#nacbufilteparams').val('');
	
	
}
</script>
<s:form>
 <input type="hidden" name="entries1" id="entries1" value=<%=paginationSearch.getiShowRows() %> />
 <input type="hidden" name="totalRows" id="NAVtotalRows" value=<%=paginationSearch.getiTotalRows() %> />
 <table align="right">
				<tr align="right" style="vertical-align: top;">
						<td>
						<a href="#" onclick="fn_filterNACBU()" ><s:text name="garuda.pendingorderpage.label.executeFilter"/></a>
						&nbsp;&nbsp;&nbsp;
						<a href="#" onclick="fn_resetFilterNACBU()" ><s:text name="garuda.landingpage.button.label.reset"/></a>
						&nbsp;&nbsp;
						<a href="#"  id="resetSortCENACBU"><span><s:text name="garuda.pendingorderpage.label.resetsort"/></span></a>
					    </td>
				</tr>
</table>
 
 
 <s:hidden name="naCBUStrDt" id="naCBUStrDt"/>
 <s:hidden name="naCBUEndDt" id="naCBUEndDt"/>
 <table width="100%" cellpadding="0" cellspacing="0" border="0" class="displaycdr searchResults" id="searchResults1">
    <thead>
		<tr id="nacbuTheadTr">
			<th><s:text name="garuda.cbuentry.label.registrycbuid"/></th><th><s:text name="garuda.cbuentry.label.cbu_status"/></th><th><s:text name="garuda.pendingorderpage.label.dateofentry"/></th>
		</tr>
		<tr>
			<th><s:textfield name="naCBURegId" id="regId" placeholder="Enter CBU Registry ID" /></th>
			<th>
			<select id="cbuReason1" name="cbuReason1">
			<option value="">Select</option>
			<s:iterator value="getCBUStatusValues(@com.velos.ordercomponent.business.util.VelosMidConstants@DEFRED_CORD)">
			<s:if test="%{pkcbustatus==naCBUStat}">
			<option value="<s:property value='pkcbustatus'/>" selected="selected" ><s:property value="cbuStatusDesc"/></option>
			</s:if>
			<s:else>
			<option value="<s:property value='pkcbustatus'/>"> <s:property value="cbuStatusDesc"/></option>
			</s:else>
			</s:iterator>
			</select>
		   <th>
		   <button type="button" id="dateRange1" onclick="showDateDiv(this,'dateDiv','dateStart','dateEnd'); resetValidForm('dummyForm');">
			<span class="ui-button-text" id="buttonSpan"><s:text name="garuda.pendingorder.button.dateRange"/></span>
		   </button>
		   </th>
		</tr>
	</thead>
	<tbody>
		<s:iterator value="#request.cbuDeferlist" var="rowVal" status="row">
			<!--<tr onclick="deferedCord('<s:property value="cordID" />')" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')">	-->										        
			<tr id="<s:property	value="%{#row.index}"/>" onMouseOver="highlight('<s:property	value="%{#row.index}"/>')" onmouseout="removeHighlight('<s:property	value="%{#row.index}"/>')"> 
				<td>
				  <div style="display:none"></div>
			    	<s:if test="hasEditPermission(#request.notavilCbu)==true">
			    	<s:if test="%{#rowVal[4]!=null && #rowVal[4]==0}">
					   <u onclick="submitpost('openWholeCordEntry',{'cdrCbuPojo.cordID':'<s:property value="%{#rowVal[0]}" />'})"  style="cursor: pointer;">
						    <s:set name="registryIdFld" value="%{#rowVal[1]}" />
	        	 	        <s:property value="insertHyphenIntoRegistryId(#registryIdFld)"/>
					   </u>
					</s:if>					
					<s:elseif test="%{#rowVal[4]!=null && #rowVal[4]==1}">
					    <u onclick="deferedCord('<s:property value="%{removeSymbol(#rowVal[1],'-')}"/>')"  style="cursor: pointer;">
						  <s:property value="%{#rowVal[1]}" />
					    </u>					   
					</s:elseif>
					<s:else>
					    <s:property value="%{#rowVal[1]}" />
					</s:else>
					</s:if>
					<s:else>
					<s:if test="hasEditPermission(#request.notavilCbu)==false">
					   <span onclick="alert('User do not have permission to edit Not Available CBUs ');"  style="cursor: pointer;">
						    <s:set name="registryIdFld" value="%{#rowVal[1]}" />
	        	 	        <s:property value="insertHyphenIntoRegistryId(#registryIdFld)"/>
					   </span>
					</s:if>	
					<s:else>
					    <s:property value="%{#rowVal[1]}" />
					</s:else>	
					</s:else>					
				</td>
				<td>
					<s:set name="pkCbuStatus" value="%{#rowVal[2]}"  scope="request"/>
					<s:property value="%{#rowVal[2]}"/>
				</td>
				<td><s:date name="%{#rowVal[3]}" id="datepicker" format="MMM dd, yyyy" />
					<s:property value="%{datepicker}" />
				</td>
			</tr>
		</s:iterator>									     
	</tbody>
	 <tfoot>
		<tr><td colspan="4"></td></tr>			
		<tr >
		   <td colspan="4">
			    <jsp:include page="../paginationFooter.jsp">
				  	<jsp:param value="searchTble1" name="divName"/>
				  	<jsp:param value="Tableform" name="formName"/>
				  	<jsp:param value="showsRow1" name="showsRecordId"/>
				  	<jsp:param value="getNotAvailCordsPaginationData" name="url"/>
				  	<jsp:param value="inputs" name="cbuid"/>
				  	<jsp:param value="notAvailableCbusOnCordEntry" name="paginateSearch"/>
				  	<jsp:param value="1" name="idparam"/>
				  	<jsp:param value="constructTableNACBU" name="bodyOnloadFn"/>
			    </jsp:include>
	       </td>      
	   </tr>
	</tfoot>							     								     
 </table>
</s:form>