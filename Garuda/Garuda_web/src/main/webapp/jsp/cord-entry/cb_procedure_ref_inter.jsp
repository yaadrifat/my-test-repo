<%@ taglib prefix="s"  uri="/struts-tags"%>
<script>
$j(function(){
	if($j("#fkProcMethId").val()=='<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING, @com.velos.ordercomponent.util.VelosGarudaConstants@AUTOMATED)"/>')
	{
	$j("#fkIfAutomated").addClass("cbuProcMandatory");
	}
	if ( ($j("#otherAnticoagulant").val() !='' || $j("#otherAnticoagulantper").val() != '')  ) {
			$j("#otherAnticoagulantDivAdd").show();
		}
		else{
		$j("#specifyOthAnti").val("");
		$j('#otherAnticoagulantDivAdd').hide();
		}	

	  if ( ($j("#othCryoprotectant").val() !='' || $j("#othCryoprotectantper").val() != '')) {
			$j("#otherCryoprotectantDivDis").show();
		}
		else{
		$j("#specOthCryopro").val("");
		$j('#otherCryoprotectantDivDis').hide();
		}	

	  if ( ($j("#othDiluents").val() !='' || $j("#othDiluentsper").val() != '')) {
			$j("#otherDiluentsDivDis").show();
		}
		else{
		$j("#specOthDiluents").val("");
		$j('#otherDiluentsDivDis').hide();
		}	
	
});
	 function showhelpmsgField(id){
	  var img = $j("#img_srcId").html();
		msg=$j('#'+id+'_msg').html();
		msg = img+msg;
		overlib(msg,CAPTION,'Help');
		$j('#overDiv').css('z-index','99999');		
	}	
	$j(".physfindhelp").hover(function(){
	showhelpmsgField('physfind3b');
	},function(){return nd();});
	
	$j(".physfindhelptd").hover(function(){
	overlib('<s:text name='garuda.cbbprocedure.label.numberThawedtooltip'/>');
	},function(){return nd();});
	if('<s:property value="esignFlag" />'=='review' || '<s:property value="esignFlag" />'=='modal')
	{
	$j('#cbbProcedureView .cbuProcMandatory').each(function(){
		 $j(this).css("border", "1px solid #DBDAD9");
		 var fieldVal = $j(this).val();
		 fieldVal = $j.trim(fieldVal);
		 if(fieldVal==null || fieldVal=="" || fieldVal=="-1" ){
			 $j(this).css("border", "1px solid red");
			}
	  });
	  }
</script>
<div id="img_srcId" style="display: none;">
<img src="./images/fractions.png">
</div>
<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
		  <td colspan="6">	   
					<table>
					  <s:if test="#request.cbbProdList!=null">
						<s:iterator value="#request.cbbProdList[0]" var="cbbProcedureInfoPojo">
							<tr>
								<td>
								<div class="portlet" id="cbbProcedureViewparent">
								<div id="cbbProcedureView"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div 
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><!--<span
									class="ui-icon ui-icon-minusthick"></span><span
									class="ui-icon ui-icon-close"></span>--><s:text
									name="garuda.cbbprocedures.label.processingprocedure"></s:text>:<b><u><s:property
									value="cbbProcedures.procName"></s:property></u></b></div>
								<div class=portlet-content>
								<div>							
								<div id="<s:property value="esignFlag" /><s:property value="cbbProcedures.pkProcId" /><s:property value="fkProcMethId" />content"
									onclick="toggleDiv('<s:property value="esignFlag" /><s:property value="cbbProcedures.pkProcId" /><s:property value="fkProcMethId" />');"
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-triangle-1-s"></span> <s:text
									name="garuda.cbbprocedures.label.cbbprocessingprocedures"></s:text>
								</div>
								<div
									id="<s:property value="esignFlag" /><s:property value="cbbProcedures.pkProcId" /><s:property value="fkProcMethId" />">
		
								<table>
									<tr>
								   <tr>
									   <td width="20%" align="left" onmouseover="return overlib('<strong><s:text name="garuda.cbbprocedures.label.procnametooltip"></s:text></strong>',CAPTION,'<s:property value="cbbProcedures.procName"/>');" 
								         onmouseout="return nd();">
								       <s:text name="garuda.cbbprocedures.label.procname"></s:text>:</td>
										<td width="20%"><s:textfield name="cbbProcedures.procName" cssClass="cbuProcMandatory" disabled="true" /></td> 
										
										<!-- <td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.procno"></s:text>:</td>
										<td width="30%" align="left"><s:textfield
											name="cbbProcedures.procNo" disabled="true" /></td> --> 
										
										<!--  td width="20%" align="left"><!--<s:text
											name="garuda.cbbprocedures.label.procversion"></s:text>:--></td>
										<!-- td width="30%" align="left"><!--<s:textfield
											name="cbbProcedures.procVersion" disabled="true" />--></td-->
						<s:date name="cbbProcedures.procStartDate" id="datepickerDate" format="dd-MMM-yyyy" />
						<s:date  name="cbbProcedures.createdOn"  format="dd-MMM-yyyy" id="createdOnDatepicker"/>		
												
						<s:set name="procDate"  value="datepickerDate"/>                                  
                         <%--   <s:property value="%{checkContantDate(#datepicker9)}"/> --%>
                         <s:if test="%{checkContantDate(#createdOnDatepicker)}">  
       						 <td width="20%" align="left"><s:text name="garuda.cbuentry.label.productcode" /></td> 
       						 <td width="20%"><s:textfield  name="cbbProcedures.productCode" id="productCode" disabled="true"></s:textfield> </td>        								 
        					       </s:if>
        							<s:else>
        								<!--   <s:text name="garuda.cbbprocedures.label.procversion"></s:text>:</td>
										<td width="30%" align="left">
										    <s:textfield width="auto" name="cbbProcedures.productCode" id="productCode" disabled="true"></s:textfield>
										    <s:textfield name="cbbProcedures.procVersion" disabled="true" />
										</td>	-->
        							</s:else>		
								</tr>
										<tr>
										<!-- <td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.procno"></s:text>:</td>
										<td width="30%" align="left"><s:textfield
											name="cbbProcedures.procNo" disabled="true" /></td> --> 
										
										<!--  td width="20%" align="left"><!--<s:text
											name="garuda.cbbprocedures.label.procversion"></s:text>:--></td>
										<!-- td width="30%" align="left"><!--<s:textfield
											name="cbbProcedures.procVersion" disabled="true" />--></td-->
											<!--
										<td width="20%" align="left"><s:text name="garuda.cbuentry.label.productcode" /><s:text
											name="garuda.cbbprocedures.label.procversion"></s:text>:</td>
										<td width="30%" align="left"><s:textfield width="auto" name="cbbProcedures.productCode" id="productCode" disabled="true"></s:textfield><s:textfield
											name="cbbProcedures.procVersion" disabled="true" /></td>	-->
																				
									</tr>
									<tr>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.procstartdate"></s:text>:</td>
										<td width="30%" align="left"><s:date 
											name="cbbProcedures.procStartDate" id="datepicker22"
											format="MMM dd, yyyy"/> <s:textfield width="auto" readonly="true" onkeydown="cancelBack();" 
											name="procStartDateStr" class="datepic" id="datepicker22"
											value="%{datepicker22}" cssClass="cbuProcMandatory" disabled="true" /></td>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.proctermidate"></s:text>:</td>
										<td width="30%" align="left"><s:date
											name="cbbProcedures.procTermiDate" id="datepicker33"
											format="MMM dd, yyyy" /> <s:textfield width="auto" readonly="true" onkeydown="cancelBack();" 
											name="procTermiDateStr" class="datepic" id="datepicker33"
											value="%{datepicker33}" disabled="true" /></td>
									</tr>
								<!-- <tr>
									<td width="10%" style="padding-left: 5px;"><s:text name="garuda.cbuentry.label.CBB" /></td>
							    	 <td width="10%" style="padding-left: 5px;">
							      	 <s:select name="cbbProcedures.fkSite" listKey="siteId" listValue="siteName" list="sites" disabled = "true"></s:select> 
							     	</td>
								</tr> -->
									
								</table>
								</div>
								</div>
								<div>
								<div id="<s:property value="esignFlag" /><s:property value="cbbProcedures.pkProcId" /><s:property value='fkBagType'/>content"
									onclick="toggleDiv('<s:property value="esignFlag" /><s:property value="cbbProcedures.pkProcId" /><s:property value="fkBagType"/>')"
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-triangle-1-s"></span> <s:text
									name="garuda.cbbprocedures.label.productmodification"></s:text></div>
								<div id="<s:property value="esignFlag" /><s:property value="cbbProcedures.pkProcId" /><s:property value='fkBagType'/>">
								<table>
									<tr>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.processing"></s:text>:</td>
										<td width="30%" align="left"><s:select name="fkProcMethId"
											id="fkProcMethId" cssClass="cbuProcMandatory"
											list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING]"
											listKey="pkCodeId" listValue="description" headerKey=""
											headerValue="Select" disabled="true" /></td>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.ifautomated"></s:text>:</td>
										<td width="30%" align="left"><s:select style="width:auto;" name="fkIfAutomated"
											id="fkIfAutomated"
											list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IF_AUTOMATED]"
											listKey="pkCodeId" listValue="description" headerKey=""
											headerValue="Select" disabled="true" /></td>
									</tr>
									<s:if test="fkIfAutomated==pkIfAutomatedOther">
										<tr>
										<td width="20%" align="left"></td>
										<td width="30%" align="left"></td>

										<td width="20%" align="left"><s:text
													name="garuda.cbbprocedures.label.specifyoth"></s:text>:</td>
										<td width="30%" align="left"><s:textfield width="auto"
													name="otherProcessing" cssClass="cbuProcMandatory" disabled="true" /></td>
									</tr>
									</s:if>
									<tr>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.productmodification"></s:text>:</td>
										<td width="30%" align="left"><s:select cssClass="cbuProcMandatory" style="width:auto;"
											name="fkProductModification" id="fkProductModification"
											list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PRODUCT_MODI]"
											listKey="pkCodeId" listValue="description" headerKey=""
											headerValue="Select" disabled="true" /></td>
										<td width="20%" align="left">
										</td>
										<td width="30%" align="left"></td>
									</tr>
									<s:if test="fkProductModification==pkProdModiOther">
									<tr>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.specifyoth"></s:text>:</td>
										<td width="30%" align="left"><s:textfield width="auto"
											name="otherProdModi" disabled="true" cssClass="cbuProcMandatory" /></td>
										<td width="20%" align="left"></td>
										<td width="30%" align="left"></td>
									</tr>
									</s:if>
								</table>
								</div>
								</div>
								<div>
								<div id="<s:property value="esignFlag" /><s:property value="cbbProcedures.pkProcId" /><s:property value="fkStorMethod" />content"
									onclick="toggleDiv('<s:property value="esignFlag" /><s:property value="cbbProcedures.pkProcId" /><s:property value="fkStorMethod" />')"
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-triangle-1-s"></span> <s:text
									name="garuda.cbbprocedures.label.storageconditions"></s:text></div>
								<div id="<s:property value="esignFlag" /><s:property value="cbbProcedures.pkProcId" /><s:property value='fkStorMethod'/>">
								<table>
									<tr>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.stormethod"></s:text>:</td>
										<td width="30%" align="left"><s:select style="width:auto;" name="fkStorMethod" 
											list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@STORAGE_METHOD]"
											listKey="pkCodeId" listValue="description" headerKey=""
											headerValue="Select" disabled="true" /></td>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.freezmanufac"></s:text>:</td>
										<td width="30%" align="left"><s:select style="width:auto;"
											name="fkFreezManufac" id="fkFreezManufac" 
											list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FREEZER_MANUFACTURER]"
											listKey="pkCodeId" listValue="description" headerKey=""
											headerValue="Select" disabled="true" /></td>
									</tr>
									<s:if test="fkFreezManufac==pkFreezManuOther">
									<tr>
										<td width="20%" align="left"></td>
										<td width="30%" align="left"></td>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.freezmanufac"></s:text> - <s:text
											name="garuda.cbbprocedures.label.specifyoth"></s:text>:</td>
										<td width="30%" align="left"><s:textfield width="auto"
											name="otherFreezManufac" cssClass="cbuProcMandatory" disabled="true" /></td>
									</tr>
									</s:if>
									<tr>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.frozenin"></s:text>:</td>
										<td width="30%" align="left"><s:select name="fkFrozenIn"
											id="fkFrozenIn"
											list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FROZEN_IN]"
											listKey="pkCodeId" listValue="description" headerKey=""
											headerValue="Select" cssClass="cbuProcMandatory" disabled="true" /></td>
										<td width="20%" align="left"></td>
										<td width="30%" align="left"></td>
									</tr>
									
									<s:if test="fkFrozenIn==pkFrozenInBag">
									<tr>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.noofbags"></s:text>:</td>
										<td width="30%" align="left"><s:select 
											name="noOfBags" id="fkNoOfBags" cssClass="cbuProcMandatory"
											list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NUM_OF_BAGS]"
											listKey="pkCodeId" listValue="description" headerKey=""
											headerValue="Select"  disabled="true"/></td>
										<td width="20%" align="left"></td>
										<td width="30%" align="left"></td>
								 	</tr>
									</s:if>
									<s:if test="fkFrozenIn==pkFrozenInOther">
									<tr>
										<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
										</s:text>
										</td>
										<td width="30%" align="left"><s:textfield width="auto" name="otherFrozenCont" cssClass="cbuProcMandatory" disabled="true"></s:textfield>
										</td>
									</tr>
									</s:if>
									<s:if test="noOfBags==pkBag1Type">
										<tr>
										<td align="left" colspan="4">
										<table>
										<tr>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.bagtype"></s:text>:</td>
										<td width="30%" align="left"><s:select 
											name="fkBag1Type" id="fkBag1TypeView" cssClass="cbuProcMandatory"
											list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BAG_TYPE]"
											listKey="pkCodeId" listValue="description" headerKey=""
											headerValue="Select"  disabled="true"/></td>
										</tr>
										<tr>
											<td width="20%" align="left"><s:text
												name="garuda.cbbprocedures.label.cryobagmanufac"></s:text>:</td>
											<td width="30%" align="left"><s:textfield width="auto"
												name="cryoBagManufac"
												id="cryoBagManufacView" cssClass="cbuProcMandatory" maxlength="50" disabled="true"/></td>
											<td width="20%" align="left"><s:text
												name="garuda.cbbprocedures.label.maxvalue"></s:text>(<s:text
												name="garuda.cbbprocedures.label.ml"></s:text>):</td>
											<td width="30%" align="left"><s:textfield width="auto"
												name="maxValue" id="maxValueView" cssClass="cbuProcMandatory"
												disabled="true"/></td>
										</tr>
										<s:if test="fkBag1Type==pkOtherBagType">
										<tr>
										<td width="50%" align="left" colspan="2">
												<table>
													<tr>
														<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
														</s:text>
														</td>
														<td width="30%" align="left"><s:textfield width="auto" name="otherBagType1" cssClass="cbuProcMandatory" id="otherBagType1View" disabled="true"></s:textfield>
														</td>
													</tr>
												</table>
										</td>
										</tr>
										</s:if>
										
										</table>
										</td>
										</tr>
									</s:if>
									<s:if test="noOfBags==pkBag2Type">
										<tr>
										<td align="left" colspan="4">
										<table>
										<tr>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.bag1type"></s:text>:</td>
										<td width="30%" align="left"><s:select
											name="fkBag1Type" id="fkBag1TypeView" cssClass="cbuProcMandatory"
											list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BAG_TYPE]"
											listKey="pkCodeId" listValue="description" headerKey=""
											headerValue="Select"  disabled="true"/></td>
										</tr>
										<tr>
											<td width="20%" align="left"><s:text
												name="garuda.cbbprocedures.label.cryobagmanufac.bagtype1"></s:text>:</td>
											<td width="30%" align="left"><s:textfield
												name="cryoBagManufac" cssClass="cbuProcMandatory" width="auto"
												id="cryoBagManufacView" maxlength="50" disabled="true"/></td>
											<td width="20%" align="left"><s:text
												name="garuda.cbbprocedures.label.maxvalue.bagtype1"></s:text>(<s:text
												name="garuda.cbbprocedures.label.ml"></s:text>):</td>
											<td width="30%" align="left"><s:textfield width="auto" cssClass="cbuProcMandatory"
												name="maxValue" id="maxValueView"
												disabled="true"/></td>
										</tr>
										<s:if test="fkBag1Type==pkOtherBagType">
										<tr>
										<td width="50%" align="left" colspan="2">
												<table>
													<tr>
														<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
														</s:text>
														</td>
														<td width="30%" align="left"><s:textfield width="auto" name="otherBagType1" cssClass="cbuProcMandatory" id="otherBagType1View" disabled="true"></s:textfield>
														</td>
													</tr>
												</table>
										</td>
										</tr>
										</s:if>
										
										</table>
										</td>
										</tr>
										<tr>
										<td align="left" colspan="4">
										<table>
										<tr>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.bag2type"></s:text>:</td>
										<td width="30%" align="left"><s:select
											name="fkBag2Type" id="fkBag2TypeView" cssClass="cbuProcMandatory"
											list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BAG_TYPE]"
											listKey="pkCodeId" listValue="description" headerKey=""
											headerValue="Select" disabled="true"/></td>
										</tr>
										<tr>
											<td width="20%" align="left"><s:text
												name="garuda.cbbprocedures.label.cryobagmanufac.bagtype2"></s:text>:</td>
											<td width="30%" align="left"><s:textfield width="auto" cssClass="cbuProcMandatory"
												name="cryoBagManufac2"
												id="cryoBag2ManufacView" disabled="true"/></td>
											<td width="20%" align="left"><s:text
												name="garuda.cbbprocedures.label.maxvalue.bagtype2"></s:text>(<s:text
												name="garuda.cbbprocedures.label.ml"></s:text>):</td>
											<td width="30%" align="left"><s:textfield cssClass="cbuProcMandatory" width="auto"
												name="maxValue2" disabled="true"/></td>
										</tr>
										<s:if test="fkBag2Type==pkOtherBagType">
										<tr>
										<td width="50%" align="left" colspan="2">
												<table>
													<tr>
														<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
														</s:text>
														</td>
														<td width="30%" align="left"><s:textfield width="auto" name="otherBagType2" cssClass="cbuProcMandatory" id="otherBagType2View" disabled="true"></s:textfield>
														</td>
													</tr>
												</table>
										</td>
										</tr>
										</s:if>
										</table>
										</td>
										</tr>
									</s:if>
									<s:if test="noOfBags==pkNoOfBagsOthers">
									<tr>
										<td width="50%" align="left" colspan="2">
												<table>
													<tr>
														<td width="20%" align="left"><s:text name="garuda.cbbprocedures.label.specifyother">
														</s:text>
														</td>
														<td width="30%" align="left"><s:textfield width="auto" name="otherBagType" cssClass="cbuProcMandatory" id="otherBagTypeView" disabled="true"></s:textfield>
														</td>
													</tr>
												</table>
										</td>
									</tr>
									</s:if>
									
									<tr>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.stortemp"></s:text>:</td>
											
										<td width="30%" align="left">
										<select name="fkStorTemp" cssClass="cbuProcMandatory" disabled="true">
										<option value="">Select</option>
										<s:iterator value = "#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@STORAGE_TEMPERATURE]">
											<s:if test="%{pkCodeId==fkStorTemp}">
												<option value ='<s:property value='%{pkCodeId}'/>' selected="selected"><s:property value="%{description}" escapeHtml="false"/></option>
											</s:if><s:else>
												<option value ='<s:property value='%{pkCodeId}'/>'> <s:property value="%{description}" escapeHtml="false"/></option>
											</s:else>
										</s:iterator>
										</select> </td>	
											
										<td width="20%" align="left"></td>
										<td width="30%" align="left"></td>
									</tr>
									<tr>
										<td width="20%" align="left"><s:text
											name="garuda.cbbprocedures.label.controlledratefreezing"></s:text>:</td>
										<td width="30%" align="left"><s:select
											name="fkContrlRateFreezing" 
											list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CONTR_RATE_FREEZ]"
											listKey="pkCodeId" listValue="description" headerKey=""
											headerValue="Select" disabled="true" /></td>
										<td width="20%" align="left" class="physfindhelptd">
											<s:text	name="garuda.cbbprocedures.label.noofindifrac"></s:text>:<img id="physfindhelp1" class="physfindhelp" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();"></td>											
										<td width="30%" align="left"><s:textfield cssClass="cbuProcMandatory"
											name="noOfIndiFrac" disabled="true" /></td>
									</tr>
								</table>
								</div>
								</div>
		
								<div>
								<div id="<s:property value="esignFlag" /><s:property value="cbbProcedures.pkProcId" /><s:property value="fkContrlRateFreezing" />content"
									onclick="toggleDiv('<s:property value="esignFlag" /><s:property value="cbbProcedures.pkProcId" /><s:property value="fkContrlRateFreezing" />')"
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-triangle-1-s"></span> <s:text
									name="garuda.cbbprocedures.label.frozenproductcomposition"></s:text>
								</div>
								<div id="<s:property value="esignFlag" /><s:property value="cbbProcedures.pkProcId" /><s:property value='fkContrlRateFreezing'/>">
								<fieldset><legend> <s:text
									name="garuda.cbbprocedures.label.additivesbeforevolumereductions"></s:text>
								</legend>
								<table>
								<tr>
									
									<td width="20%">
									</td>
									<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
									</td>
									<td width="15" align="center"> <s:text name="garuda.cbbprocedures.label.percentage"></s:text>
									</td>
									<td width="20%">
									</td>
									<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
									</td>
									<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
									</td>
											
								</tr>
								<tr>
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.thouunitpermlheparin"></s:text>:
									</td>
									<td width="15%"><s:textfield
										name="thouUnitPerMlHeparin" disabled="true">
										</s:textfield></td>
									<td width="15%"><s:textfield
										name="thouUnitPerMlHeparinper" disabled="true">
										</s:textfield></td>
									
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.cpda"></s:text>:</td>
									<td width="15%"><s:textfield
										name="cpda" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="cpdaper" disabled="true"></s:textfield></td>
								</tr>
								<tr>
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.fiveunitpermlheparin"></s:text>:
									</td>
									<td width="15%"><s:textfield
										name="fiveUnitPerMlHeparin" disabled="true">
										</s:textfield></td>
									<td width="15%"><s:textfield
										name="fiveUnitPerMlHeparinper" disabled="true">
										</s:textfield></td>
									
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.cpd"></s:text>:</td>
									<td width="15%"><s:textfield name="cpd" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield name="cpdper" disabled="true"></s:textfield></td>
								
								</tr>
								<tr>
								<td width="20%"><s:text
										name="garuda.cbbprocedures.label.tenunitpermlheparin"></s:text>:
									</td>
									<td width="15%"><s:textfield
										name="tenUnitPerMlHeparin" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="tenUnitPerMlHeparinper" disabled="true"></s:textfield></td>	
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.acd"></s:text>:</td>
									<td width="15%"><s:textfield name="acd" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield name="acdper" disabled="true"></s:textfield></td>
								</tr>
								<tr>
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.sixperhydroxyethylstarch"></s:text>:
									</td>
									<td width="15%"><s:textfield
										name="sixPerHydroxyethylStarch" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="sixPerHydroxyethylStarchper" disabled="true"></s:textfield></td>
									
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.otheranticoagulant"></s:text>:</td>
									<td width="15%"><s:textfield
										name="otherAnticoagulant"
										disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="otherAnticoagulantper"
										disabled="true"></s:textfield></td>
								</tr>
								<s:if test="(otherAnticoagulant != null)  || (otherAnticoagulantper != null)">
								<tr id="otherAnticoagulantDivAdd">
											<td width="20%" align="left"><s:text
												name="garuda.cbbprocedures.label.specifyothanti"></s:text>:</td>
											<td width="30%" align="left" colspan="2"><s:textfield
												name="specifyOthAnti"
												disabled="true"/></td>
											<td width="20%" align="left"></td>
											<td width="30%" align="left" colspan="2"></td>
								</tr>
								</s:if>
							</table>
								</fieldset>
								<fieldset><legend> <s:text
									name="garuda.cbbprocedures.label.cryoprotectantsandotheradditives"></s:text>:
								</legend>
								<table>
								<tr>
									
									<td width="20%">
									</td>
									<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
									</td>
									<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
									</td>
									<td width="20%">
									</td>
									<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
									</td>
									<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
									</td>
											
								</tr>
								<tr>
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.hunperdmso"></s:text>:</td>
									<td width="15%"><s:textfield
										name="hunPerDmso" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="hunPerDmsoper" disabled="true"></s:textfield></td>
									
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.twenfivehumalbu"></s:text>:</td>
									<td width="15%"><s:textfield
										name="twenFiveHumAlbu" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="twenFiveHumAlbuper" disabled="true"></s:textfield></td>
							
								</tr>
								<tr>
									
											
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.hunperglycerol"></s:text>:</td>
									<td width="15%"><s:textfield
										name="hunPerGlycerol" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="hunPerGlycerolper" disabled="true"></s:textfield></td>
									
										
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.plasmalyte"></s:text>:</td>
									<td width="15%"><s:textfield
										name="plasmalyte" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="plasmalyteper" disabled="true"></s:textfield></td>
								
								</tr>
								<tr>
									
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.tenperdextran40"></s:text>:</td>
									<td width="15%"><s:textfield
										name="tenPerDextran_40" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="tenPerDextran_40per" disabled="true"></s:textfield></td>
										
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.othcryoprotectant"></s:text>:</td>
									<td width="15%"><s:textfield
										name="othCryoprotectant"
										disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="othCryoprotectantper"
										disabled="true"></s:textfield></td>	
								</tr>
								<tr>
									
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.fiveperhumanalbu"></s:text>:</td>
									<td width="15%"><s:textfield
										name="fivePerHumanAlbu" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="fivePerHumanAlbuper" disabled="true"></s:textfield></td>
									<td width="20%"></td>
									<td width="30%" colspan="2"></td>
									
								</tr>
								<s:if test="(othCryoprotectant != null)  || (othCryoprotectantper != null)">
								<tr id="otherCryoprotectantDivDis">
									<td width="20%" align="left"><s:text
										name="garuda.cbbprocedures.label.specothcryopro"></s:text>:</td>
									<td width="30%" align="left" colspan="2"><s:textfield
										name="specOthCryopro"
										disabled="true"/></td>
									
									<td width="20%" align="left"></td>
									<td width="30%" align="left" colspan="2"></td>						
								</tr>
								</s:if>
							</table>
								</fieldset>
								<fieldset><legend> <s:text
									name="garuda.cbbprocedures.label.diluents"></s:text> </legend>
								<table width="100%">
								<tr>
									
									<td width="20%">
									</td>
									<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.millilitres"></s:text>
									</td>
									<td width="15" align="center"><s:text name="garuda.cbbprocedures.label.percentage"></s:text>
									</td>
									<td width="20%">
									</td>
									<td width="15" align="center">
									</td>
									<td width="15" align="center">
									</td>
											
								</tr>
								<tr>
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.fiveperdextrose"></s:text>:</td>
									<td width="15%"><s:textfield
										name="fivePerDextrose" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="fivePerDextroseper" disabled="true"></s:textfield></td>
									<td width="20%"></td>
									<td width="30%"colspan="2"></td>
								</tr>
								<tr>
								<td width="20%"><s:text
										name="garuda.cbbprocedures.label.pointninepernacl"></s:text>:</td>
									<td width="15%"><s:textfield
										name="pointNinePerNacl" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="pointNinePerNaclper" disabled="true"></s:textfield></td>
									<td width="20%"></td>
									<td width="30%"colspan="2"></td>	
								</tr>
								
								<tr>
									<td width="20%"><s:text
										name="garuda.cbbprocedures.label.otherdiluents"></s:text>:</td>
									<td width="15%"><s:textfield
										name="othDiluents" disabled="true"></s:textfield></td>
									<td width="15%"><s:textfield
										name="othDiluentsper" disabled="true"></s:textfield></td>
									<td width="20%"></td>
									<td width="30%"colspan="2"></td>
								</tr>
								
								<s:if test="(othDiluents != null)  || (othDiluentsper != null)">
								<tr id="otherDiluentsDivDis">
											<td width="20%" align="left"><s:text
												name="garuda.cbbprocedures.label.specothdiluents"></s:text>:</td>
											<td width="30%" align="left" colspan="2"><s:textfield
												name="specOthDiluents" disabled="true" /></td>
											<td width="20%" align="left"></td>
											<td width="30%" align="left" colspan="2"></td>
											
											
								</tr>
								</s:if>
							</table>
								</fieldset>
								</div>
								</div>										
								</div>
								</div>
								</div>
								</td>
							</tr>
						</s:iterator>
					  </s:if>
					</table>
		  </td>
		</tr>
	</table>
<script>
$j(function(){
	$j('#procStartDt').val($j('#datepicker22').val());
	$j('#procTermiDt').val($j('#datepicker33').val());	
});
</script>