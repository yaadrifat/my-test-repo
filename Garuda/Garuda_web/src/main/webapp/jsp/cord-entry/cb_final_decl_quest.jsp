<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", 0); 
String val = request.getParameter("readonly");
request.setAttribute("readonly",val);
%>
<script>
$j(document).ready(function(){
	if('<s:property value="esignFlag"/>'== '<s:text name="review"/>' && ('<s:property value="cdrCbuPojo.fkCordCbuStatus"/>'=='<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH)"/>' || '<s:property value="cdrCbuPojo.fkCordCbuStatus"/>'=='<s:property value="getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN)"/>')){
		disableFormFlds("finalDeclReadOnly");
	}
	if(('<s:property value="esignFlag"/>'=='reviewCriView' || '<s:property value="esignFlag"/>'=='review')){
		disableFormFlds("finalDeclReadOnly");
	}
});

 function resetValue(arr){
	 for(var j=0; j<arr.length;j++){
		 $j("#"+arr[j]+ " input[type=radio]").each(function(){
			 $j(this).attr('checked',false);
		 });
		 $j("#"+arr[j]).find("textarea").each(function(){
			 $j(this).val("");           
		 });		
	 }
 }

 function limitText(txtField, maxLength) {
		if (txtField.value.length > maxLength) {
			txtField.value = txtField.value.substring(0, maxLength)
		} 
	}

  function showQuestionWithDetail(val,detailid,quesid,quesidb){
	  var arr = new Array();
	  arr[0] = detailid;
	  arr[1] = quesid;
	  arr[2] = quesidb;
	  if(val=='false'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
         $j("#"+quesidb).show();
     }else if(val=='true'){
    	 $j("#"+detailid).hide();
         $j("#"+quesid).hide();
         $j("#"+quesidb).hide();
         resetValue(arr);
     }
  }

  function showYesQuestionWithDetail(val,detailid,quesid){
	  var arr = new Array();
	  arr[0] = detailid;
	  arr[1] = quesid;
	  if(val=='true'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
     }else if(val=='false'){
    	 $j("#"+detailid).hide();
         $j("#"+quesid).hide();
         resetValue(arr);
     }
  }

  function showMultipeQuestionWithDetail(val,detailid,quesid,quesid1,quesid2,quesid3){
	  var arr = new Array();
	  arr[0] = detailid;
	  arr[1] = quesid;
	  arr[2] = quesid1;
	  arr[3] = quesid2;
	  arr[4] = quesid3;
	  if(val=='false'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
         $j("#"+quesid1).show();
         $j("#"+quesid2).show();
         $j("#"+quesid3).show();
     }else if(val=='true'){
         $j("#"+detailid).hide();
         $j("#"+quesid).hide();
         $j("#"+quesid1).hide();
         $j("#"+quesid2).hide();
         $j("#"+quesid3).hide();
         resetValue(arr);
     }
  }

  function showMultipeYesQuestionWithDetail(val,detailid,quesid,quesid1,quesid3,quesid4,quesid5,quesid6,quesid7,quesid8,quesid9){
	  var arr = new Array();
	  arr[0] = detailid;
	  arr[1] = quesid;
	  arr[2] = quesid1;
	  arr[3] = quesid3;
	  arr[4] = quesid4;
	  arr[5] = quesid5;
	  arr[6] = quesid6;
	  arr[7] = quesid7;
	  arr[8] = quesid8;
	  arr[9] = quesid9;	  
	  if(val=='true'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
         $j("#"+quesid1).show();
         $j("#"+quesid3).show();
         $j("#"+quesid4).show();
         $j("#"+quesid5).show();
         $j("#"+quesid6).show();
         $j("#"+quesid7).show();
         $j("#"+quesid8).show();
         $j("#"+quesid9).show(); 
     }else if(val=='false'){
         $j("#"+detailid).hide();
         $j("#"+quesid).hide();
         $j("#"+quesid1).hide();
         $j("#"+quesid3).hide();
         $j("#"+quesid4).hide();
         $j("#"+quesid5).hide();
         $j("#"+quesid6).hide();
         $j("#"+quesid7).hide();
         $j("#"+quesid8).hide();
         $j("#"+quesid9).hide();
         resetValue(arr); 
     }
  }

  function showQuestion(val,quesid){
	  var arr = new Array();
	  arr[1] = quesid;	  
	  if(val=='false'){
	         $j("#"+quesid).show();
	  }else if(val=='true'){
	         $j("#"+quesid).hide();
	         resetValue(arr);
	  }
  }

  function showUserMessage(val){
	  if(val=='true'){
		  var str = "<s:text name="garuda.cbu.quest.exitAlert"/>";
		  jConfirm(str, '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
			    if(r==true){
			    	//submitform('cdrentryform'); 
			    	//window.location='cdrcbuView?cdrCbuPojo.cordID='+$j("#ncordId").val()+'&orderId='+$j("#norderId").val()+'&orderType='+$j("#norderType").val()+'&pkcordId='+$j("#npkcordId").val();
			    	submitpost('loadClinicalDataFromPF',{'cdrCbuPojo.cordID':$j("#ncordId").val()});
			    	
				}			    
		 });
	  }
  }

  function showMultipleQuestion(val,quesid,quesid1){
	  var arr = new Array();
	  arr[1] = quesid;
	  arr[2] = quesid1;	  
	  if(val=='false'){
	         $j("#"+quesid).show();
	         $j("#"+quesid1).show();
	  }else if(val=='false'){
	         $j("#"+quesid).hide();
	         $j("#"+quesid1).hide();
	         resetValue(arr);
	  }
  }

  function showDeatil(val,detailid){
	  var arr = new Array();
	  arr[0] = detailid;	  
	  if(val=='false'){
	         $j("#"+detailid).show();
	  }else if(val=='true'){
	         $j("#"+detailid).hide();
	         resetValue(arr);
	  }
  }

  function showYesDeatil(val,detailid){
	  var arr = new Array();
	  arr[0] = detailid;	  
	  if(val=='true'){
	         $j("#"+detailid).show();
	  }else if(val=='false'){
	         $j("#"+detailid).hide();
	         resetValue(arr);
	  }
  }

  function showhelpmsg(id){
		msg=$j('#'+id+'_msg').html();
		overlib(msg,CAPTION,'Help');
		$j('#overDiv').css('z-index','99999');
	}

	function showMessage(val){
		if(val=='false'){
			if(($j("#srFkCordCbuEligible").val()==$j("#srIncompleteReasonId").val()) && checkEligDates('collDate','defDatVal')){
				alert("<s:text name="garuda.cbu.quest.incompleteAlert"/>");
			}
		}
	}
</script>
<input type="hidden" name="srFkCordCbuEligible" id="srFkCordCbuEligible" value="<s:property value="cdrCbuPojo.fkCordCbuEligible"></s:property>" />
<input type="hidden" name="srIncompleteReasonId" id="srIncompleteReasonId" value="<s:property value="cdrCbuPojo.incompleteReasonId"></s:property>" />
<s:date name="cdrCbuPojo.specimen.specCollDate" id="dtPick" format="yyyy-MM-dd" />	
<s:hidden name="collDate" id="collDate" value="%{dtPick}"></s:hidden>
<s:hidden name="defDatVal" id="defDatVal" value="2005-05-25"></s:hidden>
<input type="hidden" name="ncordId" value="<s:property value="cdrCbuPojo.cordID"/>" id="ncordId" />
<input type="hidden" name="npkcordId" value="<s:property value="cdrCbuPojo.cordID"/>" id="npkcordId" />
<input type="hidden" name="norderId" value="<s:property value="#request.orderId"/>" id="norderId"/>
<input type="hidden" name="norderType" value="<s:property value="#request.orderType"/>"  id="norderType" />

<div id="finalDeclReadOnly">
   <table width="100%" cellpadding="0" cellspacing="0" class="displaycdr">
       <tr>
           <td valign="top" width="35%">
               <s:text name="garuda.questionnaire.level.mrq" /> :
           </td>
           <td style="border: 1px solid #EDECEB;" width="65%" bgcolor="#7DCFD5">
              <table width="100%" cellpadding="0" cellspacing="0" >
                <tr valign="top">
                    <td width="75%"><s:text name="garuda.questionnaire.level.mrqques1" />:</td>
                    <td nowrap="nowrap" width="25%" style="padding:0px"><s:radio name="declPojo.mrqQues1" id="mrqques1" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.mrqQues1}','elgFcrChanges');showQuestionWithDetail(this.value,'mrqques1adddetail','mrqQues1atr','mrqQues1btr')"></s:radio></td>
                </tr>
                <tr id="mrqques1adddetail" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/>:<span style="color: red;">*</span></td>
                	<td width="25%">
                        <textarea rows="5" cols="20" onkeyup="limitText(this,500);" name="declPojo.mrqQues1AddDetail" onchange="isChangeDone(this,'<s:property value="%{declPojo.mrqQues1AddDetail}" escapeJavaScript="true" />','elgFcrChanges');" ><s:property value="%{declPojo.mrqQues1AddDetail}"/></textarea>
                    </td>
                </tr>
                <tr id="mrqQues1atr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.mrqques2" />:
                    	<img id="mrq1a" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
                    </td>
                    <td nowrap="nowrap" width="25%" style="padding:0px;" >
                    	<s:radio name="declPojo.mrqQues1a" id="mrqQues1a" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.mrqQues1a}','elgFcrChanges');showQuestion(this.value,'mrqQues1btr');showUserMessage(this.value)" ></s:radio>
                    </td>                    
                </tr>
                <tr id="mrqQues1btr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 45px;"><s:text name="garuda.questionnaire.level.mrqques3" />:
                    	<img id="mrq1b" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
                    </td>
                    <td nowrap="nowrap" width="25%" style="padding:0px">
                    	<s:radio name="declPojo.mrqQues1b" id="mrqQues1b" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.mrqQues1b}','elgFcrChanges');" ></s:radio>
                    </td>                    
                </tr>
                <tr valign="top">
                   	<td width="75%"><s:text name="garuda.questionnaire.level.mrqques4" />:
                  		<img id="mrq2" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);"></td>
                   	<td nowrap="nowrap" width="25%" style="padding:0px">
                   		<s:radio name="declPojo.mrqQues2" id="mrqQues2" list="#{'true':'Yes','false':'NO'}" onclick="showYesDeatil(this.value,'mrqQues2adddetail');isChangeDone(this,'%{declPojo.mrqQues2}','elgFcrChanges');"></s:radio>
                   </td>
                </tr>
                <tr id="mrqQues2adddetail" style="display: none;"><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/>:<span style="color: red;">*</span></td>
                 <td width="25%">
                        <textarea rows="5" cols="20" name="declPojo.mrqQues2AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declPojo.mrqQues2AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declPojo.mrqQues2AddDetail}"/></textarea>
                   </td>
                </tr>
              </table>
       		</td>
       </tr>
       <tr>
       		<td colspan="2" height="5%"></td>
       </tr>
       <tr>
           <td valign="top" width="35%">
               <s:text name="garuda.questionnaire.level.physfind" /> :
           </td>
           <td style="border: 1px solid #EDECEB;" bgcolor="#7DCFD5" width="65%">
              <table width="100%" cellpadding="0" cellspacing="0" >
                <tr valign="top">
                   <td width="75%"><s:text name="garuda.questionnaire.level.physfind0" />:</td>
                   <td nowrap="nowrap" width="25%" style="padding:0px"><s:radio name="declPojo.phyFindQues3" id="phyFindQues3" list="#{'true':'Yes','false':'NO'}" onclick="showMultipeQuestionWithDetail(this.value,'phyFindQues3AddDetail','phyFindQues3atr','phyFindQues3btr','phyFindQues3ctr','phyFindQues3dtr');showMessage(this.value);isChangeDone(this,'%{declPojo.phyFindQues3}','elgFcrChanges');"></s:radio></td>
                </tr>
                <tr id="phyFindQues3AddDetail" style="display: none;">
                	<td  width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/>:<span style="color: red;">*</span></td>
                	<td width="25%">
                        <textarea rows="5" cols="20" name="declPojo.phyFindQues3AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declPojo.phyFindQues3AddDetail}" escapeJavaScript="true" />','elgFcrChanges');" ><s:property value="%{declPojo.phyFindQues3AddDetail}"/></textarea>
                   </td>
                </tr>
                <tr id="phyFindQues3atr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.physfind1" />:
                   		<img id="physfind3a" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
                    </td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px">
                    	<s:radio name="declPojo.phyFindQues3a" id="phyFindQues3a" list="#{'true':'Yes','false':'NO'}" onclick="showQuestion(this.value,'phyFindQues3btr');isChangeDone(this,'%{declPojo.phyFindQues3a}','elgFcrChanges');"></s:radio>
                    </td>                    
                </tr>
                <tr id="phyFindQues3btr" style="display: none;padding-left: 45px;" valign="top">
                    <td width="75%" style="padding-left: 45px;"><s:text name="garuda.questionnaire.level.physfind2" />:
                    	<img id="physfind3b" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
                    </td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px">
                    	<s:radio name="declPojo.phyFindQues3b" id="phyFindQues3b" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.phyFindQues3b}','elgFcrChanges');" ></s:radio>
                    </td>                    
                </tr>
                <tr id="phyFindQues3ctr" style="display: none;" valign="top">
                    <td width="75%"><s:text name="garuda.questionnaire.level.physfind3" />:
                    	<img id="physfind3c" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
                    </td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px">
                    	<s:radio name="declPojo.phyFindQues3c" id="phyFindQues3c" list="#{'true':'Yes','false':'NO'}" onclick="showQuestion(this.value,'phyFindQues3dtr');isChangeDone(this,'%{declPojo.phyFindQues3c}','elgFcrChanges');"></s:radio>
                    </td>                    
                </tr>
                <tr id="phyFindQues3dtr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.physfind4" />:
                    	<img id="physfind3d" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
                    </td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px">
                    	<s:radio name="declPojo.phyFindQues3d" id="phyFindQues3d" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.phyFindQues3d}','elgFcrChanges');" ></s:radio>
                    </td>                    
                </tr>
                <tr valign="top">
                   	<td width="75%"><s:text name="garuda.questionnaire.level.physfind5" />:
                   		<img id="physfind4" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
                   	</td>
                   	<td nowrap="nowrap" width="25%" style="padding:0px">
                   		<s:radio name="declPojo.phyFindQues4" id="phyFindQues4" list="#{'true':'Yes','false':'NO'}" onclick="showYesDeatil(this.value,'phyFindQues4AddDetail');isChangeDone(this,'%{declPojo.phyFindQues4}','elgFcrChanges');"></s:radio>
                   	</td>
                </tr>
                <tr id="phyFindQues4AddDetail" style="display: none;">
                	<td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/>:<span style="color: red;">*</span></td>
                	<td  width="25%">
                    	<textarea rows="5" cols="20" name="declPojo.phyFindQues4AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declPojo.phyFindQues4AddDetail}" escapeJavaScript="true"/>','elgFcrChanges');" ><s:property value="%{declPojo.phyFindQues4AddDetail}"/></textarea>
                   </td>
                </tr>
                <tr valign="top">
                   	<td width="75%"><s:text name="garuda.questionnaire.level.physfind6" />:
                   		<img id="physfind5" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
                   	</td>
                   	<td nowrap="nowrap" width="25%" style="padding:0px">
                   		<s:radio name="declPojo.phyFindQues5" id="phyFindQues5" list="#{'true':'Yes','false':'NO'}" 
                   				onclick="showMultipeYesQuestionWithDetail(this.value,'phyFindQues5AddDetail','phyFindQues5atr','phyFindQues5btr','phyFindQues5ctr','phyFindQues5dtr','phyFindQues5etr','phyFindQues5ftr','phyFindQues5gtr','phyFindQues5htr','phyFindQues5itr');isChangeDone(this,'%{declPojo.phyFindQues5}','elgFcrChanges');"></s:radio>
                   </td>
                </tr>
                <tr id="phyFindQues5AddDetail" style="display: none;" >
                	<td  width="75%">
                		<s:text name="garuda.questionnaire.level.additionaldetail"/>:<span style="color: red;">*</span></td>
                	<td width="25%">
                    	<textarea rows="5" cols="20" name="declPojo.phyFindQues5AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declPojo.phyFindQues5AddDetail}" escapeJavaScript="true"/>','elgFcrChanges');" ><s:property value="%{declPojo.phyFindQues5AddDetail}"/></textarea>
                   	</td>
                </tr>
                <tr id="phyFindQues5atr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.physfind7" /></td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><s:radio name="declPojo.phyFindQues5a" id="phyFindQues5a" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.phyFindQues5a}','elgFcrChanges');"></s:radio></td>                    
                </tr>
                <tr id="phyFindQues5btr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.physfind8" />:</td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><s:radio name="declPojo.phyFindQues5b" id="phyFindQues5b" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.phyFindQues5b}','elgFcrChanges');" ></s:radio></td>                    
                </tr>
                <tr id="phyFindQues5ctr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.physfind9" />:</td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><s:radio name="declPojo.phyFindQues5c" id="phyFindQues5c" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.phyFindQues5c}','elgFcrChanges');" ></s:radio></td>                    
                </tr>
                <tr id="phyFindQues5dtr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.physfind10" />:</td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><s:radio name="declPojo.phyFindQues5d" id="phyFindQues5d" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.phyFindQues5d}','elgFcrChanges');" ></s:radio></td>                    
                </tr>
                <tr id="phyFindQues5etr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.physfind11" />:</td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><s:radio name="declPojo.phyFindQues5e" id="phyFindQues5e" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.phyFindQues5e}','elgFcrChanges');" ></s:radio></td>                    
                </tr>
                <tr id="phyFindQues5ftr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.physfind12" />:</td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><s:radio name="declPojo.phyFindQues5f" id="phyFindQues5f" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.phyFindQues5f}','elgFcrChanges');" ></s:radio></td>                    
                </tr>
                <tr id="phyFindQues5gtr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.physfind13" />:</td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><s:radio name="declPojo.phyFindQues5g" id="phyFindQues5g" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.phyFindQues5g}','elgFcrChanges');" ></s:radio></td>                    
                </tr>
                <tr id="phyFindQues5htr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.physfind14" />:</td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><s:radio name="declPojo.phyFindQues5h" id="phyFindQues5h" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.phyFindQues5h}','elgFcrChanges');" ></s:radio></td>                    
                </tr>
                <tr id="phyFindQues5itr" style="display: none;" valign="top">
                    <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.physfind15" />:</td>
                    <td  nowrap="nowrap" width="25%" style="padding:0px"><s:radio name="declPojo.phyFindQues5i" id="phyFindQues5i" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.phyFindQues5i}','elgFcrChanges');" ></s:radio></td>                    
                </tr>
              </table>
           </td>
       </tr>
       <tr>
       		<td colspan="2" height="5%"></td>
       </tr>
       <tr>
           <td valign="top" width="35%">
               <s:text name="garuda.questionnaire.level.idm" /> :
           </td>
           <td style="border: 1px solid #EDECEB;" bgcolor="#7DCFD5" width="65%">
               <table width="100%" cellpadding="0" cellspacing="0" >
                   <tr valign="top">
	                   <td width="75%"><s:text name="garuda.questionnaire.level.idm1" />:</td>
	                   <td nowrap="nowrap" width="25%" style="padding:0px"><s:radio name="declPojo.idmQues6" id="idmQues6" list="#{'true':'Yes','false':'NO'}" 
	                   onclick="showQuestionWithDetail(this.value,'idmQues6AddDetail','idmQues6atr','idmQues6btr');isChangeDone(this,'%{declPojo.idmQues6}','elgFcrChanges');"></s:radio></td>
                   </tr>
                   <tr id="idmQues6AddDetail" style="display: none;"><td  width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/>:<span style="color: red;">*</span></td>
                   		<td width="25%">
	                        <textarea rows="5" cols="20" name="declPojo.idmQues6AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declPojo.idmQues6AddDetail}" escapeJavaScript="true"/>','elgFcrChanges');"><s:property value="%{declPojo.idmQues6AddDetail}"/></textarea>
	                   	</td>
                   </tr>
                   <tr id="idmQues6atr" style="display: none;" valign="top">
    	                <td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.idm2" />:
	    	                <img id="idm6a" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
            	        </td>
                	    <td  nowrap="nowrap" width="25%" style="padding:0px">
                    		<s:radio name="declPojo.idmQues6a" id="idmQues6a" list="#{'true':'Yes','false':'NO'}" onclick="showQuestion(this.value,'idmQues6btr');isChangeDone(this,'%{declPojo.idmQues6a}','elgFcrChanges');"></s:radio>
                    	</td>                    
                   </tr>
                   <tr id="idmQues6btr" style="display: none;" valign="top">
                    	<td width="75%" style="padding-left: 45px;"><s:text name="garuda.questionnaire.level.idm3" />:
                    		<img id="idm6b" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
                    	</td>
                    	<td  nowrap="nowrap" width="25%" style="padding:0px">
                    		<s:radio name="declPojo.idmQues6b" id="idmQues6b" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.idmQues6b}','elgFcrChanges');" ></s:radio>
	                    </td>                    
                   </tr>
                   <tr valign="top">
	                   	<td width="75%">
	                   		<s:text name="garuda.questionnaire.level.idm4" />:</td>
	                   	<td nowrap="nowrap" width="25%" style="padding:0px">
	                   		<s:radio name="declPojo.idmQues7" id="idmQues7" list="#{'true':'Yes','false':'NO'}" 
	                   		onclick="showQuestionWithDetail(this.value,'idmQues7AddDetail','idmQues7atr','idmQues7btr');isChangeDone(this,'%{declPojo.idmQues7}','elgFcrChanges');"></s:radio></td>
                   </tr>
                   <tr id="idmQues7AddDetail" style="display: none;" ><td width="75%"><s:text name="garuda.questionnaire.level.additionaldetail"/>:<span style="color: red;">*</span></td>
                   <td width="25%">
	                        <textarea rows="5" cols="20" name="declPojo.idmQues7AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declPojo.idmQues7AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declPojo.idmQues7AddDetail}"/></textarea>
	                   </td>
                   </tr>
                   <tr id="idmQues7atr" style="display: none;" valign="top">
                    	<td width="75%" style="padding-left: 25px;"><s:text name="garuda.questionnaire.level.idm5" />:
                    		<img id="idm7a" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
                    	</td>
	                    <td  nowrap="nowrap" width="25%" style="padding:0px">
	                    	<s:radio name="declPojo.idmQues7a" id="idmQues7a" list="#{'true':'Yes','false':'NO'}" onclick="showQuestion(this.value,'idmQues7btr');isChangeDone(this,'%{declPojo.idmQues7a}','elgFcrChanges');"></s:radio>
    	                </td>                    
	               </tr>
                   <tr id="idmQues7btr" style="display: none;" valign="top">
                    	<td width="75%" style="padding-left: 45px;">
                    		<s:text name="garuda.questionnaire.level.idm3" />:<img id="idm7b" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);"></td>
                    	<td  nowrap="nowrap" width="25%" style="padding:0px"><s:radio name="declPojo.idmQues7b" id="idmQues7b" list="#{'true':'Yes','false':'NO'}" onclick="isChangeDone(this,'%{declPojo.idmQues7b}','elgFcrChanges');" ></s:radio>                        	
                    	</td>                    
                   </tr>
                   <tr valign="top">
	                   	<td width="75%">
	                   		<s:text name="garuda.questionnaire.level.idm6" />:
	                   		<img id="idm8" height="15px" src="./images/help_24x24px.png" onmouseout="return nd();" onmouseover="return showhelpmsg(this.id);">
	                   	</td>
	                   	<td nowrap="nowrap" width="25%" style="padding:0px">
	                   		<s:radio name="declPojo.idmQues8" id="idmQues8" list="#{'true':'Yes','false':'NO'}" 
	                   			onclick="showYesDeatil(this.value,'idmQues8AddDetail');isChangeDone(this,'%{declPojo.idmQues8}','elgFcrChanges');"></s:radio>
	                   </td>
                   </tr>
                   <tr id="idmQues8AddDetail" style="display: none;">
                   		<td style="width: 75%">
                   			<s:text name="garuda.questionnaire.level.additionaldetail"/>:<span style="color: red;">*</span>
                   		</td>
                   		<td  width="25%">
	                        <textarea rows="5" cols="20" name="declPojo.idmQues8AddDetail" onkeyup="limitText(this,500);" onchange="isChangeDone(this,'<s:property value="%{declPojo.idmQues8AddDetail}" escapeJavaScript="true" />','elgFcrChanges');"><s:property value="%{declPojo.idmQues8AddDetail}" /></textarea>
	                   </td>
                   </tr>
               </table>
           </td>
       </tr>
       <tr>
       		<td colspan="2" height="5%"></td>
       </tr>
    </table>
</div>
<div id="helpmsg" style="display: none;">
	<div id=mrq1a_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incompleteInfo"/></td></tr>
   		</table>
	</div>
	<div id=mrq1b_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=mrq2_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.ineligibleMatRisk"/></td></tr>
   		</table>
	</div>
	<div id=physfind3a_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incompReleventMedRecord"/></td></tr>
   		</table>
	</div>
	<div id=physfind3b_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=physfind3c_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incompPhysicalFindings"/></td></tr>
   		</table>
	</div>
	<div id=physfind3d_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=physfind4_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.ineligibleReleventMedRecord"/></td></tr>
   		</table>
	</div>
	<div id=physfind5_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.imeligiblePhysicalFindings"/></td></tr>
   		</table>
	</div>
	<div id=idm6a_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incomInfectDiseaseMarker"/></td></tr>
   		</table>
	</div>
	<div id=idm6b_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=idm7a_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.obtainMissingInfo"/></td></tr>
   			<tr><td><s:text name="garuda.cbu.quest.incomInfectDiseaseMarker"/></td></tr>
   		</table>
	</div>
	<div id=idm7b_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.resendObtainedInfo"/></td></tr>
   		</table>
	</div>
	<div id=idm8_msg>
   		<table>
   			<tr><td><s:text name="garuda.cbu.quest.ineligibleInfectDiseaseMarker"/></td></tr>
   		</table>
	</div>
</div>

<s:if test="declPojo.mrqQues1!=null && declPojo.mrqQues1==false">
    <script>
          showQuestionWithDetail('false','mrqques1adddetail','mrqQues1atr');
    </script>
</s:if>
<s:if test="declPojo.mrqQues1a!=null && declPojo.mrqQues1a==false">
    <script>
          showQuestion('false','mrqQues1btr');
    </script>
</s:if>
<s:if test="declPojo.mrqQues2!=null && declPojo.mrqQues2==true">
    <script>
         showYesDeatil('true','mrqQues2adddetail');
    </script>
</s:if>
<s:if test="declPojo.phyFindQues3!=null && declPojo.phyFindQues3==false">
    <script>
        showMultipeQuestionWithDetail('false','phyFindQues3AddDetail','phyFindQues3atr','phyFindQues3ctr');
    </script>
</s:if>
<s:if test="declPojo.phyFindQues3a!=null && declPojo.phyFindQues3a==false">
    <script>
        showQuestion('false','phyFindQues3btr');
    </script>
</s:if>
<s:if test="declPojo.phyFindQues3c!=null && declPojo.phyFindQues3c==false">
    <script>
        showQuestion('false','phyFindQues3dtr');
    </script>
</s:if>
<s:if test="declPojo.phyFindQues4!=null && declPojo.phyFindQues4==true">
    <script>
        showYesDeatil('true','phyFindQues4AddDetail');
    </script>
</s:if>
<s:if test="declPojo.phyFindQues5!=null && declPojo.phyFindQues5==true">
    <script>
        showMultipeYesQuestionWithDetail('true','phyFindQues5AddDetail','phyFindQues5atr','phyFindQues5btr','phyFindQues5ctr','phyFindQues5dtr','phyFindQues5etr','phyFindQues5ftr','phyFindQues5gtr','phyFindQues5htr','phyFindQues5itr');
    </script>
</s:if>
<s:if test="declPojo.idmQues6!=null && declPojo.idmQues6==false">
    <script>
        showQuestionWithDetail('false','idmQues6AddDetail','idmQues6atr');
    </script>
</s:if>
<s:if test="declPojo.idmQues6a!=null && declPojo.idmQues6a==false">
    <script>
       showQuestion('false','idmQues6btr');
    </script>
</s:if>
<s:if test="declPojo.idmQues7!=null && declPojo.idmQues7==false">
    <script>
    showQuestionWithDetail('false','idmQues7AddDetail','idmQues7atr')
    </script>
</s:if>
<s:if test="declPojo.idmQues7!=null && declPojo.idmQues7==false && declPojo.idmQues7a!=null && declPojo.idmQues7a==false">
    <script>
      showQuestion('false','idmQues7btr');
    </script>
</s:if>
<s:if test="declPojo.idmQues8!=null && declPojo.idmQues8==true">
    <script>
    showYesDeatil('true','idmQues8AddDetail');
    </script>
</s:if>

<s:if test="#request.readonly=='true' || hasEditPermission(#request.conElgble)==false">
	  <script>
		  disableFormFlds("finalDeclReadOnly");	 
	  </script>
</s:if>