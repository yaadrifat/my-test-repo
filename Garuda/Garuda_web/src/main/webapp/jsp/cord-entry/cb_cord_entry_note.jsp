<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%
HttpSession tSession = request.getSession(true); 
UserJB tUser = (UserJB) session.getAttribute("currentUser");
String registryId = request.getParameter("registryId");
request.setAttribute("registryId",registryId);
%>
<script>
$j(function(){
	if($j('#checkbox').attr('checked'))
	{
	$j("#comment").show();
	$j("#commentText").show();
	}
	else
	{
		$j("#comment").hide();
		$j("#commentText").hide();
	}
	});
function limitText(limitField, limitNum){
    if (limitField.value.length > limitNum) {
        limitField.value = limitField.value.substring(0, limitNum);
    } 
}

function enable()
{
	var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
 	var value = field.GetHTML(true); 	
 	var categorySelected = document.getElementById('fkNotescategory');
 	
 	if(value=='<br>' || value=='<P>&nbsp;</P>')
 		{
 		value="";
 		
 		}

 	if((value.length>0) || (categorySelected.selectedIndex>0))
 		{
 			
	 		$j("#addMultipleNote").removeAttr('disabled');
 		}
 	else
 		{
 		
 		$j("#addMultipleNote").attr('disabled', 'disabled' );
 		}
} 

function showDataModal(title,url,height,width,regisId,id)
{
	var id1 = "#" + id;
	var title1 = title + " " + regisId;
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}

		$j(id1).dialog(
				   {autoOpen: false,
					title: title1  ,
					resizable: false,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery(id1).dialog("destroy");
				    }
				   }
				  ); 
		$j(id1).dialog("open");
	$j.ajax({
        type: "POST",
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j(id1).html(result);
        	       	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	
}


function clinicalValidation()
{
	var categorySelected = document.getElementById('fkNotescategory');
	var clinicalFlag=true;
	var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
 	var value = field.GetHTML(true); 	
 	if(value=='<br>' || value=='<P>&nbsp;</P>')
 		{
 		value="";	
 		}
 	if(value.length>4000)
	{
 		$j("#note_error1").show();
	}
 	if(value.length<4000)
 		{
 		$j("#note_error1").hide();
 		}
 	if(value.length>0)
		{
		$j("#note_error").hide();
		if($j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val()==null)
		{
			$j("#note_assess").show();
			clinicalFlag=false;
		}
		if(categorySelected.selectedIndex==0)
			{
				$j("#note_category").show();
				clinicalFlag=false;
			} 
		}
	if($j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val()!=null)
		{
		if(value.length==0)
			{
			$j("#note_error").show();
			clinicalFlag=false;
			}
		if(categorySelected.selectedIndex==0)
			{
			$j("#note_category").show();
			clinicalFlag=false;
		}  
		}
	  if($j("#fkNotescategory").val()!=""){
		if(value.length==0)
		{
		$j("#note_error").show();
		clinicalFlag=false;
		}
		if($j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val()==null)
		{
			$j("#note_assess").show();
			clinicalFlag=false;
		}
	 } 
	  if(($j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val()==null) || ($j("#fkNotescategory").val()=="") || (value.length==0))
		  {
		 		clinicalFlag=false;
		  } 
	  if(($j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val()!=null) && ($j("#fkNotescategory").val()!="") && (value.length>0))
	  {
	 	  clinicalFlag=true;
	  }
	  if(($j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val()==null) && ($j("#fkNotescategory").val()=="") && (value.length==0))
	  {
	 	  clinicalFlag=true;
	  }
	return clinicalFlag;
	}

var clinicalNoteCount1 = 0;   
//var maxclinicalNoteCount1 = 0;
function saveNotesData(){
	//alert("savenotedata");
	 var clinicalFlag=true;
	clinicalFlag = clinicalValidation();
	if(clinicalFlag==true){ 
	var visibleToFlag=0;
	var categoryName = $j("#fkNotescategory option:selected").text();
	var flagForLater= $j('#flagForLater:checked').val();
	var categoryId = $j("#fkNotescategory option:selected").val();
	var noteKeyword = $j("#keyword").val();
	var flagComment = $j("#commentNote").val();
	if(flagComment!=null)
		{
		$j("#comment").hide();
		$j("#commentText").hide();
		}
	var assessment = $j('input[name=clinicalNotePojo.noteAssessment]:radio:checked').val();
	var assessmentId;
	if(assessment=='no_cause')
		{
		assessmentId = assessment;
		assessment = 'No Cause For Deferral';
		}
		else
			{
			assessmentId = assessment;
			assessment = 'Defer';
			}
	var visibleToTc = $j('#visibileToTC:checked').val();
	if(visibleToTc == "true")
		{
		visibleToTc= "<s:text name="garuda.clinicalnote.label.visibleToTc" />";
		visibleToFlag = "true";
		}
	else
		{
		visibleToTc= "<s:text name="garuda.clinicalnote.label.notvisibleToTc" />";
		visibleToFlag = "false";
		}
	var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
 	var value = field.GetHTML(true);
 	 var today = new Date(); 
 	today = today.format("mmm dd, yyyy / hh:mm:ss TT");
 	
 	
/*  	SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
 	String dates=formatter.format(today); */
 	//alert(dates);
 
	 /* if(maxclinicalNoteCount1 < 10){ */
	 var temp=value.replace(/"/g,"'");
		if(categoryId!=""){ 
		$j('#clinicalNotetable').each(function(){			
	       var n = 0;              
		       n = (clinicalNoteCount1==0) ? 0  : clinicalNoteCount1 ; 
				   clinicalNoteCount1++;
				   //maxclinicalNoteCount1++;			       
	    	   var $table = $j(this);    	   
	    	   var tds = '';		    	  
	    	       tds += '<tr id="clincialnote_row'+n+'"><td><s:text name="garuda.clinicalnote.label.posted_by"/><%=tUser.getUserFirstName()%>&nbsp;&nbsp;<%=tUser.getUserLastName()%></td><td><s:text name="garuda.clinicalnote.label.posted_on"/></td><td>'+today+'</td><td><s:hidden name="multipleClinicalNotes['+n+'].fkNotesCategory" value="'+categoryId+'"/><s:hidden name="multipleClinicalNotes['+n+'].comments" value="'+flagComment+'"/><s:hidden name="multipleClinicalNotes['+n+'].flagForLater" value="'+flagForLater+'"/><s:hidden name="multipleClinicalNotes['+n+'].keyword" value="'+noteKeyword+'"/><s:hidden name="multipleClinicalNotes['+n+'].visibility" value="'+visibleToFlag+'"/><s:hidden name="multipleClinicalNotes['+n+'].notes" value="'+value+'"/><s:hidden name="multipleClinicalNotes['+n+'].noteAssessment" value="'+assessmentId+'"/><s:text name="garuda.cbu.label.clinical"/>'+(n+1)+categoryName+'</td><td><s:text name="garuda.clinicalnote.label.assessment"></s:text></td><td>'+assessment+'</td><td><s:text name="garuda.clinicalnote.label.visibility"></s:text></td><td>'+visibleToTc+'</td><td><s:text name="garuda.cbu.label.note"></s:text></td><td>'+value+'</td><td><img style="cursor: pointer;vertical-align:middle;"  src="images/cross.png" border="0"  onclick="javascript:deleteNotes('+n+');" /></td></tr>'
	        	  
         	       $j(this).append(tds);
	           /* 	if(maxclinicalNoteCount1==10){
	   		      $j("#addbutton").css("cursor","default");
	   	        } 	 */   
	       });	
	 }
	// }				 
			    /* else{
			      $j("#addbutton").css("cursor","default");
			  } */
		clearNoteData();
	}
}
 
function deleteNotes(rowid) {            	 
	$j('#clincialnote_row'+rowid).remove();	
  		

}	
 
function clearNoteData()
{

	clearEditorData();
	$j('input[name=clinicalNotePojo.noteAssessment]:radio').attr('checked', false);
	document.getElementById("fkNotescategory").selectedIndex = "";
	$j("#keyword").val("");
	$j('#visibileToTC').attr('checked',false);
	$j("#commentNote").val("");
	$j('#flagForLater').attr('checked',false);
	$j("#addMultipleNote").attr('disabled', 'disabled' );
}

function clearEditorData()
{
	var editor = FCKeditorAPI.GetInstance('sectionContentsta');
	editor.SetHTML('');
}

function commonAutodefer1(Id){
	var flag = false;
	jConfirm('<s:text name="garuda.cbu.cordentry.autoDefer"/>', '<s:text name="garuda.common.lable.confirm"/>',
			function(r) {
				if (r == false) {
				   $j("#"+Id).removeAttr("checked");
				   $j("#"+Id).focus();				   
				  }else if(r == true){
					    var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
					 	var value = field.GetHTML(true);
					  	$j('#sectionContentsta').val(value);
					    $j("#cordSearchable").val("1");	  
					    loadDivWithFormSubmitDefer('saveCordEntry?autoDeferFlag=false','maincontainerdiv','cordentryform1');					
				  }
	});
	return flag;
}


function showCbuStatus(val){
	$j("#assess_error").hide();	
	var deferId = 'noteAssessment'+val;
	 if(val=='na' || val=='tu'){
		 var url='updateCbuStatus?assessmentstatus=true&module=notes&clinicalNotePojo.noteAssessment='+val+'&licenceUpdate=False&cbustatusin=IS_LOCAL_REG&cdrCbuPojo.cordID='+'<s:property value="cdrCbuPojo.cordID"/>'+'&orderId=&orderType=&applyResol=&iscordavailfornmdp=&deferId='+deferId;
		showCbuStatusModal('Update CBU Status For CBU Registry ID '+'<s:property value="#request.registryId"/>',url,'450','600',val);
	} 
	
}

function showCbuStatusModal(title,url,height,width,val)
{
  
	$j('.progress-indicator').css( 'display', 'block' );
	if(height==null || height==""){
		height = 650;
	}
	if(width==null || width==""){
		width = 750;
	}
var progressMsg="<table width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/> <img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	$j("#modelPopup1").html(progressMsg);
		$j("#modelPopup1").dialog(
				   {autoOpen: false,
					title: title  ,
					resizable: true,
					closeText: '',
					closeOnEscape: false ,
					modal: true, width : width, height : height,
					close: function() {
						//$(".ui-dialog-content").html("");
						//jQuery("#subeditpop").attr("id","subeditpop_old");
		      		jQuery("#modelPopup1").dialog("destroy");
		      		jQuery("#modelPopup1").html("");
				    }
				   }
				  ); 
		$j("#modelPopup1").dialog("open");
	$j.ajax({
        type: "POST",
        cache : false,		
        url: url,
       // async:false,
        success: function (result, status, error){
	       	$j("#modelPopup1").html(result);
	       	$j("#noteAssessment"+val).attr("checked","checked");      	
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }

	});
	$j('.progress-indicator').css( 'display', 'none' );
}

function hideMessage()
{
	enable();
	if($j("#fkNotescategory").val()>0){
		$j("#note_category").hide();
	}
}

function showVisibleToTCmessage()
{
	var checked = $j('#visibileToTC:checked').val();
	if(checked=='true')
		{
				var answer = confirm('<s:text name="garuda.clinicalnote.label.visibility_warning_msg"/>');
				if(!answer)
							{
								$j("#visibileToTC").removeAttr("checked");
							}
		}
	}

function showfield(){
	if($j('#flagForLater').attr('checked'))
	{
	$j("#comment").show();
	$j("#commentText").show();
	}
	else
	{
		$j("#comment").hide();
		$j("#commentText").hide();
	}
}

function getKeyword(){
	var word = $j("#keyword").val();
	while(word.indexOf(",")>0)
	{
		 word = word.replace(",", " ");
	}
	$j("#keyword").val(word);
   
	
}

function showDiv(value){ 
	$j("#explain").val('');
	$j("#explain1").val('');
	if(value=="na")
	{
		var explain = $j("#explain").val();
		$j("#cbustat").show();	
		$j("#tustat").hide();
		
	}else if(value=="tu")
	{
		var avail = $j("#datepicker9").val();
		$j("#tustat").show();
		$j("#cbustat").hide();
		
		
	}else
	{
		$j("#cbustat").hide();
		$j("#tustat").hide();
	}
}
$j(function(){
	// Load Validation If All Widget Is Fully Loaded
    LoadedPage.clinicalNote=true;
    if(LoadedPage.idm && LoadedPage.fmhq && LoadedPage.mrq && LoadedPage.processingProc && LoadedPage.cbuHla && LoadedPage.labSummary){
  	  jQueryValidationInitializer();
    }	
});

</script>

<div class='popupdialog tabledisplay ' >
<s:hidden name="clinicalNotePojo.pkNotes" />
<s:hidden name="clinicalNotePojo.noteSeq" id="noteSeq"/>



    <table width="100%" cellspacing="0" cellpadding="0" border="1" >
    <tr>
	   <td><span style="color: red;">
	         <s:text name="garuda.clinicalnote.label.warning_msg"/>
	       </span>
	  </td>				
   </tr>
   <tr>
      <td>
		    <s:textarea id="sectionContentsta" tabindex="-1" cssClass="clinicalclass"  name="clinicalNotePojo.notes"   rows="5" cols="50" wrap="hard"  cssStyle="width:800;height:190;"></s:textarea>
     <script>
     init();
     </script>
     <div id='note_error' style="display: none;">
					<span style="color: red;" ><s:text name="garuda.cbu.cordentry.reqnotes"/></span>
			</div>
	 <div id='note_error1' style="display: none;">
					<span style="color: red;"><s:text name="garuda.cbu.clinicalnote.notes"/></span>
			</div>
     </td>
   </tr>
   <tr>  
	  <td  valign="middle">
	      <fieldset style="display:inline-block"  ><legend> <s:text  name="garuda.clinicalnote.label.assessment"></s:text>
			</legend>
					<table>
						<tr>
							<td valign="middle" ><s:radio name="clinicalNotePojo.noteAssessment" id="assess"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_ASSESS]" listKey="subType" listValue="description" id="noteAssessment" onclick="showCbuStatus(this.value)"  />
							<div id='note_assess' style="display: none;">
							<span style="color: red;"><s:text name="garuda.cbu.assessment.selectAssess"/></span>
							</div>
							</td>
							<td>
								<s:checkbox name="clinicalNotePojo.flagForLater" id="flagForLater"  value="clinicalNotePojo.flagForLater"  onclick="javascript:showfield();"></s:checkbox><s:text
								name="garuda.clinicalnote.label.flagForLater"></s:text>
							</td>						
							<td id="commentText" style="display:none">
									<s:text name ="garuda.clinicalnote.label.comment"></s:text>
									
							</td>
							<td id="comment" style="display:none">
									<s:textfield name="clinicalNotePojo.comments" maxlength="25" id="commentNote" />
							</td>									           
				  </tr>
		  </table>
        </fieldset>
	</td>
  </tr>
  <tr>
   <td>
      <table cellspacing="0" class="displaycdr">
       <tr><td></td></tr><tr><td></td></tr>
         <tr>
            <td >
                <fieldset style="display:inline-block">
   				<legend> 
   						<s:text name="garuda.clinicalnote.label.visibility"></s:text>
				</legend>
				    <s:checkbox name="clinicalNotePojo.visibility" onClick="javascript:showVisibleToTCmessage();" id="visibileToTC"  value="clinicalNotePojo.visibility" ></s:checkbox><s:text
					name="Transplant Center"></s:text>								
				</fieldset>
			</td>
	        <td >
				<s:text name="garuda.clinicalnote.label.keyword" />
			</td>
			<td>
				<s:textfield name="clinicalNotePojo.keyword" maxlength="25" id="keyword" onblur="javascript:getKeyword();" cssStyle="width: 200px"  />
			</td>


		    <td ><s:text name="garuda.clinicalnote.label.category" />
		  		<s:select name="clinicalNotePojo.fkNotesCategory" cssStyle="width:200px" id="fkNotescategory"	list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CLINIC_CATEGORY]"
				listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select" onchange="hideMessage();" />
					<div id='note_category' style="display: none;">
							<span style="color: red;"><s:text name="garuda.cbu.clinicalnote.notescategory"/></span>
							</div>
		   </td>
		   
		  
	</tr>

	 </table>
  </td>
 </tr>
 	<tr>
			 <td>
  	<s:if test="#request.clinicalNotelist!=null && #request.clinicalNotelist.size>0">
  		<div id="historyNotes" style=" width: 800px; height: 100px; overflow: auto;" >  		
  		<table>
  		 	<tr>
  		 	
  		 		<td>   				
					<table>		
						<s:iterator value="#request.clinicalNotelist">
							<s:if test ="reply==true">
							<tr>
									<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
											<s:text name="garuda.cdrcbuview.label.clinical_notes"></s:text>
											<s:text name="garuda.cbu.label.reClinical"/><s:property value="noteSeq"/>											
									</td>
											
											<s:set name="cellValue1" value="createdBy" scope="request"/>
													<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>	
									</td>
									 <s:if test="hasEditPermission(#request.vClnclNote)==true">
									<%-- <td>
												<a href ="#" onClick="loadPageByGetRequset('revokeNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&isCordEntryNote=Y&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes='+<s:property value="pkNotes"/>,'clinicalNoteDiv','clinical')"> <s:text name="garuda.clinicalnote.label.revoke" /></a>
									</td> --%>
									</s:if>
									 <s:if test="hasEditPermission(#request.vClnclNote)==true">
									<td>
											<a href ="#" onClick="showModals('Reply Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','replyNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REPLY_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.fkNotesCategory=<s:property value="fkNotesCategory"/>&clinicalNotePojo.noteSeq=<s:property value="#request.noteSeq"/>','500','850','notesModalsDiv');"> <s:text name="garuda.clinicalnote.label.reply" /></a>
									</td> 
									</s:if>
							</tr>
							</s:if>
							<s:else>
							<tr>
							
								<s:set name="cellValue1" value="createdBy" scope="request"/>
													<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
													<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>										
													</td>
								
								<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
								<s:text name="garuda.cdrcbuview.label.clinical_notes"></s:text>
									:<s:text name="garuda.cbu.label.clinical_note"/><s:property value="noteSeq"/> <s:property value="getCodeListDescById(fkNotesCategory)" />
								</td>
								<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
								<input type="hidden" name="pkNotes"/>
								</td>
								<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
								<s:text name="garuda.clinicalnote.label.posted_on"></s:text>
								<s:set name="cellValue15" value="createdOn" />
							        			<s:date name="cellValue15" id="cellValue15" format="MMM dd, yyyy / hh:mm:ss a"  />
							        			<s:property value="cellValue15"/>
								</td>
								
								<td >
								    <fieldset style="display:inline-block" >
						   				<legend> 
						   						<s:text name="garuda.clinicalnote.label.assessment"></s:text>
										</legend>
											<table width="100%" cellpadding="0" cellspacing="0">
											    <tr>
											        <td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
											             <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_ASSESS]">
														    <s:if test="subType==noteAssessment">
														         <input type="radio" checked="checked" disabled="disabled"  ><s:property value="description"/>
														    </s:if> 
														</s:iterator>
											        </td>
											    </tr>								
											</table>
									</fieldset>
								</td>
								<td <s:if test="amended==true">style="text-decoration:line-through;"</s:if>>
											           <s:checkbox name="visibility" disabled="true"></s:checkbox><s:text name="garuda.clinicalnote.label.visibleToTc"></s:text>
								</td>
										 
											<%-- <td>
													<a href ="#" onClick="loadPageByGetRequset('revokeCordEntryNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&isCordEntryNote=Y&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.fkNotesCategory=<s:property value="clinicalNotePojo.fkNotesCategory" />&clinicalNotePojo.pkNotes='+<s:property value="pkNotes"/>,'notesAsyncData')"> <s:text name="garuda.clinicalnote.label.revoke" /></a>
											</td> --%>
										
											<td>
													<a href ="#" onClick="showDataModal('Clinical Note for CBU Registry ID','viewNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property value="pkNotes"/>','500','700','<%=registryId %>','modelPopup1')"> <s:text name="garuda.clinicalnote.label.view" /></a>
											</td>
											
											<%--  <td>
											 <a href="javascript:void(0)" onClick="showModal('Reply Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','replyNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REPLY_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.fkNotesCategory=<s:property value="fkNotesCategory"/>&clinicalNotePojo.noteSeq=<s:property value="#request.noteSeq"/>','500','850','notesModalsDiv');"> <s:text name="garuda.clinicalnote.label.reply" /></a>		
											</td>   --%>
											
						    </tr>	
					</s:else>
						   
						</s:iterator>
				   </table>
				
  		 				</td>
  		 	</tr>
  		</table></div>
  	
  		</s:if>
  </td>
	</tr>
 <tr>
    <td>
       <div >
          <table id="clinicalNotetable">
          </table>
       </div>
    </td>
 </tr>
 <tr >
	
				<td>
						<button type="button" id="addMultipleNote" disabled="disabled" onclick="saveNotesData()" ><s:text
						name="garuda.cdrcbuview.label.button.addnewnotes" ></s:text></button>
						
    			</td>
			
</tr> 
</table>
</div>