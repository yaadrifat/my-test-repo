<%@ taglib prefix="s"  uri="/struts-tags"%>	
<%@ include file="../cb_includes.jsp"%>
<SCRIPT language="javascript">

var autoFilled= false;
var autoFillee= false;
function showAssment(Id,buttonTd,divId,mDivId,hideButtonTd){
	var assesmentCheckFlag="";
	
	var responsestr=$j('#'+Id+' option:selected').text();

	var val = $j('#'+Id+' option:selected').val();

	var assesTest1=$j("#alphaThalismiaTraitPkVal").val();
	
	var assesTest2=$j("#hemozygousNoDiesasePkVal").val();
	
	var assesTest3=$j("#hemoAlphaThalassmiaPkVal").val();
	
	var assesTest4=$j("#hemoTraitPkVal").val();

	var assesTest5=$j("#hemozygousNoDonePkVal").val();

	var assesTest6=$j("#hemozygousUnknownPkVal").val();
	
	var assesTest7=$j("#hemoMultiTraitPkVal").val();
	
	var fungLabAssesPoes=$j("#fungalCulpostive").val();
	
	var fungLabAssesPoes=$j("#fungalCulpostive").val();

	var fungLabAssesPoes1=$j("#fungalCulnotdone").val();

	var data=null;
	if(val==fungLabAssesPoes || val==fungLabAssesPoes1){
		if(val==$j("#lb_fungresult").val()){
			assesmentCheckFlag="true";
		}
		else{
			assesmentCheckFlag="flase";
		}
		data='entityId='+$j("#cordID").val()+'&subEntityId='+val+'&shrtName=LAB_SUM_Fung&subentitytype='+$j("#subentityType").val()+'&questionNo=FungalCulture&responsestr='+responsestr+'&assessDivName='+mDivId+'&responseval='+val;
		
	}
	else if(val==assesTest1 || val==assesTest2 || val==assesTest3 || val==assesTest4 || val==assesTest5 || val==assesTest6 || val==assesTest7){
		if(val==$j("#lb_hemoglobinscrn").val()){
			assesmentCheckFlag="true";
		}
		else{
			assesmentCheckFlag="false";
		}
		data='entityId='+$j("#cordID").val()+'&subEntityId='+val+'&shrtName=LAB_SUM_Hemo&subentitytype='+$j("#subentityType").val()+'&questionNo=Hemoglobinopathy Testing&responsestr='+responsestr+'&assessDivName='+mDivId+'&responseval='+val;
	}

	if(val!=null && val!= ""){
		$j('#'+mDivId).show();
		$j('#'+hideButtonTd).show();
		$j('#'+buttonTd).hide();
		if($j('#'+divId).html() != null && $j('#'+divId).html() != "" && $j('#'+divId).html() != 'undefined'){
			//$j('#'+divId).show();
		}else{
		refreshassessment1('getassessmentHemo?assesmentCheckFlag='+assesmentCheckFlag,divId,data);
		}
	}else{
		$j('#'+mDivId).hide();
		$j('#'+buttonTd).hide();
	}
}

function hideAssment(mDivId,shwbuttonTd,hideButtonTd){
	$j('#'+mDivId).hide();
	$j('#'+shwbuttonTd).show();
	$j('#'+hideButtonTd).hide();
}

function checkassessment1(tname,masterDiv,buttonId,hideButtonTd,id,val){

	var divid=tname;

	var mDiv=masterDiv;
	var assesmentCheckFlag="";
	var assesTest1=$j("#alphaThalismiaTraitPkVal").val();
	
	var assesTest2=$j("#hemozygousNoDiesasePkVal").val();
	
	var assesTest3=$j("#hemoAlphaThalassmiaPkVal").val();
	
	var assesTest4=$j("#hemoTraitPkVal").val();

	var assesTest5=$j("#hemozygousNoDonePkVal").val();

	var assesTest6=$j("#hemozygousUnknownPkVal").val();
	
	var assesTest7=$j("#hemoMultiTraitPkVal").val();
	
	var fungLabAssesPoes=$j("#fungalCulpostive").val();

	var fungLabAssesPoes1=$j("#fungalCulnotdone").val();
	
	//alert(data);
	//alert("tname : "+tname);
	var responsestr=$j('#'+id+' option:selected').text();
	
	//var data='entityId='+$j("#cordID").val()+'&subEntityId='+$j("#cordID").val()+'&shrtName=LAB_SUM&subentitytype='+$j("#subentityType").val()+'&&questionNo=Hemoglobinopathy&responsestr='+responsestr;
	$j('#'+hideButtonTd).hide();
	if(val!=null && val!= "" && (val==assesTest1 || val==assesTest2 || val==assesTest3 || val==assesTest4 || val==assesTest5 || val==assesTest6 || val==assesTest7)){
		if(val==$j("#lb_hemoglobinscrn").val()){
			assesmentCheckFlag="true";
		}
		else{
			assesmentCheckFlag="false";
		}
		var data='entityId='+$j("#cordID").val()+'&subEntityId='+val+'&shrtName=LAB_SUM_Hemo&subentitytype='+$j("#subentityType").val()+'&questionNo=Hemoglobinopathy Testing&responsestr='+responsestr+'&assessDivName='+mDiv+'&responseval='+val;	
		$j('#'+mDiv).show();
		$j('#'+buttonId).hide();
		$j('#'+hideButtonTd).show();
		refreshassessment1('getassessmentHemo?assesmentCheckFlag='+assesmentCheckFlag,divid,data);
	}
	else if(val==fungLabAssesPoes1){
		if(val==$j("#lb_fungresult").val()){
			assesmentCheckFlag="true";
		}
		else{
			assesmentCheckFlag="flase";
		}
		var data='entityId='+$j("#cordID").val()+'&subEntityId='+val+'&shrtName=LAB_SUM_Fung&subentitytype='+$j("#subentityType").val()+'&questionNo=FungalCulture&responsestr='+responsestr+'&assessDivName='+mDiv+'&responseval='+val;
		$j('#'+mDiv).show();
		//$j('#'+buttonId).show();
		$j('#'+buttonId).hide();
		$j('#'+hideButtonTd).show();
		refreshassessment1('getassessmentHemo?assesmentCheckFlag='+assesmentCheckFlag,divid,data);
	}
	else{
			$j('#'+mDiv).hide();
			//$j('#showassesmentFung').hide();
			$j('#'+buttonId).hide();
		}
	//2nd Type Assessment
}

function autoDefer(Val,Id,ckVal,className,element){
	 var flag=false;
	 if( Val !='' && Val>=0 && Val<85 ){
		 if((typeof labSummaryElements !="undefined") && (typeof labSummaryElementVal !="undefined")){
				var param = prepareParam(element,className,labSummaryElements,labSummaryElementVal);
				AutoDeferChange.deferElement(param);
				flag=true;
			}
		jConfirm('Response you entered would cause CBU to be automatically status as permanently medically deferred. Please confirm if you would like to proceed', '<s:text name="garuda.common.lable.confirm"/>',
			function(r) {
				if (r == false) {
					if(flag==true && Id=="savePostTestList13testresult"){
						 $j("#"+Id).val(AutoDeferElementInitVal[$j(element).attr('name')]);							
					     AutoDeferChange.elementProgress();
					     AutoDeferChange.elementFocus();
					     AutoDeferChange.gc();
					}else{
				      $j("#"+Id).val("");
				      $j('#'+Id).focus();
					 }				   
				  }else if(r == true){
					    var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
					 	var value = field.GetHTML(true);
					  	$j('#sectionContentsta').val(value);
					    $j("#cordSearchable").val("1");	  
					    commonMethodForSaveAutoDefer();
					    //atuoDeferEsignId = Id; 
					    if(ckVal!="" && parseInt(ckVal)==0)
					      ckTextField = false;
					    else if(ckVal!="" && parseInt(ckVal)==1)
					      ckTextField = true;  
					    atuoDeferEsignId =Id;						
					  }
			});
		  }
	  }

function autoDeferCFU(Val,Id,ckVal,className,element){
	 var flag=false;
	 if(Val !=''&& Val==0){
		 if((typeof labSummaryElements !="undefined") && (typeof labSummaryElementVal !="undefined")){
				var param = prepareParam(element,className,labSummaryElements,labSummaryElementVal);
				AutoDeferChange.deferElement(param);
				flag=true;
			}
		jConfirm('Response you entered would cause CBU to be automatically status as permanently medically deferred. Please confirm if you would like to proceed', '<s:text name="garuda.common.lable.confirm"/>',
			function(r) {
				if (r == false) {
					if(flag==true && Id=="savePostTestList12testresult"){
						 $j("#"+Id).val(AutoDeferElementInitVal[$j(element).attr('name')]);							
					     AutoDeferChange.elementProgress();
					     AutoDeferChange.elementFocus();
					     AutoDeferChange.gc();
					}else{
				     $j("#"+Id).val("");
				      $j('#'+Id).focus();
					}					     
				  }else if(r == true){
					    var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
					 	var value = field.GetHTML(true);
					  	$j('#sectionContentsta').val(value);
					    $j("#cordSearchable").val("1");	  
					    commonMethodForSaveAutoDefer();
					   // atuoDeferEsignId = Id; 
					    ckTextField =ckVal;
					    ckTextFieldId =Id;						
					  }
			});
		  }
	  }
function refreshassessment1(url,divname,data){
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data:data,
        success: function (result){
            var $response=$j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	$j("#"+divname).html(result);
            }
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
}


function vidSaved(id,value){
	if(value!=null && value!="" && value!="undefined"){
		$j("#"+id).show();
		}
		else{
			$j("#"+id).hide();
			$j("#"+id).children().each(function(n, i) {
				  var idParent = this.id;
				 if((idParent.indexOf('fktestreason') != -1 || idParent.indexOf('fktestspecimen') != -1 || idParent.indexOf('fktestmethod') != -1)&& 
						  (idParent.indexOf('fktestreason_error') == -1 || idParent.indexOf('fktestspecimen_error') == -1 || idParent.indexOf('fktestmethod_error') == -1)){
						$j("#"+idParent+" option[value='-1']").attr("selected", "selected");
				  }
				 if(idParent.indexOf('reasonFrTestDescOthTm')!= -1 || idParent.indexOf('testMethDescOthTm')!= -1 ){
						$j("#"+idParent).children().each(function(n, i) {
							var idChild = this.id;
						  	if((idChild.indexOf('reasonTestDesc') != -1 || idChild.indexOf('testMthdDesc') != -1 )&& 
									  (idChild.indexOf('reasonTestDesc_error') == -1  || idChild.indexOf('testMthdDesc_error') == -1)){
									$j("#"+idChild).val('');
							  }
						});
						$j("#"+idParent).hide();
				 }
		 });

			
		}
	//alert("ffffff");
}

function showAssessment(val)
{
	//alert(val);
}

function vid(num,value){
	if(value!=null && value!="" && value!="undefined"){
	$j("#showDropDown"+num).show();
	}
else{
	$j("#showDropDown"+num).hide();

	$j("#showDropDown"+num).children().each(function(n, i) {
		  var id = this.id;
		 if((id.indexOf('fktestreason') != -1 || id.indexOf('fktestspecimen') != -1 || id.indexOf('fktestmethod') != -1)&& 
				  (id.indexOf('fktestreason_error') == -1 || id.indexOf('fktestspecimen_error') == -1 || id.indexOf('fktestmethod_error') == -1)){
				$j("#"+id+" option[value='-1']").attr("selected", "selected");
		  }
		 if(id.indexOf('reasonFrTestDescOthTm')!= -1 || id.indexOf('testMethDescOthTm')!= -1 ){
				$j("#"+id).children().each(function(n, i) {
					var idChild = this.id;
				  	if((idChild.indexOf('reasonTestDesc') != -1 || idChild.indexOf('testMthdDesc') != -1 )&& 
							  (idChild.indexOf('reasonTestDesc_error') == -1  || idChild.indexOf('testMthdDesc_error') == -1)){
							$j("#"+idChild).val('');
					  }
				});
				$j("#"+id).hide();
		 }
   });

}
	//alert("Still Wating ! Is That you ");
}
function showDescField(divId,compValue,fieldval){
	if(compValue==fieldval){
		$j("#"+divId).show();
	}else{
		$j("#"+divId).hide();
		$j("#"+divId).children().each(function(n, i) {
			  var id = this.id;
			  if(id.indexOf('reasonTestDesc') != -1 && id.indexOf('reasonTestDesc_error') == -1){
					$j("#"+id).val('');
			  }
			  if(id.indexOf('testMthdDesc') != -1 && id.indexOf('testMthdDesc_error') == -1){
					$j("#"+id).val('');
			  }
			});

		
	}
}

function addInfo(fkSpecimenId,fktestid,pkpatlabs){
	//alert("ssssss");
	}
function cfuCount(cfuVal){
	if(cfuVal ==$j("#pkOther").val()){
		document.getElementById("cfuCount").style.display="block";
		
	}
	else{
		document.getElementById("cfuCount").style.display="none";
		$j('#cfuDescOther').val('');
		}
 }
function test(viabilityVal){
	if(viabilityVal ==$j("#pkViaOther").val()){
		 document.getElementById("viability").style.display="block";
		 
		}
	else{
		document.getElementById("viability").style.display="none";
		$j('#viaDescOther').val('');
		}
	     }

function saveValues(obJ)
{
	var fldValue="#flD"+obJ.id;
	var selVal= $j("#"+obJ.id).val();
	
	if(selVal!=""){
		var selMulPk = $j.map($j("#"+obJ.id+" option:selected"), function (el, i) {
			if($j(el).val()!=""){
			return $j(el).val();
			}else{
			   return ;
			}
		});
	   	 $j(fldValue).val(selMulPk.join(","));
	}else{

	   	 $j(fldValue).val("");
	}
}
		     
   </SCRIPT>
	
<script>
 $j(function(){
	//var today = new Date();
	//
	//var d = today.getDate();
	//var m = today.getMonth();
	//var y = today.getFullYear();
	
	//var h=today.getHours();
	//var mn=today.getMinutes()+1;
	 getDatePic();
	 jQuery('#preTestDateStrn').datepicker('option', { beforeShow: customRange }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	 jQuery('#postTestDateStrn').datepicker('option', { beforeShow: customRange }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	 jQuery('#otherTestDateStrn1').datepicker('option', { beforeShow: customRange }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	 jQuery('#otherTestDateStrn2').datepicker('option', { beforeShow: customRange }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	 jQuery('#otherTestDateStrn3').datepicker('option', { beforeShow: customRange }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	 jQuery('#otherTestDateStrn4').datepicker('option', { beforeShow: customRange }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	 jQuery('#otherTestDateStrn5').datepicker('option', { beforeShow: customRange }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	 jQuery('#otherTestDateStrn6').datepicker('option', { beforeShow: customRange }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	 jQuery('#datepicker6').datepicker('option', { beforeShow: customRange }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	 jQuery('#datepicker7').datepicker('option', { beforeShow: customRange }).focus(function(){
			if(!$j("#ui-datepicker-div").is(":visible"))
				customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	 //jQuery('#datepicker4').datepicker('option', { beforeShow: customRange });
	 
	 if($j("#pkViaOther").val()==$j("#viabSelected").val()){
		 $j("#viability").show();
		}else{
			$j("#viability").hide();
		}

	 if($j("#pkOther").val()==$j("#cfuSelected").val()){
		$j("#cfuCount").show();
	   }else{
		$j("#cfuCount").hide();
		  }
	 	
 });
 
 function customRange(input){
		var today = new Date();
		var d = today.getDate();
		var m = today.getMonth();
		var y = today.getFullYear();
		var h=today.getHours();
		var mn=today.getMinutes()+1;
		if($j("#datepicker2").val()!="" && $j("#datepicker2").val()!=null){
			var minTestDate=new Date($j("#datepicker2").val());
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
			if(input.id == 'preTestDateStrn' || input.id == 'postTestDateStrn' || input.id == 'otherTestDateStrn1' || input.id == 'otherTestDateStrn2' || input.id == 'otherTestDateStrn3' || input.id == 'otherTestDateStrn4' || input.id == 'otherTestDateStrn5' || input.id == 'otherTestDateStrn6' || input.id == 'datepicker6' || input.id == 'datepicker7'){
				/*return {
			    	 minDate: new Date(y1,m1,d1)
			    };*/
				$j('#'+input.id).datepicker( "option", "minDate", new Date(y1,m1,d1));
				if(input.id == 'preTestDateStrn' && $j("#datepicker4").val()!=='' && $j("#datepicker4").val()!=null){
					var maxTestDate=new Date($j("#datepicker4").val());// processing start date
					var d2 = maxTestDate.getDate();
					var m2 = maxTestDate.getMonth();
					var y2 = maxTestDate.getFullYear();
					if(today.getTime() > maxTestDate.getTime()){
						$j('#'+input.id).datepicker( "option", "maxDate", new Date(y2,m2,d2));
					}else{
						$j('#'+input.id).datepicker( "option", "maxDate", new Date(y,m,d));
					}
					
				}else if(input.id == 'preTestDateStrn' && ($j("#datepicker4").val()==='') &&  $j("#datepicker2").val()!=='' && $j("#datepicker2").val()!=null ){// processing is null but collection is not null
					  var maxTestDate=new Date($j("#datepicker2").val());// collection date
					  var cd = maxTestDate.getDate();
					  var cm = maxTestDate.getMonth();
					  var cy = maxTestDate.getFullYear();
					  //if(today.getTime() > maxTestDate.getTime()){
						  	$j('#'+input.id).datepicker( "option", "minDate", new Date(cy,cm,cd));
						  	$j('#'+input.id).datepicker( "option", "maxDate", new Date(y,m,d));
					  //}					
					 
				}else if(input.id !== 'datepicker6' || input.id !== 'datepicker7' && $j("#datepicker4").val()!=='' && $j("#datepicker4").val()!=null){
					LabSumPostCustomDate(input);				
				}
			}
		}else if($j("#datepicker4").val()==='' && $j("#datepicker2").val()==='' &&  $j("#datepicker3").val()!=='' && $j("#datepicker3").val()!=null ){// processing is null and collection is null but Birth date is not null
          	  var babyBirthDate=new Date($j("#datepicker3").val());// birth  date
			  var cd = babyBirthDate.getDate();
			  var cm = babyBirthDate.getMonth();
			  var cy = babyBirthDate.getFullYear();
			  //if(today.getTime() > maxTestDate.getTime()){
				  	$j('#'+input.id).datepicker( "option", "minDate", new Date(cy,cm,cd));
				  	$j('#'+input.id).datepicker( "option", "maxDate", new Date(y,m,d));
			  //}	

		}else{
			$j('#'+input.id).datepicker( "option", "minDate", null);
		}
	}
	
 var LabSumPostCustomDate=function(input){
	            if($j("#datepicker4").val()!=='' && $j("#datepicker4").val()!=null){
						    var today = new Date();
							var d = today.getDate();
							var m = today.getMonth();
							var y = today.getFullYear();
							
							var maxTestDate=new Date($j("#datepicker4").val());// processing start date
							var d2 = maxTestDate.getDate();
							var m2 = maxTestDate.getMonth();
							var y2 = maxTestDate.getFullYear();
							
							$j('#'+input.id).datepicker( "option", "minDate", new Date(y2,m2,d2));
							$j('#'+input.id).datepicker( "option", "maxDate", new Date(y,m,d));
				}else if($j("#datepicker4").val()==='' &&  $j("#datepicker2").val()!=='' && $j("#datepicker2").val()!=null ){// processing is null but collection is not null
							var maxTestDate=new Date($j("#datepicker2").val());// collection date
							var d2 = maxTestDate.getDate();
							var m2 = maxTestDate.getMonth();
							var y2 = maxTestDate.getFullYear();
							$j('#'+input.id).datepicker( "option", "minDate", new Date(y2,m2,d2));
							$j('#'+input.id).datepicker( "option", "maxDate", new Date());
			           }
 }

 
     function restrict(mytextarea) 
     {  
         var maxlength = 15;
         mytextarea.value = mytextarea.value.substring(0,maxlength);
     }    

     function rhSpecify(val){
           if($j("#rhtypeother").val()==val){
               $j("#rhothspec").show();
               $j("#rhothspec1").show();
               
           }else{
        	   $j("#rhothspec").hide();
        	   $j("#rhothspec1").hide();
        	   $j("#cordentryform1_cdrCbuPojo_specRhTypeOther").val('');
        	  // $j('input[name="cdrCbuPojo.specRhTypeOther"]').val('');
           }
     }
     
     function addTestDateModel(specId,removeUnknownTestFlag){
    	 if($j("#datepicker2").val()==null || $j("#datepicker2").val()==""){
   		  alert("<s:text name="garuda.cbu.cordentry.selectCollDate"/>");
   		  document.cordentryform1.cbuCollectionDateStr.focus();
   	  }else{
   		var id="datepicker2";
		var msg="<s:text name='garuda.message.modal.colldatecantbecurdate'/>";
		/* var compCurNCordDate=checkCurrentDateCordDate(id,msg);
		if(compCurNCordDate){ */ 
   		var specCollDate = $j("#datepicker2").val();
    	 var url="addtestdatemodal?cordentry=true&fkSpecimenId="+specId+"&specCollDate="+specCollDate+"&removeUnknownTestFlag="+removeUnknownTestFlag;
    	 showModal('ADD TEST DATE',url,'200','300');
     	/* } */
	}
    }

     $j(function(){
		   $j(".labclass").each(function(){
				  noOfMandatoryLabSummaryField++;
				  noOfMandatoryField++;
			   });
		   var labfocus = "";                                            
		   $j(".labclass").each(function(){
			   var val1 = $j(this).val();
			   labSummaryElements[labSummaryElements.length]=$j(this).attr("id");
			   labSummaryElementVal[labSummaryElementVal.length]=val1;			   	   
			   if(val1!="" && val1!=-1)
			   {							   				    
					  labsummarycount++;		  
					  val = parseInt((labsummarycount*100)/(noOfMandatoryLabSummaryField));
					  $j("#labsummarybar").progressbar({
							value: val
						}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
					  addTotalCount();	
			   }
		   });					 
			$j(".labclass").bind({change:function() {
			   var val = $j(this).val();
			   var val1=val;
			   var j=0;
			   var key = $j(this).attr("id");
			   for(j=0; j<labSummaryElements.length;j++){
				   if(labSummaryElements[j]==key)
					 break;
			   }
			   labfocus=labSummaryElementVal[j];
				 if((labfocus=="" || labfocus==-1 || labfocus==null) && val!="" && val!=-1){
					   labsummarycount++;		  
					   val = parseInt((labsummarycount*100)/(noOfMandatoryLabSummaryField));
						  $j("#labsummarybar").progressbar({
								value: val
							}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
						  addTotalCount();	
				 }else if((labfocus!="" && labfocus!=-1) && (val=="" || val==-1)){
					 labsummarycount--;		  
					 val = parseInt((labsummarycount*100)/(noOfMandatoryLabSummaryField));
					 $j("#labsummarybar").progressbar({
							value: val
						}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
					 minusTotalCount();
				 }
				 labSummaryElementVal[j]=val1;							
			}
		});
			//call method to refersh total count progress
			  totalcount--;
		      addTotalCount();
		   // Load Validation If All Widget Is Fully Loaded
		      LoadedPage.labSummary=true;
		      if(LoadedPage.idm && LoadedPage.fmhq && LoadedPage.mrq && LoadedPage.processingProc && LoadedPage.cbuHla && LoadedPage.clinicalNote){
		    	  jQueryValidationInitializer();
		      }
			      	
	});
	  
	</script>
			<script>
					function limitText(limitField, limitNum) {
					    if (limitField.value.length > limitNum) {
					        limitField.value = limitField.value.substring(0, limitNum);
					    } 
					}

					$j(document).ready(function(){
						if($j("#esignFlag").val()=='labSummary'){
							var div=$j('#labDiv').html();
							var formContent='<form id="labSummaryForm">'+div+'</form>';
							$j('#labDiv').replaceWith(formContent);
						}
						if('<s:property value="esignFlag"/>'=='review'){
							var div=$j('#labSummaryModal').html();
							var formContent='<div id="labSumTargetDiv" style="height: 375px;width: 100%;overflow: auto;"><form id="labSummaryForm">'+div+'</form></div>';
							$j('#labSumTargetDiv').replaceWith(formContent);
							$j('#procandcount select,#procandcount input[type=radio],#procandcount input[type=checkbox]').attr('disabled',true);
							$j('#procandcount input[type=text]').attr('disabled',true);
							$j('#procandcount button').css('display','none');
						}
						if('<s:property value="esignFlag"/>'=='modal'){
							var div=$j('#labSummaryModal').html();
							$j('#labSumTargetDiv').empty();
							var formContent='<form id="labSummaryForm">'+div+'</form>';
							$j('#labSummaryModal').html(formContent);
							$j('#esignForLabSummary').show();
							}
						});
					$j(function(){
                       $j('input[name="cfuCountPk"]').val($j('#patlabs12').val());
                       $j('input[name="viabilityPk"]').val($j('#patlabs13').val());

						});
			</script>
		
			
	                <s:hidden name="otherLabTstsLstId" id="otherLabTstsLstId"></s:hidden>
	                <s:hidden name="esignFlag" id="esignFlag"></s:hidden>
					
	                   <s:hidden name="pkCfuOther" id="pkOther"></s:hidden>	
	                   <s:hidden name="pkViabilityOther" id="pkViaOther"></s:hidden>	
	                   <s:hidden value="%{cdrCbuPojo.fungalResult}" id="lb_fungresult"></s:hidden>
						<s:hidden value="%{cdrCbuPojo.hemoglobinScrn}" id="lb_hemoglobinscrn"></s:hidden>	
	                  <%-- <s:hidden name="prePatLabsPojo.fkTimingOfTest" id="fkTimingOfTest"></s:hidden>
					    <s:hidden name="postPatLabsPojo.fkTimingOfTest" id="fkTimingOfTest"></s:hidden>
					    <s:hidden name="otherPatLabsPojo.fkTimingOfTest" id="fkTimingOfTest"></s:hidden>
					     --%>
					   <s:hidden name="patLabsPojo.fkTimingOfTest"></s:hidden>
					   <s:hidden name="patLabsPojo.fktestid" id="fktestid"></s:hidden>
					   <s:hidden name="cdrCbuPojo.fkSpecimenId" id="fkSpecimenId"></s:hidden>
					   <s:hidden name="patLabsPojo.testresult"></s:hidden>
					   <input type="hidden" name="subentityType" id="subentityType" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@SUBENTITY_CODE_TYPE, @com.velos.ordercomponent.util.VelosGarudaConstants@SUBENTITY_CODE_SUB_TYPE_LAB_SUM)"/>"/>
					   <input type="hidden" name="cdrCbuPojo.rhTypeOther" id="rhtypeother" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@RH_TYPE, @com.velos.ordercomponent.util.VelosGarudaConstants@RH_TYPE_OTHER)" />" />
					  
					   <s:hidden value="%{#request.valuePreFkTimingTest}" id="valuePreFkTimingTest"></s:hidden>
					   <s:hidden value="%{#request.valuePostFkTimingTest}" id="valuePostFkTimingTest"></s:hidden>
					   <s:hidden value="%{#request.valueOtherFkTimingTest}" id="valueOtherFkTimingTest"></s:hidden>
					   
					   <s:hidden value="%{#request.valueOtherPostTestFirst}" id="valueOtherPostTestFirst"></s:hidden>
					   <s:hidden value="%{#request.valueOtherPostTestSecond}" id="valueOtherPostTestSecond"></s:hidden>
					   <s:hidden value="%{#request.valueOtherPostTestThird}" id="valueOtherPostTestThird"></s:hidden>
					   <s:hidden value="%{#request.valueOtherPostTestFourth}" id="valueOtherPostTestFourth"></s:hidden>
					   <s:hidden value="%{#request.valueOtherPostTestFifth}" id="valueOtherPostTestFifth"></s:hidden>
					   <s:iterator value="processingTest" var="rowVal" status="row">
							<s:hidden value="%{pkLabtest}" id="%{shrtName}" name="%{labtestName}"></s:hidden>
						</s:iterator>
						<s:hidden value="%{#request.labGroupTypTests}" id="noOfLabGrpTypTests" name="noOfLabGrpTypTests"/>
						<s:hidden value="%{#request.TNCUncFkTestId}" id="UNCRCT" name="Total CBU Nucleated Cell Count (uncorrected)"/>
						<s:hidden value="%{#request.TNCUnkIfUncFkTestId}" id="TCBUUN" name="Total CBU Nucleated Cell Count (unknown if uncorrected)"/>
						<s:hidden value="%{#request.cfuOtherPkVal}" id="valueCfuOtherPkVal"></s:hidden>
						<s:hidden value="%{#request.testReasonOtherPkVal}" id="valueTestReasonOtherPkVal"></s:hidden>
						<s:hidden value="%{#request.viablityOtherPkVal}" id="valueViablityOtherPkVal"></s:hidden>
						<s:hidden name="cfuCountPk"/>
						<s:hidden name="viabilityPk"/>
					 <!-- <table width="100%" cellpadding="0" cellspacing="0">   
					     <tr>
					     		
						     <td>  
						         
						 </td>
					  </tr>
				  </table>  -->
				
				<table width="100%" cellpadding="0" cellspacing="0"  style="overflow: auto;" id="added">   
		   		<tr>
				  <td align="right" valign="top">
				     <button type="button"  id="submit1" onclick="addTestDateModel('<s:property  value="cdrCbuPojo.fkSpecimenId" />','true');"><s:text name="garuda.cordentry.label.addTestTime"/></button>
				  <table width="100%" cellpadding="0" cellspacing="0" border="1" id="labTestTable">
				  <% int headerCount=3; %>
					<thead>
					  <tr>
						  <th width="25%"><s:text name="garuda.common.lable.test"></s:text></th>
						 
						 
						  <th width="25%">
						      <s:text name="garuda.common.lable.preProcess"></s:text><br></br>
							  <s:text name="garuda.common.lable.testingDate"></s:text>
							  <s:date  name="prePatLabsPojo.testdate" id="datepicker52" format="MMM dd, yyyy" /> 
							  <s:textfield  onkeydown="cancelBack();"  name="preTestDateStrn" id="preTestDateStrn" value="%{datepicker52}" cssClass="datePicWMaxDate" cssStyle="width:85%;"></s:textfield>
						  </th>
							        	
						  <th width="25%"><s:text name="garuda.common.lable.postProcess"></s:text><br></br>
						  <s:text name="garuda.common.lable.testingDate"></s:text>
							  <s:date name="postPatLabsPojo.testdate" id="datepicker53" format="MMM dd, yyyy" />
							  <s:textfield onkeydown="cancelBack();"  name="postTestDateStrn" id="postTestDateStrn" value="%{datepicker53}" cssClass="datePicWMaxDate" cssStyle="width:85%;"></s:textfield>
						      
						  </th>
						  
						  <s:if test="otherPostTestList!=null && otherPostTestList.size()>0 ">
						  <%  headerCount=headerCount+1; %>
						    <th width="25%"><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
						       <s:date name="otherPatLabsPojo.testdate" id="datepicker92" format="MMM dd, yyyy" />
							   <s:textfield onkeydown="cancelBack();"  name="otherTestDateStrn1" id="otherTestDateStrn1" value="%{datepicker92}" cssClass="datePicWMaxDate" cssStyle="width:85%;"></s:textfield>
						    </th>
						 </s:if>
						  
						<s:if test="otherPostTestListFirst!=null && otherPostTestListFirst.size()>0 ">
						<%  headerCount=headerCount+1; %>
						    <th width="25%"><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
						      <s:date name="otherPatLabsPojoFirst.testdate" id="datepicker94" format="MMM dd, yyyy" />
							  <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn2" id="otherTestDateStrn2" value="%{datepicker94}" cssClass="datePicWMaxDate" cssStyle="width:210px;"></s:textfield>
						    </th>
						  </s:if>
						  
						 <s:if test="otherPostTestListSecond!=null && otherPostTestListSecond.size()>0 ">
						 <%  headerCount=headerCount+1; %>
							   <th width="25%"><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
							      <s:date name="otherPatLabsPojoSecond.testdate" id="datepicker95" format="MMM dd, yyyy" />
							      <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn3" id="otherTestDateStrn3" value="%{datepicker95}" cssClass="datePicWMaxDate" cssStyle="width:210px;"></s:textfield>
							   </th>
						 </s:if>
						
						<s:if test="otherPostTestListThird!=null && otherPostTestListThird.size()>0 ">
						<%  headerCount=headerCount+1; %>
							    <th width="25%"><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
							        <s:date name="otherPatLabsPojoThird.testdate" id="datepicker96" format="MMM dd, yyyy" />
							        <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn4" id="otherTestDateStrn4" value="%{datepicker96}" cssClass="datePicWMaxDate" cssStyle="width:210px;"></s:textfield>
							    </th>
						 </s:if>
					 
						<s:if test="otherPostTestListFourth!=null && otherPostTestListFourth.size()>0 ">
						<%  headerCount=headerCount+1; %>
							    <th width="25%"><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
							       <s:date name="otherPatLabsPojoFourth.testdate" id="datepicker97" format="MMM dd, yyyy" />
							       <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn5" id="otherTestDateStrn5" value="%{datepicker97}" cssClass="datePicWMaxDate" cssStyle="width:210px;"></s:textfield>
							    </th>
						 </s:if>
					 
						 <s:if test="otherPostTestListFifth!=null && otherPostTestListFifth.size()>0 ">
						 <%  headerCount=headerCount+1; %>
							  <th width="25%"><s:text name="garuda.common.lable.addTestOther"></s:text><br></br>
							  <s:text name="garuda.common.lable.testDate"></s:text>
								 <s:date name="otherPatLabsPojoFive.testdate" id="datepicker98" format="MMM dd, yyyy" />
							     <s:textfield readonly="true" onkeydown="cancelBack();"  name="otherTestDateStrn6" id="otherTestDateStrn6" value="%{datepicker98}" cssClass="datePicWMaxDate" cssStyle="width:210px;"></s:textfield>
 							  </th>
						 </s:if>
						<%--  <%headerCount=headerCount+1; %> --%>
					</tr>	                      <%--   <th width="25%"><s:text name="garuda.common.lable.addTesting"></s:text></th> --%>
				</thead>	
					<tbody>
						<% int testCount=1; %> 
						<s:iterator value="processingTest" var="rowVal" status="row">
								<tr style="width: 100%;" id="<s:property value="%{#row.index}"/>"
								  onMouseOver="highlight('<s:property value="%{#row.index}"/>')"
								   onmouseout="removeHighlight('<s:property   value="%{#row.index}"/>')">
									<% request.setAttribute("testCount",testCount); %>
								  <s:if test="labtestHower!=' '"> 										            	
								      <td width="30%" ><s:property value="labtestName"/></td>
								  </s:if>
								  <s:else>
									  <td width="30%" onmouseover="return overlib('<s:property value="labtestHower"/>');"
											onmouseout="return nd();"> <s:property value="labtestName"/>
									  </td>
								  </s:else>	
								<%-- 	<% int precount=0; %> --%>
								<s:set name="checkEmpty" value="0" scope="request"/>
									<td class="BasicText" align="left">
									 <s:iterator value="preTestList">
						            <%--  ondblclick="addInfo('<s:property value="cdrCbuPojo.fkSpecimenId" />','<s:property value="fktestid" />','<s:property value="pkpatlabs" />');	 
								          <s:property value="cdrCbuPojo.fkSpecimenId" />	  <s:property value="pkLabtest" /> <s:property value="fktestid" /><s:property value="pkpatlabs" /> 
									 <s:property value="pkLabtest" /> <s:property value="fktestid" /> <s:property value="pkpatlabs" />  --%>
									 		
									   	  <s:if test="pkLabtest==fktestid">
									   	   <s:if test="#request.valuePreFkTimingTest==fkTimingOfTest">
									   	   <s:set name="checkEmpty" value="1" scope="request"/>
									   	<%--   <%precount++; %> --%>
									   	   <%-- preeee<s:property value="fkTimingOfTest"/> --%>
									   		<s:set name="check" value="%{'check'}" />
												   <s:hidden name="savePreTestList[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
												   <s:hidden name="savePreTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden> 
												   <s:hidden name="savePreTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePreFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><span style="color: red;">*</span></s:if>
												<s:else><span>&nbsp;&nbsp;</span></s:else>
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count' || labtestName=='CBU Nucleated Cell Count Concentration'">
														<s:text name="garuda.cbuentry.label.testresult"></s:text>
														<input style="width:50%" type="text" id="savePreTestList<s:property value="%{#row.index}"/>testresult" name="savePreTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" style=""size="10" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'pre')" <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">class="labclass positive"</s:if><s:else>class="positive"</s:else> />
					 									<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
					 							</s:if>	
												<s:else>
												
													 <s:set name="testresult" value="%{testresult}" scope="request"/>
													<s:if test="#request.testresult != null">
													<s:text name="garuda.cbuentry.label.testresult"></s:text>
													<s:textfield name="savePreTestList[%{#row.index}].testresult" id="savePreTestList%{#row.index}testresult" cssClass="width:50%" cssClass="positive" value="%{testresult}" size="10" theme="simple" readonly="true" onkeydown="cancelBack();"  onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'pre')" /> 
													<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
												   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
												   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
												   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
												   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
												   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
												   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Viability'">%</s:if>
												   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
													<s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePreTestList[%{#row.index}].fktestspecimen" id="savePreTestList%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
													<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
													</s:if> 
												</s:else>
													   <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
													   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
						            	    
					            	  </s:if>
					            	  
					               </s:if>	
					               	  
								  </s:iterator>
								    <s:if test="#request.checkEmpty==0 && (preTestList.size()>0)">
					               		 		&nbsp;
					               	</s:if> 
					               
									<%--<%=precount%> --%>
							           <s:if test="%{#check!='check'}">	
								           
								          
									             <s:hidden name="savePreTestList[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
									             <s:hidden name="savePreTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden> 
									              <s:hidden name="savePreTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePreFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><span style="color: red;">*</span></s:if>
												<s:else><span>&nbsp;&nbsp;</span></s:else>
												<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count' || labtestName=='CBU Nucleated Cell Count Concentration'">		 
					 								 <s:text name="garuda.cbuentry.label.testresult"></s:text>
					 								 <input type="text" style="width:50%" id="savePreTestList<s:property value="%{#row.index}"/>testresult" name="savePreTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'pre')" <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'">class="labclass positive"</s:if><s:else>class="positive"</s:else> />
				 							    	<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
				 							    </s:if>	
											    <s:else>
											    <s:set name="testresult" value="%{testresult}" scope="request"/>
												<s:if test="#request.testresult != null">
												<s:text name="garuda.cbuentry.label.testresult"></s:text>
												<s:textfield cssClass="width:50%" cssClass="positive" id="savePreTestList%{#row.index}testresult" name="savePreTestList[%{#row.index}].testresult" value="%{testresult}" size="10" theme="simple" readonly="true" onkeydown="cancelBack();" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'pre')" />
												<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
												<s:text name="garuda.common.label.sampType"></s:text>
												<s:select name="savePreTestList[%{#row.index}].fktestspecimen" id="savePreTestList%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
												<br/><span style="display:none" id="savePreTestList<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
												</s:if> 
											  	</s:else>
													<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												    <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
												    <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;/<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
					                	   
 	                                    </s:if></td>
 	                           			<%-- <% int postcount=0;  %> --%>
										<s:set name="checkEmpty" value="0" scope="request"/>
										<s:set name="cellCountFlag" value="0" scope="request"/>
 							           <td class="BasicText" align="left">
 							            <s:set name="check1" value="%{'check2'}"/>
 							           <s:iterator value="postTestList">
 									           	  <%-- <s:property value="fktestid" />
 									     post<s:property value="fkTimingOfTest"/>  --%>
 									     <s:if test="pkLabtest == #request.TNCFkTestId && fktestid==#request.TNCUncFkTestId">
 									     <s:if test="#request.valuePostFkTimingTest==fkTimingOfTest">
									     <s:if test="#request.cellCountFlag==0">
									        <s:set name="checkEmpty" value="1" scope="request"/>  
									        <s:set name="check1" value="%{'check'}"/>
									         	   <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
												   <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												   <span style="color: red;">*</span><s:text name="garuda.cbuentry.label.testresult"></s:text>
									           	    <s:iterator value="postTestList" status="innerrow">
									           	    <s:if test="fktestid==#request.TNCUncFkTestId">
									           	    <s:if test="#request.cellCountFlag==0">
											       	<input type="text" style="width:45%" id="savePostTestList<s:property value="%{#row.index}"/>testresult"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')"  <s:if test="labtestName=='Total CBU Nucleated Cell Count' || labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CD34+ Cell Count' || labtestName=='CBU nRBC Absolute Number' || labtestName=='CBU nRBC %' || labtestName=='CFU Count' || labtestName=='Viability'">class="labclass positive"</s:if><s:else>class="positive"</s:else> />
											       	<br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
											       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
											       	<s:set name="cellCountFlag" value="1" scope="request"/>
											       	</s:if></s:if>
											       	</s:iterator>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										      </s:if></s:if>
 									     </s:if>
									     <s:elseif test="pkLabtest==fktestid &&!(pkLabtest == #request.TNCFkTestId) && (labtestName!='NAT HIV' && labtestName!='NAT HCV')">
									        <s:if test="#request.valuePostFkTimingTest==fkTimingOfTest">
									        	<s:set name="checkEmpty" value="1" scope="request"/>  
									       <%--  <%postcount++;%> --%>
									     <%--   <%=postcount %>--%>
									         <%-- post<s:property value="fkTimingOfTest" /> --%>
									          
									          <s:set name="check1" value="%{'check'}"/>
									         <%-- <%if(postcount>precount){%>
									         <td>&nbsp;</td> 
									         <%} %> --%>
									           
											       <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
												   <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Viability' || labtestName=='Total CD34+ Cell Count' || labtestName=='CBU nRBC Absolute Number' || labtestName=='CBU nRBC %' || labtestName=='CFU Count' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'"><span style="color: red;">*</span></s:if>
												   <%-- <s:if test=""><span style="color: red;">*</span></s:if>
												   <s:if test=" "><span style="color: red;">*</span></s:if>
												   <s:if test=""><span style="color: red;">*</span></s:if>
												   <s:if test=""><span style="color: red;">*</span></s:if>
												  <s:if test=""><span style="color: red;">*</span></s:if> --%>
												  <s:else><span>&nbsp;&nbsp;</span></s:else>
												
												  <%--  <s:textfield name="savePostTestList[%{#row.index}].testresult" value="%{testresult}" size="10" theme="simple" /> --%>
												
											      
											      <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      	&nbsp;
											      </s:if>
											      <s:else>
											      	<s:text name="garuda.cbuentry.label.testresult"></s:text>
						                        	<input type="text" id="savePostTestList<s:property value="%{#row.index}"/>testresult" style="width:50%" name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')"<s:if test="labtestName=='Total CBU Nucleated Cell Count' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)' || labtestName=='Total CD34+ Cell Count' || labtestName=='CBU nRBC Absolute Number' || labtestName=='CBU nRBC %' || labtestName=='Viability' || labtestName=='CFU Count'">class="labclass positive autodeferclass"</s:if><s:else>class="positive"</s:else><s:if test="labtestName=='Viability'">  onblur="autoDefer(this.value,this.id,0,'labclass',this)" </s:if><s:if test="labtestName=='CFU Count'"> onblur="autoDeferCFU(this.value,this.id,0,'labclass',this)"</s:if> />
												   <br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
												   <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if> 
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
												   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
												   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
												   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
												   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
												   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
												   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
												   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>&nbsp;/<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Viability'">%</s:if>
												   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   </s:else>
												 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL">
												   <div id="savePostTestListcfuCount"> 
												   <br><span style="color: red;">*</span><s:text name="garuda.common.lable.method"></s:text>
												     <span>&nbsp;&nbsp;</span><s:select name="savePostTestList[%{#row.index}].fktestmethod"   cssClass="labclass" id="cfuSelected" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="cfuCount(this.value)"></s:select>
													   </div>
													   <div id="cfuCount" > 
														 <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
														 <s:textfield name="savePostTestList[%{#row.index}].testMthdDesc" id="cfuDescOther" maxLength="50" value="%{testMthdDesc}" maxlength="50" onkeypress="validate();"></s:textfield>
													   </div>
												 </s:if>
												  
												   <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
													<div id="savePostTestListviability"> 
													<br><span style="color: red;">*</span><s:text name="garuda.common.lable.method"></s:text>
													    <span>&nbsp;&nbsp;</span><s:select name="savePostTestList[%{#row.index}].fktestmethod"   cssClass="labclass" id="viabSelected" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
												     </div>
												      <div id="viability"> 
														<span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
														<s:textfield name="savePostTestList[%{#row.index}].testMthdDesc" maxLength="50" value="%{testMthdDesc}" id="viaDescOther"  maxlength="50" onkeypress="validate"></s:textfield>
													  </div>													 
												   </s:if>
												   <s:if test="#request.testCount>#request.labGroupTypTests">
												   	<s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePostTestList[%{#row.index}].fktestspecimen" id="savePostTestList%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
												   <br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
												   </s:if>
									            
									        </s:if>
									        
									       </s:elseif>
									       
									   </s:iterator>
									     <s:if test="#request.checkEmpty==0 && postTestList.size()>=#request.labGroupTypTests">
					               		 	&nbsp;
					               		</s:if>  
					               		<s:if test="#request.testCount<=#request.labGroupTypTests">
									       <s:if test="%{#check1!='check'}">
									           <s:if test="pkLabtest == #request.TNCFkTestId">
									         		<s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													<s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												   <span style="color: red;">*</span><s:text name="garuda.cbuentry.label.testresult"></s:text>
									           	   <input type="text" style="width:45%" id="savePostTestList<s:property value="%{#row.index}"/>testresult"  name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')"  <s:if test="labtestName=='Total CBU Nucleated Cell Count' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)' || labtestName=='Total CD34+ Cell Count' || labtestName=='CBU nRBC Absolute Number' || labtestName=='CBU nRBC %' || labtestName=='Viability' || labtestName=='CFU Count'">class="labclass positive"</s:if> <s:else>class="positive"</s:else> />
													<br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													<s:text name="garuda.labtest.label.uncrctd"></s:text>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										 
 									     </s:if>
 									     <s:elseif test="!(pkLabtest == #request.TNCFkTestId) && (labtestName!='NAT HIV' && labtestName!='NAT HCV')">
									           	   <s:hidden name="savePostTestList[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
												   <s:hidden name="savePostTestList[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
												   <s:hidden name="savePostTestList[%{#row.index}].fkTimingOfTest" value="%{#request.valuePostFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Viability' || labtestName=='Total CD34+ Cell Count' || labtestName=='CBU nRBC Absolute Number' || labtestName=='CBU nRBC %' || labtestName=='CFU Count' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'"><span style="color: red;">*</span></s:if>
												   <%-- <s:if test=""><span style="color: red;">*</span></s:if>
												   <s:if test=" "><span style="color: red;">*</span></s:if>
												   <s:if test=""><span style="color: red;">*</span></s:if>
												   <s:if test=""><span style="color: red;">*</span></s:if>
												  <s:if test=""><span style="color: red;">*</span></s:if> --%>
												  <s:else><span>&nbsp;&nbsp;</span></s:else>
												   <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      	&nbsp;
											      </s:if>
											      <s:else>
											      	<s:text name="garuda.cbuentry.label.testresult"></s:text>
												   <input type="text" id="savePostTestList<s:property value="%{#row.index}"/>testresult" style="width:50%" name="savePostTestList[<s:property value="%{#row.index}"/>].testresult" value="<s:property value="%{testresult}"/>" size="10" onkeyup="checkRangeMthd(this.id,'<s:property value="%{pkLabtest}"/>',this.value,'post')"<s:if test="labtestName=='Total CBU Nucleated Cell Count' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CD34+ Cell Count' || labtestName=='CBU nRBC Absolute Number' || labtestName=='CBU nRBC %' || labtestName=='Viability' || labtestName=='CFU Count'">class="labclass positive autodeferclass"</s:if><s:else>class="positive"</s:else><s:if test="labtestName=='Viability'"> onblur="autoDefer(this.value,this.id,0,'labclass',this)" </s:if><s:if test="labtestName=='CFU Count'"> onblur="autoDeferCFU(this.value,this.id,0,'labclass',this)"</s:if> />
												   <br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
												   <s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if> 
												   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
										 		   <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
												   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												   <s:if test="labtestName=='CFU Count' ">&nbsp;/&nbsp;10<sup>5</sup></s:if>
												   
												   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
												   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
												   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
												   <s:if test="labtestName=='% of CD34+ Cells in Total Mononuclear Cell Count'">%</s:if>
												   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
												   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
												   <s:if test="labtestName=='Viability'">%</s:if>
												   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
												  </s:else>
											    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL">
												  <div id="savePostTestListcfuCount"> 
												  <br><span style="color: red;">*</span><s:text name="garuda.common.lable.method"></s:text>
												     <span>&nbsp;&nbsp;</span><s:select name="savePostTestList[%{#row.index}].fktestmethod"  cssClass="labclass" id="cfuSelected" value="%{fktestmethod}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="cfuCount(this.value)"></s:select>
												  </div>    	
												  <div id="cfuCount">   
													 <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
													 <s:textfield name="savePostTestList[%{#row.index}].testMthdDesc" maxLength="50" value="%{testMthdDesc}" id="cfuDescOther" maxlength="50"  onkeypress="validate();"></s:textfield>
										             
										          </div>		
										        </s:if>
												
												<s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
										         <div id="savePostTestListviability"> 
										         <br><span style="color: red;">*</span><s:text name="garuda.common.lable.method"></s:text>
										              <span>&nbsp;&nbsp;</span><s:select name="savePostTestList[%{#row.index}].fktestmethod"  cssClass="labclass" id="viabSelected" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
												    </div>  
												   <div id="viability"> 
												     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
												      <s:textfield name="savePostTestList[%{#row.index}].testMthdDesc" maxLength="50" value="%{testMthdDesc}" id="viaDescOther"  maxlength="50" onkeypress="validate"></s:textfield>
												 	  
												  </div>
												</s:if>
									        	<s:if test="#request.testCount>#request.labGroupTypTests">
												   	<s:text name="garuda.common.label.sampType"></s:text>
													<s:select name="savePostTestList[%{#row.index}].fktestspecimen" id="savePostTestList%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
												  <br/><span style="display:none" id="savePostTestList<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
												  </s:if></s:elseif>
									      </s:if></s:if></td>
											<s:if test="otherPostTestList!=null && otherPostTestList.size()>0 ">					  
													<s:set name="checkEmpty" value="0" scope="request"/>
													<s:set name="cellCountFlag" value="0" scope="request"/>
													<td Class="BasicText">
													<s:iterator value="otherPostTestList">
													 <s:if test="pkLabtest == #request.TNCFkTestId && fktestid==#request.TNCUncFkTestId">
 									     				<s:if test="#request.valueOtherFkTimingTest==fkTimingOfTest">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check2" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	    <s:iterator value="otherPostTestList" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																		<span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason"  id="saveOtherPostTestList1%{#row.index}fktestreason"  value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																		<span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																		</div>
																									 	
																	    <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		    <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   		<span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																	   		<s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																   			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																   			</div></s:if>
																   		</div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>								   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
														<s:if test="#request.valueOtherFkTimingTest==fkTimingOfTest">
														 <s:set name="checkEmpty" value="1" scope="request"/>
														 <s:set name="check2" value="%{'check'}"/>
																	<s:set name="test" value="pkLabtest" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:set name="abc" value="%{#row.index}" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
																	 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" cssClass="positive" id="saveOtherPostTestList1%{#row.index}testresult" cssClass="width:50%" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" cssClass="positive" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:50%" size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																			<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																		   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
																           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																		   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																		   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		   <s:if test="labtestName=='Viability'">%</s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>				 
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																																 
																		<div id="showHardDrop1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																		<span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason"  id="saveOtherPostTestList1%{#row.index}fktestreason"  value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value)"></s:select>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																		<span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onkeypress="validate"></s:textfield>
																		</div>
																									 	
																	    <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>						
																	    <s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>	
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.viablityOtherPkVal}',this.value)"></s:select>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.cfuOtherPkVal}',this.value)"></s:select>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   		<span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																	   		<s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeypress="validate"></s:textfield>
																   			</div></s:if>
																   		</div></s:else> 
																	<%-- 						  
																	  <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																          <s:text name="garuda.common.lable.viability"></s:text>
																          <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		  <s:text name="garuda.common.lable.describe"></s:text>
																		  <s:textfield name="saveOtherPostTestList1[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	 </s:if> --%>
											 				 </s:if>	 
											 				</s:elseif>
										 				</s:iterator>
										 				  <s:if test="#request.checkEmpty==0 && otherPostTestList.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if> 
										               	<s:if test="#request.testCount<=#request.labGroupTypTests"> 
										 		    <s:if test="%{#check2!='check'}">
										 		           <s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		<s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																		<span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason"  id="saveOtherPostTestList1%{#row.index}fktestreason"  value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																		<span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																		</div>
																									 	
																	    <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   		<span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																	   		<s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																   			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																   			</div></s:if>
																   		</div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList1[%{#row.index}].fkTimingOfTest" value="%{#request.valueOtherFkTimingTest}" id="fkTimingOfTest%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%"id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
											     						<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     				</s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList1<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList1[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList1[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList1%{#row.index}testresult" value="%{testresult}" cssClass="width:50%" size="10" theme="simple" onchange="vidSaved('showHardDrop1%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>		
																	 <s:set name="test" value="pkLabtest" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 
												                   <div id="showHardDrop1<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																		<span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																		<s:select name="saveOtherPostTestList1[%{#row.index}].fktestreason"  id="saveOtherPostTestList1%{#row.index}fktestreason"  value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm1%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																		<div id="reasonFrTestDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																		<span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		<s:textfield name="saveOtherPostTestList1[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}reasonTestDesc"  value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																		</div>
																									 	
																	    <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																	    <s:select name="saveOtherPostTestList1[%{#row.index}].fktestspecimen" id="saveOtherPostTestList1%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																		<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																	    <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" id="saveOtherPostTestList1%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm1%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm1<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																	   		<span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																	   		<s:textfield name="saveOtherPostTestList1[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList1%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																   			<br/><span style="display:none" id="saveOtherPostTestList1<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																   			</div></s:if>
																   		</div></s:else>
													               <%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																       <s:text name="garuda.common.lable.viability"></s:text>
																       <s:select name="saveOtherPostTestList1[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																	   <s:text name="garuda.common.lable.describe"></s:text>
																	   <s:textfield name="saveOtherPostTestList1[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																 </s:if> --%>
														  </s:elseif>   	 
											        </s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										     </s:if>
										     
										     
											<s:if test="otherPostTestListFirst!=null && otherPostTestListFirst.size()>0">
												<s:set name="checkEmpty" value="0" scope="request"/>					  
												<td Class="BasicText">
												<s:iterator value="otherPostTestListFirst">
														<s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestFirst==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check3" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValue%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	    <s:iterator value="otherPostTestListFirst" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		 
																			  <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>																																      
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason"  id="saveOtherPostTestList2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																										 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">					 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								    <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																	   		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																	   		</div></s:if>
																	   </div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>														   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
														 <s:if test="#request.valueOtherPostTestFirst==otherListValue">
														 	<s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check3" value="%{'check'}"/>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValue%{#row.index}"></s:hidden>
																	 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
											     					 	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	   <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"></s:textfield> 
																					<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																				<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																			   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
																	           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																			   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																			   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																			   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																			   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																			   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																			   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																			   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																			   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																			   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																			   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																			   <s:if test="labtestName=='Viability'">%</s:if>
																			   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																			   						    
																      <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																			
																	 <div id="showHardDrop2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		 
																			  <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>																																      
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason"  id="saveOtherPostTestList2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																										 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">					 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								    <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																	   		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																	   		</div></s:if>
																	   </div>
																</s:else>
																
																	<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																           <s:text name="garuda.common.lable.viability"></s:text>
																           <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		   <s:text name="garuda.common.lable.describe"></s:text>
																		   <s:textfield name="saveOtherPostTestList2[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	 </s:if> --%>
																
																
											 				 </s:if>
											 				 
											 				</s:elseif>
											 					
										 				</s:iterator>
										 				 <s:if test="#request.checkEmpty==0 && otherPostTestListFirst.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>  
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check3!='check'}">
										 		    	<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		 
																			  <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>																																      
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason"  id="saveOtherPostTestList2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																										 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">					 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								    <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																	   		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																	   		</div></s:if>
																	   </div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList2[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList2[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFirst}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList2<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList2[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList2[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList2%{#row.index}testresult" value="%{testresult}" size="10" theme="simple" onchange="vidSaved('showHardDrop2%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" >  </s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																			<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																		   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
																           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																		   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																		   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		   <s:if test="labtestName=='Viability'">%</s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>					
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
																	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>			
																		
																	 <div id="showHardDrop2<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>		 
																			  <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>																																      
																			  <s:select name="saveOtherPostTestList2[%{#row.index}].fktestreason"  id="saveOtherPostTestList2%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm2%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList2[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																										 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList2[%{#row.index}].fktestspecimen" id="saveOtherPostTestList2%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">					 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								    <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" id="saveOtherPostTestList2%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm2%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm2<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList2[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList2%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																	   		<br/><span style="display:none" id="saveOtherPostTestList2<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																	   		</div></s:if>
																	   </div></s:else>    
																		    
																	 
																	    <%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList2[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList2[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if> --%>
													          	</s:elseif>
													          </s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/> 
										                  </s:if>	     
										     
										     
		                         <s:if test="otherPostTestListSecond!=null && otherPostTestListSecond.size()>0 ">
		                         	<s:set name="checkEmpty" value="0" scope="request"/>					  
												<td Class="BasicText">
												<s:iterator value="otherPostTestListSecond">
												 <s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestSecond==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check4" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	    <s:iterator value="otherPostTestListSecond" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason"  id="saveOtherPostTestList3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
															           		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
															           		</div></s:if>
															           </div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>											   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
												 	<s:if test="#request.valueOtherPostTestSecond==otherListValue">
												 		<s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check4" value="%{'check'}"/>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
											                         	
																	 <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValue%{#row.index}"></s:hidden>
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
											     					 	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	   <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"></s:textfield> 
																			<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																				<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																			   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
																	           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																			   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																			   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																			   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																			   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																			   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																			   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																			   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																			   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																			   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																			   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																			   <s:if test="labtestName=='Viability'">%</s:if>
																			   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>				   
																       <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																		
																		 <div id="showHardDrop3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason"  id="saveOtherPostTestList3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
															           		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
															           		</div></s:if>
															           </div></s:else>
															
																		<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList3[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if>
																  --%>
											 				</s:if>
											 			 </s:elseif>	
										 				</s:iterator>
										 				 <s:if test="#request.checkEmpty==0 && otherPostTestListSecond.size()>=#request.labGroupTypTests">
										               		 		&nbsp; 
										               	</s:if>  
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check4!='check'}">
										 		    		<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason"  id="saveOtherPostTestList3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
															           		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
															           		</div></s:if>
															           </div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList3[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList3[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestSecond}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
											     					 	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList3<s:property value="%{#row.index}"/>div">
																    <s:hidden name="saveOtherPostTestList3[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList3[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList3%{#row.index}testresult" value="%{testresult}" size="10" theme="simple" onchange="vidSaved('showHardDrop3%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')">  </s:textfield>
																		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																			<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																		   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
																           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																		   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																		   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		   <s:if test="labtestName=='Viability'">%</s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>					
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												                     <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
												                      
												                      <div id="showHardDrop3<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
																			  <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>																																												      
																			  <s:select name="saveOtherPostTestList3[%{#row.index}].fktestreason"  id="saveOtherPostTestList3%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm3%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList3[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList3%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList3[%{#row.index}].fktestspecimen" id="saveOtherPostTestList3%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" id="saveOtherPostTestList3%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm3%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm3<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		    <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList3[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList3%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
															           		<br/><span style="display:none" id="saveOtherPostTestList3<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
															           		</div></s:if>
															           </div></s:else>
																		<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList3[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList3[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	    </s:if> --%>
											                  </s:elseif>
											                  </s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										               </s:if>	     

					<s:if test="otherPostTestListThird!=null && otherPostTestListThird.size()>0 ">	
					<s:set name="checkEmpty" value="0" scope="request"/>				  
												<td Class="BasicText">
												<s:iterator value="otherPostTestListThird">
												  	<s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestThird==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check5" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValue%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	    <s:iterator value="otherPostTestListThird" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>
													       	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>      
																	        <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason"  id="saveOtherPostTestList4%{#row.index}fktestreason"  value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
														                	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
														                	</div></s:if>
														                </div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>									   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
												  		<s:if test="#request.valueOtherPostTestThird==otherListValue">
														  <s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check5" value="%{'check'}"/>
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValue%{#row.index}"></s:hidden>
																	 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
											     					 	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" cssClass="positive" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:50%" size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"></s:textfield> 
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																			<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																		   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
																           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																		   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																		   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		   <s:if test="labtestName=='Viability'">%</s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>					
																     <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																			
																	<div id="showHardDrop4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>      
																	        <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason"  id="saveOtherPostTestList4%{#row.index}fktestreason"  value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			 <span style="color: red;">*</span> <s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
														                	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
														                	</div></s:if>
														                </div></s:else>
																	  <%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																           <s:text name="garuda.common.lable.viability"></s:text>
																           <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		   <s:text name="garuda.common.lable.describe"></s:text>
																		   <s:textfield name="saveOtherPostTestList4[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if> --%>
																
											 				  </s:if>
											 				 </s:elseif> 				
										 				</s:iterator>
										 				  <s:if test="#request.checkEmpty==0 && otherPostTestListThird.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>  
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check5!='check'}">
										 		    	<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>      
																	        <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason"  id="saveOtherPostTestList4%{#row.index}fktestreason"  value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
														                	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
														                	</div></s:if>
														                </div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList4[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList4[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestThird}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
											     						<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     				</s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList4<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList4[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}" onchange="vidSaved('showHardDrop4%{#row.index}',this.value)"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList4[%{#row.index}].testresult" id="saveOtherPostTestList4%{#row.index}testresult" value="%{testresult}"  cssClass="width:50%" size="10" theme="simple" onchange="vidSaved('showHardDrop4%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"></s:textfield>
																		<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>					
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												                     <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	        
																	  <div id="showHardDrop4<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>      
																	        <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList4[%{#row.index}].fktestreason"  id="saveOtherPostTestList4%{#row.index}fktestreason"  value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm4%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList4[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList4%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList4[%{#row.index}].fktestspecimen" id="saveOtherPostTestList4%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">						 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" id="saveOtherPostTestList4%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm4%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm4<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList4[%{#row.index}].testMthdDesc"  maxlength="50" id="saveOtherPostTestList4%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
														                	<br/><span style="display:none" id="saveOtherPostTestList4<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
														                	</div></s:if>
														                </div></s:else>
																		<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList4[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		    <s:text name="garuda.common.lable.describe"></s:text>
																		    <s:textfield name="saveOtherPostTestList4[%{#row.index}].notes" value="%{notes}" maxlength="50" onkeypress="validate"></s:textfield>
																	   </s:if> --%>
											              </s:elseif>
											              </s:if></s:if></td><s:set name="cellCountFlag" value="0" scope="request"/>
										         </s:if>	     
			
								<s:if test="otherPostTestListFourth!=null && otherPostTestListFourth.size()>0 ">					  
												<s:set name="checkEmpty" value="0" scope="request"/>
												<td Class="BasicText">
												<s:iterator value="otherPostTestListFourth">
												  <s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestFourth==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check6" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValue%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	    <s:iterator value="otherPostTestListFourth" status="innerrow">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop5%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>	
																			  <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason"  id="saveOtherPostTestList5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																</div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>											   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
													<s:if test="#request.valueOtherPostTestFourth==otherListValue">
														  <s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check6" value="%{'check'}"/>
														   			 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValue%{#row.index}"></s:hidden>
																	<s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
											     					 	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	  <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onchange="vidSaved('showHardDrop5%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"></s:textfield> 
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																			<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																		   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
																           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																		   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																		   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																		   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																		   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																		   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																		   <s:if test="labtestName=='Viability'">%</s:if>
																		   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>					
																		<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																		
																		<div id="showHardDrop5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>	
																			  <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason"  id="saveOtherPostTestList5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																		</div></s:else>
																	<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																            <s:text name="garuda.common.lable.viability"></s:text>
																            <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].notes" value="%{notes}" maxlength="50" onkeypress="validate"></s:textfield>
																		 	 
																	   </s:if> --%>
																
																
																
											 				</s:if>
											 			  </s:elseif>
										 				</s:iterator>
										 				  <s:if test="#request.checkEmpty==0 && otherPostTestListFourth.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if>  
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    <s:if test="%{#check6!='check'}">
										 		   			<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop5%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>	
																			  <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason"  id="saveOtherPostTestList5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																		</div>
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList5[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList5[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFourth}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      						&nbsp;
											     				 </s:if>
											     				 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
											     					 	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     				 <s:else>
																 <div id="saveOtherPostTestList5<s:property value="%{#row.index}"/>div">
																 <s:hidden name="saveOtherPostTestList5[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList5[%{#row.index}].testresult" cssClass="positive" id="saveOtherPostTestList5%{#row.index}testresult" value="%{testresult}"  cssClass="width:50%" size="10" theme="simple"  onchange="vidSaved('showHardDrop5%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')">  </s:textfield>
																		<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>							
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												                     <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	<div id="showHardDrop5<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>	
																			  <span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																			  <s:select name="saveOtherPostTestList5[%{#row.index}].fktestreason"  id="saveOtherPostTestList5%{#row.index}fktestreason" value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm5%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList5[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList5[%{#row.index}].fktestspecimen" id="saveOtherPostTestList5%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>					
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>						 
																		     <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" id="saveOtherPostTestList5%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm5%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm5<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList5%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList5<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																		</div></s:else>
																		 
	
																	    <%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																             <s:text name="garuda.common.lable.viability"></s:text>
																             <s:select name="saveOtherPostTestList5[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList5[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	   </s:if>
											          					--%>
											         </s:elseif>
											         </s:if> </s:if> </td><s:set name="cellCountFlag" value="0" scope="request"/>
										     </s:if>	     
			
													     
								<s:if test="otherPostTestListFifth!=null && otherPostTestListFifth.size()>0 ">					  
										<s:set name="checkEmpty" value="0" scope="request"/>
										<td Class="BasicText">
										<s:iterator value="otherPostTestListFifth">
											<s:if test="pkLabtest == #request.TNCFkTestId && (fktestid==#request.TNCUncFkTestId)">
 									     				<s:if test="#request.valueOtherPostTestFifth==otherListValue">
													     <s:if test="#request.cellCountFlag==0">
													        <s:set name="checkEmpty" value="1" scope="request"/>  
													        <s:set name="check7" value="%{'check'}"/>
											         		<s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValue%{#row.index}"></s:hidden>
														    <div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	    <s:iterator value="otherPostTestListFifth" status="innerrow">
											           	    <s:if test="fktestid==#request.TNCUncFkTestId">
											           	    <s:if test="#request.cellCountFlag==0">
											           	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
													       	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       <div id="showHardDrop6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason"  id="saveOtherPostTestList6%{#row.index}fktestreason"  value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																			</div>
													       	<s:set name="cellCountFlag" value="1" scope="request"/>
													       	</s:if></s:if>
													       	</s:iterator>											   
											      			</s:if></s:if>
 									    				 </s:if>
									    				 <s:elseif test="pkLabtest==fktestid && !(pkLabtest == #request.TNCFkTestId)">
												 		<s:if test="#request.valueOtherPostTestFifth==otherListValue">
														<s:set name="checkEmpty" value="1" scope="request"/>
														   <s:set name="check7" value="%{'check'}"/>
														   			<s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												
																	 <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																	 <s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValue%{#row.index}"></s:hidden>
																	 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      						&nbsp;
											     					 </s:if>
											     					 <s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
																	 	 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
											     					 	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     					 <s:else>
																	 <div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	 <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 </div>
																	 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 <s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" cssClass="positive" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:50%" size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"></s:textfield> 
																		 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>						   
																	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																		
																	<div id="showHardDrop6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason"  id="saveOtherPostTestList6%{#row.index}fktestreason"  value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																			</div></s:else>
																		<%-- <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																             <s:text name="garuda.common.lable.viability"></s:text>
																             <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	    </s:if> --%>
																
											 			 </s:if>
											 	    </s:elseif>
										 	</s:iterator>
										 	  <s:if test="#request.checkEmpty==0 && otherPostTestListFifth.size()>=#request.labGroupTypTests">
										               		 		&nbsp;
										               	</s:if> 
										               	<s:if test="#request.testCount<=#request.labGroupTypTests">
										 		    	<s:if test="%{#check7!='check'}">
										 		    	<s:if test="pkLabtest == #request.TNCFkTestId">
     																<s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{#request.TNCUncFkTestId}" id="test%{#row.index}"></s:hidden>  
															  		 <s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValue%{#row.index}"></s:hidden>
														   <div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	    <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
															</div>
														  	<s:text name="garuda.cbuentry.label.testresult"></s:text>
											           	   	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}" cssClass="width:45%"  size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')"> </s:textfield> 
													       	 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
													       	 <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
													       	<s:text name="garuda.labtest.label.uncrctd"></s:text>
													       	<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>
													       	<div id="showHardDrop6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason"  id="saveOtherPostTestList6%{#row.index}fktestreason"  value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																			</div>
																		<s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
											      			
 									    				 </s:if>
									    				 <s:elseif test="!(pkLabtest == #request.TNCFkTestId)">
																 <s:hidden name="saveOtherPostTestList6[%{#row.index}].fktestid" value="%{pkLabtest}" id="test%{#row.index}"></s:hidden>  
																 <s:hidden name="saveOtherPostTestList6[%{#row.index}].otherListValue" value="%{#request.valueOtherPostTestFifth}" id="otherListValue%{#row.index}"></s:hidden>
																 <s:if test="labtestName=='CBU volume (without anticoagulant/additives)' || labtestName=='Total CBU Nucleated Cell Count'">
											      						&nbsp;
											     				</s:if>
											     				<s:elseif test="#request.testCount>#request.labGroupTypTests">
											     					 	<div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																	    	<s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																	 	</div>
																	 	<s:text name="garuda.cbuentry.label.testresult"></s:text>
																	 	<s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" cssClass="positive" cssClass="width:50%" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}"  size="10" theme="simple" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" > </s:textfield> 
																	 	 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																	 	<s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																	 	<s:text name="garuda.common.label.sampType"></s:text>
																		<s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen"  value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
											     					 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name=""></s:text> </span>
											     					 </s:elseif>
											     				<s:else>
																 <div id="saveOtherPostTestList6<s:property value="%{#row.index}"/>div">
																   <s:hidden name="saveOtherPostTestList6[%{#row.index}].pkpatlabs" value="%{pkpatlabs}" id="patlabs%{#row.index}"></s:hidden>
																 </div>
																 <s:text name="garuda.cbuentry.label.testresult"></s:text>
																 <s:textfield name="saveOtherPostTestList6[%{#row.index}].testresult" cssClass="positive" id="saveOtherPostTestList6%{#row.index}testresult" value="%{testresult}"  cssClass="width:50%"  size="10" theme="simple" onchange="vidSaved('showHardDrop6%{#row.index}',this.value)" onkeyup="checkRangeMthd(this.id,'%{pkLabtest}',this.value,'otherThaw')" >  </s:textfield>
																		 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testresult_error" class="error"><s:text name=""></s:text> </span>
																		<s:if test="labtestName=='CBU volume (without anticoagulant/additives)'"><s:text name="garuda.cbbprocedures.label.millilitres"/></s:if> 
																	   <s:if test="labtestName=='Total CBU Nucleated Cell Count'">x&nbsp;x&nbsp;&nbsp;10<sup>7</sup></s:if>												   
															           <s:if test="labtestName=='Total CBU Nucleated Cell Count (unknown if uncorrected)' || labtestName=='Total CBU Nucleated Cell Count (uncorrected)'">x&nbsp;&nbsp;10<sup>7</sup></s:if>   
																	   <s:if test="labtestName=='Total CD34+ Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='Total CD3 Cell Count'">x&nbsp;&nbsp;10<sup>6</sup></s:if>
																	   <s:if test="labtestName=='CFU Count'">&nbsp;/&nbsp;10<sup>5</sup></s:if>
																	   <s:if test="labtestName=='CBU nRBC %'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+  Viable'">%</s:if>
																	   <s:if test="labtestName==' % of Mononuclear Cells in Total Nucleated Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% of CD34+ Cells in Total Monunuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='% CD3 Cells in Total Mononuclear Cell Count'">%</s:if>
																	   <s:if test="labtestName=='CBU Nucleated Cell Count Concentration'">x&nbsp;&nbsp;10<sup>3</sup>/&nbsp;<s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Final Product Volume'"><s:text name="garuda.cbbprocedures.label.ml"/></s:if>
																	   <s:if test="labtestName=='Viability'">%</s:if>
																	   <s:if test="labtestName=='CBU nRBC Absolute Number'">x&nbsp;&nbsp;10<sup>6</sup></s:if>								
																	 <s:set name="test" value="fktestid" scope="request"/>
																	 <s:set name="patlabs" value="pkpatlabs" scope="request"/>
																	 <s:set name="fkSpeciId" value="cdrCbuPojo.fkSpecimenId" scope="request"/>
												                      <s:set name="checkTextValue" value="%{testresult}" scope="request"/>
																		<div id="showHardDrop6<s:property value="%{#row.index}"/>"<s:if test="#request.checkTextValue=='' || #request.checkTextValue==null ">style="display:none;"</s:if>>
											         						<span style="color: red;">*</span><s:text name="garuda.common.label.reaForTest"></s:text>
																			<s:select name="saveOtherPostTestList6[%{#row.index}].fktestreason"  id="saveOtherPostTestList6%{#row.index}fktestreason"  value="%{fktestreason}"  list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_REASON]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('reasonFrTestDescOthTm6%{#row.index}','%{#request.testReasonOtherPkVal}',this.value,this.id)"></s:select>
																			 <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestreason_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			 <s:set name="reasonFrTestSelectVal" value="%{fktestreason}" scope="request"/>
																			 
																			 <div id="reasonFrTestDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.reasonFrTestSelectVal!=#request.testReasonOtherPkVal">style="display:none;"</s:if>> 
																			  <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																			  <s:textfield name="saveOtherPostTestList6[%{#row.index}].reasonTestDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}reasonTestDesc" value="%{reasonTestDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>reasonTestDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div>
																									 	
																		      <span style="color: red;">*</span><s:text name="garuda.common.label.sampType"></s:text>
																		      <s:select name="saveOtherPostTestList6[%{#row.index}].fktestspecimen" id="saveOtherPostTestList6%{#row.index}fktestspecimen" value="%{fktestspecimen}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SAMP_TYPE]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('notReq','notReq',this.value,this.id)"></s:select>
																				<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestspecimen_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																			<s:if test='#request.isTasksRequiredFlag == false || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL || labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT_VAL'>							
																			 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">				 
																		     <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>	
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.viablityOtherPkVal}',this.value,this.id)"></s:select>
																		    	<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		    	<s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:if>
																		    <s:else>						
																			 <span style="color: red;">*</span><s:text name="garuda.common.label.testMthd"></s:text>
									      								     <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" id="saveOtherPostTestList6%{#row.index}fktestmethod" value="fktestmethod" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CFU_COUNT]" listKey="pkCodeId" listValue="description" headerKey="-1" headerValue="Select" onchange="showDescField('testMethDescOthTm6%{#row.index}','%{#request.cfuOtherPkVal}',this.value,this.id)"></s:select>
																		     <br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>fktestmethod_error" class="error"><s:text name="garuda.common.validation.value"></s:text> </span>
																		     <s:set name="testMethSelectVal" value="%{fktestmethod}" scope="request"/>
																		    </s:else>
																		    <div id="testMethDescOthTm6<s:property value="%{#row.index}"/>"<s:if test="#request.testMethSelectVal!=#request.viablityOtherPkVal && #request.testMethSelectVal!=#request.cfuOtherPkVal">style="display:none;"</s:if>>
																		     <span style="color: red;">*</span><s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].testMthdDesc" maxlength="50" id="saveOtherPostTestList6%{#row.index}testMthdDesc" value="%{testMthdDesc}" onkeyup="chekDepandtFldPrs(this.id,this.value)" onkeypress="validate"></s:textfield>
																			<br/><span style="display:none" id="saveOtherPostTestList6<s:property value="%{#row.index}"/>testMthdDesc_error" class="error"><s:text name="garuda.common.validation.description"></s:text> </span>
																			</div></s:if>
																		</div>
													       		 </s:else>
																		     
											         					<%-- 
											         					 <s:if test="labtestName==@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY_VAL">
																             <s:text name="garuda.common.lable.viability"></s:text>
																             <s:select name="saveOtherPostTestList6[%{#row.index}].fktestmethod" value="%{fktestmethod}" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@VAIBILITY]" listKey="pkCodeId" listValue="description" headerKey="-1"  headerValue="Select Type" onchange="test(this.value)"></s:select>
																		    
																		     <s:text name="garuda.common.lable.describe"></s:text>
																		     <s:textfield name="saveOtherPostTestList6[%{#row.index}].notes" maxlength="50" value="%{notes}" onkeypress="validate"></s:textfield>
																	    </s:if> --%>
																	     </s:elseif>
											            </s:if></s:if>  </td><s:set name="cellCountFlag" value="0" scope="request"/>
										      </s:if>	     
                                        </tr>
                                       <s:if test="#request.testCount==#request.labGroupTypTests && processingTest.size()>#request.labGroupTypTests">
                                        <tr><td colspan="<%=headerCount%>">
                                        <div
											class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
											style="text-align: left;">
											 <s:text name="garuda.cordentry.label.otherLabTests"/>
										</div>
                                       </td>
                                        </tr></s:if>
                                        <% testCount=testCount+1; %> 
                                        <%-- <s:set name="check" value="%{'uncheck'}"/> --%>
                                        <%-- <s:set name="check1" value="%{'uncheck'}"/>
                                        <s:set name="check2" value="%{'uncheck'}"/>
                                        <s:set name="check3" value="%{'uncheck'}"/>
                                        <s:set name="check4" value="%{'uncheck'}"/>
                                        <s:set name="check5" value="%{'uncheck'}"/>
                                        <s:set name="check6" value="%{'uncheck'}"/>
                                        <s:set name="check7" value="%{'uncheck'}"/> --%>
								  </s:iterator>
						     </tbody>
					      </table>      
						</td>
					</tr>
				</table>
				
			               <!-- <div>
						  	
						  </div>	 -->
				
			
		
	
    <s:if test="cdrCbuPojo.rhType==cdrCbuPojo.rhTypeOther">
	   <script>
	   rhSpecify('<s:property value="cdrCbuPojo.rhType"/>');    
	  </script>
	</s:if>
	<%-- <div id="esignForLabSummary" style="display: none;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="labSummarySubmit" bgcolor="#cccccc">
			 <tr valign="baseline" bgcolor="#cccccc">
			   <td><jsp:include page="../cb_esignature.jsp" flush="true">
	   					<jsp:param value="labSummarysubmit" name="submitId"/>
						<jsp:param value="labSummaryinvalid" name="invalid" />
						<jsp:param value="labSummaryminimum" name="minimum" />
						<jsp:param value="labSummarypass" name="pass" />
   					</jsp:include>
   				</td>	
			<td align="center" colspan="2">
			   <input type="button" disabled="disabled" id="labSummarysubmit" onclick="submitTableLabSummary()"  value="<s:text name="garuda.common.lable.confirm"/>" />
			   <input type="button"  onclick="closeModal();" value="<s:text name="garuda.openorder.label.button.cancel"/>" />
			   </td>	
			</tr>
			</table>
	</div>
	<script>
	function submitTableLabSummary(){
	  var url="saveTestWithFields?cordId="+'<s:property value="cdrCbuPojo.cordID"/>';
	   var noMandatory=true;
   		$j('#labSummaryModal .cbuLabSummMandatory').each(function(){
   			 if($j(this).val()==null || $j(this).val()=="" || $j(this).val()=="-1"){
   				 noMandatory=false;
   				}
   			  });
		      url = url+"&esignFlag=review&noMandatory="+noMandatory;
			  modalFormSubmitRefreshDiv('labSummaryForm',url,'finalreviewmodel');
		          $j("#successLab").show();
		          $j("#labSummaryModal").hide();
	}
	</script> --%>