 <%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="userLB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<script>


function manageImportedCords(){
		
		var is_all_entries=$j('#showsRow').val();
		var pageNo=$j('#paginateWrapper1').find('.currentPage').text();
		var showEntries=$j('#cimport_tbl_sentries').val();
		var tEntries=$j('#tentries').val();
		var sort=1;
		var sortDir="desc";
		var SortParm=$j('#cimport_tbl_sort_col').val();
		var seachTxt="";
		var preTxt=$j('#cimport_search_txt').val();
		
		if((preTxt!=null && $j.trim(preTxt)!="")){
			seachTxt=preTxt;
		}

		if(SortParm!=undefined && SortParm!=null && SortParm!=""){
			sort=$j('#cimport_tbl_sort_col').val();
			sortDir=$j('#cimport_tbl_sort_type').val();
		}	

		if((showEntries==null || showEntries==undefined || showEntries=="undefined" || $j.trim(showEntries)=="")){
			is_all_entries='5';
			$j('#cimport_tbl_sentries').val(is_all_entries);
			showEntries=$j('#cimport_tbl_sentries').val();
		}
		
		showEntriesTxt='Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;"><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option><option value="'+tEntries+'" >ALL</option></select> Entries';
		
	    if(showEntries=="ALL"){
	    	
	    	$j('#searchResults').dataTable({
	            "sScrollY": "150",
	            "sDom": "frtiS",
	            "sAjaxSource": 'getJsonPagination.action?TblFind=cordImportHist&allEntries=ALL',
	     		"bServerSide": true,
	     		"bProcessing": true, 
	     		"aaSorting": [[ sort, sortDir ]],
	     		"oSearch": {"sSearch": seachTxt},
	     		"bRetrieve" : true,
	     		"bDeferRender": true,
	     		"bDestroy": true,
	     		"sAjaxDataProp": "listCordImportHistory",
	     		"aoColumnDefs": [
									{		
										   "aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
										    return "";
									}},
									{		
										    "aTargets": [1],"mDataProp": function ( source, type, val ) {
										    	 var key='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@MESSAGE_LEVEL_ZERO"/>';
												    var temp1="getCordImportHistoryErrorMessages?historyPojo.pkCordimportHistory="+source[0]+"&level="+key+"&registryId=";
												    var temp="<span onclick='showModal(\""+"Cord Import Error History"+"\",\""+temp1+"\",\""+"700"+"\",\""+"700"+"\")' >"+source[0]+"</span>";
												    return temp;
									}},
									{
										   "aTargets": [2],"mDataProp": function ( source, type, val ) {
										    return source[1];
									}},
									{
											"aTargets": [3],"mDataProp": function ( source, type, val ) {
											return source[3];
								    }},
								    {
											"aTargets": [4],"mDataProp": function ( source, type, val ) {
											return source[2];
								    }},
								    {
											"aTargets": [5],"mDataProp": function ( source, type, val ) {
											return source[5]+" "+source[6];
								    }},
								    {
											"aTargets": [6],"mDataProp": function ( source, type, val ) {
											return source[4];
								    }},
								    {
											"aTargets": [7],"bSortable": false,"mDataProp": function ( source, type, val ) {
											 return "<span onclick=showImportFile('"+source[0]+"') class='ui-icon ui-icon-arrowthickstop-1-s'></span>";
								    }}
	  						],
	  						"fnDrawCallback": function() { 
	  							$j('#searchResults_wrapper').find('.dataTables_scrollFoot').hide();
							    $j('#searchResults_info').show();
							    $j("#showsRow option:contains('ALL')").attr('selected', 'selected'); 	
							    setSortData('cimportThead','cimport_tbl_sort_col','cimport_tbl_sort_type');
							    //frameOnclick();
	  						}
			});
	    	$j('#searchResults_filter').before(showEntriesTxt);    	
	    	
	    }else{
	    	
	    	$j('#searchResults').dataTable({
	            "sScrollY": "150",
	            "sAjaxSource": 'getJsonPagination.action?TblFind=cordImportHist&allEntries=ALL&iDisplayStart=0&iDisplayLength='+showEntries+'&otherThanAll=1&iPageNo='+pageNo+'&iShowRows='+showEntries,
	            "bServerSide": true,
	            "bProcessing": true, 
	     		"bRetrieve" : true,
				"bDestroy": true,
				"oSearch": {"sSearch": seachTxt},
	     		"aaSorting": [[ sort, sortDir ]],
	     		"bDeferRender": true,
	     		"sAjaxDataProp": "listCordImportHistory",
	     		"aoColumnDefs": [
									{		
										   "aTargets": [0],"bSortable": false,"mDataProp": function ( source, type, val ) {
										    return "";
									}},
									{		
										    "aTargets": [1],"mDataProp": function ( source, type, val ) {
										     var key='<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@MESSAGE_LEVEL_ZERO"/>';
											 var temp1="getCordImportHistoryErrorMessages?historyPojo.pkCordimportHistory="+source[0]+"&level="+key+"&registryId=";
											 var temp="<span onclick='showModal(\""+"Cord Import Error History"+"\",\""+temp1+"\",\""+"700"+"\",\""+"700"+"\")' >"+source[0]+"</span>";
											 return temp;
									}},
									{
										   "aTargets": [2],"mDataProp": function ( source, type, val ) {
										    return source[1];
									}},
									{
											"aTargets": [3],"mDataProp": function ( source, type, val ) {
											return source[3];
								    }},
								    {
											"aTargets": [4],"mDataProp": function ( source, type, val ) {
											return source[2];
								    }},
								    {
											"aTargets": [5],"mDataProp": function ( source, type, val ) {
											return source[5]+" "+source[6];
								    }},
								    {
											"aTargets": [6],"mDataProp": function ( source, type, val ) {
											return source[4];
								    }},
								    {
											"aTargets": [7],"bSortable": false,"mDataProp": function ( source, type, val ) {
											 return "<span onclick=showImportFile('"+source[0]+"') class='ui-icon ui-icon-arrowthickstop-1-s'></span>";
								    }}
	  						],
	  						"fnDrawCallback": function() { 
	  							setSortData('cimportTblThead','cimport_tbl_sort_col','cimport_tbl_sort_type');
	  							$j('#showsRow').val(showEntries);
	  							//frameOnclick();
	  						}
			});
	    	
	    }
		
		
		$j('#searchResults_info').hide();
		$j('#searchResults_paginate').hide();
		$j('#searchResults_length').empty();
		$j('#searchResults_length').replaceWith(showEntriesTxt);
		

		$j('#showsRow').change(function(){
			var textval=$j('#showsRow :selected').html();
			$j('#cimport_tbl_sentries').val(textval);
			paginationFooter(0,"getCordFileUploadPaginationData,showsRow,inputs,Tableform,searchTble1,manageImportedCords",'<s:property value="paginateSearch.getiTotalRows()"/>','<s:property value="paginateModule"/>');
		});	
		
		$j('#searchResults_filter').find(".searching1").keyup(function(e){
			  var val = $j(this).val();
			  val=$j.trim(val);
			  $j('#cimport_search_txt').val(val);
			  if(e.keyCode==13)
			  {
				  paginationFooter(0,"getCordFileUploadPaginationData,showsRow,inputs,Tableform,searchTble1,manageImportedCords","0",'<s:property value="paginateModule"/>');
			  }
		});
}

</script>
 <input type="hidden" name="entries" id="entries" value=<%=paginateSearch.getiShowRows() %> />
  <input type="hidden" name="tentries" id="tentries" value=<%=paginateSearch.getiTotalRows() %> />
 <table width="100%" cellpadding="0" cellspacing="0" border="0" class="displaycdr searchResults" id="searchResults">
		      <thead>
		         <tr id="cimportTblThead">
		             <th></th>
		             <th><s:text name="garuda.cordimport.label.jobid"/></th>
		             <th><s:text name="garuda.cordimport.label.startdate"/></th>
		             <th><s:text name="garuda.cordimport.label.enddate"/></th>
		             <th><s:text name="garuda.cordimport.label.filename"/></th>
		             <th><s:text name="garuda.cordimport.label.importuser"/></th>
		             <th><s:text name="garuda.cordimport.label.result"/></th>	
		             <th><s:text name="garuda.common.lable.view"/></th>					             
		         </tr>
		      </thead>
		      <tbody id="cimportTblTBody">
		          <s:iterator value="#request.listCordImportHistory">
		            <tr>
		               <td></td>
		               <td onclick="showModal('Cord Import Error History','getCordImportHistoryErrorMessages?historyPojo.pkCordimportHistory=<s:property value="pkCordimportHistory"/>&level=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@MESSAGE_LEVEL_ZERO"/>&registryId=','700','700')">
		                  <s:property value="pkCordimportHistory"/>
		               </td>
		               <td>
		                  <s:date name="startTime" id="datepicker3" />
		                  <s:property value="%{datepicker3}"/>
		                  
		               </td>
		               <td>
		                  <s:date name="endTime" id="datepicker4" />
		                  <s:property value="%{datepicker4}"/>
		                  
		               </td>
		               <td>
		                  <s:property value="importFileName"/>
		               </td>
		               <td>
		                   <s:property value="userDomain.firstName"/>&nbsp;<s:property value="userDomain.lastName"/>						
		               </td>
		               <td>
		                  <s:property value="importHistoryDesc"/>
		               </td>
		               <td>
		                  <span onclick="showImportFile('<s:property value="pkCordimportHistory"/>')" class='ui-icon ui-icon-arrowthickstop-1-s'></span>
		               </td>
		            </tr>
		          </s:iterator>
		      </tbody>
		<tfoot>
			<tr>
			   <td colspan="8">
				    <jsp:include page="../paginationFooter.jsp">
					  	<jsp:param value="searchTble1" name="divName"/>
					  	<jsp:param value="Tableform" name="formName"/>
					  	<jsp:param value="showsRow" name="showsRecordId"/>
					  	<jsp:param value="getCordFileUploadPaginationData" name="url"/>
					  	<jsp:param value="inputs" name="cbuid"/>
					  	<jsp:param value="workflow" name="paginateSearch"/>
					    <jsp:param value="1" name="idparam"/>
					  	<jsp:param value="manageImportedCords" name="bodyOnloadFn"/>
				    </jsp:include>
		       </td>      
		   </tr>
	 </tfoot>
</table>