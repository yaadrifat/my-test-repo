<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<jsp:include page="../cb_user_rights.jsp"></jsp:include>
<% String contextpath=request.getContextPath();%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<%
	if(request.getParameter("submit")!=null){
		request.setAttribute("submit",request.getParameter("submit"));
	}
%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
    <script>   
    
    $j(function(){
    	
		  f_getSessionData();    	
		  paginationFooter(0,"getCordSaveInProgressPaginationData,sip_tbl_se,inputs,Tableform,searchTble,constructSavedinProgTable",'<s:property value="cbuCount"/>','<s:property value="paginateModule"/>');
		  paginationFooter(0,"getNotAvailCordsPaginationData,nacbu_tbl_se,inputs,Tableform,searchTble1,constructTableNACBU",'<s:property value="paginationSearch.getiTotalRows()"/>','<s:property value="paginateModule"/>');
		  getDatePic();
			jQuery(function() {
				  jQuery('#dateEnd1, #dateStart1').datepicker('option', {
				    beforeShow: customRange1
				  }).focus(function(){
					  if(!$j("#ui-datepicker-div").is(":visible"))
					  		customRange1(this);
						if($j(this).val()!=""){
							var thisdate = new Date($j(this).val());
							if(!isNaN(thisdate)){
								var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
								$j(this).val(formattedDate);
							}
						}
					});
			});
			jQuery(function() {
				  jQuery('#dateEnd, #dateStart').datepicker('option', {
				    beforeShow: customRange
				  }).focus(function(){
					  if(!$j("#ui-datepicker-div").is(":visible"))
					  		customRange(this);
						if($j(this).val()!=""){
							var thisdate = new Date($j(this).val());
							if(!isNaN(thisdate)){
								var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
								$j(this).val(formattedDate);
							}
						}
					});
			});
		  var validator = $j("#dummyForm").validate({
				invalidHandler: function(form, validator) {
			        var errors = validator.numberOfInvalids();
			        if (errors) {
			            validator.errorList[0].element.focus();
			        }
			    }
			});
 	});
    
    $j(window).unload(function(){
    	//alert("Calling unload");
    	setSessionData();
    });
    
    function f_getSessionData(){
    	
    	var sv_sip_se='<%=session.getAttribute("V_CE_SIP_SE")%>';
    	var sv_nacbu_se='<%=session.getAttribute("V_CE_NACBU_SE")%>';
    	var sv_sip_sc='<%=session.getAttribute("V_CE_SIP_SC")%>';
    	var sv_sip_st='<%=session.getAttribute("V_CE_SIP_ST")%>';
    	var sv_nacbu_sc='<%=session.getAttribute("V_CE_NACBU_SC")%>';
    	var sv_nacbu_st='<%=session.getAttribute("V_CE_NACBU_ST")%>';
    	var sv_sip_fval='<%=session.getAttribute("V_CE_SIP_FVAL")%>';
    	var sv_nacbu_fval='<%=session.getAttribute("V_CE_NACBU_FVAL")%>';
    	var param1="";
    	var param2="";
    	
    	/*********************************************Getting show entries values from session*************************************************/
    	
    	if(sv_sip_se!='null'  && sv_sip_se!=undefined && sv_sip_se!='' && sv_sip_se!='undefined' && sv_sip_se!='0'){
    	    $j('#sip_tbl_se').val(sv_sip_se);
    	}else{
    		$j('#sip_tbl_se').val('05');
    	}
    	
    	
    	if(sv_nacbu_se!='null'  && sv_nacbu_se!=undefined && sv_nacbu_se!='' && sv_nacbu_se!='undefined' && sv_nacbu_se!='0'){
    	    $j('#nacbu_tbl_se').val(sv_nacbu_se);
    	}else{
    		$j('#nacbu_tbl_se').val('05');
    	}
    	
    	
    	/*********************************************Getting sort values from session*************************************************/
    	
    	if(sv_sip_sc!='null'  && sv_sip_sc!=undefined && sv_sip_sc!='' && sv_sip_sc!='undefined' && sv_sip_sc!='0'){
    	    $j('#sip_sortParam').val(sv_sip_sc);
    	    $j('#sip_sort_type').val(sv_sip_st);
    	}
    	
    	if(sv_nacbu_sc!='null'  && sv_nacbu_sc!=undefined && sv_nacbu_sc!='' && sv_nacbu_sc!='undefined'){
    	    $j('#nacbu_sortParam').val(sv_nacbu_sc);
    	    $j('#nacbu_sort_type').val(sv_nacbu_st);
    	}
    	
    	/*********************************************Getting Filtervalues values from session*************************************************/
    	
    	if(sv_sip_fval!='null' && sv_sip_fval!=''){
			param1=sv_sip_fval.replace(/----/gi,'&');
			$j('#sip_filterFlag').val("1");
			$j('#sipStatFilterFlag').val('1');
			$j('#sipfilteparams').val(param1);
		}
    	
    	if(sv_nacbu_fval!='null' && sv_nacbu_fval!=''){
			param2=sv_nacbu_fval.replace(/----/gi,'&');
			$j('#nacbu_filterFlag').val("1");
			$j('#naCBUStatFilterFlag').val('1');
			$j('#nacbufilteparams').val(param2);
		}
    	
    	
    }
    
    function setSessionData(){
    	
    	
    	var sipshowEntries=$j("#showsRow :selected").text();
    	var nacbushowEntries=$j("#showsRow1 :selected").text();
    	var sipLastSortColInd=$j("#sip_sortParam").val();
    	var nacbuLastSortColInd=$j("#nacbu_sortParam").val();
    	var sipLastSortDir=$j("#sip_sort_type").val();
    	var nacbuLastSortDir=$j("#nacbu_sort_type").val();
    	var tbl1_site_filter=$j('#sipfilteparams').val();
   	  	var tbl2_site_filter=$j('#nacbufilteparams').val();
    	var param1="";
    	var param2="";
    	var str=0;
    	var str2=0;
    	var url="";
    	
    	
    	
    	if(tbl1_site_filter!='undefined' && tbl1_site_filter!=''){
  		  
   		   str=tbl1_site_filter.indexOf("&cbuRegId");
   		  
   		   if(str!=-1){
   			  var len=tbl1_site_filter.length;
   			  var removedStr1=tbl1_site_filter.substring(str,len);
   			  param1=removedStr1.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
   			  if(param1==""){
   				  param1=tbl1_site_filter.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
   			  }
   		   } 
   		  
   	    }
   	 
   	    if(tbl2_site_filter!='undefined' && tbl2_site_filter!=''){
   		
   		  str2=tbl2_site_filter.indexOf("&cbuRegId");
   		  
   		  if(str2!=-1){
   			  var len2=tbl2_site_filter.length;
   			  var removedStr2=tbl2_site_filter.substring(str2,len2);
   			  param2=removedStr2.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
   			  if(param2==""){
   				  param2=tbl2_site_filter.replace(/&/gi,'<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@REPLACE_STR"/>');
   			  }
   		  }
   	    }
   	  
   	    //alert("showEntriesVal::::"+sipshowEntries+":nacbushowentries::"+nacbushowEntries+":siplastcolInd:"+sipLastSortColInd+":sipsortTy:"+sipLastSortDir+":nacbuSortInd:"+nacbuLastSortColInd+":nacbusortTyp:"+nacbuLastSortDir+":filteparam1:"+param1+":filterparam2:"+param2)
   	    url='setSessionDataCE?sipSE='+sipshowEntries+'&nacbuSE='+nacbushowEntries+"&sipSortInd="+sipLastSortColInd+'&sipSortTyp='+sipLastSortDir+'&nacbuSortInd='+nacbuLastSortColInd+'&nacbuSortTyp='+nacbuLastSortDir+'&sipFilterVal='+param1+"&nacbuFilteVal="+param2;
   	    setValueInSession(url);
    	
    }
	function removeSymbolfromId(regID){
		var resultString = "";
		var result ="";
		if(regID != null && regID !="" && regID!="undefined"){
			regID = $j.trim(regID);
			resultString = regID.split("-");
		//	console.log("resultString:::"+resultString);
		}
		for(var i=0; i<resultString.length;i++){
			result += resultString[i];
		}
		return result;
	}
    function cordEntryInProgres(){
    	document.location.href="cordEntry";
    }
    function cordEditData(url){
    	document.location.href=url;	
    }
    function resetValidForm(formId){
    	$j("#"+formId).validate().resetForm();
    }
    function deferedCord(cordId){
       //window.location.href="getMainCBUDetails?cdrCbuPojo.cordID="+cordId+"&deferedCord=true";
       //window.location.href="getMainCBUDetails?cbuid="+cordId+"&deferedCord=true&paginateModule=viewClinical&iShowRows=5&iPageNo=0";
       submitpost('getMainCBUDetails',{'cbuid':cordId,'sourceType':$j("#cbu").val(),'deferedCord':'true','paginateModule':'viewClinical','iShowRows':'5','iPageNo':'0'});
    }
    
    function deferedCordEntry(url){
	    document.location.href=url;
     }
    
    function showDateDiv(el,id,id1,id2){
    	var t=$j(el);
    	var obj=t.offset();
    	var xOffset=380;
    	var yOffset=30;
    	var val1="";
    	var val2="";
    	
    	if(id=='dateDiv'){
    		val1=$j('#naCBUStrDt').val();
    		val2=$j('#naCBUEndDt').val();
    	}
    	if(id=='dateDiv1'){
    		val1=$j('#sipStrDt').val();
    		val2=$j('#sipEndDt').val();
    	}
    	
    	
    	//alert("----"+val1+"---"+val2);
    	$j('#'+id1).val(val1);
    	$j('#'+id2).val(val2);
    	
    	if(navigator.appName=="Microsoft Internet Explorer"){
    			
    		    yOffset=yOffset+15;
    		    xOffset=xOffset+50;
    			if(id=="dateDiv1"){
    					xOffset=xOffset-150;
    			}
    	}
    	if(navigator.appName!="Microsoft Internet Explorer"){
    		
    	    yOffset=yOffset+15;
    	    xOffset=xOffset+26;
    		if(id=="dateDiv1"){
    			xOffset=xOffset-150;
    		}
    	}
    	$j("#"+id)
    			.css({left:obj.left-xOffset,top:obj.top+yOffset})
    			.fadeIn("fast").show()
    			.addClass('dateDivClass')
    	$j("#"+id).show();
    	if(val1==null || val1==""){
    		$j('#'+id1).focus();
    	}
    	
    		
    }
    
    </script>
    <s:hidden name="sipStatFilterFlag" id="sipStatFilterFlag"/>
    <s:hidden name="naCBUStatFilterFlag" id="naCBUStatFilterFlag"/>
    <input type="hidden" name="sip_tbl_se" id="sip_tbl_se" />
    <input type="hidden" name="nacbu_tbl_se" id="nacbu_tbl_se" />
    <input type="hidden" name="sip_sortParam" id="sip_sortParam" />
    <input type="hidden" name="sip_sort_type" id="sip_sort_type" />
    <input type="hidden" name="sip_filterFlag" id="sip_filterFlag" />
    <input type="hidden" name="nacbu_filterFlag" id="nacbu_filterFlag" />
    <input type="hidden" name="nacbu_sortParam" id="nacbu_sortParam" />
    <input type="hidden" name="nacbu_sort_type" id="nacbu_sort_type" />
    <s:form name="Tableform" id="Tableform">
    </s:form>
    <s:if test="#request.submit=='cordinprogress'">
	    <table class="tabledisplay" align="center" border="0">
	    <tbody>
	      <tr>
	        <td><h2><s:text name="garuda.unitreport.label.thankyousaveinprogresscord" /> </h2></td>
	      </tr>
	    </tbody>
	    </table>
	</s:if>
	<s:if test="#request.submit=='cord'">
	    <table class="tabledisplay" align="center" border="0">
	    <tbody>
	      <tr>
	        <td><h2><s:text name="garuda.unitreport.label.thankyoucord" /> </h2></td>
	      </tr>
	    </tbody>
	    </table>
	</s:if>
	<s:if test="#request.submit=='autoDeferCord'">
	    <table class="tabledisplay" align="center" border="0">
	    <tbody>
	      <tr>
	        <td><h2><s:text name="garuda.unitreport.label.thankyoucordautodefer" /> </h2></td>
	      </tr>
	    </tbody>
	    </table>
	</s:if>
	<s:if test="hasViewPermission(#request.newCBU)==true">
		<table width="98%" cellpadding="0" cellspacing="0" border="0" >
	       <tr>
	           <td align="center" style="padding: 10px;height:60px">             
	               <button class="button" onclick="cordEntryInProgres()" ><s:text name="garuda.cordentry.label.addcbu"></s:text></button>
	             </td>
	       </tr>
	    </table>
    </s:if>
    <div class="column">
     	<s:if test="hasViewPermission(#request.cordImprt)==true">
			<div class="portlet" id="cbuimportparent" >
				<div id="cbuimport" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
						<span class="ui-icon ui-icon-minusthick"></span>
						<!--<span class="ui-icon ui-icon-close" ></span>-->
						<s:text name="garuda.cordentry.label.cordentry.cbuimport" />
					</div>
					<div class="portlet-content" >
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td align="left">							    	
							    	<s:if test="hasViewPermission(#request.ImprtXmlfle)==true">
							        	<button type="button" onclick="cordEditData('cordFileUpload')"><s:text name="garuda.cordentry.label.cordentry.importcbuxmlfile"></s:text></button>
							        </s:if>
							    </td>
							    <td align="right">                                               									  
							    	<s:if test="hasViewPermission(#request.cbuImprtHis)==true">
							        	<button type="button" onclick="cordEditData('cordFileUpload')"><s:text name="garuda.cordentry.label.cordentry.importcbuhistory"></s:text></button>
							        </s:if>
							    </td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</s:if>
		<s:if test="hasViewPermission(#request.saveinprg)==true">
			<div class="portlet" id="cdrentryinprogparent" >
				<div id="cdrentryinprog" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
						<span class="ui-icon ui-icon-minusthick"></span>
						<!--<span class="ui-icon ui-icon-close" ></span>-->
						<s:text name="garuda.cordentry.label.cordentry.inprogCBU" />
					</div>
					<div class="portlet-content" >
					    <s:hidden name="sipfilteparams" id="sipfilteparams" />
						<div id="searchTble">
	                    	<jsp:include page="cb_cord_savedin_progress_pagination.jsp"></jsp:include>
	                    </div>
	                </div>
	                <div align="center"><span id="sipStatFilterFlagTxt"></span></div>
	            </div>
	        </div>
		</s:if>
		
		<s:if test="hasViewPermission(#request.notavilCbu)==true">
			<div class="portlet" id="notavailablecbusparent" >
				<div id="notavailablecbus" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
						<span class="ui-icon ui-icon-minusthick"></span>
						<!--<span class="ui-icon ui-icon-close" ></span>-->
						<s:text name="garuda.cordentry.label.cordentry.notavailablecbus" />
					</div>
					<s:hidden name="nacbufilteparams" id="nacbufilteparams" />
					<div class="portlet-content" >	
					  <div id="searchTble1">
					       <jsp:include page="cb_not_avail_cords_pagination.jsp"></jsp:include>
					  </div>
					  </div>
					   <div align="center"><span id="naCBUStatFilterFlagTxt"></span></div>
				</div>
			</div>
		</s:if>
	</div>
	<s:form id="dummyForm">
	<div style="display: none;" id="dateDiv1" class="dataEnteringDiv">
	<div class="closeDiv" onclick="$j('#dateDiv1').hide()" ><img src="images/cross.png" /></div>
																		<table align="center">
																			<tr align="center">
																			<td>
																			<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart1" id="dateStart1" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart1" id="dateStart1" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
																			</td>
																			<td>
																			<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd1" id="dateEnd1" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd1" id="dateEnd1" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
																			</td>
																			<td align="center">
																			<button type="button" id="clear" onclick="fnclear('dateStart1','dateEnd1')" >
																			<s:text name="garuda.pendingorderpage.label.clear"/></button>
																			</td>
																			</tr>
																			<tr align="center">
																			<td colspan="3">
																			<button type="button" id="dateStartbt1" onclick="$j('#dateDiv1').hide();fn_filterSIP();"><s:text name="garuda.pendingorderpage.label.filter"/></button>
																			<button type="button" id="closedt1" onclick="$j('#dateDiv1').hide()"><s:text name="garuda.pendingorderpage.label.close"/></button>
																			</td>
																			</tr>
																		</table>
	</div>
	<div style="display: none;" id="perCentageDiv"  class="dataEnteringDiv">
																<div class="closeDiv" onclick="$j('#perCentageDiv').hide()" ><img src="images/cross.png" /></div>
																		<table align="center">
																			<tr align="center">
																			<td>
																			<s:text name="garuda.pendingorderpage.label.percentage"/>
																			</td>
																			<td>
																			<s:textfield name="sipPerComp" id="sippercentval" placeholder="Enter Percentage"  />
																			</td>
																			</tr>
																			<tr align="center">
																			<td colspan="3">
																			<input  id="noPerFilterbt" type="button" onclick="$j('#perCentageDiv').hide();fn_filterSIP();" value='<s:text name="garuda.pendingorderpage.label.filter"/>' /> 
																			<input type="button" id="closePercent" onclick="$j('#perCentageDiv').hide();" value='<s:text name="garuda.pendingorderpage.label.close"/>'  />
																			<input type="button" id="clear" onclick="$j('#sippercentval').val('');" value='<s:text name="garuda.pendingorderpage.label.clear"/>' />
																			</td>
																			</tr>
																		</table>
	</div>
	<div style="display: none;" id="dateDiv" class="dataEnteringDiv">
	<div class="closeDiv" onclick="$j('#dateDiv').hide()" ><img src="images/cross.png" /></div>
												<table align="center">
													<tr align="center">
													<td>
														<s:text name="garuda.pendingorderpage.label.fromdt"/> <s:date name="dateStart" id="dateStart" format="M dd,yy" /><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateStart" id="dateStart" placeholder='<s:text name="garuda.pendingorderpage.label.enterstartdate"/>' readonly="readonly" />
													</td>
													<td>
														<s:text name="garuda.pendingorderpage.label.todt"/> <s:date name="dateEnd" id="dateEnd" format="M dd,yy"/><input type="text" class="datepic datePicWOMinDate" requiredposition="true" name="dateEnd" id="dateEnd" placeholder='<s:text name="garuda.pendingorderpage.label.enterenddate"/>' readonly="readonly" />
													</td>
													<td align="center">
														<button  id="clear" type="button" onclick="fnclear('dateStart','dateEnd')"> 
														<s:text name="garuda.pendingorderpage.label.clear"/></button>
													</td>
													</tr>
													<tr align="center">
													<td colspan="3">
													<button  id="dateStartbt" type="button" onclick="$j('#dateDiv').hide();fn_filterNACBU();" ><s:text name="garuda.pendingorderpage.label.filter"/></button> 
													<button type="button" id="datebtclse" onclick="$j('#dateDiv').hide();" ><s:text name="garuda.pendingorderpage.label.close"/></button> 
													</td>
													</tr>
												</table>
											 
	</div>
	</s:form>