<%@ taglib prefix="s"  uri="/struts-tags"%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", 0); 
%>
<script>
$j(document).ready(function(){
	if($j("#esignFlag").val()=='sampleInventory'){
		var div=$j('#sampleInventoryDiv').html();
		var formContent='<form id="sampleInventoryForm">'+div+'</form>';
		$j('#sampleInventoryDiv').replaceWith(formContent);
	}
	});
function updateSampleInventory(){
	//alert("Functionality Not Available");
	modalFormSubmitRefreshDiv('sampleInventoryDiv','updateSampleInventory','main');
}
</script>
<div id="sampleInventoryDiv">
     <div class="column">
      <s:hidden name="esignFlag" id="esignFlag"></s:hidden>
      <s:hidden name="entitySamplesPojo.entityId"></s:hidden>
      <s:hidden name="entitySamplesPojo.entityType"></s:hidden>
      <s:hidden name="entitySamplesPojo.orderId" id="orderId"></s:hidden>
      <s:hidden name="entitySamplesPojo.orderType" id="orderType"></s:hidden>
 		 <div class="portlet" id="procandcountparent" >
 		 
    <s:if test="entitySamplesPojo.convFiltPap!=null || entitySamplesPojo.convRbcPel!=null || entitySamplesPojo.convExtDnaAli!=null || entitySamplesPojo.convNoSerAli!=null ||
     entitySamplesPojo.convNoPlasAli!=null || entitySamplesPojo.convNonViaAli!=null || entitySamplesPojo.convViaCelAli!=null || entitySamplesPojo.convNoSegAvail!=null || 
     entitySamplesPojo.convSerMatAli!=null || entitySamplesPojo.convPlasMatAli!=null || entitySamplesPojo.convExtDnaMat!=null || entitySamplesPojo.convNoMiscMat!=null ">
 	<div id="<s:property value="esignFlag" />sampInventcontent" onclick="toggleDiv('<s:property value="esignFlag" />sampInvent')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
	 <span class="ui-icon ui-icon-triangle-1-s"></span> <s:text name="garuda.cbbprocedures.label.sampInvent" />
	</div>	
	  <div id="<s:property value="esignFlag" />sampInvent" class="portlet-content">		
 		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td><s:text name="garuda.cordentry.label.filtPap"></s:text>:</td>																									    
				<td><s:textfield name="entitySamplesPojo.convFiltPap" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
				<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.rbcPel"></s:text>:</td>																									     
				<td><s:textfield name="entitySamplesPojo.convRbcPel" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
			</tr>
			<tr>
			   <td><s:text name="garuda.cordentry.label.extDnaAli"></s:text>:</td>																									    
			   <td><s:textfield name="entitySamplesPojo.convExtDnaAli" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
			   <td style="padding-left:10px;" onmouseover="return overlib('<s:text name='garuda.cbbprocedure.label.numberSerumToolTip'/>');" 
								   onmouseout="return nd();"><s:text name="garuda.cordentry.label.noSerAli"></s:text>:</td>																									  
			   <td><s:textfield name="entitySamplesPojo.convNoSerAli" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
	       </tr>
		   <tr>
			   <td><s:text name="garuda.cordentry.label.noPlasAli"></s:text>:</td>																									   
			   <td><s:textfield name="entitySamplesPojo.convNoPlasAli" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
			   <td style="padding-left:10px;"><s:text name="garuda.cordentry.label.nonViaAli"></s:text>:</td>																									   
			   <td><s:textfield name="entitySamplesPojo.convNonViaAli" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
		   </tr>
	       <tr>
			  <td><s:text name="garuda.cordentry.label.convViaCelAli"></s:text>:</td>																									    
			  <td><s:textfield name="entitySamplesPojo.convViaCelAli" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
			  <td style="padding-left:10px;"><s:text name="garuda.cordentry.label.segAvail"></s:text>:</td>
			  <td><s:textfield name="entitySamplesPojo.convNoSegAvail" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
		  </tr>
		  
		  <tr>
		  		<td><s:text name="garuda.cordentry.label.convSerMatAli"></s:text>:</td>																									 
				<td><s:textfield name="entitySamplesPojo.convSerMatAli" cssClass="positive" maxlength="2" disabled="true"> </s:textfield></td>
				<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.plasMatAli"></s:text>:</td>	
				<td><s:textfield name="entitySamplesPojo.convPlasMatAli" cssClass="positive" maxlength="2" disabled="true"> </s:textfield></td>
				
		  </tr>
	      <tr>
				<td><s:text name="garuda.cordentry.label.extDnaMat"></s:text>:</td>	
				<td><s:textfield name="entitySamplesPojo.convExtDnaMat" cssClass="positive" maxlength="2" disabled="true"> </s:textfield></td>
				
				<td style="padding-left:10px;"><s:text name="garuda.cbbprocedures.label.noMiscMat"></s:text>:</td>	
				<td><s:textfield name="entitySamplesPojo.convNoMiscMat" cssClass="positive" maxlength="2" disabled="true"></s:textfield></td>
				
	     </tr>
	 </table>
 	</div>	 
 </s:if>		 
 		 
 		 
			<div id="procandcountCbu" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
				  <div id= "<s:property value="esignFlag" />cbusmplcontent" onclick="toggleDiv('<s:property value="esignFlag" />cbusmpl')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-triangle-1-s"></span> <s:text name="garuda.cbbprocedures.label.cbusample" />
				  </div>
				<div id ="<s:property value="esignFlag" />cbusmpl" class="portlet-content">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">																										 
							<tr>
								<td><s:text name="garuda.cordentry.label.filtPap"></s:text>:</td>	
								<td><s:textfield name="entitySamplesPojo.filtPap" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>										  
								<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.rbcPel"></s:text>:</td>																						
								<td><s:textfield name="entitySamplesPojo.rbcPel" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
							</tr>															
							<tr>
							   <td><s:text name="garuda.cordentry.label.extDnaAli"></s:text>:</td>																								
							   <td><s:textfield name="entitySamplesPojo.extDnaAli" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>														
							   <td style="padding-left:10px;" onmouseover="return overlib('<s:text name='garuda.cbbprocedure.label.numberSerumToolTip'/>');" 
								   onmouseout="return nd();"><s:text name="garuda.cordentry.label.noSerAli"></s:text>:</td>																								
							   <td><s:textfield name="entitySamplesPojo.noSerAli" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
					       </tr>																											 
						   <tr>
							    <td><s:text name="garuda.cordentry.label.noPlasAli"></s:text>:</td>																									   
							    <td><s:textfield name="entitySamplesPojo.noPlasAli" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
								<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.nonViaAli"></s:text>:</td>																									     
								<td><s:textfield name="entitySamplesPojo.nonViaAli" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
						   </tr>
						    <tr>
								<td> <s:text name="garuda.cordentry.label.viaCelAli"></s:text>:</td>																									     
								<td><s:textfield name="entitySamplesPojo.viaCelAli" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
								<td></td>																									    
								<td></td>
						  </tr>																				  
					
					<tr>
					   <td>
						  <b><s:text name="garuda.cbbprocedures.label.aliqrepoffinalprod"></s:text></b>
					  </td>
				    </tr>	
						
							 <tr>
							    <td><s:text name="garuda.cordentry.label.segAvail"></s:text>:</td>																									    
								<td><s:textfield name="entitySamplesPojo.noSegAvail" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
								<td></td>																									     
								<td></td>
						  </tr>																			  
					      <tr>
								<td><s:text name="garuda.cordentry.label.cbuOthRepConFin"></s:text>:</td>	
								<td><s:textfield name="entitySamplesPojo.cbuOthRepConFin" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
								<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.cbuRepAltCon"></s:text>:</td>																									    
								<td><s:textfield name="entitySamplesPojo.cbuRepAltCon" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>								
					      </tr>	
						</table>
					
				</div>
		     </div>  
		     <div id="procandcountMat" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
	    	 <div id= "<s:property value="esignFlag" />matsmplcontent" onclick="toggleDiv('<s:property value="esignFlag" />matsmpl')" class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
				<span class="ui-icon ui-icon-triangle-1-s"></span><s:text name="garuda.cbbprocedures.label.matsample" />
			  </div>
					<div id="<s:property value="esignFlag" />matsmpl" class="portlet-content">		  
					     <table width="100%" cellpadding="0" cellspacing="0" border="0">
			     			  <tr>
								    <td><s:text name="garuda.cordentry.label.serMatAli"></s:text>:</td>
									<td><s:textfield name="entitySamplesPojo.serMatAli" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
									<td style="padding-left:10px;"><s:text name="garuda.cordentry.label.plasMatAli"></s:text>:</td>	
									<td><s:textfield name="entitySamplesPojo.plasMatAli" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
							  </tr>
			     	           <tr>
								    <td><s:text name="garuda.cordentry.label.extDnaMat"></s:text>:</td>	
								    <td><s:textfield name="entitySamplesPojo.extDnaMat" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
								    <td style="padding-left:10px;" onmouseover="return overlib('<s:text name='garuda.cbbprocedure.label.numberMisMatToolTip'/>');" 
										onmouseout="return nd();"><s:text name="garuda.cbbprocedures.label.noMiscMat"></s:text>:</td>	
									<td><s:textfield name="entitySamplesPojo.noMiscMat" cssClass="positive procclass cbuProcMandatory" maxlength="2" readonly="true" onkeydown="cancelBack();" ></s:textfield></td>
							  </tr>
					     
					     
						  </table>	
					</div>
					<s:if test="esignFlag!=null">
					  <s:if test="#request.showProc!=true">
						<table style="background-color: #cccccc;" id="esignTbl">
							<tr valign="baseline">
								<td width="100%">
									<jsp:include page="../cb_esignature.jsp" flush="true">
									<jsp:param value="submitSampleInv" name="submitId"/>
									<jsp:param value="invalidSampleInv" name="invalid" />
									<jsp:param value="minimumSampleInv" name="minimum" />
									<jsp:param value="passSampleInv" name="pass" />
									</jsp:include>
								</td>
								<td align="center" width="30%">
									<input type="button" onclick="updateSampleInventory();" value="<s:text name="garuda.unitreport.label.button.submit"/>" id="submitSampleInv" disabled="disabled"/>
								</td>
							</tr>
						</table>
					  </s:if>
					</s:if>
			   </div>
			 </div> 
		  </div>		
	</div>