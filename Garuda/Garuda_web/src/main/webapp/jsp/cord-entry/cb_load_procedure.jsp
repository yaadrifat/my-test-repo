 <%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants"%>
    <s:hidden value="%{entitySamplesPojo.filtPap}" id="filtPap"></s:hidden>
	<s:hidden value="%{entitySamplesPojo.rbcPel}" id="rbcPel"></s:hidden>
	<s:hidden value="%{entitySamplesPojo.extDnaAli}" id="extDnaAli"></s:hidden>
	<s:hidden value="%{entitySamplesPojo.noSerAli}" id="noSerAli"></s:hidden>
	<s:hidden value="%{entitySamplesPojo.noPlasAli}" id="noPlasAli"></s:hidden>
	<s:hidden value="%{entitySamplesPojo.nonViaAli}" id="nonViaAli"></s:hidden>
	<s:hidden value="%{entitySamplesPojo.viaCelAli}" id="viaCelAli"></s:hidden>
	<s:hidden value="%{entitySamplesPojo.noSegAvail}" id="noSegAvail"></s:hidden>
	<s:hidden value="%{entitySamplesPojo.cbuOthRepConFin}" id="cbuOthRepConFin"></s:hidden>
	<s:hidden value="%{entitySamplesPojo.cbuRepAltCon}" id="cbuRepAltCon"></s:hidden>
	<s:hidden value="%{entitySamplesPojo.celMatAli}" id="celMatAli"></s:hidden>
	<s:hidden value="%{entitySamplesPojo.plasMatAli}" id="plasMatAli"></s:hidden>
	<s:hidden value="%{entitySamplesPojo.extDnaMat}" id="extDnaMat"></s:hidden>
	<s:hidden value="%{entitySamplesPojo.noMiscMat}" id="noMiscMat"></s:hidden> 
	<s:hidden value="%{#request.procStartDt}" id="procStartDt" name="procStartDate"></s:hidden>
	<s:hidden value="%{#request.procTermiDt}" id="procTermiDt" name="procTermiDate"></s:hidden>
	<s:hidden name="entitySamplesPojo.pkEntitySamples" id="samplAliquoPk"></s:hidden> 
<table>
   <tr>
     <td width="50%" align="right" style="padding-left: 5px;"><s:text name="garuda.cbbprocedures.label.cbbprocessingprocedures" />:<span style="color: red;">*</span></td>
     <td width="50%" style="padding-left: 5px;" >
     <div id="procDiv">
		<jsp:include page="../cb_cbbprocdropdown.jsp"></jsp:include>
	  </div>
     </td>
     <s:if test="cdrCbuPojo.fkCbbProcedure != null && cdrCbuPojo.fkCbbProcedure != -1">
         <td>
             <a href="javascript:void(0);" onclick="showModal('View CBB Procedure','refreshCBBProcedures?cbbProcedures.pkProcId=<s:property value='cdrCbuPojo.fkCbbProcedure'/>','500','850');"><s:text name="garuda.common.lable.view"></s:text> </a>
         </td><td colspan="3"></td>
     </s:if>
     <s:else>
         <td colspan="4"></td>
     </s:else>									     
  </tr>
  <tr>
    <td colspan="6">
      <div id="cbbprocedurerefresh">
        <jsp:include page="cb_procedure_refresh.jsp"></jsp:include>
	  </div>
	</td>
  </tr>
</table>