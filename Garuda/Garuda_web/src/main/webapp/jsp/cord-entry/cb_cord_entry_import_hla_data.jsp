<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<script>
$j(function(){	

	 noOfMandatoryField = noOfMandatoryField - HlaLoadedObject.hlaMandatoryField;
	 totalcount = totalcount - HlaLoadedObject.hlaCount;
	 
     noOfMandatoryHlaField = parseInt('<s:property value="#request.hlaTotaFiledCount"/>');
     noOfMandatoryField = noOfMandatoryField + parseInt('<s:property value="#request.hlaTotaFiledCount"/>');
     hlacount = parseInt('<s:property value="#request.hlaMadatoryField"/>');
     var val = parseInt((hlacount*100)/(noOfMandatoryHlaField));
	 $j("#hlabar").progressbar({
			value: val
		}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
	 var val1;	
	 totalcount += hlacount;
	 val1 = parseInt((totalcount*100)/(noOfMandatoryField));
	 $j("#totalbar").progressbar({
			value: val1
		}).children('.ui-progressbar-value').html(val1.toPrecision(3) + '%');
     $j("#cordProgress").val(val1);	
     HlaLoadedObject.hlaMandatoryField = noOfMandatoryHlaField;
     HlaLoadedObject.hlaCount = hlacount;
     HlaLoadedObject.percentage = val;
     parent.document.getElementById("hlaPercentageId").value = val;
	/* $j(".hlaclass").each(function(){
		  noOfMandatoryHlaField++;
		   noOfMandatoryField++;			   
	  });
	 var hlafocus = "";
	   $j(".hlaclass").each(function(){
		   var val1 = $j(this).val();			   	   
		   if(val1!="" && val1!=-1)
		   {
			   hlacount++;		  
			   val = parseInt((hlacount*100)/(noOfMandatoryHlaField));
			   $j("#hlabar").progressbar({
						value: val
					}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
			   addTotalCount();	
		   }
	   });
	   
		$j(".hlaclass").bind({focusin:function() {
			 var val = $j(this).val();
			 hlafocus = val;
		},change:function() {
		 var val = $j(this).val();
		
			 if((hlafocus=="" || hlafocus==-1) && val!="" && val!=-1){
				 hlacount++;
				 hlacount=(hlacount>noOfMandatoryHlaField)?noOfMandatoryHlaField:hlacount;		  
				 val = parseInt((hlacount*100)/(noOfMandatoryHlaField));
				 $j("#hlabar").progressbar({
						value: val
					}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
				 addTotalCount();
			 }else if((hlafocus!="" && hlafocus!=-1) && (val=="" || val==-1)){
				
				 hlacount--;		  
				 val = parseInt((hlacount*100)/(noOfMandatoryHlaField));
				 $j("#hlabar").progressbar({
						value: val
					}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
				 minusTotalCount();
			 }
		},focusout:function() {
			 var val = $j(this).val();
			 hlafocus = val;
		}});
		//call method to refersh total count progress
		  totalcount--;
	      addTotalCount();*/
	   // Load Validation If All Widget Is Fully Loaded	   
	      LoadedPage.cbuHla=true;
	      if(LoadedPage.idm && LoadedPage.fmhq && LoadedPage.mrq && LoadedPage.processingProc && LoadedPage.clinicalNote && LoadedPage.labSummary){
	    	  jQueryValidationInitializer();
	      }
   });
</script>
	<div id="cbubesthlainfocontent" onclick="toggleDiv('cbubesthlainfo')"
					class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
					style="text-align: center;"><span
					class="ui-icon ui-icon-triangle-1-s"></span> <s:text
					name="garuda.cbuentry.label.CBU"></s:text></div>
				<div id="cbubesthlainfo" style="width:100%; overflow-x: scroll; overflow-y:hidden;">
				   <!-- table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults11">
						 <thead>
						   <tr>
						      <th></th>			   			   
							   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							       <th>
							          <s:property value="description.replace('HLA-','')"/>
							       </th>				     
							   </s:iterator>
						   </tr>
						 </thead>			 
						 <tbody>
						    <tr>
							    <td>
									 <s:text name="garuda.cbu.label.besthla"/>								            
								</td>								    
								  <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							            <s:iterator value="#request.bestHlas" var="bhla">
									             <s:if test="pkCodeId==fkHlaCodeId">
									                 <s:set name="bhlaCheck" value="%{'true'}" />
										             <td>
											            <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
										                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>	  
												     </td>	
											      </s:if>				      										      	        	
									        </s:iterator> 
									        <s:if test="%{#bhlaCheck!='true'}">
											         <td></td>
											</s:if>	
											<s:set name="bhlaCheck" value="%{'false'}" />
								  </s:iterator>
							</tr>
						   </tbody>
						   <tfoot>
							    <tr>
									<td colspan="13"></td>
								</tr>
							</tfoot>
						</table>
						<table width="100%" style="padding-top: 50px;">
						      <tr>
						         <td></td>
						      </tr>
						</table-->
						<table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults10">
							 <thead>
							   <tr>
							      <th><s:text name="garuda.cbuentry.label.typingdate"/></th>	
							      <th><s:text name="garuda.cbuentry.label.recieveddate"/></th>
							      <th><s:text name="garuda.cbuentry.label.source"/></th>		   			   
								   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
								       <th>
								          <s:property value="description.replace('HLA-','')"/>
								       </th>				     
								   </s:iterator>
							   </tr>
							 </thead>			 
							 <tbody>	
							    <s:iterator value="#request.cbuHlaMap">
							        <s:set name="cbuHlaMapKey" value="key" />
							        <tr>				
							        <td>
							            <s:if test="#request.cbuHlaMap[#cbuHlaMapKey][0].hlaTypingDate !=null">
							                <s:set name="cbuhlatypedate" value="#request.cbuHlaMap[#cbuHlaMapKey][0].hlaTypingDate" />
									        <s:date name="cbuhlatypedate" id="cbuhlatypedate" format="MMM dd, yyyy" />
									        <s:property value="%{#request.cbuhlatypedate}"/>									       
							            </s:if>									        
							        </td>
							        <td>
							            <s:if test="#request.cbuHlaMap[#cbuHlaMapKey][0].hlaRecievedDate !=null">
									        <s:set name="cbuhlarecievedate" value="#request.cbuHlaMap[#cbuHlaMapKey][0].hlaRecievedDate" />
									        <s:date name="cbuhlarecievedate" id="cbuhlarecievedate" format="MMM dd, yyyy" />
									        <s:property value="%{#request.cbuhlarecievedate}"/>									        
									    </s:if>
							        </td>
							        <td>
							           <!--<s:if test="#request.cbuHlaMap[#cbuHlaMapKey][0].fkSource !=null && #request.cbuHlaMap[#cbuHlaMapKey][0].fkSource!=''">
							               <s:set name="cbuhlasource" value="#request.cbuHlaMap[#cbuHlaMapKey][0].fkSource" />
								           <s:property value="getCodeListDescById(#cbuhlasource)"/>
								       </s:if>
								       --><s:if test="#request.cbuHlaMap[#cbuHlaMapKey][0].createdBy !=null && #request.cbuHlaMap[#cbuHlaMapKey][0].createdBy!=''">
							               <s:set name="cbuhlacreator" value="#request.cbuHlaMap[#cbuHlaMapKey][0].createdBy" />
								           <s:property value="getUserNameById(#cbuhlacreator)"/>
								       </s:if>
							        </td>
							        <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">					       
								            <s:iterator value="#request.cbuHlaMap[#cbuHlaMapKey]" var="bhla">
									             <s:if test="pkCodeId==fkHlaCodeId">
									                 <s:set name="hlaCheck" value="%{'true'}" />
										             <td>
											            <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
										                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>	  
												     </td>	
											      </s:if>				      										      	        	
									        </s:iterator> 
									        <s:if test="%{#hlaCheck!='true'}">
											         <td></td>
											</s:if>	
											<s:set name="hlaCheck" value="%{'false'}" />	    								          
									 </s:iterator> 
									 </tr>
								 </s:iterator>
							   </tbody>
							   <tfoot>
								    <tr>
										<td colspan="13"></td>
									</tr>
								</tfoot>
			              </table>	
			              <table width="100%" cellpadding="0" cellspacing="0" border="0">
						    <tr>
						       <td align="right">						             
						       		 <input type="button" name="editHLA" class="button" onclick="fn_showModalHla('<s:text name="garuda.cbu.cbuhlatype.edit"/> <s:text name="garuda.message.modal.headerregid"/> <s:property value="cdrCbuPojo.registryId" />','updateHlas?updateFrom=cordEntry&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&module=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CLINICAL_RECORD_HLA_MATERNAL" />&update=False&source=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE)" />&entityId=<s:property value="cdrCbuPojo.cordID" />&entityType=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_TYPE" />&entitySubtype=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_SUB_TYPE" />','500','900');" value="<s:text name="garuda.common.lable.edit"/>"></input>						       		  
						       </td>
						    </tr>
						</table>
				</div>			     
			    <div>		
				<div id="matbesthlainfocontent" onclick="toggleDiv('matbesthlainfo')"
					class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
					style="text-align: center;"><span
					class="ui-icon ui-icon-triangle-1-s"></span> <s:text
					name="garuda.cbuentry.label.maternal"></s:text></div>				
				<div id="matbesthlainfo" style="width:100%; overflow-x: scroll; overflow-y:hidden;">
				   <!--<table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults10">
						 <thead>
						   <tr>
						      <th></th>
						      <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
							       <th>
							          <s:property value="description"/>
							       </th>				     
							   </s:iterator>
						   </tr>
						 </thead>			 
						 <tbody>	
						        <s:if test="fkSource==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_CODESUBTYPE)">
								    <tr>
								        <td>Best HLA</td>				    		         
									        <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
									             <s:if test="pkCodeId==fkHlaCodeId">
										             <td>
											            <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
										                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>	  
												     </td>	
											      </s:if>
											      <s:else>
											          <td>
											          </td>	
											      </s:else>			        	
									        </s:iterator>	
								   </tr>
								</s:if> 
						   </tbody>
						   <tfoot>
							    <tr>
									<td colspan="13"></td>
								</tr>
							</tfoot>
						</table>
						<table width="100%" style="padding-top: 50px;">
						      <tr>
						         <td></td>
						      </tr>
						</table>
						-->
						<table><tr><td></td></tr></table>
						<table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults10">
							 <thead>
							   <tr>
							      <th><s:text name="garuda.cbuentry.label.typingdate"/></th>	
							      <th><s:text name="garuda.cbuentry.label.recieveddate"/></th>
							      <th><s:text name="garuda.cbuentry.label.source"/></th>							   		   			   
								   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
								       <th>
								          <s:property value="description.replace('HLA-','')"/>
								       </th>				     
								   </s:iterator>
							   </tr>
							 </thead>			 
							 <tbody>	
							    <s:iterator value="#request.matHlaMap">
							        <s:set name="matHlaMapKey" value="key" />
							        <tr>				
							        <td>
							            <s:if test="#request.matHlaMap[#matHlaMapKey][0].hlaTypingDate !=null">
							                <s:set name="mathlatypedate" value="#request.matHlaMap[#matHlaMapKey][0].hlaTypingDate" />
									        <s:date name="mathlatypedate" id="mathlatypedate" format="MMM dd, yyyy" />
									        <s:property value="%{mathlatypedate}"/>
							            </s:if>									        
							        </td>
							        <td>
							            <s:if test="#request.matHlaMap[#matHlaMapKey][0].hlaRecievedDate !=null">
									        <s:set name="mathlarecievedate" value="#request.matHlaMap[#matHlaMapKey][0].hlaRecievedDate" />
									        <s:date name="mathlarecievedate" id="mathlarecievedate" format="MMM dd, yyyy" />
									        <s:property value="%{mathlarecievedate}"/>
									    </s:if>
							        </td>
							        <td>
							           <!--<s:if test="#request.matHlaMap[#matHlaMapKey][0].fkSource !=null && #request.matHlaMap[#matHlaMapKey][0].fkSource!=''">
							               <s:set name="mathlasource" value="#request.matHlaMap[#matHlaMapKey][0].fkSource" />
								           <s:property value="getCodeListDescById(#mathlasource)"/>
								       </s:if>
								       --><s:if test="#request.matHlaMap[#matHlaMapKey][0].createdBy !=null && #request.matHlaMap[#matHlaMapKey][0].createdBy!=''">
							               <s:set name="mathlacreator" value="#request.matHlaMap[#matHlaMapKey][0].createdBy" />
								           <s:property value="getUserNameById(#mathlacreator)"/>
								       </s:if>
							        </td>					        					       
								    <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
									     	<s:iterator value="#request.matHlaMap[#matHlaMapKey]" var="bhla">
									             <s:if test="pkCodeId==fkHlaCodeId">
									             <s:set name="matCheck" value="%{'true'}" />
										             <td>
										                <table>
										                    <tr>
										                        <td>
										                            <s:property value="hlaType1" />
										                        </td>
										                    </tr>
										                     <tr>
										                        <td>
										                            <s:property value="hlaType2" />
										                        </td>
										                    </tr>
										                </table>										            
												     </td>	
											      </s:if>										      		        	
									        </s:iterator>    
									        <s:if test="%{#matCheck!='true'}">
											         <td></td>
											</s:if>	
											<s:set name="matCheck" value="%{'false'}" />    
									 </s:iterator> 
									 </tr>
								 </s:iterator>
							   </tbody>
							   <tfoot>
								    <tr>
										<td colspan="13"></td>
									</tr>
								</tfoot>
						</table>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
						    <tr>
						       <td align="right">						           
						           <input type="button" name="editHLA1" class="button" onclick="fn_showModalHla('<s:text name="garuda.cbu.maternalhlatype.edit"/> <s:text name="garuda.message.modal.headerregid"/> <s:property value="cdrCbuPojo.registryId" />','updateHlas?updateFrom=cordEntry&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&module=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CBU" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_CLINICAL_RECORD_HLA_CBU" />&update=False&source=<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_CODESUBTYPE)" />&entityId=<s:property value="cdrCbuPojo.cordID" />&entityType=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_TYPE" />&entitySubtype=<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@CORD_ENTITY_SUB_TYPE" />','500','900');" value="<s:text name="garuda.common.lable.edit"/>"></input>				           
						       </td>
						    </tr>
						</table>
				</div>
				</div>
				<script>
			    function fn_showModalHla(title,url,height,width,id){
				var upiob = $j('#idonbagvalresult').val();
				var upiob_data = '';
				if(upiob!="" && upiob != null && upiob != "undefined"){
					upiob_data = ', <s:text name="garuda.cbuentry.label.idbag"/>:'+upiob;
				}
				title=title+upiob_data;
				showModal(title,url,height,width);			
			}
			</script>