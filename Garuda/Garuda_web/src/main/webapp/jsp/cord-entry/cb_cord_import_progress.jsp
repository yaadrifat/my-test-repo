<%@ taglib prefix="s"  uri="/struts-tags"%>
<!--<s:if test="cordImportPojo.progress!=null">
     <script>
     $j("#totalbar").progressbar({
			value: '<s:property value="%{cordImportPojo.progress}" />'
		});
     </script>
</s:if>
<s:if test="cordImportPojo.progress==null">
     <script>
     $j("#totalbar").progressbar({
			value: 0
		});
     </script>
</s:if>
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
					<tr>
						<td align="left" colspan="3">
							  Preview Progress
						</td>
					</tr>
					<tr>
						<td align="left" >
							  Name: 
						</td>
						<td colspan="2">
						       <s:property value="cordImportPojo.fileUploadFileName" />
						</td>
					</tr>
					<tr>
						<td align="left" >
							  Type:
						</td>
						<td colspan="2">
						     <s:property value="cordImportPojo.fileUploadContentType" />
						</td>
					</tr>	
					<tr>
						<td align="left" >							  
						</td>
						<td colspan="2">						
						    <div id="totalbar"></div>
						</td>
					</tr>
					<tr>
					    <td colspan="2"></td>
					    <td>
					      <s:property value="cordImportPojo.totalProcessedCords" /> of <s:property value="cordImportPojo.totalNoCords" /> Cords have been imported.
					    </td>
					</tr>				
</table>-->
<table width="100%" cellpadding="0" cellspacing="0" border="0">
  <s:if test="#request.cordException==false">
	<tr >
		<td colspan="2">
			<div id="cordImportStart" style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text  name="garuda.cordImport.startMessage"></s:text></strong></p>
			</div>
		</td>
	</tr>
	<!--<script>
	   var url='loadCordImportProgress?historyPojo.pkCordimportHistory='+'<s:property value="historyPojo.pkCordimportHistory"/>';
	   window.open( url, "Cord Import Progress", "status = 1, height = 150, width = 300, resizable = 0,location=0" );
	</script>
  --></s:if>
  <s:else>
    <tr >
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-error ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-alert"></span>
				<strong>XML Format Error!</strong></p>
			</div>
		</td>
	</tr>
  </s:else>
</table>