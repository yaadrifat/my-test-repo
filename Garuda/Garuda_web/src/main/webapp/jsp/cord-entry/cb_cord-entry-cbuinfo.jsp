<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.eres.web.site.SiteJB" %>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%@page import="com.velos.ordercomponent.util.GarudaMessages" %>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<jsp:useBean id="modCtlDao" scope="request" class="com.velos.eres.business.common.CtrlDao"/>
<%
	UserJB userB = (UserJB)session.getAttribute("currentUser");
	request.setAttribute("userid",Integer.parseInt(userB.getUserSiteId()));

%>
<%
	HttpSession tSession = request.getSession(true); 
	GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
	int editCBURght = 0;
	if(grpRights.getFtrRightsByValue("CB_ADD")!=null && !grpRights.getFtrRightsByValue("CB_ADD").equals(""))
	 {editCBURght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ADD"));}
	else
		editCBURght = 4;	
	request.setAttribute("editCBURght",editCBURght);
%>
<script>
  $j( "input:submit, button", ".column" ).button(); 
   var additionalIdCount = 0;   
   var maxadditionalidcount=0;
   var valFocus = "";
   function showAddWidget(url){
		 url = url + "?pageId=20" ; 
		 showAddWidgetModal(url);
	 }
   
   $j(function(){
		$j("#cdrentryform").validate({
				
			rules:{
			        "cdrCbuPojo.fkCbbId":{checkSelectCordEntry : true},		
					"cdrCbuPojo.registryId":{checkspecialchar : true,checklength:true,checkformat:true,validateValues:["cburegid","regMaternalId"],checksum : true,checkdbdata1:["CdrCbu","registryId"],checkdbdata:["CdrCbu","registryMaternalId"]},					
					"cdrCbuPojo.registryMaternalId":{checkspecialchar : true,checklength:true,checkformat:true,validateValues:["cburegid","regMaternalId"],checksum : true,checkdbmatreg:["CdrCbu","registryMaternalId","fkCbbId"],checkdbreg:["CdrCbu","registryId"],checkDeferMaternalRegistryId:["CdrCbu","registryMaternalId","fkCordCbuStatus"]},
                    "cdrCbuPojo.localMaternalId":{validateValues:["matlocalid","loccbuid"],checkdbadditionaldata:["CdrCbu","localMaternalId","registryMaternalId","fkCbbId"],checklocalcbuidadditionaldbdata1:["CdrCbu","localCbuId","fkCbbId"]},					
                    "cdrCbuPojo.cordIsbiDinCode":{checkisbtdbdata:["CdrCbu","cordIsbiDinCode"],checkisbtdata:["001"]},
					"cdrCbuPojo.localCbuId":{required:true,validateValues:["matlocalid","loccbuid"],checklocalcbuidadditionaldbdata:["CdrCbu","localMaternalId","fkCbbId"],checklocalcbuidadditionaldbdata1:["CdrCbu","localCbuId","fkCbbId"]},
					//"cdrCbuPojo.cordDonorIdNumber":{checkisbtdata:["019"]},
					"cbuOnBag":{checkselect:true},
					"cbuRegistryId":{required:true,validateSameValues:["cburegid","othercburegid"]},
					"cbuRegistryMaternalId":{required:true,validateSameValues:["regMaternalId","otherregMaternalId"]},
					"cbuLocalCbuId":{required:true,validateSameValues:["loccbuid","otherloccbuid"]},
					"cbuLocalMaternalId":{validateSameValues:["matlocalid","othermatlocalid"]},
					"cbuCordIsbiDinCode":{validateSameValues:["isbidin","otherisbidin"]},
					"selectName":{min:1}
				},
				messages:{
					"cdrCbuPojo.registryId":{checkspecialchar : "<s:text name="garuda.common.validation.alphanumeric"/>",checklength:"<s:text name="garuda.common.validation.alphalength"/>",checkformat:"<s:text name="garuda.cbu.cordentry.checkformat"/>",validateValues:"<s:text name="garuda.cbu.cordentry.cbumatsameregid"/>",checksum :"<s:text name="garuda.common.validation.checksumrid"/>",checkdbdata1:"<br/><s:text name="garuda.common.id.exist"/>",checkdbdata:"<br/><s:text name="garuda.common.id.exist"/>"},
					"cdrCbuPojo.registryMaternalId":{checkspecialchar : "<s:text name="garuda.common.validation.alphanumeric"/>",checklength:"<s:text name="garuda.common.validation.alphalength"/>",checkformat:"<s:text name="garuda.cbu.cordentry.checkformat"/>",validateValues:"<s:text name="garuda.cbu.cordentry.cbumatsameregid"/>",checksum :"<s:text name="garuda.common.validation.checksummid"/>",checkdbmatreg:"<s:text name="garuda.cbu.cordentry.samematregid"/>",checkdbreg:"<s:text name="garuda.cbu.cordentry.samematregidcbuid"/>",checkdbdata1:"<s:text name="garuda.common.id.exist"/>",checkdbdata:"<s:text name="garuda.common.id.exist"/>",checkDeferMaternalRegistryId:"<s:text name="garuda.common.validation.data"/>"},
					"cdrCbuPojo.localMaternalId":{validateValues:"<s:text name="garuda.cbu.cordentry.cbumatlocsamecbulocid"/>",checkdbadditionaldata:"<s:text name="garuda.cbu.cordentry.cbumatlocexistdiffmatid"/>",checklocalcbuidadditionaldbdata1:"<s:text name="garuda.cbu.cordentry.locmatidexistascblocid"/>"},
					"cdrCbuPojo.cordIsbiDinCode":{checkisbtdbdata:"<s:text name="garuda.common.id.exist"/>",checkisbtdata:"<s:text name="garuda.cbu.cordentry.reqvalidisbtdin"/>"},	
					//"cdrCbuPojo.cordDonorIdNumber":{checkisbtdata:"Please Enter Valid Donor Identification Number"},	
					"cdrCbuPojo.localCbuId":{required:"<s:text name="garuda.cbu.cordentry.reqcbulocid"/>",validateValues:"<s:text name="garuda.cbu.cordentry.cbulocsamematlocid"/>",/*checklocalcbuiddbdata:"<s:text name="garuda.common.id.exist"/>",*/checklocalcbuidadditionaldbdata:"<s:text name="garuda.cbu.cordentry.localCbuIdexistasMatId"/>",checklocalcbuidadditionaldbdata1:"<s:text name="garuda.cbu.cordentry.localCbuIdexist"/>"},
					"cbuOnBag":{checkselect:"<s:text name="garuda.common.validation.value"/>"},
					"cbuRegistryId":{required:"<s:text name="garuda.cbu.cordentry.reqcburegid"/>",validateSameValues:"<s:text name="garuda.cbu.cordentry.reqcbusameregid"/>"},
					"cbuRegistryMaternalId":{required:"<s:text name="garuda.cbu.cordentry.reqmatregid"/>",validateSameValues:"<s:text name="garuda.cbu.cordentry.reqmatsameregid"/>"},
					"cbuLocalCbuId":{required:"<s:text name="garuda.cbu.cordentry.reqcbulocid"/>",validateSameValues:"<s:text name="garuda.cbu.cordentry.reqcbusamelocid"/>"},
					"cbuLocalMaternalId":{validateSameValues:"<s:text name="garuda.cbu.cordentry.reqmatsamelocid"/>"},
					"cbuCordIsbiDinCode":{validateSameValues:"<s:text name="garuda.cbu.cordentry.reqisbtsamedin"/>"}
										
				}
			});

		$j("#cburegid").keydown(function(e) {
			  var val = $j("#cburegid").val();
			  if(e.keyCode!=8 && e.keyCode!=46 )
			  {
				  if(val.length==4 || val.length==9){
					  if(e.which!=109){
						   $j("#cburegid").val(val+"-");
					  }
				  }
			  }
		});

		$j("#regMaternalId").keydown(function(e) {
			  var val = $j("#regMaternalId").val();
			  if(e.keyCode!=8 && e.keyCode!=46 )
			  {
				  if(val.length==4 || val.length==9){
					  if(e.which!=109){
					   $j("#regMaternalId").val(val+"-");
					  }
				  }
			  }
		});

		$j("#othercburegid").keydown(function(e) {
			  var val = $j("#othercburegid").val();
			  if(e.keyCode!=8 && e.keyCode!=46 )
			  {
				  if(val.length==4 || val.length==9){
					  if(e.which!=109){
						   $j("#othercburegid").val(val+"-");
					  }
				  }
			  }
		});

		$j("#otherregMaternalId").keydown(function(e) {
			  var val = $j("#otherregMaternalId").val();
			  if(e.keyCode!=8 && e.keyCode!=46 )
			  {
				  if(val.length==4 || val.length==9){
					  if(e.which!=109){
					   $j("#otherregMaternalId").val(val+"-");
					  }
				  }
			  }
		});
        
		$j("#matlocalid").click(function() {
	      $j("#cdrentryform").validate().element("#fkCbbId");
         });
		 
		 $j("#loccbuid").click(function() {
	      $j("#cdrentryform").validate().element("#fkCbbId");
         });
		 
		 $j("#regMaternalId").click(function() {
	      $j("#cdrentryform").validate().element("#fkCbbId");
         });
		
		$j("#cburegid").bind("contextmenu",function(e){
	        return false;
	    });

		$j("#othercburegid").bind("contextmenu",function(e){
	        return false;
	    });
	    
		$j("#regMaternalId").bind("contextmenu",function(e){
	        return false;
	    });

		$j("#otherregMaternalId").bind("contextmenu",function(e){
	        return false;
	    });
		
		/*$j("#submitcdrnext").easyconfirm({locale: { title: 'Please double check the entered IDs,once you leave this page you will not be able to Edit the entered IDs', button: ['Cancel','Proceed']}});
		$j("#submitcdrnext").click(function() {
			  if($j("#cdrentryform").valid()){ 		  
				  submitform('cdrentryform'); 
			  }
		});*/
		
		jQuery.validator.addMethod("checkSelectCordEntry", function(value, element) {
		    if($j("#cordSearchable").val()=='0'){
                return true;
			}else{
				return (true && (parseInt(value) != -1));
			}			
		}, "<s:text name='garuda.cordentry.PlsSel'/>");/*}, "Please Select Value");*****/
		
	});

	function cdrNext(){
		setTimeout(function(){
	  		showprogressMgs();
		},0);
		setTimeout(function(){
		if($j("#cdrentryform").valid()){
			var str = "<s:text name="garuda.cbu.cordentry.checkId"/>";
			if($j("#checkMatRegDefer").val()=='false' || $j("#checkMatRegDefer").val()==false){
				str += "<br><s:text name="garuda.cbu.cordentry.matregcbustatus"/> <img height='15px' src='./images/important.gif'></br>";
			}
			/*if($j("#checkMatReg").val()=='false' || $j("#checkMatReg").val()==false){
				str += "<br><s:text name="garuda.cbu.cordentry.samematregid"/> <img height='15px' src='./images/important.gif'></br>";
			}
			if($j("#checkReg").val()=='false' || $j("#checkReg").val()==false){
				str += "<br><s:text name="garuda.cbu.cordentry.samematregidcbuid"/> <img height='15px' src='./images/important.gif'></br>";
			}*/
			str += "<br><s:text name="garuda.cbu.cordentry.continueFlag"/></br>";
			jConfirm(str, '<s:text name="garuda.common.dialog.confirm"/>', function(r) {
			    if(r==true){
			    	//submitform('cdrentryform'); 
			    	document.getElementById('cdrentryform').submit();
				}			    
			});
		}
		},100);
		setTimeout(function(){
			closeprogressMsg();
		},0);
	}
	
	function addAdditionalId(){
		 if(maxadditionalidcount < 10){
			$j('#cordentrytable').each(function(){			
		       var n = 0;              
			       n = (additionalIdCount==0) ? 0  : additionalIdCount ; 
					   additionalIdCount++;
					   maxadditionalidcount++;			       
                 			  
		    	   var $table = $j(this);    	   
		    	   var tds = '';		    	  
		    	       tds += '<tr id="additionalid_row'+n+'"><td><s:text name="garuda.cordentry.label.additionalIdDesc"></s:text>:<span style="color: red;">*</span></td><td><s:textfield cssClass="required" id="additionalIds'+n+'additionalIdDescid"  name="additionalIds['+n+'].additionalIdDesc" maxlength="30" onchange="additonalIdscall('+n+')" ></s:textfield></td>'
		        	   tds +='<td style="width:7%;"><s:text name="garuda.cordentry.label.additionalTypeId"></s:text>:<span style="color: red;">*</span></td><td><select id="additionalIds'+n+'additionalIdValueTypeid" name="additionalIds['+n+'].additionalIdValueType" class="required" onchange="additonalIdscall('+n+')" ><option value="">Select</option><option value="cbu">CBU</option><option value="maternal">Maternal</option></select></td>';
		       	       tds += '<td><s:text name="garuda.cordentry.label.additionalId"></s:text>:<span style="color: red;">*</span></td><td style="width:60%;"><s:textfield cssClass="required" id="additionalIds'+n+'additionalId" name="additionalIds['+n+'].additionalId" maxlength="30"  onchange="additonalIdscall('+n+')"></s:textfield></td><td><img style="cursor: pointer;vertical-align:middle;"  src="images/cross.png" border="0"  onclick="javascript:deleteRow('+n+');" /></td></tr>';
	          	       $j(this).append(tds);   
		           	if(maxadditionalidcount==10){
		   		      $j("#addbutton").css("cursor","default");
		   	        } 	   
		       });			   
		 }				 
				    else{
				      $j("#addbutton").css("cursor","default");
				  }
	 }
	 
	function htmlUniqueIdOnBag(){
		var paramObj={
	    	     data:$j('#idonbagvalresult').val(),
	    	     regId:$j('#cburegid').val(),
	    	     isbt:$j('#isbidin').val(),
	    	     locId:$j('#loccbuid').val()
	     }
    var  url = "getUniqIdOnBagHtml?"+$j.param(paramObj);
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
			data : $j("#cdrentryform").serialize(),
	        success: function (result, status, error){		        	
	        	     	$j("#idonbag").html(result.htmlStr);
	        },
	        error: function (request, status, error) {
	        	alert("Error " + error);
	            alert(request.responseText);
	        }
		});	            
   }

	function uniqueIdOnBagVal(val,id){	        
        $j("#idonbagval").show();
        if(val==-1){
      	  $j("#idonbagvalresult").val("");			 
        }else if(val==$j("#uRegId").val()){
            var val1 = $j("#cburegid").val();
            $j("#idonbagvalresult").val(val1);
        }else if(val==$j("#uLocalId").val()){
      	  var val1 = $j("#loccbuid").val();
            $j("#idonbagvalresult").val(val1);
        }else if(val==$j("#uIsbtId").val()){
      	  var val1 = $j("#isbidin").val();
            $j("#idonbagvalresult").val(val1);
        }else if(val==$j("#uAddId").val()){        	  
			    setIdOnBagValResult();
        }
	 }

	function deleteRow(rowid) {            	 
		$j('#additionalid_row'+rowid).remove();	
        maxadditionalidcount--;	
        if(maxadditionalidcount < 10){            			    
		   $j("#addbutton").css("cursor","pointer");
        }			
     /* removing the additional ID Unique Product identity on bag drop down.*/           	
		htmlUniqueIdOnBag();	 
   }	

	function setIdOnBagValResult(){        
		$j("#idonbagvalresult").val($j("#cbuOnBag option:selected").text());			
  }

   function additonalIdscall(rownumber){
        var description   = $j("#additionalIds"+rownumber+"additionalIdDescid").val();
		  var typeofid      = $j("#additionalIds"+rownumber+"additionalIdValueTypeid").val();
		  var additionalid  = $j("#additionalIds"+rownumber+"additionalId").val();
		  if(description !='' && typeofid !='Select' && additionalid !=''){
		     htmlUniqueIdOnBag();				 
		 }
   }   

     function resetForm(){
    	 document.getElementById("cdrentryform").reset();	  
     }
</script>	
	<div class="col_100 maincontainer "><div class="col_100">
	<s:form id="cdrentryform" action="openWholeCordEntry" method="POST">
	<s:hidden id="userid" value="%{#request.userid}" name="userid"></s:hidden>
	<s:hidden id="checkMatRegDefer" name="checkMatRegDefer"/>
	<s:hidden id="checkMatReg" name="checkMatReg"/>
	<s:hidden id="checkReg" name="checkReg"/>
	<s:hidden id="checkMatLocal" name="checkMatLocal"/>
	<s:hidden id="checkLocal" name="checkLocal"/>	
	<s:hidden id="uRegId" name="cdrCbuPojo.uniqueRegId"></s:hidden>
	<s:hidden id="uLocalId" name="cdrCbuPojo.uniqueLocaId"></s:hidden>
	<s:hidden id="uIsbtId" name="cdrCbuPojo.uniqueIsbtId"></s:hidden>
	<s:hidden id="uAddId" name="cdrCbuPojo.uniqueAddId"></s:hidden>
	<s:hidden id="deferId" name="deferId" value="%{#request.deferStatus}"/>
	<s:hidden id="regmatidstatus" name="registrymaternalidstaus" />
	<s:hidden id="cordEntryOnceToken" name="cordEntryOnceToken" value="%{#session.cordEntryOnceToken}"/>
	     <div class="column">
	      <div class="portlet" id="cdrentryparent" >
					<div id="cdrentry" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
					<span class="ui-icon ui-icon-minusthick"></span>
					<!--<span class="ui-icon ui-icon-close" ></span>-->
					<s:text name="garuda.cordentry.label.cordentry" />
					</div>
					<div class="portlet-content">						
						    <table width="100%" cellspacing="0" cellpadding="0" border="0" id="cordentrytable">
                              <tr>
						         <td width="20%" style="vertical-align:top;"><s:text name="garuda.cbuentry.label.CBB" />:<span style="color: red;">*</span></td>
							     <td width="25%" style="vertical-align:top;">
							        <s:select name="cdrCbuPojo.fkCbbId" cssClass="cbuclass" listKey="siteId" listValue="siteIdentifier" list="#request.sites" id="fkCbbId" headerKey="-1" headerValue="Select"></s:select>
							     </td>
							     <td colspan="2">
							     </td>
						      </tr>
						      
						      <tr>
						          <td width="20%" onmouseover="return overlib('<s:text
										name="garuda.cbuentry.label.cbuRegToolTip"></s:text>');" 
										onmouseout="return nd();">
						              <s:text name="garuda.cbuentry.label.registrycbuid"></s:text>:<span style="color: red;">*</span>
						          </td>
						          <td width="25%" style="vertical-align:top;">
						              <s:textfield maxlength="11" name="cdrCbuPojo.registryId" id="cburegid" cssClass="copyPaste" onchange="htmlUniqueIdOnBag()"></s:textfield>
						          </td>
						          <td colspan="2" width="25%" style="vertical-align:top;">
						             <s:textfield maxlength="11" name="cbuRegistryId" id="othercburegid" cssClass="copyPaste" ></s:textfield>
						          </td>	
						          <td colspan="2" width="30%"></td>					          
						      </tr>
						      <tr>
						          <td width="20%" onmouseover="return overlib('<s:text
										name="garuda.cbuentry.label.matRegToolTip"></s:text>');" 
										onmouseout="return nd();">
						              <s:text name="garuda.cbuentry.label.maternalregistryid"></s:text>:<span style="color: red;">*</span>
						          </td>
						          <td width="25%" style="vertical-align:top;">
						              <s:textfield maxlength="11" name="cdrCbuPojo.registryMaternalId" id="regMaternalId" cssClass="copyPaste" ></s:textfield>
						          </td>
						          <td colspan="2" width="25%" style="vertical-align:top;">						          
						             <s:textfield maxlength="11" name="cbuRegistryMaternalId" id="otherregMaternalId" cssClass="copyPaste"></s:textfield>
						          </td>
						          <td colspan="2" width="30%"></td>
						      </tr>
						       <tr>
						          <td width="20%" onmouseover="return overlib('<s:text
										name="garuda.cbuentry.label.cbuLocalToolTip"></s:text>');" 
										onmouseout="return nd();">
						              <s:text name="garuda.cbuentry.label.localcbuid"></s:text>:<span style="color: red;">*</span>
						          </td>
						          <td width="25%" style="vertical-align:top;">
						              <s:textfield maxlength="30" name="cdrCbuPojo.localCbuId" id="loccbuid" cssClass="copyPaste" onchange="htmlUniqueIdOnBag()"></s:textfield>
						          </td>
						          <td colspan="2" width="25%" style="vertical-align:top;">
						              <s:textfield maxlength="30" name="cbuLocalCbuId" id="otherloccbuid" cssClass="copyPaste"></s:textfield>
						          </td>
						          <td colspan="2" width="30%"></td>
						      </tr>
						      <tr>
						         <td width="20%" onmouseover="return overlib('<s:text
										name="garuda.cbuentry.label.matLocalToolTip"></s:text>');" 
										onmouseout="return nd();">
						              <s:text name="garuda.cbuentry.label.maternallocalid"></s:text>:
						          </td>
						          <td width="25%" style="vertical-align:top;">
						              <s:textfield maxlength="30" name="cdrCbuPojo.localMaternalId" id="matlocalid" cssClass="copyPaste"></s:textfield>
						          </td>
						          <td colspan="2" width="25%" style="vertical-align:top;">
						              <s:textfield maxlength="30" name="cbuLocalMaternalId" id="othermatlocalid" cssClass="copyPaste"></s:textfield>
						          </td>
						          <td colspan="2" width="30%"></td>
						      </tr>
						       <tr>
						          <td width="20%" onmouseover="return overlib('<s:text
										name="garuda.cbuentry.label.isbtDinToolTip"></s:text>');" 
										onmouseout="return nd();">
						              <s:text name="garuda.cordentry.label.isbtid"></s:text>:
						          </td>
						          <td width="25%" style="vertical-align:top;">
						              <s:textfield id="isbidin" name="cdrCbuPojo.cordIsbiDinCode" cssClass="copyPaste" onchange="htmlUniqueIdOnBag()"></s:textfield>
						         </td>
						         <td colspan="2" width="25%" style="vertical-align:top;">
						              <s:textfield id="otherisbidin" name="cbuCordIsbiDinCode" cssClass="copyPaste"></s:textfield>
						         </td>
						          <!--<td>
						              <s:text name="garuda.cdrcbuview.label.donornumber"></s:text> <span style="color: red;">*</span>
						          </td>
						          <td>
						              <s:textfield name="cdrCbuPojo.cordDonorIdNumber"></s:textfield>
						          </td>
						      </tr>
						       <tr>
						          <td>
						              <s:text name="garuda.cdrcbuview.label.isbt_product_code"></s:text>
						          </td>
						          <td>
						              <s:textfield name="cdrCbuPojo.cordIsitProductCode"></s:textfield>						              
						          </td>
						          <td colspan="2">&nbsp;		             
						          </td>-->
						          <td colspan="2" width="30%"></td>
						      </tr>
						       <tr>
						          <td width="20%" onmouseover="return overlib('<s:text
										name="garuda.cbuentry.label.uniqueIdToolTip"></s:text>');" 
										onmouseout="return nd();">
						              <s:text name="garuda.cbuentry.label.idbag"></s:text>:<span style="color: red;">*</span>
						          </td>
						          <td width="25%">
						              <!--<s:select name="cbuOnBag" id="cbuOnBag" multiple="true" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_IDS]" listKey="pkCodeId" listValue="description" ></s:select>
						          -->
						             <div id="idonbag">
						                 <select name="cbuOnBag" id="cbuOnBag">
						                    <option value="-1">Select</option>
						                 </select>
						             </div>
						          </td>
						          <td colspan="2" width="25%">	
						              <div id="idonbagval" style="display: none;">		                   
						                  <s:hidden name="cdrCbuPojo.numberOnCbuBag" id="idonbagvalresult"></s:hidden>
						              </div>	             
						          </td>	
						          <td colspan="2" width="30%"></td>					          
						      </tr>						      
						    </table>
						    <table width="100%" cellspacing="0" cellpadding="0" border="0" >
						        <tr >
							         <td colspan="5" width="30%">
									  <span style="cursor: pointer;" onclick="addAdditionalId()" id="addbutton">
									 <img style="vertical-align: middle;" src="images/addcomment.png" border="0" id="plus"/>
									 <s:text name="garuda.cordentry.label.addAdditionalId"></s:text></span>
									 </td>						         		    
						        </tr>
							  <tr >
							  <td colspan="5" width="100%">
							    <table bgcolor="#cccccc" width="100%">
							         <tr bgcolor="#cccccc" valign="baseline">
									   <td width="70%">
									             <jsp:include page="../cb_esignature.jsp" flush="true">
								                        <jsp:param value="submitcdrnext" name="submitId"/>
														<jsp:param value="cordIdEntryValid" name="invalid" />
														<jsp:param value="cordIdEntryMinimum" name="minimum" />
														<jsp:param value="cordIdEntryPass" name="pass" />
									             </jsp:include>
									   </td>
									   <td align="right" width="15%" valign="top">
									        <s:if test="hasEditPermission(#request.editCBURght)==true">
									            <input type="button" id="submitcdrnext" disabled="disabled" onclick="cdrNext()" value="<s:text name="garuda.common.lable.next"/>" />
									        </s:if>
									   </td>
									   <td width="15%" valign="top"><input type="button"  onclick="resetForm()" value="<s:text name="garuda.unitreport.label.button.cancel"/>" /></td>
									 </tr>
							    </table>						          		           
						      </td>
							  </tr >
						    </table>						
					</div></div></div>
	    </div></s:form>
	</div></div>
	<script type="text/javascript">
	   resetForm();
	</script>