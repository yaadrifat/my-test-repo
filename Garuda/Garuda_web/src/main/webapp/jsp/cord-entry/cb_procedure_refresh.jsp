<%@ taglib prefix="s"  uri="/struts-tags"%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", 0);
if(request.getParameter("sampInventReadOnly")!=null && !request.getParameter("sampInventReadOnly").equals("")){
	Boolean val = Boolean.valueOf(request.getParameter("sampInventReadOnly"));
	request.setAttribute("sampInventReadOnly",val);
}
%>
<s:hidden name="cdrCbuPojo.procVersion" />
<s:hidden name="cdrCbuPojo.procStartDate" />
<s:hidden name="cdrCbuPojo.procTermiDate" />
<div id="refCbbProcOnly">
	<s:if test="#request.showProc==true">
		<jsp:include page="cb_procedure_ref_inter.jsp"></jsp:include>
	</s:if>
</div>
<table width="100%" cellpadding="0" cellspacing="0">
   <tr>
       <td>
       </td>
       <td>
       </td>
       <td colspan="4"></td>
   </tr>
   <tr>
       <td colspan="6">
          <s:if test="#request.sampInventReadOnly==true">
              <jsp:include page="cb_cord-entry_sampinvent_ro.jsp" flush="true"></jsp:include>
          </s:if>
          <s:else>
              <jsp:include page="cb_cord-entry_sampinvent.jsp" flush="true"></jsp:include>
          </s:else>
       </td>
   </tr>
</table>