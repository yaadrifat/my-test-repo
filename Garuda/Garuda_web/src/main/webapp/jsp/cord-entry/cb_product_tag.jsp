<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", 0); 
%>
<div id="readOnlyProductTag">
    <table>
         <tr>
            <td align="left">
                <a href="#" style="cursor:pointer;" onclick="showPdfForms('<s:property value="cdrCbuPojo.CordID"/>','<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@PDF_PRODUCT_TAG"/>')"><s:text name="garuda.pdf.nmdp.viewproducttag"/></a>
            </td>
         </tr>
    </table>
	<!--<table width="100%" cellpadding="0" cellspacing="0">
	   <tr>
	       <td>
	           <s:text name="garuda.cbuentry.label.cbbname" />
	       </td>
	       <td>
	          <s:if test="cdrCbuPojo.site!=null">
	              <input type="text" name="sTagName" value="<s:property value="%{cdrCbuPojo.site.siteName}"/>" />
	          </s:if>
	       </td>
	       <td colspan="2"></td>
	   </tr>
	   <tr>
	      <td width="25%">
	           <s:text name="garuda.cbuentry.label.idoncbubag" />:
	       </td>
		  <td width="25%">
		       <s:textfield name="cdrCbuPojo.numberOnCbuBag" id="idonbagvalresult"></s:textfield>
		  </td>
	
		   <td width="25%">
	         <s:text name="garuda.fdoe.level.notapplicable" />:
	      </td>
	      <td width="25%">
	        <input type="checkbox" name="producttagnumberOnCbuBag" />
	      </td>
	   </tr>
	   <tr>
	      <td width="25%">
	         <s:text name="garuda.cbuentry.label.registrycbuid"></s:text>:
	      </td>
	      <td width="25%">
	         <s:textfield maxlength="11" name="cdrCbuPojo.registryId" readonly="true" onkeydown="cancelBack();" ></s:textfield>
	      </td>
	      <td width="25%">
	         <s:text name="garuda.fdoe.level.notapplicable" />:
	      </td>
	      <td width="25%">
	        <input type="checkbox" name="producttagregistrycbuid" />
	      </td>
	   </tr>
	   <tr>
	      <td width="25%">
	         <s:text name="garuda.cbuentry.label.localcbuid"></s:text>:
	      </td>
	      <td width="25%">
	         <s:textfield maxlength="11" name="cdrCbuPojo.localCbuId" readonly="true" onkeydown="cancelBack();" ></s:textfield>
	      </td>
	      <td width="25%">
	         <s:text name="garuda.fdoe.level.notapplicable" />:
	      </td>
	      <td width="25%">
	        <input type="checkbox" name="producttaglocalcbuid" />
	      </td>
	   </tr>
	   <tr>
	      <td width="25%">
	         <s:text name="garuda.cordentry.label.isbtid"></s:text>:
	      </td>
	      <td width="25%">
	         <s:textfield maxlength="11" name="cdrCbuPojo.cordIsbiDinCode" readonly="true" onkeydown="cancelBack();" ></s:textfield>
	      </td>
	      <td width="25%">
	         <s:text name="garuda.fdoe.level.notapplicable" />:
	      </td>
	      <td width="25%">
	        <input type="checkbox" name="producttagisbtid" />
	      </td>
	   </tr>
	  
		   
		       <s:if test="declPojo.idmQues7a==false || declPojo.idmQues6==false || declPojo.mrqQues2==false">
		       <tr>
		       	 	<td>
		       	 			<s:text name="garuda.fdoe.level.applicable_hpc"></s:text>:
		       	 	</td>
		      		<td>
		         			<h1><s:text name="garuda.cbu.product.tag1"/></h1>
		      				      		      
		      		</td>		      
		       		<td>
		       				<input type="checkbox" name="applicable_hpc_tag1" checked="checked" disabled="disabled"/>
		       		</td>
		       		<td>
		       				<s:text name="garuda.fdoe.level.nonapplicable_hpc" />:
		       		</td>
		       		<td>
		       				<input type="checkbox" name="producttagnonapplicable_hpc" disabled="disabled" />
		       		</td>
		       	</tr>
		       	<tr>
		           <td colspan="5">
		                 <img style="width:100%;height:50%" src="images/tag1.jpg"/>
		           </td>
		       </tr>
		       </s:if>
	       		<s:else>
	       			<tr>
	       				<td>
	       	 					<s:text name="garuda.fdoe.level.applicable_hpc"></s:text>:
	       	 			</td>
	       	 			<td>
	       	 			</td>
	       		 		<td>
	       						<input type="checkbox" name="applicable_hpc_tag1"  disabled="disabled"/>
	       				</td>
	       			
	       				<td>
	       						<s:text name="garuda.fdoe.level.nonapplicable_hpc" />:
	       				</td>
	       				<td>
	       						<input type="checkbox" name="producttagnonapplicable_hpc" checked="checked" disabled="disabled"/>
	       				</td>
	       		   </tr>
	      		 </s:else>		   
		    <s:if test="declPojo.idmQues8==true">
		     <tr>
		      		<td>
		       	 			<s:text name="garuda.fdoe.level.applicable_hpc"></s:text>:
		       	 	</td>
		      		<td>
		         			<h1><s:text name="garuda.cbu.product.tag2"/></h1>
		      		
		      		        
		      		</td>		      
		       		<td>
		       				<input type="checkbox" name="applicable_hpc_tag2" checked="checked" disabled="disabled"/>
		       		</td>
		       		<td>
		       				<s:text name="garuda.fdoe.level.nonapplicable_hpc" />:
		       		</td>
		       		<td>
		       				<input type="checkbox" name="producttagnonapplicable_hpc" disabled="disabled" />
		       		</td>
		       </tr>
		       <tr>
		           <td colspan="5">
		                 <img style="width:100%;height:50%" src="images/tag2.jpg"/>
		           </td>
		       </tr>
		      </s:if>
		      	<s:else>
		       			<tr>
		       				<td>
		       	 					<s:text name="garuda.fdoe.level.applicable_hpc"></s:text>:
		       	 			</td>
		       		 		<td>
		       	 			</td>
		       				<td>
		       						<input type="checkbox" name="applicable_hpc_tag2"  disabled="disabled"/>
		       				</td>
		       				<td>
		       						<s:text name="garuda.fdoe.level.nonapplicable_hpc" />:
		       				</td>
		       				<td>
		       						<input type="checkbox" name="producttagnonapplicable_hpc" checked="checked" disabled="disabled"/>
		       				</td>
		       			</tr>
		      		 </s:else> 
		   <s:if test="declPojo.phyFindQues5==true || declPojo.mrqQues2==true || declPojo.phyFindQues4==true">
		   <tr>
		           <td>
		       	 			<s:text name="garuda.fdoe.level.applicable_hpc"></s:text>:
		       	 	</td>
		      		<td>
		         			<h1><s:text name="garuda.cbu.product.tag3"/></h1>
		      		
		      		        
		      		</td>		      
		       		<td>
		       				<input type="checkbox" name="applicable_hpc_tag3" checked="checked" disabled="disabled"/>
		       		</td>
		       		<td>
		       				<s:text name="garuda.fdoe.level.nonapplicable_hpc" />:
		       		</td>
		       		<td>
		       				<input type="checkbox" name="producttagnonapplicable_hpc" disabled="disabled" />
		       		</td>
		      </tr>
		      <tr>
		          <td colspan="5">
		               <img style="width:100%;height:50%" src="images/tag3.jpg"/>
		          </td>
		      </tr>
		     </s:if>
		     <s:else>
		      <tr> 			
			       <td>
			       	 		<s:text name="garuda.fdoe.level.applicable_hpc"></s:text>:
			       	</td>
			       	<td>
			       	</td>
			       	<td>
			       		<input type="checkbox" name="applicable_hpc_tag3"  disabled="disabled"/>
			       	</td>
			       	<td>
			       		<s:text name="garuda.fdoe.level.nonapplicable_hpc" />:
			       	</td>
			       	<td>
			       		<input type="checkbox" name="producttagnonapplicable_hpc" checked="checked" disabled="disabled"/>
			       	</td>
			    </tr>
		      </s:else>		        
		   	
	</table>
--></div>
<s:if test="#request.readonly=='true'">
	  <script>
		 $j("#readOnlyProductTag").find('input, textarea','select').each(function(){
			  $j(this).attr('disabled', 'disabled');
		  });
	  </script>
</s:if>