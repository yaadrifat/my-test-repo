<%@ taglib prefix="s"  uri="/struts-tags"%>
<%@page import="com.velos.ordercomponent.action.CBUAction" %>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction" %>
<%
String mrq = (String)request.getParameter("mrq");
request.setAttribute("mrq",mrq);
String idm = (String)request.getParameter("idm");
request.setAttribute("idm",idm);
String physi = (String)request.getParameter("physi");
request.setAttribute("physi",physi);
String other = (String)request.getParameter("other");
request.setAttribute("other",other);
%>
<%
response.setHeader("Cache-Control","no-cache"); 
response.setHeader("Pragma","no-cache"); 
response.setDateHeader ("Expires", 0); 
String val = request.getParameter("readonly");
request.setAttribute("readonly",val);
%>
<script>
  function showQuestionDumnWithDetailDumn(val,detailid,quesid){
	  if(val=='false'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
     }else if(val=='true'){
    	 $j("#"+detailid).hide();
         $j("#"+quesid).hide();
     }
  }

  function showYesQuestionWithDetailDumn(val,detailid,quesid){
	  if(val=='true'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
     }else if(val=='false'){
    	 $j("#"+detailid).hide();
         $j("#"+quesid).hide();
     }
  }

  function showMultipeQuestionWithDetailDumn(val,detailid,quesid,quesid1){
	  if(val=='false'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
         $j("#"+quesid1).show();
     }else if(val=='true'){
         $j("#"+detailid).hide();
         $j("#"+quesid).hide();
         $j("#"+quesid1).hide();
     }
  }

  function showMultipeYesQuestionWithDetailDumn(val,detailid,quesid,quesid1,quesid3,quesid4,quesid5,quesid6,quesid7,quesid8,quesid9){
	  if(val=='true'){
         $j("#"+detailid).show();
         $j("#"+quesid).show();
         $j("#"+quesid1).show();
         $j("#"+quesid3).show();
         $j("#"+quesid4).show();
         $j("#"+quesid5).show();
         $j("#"+quesid6).show();
         $j("#"+quesid7).show();
         $j("#"+quesid8).show();
         $j("#"+quesid9).show(); 
     }else if(val=='false'){
         $j("#"+detailid).hide();
         $j("#"+quesid).hide();
         $j("#"+quesid1).hide();
         $j("#"+quesid3).hide();
         $j("#"+quesid4).hide();
         $j("#"+quesid5).hide();
         $j("#"+quesid6).hide();
         $j("#"+quesid7).hide();
         $j("#"+quesid8).hide();
         $j("#"+quesid9).hide(); 
     }
  }

  function showQuestionDumn(val,quesid){
	  if(val=='false'){
	         $j("#"+quesid).show();
	  }else if(val=='true'){
	         $j("#"+quesid).hide();
	  }
  }

  function showMultipleQuestionDumn(val,quesid,quesid1){
	  if(val=='false'){
	         $j("#"+quesid).show();
	         $j("#"+quesid1).show();
	  }else if(val=='false'){
	         $j("#"+quesid).hide();
	         $j("#"+quesid1).hide();
	  }
  }

  function showDeatilDumn(val,detailid){
	  if(val=='false'){
	         $j("#"+detailid).show();
	  }else if(val=='true'){
	         $j("#"+detailid).hide();
	  }
  }

  function showYesDeatilDumn(val,detailid){
	  if(val=='true'){
	         $j("#"+detailid).show();
	  }else if(val=='false'){
	         $j("#"+detailid).hide();
	  }
  }
</script>
<s:if test="#request.mrq=='true'">
	<s:if test="dumnPojo.mrqQues1!=null && dumnPojo.mrqQues1==false">
	    <script>
	          showQuestionDumnWithDetailDumn('false','mrqques1adddetaildumn','mrqQues1atrdumn');
	    </script>
	</s:if>
	<s:if test="dumnPojo.mrqQues1a!=null && dumnPojo.mrqQues1a==false">
	    <script>
	          showQuestionDumn('false','mrqQues1btrdumn');
	    </script>
	</s:if>
	<s:if test="dumnPojo.mrqQues2!=null && dumnPojo.mrqQues2==true">
	    <script>
	         showYesDeatilDumn('true','mrqQues2adddetaildumn');
	    </script>
	</s:if>
</s:if>
<s:if test="#request.physi=='true'"> 
	<s:if test="dumnPojo.phyFindQues3!=null && dumnPojo.phyFindQues3==false">
	    <script>
	        showMultipeQuestionWithDetailDumn('false','phyFindQues3AddDetaildumn','phyFindQues3atrdumn','phyFindQues3ctrdumn');
	    </script>
	</s:if>
	<s:if test="dumnPojo.phyFindQues3a!=null && dumnPojo.phyFindQues3a==false">
	    <script>
	        showQuestionDumn('false','phyFindQues3btrdumn');
	    </script>
	</s:if>
	<s:if test="dumnPojo.phyFindQues3c!=null && dumnPojo.phyFindQues3c==false">
	    <script>
	        showQuestionDumn('false','phyFindQues3dtrdumn');
	    </script>
	</s:if>
	<s:if test="dumnPojo.phyFindQues4!=null && dumnPojo.phyFindQues4==true">
	    <script>
	        showYesDeatilDumn('true','phyFindQues4AddDetaildumn');
	    </script>
	</s:if>
	<s:if test="dumnPojo.phyFindQues5!=null && dumnPojo.phyFindQues5==true">
	    <script>
	        showMultipeYesQuestionWithDetailDumn('true','phyFindQues5AddDetaildumn','phyFindQues5atrdumn','phyFindQues5btrdumn','phyFindQues5ctrdumn','phyFindQues5dtrdumn','phyFindQues5etrdumn','phyFindQues5ftrdumn','phyFindQues5gtrdumn','phyFindQues5htrdumn','phyFindQues5itrdumn');
	    </script>
	</s:if>
</s:if>
<s:if test="#request.idm=='true'">
	<s:if test="dumnPojo.idmQues6!=null && dumnPojo.idmQues6==false">
	    <script>
	        showQuestionDumnWithDetailDumn('false','idmQues6AddDetaildumn','idmQues6atrdumn');
	    </script>
	</s:if>
	<s:if test="dumnPojo.idmQues6a!=null && dumnPojo.idmQues6a==false">
	    <script>
	       showQuestionDumn('false','idmQues6btrdumn');
	    </script>
	</s:if>
	<s:if test="dumnPojo.idmQues7!=null && dumnPojo.idmQues7==false">
	    <script>
	      showQuestionDumnWithDetailDumn('false','idmQues7AddDetaildumn','idmQues7atrdumn')
	    </script>
	</s:if>
	<s:if test="dumnPojo.idmQues7a!=null && dumnPojo.idmQues7a==false">
	    <script>
	      showQuestionDumn('false','idmQues7btrdumn');
	    </script>
	</s:if>
</s:if>
<s:if test="#request.other=='true'">
   <s:if test="dumnPojo.otherQues9!=null && dumnPojo.otherQues9==false">
	    <script>
	      showQuestionDumn('false','otherQues9atrdumn');
	    </script>
	</s:if>
	<s:if test="dumnPojo.otherQues9a!=null && dumnPojo.otherQues9a==false">
	    <script>
	      showQuestionDumn('false','otherQues9btrdumn');
	    </script>
	</s:if>
	<s:if test="dumnPojo.otherQues9b!=null && dumnPojo.otherQues9b==false">
	    <script>
	      showQuestionDumn('false','idmQues6btrdumn');
	    </script>
	</s:if>
	<s:if test="dumnPojo.otherQues10!=null && dumnPojo.otherQues10==true">
	    <script>
	      showYesDeatilDumn('true','otherQues10AddDetaildumn')
	    </script>
	</s:if>	
</s:if>
<div id="dumnReadOnly">
   <table width="100%" cellpadding="0" cellspacing="0" class="displaycdr">
	    <thead>
				<tr>
					<th><s:text name="garuda.common.eligibility.factors"/></th>
					<th><s:text name="garuda.questionnaire.level.additionaldetail"/></th>
				</tr>
		</thead>
		<tbody>
		     <s:if test="#request.mrq=='true'">
		       <tr>
		           <td valign="top" width="35%">
		               <s:text name="garuda.questionnaire.level.mrq" />
		           </td>
		           <td style="border: 1px solid #EDECEB;" width="65%" bgcolor="#7DCFD5">
		              <table width="100%" cellpadding="0" cellspacing="0">
		                <tr valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.mrqques1" /></td>
		                    <td nowrap="nowrap" width="50%"><s:radio name="dumnPojo.mrqQues1" id="mrqques1dumn" list="#{'true':'Yes','false':'NO'}" onclick="showQuestionDumnWithDetailDumn(this.value,'mrqques1adddetaildumn','mrqQues1atrdumn')"></s:radio></td>
		                </tr>
		                <tr><td width="50%"></td>
		                <td id="mrqques1adddetaildumn" style="display: none;" width="50%">
		                    <s:textarea rows="3" cols="10" name="dumnPojo.mrqQues1AddDetail"></s:textarea>
		                </td>
		                </tr>
		                <tr id="mrqQues1atrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.mrqques2" /></td>
		                    <td nowrap="nowrap" width="50%"><s:radio name="dumnPojo.mrqQues1a" id="mrqQues1adumn" list="#{'true':'Yes','false':'NO'}" onclick="showQuestionDumn(this.value,'mrqQues1btrdumn')" ></s:radio></td>                    
		                </tr>
		                <tr id="mrqQues1btrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.mrqques3" />:</td>
		                    <td nowrap="nowrap" width="50%"><s:radio name="dumnPojo.mrqQues1b" id="mrqQues1bdumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>                    
		                </tr>
		                <tr valign="top">
		                   <td width="50%"><s:text name="garuda.questionnaire.level.mrqques4" />:</td>
		                   <td nowrap="nowrap" width="50%"><s:radio name="dumnPojo.mrqQues2" id="mrqQues2dumn" list="#{'true':'Yes','false':'NO'}" onclick="showYesDeatilDumn(this.value,'mrqQues2adddetaildumn')"></s:radio></td>
		                </tr>
		                <tr>
		                	<td width="50%"></td>
		                	<td id="mrqQues2adddetaildumn" style="display: none;" width="50%">
		                        <s:textarea rows="3" cols="10" name="dumnPojo.mrqQues2AddDetail"></s:textarea>
		                	</td>
		                </tr>
		              </table>
		           </td>
		       </tr>
		       <tr>
       				<td colspan="2" height="5%"></td>
       			</tr>
		     </s:if>
		     <s:if test="#request.physi=='true'"> 
		       <tr>
		           <td valign="top" width="35%">
		               <s:text name="garuda.questionnaire.level.physfinddumn" />
		           </td>
		           <td style="border: 1px solid #EDECEB;" width="65%" bgcolor="#7DCFD5">
		              <table width="100%" cellpadding="0" cellspacing="0">
		                <tr valign="top">
		                   <td width="50%"><s:text name="garuda.questionnaire.level.physfind0" />:</td>
		                   <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues3" id="phyFindQues3dumn" list="#{'true':'Yes','false':'NO'}" onclick="showQuestionDumn(this.value,'phyFindQues3atrdumn')"></s:radio></td>
		                </tr>
		                <!--<tr><td width="50%"></td>
		                	<td width="50%" id="phyFindQues3AddDetaildumn" style="display: none;">
		                        <s:textarea rows="3" cols="10" name="dumnPojo.phyFindQues3AddDetail"></s:textarea>
		                   </td>
		                </tr>
		                --><tr id="phyFindQues3atrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.physfind1" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues3a" id="phyFindQues3adumn" list="#{'true':'Yes','false':'NO'}" onclick="showQuestionDumn(this.value,'phyFindQues3btrdumn')" ></s:radio></td>                    
		                </tr>
		                <tr id="phyFindQues3btrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.physfind2" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues3b" id="phyFindQues3bdumn" list="#{'true':'Yes','false':'NO'}"  ></s:radio></td>                    
		                </tr>
		                <!--<tr id="phyFindQues3ctrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.physfind3" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues3c" id="phyFindQues3cdumn" list="#{'true':'Yes','false':'NO'}" onclick="showQuestionDumn(this.value,'phyFindQues3dtrdumn')" ></s:radio></td>                    
		                </tr>
		                <tr id="phyFindQues3dtrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.physfind4" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues3d" id="phyFindQues3ddumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>                    
		                </tr>
		                --><!--<tr valign="top">
		                   <td width="50%"><s:text name="garuda.questionnaire.level.physfind5" />:</td>
		                   <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues4" id="phyFindQues4dumn" list="#{'true':'Yes','false':'NO'}" onclick="showYesDeatilDumn(this.value,'phyFindQues4AddDetaildumn')"></s:radio></td>
		                </tr>
		                <tr><td width="50%"></td>
		                	<td id="phyFindQues4AddDetaildumn" style="display: none;" width="50%">
		                        <s:textarea rows="3" cols="10" name="dumnPojo.phyFindQues4AddDetail"></s:textarea>
		                   </td>
		                </tr>
		                --><tr valign="top">
		                   <td width="50%"><s:text name="garuda.questionnaire.level.physfind6" />:</td>
		                   <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues5" id="phyFindQues5dumn" list="#{'true':'Yes','false':'NO'}" 
		                   onclick="showMultipeYesQuestionWithDetailDumn(this.value,'phyFindQues5AddDetaildumn','phyFindQues5atrdumn','phyFindQues5btrdumn','phyFindQues5ctrdumn','phyFindQues5dtrdumn','phyFindQues5etrdumn','phyFindQues5ftrdumn','phyFindQues5gtrdumn','phyFindQues5htrdumn','phyFindQues5itrdumn')"></s:radio></td>
		                </tr>
		                <tr><td width="50%"></td>
		                	<td width="50%" id="phyFindQues5AddDetaildumn" style="display: none;" width="25%">
		                        <s:textarea rows="3" cols="10" name="dumnPojo.phyFindQues5AddDetail"></s:textarea>
		                	</td>
		                </tr>
		                <tr id="phyFindQues5atrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.physfind7" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues5a" id="phyFindQues5adumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>                    
		                </tr>
		                <tr id="phyFindQues5btrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.physfind8" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues5b" id="phyFindQues5bdumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>                    
		                </tr>
		                <tr id="phyFindQues5ctrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.physfind9" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues5c" id="phyFindQues5cdumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>                    
		                </tr>
		                <tr id="phyFindQues5dtrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.physfind10" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues5d" id="phyFindQues5ddumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>                    
		                </tr>
		                <tr id="phyFindQues5etrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.physfind11" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues5e" id="phyFindQues5edumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>                    
		                </tr>
		                <tr id="phyFindQues5ftrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.physfind12" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues5f" id="phyFindQues5fdumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>                    
		                </tr>
		                <tr id="phyFindQues5gtrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.physfind13" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues5g" id="phyFindQues5gdumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>                    
		                </tr>
		                <tr id="phyFindQues5htrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.physfind14" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues5h" id="phyFindQues5hdumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>                    
		                </tr>
		                <tr id="phyFindQues5itrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.physfind15" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.phyFindQues5i" id="phyFindQues5idumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>                    
		                </tr>
		              </table>
		           </td>
		       </tr>
		     </s:if> 
		     <s:if test="#request.idm=='true'">
		       <tr>
		           <td valign="top" width="35%">
		               <s:text name="garuda.questionnaire.level.idm" />
		           </td>
		           <td style="border: 1px solid #EDECEB;" width="65%" bgcolor="#7DCFD5">
		               <table width="100%" cellpadding="0" cellspacing="0">
		                   <tr valign="top">
			                   <td width="50%"><s:text name="garuda.questionnaire.level.idm1" />:</td>
			                   <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.idmQues6" id="idmQues6dumn" list="#{'true':'Yes','false':'NO'}" 
			                   onclick="showQuestionDumnWithDetailDumn(this.value,'idmQues6AddDetaildumn','idmQues6atrdumn')"></s:radio></td>
		                   </tr>
		                   <tr><td width="50%"></td>
		                   <td id="idmQues6AddDetaildumn" style="display: none;" width="50%">
			                        <s:textarea rows="3" cols="10" name="dumnPojo.idmQues6AddDetail"></s:textarea>
			                   </td>
		                   </tr>
		                   <tr id="idmQues6atrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.idm2" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.idmQues6a" id="idmQues6adumn" list="#{'true':'Yes','false':'NO'}" onclick="showQuestionDumn(this.value,'idmQues6btrdumn')"></s:radio></td>                    
		                   </tr>
		                   <tr id="idmQues6btrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.idm3" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.idmQues6b" id="idmQues6bdumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>                    
		                   </tr>
		                   <tr valign="top">
			                   <td width="50%"><s:text name="garuda.questionnaire.level.idm4" />:</td>
			                   <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.idmQues7" id="idmQues7dumn" list="#{'true':'Yes','false':'NO'}" 
			                   onclick="showQuestionDumnWithDetailDumn(this.value,'idmQues7AddDetaildumn','idmQues7atrdumn')"></s:radio></td>
		                   </tr>
		                   <tr>
		                   <td width="50%"></td>
		                   <td id="idmQues7AddDetaildumn" style="display: none;" width="50%">
			                        <s:textarea rows="3" cols="10" name="dumnPojo.idmQues7AddDetail"></s:textarea>
			                   </td>
		                   </tr>
		                   <tr id="idmQues7atrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.idm5" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.idmQues7a" id="idmQues7adumn" list="#{'true':'Yes','false':'NO'}" onclick="showQuestionDumn(this.value,'idmQues7btrdumn')"></s:radio></td>                    
		                   </tr>
		                   <tr id="idmQues7btrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.idm3" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.idmQues7b" id="idmQues7bdumn" list="#{'true':'Yes','false':'NO'}" ></s:radio></td>                    
		                   </tr>
		                   <tr valign="top">
			                   <td width="50%"><s:text name="garuda.questionnaire.level.idm6" />:</td>
			                   <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.idmQues8" id="idmQues8dumn" list="#{'true':'Yes','false':'NO'}" 
			                   onclick="showYesDeatilDumn(this.value,'idmQues8AddDetaildumn')"></s:radio></td>
		                   </tr>
		                   <tr>
		                   <td width="50%"></td>
		                   <td id="idmQues8AddDetaildumn" style="display: none;" width="50%">
			                        <s:textarea rows="3" cols="10" name="dumnPojo.idmQues8AddDetail"></s:textarea>
			                   </td>
		                   </tr>
		               </table>
		           </td>
		       </tr>
		     </s:if>
		     <s:if test="#request.other=='true'">
		       <tr>
		           <td valign="top" width="35%">
		               <s:text name="garuda.questionnaire.level.other" />
		           </td>
		           <td style="border: 1px solid #EDECEB;" width="65%" bgcolor="#7DCFD5">
		               <table width="100%" cellpadding="0" cellspacing="0">
		                  <tr id="otherQues9trdumn" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.other0" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.otherQues9" id="otherQues9dumn" list="#{'true':'Yes','false':'NO'}" onclick="showQuestionDumn(this.value,'otherQues9atrdumn')"></s:radio></td>                    
		                  </tr>
		                  <tr id="otherQues9atrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.other1" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.otherQues9a" id="otherQues9adumn" list="#{'true':'Yes','false':'NO'}" onclick="showQuestionDumn(this.value,'otherQues9btrdumn')"></s:radio></td>                    
		                  </tr>
		                  <tr id="otherQues9btrdumn" style="display: none;" valign="top">
		                    <td width="50%"><s:text name="garuda.questionnaire.level.other2" />:</td>
		                    <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.otherQues9b" id="otherQues9bdumn" list="#{'true':'Yes','false':'NO'}" onclick="showQuestionDumn(this.value,'idmQues6btrdumn')"></s:radio></td>                    
		                  </tr>
		                  <tr valign="top">
			                   <td width="50%"><s:text name="garuda.questionnaire.level.other3" />:</td>
			                   <td width="50%" nowrap="nowrap"><s:radio name="dumnPojo.otherQues10" id="otherQues10dumn" list="#{'true':'Yes','false':'NO'}" 
			                   onclick="showYesDeatilDumn(this.value,'otherQues10AddDetaildumn')"></s:radio></td>
		                   </tr>
		                   <tr>
		                   <td width="50%"></td>
		                   <td id="otherQues10AddDetaildumn" style="display: none;" width="50%">
			                        <s:textarea rows="3" cols="10" name="dumnPojo.otherQues10AddDetail"></s:textarea>
			                   </td>
		                   </tr>
		               </table>
		           </td>
		        </tr>
		     </s:if>    
      </tbody>
      <tfoot></tfoot>
    </table>
</div>
<s:if test="#request.readonly=='true'">
	  <script>
		 $j("#dumnReadOnly").find('input, textarea','select').each(function(){
			  $j(this).attr('disabled', 'disabled');
		  });
	  </script>
</s:if>