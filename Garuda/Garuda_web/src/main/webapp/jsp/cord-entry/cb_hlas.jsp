<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:include page="../cb_custom_messages.jsp"></jsp:include>
<jsp:include page="../cb_track_session_logging.jsp" />


<s:if test="#request.source==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE)">
   <script>
   $j(function(){
   jQuery.validator.addMethod("hlaformat", function(value, element) {
		  if(value!="" && value.indexOf(":")==-1){
         return false;
	      }else{
         return true;
		  }		 		 
	}, "Please Enter Value in xx:xx or xx:xxx format");	 
	
	jQuery.validator.addMethod("checkSelectMethodId", function(value, element) {
	
	var pele =$j(element).parent();
	var type1 = $j(pele.siblings()[1]).find('input.hlaValid');
	var type2 = $j(pele.siblings()[2]).find('input.hlaValid');
	
		    if(parseInt(value) != -1){
			
                return true;
				
			}else if(type1.val() === "" && type2.val() ===""){			
			
				return true;	
				
			}else{
			
				return false ;
			}	
			
		}, '<s:text name="garuda.cbu.hlaType.methodName"/>');
		
   }); 
   $j(function(){

	   var validator = $j("#edithlaform").validate({	
		   
		  /* errorPlacement: function(error, element) {
			  debugger;
			  var ele = $j(element).parent().prev().find('select');
			    if(!(ele.val() === -1 ||  ele.val() === "" || ele.val() === null )){
					
					//error.text('<s:text name="garuda.cbu.hlaType.methodName"/>');
					this.messages[element.attr('name')]['validateAntigen'] = '<s:text name="garuda.cbu.hlaType.methodName"/>';
					error.insertAfter(ele);
					
			    }	
				
			    },*/
		rules:{	
				
				"hlas[0].fkHlaMethodId":{checkSelectMethodId : true},
				"hlas[0].hlaType1":{validateAntigen:{				
														param:["0hlaantigen1"], 
														depends: function(element){
														
														var _pele  =  $j(element).parent();
														var _tdEle = $j(_pele.siblings()[1]);
														var ele = _tdEle.find('select');
														
														if(parseInt(ele.val()) === -1)
															return false;
														return true;
														}
													
													}},
				"hlas[0].hlaType2":{validateAntigen:{				
														param:["0hlaantigen2"],
														depends: function(element){
														
														var _pele  =  $j(element).parent();
														var _tdEle = $j(_pele.siblings()[1]);
														var ele = _tdEle.find('select');
														
														if(parseInt(ele.val()) === -1)
															return false;
														return true;
														}			
													}},
				
				"hlas[1].fkHlaMethodId":{checkSelectMethodId : true},
				"hlas[1].hlaType1":{validateAntigen:{
					param:["1hlaantigen1"],
					depends: function(element){
					
					var _pele  =  $j(element).parent();
					var _tdEle = $j(_pele.siblings()[1]);
					var ele = _tdEle.find('select');
					
					if(parseInt(ele.val()) === -1)
						return false;
					return true;
					}
				
				}},

					
				"hlas[1].hlaType2":{validateAntigen:{
					param:["1hlaantigen2"],
					depends: function(element){
						
						var _pele  =  $j(element).parent();
						var _tdEle = $j(_pele.siblings()[1]);
						var ele = _tdEle.find('select');
						
						if(parseInt(ele.val()) === -1)
							return false;
						return true;
						}
					
					}},
				
				"hlas[2].fkHlaMethodId":{checkSelectMethodId : true},
				"hlas[2].hlaType1":{validateAntigen:{param:["2hlaantigen1"],
					depends: function(element){
					
					var _pele  =  $j(element).parent();
					var _tdEle = $j(_pele.siblings()[1]);
					var ele = _tdEle.find('select');
					
					if(parseInt(ele.val()) === -1)
						return false;
					return true;
					}
				
				}},
				"hlas[2].hlaType2":{validateAntigen:{
					param:["2hlaantigen2"],
					depends: function(element){
						
						var _pele  =  $j(element).parent();
						var _tdEle = $j(_pele.siblings()[1]);
						var ele = _tdEle.find('select');
						
						if(parseInt(ele.val()) === -1)
							return false;
						return true;
						}
					
					}},
			
				"hlas[3].fkHlaMethodId":{checkSelectMethodId : true},
				"hlas[3].hlaType1":{validateAntigen:{
					param:["3hlaantigen1"],
					depends: function(element){
					
					var _pele  =  $j(element).parent();
					var _tdEle = $j(_pele.siblings()[1]);
					var ele = _tdEle.find('select');
					
					if(parseInt(ele.val()) === -1)
						return false;
					return true;
					}
				
				}},
				"hlas[3].hlaType2":{validateAntigen:{
					param:["3hlaantigen2"],
					depends: function(element){
					
					var _pele  =  $j(element).parent();
					var _tdEle = $j(_pele.siblings()[1]);
					var ele = _tdEle.find('select');
					
					if(parseInt(ele.val()) === -1)
						return false;
					return true;
					}
				
				}},
			
				"hlas[4].fkHlaMethodId":{checkSelectMethodId : true},
				"hlas[4].hlaType1":{validateAntigen:{
					param:["4hlaantigen1"],
					depends: function(element){
						
						var _pele  =  $j(element).parent();
						var _tdEle = $j(_pele.siblings()[1]);
						var ele = _tdEle.find('select');
						
						if(parseInt(ele.val()) === -1)
							return false;
						return true;
						}
					
					}},
					
				"hlas[4].hlaType2":{validateAntigen:{
					param:["4hlaantigen2"],
					depends: function(element){
						
						var _pele  =  $j(element).parent();
						var _tdEle = $j(_pele.siblings()[1]);
						var ele = _tdEle.find('select');
						
						if(parseInt(ele.val()) === -1)
							return false;
						return true;
						}
					
					}},
				
				"hlas[5].fkHlaMethodId":{checkSelectMethodId : true},
				"hlas[5].hlaType1":{validateAntigen:{
					param:["5hlaantigen1"],
					depends: function(element){
						
						var _pele  =  $j(element).parent();
						var _tdEle = $j(_pele.siblings()[1]);
						var ele = _tdEle.find('select');
						
						if(parseInt(ele.val()) === -1)
							return false;
						return true;
						}
					
					}},
				"hlas[5].hlaType2":{validateAntigen:{
					param:["5hlaantigen2"],
					depends: function(element){
						
						var _pele  =  $j(element).parent();
						var _tdEle = $j(_pele.siblings()[1]);
						var ele = _tdEle.find('select');
						
						if(parseInt(ele.val()) === -1)
							return false;
						return true;
						}
					
					}},
			
				"hlas[6].fkHlaMethodId":{checkSelectMethodId : true},
				"hlas[6].hlaType1":{validateAntigen:{
					param:["6hlaantigen1"],
					depends: function(element){
						
						var _pele  =  $j(element).parent();
						var _tdEle = $j(_pele.siblings()[1]);
						var ele = _tdEle.find('select');
						
						if(parseInt(ele.val()) === -1)
							return false;
						return true;
						}
					
					}},
				"hlas[6].hlaType2":{validateAntigen:{
					param:["6hlaantigen2"],
					depends: function(element){
						
						var _pele  =  $j(element).parent();
						var _tdEle = $j(_pele.siblings()[1]);
						var ele = _tdEle.find('select');
						
						if(parseInt(ele.val()) === -1)
							return false;
						return true;
						}
					
					}},
				
				"hlas[7].fkHlaMethodId":{checkSelectMethodId : true},
				"hlas[7].hlaType1":{validateAntigen:{
					param:["7hlaantigen1"],
					depends: function(element){
						
						var _pele  =  $j(element).parent();
						var _tdEle = $j(_pele.siblings()[1]);
						var ele = _tdEle.find('select');
						
						if(parseInt(ele.val()) === -1)
							return false;
						return true;
						}
					
					}},
				"hlas[7].hlaType2":{validateAntigen:{
					param:["7hlaantigen2"],
					depends: function(element){
						
						var _pele  =  $j(element).parent();
						var _tdEle = $j(_pele.siblings()[1]);
						var ele = _tdEle.find('select');
						
						if(parseInt(ele.val()) === -1)
							return false;
						return true;
						}
					
					}},
				
				"hlas[8].fkHlaMethodId":{checkSelectMethodId : true},
				"hlas[8].hlaType1":{validateAntigen:{
					param:["8hlaantigen1"],
					depends: function(element){
					
					var _pele  =  $j(element).parent();
					var _tdEle = $j(_pele.siblings()[1]);
					var ele = _tdEle.find('select');
					
					if(parseInt(ele.val()) === -1)
						return false;
					return true;
					}
				
				}},
				"hlas[8].hlaType2":{validateAntigen:{
					param:["8hlaantigen2"],
					depends: function(element){
					
					var _pele  =  $j(element).parent();
					var _tdEle = $j(_pele.siblings()[1]);
					var ele = _tdEle.find('select');
					
					if(parseInt(ele.val()) === -1)
						return false;
					return true;
					}
				
				}},
					
	           },				
			messages:{
				"hlas[0].hlaType1":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[0].hlaType2":{required:"<s:text name="garuda.cbu.hlatype.hlatype2"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[1].hlaType1":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[1].hlaType2":{required:"<s:text name="garuda.cbu.hlatype.hlatype2"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[2].hlaType1":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[2].hlaType2":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[3].hlaType1":{required:"<s:text name="garuda.cbu.hlatype.hlatype1"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[3].hlaType2":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[4].hlaType1":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[4].hlaType2":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[5].hlaType1":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[5].hlaType2":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[6].hlaType1":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[6].hlaType2":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[7].hlaType1":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[7].hlaType2":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[8].hlaType1":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[8].hlaType2":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[9].hlaType1":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[9].hlaType2":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[10].hlaType1":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[10].hlaType2":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[11].hlaType1":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[11].hlaType2":{validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
				"hlas[0].hlaTypingDate":{required:"<s:text name="garuda.cbu.hlatype.receiveddate"/>"},
				"hlas[1].hlaTypingDate":{required:"<s:text name="garuda.cbu.hlatype.receiveddate"/>"},
				"hlas[3].hlaTypingDate":{required:"<s:text name="garuda.cbu.hlatype.receiveddate"/>"}						
			}
		});

	/*$j(".hlaValid").keypress(function(e) {
		  var val = $j(this).val();
		  if(e.keyCode!=8 && e.keyCode!=46 )
		  {
			  if(val.length==2){						  			  
				  if(e.which!=58){
					   $j(this).val(val+":");
				  }
			  }
		  }
	});*/

	/*var today = new Date();
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();

	var h=today.getHours();
	var mn=today.getMinutes()+1;

	 $j( "#datepicker41" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
		changeYear: true });
	
	 $j( "#datepicker43" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
			changeYear: true });*/
	getDatePic(); 
	
	$j(".hlaValid").click(function() {	
	
	var pele  =  $j(this).parent();
	var tdEle = $j(pele.siblings()[1]);
	var ele = tdEle.find('select');
	
	
	      $j("#edithlaform").validate().element(ele);
		  
         });
		 
	});
	
	</script>
</s:if>
<s:if test="#request.source==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_CODESUBTYPE)">
   <script>
   $j(function(){
	   jQuery.validator.addMethod("hlaformat", function(value, element) {
			  if(value!="" && value.indexOf(":")==-1){
	         return false;
		      }else{
	         return true;
			  }		 		 
		}, "Please Enter Value in xx:xx or xx:xxx format");	 

		//=======================================================
	   jQuery.validator.addMethod("checkSelectMethodId", function(value, element) {
			
			var pele =$j(element).parent();
			var type1 = $j(pele.siblings()[1]).find('input.hlaValid');
			var type2 = $j(pele.siblings()[2]).find('input.hlaValid');
			
				    if(parseInt(value) != -1){
					
		                return true;
						
					}else if(type1.val() === "" && type2.val() ===""){			
					
						return true;	
						
					}else{
					
						return false ;
					}	
					
				}, '<s:text name="garuda.cbu.hlaType.methodName"/>');
		//=============================================================
	   });
     $j(function(){

    	 var validator = $j("#edithlaform").validate({    			
    			rules:{	

    				    "maternalHlas[0].fkHlaMethodId":{checkSelectMethodId : true},
    					"maternalHlas[0].hlaType1":{validateAntigen:{				
							param:["0hlamaternalantigen1"], 
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}
						
						}},
    					"maternalHlas[0].hlaType2":{validateAntigen:{				
							param:["0hlamaternalantigen2"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					//"maternalHlas[0].hlaTypingDate":{validateRequiredByName:["0maternaltype1"]},
    					//"maternalHlas[0].hlaRecievedDate":{validateRequiredByName:["0maternaltype1"]},
    					//"maternalHlas[0].fkSpecType":{checkselectbyname:["0maternaltype1"]},
    					"maternalHlas[1].fkHlaMethodId":{checkSelectMethodId : true},
    					"maternalHlas[1].hlaType1":{validateAntigen:{				
							param:["1hlamaternalantigen1"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					"maternalHlas[1].hlaType2":{validateAntigen:{				
							param:["1hlamaternalantigen2"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					//"maternalHlas[1].hlaTypingDate":{validateRequiredByName:["1maternaltype1"]},
    					//"maternalHlas[1].hlaRecievedDate":{validateRequiredByName:["1maternaltype1"]},
    					//"maternalHlas[1].fkSpecType":{checkselectbyname:["1maternaltype1"]},
    					"maternalHlas[2].fkHlaMethodId":{checkSelectMethodId : true},
    					"maternalHlas[2].hlaType1":{validateAntigen:{				
							param:["2hlamaternalantigen1"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					"maternalHlas[2].hlaType2":{validateAntigen:{				
							param:["2hlamaternalantigen2"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					//"maternalHlas[2].hlaTypingDate":{validateRequiredByName:["2maternaltype1"]},
    					//"maternalHlas[2].hlaRecievedDate":{validateRequiredByName:["2maternaltype1"]},
    					//"maternalHlas[2].fkSpecType":{checkselectbyname:["2maternaltype1"]},
    					"maternalHlas[3].fkHlaMethodId":{checkSelectMethodId : true},
    					"maternalHlas[3].hlaType1":{validateAntigen:{				
							param:["3hlamaternalantigen1"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					"maternalHlas[3].hlaType2":{validateAntigen:{				
							param:["3hlamaternalantigen2"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					//"maternalHlas[3].hlaTypingDate":{validateRequiredByName:["3maternaltype1"]},
    					//"maternalHlas[3].hlaRecievedDate":{validateRequiredByName:["3maternaltype1"]},
    					//"maternalHlas[3].fkSpecType":{checkselectbyname:["3maternaltype1"]},
    					"maternalHlas[4].fkHlaMethodId":{checkSelectMethodId : true},
    					"maternalHlas[4].hlaType1":{validateAntigen:{				
							param:["4hlamaternalantigen1"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					"maternalHlas[4].hlaType2":{validateAntigen:{				
							param:["4hlamaternalantigen2"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					//"maternalHlas[4].hlaTypingDate":{validateRequiredByName:["4maternaltype1"]},
    					//"maternalHlas[4].hlaRecievedDate":{validateRequiredByName:["4maternaltype1"]},
    					//"maternalHlas[4].fkSpecType":{checkselectbyname:["4maternaltype1"]},
    					"maternalHlas[5].fkHlaMethodId":{checkSelectMethodId : true},
    					"maternalHlas[5].hlaType1":{validateAntigen:{				
							param:["5hlamaternalantigen1"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					"maternalHlas[5].hlaType2":{validateAntigen:{				
							param:["5hlamaternalantigen2"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					//"maternalHlas[5].hlaTypingDate":{validateRequiredByName:["5maternaltype1"]},
    					//"maternalHlas[5].hlaRecievedDate":{validateRequiredByName:["5maternaltype1"]},
    					//"maternalHlas[5].fkSpecType":{checkselectbyname:["5maternaltype1"]},
    					"maternalHlas[6].fkHlaMethodId":{checkSelectMethodId : true},
    					"maternalHlas[6].hlaType1":{validateAntigen:{				
							param:["6hlamaternalantigen1"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					"maternalHlas[6].hlaType2":{validateAntigen:{				
							param:["6hlamaternalantigen2"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					//"maternalHlas[6].hlaTypingDate":{validateRequiredByName:["6maternaltype1"]},
    					//"maternalHlas[6].hlaRecievedDate":{validateRequiredByName:["6maternaltype1"]},
    					//"maternalHlas[6].fkSpecType":{checkselectbyname:["6maternaltype1"]},
    					"maternalHlas[7].fkHlaMethodId":{checkSelectMethodId : true},
    					"maternalHlas[7].hlaType1":{validateAntigen:{				
							param:["7hlamaternalantigen1"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					"maternalHlas[7].hlaType2":{validateAntigen:{				
							param:["7hlamaternalantigen2"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					//"maternalHlas[7].hlaTypingDate":{validateRequiredByName:["7maternaltype1"]},
    					//"maternalHlas[7].hlaRecievedDate":{validateRequiredByName:["7maternaltype1"]},
    					//"maternalHlas[7].fkSpecType":{checkselectbyname:["7maternaltype1"]},
    					"maternalHlas[8].fkHlaMethodId":{checkSelectMethodId : true},
    					"maternalHlas[8].hlaType1":{validateAntigen:{				
							param:["8hlamaternalantigen1"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}},
    					"maternalHlas[8].hlaType2":{validateAntigen:{				
							param:["8hlamaternalantigen2"],
							depends: function(element){
							
							var _pele  =  $j(element).parent();
							var _tdEle = $j(_pele.siblings()[1]);
							var ele = _tdEle.find('select');
							
							if(parseInt(ele.val()) === -1)
								return false;
							return true;
							}			
						}}
    					//"maternalHlas[8].hlaTypingDate":{validateRequiredByName:["8maternaltype1"]},
    					//"maternalHlas[8].hlaRecievedDate":{validateRequiredByName:["8maternaltype1"]},
    					//"maternalHlas[8].fkSpecType":{checkselectbyname:["8maternaltype1"]}
    					//"maternalHlas[9].hlaType1":{validateAntigen:["9hlamaternalantigen1"]},
    					//"maternalHlas[9].hlaType2":{validateAntigen:["9hlamaternalantigen2"]},
    					//"maternalHlas[10].hlaType1":{validateAntigen:["10hlamaternalantigen1"]},
    					//"maternalHlas[10].hlaType2":{validateAntigen:["10hlamaternalantigen2"]},
    					//"maternalHlas[11].hlaType1":{validateAntigen:["11hlamaternalantigen1"]},
    					//"maternalHlas[11].hlaType2":{validateAntigen:["11hlamaternalantigen2"]}    					
        				},
    				messages:{
    					"maternalHlas[0].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[0].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[1].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[1].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[2].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[2].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[3].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[3].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[4].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[4].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[5].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[5].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[6].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[6].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[7].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[7].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[8].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[8].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[9].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[9].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[10].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[10].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[11].hlaType1":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"},
    					"maternalHlas[11].hlaType2":{hlaformat:"<s:text name="garuda.cbu.cordentry.hlatype"/>",validateAntigen:"<s:text name="garuda.cbu.hlaType.validateAntigen"/>"}    										
    				}
    			});

    	/* $j(".hlaValid").keypress(function(e) {
   		  var val = $j(this).val();
	   		  if(e.keyCode!=8 && e.keyCode!=46 )
	   		  {
	   			  if(val.length==2){						  			  
	   				  if(e.which!=58){
	   					   $j(this).val(val+":");
	   				  }
	   			  }
	   		  }
   	    });*/

  /*  	var today = new Date();
 		var d = today.getDate();
 		var m = today.getMonth();
 		var y = today.getFullYear();

 		var h=today.getHours();
 		var mn=today.getMinutes()+1;

    	$j( "#datepicker41" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
    			changeYear: true });
    		
    	$j( "#datepicker43" ).datepicker({dateFormat: 'M dd, yy',maxDate: new Date(y, m, d),changeMonth: true,
    				changeYear: true });*/
    	 getDatePic();
    	 $j(".hlaValid").click(function() {	
    			
    			var pele  =  $j(this).parent();
    			var tdEle = $j(pele.siblings()[1]);
    			var ele = tdEle.find('select');
    			
    			
    			      $j("#edithlaform").validate().element(ele);
    				  
    		         });
      });
      
   </script>
</s:if>
<script>


function validateHlaFormat(id,val){
	  if(val.length==2){
		  $j("#"+id).val(val+":");
	  }else{
		  $j("#"+id).val(val);
	  }
}

function getAntigen(locus,method,genomicformat,divId,ext){
	  var meth = $j("#"+method).val();
	  if(parseInt(meth)!=-1){
	    var url = 'getAntigenIdType1?hlaLocus='+locus+'&genomicFormat='+genomicformat+'&typingMethod='+meth+'&ext='+ext;
	    loadPageByGetRequset(url,divId);  
	  }
	  $j("#edithlaform").valid();
	}

function getAntigen2(locus,method,genomicformat,divId,ext){
	  var meth = $j("#"+method).val();
	  if(parseInt(meth)!=-1){
	  loadPageByGetRequset('getAntigenIdType2?hlaLocus='+locus+'&genomicFormat='+genomicformat+'&typingMethod='+meth+'&ext='+ext,divId);  
   }
   $j("#edithlaform").valid();
 }  

function getMaternalAntigen(locus,method,genomicformat,divId,ext){
	  var meth = $j("#"+method).val();
	  if(parseInt(meth)!=-1){
	     var url = 'getMaternalAntigenIdType1?hlaLocus='+locus+'&genomicFormat='+genomicformat+'&typingMethod='+meth+'&ext='+ext;
	     loadPageByGetRequset(url,divId);
	  }
	  $j("#edithlaform").valid();
    }



function getMaternalAntigen2(locus,method,genomicformat,divId,ext){
	  var meth = $j("#"+method).val();
	  if(parseInt(meth)!=-1){
	  loadPageByGetRequset('getMaternalAntigenIdType2?hlaLocus='+locus+'&genomicFormat='+genomicformat+'&typingMethod='+meth+'&ext='+ext,divId);  
  }
  $j("#edithlaform").valid();
}
function refreshType1AndType2(method,rowindex,locus){
     if(parseInt(method)!=-1){  
   var val1 = $j("#"+rowindex+"type1").val();
   var divId = rowindex+"type1div";
   var divId2 = rowindex+"type2div";
   var val2 = $j("#"+rowindex+"type2").val();
   if(val1!=""){
  	 var url = 'getAntigenIdType1?hlaLocus='+locus+'&genomicFormat='+val1+'&typingMethod='+method+'&ext='+rowindex;
       loadPageByGetRequset(url,divId);
   }
   if(val2!=""){
  	 var url1 = 'getAntigenIdType2?hlaLocus='+locus+'&genomicFormat='+val2+'&typingMethod='+method+'&ext='+rowindex;
       loadPageByGetRequset(url1,divId2);
   }
 }
 $j("#edithlaform").valid();
}
function refreshMaternalType1AndType2(method,rowindex,locus){ 
          if(parseInt(method)!=-1){   
	     var val1 = $j("#"+rowindex+"maternaltype1").val();
	     var divId = rowindex+"maternaltype1div";
	     var divId2 = rowindex+"maternaltype2div";
	     var val2 = $j("#"+rowindex+"maternaltype2").val();
	     if(val1!=""){
	    	 var url = 'getMaternalAntigenIdType1?hlaLocus='+locus+'&genomicFormat='+val1+'&typingMethod='+method+'&ext='+rowindex;
	         loadPageByGetRequset(url,divId);
	     }
	     if(val2!=""){
	    	 var url1 = 'getMaternalAntigenIdType2?hlaLocus='+locus+'&genomicFormat='+val2+'&typingMethod='+method+'&ext='+rowindex;
	         loadPageByGetRequset(url1,divId2);
	     }
	  }
      $j("#edithlaform").valid();
}
function populateHlaTypingDate(id,val){
	  var today = new Date();
	  var d = today.getDate();
	  var m = today.getMonth();
	  var y = today.getFullYear();
	  var h=today.getHours();
	  var mn=today.getMinutes()+1;
	  var date = $j.datepicker.formatDate('M dd, yy', new Date(y, m, d));
	  $j("#"+id).val(date);
}


function submitHlaDetails(){
	if($j("#orderId").val()!=null && $j("#orderId").val()!=''){
		if($j("#edithlaform").valid()){
		loadMoredivs('submitHlasFromPF?pkcordId=<s:property value="cdrCbuPojo.cordID"/>','currentRequestProgressMainDiv,additityingandtestcontentDiv,taskCompleteFlagsDiv','edithlaform','update','status');
		currenthlatbl_onload();
		hlatblOther_onload();
		setpfprogressbar();
		getprogressbarcolor();
		historyTblOnLoad();
		}
	}else{
		modalFormSubmitRefreshDiv('edithlaform','submitHlas','');
		refreshMultipleDivsOnViewClinical('64','<s:property value="cdrCbuPojo.cordID"/>');
		}
}
$j(function(){
	   $j('#hlainfo').find(".portlet-header .ui-icon").bind("click",function() {
		   if($j(this).is(".ui-icon-minusthick") || $j(this).is(".ui-icon-plusthick"))
			{
				$j(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
				
				$j(this).parents(".portlet:first").find(".portlet-content").toggle();
				
				/*var divid = $j(this).parent().parent().attr("id");
				if(divid !="undefined" || divid !=null)
				divHeight=document.getElementById(divid).style.height;
				if(divHeight=='100%')
				{
					divHeight = 'auto';
					}	
			    $j(this).parents(".portlet:first").css('height',divHeight);*/
				
			}
		});	
});

function proxyHLASubmition(){
	var isChange = divChanges('IDsChanges',$j('#status').html());
	
	if(isChange){
	//	alert('inside');
		modalFormSubmitRefreshDiv('edithlaform','saveHlaAsyncData','hlaAsyncData');
	}else{
		showSuccessdiv("update","status");
	}
	 addTotalCount();
	
}
</script>
<s:form id="edithlaform">
<s:hidden name="entityId"></s:hidden>
<s:hidden name="entityType"></s:hidden>
<s:hidden name="update"></s:hidden>
<s:hidden name="orderId"></s:hidden>
<s:hidden name="orderType"></s:hidden>
<s:hidden name="HlaTypingAvail"></s:hidden>
<s:hidden name="sampleAtLab"></s:hidden>
<s:hidden name="cdrCbuPojo.cordID" ></s:hidden>
<div id="update" style="display: none;" >
		  <jsp:include page="../cb_update.jsp">
		    <jsp:param value="garuda.message.modal.edithla" name="message"/>
		  </jsp:include>	   
</div>
<div class="column" id="status">
<s:if test="#request.source==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE)">
<div id="cbubesthlainfoeditcontent" onclick="toggleDiv('cbubesthlainfoedit')"
					class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
					style="text-align: center;"><span
					class="ui-icon ui-icon-triangle-1-s"></span> <s:text
					name="garuda.cbuentry.label.CBU"></s:text></div>
 <s:if test="%{#request.updateFrom==null || #request.updateFrom!='cordEntry'}">
		<div id="cbubesthlainfoedit">
		   <table align="left" cellpadding="0" cellspacing="0" border="0" class="displaycdr datatablewithoutsearch" id="searchResults11">
				 <thead>
				   <tr>
				      <th></th>			   			   
					   <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
					       <th>
					          <s:property value="description.replace('HLA-','')"/>
					       </th>				     
					   </s:iterator>
				   </tr>
				 </thead>			 
				 <tbody>
				    <tr>
					    <td>
							 <s:text name="garuda.cbu.label.besthla"/>								            
						</td>								    
						  <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
					            <s:iterator value="#request.bestHlas" var="bhla">
							             <s:if test="pkCodeId==fkHlaCodeId">
							                 <s:set name="bhlaCheck" value="%{'true'}" />
								             <td>
									            <table>
								                    <tr>
								                        <td>
								                            <s:property value="hlaType1" />
								                        </td>
								                    </tr>
								                     <tr>
								                        <td>
								                            <s:property value="hlaType2" />
								                        </td>
								                    </tr>
								                </table>	  
										     </td>	
									      </s:if>				      										      	        	
							        </s:iterator> 
							        <s:if test="%{#bhlaCheck!='true'}">
									         <td></td>
									</s:if>	
									<s:set name="bhlaCheck" value="%{'false'}" />
						  </s:iterator>
					</tr>
				   </tbody>
				   <tfoot>
					    <tr>
							<td colspan="13"></td>
						</tr>
					</tfoot>
				</table>
		</div>
</s:if>
</s:if>
	      <div class="portlet" id="hlainfoparent" >
					<div id="hlainfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
					<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">					
					<!--<span class="ui-icon ui-icon-close" ></span>-->
					<s:if test="#request.source==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE)">
					  <s:text name="garuda.advancelookup.label.cbu" />
					</s:if>
					<s:if test="#request.source==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_CODESUBTYPE)">
					  <s:text name="garuda.advancelookup.label.maternal" />
					</s:if>
					<span class="ui-icon ui-icon-minusthick"></span></div>
					<div class="portlet-content">
					    <s:if test="#request.source==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE)">
					        <!--<table style="border: 1px;" align="center" cellpadding="0"	cellspacing="0">
									    <tr>
									      <td><s:text name="garuda.unitreport.label.testdate" />:<span style="color: red;">*</span></td>
									      <td>
									           <s:date name="hlaPojo.hlaTypingDate" id="datepicker40" format="MMM dd, yyyy" />									      
											   <s:textfield readonly="true" onkeydown="cancelBack();"  name="hlaTestDate" id="datepicker41" value="%{datepicker40}" cssClass="datePicWMaxDate"></s:textfield>
										 </td>
									      <td><s:text name="garuda.cordentry.label.sampletype" /></td>
									      <td>
									          <s:select name="hlaPojo.fkSpecType" listKey="pkCodeId" listValue="description" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SPEC_TYPE]" headerKey="-1" headerValue="Select" />
									      </td>
									    </tr>
							</table>
					    --></s:if>
					    <s:if test="#request.source==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_CODESUBTYPE)">
					        <!--<table style="border: 1px;" align="center" cellpadding="0"	cellspacing="0">
									    <tr>
									      <td><s:text name="garuda.unitreport.label.testdate" />:<span style="color: red;">*</span></td>
									      <td>
									           <s:date name="maternalHlaPojo.hlaTypingDate" id="datepicker42" format="MMM dd, yyyy" />									      
											   <s:textfield readonly="true" onkeydown="cancelBack();"  name="maternalHlaTestDate" id="datepicker43" value="%{datepicker41}" cssClass="datePicWMaxDate"></s:textfield>
										 </td>
									      <td><s:text name="garuda.cordentry.label.sampletype" /></td>
									      <td>
									          <s:select name="maternalHlaPojo.fkSpecType" listKey="pkCodeId" listValue="description" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SPEC_TYPE]" headerKey="-1" headerValue="Select" />
									      </td>
									    </tr>
							</table>
					    --></s:if>
						<table style="border: 1px;width: 100%" align="center" cellpadding="0"	cellspacing="0" id="searchResults">
							<thead>
								<tr>
									<th><s:text name="garuda.cbuentry.label.hlaLocus" /></th>
									<th><s:text name="garuda.cbuentry.label.method" /></th>
									<th><s:text name="garuda.cbuentry.label.type1" /></th>
									<th><s:text name="garuda.cbuentry.label.type2" /></th>
									<!--<th><s:text name="garuda.unitreport.label.testdate" /></th>
									<th><s:text name="garuda.cbuentry.label.recieveddate" /></th>
									<th><s:text name="garuda.cordentry.label.sampletype" /></th>
								--></tr>
							</thead>	
							<tbody>
							  <s:if test="#request.source==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE)">
                                    <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
											        <s:set name="check" value="%{'notcheck'}" />
											          <s:iterator value="#request.editHlas">
											          <s:if test="pkCodeId==fkHlaCodeId">
											              <s:set name="check" value="%{'check'}" />
											                <tr style="vertical-align:top;">
													           <td width="10%" style="padding-left: 5px;">
													           <s:hidden name="hlas[%{#row.index}].fkSource" value="%{getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE)}" />
													           <s:hidden name="hlas[%{#row.index}].pkHla" value="%{pkHla}" />
													           <s:hidden name="hlas[%{#row.index}].createdBy" value="%{createdBy}" />
													           <s:hidden name="hlas[%{#row.index}].hlaOrderSeq" value="%{#request.cbuHlaOrderSeq}" />
													           <s:property value="description.replace('HLA-','')"/>:<s:hidden name="hlas[%{#row.index}].fkHlaCodeId" value="%{pkCodeId}"></s:hidden>
													           
													            </td>									           
													           <td width="10%" style="padding-left: 5px;">
													              <s:select id="%{#row.index}method" name="hlas[%{#row.index}].fkHlaMethodId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_METHOD]" listKey="pkCodeId" listValue="description" value="%{fkHlaMethodId}" onchange="refreshType1AndType2(this.value,'%{#row.index}','%{description}');isChangeDone(this,'%{fkHlaMethodId}','IDsChanges');" headerKey="-1" headerValue="Select"></s:select>
													          </td>
													           <td width="10%" style="padding-left: 5px;"><input type="text"  class="hlaValid" id="<s:property value="%{#row.index}" />type1" value="<s:property value="hlaType1"/>" name="hlas[<s:property value="%{#row.index}" />].hlaType1" 
													           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_A">onchange="getAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');"</s:if>
													           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_B">onchange="getAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');" </s:if>
													           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_DRB1">onchange="getAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');" </s:if>
													           <s:else>onchange="getAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');"</s:else>></input>
													               <div id="<s:property value="%{#row.index}" />type1div">
													                   <s:set name="rowindex" value="%{#row.index}" scope="request"/>		
													                   <jsp:include page="cb_refresh-antigen1.jsp">
													                      <jsp:param value="<%=request.getAttribute(\"rowindex\")%>" name="hlaext"/>
													                   </jsp:include>
													               </div>									               
													           </td>
													           <td width="10%" style="padding-left: 5px;"><input type="text"  class="hlaValid" id="<s:property value="%{#row.index}" />type2" value="<s:property value="hlaType2"/>" name="hlas[<s:property value="%{#row.index}" />].hlaType2" 
													           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_A">onchange="getAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');" </s:if>
													           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_B">onchange="getAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');" </s:if>
													           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_DRB1">onchange="getAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');" </s:if>
													           <s:else>onchange="getAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');"</s:else>
													           ></input>
													               <div id="<s:property value="%{#row.index}" />type2div">
													                   <s:set name="rowindex" value="%{#row.index}" scope="request"/>		
													                   <jsp:include page="cb_refresh-antigen2.jsp">
													                      <jsp:param value="<%=request.getAttribute(\"rowindex\")%>" name="hlaext"/>
													                   </jsp:include>
													               </div>									              
													           </td>
													           <!--<td width="10%" style="padding-left: 5px;">
													                <s:date name="hlaTypingDate" id="hladatepic%{#row.index}" format="MMM dd, yyyy" var="hlavar"/>		
													                <input type="text" onchange="populateHlaTypingDate('hlasRecivedDatePic<s:property value="%{#row.index}"/>',this.value)" name="hlas[<s:property value="%{#row.index}"/>].hlaTypingDate" value="<s:property value="%{#request.hlavar}"/>" class="datePicWMaxDate hlaclass"
													                <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_A">class="datePicWMaxDate hlaclass"</s:if>
													                <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_B">class="datePicWMaxDate hlaclass"</s:if>
													                <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_DRB1">class="datePicWMaxDate hlaclass"</s:if>
													                <s:else>class="datePicWMaxDate"</s:else>
													                readonly="readonly" />
													           </td>
													           <td width="10%" style="padding-left: 5px;">
													                <s:date name="hlaRecievedDate" id="hlarecdatepic%{#row.index}" format="MMM dd, yyyy" var="hlarec"/>		
													                <input type="text" id="hlasRecivedDatePic<s:property value="%{#row.index}"/>" name="hlas[<s:property value="%{#row.index}"/>].hlaRecievedDate" value="<s:property value="%{#request.hlarec}"/>" readonly="readonly" />
													           </td>
													           <td width="40%" style="padding-left: 5px;">
													               <s:select name="hlas[%{#row.index}].fkSpecType" value="fkSpecType" listKey="pkCodeId" listValue="description" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_SPEC_TYPE]" headerKey="-1" headerValue="Select" ></s:select>
													           </td>
													        --></tr>
											            </s:if>  
											        </s:iterator>
											        <s:if test="%{#check!='check'}">
											            <tr style="vertical-align:top;">
															           <td width="10%" style="padding-left: 5px;"><s:property value="description.replace('HLA-','')"/>:
																           <s:hidden name="hlas[%{#row.index}].fkHlaCodeId" value="%{pkCodeId}"></s:hidden>
																           <s:hidden name="hlas[%{#row.index}].createdBy" value="%{createdBy}" />
																           <s:hidden name="hlas[%{#row.index}].fkSource" value="%{getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE)}" />
															               <s:hidden name="hlas[%{#row.index}].hlaOrderSeq" value="%{#request.cbuHlaOrderSeq}" />
															           </td>
															           <td width="10%" style="padding-left: 5px;">
															              <s:select id="%{#row.index}method" name="hlas[%{#row.index}].fkHlaMethodId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_METHOD]" listKey="pkCodeId" listValue="description" onchange="refreshType1AndType2(this.value,'%{#row.index}','%{description}');isChangeDone(this,'-1','IDsChanges');" headerKey="-1" headerValue="Select" ></s:select></td>
															           <td width="10%" style="padding-left: 5px;"><input type="text"  class="hlaValid" id="<s:property value="%{#row.index}" />type1" name="hlas[<s:property value="%{#row.index}" />].hlaType1" 
															           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_A">onchange="getAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');" </s:if>
															           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_B">onchange="getAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');" </s:if>
															           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_DRB1">onchange="getAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');" </s:if>
															           <s:else>onchange="getAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');"</s:else>
															           ></input>
															               <div id="<s:property value="%{#row.index}" />type1div">
															                   <s:set name="rowindex" value="%{#row.index}" scope="request"/>		
															                   <jsp:include page="cb_refresh-antigen1.jsp">
															                      <jsp:param value="<%=request.getAttribute(\"rowindex\")%>" name="hlaext"/>
															                   </jsp:include>
													                       </div>
															           </td>
															           <td width="10%" style="padding-left: 5px;"><input type="text"  class="hlaValid" id="<s:property value="%{#row.index}" />type2" name="hlas[<s:property value="%{#row.index}" />].hlaType2" 
															           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_A">onchange="getAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');" </s:if>
															           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_B">onchange="getAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');" </s:if>
															           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_DRB1">onchange="getAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');" </s:if>
															           <s:else>onchange="getAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />method',this.value,'<s:property value="%{#row.index}" />type2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');"</s:else>
															           ></input>
															               <div id="<s:property value="%{#row.index}" />type2div">
													                           <s:set name="rowindex" value="%{#row.index}" scope="request"/>		
													                           <jsp:include page="cb_refresh-antigen2.jsp">
															                      <jsp:param value="<%=request.getAttribute(\"rowindex\")%>" name="hlaext"/>
															                   </jsp:include>
													                       </div>
															           </td>
															           <!--<td width="10%" style="padding-left: 5px;">
															                <input type="text" onchange="populateHlaTypingDate('hlasRecivedDatePic<s:property value="%{#row.index}"/>',this.value)" name="hlas[<s:property value="%{#row.index}"/>].hlaTypingDate" 
															                <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_A">class="datePicWMaxDate hlaclass"</s:if>
															                <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_B">class="datePicWMaxDate hlaclass"</s:if>
															                <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_DRB1">class="datePicWMaxDate hlaclass"</s:if>
															                <s:else>class="datePicWMaxDate"</s:else>
															                readonly="readonly" />
															          </td> 
															          <td width="10%" style="padding-left: 5px;">
															                <input type="text" id="hlasRecivedDatePic<s:property value="%{#row.index}"/>" name="hlas[<s:property value="%{#row.index}"/>].hlaRecievedDate" readonly="readonly" />
															          </td>
															          <td width="40%" style="padding-left: 5px;">
															               <s:select name="hlas[%{#row.index}].fkSpecType" listKey="pkCodeId" listValue="description" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_SPEC_TYPE]" headerKey="-1" headerValue="Select" ></s:select>
															          </td>
													   --></tr>										   								    
											        </s:if>		
											        <s:set name="check" value="%{'notcheck'}" />									    							        
											    </s:iterator>
							  </s:if>
							  <s:if test="#request.source==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_CODESUBTYPE)">
							        <s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_LOCUS]" status="row">
											        <s:set name="check1" value="%{'notcheck'}" />
											        <s:iterator value="#request.editMaternalHlas">
											           <s:if test="pkCodeId==fkHlaCodeId">
											             <s:set name="check1" value="%{'check'}" />
											                <tr style="vertical-align:top;">
													           <td width="10%" style="padding-left: 5px;">
													           <s:hidden name="maternalHlas[%{#row.index}].fkSource" value="%{getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_CODESUBTYPE)}" />
													           <s:hidden name="maternalHlas[%{#row.index}].pkHla" value="%{pkHla}" />
													           <s:hidden name="maternalHlas[%{#row.index}].hlaOrderSeq" value="%{#request.matHlaOrderSeq}" />
													           <s:property value="description.replace('HLA-','')"/>:<s:hidden name="maternalHlas[%{#row.index}].fkHlaCodeId" value="%{pkCodeId}"></s:hidden>
													           
													            </td>									           
													           <td width="10%" style="padding-left: 5px;">
													              <s:select id="%{#row.index}maternalmethod" name="maternalHlas[%{#row.index}].fkHlaMethodId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_METHOD]" listKey="pkCodeId" listValue="description" value="%{fkHlaMethodId}" onchange="refreshMaternalType1AndType2(this.value,'%{#row.index}','%{description}');isChangeDone(this,'%{fkHlaMethodId}','IDsChanges');" headerKey="-1" headerValue="Select" ></s:select></td>
													           <td width="10%" style="padding-left: 5px;"><input type="text"  class="hlaValid" id="<s:property value="%{#row.index}" />maternaltype1" value="<s:property value="hlaType1"/>" name="maternalHlas[<s:property value="%{#row.index}" />].hlaType1" 
													           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_A">onchange="getMaternalAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');"</s:if>
													           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_B">onchange="getMaternalAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');" </s:if>
													           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_DRB1">onchange="getMaternalAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');" </s:if>
													           <s:else>onchange="getMaternalAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');"</s:else>></input>
													               <div id="<s:property value="%{#row.index}" />maternaltype1div">
													                   <s:set name="rowindex" value="%{#row.index}" scope="request"/>		
													                   <jsp:include page="cb_refresh-maternal-antigen1.jsp">
													                      <jsp:param value="<%=request.getAttribute(\"rowindex\")%>" name="hlaext"/>
													                   </jsp:include>
													               </div>									               
													           </td>
													           <td width="10%" style="padding-left: 5px;"><input type="text"  class="hlaValid" id="<s:property value="%{#row.index}" />maternaltype2" value="<s:property value="hlaType2"/>" name="maternalHlas[<s:property value="%{#row.index}" />].hlaType2" 
													           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_A">onchange="getMaternalAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');" </s:if>
													           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_B">onchange="getMaternalAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');" </s:if>
													           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_DRB1">onchange="getMaternalAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');" </s:if>
													           <s:else>onchange="getMaternalAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');"</s:else>
													           ></input>
													               <div id="<s:property value="%{#row.index}" />maternaltype2div">
													                   <s:set name="rowindex" value="%{#row.index}" scope="request"/>		
													                   <jsp:include page="cb_refresh-maternal-antigen2.jsp">
													                      <jsp:param value="<%=request.getAttribute(\"rowindex\")%>" name="hlaext"/>
													                   </jsp:include>
													               </div>									              
													           </td>
													           <!--<td width="10%" style="padding-left: 5px;">
													                <s:date name="hlaTypingDate" id="hladatepic%{#row.index}" format="MMM dd, yyyy" var="mhlavar"/>		
													                <input type="text" onchange="populateHlaTypingDate('mhlasRecivedDatePic<s:property value="%{#row.index}"/>',this.value)" name="maternalHlas[<s:property value="%{#row.index}"/>].hlaTypingDate" value="<s:property value="%{#request.mhlavar}"/>" class="datePicWMaxDate" readonly="readonly"/>
													           </td>
													           <td width="10%" style="padding-left: 5px;">
													                <s:date name="hlaRecievedDate" id="hlarecdatepic%{#row.index}" format="MMM dd, yyyy" var="mhlarec"/>		
													                <input type="text" id="mhlasRecivedDatePic<s:property value="%{#row.index}"/>" name="maternalHlas[<s:property value="%{#row.index}"/>].hlaRecievedDate" value="<s:property value="%{#request.mhlarec}"/>" readonly="readonly" />
													           </td>
													           <td width="40%" style="padding-left: 5px;">
													              <s:select name="maternalHlas[%{#row.index}].fkSpecType" value="fkSpecType" listKey="pkCodeId" listValue="description" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MAT_SPEC_TYPE]" headerKey="-1" headerValue="Select" />
													           </td>
													        --></tr>
											            </s:if>  
											        </s:iterator>
											        <s:if test="%{#check1!='check'}">
											            <tr style="vertical-align:top;">
															           <td width="10%" style="padding-left: 5px;"><s:property value="description.replace('HLA-','')"/>:
																           <s:hidden name="maternalHlas[%{#row.index}].fkHlaCodeId" value="%{pkCodeId}"></s:hidden>
																           <s:hidden name="maternalHlas[%{#row.index}].fkSource" value="%{getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES, @com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_CODESUBTYPE)}" />
															               <s:hidden name="maternalHlas[%{#row.index}].hlaOrderSeq" value="%{#request.matHlaOrderSeq}" />
															           </td>
															           <td width="10%" style="padding-left: 5px;">
															              <s:select id="%{#row.index}maternalmethod" name="maternalHlas[%{#row.index}].fkHlaMethodId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_METHOD]" listKey="pkCodeId" listValue="description" onchange="refreshMaternalType1AndType2(this.value,'%{#row.index}','%{description}');isChangeDone(this,'%{fkHlaMethodId}','IDsChanges');" headerKey="-1" headerValue="Select"></s:select>
																	  </td>
															           <td width="10%" style="padding-left: 5px;"><input type="text"  class="hlaValid" id="<s:property value="%{#row.index}" />maternaltype1" name="maternalHlas[<s:property value="%{#row.index}" />].hlaType1" 
															           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_A">onchange="getMaternalAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');" </s:if>
															           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_B">onchange="getMaternalAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');" </s:if>
															           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_DRB1">onchange="getMaternalAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');" </s:if>
															           <s:else>onchange="getMaternalAntigen('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype1div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType1"/>','IDsChanges');"</s:else>
															           ></input>
															               <div id="<s:property value="%{#row.index}" />maternaltype1div">
															                   <s:set name="rowindex" value="%{#row.index}" scope="request"/>		
															                   <jsp:include page="cb_refresh-maternal-antigen1.jsp">
															                      <jsp:param value="<%=request.getAttribute(\"rowindex\")%>" name="hlaext"/>
															                   </jsp:include>
													                       </div>
															           </td>
															           <td width="10%" style="padding-left: 5px;"><input type="text"  class="hlaValid" id="<s:property value="%{#row.index}" />maternaltype2" name="maternalHlas[<s:property value="%{#row.index}" />].hlaType2" 
															           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_A">onchange="getMaternalAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');" </s:if>
															           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_B">onchange="getMaternalAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');" </s:if>
															           <s:if test="description==@com.velos.ordercomponent.util.VelosGarudaConstants@HLA_DRB1">onchange="getMaternalAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');" </s:if>
															           <s:else>onchange="getMaternalAntigen2('<s:property value="description" />','<s:property value="%{#row.index}" />maternalmethod',this.value,'<s:property value="%{#row.index}" />maternaltype2div','<s:property value="%{#row.index}" />');isChangeDone(this,'<s:property value="hlaType2"/>','IDsChanges');"</s:else>
															           ></input>
															               <div id="<s:property value="%{#row.index}" />maternaltype2div">
													                           <s:set name="rowindex" value="%{#row.index}" scope="request"/>		
													                           <jsp:include page="cb_refresh-maternal-antigen2.jsp">
															                      <jsp:param value="<%=request.getAttribute(\"rowindex\")%>" name="hlaext"/>
															                   </jsp:include>
													                       </div>
															           </td><!--
															           <td width="10%" style="padding-left: 5px;">
															                <input type="text" onchange="populateHlaTypingDate('mhlasRecivedDatePic<s:property value="%{#row.index}"/>',this.value)" name="maternalHlas[<s:property value="%{#row.index}"/>].hlaTypingDate" class="datePicWMaxDate" readonly="readonly"/>
															          </td>
															          <td width="10%" style="padding-left: 5px;">
															                <input type="text" id="mhlasRecivedDatePic<s:property value="%{#row.index}"/>" name="maternalHlas[<s:property value="%{#row.index}"/>].hlaRecievedDate" readonly="readonly" />
															          </td>
															          <td width="40%" style="padding-left: 5px;">
															              <s:select name="maternalHlas[%{#row.index}].fkSpecType" listKey="pkCodeId" listValue="description" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MAT_SPEC_TYPE]" headerKey="-1" headerValue="Select" />
															          </td>
													   --></tr>										   								    
											        </s:if>		
											        <s:set name="check1" value="%{'notcheck'}" />									    							        
											    </s:iterator>
							  </s:if>
							</tbody>						
							<tfoot>
								<tr style="vertical-align:top;">
									<td colspan="4"></td>
								</tr>
							</tfoot>
						</table>
						<table width="100%" cellpadding="0" cellspacing="0" border="0" id="hlaesignTable">
						    <tr bgcolor="#cccccc">
								 <td ><jsp:include page="../cb_esignature.jsp" flush="true"></jsp:include></td>
									
							    <td  align="center" colspan="2">
							    <s:if test="#request.module!=null && #request.module==@com.velos.ordercomponent.util.VelosGarudaConstants@CBU">
							        <input type="button" id="submit" disabled= "disabled" onclick="proxyHLASubmition();<%--modalFormSubmitRefreshDiv('edithlaform','saveHlaAsyncData','hlaAsyncData');--%>" value="<s:text name="garuda.common.save"/>" />
							    </s:if>
							    <s:else>
							      <input type="button" id="submit" disabled= "disabled" onclick="submitHlaDetails()" value="<s:text name="garuda.common.save"/>" />
							    </s:else>   
							    <input type="button"  onclick="closeModal()" value="<s:text name="garuda.unitreport.label.button.cancel"/>" /></td>
						   </tr>
						</table>
					</div></div></div>
	    </div></s:form>