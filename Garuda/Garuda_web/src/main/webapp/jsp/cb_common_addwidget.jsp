<%@taglib prefix="s" uri="/struts-tags"%>
<%@ page import="com.velos.eres.web.grpRights.GrpRightsJB" %>
<% 
Long pageid = Long.parseLong(request.getParameter("pageId"));
request.setAttribute("pageId",pageid);

HttpSession tSession = request.getSession(true); 
GrpRightsJB grpRights =null;
if (tSession.getAttribute("LocalGRights")!=null){
 grpRights = (GrpRightsJB) tSession.getAttribute("LocalGRights");
}else
{
 grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
}

int viewLukup=0;
int viewadvncLukup = 0;
if(grpRights.getFtrRightsByValue("CB_LOOKUP")!=null && !grpRights.getFtrRightsByValue("CB_LOOKUP").equals(""))
{	viewLukup = Integer.parseInt(grpRights.getFtrRightsByValue("CB_LOOKUP"));}
else
	viewLukup = 4;
if(grpRights.getFtrRightsByValue("CB_ADVLOOKUP")!=null && !grpRights.getFtrRightsByValue("CB_ADVLOOKUP").equals(""))
{	viewadvncLukup = Integer.parseInt(grpRights.getFtrRightsByValue("CB_ADVLOOKUP"));}
else
viewadvncLukup = 4;

request.setAttribute("viewLukup",viewLukup);
request.setAttribute("viewadvncLukup",viewadvncLukup);
%>
<script>

function advancelookup(){
window.location="advancedLookUp";
}
$j(function(){
		$j("#lookupmenu").validate({
		rules:{			
			"cbuid":{
					required:true,
					minlength:3
				}
			},
		 messages:{
			"cbuid":{
				  required : "<s:text name="garuda.common.id.required"/>",
	                 minlength : "<s:text name="garuda.cbu.cordentry.inputIdminlength"/>"
				}
		 	}
	});
});
function actionSubmit(formName,e){
	evt = e || window.event;
    var keyPressed = evt.which || evt.keyCode;
	if (keyPressed == 13){
      <%-- if(document.getElementById('callresolver').value=="true"){--%>
              if($j("#"+formName).valid()){	   
	        	submitform('lookupmenu');
				return false;
				}
			<%--}--%>
		else{
		  if($j("#"+formName).valid()){
			$j("#"+formName).submit();
		   		}
			}//end of else
		}//end of outer if
	 else{	   
	    	return true;
		}	
    }	
</script>
<table width="100%">
	<tr>
		<td width="50%" valign="top">
			<jsp:include page="cb_displayAppMsgs.jsp"></jsp:include>
		</td>
		<td valign="top" width="50%">
			<div class="lookupmenu" align="right">
				<%--<s:hidden id="callresolver" name="pageloadstatus" value="false" />--%>
				<s:form id="lookupmenu" action="getMainCBUDetails" method="post">
					<table>
						<tr>
							<s:if test="hasViewPermission(#request.viewLukup)==true">
							<td>
								<input type="radio" name="sourceType" id="cbu" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES,@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_CODESUBTYPE)"/>" ><s:text name="garuda.advancelookup.label.cbu"/>
					 			<input type="radio" name="sourceType" id="maternal" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES,@com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_CODESUBTYPE)"/>" ><s:text name="garuda.advancelookup.label.maternal"/>
							</td>
							</s:if>
							<td id="lookupid">
								<input type="hidden" name="paginateModule" value="viewClinical"/>
								<input type="hidden" name="iShowRows" id="iShowRows" value="5"/>
								<input type="hidden" name="iPageNo" id="iPageNo" value="0"/>
								<s:if test="hasViewPermission(#request.viewLukup)==true || hasViewPermission(#request.viewadvncLukup)==true">
								<s:textfield name="cbuid" onkeypress="return actionSubmit('lookupmenu',event);" id="cbuid" placeholder="Enter Any ID" ></s:textfield>
								</s:if>
							</td>
							<td>
								<s:if test="hasViewPermission(#request.viewLukup)==true">
								<input type="button" style="text-decoration: none;" onclick="return submitform('lookupmenu');" value="<s:text name='garuda.common.able.lookup'></s:text>">
								</s:if>
								<s:if test="hasViewPermission(#request.viewadvncLukup)==true">
								<input type="button" style="text-decoration: none;" onclick="javascript:advancelookup();" value="<s:text name='garuda.common.able.advancedlookup'></s:text>">
								</s:if>
							</td>
							<!--<td class="align_right" nowrap="nowrap">
							<button type="button" id="widgetId" style="text-decoration: none;"
							                onclick="showAddWidgetModal('addWidgets?pageId=<s:property value="#request.pageId" />');" >Add Widget</button>
							
							</td>
						--></tr>
					</table> 
				</s:form>
			</div>
		</td>
	</tr>
</table>
<script>
$j(function(){
	$j('input:radio[name=sourceType]')[0].checked = true;
	var source = '<s:property value="sourceType"/>';
	var cbu = $j('#cbu').val(); 
	var mat = $j('#maternal').val();
	//alert(source+"----------"+cbu+"----------"+mat); 
	if(source==cbu){
		$j('input:radio[name=sourceType]')[0].checked = true;
	}
	else if(source==mat){
		$j('input:radio[name=sourceType]')[1].checked = true;
	}
});
</script>