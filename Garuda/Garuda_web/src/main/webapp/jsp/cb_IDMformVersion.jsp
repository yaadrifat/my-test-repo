<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<script>
function getIDMForm(iseditable,fkformv,frmvers,cordId,forseq,isAdd){
	var moduleEntityEventCode = ""
	if(iseditable=="1"){
		moduleEntityEventCode = "<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_FORM'/>";
	}else if(iseditable=="0"){
		moduleEntityEventCode = "<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_FORM'/>";
	}
	if(isAdd=="0"){
		moduleEntityEventCode = "<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_FORM'/>";
	}
	moduleEntityEventCode = moduleEntityEventCode.replace("@"," IDM ");
	moduleEntityEventCode+=" - Version "+frmvers;

	if(isAdd!="0"){
		var isCordEntryPage=$j('#idmisCordEntryPage').val();
		if(isCordEntryPage=='true'){
			var cbuRegID = $j('#cburegid').val();
			if(cbuRegID == "" || cbuRegID == null || cbuRegID == "undefined"){
				cbuRegID = "";
			}
			if($j('#licenseid').val()!="" && $j('#licenseid').val()!=null && $j('#licenseid').val()!=undefined && $j('#licenseid').val()!="-1"){
				if($j('#fkCbbId').val()!='' && $j('#fkCbbId').val()!=null && $j('#fkCbbId').val()!='undefined' && $j('#fkCbbId').val()!="-1"){
					fn_showModalIdm('<s:text name="garuda.label.forms.idmcbuid"/> '+cbuRegID,'getDynamicFrm?formId=IDM&entityId=<s:property value="cdrCbuPojo.cordID" />&iseditable='+iseditable+'&fkfrmversion='+fkformv+'&formvers='+frmvers+'&isCordEntry=1'+'&isCordEntryPage=true&seqnumber='+forseq+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU"/>&moduleEntityIdentifier='+cbuRegID+"&moduleEntityEventCode="+moduleEntityEventCode+'&collectionDate='+$j("#datepicker2").val(),'500','950','IDMModalForm',true,true);
				}else{
					alert('<s:text name="garuda.common.cbu.reqSiteAlert"/>');
					$j('#fkCbbId').focus();
				}
			}else{
				alert('<s:text name="garuda.common.cbu.reqLicensureAlert"/>');
				$j('#licenseid').focus();
			}
		}else{
			var cbuRegID = $j('#cburegIdHidden').val();
			if(cbuRegID == "" || cbuRegID == null || cbuRegID == "undefined"){
				cbuRegID = "";
			}
			fn_showModalIdm('<s:text name="garuda.label.forms.idmcbuid"/> '+cbuRegID,'getDynamicFrm?formId=IDM&entityId='+cordId+'&iseditable='+iseditable+'&fkfrmversion='+fkformv+'&formvers='+frmvers+'&isCordEntry=0&seqnumber='+forseq+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU"/>&moduleEntityIdentifier='+cbuRegID+"&moduleEntityEventCode="+moduleEntityEventCode,'500','950','IDMModalForm',true,true);
		}
	}else{
		fn_showModalIdm('<s:text name="garuda.label.forms.idmcbuid"/> <s:property value="cdrCbuPojo.registryId" />','getDynamicFrm?formId=IDM&entityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU"/>&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode='+moduleEntityEventCode,'500','950','IDMModalForm',true,true);
	}
}

function fn_showModalIdm(title,url,hight,width,id,min,max){
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title=title+patient_data;
	showModals(title,url,hight,width,id,min,max);
	}

	$j(function(){
		if($j("#idmisCordEntryPage").val()=="true"){
			if(idmcount==0){
			    if($j("#idmformversionlstsize").val()!=null && $j('#idmformversionlstsize').val()>0){
					  idmcount++;
					  val = parseInt((idmcount*100)/(noOfMandatoryIdmField));
					  $j("#IDMbar").progressbar({
						  value: val
						  }).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
					  addTotalCount();	
					}
				//call method to refersh total count progress
				  totalcount--;
			      addTotalCount();
			   // Load Validation If All Widget Is Fully Loaded	   
			      LoadedPage.idm=true;
			      if(LoadedPage.fmhq && LoadedPage.mrq && LoadedPage.processingProc && LoadedPage.cbuHla && LoadedPage.clinicalNote && LoadedPage.labSummary){
			    	  jQueryValidationInitializer();
			    }
			}
		}
	});	
</script>
<s:div id="IDM_formVersion">
<s:hidden name="isCordEntryPage" id="idmisCordEntryPage"></s:hidden>
<s:hidden name="cbustatusDesc" id="cbustatusDesc"></s:hidden>
<s:hidden name="statusCode" id="statusCode"></s:hidden>
<s:hidden value="%{idmformversionlst.size()}" id="idmformversionlstsize" name="idmFormVersion"></s:hidden>
	<table class="displaycdr" style="width: 98%">
		<thead>
			<tr>
				<th width="20%"><s:text name="garuda.label.dynamicform.date"></s:text></th>
				<th width="55%"><s:text name="garuda.label.dynamicform.formver"></s:text></th>
				<th width="20%"></th>
			</tr>
		</thead>
		<tbody>
		<s:set name="idmversionflag" value="" id="idmversionflag"></s:set>
			<s:if test="%{idmformversionlst!=null && idmformversionlst.size()>0}">
				<s:iterator value="idmformversionlst" var="rowVal" status="row">
					<tr>
						<td><s:property value="%{getText('collection.date',{#rowVal[7]})}"/></td>
						<td><s:if test="%{#rowVal[6]!=idmversion}"><s:property value="%{#rowVal[6]}"/></s:if><s:else><s:text name="garuda.label.forms.current"/></s:else></td>
						<td>
							<s:if test="%{#row.index==0}">
							<s:hidden name="idmLatestPk" id="idmLatestPk" value="%{#rowVal[0]}"></s:hidden>
							<s:hidden name="idmFKForm" id="idmFKForm" value="%{#rowVal[4]}"></s:hidden>
							<s:set name="idmversionflag" value="%{#rowVal[6]}" id="idmversionflag"></s:set>
								<a name="idmFormViewLink<s:property value="%{#row.index}"/>" id="idmFormViewLink<s:property value="%{#row.index}"/>" href="#" style="cursor: pointer;" onclick="getIDMForm('1','<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="%{#rowVal[2]}"/>','1')">
									<s:text name="garuda.label.dynamicform.view"/>
								</a>&nbsp;&nbsp;
								<s:if test="%{isCordEntryPage==false}">
									<s:if test="cdrCbuPojo.site.isCDRUser()==true && #idmversionflag==idmversion && hasEditPermission(#request.updateInfectDisease)==true ">
										<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
										</s:if>
										<s:else>
											<a href="#" name="idmFormUpdateLink<s:property value="%{#row.index}"/>" id="idmFormUpdateLink<s:property value="%{#row.index}"/>" style="cursor: pointer;" onclick="getIDMForm('0','<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="%{#rowVal[2]}"/>','1');">
												<s:text name="garuda.label.dynamicform.update"/>
											</a>
										</s:else>
									</s:if>
								</s:if>
								<s:else>
									<s:set name="IdmbloodCollectionDate" value="%{#rowVal[3]}"></s:set>
									<s:date name="IdmbloodCollectionDate" id="IdmbloodCollectionDate" format="MMM dd, yyyy" />
									<s:hidden value="%{IdmbloodCollectionDate}" id="idmbloodCollectionDate" name="IdmbloodCollectionDate"/>
									<a href="#" name="idmFormUpdateLink<s:property value="%{#row.index}"/>" id="idmFormUpdateLink<s:property value="%{#row.index}"/>" style="cursor: pointer;" onclick="getIDMForm('0','<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="%{#rowVal[2]}"/>','1');">
										<s:text name="garuda.label.dynamicform.update"/>
									</a>
								</s:else>
							</s:if>
							<s:else>
								<a href="#" name="idmFormViewLink<s:property value="%{#row.index}"/>" id="idmFormViewLink<s:property value="%{#row.index}"/>" style="cursor: pointer;" onclick="getIDMForm('1','<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="%{#rowVal[2]}"/>','1')">
									<s:text name="garuda.label.dynamicform.view"/>
								</a>
							</s:else>
						</td>
					</tr>
				</s:iterator>
			</s:if>			
<s:if test="%{isCordEntryPage==false}">
<tr><td colspan="3" style="padding-left: 0">
  <s:if test="hasEditPermission(#request.updateInfectDisease)==true && cdrCbuPojo.site.isCDRUser()==true">
  		<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
  		</s:if>
  		<s:else>
			<s:if test="%{idmformversionlst.size()==0}">
				<button type="button" name="idmFormButton" id="idmFormButton" onclick="getIDMForm('','','<s:property value="%{idmversion}"/>','','','0')"><s:text name="garuda.label.forms.addidm" /></button>
			</s:if>
			<s:else>
				<s:if test="%{#idmversionflag!=idmversion}">
					<button type="button" name="idmFormButton" id="idmFormButton" onclick="getIDMForm('','','<s:property value="%{idmversion}"/>','','','0')"><s:text name="garuda.label.forms.addidm" /></button>
				</s:if>
			</s:else>
		</s:else>
 </s:if>
</td></tr>
</s:if>
<s:else>
<tr><td style="padding-left: 0">
	<s:if test="%{idmformversionlst.size()==0}">
		<button type="button" name="idmFormButton" id="idmFormButton" onclick="getDynameicForms('IDM','<s:property value="%{idmversion}"/>');"><s:text name="garuda.label.forms.addidm" /></button>
		<span class="error" id="idmFormMan" style="display:none"><s:text name="garuda.cordentry.label.idm"/>&nbsp;<s:text name="garuda.label.forms.validationmsg" /></span>
	</s:if>
	<s:else>
		<s:if test="%{#idmversionflag!=idmversion}">
			<button type="button" name="idmFormButton" id="idmFormButton" onclick="getDynameicForms('IDM','<s:property value="%{idmversion}"/>');"><s:text name="garuda.label.forms.addidm" /></button>
			<span class="error" id="idmFormMan" style="display:none"><s:text name="garuda.cordentry.label.idm"/>&nbsp;<s:text name="garuda.label.forms.validationmsg" /></span>
		</s:if>
	</s:else>
</td></tr>
</s:else>
</tbody>
</table>
</s:div>