<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<script type="text/javascript">
$j(function(){
	$j("#advancedForm").validate({
		invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    },
	rules:{
		"collectionDateFromStr":{required:
									{
									  depends: function(element){
										  return $j("#collectionDateTo").val()!="";
										}
									},dpDate:true
								},
		"collectionDateToStr":{required:
									{
									  depends: function(element){
										  return $j("#collectionDateFrom").val()!="";
										}
									},dpDate:true
								},
		"submitionDateFromStr":{required:
								{
								  depends: function(element){
									  return $j("#submitionDateTo").val()!="";
									}
								},dpDate:true
							},
		"submitionDateToStr":{required:
							{
							  depends: function(element){
								  return $j("#submitionDateFrom").val()!="";
								}
							},dpDate:true
						}
 },

 messages:{
	 "collectionDateFromStr":{required:"<s:text name="garuda.cbu.cordentry.collectionDateFromStr"/>"},
	 "collectionDateToStr":{required:"<s:text name="garuda.cbu.cordentry.collectionDateToStr"/>"},
	 "submitionDateFromStr":{required:"<s:text name="garuda.cbu.cordentry.submitionDateFromStr"/>"},
	 "submitionDateToStr":{required:"<s:text name="garuda.cbu.cordentry.submitionDateToStr"/>"}
 }
});
});

$j(function(){
	getDatePic();
	jQuery('#collectionDateTo, #collectionDateFrom').datepicker('option', {
	    beforeShow: customRange2
	  }).focus(function(){
		  if(!$j("#ui-datepicker-div").is(":visible"))
		  		customRange2(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	jQuery('#submitionDateTo, #submitionDateFrom').datepicker('option', {
	    beforeShow: customRange
	  }).focus(function(){
		  if(!$j("#ui-datepicker-div").is(":visible"))
		  		customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
});

function customRange2(input) {
	 if (input.id == 'collectionDateTo') {
		 if($j("#collectionDateFrom").val()!="" && $j("#collectionDateFrom").val()!=null){
		    var minTestDate=new Date($j("#collectionDateFrom").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
			  $j('#collectionDateTo').datepicker( "option", "minDate", new Date(y1,m1,d1));
		 }else{
			 $j('#collectionDateTo').datepicker( "option", "minDate", null);
		}
		  } else if (input.id == 'collectionDateFrom') {
			  if($j("#collectionDateTo").val()!="" && $j("#collectionDateTo").val()!=null){
			  var minTestDate=new Date($j("#collectionDateTo").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
			 	$j('#collectionDateFrom').datepicker( "option", "maxDate", new Date(y1,m1,d1));
			  }else{
				  $j('#collectionDateFrom').datepicker( "option", "maxDate", null);
			}
		  }
	}

function customRange(input) {
	 if (input.id == 'submitionDateTo') {
		 if($j("#submitionDateFrom").val()!="" && $j("#submitionDateFrom").val()!=null){
		    var minTestDate=new Date($j("#submitionDateFrom").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
			  $j('#submitionDateTo').datepicker( "option", "minDate", new Date(y1,m1,d1));
		 }else{
			 $j('#submitionDateTo').datepicker( "option", "minDate", null);
		 }
		  } else if (input.id == 'submitionDateFrom') {
			  if($j("#submitionDateTo").val()!="" && $j("#submitionDateTo").val()!=null){
			  var minTestDate=new Date($j("#submitionDateTo").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
			  $j('#submitionDateFrom').datepicker( "option", "maxDate", new Date(y1,m1,d1));
			  }else{
				  $j('#submitionDateFrom').datepicker( "option", "maxDate", null);
			}
		  }
	}

function clearerreor(val,idclear){
	//alert(val);
	if(val != ""){
	$j('#'+idclear).html("");
	}
}

function checkids()
{	
	var cbucheckFlag = 0;
	var cbucheckboxlist = document.getElementsByName('cbusearchId');
	for(var i=0;i<cbucheckboxlist.length;i++){
		if(cbucheckboxlist[i].checked){
			cbucheckFlag = 1;
		}
	}
	
	var matcheckFlag = 0;
	var matcheckboxlist = document.getElementsByName('maternalSearchId');
	for(var i=0;i<matcheckboxlist.length;i++){
		if(matcheckboxlist[i].checked){
			matcheckFlag = 1;
		}
	}

	var eligible = 0;
	var eligiblelist = document.getElementsByName('cordCbuEliStatus');
	for(var i=0;i<eligiblelist.length;i++){
		if(eligiblelist[i].checked){
			eligible = 1;
		}
	}

	var licensure = 0;
	var licensurelist = document.getElementsByName('cordCbuLicStatus');
	for(var i=0;i<licensurelist.length;i++){
		if(licensurelist[i].checked){
			licensure = 1;
		}
	}

	var cordStatuss = 0;
	var cordStatuslist = document.getElementsByName('cordStatus');
	for(var i=0;i<cordStatuslist.length;i++){
		if(cordStatuslist[i].checked){
			cordStatuss = 1;
		}
	}
	//var cordStatuss = $j('#cordStatus').val();

	var search = $j("input[name='searchId']:checked").length;
	
	var workFlowFlag = 0;
	var workFlowlist = document.getElementsByName('workflow');
	for(var i=0;i<workFlowlist.length;i++){
		if(workFlowlist[i].checked){
			workFlowFlag = 1;
		}
	}
	var wokflowSampleAtLab = 0;
	var wokflowSampleAtLabList = document.getElementsByName('sampleAtLab');
	for(var i=0;i<wokflowSampleAtLabList.length;i++){
		if(wokflowSampleAtLabList[i].checked){
			wokflowSampleAtLab = 1;
		}
	}
	
	var collectionDateFrom = $j('#collectionDateFrom').val();
	var collectionDateTo = $j('#collectionDateTo').val();
	var submitionDateFrom = $j('#submitionDateFrom').val();
	var submitionDateTo = $j('#submitionDateTo').val(); 
	var inputId = $j('#inputId').val();
	inputId = $j.trim(inputId);
	if(cbucheckFlag==0 && matcheckFlag==0 && eligible==0 && licensure==0 && cordStatuss==0 && workFlowFlag==0 && wokflowSampleAtLab==0 && collectionDateFrom=="" && collectionDateTo=="" && submitionDateFrom=="" && submitionDateTo==""){
		$j('#totallblerror').html('<s:text name="garuda.cbu.advancedlookup.msgForAll"/>');
		$j('#selectSearch').html("");
		$j('#inputIdreq').html("");
	}
	else if((cbucheckFlag==1 || matcheckFlag==1) && inputId==""){
		$j('#totallblerror').html("");
		$j('#selectSearch').html("");
		$j('#inputIdreq').html('<s:text name="garuda.common.id.required"/>');
	}else if((cbucheckFlag==1 || matcheckFlag==1) && inputId.length <= 2){
		$j('#totallblerror').html("");
		$j('#selectSearch').html("");
		$j('#inputIdreq').html('<s:text name="garuda.cbu.cordentry.inputIdminlength"/>');
	}else if((cbucheckFlag==1 || matcheckFlag==1) && inputId.length >= 2 && search==0){
		$j('#totallblerror').html("");
		$j('#inputIdreq').html("");
		$j('#selectSearch').html('<s:text name="garuda.cbu.advancedlookup.msgAny"/>');
	}else if((cbucheckFlag==0 && matcheckFlag==0) && inputId!=""){
		$j('#totallblerror').html('<s:text name="garuda.cbu.advancedlookup.msgForid"/>');
		$j('#selectSearch').html("");
		$j('#inputIdreq').html("");
	}
	else{
		$j('#totallblerror').html("");
		$j('#inputIdreq').html("");
		$j('#selectSearch').html("");
		if($j('#advancedForm').valid()){
			submitform('advancedForm');
		}
	}	
}

function ShowResult(){

	window.location="advancedLookUp1";

}

function showAddWidget(url){
	 url = url + "?pageId=22" ; 
	 showAddWidgetModal(url);
}

</script>

<div class="col_100" id="mainDiv">
<table width="100%" id="container1">
	<tr>
		<td>
		<div class="column">
		<div class="portlet" id="advLookupparent">
		<div id="addvancedlookupdiv"
			class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
		<div
			class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
		<span class="ui-icon ui-icon-minusthick"></span> <!--<span
			class="ui-icon ui-icon-close"></span>--> <s:text
			name="Advanced Lookup" /></div>
		<div class="portlet-content">
		<div id="advanced">

<s:form id="advancedForm" name="advancedForm" action="getMainCBUDetails" >
<s:hidden name="starts" id="starts"></s:hidden>
<s:hidden name="ends" id="ends"></s:hidden>
<s:hidden name="contains" id="contains"></s:hidden>
<s:hidden name="exact" id="exact"></s:hidden>
<input type="hidden" name="paginateModule" value="advanceLookUp"/>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="2">
			<table style="width: 55%">
				<tr>
					<td width="5%"><s:text name="garuda.advancelookup.label.id"></s:text>:
					</td>
					<td width="30%">
					<s:radio name="searchId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SEARCH_IDS]" listKey="pkCodeId" listValue="description" id="searchId" onclick="clearerreor(this.value,'selectSearch');" />
					<br/><span class="error"><label id="selectSearch"></label></span></td>
					<td width="20%"><s:textfield name="inputId" value="" placeholder="Enter any ID" id="inputId" />
					<br/><span class="error"><label id="inputIdreq"></label></span></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td width="100%">
			<fieldset><legend><s:text
				name="garuda.advancelookup.label.cbu" /></legend>
			<table>
				<tr>
					<td>
					<s:checkboxlist list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_IDS]" listKey="pkCodeId" listValue="description" name="cbusearchId" onclick ="clearerreor(this.value,'cbulblerror');clearerreor(this.value,'totallblerror');"/>
					<span class="error"><label id="cbulblerror"></label> </span>
					</td>
				</tr>
			</table>
			</fieldset>
			</td>
		</tr>
		
		<tr>
			<td width="100%">
			<fieldset><legend><s:text
				name="garuda.advancelookup.label.maternal" /></legend>
			<table>
				<tr>
					<td>
					<s:checkboxlist list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_IDS]" listKey="pkCodeId" listValue="description" name="maternalSearchId" onclick ="clearerreor(this.value,'matlblerror');clearerreor(this.value,'totallblerror');"/>
					<span class="error"><label id="matlblerror"></label> </span>
					</td>
					<td></td>
				</tr>
			</table>
			</fieldset>
			</td>
		</tr>
		<!--  <tr>
			<td>
		 <fieldset><legend><s:text
				name="garuda.advancelookup.label.donor" /></legend>
			<table>
				<tr>
					<td><span class="error"><s:text name="garuda.cbu.label.id_discussederror"></s:text></span></td>
				</tr>
			</table>
			</fieldset>
			</td>
			<td>
			 <fieldset><legend><s:text
				name="garuda.advancelookup.label.patient" /></legend>
			<table>
				<tr>
					<td><span class="error"><s:text
						name="garuda.cbu.label.id_discussederror"></s:text></span></td>
				</tr>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<fieldset><legend><s:text
				name="garuda.advancelookup.label.product" /></legend>
			<table>
				<tr>
					<td><span class="error"><s:text
						name="garuda.cbu.label.id_discussederror"></s:text></span></td>
				</tr>
			</table>
			</fieldset>
			</td>
		</tr>  -->
		<tr>
			<td colspan="2">
			<table>
				<tr style="width: 100%">
					<td width="25%"><s:text name="garuda.advancelookup.label.eligibility"></s:text>:</td>
					<td width="75%"><s:checkboxlist list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS]" listKey="pkCodeId" listValue="description" name="cordCbuEliStatus" onclick ="clearerreor(this.value,'totallblerror');"/></td>
				</tr>
				<tr>
					<td><s:text name="garuda.advancelookup.label.licstus"></s:text>:</td>
					<td><s:checkboxlist list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS]" listKey="pkCodeId" listValue="description" name="cordCbuLicStatus" onclick ="clearerreor(this.value,'totallblerror');"/></td>
				</tr>
				<tr>
					<td><s:text name="garuda.advancelookup.label.crdstus"></s:text>:</td>
					<td><s:checkboxlist list="getCBUStatusValues(@com.velos.ordercomponent.business.util.VelosMidConstants@NATIONAL_STATUS)" listKey="pkcbustatus" listValue="cbuStatusDesc" name="cordStatus" onclick ="clearerreor(this.value,'totallblerror');"/></td>
				
				</tr>
				<tr>
					<td><s:text name="garuda.advancelookup.label.workflow"></s:text>:</td>
					<td>
					 <input type="checkbox" name="sampleAtLab" value="Y" onclick ="clearerreor(this.value,'totallblerror');" ><s:text name="garuda.orOrderDetail.portletname.ctlaborder"/>
					 <input type="checkbox" name="workflow" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_TYPE_NEW,@com.velos.ordercomponent.util.VelosGarudaConstants@CTORDER)"/>" onclick ="clearerreor(this.value,'totallblerror');" ><s:text name="garuda.orOrderDetail.portletname.ctshiporder"/>
					 <input type="checkbox" name="workflow" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_TYPE_NEW,@com.velos.ordercomponent.util.VelosGarudaConstants@HEORDER)"/>" onclick ="clearerreor(this.value,'totallblerror');" ><s:text name="garuda.heOrderDetail.portletname.heOrder"/>
					 <input type="checkbox" name="workflow" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_TYPE_NEW,@com.velos.ordercomponent.util.VelosGarudaConstants@ORORDER)"/>" onclick ="clearerreor(this.value,'totallblerror');" ><s:text name="garuda.orOrderDetail.portletname.orOrder"/>
					</td>
				</tr>

			</table>
			</td>
		</tr>	
		<tr>
		<td colspan="2">
		<table style="width: 55%"><tr>
			<td width="25%"><s:text name="garuda.advancelookup.label.colldate"></s:text>:</td>
			<td><s:date name="collectionDateFrom" id="collectionDateFrom" format="MMM dd, yyyy" />
			    <s:textfield readonly="true" onkeydown="cancelBack();"  name="collectionDateFromStr" class="datepic" id="collectionDateFrom"
				value="%{collectionDateFrom}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/></td>
			<td><s:text name="garuda.advancelookup.label.to"></s:text>:</td>
			<td><s:date name="collectionDateTo" id="collectionDateTo" format="MMM dd, yyyy" />
			<s:textfield readonly="true" onkeydown="cancelBack();"  name="collectionDateToStr" class="datepic"
				id="collectionDateTo" value="%{collectionDateTo}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/></td>
		</tr>
		<tr>
			<td><s:text name="garuda.advancelookup.label.subdate"></s:text>:</td>
			<td><s:date name="submitionDateFrom" id="submitionDateFrom" format="MMM dd, yyyy" />
			<s:textfield readonly="true" onkeydown="cancelBack();"  name="submitionDateFromStr" class="datepic"
				id="submitionDateFrom" value="%{submitionDateFrom}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/></td>
			<td><s:text name="garuda.advancelookup.label.to"></s:text>:</td>
			<td><s:date name="submitionDateTo" id="submitionDateTo" format="MMM dd, yyyy" />
			<s:textfield readonly="true" onkeydown="cancelBack();"  name="submitionDateToStr" class="datepic"
				id="submitionDateTo" value="%{submitionDateTo}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/></td>
		</tr>
		<tr>
		<td colspan="4" align="center"><span class="error"><label id="totallblerror"></label> </span></td>
		</tr>
		<tr>
			<td colspan="4" align="center">
			<button type="button" onclick="javascript:checkids();"><s:text
				name="garuda.advancelookup.label.lookup" ></s:text></button>
			<button type="button" onclick="this.form.reset()"><s:text
				name="garuda.advancelookup.label.reset"></s:text></button>
			</td>
		</tr>
		</table>
		</td>
		</tr>
	</table>
	
</s:form>
</div>
</div>
</div>
</div>
</div>
</td>
</tr>
</table>
</div>