<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB" %>
<%
HttpSession tSession = request.getSession(true);
GrpRightsJB grpRights = (GrpRightsJB) tSession.getAttribute("GRights");
int cbuMngLnkMenuRght=0;
if(grpRights.getFtrRightsByValue("CB_MNGLINKS")!=null && !grpRights.getFtrRightsByValue("CB_MNGLINKS").equals(""))
{cbuMngLnkMenuRght = Integer.parseInt(grpRights.getFtrRightsByValue("CB_MNGLINKS"));}
else
	cbuMngLnkMenuRght = 4;		
request.setAttribute("cbuMngLnkMenuRght",cbuMngLnkMenuRght);
%>
<script>
function editLink(obj){
	$j(obj).prev('input[type=text]').removeAttr('disabled');
	$j(obj).prev('input[type=text]').focus();
	$j(obj).hide();
	$j(obj).next('button').show();
}
function saveLink(obj){
	var rowId=$j(obj).parent().parent().attr('id');
	refereshLinkRow(rowId,'updatenmdpLink');
	//$j(obj).prev('input[type=text]').attr('disabled','true');
	$j(obj).hide();
	$j(obj).prev('button').show().focus();
}
function refereshLinkRow(rowId,url){
	/*var progressMsg="<table  width='100%' height='100%'><tr><td width='100%' align='center'><table><tr><td align='center'><s:text name="garuda.common.message.wait"/><img src=\"../images/jpg/loading_pg.gif\" /></td></tr></table></td></tr></table>";
	var processingSaveDialog = document.createElement('div');
		processingSaveDialog.innerHTML=progressMsg;		
		$j(processingSaveDialog).dialog({autoOpen: false,
			resizable: false, width:400, height:90,closeOnEscape: false,
			modal: true}).siblings('.ui-dialog-titlebar').remove();		
		$j(processingSaveDialog).dialog("open");*/
		showprogressMgs();
	$j.ajax({
        type: "POST",
        url: url,
        async:false,
        data:$j('#'+rowId+' *').serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
            var $response=$j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
            	result=$j(result).find('#'+rowId).html();
            	$j("#"+rowId).html(result);
            }
            $j('button').button();
        },
        error: function (request, status, error) {
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
		closeprogressMsg();
}

</script>
<div class="column">
<div class="portlet" id="manageLinkpar" >
<div id="manageLinkinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
	<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
		<span class="ui-icon ui-icon-minusthick"></span>
		<s:text name="garuda.cordentry.label.mngnmdblink" />
	</div>
<s:form>
<s:if test="%{urlList!=null && urlList.size()>0}">
<table width="100%" class="displaycdr" cellpadding="0" cellspacing="0">
<thead>
	<tr><th><s:text name="garuda.common.description"></s:text> </th><th><s:text name="garuda.common.links"></s:text> </th></tr>
</thead>
<tbody>
	<s:iterator value="%{urlList}" var="rowval" status="row">
		<tr class="<s:if test="#row.even">even</s:if><s:else>odd</s:else>" id="row_<s:property value='%{#row.index}'/>" style="width: 100%">
			<td align="left" width="20%"><span><s:property value="%{#rowval[1]}"/></span></td>
			<td align="left" width="80%">
				<s:hidden name="linkId" value="%{#rowval[0]}" id="linkId_%{#row.index}"></s:hidden>
				<s:hidden name="urlDesc" value="%{#rowval[1]}" id="urlDesc_%{#row.index}"></s:hidden>
				<s:textfield name="url" value="%{#rowval[2]}" cssStyle="width:90%" disabled="true"/>
				<s:if test="hasEditPermission(#request.cbuMngLnkMenuRght)==true">  
				<button type="button" onclick="editLink(this);" style="float: right;"><s:text name="garuda.common.lable.edit"></s:text></button>
				</s:if>
				<button type="button" style="display: none;float: right;" onclick="saveLink(this);"><s:text name="garuda.common.save"></s:text></button>
			</td>
		</tr>
	</s:iterator>
</tbody>
</table>
</s:if>
</s:form>
</div>
</div>
</div>