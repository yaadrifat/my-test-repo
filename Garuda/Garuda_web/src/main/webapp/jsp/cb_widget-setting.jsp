<%@ taglib prefix="s"  uri="/struts-tags"%>
<% String contextpath=request.getContextPath();%>

<!-- CSS -->
<!-- jquery Common CSS -->
<link href="<%=contextpath%>styles/jquery-ui-1.8.7.custom.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<%=contextpath%>/jsp/js/jquery/themes/nmdp/jquery.ui.all.css">

<!--jquery js -->
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-ui-1.8.7.custom.min.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.core.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/ui/jquery.ui.mouse.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/ui/jquery.ui.sortable.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/ui/jquery.ui.resizable.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-latest.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-tootltip.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.columnmanager.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.columnmanager.min.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.columnmanager.pack.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.clickmenu.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.clickmenu.pack.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.dataTables.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.numeric.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.decimalMask.1.1.1.min.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery-fieldselection.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.jclock.js"></script>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/common/table2csv.js"></script>


<%@ page import="com.velos.ordercomponent.business.domain.User,java.util.List,java.util.ArrayList,java.util.Map,java.util.HashMap" %>
<%@ page import="com.velos.ordercomponent.business.domain.Widget" %>
<%@ page import="com.velos.ordercomponent.business.domain.WidgetInstance" %>
<%@page import="java.util.List"%>
<% 
Map<Long,List<Widget>> widgetInfo = (Map<Long,List<Widget>>)application.getAttribute("widgetInfoMap");
Long pageid = Long.parseLong(request.getParameter("pageId"));
List<Widget> widgetList =  widgetInfo.get(pageid);
request.setAttribute("widgetlist",widgetList);
%>
<style>
.ui-resizable-helper { border: 2px dotted #00F; }
<!--

-->
</style>

<script>
var $j = jQuery;

$j(document).ready(function(){
	var flag =true;
	var flag1 = true;
	$j( ".column" ).sortable({
		connectWith: ".column"
	});
	$j( ".column" ).sortable({ distance: 200 });
	<s:iterator value="#request.widgetlist" >
	 if('<s:property value="isMinimizable" />'=='true')
	 {
		 var parentDiv = '<s:property value="widgetDivId" />'+'parent';
		 var widgetDiv = '<s:property value="widgetDivId" />';
		$j('#'+'<s:property value="widgetDivId" />').find(".portlet-header .ui-icon").bind("click",function() {

			if('<s:property value="isClosable" />'=='true')
			 {
		    	if($j(this).is(".ui-icon-close"))
				{
						
				$j(this).parent().parent().parent().css('display','none');
					
				}
			 }
			if($j(this).is(".ui-icon-circle-plus"))
			{
				var maxId = $j(this).parent().parent().attr("id");
				//alert(maxId);
				$j("#"+maxId).dialog({
     				modal: true, width:1024, height:1024,
     				closeText: '',
					closeOnEscape: false ,
     				close: function() {
     					//$j(".ui-dialog-content").html("");
     					//jQuery("#subeditpop").attr("id","subeditpop_old");
     					$j("#"+maxId).dialog("destroy");
     			    }
     			   });			
			}		
			
			if($j(this).is(".ui-icon-minusthick") || $j(this).is(".ui-icon-plusthick"))
			{
				$j(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
				
				$j(this).parents(".portlet:first").find(".portlet-content").toggle();
				
				var divid = $j(this).parent().parent().attr("id");
				var divHeight;
				if(flag)
				divHeight=document.getElementById(divid).style.height;
				if(divHeight=='100%')
				{
					divHeight = 'auto';
					}	
			    $j(this).parents(".portlet:first").css('height',divHeight);
				
			}
			
			flag=false;			
			});
	 }

	 if('<s:property value="isResizable" />'=='true')
	 {
		  var minHeight = '<s:property value="resizeMinHeight" />';
		  var minWidth = '<s:property value="resizeMinWidth" />';
		  var maxHeight = '<s:property value="resizeMaxHeight" />';
		  var maxWidth = '<s:property value="resizeMaxWidth" />';
		  $j('#'+parentDiv).resizable({helper: "ui-resizable-helper"});		  
		  $j('#'+parentDiv).resizable({minHeight: minHeight});
		  $j('#'+parentDiv).resizable({minWidth: minWidth});	
		  $j('#'+parentDiv).resizable({maxHeight: maxHeight});
		  $j('#'+parentDiv).resizable({maxWidth: maxWidth});			 
		 // $j('#'+parentDiv).resizable("option", "minWidth",minWidth );		  
	 }

	 if('<s:property value="isDragable" />'=='true')
	 {
	  //$j('#'+parentDiv).draggable();
	 }		
	 //$j('#'+parentDiv).droppable({
	 //     drop: function() { alert('dropped'); }
	//    });
	
	//for Setting the Height on the div
	$j('#'+'<s:property value="widgetDivId" />').css('height','100%');
	$j('#'+'<s:property value="widgetDivId" />').find('.portlet-content').each(function(){
          $j(this).css('height','100%');
		});
	</s:iterator>		
	//$j( ".column" ).disableSelection();

	jQuery.validator.addMethod("checkselect", function(value, element) {
		return (true && (parseInt(value) != -1));
	}, "Please Select Value");

	jQuery.validator.addMethod("greaterThanZero", function(value, element) {
	    return this.optional(element) || (parseFloat(value) < 101);
	}, "<s:text name="garuda.cbu.widget.percentage"/>");
	
	jQuery.validator.addMethod("checksum", function(value, element) {
		return (true && (CheckSum(value)));
	}, "<s:text name="garuda.cbu.widget.reqValidValue"/>");	

	jQuery.validator.addMethod("checkdbdata", function(value, element, params) {
		 var url = "getValidateDbData?entityName="+params[0]+"&entityField="+params[1]+"&entityValue="+value;
		 return (true && (validateDbData(url)));
	}, "<s:text name="garuda.common.validation.data"/>");	

	jQuery.validator.addMethod("checkisbtdata", function(value, element) {
		 var url = "getCodeValidate?refnum="+value+"&codeval="+$j("#isbidin").val();
		 return (true && (validateDbData(url)));		 
	}, "<s:text name="garuda.cbu.widget.validIsbtData"/>");

	jQuery.validator.addMethod("checkspecialchar", function(value, element) {
		 return (true && (checkSpecialCharacters(value)));		 
	}, "<s:text name="garuda.cbu.widget.specialCharacter"/>");
	
});

</script>
