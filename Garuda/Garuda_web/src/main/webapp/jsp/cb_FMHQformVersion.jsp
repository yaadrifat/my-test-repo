<%@taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<script>
function getFMHQForm(iseditable,fkformv,frmvers,cordId,forseq,isAdd){
	var moduleEntityEventCode = ""
		if(iseditable=="1"){
			moduleEntityEventCode = "<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_FORM'/>";
		}else if(iseditable=="0"){
			moduleEntityEventCode = "<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_FORM'/>";
		}
		if(isAdd=="0"){
			moduleEntityEventCode = "<s:property value='@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_FORM'/>";
		}
		moduleEntityEventCode = moduleEntityEventCode.replace("@"," FMHQ ");
		moduleEntityEventCode += " - Version "+frmvers;
		if(isAdd!="0"){
	var isCordEntryPage=$j('#fmhqisCordEntryPage').val();
	if(isCordEntryPage=='true'){
		var cbuRegID = $j('#cburegid').val();
		if(cbuRegID == "" || cbuRegID == null || cbuRegID == "undefined"){
			cbuRegID = "";
		}
		if($j('#licenseid').val()!="" && $j('#licenseid').val()!=null && $j('#licenseid').val()!=undefined && $j('#licenseid').val()!="-1"){
			if($j('#fkCbbId').val()!='' && $j('#fkCbbId').val()!=null && $j('#fkCbbId').val()!='undefined' && $j('#fkCbbId').val()!="-1"){
				fn_showModalFMHQ('<s:text name="garuda.label.forms.fmhqcbuid"/> '+cbuRegID,'getDynamicFrm?formId=FMHQ&entityId='+cordId+'&iseditable='+iseditable+'&fkfrmversion='+fkformv+'&formvers='+frmvers+'&isCordEntry=1'+'&isCordEntryPage=true&seqnumber='+forseq+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU"/>&moduleEntityIdentifier='+cbuRegID+"&moduleEntityEventCode="+moduleEntityEventCode,'500','1100','FMHQModalForm',true,true);
			}else{
				alert('<s:text name="garuda.common.cbu.reqSiteAlert"/>');
				$j('#fkCbbId').focus();
			}
		}else{
			alert('<s:text name="garuda.common.cbu.reqLicensureAlert"/>');
			$j('#licenseid').focus();
		}
	}else{
		var cbuRegID = $j('#cburegIdHidden').val();
		if(cbuRegID == "" || cbuRegID == null || cbuRegID == "undefined"){
			cbuRegID = "";
		}
		fn_showModalFMHQ('<s:text name="garuda.label.forms.fmhqcbuid"/> '+cbuRegID,'getDynamicFrm?formId=FMHQ&entityId='+cordId+'&iseditable='+iseditable+'&fkfrmversion='+fkformv+'&formvers='+frmvers+'&isCordEntry=0&seqnumber='+forseq+'&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU"/>&moduleEntityIdentifier='+cbuRegID+"&moduleEntityEventCode="+moduleEntityEventCode,'500','1100','FMHQModalForm',true,true);
	}
		}else{
			fn_showModalFMHQ('<s:text name="garuda.label.forms.fmhqcbuid"/> <s:property value="cdrCbuPojo.registryId" />','getDynamicFrm?formId=FMHQ&entityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CBU"/>&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode='+moduleEntityEventCode,'500','950','FMHQModalForm',true,true);
		}
}
function fn_showModalFMHQ(title,url,hight,width,id,min,max){
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title=title+patient_data;
	showModals(title,url,hight,width,id,min,max);
}
if($j('#fmhqisCordEntryPage').val()!='true'){
	showformsFromStatic();	
}

$j(function(){
	if($j("#fmhqisCordEntryPage").val()=="true"){
		if(fmhqcount==0){
    		if($j("#fmhqformversionlstsize").val()!=null && $j('#fmhqformversionlstsize').val()>0){
		   		fmhqcount++;		  
		   		val = parseInt((fmhqcount*100)/(noOfMandatoryFmhqField));
		   		$j("#FMHQbar").progressbar({
					value: val
				}).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
		   		addTotalCount();		
	   		}
		     //call method to refersh total count progress
			  totalcount--;
		      addTotalCount();
		   // Load Validation If All Widget Is Fully Loaded	   
		      LoadedPage.fmhq=true;
		      if(LoadedPage.idm && LoadedPage.mrq && LoadedPage.processingProc && LoadedPage.cbuHla && LoadedPage.clinicalNote && LoadedPage.labSummary){
		    	  jQueryValidationInitializer();
		      }
		}
	}
});	
</script>
<s:div id="FMHQ_formVersion">
<s:hidden name="isCordEntryPage" id="fmhqisCordEntryPage"></s:hidden>
<s:hidden value="%{fmhqformversionlst.size()}" id="fmhqformversionlstsize" name="fmhqFormVersion"></s:hidden>
	<table class="displaycdr" style="width: 98%">
		<thead>
			<tr>
				<th width="20%"><s:text name="garuda.label.dynamicform.date"></s:text></th>
				<th width="55%"><s:text name="garuda.label.dynamicform.formver"></s:text></th>
				<th width="20%"></th>
			</tr>
		</thead>
		<tbody>
		<s:set name="fmhqversionflag" value="" id="fmhqversionflag"></s:set>
			<s:if test="%{fmhqformversionlst!=null && fmhqformversionlst.size()>0}">
				<s:iterator value="fmhqformversionlst" var="rowVal" status="row">
					<tr>
						<td><s:property value="%{getText('collection.date',{#rowVal[7]})}"/></td>
						<td><s:if test="%{#rowVal[6]!=fmhqversion}"><s:property value="%{#rowVal[6]}"/></s:if><s:else><s:text name="garuda.label.forms.current"/></s:else></td>
						<td>
							<s:if test="%{#row.index==0}">
							<s:set name="fmhqversionflag" value="%{#rowVal[6]}" id="fmhqversionflag"></s:set>
								<a name="fmhqFormViewLink<s:property value="%{#row.index}"/>" id="fmhqFormViewLink<s:property value="%{#row.index}"/>" href="#" style="cursor: pointer;" onclick="getFMHQForm('1','<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="%{#rowVal[2]}"/>','1')">
									<s:text name="garuda.label.dynamicform.view"/>
								</a>&nbsp;&nbsp;
								<s:if test="%{isCordEntryPage==false}">
									<s:if test="cdrCbuPojo.site.isCDRUser()==true && #fmhqversionflag==fmhqversion && hasEditPermission(#request.updateHlthHis)==true">
										<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
										</s:if>
										<s:else>
											<a name="fmhqFormUpdateLink<s:property value="%{#row.index}"/>" id="fmhqFormUpdateLink<s:property value="%{#row.index}"/>" href="#" style="cursor: pointer;" onclick="getFMHQForm('0','<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="%{#rowVal[2]}"/>','1');" class="formsClass">
												<s:text name="garuda.label.dynamicform.update"/>
											</a>
										</s:else>
									</s:if>
								</s:if>
								<s:else>
									<a href="#" name="fmhqFormUpdateLink<s:property value="%{#row.index}"/>" id="fmhqFormUpdateLink<s:property value="%{#row.index}"/>" style="cursor: pointer;" onclick="getFMHQForm('0','<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="%{#rowVal[2]}"/>','1');" class="formsClass">
										<s:text name="garuda.label.dynamicform.update"/>
									</a>
								</s:else>
							</s:if>
							<s:else>
								<a name="fmhqFormViewLink<s:property value="%{#row.index}"/>" id="fmhqFormViewLink<s:property value="%{#row.index}"/>" href="#" style="cursor: pointer;" onclick="getFMHQForm('1','<s:property value="%{#rowVal[0]}"/>','<s:property value="%{#rowVal[6]}"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="%{#rowVal[2]}"/>','1');">
									<s:text name="garuda.label.dynamicform.view"/>
								</a>
							</s:else>
						</td>
					</tr>
				</s:iterator>
			</s:if>
<s:if test="%{isCordEntryPage==false}">
<tr class="formsClass"><td colspan="3">
	<s:if test="hasEditPermission(#request.updateHlthHis)==true  && cdrCbuPojo.site.isCDRUser()==true">
	<s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
	</s:if>
	<s:else>
		<s:if test="%{fmhqformversionlst.size()==0}">
			<button type="button" name="fmhqFormButton" id="fmhqFormButton" onclick="getFMHQForm('','','<s:property value="%{fmhqversion}"/>','','','0')"><s:text name="garuda.label.forms.addfmhq"/></button>
		</s:if>
		<s:else>
			<s:if test="%{#fmhqversionflag!=fmhqversion}">
				<button type="button" name="fmhqFormButton" id="fmhqFormButton" onclick="getFMHQForm('','','<s:property value="%{fmhqversion}"/>','','','0')"><s:text name="garuda.label.forms.addfmhq"/></button>
			</s:if>
		</s:else>
	</s:else>
	</s:if>
</td></tr>
</s:if>
<s:else>
<tr class="formsClass"><td colspan="3">
	<s:if test="%{fmhqformversionlst.size()==0}">
		<button type="button" onclick="getDynameicForms('FMHQ','<s:property value="%{fmhqversion}"/>');" name="fmhqFormButton" id="fmhqFormButton"><s:text name="garuda.label.forms.addfmhq" /></button>
		<span class="error" id="fmhqFormMan" style="display:none"><s:text name="garuda.cordentry.label.fmhq"/>&nbsp;<s:text name="garuda.label.forms.validationmsg" /></span>
	</s:if>
	<s:else>
		<s:if test="%{#fmhqversionflag!=fmhqversion}">
			<button type="button" onclick="getDynameicForms('FMHQ','<s:property value="%{fmhqversion}"/>');" name="fmhqFormButton" id="fmhqFormButton"><s:text name="garuda.label.forms.addfmhq" /></button>
			<span class="error" id="fmhqFormMan" style="display:none"><s:text name="garuda.cordentry.label.fmhq"/>&nbsp;<s:text name="garuda.label.forms.validationmsg" /></span>
		</s:if>
	</s:else>
</td></tr>
</s:else>
</tbody>
	</table>
</s:div>