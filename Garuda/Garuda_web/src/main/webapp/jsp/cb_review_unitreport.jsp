<%@ taglib prefix="s"  uri="/struts-tags"%>
<script type="text/javascript">
function compareTwoValue(val1,val2,imag1){
	
	//alert($j("#"+val1).val());
	var s = $j("#"+val1).val();
	s=$j.trim(s);
	var t= $j("#"+val2).val()
	t=$j.trim(t);
	//alert(s+"  "+t);
	if(s==t){
		if(s=="" && t==""){
			document.getElementById(imag1).style.display = "none";
			}
		else{
			document.getElementById(imag1).style.display = "block";
		}
	}else{
		document.getElementById(imag1).style.display = "none";
		}
}


function updateReviewUnitReportHome(){
if($j("#reviewerForm").valid()){
$j.ajax({
			type : "POST",
			async : false,
			url : 'updateReview',
			data : $j("#reviewerForm").serialize(),
			success : function(result) {
				$j('#main').html(result);
				alert("<s:text name="garuda.unitreport.review.updateAlert"/>");
			}
			
		});
}
//		loadpage('updateReview');

}
$j(function(){
	$j("#reviewerForm").validate({
		rules:{			
			"cbuUnitReportTempPojo1.cordViabPostProcess":{
				required : true,
				range : [1, 100.01]
				},
			"cbuUnitReportTempPojo1.cordCompletedBy":{ required:true },
			"cbuUnitReportTempPojo2.cordCompletedBy":{ required:true }	
			},
			
			messages:{
				"cbuUnitReportTempPojo1.cordViabPostProcess" : {
					range : "<s:text name="garuda.common.validation.range"/>"
					}
			}
		});
	});

//--- Compare   Radio ----

function compare2radios(values,img,option1,option2){
	$j('#'+option1).val(values);
	if($j('#'+option1).val() == $j('#'+option2).val()){
		document.getElementById(img).style.display = "block";
		}
	else
		document.getElementById(img).style.display = "none";
}

function compare2radios1(values,img,option1,option2){
	$j('#'+option2).val(values);
	if($j('#'+option1).val() == $j('#'+option2).val()){
		document.getElementById(img).style.display = "block";
		}
	else
		document.getElementById(img).style.display = "none";
}



$j(document).ready(function(){

	document.getElementById('user').value=username;
	

});

function showAddWidget(url){
	 url = url + "?pageId=15" ; 
	 showAddWidgetModal(url);
}
</script>
<style type="text/css">

.tblvarn {
	/*background-color: #f85d5d;*/	
	color: #FF0000;
	font-weight:bold;	
	height: auto;
	border-bottom: 1px solid #cccccc;
}

.tbleql {	
	height: auto;
	border-bottom: 1px solid #cccccc;
}

.txtcolor, textfield, .txtcolor select {
	color: #FF0000;
}

</style>
<div class="col_100 maincontainer ">
<s:form id="reviewerForm" name="reviewerForm">
<div class="portlet" id="revprodinfodiv">

	<div id="revprodinfo" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div  class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
		<span class="ui-icon ui-icon-minusthick">
		</span><!--<span class="ui-icon ui-icon-close" ></span>-->
		<s:text
		name="garuda.unitreport.label.prodinfo.subheading" /></div>
	<div class="portlet-content">
	<div id="productinfo">
	<s:hidden id="pkCordExtInfoTemp1"
		name="cbuUnitReportTempPojo1.pkCordExtInfoTemp" /> <s:hidden
		id="pkCordExtInfoTemp2"
		name="cbuUnitReportTempPojo2.pkCordExtInfoTemp" />
		
		<s:hidden id="fkCordExtInfo"
		name="cbuUnitReportTempPojo1.fkCordExtInfo" /> 
		
		<s:hidden id="fkCordHlaExt"
		name="cordHlaExtTempPojo1.fkCordHlaExt" /> 
		
		<s:hidden name="username" id="user"></s:hidden>
		<s:hidden name="cordHlaExtTempPojo1.cordHlaTwice" id="cordhlapojo1"></s:hidden>
		<s:hidden name="cordHlaExtTempPojo2.cordHlaTwice" id="cordhlapojo2"></s:hidden>
	<table border="0" cellpadding="0" cellspacing="0" width="100%"
		class="col_100 ">
		<tbody>
			<tr>
				<th  width="70%"><s:text
					name="garuda.unitreport.label.prodinfo.subheading" /></th>
				<th></th>
				<th  width="15%"><s:text name="garuda.unitreport.review.entry1"/></th>
				<th  width="15%"><s:text name="garuda.unitreport.review.entry2"/></th>
			</tr>
			<s:if
				test="cbuUnitReportTempPojo1.cordViabPostProcess!=cbuUnitReportTempPojo2.cordViabPostProcess">
				<tr>
					<td class=" tblvarn"><s:text 
						name="garuda.unitreport.label.cellviability" /></td>
					<td class="tblvarn"><img  id="viab" src="images/tick_mark.gif" style="display :none"></td>
					<td class=" tblvarn"><s:textfield 
						name="cbuUnitReportTempPojo1.cordViabPostProcess" id="entry1CordViabPostProcess" cssClass="txtcolor" onblur="compareTwoValue('entry1CordViabPostProcess','entry2CordViabPostProcess','viab')" cssStyle="txtcolor"/></td>
					<td class=" tblvarn"><s:textfield 
						name="cbuUnitReportTempPojo2.cordViabPostProcess" id="entry2CordViabPostProcess" cssClass="txtcolor" cssStyle="txtcolor" onblur="compareTwoValue('entry1CordViabPostProcess','entry2CordViabPostProcess','viab')" /></td>
				</tr>
			</s:if>
			<s:else>
				<tr>
					<td class=" tbleql"><s:text
						name="garuda.unitreport.label.cellviability" /></td><td class="tblvarn"><img  id="viab" src="images/tick_mark.gif" style="display :none"></td>
						<td class=" tbleql" ><s:textfield
						name="cbuUnitReportTempPojo1.cordViabPostProcess" id="entry1CordViabPostProcess"  onblur="compareTwoValue('entry1CordViabPostProcess','entry2CordViabPostProcess','viab')" /></td>
					<td class=" tbleql" ><s:textfield
						name="cbuUnitReportTempPojo2.cordViabPostProcess" id="entry2CordViabPostProcess"  onblur="compareTwoValue('entry1CordViabPostProcess','entry2CordViabPostProcess','viab')" /></td>
				</tr>
			</s:else>

			<s:if
				test="cbuUnitReportTempPojo1.cordBacterialCul!=cbuUnitReportTempPojo2.cordBacterialCul">
				<tr>
					<td class=" tblvarn"><s:text
						name="garuda.unitreport.label.bacterialculture" /></td>
					<td class="tblvarn"><img  id="bact" src="images/tick_mark.gif" style="display :none"></td>
					<td class=" tblvarn" style="height: 20px;"><s:select id="entry1bactCulture"
						name="cbuUnitReportTempPojo1.cordBacterialCul"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS]" listKey="pkCodeId" listValue="description"
						headerKey=""  cssClass="txtcolor" cssStyle="txtcolor" onblur="compareTwoValue('entry1bactCulture','entry2bactCulture','bact')"/>
					<td class=" tblvarn"><s:select id="entry2bactCulture"
						name="cbuUnitReportTempPojo2.cordBacterialCul"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS]" listKey="pkCodeId" listValue="description"
						headerKey="" cssClass="txtcolor" cssStyle="txtcolor" onblur="compareTwoValue('entry1bactCulture','entry2bactCulture','bact')"/></td>
				</tr>
			</s:if>
			<s:else>
				<tr>
					<td class=" tbleql"><s:text
						name="garuda.unitreport.label.bacterialculture" /></td>
					<td class="tblvarn"><img  id="bact" src="images/tick_mark.gif" style="display :none"></td>
					<td class=" tbleql" ><s:select id="entry1bactCulture"
						name="cbuUnitReportTempPojo1.cordBacterialCul"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS]" listKey="pkCodeId" listValue="description"
						headerKey=""  onblur="compareTwoValue('entry1bactCulture','entry2bactCulture','bact')" /></td>
					<td class=" tbleql" ><s:select id="entry2bactCulture"
						name="cbuUnitReportTempPojo2.cordBacterialCul"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS]" listKey="pkCodeId" listValue="description"
						headerKey=""  onblur="compareTwoValue('entry1bactCulture','entry2bactCulture','bact')" /></td>
				</tr>
			</s:else>


			<s:if
				test="cbuUnitReportTempPojo1.cordFungalCul!=cbuUnitReportTempPojo2.cordFungalCul">
				<tr>
					<td class=" tblvarn"><s:text
						name="garuda.unitreport.label.fungalculture" /></td>
					<td class="tblvarn"><img  id="fungal" src="images/tick_mark.gif" style="display :none"></td>
					<td class=" tblvarn" ><s:select id="entry1fungalCulture"
						name="cbuUnitReportTempPojo1.cordFungalCul" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS]"
						listKey="pkCodeId" listValue="description" headerKey="" cssClass="txtcolor" cssStyle="txtcolor"
						onblur="compareTwoValue('entry1fungalCulture','entry2fungalCulture','fungal')" /> </td>
					<td class=" tblvarn" ><s:select id="entry2fungalCulture"
						name="cbuUnitReportTempPojo2.cordFungalCul" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS]"
						listKey="pkCodeId" listValue="description" headerKey="" cssClass="txtcolor" cssStyle="txtcolor"
						onblur="compareTwoValue('entry1fungalCulture','entry2fungalCulture','fungal')" /> </td>
				</tr>
			</s:if>
			<s:else>
				<td class=" tbleql"><s:text
					name="garuda.unitreport.label.fungalculture" /></td>
				<td class="tblvarn"><img  id="fungal" src="images/tick_mark.gif" style="display :none"></td>
				<td class=" tbleql" ><s:select id="entry1fungalCulture"
					name="cbuUnitReportTempPojo1.cordFungalCul" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS]"
					listKey="pkCodeId" listValue="description" headerKey="" 
					onblur="compareTwoValue('entry1fungalCulture','entry2fungalCulture','fungal')"
					 /></td>
				<td class=" tbleql" ><s:select id="entry2fungalCulture"
					name="cbuUnitReportTempPojo2.cordFungalCul" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS]"
					listKey="pkCodeId" listValue="description" headerKey=""
					onblur="compareTwoValue('entry1fungalCulture','entry2fungalCulture','fungal')"
					 /></td>
			</s:else>
			<s:if
				test="cbuUnitReportTempPojo1.cordHemogScreen!=cbuUnitReportTempPojo2.cordHemogScreen">
				<tr>
					<td class=" tblvarn"><s:text
						name="garuda.unitreport.label.hemoglobinopathyscr" /></td>
					<td class="tblvarn"><img id="hemo" src="images/tick_mark.gif" style="display :none"></td>
					<td class=" tblvarn" ><s:select id="hemo1"
						name="cbuUnitReportTempPojo1.cordHemogScreen"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN]" listKey="pkCodeId" listValue="description"
						headerKey="" cssClass="txtcolor" cssStyle="txtcolor"
						onblur="compareTwoValue('hemo1','hemo2','hemo')" /></td>
					<td class=" tblvarn" ><s:select id="hemo2"
						name="cbuUnitReportTempPojo2.cordHemogScreen"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN]" listKey="pkCodeId" listValue="description"
						headerKey=""  cssClass="txtcolor" cssStyle="txtcolor" 
							onblur="compareTwoValue('hemo1','hemo2','hemo')" /></td>
				</tr>
			</s:if>
			<s:else>
				<tr>
					<td class=" tbleql"><s:text
						name="garuda.unitreport.label.hemoglobinopathyscr" /></td>
					<td class="tblvarn"><img id="hemo" src="images/tick_mark.gif" style="display :none"></td>
					<td class=" tbleql" ><s:select id="hemo1"
						name="cbuUnitReportTempPojo1.cordHemogScreen"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN]" listKey="pkCodeId" listValue="description"
						headerKey="" 	onblur="compareTwoValue('hemo1','hemo2','hemo')" />  </td>
					<td class=" tbleql" ><s:select id="hemo2"
						name="cbuUnitReportTempPojo2.cordHemogScreen"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@HEMO_PATHYSCREEN]" listKey="pkCodeId" listValue="description"
						headerKey=""  	onblur="compareTwoValue('hemo1','hemo2','hemo')" /></td>
				</tr>

			</s:else>
		</tbody>
	</table>
	</div>
	</div>
	</div></div>
	<div class="portlet" id="revhlainfodiv">
<div id="revhlainfo"
		class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">	
	<div 
		class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span>
				<!--<span class="ui-icon ui-icon-close" ></span>--><s:text
		name="garuda.unitreport.label.hlainfo.subheading" /></div>
	<div class="portlet-content">
	<div id="hlainfo">
	<table border="0" cellpadding="0" cellspacing="0" width="100%"
		class="col_100 ">
		<tbody>
			<tr>
				<th  width="70%"><s:text
					name="garuda.unitreport.label.hlainfo.subheading" /></th>
				<th></th>
				<th  width="15%"><s:text name="garuda.unitreport.review.entry1"/></th>
				<th  width="15%"><s:text name="garuda.unitreport.review.entry2"/></th>
			</tr>
          <s:hidden name="cordHlaExtTempPojo1.cordHlaTwice" id="hla1"/>
          <s:hidden name="cordHlaExtTempPojo2.cordHlaTwice" id="hla2"/>
			<s:if
				test="cordHlaExtTempPojo1.cordHlaTwice!=cordHlaExtTempPojo2.cordHlaTwice">
				<tr>
					<td class=" tblvarn">
					
					<s:text
						name="garuda.unitreport.question.hlatype" /></td>
					<td class="tblvarn"><img  id="hlatype" src="images/tick_mark.gif" style="display :none"></td>
					
					<td class=" tblvarn" align="center">
					
					<s:radio name="cordHlaExtTempPojo1.cordHlaTwice" list="#{'1':'Yes','0':'No'}" 
					id="hlatype1"  onclick="compare2radios(this.value,'hlatype','hla1','hla2')" />
					</td>
					<td class=" tblvarn" align="center">
					<s:radio name="cordHlaExtTempPojo2.cordHlaTwice" list="#{'1':'Yes','0':'No'}"
					id="hlatype2" onclick="compare2radios1(this.value,'hlatype','hla1','hla2')" />
					</td>
				</tr>
			</s:if>
			<s:else>
			<tr>
				<td class=" tbleql"><s:text name="garuda.unitreport.question.hlatype" /></td>
				<td class="tblvarn"><img  id="hlatype" src="images/tick_mark.gif" style="display :none"></td>
				<td class=" tbleql" align="center"><s:radio name="cordHlaExtTempPojo1.cordHlaTwice" list="#{'1':'Yes','0':'No'}" 
				id="hlatype1" onclick="compare2radios(this.value,'hlatype','hla1','hla2')"  /></td>
				<td class=" tbleql" align="center"><s:radio name="cordHlaExtTempPojo2.cordHlaTwice" list="#{'1':'Yes','0':'No'}" 
				id="hlatype2" onclick="compare2radios1(this.value,'hlatype','hla1','hla2')" /></td>
				</tr>
			</s:else>
			 <s:hidden name="cordHlaExtTempPojo1.cordHlaContiSeg" id="cont1"/>
          <s:hidden name="cordHlaExtTempPojo2.cordHlaContiSeg" id="cont2"/>
			<s:if
				test="cordHlaExtTempPojo1.cordHlaContiSeg!=cordHlaExtTempPojo2.cordHlaContiSeg">
				<tr>
					<td class=" tblvarn"><s:text
						name="garuda.unitreport.question.hlacontguoussegment" /></td>
					<td class="tblvarn"><img  id="hlacon" src="images/tick_mark.gif" style="display :none"></td>
					<td class=" tblvarn" align="center">
					<s:radio id="hlacont1" name="cordHlaExtTempPojo1.cordHlaContiSeg" list="#{'1':'Yes','0':'No'}" onclick="compare2radios(this.value,'hlacon','cont1','cont2')"/>
					</td>
					<td class=" tblvarn" align="center"><s:radio id="hlacont2" name="cordHlaExtTempPojo2.cordHlaContiSeg" list="#{'1':'Yes','0':'No'}" onclick="compare2radios1(this.value,'hlacon','cont1','cont2')"/></td>
				</tr>
			</s:if>
			<s:else>
			<tr>
				<td class=" tbleql"><s:text
					name="garuda.unitreport.question.hlacontguoussegment" /></td>
				<td class="tblvarn"><img  id="hlacon" src="images/tick_mark.gif" style="display :none"></td>
				<td class=" tbleql" align="center"><s:radio id="hlacont1" name="cordHlaExtTempPojo1.cordHlaContiSeg" list="#{'1':'Yes','0':'No'}" onclick="compare2radios(this.value,'hlacon','cont1','cont2')" /></td>
				<td class=" tbleql" align="center"><s:radio id="hlacont2" name="cordHlaExtTempPojo2.cordHlaContiSeg" list="#{'1':'Yes','0':'No'}" onclick="compare2radios1(this.value,'hlacon','cont1','cont2')" /></td>
				</tr>
			</s:else>
			
			<s:hidden name="cordHlaExtTempPojo1.cordHlaContiSegBefRel" id="befrel1"/>
          <s:hidden name="cordHlaExtTempPojo2.cordHlaContiSegBefRel" id="befrel2"/>
			<s:if
				test="cordHlaExtTempPojo1.cordHlaContiSegBefRel!=cordHlaExtTempPojo2.cordHlaContiSegBefRel">
				<tr>
					<td class=" tblvarn"><s:text
						name="garuda.unitreport.question.hlarelease" /></td>
					<td class="tblvarn"><img  id="hlabef" src="images/tick_mark.gif" style="display :none"></td>
					<td class=" tblvarn" align="center">
					<s:radio id="hlabef1" name="cordHlaExtTempPojo1.cordHlaContiSegBefRel" list="#{'1':'Yes','0':'No'}" onclick="compare2radios(this.value,'hlabef','befrel1','befrel2')"  />
					</td>
					<td class=" tblvarn" align="center">
					<s:radio id="hlabef2" name="cordHlaExtTempPojo2.cordHlaContiSegBefRel" list="#{'1':'Yes','0':'No'}" onclick="compare2radios1(this.value,'hlabef','befrel1','befrel2')" />
					</td>
				</tr>
			</s:if>
			<s:else>
			<tr>
				<td class=" tbleql"><s:text name="garuda.unitreport.question.hlarelease" /></td>
				<td class="tblvarn"><img  id="hlabef" src="images/tick_mark.gif" style="display :none"></td>
				<td class=" tbleql" align="center">
				<s:radio id="hlabef1" name="cordHlaExtTempPojo1.cordHlaContiSegBefRel"  list="#{'1':'Yes','0':'No'}" onclick="compare2radios(this.value,'hlabef','befrel1','befrel2')" />
				</td>
				<td class=" tbleql" align="center">
				<s:radio id="hlabef2" name="cordHlaExtTempPojo2.cordHlaContiSegBefRel"  list="#{'1':'Yes','0':'No'}" onclick="compare2radios1(this.value,'hlabef','befrel1','befrel2')"/>
				</td>
					</tr>
			</s:else>
			
			<s:hidden name="cordHlaExtTempPojo1.cordHlaIndBefRel" id="indbefrel1"/>
          <s:hidden name="cordHlaExtTempPojo2.cordHlaIndBefRel" id="indbefrel2"/>
			<s:if
				test="cordHlaExtTempPojo1.cordHlaIndBefRel!=cordHlaExtTempPojo2.cordHlaIndBefRel">
				<tr>
					<td class=" tblvarn"><s:text
						name="garuda.unitreport.question.hlabeforerelease" /></td>
					<td class="tblvarn"><img  id="hlaindbef" src="images/tick_mark.gif" style="display :none"></td>
					<td class=" tblvarn" align="center">
					<s:radio id="hlaindbef1" name="cordHlaExtTempPojo1.cordHlaIndBefRel" list="#{'1':'Yes','0':'No'}"  onclick="compare2radios(this.value,'hlaindbef','indbefrel1','indbefrel2')" />
					</td>
					<td class=" tblvarn" align="center">
					<s:radio id="hlaindbef1" name="cordHlaExtTempPojo2.cordHlaIndBefRel" list="#{'1':'Yes','0':'No'}"  onclick="compare2radios1(this.value,'hlaindbef','indbefrel1','indbefrel2')"   />
					</td>
				</tr>
			</s:if>
			<s:else>
			<tr>
				<td class=" tbleql"><s:text name="garuda.unitreport.question.hlabeforerelease" /></td>
				<td class="tblvarn"><img  id="hlaindbef" src="images/tick_mark.gif" style="display :none"></td>
				<td class=" tbleql" align="center">
				<s:radio id="hlaindbef1" name="cordHlaExtTempPojo1.cordHlaIndBefRel" list="#{'1':'Yes','0':'No'}"  onclick="compare2radios(this.value,'hlaindbef','indbefrel1','indbefrel2')"  />
				</td>
				<td class=" tbleql" align="center">
				<s:radio id="hlaindbef2" name="cordHlaExtTempPojo2.cordHlaIndBefRel" list="#{'1':'Yes','0':'No'}"  onclick="compare2radios1(this.value,'hlaindbef','indbefrel1','indbefrel2')"  />
				</td>
				</tr>
			</s:else>
			<s:if
				test="cordHlaExtTempPojo1.cordHlaContiSegNum!=cordHlaExtTempPojo2.cordHlaContiSegNum">
				<tr>
					<td class=" tblvarn"><s:text
						name="garuda.unitreport.question.unitrequestedshipment" /></td>
					<td class="tblvarn"><img  id="unitentry" src="images/tick_mark.gif" style="display :none"></td>
					<td class=" tblvarn"><s:select name="cordHlaExtTempPojo1.cordHlaContiSegNum" id="unitentry1"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NUM_OF_SEGMENTS]" listKey="pkCodeId" listValue="description"
						headerKey=""  cssClass="txtcolor" cssStyle="txtcolor" onblur="compareTwoValue('unitentry1','unitentry2','unitentry')"/></td>
					<td class=" tblvarn">
					<s:select name="cordHlaExtTempPojo2.cordHlaContiSegNum" id="unitentry2"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NUM_OF_SEGMENTS]" listKey="pkCodeId" listValue="description"
						headerKey=""  cssClass="txtcolor" cssStyle="txtcolor" onblur="compareTwoValue('unitentry1','unitentry2','unitentry')"/></td>
				</tr>
			</s:if>
			<s:else>
			<tr>
				<td class=" tbleql"><s:text
					name="garuda.unitreport.question.unitrequestedshipment" /></td>
				<td class="tblvarn"><img  id="unitentry" src="images/tick_mark.gif" style="display :none"></td>
				<td class=" tbleql"><s:select name="cordHlaExtTempPojo1.cordHlaContiSegNum" id="unitentry1"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NUM_OF_SEGMENTS]" listKey="pkCodeId" listValue="description"
						headerKey="" onblur="compareTwoValue('unitentry1','unitentry2','unitentry')"/></td>
				<td class=" tbleql"><s:select name="cordHlaExtTempPojo2.cordHlaContiSegNum" id="unitentry2"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NUM_OF_SEGMENTS]" listKey="pkCodeId" listValue="description"
						headerKey=""  onblur="compareTwoValue('unitentry1','unitentry2','unitentry')"/></td>
				</tr>
			</s:else>
		</tbody>
	</table>
	</div>
	</div>
	</div></div>
	
	<div class="portlet" id="revprocinfodiv">
	<div id="revprocinfo"
		class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div 
		class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span>
				<!--<span class="ui-icon ui-icon-close" ></span>--><s:text
		name="garuda.unitreport.label.processinginfo.subheading" /></div>
	<div class="portlet-content">
	<div id="productinfo">
	<table border="0" cellpadding="0" cellspacing="2" width="100%"
		class="col_100 ">
		<tbody>
			<tr>
				<th  width="70%"><s:text
					name="garuda.unitreport.label.processinginfo.subheading" /></th>
				<th></th>
				<th  width="15%"><s:text name="garuda.unitreport.review.entry1"/></th>
				<th  width="15%"><s:text name="garuda.unitreport.review.entry2"/></th>
			</tr>


			<s:if
				test="cbuUnitReportTempPojo1.cordProcMethod!=cbuUnitReportTempPojo2.cordProcMethod">
				<tr>
					<td class=" tblvarn"><s:text
						name="garuda.unitreport.label.processingmethod" /></td>
					<td class="tblvarn"><img  id="processentry" src="images/tick_mark.gif" style="display :none"></td>
					<td class=" tblvarn" ><s:select
						name="cbuUnitReportTempPojo1.cordProcMethod" id="processenrty1"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_METHOD]" listKey="pkCodeId"
						listValue="description" headerKey="" cssClass="txtcolor" cssStyle="txtcolor"
						onblur="compareTwoValue('processenrty1','processenrty2','processentry')"/>
					<td class=" tblvarn" ><s:select
						name="cbuUnitReportTempPojo2.cordProcMethod" id="processenrty2"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_METHOD]" listKey="pkCodeId"
						listValue="description" headerKey="" cssClass="txtcolor" cssStyle="txtcolor"
						 onblur="compareTwoValue('processenrty1','processenrty2','processentry')"/></td>
				</tr>
			</s:if>
			<s:else>
				<tr>
					<td class=" tbleql"><s:text
						name="garuda.unitreport.label.processingmethod" /></td>
					<td class="tblvarn"><img  id="processentry" src="images/tick_mark.gif" style="display :none"></td>
					<td class=" tbleql" ><s:select
						name="cbuUnitReportTempPojo1.cordProcMethod" id="processenrty1"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_METHOD]" listKey="pkCodeId"
						listValue="description" headerKey=""
						 onblur="compareTwoValue('processenrty1','processenrty2','processentry')"/></td>
					<td class=" tbleql" ><s:select
						name="cbuUnitReportTempPojo2.cordProcMethod" id="processenrty2"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PROCESSING_METHOD]" listKey="pkCodeId"
						listValue="description" headerKey=""
						onblur="compareTwoValue('processenrty1','processenrty2','processentry')"/></td>
				</tr>

			</s:else>

			<s:if
				test="cbuUnitReportTempPojo1.cordTypeBag!=cbuUnitReportTempPojo2.cordTypeBag">

				<tr>
					<td class=" tblvarn"><s:text
						name="garuda.unitreport.label.typeofbagused" /></td>
					<td class="tblvarn"><img  id="bagentry" src="images/tick_mark.gif" style="display :none"></td>
					<td class=" tblvarn" ><s:select
						name="cbuUnitReportTempPojo1.cordTypeBag" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TYPE_OF_BAG]" id="bagentry1"
						listKey="pkCodeId" listValue="description" headerKey="" cssClass="txtcolor" cssStyle="txtcolor"
						 onblur="compareTwoValue('bagentry1','bagentry2','bagentry')"/></td>
					<td class=" tblvarn" ><s:select
						name="cbuUnitReportTempPojo2.cordTypeBag" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TYPE_OF_BAG]"  id="bagentry2"
						listKey="pkCodeId" listValue="description" headerKey="" cssClass="txtcolor" cssStyle="txtcolor"
						 onblur="compareTwoValue('bagentry1','bagentry2','bagentry')"/></td>
				</tr>
			</s:if>
			<s:else>
				<tr>
					<td class=" tbleql"><s:text
						name="garuda.unitreport.label.typeofbagused" /></td>
					<td class="tblvarn"><img src="images/tick_mark.gif" id="bagentry" style="display :none"></td>
					<td class=" tbleql" ><s:select
						name="cbuUnitReportTempPojo1.cordTypeBag" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TYPE_OF_BAG]" id="bagentry1"
						listKey="pkCodeId" listValue="description" headerKey=""
						 onblur="compareTwoValue('bagentry1','bagentry2','bagentry')"/></td>
					<td class=" tbleql" ><s:select
						name="cbuUnitReportTempPojo2.cordTypeBag" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TYPE_OF_BAG]"  id="bagentry2"
						listKey="pkCodeId" listValue="description" headerKey=""
						onblur="compareTwoValue('bagentry1','bagentry2','bagentry')"/></td>
				</tr>
			</s:else>

			<s:if
				test="cbuUnitReportTempPojo1.cordProdModification!=cbuUnitReportTempPojo2.cordProdModification">
				<tr>
					<td class=" tblvarn"><s:text
						name="garuda.unitreport.label.productmodification" /></td>
					<td class="tblvarn"><img src="images/tick_mark.gif" id="productentry" style="display :none"></td>
					<td class=" tblvarn" ><s:select
						name="cbuUnitReportTempPojo1.cordProdModification" id="productentry1"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PRODUCT_MODIFICATION]" listKey="pkCodeId"
						listValue="description" headerKey="" cssClass="txtcolor" cssStyle="txtcolor"
						 onblur="compareTwoValue('productentry1','productentry2','productentry')"/></td>
					<td class=" tblvarn" ><s:select
						name="cbuUnitReportTempPojo2.cordProdModification" id="productentry2"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PRODUCT_MODIFICATION]" listKey="pkCodeId"
						listValue="description" headerKey="" cssClass="txtcolor" cssStyle="txtcolor"
							 onblur="compareTwoValue('productentry1','productentry2','productentry')" /></td>
				</tr>
			</s:if>
			<s:else>
				<tr>
					<td class=" tbleql"><s:text
						name="garuda.unitreport.label.productmodification" /></td>
					<td class="tblvarn"><img src="images/tick_mark.gif" id="productentry" style="display :none"></td>
					<td class=" tbleql" ><s:select
						name="cbuUnitReportTempPojo1.cordProdModification" id="productentry1"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PRODUCT_MODIFICATION]" listKey="pkCodeId"
						listValue="description" headerKey=""
							 onblur="compareTwoValue('productentry1','productentry2','productentry')" /></td>
					<td class=" tbleql" ><s:select
						name="cbuUnitReportTempPojo2.cordProdModification" id="productentry2"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PRODUCT_MODIFICATION]" listKey="pkCodeId"
						listValue="description" headerKey=""
						 	 onblur="compareTwoValue('productentry1','productentry2','productentry')"/></td>
				</tr>

			</s:else>
		</tbody>
	</table>
	</div>
	</div>
	</div></div>
	<div class="portlet" id="revaddinfodiv">
	<div id="revaddinfo"
		class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div 
		class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span>
				<!--<span class="ui-icon ui-icon-close" ></span>--><s:text
		name="garuda.unitreport.label.additionalinfo" /></div>
	<div class="portlet-content">
	<div id="productinfo">
	<table border="0" cellpadding="0" cellspacing="0" width="100%"
		class="col_100 ">
		<tbody>
			<tr>
				<th  width="70%"><s:text
					name="garuda.unitreport.label.additionalinfo" /></th>
					<th></th>
				<th  width="15%"><s:text name="garuda.unitreport.review.entry1"/></th>
				<th  width="15%"><s:text name="garuda.unitreport.review.entry2"/></th>
			</tr>
			<s:if
				test="cbuUnitReportTempPojo1.cordAddNotes!=cbuUnitReportTempPojo2.cordAddNotes">
				<tr>
					<td class=" tblvarn"><s:text
						name="garuda.unitreport.label.additionalnotes" /></td>
					<td class="tblvarn"><img src="images/tick_mark.gif" id="addentry" style="display :none"></td>
					<td class=" tblvarn" ><s:textfield
						name="cbuUnitReportTempPojo1.cordAddNotes" id="addentry1"
						 onblur="compareTwoValue('addentry1','addentry2','addentry')"/></td>
					<td class=" tblvarn" ><s:textfield
						name="cbuUnitReportTempPojo2.cordAddNotes" id="addentry2"
						 onblur="compareTwoValue('addentry1','addentry2','addentry')"/></td>
				</tr>
			</s:if>
			<s:else>
				<tr>
					<td class=" tbleql"><s:text
						name="garuda.unitreport.label.additionalnotes" /></td>
					<td class="tblvarn"><img src="images/tick_mark.gif" id="addentry" style="display :none"></td>
					<td class=" tbleql" ><s:textfield
						name="cbuUnitReportTempPojo1.cordAddNotes" id="addentry1"
						 onblur="compareTwoValue('addentry1','addentry2','addentry')"/></td>
					<td class=" tbleql" ><s:textfield
						name="cbuUnitReportTempPojo2.cordAddNotes" id="addentry2"
						 onblur="compareTwoValue('addentry1','addentry2','addentry')"/></td>
				</tr>
			</s:else>
			<tr>
				<td ><s:text
					name="garuda.unitreport.label.completedby" /></td>
				<td><img src="images/tick_mark.gif"  style="display :none"></td>
				<td  ><s:textfield
					name="cbuUnitReportTempPojo1.cordCompletedBy" /></td>
				<td  ><s:textfield
					name="cbuUnitReportTempPojo2.cordCompletedBy" /></td>
			</tr>
		</tbody>
	</table>
	</div>
	</div>
	</div></div>
	
	<div class="portlet" align="right">
			<button type="button" onclick="updateReviewUnitReportHome()"><s:text
		name="garuda.reviewunitreport.label.confirm" /></button>
	</div>
</s:form>
</div>