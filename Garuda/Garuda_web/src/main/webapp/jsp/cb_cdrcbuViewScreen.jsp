<%@ taglib prefix="s"  uri="/struts-tags"%>
<script>

$j(function() {
	$j( "#datepicker" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		changeYear: true});
	$j( "#datepicker1" ).datepicker({dateFormat: 'M dd, yy',changeMonth: true,
		changeYear: true});
	
});

function sum(val){
                var txt = document.getElementById('tncfrozenpatwt');
				
                txt.value = (parseFloat(txt.value) + val).toFixed(1);
               } 
        
        
	function sum1(val){
                
				var txt1 = document.getElementById('totcelfrozen');
				
                txt1.value = (parseFloat(txt1.value) + val).toFixed(1);
        }
        
        function constructTable() {

			 $j('#searchResults1').dataTable();
		  
		}
		$j(document).ready(function(){
		
			if(document.getElementById('tncfrozenpatwt').value == " " && document.getElementById('tncfrozenpatwt') == null){
			document.getElementById('tncfrozenpatwt').value = 0;
			}
			if(document.getElementById('totcelfrozen').value == " " && document.getElementById('totcelfrozen') == null){
			document.getElementById('totcelfrozen').value = 0;
			}
			

			if(document.getElementById('unLicensedPk').value == document.getElementById('cbuLicStatus').value){
				$j('#cdrcbu_cdrCbuPojo_cbuLicStatus'+$j('#unLicensedPk').val()).attr("checked","checked");
				}
				else{
				$j('#cdrcbu_cdrCbuPojo_cbuLicStatus'+$j('#cbuLicStatus').val()).attr("checked","checked");
				}	
					
		});

		function showAddWidget(url){
			 url = url + "?pageId=5" ; 
			 showAddWidgetModal(url);
		}
		
</script>

<div class="col_100 maincontainer">
<div class="col_100">
<table id="container1">
		<tr>
		<td>
		<div class="column">
<div class="portlet" id="cdrviewparent" ><div id="cdrview" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ><div class="portlet-header ui-widget-header ui-widget-content ui-corner-all"><span class="ui-icon ui-icon-minusthick"></span><span class="ui-icon ui-icon-close" ></span><s:text name="garuda.cbuentry.label.cbdrcbudataviewscreen" /></div><div class="portlet-content">
<div id="editDiv2" ><s:form id="cdrcbu" action="cbucall">
	<table class="col_100 ">
		<tr>
			<td colspan="4" align="right">
			<button type="button" onclick="javascript:submitform('cdrcbu');">New
			Search</button>
			</td>
		</tr>
		<s:hidden name="cdrCbuPojo.unLicesePkid" id="unLicensedPk"></s:hidden>
		<s:hidden name="cdrCbuPojo.cbuLicStatus" id="cbuLicStatus"></s:hidden>
		<tr>
			<td ><s:text
				name="garuda.cbuentry.label.externalcbuid" />:</td>
			<td ><s:textfield
				name="CdrCbuPojo.externalCbuId" id="externalCbuId" readonly="true" onkeydown="cancelBack();"   /></td>
			<td ><s:text
				name="garuda.cbuentry.label.clinicalstatus" />:</td>
			<td ><s:select
				name="CdrCbuPojo.clinicalStatus" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CLINICAL_STATUS]"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select" disabled="true"  /></td>
		</tr>
		<tr>
			<td><s:text name="garuda.cbuentry.label.idoncbubag" />:</td>
			<td><s:textfield name="CdrCbuPojo.numberOnCbuBag" readonly="true" onkeydown="cancelBack();"   /></td>

			<td><s:text name="garuda.cbuentry.label.gender" />:</td>
			<td><s:select name="CdrCbuPojo.babyGenderId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BABY_GENDER]"
				listKey="pkCodeId" listValue="description" headerKey=""
				headerValue="Select" disabled="true"  /></td>
		</tr>
		<tr>
			<td><s:text name="garuda.cbuentry.label.birthdate" />:</td>
			<td>
			<s:date name="CdrCbuPojo.birthDate" id="datepicker" format="MMM dd, yyyy" />						
                      <s:textfield class="txtarea75" name="CdrCbuPojo.birthDate" placeholder="Date" id="datepicker" readonly="true" onkeydown="cancelBack();"  disabled="true" value="%{datepicker}"/>
				</td>
			

			<td><s:text name="garuda.cbuentry.label.cbulicensure" />:</td>
			<td><s:radio list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS]" name="cdrCbuPojo.cbuLicStatus" listKey="pkCodeId" listValue="description" disabled="true" ></s:radio></td>
		</tr>


		<tr>
			<td><s:text name="garuda.cbuentry.label.collectiondate" />:</td>
			<td>				
				<s:date name="CdrCbuPojo.cbuCollectionDate" id="datepicker1" format="MMM dd, yyyy" />						
                      <s:textfield class="txtarea75" name="CdrCbuPojo.specimen.specCollDate" placeholder="Date" id="datepicker1" readonly="true" onkeydown="cancelBack();"  disabled="true" value="%{datepicker1}"/>
				</td>

			<td><s:text name="garuda.cbuentry.label.tncfrozen" />:</td>
			<td><s:textfield name="CdrCbuPojo.tncFrozen" id="tncfrozen" readonly="true" onkeydown="cancelBack();"   />
			</td>
		</tr>
		<tr>
			<td ><s:text name="garuda.cbuentry.label.tncfrozenpatwt" />:</td>
			<td><s:textfield name="CdrCbuPojo.frozenPatWt" id="tncfrozenpatwt" maxlength="5" size="5" readonly="true" onkeydown="cancelBack();"  /></td>							
			<td ><s:text name="garuda.cbuentry.label.totcelfrozen" />:</td>
			<td><s:textfield  name="CdrCbuPojo.cellCount" id="totcelfrozen" maxlength="5" size="5" readonly="true" onkeydown="cancelBack();"   /></td>			
		</tr>
		<tr>
			<td ><s:text name="garuda.cbuentry.label.abo" />:</td>
			<td >
			<s:textfield name="CdrCbuPojo.bloodGpDes" readonly="true" onkeydown="cancelBack();"   />
			</td>
			<td ><s:text	name="garuda.cbuentry.label.rhtype" />:</td>
			<td >
			<s:textfield name="CdrCbuPojo.rhTypeDes" readonly="true" onkeydown="cancelBack();"   />
			</td>
		</tr>
		<tr>
					<td  valign="middle"><s:text name="garuda.cbuentry.label.bcresult" />:</td>
					<td ><s:select
						name="CdrCbuPojo.bacterialResult" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@BACTERIAL_STATUS]"
						listKey="pkCodeId" listValue="description" headerKey=""
						headerValue="Select" disabled="true"  /></td>
				    <td  valign="middle"><s:text
						name="garuda.cbuentry.label.fcresult" />:</td>
					<td ><s:select name="CdrCbuPojo.fungalResult"
						list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@FUNGAL_STATUS]" listKey="pkCodeId" listValue="description"
						headerKey="" headerValue="Select" disabled="true"  /></td>
				</tr>
			</table></s:form>
</div></div></div></div>

</div></td></tr></table>

<table id="container2">
		<tr>
		<td>
<div class="column">
<div class="portlet" id="hlasearchparent" >
<div id="hlasearch" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
<span class="ui-icon ui-icon-minusthick"></span>
<span class="ui-icon ui-icon-close" ></span>
<s:text name="garuda.cbuentry.label.hlaInformation"/></div>
<div class="portlet-content">
			<div id="searchTble">
			<table  align="center" cellpadding="0"	cellspacing="0" class="col_100 " id="searchResults">
				<thead>
					<tr>
						<th><s:text name="garuda.cbuentry.label.hlaLocus" /></th>
						<th><s:text name="garuda.cbuentry.label.method" /></th>
						<th><s:text name="garuda.cbuentry.label.type1" /></th>
						<th><s:text name="garuda.cbuentry.label.type2" /></th>
					</tr>
				</thead>
				<tbody>


						<%String oddEven=""; %>
					<s:iterator value="lstHla" var="rowVal" status="row" >
					<% oddEven = (oddEven == "even") ? "odd" : "even"; %>
					
						<tr class=<%=oddEven %> >
							<s:iterator value="rowVal" var="cellValue" status="col">
								<s:if test="%{#col.index == 0}">
									<td><s:property /></td>
								</s:if>
								<s:if test="%{#col.index == 1}">
									<td><s:property /></td>
								</s:if>
								<s:if test="%{#col.index == 2}">
									<td><s:property /></td>
								</s:if>
								<s:if test="%{#col.index == 3}">
									<td><s:property /></td>
								</s:if>
							</s:iterator>
						</tr>
					</s:iterator>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4"></td>
					</tr>
				</tfoot>
			</table>
			</div>

		</div>
	</div>	

</div>

</div>
</td></tr></table>

</div>
</div>