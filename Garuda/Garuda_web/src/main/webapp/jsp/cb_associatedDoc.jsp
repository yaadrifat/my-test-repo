<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<jsp:include page="cb_track_session_logging.jsp" />
<%String contextpath = request.getContextPath()+"/jsp/modal/"; %>
<script>
$j(function(){	
	 $j("#uploadDocs").validate({		 
              errorPlacement: function(error, element) {               
            } 
    });
	 jQuery.validator.addClassRules('finalReviewDocs', {
	
         selectDoc: {
             depends: function(element) {            
                 return true;
             }
         }
	  });	  
	 jQuery.validator.addMethod("selectDoc", function (value, element, params) {		
			if($j('.finalReviewDocs:checked').length > 0){					
				      $j("#errorHolder").css('display','none');				      
	                  return true;
			}
			else{			
				  $j("#errorHolder").css('display','block');
			      return false;
			}
			}, "Please Select Value");	
});

 function submitData(){
	 if($j("#uploadDocs").valid()){		 
		  jQuery.ajax({
				  url: 'finalreviewassociatedDocuments',
				  type:'GET',
				  async:false,
				  data:'pkCordFinalReview=<s:property value="cbuFinalReviewPojo.pkCordFinalReview"/>&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&'+$j(".finalReviewDocs").serialize(),
				  success : function(result, status, error) {
						
						FinRevAssoDocsFlag.statusFlag=result.finReviewAssoDocsFlag;
						
						showSuccessdiv('updateAssociatedoc','searchresultparent');

						closeModal();
						SelectDocPopup.checkPopupStatus('finalCBUReview');
						
					},
					error:function() { 
						alert("Error \n\n"+error);
					}
		  });
 }
 }

 $j(function(){
	 $j('#finalRevDocId').dataTable({
			"oLanguage": {
				"sEmptyTable": "<s:text name="garuda.cbu.uploaddocument.norecord"/>"
			},
	   "bRetrieve": true
		});	      
	 });
	 
	 $j(function(){

		   $j('#rowuments').find(".portlet-header .ui-icon").bind("click",function() {
			   if($j(this).is(".ui-icon-minusthick") || $j(this).is(".ui-icon-plusthick"))
				{
					$j(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
					
					$j(this).parents(".portlet:first").find(".portlet-content").toggle();
					
					var divid = $j(this).parent().parent().attr("id");
					if(divid !="undefined" || divid !=null)
					divHeight=document.getElementById(divid).style.height;
					if(divHeight=='100%')
					{
						divHeight = 'auto';
						}	
				    $j(this).parents(".portlet:first").css('height',divHeight);
					
				}
			});
		 });


	 function closeModal(){
		 $j("#modelPopup2").dialog("close");
		 $j("#modelPopup2").dialog("destroy");
	}

	 function showAssociatedDoc(AttachmentId){
		
		//var signed='true';
		var encrypted='false';
		var url='getAttachmentFromDcms?docId='+AttachmentId+'&encrypted='+encrypted;
		window.open( url, "Attachment", "status = 1, height = 500, width = 800, resizable = 0" );
	} 
		
</script>
<style>
body {
    font-family: Verdana,Arial,sans-serif;
    font-size: 10px;
}

p {
    margin: 1em 0;   
}

strong {
    font-weight: 900;   
}
</style>
<div class="column">
		<s:form id="uploadDocs" name="uploadDocs" method="post">
		<s:hidden name="cdrCbuPojo.cordID" id="cdrCbucordID"></s:hidden>	
		<s:hidden name="cdrCbuPojo.registryId" id="registryId"></s:hidden>
		
		<div id="updateAssociatedoc" style="display: none;" >
	     <jsp:include page="./cb_update.jsp">
	       <jsp:param value="garuda.message.modal.finalattacheddoc" name="message"/>
	     </jsp:include>	   
	    </div>	
		
		<div class="portlet" id="searchresultparent" >		   
		     <div class="ui-widget" id="errorHolder" style="display:none;height:45px;">
                       <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;height:45px"> 
                       <table style="width:100%;" >
                       <tr><td>
                       <p>
                          <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                          <s:text name="garuda.common.message.selectdoc" />
                         </p>
                           </td></tr>
		   </table>
                     </div>
              </div>
		 <div></div>
			<div id="rowuments"  class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
			<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			<s:text name="garuda.uploaddoc.label.uploadeddocument" /> <span class="ui-icon ui-icon-minusthick"></span></div>			
			<div class="portlet-content"  style="height:350px;scroll:auto;">			
			<div id="searchTble">	
			
			<table border="0" align="left" cellpadding="0" cellspacing="0" class="displaycdr" id="finalRevDocId" >
				<thead>
					<tr>						
						<th><s:text name="garuda.openorder.label.select"/></th>
						<th><s:text name="garuda.uploaddoc.label.date" /></th>
						<th><s:text name="garuda.uploaddoc.label.documentuploadedby" /></th>
						<th><s:text name="garuda.uploaddoc.label.documentcategory" /></th>
						<th><s:text name="garuda.uploaddoc.label.documentsubcategory" /></th>
						<th><s:text name="garuda.common.description" /></th>
						<th><s:text name="garuda.openorder.label.completiondate" /></th>
						<th><s:text name="garuda.uploaddoc.label.diffrntDates" /></th>
						<th><s:text name="garuda.uploaddoc.label.view" /></th>									
					</tr>
				</thead>
				<tbody>
					<s:if test="#request.categoryMap!=null">
					<s:set name="status" value="0"/>
            <s:iterator value="#request.categoryMap">
            
               <s:set name="docKey" value="key" />                
	               <s:iterator value="#request.categoryMap[#docKey]" var="row" >
	                  <s:if test="%{#row[7]=='application/pdf'}">
						<tr id="<s:property	value="%{#status}"/>">
								<td >
								 <s:checkbox name="finRevUploadInfo[%{#status}].fkCbuUploadInfo" cssClass="finalReviewDocs" fieldValue="%{#row[5]}" ></s:checkbox>
								 <s:hidden  name="finRevUploadInfo[%{#status}].attachmentType" cssClass="finalReviewDocs"  value="%{#row[9]}"/>
								</td>
								<td ><s:date name="%{#row[10]}" id="lastModifiedOn1" format="MMM dd, yyyy / hh:mm:ss a" />
									 <s:property  value="%{lastModifiedOn1}" /></td>
									  <s:set name="cellValue1" value="%{#row[14]}" scope="request"/>
									 				<td>												
													<%
													String cellValue1 = request.getAttribute("cellValue1").toString();
													UserJB userB = new UserJB();
													userB.setUserId(EJBUtil.stringToNum(cellValue1));
													userB.getUserDetails();													
													%>	
													<%=userB.getUserLastName()+" "+userB.getUserFirstName() %>	
							    </td>		
								<s:if test ="#row[15]!=null"><s:set name="cellValue4" value="%{#row[15]}" scope="request"/>
								<td><s:property  value="getCodeListDescById(#request.cellValue4)" /></td>
								</s:if><s:else><td></td> </s:else>
								<s:if test="#row[12]!=null"><s:set name="cellValue5" value="%{#row[12]}" scope="request"/>
								<td><s:property  value="getCodeListDescById(#request.cellValue5)" /></td>
								</s:if><s:else><td></td> </s:else>
								<td><s:property value="%{#row[13]}" /></td>
								<td ><s:if test="#row[20]!=null "><s:date name="%{#row[20]}" id="CompletionDate1" format="MMM dd, yyyy" /><s:property  value="%{CompletionDate1}" /></s:if></td>
								<td><s:if test="#row[16]!=null "><s:date name="%{#row[16]}" id="ReportDate" format="MMM dd, yyyy" /><s:property  value="%{ReportDate}"/></s:if>
								<s:if test="#row[17]!=null"><s:date name="%{#row[17]}" id="ProcessDate" format="MMM dd, yyyy" /><s:property  value="%{ProcessDate}"/></s:if>
									<s:if test="#row[18]!=null"><s:date name="%{#row[18]}" id="TestDate" format="MMM dd, yyyy" /><s:property  value="%{TestDate}"/></s:if>
									<s:if test="#row[19]!=null"><s:date name="%{#row[19]}" id="RecievedDate" format="MMM dd, yyyy" /><s:property  value="%{RecievedDate}"/></s:if></td>
								
								<td><a href="#"><img src="images/attachment-icon.png"
			<!%--review-->		onclick="javascript: return showAssociatedDoc('<s:property value="%{#row[5]}" />')" /></a></td>								
								</tr>
								<s:set name="status" value="%{#status+1}"/>
							</s:if>
					  </s:iterator>
                 </s:iterator>
              </s:if>
				</tbody>	
				<tfoot>
					<tr>
						<td colspan="6"></td>
					</tr>
				</tfoot>
			</table>
			</div>
			</div>
			</div>
			<table style="width: 100%;">
				<tr>
					<td >					  
					</td>
					<td  align="right"><button type="button" onclick="submitData()"><s:text
						name="garuda.unitreport.label.button.submit" /></button>
						<button type="reset" onclick="closeDialog('modelPopup2')"><s:text
						name="garuda.common.lable.cancel" /></button>						
					</td>
				</tr>
				</table>
			
		</div>
	</s:form>
</div>

