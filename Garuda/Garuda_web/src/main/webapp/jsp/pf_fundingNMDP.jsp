<%@ taglib prefix="s" uri="/struts-tags" %>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<script>
/* $j("#fundingreportscheduledtab").click(function(){
	alert("hi");
	refreshDiv('getLstFundingReportSchedule','fundingReportScheduleds','cbuFunding');
}); */
	
$j(document).ready(function(){
	var $jtabs = $j('#tabs').tabs();
	$jtabs.tabs('select', 0);
 	var url1="getCbbDetailsForFunding";
 	fundingPaginateTable();
 	fundingrptsPaginateTable();
	
});
/* function hideTabDiv(divid){
	
	var DivId=new Array();
	DivId[0]="funding-guideline";
	DivId[1]="schedule-funding-report";
	DivId[2]="funding-report-scheduled";
	DivId[2]="funding-report-his";
	$j.each(DivId,function(){
		if(this==divid){
			$j('#'+this).removeClass('ui-tabs-hide');
		}
		else{
			$j('#'+this).addClass('ui-tabs-hide');
		}
	});
	
} */

function getLstFundingReportSchedule(){
	loadPageByGetRequset('getLstFundingReportSchedule','fundingReportScheduleds');
	fundingrptsPaginateTable();
	/* hideTabDiv('funding-report-scheduled'); */
}

function getLstScheduleFundingReport(){
	loadPageByGetRequset('getLstScheduleFundingReport','scheduleFundingReports');
	$j("#cbbforSchedule").val("");
	$j("#notificationBeforeId0").val("");
	/* hideTabDiv('schedule-funding-report'); */
}

function fundingPaginateTable(){
	$j('#cbbguidlinesdt1').dataTable({
		"oLanguage": {
			"sEmptyTable": "<s:text name="garuda.common.message.noMatchingRecords"/>"
		},
		"iDisplayLength":100,
		"bRetrieve": true,
		"bDestroy" :true
		
	});
	 $j('#cbbguidlinesdt1_info').hide();
		$j('#cbbguidlinesdt1_paginate').hide();
		$j('#cbbguidlinesdt1_length').empty();
		$j('#cbbguidlinesdt1_length').replaceWith('Show <select name="showsRow" id="showsRow" style="width:auto; height:25px;" ><option value="05">05</option><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option></select> Entries');
		if($j('#entries').val()!=null || $j('#entries').val()!=""){
			$j('#showsRow').val($j('#entries').val());
			}
		$j('#showsRow').change(function(){
			var cbbId = document.getElementById("cbbIdForFund").value;
			var cbbName = document.getElementById("cbbNameForFund").value;
			var url="getCbbDetailsForFunding?cbbId="+cbbId+"&cbbName="+cbbName;
			paginationFooter(0,"getCbbDetailsForFunding,showsRow,temp,cbuFundingGuidelines,cbbdefaultparent,fundingPaginateTable,cbbIdForFund,cbbNameForFund,flag");
		});
}

function showSelectedCBBGuidlines(siteId,rowIndex){
	var url="showSelectedCBBGuidlines?siteId="+siteId;
	refreshDiv(url,'addFundingGuidlines','cbuFundingGuidelines');
	$j("#addFundingGuidlines").show();
	/* $j('#fundguideline').dataTable({
		"oLanguage": {
			"sEmptyTable": "No Records Found"
		}
	}); */
	
	}

function showCBB(){		
		var cbbId = document.getElementById("cbbIdForFund").value;
		var cbbName = document.getElementById("cbbNameForFund").value;
		var url="getCbbDetailsForFunding?cbbId="+cbbId+"&cbbName="+cbbName;
		refreshDiv(url,'cbbdefaultparent','cbuFundingGuidelines');
		$j("#cbbdefaultparent").show();
		fundingPaginateTable();
		$j("#addFundingGuidlines").hide();
	}
 function editCBBGuidlines(siteId,listSize){
	 if(listSize<=0){
		 alert("<s:text name="garuda.funding.label.requidelines"/>");
	 }
	 else
	 {
	 var url="editCBBGuidlines?siteId="+siteId;
	//refreshDiv(url,'addFundingGuidlines','cbuFunding');
	loadDivWithFormSubmit(url,'addFundingGuidlines','cbuFundingGuidelines');
	$j("#addFundingGuidlines").show();
	$j("#addeditfunding").hide();
	 }
}
 /*
 function editStatus(status,rowIndex){
	var url="editFundingStatus?status="+status+"&rowIndex="+rowIndex;
	refreshDiv(url,'addFundingGuidlines','cbuFunding');
	$j('#addFundingGuidlines').show();
}


 */
 
 function updateCBBGuidlines(){
		//var url="editFundingStatus?status="+status+"&rowIndex="+rowIndex;
		refreshDiv('updateCBBGuidlines','addFundingGuidlines','cbuFundingGuidelines');
		$j("#addFundingGuidlines").show();
	}
 
function addCBBGuidlines(cbbId,lstSize){
	if(lstSize<10){
	var url="addCBBGuidlines?cbbId="+cbbId;
	//refreashDiv(url,'addFundingGuidlines','cbuFunding');
	showModal('Add Funding Guidelines',url,'500','700');
	$j("#addFundingGuidlines").show();
	}
	else{
		alert("<s:text name="garuda.funding.alert.notMoreThan10Guidelines"/>");
	}
}
/* $j('#cbbguidlinesdt1').dataTable({
	"oLanguage": {
		"sEmptyTable": "No Attachments are available"
	}
}); */
//$j('#cbbguidlinesdt1').dataTable();
//$j('#fundguideline').dataTable();
</script>
			    <div id="tabs" class="pendingtabs">
					 <ul class="pendingtabsul">
						<li ><a href="#funding-guideline" ><span><s:text name="garuda.funding.label.fundingGuidelines"/></span></a></li>
						<li ><a href="#schedule-funding-report" onclick="getLstScheduleFundingReport();"><span><s:text name="garuda.funding.report.scheduleFundingReport"/></span></a></li>
						<li ><a href="#funding-report-scheduled" onclick="getLstFundingReportSchedule();"><span><s:text name="garuda.funding.report.fundingReportScheduled"/></span></a></li>
						<li ><a href="#funding-report-his" ><span><s:text name="garuda.funding.report.reportHistory"/></span></a></li>		       
					</ul>
					<div id="funding-guideline">
						          <div id="fundingGuidelines">
						          	 <jsp:include page="pf_fundingCBB_Guidelines.jsp">
						          	 	<jsp:param name="scheduleFlag" value="true"/>
						          	 </jsp:include>
						          </div>
					</div>
					<div id="schedule-funding-report">
						          <div id="scheduleFundingReports">
						          	 <jsp:include page="pf_schedule_funding_reports.jsp">
						          	 	<jsp:param name="scheduleFlag" value="true"/>
						          	 </jsp:include>
						          </div>
					</div>
					<div id="funding-report-scheduled" >		
								       	<div id="fundingReportScheduleds">
											<jsp:include page="pf_schedule_funding_reports.jsp">
								          	 	<jsp:param name="scheduleFlag" value="false"/>
								          	 </jsp:include> 
							          	 </div>
					</div>
					<div id="funding-report-his" >
								 <table border="0" cellpadding="0" cellspacing="0" width="100%" class="displaycdr">
										<thead>
											<tr>
												<th ><s:text name="garuda.funding.report.ID" /></th>
												<th ><s:text name="garuda.pendingorderpage.label.cddid" /></th>
												<th ><s:text name="garuda.landingpage.label.CBB" /></th>
												<th><s:text name="garuda.funding.report.dateGenerated" /></th>
												<th ><s:text name="garuda.funding.report.status" /></th>
												<th ><s:text name="garuda.funding.report.dateSubmitted" /></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th><s:text name="garuda.common.lable.workinprogress" /></th>
												<td colspan="5">
												
												</td>
											</tr>
										</tbody>
										<tfoot>
											<tr>
												<td colspan="6"></td>
											</tr>
										</tfoot>	
								</table>
					</div>
			</div>