<%@ taglib prefix="s"  uri="/struts-tags"%>

<%
String message = request.getParameter("message");
request.setAttribute("message",message);
String divName = "";
if(request.getParameter("divName")!=null && !request.getParameter("divName").trim().equals("")){
	divName = request.getParameter("divName");
}
%>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr >
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:text name="%{#request.message}" /> </strong></p>
			</div>
		</td>
	</tr>
	<tr>
		<td>
		</td>
		<td>
		<%
		if(divName!=null && !divName.equals("")){
		%>
		<input type="button" onclick="closeModals('<%=divName%>')" value="<s:text name="garuda.common.close"/>" />
		<%}else{ %>
			<input type="button" onclick="closeModal()" value="<s:text name="garuda.common.close"/>" />
			<%} %>
		</td>
	</tr>
</table>