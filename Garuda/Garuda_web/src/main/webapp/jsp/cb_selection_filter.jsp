<%@page import="com.velos.ordercomponent.util.VelosGarudaConstants"%>
<%@ taglib prefix="s"  uri="/struts-tags"%>
<script type="text/javascript">

function removeCri()
{
	$j("#completeRequiredInfo").val('');
}

function setPredefinedDateRange(val,div){
	 var selVal = val;
	 if(val==-1)
		 {
		 $j("#"+div+"DateTo").val('');
	     $j("#"+div+"DateFrom").val('');
		 }
	    var p = div+"DateDiv";
	    if(selVal == 'custom'){
	     $j("#"+div+"DateTo").val('');
	     $j("#"+div+"DateFrom").val('');
	     $j(".placeHolderOfid_"+$j("#"+div+"DateTo").attr("id")).show();
	     $j(".placeHolderOfid_"+$j("#"+div+"DateFrom").attr("id")).show();
	    }
	    if (selVal == -1) {
	        $j("#"+p).hide();
	    }else{
	    	if (selVal == 'past_month') {
	           var today = new Date();
	           // var today = new Date(2014,2,1);
	           var d = today.getDate();
	           var m = today.getMonth();
	           var y = today.getFullYear();
	     var lastDate = new Date((new Date(y, m,1))-1); // to get the last day of the past month 

	     lastDate = dateFormat(lastDate,'mmm dd, yyyy');
	           $j("#"+div+"DateTo").val(lastDate);

	           var pastMon = new Date(y,m-1,1); // to get the first day of the past month
	              pastMon = dateFormat(pastMon,'mmm dd, yyyy');
	              $j("#"+div+"DateFrom").val(pastMon);

	              $j(".placeHolderOfid_"+$j("#"+div+"DateTo").attr("id")).hide();
	            $j(".placeHolderOfid_"+$j("#"+div+"DateFrom").attr("id")).hide();
	          }
	        if (selVal == 'past_week') {
	          var today = new Date();
	          var d = today.getDate();
	          var m = today.getMonth();
	          var y = today.getFullYear();
	          
	          today = dateFormat(today,'mmm dd, yyyy');
	          $j("#"+div+"DateTo").val(today);
	             var pastWeek = new Date(y,m,d-7);
	             pastWeek = dateFormat(pastWeek,'mmm dd, yyyy');
	             $j("#"+div+"DateFrom").val(pastWeek);
	             $j(".placeHolderOfid_"+$j("#"+div+"DateTo").attr("id")).hide();
	          $j(".placeHolderOfid_"+$j("#"+div+"DateFrom").attr("id")).hide();
	        }
	        $j('#'+p).show();
	    }
	  
	}

/* function selectedSiteIDs(value)
{
	alert(value);
	var e = document.getElementById("cbbdrpdwn");
	var selectedSites = e.options[e.selectedIndex].value;
	//$j("selectedIdsList").show();
	
	
} */
function f_upDate_SAL(value){
	
	if(value=='Y'){
		$j('#qbsampleAtLab1').val(value);
	}
	if(value=='N'){
		$j('#qbsampleAtLab2').val(value);
	}
	
}
function f_set_lab_hidden(){
	
	if($j(".ctsamp").is(':checked')){
		$j('#qbsampleAtLab1').val("Y");
	}else{
		$j('#qbsampleAtLab1').val("");
	}
	if($j(".ctship").is(':checked')){
		$j('#qbsampleAtLab2').val("N");
	}else{
		$j('#qbsampleAtLab2').val("");
	} 
	
	
}

function selectedSiteIDs()
{
  var selectedSites="";
  var selectedAllSites="";
  var selectedAllSitesTemp="";
  var x=document.getElementById("cbbdrpdwn");
  var count=0;
  var length = x.options.length+1;
   for (var i = 0; i < x.options.length; i++) {
	   count++; 
		 if(count<x.options.length)
			  selectedAllSites += x.options[i].text +'</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ; 
			  else	  
				  selectedAllSites += x.options[i].text +'</br>';
			  selectedAllSitesTemp+=x.options[i].text +'</br>';
			 
  }
  count=0; 
  for (var i = 0; i < x.options.length; i++) {
	 
     if(x.options[i].selected ==true){
    	 if(x.options[i].text=="ALL")
    		 {
    		 	selectedSites = "ALL";
    		 	$j("#cbbdrpdwn").val(selectedSites);
    		 	selectedSites = "";	
    		 	selectedAllSites = selectedAllSites.replace("ALL</br>"," ");
    		 	selectedSites = selectedAllSitesTemp.replace("ALL</br>"," ");
    		 	$j("#allSiteName").val(selectedAllSites.replace("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"," "));
    		 	break;
    		 }
    	 else
    		 {
    		 
    	 		
    	     			selectedSites += x.options[i].text +'</br>' ;
    	 		
    	
    		 }
      }
  }
/*  if(count>4)
	 {
	 $j("#selectedIdsList").hide();
	 $j("#cbb_error").show();
	 }
 else
	 {
	 $j("#cbb_error").hide();
	 } */
var html = "<table width='100%' cellspacing='0' cellpadding='0'><tr><td valign='top' width='15%'>Selected CBB(S):</td><td width='75%' align='left'>"+selectedSites+"</td></tr></table>" ;
 $j("#selectedIdsList").html(html);
 $j("#selectedIdsList").show();
}




function hideCBBDropDown()
{  
	window.location.href =  "simpleCbuReports";
		$j("#collectionDateDiv").hide();
		$j("#registrationDateDiv").hide();
		$j("#submissionDateDiv").hide();
		$j("#ctShipDateDiv").hide();
		$j("#orShipDateDiv").hide();
		$j("#multipleOrgList").hide();
		$j("#selectedIdsList").hide();
}


function showMultipleOrg()
{
	var checked = $j('#multipleOrgAccess:checked').val();
	if(checked=='true')
		{
			$j("#multipleOrgList").show();
			$j("#cbbOrganisation").hide();
		}
	else
		{
			$j("#cbbOrganisation").show();
			$j("#multipleOrgList").hide();
			$j('#cbbdrpdwn :selected').attr('selected', '');
			$j("#selectedIdsList").hide();
		}
	}

function dateSelector(id,value)
{
	if(id=='cbuCollectionRangeId'&& value!=-1)
	{
	$j("#collectionDateDiv").show();
	}
	
	if(id=='cbuSubmissionDateId'&& value!=-1)	
	{
	$j("#submissionDateDiv").show();
	}
	
	if(id=='cbuRegistrationRangeId'&& value!=-1)	
	{
	$j("#registrationDateDiv").show();
	}	
	if(id=='ctShipDateRangeId'&& value!=-1)	
	{
	$j("#ctShipDateDiv").show();
	}	
	if(id=='orShipDateRangeId'&& value!=-1)	
	{
	$j("#orShipDateDiv").show();
	}	
}


$j(function(){
	getDatePic();
/*	selectedSiteIDs(); Commented during merging 1.3 */
	jQuery('#collectionDateTo, #collectionDateFrom').datepicker('option', {
	    beforeShow: customRange2
	  });
	jQuery('#submissionDateTo, #submissionDateFrom').datepicker('option', {
	    beforeShow: customRange
	  
	  });
	jQuery('#registrationDateTo, #registrationDateFrom').datepicker('option', {
	    beforeShow: customRange3
	  });
	jQuery('#ctShipDateTo, #ctShipDateFrom').datepicker('option', {
	    beforeShow: customRange4
	  });
	jQuery('#orShipDateTo, #orShipDateFrom').datepicker('option', {
	    beforeShow: customRange5
	  });
});

function customRange2(input) {
	  /*if (input.id == 'collectionDateTo') {
	    return {
	      minDate: jQuery('#collectionDateFrom').datepicker("getDate")
	    };
	  } else if (input.id == 'collectionDateFrom') {
	    return {
	      maxDate: jQuery('#collectionDateTo').datepicker("getDate")
	    };
	  }*/
	if (input.id == 'collectionDateTo') {
		if($j("#collectionDateFrom").val()!="" && $j("#collectionDateFrom").val()!=null){
	    	var minTestDate=new Date($j("#collectionDateFrom").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  	$j('#collectionDateTo').datepicker( "option", "minDate", new Date(y1,m1,d1));
		}else{
			$j('#collectionDateTo').datepicker( "option", "minDate", "");
		}
	  } else if (input.id == 'collectionDateFrom') {
		  if($j("#collectionDateTo").val()!="" && $j("#collectionDateTo").val()!=null){
		  	var minTestDate=new Date($j("#collectionDateTo").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  	$j('#collectionDateFrom').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#collectionDateFrom').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
	}
function customRange3(input) {
	  /*if (input.id == 'registrationDateTo') {
	    return {
	      minDate: jQuery('#registrationDateFrom').datepicker("getDate")
	    };
	  } else if (input.id == 'registrationDateFrom') {
	    return {
	      maxDate: jQuery('#registrationDateTo').datepicker("getDate")
	    };
	  }*/
	if (input.id == 'registrationDateTo') {
		if($j("#registrationDateFrom").val()!="" && $j("#registrationDateFrom").val()!=null){
	    	var minTestDate=new Date($j("#registrationDateFrom").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  	$j('#registrationDateTo').datepicker( "option", "minDate", new Date(y1,m1,d1));
		}else{
			$j('#registrationDateTo').datepicker( "option", "minDate", "");
		}
	  } else if (input.id == 'registrationDateFrom') {
		  if($j("#registrationDateTo").val()!="" && $j("#registrationDateTo").val()!=null){
		  	var minTestDate=new Date($j("#registrationDateTo").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  	$j('#registrationDateFrom').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#registrationDateFrom').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
	}


function customRange4(input) {
	  /*if (input.id == 'registrationDateTo') {
	    return {
	      minDate: jQuery('#registrationDateFrom').datepicker("getDate")
	    };
	  } else if (input.id == 'registrationDateFrom') {
	    return {
	      maxDate: jQuery('#registrationDateTo').datepicker("getDate")
	    };
	  }*/
	if (input.id == 'ctShipDateTo') {
		if($j("#ctShipDateFrom").val()!="" && $j("#ctShipDateFrom").val()!=null){
	    	var minTestDate=new Date($j("#ctShipDateFrom").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  	$j('#ctShipDateTo').datepicker( "option", "minDate", new Date(y1,m1,d1));
		}else{
			$j('#ctShipDateTo').datepicker( "option", "minDate", "");
		}
	  } else if (input.id == 'ctShipDateFrom') {
		  if($j("#ctShipDateTo").val()!="" && $j("#ctShipDateTo").val()!=null){
		  	var minTestDate=new Date($j("#ctShipDateTo").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  	$j('#ctShipDateFrom').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#ctShipDateFrom').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
	}

function customRange5(input) {
	  /*if (input.id == 'registrationDateTo') {
	    return {
	      minDate: jQuery('#registrationDateFrom').datepicker("getDate")
	    };
	  } else if (input.id == 'registrationDateFrom') {
	    return {
	      maxDate: jQuery('#registrationDateTo').datepicker("getDate")
	    };
	  }*/
	if (input.id == 'orShipDateTo') {
		if($j("#orShipDateFrom").val()!="" && $j("#orShipDateFrom").val()!=null && $j("#orShipDateFrom").length==10){
	    	var minTestDate=new Date($j("#orShipDateFrom").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  	$j('#orShipDateTo').datepicker( "option", "minDate", new Date(y1,m1,d1));
		}else{
			$j('#orShipDateTo').datepicker( "option", "minDate", "");
		}
	  } else if (input.id == 'orShipDateFrom') {
		  if($j("#orShipDateTo").val()!="" && $j("#orShipDateTo").val()!=null && $j("#orShipDateTo").length==10){
		  	var minTestDate=new Date($j("#orShipDateTo").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  	$j('#orShipDateFrom').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#orShipDateFrom').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
	}


function customRange(input) {
	 /* if (input.id == 'submissionDateTo') {
	    return {
	      minDate: jQuery('#submissionDateFrom').datepicker("getDate")
	    };
	  } else if (input.id == 'submissionDateFrom') {
	    return {
	      maxDate: jQuery('#submissionDateTo').datepicker("getDate")
	    };
	  }*/
	if (input.id == 'submissionDateTo') {
		if($j("#submissionDateFrom").val()!="" && $j("#submissionDateFrom").val()!=null && $j("#submissionDateFrom").length==10){
	    var minTestDate=new Date($j("#submissionDateFrom").val()); 	 
		var d1 = minTestDate.getDate();
		var m1 = minTestDate.getMonth();
		var y1 = minTestDate.getFullYear();
		  $j('#submissionDateTo').datepicker( "option", "minDate", new Date(y1,m1,d1));
		}else{
			$j('#submissionDateTo').datepicker( "option", "minDate", "");
		}
	  } else if (input.id == 'submissionDateFrom') {
		  if($j("#submissionDateTo").val()!="" && $j("#submissionDateTo").val()!=null && $j("#submissionDateTo").length==10){
		  	var minTestDate=new Date($j("#submissionDateTo").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
		  	$j('#submissionDateFrom').datepicker( "option", "maxDate", new Date(y1,m1,d1));
		  }else{
			  $j('#submissionDateFrom').datepicker( "option", "maxDate", "");
		}
	  }
	  var result = $j.browser.msie ? !this.fixFocusIE : true;
	  this.fixFocusIE = false;
	  return result;
	}


$j(function(){
	$j("#simpleCBUForm").validate({
		invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    },
	rules:{
		"collectionDateFromStr":{required:
									{
									  depends: function(element){
										  return ($j("#collectionDateTo").val()!="" || $j("#collectionDateDiv").is(':visible') );
										}
									},dpDate:true
								},
		"collectionDateToStr":{required:
									{
									  depends: function(element){
										  return ($j("#collectionDateFrom").val()!="" || $j("#collectionDateDiv").is(':visible') );
										}
									},dpDate:true
								},
		"submitionDateFromStr":{required:
								{
								  depends: function(element){
									  return ( $j("#submissionDateTo").val()!="" || $j("#submissionDateDiv").is(':visible') );
									}
								},dpDate:true
							},
		"submitionDateToStr":{required:
							{
							  depends: function(element){
								  return ( $j("#submissionDateFrom").val()!="" || $j("#submissionDateDiv").is(':visible') );
								}
							},dpDate:true
						},
		"registrationDateFromStr":{required:
						{
						  depends: function(element){
							  return ($j("#registrationDateTo").val()!="" || $j("#registrationDateDiv").is(':visible') );
							}
						},dpDate:true
					},
		"registrationDateToStr":{required:
						{
						  depends: function(element){
							  return ($j("#registrationDateFrom").val()!="" || $j("#registrationDateDiv").is(':visible') );
							}
						},dpDate:true
					},
		"ctShipDateFromStr":{required:
					{
					  depends: function(element){
						  return ($j("#ctShipDateTo").val()!="" || $j("#ctShipDateDiv").is(':visible') );
						}
					},dpDate:true
				},
		"ctShipDateToStr":{required:
					{
					  depends: function(element){
						  return ($j("#ctShipDateFrom").val()!="" || $j("#ctShipDateDiv").is(':visible') );
						}
					},dpDate:true
				},	
		"orShipDateFromStr":{required:
				{
				  depends: function(element){
					  return ($j("#orShipDateTo").val()!="" || $j("#orShipDateDiv").is(':visible') );
					}
				},dpDate:true
			},
	   "orShipDateToStr":{required:
				{
				  depends: function(element){
					  return ($j("#orShipDateFrom").val()!="" || $j("#orShipDateDiv").is(':visible') );
					}
				},dpDate:true
			},	
 },
 messages:{
	 "collectionDateFromStr":{required:"<s:text name="garuda.cbu.cordentry.collectionDateFromStr"/>"},
	 "collectionDateToStr":{required:"<s:text name="garuda.cbu.cordentry.collectionDateToStr"/>"},
	 "submitionDateFromStr":{required:"<s:text name="garuda.cbu.cordentry.submitionDateFromStr"/>"},
	 "submitionDateToStr":{required:"<s:text name="garuda.cbu.cordentry.submitionDateToStr"/>"},
	 "registrationDateFromStr":{required:"<s:text name="garuda.cbu.cordentry.registrationDateFromStr"/>"},
	 "registrationDateToStr":{required:"<s:text name="garuda.cbu.cordentry.registrationDateToStr"/>"},
	 "ctShipDateFromStr":{required:"<s:text name="garuda.cbu.cordentry.ctDateFromStr"/>"},
	 "ctShipDateToStr":{required:"<s:text name="garuda.cbu.cordentry.ctDateToStr"/>"},
	 "orShipDateFromStr":{required:"<s:text name="garuda.cbu.cordentry.orDateFromStr"/>"},
	 "orShipDateToStr":{required:"<s:text name="garuda.cbu.cordentry.orDateToStr"/>"},
 }
});
});

$j(function(){
	getDatePic();
	selectedSiteIDs();
	 jQuery('#collectionDateTo, #collectionDateFrom').datepicker('option', {
	    beforeShow: customRange2
	  }).focus(function(){
		  if(!$j("#ui-datepicker-div").is(":visible"))
		  		customRange2(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mmm dd, yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	jQuery('#submissionDateTo, #submissionDateFrom').datepicker('option', {
	    beforeShow: customRange
	  }).focus(function(){
		  if(!$j("#ui-datepicker-div").is(":visible"))
		  		customRange(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					
					var formattedDate = dateFormat($j(this).val(),'mmm dd, yyyy');
					$j(this).val(formattedDate);
				}
			}
		});
	jQuery('#registrationDateTo, #registrationDateFrom').datepicker('option', {
	    beforeShow: customRange3
	  }).focus(function(){
		  if(!$j("#ui-datepicker-div").is(":visible"))
		  		customRange3(this);
			if($j(this).val()!=""){
				var thisdate = new Date($j(this).val());
				if(!isNaN(thisdate)){
					var formattedDate = dateFormat($j(this).val(),'mmm dd, yyyy');
					$j(this).val(formattedDate);
				}
			}
		}); 
});

function customRange2(input) {
	
	 if (input.id == 'collectionDateTo') {
		 if($j("#collectionDateFrom").val()!="" && $j("#collectionDateFrom").val()!=null && $j("#collectionDateFrom").length==10){
			 var minTestDate=new Date($j("#collectionDateFrom").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
			  $j('#collectionDateTo').datepicker( "option", "minDate", new Date(y1,m1,d1));
		 }else{
			 $j('#collectionDateTo').datepicker( "option", "minDate", null);
		}
		  } else if (input.id == 'collectionDateFrom') {
			  if($j("#collectionDateTo").val()!="" && $j("#collectionDateTo").val()!=null && $j("#collectionDateFrom").length==10){
			  var minTestDate=new Date($j("#collectionDateTo").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
			 	$j('#collectionDateFrom').datepicker( "option", "maxDate", new Date(y1,m1,d1));
			  }else{
				  $j('#collectionDateFrom').datepicker( "option", "maxDate", null);
			}
		  }
	}

function customRange3(input) {
	 if (input.id == 'registrationDateTo') {
		 if($j("#registrationDateFrom").val()!="" && $j("#registrationDateFrom").val()!=null && $j("#registrationDateFrom").length==10){
		    var minTestDate=new Date($j("#registrationDateFrom").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
			  $j('#registrationDateTo').datepicker( "option", "minDate", new Date(y1,m1,d1));
		 }else{
			 $j('#registrationDateTo').datepicker( "option", "minDate", null);
		}
		  } else if (input.id == 'registrationDateFrom') {
			  if($j("#registrationDateTo").val()!="" && $j("#registrationDateTo").val()!=null && $j("#registrationDateTo").length==10){
			  var minTestDate=new Date($j("#registrationDateTo").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
			 	$j('#registrationDateFrom').datepicker( "option", "maxDate", new Date(y1,m1,d1));
			  }else{
				  $j('#registrationDateFrom').datepicker( "option", "maxDate", null);
			}
		  }
	}

function customRange(input) {
	 if (input.id == 'submissionDateTo') {
		 if($j("#submissionDateFrom").val()!="" && $j("#submissionDateFrom").val()!=null && $j("#submissionDateFrom").length==10){
		    var minTestDate=new Date($j("#submissionDateFrom").val()); 	 
			var d1 = minTestDate.getDate();
			var m1 = minTestDate.getMonth();
			var y1 = minTestDate.getFullYear();
			  $j('#submissionDateTo').datepicker( "option", "minDate", new Date(y1,m1,d1));
		 }else{
			 $j('#submissionDateTo').datepicker( "option", "minDate", null);
		 }
		  } else if (input.id == 'submissionDateFrom') {
			  if($j("#submissionDateTo").val()!="" && $j("#submissionDateTo").val()!=null && $j("#submissionDateTo").length==10){
			  var minTestDate=new Date($j("#submissionDateTo").val()); 	 
				var d1 = minTestDate.getDate();
				var m1 = minTestDate.getMonth();
				var y1 = minTestDate.getFullYear();
			  $j('#submissionDateFrom').datepicker( "option", "maxDate", new Date(y1,m1,d1));
			  }else{
				  $j('#submissionDateFrom').datepicker( "option", "maxDate", null);
			}
		  }
	}

function clearerreor(val,idclear){
	if(val != ""){
	$j('#'+idclear).html("");
	}
}

function checkids()
{
		  if($j('#simpleCBUForm').valid()){
					f_set_lab_hidden();
					asyncLoadDivWithFormSubmit('getSimpleCbuReport','simpleCBUReportID','simpleCBUForm');
				
			//loadPageByGetRequset('getSimpleCbuReport','simpleCBUPage');
		}
	/* } */	
}

/* function ShowResult(){

	window.location="advancedLookUp1";

} */

/* function toggleFilter()
{
	
		   $j("#mainDiv").toggleDiv('simpleQuery');
		
} */

/* function showAddWidget(url){
	 url = url + "?pageId=22" ; 
	 showAddWidgetModal(url);
} */

function hideDiv()
{
	$j("#hideFilterDiv").hide();
	$j("#showFilterDiv").show();
	
}

function showFilterDivs()
{
	
	$j("#hideFilterDiv").show();
	$j("#showFilterDiv").hide();
	
}
</script>

<div class="col_100" id="simpleCBUPage">

<table width="100%" id="container1">
<tr><td id="hideFilterDiv" align="right"><a href="#" onclick="hideDiv();toggleDiv('simpleQuery');"><s:text name="garuda.queryBuilder.label.hideFilter"></s:text></a></td></tr>
<tr id="showFilterDiv" align="right" style="display:none" ><td><a href="#" onclick="showFilterDivs();toggleDiv('simpleQuery');"><s:text name="Show Filter"></s:text></a></td></tr>
	<!--<tr>
    	<td>
    			<s:if test="customFilter!=null && customFilter.size()>0"><s:select name="customFilters" id="customFilters" list="customFilter" onchange="ExecuteCustomFilterOrd(this.value)" listKey="selected_param" listValue="filter_name" headerKey="" headerValue="-Custom Filter-"/></s:if>
									
    	</td>
    </tr>--><tr>
		<td>
		<div class="column">
		<div class="portlet" id="advLookupparent">
		<div id="addvancedlookupdiv"
			class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
		<div
			class="portlet-header ui-widget-header ui-widget-content ui-corner-all" id="simpleQuerycontent">
		<span class="ui-icon ui-icon-minusthick"></span> <!--<span
			class="ui-icon ui-icon-close"></span>--> <s:text
			name="garuda.queryBuilder.label.queryBuilderLable" /></div>
		<div class="portlet-content" id="simpleQuery">
		

<s:form id="simpleCBUForm">
<s:hidden name="starts" id="starts"></s:hidden>
<%-- <s:hidden name="ends" id="ends"></s:hidden>
<s:hidden name="contains" id="contains"></s:hidden>
<s:hidden name="exact" id="exact"></s:hidden> --%>
<input type="hidden" name="paginateModule" value="queryBuilder"/>
<input type = "hidden" id="priamryOrg" name="priamryOrg" value="<s:property value="#request.primaryOrg"/>"/>
<input type = "hidden" id= "priamryOrgName" name="priamryOrgName" value="<s:property value="#request.primaryOrgName"/>"/>
<input type = "hidden" id="primaryOrgSiteId" name="primaryOrgSiteId" value="<s:property value="#request.primaryOrgSiteId"/>"/>
<input type = "hidden" id="primaryOrg" name="primaryOrg" value="<s:property value="#request.primaryOrg"/>"/>
<input type = "hidden" id="allSiteName" name="allSiteName"/>
	<table width="100%" cellpadding="0" cellspacing="0">
	
<%--  		<tr>
					<s:if test="#request.CbbList!=null">
					<td colspan="2">
					<s:checkbox name="Multiple" id="multipleOrgAccess"  onclick="javascript:showMultipleOrg();"></s:checkbox><s:text
					name="garuda.queryBuilder.label.multipleOrganisation"></s:text>
					</td>
					</s:if>
		</tr> --%>
		<tr>
			          <td width="200px"><s:text name="garuda.cbbdefaults.lable.cbbid" /></td>
	          		  <s:if test="#request.CbbList!=null">
	          		  <td colspan="2"     id="multipleOrgList">
	             			
							 <select id="cbbdrpdwn"   name="cbbdrpdwn" onChange="selectedSiteIDs()" style="width:300px;height:auto;overflow:visible;"  size = "5" multiple="multiple"> 
							<s:if test="#request.primaryOrgSiteId!='NMDP'">
							<option value="<s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_OPTION"/>"><s:property value="@com.velos.ordercomponent.util.VelosGarudaConstants@ALL_OPTION"/></option>
		  					</s:if>  
							<s:iterator value="#request.CbbList"> 
							<s:if test="siteId==#request.primaryOrg">
							<option value=<s:property value='siteId'/> selected="selected" > <s:property value="siteIdentifier"/> - <s:property value="siteName"/></option> 
							</s:if>
							<s:else>
							<option value=<s:property value='siteId'/>> <s:property value="siteIdentifier"/> - <s:property value="siteName"/></option>
							</s:else>
						 </s:iterator>
		  					 </select> 
		  					<div id='cbb_error' style="display: none;">
									<span style="color: red;"><s:text name="garuda.queryBuilder.label.errorMessage"/></span>
							</div>
							
					 </td>
					 </s:if>
					 <s:if test="#request.primaryOrg!=null && #request.CbbList==null">
					<td colspan="2" id="cbbOrganisation">
					 
					 <s:property value="#request.primaryOrgSiteId"/>-<s:property value="#request.primaryOrgName"/>
					 
					 </td>
					 </s:if>
					
		 		 </tr>
		 		 	<tr  >
		 		 	        <td width="200px"></td>
		 		 			<td colspan="2" style="display:none" id="selectedIdsList">
					 
					 		</td>
		 		 	</tr>
		 		
	 			
				<tr>
					<td width="200px"><s:text name="garuda.advancelookup.label.crdstus"></s:text>:      </td>
					<td>
					<s:iterator value="getCBUStatusValues(@com.velos.ordercomponent.business.util.VelosMidConstants@NATIONAL_STATUS)" status="row">
					<input type= "checkbox" name="cordStatus" value="<s:property value="pkcbustatus"/>"/>
					<s:property value="cbuStatusDesc"/>					
					</s:iterator>			
					<input type="checkbox" name="noStatus" ><s:text name="garuda.queryBuilder.label.cbuStatusNoStatus"/>
					</td>
				
				</tr>
			
			 	<tr>
					<td width="200px" valign="top"><s:text name="garuda.cdrcbuview.label.clinical_Reason"></s:text>:</td>
					<td colspan="2" ><table width="100%" cellspacing="0" cellpadding="0">
					<s:iterator value="#request.defferedReason" status="row">
					<%-- <s:if test="#row.index==3">
					<input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					<s:property value = "cbuStatusDesc" />&nbsp;&nbsp;&nbsp;
					</s:if>
					<s:elseif test="#row.index==4">
					<input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					<s:property value = "cbuStatusDesc" />&nbsp;&nbsp;
					</s:elseif>
					<s:elseif test="#row.index==6">
					<input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					<s:property value = "cbuStatusDesc" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					
					</s:elseif>
					<s:elseif test="#row.index==7">
					<input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					<s:property value = "cbuStatusDesc" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</s:elseif>
					<s:elseif test="#row.index==9">
					<input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					<s:property value = "cbuStatusDesc" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					
					</s:elseif>
					<s:else>
					<input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					<s:property value = "cbuStatusDesc" />&nbsp;&nbsp;
					</s:else>
					 <s:if test="#row.index==2 || #row.index==5 || #row.index==8">
					   </br>
					</s:if> --%>
					<s:if test="#row.index==0">
					  <tr>
					     <td><input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					     <s:property value = "cbuStatusDesc" />
					     </td>
					</s:if>
					<s:if test="#row.index==1">
					  
					     <td><input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					     <s:property value = "cbuStatusDesc" />
					     </td>
					</s:if>
					<s:if test="#row.index==2">
					      <td><input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					     <s:property value = "cbuStatusDesc" />
					     </td>
					   </tr>
					</s:if>
					<s:if test="#row.index==3">
					  <tr>
					     <td><input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					     <s:property value = "cbuStatusDesc" />
					     </td>
					</s:if>
					<s:if test="#row.index==4">
					  
					     <td><input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					     <s:property value = "cbuStatusDesc" />
					     </td>
					</s:if>
					<s:if test="#row.index==5">
					      <td><input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					     <s:property value = "cbuStatusDesc" />
					     </td>
					   </tr>
					</s:if>
					<s:if test="#row.index==6">
					  <tr>
					     <td><input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					     <s:property value = "cbuStatusDesc" />
					     </td>
					</s:if>
					<s:if test="#row.index==7">
					  
					     <td><input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					     <s:property value = "cbuStatusDesc" />
					     </td>
					</s:if>
					<s:if test="#row.index==8">
					      <td><input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					     <s:property value = "cbuStatusDesc" />
					     </td>
					   </tr>
					</s:if>
					<s:if test="#row.index==9">
					  <tr>
					     <td><input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					     <s:property value = "cbuStatusDesc" />
					     </td>
					</s:if>
					<s:if test="#row.index==10">
					  
					     <td colspan="2"><input type= "checkbox" name="cordStatusReason" value="<s:property value="pkcbustatus"/>"/>
					     <s:property value = "cbuStatusDesc" />
					     </td>
					   </tr>
					</s:if>
					
					</s:iterator>
					</table>
				    </td>
				</tr> 
		<%-- <tr>
			<td colspan="2">
			<table style="width: 55%">
				<tr>
					<td width="5%"><s:text name="garuda.advancelookup.label.id"></s:text>:
					</td>
					<td width="30%">
					<s:radio name="searchId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@SEARCH_IDS]" listKey="pkCodeId" listValue="description" id="searchId" onclick="clearerreor(this.value,'selectSearch');" />
					<br/><span class="error"><label id="selectSearch"></label></span></td>
					<td width="20%"><s:textfield name="inputId" value="" placeholder="Enter any ID" id="inputId" />
					<br/><span class="error"><label id="inputIdreq"></label></span></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td width="100%">
			<fieldset><legend><s:text
				name="garuda.advancelookup.label.cbu" /></legend>
			<table>
				<tr>
					<td>
					<s:checkboxlist list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_IDS]" listKey="pkCodeId" listValue="description" name="cbusearchId" onclick ="clearerreor(this.value,'cbulblerror');clearerreor(this.value,'totallblerror');"/>
					<span class="error"><label id="cbulblerror"></label> </span>
					</td>
				</tr>
			</table>
			</fieldset>
			</td>
		</tr>
		
		<tr>
			<td width="100%">
			<fieldset><legend><s:text
				name="garuda.advancelookup.label.maternal" /></legend>
			<table>
				<tr>
					<td>
					<s:checkboxlist list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@MATERNAL_IDS]" listKey="pkCodeId" listValue="description" name="maternalSearchId" onclick ="clearerreor(this.value,'matlblerror');clearerreor(this.value,'totallblerror');"/>
					<span class="error"><label id="matlblerror"></label> </span>
					</td>
					<td></td>
				</tr>
			</table>
			</fieldset>
			</td>
		</tr> --%>
		<!--  <tr>
			<td>
		 <fieldset><legend><s:text
				name="garuda.advancelookup.label.donor" /></legend>
			<table>
				<tr>
					<td><span class="error"><s:text name="garuda.cbu.label.id_discussederror"></s:text></span></td>
				</tr>
			</table>
			</fieldset>
			</td>
			<td>
			 <fieldset><legend><s:text
				name="garuda.advancelookup.label.patient" /></legend>
			<table>
				<tr>
					<td><span class="error"><s:text
						name="garuda.cbu.label.id_discussederror"></s:text></span></td>
				</tr>
			</table>
			</fieldset>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<fieldset><legend><s:text
				name="garuda.advancelookup.label.product" /></legend>
			<table>
				<tr>
					<td><span class="error"><s:text
						name="garuda.cbu.label.id_discussederror"></s:text></span></td>
				</tr>
			</table>
			</fieldset>
			</td>
		</tr>  -->
		
		<tr>
			<td colspan="3">
			<table>
				
				
			
				<tr>
					<td ><s:text name="garuda.idsReport.currworkactstage"></s:text>:</td>
					<td colspan = "2">
					 <s:hidden name="qbsampleAtLab1" id="qbsampleAtLab1" ></s:hidden> 
					 <s:hidden name="qbsampleAtLab2" id="qbsampleAtLab2" ></s:hidden> 
					 <input type="checkbox" name="workflow" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_TYPE_NEW,@com.velos.ordercomponent.util.VelosGarudaConstants@ORORDER)"/>" onclick ="clearerreor(this.value,'totallblerror');" ><s:text name="garuda.orOrderDetail.portletname.orOrder"/>
					 <input type="checkbox" name="workflow" class="ctship" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_TYPE_NEW,@com.velos.ordercomponent.util.VelosGarudaConstants@CTORDER)"/>" onclick ="clearerreor(this.value,'totallblerror');f_upDate_SAL('N');" ><s:text name="garuda.orOrderDetail.portletname.ctshiporder"/>
					 <input type="checkbox" name="workflow" class="ctsamp" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_TYPE_NEW,@com.velos.ordercomponent.util.VelosGarudaConstants@CTORDER)"/>" onclick ="clearerreor(this.value,'totallblerror');f_upDate_SAL('Y');" ><s:text name="garuda.orOrderDetail.portletname.ctlaborder"/> 
					 <input type="checkbox" name="workflow" value="<s:property value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@ORDER_TYPE_NEW,@com.velos.ordercomponent.util.VelosGarudaConstants@HEORDER)"/>" onclick ="clearerreor(this.value,'totallblerror');" ><s:text name="garuda.heOrderDetail.portletname.heOrder"/>
					 
					</td>
				</tr>
				
				<tr>
					<td><s:text name="garuda.advancelookup.label.licstus"></s:text>:</td>
					<td>
					<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@LICENCE_STATUS]">
					<input type= "checkbox" name="cordCbuLicStatus" value="<s:property value="pkCodeId"/>"/>
					<s:property value="description"/>
					</s:iterator>
					</td>
				</tr>
				
				
				<tr >
					<td width="20%" ><s:text name="garuda.advancelookup.label.eligibility"></s:text>:</td>
					<td  width="80%">
					<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@ELIGIBILITY_STATUS]">
					<input type= "checkbox" name="cordCbuEliStatus" value="<s:property value="pkCodeId"/>"/>
					<s:property value="description"/>
					</s:iterator>
					<input type="checkbox" name="notDetermined" value="Y"><s:text name="garuda.queryBuilder.label.eligibilityNotDetermined"/>
					</td>
				</tr>
				
				 <tr>
	 			    <td width="200px"><s:text name="garuda.queryBuilder.label.completeReqInfo"></s:text>:</td>
					 <td><s:select name="completeRequiredInfo" cssStyle="width:100px" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@COMPLETE_REQ_INFO]"
						  listKey="description" listValue="description" onchange="removeCri()" headerKey="-1" headerValue="Select"/></td>
				 </tr>
				<tr>
					<td>
						<s:text name="garuda.queryBuilder.label.other"></s:text>:</td>
					<td>
						<input type="checkbox" name="fundedCBU" value="Y" onclick ="clearerreor(this.value,'totallblerror');" ><s:text name="garuda.queryBuilder.label.ncbi"/>
				
					</td>
				</tr>
				<tr>
					<td>
						
					<td>
						
				
						<input type="checkbox" name="antigenValue"  onclick ="clearerreor(this.value,'totallblerror');" ><s:text name="garuda.queryBuilder.label.shwWithOutAntigen"/>
					</td>
				</tr>

			</table>
			</td>
		</tr>
		<tr>
		  <td colspan="3">		
		    <table width="100%">
		       <tr>
	         <td >
		             <fieldset style="width:90%" >	
		                <legend><s:text name="garuda.queryBuilder.cbuCollectDate" /></legend>	                   
						<table>
						   <tr>
						   		<td>
						   			<s:text name="garuda.queryBuilder.collectionRange" />
						   		</td>
						       <td>
						           <s:select name="collecDate" style="width:auto;" id="cbuCollectionRangeId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DATE_RANGE]"
						             listKey="subType" listValue="description"  headerKey="-1" headerValue="Select" onchange="setPredefinedDateRange(this.value,'collection')" />
						       </td>						       
						   </tr>
						   <tr>
						      <td colspan="2">
									<div id="collectionDateDiv" style="display:none">
										<table>
											<tr>
												<td><s:text name="From"></s:text></td>
												<td>	<s:text name="To"></s:text></td>
											</tr>
											<tr>
												<td><s:date name="collectionFrom" id="collectionDateFrom" format="MMM dd, yyyy" />
													<s:textfield readonly="true" name="collectionDateFromStr" cssClass="datepic" id="collectionDateFrom" 
														value="%{collectionDateFrom}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/>
												</td><td><s:date name="collectionTo" id="collectionDateTo" format="MMM dd, yyyy" />
													<s:textfield readonly="true" name="collectionDateToStr" cssClass="datepic" id="collectionDateTo" 
														value="%{collectionDateTo}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/>								
												</td>
											</tr>
										</table>
									</div>
							</td>
							
												
								
						   </tr>
						</table>             
		             </fieldset>		         
		         </td> 
		         <td>
		             <fieldset style=" width:90%" >	
		                <legend><s:text name="garuda.queryBuilder.cbuSubmitDate" /></legend>	                   
						<table>
						   <tr>
						   		<td>
						   			<s:text name="garuda.queryBuilder.submissionRange" />
						   		</td>
						       <td>
						           <s:select name="submDate" style="width:auto;" id="cbuSubmissionDateId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DATE_RANGE]"
						  listKey="subType" listValue="description"  headerKey="-1" headerValue="Select" onchange="setPredefinedDateRange(this.value,'submission')" />
						       </td>						       
						   </tr>
						   <tr>
						      <td colspan="2">
								<div id="submissionDateDiv" style="display:none">
									<table>
										<tr>
											<td><s:text name="From"></s:text></td>
											<td>	<s:text name="To"></s:text></td>
										</tr>
										<tr>
											<td><s:date name="submissionFrom" id="submissionDateFrom" format="MMM dd, yyyy" />
										 <s:textfield readonly="true" name="submitionDateFromStr" cssClass="datepic" id="submissionDateFrom" 
											value="%{submissionDateFrom}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/>
											</td><td><s:date name="submissionTo" id="submissionDateTo" format="MMM dd, yyyy" />
										 			<s:textfield readonly="true" name="submitionDateToStr" cssClass="datepic" id="submissionDateTo" 
														value="%{submissionDateTo}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/>								
											</td>
										</tr>
									</table>
								</div>
							</td>
						   </tr>
						</table>             
		             </fieldset>		         
		         </td>
		         <td>
		             <fieldset style=" width:90% " >
		                <legend><s:text name="garuda.queryBuilder.cbuRegisterDate" /></legend>		                   
						<table>
						   <tr>
						   		<td>
						   				<s:text name="garuda.queryBuilder.registrationRange" />
						   		</td>
						       <td>
						           <s:select name="regisDate" style="width:auto;" id="cbuRegistrationRangeId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DATE_RANGE]"
						  listKey="subType" listValue="description"  headerKey="-1" headerValue="Select" onchange="setPredefinedDateRange(this.value,'registration')" />
						       </td>						       
						   </tr>
						   <tr>
						      <td colspan="2">
								<div id="registrationDateDiv" style="display:none">
									<table>
										<tr>
											<td><s:text name="From"></s:text></td>
											<td>	<s:text name="To"></s:text></td>
										</tr>
										<tr>
											<td><s:date name="registrationFrom" id="registrationDateFrom" format="MMM dd, yyyy" />
										 		<s:textfield readonly="true" name="registrationDateFromStr" cssClass="datepic" id="registrationDateFrom" 
													value="%{registrationDateFrom}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/>
											</td><td><s:date name="registrationTo" id="registrationDateTo" format="MMM dd, yyyy" />
										 			<s:textfield readonly="true" name="registrationDateToStr" cssClass="datepic" id="registrationDateTo" 
														value="%{registrationDateTo}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/>								
											</td>
										</tr>
									</table>
								</div>
							</td>
						   </tr>
						   
						</table>             
		             </fieldset>		         
		         </td>
		       </tr>
		       <tr>
		       <td >
		             <fieldset style="width:90%"  >	
		                <legend><s:text name="garuda.ctShipmentInfo.label.ctshipdate" /></legend>	                   
						<table>
						   <tr>
						   		<td>
						   			<s:text name="garuda.queryBuilder.ctShipRange" />
						   		</td>
						       <td>
						           <s:select name="ctShipDate" style="width:auto;" id="ctShipDateRangeId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DATE_RANGE]"
						             listKey="subType" listValue="description"  headerKey="-1" headerValue="Select" onchange="setPredefinedDateRange(this.value,'ctShip')" />
						       </td>						       
						   </tr>
						   <tr>
						      <td colspan="2">
									<div id="ctShipDateDiv" style="display:none">
										<table>
											<tr>
												<td><s:text name="From"></s:text></td>
												<td>	<s:text name="To"></s:text></td>
											</tr>
											<tr>
												<td><s:date name="ctShipFrom" id="ctShipDateFrom" format="MMM dd, yyyy" />
													<s:textfield readonly="true" name="ctShipDateFromStr" cssClass="datepic" id="ctShipDateFrom" 
														value="%{ctShipDateFrom}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/>
												</td><td><s:date name="ctShipTo" id="ctShipDateTo" format="MMM dd, yyyy" />
													<s:textfield readonly="true" name="ctShipDateToStr" cssClass="datepic" id="ctShipDateTo" 
														value="%{ctShipDateTo}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/>								
												</td>
											</tr>
										</table>
									</div>
							</td>
						   </tr>
						</table>             
		             </fieldset>		         
		         </td>
		         <td>
		             <fieldset style="width:90%">	
		                <legend><s:text name="garuda.queryBuilder.orShipDate" /></legend>	                   
						<table>
						   <tr>
						   		<td>
						   			<s:text name="garuda.queryBuilder.orShipRange" />
						   		</td>
						       <td>
						           <s:select name="orShipDate" style="width:auto;" id="orShipDateId" list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@DATE_RANGE]"
						  listKey="subType" listValue="description"  headerKey="-1" headerValue="Select" onchange="setPredefinedDateRange(this.value,'orShip')" />
						       </td>						       
						   </tr>
						   <tr>
						      <td colspan="2">
								<div id="orShipDateDiv" style="display:none">
									<table>
										<tr>
											<td><s:text name="From"></s:text></td>
											<td>	<s:text name="To"></s:text></td>
										</tr>
										<tr>
											<td><s:date name="orShipFrom" id="orShipDateFrom" format="MMM dd, yyyy" />
										 <s:textfield readonly="true" name="orShipDateFromStr" cssClass="datepic" id="orShipDateFrom" 
											value="%{orShipDateFrom}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/>
											</td><td><s:date name="orShipTo" id="orShipDateTo" format="MMM dd, yyyy" />
										 			<s:textfield readonly="true" name="orShipDateToStr" cssClass="datepic" id="orShipDateTo" 
														value="%{submissionDateTo}" cssClass="datePicWOMinDate" onchange="clearerreor(this.value,'totallblerror');"/>								
											</td>
										</tr>
									</table>
								</div>
							</td>
						   </tr>
						</table>             
		             </fieldset>		         
		         </td>
		         <td></td>
		       </tr>
		    </table>
		</td>
		</tr>
		
	
		
		<tr>
		   <td colspan="2">
		    <table>
		      <tr>
		         <td colspan="2" align="center"><span class="error"><label id="totallblerror"></label> </span></td>
		      </tr>
		      <tr>
				<td colspan="2">
				<button type="button" onclick="javascript:checkids();"><s:text
					name="garuda.queryBuilder.label.submitQuery" ></s:text></button>
				<button type="button" onclick="hideCBBDropDown()"><s:text
					name="garuda.advancelookup.label.reset"></s:text></button>
				<button type="button" onclick="cancelQueryRequest()"><s:text
					name="garuda.common.lable.cancel"></s:text></button>
				</td>
		     </tr>
		   </table>
		  </td>
		</tr>
	</table>	
</s:form>

</div>
</div>
</div>
</div>
</td>
</tr>
</table>
</div>
    <div id="simpleCBUReportID">
       
	</div>
