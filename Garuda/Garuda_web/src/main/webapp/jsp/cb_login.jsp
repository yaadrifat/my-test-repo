<%@ taglib prefix="s"  uri="/struts-tags"%>
<script type="text/javascript">
function cleanData(){
	$j("#forgotuserName").val("");
	$j("#email").val("");
	$j("#errormesssagetd").html("");
}
$j(function() {
	  $j( "#forgotpasswddiv").dialog(
	   {autoOpen: false,
	   	title: '<s:text name="garuda.common.login.forgotPassword"/> ',
	    modal: true, width:500, height:300,
	    close : function(){
	    	cleanData();  
	      }
	    }
	   );
	 }); 

	 function showpopup(){
		 $j("#forgotpasswddiv").dialog("open");
	}
	 $j(document).ready(function(){
			
			setTimeout(function() { $j('#hideMsg').fadeOut('fast'); }, 10000);
			
			
		});
	
</script>

<div id="main">
<div class="col_100 maincontainer "><div class="col_100">
<p>&nbsp;</p>
<br>
<p>&nbsp;</p>
<br>
<div class="loginform">
<s:form action="getLoginDetails" method="Post" id="login" class="tabledisplay">
<table id="login" width="100%" border="0" cellspacing="0"
	cellpadding="0">
	<tr>
		<td colspan="2" class="titles"><s:text name="garuda.common.label.signIn"/></td>
	</tr>
	<tr>
		<td style="vertical-align: middle"><s:text name="garuda.cdrcbuview.label.user_name"/>:</td>
		<td><input type="text" name="username" class="left" id="username" required /></td>
	</tr>
	<tr>
		<td style="vertical-align: middle"><s:text name="garuda.common.label.password"/>:</td>
		<td><input type="password" name="password" class="left"	id="password" required /></td>
	</tr>
	<tr>
		<td colspan="2" class="align_center" id="errormessagetd">
		<span class="error" id="hideMsg" >
				  <s:property value="errorMessage"></s:property>
		</span>
		</td>
	</tr>
	<tr>
		<td></td>
		<td colspan="1" class="align_left"><input type="submit"
			value="<s:text name="garuda.common.label.signIn"/>" /> &nbsp;
		<input type="button" onclick="clearFields();" value="<s:text name="garuda.common.label.clear"/>" />
		</td>
	</tr>
	<tr>
		<td colspan="2" class="align_right"><a href="#"
			onclick="showpopup();"> <s:text name="garuda.common.login.forgotPassword"/>?</a></td>
	</tr>
	<tr>
		<td colspan="2" class="align_right " id="versiontd"></td>
	</tr>
</table>
</s:form>
</div>
</div></div>
</div>
<!-- Forgot Password -->
<div id="forgotpasswddiv" style="display: none;"><s:form id="forgotform" method="post">
	<table>
		<tr>
			<td><s:text name="garuda.cdrcbuview.label.user_name"/> :</td>
			<td><input type="text" id="forgotuserName" name="forgotuserName" /></td>
		</tr>
		<tr>
			<td><s:text name="garuda.common.label.email"/> :</td>
			<td><input type="text" id="email" name="email" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center" id="errormesssagetd"></td>
		</tr>
		<tr>
			<td colspan="2" align="center">
			<button type="button" onclick="javascript:return forgotpassword();"><s:text name="garuda.unitreport.label.button.submit"/></button>
			</td>
		</tr>
	</table>
</s:form></div>