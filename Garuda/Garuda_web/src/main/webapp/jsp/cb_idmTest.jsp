<%@taglib prefix="s" uri="/struts-tags"%>
<%@ page import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB,com.velos.ordercomponent.util.GetResourceBundleData" %>
<jsp:include page="./cb_track_session_logging.jsp" />
<%
String contextpath=request.getContextPath();
%>
<script type="text/javascript" src="<%=contextpath%>/jsp/js/jquery/jquery.ui.datepicker.validation.js"></script>
<% GetResourceBundleData propData = new GetResourceBundleData();%>
<div id="dynaFormDiv">
<s:form id="dynamicForm" >
<s:hidden name="cordCbuStatus"> </s:hidden>
<s:hidden name="autoDeferExecFlag" id="idmTestAutoDeferExecFlag"></s:hidden>
<s:hidden name="entityId" id="cordID" ></s:hidden>
<s:hidden name="fromViewClinicalidm" id="fromViewClinicalidm" ></s:hidden>
<s:hidden name="subentityType" id="subentityType"></s:hidden>
<s:hidden name="formId" id="formId" value="%{formId}"></s:hidden>
<s:hidden name="isCordEntry" id="isCordEntry"></s:hidden>
<s:hidden name="isCompInfo" id="isCompInfo"></s:hidden>
<s:hidden name="idmlabel" value="%{@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_IDM}" id="idmlabel"></s:hidden>
<s:hidden name="listsize" value="%{mrqFormList.size()}" id="listsize"/>
<s:hidden name="seqnumber" value="%{seqnumber+1}" id="seqnumber"></s:hidden>
<s:hidden name="iseditable" id="iseditable"/>
<s:hidden name="idmsubquesnonepk" id="idmsubquesnonepk" value="%{#request.idmsubquesnonepk}"></s:hidden>
<s:hidden name="idmsubquesypk" id="idmsubquesypk" value="%{#request.idmsubquesypk}"></s:hidden>
<s:hidden name="idmsubquessopk" id="idmsubquessopk" value="%{#request.idmsubquessopk}"></s:hidden>
<s:hidden name="idmSubQY" id="idmSubQY" value="%{#request.idmSubQY}"></s:hidden>
<s:hidden name="idmSubQN" id="idmSubQN" value="%{#request.idmSubQN}"></s:hidden>
<s:hidden name="pkformversion" id="pkformversion" value="%{formversion.pkformversion}"/>
<s:hidden name='iscdrMember' id='iscdrMember'></s:hidden>
<s:hidden name="notdonepk" id="idmnotdonepk"/>
<s:hidden name="notdonepk1" id="idmnotdonepk1"/>
<s:hidden name="idmpositivePk" id="idmpositivepk" value="%{#request.idmpositivePk}"/>
<s:hidden name="formvers" id="formvers"></s:hidden>
<s:hidden id="regid" value="%{cdrCbuPojo.registryId}"/>
<s:hidden name="isCordEntryPage" id="isCordEntryPage"/>
<s:hidden name="autoDeferFlag" id="autoDeferFlag"></s:hidden>
<s:hidden name="idmtempDeferId" id="idmtempDeferId"></s:hidden>
<s:hidden name="CdrCbuPojo.specimen.specCollDate" id="cbucollectiondateForIdm"></s:hidden>
<s:date name="CdrCbuPojo.specimen.specCollDate" id="cbucollectiondateForIdm" format="MMM dd, yyyy" />
<s:hidden value="%{cbucollectiondateForIdm}" id="cbucollectiondateForIdm" name="cbucollectiondateForIdm"/>
<s:hidden id="idmaddNewtestFlag"/>
<s:hidden id="IDMautoPopulateDate" name="IDMautoPopulateDate"/>
<s:hidden id="IDMautoPopulateDateFld" name="IDMautoPopulateDateFld"/>
<div id="dynaUpdate" style="display: none;" >
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<%--  <td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p>
					<span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
					<strong>
						<s:property value="%{formId}"/>&nbsp;<%=propData.getPropertiesData("garuda.label.dynamicform.savemsg") %>
				   <label id="normalSaveMsg"><s:property value="%{formId}"/>&nbsp;<%=propData.getPropertiesData("garuda.label.dynamicform.savemsg") %></label>
					    <label id="autoDeferSaveMsg"><s:property value="%{formId}"/>&nbsp;<%=propData.getPropertiesData("garuda.label.dynamicform.savemsg") %>&nbsp;and&nbsp;<%=propData.getPropertiesData("garuda.unitreport.label.thankyoucordautodefer") %></label>
				</strong>
				</p>
			</div>
		</td>--%>
		
		<td colspan="2">
			<div style="margin-top: 10px; padding: 0.5em 0.5em;" class="ui-state-highlight ui-corner-all"> 
				<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-check"></span>
				<strong><s:property value="%{formId}"/>&nbsp;<%=propData.getPropertiesData("garuda.label.dynamicform.savemsg") %></strong></p>
			</div>
		</td>
		
	</tr>
	<tr>
		<td>
		</td>
		<td>
			<input type="button" onclick="closeModals('<s:property value="%{formId}"/>ModalForm')" value="<%=propData.getPropertiesData("garuda.common.close") %>" />
			<%--
commented while merging
<input type="button" id="normalFormClose" onclick="closeModals('<s:property value="%{formId}"/>ModalForm')" value="<%//=propData.getPropertiesData("garuda.common.close") %>" />
			<input type="button" id="formAutoDeferClose" onclick="autoDeferCloseModals('<s:property value="%{formId}"/>ModalForm')" value="<%=propData.getPropertiesData("garuda.common.close") %>" /> --%>
		</td>
	</tr>
</table>		
</div>
<div class="dynamicForm">
	<div id="dynaInfo">
	<s:hidden name ="assess_ncd" id="assess_ncd" value="%{#request.assess_ncd}"/>
	<div>
	<table width="100%" cellpadding="0" cellspacing="0" class="displaycdr" id="formsTable">
		<thead>
			<tr>
				<th width="5%"><%=propData.getPropertiesData("garuda.label.dynamicform.question") %></th>
				<th width="65%"><%=propData.getPropertiesData("garuda.label.dynamicform.description") %></th>
				<th width="30%"><%=propData.getPropertiesData("garuda.label.dynamicform.response") %></th>
			</tr>
			<tr bgcolor="#E6E6E5" id="<s:property value='%{formId}'/>_sourceTr">
				<td></td>
				<td>
					<%=propData.getPropertiesData("garuda.cbuentry.label.source") %>
					<span class='error' id="dynaFormsourceMan">
						<%=propData.getPropertiesData("garuda.label.dynamicform.astrik") %>
					</span>
				</td>
				<td>
				
					<select id="fktestsource" name="formversion.entityType" onchange="fieldclear(this);isChangeDone(this,'<s:property value="formversion.entityType"/>','idmchangeClass');" class="iseditable IsMandatory isReqPriorToShip" tabindex="1">
						<option value="">Select</option>
						<s:iterator value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@TEST_SOURCES]">
							<s:if test="%{formvers=='N2F'}">
								<s:if test="%{pkCodeId==#request.pkmaternal}">
										<option value='<s:property value='pkCodeId'/>' selected="selected"><s:property value="%{description}"/></option>
								</s:if>
							</s:if>
							<s:else>
							<s:if test="%{pkCodeId!=#request.pkpatient}">
								<s:if test="%{formversion.entityType!=null}">
									<option value='<s:property value='pkCodeId'/>' selected="selected"><s:property value="%{description}"/></option>
								</s:if>
								<s:else>
									<option value='<s:property value='pkCodeId'/>'><s:property value="%{description}"/></option>
								</s:else>
							</s:if>
							</s:else>
						</s:iterator>
					</select>
				<span class='error fieldMan' style="display:none" id="errorMsg_fktestsource">
					<%=propData.getPropertiesData("garuda.label.dynamicform.errormsg") %>
				</span>
				</td>
			</tr>
			<tr bgcolor="#E6E6E5" id="<s:property value='%{formId}'/>_dateTr">
				<td></td>
				<td>
					<%=propData.getPropertiesData("garuda.label.dynamicform.bldcollectdate") %>
					<span class='error' id="dynaFormDateMan">
						<%=propData.getPropertiesData("garuda.label.dynamicform.astrik") %></span>
				</td>
				<td>
						<s:textfield name="dynaFormDate" id="%{formId}dynaFormDate" tabindex="2"
							cssClass="datePicWMaxDate iseditable IsMandatory isReqPriorToShip " onchange="validateIDMBloodCollDate('%{cbucollectiondateForIdm}',this.value,'bldWrngMsg');fieldclear(this);isChangeDone(this,'%{formversion.dynaFormDateStr}','idmchangeClass');"
							value="%{formversion.dynaFormDateStr}" cssStyle="width:75% !important;vertical-align: inherit;"/>
					<span class='error fieldMan' style="display:none" id="dynaFormDateError">
						<br/><%=propData.getPropertiesData("garuda.label.forms.datealertmsg") %>
					</span>
					<span class='error fieldMan' style="display:none" id="bldWrngMsg">
						<br/><%=propData.getPropertiesData("garuda.label.dynamicform.bldWrngMsg") %>
					</span>
					<span class='error fieldMan' style="display:none" id="errorMsg_<s:property value='formId'/>dynaFormDate">
						<br/><%=propData.getPropertiesData("garuda.label.dynamicform.errormsg") %>
					</span>
				</td>
			</tr>
		</thead>
		<tbody id="idmTableBody">
		<s:set name="indexval" value="0"></s:set>
		<s:set name="row" value="1"></s:set>
		<s:iterator value="%{quesGrp}" var="grpval">
		<s:if test="%{#grpval[1]!=null}">
			<tr bgcolor="#E6E6E5">
				<td colspan="3"><s:property value="%{#grpval[1]}"/> </td>
			</tr>
		</s:if>
		<s:iterator value="%{mrqFormList}" var="rowval">
		<s:if test="%{#request.formpk==null}">
			<s:hidden name="fkform" id="fkform" value="%{#rowval[25]}"></s:hidden>
			<s:set name="formpk" scope="request" value="%{#rowval[25]}"></s:set>
		</s:if>
		<s:if test="%{#grpval[0]==#rowval[22]}">
		<s:if test="%{#rowval[3]==null}">
			<tr id="Question_<s:property value='%{#row}'/>" class="<s:if test='%{#grpval[1]=="Additional / Miscellaneous IDM Tests"}'><s:if test='%{#rowval[16]==null}'>hidechild additionalIDMTest</s:if></s:if>">
				<td width="5%" align="center" style="vertical-align: top;" id="Question_<s:property value='%{#row}'/>_td">
					<s:hidden id="%{formId}_formDate" value="%{#rowval[21]}"></s:hidden>
					<s:hidden name="%{formId}_Question[%{#indexval}].pkresponse" value="%{#rowval[13]}"></s:hidden>
					<s:hidden id="%{formId}_fkform" name="%{formId}_Question[%{#indexval}].fkform" value="%{#rowval[25]}"></s:hidden>
					<s:hidden name="%{formId}_Question[%{#indexval}].fkquestion" value="%{#rowval[0]}"
							  id="%{formId}_Ques%{#row}_s_fktestid"></s:hidden>
					<s:hidden name="shrtName" value="%{formId}_Ques%{#row}_s" id="%{#rowval[0]}_assessshname"/>
					<s:hidden name="subEntityId" value="%{#rowval[13]}" />
					<s:hidden name="formflag" value="true" />
					<s:hidden name="questionNo" value="%{formId} Q#%{#rowval[24]}" />
					<s:hidden name="fieldIndex" value="%{#rowval[0]}" id="%{formId}_index%{#indexval}" />
					<s:hidden name="assesCon" value="%{#rowval[7]}" id="%{formId}_assesCon%{#indexval}" />
					<s:hidden name="unreqFlag" value="%{#rowval[23]}" id="%{formId}_unreqFlag%{#rowval[0]}" />
					<s:hidden name="unreqPreFlag" value="%{#rowval[23]}" id="%{formId}_unreqPreFlag%{#rowval[0]}" />
					<s:hidden name="assessmentPk" value="%{#rowval[19]}" id="assessmentPk_%{#rowval[0]}" />
					<s:hidden name="assessDivName1" id='assessDivName_%{#rowval[0]}' value="%{formId}_Ques%{#row}_s_assessmentDiv"/>
					<s:hidden name="%{formId}_Question[%{#indexval}].commentbox" id="%{formId}_comment_%{#indexval}" value="%{#rowval[17]}"></s:hidden>
					<s:hidden id="%{formId}_QuestionRes%{#indexval}_prv" value="%{#rowval[15]}"></s:hidden>
					<s:hidden id="%{formId}_QuestionRes%{#indexval}_index" value="%{#indexval}"></s:hidden>
					
					<s:property value="%{#rowval[24]}"/>
					<s:if test="%{#rowval[20]=='cms_cert_lab_ind'}">
						<s:set name='cmslab' value="%{#rowval[15]}" scope="request"></s:set>
						<s:hidden name="cmslab" id="cmslab" value="%{#request.cmslab}"/>
					</s:if>
					<s:if test="%{#rowval[20]=='fda_lic_mfg_inst_ind'}">
						<s:set name='fdalic' value="%{#rowval[15]}" scope="request"></s:set>
						<s:hidden name="fdalic" id="fdalic" value="%{#request.fdalic}"/>
					</s:if>  
				</td>
				<td width="70%" >
					<div>
						<s:if test="%{#rowval[4]!=null}">	
							<span style="padding-right:5px">
								<img height="15px" src="./images/help_24x24px.png" onmouseout="return nd();"
									onmouseover="return overlib('<s:if test="%{#rowval[20]=='nat_hiv_1_hcv_hbv_mpx_ind' && isFMHQNewForm==false}"><s:text name="garuda.idmreport.helpMsg.10question"/></s:if><s:else><s:property value='%{#rowval[4]}'/></s:else>',CAPTION,'<%=propData.getPropertiesData("garuda.label.dynamicform.Help") %>',ABOVE);">
							</span>
						</s:if>
					<span <s:if test='%{#rowval[5]!=null}'>onmouseout="return nd();" onmouseover="return overlib('<s:property value='%{#rowval[5]}'/>',CAPTION,'<%=propData.getPropertiesData("garuda.label.dynamicform.Help") %>',ABOVE);"</s:if>>
						<s:if test="%{#grpval[1]!='Additional / Miscellaneous IDM Tests'}">
							<s:property value="%{#rowval[1]}"/>
						</s:if>
						<s:if test="%{#rowval[20]=='nat_hiv_1_hcv_hbv_mpx_ind' && isFMHQNewForm==false}">
							&nbsp;<%=propData.getPropertiesData("garuda.idmreport.txt.mpx") %>&nbsp;
						</s:if>
						<s:if test="%{#grpval[1]=='Additional / Miscellaneous IDM Tests'}">
							<s:textfield id="%{formId}_TestNO%{#indexval}" name="%{formId}_Question[%{#indexval}].responsevalue"  onchange="fieldclear(this);isChangeDone(this,'%{#rowval[16]}','idmchangeClass');"
								cssClass="iseditable thisquesReq tabclass" value="%{#rowval[16]}" cssStyle="width:45%" maxlength="30" onkeydown="setFocusToRes(event,'%{formId}_QuestionRes%{#indexval}');"/>
							<span id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#rowval[0]}'/>" class="error <s:property value='%{formId}'/>_classMan<s:property value='%{#rowval[11]}'/><s:property value='%{#rowval[12]}'/>">
								*
							</span>
						</s:if>	
					</span>
					<s:if test="%{#grpval[1]!='Additional / Miscellaneous IDM Tests'}">
						<s:if test="%{#rowval[11]!=null}">
							<s:if test="%{#rowval[29]==1}">
								<s:if test="%{#rowval[20]=='hiv_antigen_rslt'}">
									<s:if test="%{#rowval[30]==1}">
										<span id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#rowval[0]}'/>" class="error <s:property value='%{formId}'/>_classMan<s:property value='%{#rowval[11]}'/><s:property value='%{#rowval[12]}'/> classQues12">
											*
										</span>
									</s:if>
									<s:else>
										<span id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#rowval[0]}'/>" class="error <s:property value='%{formId}'/>_classMan<s:property value='%{#rowval[11]}'/><s:property value='%{#rowval[12]}'/> classQues12" style="display:none;">
											*
										</span>
									</s:else>
								</s:if>
								<s:else>
									<span id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#rowval[0]}'/>" class="error <s:property value='%{formId}'/>_classMan<s:property value='%{#rowval[11]}'/><s:property value='%{#rowval[12]}'/>">
										*
									</span>
								</s:else>
							</s:if>
							<s:else>
								<span id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#rowval[0]}'/>" class="error <s:property value='%{formId}'/>_classMan<s:property value='%{#rowval[11]}'/><s:property value='%{#rowval[12]}'/>" style="display:none;">
									*
								</span>
							</s:else>
						</s:if>
						<s:else>
							<span id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#rowval[0]}'/>" class="error <s:property value='%{formId}'/>_classMan<s:property value='%{#rowval[11]}'/><s:property value='%{#rowval[12]}'/>" style="display:none;">
								*
							</span>
						</s:else>
					</s:if>
						<s:if test="%{#rowval[6]==1}">
							<a href="#" onclick="dynacomment(this);" style="text-decoration: none;white-space: nowrap;" tabindex="-1">
								<span id='spancomment<s:property value="%{#indexval}"/>'>
									<s:if test="%{#rowval[17]!=null}">
										<img height="15px" src="./images/addcomment.png">
										<%=propData.getPropertiesData("garuda.label.dynamicform.addorviewcomment") %>
									</s:if>
									<s:else>
										<s:if test="%{iseditable != 1}">
											<img height="15px" src="./images/addcomment.png">
											<%=propData.getPropertiesData("garuda.label.dynamicform.addcomment") %>
										</s:if>
									</s:else>
								</span>
							</a>
							<div class="commentbox" style="display: none;">
								<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
								<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
									<%=propData.getPropertiesData("garuda.label.dynamicform.commentbox") %>
								</div>
								
									<s:textarea name="%{formId}_Question[%{#indexval}].comment" cols="20" rows="5" value="%{#rowval[17]}" cssClass="iseditable" onkeypress="commentclear(this,%{#indexval});"
									cssStyle="width:95%" maxlength="3000" onchange="isChangeDone(this,'%{#rowval[17]}','idmchangeClass');"></s:textarea>
									<br/><span class='error' style="display:none" id='<s:property value="%{#indexval}"/>_Commenterror'><%=propData.getPropertiesData("garuda.label.dynamicform.errormsg") %></span>
									<br/><s:if test="%{iseditable!=1}">
									<input type="button" onclick="saveComment(this,'<s:property value="%{#indexval}"/>')" value="<%=propData.getPropertiesData("garuda.common.save") %>" tabindex="1"/>
									</s:if>
									<input type="button" onclick="hideDynaComment(this,'<s:property value="%{#indexval}"/>')" value="<%=propData.getPropertiesData("garuda.common.close") %>" tabindex="2"/>&nbsp;&nbsp;
								</div>
							</div>
						</s:if>
						<br/>
						<span class='error fieldMan' style="display:none" id='errorMsg_<s:property value="%{formId}"/>_TestNO<s:property value="%{#indexval}"/>'>
								<%=propData.getPropertiesData("garuda.label.dynamicform.errormsg2") %>
						</span>
					</div>
					<s:if test="%{#rowval[7]==1}">
						<!-- Loading Assessment part -->
						<div id="assess_<s:property value='%{#rowval[0]}'/>" style="display: none;" class="assessMentDiv">
							
						</div>
					</s:if>
				</td>
				<td  width="25%" <s:if test="%{#grpval[1]==null}">id="IdmQ_<s:property value='%{#row}'/>"</s:if> style="vertical-align: top;">
				<s:if test="%{#rowval[9]!=null}">
					<div style="display: none;">
						<s:select id="Ques%{#rowval[0]}_UNRes" style="display:none" 
						list="%{#application.codeListValues[#rowval[9]]}" listKey="pkCodeId" listValue="description"/>
					</div>
				</s:if>
				<s:if test="%{#rowval[11]!=null}">
					<s:generator val="%{#rowval[12]}" separator=",">
					<select style="display:none" 
						id="<s:property value='%{formId}'/>_depentQVal_<s:property value='%{#rowval[11]}'/>">
						<s:iterator>
  							<option value='<s:property/>'><s:property/></option>
						</s:iterator>
					</select>
					</s:generator>
				</s:if>
				<s:if test="%{#rowval[27]!=null}">
					<s:generator val="%{#rowval[27]}" separator=",">
					<select style="display:none" 
						id="<s:property value='%{formId}'/>_deferVal_<s:property value='%{#indexval}'/>">
						<s:iterator>
  							<option value='<s:property/>'><s:property/></option>
						</s:iterator>
					</select>
					</s:generator>
				</s:if>
				<s:if test="%{getCodeListDescById(#rowval[2])==@com.velos.ordercomponent.util.VelosGarudaConstants@DROPDOWN_FIELD}">
				<s:if test="%{#rowval[8]!=null}">
					<s:if test="%{#grpval[1]!=null}">
						<s:if test="%{#grpval[1]!='Additional / Miscellaneous IDM Tests'}">
						<s:if test="%{#rowval[11]!=null}">
							<s:if test="%{#rowval[29]==1}">
								<s:if test="%{#rowval[20]=='hiv_antigen_rslt'}">
									<s:if test="%{#rowval[30]==1}">
										<s:select name="%{formId}_Question[%{#indexval}].responsecode" id="%{formId}_QuestionRes%{#indexval}" 
												list="%{#application.codeListValues[#rowval[8]]}" listKey="pkCodeId" listValue="description" headerKey="" 
												headerValue="Select" tabindex="%{#indexval+3}" 
												onchange="checkDefervalue(this,'%{formId}','%{#indexval}','%{#rowval[15]}');
															showDynaAssess('%{#rowval[0]}',this.id,%{#rowval[7]},'1');
															checkDependentques(this.id,'%{#rowval[0]}','%{formId}');fieldclear(this);
															checkDependent(this.id,'%{#rowval[0]}','%{formId}');
															isChangeDone(this,'%{#rowval[15]}','idmchangeClass');"
												cssClass="%{formId}_dependclass%{#rowval[11]} findlen iseditable %{formId}_Ques%{#row}_s_res checkdeferclass" value="%{#rowval[15]}"/>
									</s:if>
									<s:else>
										<s:select name="%{formId}_Question[%{#indexval}].responsecode" id="%{formId}_QuestionRes%{#indexval}" 
												list="%{#application.codeListValues[#rowval[8]]}" listKey="pkCodeId" listValue="description" headerKey="" 
												headerValue="Select" tabindex="%{#indexval+3}" 
												onchange="checkDefervalue(this,'%{formId}','%{#indexval}','%{#rowval[15]}');
															showDynaAssess('%{#rowval[0]}',this.id,%{#rowval[7]},'1');
															checkDependentques(this.id,'%{#rowval[0]}','%{formId}');fieldclear(this);
															checkDependent(this.id,'%{#rowval[0]}','%{formId}');
															isChangeDone(this,'%{#rowval[15]}','idmchangeClass');"
												cssClass="%{formId}_dependclass%{#rowval[11]} findlen iseditable %{formId}_Ques%{#row}_s_res disableclass checkdeferclass" value="%{#rowval[15]}"/>
									</s:else>
								</s:if>
								<s:else>
									<s:select name="%{formId}_Question[%{#indexval}].responsecode" id="%{formId}_QuestionRes%{#indexval}" 
											list="%{#application.codeListValues[#rowval[8]]}" listKey="pkCodeId" listValue="description" headerKey="" 
											headerValue="Select" tabindex="%{#indexval+3}" 
											onchange="checkDefervalue(this,'%{formId}','%{#indexval}','%{#rowval[15]}');
														showDynaAssess('%{#rowval[0]}',this.id,%{#rowval[7]},'1');
														checkDependentques(this.id,'%{#rowval[0]}','%{formId}');fieldclear(this);
														checkDependent(this.id,'%{#rowval[0]}','%{formId}');
														isChangeDone(this,'%{#rowval[15]}','idmchangeClass');"
											cssClass="%{formId}_dependclass%{#rowval[11]} findlen iseditable %{formId}_Ques%{#row}_s_res checkdeferclass" value="%{#rowval[15]}"/>
								</s:else>
							</s:if>
							<s:else>
								<s:select name="%{formId}_Question[%{#indexval}].responsecode" id="%{formId}_QuestionRes%{#indexval}" 
										list="%{#application.codeListValues[#rowval[8]]}" listKey="pkCodeId" listValue="description" headerKey="" 
										headerValue="Select" tabindex="%{#indexval+3}" 
										onchange="checkDefervalue(this,'%{formId}','%{#indexval}','%{#rowval[15]}');
													showDynaAssess('%{#rowval[0]}',this.id,%{#rowval[7]},'1');
													checkDependentques(this.id,'%{#rowval[0]}','%{formId}');fieldclear(this);
													checkDependent(this.id,'%{#rowval[0]}','%{formId}');
													isChangeDone(this,'%{#rowval[15]}','idmchangeClass');"
										cssClass="%{formId}_dependclass%{#rowval[11]} findlen iseditable %{formId}_Ques%{#row}_s_res disableclass checkdeferclass" value="%{#rowval[15]}"/>
							</s:else>
						</s:if>
						<s:else>
							<s:select name="%{formId}_Question[%{#indexval}].responsecode" id="%{formId}_QuestionRes%{#indexval}" 
									list="%{#application.codeListValues[#rowval[8]]}" listKey="pkCodeId" listValue="description" headerKey="" 
									headerValue="Select" tabindex="%{#indexval+3}" 
									onchange="checkDefervalue(this,'%{formId}','%{#indexval}','%{#rowval[15]}');
												showDynaAssess('%{#rowval[0]}',this.id,%{#rowval[7]},'1');
												checkDependentques(this.id,'%{#rowval[0]}','%{formId}');fieldclear(this);
												checkDependent(this.id,'%{#rowval[0]}','%{formId}');
												isChangeDone(this,'%{#rowval[15]}','idmchangeClass');"
									cssClass="%{formId}_dependclass%{#rowval[11]} findlen iseditable %{formId}_Ques%{#row}_s_res checkdeferclass" value="%{#rowval[15]}"/>
						</s:else>
					</s:if>
					<s:else>
						<s:select name="%{formId}_Question[%{#indexval}].responsecode" id="%{formId}_QuestionRes%{#indexval}" 
									list="%{#application.codeListValues[#rowval[8]]}" listKey="pkCodeId" listValue="description" headerKey="" 
									headerValue="Select" tabindex="%{#indexval+3}" 
									onchange="checkDefervalue(this,'%{formId}','%{#indexval}','%{#rowval[15]}');
												showDynaAssess('%{#rowval[0]}',this.id,%{#rowval[7]},'1');
												checkDependentques(this.id,'%{#rowval[0]}','%{formId}');fieldclear(this);
												checkDependent(this.id,'%{#rowval[0]}','%{formId}');
												isChangeDone(this,'%{#rowval[15]}','idmchangeClass');"
									cssClass="%{formId}_dependclass%{#rowval[11]} findlen IsMandatory isReqPriorToShip iseditable %{formId}_Ques%{#row}_s_res checkdeferclass" value="%{#rowval[15]}"/>
					</s:else>
					</s:if>
					<s:else>
						<s:select name="%{formId}_Question[%{#indexval}].responsecode" id="%{formId}_QuestionRes%{#indexval}" 
								list="%{#application.codeListValues[#rowval[8]]}" listKey="pkCodeId" listValue="description" headerKey="" 
								headerValue="Select"  tabindex="%{#indexval+3}"
								onchange="showDynaAssess('%{#rowval[0]}',this.id,%{#rowval[7]},'1');fieldclear(this);checkques('%{formId}');isChangeDone(this,'%{#rowval[15]}','idmchangeClass');"
								cssClass="%{formId}_dependclass%{#rowval[11]} findlen iseditable %{formId}_Ques%{#row}_s_res checkdeferclass" value="%{#rowval[15]}"/>
					</s:else>
				</s:if>
				</s:if>
				<s:if test="%{getCodeListDescById(#rowval[2])==@com.velos.ordercomponent.util.VelosGarudaConstants@TEXT_FIELD}">
					<s:if test="%{#rowval[11]!=null}">
						<s:if test="%{#rowval[29]==1}">
							<s:textfield name="%{formId}_Question[%{#indexval}].dynaFormDateStr"  onchange="fieldclear(this);validateCordDates('IDMdynaFormDate',this.id,'dynaFormDateError');isChangeDone(this,'%{#rowval[21]}','idmchangeClass');isFirstDateChange(this);" readonly="true" onkeydown="cancelBack();"  
										tabindex="%{#indexval+3}" id="%{formId}_QuestionRes%{#indexval}"
										cssClass="%{#rowval[11]}_dependclass%{#rowval[11]} findlen iseditable custIDMdatePic" 
										value="%{#rowval[21]}" cssStyle="width:85%"/>
						</s:if>
						<s:else>
							<s:textfield name="%{formId}_Question[%{#indexval}].dynaFormDateStr"  onchange="fieldclear(this);validateCordDates('IDMdynaFormDate',this.id,'dynaFormDateError');isChangeDone(this,'%{#rowval[21]}','idmchangeClass');isFirstDateChange(this);" readonly="true" onkeydown="cancelBack();"  
										tabindex="%{#indexval+3}" id="%{formId}_QuestionRes%{#indexval}" disabled="true"
										cssClass="%{#rowval[11]}_dependclass%{#rowval[11]} findlen iseditable custIDMdatePic" 
										value="%{#rowval[21]}" cssStyle="width:85%"/>
						</s:else>
					</s:if>
					<s:else>
						<s:textfield name="%{formId}_Question[%{#indexval}].dynaFormDateStr"  onchange="fieldclear(this);validateCordDates('IDMdynaFormDate',this.id,'dynaFormDateError');isChangeDone(this,'%{#rowval[21]}','idmchangeClass');isFirstDateChange(this);" readonly="true" onkeydown="cancelBack();"  
									tabindex="%{#indexval+3}" id="%{formId}_QuestionRes%{#indexval}"
									cssClass="%{#rowval[11]}_dependclass%{#rowval[11]} findlen iseditable custIDMdatePic" 
									value="%{#rowval[21]}" cssStyle="width:85%"/>
					</s:else>
				</s:if>
					<span class='error fieldMan' style="display:none" id='errorMsg_<s:property value="%{formId}"/>_QuestionRes<s:property value="%{#indexval}"/>'>
						<%=propData.getPropertiesData("garuda.label.dynamicform.errormsg") %>
					</span>
					<span class='error  <s:property value="%{formId}"/>_Ques<s:property value="%{#row}"/>_s_assessError' style="display:none" id="assesError_<s:property value='%{#rowval[0]}'/>">
						<%=propData.getPropertiesData("garuda.label.dynamicform.assessmentmsg") %>
					</span>
					<div>
						<a id="showasses_<s:property value='%{#rowval[0]}'/>" href="#" onclick="showDynaAssessLink(this,'<s:property value="%{#rowval[0]}"/>','<s:property value="%{formId}"/>_QuestionRes<s:property value="%{#indexval}"/>',<s:property value='%{#rowval[7]}'/>,'1');"
							style="display: none;" class="<s:property value="%{formId}"/>_Ques<s:property value="%{#row}"/>_s_showassess">
							<%=propData.getPropertiesData("garuda.label.dynamicform.showassess") %>
						</a>
						<a id="hideasses_<s:property value='%{#rowval[0]}'/>" href="#" onclick="hideDynaAssess(this,'<s:property value="%{#rowval[0]}"/>');" style="display: none;"
							class="<s:property value="%{formId}"/>_Ques<s:property value="%{#row}"/>_s_hideassess">
							<%=propData.getPropertiesData("garuda.label.dynamicform.hideassess") %>
						</a>
					</div>
				</td>
			</tr>
			<s:set name="indexval" value="%{#indexval+1}"></s:set>
		</s:if>
		<s:set name="subrow" value="1"/>
		<s:iterator value="%{mrqFormList}" var="secrowval">
		<s:if test="%{#rowval[0]==#secrowval[3]}">
			<tr id="Question_<s:property value='%{#row}'/>_<s:property value='%{#subrow}'/>" class="<s:if test='%{#grpval[1]=="Additional / Miscellaneous IDM Tests"}'><s:if test='%{#rowval[16]==null}'>hidechild additionalIDMTest</s:if></s:if>">
				<td width="5%" align="center" style="vertical-align: top;" id="Question_<s:property value='%{#row}'/>_<s:property value='%{#subrow}'/>_td">
					<s:hidden name="%{formId}_Question[%{#indexval}].pkresponse" value="%{#secrowval[13]}"></s:hidden>
					<s:hidden name="%{formId}_Question[%{#indexval}].fkform" value="%{#secrowval[25]}"></s:hidden>
					<s:hidden name="%{formId}_Question[%{#indexval}].fkquestion" value="%{#secrowval[0]}"
					id="%{formId}_Ques%{#row}%{#subrow}_s_fktestid"></s:hidden>
					<s:hidden name="shrtName" value="%{formId}_Ques%{#row}%{#subrow}_s" id="%{#rowval[0]}_assessshname"/>
					<s:hidden name="subEntityId" value="%{#secrowval[13]}" />
					<s:hidden name="formflag" value="true" />
					<s:hidden name="questionNo" value="%{formId} Q#%{#secrowval[24]}" />
					<s:hidden name="fieldIndex" value="%{#secrowval[0]}" id="%{formId}_index%{#indexval}" />
					<s:hidden name="assesCon" value="%{#secrowval[7]}" id="%{formId}_assesCon%{#indexval}" />
					<s:hidden name="unreqFlag" value="%{#secrowval[23]}" id="%{formId}_unreqFlag%{#secrowval[0]}" />
					<s:hidden name="unreqPreFlag" value="%{#secrowval[23]}" id="%{formId}_unreqPreFlag%{#secrowval[0]}" />
					<s:hidden name="assessmentPk" value="%{#secrowval[19]}" id="assessmentPk_%{#secrowval[0]}" />
					<s:hidden name="assessDivName1" id="assessDivName_%{#secrowval[0]}" value="%{formId}_Ques%{#row}%{#subrow}_s_assessmentDiv"/>
					<s:hidden name="%{formId}_Question[%{#indexval}].commentbox" id="%{formId}_comment_%{#indexval}" value="%{#secrowval[17]}"></s:hidden>
					<s:property value="%{#secrowval[24]}"/>
				</td>
				<td width="70%" style="text-align: right;">
					<div class="childquestions">
						<s:if test="%{#secrowval[4]!=null}">
							<span style="padding-right:5px">
								<img height="15px" src="./images/help_24x24px.png" onmouseout="return nd();"
									onmouseover="return overlib('<s:property value='%{#secrowval[4]}'/>',CAPTION,'<%=propData.getPropertiesData("garuda.label.dynamicform.Help") %>',ABOVE);">
							</span>
						</s:if>
					<span <s:if test='%{#secrowval[5]!=null}'>onmouseout="return nd();" onmouseover="return overlib('<s:property value='%{#secrowval[5]}'/>',CAPTION,'<%=propData.getPropertiesData("garuda.label.dynamicform.Help") %>',ABOVE);"</s:if>>
						<s:property value="%{#secrowval[1]}"/>
					</span>
					<s:if test="%{#secrowval[11]!=null}">
						<s:if test="%{#secrowval[29]==1}">
							<span id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#secrowval[0]}'/>" class="error <s:property value='%{formId}'/>_classMan<s:property value='%{#secrowval[11]}'/>">
								*
							</span>
						</s:if>
						<s:else>
							<span id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#secrowval[0]}'/>" class="error <s:property value='%{formId}'/>_classMan<s:property value='%{#secrowval[11]}'/>" style="display:none">
								*
							</span>
						</s:else>
					</s:if>
					<s:else>
						<span id="<s:property value='%{formId}'/>_mandatory<s:property value='%{#secrowval[0]}'/>" class="error <s:property value='%{formId}'/>_classMan<s:property value='%{#secrowval[11]}'/>" style="display:none">
							*
						</span>
					</s:else>
						<s:if test="%{#secrowval[6]==1}">
							<a href="#" onclick="dynacomment(this);" style="text-decoration: none;white-space: nowrap;" tabindex="-1">
								<span id='spancomment<s:property value="%{#indexval}"/>'>
									<s:if test="%{#secrowval[17]!=null}">
										<img height="15px" src="./images/addcomment.png">
										<%=propData.getPropertiesData("garuda.label.dynamicform.addorviewcomment") %>
									</s:if>
									<s:else>
										<s:if test="%{iseditable != 1}">
											<img height="15px" src="./images/addcomment.png">
											<%=propData.getPropertiesData("garuda.label.dynamicform.addcomment") %>
										</s:if>
									</s:else>
								</span>
							</a>
							<div class="commentbox" style="display: none;" align="left">
								<div class="ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"">
								<div class="ui-widget-header ui-widget-content ui-corner-all" style="text-align: center;">
									<%=propData.getPropertiesData("garuda.label.dynamicform.commentbox") %>
								</div>
									<s:textarea name="%{formId}_Question[%{#indexval}].comment" cols="20" rows="5" value="%{#secrowval[17]}" cssClass="iseditable" onkeypress="commentclear(this,%{#indexval});"
									onchange="isChangeDone(this,'%{#secrowval[17]}','idmchangeClass');" cssStyle="width:95%" maxlength="3000"></s:textarea>
									<br/><span class='error' style="display:none;text-align:left;width:100%;" id='<s:property value="%{#indexval}"/>_Commenterror'><%=propData.getPropertiesData("garuda.label.dynamicform.errormsg") %></span>
									<br/><s:if test="%{iseditable!=1}">
									<input type="button" onclick="saveComment(this,'<s:property value="%{#indexval}"/>')" value="<%=propData.getPropertiesData("garuda.common.save") %>" tabindex="1"/>
									</s:if>
									<input type="button" onclick="hideDynaComment(this,'<s:property value="%{#indexval}"/>')" value="<%=propData.getPropertiesData("garuda.common.close") %>" tabindex="2"/>
								</div>
							</div>
						</s:if>
					</div>
					<s:if test="%{#secrowval[7]==1}">
						<div id="assess_<s:property value='%{#secrowval[0]}'/>" style="display: none;text-align: left;" class="assessMentDiv" >
							
						</div>
					</s:if>
				</td>
				<td width="25%" style="vertical-align: top;">
				<s:if test="%{#secrowval[9]!=null}">
					<div style="display: none;">
						<s:select id="Ques%{#secrowval[0]}_UNRes" 
								list="%{#application.codeListValues[#secrowval[9]]}" 
								listKey="pkCodeId" listValue="description" style="display:none"/>
					</div>
				</s:if>
				<s:if test="%{#secrowval[11]!=null}">
				<s:generator val="%{#secrowval[12]}" separator=",">
					<select style="display:none" 
						id="<s:property value='%{formId}'/>_depentVal_<s:property value='%{#secrowval[11]}'/><s:if test='%{formId==@com.velos.ordercomponent.util.VelosGarudaConstants@FORM_FMHQ}'><s:property value='%{#secrowval[12]}'/></s:if>">
						<s:iterator>
  							<option value='<s:property/>'><s:property/></option>
						</s:iterator>
					</select>
					</s:generator>
				</s:if>
				<s:if test="%{getCodeListDescById(#secrowval[2])==@com.velos.ordercomponent.util.VelosGarudaConstants@DROPDOWN_FIELD}">
					<s:if test="%{#secrowval[8]!=null}">
						<s:if test="%{#secrowval[11]!=null}">
							<s:if test="%{#secrowval[29]==1}">
								<s:if test="%{#secrowval[20]=='hbsag_cms' ||#secrowval[20]=='anti_hbc_cms'||#secrowval[20]=='anti_hcv_cms'||#secrowval[20]=='anti_hiv_1_2_o_cms'||#secrowval[20]=='anti_hiv_1_2_cms'||#secrowval[20]=='anti_htlv1_cms'||#secrowval[20]=='cmv_total_cms'||#secrowval[20]=='nat_hiv_1_hcv_hbv_mpx_cms'||#secrowval[20]=='nat_hiv_react_cms'||#secrowval[20]=='hiv_antigen_cms'||#secrowval[20]=='nat_hcv_react_cms'||#secrowval[20]=='nat_hbv_react_cms'||#secrowval[20]=='syphilis_react_cms'||#secrowval[20]=='nat_west_nile_react_cms'||#secrowval[20]=='chagas_react_cms'||#secrowval[20]=='syphilis_ct_sup_react_cms'}">
									<s:if test="%{#request.cmslab==#request.idmsubquessopk}">
										<s:select name="%{formId}_Question[%{#indexval}].responsecode"
												id="%{formId}_QuestionRes%{#indexval}" tabindex="%{#indexval+3}"
												list="%{#application.codeListValues[#secrowval[8]]}" 
												listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
												onchange="showDynaAssess('%{#secrowval[0]}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
												isChangeDone(this,'%{#secrowval[15]}','idmchangeClass');"
												cssClass="%{formId}_unclassSQ%{#subrow}_%{#secrowval[11]} findlen iseditable subqfield%{#subrow}" value="%{#secrowval[15]}"/>
									</s:if>
									<s:elseif test="%{#request.cmslab==#request.idmsubquesypk}">
										<s:select name="%{formId}_Question[%{#indexval}].responsecode"
												id="%{formId}_QuestionRes%{#indexval}" tabindex="%{#indexval+3}"
												list="%{#application.codeListValues[#secrowval[8]]}" disabled="true"
												listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
												onchange="showDynaAssess('%{#secrowval[0]}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
												isChangeDone(this,'%{#request.idmSubQY}','idmchangeClass');" 
												cssClass="%{formId}_unclassSQ%{#subrow}_%{#secrowval[11]} findlen iseditable subqfield%{#subrow}" value="%{#request.idmSubQY}"/>
									</s:elseif>
									<s:elseif test="%{#request.cmslab==#request.idmsubquesnonepk}">
										<s:select name="%{formId}_Question[%{#indexval}].responsecode"
												id="%{formId}_QuestionRes%{#indexval}" tabindex="%{#indexval+3}"
												list="%{#application.codeListValues[#secrowval[8]]}" disabled="true"
												listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
												onchange="showDynaAssess('%{#secrowval[0]}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
												isChangeDone(this,'%{#request.idmSubQN}','idmchangeClass');"
												cssClass="%{formId}_unclassSQ%{#subrow}_%{#secrowval[11]} findlen iseditable subqfield%{#subrow}" value="%{#request.idmSubQN}"/>
									</s:elseif>
									<s:else>
										<s:select name="%{formId}_Question[%{#indexval}].responsecode"
												id="%{formId}_QuestionRes%{#indexval}" tabindex="%{#indexval+3}"
												list="%{#application.codeListValues[#secrowval[8]]}" disabled="true"
												listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
												onchange="showDynaAssess('%{#secrowval[0]}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
												isChangeDone(this,'%{#secrowval[15]}','idmchangeClass');"
												cssClass="%{formId}_unclassSQ%{#subrow}_%{#secrowval[11]} findlen iseditable subqfield%{#subrow}" value="%{#secrowval[15]}"/>
									</s:else>
								</s:if>
								<s:elseif test="%{#secrowval[20]=='hbsag_fda' ||#secrowval[20]=='anti_hbc_fda'||#secrowval[20]=='anti_hcv_fda'||#secrowval[20]=='anti_hiv_1_2_o_fda'||#secrowval[20]=='anti_hiv_1_2_fda'||#secrowval[20]=='anti_htlv1_fda'||#secrowval[20]=='cmv_total_fda'||#secrowval[20]=='nat_hiv_1_hcv_hbv_mpx_fda'||#secrowval[20]=='nat_hiv_react_fda'||#secrowval[20]=='hiv_antigen_fda'||#secrowval[20]=='nat_hcv_react_fda'||#secrowval[20]=='nat_hbv_react_fda'||#secrowval[20]=='syphilis_react_fda'||#secrowval[20]=='nat_west_nile_react_fda'||#secrowval[20]=='chagas_react_fda'||#secrowval[20]=='syphilis_ct_sup_react_fda'}">
									<s:if test="%{#request.fdalic==#request.idmsubquessopk}">
										<s:select name="%{formId}_Question[%{#indexval}].responsecode"
												id="%{formId}_QuestionRes%{#indexval}" tabindex="%{#indexval+3}"
												list="%{#application.codeListValues[#secrowval[8]]}" 
												listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
												onchange="showDynaAssess('%{#secrowval[0]}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
												isChangeDone(this,'%{#secrowval[15]}','idmchangeClass');"
												cssClass="%{formId}_unclassSQ%{#subrow}_%{#secrowval[11]} findlen iseditable subqfield%{#subrow}" value="%{#secrowval[15]}"/>
									</s:if>
									<s:elseif test="%{#request.fdalic==#request.idmsubquesypk}">
										<s:select name="%{formId}_Question[%{#indexval}].responsecode"
												id="%{formId}_QuestionRes%{#indexval}" tabindex="%{#indexval+3}"
												list="%{#application.codeListValues[#secrowval[8]]}" disabled="true"
												listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
												onchange="showDynaAssess('%{#secrowval[0]}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
												isChangeDone(this,'%{#request.idmSubQY}','idmchangeClass');"
												cssClass="%{formId}_unclassSQ%{#subrow}_%{#secrowval[11]} findlen iseditable subqfield%{#subrow}" value="%{#request.idmSubQY}"/>
									</s:elseif>
									<s:elseif test="%{#request.fdalic==#request.idmsubquesnonepk}">
										<s:select name="%{formId}_Question[%{#indexval}].responsecode"
												id="%{formId}_QuestionRes%{#indexval}" tabindex="%{#indexval+3}"
												list="%{#application.codeListValues[#secrowval[8]]}" disabled="true"
												listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
												onchange="showDynaAssess('%{#secrowval[0]}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
												isChangeDone(this,'%{#request.idmSubQN}','idmchangeClass');"
												cssClass="%{formId}_unclassSQ%{#subrow}_%{#secrowval[11]} findlen iseditable subqfield%{#subrow}" value="%{#request.idmSubQN}"/>
									</s:elseif>
									<s:else>
										<s:select name="%{formId}_Question[%{#indexval}].responsecode"
												id="%{formId}_QuestionRes%{#indexval}" tabindex="%{#indexval+3}"
												list="%{#application.codeListValues[#secrowval[8]]}" disabled="true"
												listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
												onchange="showDynaAssess('%{#secrowval[0]}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
												isChangeDone(this,'%{#secrowval[15]}','idmchangeClass');"
												cssClass="%{formId}_unclassSQ%{#subrow}_%{#secrowval[11]} findlen iseditable subqfield%{#subrow}" value="%{#secrowval[15]}"/>
									</s:else>
								</s:elseif>
								<s:else>
									<s:select name="%{formId}_Question[%{#indexval}].responsecode"
											id="%{formId}_QuestionRes%{#indexval}" tabindex="%{#indexval+3}"
											list="%{#application.codeListValues[#secrowval[8]]}" 
											listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
											onchange="showDynaAssess('%{#secrowval[0]}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
											isChangeDone(this,'%{#secrowval[15]}','idmchangeClass');"
											cssClass="%{formId}_unclassSQ%{#subrow}_%{#secrowval[11]} findlen iseditable subqfield%{#subrow}" value="%{#secrowval[15]}"/>
								</s:else>
							</s:if>
							<s:else>
								<s:select name="%{formId}_Question[%{#indexval}].responsecode"
										id="%{formId}_QuestionRes%{#indexval}" tabindex="%{#indexval+3}"
										list="%{#application.codeListValues[#secrowval[8]]}" disabled="true"
										listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
										onchange="showDynaAssess('%{#secrowval[0]}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
										isChangeDone(this,'%{#secrowval[15]}','idmchangeClass');"
										cssClass="%{formId}_unclassSQ%{#subrow}_%{#secrowval[11]} findlen iseditable subqfield%{#subrow}" value="%{#secrowval[15]}"/>
							</s:else>
						</s:if>
						<s:else>
							<s:select name="%{formId}_Question[%{#indexval}].responsecode"
									id="%{formId}_QuestionRes%{#indexval}" tabindex="%{#indexval+3}"
									list="%{#application.codeListValues[#secrowval[8]]}" 
									listKey="pkCodeId" listValue="description" headerKey="" headerValue="Select"
									onchange="showDynaAssess('%{#secrowval[0]}',this.id,%{#secrowval[7]},'1');checkDependent(this.id,'%{#secrowval[0]}','%{formId}');fieldclear(this);
									isChangeDone(this,'%{#secrowval[15]}','idmchangeClass');"
									cssClass="%{formId}_unclassSQ%{#subrow}_%{#secrowval[11]} findlen iseditable subqfield%{#subrow}" value="%{#secrowval[15]}"/>
						</s:else>
					</s:if>
				</s:if>
				<s:if test="%{getCodeListDescById(#secrowval[2])==@com.velos.ordercomponent.util.VelosGarudaConstants@TEXT_FIELD}">
				<s:if test='%{#grpval[1]!="Additional / Miscellaneous IDM Tests"}'>
					<s:if test="%{#secrowval[11]!=null}">
						<s:if test="%{#secrowval[29]==1}">
							<s:textfield name="%{formId}_Question[%{#indexval}].dynaFormDateStr" 
										onchange="fieldclear(this);validateCordDates('IDMdynaFormDate',this.id,'dynaFormDateError');isChangeDone(this,'%{#secrowval[21]}','idmchangeClass');isFirstDateChange(this);" tabindex="%{#indexval+3}"
										id="%{formId}_QuestionRes%{#indexval}"  
										cssClass="%{formId}_unclass%{#secrowval[11]} findlen iseditable custIDMdatePic" 
										value="%{#secrowval[21]}" cssStyle="width:75% !important;vertical-align: inherit;"/>
						</s:if>
						<s:else>
							<s:textfield name="%{formId}_Question[%{#indexval}].dynaFormDateStr" 
										onchange="fieldclear(this);validateCordDates('IDMdynaFormDate',this.id,'dynaFormDateError');isChangeDone(this,'%{#secrowval[21]}','idmchangeClass');isFirstDateChange(this);" tabindex="%{#indexval+3}"
										id="%{formId}_QuestionRes%{#indexval}"  disabled="true"
										cssClass="%{formId}_unclass%{#secrowval[11]} findlen iseditable custIDMdatePic" 
										value="%{#secrowval[21]}" cssStyle="width:75% !important;vertical-align: inherit;"/>
						</s:else>
					</s:if>
					<s:else>
						<s:textfield name="%{formId}_Question[%{#indexval}].dynaFormDateStr" 
									onchange="fieldclear(this);validateCordDates('IDMdynaFormDate',this.id,'dynaFormDateError');isChangeDone(this,'%{#secrowval[21]}','idmchangeClass');isFirstDateChange(this);" tabindex="%{#indexval+3}"
									id="%{formId}_QuestionRes%{#indexval}" 
									cssClass="%{formId}_unclass%{#secrowval[11]} findlen iseditable custIDMdatePic" 
									value="%{#secrowval[21]}" cssStyle="width:75% !important;vertical-align: inherit;"/>
					</s:else>
					</s:if>
					<s:else>
						<s:if test='%{#rowval[15]!=null && #rowval[15]!=notdonepk1}'>
							<s:textfield name="%{formId}_Question[%{#indexval}].dynaFormDateStr" 
									onchange="fieldclear(this);validateCordDates('IDMdynaFormDate',this.id,'dynaFormDateError');isChangeDone(this,'%{#secrowval[21]}','idmchangeClass');isFirstDateChange(this);" tabindex="%{#indexval+3}"
									id="%{formId}_QuestionRes%{#indexval}" 
									cssClass="%{formId}_unclass%{#secrowval[11]} findlen IsMandatory isReqPriorToShip iseditable custIDMdatePic" 
									value="%{#secrowval[21]}" cssStyle="width:75% !important;vertical-align: inherit;"/>
						</s:if>
						<s:else>
							<s:textfield name="%{formId}_Question[%{#indexval}].dynaFormDateStr" 
									onchange="fieldclear(this);validateCordDates('IDMdynaFormDate',this.id,'dynaFormDateError');isChangeDone(this,'%{#secrowval[21]}','idmchangeClass');isFirstDateChange(this);" tabindex="%{#indexval+3}"
									id="%{formId}_QuestionRes%{#indexval}" 
									cssClass="%{formId}_unclass%{#secrowval[11]} findlen  iseditable custIDMdatePic" 
									value="" disabled="true" cssStyle="width:75% !important;vertical-align: inherit;"/>
						</s:else>
					</s:else>
				</s:if>
					<span class='error fieldMan' style="display:none" id='errorMsg_<s:property value="%{formId}"/>_QuestionRes<s:property value="%{#indexval}"/>'>
						<br/><%=propData.getPropertiesData("garuda.label.dynamicform.errormsg") %>
					</span>
					<span class='error fieldMan' style="display:none" id='dateMsg_<s:property value="%{formId}"/>_QuestionRes<s:property value="%{#indexval}"/>'>
						<br/><%=propData.getPropertiesData("garuda.label.dynamicform.testdatemsg") %>
					</span>
					<span class='error <s:property value="%{formId}"/>_Ques<s:property value="%{#row}"/><s:property value="%{#subrow}"/>_s_assessError' style="display:none" id="assesError_<s:property value='%{#secrowval[0]}'/>">
						<%=propData.getPropertiesData("garuda.label.dynamicform.assessmentmsg") %>
					</span>
					<div>
						<a href="#" onclick="showDynaAssessLink(this,'<s:property value="%{#secrowval[0]}"/>','<s:property value="%{formId}"/>_QuestionRes<s:property value="%{#indexval}"/>',<s:property value='%{#secrowval[7]}'/>,'1');"
						style="display: none;" id="showasses_<s:property value='%{#secrowval[0]}'/>" class="<s:property value="%{formId}"/>_Ques<s:property value="%{#row}"/><s:property value="%{#subrow}"/>_s_showassess">
							<%=propData.getPropertiesData("garuda.label.dynamicform.showassess") %>
						</a>
						<a href="#" onclick="hideDynaAssess(this,'<s:property value="%{#secrowval[0]}"/>');" style="display: none;"
							id="hideasses_<s:property value='%{#secrowval[0]}'/>" class="<s:property value="%{formId}"/>_Ques<s:property value="%{#row}"/><s:property value="%{#subrow}"/>_s_hideassess">
							<%=propData.getPropertiesData("garuda.label.dynamicform.hideassess") %>
						</a>
					</div>
				</td>
			</tr>
			<s:set name="subrow" value="%{#subrow+1}"/>
			<s:set name="indexval" value="%{#indexval+1}"/>
		</s:if>
		</s:iterator>
		<s:if test="%{#rowval[3]==null}">
			<s:set name="row" value="%{#row+1}"></s:set>
		</s:if>
		</s:if>
		</s:iterator>
		<s:if test='%{#grpval[1]=="Additional / Miscellaneous IDM Tests"}'>
			<tr>
				<td></td>
				<td>
					<s:if test="%{iseditable!=1}">
						<button type="button" id="IDM_additionalButton" onclick="addAdditionalIDMTest(this)"><%=propData.getPropertiesData("garuda.label.dynamicform.addtest") %></button>
					</s:if>
				</td>
				<td></td>
			</tr>
		</s:if>
		</s:iterator>
		<s:hidden name="indexvalue" id="indexvalue" value="%{#indexval}"></s:hidden>
		</tbody>
	</table>
</div>
</div>
</div>
<br/>
<div id="cordDeferMsg" style="display: none;">
	<span><%=propData.getPropertiesData("garuda.common.label.codedefermsg") %></span>
</div>
<table width="100%"  bgcolor="#cccccc" id="eSignTable">
	<tr>
	<td style="float: right;">
		<table>
			<tr valign="baseline" >
				<td width="70%">
					<jsp:include page="./cb_esignature.jsp" flush="true">
			   			<jsp:param value="IDMsaveForm" name="submitId"/>
						<jsp:param value="IDMsaveForminvalid" name="invalid" />
						<jsp:param value="IDMsaveFormminimum" name="minimum" />
						<jsp:param value="IDMsaveFormpass" name="pass" />
						<jsp:param value="IDMsaveFormesign" name="esign" />
	   				</jsp:include></td>
	    		<td width="15%" align="right">
	    			<button type="button" name="saveForm" id="IDMsaveForm" onclick="checkValid();" disabled="disabled">
	    				<%=propData.getPropertiesData("garuda.unitreport.label.button.submit") %>
	    			</button>
	    		</td>
	    		<td width="15%">
	    			<button type="button" name="closemodel" id="IDMclosemodel" onclick="closeModals('IDMModalForm')">
	    				<%=propData.getPropertiesData("garuda.unitreport.label.button.cancel") %>
	    			</button>
	    		</td>
	    	</tr>
		</table>
	</td>
</table>
</s:form>
<div style="display: none;" id="autoDeferEsignModal">
<s:form>
 <table width="100%"  bgcolor="#cccccc" id="defereSignTable">
	<tr>
	<td style="float: right;">
		<table>
			<tr valign="baseline" >
				<td width="70%">
					<jsp:include page="./cb_esignature.jsp" flush="true">
			   			<jsp:param value="IDMdefersaveForm" name="submitId"/>
						<jsp:param value="IDMdefersaveForminvalid" name="invalid" />
						<jsp:param value="IDMdeferaveFormminimum" name="minimum" />
						<jsp:param value="IDMdefersaveFormpass" name="pass" />
						<jsp:param value="IDMdefersaveFormesign" name="esign" />
	   				</jsp:include></td>
	    		<td width="15%" align="right">
	    			<button type="button" name="saveForm" id="IDMdefersaveForm" onclick="deferSubmitAction('<s:property value="%{formId}"/>','autoDeferEsignModal,cordDeferMsg');" disabled="disabled">
	    				<%=propData.getPropertiesData("garuda.unitreport.label.button.submit") %>
	    			</button>
	    		</td>
	    		<td width="15%">
	    			<button type="button" name="closemodel" id="IDMdeferclosemodel" onclick="closeDeferEsign('autoDeferEsignModal')">
	    				<%=propData.getPropertiesData("garuda.unitreport.label.button.cancel") %>
	    			</button>
	    		</td>
	    	</tr>
		</table>
	</td>
</table>
</s:form>
</div>
</div>
<script>
var focusFlag=0;
var idmlabel=$j('#idmlabel').val();
var keyPressFlag = false;
var elements = $j('.findlen').get(), i, len = elements.length;
var totalF = parseInt($j('#indexvalue').val());
//var formId = $j('#formId').val();
function isFirstDateChange(el){
	if($j('#IDMautoPopulateDate').val()==""){
		$j('#IDMautoPopulateDate').val($j(el).val());
		$j('#IDMautoPopulateDateFld').val($j(el).attr('id'));
	}else if($j('#IDMautoPopulateDateFld').val()==$j(el).attr('id')){
		$j('#IDMautoPopulateDate').val($j(el).val());
	}
}
function getKeyEvent(){
	keyPressFlag = true;
}
function dynacomment(obj){
	
	var nextDiv=$j(obj).next('div');
	
	$j(nextDiv).next('div').css('padding','10px');
	$j(nextDiv).toggle();
	$j(nextDiv).find('textarea').focus();
}
function saveComment(obj,index){
	var parentDiv=$j(obj).parent().parent();
	var textFld=$j(obj).parent().find('textarea').val();
	var commentID=idmlabel+'_comment_'+index;
	var errorComment=index+'_Commenterror';
	$j('#'+commentID).val(textFld);
	if(textFld!='' && textFld!=null){
		var spanText = "<img height='15px' src='./images/addcomment.png'>&nbsp;"+"<%=propData.getPropertiesData("garuda.label.dynamicform.addorviewcomment") %>";
		$j(parentDiv).prev('a').find('span').html(spanText);
		$j(parentDiv).hide();
		//$j(obj).hide();
		//$j(obj).parent().find('textarea').attr('disabled','disabled');
		$j('#'+errorComment).hide();
	}else{
		$j('#'+errorComment).show();
	}
}
function hideDynaComment(obj,index){
	var parentDiv=$j(obj).parent().parent();
	$j(parentDiv).hide();
	var textFld=$j(obj).parent().find('textarea').attr('id');
	var commentID=idmlabel+'_comment_'+index;
	commentID=$j('#'+commentID).val();
	$j('#'+textFld).val(commentID);
}

function showDynaAssess(rowId,id,condition,code){
	if(condition==1){
		var flag=false;
		var unResId="Ques"+rowId+"_UNRes";
		var divId='assess_'+rowId;
		var showasses="showasses_"+rowId;
		var hideasses="hideasses_"+rowId;
		var assesError="assesError_"+rowId;
		var assessmentPk="assessmentPk_"+rowId;
		var shname=$j('#'+rowId+"_assessshname").val();
		var assessDivName = $j('#assessDivName_'+rowId).val();
		var rowId=$j('#'+id).parent().parent().attr('id');
			rowId=rowId+"_td";
		$j("#"+id+" option:selected").each(function(index,el){
			$j("#"+unResId+" option").each(function(index1,el1){
				if($j(el).text()==$j(el1).text()){
					flag=true;
				}
			});
		});
		if(flag){
			if(code=='1'){
				$j("#"+id+" option").each(function(ind,el){
					setTimeout(function(){
						var responsestr = $j(el).text();
						var responseval = $j(el).val();
						var assessCloseDiv = responseval+'_'+assessDivName;
						var url='getassessment?entityId='+$j('#cordID').val()+'&responsestr='+responsestr+'&assessDivName='+assessCloseDiv+'&responseval='+responseval;
						if($j(el).is(':selected')){
							if($j('#'+assessCloseDiv).html()!=null && $j('#'+assessCloseDiv).html()!="" && $j('#'+assessCloseDiv).html()!='undefined'){
								$j('#'+assessCloseDiv).show();
							}else{
								refreshassessmentDiv(url,rowId,divId,assessCloseDiv);
							}
						}else{
							$j('#'+assessCloseDiv).remove();
						}
					},0);
				});
				$j('#'+divId).show();
				$j('#'+showasses).hide();
				$j('#'+hideasses).show();
				//if($j('#isCompInfo').val()!="1"){
					if($j('#'+assessmentPk).val()=="" || $j('#'+assessmentPk).val()==null){
						$j('#'+id).addClass('unexpectedRes');
					}
				//}
				if($j('#isCompInfo').val()=="1"){
					if($j('#'+assessmentPk).val()=="" || $j('#'+assessmentPk).val()==null){
						$j('#'+id).addClass('assesReqPriorToShip');
					}
				}
			}else{
				$j('#'+showasses).show();
				$j('#'+hideasses).hide();
				if($j('#isCompInfo').val()=="1"){
					if($j('#'+assessmentPk).val()=="" || $j('#'+assessmentPk).val()==null){
						$j('#'+assesError).show();
						$j('#'+id).addClass('assesReqPriorToShip');
					}
				}
				//if($j('#isCompInfo').val()!="1"){
					if($j('#'+assessmentPk).val()=="" || $j('#'+assessmentPk).val()==null){
						$j('#'+id).addClass('unexpectedRes');
					}
				//}
			}
		}else{
			if(code=='1'){
				//$j('#'+divId).hide();
				$j('#'+divId).empty();
				$j('#'+showasses).hide();
				$j('#'+hideasses).hide();
				if($j('#isCompInfo').val()=="1"){
					$j('#'+assesError).hide();
					$j('#'+id).removeClass('assesReqPriorToShip');
				}
				//if($j('#isCompInfo').val()!="1"){
					$j('#'+id).removeClass('unexpectedRes');
				//}
			}else{
				$j('#'+showasses).hide();
				$j('#'+hideasses).hide();
				if($j('#isCompInfo').val()=="1"){
					$j('#'+assesError).hide();
					$j('#'+id).removeClass('assesReqPriorToShip');
				}
				//if($j('#isCompInfo').val()!="1"){
					$j('#'+id).removeClass('unexpectedRes');
				//}
			}
		}
	}
}
function refreshassessmentDiv(url,rowId,divId){
	var iseditable = $j('#iseditable').val();
	$j.ajax({
        type: "POST",
        url: url,
        async:true,
        data:$j('#'+rowId+' *').serialize(),
        success: function (result){
            	$j("#"+divId).html(result);
            	$j("#"+divId).show();
            	if(iseditable == "1"){
            		$j("#"+divId).find(".iseditable").attr("disabled","disabled");
    			}
            }
	});
}

function checkDefervalue(obj,formId,rowIndex,prevVal,frmWhere){
	var deferId = formId+"_deferVal_"+rowIndex;
	var nextIndex = parseInt(rowIndex)+1;
	var resVal = $j(obj).val();
	var ObjId = $j(obj).attr('id');
	var nextObj = formId+"_QuestionRes"+nextIndex;
	var nextObjVal = $j('#'+nextObj).val();
	var cnfmFlag;
	var triggerflag = true;
	var returnFlag = false;
	if(resVal==$j("#idmnotdonepk").val()){
		if(ObjId=='IDM_QuestionRes14' || ObjId=='IDM_QuestionRes18'){
			if($j("#IDM_QuestionRes14").val()==$j("#idmnotdonepk").val() && $j("#IDM_QuestionRes18").val()==$j("#idmnotdonepk").val()){
				triggerflag = true;
			}else{
				triggerflag = false;
			}
		}
		if(ObjId=='IDM_QuestionRes30' || ObjId=='IDM_QuestionRes34' || ObjId=='IDM_QuestionRes38'){
			if($j("#IDM_QuestionRes30").val()==$j("#idmnotdonepk").val() && $j("#IDM_QuestionRes34").val()==$j("#idmnotdonepk").val() && $j("#IDM_QuestionRes38").val()==$j("#idmnotdonepk").val()){
				triggerflag = true;
			}else{
				triggerflag = false;
			}
		}
	}
	if(resVal==$j("#idmnotdonepk1").val() || resVal==$j("#idmpositivepk").val()){
		if(ObjId=='IDM_QuestionRes50' || ObjId=='IDM_QuestionRes62'){
			if($j("#IDM_QuestionRes50").val()==$j("#idmpositivepk").val() && $j("#IDM_QuestionRes62").val()==$j("#idmnotdonepk1").val()){
				triggerflag = true;
			}else{
				triggerflag = false;
			}
		}
	}
	$j('#'+deferId+' option').each(function(){
		if(prevVal==this.value){
			cnfmFlag = true;
		}
	});
	if(triggerflag){
		$j('#'+deferId+' option').each(function(){
			if(resVal==this.value && frmWhere!='fromValid'){
				$j( "#cordDeferMsg" ).dialog({
					resizable: false,
					title:'<%=propData.getPropertiesData("garuda.common.lable.confirm") %>',
					modal: true,
					closeText: '',
					closeOnEscape: false ,
					 close: function() {
						 $j('#idmtempDeferId').val(ObjId);
						 deferCancelAction('cordDeferMsg',prevVal,nextObj,nextObjVal,resVal,cnfmFlag,frmWhere);
					    },
					buttons: {
						"Yes": function() {
							//$j( this ).dialog( "close" );
							//$j('#cordDeferMsg').dialog( "destroy" );
							$j('#idmtempDeferId').val(ObjId);
							$j('#autoDeferEsignModal').dialog({
								autoOpen: true,
								resizable: false, width:500, height:120,closeOnEscape: false,
								modal: true}).siblings('.ui-dialog-titlebar').remove();	
						},
						"No": function() {
							$j('#idmtempDeferId').val(ObjId);
							deferCancelAction('cordDeferMsg',prevVal,nextObj,nextObjVal,resVal,cnfmFlag,frmWhere);
						}
					}
				});
			}else if(frmWhere == 'fromValid'){
				if(!returnFlag){
					if(resVal==this.value){
						returnFlag = true;
					}else{
						returnFlag = false;
					}
				}
			}
		});
	}
	return returnFlag;
}
function deferSubmitAction(formId,divId){
	$j('#autoDeferFlag').val("true");
	//$j('#cordDeferMsg').remove();
	var divArray = divId.split(",");
	for(i = 0; i < divArray.length; i++) {
		$j('#'+divArray[i]).dialog("destroy");
		//closeModals(divArray[i]);
		$j('#'+divArray[i]).remove();
	}
	//$j('.ui-widget-overlay').remove();
	if($j('#isCompInfo').val()=='1'){
        loadMoredivsCRI('saveDynamicFrm','IDMModalForm','dynamicForm');
        var cordid = $j("#cordID").val();
    	var orderid =$j("#orderId").val();
    	var ordertype=$j("#orderType").val();
    	if(orderid!=null && orderid!="" && typeof(orderid)!="undefined" && ordertype!=null && ordertype!="" && typeof(ordertype)!="undefined"){
        	var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID='+cordid+'&orderId='+orderid+'&orderType='+ordertype+'&pkcordId='+cordid+'&currentReqFlag=true';
    		loadMoredivs(urlb,'viewcbutoppanel,quicklinksDiv,taskCompleteFlagsDiv,ctReqClinInfoContentDiv,finalreviewcontentDiv,currentRequestProgressMainDiv','cbuProcedureMain','update','status');//url,divname,formId,updateDiv,statusDiv
    		setpfprogressbar();
    		getprogressbarcolor();
        }
    	else{
    		var urla = "loadClinicalData?cdrCbuPojo.cordID="+cordid;
    		loadPageByGetRequset(urla,'cordSelectedData');
    	}
	}else{
        refreshDynaForm('saveDynamicFrm',formId+'_formVersion','dynamicForm');
	}
	if($j('#isCordEntry').val()=="1"){
		var field = FCKeditorAPI.GetInstance('sectionContentsta'); 
		var value = field.GetHTML(true);
		$j('#sectionContentsta').val(value);
		$j("#cordSearchable").val("1");
		loadDivWithFormSubmitDefer('saveAutoDeferPartialCordEntry?autoDeferFlag=false','maincontainerdiv','cordentryform1');
	}
	if($j('#isCompInfo').val()=="1"){
		closeModals('dialogForCRI');
	}
	setTimeout(function(){closeModals('IDMModalForm'),2000});
}
function deferCancelAction(divId,prevVal,nextObj,nextObjVal,resVal,cnfmFlag,frmWhere){
	var obj = $j('#idmtempDeferId').val();
	var divArray = divId.split(",");
	for(i = 0; i < divArray.length; i++) {
		$j('#'+divArray[i]).dialog("destroy");
	}
	$j('#IDMdefersaveFormesign').val("");
	$j('#IDMdefersaveForm').attr('disabled','disabled');
	
	if(frmWhere!="fromValidElse"){
		if(cnfmFlag==true){
			$j('#'+obj).val("");
			$j('#'+obj).trigger('change');
		}else{
			$j('#'+obj).val(prevVal);
			$j('#'+nextObj).val(nextObjVal);
			$j('#'+obj).trigger('change');
		}
		
		if($j('#'+nextObj).hasClass("hasDatepicker")){
				if(prevVal!="" && prevVal!=null){
					$j("#placeHolderOfid_"+$j('#'+nextObj).attr("id")).hide();
				}
		}
		
	}
	//$j('#'+obj).css("border","1px solid red");
	$j('#'+obj).focus();
}
function closeDeferEsign(divId){
	$j('#'+divId).dialog("destroy");
	$j('#cordDeferMsg').next('div').find('button:last').click();
}
function checkDependent(id,index,formId){
	//$j('#'+id).css("border","1px solid #DBDAD9");
	var depentValId=formId+'_depentVal_'+index;
	var depentClass=formId+'_unclass'+index;
	var subQ1=formId+'_unclassSQ2_'+index;
	var subQ2=formId+'_unclassSQ3_'+index;
	var unreqFlag=formId+'_unreqFlag'+index;
	var unreqPreFlag=formId+'_unreqPreFlag'+index;
	var mandatory=formId+'_classMan'+index;
	var flag=false;
	$j("#"+id+" option:selected").each(function(index,el){
			$j("#"+depentValId+" option").each(function(index1,el1){
				if($j(el).val()==$j(el1).val()){
					flag=true;
				}
			});
		});
	if(flag){
		if($j("#"+depentValId).html()){
			$j('.'+mandatory).show();
			$j('.'+depentClass).each(function(){
                $j("#placeHolderOfid_"+$j(this).attr("id")).hide();
               	$j(this).val($j('#IDMautoPopulateDate').val());               
                $j(this).removeAttr('disabled');
                $j(this).datepicker("enable");
            });
			if(focusFlag==0){
				if(keyPressFlag){
					//$j('.'+depentClass).first().focus();
					keyPressFlag = false;
				}
			}
			if($j('#IDM_QuestionRes0').val()==$j('#idmsubquessopk').val()){
				$j('.'+subQ1).removeAttr('disabled');
				$j('.'+subQ1).each(function(){
					$j(this).val("");
				});
				//if($j('#'+unreqFlag).val()=="1"){
					$j('.'+subQ1).addClass('IsMandatory');
				//}
				if($j('#isCDRUser').val()=="true"){
					if($j('#isCompInfo').val()=="1"){
						//if($j('#'+unreqPreFlag).val()=="1"){
							$j('.'+subQ1).addClass('isReqPriorToShip');
							$j('.'+mandatory).show();
							$j('.'+subQ1).each(function(index,el){
								if($j(el).val()==""){
									$j(el).addClass('criHighlight');
								}
							});
						//}
					}
				}
			}else{
				$j('.'+subQ1).attr("disabled","disabled");
				if($j('#IDM_QuestionRes0').val()==$j('#idmsubquesypk').val()){
					$j('.'+subQ1).val($j('#idmSubQY').val());
				}
				if($j('#IDM_QuestionRes0').val()==$j('#idmsubquesnonepk').val()){
					$j('.'+subQ1).val($j('#idmSubQN').val());
				}
				$j('.'+subQ1).removeClass('IsMandatory');
				$j('.'+subQ1).removeClass('isReqPriorToShip');
			}
			if($j('#IDM_QuestionRes1').val()==$j('#idmsubquessopk').val()){
				$j('.'+subQ2).removeAttr("disabled");
				$j('.'+subQ2).each(function(){
					$j(this).val("");
				});
			//	if($j('#'+unreqFlag).val()=="1"){
					$j('.'+subQ2).addClass('IsMandatory');
			//	}
				if($j('#isCDRUser').val()=="true"){
					if($j('#isCompInfo').val()=="1"){
					//	if($j('#'+unreqPreFlag).val()=="1"){
							$j('.'+subQ2).addClass('isReqPriorToShip');
							$j('.'+mandatory).show();
							$j('.'+subQ2).each(function(index,el){
								if($j(el).val()==""){
									$j(el).addClass('criHighlight');
								}
							});
					//	}
					}
				}
			}else{
				$j('.'+subQ2).attr("disabled","disabled");
				if($j('#IDM_QuestionRes1').val()==$j('#idmsubquesypk').val()){
					$j('.'+subQ2).val($j('#idmSubQY').val());
				}
				if($j('#IDM_QuestionRes1').val()==$j('#idmsubquesnonepk').val()){
					$j('.'+subQ2).val($j('#idmSubQN').val());
				}
				$j('.'+subQ2).removeClass('IsMandatory');
				$j('.'+subQ1).removeClass('isReqPriorToShip');
			}
			//if($j('#'+unreqFlag).val()=="1"){
				$j('.'+depentClass).addClass('IsMandatory');
				$j('.'+mandatory).show();
			//}
				if($j('#isCDRUser').val()=="true"){
					if($j('#isCompInfo').val()=="1"){
					//	if($j('#'+unreqPreFlag).val()=="1"){
							$j('.'+depentClass).addClass('isReqPriorToShip');
							$j('.'+depentClass).each(function(index,el){
								if($j(el).val()==""){
									$j(el).addClass('criHighlight');
								}
							});
							$j('.'+mandatory).show();
					//	}
					}
				}
		}
	}else{
		if(($j("#"+depentValId).html())){
			$j('.'+mandatory).hide();
			$j('.'+depentClass).attr("disabled","disabled");
			$j('.'+depentClass).val("");
			$j('.'+depentClass).each(function(){
				$j("#placeHolderOfid_"+$j(this).attr("id")).show();
				$j('#errorMsg_'+this.id).hide().next('span').hide();
			});
			$j('.'+depentClass).datepicker("disable");
			$j('.'+subQ1).val("");
			$j('.'+subQ2).val("");
			$j('.'+depentClass).removeClass('IsMandatory');
			$j('.'+depentClass).removeClass('isReqPriorToShip');
			$j('.'+subQ1).attr("disabled","disabled");
			$j('.'+subQ1).removeClass('IsMandatory');
			$j('.'+subQ1).removeClass('isReqPriorToShip');
			$j('.'+subQ1).next('span').hide();
			$j('.'+subQ2).attr("disabled","disabled");
			$j('.'+subQ2).removeClass('IsMandatory');
			$j('.'+subQ2).removeClass('isReqPriorToShip');
			$j('.'+subQ2).next('span').hide();
			$j('.'+depentClass).removeClass('criHighlight');
			$j('.'+subQ1).removeClass('criHighlight');
			$j('.'+subQ2).removeClass('criHighlight');
		}
	}
}
function assignAutoDateVal(){
	$j( ".custIDMdatePic" ).each(function(index,el){
		if($j(el).val()!="" || $j(el).val()!=null){
			$j('#IDMautoPopulateDate').val($j(el).val());
			$j('#IDMautoPopulateDateFld').val($j(el).attr('id'));
			return false;
			}
		});
}
function checkDependentques(id,index,formId){
	var depentValId = formId+'_depentQVal_'+index;
	var depentClass = formId+'_dependclass'+index;
	var unreqFlag = formId+'_unreqFlag'+index;
	var unreqPreFlag = formId+'_unreqPreFlag'+index;
	var mandatory = formId+'_classMan'+index+$j('#'+depentValId).val();
	var flag = false;
	$j("#"+id+" option:selected").each(function(index,el){
			$j("#"+depentValId+" option").each(function(index1,el1){
				if($j(el).val()==$j(el1).val()){
					flag=true;
				}
			});
		});
	
	if(flag){
		if($j("#"+depentValId).html()){
			$j('.'+mandatory).show();
			$j('.'+depentClass).each(function(index,el){
				$j(el).addClass('IsMandatory');
				$j('.'+mandatory).show();
				if($j('#isCDRUser').val()=="true"){
					if($j('#isCompInfo').val()=="1"){
						$j(el).addClass('isReqPriorToShip');
						$j('.'+mandatory).show();
						if($j(el).val()==""){
							$j(el).addClass('criHighlight');
						}
					}
				}
			});
			if($j('#formvers').val()=='N2F'){
				if(id == "IDM_QuestionRes30" || id == "IDM_QuestionRes34"){
					if($j("#IDM_QuestionRes30").val()==$j("#idmnotdonepk").val() && $j("#IDM_QuestionRes34").val()==$j("#idmnotdonepk").val()){
						$j('#IDM_QuestionRes38').addClass('IsMandatory');
						$j('#IDM_QuestionRes38').addClass('isReqPriorToShip');
						$j('.classQues12').show();
					}else{
						$j('#IDM_QuestionRes38').removeClass('IsMandatory');
						$j('#IDM_QuestionRes38').removeClass('isReqPriorToShip');
						$j('.classQues12').hide();
					}
				}
				if(id == "IDM_QuestionRes30"){
					$j('#IDM_QuestionRes46').removeClass('IsMandatory');
					$j('#IDM_QuestionRes46').removeClass('isReqPriorToShip');
					$j('#IDM_QuestionRes46').parent().parent().find('.'+mandatory).hide();
				}
			}
		}
	}else{
		if(($j("#"+depentValId).html())){
			$j('.'+mandatory).hide();
			$j('.'+depentClass).each(function(index,el){
				//$j(el).next('img').next('span').hide().next('span').hide();
				$j('#errorMsg_'+this.id).hide().next('span').hide();
				$j(el).removeClass('IsMandatory');
				if($j('#isCDRUser').val()=="true"){
					if($j('#isCompInfo').val()=="1"){
						$j(el).removeClass('isReqPriorToShip');
					}
				}
			});
			$j('.'+depentClass).removeClass('criHighlight');
			if($j('#formvers').val()=='N2F'){
				if(id == "IDM_QuestionRes30" || id == "IDM_QuestionRes34"){
					if($j("#IDM_QuestionRes30").val()==$j("#idmnotdonepk").val() && $j("#IDM_QuestionRes34").val()==$j("#idmnotdonepk").val()){
						$j('#IDM_QuestionRes38').addClass('IsMandatory');
						$j('#IDM_QuestionRes38').addClass('isReqPriorToShip');
						$j('.classQues12').show();
					}else{
						$j('#IDM_QuestionRes38').removeClass('IsMandatory');
						$j('#IDM_QuestionRes38').removeClass('isReqPriorToShip');
						$j('.classQues12').hide();
					}
				}
			}
		}
	}
}
function refreshDynaForm(url,divname,formId){
		showprogressMgs();	
		var formId1 = $j('#formId').val();
		var changeFlag = divChanges("idmchangeClass",$j('#'+formId1+'ModalForm').html());
		if(!changeFlag){
			changeFlag = divChanges("assessmentChngCls",$j('#'+formId1+'ModalForm').html());
		}
		url = url+'?changeFlag='+changeFlag;
		$j('.findlen').removeAttr('disabled');
		$j('#dynaFormDiv textarea').removeAttr('disabled');
	$j.ajax({
        type: "POST",
        url: url,
        async:true,
        data:$j('#'+formId).serialize(),
        success: function (result){
        	$j('.ui-datepicker').html("");
            var $response=$j(result);
            var errorpage = $response.find('#errorForm').html();
            if(errorpage != null &&  errorpage != "" ){
            	$j('#main').html(result);
            }else{
                $j("#"+divname).html(result);
                $j('#dynaInfo').hide();
            	$j('#eSignTable').hide();
                $j('#autoDeferSaveMsg').hide();
        		$j('#formAutoDeferClose').hide();
        		$j('#dynaUpdate').show();
                if($j('#isCordEntry').val()=="1"){
                	if(idmcount==0){
                        idmcount++;
                        val = parseInt((idmcount*100)/(noOfMandatoryIdmField));
                        $j("#IDMbar").progressbar({
                           	value: val
                        }).children('.ui-progressbar-value').html(val.toPrecision(3) + '%');
                        addTotalCount();
                    }
                }else{
                    
                	refreshMultipleDivsOnViewClinical('1024','<s:property value="cdrCbuPojo.cordID"/>');
                	if(changeFlag){
                    	if($j('#fcrFlag').val() == "Y"){
            				refreshMultipleDivsOnViewClinical('2','<s:property value="cdrCbuPojo.cordID"/>');
            				}
                	}
                }
                closeprogressMsg();
            }
        },
        error: function (request, status, error) {
        	closeprogressMsg();
        	alert("Error " + error);
            alert(request.responseText);
        }
	});
	
}
$j(function(){
	showprogressMgs();
	focusFlag = 1;
	
	var formId = $j('#formId').val();
	var seqnumber = $j('#seqnumber').val();
	if(seqnumber == "" || seqnumber == null){
		$j('#seqnumber').val("1");
	}

	var iseditable = $j('#iseditable').val();
	
			for( i = 0; i < len; i++ ) {
				var id = elements[i].getAttribute('id');
					var pId = $j(elements[i]).parent().parent().attr('id');
					if( pId != null && pId != "" ){
						if ($j('#'+pId).css("display") != 'none' && !$j('#'+id).is(':disabled')){
							var fieldIndex = $j('#'+formId+'_index'+i).val();
							var unreqFlag = formId+'_unreqFlag'+fieldIndex;
							var unreqPreFlag = formId+'_unreqPreFlag'+fieldIndex;
							var condition = $j('#'+formId+'_assesCon'+i).val();
							if(id != 'IDM_QuestionRes46'){
								if(!$j("#"+id).hasClass('disableclass')){
									$j(elements[i]).addClass('IsMandatory');
									$j('#'+formId+'_mandatory'+fieldIndex).show();
									if( $j('#isCompInfo').val() == "1" ){
										if( $j('#isCDRUser').val() == "true" ){
											$j(elements[i]).addClass('isReqPriorToShip');
											$j('#'+formId+'_mandatory'+fieldIndex).show();
										}
									}
								}
							}else{
								var fieldIndex = $j('#'+formId+'_index'+i).val();
								$j('#IDM_QuestionRes46').removeClass('IsMandatory');
								$j('#IDM_QuestionRes46').removeClass('isReqPriorToShip');
								$j('#IDM_QuestionRes46').parent().parent().find('#'+formId+'_mandatory'+fieldIndex).hide();
							}
							if(condition == 1){
								showDynaAssess(fieldIndex,id,condition,'0');
							}
						}
					}
			}
	
	if($j('#formvers').val()=='N2F'){
		$j('#IDM_mandatory'+$j('#IDM_index0').val()).show();
		$j('#IDM_QuestionRes0').addClass('IsMandatory').addClass('isReqPriorToShip');
		$j('#IDM_mandatory'+$j('#IDM_index1').val()).show();
		$j('#IDM_QuestionRes1').addClass('IsMandatory').addClass('isReqPriorToShip');
	}
//	getDatePic();
	if( $j('#isCompInfo').val() == "1" ){
		if( $j('#isCDRUser').val() == "true" ){
			 $j('.isReqPriorToShip').each(function(){
				 //$j(this).css("border", "");
				 if($j(this).val() == null || $j(this).val() == "" || $j(this).val() == "-1"){
					 $j(this).addClass('criHighlight');
					}
				  });
			 $j('.assesReqPriorToShip').each(function(){
				 	$j(this).addClass('unexpectedRes');
			 });
		}
	}
	
	if(iseditable == "1"){
		$j("#dynaFormDiv").find(".iseditable").attr("disabled","disabled");
		$j("#eSignTable").hide();
		$j('#dynaFormDiv button').attr('disabled','disabled');
		$j('#dynaFormDiv input [type=button]').attr('disabled','disabled');
	}else{
		$j('#fktestsource').focus();
	}
	focusFlag=0;
	var lastIndex = $j('#indexvalue').val();
	$j('#IDMsaveFormesign').addClass('tabclass');
	$j('#IDMsaveFormesign').attr('tabindex',++lastIndex);
	//setTabIndex();
	closeprogressMsg();
	getIDMCustomDatePic();
	assignAutoDateVal();
	var deferEsignLen = $j('body').find("#autoDeferEsignModal").length;
	if(deferEsignLen > 1){
		$j('body').find("#autoDeferEsignModal").not(":first").remove();
	}
	var deferMsgLen = $j('body').find("#cordDeferMsg").length;
	if(deferMsgLen > 1){
		$j('body').find("#cordDeferMsg").not(":first").remove();
	}
	$j('#IDMdefersaveFormesign').val("");
	$j('#IDMdefersaveForm').attr('disabled','disabled');
	$j('#IDMdefersaveForminvalid').hide();
	$j('#IDMdeferaveFormminimum').hide();
	$j('#IDMdefersaveFormpass').hide();
	var additionalTests=$j('.additionalIDMTest').length;
	if(additionalTests == 0 ){
		$j("#IDM_additionalButton").hide();
	}
	
	var validator = $j("#dynamicForm").validate({
		invalidHandler: function(form, validator) {
	        var errors = validator.numberOfInvalids();
	        if (errors) {
	            validator.errorList[0].element.focus();
	        }
	    }
	});
});
$j(document).ready(function(){
	if($j('#iseditable').val()=="1"){
	$j('#dynaFormDiv .dpDate').each(function(){
		if($j(this).is(":disabled")){
			var imgElement = $j(this).parent().find("img");
			if(imgElement.hasClass('ui-datepicker-trigger')){
				imgElement.unbind("click");
			}
		}
	});
	}
});
function getIDMCustomDatePic(){
	getClearButton();
	var today = new Date();
	var d = today.getDate();
	var m = today.getMonth();
	var y = today.getFullYear();
	var h=today.getHours();
	var mn=today.getMinutes()+1;
	
	$j( ".custIDMdatePic" ).addClass("dpDate").each(function(){
		if($j(this).is(":disabled")){
			$j(this).datepicker("disable");
		}
        var s = this;
        var selectHolder = $j('<span>').css({ position: 'relative' });
	    $j(this).wrap(selectHolder);
        var titleSpan = $j('<label>').attr({'class': 'tooltipspan','id':'placeHolderOfid_'+$j(this).attr("id")}).html("mm/dd/yyyy")
	                				.css({'width':0,'height':0,'top':$j(s).position().top,'left':$j(s).position().left}).click(function(){
	                					var txtFldId = $j(this).attr("id"); 
	                					txtFldId = txtFldId.replace("placeHolderOfid_","");
	                					if(!$j("#"+txtFldId).is(":disabled")){
		                					$j("#"+txtFldId).focus();
			                			}
	                				});
		if($j(this).val()!=""){
			$j(titleSpan).css({'display':'none'});
		}else{
			$j(titleSpan).css({'display':'block'});
		}
        $j(this).parent().append(titleSpan);
	}).datepicker({
		dateFormat: 'M dd, yy',
		maxDate: new Date(y, m, d),
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true, 
		prevText: '',
		nextText: '',
		showOn: "button",
		buttonImage: "./images/calendar-icon.png",
		buttonImageOnly: true,

		/* fix buggy IE focus functionality */
		fixFocusIE: false,
		
        /* blur needed to correctly handle placeholder text */
        onSelect: function(dateText, inst) {
        	this.fixFocusIE = true;
        	$j(this).blur().change().focus();
        },
        beforeShow: function(input, inst) {
      	  if($j("#IDMdynaFormDate").val()!=null){
          	  	var date = new Date($j("#IDMdynaFormDate").val());
          		var d1 = date.getDate();
 		   		var m1 = date.getMonth();
 		   		var y1 = date.getFullYear();
 		   		$j(input).datepicker( "option", "minDate", new Date(y1,m1,d1));
          }
       },
  	   onChangeMonthYear:function(y, m, i){                                
  	        var d = i.selectedDay;
  	        $j(this).datepicker('setDate', new Date(y, m - 1, d));
  	      	$j(this).val(dateFormat(new Date(y, m - 1, d),'mm/dd/yyyy'));
  	    }
	}).focus(function(){
		$j("#placeHolderOfid_"+$j(this).attr("id")).hide();
		if(!$j("#ui-datepicker-div").is(":visible")){
			if($j("#IDMdynaFormDate").val()!=null && $j("#IDMdynaFormDate").val()!=="" && $j("#IDMdynaFormDate").val()!=undefined){
          	  	var date = new Date($j("#IDMdynaFormDate").val());
          		var d1 = date.getDate();
 		   		var m1 = date.getMonth();
 		   		var y1 = date.getFullYear();
 		   		$j(this).datepicker( "option", "minDate", new Date(y1,m1,d1));
          	}
		}
		if($j(this).val()!=""){
			var thisdate = new Date($j(this).val());
			if(!isNaN(thisdate)){
				var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
				$j(this).val(formattedDate);
			}
		}
	}).keydown(function(e) {
		  var val = $j(this).val();
		  if(e.keyCode==191 || e.keyCode==111){
			  return false;
		  }else if(e.keyCode!=8 && e.keyCode!=46 && e.keyCode!=191 && e.keyCode!=111){
			  if(val.length==2 || val.length==5){
				  $j(this).val(val+"/");
			  }
		  }else if(e.keyCode==8){
			if(val==""){
				return false;
			}
		  }
	}).focusout(function(){
		if($j(this).val()!=""){
			$j("#placeHolderOfid_"+$j(this).attr("id")).hide();
		}else{
			$j("#placeHolderOfid_"+$j(this).attr("id")).show();
		}
	});
	
	$j( "#IDMdynaFormDate" ).addClass("dpDate").each(function(){
		if($j(this).is(":disabled")){
			$j(this).datepicker("disable");
		}
		var s = this;
        var selectHolder = $j('<span>').css({ position: 'relative' });
	    $j(this).wrap(selectHolder);
        var titleSpan = $j('<label>').attr({'class': 'tooltipspan','id':'placeHolderOfid_'+$j(this).attr("id")}).html("mm/dd/yyyy")
	                				.css({'width':0,'height':0,'top':$j(s).position().top,'left':$j(s).position().left}).click(function(){
	                					var txtFldId = $j(this).attr("id"); 
	                					txtFldId = txtFldId.replace("placeHolderOfid_","");
	                					if(!$j("#"+txtFldId).is(":disabled")){
		                					$j("#"+txtFldId).focus();
			                			}
	                				});
		if($j(this).val()!=""){
			$j(titleSpan).css({'display':'none'});
		}else{
			$j(titleSpan).css({'display':'block'});
		}
        $j(this).parent().append(titleSpan);
	}).datepicker({
		dateFormat: 'M dd, yy',
		maxDate: new Date(y, m, d),
		changeMonth: true,
		changeYear: true,
		showButtonPanel: true, 
		prevText: '',
		nextText: '',
		showOn: "button",
		buttonImage: "./images/calendar-icon.png",
		buttonImageOnly: true,
		/* fix buggy IE focus functionality */
		fixFocusIE: false,

	    /* blur needed to correctly handle placeholder text */
	    onSelect: function(dateText, inst) {
		    this.fixFocusIE = true;
        	$j(this).blur().change().focus();
	    },
	    onChangeMonthYear:function(y, m, i){                                
	        var d = i.selectedDay;
	        $j(this).datepicker('setDate', new Date(y, m - 1, d));
	        $j(this).val(dateFormat(new Date(y, m - 1, d),'mm/dd/yyyy'));
	    }
	}).focus(function(){
		$j("#placeHolderOfid_"+$j(this).attr("id")).hide();
		if($j(this).val()!=""){
			var thisdate = new Date($j(this).val());
			if(!isNaN(thisdate)){
				var formattedDate = dateFormat($j(this).val(),'mm/dd/yyyy');
				$j(this).val(formattedDate);
			}
		}
	}).keydown(function(e) {
		  var val = $j(this).val();
		  if(e.keyCode==191 || e.keyCode==111){
			  return false;
		  }else if(e.keyCode!=8 && e.keyCode!=46 && e.keyCode!=191 && e.keyCode!=111){
			  if(val.length==2 || val.length==5){
				  $j(this).val(val+"/");
			  }
		  }else if(e.keyCode==8){
			if(val==""){
				return false;
			}
		  }
	}).focusout(function(){
		if($j(this).val()!=""){
			$j("#placeHolderOfid_"+$j(this).attr("id")).hide();
		}else{
			$j("#placeHolderOfid_"+$j(this).attr("id")).show();
		}
	});
}

function checkques(formId){
	$j('.findlen').each(function(index,el){
		var id=$j(el).attr('id');
		if($j('#'+id+":visible")){
			var fieldIndex=$j('#'+formId+'_index'+index).val();
			checkDependent(id,fieldIndex,formId);
		}
	});
}
function checkValid(){
	var flag = true;
	var url = "saveDynamicFrm";
	var noMandatory = true;
	var formId = $j('#formId').val();
	var focusField = "1";
	var deferFlag = false;
	var deferField,deferIndex,deferFldPrv;
	
		if($j("#IDMdynaFormDate").val()!=null){
	  	  	var date = new Date($j("#IDMdynaFormDate").val());
	  		var d1 = date.getDate();
		   		var m1 = date.getMonth();
		   		var y1 = date.getFullYear();
		   		$j(".custIDMdatePic").datepicker( "option", "minDate", new Date(y1,m1,d1));
	  	}
		var deferList = $j('#'+formId+'ModalForm .checkdeferclass').get(),d,dlen = deferList.length;
		for(d=0; d<dlen; d +=1){
			var dpId = $j(deferList[d]).parents("tr").attr('id');
			if(dpId !="" && dpId !=null && dpId != 'undefined'){
				var dpObj = deferList[d].parentNode.parentNode; 
				var dPflag = $j(dpObj).hasClass('hidechild');
				if(!dPflag && deferList[d].disabled != 'true'){
					var dId = deferList[d].getAttribute('id');
					var dIndex = $j("#"+dId+"_index").val();
					var dPrv = $j("#"+dId+"_prv").val();
					if(!deferFlag){
						deferFlag = checkDefervalue(deferList[d],formId,dIndex,dPrv,'fromValid');
						deferField = deferList[d];
						deferIndex = dIndex;
						deferFldPrv = dPrv; 
					} 
				}
			}
		}
		if(!deferFlag){
			if($j('#isCompInfo').val() == "1"){
				if($j('#isCDRUser').val() == "true"){
					var isReqPriorToShipEl = $j('#'+formId+'ModalForm .isReqPriorToShip').get(),s,len = isReqPriorToShipEl.length; 
					for(s = 0; s < len; s +=1){
						var pId = $j(isReqPriorToShipEl[s]).parents("tr").attr('id');
						if(pId !="" && pId !=null && pId != 'undefined'){
							var pObj = isReqPriorToShipEl[s].parentNode.parentNode;
							var Pflag = $j(pObj).hasClass('hidechild');
							if(!Pflag && isReqPriorToShipEl[s].disabled != 'true'){
								var id = isReqPriorToShipEl[s].getAttribute('id');
								if(isReqPriorToShipEl[s].value !="" && isReqPriorToShipEl[s].value != null && isReqPriorToShipEl[s].value != "undefined"){
									$j('#errorMsg_'+id).hide();
								}else{
									 noMandatory = false;
									 $j('#errorMsg_'+id).show();
									flag = false;
									if(focusField=="1"){
										focusField = id;
									}
								}
							}
						}
					}	
					if(noMandatory){
						var len = $j('#'+formId+'ModalForm .assesReqPriorToShip').length;
						if(len != 0){
							noMandatory = false;
						}
						//url = url+'?noMandatory='+noMandatory;
					}
				}
			}else{
				var isMandatoryEl  = $j('.IsMandatory').get(),s,len = isMandatoryEl.length;
				for(s=0; s<len; s +=1){
					var pId = $j(isMandatoryEl[s]).parents("tr").attr('id');
					if(pId !="" && pId !=null && pId != 'undefined'){
						var pObj = isMandatoryEl[s].parentNode.parentNode; 
						var Pflag = $j(pObj).hasClass('hidechild');
						if(!Pflag && isMandatoryEl[s].disabled != 'true'){
							var id = isMandatoryEl[s].getAttribute('id');
							if(isMandatoryEl[s].value !="" && isMandatoryEl[s].value != null && isMandatoryEl[s].value != "undefined"){
								$j('#errorMsg_'+id).hide();
							}else{
								$j('#errorMsg_'+id).show();
								flag = false;
								if(focusField=="1"){
									focusField = id;
								}
							}
						}
					}
				}
			}
			var thisquesReqObj = $j('.thisquesReq').get(), h, thisquesReqLen = thisquesReqObj.length;
			for(h = 0; h < thisquesReqLen; h++){
				var parObj = $j(thisquesReqObj[h]).parents('tr').attr('id');
				
				if(!$j('#'+parObj).hasClass('hidechild')){
					var id = thisquesReqObj[h].getAttribute('id');
					if(thisquesReqObj[h].value !="" && thisquesReqObj[h].value != null && thisquesReqObj[h].value != "undefined"){
						$j('#errorMsg_'+id).hide();
					}else{
						$j('#errorMsg_'+id).show();
						flag = false;
					}
				}
			}
			if(focusField !="" && focusField != null && focusField != "undefined" && focusField!="1"){
				$j('#'+focusField).focus();
			}
			var validateForm = true;
			$j("#dynaFormDiv .hasDatepicker").each(function(){
				if(!$j(this).is(":disabled")){
					if(!$j(this).valid()){
						validateForm = false;
					}
				}
			});
			if(flag==true && validateForm == true){
				var assessmentLen = $j('#'+formId+'ModalForm .warningFlag').length;
				var warningFlag = false;
				$j(".warningFlag").each(function(i,el){
					if($j(el).is(":visible")){
						warningFlag = true;
					}else{
					}
				});
				if(warningFlag){
					jConfirm('<s:text name="garuda.label.forms.assessmentwarn"/>', '<s:text name="garuda.common.lable.confirm"/>',
						function(r) {
							if (r == false) {
							}else if(r == true){
								if($j('#isCompInfo').val()=='1'){
									var div2;
									if($j('#formId').val()==idmlabel){
										div2='idmTargetDiv';
									}
								//if(changeFlag){
										loadMoredivsCRI(url,'IDMModalForm,'+div2,'dynamicForm');
									/*}else{
										$j('#dynaInfo').hide();
								    	$j('#eSignTable').hide();
								        $j('#dynaUpdate').show();
									}*/
								}else{
									//if(changeFlag){
										refreshDynaForm('saveDynamicFrm',formId+'_formVersion','dynamicForm');
									/*}else{
										$j('#dynaInfo').hide();
								    	$j('#eSignTable').hide();
								        $j('#dynaUpdate').show();
									}*/
								}
							}
					});
				}else{
					if($j('#isCompInfo').val()=='1'){
						var div2;
						if($j('#formId').val()==idmlabel){
							div2='idmTargetDiv';
						}
						//if(changeFlag){
							loadMoredivsCRI(url,'IDMModalForm,'+div2,'dynamicForm');
						/*}else{
							$j('#dynaInfo').hide();
					    	$j('#eSignTable').hide();
					        $j('#dynaUpdate').show();
						}*/
					}else{
						//if(changeFlag){
							refreshDynaForm('saveDynamicFrm',formId+'_formVersion','dynamicForm');
						/*}else{
							$j('#dynaInfo').hide();
					    	$j('#eSignTable').hide();
					        $j('#dynaUpdate').show();
						}*/
					}
				}
			}
			$j('#fromViewClinicalidm').val('1');
		}else{
			checkDefervalue(deferField,formId,deferIndex,deferFldPrv,"fromValidElse");
		}
}
function hideDynaAssess(obj,rowId){
	var divId='assess_'+rowId;
	$j('#'+divId).hide();
	$j(obj).hide();
	$j(obj).prev('a').show();
}
function showDynaAssessLink(obj,rowId,id,condition,code){
	showDynaAssess(rowId,id,condition,code);
	$j(obj).hide();
	$j(obj).next('a').show();
}

function fieldclear(obj){
	if(!$j(obj).is(":disabled")){
		if($j(obj).val()!=""){
			var id=$j(obj).attr('id');
			$j('#errorMsg_'+id).hide();
			$j('#dateMsg_'+id).hide();
			$j(obj).removeClass("criHighlight");
			if(id=='IDMdynaFormDate'){
				$j('#dynaFormDateError').hide();
			}
		}else{
			var id=$j(obj).attr('id');
			$j('#dateMsg_'+id).hide();
			if( $j('#isCompInfo').val() == "1" ){
				if( $j('#isCDRUser').val() == "true" ){
					if($j(obj).hasClass('isReqPriorToShip')){
						$j(obj).addClass("criHighlight");
					}
				}
			}
		}
	}else{
		var id=$j(obj).attr('id');
		$j('#errorMsg_'+id).hide();
	}
}
function loadMoredivsCRI(url,divname,formId){
	showprogressMgs();
	var formId1 = $j('#formId').val();
	var changeFlag = divChanges("idmchangeClass",$j('#'+formId1+'ModalForm').html());
	if(!changeFlag){
		changeFlag = divChanges("assessmentChngCls",$j('#'+formId1+'ModalForm').html());
	}
	var criChFlag;
	if(changeFlag==false){
		criChFlag = $j("#dialogForCRI").find("#criChangeFlag").val();
	}else if(changeFlag==true){
		criChFlag = changeFlag;
	}
	url = url+'?changeFlag='+changeFlag+'&criChangeFlag='+criChFlag;
	var divnamearr = divname.split(",");
	var newdivname = "";
	$j('.findlen').removeAttr('disabled');
	$j('#dynaFormDiv textarea').removeAttr('disabled');
	 $j.ajax({
	        type: "POST",
	        url: url,
	        async:false,
	        data:$j('#'+formId).serialize(),
	        success: function (result){
	        	var $response=$j(result);
	        	var errorpage = $response.find('#errorForm').html();
	        	if(errorpage != null &&  errorpage != "" ){
	            	$j('#main').html(result);
	           	 	closeprogressMsg();
	            }else{ 
		        		newdivname = "finalreviewmodel";
		        		$j("#"+newdivname).html(result);
		        	$j('#'+divnamearr[0]+' #dynaInfo').hide();
            		$j('#'+divnamearr[0]+' #eSignTable').hide();
                	$j('#'+divnamearr[0]+' #dynaUpdate').show();

                	/*var cordid = $j("#cordID").val();
                	var orderid =$j("#orderId").val();
                	var ordertype=$j("#orderType").val();
                	if(orderid!=null && orderid!="" && typeof(orderid)!="undefined" && ordertype!=null && ordertype!="" && typeof(ordertype)!="undefined"){
	                	var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID='+cordid+'&orderId='+orderid+'&orderType='+ordertype+'&pkcordId='+cordid+'&currentReqFlag=true';
	            		loadMoredivs(urlb,'viewcbutoppanel,quicklinksDiv,taskCompleteFlagsDiv,ctReqClinInfoContentDiv,finalreviewcontentDiv,currentRequestProgressMainDiv','cbuProcedureMain','update','status');//url,divname,formId,updateDiv,statusDiv
	            		setpfprogressbar();
	            		getprogressbarcolor();
	                }
	            	else{
	            		var urla = "loadClinicalData?cdrCbuPojo.cordID="+cordid;
	            		loadPageByGetRequset(urla,'cordSelectedData');
	            	}*/
               	 	closeprogressMsg();
	            }
	        },
	        error: function (request, status, error) {
	        	closeprogressMsg();
	        	alert("Error " + error);
	            alert(request.responseText);
	        }
		});
}
function addAdditionalIDMTest(obj){
	var additionalTests=$j('.additionalIDMTest').length;
	$j('.additionalIDMTest:first').removeClass('hidechild').next().removeClass('hidechild');
	$j('.additionalIDMTest:first').removeClass('additionalIDMTest').next().removeClass('additionalIDMTest');
	if(additionalTests <=2 ){
		$j(obj).hide();
	}
	$j("#idmaddNewtestFlag").addClass("idmchangeClass");
}

function commentclear(obj,index){
	var errorComment=index+'_Commenterror';
	if($j(obj).val()!='' && $j(obj).val()!=null){
		$j('#'+errorComment).hide();
	}else{
		$j('#'+errorComment).show();
	}
}
function setTabIndex(){
	var tabElement = $j(".tabclass").get(), t, tLen = tabElement.length;
	var ind = 0;
	$j(".tabclass").each(function(ind){
		$j(this).attr('tabindex',++ind);
	});
}
function setFocusToRes(evt,objId){
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if(charCode == 9){
		var obj = document.getElementById(objId); 
		$j(obj).focus();
	}
}

$j('[name="cordCbuStatus"]').val($j('#cordCbuStatus').val());
$j('#executeAutoDeferFlagID').val('idmTestAutoDeferExecFlag');

function autoDeferCloseModals(formCloseId){
	showprogressMgs();
	var cordid = $j("#cordID").val();
	var orderid =$j("#orderId").val();
	var ordertype=$j("#orderType").val();
	if(orderid!=null && orderid!="" && typeof(orderid)!="undefined" && ordertype!=null && ordertype!="" && typeof(ordertype)!="undefined"){
		var urlb = 'loadOrderPageDivs?cdrCbuPojo.cordID='+cordid+'&orderId='+orderid+'&orderType='+ordertype+'&pkcordId='+cordid+'&currentReqFlag=true';
		closeModals(formCloseId);
		loadMoredivs(urlb,'viewcbutoppanel,quicklinksDiv,taskCompleteFlagsDiv,ctReqClinInfoContentDiv,finalreviewcontentDiv,currentRequestProgressMainDiv','cbuProcedureMain','update','status');//url,divname,formId,updateDiv,statusDiv
		setpfprogressbar();
		getprogressbarcolor();
	}else{
		closeModals(formCloseId);
		var urla = "loadClinicalData?cdrCbuPojo.cordID="+cordid;
		loadPageByGetRequset(urla,'cordSelectedData');
	}
	closeprogressMsg();
}
</script>