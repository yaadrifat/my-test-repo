<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.aithent.audittrail.reports.EJBUtil"%>
<%@page import="com.velos.eres.web.user.UserJB"%>
<%@page import="com.velos.ordercomponent.business.util.VelosMidConstants"%>
<%@page import="com.velos.ordercomponent.action.VelosBaseAction"%>
<%@ page
	import="java.util.*,java.io.*,org.w3c.dom.*,com.velos.eres.web.grpRights.GrpRightsJB"%>
<jsp:useBean id="modCtlDao" scope="request"
	class="com.velos.eres.business.common.CtrlDao" />
<jsp:useBean id="sessionmaint" scope="session"
	class="com.velos.eres.service.util.SessionMaint" />
<%
	HttpSession tSession = request.getSession(true);
	GrpRightsJB grpRights = (GrpRightsJB) tSession
			.getAttribute("GRights");
	String accId = null;
	String logUsr = null;
	if (sessionmaint.isValidSession(tSession)) {
		accId = (String) tSession.getValue("accountId");
		logUsr = (String) tSession.getValue("userId");
		request.setAttribute("username", logUsr);
	}
%>
<script>	   
function addPrintContent(divId){
	var sourceFile ="";
	var patientStr=$j.trim($j("#patientId").val());
	var regidStr=$j.trim($j("#registryId").val());
	$j("#printDiv").html(sourceFile);
	sourceFile +="<table><thead><tr><th><center><h3>";
	 if(regidStr!="" && regidStr!="undefined"){
	 sourceFile +="<u>CBU Registry ID:"+regidStr+"</u>";
	}
	 if(patientStr!="" && patientStr!="undefined"){
	 	 sourceFile +="&nbsp;&nbsp;&nbsp;<u>Patient ID:"+patientStr+"</u>";
	 }
	 if(divId=="cbuShipmentInfoContentDiv"){
		    	sourceFile +="</h3></center><br/></th></tr></thead><tbody><tr><td>"+$j("#ctshipmentbarDiv").html()+"</td></tr></tbody></table>";
		 }
	 else if(divId=="orresolsubDiv"){
	    	sourceFile +="</h3></center><br/></th></tr></thead><tbody><tr><td>"+$j("#ctresolutionbarDiv").html()+"</td></tr></tbody></table>";
	 }
	 else if(divId=="ctcbuavailconfbarDiv"){
	    	sourceFile +="</h3></center><br/></th></tr></thead><tbody><tr><td>"+$j("#cbuavailconfbarDiv").html()+"</td></tr></tbody></table>";
	 }
	 else{
		 	    sourceFile +="</h3></center><br/></th></tr></thead><tbody><tr><td>"+$j('#'+divId).html()+"</td></tr></tbody></table>";
		 }
	 var source = $j("#printDiv").html(sourceFile);
	 $j("#printDiv").hide();
	 if(divId=="openOrdercbuhis"){
		printHistoryWidget('openOrdercbuhis',regidStr);
	 }
	 else{
		 clickheretoprint('printDiv','','1');
	 }
	 }
function shwMdal(pfcat,resistryId,divId)
{
	 var type = $j('#orderType').val();
	 var orderId = $j("#orderId").val();
	 var requestedDate = $j("#requestedDate").val();
	 var progresspermission = $j("#progressNotePermiossion").val(); 
	 var clinicalNotePermiossion = $j("#clinicalNotePermiossion").val();
		     	    	var subtp = '<s:text name="garuda.cbu.order.processConst"/>';
		     	    			 jConfirm('<s:text name="garuda.clinicalnote.label.confirmationmsg"/>', '<s:text name="garuda.common.lable.confirm"/>',
		     	    					 function(r) {
		     	   		     	 if (r == true) {
		     	   		     	    	var subtp = '<s:text name="garuda.cbu.order.noteConst"/>';
		     	   		     	   		if(clinicalNotePermiossion=="true")
		     	   		     	    			fn_showModalORNotes('<s:text name="garuda.header.label.clinicalNote" /> <s:property value="cdrCbuPojo.registryId" />','clinicalNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&subtype='+subtp,'550','850','notesModalsDiv');
		     	   		     	   		else
		     	   		     				alert('<s:text name ="garuda.clinicalNote.accessRights"/>');
		     	   		     	   return result;
		     	   		     	    }
		     	    	 		    else {
		     	    	 		     	    	var subtp = '<s:text name="garuda.cbu.order.processConst"/>';
		     	    	 		     	    	if(progresspermission=="true"){
		     	    	 		     	    		if(requestedDate!=null)
		    	 		     	    					{
		    	 		     	    						fn_showModalORNotes('<s:text name="garuda.header.label.progressNote"/> <s:property value="cdrCbuPojo.registryId" />','pfNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&pfcat='+pfcat+'&pkcordId='+$j("#pkcordId").val()+'&divId='+divId+'&requestedDate='+requestedDate,'600','850','notesModalDiv');
		    	 		     	    					}
		     	    	 		     				else
		 	 		     	    						{
		 	 		     	    							fn_showModalORNotes('<s:text name="garuda.header.label.progressNote"/> <s:property value="cdrCbuPojo.registryId" />','pfNotesCategory?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&pfcat='+pfcat+'&pkcordId='+$j("#pkcordId").val()+'&divId='+divId,'600','850','notesModalDiv');
		 	 		     	    						}
		     	    	 		     	    	}
		     	    	 		     	    	else
		     	    	 		     	    		{
		     	    	 		     	    				alert('<s:text name ="garuda.progressNote.accessRights"/>');
		     	    	 		     	    		}
		     	    	 		     	    }
		     	    	 		     });
		     
	  
}
function shwMdal1(resistryId,divId)
{
	 var type = $j('#orderType').val();
	 var orderId = $j("#orderId").val();
	 var requestedDate = $j("#requestedDate").val();
	var progresspermission = $j("#progressNotePermiossion").val(); 
	var clinicalNotePermiossion = $j("#clinicalNotePermiossion").val();
	 jConfirm('<s:text name="garuda.clinicalnote.label.confirmationmsg"/>', '<s:text name="garuda.common.lable.confirm"/>',
		       function(r) {
		     	 if (r == true) {
		     	    	var subtp = '<s:text name="garuda.cbu.order.noteConst"/>';
		     	    	if(clinicalNotePermiossion=="true")
		     	    		fn_showModalORNotes('<s:text name="garuda.header.label.clinicalNote" /> <s:property value="cdrCbuPojo.registryId" />','clinicalNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />&subtype='+subtp,'500','850','notesModalsDiv');
		     	    	else
		     	    		alert('<s:text name ="garuda.clinicalNote.accessRights"/>');
		     	    	return result;
		     	    }
		     	    else {
		     	    	var subtp = '<s:text name="garuda.cbu.order.processConst"/>';
		     	    	if(progresspermission=="true")
   	    			{
		     	    	if(requestedDate!=null)
		     	    		{
		     	    		fn_showModalORNotes('<s:text name="garuda.header.label.progressNote"/> <s:property value="cdrCbuPojo.registryId" />','pfNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&pkcordId='+$j("#pkcordId").val()+'&divId='+divId+'&requestedDate='+requestedDate,'500','850','notesModalDiv');
		     	    		}
		     	    		
		     	    	else
		     	    		{
		     	    		fn_showModalORNotes('<s:text name="garuda.header.label.progressNote"/> <s:property value="cdrCbuPojo.registryId" />','pfNotes?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_ADD_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&pkcordId='+$j("#pkcordId").val()+'&divId='+divId,'500','850','notesModalDiv');
		     	    		}
   	    			}
		     	    	else
   	    			{
   	    			alert('<s:text name ="garuda.progressNote.accessRights"/>');
   	    			}
		     	    }
		     });
	 getHiddedValues();
}
	 
function fn_showModalORNotes(title,url,hight,width,id){
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title=title+patient_data;
	showModals(title,url,hight,width,id);
	}
function fn_showModalORdetails(title,url,hight,width){
	var patientId = $j('#patientId').val();
	var patient_data = '';
	if(patientId!="" && patientId != null && patientId != "undefined"){
		patient_data = ', <s:text name="garuda.recipient&tcInfo.label.recipientid"/>:'+patientId;
	}
	title=title+patient_data;
	showModal(title,url,hight,width);
	}



function savecordAvailNmdp(){
	var orderId = $j("#orderId").val();
	 var type = $j('#orderType').val();
	fn_showModalORdetails('<s:text name="garuda.pf.modalwindowheader.cbuavailconfirm"/><s:property value="cdrCbuPojo.registryId" />','savecordAvailNmdp?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&orderId='+orderId+'&orderType='+type+'&cbuid=<s:property value="cdrCbuPojo.cordID" />'+'&moduleEntityId='+$j("#orderId").val()+'&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_ORDER" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_EDIT_CORD_AVAIL_CONFIRM" />','300','700');
}
$j(function() {
$j(".positive-integer").numeric({ decimal: false, negative: false }, function() { alert("<s:text name="garuda.common.cbu.positiveIntAlert"/>"); this.value = ""; this.focus(); });	
	$j("#confirmShipmentBtn").attr('disabled', true);
	$j("#ororderdetailsForm").validate({
		
		rules:{			
				"shipmentPojo.shipmentTime":{required:true,checkTimeFormat:true}					
			},
			messages:{
				"shipmentPojo.shipmentTime":{required:"<s:text name="garuda.cbu.cordentry.babybirthtime"/>",checkTimeFormat:"<s:text name="garuda.common.validation.timeformat"/>"}
			}
	});
	

	$j("#indSponserDetailsDiv").hide();	
	showIndSponserDetails();
	validateTime();

});
function validateTime(){
	$j(".timeValid").keyup(function(e) {
		  var val = $j(this).val();
		  if(e.keyCode!=8 && e.keyCode!=46 )
		  {
				  if(val.length==2){

						  if( IsNumeric(val) && val<24){
							  if(e.which!=58){
								   $j('#shedShipmentTime').val(val+":");
						  		}
						  }else{
							  alert("<s:text name="garuda.common.timeFormat.validHour"/>");
							  $j(this).val('');
						  }

			  	  }
				  
				  if(val.length==4 || val.length==5){
					  //alert("val lenght 4 && 3:::"+val.length+":::val:::"+val)
					  
					  if(val.length==4){
						  temp=val.substring(3);
					  }
					  if(val.length==5){
						  temp=val.substring(3,5);
					  }
					  
					  
					  //alert("temp leng 4 3::"+temp+"result of isnumberic"+IsNumeric(temp))
					  if( IsNumeric(temp)==false || (Number(temp)>59)){
						  alert("<s:text name="garuda.common.timeFormat.validMin"/>");
						  var str=$j(this).val();
						  var index=str.indexOf(":");
						  var modistr=str.substring(0,index+1);
						  $j('#shedShipmentTime').val(modistr);
					  }
					  
				  }
		
		  }
	});	
}
function IsNumeric(sText){
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++){ 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1){
         IsNumber = false;
      }
   }
   return IsNumber;
}



function submitcbuOrdResolution(){
	var availableDt="";
	var resolbycbb="";
	var resolbytc="";
	var resolutioncheck="";
	var userid="";
	if($j("#availdatepicker").val()==null){
		availableDt="";
	}
	else{
		availableDt=$j("#availdatepicker").val();
	}
	if($j("#ResolutionCBB").val()==null){
		resolbycbb="";
	}
	else{

		resolbycbb=$j("#ResolutionCBB").val();
	}
	if($j("#ResolutionTC").val()==null){
		resolbytc="";
	}
	else{
		resolbytc=$j("#ResolutionTC").val();
	}
	if($j("#ResoluAck").attr('checked')){
		resolutioncheck="Y";
	}
	else{
		resolutioncheck="N";
	}
	if($j("#currentuserid").val()==null){
		userid="";
	}
	else{
		userid=$j("#currentuserid").val();
	}
	/*
	if($j("#acceptToCancel").val()==null){var acceptToCancel="";}
	else{var acceptToCancel=$j("#acceptToCancel").val();}
	if($j("#datepicker6").val()==null){var rejectDt="";}
	else{var rejectDt=$j("#datepicker6").val();}
	if($j("#rejectby").val()==null){var rejectBy="";}
	else{var rejectBy=$j("#rejectby").val();}
	*/
	loadmultiplediv('submitCtResolution?pkcordId='+$j('#pkcordId').val()+'&availableDt='+availableDt+'&resolbycbb='+resolbycbb+'&resolbytc='+resolbytc+'&ackResolution='+resolutioncheck+'&userID='+userid+'&orderType='+$j('#orderType').val()+'&orderId='+$j("#orderId").val(),'orresolsubDiv,openOrdercbuhis,openOrdercbuInfo');
	historyTblOnLoad();
	Fnresol();
	if($j('#canackorders').val()!=""){
    	alert("<s:text name="garuda.cbu.ORorder.orderNotAckReason"/>");
    }
	getprogressbarcolor();
}

/*function createPackageSlip(){
	var url = 'createPackageSlip.action?orderId='+$j("#orderId").val()+'&pkcordId='+$j("#pkcordId").val()+'&shipmentType='+$j("#pkCbuShipment").val();
		window.open(url);
	
}*/

function confirmShipmentInfo(){
	jConfirm('<s:text name="garuda.cbu.ORorder.confirmShipmentAlert"/>', '<s:text name="garuda.pf.order.confirmation_alert"/>', function(r) {
	    if(r){
	    	//refreshDiv('confirmShipmentInfo?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val(),'cbuCordInfoMainDiv','cbuCordInformationForm');
	    	//loadMoredivs('confirmShipmentInfo?pkcordId='+$j("#pkcordId").val()+'&orderId='+$j("#orderId").val()+'&orderType='+$j("#orderType").val(),'main','ororderdetailsForm');
	    	submitpost('confirmShipmentInfo',{'pkcordId':$j("#pkcordId").val(),'orderId':$j("#orderId").val(),'orderType':$j("#orderType").val()});
	    	/*getHiddedValues();
	    	setpfprogressbar();
        	getprogressbarcolor();
        	historyTblOnLoad();
	    	getDayfromshipdate($j("#datepicker15").val(),'cbushimentdayDiv');
	    	*/
			}
	});	
		
	
}

function disableExcuseFlag(){
	if($j("#datepicker3").val()==null || $j("#datepicker3").val()==''){
		$j("#nmdpShipExcuse").removeAttr('disabled');
		$j("#researchExcuseDiv").show();
		$j("#shipcompmandspan").hide();
	}else{
		$j("#nmdpShipExcuse").attr('disabled','disabled');
		$j("#nmdpShipExcuse").removeAttr('checked');
		$j("#researchExcuseDiv").hide();
		$j("#shipcompmandspan").show();
	}
	
}

function disableShipmentDet(){
	var checkedflag=$j("#nmdpShipExcuse").attr('checked');
	if(checkedflag){
		$j("#fkNmdpshipingCompanyId").attr('disabled','disabled');
		$j("#fkNmdpshipingCompanyId").val("");
		$j("#researchSampleshipmentTrackingNo").attr('disabled','disabled');
		$j("#researchSampleshipmentTrackingNo").val("");
		$j("#datepicker3").attr('disabled','disabled');
		$j("#datepicker3").val("");
		$j("#shipcompmandspan").hide();
	}else{
		$j("#fkNmdpshipingCompanyId").removeAttr('disabled');
		$j("#researchSampleshipmentTrackingNo").removeAttr('disabled');
		$j("#datepicker3").removeAttr('disabled');
		if($j("#datepicker3").val()!=null && $j("#datepicker3").val()!='' && $j("#datepicker3").val()!='undefined'){
			$j("#shipcompmandspan").show();
		}
	}
	
}

function showIndSponserDetails(){
	var indsponser=$j('input:checkbox[name=ind_sponsor]:checked').val();
	if(indsponser==$j("#indSponsorNoPk").val()){
		$j("#indSponserDetailsDiv").show();
	}
}

$j(function(){
	getHiddedValues();
});
</script>
<s:hidden name="cdrCbuPojo.registryId" id="registryId"/>
<s:form id="ororderdetailsForm">
	<s:set name="orderType" value="orderType" scope="request" />
	<s:hidden name="clinicalNotePojo.requestType" />
	<input type="hidden" id="clinicalNotePermiossion" value="<s:property value="hasNewPermission(#request.vClnclNote)"/>"/>
    <input type="hidden" id="progressNotePermiossion" value="<s:property value="hasNewPermission(#request.vPrgrssNote)"/>"/>
	
	 <table>
		<tr>
			<td width="100%">
			<div id="orordertaskListProgressBarDiv">
			<div class="portlet" id="openOrdercbuInfoparent">
			<s:if test="hasViewPermission(#request.currntReqst)==true">
			<div id="openOrdercbuInfo"
				class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
			<div  class="portlet-header ui-widget-header ui-widget-content ui-corner-all">
			<span class="ui-icon ui-icon-print" onclick="getCurrentRequestProgressDetails('currentRequestProgressMainDiv,ortcorderdetailscontent,caseManagerPatientandTCdetailsDiv,cbuShipmentInfoContentDiv,ctReqClinInfoContentDiv');addPrintContent('openOrdercbuInfo')"></span>
			<span class="ui-icon ui-icon-plusthick" id="currentReqProSpan" onclick="getCurrentRequestProgressDetails('currentRequestProgressMainDiv,ortcorderdetailscontent,caseManagerPatientandTCdetailsDiv,cbuShipmentInfoContentDiv,ctReqClinInfoContentDiv');"></span>
			<!--<span class="ui-icon ui-icon-minusthick"></span>--> <s:text
				name="garuda.ctOrderDetail.portletname.currentReqProgress" /></div>
			<div class="portlet-content" id="currentRequestProgressMainDiv" style="display: none;">
			<s:hidden name="tskReqClincInfocompl" id="tskReqClincInfocompl"></s:hidden>
			<s:hidden name="tskFinalCbuReviewcompl" id="tskFinalCbuReviewcompl"></s:hidden>
			<s:hidden name="tskShipmentInfocompl" id="tskShipmentInfocompl"></s:hidden>
			<s:hidden name="tskGenPackageSlipcompl" id="tskGenPackageSlipcompl"></s:hidden>
			<s:hidden name="tskAdditiTypingcompl" id="tskAdditiTypingcompl"></s:hidden>
			<s:hidden name="tskShipmentConfirmcompl" id="tskShipmentConfirmcompl"></s:hidden>
			<s:hidden name="tskSampleShipmentcompl" id="tskSampleShipmentcompl"></s:hidden>
			<s:hidden name="tskAcknowledgeResolcompl" id="tskAcknowledgeResolcompl"></s:hidden> 
			<table>
					<tr>
						<td colspan="10" rowspan="5" align="right">
						    <s:if test="hasViewPermission(#request.viewORReport)==true">
						    <s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
							<button type="button" onclick="window.open('cbuPdfReports?cdrCbuPojo.CordID=<s:property value="cdrCbuPojo.CordID"/>&repId=183&repName=NMDP OR Detail Report&reqDate=<s:property value="%{#rowVal[45]}" />&selDate=&params=<s:property value="orderId"/>','Information','toolbar=no,scrollbars=yes,resizable=yes,menubar=no,status=yes,width=950,height=700');"><s:text
							name="garuda.ordersreport.button.label"></s:text></button>
							</s:iterator>
							</s:if>
							</s:if>
						</td>
					</tr>
			</table>
				<s:if test='NMDPResearchSampleFlag==true'>
				<table>
					<tr style="font-style: italic; color: blue;">
						<td width="5%" height="20px"></td>
						<td width="10%" align="center"><a href="#cbuavailconfbarDiv"
							id="cbuavailtd" onclick="togglePFDivs('cordAvailspan','ctcbuavailconfbarDiv','getCordAvailDetails')"><s:property value="tskConfirmCbuAvail" /></a></td>
						<td width="10%" align="center"><a href="#reqclincinfobarDiv" onclick="togglePFDivs('reqclinspanid','ctReqClinInfoContentDiv','')"><s:property
							value="tskReqClincInfo" /></a></td>
						<td width="10%" align="center"><a
							href="#finalcbureviewbarDiv"  onclick="togglePFDivs('finalreviewspanid','finalreviewcontentDiv','')"><s:property
							value="tskFinalCbuReview" /></a></td>
						<td width="10%" align="center"><a href="#ctshipmentbarDiv" onclick="togglePFDivs('cbushipspan','cbuShipmentInfoContentDiv','getcbushimentDetails')"><s:property
							value="tskShipmentInfo" /></a></td>
						<td width="10%" align="center"><s:if
							test="tskGenPackageSlip!=null && tskGenPackageSlip!=''">
							<a href="#ctshipmentbarDiv" onclick="togglePFDivs('cbushipspan','cbuShipmentInfoContentDiv','getcbushimentDetails')"><s:text
								name="garuda.cbuShipment.label.createshipmentpacket" /></a>
						</s:if></td>
						<td width="10%" align="center"><a href="#ctshipmentbarDiv" onclick="togglePFDivs('cbushipspan','cbuShipmentInfoContentDiv','getcbushimentDetails')"><s:property
							value="tskShipmentConfirm" /></a></td>
						<td width="10%" align="center"><a
							href="#nmdpresearchSampleDiv" onclick="togglePFDivs('nmdpshipspan','nmdpResearchSampleContentDiv','getnmdpshipmentdetails')"><s:property
							value="tskSampleShipment" /></a></td>
						<td width="10%" align="center"></td>
						<td width="10%" align="center"><a href="#ctresolutionbarDiv"
							id="resoltd" onclick="togglePFDivs('ctResoultionspan','orresolsubDiv','getResolutionDetails','orresolsubDiv')"><s:property value="tskAcknowledgeResol" /></a></td>
						<td width="5%" align="center" rowspan="4"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="%{#rowVal[48]>2}">
									<span style="color: red; font-size: 30px"><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:if>
								<s:else>
									<span style="font-size: 30px""><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:else>
							</s:iterator>
						</s:if></td>
					</tr>
					<tr>
						<td width="5%" height="20px"></td>
						<td width="10%" id="cbuavailconfbar"></td>
						<td width="10%" id="completeReqInfobar"></td>
						<td width="10%" id="finalReviewbar"></td>
						<td width="10%" id="ctshipmentbar"></td>
						<td width="10%" id="genpackageslipbar"></td>
						<td width="10%" id="cordShipedbar"></td>
						<td width="10%" id="nmdpsamplebar"></td>
						<td width="10%" id="Resolutionbar"></td>
						<td width="10%" id="Acknowledgebar"></td>

					</tr>
					<tr>
						<td width="5%"></td>
						<td width="10%" align="left"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="left"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>

					</tr>
					<tr style="font-style: italic; color: green;">
						<td width="5%"></td>
						<td width="10%" align="left" style=""><s:property
							value="msNew" /></td>
						<td width="10%" align="left"><s:property value="msReserved" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><s:property
							value="msShipmentsch" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><s:property
							value="msCordShipped" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><s:property value="msResolved" /></td>
						<td width="10%" align="right"><s:property value="msComplete" /></td>

					</tr>
				</table>
			</s:if> <s:else>
				<table>
					<tr style="font-style: italic; color: blue;">
						<td width="5%" height="20px"></td>
						<td width="10%" align="center"><a href="#cbuavailconfbarDiv"
							id="cbuavailtd" onclick="togglePFDivs('cordAvailspan','ctcbuavailconfbarDiv','getCordAvailDetails')"><s:property value="tskConfirmCbuAvail" /></a></td>
						<td width="10%" align="center"><a href="#reqclincinfobarDiv" onclick="togglePFDivs('reqclinspanid','ctReqClinInfoContentDiv','')"><s:property
							value="tskReqClincInfo" /></a></td>
						<td width="10%" align="center"><a
							href="#finalcbureviewbarDiv" onclick="togglePFDivs('finalreviewspanid','finalreviewcontentDiv','')"><s:property
							value="tskFinalCbuReview" /></a></td>
						<td width="10%" align="center"><a href="#ctshipmentbarDiv" onclick="togglePFDivs('cbushipspan','cbuShipmentInfoContentDiv','getcbushimentDetails')"><s:property
							value="tskShipmentInfo" /></a></td>
						<td width="10%" align="center"><s:if
							test="tskGenPackageSlip!=null && tskGenPackageSlip!=''">
							<a href="#ctshipmentbarDiv" onclick="togglePFDivs('cbushipspan','cbuShipmentInfoContentDiv','getcbushimentDetails')"><s:text
								name="garuda.cbuShipment.label.createshipmentpacket" /></a>
						</s:if></td>
						<td width="10%" align="center"><a href="#ctshipmentbarDiv" onclick="togglePFDivs('cbushipspan','cbuShipmentInfoContentDiv','getcbushimentDetails')"><s:property
							value="tskShipmentConfirm" /></a></td>
						<td width="10%" align="center"></td>
						<td width="10%" align="center"><a href="#ctresolutionbarDiv"
							id="resoltd" onclick="togglePFDivs('ctResoultionspan','orresolsubDiv','getResolutionDetails','orresolsubDiv')"><s:property value="tskAcknowledgeResol" /></a></td>
						<td width="15%" align="center" rowspan="4"><s:if
							test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
							<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="%{#rowVal[48]>2}">
									<span style="color: red; font-size: 30px"><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:if>
								<s:else>
									<span style="font-size: 30px""><s:property
										value="%{#rowVal[48]}" /></span>
									<br />
									<s:text name="garuda.currentReqProgress.label.noofdays" />
								</s:else>
							</s:iterator>
						</s:if></td>
					</tr>
					<tr>
						<td width="5%" ></td>
						<td width="10%" id="cbuavailconfbar"></td>
						<td width="10%" id="completeReqInfobar"></td>
						<td width="10%" id="finalReviewbar"></td>
						<td width="10%" id="ctshipmentbar"></td>
						<td width="10%" id="genpackageslipbar"></td>
						<td width="10%" id="cordShipedbar"></td>
						<td width="10%" id="Resolutionbar"></td>
						<td width="10%" id="Acknowledgebar"></td>

					</tr>
					<tr>
						<td width="5%"></td>
						<td width="10%" align="left"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="left"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>
						<td width="10%" align="right"><img src="images/sort_asc.png" /></td>

					</tr>
					<tr style="font-style: italic; color: green;">
						<td width="5%"></td>
						<td width="10%" align="left" style=""><s:property
							value="msNew" /></td>
						<td width="10%" align="left"><s:property value="msReserved" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><s:property
							value="msShipmentsch" /></td>
						<td width="10%" align="right"></td>
						<td width="10%" align="right"><s:property
							value="msCordShipped" /></td>
						<td width="10%" align="right"><s:property value="msResolved" /></td>
						<td width="10%" align="right"><s:property value="msComplete" /></td>

					</tr>
				</table>
			</s:else>
			<table>
				<tr>
					<td width="100%" colspan="3">
					<fieldset>
					<div id="currentRequestProgress">
					<table>
						<tr>
							<td width="10%"><strong><s:text
								name="garuda.cbuHistory.label.requesttype" />:</strong></td>
							<td width="15%"><s:text
								name="garuda.orOrderDetail.portletname.orOrder"></s:text></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.requeststatus"></s:text>:</strong>
							</td>
							<td width="15%"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<span style="color: red;"> <s:property
										value="%{#rowVal[0]}" /></span>
								</s:iterator>
							</s:if> <s:else>
								<s:text name="garuda.currentReqProgress.label.notprovided"></s:text>
							</s:else></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.reqStatusDate"></s:text>:</strong>
							</td>
							<td width="15%"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:if test="#rowVal[1]!=null && #rowVal[1]!=''">
									<s:property value="%{#rowVal[1]}" />
								</s:if>
								</s:iterator>
							</s:if></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.resolbytc" />:</strong></td>
							<td width="15%"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
								    <s:if test='#rowVal[61]!=null && #rowVal[61]!=""'>
									<s:hidden name="nmdpSampleShipped" id="nmdpSampleShipped" value="%{#rowVal[61]}"></s:hidden>
									</s:if>
									<s:if test='#rowVal[57]!=null && #rowVal[57]!=""'>
										<s:property value="getCbuStatusDescByPk(#rowVal[57])" />
										<s:hidden name="resolByTCFlag" id="resolByTCFlag" value="%{#rowVal[57]}"></s:hidden>
										<s:hidden name="resolByCBBFlag" id="resolByCBBFlag"	value="%{#rowVal[58]}"></s:hidden>
									</s:if>
									<s:elseif test='#rowVal[58]!=null && #rowVal[58]!=""'>
									<s:property value="getCbuStatusDescByPk(#rowVal[58])" />
										<s:hidden name="resolByTCFlag" id="resolByTCFlag" value="%{#rowVal[57]}"></s:hidden>
										<s:hidden name="resolByCBBFlag" id="resolByCBBFlag"	value="%{#rowVal[58]}"></s:hidden>
									</s:elseif>
								</s:iterator>
							</s:if></td>
						</tr>
						<tr>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.orrequestdate" />:</strong></td>
							<td width="15%"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<s:property value="%{#rowVal[45]}" />
									<s:hidden id="requestedDate" value="%{#rowVal[45]}" />
								</s:iterator>
							</s:if></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.assignedto" />:</strong></td>
							<td width="15%"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:if test="lstUsers!=null && lstUsers.size()>0">
									<s:hidden name="assignLstSize" id="assignLstSize" value="%{lstUsers.size()}"/>
								</s:if>
								<s:iterator value="ctOrderDetailsList" var="rowVal">
								<s:hidden name="hAssignTo" id="hAssignTo" value="%{#rowVal[3]}"/>
									<s:if test='#rowVal[3]!=null && #rowVal[3]!=""'>
										<s:if test="lstUsers!=null && lstUsers.size()>0">
											<s:if
												test="hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true">
												<s:if test="hasEditPermission(#request.assignOrders)!=true">
													<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">-Select-</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test='%{#rowVal[3]==#tempVal}' >selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
													</select>
												</s:if>
												<s:else>
													<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">-Select-</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<s:set name="tempVal" id="tempVal" value="%{#rowIndex[0]}" />
																			<option value='<s:property value="%{#rowIndex[0]}"/>' <s:if test='%{#rowVal[3]==#tempVal}' >selected="selected"</s:if>><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
													</select>
												</s:else>
											</s:if>
										</s:if>

									</s:if>
									<s:elseif
										test="(hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true) && (#rowVal[3]==null || #rowVal[3]=='')">
										<s:if test="lstUsers!=null && lstUsers.size()>0">
											<s:if test="hasEditPermission(#request.assignOrders)!=true">
												<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' ><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
													</select>
											</s:if>
											<s:else>
												<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' ><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
												</select>
											</s:else>
										</s:if>
									</s:elseif>
								</s:iterator>
							</s:if> <s:else>

								<s:if
									test="hasViewPermission(#request.assignOrders)==true || hasEditPermission(#request.assignOrders)==true">
									<s:if test="lstUsers!=null && lstUsers.size()>0">
										<s:if test="hasEditPermission(#request.assignOrders)!=true">
											<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)" disabled="disabled">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' ><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
													</select>
										</s:if>
										<s:else>
											<select id="userIdval" name="userIdval" style="height:25px" onchange="showEsignAssignTo(this)">
																			<option value="">Unassigned</option>
																			<s:iterator value="lstUsers" var ="rowIndex">
																			<option value='<s:property value="%{#rowIndex[0]}"/>' ><s:property value="%{#rowIndex[2]}"/></option>
																			</s:iterator>
											</select>
										</s:else>
									</s:if>
								</s:if>
							</s:else>
							</td>
							<s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<s:if test='#rowVal[3]!=null && #rowVal[3]!=""'>
										<td width="10%" align="left"><strong><s:text
											name="garuda.currentReqProgress.label.assigneddate" />:</strong></td>
										<td width="15%" align="left"><s:property
											value="%{#rowVal[4]}" /></td>
									</s:if>
									<s:else>
										<td width="10%" align="left"></td>
										<td width="15%" align="left"></td>
									</s:else>
								</s:iterator>
							</s:if>
							<s:else>
								<td width="10%" align="left"></td>
								<td width="15%" align="left"></td>
							</s:else>
							<td width="10%" align="left"><strong><s:text
								name="garuda.heResolution.label.resolutiondate" />:</strong></td>
							<td width="15%" align="left"><s:if
								test="orderResolLst!=null && orderResolLst.size>0">
								<s:iterator value="orderResolLst" var="rowVal">
									<s:if test="#rowVal[2]!=null && #rowVal[2]!=''">
										<s:property value="%{#rowVal[2]}" />
									</s:if>
								</s:iterator>
							</s:if></td>
							
						</tr>
						<tr>
							<td width="10%" align="left"><strong><s:text
									name="garuda.currentReqProgress.label.priority"></s:text>:</strong></td>
								<td width="15%" align="left"><s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:if test="#rowVal[64]!=null && #rowVal[64]!=''">
											<s:property value="%{#rowVal[64]}" />
										</s:if>
										<s:else>
											<s:text name="garuda.currentReqProgress.label.notprovided" />
										</s:else>
									</s:iterator>
								</s:if><s:else>
									<s:text name="garuda.currentReqProgress.label.notprovided" />
								</s:else></td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.lastrequestreviewedby" />:</strong>
							</td>
							<td width="15%"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<s:if test='#rowVal[5]!=null && #rowVal[5]!=""'>
										<s:property value="%{#rowVal[5]}" />
									</s:if>
								</s:iterator>
							</s:if></td>	
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.lastrevieweddate" />:</strong></td>
							<td width="15%"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<s:if test="#rowVal[6]!=null && #rowVal[6]!=''">
									<s:property value="%{#rowVal[6]}" />
								</s:if>
								</s:iterator>
							</s:if></td>
							<td width="10%" align="left"><strong><s:text
								name="garuda.currentReqProgress.label.closereason" />:</strong></td>
							<td width="15%" align="left"><s:if
								test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
								<s:iterator value="ctOrderDetailsList" var="rowVal">
									<s:if test="%{#rowVal[75]!=null && #rowVal[75]!=''}">
										<s:property value="getCbuStatusDescByPk(#rowVal[75])" />
									</s:if>
								</s:iterator>
							</s:if></td>
							
						</tr>
						<tr>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.propcbushipdate" />:</strong></td>
							<td width="15%">
								<s:if test="#rowVal[85]!=null && #rowVal[85]!=''">
									<s:property value="%{#rowVal[85]}" />
								</s:if>
								<s:else>
									<s:text name="garuda.currentReqProgress.label.notprovided"></s:text>
								</s:else>
							</td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.propprepdate" />:</strong></td>
							<td width="15%">
								<s:if test="#rowVal[26]!=null && #rowVal[26]!=''">
									<s:property value="%{#rowVal[26]}" />
								</s:if>
								<s:else>
									<s:text name="garuda.currentReqProgress.label.notprovided"></s:text>
								</s:else>
							</td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.propinfusiondate" />:</strong></td>
							<td width="15%">
								<s:if test="#rowVal[25]!=null && #rowVal[25]!=''">
									<s:property value="%{#rowVal[25]}" />
								</s:if>
								<s:else>
									<s:text name="garuda.currentReqProgress.label.notprovided"></s:text>
								</s:else>
							</td>
							<td width="10%"><strong><s:text
								name="garuda.currentReqProgress.label.infusiondate" />:</strong></td>
							<td width="15%">
								<s:if test="#rowVal[56]!=null && #rowVal[56]!=''">
									<s:property value="%{#rowVal[56]}" />
								</s:if>
								<s:else>
									<s:text name="garuda.currentReqProgress.label.notprovided"></s:text>
								</s:else>
							</td>
						</tr>
					</table>
					</div>
					</fieldset>
					</td>
				</tr>
			</table>
			</div>
			</div>
			</s:if>
			</div>
			</div>
			<div id="ctassignto" style="display: none;">
							<table bgcolor="#cccccc" class="tabledisplay disable_esign" id="">
								<tr bgcolor="#cccccc" valign="baseline">
									<td width="70%"><jsp:include page="cb_esignature.jsp"
										flush="true">
										<jsp:param value="submitAssignto_OR" name="submitId" />
										<jsp:param value="invalidAssignto" name="invalid" />
										<jsp:param value="minimumAssignto" name="minimum" />
										<jsp:param value="passAssignto" name="pass" />
									</jsp:include></td>
									<td align="left" width="30%"><input type="button"
										id="submitAssignto_OR" onclick="submitAssignto();"
										value="<s:text name="garuda.unitreport.label.button.submit"/>"
										disabled="disabled" /> <input type="button" id="closediv"
										onclick="$j('#ctassignto').hide();" value="Close" /></td>
								</tr>
							</table>
			</div>
			 </td>
		</tr>
		<tr>
			<td>
			<div id="Ororderdiv">
			<div id="pendingOrdersSearchparent">
			<div id="openOrderSearch" >
			<table>
				<tr>

					<td colspan="3">
					<table>

						<tr>
							<td width="49.5%" style="vertical-align: top"><!-- TC Order Details -->
							<s:if test="hasViewPermission(#request.viewTCORDetail)==true">
								<div class="portlet" id="ortcorderdetailsDivparent">
								<div id="ortcorderdetailsDiv"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-print"
									onclick="getcbushipmentDetails();addPrintContent('ortcorderdetailsDiv')"></span><span
									class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id="ortcorderdetailsspan" onclick="getcbushipmentDetails()"></span>
								<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
									class="ui-icon ui-icon-minusthick"></span>--> <s:text
									name="garuda.orOrderDetail.portletname.tcorderdetails" /></div>
								<div class="portlet-content" style="display: none;" id="ortcorderdetailscontent">
								<table>
									<tr>
										<td colspan="2">
										<table width="100%">
											<s:if
												test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
												<s:iterator value="ctOrderDetailsList" var="rowVal">
													<tr>
														<td width="25%"><strong><s:text
															name="garuda.tcorderdetails.label.willthisshipment" /></strong></td>
														<td width="75%" colspan="3"><s:checkboxlist
															name="ind_sponsor" listKey="pkCodeId"
															listValue="description"
															list="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@IND_PROTOCOL]"
															disabled="true" value="%{#rowVal[68]}"></s:checkboxlist>
														</td>
													</tr>

													<tr id="indSponserDetailsDiv">
														<td width="25%"><strong><s:text
															name="garuda.cbuentry.label.ind_sponsor" /></strong>:</td>
														<td width="25%"><s:property value="%{#rowVal[69]}" />
														</td>
														<td width="25%"><strong><s:text
															name="garuda.cbuentry.label.ind_number" /></strong>:</td>
														<td width="25%"><s:property value="%{#rowVal[70]}" />
														</td>
													</tr>
													<tr>
														<td width="25%"><strong><s:text
															name="garuda.tcorderdetails.label.currentdiagnosis" /></strong>:</td>
														<td width="25%"><s:property value="%{#rowVal[14]}" />
														</td>
														<td width="25%"></td>
														<td width="25%"></td>
													</tr>
													<tr>
													<td colspan="4">
														<fieldset><legend><s:text
															name="garuda.tcorderdetails.label.typeoftransplantrecipient" /></legend>
														<table width="100%">
															<tr>
																<td width="50%"><strong><s:text
																	name="garuda.tcorderdetails.label.singletrans" /></strong>:</td>
																<td width="50%">
																<s:if
															test='%{#rowVal[83]!=null && #rowVal[83]=="Y"}'>
															<s:text name="garuda.opentask.label.yes"></s:text>
														</s:if> <s:if test='%{#rowVal[83]!=null && #rowVal[83]=="N"}'>
															<s:text name="garuda.cbushipmetInfo.label.no"></s:text>
														</s:if> <s:if test='%{#rowVal[83]==null || #rowVal[83]==""}'>
															<s:text
																name="garuda.currentReqProgress.label.notprovided" />
														</s:if>
																</td>
															</tr>
															<tr>
																<td width="50%"><strong><s:text
																	name="garuda.tcorderdetails.label.multitrans" /></strong>:</td>
																<td width="50%">
																<s:if
															test='%{#rowVal[81]!=null && #rowVal[81]=="Y"}'>
															<s:text name="garuda.opentask.label.yes"></s:text>
														</s:if> <s:if test='%{#rowVal[81]!=null && #rowVal[81]=="N"}'>
															<s:text name="garuda.cbushipmetInfo.label.no"></s:text>
														</s:if> <s:if test='%{#rowVal[81]==null || #rowVal[81]==""}'>
															<s:text
																name="garuda.currentReqProgress.label.notprovided" />
														</s:if>
																</td>
															</tr>
															<tr>
																<td width="50%"><strong><s:text
																	name="garuda.tcorderdetails.label.exvivo" /></strong>:</td>
																<td width="50%">
																<s:if
															test='%{#rowVal[80]!=null && #rowVal[80]=="Y"}'>
															<s:text name="garuda.opentask.label.yes"></s:text>
														</s:if> <s:if test='%{#rowVal[80]!=null && #rowVal[80]=="N"}'>
															<s:text name="garuda.cbushipmetInfo.label.no"></s:text>
														</s:if> <s:if test='%{#rowVal[80]==null || #rowVal[80]==""}'>
															<s:text
																name="garuda.currentReqProgress.label.notprovided" />
														</s:if>
																</td>
															</tr>
															
															<tr>
																<td width="50%"><strong><s:text
																	name="garuda.tcorderdetails.label.othtrans" /></strong>:</td>
																<td width="50%">
																<s:if
															test='%{#rowVal[82]!=null && #rowVal[82]=="Y"}'>
															<s:text name="garuda.opentask.label.yes"></s:text>
														</s:if> <s:if test='%{#rowVal[82]!=null && #rowVal[82]=="N"}'>
															<s:text name="garuda.cbushipmetInfo.label.no"></s:text>
														</s:if> <s:if test='%{#rowVal[82]==null || #rowVal[82]==""}'>
															<s:text
																name="garuda.currentReqProgress.label.notprovided" />
														</s:if>
																</td>
															</tr>
															
														</table>
														</fieldset>
													</td>
													</tr>
													<tr>
														<td width="25%"><strong><s:text
															name="garuda.tcorderdetails.label.diseasestage" /></strong>:</td>
														<td width="25%"><s:property value="%{#rowVal[20]}" />
														</td>
														<td width="25%"><strong><s:text
															name="garuda.tcorderdetails.label.noofremissions" /></strong>:</td>
														<td width="25%"><s:property value="%{#rowVal[21]}" />
														</td>
													</tr>
													<tr>
														<td width="25%"><strong><s:text
															name="garuda.tcorderdetails.label.currentstateofleukemia" /></strong>:
														</td>
														<td width="25%"><s:property value="%{#rowVal[22]}" />
														</td>
														<td width="25%"><strong><s:text
															name="garuda.tcorderdetails.label.recipienttransfused" /></strong>:
														</td>
														<td width="25%"><s:property value="%{#rowVal[23]}" />
														</td>
													</tr>
													<tr>
														<td width="25%"><strong><s:text
															name="garuda.tcorderdetails.label.recipientweight" /></strong>:</td>
														<td width="25%"><s:property value="%{#rowVal[24]}" />
														</td>
														<td width="25%"><strong><s:text
															name="garuda.currentReqProgress.label.proposedcbushipdate" /></strong>:
														</td>
														<td width="25%">
																<s:property value="%{#rowVal[85]}" />
														</td>
													</tr>
													<tr>
														<td width="25%"><strong><s:text
															name="garuda.currentReqProgress.label.proposedinfusiondate" /></strong>:
														</td>
														<td width="25%"><s:property value="%{#rowVal[25]}" />
														</td>
														<td width="25%"><strong><s:text
															name="garuda.currentReqProgress.label.proposedprepationdate" /></strong>:
														</td>
														<td width="25%"><s:property value="%{#rowVal[26]}" />
														</td>
													</tr>
													<tr>
														<td width="25%"><strong><s:text
															name="garuda.tcorderdetails.label.residualcellularsamples" /></strong>:
														</td>
														<td width="25%"><s:if
															test='%{#rowVal[27]!=null && #rowVal[27]=="Y"}'>
															<s:text name="garuda.opentask.label.yes"></s:text>
														</s:if> <s:if test='%{#rowVal[27]!=null && #rowVal[27]=="N"}'>
															<s:text name="garuda.cbushipmetInfo.label.no"></s:text>
														</s:if> <s:if test='%{#rowVal[27]==null || #rowVal[27]==""}'>
															<s:text
																name="garuda.currentReqProgress.label.notprovided" />
														</s:if></td>
														<td width="25%"></td>
														<td width="25%"></td>
													</tr>
												</s:iterator>
											</s:if>
										</table>
										</td>
									</tr>
									<s:if
										test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
										<tr>
											<td colspan="2">
											<div>
											<fieldset><legend><s:text
												name="garuda.tcorderdetails.label.deliveryinst" /></legend>
											<table width="100%">
												<s:if
													test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
													<s:iterator value="ctOrderDetailsList" var="rowVal">
														<tr>
															<td width="50%"><strong> <s:text
															name="garuda.tcorderdetails.label.attnname" /></strong>:</td>
														<td width="50%">
														<s:if test='%{#rowVal[84]!=null && #rowVal[84]!=" "}'>
																<s:property value="%{#rowVal[84]}" />
														</s:if>
														<s:else>
															<s:text	name="garuda.currentReqProgress.label.notprovided" />
														</s:else>
														</td>
														</tr>
														<tr>
															<td width="50%"><strong><s:text
																name="garuda.tcorderdetails.label.centername" /></strong>:</td>
															<td width="50%"><s:property value="%{#rowVal[28]}" />
															</td>
														</tr>
														<tr>
															<td width="50%"><strong><s:text
																name="garuda.tcorderdetails.label.addline1" /></strong>:</td>
															<td width="50%"><s:property value="%{#rowVal[29]}" />
															</td>
														</tr>
														<tr>
															<td width="50%"><strong><s:text
																name="garuda.tcorderdetails.label.addline2" /></strong>:</td>
															<td width="50%"><s:property value="%{#rowVal[30]}" />
															</td>
														</tr>
														<tr>
															<td width="50%"><strong><s:text
																name="garuda.tcorderdetails.label.city" /></strong>:</td>
															<td width="50%"><s:property value="%{#rowVal[31]}" />
															</td>
														</tr>
														<tr>
															<td width="50%"><strong><s:text
																name="garuda.tcorderdetails.label.country" /></strong>:</td>
															<td width="50%"><s:property value="%{#rowVal[34]}" />
															</td>
														</tr>
														<tr>
															<td width="50%"><strong><s:text
																name="garuda.tcorderdetails.label.addcomments" /></strong>:</td>
															<td width="50%"><s:property value="%{#rowVal[35]}" />
															</td>
														<tr>
													</s:iterator>
												</s:if>
											</table>
											</fieldset>
											</div>
											</td>
										</tr>
										<tr>
											<td colspan="2">
											<div>

											<fieldset><legend> <s:text
												name="garuda.tcorderdetails.label.deliverycont1" /></legend>
											<table>
												<s:if
													test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
													<s:iterator value="ctOrderDetailsList" var="rowVal">
														<tr>
															<td width="25%"><strong><s:text
																name="garuda.tcorderdetails.label.contactname1" /></strong>:</td>
															<td width="25%">
																<s:if test='%{#rowVal[36]!=null && #rowVal[36]!=" "}'>
																		<s:property value="%{#rowVal[36]}" />
																</s:if>
																<s:else>
																	<s:text	name="garuda.currentReqProgress.label.notprovided" />
																</s:else>
															</td>
															<td width="25%"><strong><s:text
																name="garuda.tcorderdetails.label.phforcontact1" /></strong>:</td>
															<td width="25%"><s:property value="%{#rowVal[37]}" />
															</td>
														</tr>
														<tr>
															<td width="25%"><strong><s:text
																name="garuda.tcorderdetails.label.mobileforcontact1" /></strong>:
															</td>
															<td width="25%"><s:property value="%{#rowVal[38]}" />
															</td>
															<td width="25%"><strong><s:text
																name="garuda.tcorderdetails.label.faxforcontact1" /></strong>:</td>
															<td width="25%">
															<s:property value="%{#rowVal[77]}" /></td>
														</tr>
													</s:iterator>
												</s:if>
											</table>
											</fieldset>
											<fieldset><legend><s:text
												name="garuda.tcorderdetails.label.deliverycont2" /></legend>
											<table>
												<s:if
													test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
													<s:iterator value="ctOrderDetailsList" var="rowVal">
														<tr>
															<td width="25%"><strong><s:text
																name="garuda.tcorderdetails.label.contactname2" /></strong>:</td>
															<td width="25%">
																<s:if test='%{#rowVal[39]!=null && #rowVal[39]!=" "}'>
																		<s:property value="%{#rowVal[39]}" />
																</s:if>
																<s:else>
																	<s:text	name="garuda.currentReqProgress.label.notprovided" />
																</s:else>
															</td>
															<td width="25%"><strong><s:text
																name="garuda.tcorderdetails.label.phforcontact2" /></strong>:</td>
															<td width="25%"><s:property value="%{#rowVal[40]}" />
															</td>
														</tr>
														<tr>
															<td width="25%"><strong><s:text
																name="garuda.tcorderdetails.label.mobileforcontact2" /></strong>:
															</td>
															<td width="25%"><s:property value="%{#rowVal[41]}" />
															</td>
															<td width="25%"><strong><s:text
																name="garuda.tcorderdetails.label.faxforcontact2" /></strong>:</td>
															<td width="25%"><s:property value="%{#rowVal[78]}" /></td>
														</tr>
													</s:iterator>
												</s:if>
											</table>
											</fieldset>
											<fieldset><legend><s:text
												name="garuda.tcorderdetails.label.deliverycont3" /></legend>
											<table>
												<s:if
													test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
													<s:iterator value="ctOrderDetailsList" var="rowVal">
														<tr>
															<td width="25%"><strong><s:text
																name="garuda.tcorderdetails.label.contactname3" /></strong>:</td>
															<td width="25%">
																<s:if test='%{#rowVal[42]!=null && #rowVal[42]!=" "}'>
																		<s:property value="%{#rowVal[42]}" />
																</s:if>
																<s:else>
																	<s:text	name="garuda.currentReqProgress.label.notprovided" />
																</s:else>
															</td>
															<td width="25%"><strong><s:text
																name="garuda.tcorderdetails.label.phforcontact3" /></strong>:</td>
															<td width="25%"><s:property value="%{#rowVal[43]}" />
															</td>
														</tr>
														<tr>
															<td width="25%"><strong><s:text
																name="garuda.tcorderdetails.label.mobileforcontact3" /></strong>:
															</td>
															<td width="25%"><s:property value="%{#rowVal[44]}" />
															</td>
															<td width="25%"><strong><s:text
																name="garuda.tcorderdetails.label.faxforcontact3" /></strong>:</td>
															<td width="25%"><s:property value="%{#rowVal[79]}" /></td>
														</tr>
													</s:iterator>
												</s:if>
											</table>
											</fieldset>
											</div>
											</td>
										</tr>
									</s:if>
									<s:if
												test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
										<s:iterator value="ctOrderDetailsList" var="rowVal">
									<tr>
										<td colspan="2">
										<fieldset><legend> <s:text
											name="garuda.tcorderdetails.label.ordersignature" /> </legend>
										<table>
											
													<tr>
														<td width="25%"><strong><s:text
															name="garuda.tcorderdetails.label.orderdate" /></strong>:</td>
														<td width="25%"><s:property value="%{#rowVal[45]}" />
														</td>
														<td width="25%"><strong><s:text
															name="garuda.tcorderdetails.label.orderingphysician" /></strong>:
														</td>
														<td width="25%">
															<s:if test='%{#rowVal[46]!=null && #rowVal[46]!=" "}'>
																		<s:property value="%{#rowVal[46]}" />
																</s:if>
																<s:else>
																	<s:text	name="garuda.currentReqProgress.label.notprovided" />
																</s:else>
														</td>
													</tr>
											
										</table>
										</fieldset>
										</td>
									</tr>
									</s:iterator>
								</s:if>
								</table>
								</div>
								</div>
								</div>
								</s:if>
							 <!-- TC Order Details --> <!-- CBU Availability Confirmation -->
							   <s:if test="hasViewPermission(#request.viewCbuAvailCon)==true">
								<div class="portlet" id="cbuavailconfbarDivparent">
								<div id="cbuavailconfbarDiv"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-print"
									onclick="getCordAvailDetails();addPrintContent('ctcbuavailconfbarDiv')"></span><span
									class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick"  id="cordAvailspan" onclick="getCordAvailDetails();"></span>
								<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
									class="ui-icon ui-icon-minusthick"></span>--> <s:text
									name="garuda.ctOrderDetail.portletname.cbuAvailconformation" />
								</div>
								<div class="portlet-content" id="ctcbuavailconfbarDiv" style="display: none;"><s:if
									test="cordAvailConformLst!=null && cordAvailConformLst.size()>0">
									<s:iterator value="cordAvailConformLst" var="rowVal">
										<table>
											<tr>
												<td width="50%"><strong><s:text
													name="garuda.cbuAvailconformation.label.cbuavailfornmdp" /></strong>:<span
													style="color: red;">*</span></td>
												<td width="50%"><s:if
													test="#rowVal[0]!=null && #rowVal[0]!=''">
													<s:if test='%{#rowVal[0]=="Y"}'>
														<s:text name="garuda.cbushipmetInfo.label.yes"></s:text>
													</s:if>
													<s:if test='%{#rowVal[0]=="N"}'>
														<s:text name="garuda.cbushipmetInfo.label.no"></s:text>
													</s:if>
												</s:if><s:hidden id="cordavailflag" value="%{#rowVal[4]}"></s:hidden></td>
											</tr>

											<s:if test='%{#rowVal[0]!=null && #rowVal[0]=="N"}'>
												<tr>
													<td width="50%"><strong><s:text
														name="garuda.cbuAvailconformation.label.unavailreason" />:</strong>
													</td>
													<td width="50%"><s:property value="%{#rowVal[1]}" /></td>
												</tr>
											</s:if>
											<s:if test="#rowVal[4]!=null">
												<tr>
													<td width="50%"><strong><s:text
														name="garuda.cbuAvailconformation.label.cbuavailconfirmdays" />:</strong>
													</td>
													<td width="50%"><s:property value="%{#rowVal[5]}" /></td>
												</tr>
											</s:if>
											<s:if test="%{(#rowVal[2]!=null && #rowVal[2]!='') && (#rowVal[3]!=null && #rowVal[3]!='')}">
											<tr>
												<td width="100%" colspan="2">
												<fieldset><legend> </legend>
												<table>
													<tr>
														<td width="25%"><strong><s:text
															name="garuda.cbuAvailconformation.label.submittedby" /></strong>:</td>
														<td width="25%"><s:property value="%{#rowVal[2]}" /></td>
														<td width="25%"><strong><s:text
															name="garuda.cbuAvailconformation.label.submitteddate" /></strong>:</td>
														<td width="25%"><s:property value="%{#rowVal[3]}" />
														</td>
													</tr>
												</table>
												</fieldset>
												</td>
											</tr>
											</s:if>
										</table>

										

									</s:iterator>
								</s:if>
								<table id="orAvailNotesTable">
											<tbody>
												<s:set name="cbuCateg"
													value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT,@com.velos.ordercomponent.util.VelosGarudaConstants@AVAIL_CNFRM_CODESUBTYPE) " />
												<s:iterator value="#request.categoryPfNotes[#cbuCateg]"
													var="rowVal" status="row">
													<s:set name="amendFlag1" value="%{#rowVal[6]}"
														scope="request" />
													<s:set name="tagNote" value="%{#rowVal[7]}" scope="request" />
													<s:set name="notes" value="%{#rowVal[8]}" scope="request" />
													<s:set name="cellValue6" value="cellValue" scope="request" />
													<tr>
														<td><s:if test="#request.tagNote==true">
															<img width="20" height="16" src="images/exclamation.jpg">
														</s:if></td>
														<s:iterator value="rowVal" var="cellValue" status="col">
															<s:if test="%{#col.index == 1}">
																<s:set name="cellValue1" value="cellValue"
																	scope="request" />
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
																 <%
														String creatorName="";
														if (request.getAttribute("cellValue1") != null
							 									&& !request
							 											.getAttribute("cellValue1")
							 											.equals("")) {
															creatorName = request.getAttribute(
							 										"cellValue1").toString();
							 							}
															
																						UserJB userB = new UserJB();
																						userB.setUserId(EJBUtil
																								.stringToNum(creatorName));
																						userB.getUserDetails();
														
														%> <%=userB.getUserLastName() + " "
												  + userB.getUserFirstName()%>
																</td>
															</s:if>
															<s:if test="%{#col.index == 2}">
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
																	name="garuda.cbu.order.progressNote" /><s:property /></td>
															</s:if>
															<s:if test="%{#col.index == 0}">
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
																	name="garuda.clinicalnote.label.posted_on"></s:text> <s:set
																	name="cellValue15" value="cellValue" /> <s:date
																	name="cellValue15" id="cellValue15"
																	format="MMM dd, yyyy / hh:mm:ss a" /> <s:property
																	value="cellValue15" /></td>
															</s:if>
															<s:if test="%{#col.index == 3}">

																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:text name="garuda.clinicalnote.label.subject"></s:text>:
																<s:property /></td>
															</s:if>
															<s:if test="%{#col.index == 4}">
														
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:set name="cellValue3" value="cellValue"
																	scope="request" /> <s:iterator
																	value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]"
																	var="subType">


																	<%-- <s:if test="subType==#cellValue">
																		<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
																		<s:property value="description" />
																	</s:if> --%>
																</s:iterator>
															
															</td>
														</s:if>

															<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
															<s:if test="%{#col.index == 5}">
																<td><a href="#" onClick="loadMoredivs('revokePfCategoryNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&pkcordId=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&orderType=<s:property value="orderType"/>&orderId=<s:property value="orderId"/>&category=<s:property value="#request.cbuCateg"/>','orAvailNotesTable,openOrdernotesContent','ororderdetailsForm')">
																<s:text name="garuda.clinicalnote.label.revoke" /></a></td>
															</s:if>
															</s:if>

															<s:if test="%{#col.index == 5}">
																<td><a href="#"
																	onClick="fn_showModalORdetails('Progress Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfViewNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property />','500','850');">
																<s:text name="garuda.clinicalnote.label.viewnotes" /></a></td>
															</s:if>

														</s:iterator>
													</tr>
													<tr>
													<td></td>
															<td colspan="6" <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<div style="overflow: auto;">	<s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
														</div>	</td>
													<tr>
												</s:iterator>

											</tbody>
											<tfoot>
												<tr>
													<td colspan="6">
														<a href="#"
															onclick="shwMdal('AVAIL_CNFRM_CODESUBTYPE','<s:property value="cdrCbuPojo.registryId"/>','orAvailNotesTable');"><s:text
															name="garuda.cdrcbuview.label.button.addnewnotes" /></a>
													</td>
												</tr>
											</tfoot>

										</table> 
								<div <s:if test="orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED) || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)">style="display:none"</s:if> >
								   <s:if test="hasEditPermission(#request.viewCbuAvailCon)==true">
								<table>
									<tr>
										<s:if test="cordAvailConformLst!=null && cordAvailConformLst.size()>0">
									<s:iterator value="cordAvailConformLst" var="rowVal">
										<s:if test="#rowVal[0]==null || #rowVal[0]==''">
										 <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
    </s:if>
    <s:else>
											<td align="right"><input type="button" id="saveCbuavail"
												onclick="savecordAvailNmdp()"
												value="<s:text name="garuda.cbbprocedures.label.edit" />" />
											</td>
											</s:else>
										</s:if>
										</s:iterator>
										</s:if>
									</tr>
								</table>
								   </s:if>
								</div>
								</div>
								</div>
								</div>
								</s:if>
							<!-- CBU Availability confirmation --> 
							  <s:if test="hasViewPermission(#request.viewPatienttc)==true">
								<div class="portlet" id="openOrderrecTcInfoparent">
								<div id="openOrderrecTcInfo"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-print"
									onclick="getCaseManagerPatientDetails();addPrintContent('openOrderrecTcInfo')"></span><span
									class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id='openOrderrecTcInfospan' onclick="getCaseManagerPatientDetails();"></span>
								<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
									class="ui-icon ui-icon-minusthick"></span>--> <s:text
									name="garuda.ctOrderDetail.portletname.recipient&tcInfo" /></div>
								<div class="portlet-content"
									id="caseManagerPatientandTCdetailsDiv" style="display: none;">
								<table>
									<s:if
										test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
										<s:iterator value="ctOrderDetailsList" var="rowVal">
											<tr>
												<td colspan="2">
												<table width="100%">
													<tr>
														<td width="25%" align="left"></td>
														<td width="25%" align="left"></td>
														<td width="50%" align="right" colspan="2">
															<input type="button"
																value='<s:text name="garuda.recipient&tcInfo.label.button.viewantigens"/>'
																onclick="showAntigensModal('<s:property value="%{#rowVal[13]}"/>','<s:property value="%{#rowVal[87]}"/>');" />
														</td>

													</tr>
													<tr>
														<td width="25%" align="left"><strong><s:text
															name="garuda.recipient&tcInfo.label.recipientid" /></strong>:</td>
														<td width="25%" align="left"><s:property
															value="%{#rowVal[13]}" /></td>
														<td width="50%" align="right" colspan="2"></td>
													</tr>
													<tr>
														<td width="25%" align="left"><strong><s:text
															name="garuda.recipient&tcInfo.label.age" /></strong>:</td>
														<td width="25%" align="left"><s:property
															value="%{#rowVal[11]}" /></td>
														<td width="25%" align="left"><strong><s:text
															name="garuda.recipient&tcInfo.label.tcname" /></strong>:</td>
														<td width="25%" align="left"><s:property
															value="%{#rowVal[7]}" /></td>
													</tr>
													<tr>
														<td width="25%" align="left"><strong><s:text
															name="garuda.recipient&tcInfo.label.sex" /></strong>:</td>
														<td width="25%" align="left"><s:property
															value="%{#rowVal[12]}" /></td>
														<td width="25%" align="left"><strong><s:text
															name="garuda.recipient&tcInfo.label.tcno" /></strong>:</td>
														<td width="25%" align="left"><s:property
															value="%{#rowVal[8]}" /></td>
													</tr>
													<tr>
														<td width="25%" align="left"><strong><s:text
															name="garuda.tcorderdetails.label.currentdiagnosis" /></strong>:</td>
														<td width="25%" align="left"><s:property
															value="%{#rowVal[86]}" /></td>
														<td width="25%" align="left"><strong><s:text
															name="garuda.recipient&tcInfo.label.sectcno" /></strong>:</td>
														<td width="25%" align="left"><s:property
															value="%{#rowVal[9]}" /></td>
													</tr>
												</table>
												</td>
											</tr>
										</s:iterator>
									</s:if>
								</table>
								<table>
									<s:if
										test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
										<s:iterator value="ctOrderDetailsList" var="rowVal">
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.caseMangerInfo.label.cmid" /></strong>:</td>
												<td width="25%"><s:property value="%{#rowVal[15]}" /></td>
												<td width="25%"><strong><s:text
													name="garuda.caseMangerInfo.label.cmemail" /></strong>:</td>
												<td width="25%"><a
													href='mailto:<s:property value="%{#rowVal[17]}"/>'><s:property
													value="%{#rowVal[17]}" /></a></td>
											</tr>
											<tr>

												<td width="25%"><strong><s:text
													name="garuda.caseMangerInfo.label.cmname" /></strong>:</td>
												<td width="25%"><s:property value="%{#rowVal[16]}" /></td>
												<td width="25%"><strong><s:text
													name="garuda.caseMangerInfo.label.cmphno" /></strong>:</td>
												<td width="25%"><s:property value="%{#rowVal[18]}" /></td>
											</tr>
											<tr>
												<td width="100%" colspan="4" align="right">
													<input type="button"
														value='<s:text name="garuda.recipient&tcInfo.label.button.cmdirectory"/>'
														onclick="readNMDPXML('1')" />
												</td>
											</tr>
										</s:iterator>
									</s:if>
									<!--<s:else>
											<tr>
					    						<td width="25%">
					    							<strong><s:text name="garuda.caseMangerInfo.label.cmid"/></strong>:
					    						</td>
					    						<td width="25%">
					    							
					    						</td>
					    						<td width="25%">
													<strong><s:text name="garuda.caseMangerInfo.label.cmemail"/></strong>:
												</td>
												<td width="25%">
													
												</td>
					    					</tr>
											<tr>
												
												<td width="25%">
					    							<strong><s:text name="garuda.caseMangerInfo.label.cmname"/></strong>:
					    						</td>
					    						<td width="25%">
					    							
					    						</td>
												<td width="25%">
													<strong><s:text name="garuda.caseMangerInfo.label.cmphno"/></strong>:
												</td>
												<td width="25%">
													
												</td>
											</tr>
										</s:else>-->
								</table>
								</div>
								</div>
								</div>
						         </s:if>
								<s:if test="hasViewPermission(#request.viewreqclincinfo)==true">
								<div class="portlet" id="reqclincinfobarDivparent">
								<div id="reqclincinfobarDiv"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-print"
									onclick="addPrintContent('reqclincinfobarDiv')"></span><span
									class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id="reqclinspanid"></span>
								<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
									class="ui-icon ui-icon-minusthick"></span>--> <s:text
									name="garuda.ctOrderDetail.portletname.requiredClinicalInfo" />
								</div>
								<div class="portlet-content" style="display: none;" id="ctReqClinInfoContentDiv">
								<table>
									<tr>
										<td><s:if
											test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
											<s:iterator value="ctOrderDetailsList" var="rowVal">
												<s:hidden name="completeReqInfoTaskFlag"
													id="completeReqInfoTaskFlag" value="%{#rowVal[63]}"></s:hidden>
											</s:iterator>
										</s:if>
										<table width="100%">
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.requiredClinicalInfo.label.checkliststatus" /></strong>:
												</td>
												<td width="25%"><s:hidden name="clinchkstat" /> <s:if
													test="clinchkstat==null">
													<label style="color: red;"><s:text
														name="garuda.requiredClinicalInfo.label.notstarted" /></label>
												</s:if> <s:elseif test="clinchkstat==0">
													<label style="color: blue;"><s:text
														name="garuda.requiredClinicalInfo.label.pending" /></label>
												</s:elseif> <s:elseif test="clinchkstat==1">
													<label style="color: green;"><s:text
														name="garuda.requiredClinicalInfo.label.completed" /></label>
												</s:elseif></td>
												  <s:if test="hasViewPermission(#request.cmpltReqInfo)==true">
												<td width="50%" colspan="2"><strong><a
													href="#"
													onclick="f_callCompleReqInfo('<s:property value="cdrCbuPojo.registryId"/>','<s:property value="cdrCbuPojo.cordID" />','<s:property value="orderId"/>','<s:property value="orderType"/>');"><s:text
													name="garuda.requiredClinicalInfo.label.clinicalinfostatus" /></a></strong>
												</td>
												</s:if>
											</tr>
											<tr>
												<td width="25%"></td>
												<td width="25%"></td>
												<td width="25%"></td>
												<td width="25%"></td>
											</tr>
										</table>
										</td>

									</tr>
								</table>
								</div>
								</div>
								</div>
								</s:if>
							<s:if test="hasViewPermission(#request.viewFnlCBURvw)==true"> 
								<div class="portlet" id="orOrderfinalcbureviewparentDiv">
								<div id="finalcbureviewbarDiv"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-print"
									onclick="addPrintContent('finalcbureviewbarDiv')"></span><span
									class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id="finalreviewspanid"></span>
								<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
									class="ui-icon ui-icon-minusthick"></span>--> <s:text
									name="garuda.orOrderDetail.portletname.finalcbureview" /></div>
								<div class="portlet-content" style="display: none;" id="finalreviewcontentDiv">
								<table>
									<tr>
										<td colspan="2"><s:if
											test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
											<s:iterator value="ctOrderDetailsList" var="rowVal">
												<s:hidden name="finalReviewTaskFlag"
													id="finalReviewTaskFlag" value="%{#rowVal[62]}"></s:hidden>
											</s:iterator>
										</s:if>
										<table width="100%">
											<tr>
												<td width="50%" colspan="2"><strong><s:text
													name="garuda.cbuShipment.label.finalreviewstatus" /></strong>:</td>
												<td width="25%"><s:if
													test="finalReview!=null && finalReview.size()>0">
													<s:iterator value="finalReview" var="rowVal" status="row">
														<s:if test='%{#rowVal[0]=="Y"}'>
															<label style="color: green;"><s:text
																name="garuda.requiredClinicalInfo.label.completed" /></label>
															<s:set name="finalReviewFlag" value="%{#rowVal[0]}" scope='request'/>
																<s:hidden id="finalReviewFlag" value="%{#request.finalReviewFlag}"/>
														</s:if>
														<s:elseif test='%{#rowVal[0]=="N"}'>
															<label style="color: blue;"><s:text
																name="garuda.requiredClinicalInfo.label.pending" /></label>
														</s:elseif>
														<s:else>
															<label style="color: red;"><s:text
																name="garuda.requiredClinicalInfo.label.notstarted" /></label>
														</s:else>
													</s:iterator>
												</s:if> <s:else>
													<label style="color: red;"><s:text
														name="garuda.requiredClinicalInfo.label.notstarted" /></label>
												</s:else></td>
												<td width="25%"><strong><a href="#"
													onclick="showFinalCbuReviewInfo();"><s:text
													name="garuda.cbuShipment.label.gofinalreview" /></a></strong></td>

											</tr>
											<s:if test="finalReview!=null && finalReview.size()>0">
											<s:iterator value="finalReview" var="rowVal" status="row">
											<s:if test='%{#rowVal[0]=="Y"}'>
											<tr>
												<td width="50%" colspan="2"><strong><s:text
													name="garuda.cbuAvailconformation.label.finalreviewconfirmdays"></s:text>:</strong>
												</td>
												<td width="25%">
														<s:property value="%{#rowVal[2]}" />
												</td>
												<td width="25%"></td>
											</tr>
											</s:if>
											</s:iterator>
											</s:if>
										</table>
										</td>

									</tr>
								</table>
								</div>
								</div>
								</div>
								</s:if>
							</td>
							<td width="1%"></td>
							<td width="49.5%" style="vertical-align: top">
							<s:if test="hasViewPermission(#request.viewCBUShipmnt)==true"> 
							<div class="portlet" id="ctshipmentbarDivparent">
								<div id="ctshipmentbarDiv"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div 
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-print"
									onclick="getcbushimentDetails();addPrintContent('cbuShipmentInfoContentDiv');"></span><span
									class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id="cbushipspan" onclick="getcbushimentDetails();"></span>
								<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
									class="ui-icon ui-icon-minusthick"></span>--> <s:text
									name="garuda.ctOrderDetail.portletname.cbuShipment" /></div>
								<div class="portlet-content" id="cbuShipmentInfoContentDiv" style="display: none;">
								<s:hidden name="pkCbuShipment" id="pkCbuShipment"></s:hidden>
								<table width="100%" style="table-layout: fixed;">
									<s:if test="shipmentInfoLst!=null && shipmentInfoLst.size()>0">
										<s:iterator value="shipmentInfoLst" var="rowVal">
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.cbuShipment.label.cbusheduledshipdate" /></strong>:<span
													style="color: red;">*</span></td>
												<td width="25%"><s:hidden name="schShipmentDate"
													id="datepicker15" value="%{#rowVal[0]}"></s:hidden> <s:property
													value="%{#rowVal[0]}" /></td>
												
												<td width="25%" id="cbushimentdayDiv"><strong><s:text
													name="garuda.cbushipmetInfo.label.day"></s:text></strong>:<span
													style="font-style: italic; color: blue"></span></td>
												<td width="25%"><s:if
													test="%{(#rowVal[9]==null || #rowVal[9]=='Not Provided') && orderPojo.resolByTc!=pkCanceled}">
													<span style="font-style: italic; color: red;"><s:text
														name="garuda.cbushipmetInfo.label.notconfirm"></s:text></span>
												</s:if> <s:elseif
													test="%{(#rowVal[9]==null || #rowVal[9]=='Not Provided') && orderPojo.resolByTc==pkCanceled}">
													<span
														style="font-style: italic; color: red; font-size: 10px"><s:text
														name="garuda.ctshipmetInfo.label.shipmentcancelled"></s:text></span>
												</s:elseif> <s:elseif
													test="%{(#rowVal[9]!=null && #rowVal[9]!='Not Provided') && orderPojo.resolByTc==pkCanceled}">
													<span
														style="font-style: italic; color: red; font-size: 10px"><s:text
														name="garuda.ctshipmetInfo.label.shipmentcancelled"></s:text></span>
												</s:elseif> <s:elseif
													test="%{(#rowVal[29]!=null && #rowVal[29]!='Not Provided') && orderPojo.resolByTc!=pkCanceled}">
													<span style="font-style: italic; color: green;"><s:text
														name="garuda.cbushipmetInfo.label.confirm"></s:text></span>
												</s:elseif></td>
											</tr>
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.cbuShipment.label.cbusheduledshiptime" /></strong>:</td>
												<td width="25%"><s:hidden
													name="shipmentPojo.shipmentTime" value="%{#rowVal[16]}"
													id="shedShipmentTime"></s:hidden> <s:property
													value="%{#rowVal[16]}" /></td>
												<s:if test="#rowVal[17]!=null">
												<td width="25%"><strong><s:text
														name="garuda.cbuShipment.label.noofdaystoshipcbu" /></strong>:</td>
												<td width="25%"><s:hidden name="noofdaystoshipcbu"
														id="noofdaystoshipcbu" value="%{#rowVal[17]}"></s:hidden>
													<s:property value="%{#rowVal[17]}" /></td>
												</s:if>
												<s:else>
													<td width="25%"></td>
													<td width="25%"></td>
												</s:else>
											</tr>
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.cbuShipment.label.shipcompany" /></strong>:</td>
												<td width="25%"><s:hidden
													name="shipmentPojo.fkshipingCompanyId"
													id="fkshipingCompanyId" value="%{#rowVal[2]}"></s:hidden> <s:property
													value="getCodeListDescById(#rowVal[2])" /></td>
												<td width="25%"><strong><s:text
													name="garuda.ctShipmentInfo.label.trackingno" /></strong>:</td>
												<td width="25%"><div style="width: 99%" class="alignmentClass"><s:hidden
													name="shipmentPojo.shipmentTrackingNo"
													id="shipmentTrackingNo" value="%{#rowVal[3]}"></s:hidden><a href="#"
													onclick='connectToShipperWebsite($j("#fkshipingCompanyId").val(),$j("#shipmentTrackingNo").val());'><s:property
													value="%{#rowVal[3]}" /></a></div></td>
											</tr>
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.cbuShipment.label.shippingcontainer" /></strong>:</td>
												<td width="25%"><s:hidden
													name="shipmentPojo.fkShippingContainer"
													id="fkShippingContainer" value="%{#rowVal[7]}"></s:hidden>
												<s:property value="getCodeListDescById(#rowVal[7])" /></td>
												<td width="25%"><strong><s:text
													name="garuda.cbuShipment.label.tempmonitor" /></strong>:</td>
												<td width="25%"><s:hidden
													name="shipmentPojo.fkTempMoniter" id="fkTempMoniter"
													value="%{#rowVal[8]}"></s:hidden> <s:property
													value="getCodeListDescById(#rowVal[8])" /></td>
											</tr>
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.cbuShipment.label.actualshipdate" /></strong>:</td>
												<td width="25%">
												<s:if test='%{#rowVal[29]!=null && #rowVal[29]!=""}'>
													<s:hidden
													name="shipmentPojo.shipmentDate" id="actshipmentDate"
													value="%{#rowVal[9]}"></s:hidden><s:property
													value="%{#rowVal[9]}" />
												</s:if>
												<s:else>
													<s:text name="garuda.currentReqProgress.label.notprovided"></s:text>
												</s:else>
												</td>
												<td width="25%"><strong><s:text
													name="garuda.cbuShipment.label.anticipatedarrivaldate" /></strong>:
												</td>
												<td width="25%"><s:property value="%{#rowVal[10]}" /></td>
											</tr>
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.cbuShipment.label.shipperpadlockcombination" /></strong>:
												</td>
												<td width="25%"><div style="width: 99%" class="alignmentClass"><s:hidden
													name="shipmentPojo.padlockCombination"
													id="padlockCombination" value="%{#rowVal[20]}"></s:hidden>
												<s:property value="%{#rowVal[20]}" /></div></td>
												<td width="25%"><strong><s:text
													name="garuda.cbuShipment.label.additishipperdet" /></strong>:</td>
												<td width="25%"><div style="width: 99%" class="alignmentClass"><s:hidden
													name="shipmentPojo.additiShiiperDet" id="additiShiiperDet"
													value="%{#rowVal[19]}"></s:hidden> <s:property
													value="%{#rowVal[19]}" /></div></td>
											</tr>
											<tr>
												<td width="25%"><strong><s:text
													name="garuda.cbuShipment.label.paerworkloc" /></strong>:</td>
												<td width="25%"><s:hidden
													name="shipmentPojo.paperworkLoc" id="paperworkLoc"
													value="%{#rowVal[18]}"></s:hidden> <s:property
													value="getCodeListDescById(#rowVal[18])" /></td>
												<td width="50%" colspan="2">														
												<div id="shipmentItinerarychkIdDiv">												
												<s:if test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPMENT_ITER_CODESUBTYPE].size > 0">
													<s:checkbox name="shipmentItinerarychkname"
														id="shipmentItinerarychkId" checked="checked"
														disabled="true">
														<a href="#"
															onclick="fn_showModalORdetails('Shipment Itinerary','getShipmentItern?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />','400','800');"><strong><s:text
															name="garuda.cbuShipment.label.itineraryrec"></s:text></strong></a>
													</s:checkbox>
												</s:if> <s:else>
													<s:checkbox name="shipmentItinerarychkname"
														id="shipmentItinerarychkId" disabled="true">
														<strong><s:text
															name="garuda.cbuShipment.label.itineraryrec"></s:text></strong>
													</s:checkbox>
												</s:else>
												</div></td>
											</tr>
											<s:if test="%{#rowVal[15]!=null && #rowVal[15]!=''}">
												<tr>
													<td colspan="4" width="100%">
													<fieldset><legend> </legend>
													<table>
														<tr>
															<td width="25%"><strong><s:text
																name="garuda.cbuShipment.label.shipmentconformedby" /></strong>:
															</td>
															<td width="25%"><s:property value="%{#rowVal[14]}" />
															</td>
															<td width="25%"><strong><s:text
																name="garuda.cbuShipment.label.shipmentconformeddt" /></strong>:
															</td>
															<td width="25%"><s:property value="%{#rowVal[15]}" />
															</td>
														</tr>
													</table>
													</fieldset>
													</td>
												</tr>
											</s:if>
											<tr>
												<td width="100%" colspan="4">
												
												</td>
											</tr>
											<tr>
												<td width="25%" id="cbushipmentbarDiv">
												<s:if test="shipmentInfoLst!=null && shipmentInfoLst.size()>0">
													<div <s:if test="orderPojo.iscordavailfornmdp==@com.velos.ordercomponent.business.util.VelosMidConstants@FLAG_N || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED) || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)">style="display:none"</s:if>>
													<s:iterator value="shipmentInfoLst" var="rowVal">
														<s:if test='%{(#rowVal[12]!="Y" || #rowVal[11]!="Y")}'>
															<s:if test="#rowVal[31]!=null && #rowVal[31]<=0">
																<input type="button"
																	value='<s:text name="garuda.cbuShipment.button.label.confirmshpment" />'
																	onclick="showEsignconfirmShipment(this);" />
															</s:if>
															<s:else>
																<input type="button"
																	value='<s:text name="garuda.cbuShipment.button.label.confirmshpment" />'
																	disabled="disabled" />
															</s:else>
														</s:if>
														<div id="confirmShipmentDiv" style="display: none;">
														<table bgcolor="#cccccc"
															class="tabledisplay disable_esign" id="">
															<tr bgcolor="#cccccc" valign="baseline">
																<td width="70%"><jsp:include
																	page="cb_esignature.jsp" flush="true">
																	<jsp:param value="submitconfirmShipment"
																		name="submitId" />
																	<jsp:param value="invalidconfirmShipment"
																		name="invalid" />
																	<jsp:param value="minimumconfirmShipment"
																		name="minimum" />
																	<jsp:param value="passconfirmShipment" name="pass" />
																</jsp:include></td>
																<td align="left" width="30%"><input type="button"
																	id="submitconfirmShipment"
																	onclick="confirmShipmentInfo();"
																	value="<s:text name="garuda.unitreport.label.button.submit"/>"
																	disabled="disabled" /> <input type="button"
																	id="closediv"
																	onclick="$j('#confirmShipmentDiv').hide();"
																	value="<s:text name="garuda.common.close"/>" /></td>
															</tr>
														</table>
														</div>
													</s:iterator>
													</div>
												</s:if>
											</td>
												<td width="25%"></td>
												<td width="25%"></td>
												<td width="25%">
												<s:if test="shipmentInfoLst!=null && shipmentInfoLst.size()>0">
													<s:iterator value="shipmentInfoLst" var="rowVal">
														<s:if test='%{#rowVal[11]=="Y"}'>
														<s:if test='#request.finalReviewFlag == "Y"'>
														<div <s:if test="orderPojo.iscordavailfornmdp==@com.velos.ordercomponent.business.util.VelosMidConstants@FLAG_N || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED) || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)">style="display:none"</s:if>>
															<input type="button"
																value='<s:text name="garuda.productinsert.label.product_productinsert"/>'
																onclick="printShipmentPocket();"/>
																
														</div>
														</s:if>
														</s:if>
													</s:iterator>
												</s:if></td>
											</tr>
											<s:hidden id="shipschFlag" value="%{#rowVal[11]}"></s:hidden>
											<s:hidden id="packageslipFlag" value="%{#rowVal[13]}"></s:hidden>
											<s:hidden id="cordShipedFlag" value="%{#rowVal[12]}"></s:hidden>
										</s:iterator>
									</s:if>
									<s:else>
										<tr>
											<td width="25%"><strong><s:text
												name="garuda.cbuShipment.label.cbusheduledshipdate" /></strong>:<span
												style="color: red;">*</span></td>
											<td width="25%"></td>
											<s:if test='orderPojo.resolByTc!=pkCanceled'>
												<td width="25%"><strong><s:text
													name="garuda.cbushipmetInfo.label.day"></s:text></strong>:</td>
											</s:if>
											<s:if test='orderPojo.resolByTc==pkCanceled'>
												<td width="25%"><span
													style="font-style: italic; color: red;"><s:text
													name="garuda.ctshipmetInfo.label.shipmentcancelled"></s:text></span>
												</td>
											</s:if>
											<td width="25%" id="cbushimentdayDiv"><s:if
												test='orderPojo.resolByTc==pkCanceled'>
												<strong><s:text
													name="garuda.cbushipmetInfo.label.day"></s:text></strong>:
			    							</s:if> <span style="font-style: italic; color: blue"></span></td>
										</tr>
										<tr>
											<td width="25%"><strong><s:text
												name="garuda.cbuShipment.label.cbusheduledshiptime" /></strong>:</td>
											<td width="25%"></td>
											<td width="25%"></td>
											<td width="25%"></td>
										</tr>

										<tr>
											<td width="25%"><strong><s:text
												name="garuda.cbuShipment.label.shipcompany" /></strong>:</td>
											<td width="25%"></td>
											<td width="25%"><strong><s:text
												name="garuda.ctShipmentInfo.label.trackingno" /></strong>:</td>
											<td width="25%"></td>
										</tr>
										<tr>
											<td width="25%"><strong><s:text
												name="garuda.cbuShipment.label.shippingcontainer" /></strong>:</td>
											<td width="25%"></td>
											<td width="25%"><strong><s:text
												name="garuda.cbuShipment.label.tempmonitor" /></strong>:</td>
											<td width="25%"></td>
										</tr>
										<tr>
											<td width="25%"><strong><s:text
												name="garuda.cbuShipment.label.actualshipdate" /></strong>:</td>
											<td width="25%"></td>
											<td width="25%"><strong><s:text
												name="garuda.cbuShipment.label.anticipatedarrivaldate" /></strong>:</td>
											<td width="25%"><s:text
												name="garuda.currentReqProgress.label.notprovided" /></td>
										</tr>
										<tr>
											<td width="25%"><strong><s:text
												name="garuda.cbuShipment.label.shipperpadlockcombination" /></strong>:
											</td>
											<td width="25%"></td>
											<td width="25%"><strong><s:text
												name="garuda.cbuShipment.label.additishipperdet" /></strong>:</td>
											<td width="25%"></td>
										</tr>
										<tr>
											<td width="25%"><strong><s:text
												name="garuda.cbuShipment.label.paerworkloc" /></strong>:</td>
											<td width="25%"></td>
											<td width="50%" colspan="2">											
											<div id="shipmentItinerarychkIdDiv">
											<s:if
												test="#request.categoryMap[@com.velos.ordercomponent.util.VelosGarudaConstants@SHIPMENT_ITER_CODESUBTYPE].size>0">
												<s:checkbox name="shipmentItinerarychkname"
													id="shipmentItinerarychkId" checked="checked"
													disabled="true">
													<a href="#"
														onclick="fn_showModalORdetails('Shipment Itinerary','getShipmentItern?cdrCbuPojo.cordID =<s:property value="cdrCbuPojo.cordID" />&cdrCbuPojo.cdrCbuId=<s:property value="cdrCbuPojo.cdrCbuId" />&cdrCbuPojo.registryId=<s:property value="cdrCbuPojo.registryId" />','400','800');"><strong><s:text
														name="garuda.cbuShipment.label.itineraryrec"></s:text></strong></a>
												</s:checkbox>
											</s:if> <s:else>
												<s:checkbox name="shipmentItinerarychkname"
													id="shipmentItinerarychkId" disabled="true">
													<strong><s:text
														name="garuda.cbuShipment.label.itineraryrec"></s:text></strong>
												</s:checkbox>
											</s:else>
											</div></td>
										</tr>
									</s:else>
								</table>
								<table id="cbuShipmentNotesTable">
												<tbody>
													<s:set name="cbuCateg"
														value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT,@com.velos.ordercomponent.util.VelosGarudaConstants@CBU_SHHIPMENT_CODESUBTYPE) " />
													<s:iterator value="#request.categoryPfNotes[#cbuCateg]"
														var="rowVal" status="row">
														<s:set name="amendFlag1" value="%{#rowVal[6]}"
															scope="request" />
														<s:set name="tagNote" value="%{#rowVal[7]}"
															scope="request" />
															<s:set name="notes" value="%{#rowVal[8]}"
															scope="request" />
														<s:set name="cellValue6" value="cellValue" scope="request" />
														<tr>
															<td><s:if test="#request.tagNote==true">
																<img width="20" height="16" src="images/exclamation.jpg">
															</s:if></td>
															<s:iterator value="rowVal" var="cellValue" status="col">
																<s:if test="%{#col.index == 1}">
																	<s:set name="cellValue1" value="cellValue"
																		scope="request" />
																	<td
																		<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																	<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
																<%
														String creatorName="";
														if (request.getAttribute("cellValue1") != null
							 									&& !request
							 											.getAttribute("cellValue1")
							 											.equals("")) {
															creatorName = request.getAttribute(
							 										"cellValue1").toString();
							 							}
															
																						UserJB userB = new UserJB();
																						userB.setUserId(EJBUtil
																								.stringToNum(creatorName));
																						userB.getUserDetails();
														
														%> <%=userB.getUserLastName() + " "
												  + userB.getUserFirstName()%>
																	</td>
																</s:if>
																<s:if test="%{#col.index == 2}">
																	<td
																		<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
																		name="garuda.cbu.order.progressNote" /><s:property /></td>
																</s:if>
																<s:if test="%{#col.index == 0}">
																	<td
																		<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
																		name="garuda.clinicalnote.label.posted_on"></s:text> <s:set
																		name="cellValue15" value="cellValue" /> <s:date
																		name="cellValue15" id="cellValue15"
																		format="MMM dd, yyyy / hh:mm:ss a" /> <s:property
																		value="cellValue15" /></td>
																</s:if>
																<s:if test="%{#col.index == 3}">

																	<td
																		<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																	<s:text name="garuda.clinicalnote.label.subject"></s:text>:
																	<s:property /></td>
																</s:if>
																<s:if test="%{#col.index == 4}">
														
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:set name="cellValue3" value="cellValue"
																	scope="request" /> <s:iterator
																	value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]"
																	var="subType">


																	<%-- <s:if test="subType==#cellValue">
																		<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
																		<s:property value="description" />
																	</s:if> --%>
																</s:iterator>
															
																	</td>
																</s:if>

																<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
																<s:if test="%{#col.index == 5}">
																	<td><a href="#" onClick="loadMoredivs('revokePfCategoryNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&pkcordId=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&orderType=<s:property value="orderType"/>&orderId=<s:property value="orderId"/>&category=<s:property value="#request.cbuCateg"/>','cbuShipmentNotesTable,orProgressNotesTable','ororderdetailsForm')">
																<s:text name="garuda.clinicalnote.label.revoke" /></a></td>
																</s:if>
																</s:if>
																<s:if test="%{#col.index == 5}">
																	<td><a href="#"
																		onClick="fn_showModalORdetails('Progress Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfViewNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property />','500','850');">
																	<s:text name="garuda.clinicalnote.label.viewnotes" /></a>
																	</td>
																</s:if>

															</s:iterator>
														</tr>
														<tr>
														<td></td>
															<td colspan="6" <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																	<div style="overflow: auto;"><s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
															</div></td>
														<tr>
													</s:iterator>
												</tbody>
											</table>
											<s:if test='orderPojo.resolByTc!=pkCanceled'>
											<table>
											<tr>
												<td width="100%" colspan="4">
													<a href="#"
														onclick="shwMdal('CBU_SHHIPMENT_CODESUBTYPE','<s:property value="cdrCbuPojo.registryId"/>','cbuShipmentNotesTable');"><s:text
														name="garuda.cdrcbuview.label.button.addnewnotes" /></a>
												</td>
											</tr>
											<tr>
												<td width="100%" colspan="4">
												
												</td>
											</tr>
											</table>
											</s:if>
										<s:if test="shipmentInfoLst!=null && shipmentInfoLst.size()>0">
										<s:iterator value="shipmentInfoLst" var="rowVal">
										<s:if test='%{#rowVal[12]!="Y"}'>
												
												<div <s:if test="orderPojo.iscordavailfornmdp==@com.velos.ordercomponent.business.util.VelosMidConstants@FLAG_N || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED) || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)">style="display:none"</s:if>>
												<table>
													<tr>
													  <s:if test="hasEditPermission(#request.viewCBUShipmnt)==true">
													   <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
   														 </s:if>
   															 <s:else>
														<td align="right"><input type="button"
															id="editOrCbuShipment"
															onclick="openModalForCbuShipment();"
															value="<s:text name="garuda.common.lable.edit"/>" /></td>
															</s:else>
													 </s:if>	
													</tr>
												</table>
												</div>
											</s:if>
											</s:iterator>
											</s:if>
											<s:else>
											<div <s:if test="orderPojo.iscordavailfornmdp==@com.velos.ordercomponent.business.util.VelosMidConstants@FLAG_N || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@RESOLVED) || orderPojo.orderStatus==getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.business.util.VelosMidConstants@ORDER_STATUS,@com.velos.ordercomponent.business.util.VelosMidConstants@CLOSED)">style="display:none"</s:if>>
												<table>
													<tr>
													  <s:if test="hasEditPermission(#request.viewCBUShipmnt)==true">
													  <s:if test='%{(cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@SH) || cdrCbuPojo.fkCordCbuStatus == getCbuStatusPkByCode(@com.velos.ordercomponent.util.VelosGarudaConstants@IN))}'>
    </s:if>
    <s:else>
														<td align="right"><input type="button"
															id="editOrCbuShipment"
															onclick="openModalForCbuShipment();"
															value="<s:text name="garuda.common.lable.edit"/>" /></td>
															</s:else>
													 </s:if>	
													</tr>
												</table>
												</div>
											</s:else>
											
								</div>
								</div>
								</div>
								</s:if>
								
							   <s:if test="hasViewPermission(#request.viewORResolution)==true">
								<div class="portlet" id="ctresolutionbarDivparent">
								<div id="ctresolutionbarDiv"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-print"
									onclick="getResolutionDetails('orresolsubDiv');addPrintContent('orresolsubDiv')"></span><span
									class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id="ctResoultionspan" onclick="getResolutionDetails('orresolsubDiv');"></span>
								<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
									class="ui-icon ui-icon-minusthick"></span>--> <s:text
									name="garuda.orOrderDetail.portletname.orResolution" /></div>
								<div id="orresolsubDiv" class="portlet-content" style="display: none;"><s:hidden
									name="cancelled_ack_orders" id="canackorders"></s:hidden> <s:if
									test="ctOrderDetailsList!=null && ctOrderDetailsList.size()>0">
									<s:iterator value="ctOrderDetailsList" var="rowVal">
										<s:hidden name="resolByTCFlag" id="resolByTCFlag"
											value="%{#rowVal[57]}"></s:hidden>
										<s:hidden name="resolByCBBFlag" id="resolByCBBFlag"
											value="%{#rowVal[58]}"></s:hidden>
									</s:iterator>
								</s:if> <s:if test="orderResolLst!=null && orderResolLst.size>0">
									<s:iterator value="orderResolLst" var="rowVal">
										<table width="100%">
											<tr>
												<td width="40%"><strong><s:text
													name="garuda.currentReqProgress.label.closereason" /></strong>:</td>
												<td width="40%"><s:if
													test="%{#rowVal[10]!=null && #rowVal[10]!=''}">
													<s:property value="getCbuStatusDescByPk(#rowVal[10])" />
													<s:hidden name="orderCloseRsn" id="orderCloseRsn"
														value="%{#rowVal[10]}"></s:hidden>
												</s:if></td>
												<td width="20%"></td>
											</tr>
											<tr>
												<td colspan="3">
												<table cellpadding="0" cellspacing="0" border="0">
													<tr>
														<td width="40%"><strong><s:text
															name="garuda.heResolution.label.resolutiondate" /></strong>:</td>
														<td width="40%"><s:if
															test="%{#rowVal[2]!=null && #rowVal[2]!=''}">
															<s:property value="%{#rowVal[2]}" />
														</s:if></td>
														<td width="20%"></td>
													</tr>
												</table>
												</td>
											</tr>

											<tr>
												<td colspan="3"><s:if
													test="%{#rowVal[1]!=null && #rowVal[1]!=''}">
													<table id="resolBytc">
														<tr>
															<td width="40%"><strong><s:text
																name="garuda.ctResolution.label.resolutionbytc" /></strong>:</td>
															<td width="40%"><s:if
																test="%{#rowVal[1]!=null && #rowVal[1]!=''}">
																<s:hidden name="tcResolVal" id="tcResolVal"
																	value="%{#rowVal[1]}" />
																<s:hidden name="ResolutionTC" id="ResolutionTC"
																	value="%{#rowVal[1]}"></s:hidden>
																<s:property value="getCbuStatusDescByPk(#rowVal[1])" />
															</s:if></td>
															<td width="20%"></td>
														</tr>
														<tr>
															<td width="40%"><strong><s:text
																name="garuda.ctResolution.label.acknowledgetcresolution" /></strong>:<span
																style="color: red;">*</span></td>
															<td width="40%"><s:if test='%{#rowVal[9]!=null && #rowVal[9]=="Y"}'>
															<s:checkbox name="ResoluAck"
															id="ResoluAck" disabled="true" checked="true"/><s:text
															name="garuda.ctResolution.label.resolutionbytc" /> <s:hidden
															name="acknowledgeFlag" id="acknowledgeFlag"
															value="%{#rowVal[9]}"></s:hidden>
														</s:if>
														<s:else>
															<s:checkbox name="ResoluAck"
															id="ResoluAck" disabled="true" /><s:text
															name="garuda.ctResolution.label.resolutionbytc" />
														</s:else></td>
															<td width="20%"></td>
														</tr>


														<s:if test="%{#rowVal[9]!=null && #rowVal[9]!=''}">
															<tr>
																<td width="40%"><strong><s:text
																	name="garuda.cbu.order.ackDateTime" /></strong>:</td>
																<td width="40%"><s:if
																	test="%{#rowVal[6]!=null && #rowVal[6]!=''}">
																	<s:property value="%{#rowVal[6]}" />
																</s:if></td>
																<td width="20%"></td>
															</tr>
															<tr>
																<td width="40%"><strong><s:text
																	name="garuda.cbu.order.ackBy" /></strong>:</td>
																<td width="40%">
																<s:if test="%{#rowVal[7]!=null && #rowVal[7]!='' && #rowVal[7]!=0}">
																<s:set name="username"
																	value="%{#rowVal[7]}" scope="request" /> 
																	 <%
 																			String cellValue1 = "";
 																			if (request.getAttribute("username") != null && !request.getAttribute("username").equals("")) {
 																					cellValue1 = request.getAttribute("username").toString();
 																				}
 																			UserJB userB = new UserJB();
 																			userB.setUserId(EJBUtil.stringToNum(cellValue1));
 																			userB.getUserDetails();
 																	%> <%=userB.getUserLastName() + " " + userB.getUserFirstName()%> 
																</s:if>
																<s:if test="%{#rowVal[7]==null || #rowVal[7]==0}">
														  			<s:text name="garuda.cbuhistory.label.system" />
																</s:if>
																</td>
																<td width="20%"></td>
															</tr>
														</s:if>
														<s:else>
															<%
																String cellValue1 = "";
 																			if (request.getAttribute("username") != null && !request.getAttribute("username").equals("")) {
 																					cellValue1 = request.getAttribute("username").toString();
 																				}
																UserJB userB = new UserJB();
																userB.setUserId(EJBUtil.stringToNum(cellValue1));
																userB.getUserDetails();
															%>
															<input type="hidden" name="currentuser"
																id="currentuserid" value=<%=logUsr%>> 
														</s:else>
													</table>

												</s:if> <s:elseif
													test="%{ {{#rowVal[1]==null || #rowVal[1]==''} && {#rowVal[0]==null || #rowVal[0]==''}} || {#rowVal[0]!=null && #rowVal[0]!=''} }">
													<table cellpadding="0" cellspacing="0" border="0"
														id="resolByCbb">
														<tr>
															<td width="40%"><strong><s:text
																name="garuda.ctResolution.label.resolutionbycbb" /></strong>:</td>
															<td width="40%"><s:if
																test="%{#rowVal[0]!=null && #rowVal[0]!=''}">
																<s:property value="getCbuStatusDescByPk(#rowVal[0])" />
															</s:if></td>
															<td width="20%"></td>
															<s:hidden name="hresolcbb" id="hresolcbb"
																value="%{#rowVal[0]}" />
														</tr>
														<s:if test="%{#rowVal[0]!=null && #rowVal[0]!=''}">
															<tr>
																<td width="40%"><strong><s:text
																	name="garuda.ctResolution.label.acknowledgetcresolution" /></strong>:<span
																	style="color: red;">*</span></td>
																<td width="40%">
																<s:if test='%{#rowVal[9]!=null && #rowVal[9]=="Y"}'>
															<s:checkbox name="ResoluAck"
															id="ResoluAck" disabled="true" checked="true"/><s:text
															name="garuda.ctResolution.label.resolutionbytc" /> <s:hidden
															name="acknowledgeFlag" id="acknowledgeFlag"
															value="%{#rowVal[9]}"></s:hidden>
														</s:if>
														<s:else>
															<s:checkbox name="ResoluAck"
															id="ResoluAck" disabled="true" /><s:text
															name="garuda.ctResolution.label.resolutionbytc" />
														</s:else></td>
																<td width="20%"></td>
															</tr>
														</s:if>


														<s:if test="%{#rowVal[9]!=null && #rowVal[9]!=''}">
															<tr>
																<td width="40%"><strong><s:text
																	name="garuda.cbu.order.ackDateTime" /></strong>:</td>
																<td width="40%"><s:if
																	test="%{#rowVal[6]!=null && #rowVal[6]!=''}">
																	<s:property value="%{#rowVal[6]}" />
																</s:if></td>
																<td width="20%"></td>
															</tr>
															<tr>
																<td width="40%"><strong><s:text
																	name="garuda.cbu.order.ackBy" /></strong>:</td>
																<td width="40%">
																<s:if test="%{#rowVal[7]!=null && #rowVal[7]==0}">
																	<s:set name="username"
																	value="%{#rowVal[7]}" scope="request" />  
																	<%
 																	String cellValue1 = "";
 																			if (request.getAttribute("username") != null && !request.getAttribute("username").equals("")) {
 																					cellValue1 = request.getAttribute("username").toString();
 																				}
 																	UserJB userB = new UserJB();
																	userB.setUserId(EJBUtil.stringToNum(cellValue1));
 																	userB.getUserDetails();
 																	%> 
 																	
 																	<%=userB.getUserLastName() + " " + userB.getUserFirstName()%> 
 																</s:if>
 																<s:elseif  test="%{#rowVal[7]==null || #rowVal[7]==0}">
														  			<s:text name="garuda.cbuhistory.label.system" />
																</s:elseif>
																</td>
																<td width="20%"></td>
															</tr>
														</s:if>
														<s:else>
															 <%
																String cellValue1 = "";
 																			if (request.getAttribute("username") != null && !request.getAttribute("username").equals("")) {
 																					cellValue1 = request.getAttribute("username").toString();
 																				}
																UserJB userB = new UserJB();
																userB.setUserId(EJBUtil.stringToNum(cellValue1));
																userB.getUserDetails();
															%>
															<input type="hidden" name="currentuser"
																id="currentuserid" value=<%=logUsr%>> 
														</s:else>
													</table>
												</s:elseif></td>
											</tr>
										</table>
									</s:iterator>
								</s:if>
								<table id="orResolutionNotesTable">
													<tbody>
													<s:set name="cbuCateg"
														value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT,@com.velos.ordercomponent.util.VelosGarudaConstants@RESOLUTION_CODESUBTYPE) " />
													<s:iterator value="#request.categoryPfNotes[#cbuCateg]"
														var="rowVal" status="row">
														<s:set name="amendFlag1" value="%{#rowVal[6]}"
															scope="request" />
														<s:set name="tagNote" value="%{#rowVal[7]}"
															scope="request" />
															<s:set name="notes" value="%{#rowVal[8]}"
															scope="request" />
														<s:set name="cellValue6" value="cellValue" scope="request" />
														<tr>
															<td style="padding:5px"><s:if test="#request.tagNote==true">
																<img width="20" height="16" src="images/exclamation.jpg">
															</s:if></td>
															<s:iterator value="rowVal" var="cellValue" status="col">
																<s:if test="%{#col.index == 1}">
																	<s:set name="cellValue1" value="cellValue"
																		scope="request" />
																	<td
																		<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if> style="padding:5px">
																	<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
																 <%
														String creatorName="";
														if (request.getAttribute("cellValue1") != null
							 									&& !request
							 											.getAttribute("cellValue1")
							 											.equals("")) {
															creatorName = request.getAttribute(
							 										"cellValue1").toString();
							 							}
															
																						UserJB userB = new UserJB();
																						userB.setUserId(EJBUtil
																								.stringToNum(creatorName));
																						userB.getUserDetails();
														
														%> <%=userB.getUserLastName() + " "
												  + userB.getUserFirstName()%>
																	</td>
																</s:if>
																<s:if test="%{#col.index == 2}">
																	<td
																		<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if> style="padding:5px"><s:text
																		name="garuda.cbu.order.progressNote" /><s:property /></td>
																</s:if>
																<s:if test="%{#col.index == 0}">
																	<td
																		<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if> style="padding:5px"><s:text
																		name="garuda.clinicalnote.label.posted_on"></s:text> <s:set
																		name="cellValue15" value="cellValue" /> <s:date
																		name="cellValue15" id="cellValue15"
																		format="MMM dd, yyyy / hh:mm:ss a" /> <s:property
																		value="cellValue15" /></td>
																</s:if>
																<s:if test="%{#col.index == 3}">

																	<td
																		<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if> style="padding:5px">
																	<s:text name="garuda.clinicalnote.label.subject"></s:text>:
																	<s:property /></td>
																</s:if>
																<s:if test="%{#col.index == 4}">
														
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:set name="cellValue3" value="cellValue"
																	scope="request" /> <s:iterator
																	value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]"
																	var="subType">


																	<%-- <s:if test="subType==#cellValue">
																		<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
																		<s:property value="description" />
																	</s:if> --%>
																</s:iterator>
															
																</td>
															</s:if>

																<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
																<s:if test="%{#col.index == 5}">
																	<td style="padding:5px"><a href="#" onClick="loadMoredivs('revokePfCategoryNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&pkcordId=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&orderType=<s:property value="orderType"/>&orderId=<s:property value="orderId"/>&category=<s:property value="#request.cbuCateg"/>','orResolutionNotesTable,orProgressNotesTable','ororderdetailsForm')">
																<s:text name="garuda.clinicalnote.label.revoke" /></a></td>
																</s:if>
																</s:if>
																<s:if test="%{#col.index == 5}">
																	<td style="padding:5px"><a href="#"
																		onClick="fn_showModalORdetails('Progress Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfViewNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property />','500','850');">
																	<s:text name="garuda.clinicalnote.label.viewnotes" /></a>
																	</td>
																</s:if>

															</s:iterator>
														</tr>
														<tr>
														<td></td>
														<td colspan="6" <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
															<div style="overflow: auto;"><s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
														</div></td>
													<tr>
													</s:iterator>
												</tbody>
													<tfoot>
														<tr>
															<td colspan="6">
															<a href="#"
														onclick="shwMdal('RESOLUTION_CODESUBTYPE','<s:property value="cdrCbuPojo.registryId"/>','orResolutionNotesTable');"><s:text
														name="garuda.cdrcbuview.label.button.addnewnotes" /></a>
															</td>
														</tr>
													</tfoot>

												</table>
								<s:if test="hasEditPermission(#request.viewORResolution)==true"> 
								<s:if test="orderResolLst!=null && orderResolLst.size()>0">
									<s:iterator value="orderResolLst" var="rowVal">
										<s:if test='((orderPojo.resolByCbb!= null && orderPojo.resolByCbb!="") || (orderPojo.resolByTc!=null && orderPojo.resolByTc!=""))'>
									<s:if test='%{#rowVal[9]==null || #rowVal[9]=="" || #rowVal[9]=="N"}'>
											<table>
												<tr>
													<td align="right">
													<input type="button"
														onclick="checkResolution();"
														value="<s:text name="garuda.common.lable.edit"/>"
														id="submitOrResolution" />
												    </td>
												</tr>
											</table>
											</s:if>
										</s:if>
									</s:iterator>
								</s:if>
								</s:if>
								</div>
								</div>
								</div>
								</s:if>
								
						       <s:if test="hasViewPermission(#request.viewRqstCBUhis)==true">
								<div class="portlet" id="openOrdercbuhisparent">
								<div id="openOrdercbuhis"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-print"
									onclick="printHistoryWidget('openOrdercbuhis','<s:property value="cdrCbuPojo.registryId"/>');"></span><span
									class="ui-icon ui-icon-newwin"></span> <span id="cbu-req-history" class="ui-icon ui-icon-plusthick orderHistorydata"></span>
								<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
									class="ui-icon ui-icon-minusthick"></span>--> <s:text
									name="garuda.ctOrderDetail.portletname.cbunrequesthistory" /></div>
								<div class="portlet-content" style="display: none;">
								<table>
									<tr>
										<td colspan="2"><jsp:include page="cb_cbuHistory.jsp"></jsp:include>
										</td>

									</tr>
								</table>
								</div>
								</div>
								</div>
								</s:if>

							<!-- Progress Notes--> 
							 <s:if test="hasViewPermission(#request.viewPrgrssNotes)==true">
								<div class="portlet" id="openOrdernotesparent">
								<div id="openOrdernotes"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-print"
									onclick="addPrintContent('openOrdernotes')"></span><span
									class="ui-icon ui-icon-newwin"></span> <span id="progress-notes" class="ui-icon ui-icon-plusthick"></span>
								<!--<span class="ui-icon ui-icon-minusthick"></span>--> <s:text
									name="garuda.ctOrderDetail.portletname.progressnotes" /></div>
								<div class="portlet-content" id="openOrdernotesContent" style="display: none;">
								<table id="orProgressNotesTable">
									<s:iterator  value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT]">
												<s:set name="cbuCateg1" value="pkCodeId" scope="request"/>
										<s:iterator value="#request.categoryPfNotes[#request.cbuCateg1]" var="rowVal" status="row">
											<s:set name="amendFlag1" value="%{#rowVal[6]}"
												scope="request" />
											<s:set name="tagNote" value="%{#rowVal[7]}" scope="request" />
											<s:set name="notes" value="%{#rowVal[8]}" scope="request" />
											<s:set name="cellValue6" value="cellValue" scope="request" />
											<tr>
												<td><s:if test="#request.tagNote==true">
													<img width="20" height="16" src="images/exclamation.jpg">
												</s:if></td>
												<s:iterator value="rowVal" var="cellValue" status="col">
													<s:if test="%{#col.index == 1}">
														<s:set name="cellValue1" value="cellValue" scope="request" />
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
														<%
														String creatorName="";
														if (request.getAttribute("cellValue1") != null
							 									&& !request
							 											.getAttribute("cellValue1")
							 											.equals("")) {
															creatorName = request.getAttribute(
							 										"cellValue1").toString();
							 							}
															
																						UserJB userB = new UserJB();
																						userB.setUserId(EJBUtil
																								.stringToNum(creatorName));
																						userB.getUserDetails();
														
														%> <%=userB.getUserLastName() + " "
												  + userB.getUserFirstName()%> </td>
													</s:if>
													<s:if test="%{#col.index == 2}">
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
															name="garuda.cbu.order.progressNote" /><s:property /></td>
													</s:if>
													<s:if test="%{#col.index == 0}">
														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
															name="garuda.clinicalnote.label.posted_on"></s:text> <s:set
															name="cellValue15" value="cellValue" /> <s:date
															name="cellValue15" id="cellValue15"
															format="MMM dd, yyyy / hh:mm:ss a" /> <s:property
															value="cellValue15" /></td>
													</s:if>
													<s:if test="%{#col.index == 3}">

														<td
															<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
														<s:text name="garuda.clinicalnote.label.subject"></s:text>:
														<s:property /></td>
													</s:if>
													<s:if test="%{#col.index == 4}">
														
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:set name="cellValue3" value="cellValue"
																	scope="request" /> <s:iterator
																	value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]"
																	var="subType">


																	<%-- <s:if test="subType==#cellValue">
																		<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
																		<s:property value="description" />
																	</s:if> --%>
																</s:iterator>
															
														</td>
													</s:if>

													<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
													<s:if test="%{#col.index == 5}">
														<td><a href="#"
															onClick="loadMoredivs('revokePfCategoryNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_REVOKE_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&pkcordId=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&orderType=<s:property value="orderType"/>&orderId=<s:property value="orderId"/>&category=<s:property value="#request.cbuCateg"/>','orAvailNotesTable,cbuShipmentNotesTable,orResolutionNotesTable,nmdpResearchSampleTable,openOrdernotesContent','orOrderDetailsForm')">
														<s:text name="garuda.clinicalnote.label.revoke" /></a></td>
													</s:if>
													</s:if>

													<s:if test="%{#col.index == 5}">
														<td><a href="#"
															onClick="fn_showModalORdetails('Progress Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfViewNote?moduleEntityId=<s:property value="cdrCbuPojo.cordID" />&moduleEntityType=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_ET_CLINICAL_NOTES" />&moduleEntityIdentifier=<s:property value="cdrCbuPojo.registryId" />&moduleEntityEventCode=<s:property value="@com.velos.ordercomponent.util.SessionLoggingConstants@MODULE_EC_VIEW_NOTE" />&cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property />','500','850');">
														<s:text name="garuda.clinicalnote.label.viewnotes" /></a></td>
													</s:if>

												</s:iterator>
											</tr>
											<tr>
											<td></td>
													<td colspan="6" <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
															<div style="overflow: auto;"><s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
													</div></td>
											<tr>
											</s:iterator>
										</s:iterator>

								</table>
							
									<a href="#"
										onclick="shwMdal1('<s:property value="cdrCbuPojo.registryId"/>','orAvailNotesTable,cbuShipmentNotesTable,orResolutionNotesTable,nmdpResearchSampleTable');"><s:text
										name="garuda.cdrcbuview.label.button.addnewnotes" /></a>
								</div>
								</div>
								</div>
								</s:if>
                                <s:if test="hasViewPermission(#request.updtNmdpSmpl)==true && NMDPResearchSampleFlag==true">
								<div class="portlet" id="nmdpresearchSampleDivparent">
								<div id="nmdpresearchSampleDiv"
									class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
								<div
									class="portlet-header ui-widget-header ui-widget-content ui-corner-all"
									style="text-align: center;"><span
									class="ui-icon ui-icon-print"
									onclick="getnmdpshipmentdetails();addPrintContent('nmdpresearchSampleDiv')"></span><span
									class="ui-icon ui-icon-newwin"></span> <span class="ui-icon ui-icon-plusthick" id="nmdpshipspan" onclick="getnmdpshipmentdetails();"></span>
								<!--<span class="ui-icon ui-icon-close"></span>--> <!--<span
									class="ui-icon ui-icon-minusthick"></span>--> <s:text
									name="garuda.orOrderDetail.portletname.nmdpresearchsample" /></div>
								<div class="portlet-content shipmentInfoTbl" id="nmdpResearchSampleContentDiv" style="display: none;">
								<s:hidden name="pkNmdpShipment" id="pkNmdpShipment"></s:hidden>
								<table>
									<s:if test="nmdpShipmentInfoLst!=null && nmdpShipmentInfoLst.size()>0">
										<s:iterator value="nmdpShipmentInfoLst" var="rowVal">
											<tr>
												<td colspan="2">
												<table width="100%">
													<tr>
														<td width="25%"><strong><s:text
															name="garuda.cbuShipment.label.researchsampleshipdate" /></strong>:</td>
														<td width="25%"><s:if
															test="%{#rowVal[0]=='Jan 01, 1900'}">
														</s:if> <s:else>
															<s:if test="%{#rowVal[0]!=null && #rowVal[0]!=''}">
																<s:hidden name="shipmentPojo.researchSampleShipDate"
																	id="datepicker3" value="%{#rowVal[0]}"></s:hidden>
																<s:property value="%{#rowVal[0]}" />
															</s:if>
														</s:else></td>
														<td width="25%"><strong><s:text
															name="garuda.cbuShipment.label.shipcompany" /></strong>:<span id="shipcompmandspan" 
															style="color: red;">*</span></td>
														<td width="25%"><s:if
															test="%{#rowVal[2]!=null && #rowVal[2]!=''}">
															<s:hidden name="shipmentPojo.fkshipingCompanyId"
																id="fkNmdpshipingCompanyId" value="%{#rowVal[2]}"></s:hidden>
															<s:property value="getCodeListDescById(#rowVal[2])" />
														</s:if> </td>
													</tr>
													<tr>
														<td width="25%"><s:if
															test="%{#rowVal[2]!=null && #rowVal[2]!=''}">
															<strong><s:text
																name="garuda.ctShipmentInfo.label.trackingno" /></strong>:
					    						</s:if> <s:else>
															<strong><s:text
																name="garuda.ctShipmentInfo.label.trackingno" /></strong>:
					    						</s:else></td>
														<td width="25%"><s:if
															test="%{#rowVal[3]!=null && #rowVal[3]!=''}">
															<s:hidden
																name="shipmentPojo.researchSampleshipmentTrackingNo"
																id="researchSampleshipmentTrackingNo"
																value="%{#rowVal[3]}"></s:hidden>
															<a href="#"
																onclick='connectToShipperWebsite($j("#fkNmdpshipingCompanyId").val(),$j("#researchSampleshipmentTrackingNo").val());'><s:property value="%{#rowVal[3]}" /></a>
														</s:if></td>
														<td width="50%" colspan="2"><s:if
															test='%{#rowVal[23]!=null && #rowVal[23]=="Y"}'>
															<s:checkbox name="nmdpShipExcuse" id="nmdpShipExcuse"
																checked="true" onclick="disableShipmentDet();"
																disabled="true">
																<strong><s:text
																	name="garuda.cbuShipment.label.samplenotshippedexcuse"></s:text></strong>
															</s:checkbox>
														</s:if> <s:else>
															<s:checkbox name="nmdpShipExcuse" id="nmdpShipExcuse"
																onclick="disableShipmentDet();" disabled="true">
																<strong><s:text
																	name="garuda.cbuShipment.label.samplenotshippedexcuse"></s:text></strong>
															</s:checkbox>
														</s:else></td>
													</tr>
													<s:if test='%{#rowVal[23]!=null && #rowVal[23]=="Y"}'>
														<tr>
															<td width="25%"><strong><s:text
																name="garuda.cbuShipment.label.excuseformsubmitby"></s:text></strong>:
															</td>
															<td width="25%"><s:if test="%{#rowVal[32]!=null}">
																<s:set name="excuseFormSubmittedBy"
																	value="%{#rowVal[32]}" scope="request" />
																 <%
																	String finaldetails = request.getAttribute("excuseFormSubmittedBy").toString();
																	UserJB user = new UserJB();
																	user.setUserId(EJBUtil.stringToNum(finaldetails));
																	user.getUserDetails();
																%>
																<%=user.getUserLastName() + " " + user.getUserFirstName()%> 
															</s:if></td>
															<td width="25%"><strong><s:text
																name="garuda.cbuShipment.label.excuseformsubmitdatetime"></s:text></strong>:
															</td>
															<td width="25%"><s:property value="%{#rowVal[33]}" />
															</td>
														</tr>
													</s:if>
													<tr>
														<td width="100%" colspan="4"><a href="#"
															onclick="readNMDPXML('2')"><strong><s:text
															name="garuda.cbuShipment.label.researchexcuseform"></s:text></strong></a>
														</td>
													</tr>
												</table>
												</td>
											</tr>
										</s:iterator>
									</s:if>
									<s:else>
										<tr>
											<td colspan="2">
											<table width="100%">
												<tr>
													<td width="25%"><strong><s:text
														name="garuda.cbuShipment.label.researchsampleshipdate" /></strong>:
													</td>
													<td width="25%"></td>
													<td width="25%"><strong><s:text
														name="garuda.cbuShipment.label.shipcompany" /></strong>:</td>
													<td width="25%"></td>
												</tr>
												<tr>
													<td width="25%"><strong><s:text
														name="garuda.ctShipmentInfo.label.trackingno" /></strong>:</td>
													<td width="25%"></td>
													<td width="50%" colspan="2"><s:checkbox
														name="nmdpShipExcuse" id="nmdpShipExcuse"
														onclick="disableShipmentDet();" disabled="true">
														<strong><s:text
															name="garuda.cbuShipment.label.samplenotshippedexcuse"></s:text></strong>
													</s:checkbox></td>

												</tr>
												<tr>
													<td width="100%" colspan="4" id="researchExcuseDiv"><a
														href="#" onclick="readNMDPXML('2')"><strong><s:text
														name="garuda.cbuShipment.label.researchexcuseform"></s:text></strong></a>
														<a href="https://network.nmdp.org/PPP/research_facts_sheet.pdf" ><img height="15px"  src="./images/help_24x24px.png" onmouseover="return overlib('<s:text name="garuda.orOrderDetail.portletname.formoredetails"/>')" onmouseout="return nd()"  /></a>
													</td>
												</tr>
											</table>
											</td>
										</tr>

									</s:else>
									
								</table>
									<table id = "nmdpResearchSampleTable">
							
										<tr><td>
										<s:set name="cbuCateg"
													value="getCodeListPkByTypeAndSubtype(@com.velos.ordercomponent.util.VelosGarudaConstants@PF_NOTE_CAT,@com.velos.ordercomponent.util.VelosGarudaConstants@RESEARCH_SMP_CODESUBTYPE) " />
										
												<s:iterator value="#request.categoryPfNotes[#cbuCateg]"
													var="rowVal" status="row">
													<s:set name="amendFlag1" value="%{#rowVal[6]}"
														scope="request" />
													<s:set name="tagNote" value="%{#rowVal[7]}" scope="request" />
													<s:set name="notes" value="%{#rowVal[8]}" scope="request" />
													<s:set name="cellValue6" value="cellValue" scope="request" />
													<tr>
														<td><s:if test="#request.tagNote==true">
															<img width="20" height="16" src="images/exclamation.jpg">
														</s:if></td>
														<s:iterator value="rowVal" var="cellValue" status="col">
															<s:if test="%{#col.index == 1}">
																<s:set name="cellValue1" value="cellValue"
																	scope="request" />
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:text name="garuda.clinicalnote.label.posted_by"></s:text>
																 <%
														String creatorName="";
														if (request.getAttribute("cellValue1") != null
							 									&& !request
							 											.getAttribute("cellValue1")
							 											.equals("")) {
															creatorName = request.getAttribute(
							 										"cellValue1").toString();
							 							}
															
																						UserJB userB = new UserJB();
																						userB.setUserId(EJBUtil
																								.stringToNum(creatorName));
																						userB.getUserDetails();
														
														%> <%=userB.getUserLastName() + " "
												  + userB.getUserFirstName()%>
																</td>
															</s:if>
															<s:if test="%{#col.index == 2}">
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
																	name="garuda.cbu.order.progressNote" /><s:property /></td>
															</s:if>
															<s:if test="%{#col.index == 0}">
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>><s:text
																	name="garuda.clinicalnote.label.posted_on"></s:text> <s:set
																	name="cellValue15" value="cellValue" /> <s:date
																	name="cellValue15" id="cellValue15"
																	format="MMM dd, yyyy / hh:mm:ss a" /> <s:property
																	value="cellValue15" /></td>
															</s:if>
															<s:if test="%{#col.index == 3}">

																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:text name="garuda.clinicalnote.label.subject"></s:text>:
																<s:property /></td>
															</s:if>
															<s:if test="%{#col.index == 4}">
														
																<td
																	<s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																<s:set name="cellValue3" value="cellValue"
																	scope="request" /> <s:iterator
																	value="#application.codeListValues[@com.velos.ordercomponent.util.VelosGarudaConstants@NOTE_VISIBILITY]"
																	var="subType">


																	<%-- <s:if test="subType==#cellValue">
																		<s:text name="garuda.clinicalnote.label.publicPrivate"></s:text>:
																		<s:property value="description" />
																	</s:if> --%>
																</s:iterator>
															
															</td>
														</s:if>

															<s:if test="hasEditPermission(#request.vPrgrssNote)==true">
															<s:if test="%{#col.index == 5}">
																<td><a href="#" onClick="loadMoredivs('revokePfCategoryNote?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&pkcordId=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property/>&orderType=<s:property value="orderType"/>&orderId=<s:property value="orderId"/>&category=<s:property value="#request.cbuCateg"/>','nmdpResearchSampleTable,orProgressNotesTable','ororderdetailsForm')">
																<s:text name="garuda.clinicalnote.label.revoke" /></a></td>
															</s:if>
															</s:if>

															<s:if test="%{#col.index == 5}">
																<td><a href="#"
																	onClick="fn_showModalORdetails('Progress Notes for CBU Registry ID <s:property value="cdrCbuPojo.registryId"/>','pfViewNote?cdrCbuPojo.cordID=<s:property value="cdrCbuPojo.cordID" />&clinicalNotePojo.pkNotes=<s:property />','500','850');">
																<s:text name="garuda.clinicalnote.label.viewnotes" /></a></td>
															</s:if>

														</s:iterator>
													</tr>
													<tr>
															<td></td>
															<td colspan="6"  <s:if test="#request.amendFlag1==true">style="text-decoration:line-through;"</s:if>>
																	<div style="overflow: auto;"><s:text name="garuda.cbu.label.note" /><strong><s:property escapeHtml="false" value ="#request.notes"/></strong>
															</div></td>
													<tr>
												</s:iterator>

											</tbody>
											<tfoot>
												<tr>
													<td colspan="6">
														<a href="#"
															onclick="shwMdal('RESEARCH_SMP_CODESUBTYPE','<s:property value="cdrCbuPojo.registryId"/>','nmdpResearchSampleTable');"><s:text
															name="garuda.cdrcbuview.label.button.addnewnotes" /></a>
													</td>
												</tr>
											</tfoot>		
											
									
									</table>
									<table>
										<tr>
										   <s:if test="hasEditPermission(#request.updtNmdpSmpl)==true">
											<td align="right">
												<div>
												<input type="button"
													id="submitSampleShipment"
													onclick="openmodaltoeditnmdpshipment();"
													value="<s:text name="garuda.common.lable.edit"/>" />
												</div>
											</td>
										 </s:if>
										</tr>
									</table>
								</div>
								</div>
								</div>
								</s:if>
						</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</div>
			</div>
			</div>
			</td>
		</tr>
	</table>
	<div id="dummyDiv"></div>
	<div id="printDiv" style="hidden: true"></div>
</s:form>