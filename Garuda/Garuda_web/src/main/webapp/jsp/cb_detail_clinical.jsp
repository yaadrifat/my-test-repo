<%@ taglib prefix="s"  uri="/struts-tags"%>
<jsp:useBean id ="paginateSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id ="paginationSearch" scope="request" class="com.velos.ordercomponent.business.pojoobjects.PaginateSearch"/>
<jsp:useBean id="grpRights" scope="session" class="com.velos.eres.web.grpRights.GrpRightsJB"/>
<jsp:useBean id="userLB" scope="session" class="com.velos.eres.web.user.UserJB"/>
<jsp:include page="cb_user_rights.jsp"></jsp:include>
<jsp:include page="widget-setting.jsp">
  <jsp:param value="16" name="pageId"/>
</jsp:include>
<jsp:include page="cb_track_session_logging.jsp" />
<script>
var toggledivstatusflag=true;
function closeDiv(divid)
    {	
       document.getElementById(divid).style.display = 'none';
    }
function togglelocalDiv(divid)
{   $j("#"+divid).toggle();	
		     if(toggledivstatusflag){
		        $j("#toggleicon").removeClass("ui-icon ui-icon-minusthick").addClass("ui-icon ui-icon-plusthick");
		         toggledivstatusflag=false;
			 }
			else{			 
			   $j("#toggleicon").removeClass("ui-icon ui-icon-plusthick").addClass("ui-icon ui-icon-minusthick");  
		        toggledivstatusflag=true;
			  }
		
}	
function settingwidth(){
	var staticpanelwidth=$j("#tablelookup1").width();
	var columnwidth=(staticpanelwidth/2);
	var datatablewidth=columnwidth-20;	
	var staticpanelheight=$j(".viewcbutoppanel").height();
	$j('.detailhstry').css('max-height',staticpanelheight);
	$j('.datatable').dataTable({
		"bSort": false,
		"sScrollX": datatablewidth,
        "bDeferRender": true
		});
	$j('.datatablewithoutsearch').dataTable({
		"bFilter": false,
		"bLengthChange": false,
	    "bSort": false,
	    "bInfo": false,
		"sScrollX": datatablewidth,
        "bDeferRender": true
		});	
}
function showDiv(cordId,fkCbbId,userSiteId){
	$j('#recordDiv1').css('display','none');
	refreshDiv("getMain2CbuDetails?cordId="+cordId,"searchTbles1","form1");
	$j('#recordDiv').css('display','block');
	mainonload();
	//settingwidth();
	
	if($j('#completeReqInfoFlag').val()=="1"){
		$j('#finalReviewBtn').removeAttr('disabled');
	}
	else{
		$j('#finalReviewBtn').attr('disabled', true);
		}
}

function loadGroup(cordId,fkCbbId,userSiteId,userId)
{
	var url="getGroupsByIds?fkCbbId="+fkCbbId+"&cordId="+cordId+"&userSiteId="+userSiteId+"&userId="+userId;
	//alert(url);
	loadPageByGetRequset(url,'usersitegrpdropdown');
	$j('#searchTbles1').css('display','none');
}

function highlightContent(el, inputs){
	var firstDiv = $j(el).find("div:first");
    $j(firstDiv).remove();
    var text = $j(el).text();
    text = text.replace(inputs ,"<span class='highlightCont'>"+inputs+"</span>");
    $j(el).html("<div style='display:none'>"+$j(firstDiv).html()+"</div>"+text);
}
$j(function() {
	//mainonload();
	//lookupDataTable();
	var temp="";
	forAllLookUpGrid();
	//settingwidth();
});
</script>
<s:hidden name="lookup_search_txt" id="lookup_search_txt"/>
<s:hidden name="lookup_tbl_sentries" id="lookup_tbl_sentries"/>
<s:hidden name="lookup_tbl_sort_col" id="lookup_tbl_sort_col"/>
<s:hidden name="lookup_tbl_sort_type" id="lookup_tbl_sort_type"/>
<div class="col_100 maincontainer "><div class="col_100">
<div class="column">	
		<div class="portlet" id="searchresultparent"  ><div id="searchresult" class="portletstatus ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" style="display:block;" >
		<div class="portlet-header ui-widget-header ui-widget-content ui-corner-all">			
		<div>
		<span class="ui-icon ui-icon-close" onclick="closeDiv('searchresult')"></span><span id="toggleicon" class="ui-icon ui-icon-minusthick" onclick="togglelocalDiv('searchTblediv')"></span></div><s:text name="garuda.cbuentry.label.cdrcbusearchresult" />
		</div><div class="portlet-content">
<s:form name="Tableform" id="Tableform">
<s:hidden name="inputs" id="inputs" value="%{inputId}"></s:hidden>	
</s:form>

    <div id="searchTblediv">
         <jsp:include page="cb_clinical_pagination_data.jsp"></jsp:include>
	</div> 
</div></div>
  </div>
</div>
   <s:if test="cordInfoList!=null && cordInfoList.size()==1">
          <script>
			closeDiv('searchresult');
		  </script>
	<div id="cordSelectedData" >
	   <jsp:include page="cb_cord_detail_with_rights.jsp">
	     <jsp:param value="true" name="widgetSetting"/>
	   </jsp:include>
	</div>
   </s:if>
   <s:else>
     <div id="cordSelectedData">	   
	</div>
   </s:else>
</div>
</div>
<div style="display: none;" id="modalEsign1">
   <table bgcolor="#cccccc" width="100%">
         <tr bgcolor="#cccccc" valign="baseline">
		   <td width="50%">
                 <table width="98%" border="0" cellspacing="0" cellpadding="0" bgcolor="#cccccc">
					<tr valign="baseline" bgcolor="#cccccc">
					    <td>
						<span id="cordIdEntryValid1" style="display: none;" class="validation-fail"><s:text name="garuda.common.esign.invalid"/></span>
						<span id="cordIdEntryMinimum1" style="display: none;" class="validation-fail"><s:text name="garuda.common.esign.minimumLength"/></span>
						<span id="cordIdEntryPass1" style="display: none;" class="validation-pass"><s:text name="garuda.common.esign.valid"/></span>
						</td>
						<td nowrap="nowrap">
						<s:text name="garuda.esignature.lable.esign" /><span style="color:red;">*</span></td>
						<td><s:password name="userPojo.signature" id="esignAutoDefer" size="10" style="font-style:bold; color:#000000;" onkeyup="validateSign(this.value,'submitcdrsearch1','cordIdEntryValid1','cordIdEntryMinimum1','cordIdEntryPass1')" cssStyle="width:100px"></s:password>
						</td>		
					</tr>
				</table>
			</td>
	           <td width="50%" align="right" valign="top">
	           <%--  <s:if test="(deferedCord==null || deferedCord==false)"> --%>
	              <input type="button" id="submitcdrsearch1" disabled="disabled" value="<s:text name="garuda.unitreport.label.button.submit"/>"/>            
	         <%--   </s:if> --%>
	           <input type="button" id="esign_cancel" value="<s:text name="garuda.common.lable.cancel"/>"/>					          
	       </td>
        </tr>
   </table>	
</div>